/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import * as Op from '@/io.ox/office/textframework/utils/operations';

// constants ==============================================================

const LOREM_IPSUM_STRING = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';

const LAYOUT_SLIDE_IDS = ['2147483649', '2147483650', '2147483652', '2147483654', '2147483655', '2147483657'];

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Presentation.
 */
export default {

    // single client, simple operation test, no commands
    TEST_01: {
        description: 'Simple single client test',
        tooltip: 'A simple single client test',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { keyboardevents: '1 2 3 4 5' },
                    { timeout: 100 },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: 'abc' }] },
                    { selection: { start: [0, 0, 0, 1], end: [0, 0, 0, 3] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { blockexternalops: 1000 },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, // inserting a new slide
                    { timeout: 500 },
                    { keyboardevents: 'CTRL+a' }, // selecting all drawings
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } }, // setting new background color
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' }, // switching to slide selection
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } }, // inserting a specific slide from the slide picker
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { keyboardevents: [
                        'TAB TAB SHIFT+h e l l o SPACE SHIFT+w o r l d',
                        'CTRL+z CTRL+y', // undo and redo
                        '5*SHIFT+RIGHT_ARROW',
                        'CTRL+b', // bold
                        '2*ESCAPE'
                    ] },
                    { timeout: 100 },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(2) > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 7 } } },
                    { timeout: 500 },
                    // rotating the shape
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 5 } } },
                    { keyboardevents: 'CTRL+z CTRL+z ESCAPE' },
                    // testing nodeClick
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(2) > .textframecontent' } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(1) > .textframecontent' } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    // inserting tables
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '3*RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/stylesheet' } },
                    { timeout: 200 },
                    { keyboardevents: '6*RIGHT_ARROW ENTER' },
                    { timeout: 500 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via mouse (works, but is dependent from mouse postions)
                    { timeout: 500 },
                    { triggerevent: { type: 'mouseenter', definition: { selector: '.popup-container.table-size-picker a.btn' } } },
                    { timeout: 200 },
                    { triggerevent: { type: 'mousemove', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 433, pageY: 144 } } }, { timeout: 100 },
                    { triggerevent: { type: 'mousemove', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 443, pageY: 154 } } }, { timeout: 100 },
                    { triggerevent: { type: 'mousemove', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 453, pageY: 164 } } }, { timeout: 100 },
                    { triggerevent: { type: 'mousemove', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 463, pageY: 174 } } }, { timeout: 100 },
                    { triggerevent: { type: 'mousemove', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 473, pageY: 184 } } }, { timeout: 100 },
                    { triggerevent: { type: 'mousemove', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 483, pageY: 194 } } }, { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.popup-container.table-size-picker a.btn', pageX: 483, pageY: 194 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(4) > .textframecontent' } } },
                    { timeout: 500 },
                    { activatetoolbar: 'table' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'table/stylesheet' } },
                    { timeout: 200 },
                    { keyboardevents: '4*RIGHT_ARROW ENTER' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedSelection: { start: [1, 0], end: [1, 0] }
                // expectedTextContent: 'abc'
            }
        }
    },

    // two clients, with commands, adding content into different slides
    TEST_02: {
        description: 'The command reference test',
        tooltip: 'A two clients test with text input in different slides',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: 200 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'textframe', startX: 0.8, startY: 0.8, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    { keyboardevents: ['1', '2', '3', '4', '5'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 7 } } },
                    { keyboardevents: ['6', '7', '8', '9', '0'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 200 },
                    { selection: { start: [1, 0, 0, 0], end: [1, 0, 0, 0] } },
                    { keyboardevents: [{ key: 'h', shift: true }, 'a', 'l', 'l', 'o', 'SPACE', { key: 'w', shift: true }, 'e', 'l', 't'] },
                    { keyboardevents: ['ESCAPE'] }, // switching to drawing selection
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: ['ESCAPE'] }, // switching to slide selection
                    { timeout: 100 },
                    { keyboardevents: ['UP_ARROW'] }, // switching to previous slide
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: 'SHIFT+h a l l o SPACE SHIFT+d r a w i n g SPACE' },
                    { operations: [ // Info: In this tests it is always better to call the function that generates the operation, not the operation directly.
                        { name: Op.PARA_INSERT, start: [0, 1, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 1, 1, 0], text: 'abcd' },
                        { name: Op.SET_ATTRIBUTES, start: [0, 1, 1, 1], end: [0, 1, 1, 2], attrs: { character: { bold: true } } }
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'a', 'b', 'c', 'd', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 0, width: 7000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '2*RIGHT_ARROW 3*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['d', 'e', 'f', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: [ // moving the drawing
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'
                    ] },
                    { timeout: 100 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 50 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, // inserting a new slide
                    { timeout: 500 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 7 } } },
                    // adding content into the shape
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] },
                    // changing the size of the shape
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 7 } } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, // setting new background color
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.4, startY: 0.4, stepX: -0.05, stepY: -0.05, stepCount: 7 } } },
                    { timeout: 500 },
                    // selecting drawing with the selection box
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'setselection', startX: 0.0, startY: 0.0, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    // adding content into the shape
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] },
                    { keyboardevents: ['ESCAPE', 'ESCAPE'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { timeout: 1000 },
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }] }, // selecting all drawings
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="purple"]' } }, // setting new background color
                    // testing nodeClick custom event
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(2) > .textframecontent' } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(1) > .textframecontent' } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(2) > .textframecontent > .textframe > .p', textposition: 'start', forceTestSelection: true } } },
                    { keyboardevents: ['i', 'n', 'SPACE', 't', 'h', 'e', 'SPACE', 'f', 'r', 'a', 'm', 'e'] },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(2) > .textframecontent > .textframe > .p', textposition: 'middle', forceTestSelection: true } } },
                    { keyboardevents: ['SPACE', 'n', 'i', 'c', 'e'] },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: ['DOWN_ARROW'] }, // switching to second slide
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 100 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 100 },
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'e', 'f', 'g', 'h', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 0, width: 7000, height: 3000 } }] } },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['d', 'e', 'f'] },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: [ // moving the drawing
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW'
                    ] },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 100 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 100 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/underline' } }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [1, 1, 0, 8], end: [1, 1, 0, 8] }
            },
            CLIENT_2: {
                expectedSelection: { start: [3, 1, 0, 8], end: [3, 1, 0, 8] }
            }
        }
    },

    // single client as sparring partner handling with slides
    TEST_03: {
        description: 'Single client, handling with new slides and undo',
        tooltip: 'A single client test that can be used as sparring partner handling with slides',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { selection: { start: [0, 0, 0, 0], end: [0, 0, 0, 0] } },
                    { keyboardevents: ['1', '2', '3', '4', '5'] },
                    { selection: { start: [0, 1, 0, 0], end: [0, 1, 0, 0] } },
                    { keyboardevents: ['6', '7', '8', '9', '0'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 200 },
                    { selection: { start: [1, 0, 0, 0], end: [1, 0, 0, 0] } },
                    { keyboardevents: [{ key: 'h', shift: true }, 'a', 'l', 'l', 'o', 'SPACE', { key: 'w', shift: true }, 'e', 'l', 't'] },
                    { keyboardevents: ['ESCAPE'] }, // switching to drawing selection
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: ['ESCAPE'] }, // switching to slide selection
                    { timeout: 100 },
                    { keyboardevents: ['UP_ARROW'] }, // switching to previous slide
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'a', 'b', 'c', 'd', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 0, width: 7000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '2*RIGHT_ARROW 3*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['d', 'e', 'f', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: [ // moving the drawing
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'
                    ] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 50 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1'] },
                    { timeout: 1000 },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['DOWN_ARROW'] }, // switching to following slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'SPACE', { key: 'T', shift: true }, 'i', 't', 'l', 'e', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] } // switching to previous slide
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_04: {
        description: 'Two clients, moving and resizing and inserting several slides and using undo / redo',
        tooltip: 'A two client test that handles move and resize against inserting and deleting slides using undo / redo',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { functioncall: { functionname: 'changeToFirstSlideInView' } },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { keyboardevents: ['1', '2', '3', '4', '5'] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    // selecting the first drawing on the slide
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing:nth-of-type(1) > .textframecontent' } } },
                    { timeout: 500 },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 6 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 6 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } }, // inserting a specific slide from the slide picker
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-original-title="Two Content"], a[data-original-title="Zwei Inhalte"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_04A: {
        description: 'Debug test, inserting several slides and using undo',
        tooltip: 'Debug test that inserts and deletes slides using undo',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { functioncall: { functionname: 'changeToFirstSlideInView' } },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { keyboardevents: [{ key: 's', shift: true }, 'l', 'i', 'd', 'e', 'SPACE', '1'] },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: ['TAB', { key: 's', shift: true }, 'l', 'i', 'd', 'e', 'SPACE', '2'] },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: ['TAB', { key: 's', shift: true }, 'l', 'i', 'd', 'e', 'SPACE', '3'] },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to first slide
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to first slide
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to first slide
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to first slide
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' },
                    { timeout: 50 },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // two clients inserting and removing many slides
    TEST_05: {
        description: 'Two clients, handling with new slides and undo/redo',
        tooltip: 'A multi client test handling with inserting and deleting many slides',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { selection: { start: [0, 0, 0, 0], end: [0, 0, 0, 0] } },
                    { keyboardevents: ['1', '2', '3', '4', '5'] },
                    { selection: { start: [0, 1, 0, 0], end: [0, 1, 0, 0] } },
                    { keyboardevents: ['6', '7', '8', '9', '0'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 200 },
                    { selection: { start: [1, 0, 0, 0], end: [1, 0, 0, 0] } },
                    { keyboardevents: [{ key: 'h', shift: true }, 'a', 'l', 'l', 'o', 'SPACE', { key: 'w', shift: true }, 'e', 'l', 't'] },
                    { keyboardevents: ['ESCAPE'] }, // switching to drawing selection
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: ['ESCAPE'] }, // switching to slide selection
                    { timeout: 100 },
                    { keyboardevents: ['UP_ARROW'] }, // switching to previous slide
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'a', 'b', 'c', 'd', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 0, width: 7000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '2*RIGHT_ARROW 3*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['d', 'e', 'f', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 500 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: 500 },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: ['LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] }, // moving the drawing
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 50 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1'] },
                    { timeout: 1000 },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['DOWN_ARROW'] }, // switching to following slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'SPACE', { key: 'T', shift: true }, 'i', 't', 'l', 'e', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] } // switching to previous slide
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'e', 'f', 'g', 'h', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'underline', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 4000, top: 0, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: ['g', 'h', 'i'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['j', 'k', 'l', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: [ // moving the drawing
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW'
                    ] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 50 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: 1000 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['DOWN_ARROW'] }, // switching to following slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'SPACE', { key: 'T', shift: true }, 'i', 't', 'l', 'e', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] } // switching to previous slide
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_05A: {
        description: 'Two clients, handling with new slides and undo/redo (short version of TEST_05)',
        tooltip: 'A multi client test handling with inserting and deleting many slides',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { selection: { start: [0, 0, 0, 0], end: [0, 0, 0, 0] } },
                    { keyboardevents: ['1', '2', '3', '4', '5'] },
                    { selection: { start: [0, 1, 0, 0], end: [0, 1, 0, 0] } },
                    { keyboardevents: ['6', '7', '8', '9', '0'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 200 },
                    { selection: { start: [1, 0, 0, 0], end: [1, 0, 0, 0] } },
                    { keyboardevents: [{ key: 'h', shift: true }, 'a', 'l', 'l', 'o', 'SPACE', { key: 'w', shift: true }, 'e', 'l', 't'] },
                    { keyboardevents: ['ESCAPE'] }, // switching to drawing selection
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: ['ESCAPE'] }, // switching to slide selection
                    { timeout: 100 },
                    { keyboardevents: ['UP_ARROW'] }, // switching to previous slide
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'a', 'b', 'c', 'd', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 0, width: 7000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '2*RIGHT_ARROW 3*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['d', 'e', 'f', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 500 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: 500 },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: ['LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] }, // moving the drawing
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 50 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1'] },
                    { timeout: 1000 },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '1', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [{ key: 'c', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'ENTER', 'e', 'f', 'g', 'h', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'underline', true] } }, // Info: Only functions at model allowed yet
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 4000, top: 0, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: ['g', 'h', 'i'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: ['j', 'k', 'l', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['ESCAPE'] }, // selecting the drawing
                    { keyboardevents: [ // moving the drawing
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW'
                    ] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the first drawing
                    { timeout: 50 },
                    { keyboardevents: ['TAB'] }, // selecting the second drawing
                    { timeout: 50 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: 1000 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { keyboardevents: ['UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW', 'UP_ARROW'] }, // switching to previous slide
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { keyboardevents: ['TAB'] }, // selecting drawing
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'drawing/fill/color' } },
                    { keyboardevents: [{ key: 'C', shift: true }, 'l', 'i', 'e', 'n', 't', 'SPACE', '2', 'ESCAPE', 'ESCAPE'] },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.main-pane .toolbar:not(".hidden") .group[data-key="layoutslidepicker/insertslide"] > a' } },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    // single client. Testing insertTab, insertDrawing, insertTable and insertRows.
    TEST_06: {
        description: 'insertTab, insertDrawing, insertTable and insertRows',
        tooltip: 'A single client test for insertTab, insertDrawing, insertTable and insertRows',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: 'abc' },
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 3], text: 'def' }
                    ] },
                    { selection: { start: [0, 0, 0, 1], end: [0, 0, 0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['1', 'TAB', '2', '3', 'TAB', '4', 'TAB', '5', 'TAB', '6', 'TAB', '7', '8', '9'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'textframe', startX: 0.9, startY: 0.9, stepX: -0.05, stepY: -0.01, stepCount: 10 } } },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 100 },
                        { keyCode: 9, charCode: 0 }, // Tab
                        { keyCode: 66, charCode: 101 },
                        { keyCode: 67, charCode: 102 }
                    ] },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 103 },
                        { keyCode: 66, charCode: 104 },
                        { keyCode: 9, charCode: 0 }, // Tab
                        { keyCode: 67, charCode: 105 }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 2, 2, 1, 0, 1], end: [0, 2, 2, 1, 0, 1] }
            }
        }
    },

    // two clients. Testing insertTab, insertDrawing, insertTable and insertRows.
    TEST_07: {
        description: 'insertTab, insertDrawing, insertTable and insertRows',
        tooltip: 'A two clients test for insertTab, insertDrawing, insertTable and insertRows',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 1, 0, 3], text: 'DEF' }
                    ] },
                    { selection: { start: [0, 1, 0, 1], end: [0, 1, 0, 1] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1, 0, 4], end: [0, 1, 0, 4] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['1', 'TAB', '2', '3', 'TAB', '4', 'TAB', '5', 'TAB', '6', 'TAB', '7', '8', '9'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'textframe', startX: 0.8, startY: 0.8, stepX: -0.05, stepY: -0.05, stepCount: 10 } } },
                    { keyboardevents: 'a b c' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE 30*RIGHT_ARROW' },
                    { buttonclick: { dataKey: 'table/stylesheet' } },
                    { timeout: 200 },
                    { keyboardevents: '7*RIGHT_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 100 },
                        { keyCode: 66, charCode: 101 },
                        { keyCode: 67, charCode: 102 },
                        { keyCode: 9, charCode: 0 }, // Tab
                        { keyCode: 68, charCode: 103 },
                        { keyCode: 69, charCode: 104 }
                    ] },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 103 },
                        { keyCode: 66, charCode: 104 },
                        { keyCode: 9, charCode: 0 }, // Tab
                        { keyCode: 67, charCode: 105 }
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { keyboardevents: ['1', 'TAB', '2', '3', 'TAB', '4', 'TAB', '5', 'TAB', '6', 'TAB', '7', '8', '9'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'textframe', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 10 } } },
                    { keyboardevents: 'a b c' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 100 },
                        { keyCode: 66, charCode: 101 },
                        { keyCode: 67, charCode: 102 },
                        { keyCode: 9, charCode: 0 }, // Tab
                        { keyCode: 68, charCode: 103 },
                        { keyCode: 69, charCode: 104 }
                    ] },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 103 },
                        { keyCode: 66, charCode: 104 },
                        { keyCode: 9, charCode: 0 }, // Tab
                        { keyCode: 67, charCode: 105 }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 2, 2, 1, 0, 1], end: [0, 13, 2, 0, 1] }
            }
        }
    },

    // many clients, with commands.
    // Inserting much text into one single paragraph
    TEST_08: {
        description: 'insertText into single paragraph',
        tooltip: 'A two clients test for inserting content into one paragraph',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'MNOPQR' }] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1, 0, 1], end: [0, 1, 0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'PAGE_UP', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'PAGE_UP', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'PAGE_UP', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 30,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'PAGE_UP',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'PAGE_UP', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'PAGE_UP', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            },
            CLIENT_2: {
                // expectedParagraphCount: 23
            }
        }
    },

    // single client, can be used as partner working at the same document
    TEST_09: {
        description: 'Single user partner test',
        tooltip: 'A single client insert content into a document for a long time',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [0, 1, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 1, 1, 0], text: 'GHIJKL' },
                        { name: Op.PARA_INSERT, start: [0, 1, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 1, 2, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'SPACE', '9', 'SPACE',
                        '0', '1', '2', '3', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'SPACE', '3', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'SPACE', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '5', '6', 'BACKSPACE', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8',  'SPACE', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'SPACE', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8',  'SPACE', '9', 'SPACE',
                        '0', '1', '2', '3', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'SPACE', '3', '4', '5', '6', 'BACKSPACE', 'SPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'SPACE', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', 'SPACE', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    /**
     * OX Presentation, multi user undo test.
     */
    TEST_10: {
        description: 'Multi user undo/redo test for splitParagraph',
        tooltip: 'A multi client test demonstrating the undo/redo functionality for the splitParagraph operation',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'Hello World' }] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1, 0, 0], end: [0, 1, 0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: '10*ENTER' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 250, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: '10*ENTER' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] }
            },
            CLIENT_2: {
                expectedSelection: { start: [0, 1, 0, 0], end: [0, 1, 0, 0] }
            }
        }
    },

    /**
     * OX Presentation, single user undo test.
     */
    TEST_11: {
        description: 'Single user undo/redo test for splitParagraph',
        tooltip: 'A single client test demonstrating the undo/redo functionality for the splitParagraph operation',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'Hello World' }] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: '5*ENTER' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] }
            }
        }
    },

    /**
     * OX Presentation, single user splitParagraph/mergeParagraph test.
     */
    TEST_12: {
        description: 'Single user test for splitParagraph and mergeParagraph',
        tooltip: 'A single client test demonstrating the functionality for the splitParagraph and mergeParagraph operation',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: '1 2 3' },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: '4 5 6' },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'Hello World' }] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: '5*ENTER 5*BACKSPACE 5*ENTER 5*BACKSPACE' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] }
            }
        }
    },

    // many clients, with commands.
    // Inserting much text into one single paragraph and using undo and redo(Info: undo requires spaces because of grouping of operations)
    TEST_13: {
        description: 'insertText into single paragraph together with undo and redo',
        tooltip: 'A two clients test for inserting content into one paragraph and using undo and redo',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'MNOPQR' }] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1, 0, 1], end: [0, 1, 0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: ['0', '1', '2', 'SPACE', '3', '4', '5', '6', 'SPACE', '7', '8', '9', '0'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['1', '2', 'SPACE', '3', '4'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['5', '6', '7', 'SPACE', '8', '9', '0'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['1', '2', '3', 'SPACE', '4', '5', '6', '7', '8', 'PAGE_UP', '9', '0', '1', '2', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['6', '7', '8', '9', '0', 'SPACE', '1', '2', '3', '4', '5', 'SPACE', '6', 'PAGE_UP', '7', '8', '9', '0', '1', '2', '3'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['4', '5', '6', 'SPACE', '7', '8', '9', '0', '1', '2', '3', 'SPACE', '4', '5', '6', '7'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['8', '9', '0', '1', '2', 'SPACE', '3', '4', '5', '6', '7', 'SPACE', '8', '9', '0'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', 'SPACE', '7', '8', '9', '0', 'SPACE', '1', '2', '3', '4', 'SPACE', '5', '6', '7', 'PAGE_UP', '8', '9', '0', '1', '2', '3'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['4', '5', '6', '7', 'SPACE', '8', '9', '0', '1', '2', '3', 'SPACE', '4', '5', '6', '7', '8', '9'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 30,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: ['A', 'B', 'C', 'SPACE', 'D', 'E', 'F', 'G', 'H', 'SPACE', 'I', 'J', 'A', 'B', 'C'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['D', 'E', 'F', 'G', 'SPACE', 'H', 'I', 'J', 'PAGE_UP', 'A', 'B', 'C', 'D', 'SPACE', 'E', 'F'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['G', 'H', 'I', 'J', 'A', 'SPACE', 'B', 'C', 'D', 'E', 'F', 'G', 'SPACE', 'H', 'I', 'J', 'A', 'B', 'SPACE', 'C', 'D', 'E', 'F', 'G'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['H', 'I', 'PAGE_UP', 'J', 'A', 'B', 'C', 'SPACE', 'D', 'E', 'F', 'G', 'H', 'I', 'SPACE', 'J', 'A', 'B', 'C', 'D', 'E', 'F', 'G'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['H', 'I', 'J', 'A', 'B', 'C', 'D', 'SPACE', 'E', 'F', 'G', 'H', 'I', 'SPACE', 'J', 'A', 'B', 'C', 'D', 'PAGE_UP', 'E', 'F', 'G', 'H'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['I', 'J', 'A', 'B', 'C', 'D', 'E', 'SPACE', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'SPACE', 'F', 'G', 'H', 'I'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['J', 'A', 'SPACE', 'B', 'C', 'D', 'E', 'F', 'SPACE', 'G', 'H', 'I', 'J'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            },
            CLIENT_2: {
                // expectedParagraphCount: 23
            }
        }
    },

    // Testing the grouping of insertText operations for undo and redo
    TEST_14: {
        description: 'insertText into text frame and pressing undo must delete all the text',
        tooltip: 'A single client test for inserting text and using undo to delete all inserted text',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: 'test' }] },
                    { selection: { start: [0, 0, 0, 4], end: [0, 0, 0, 4] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', '3', '4'] },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedTextContent: 'test ', // not modified
                expectedParagraphCount: 1
            }
        }
    },
    // Testing the grouping of insertText operations for undo and redo with several words
    TEST_15: {
        description: 'insertText into text frame and pressing undo must delete the last word',
        tooltip: 'A single client test for inserting text and using undo to delete the last word of the inserted text',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: 'test' }] },
                    { selection: { start: [0, 0, 0, 4], end: [0, 0, 0, 4] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '3', '4', '5', '6'] },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedTextContent: 'test 012 ', // second word '3456' removed
                expectedParagraphCount: 1
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // two clients, with commands.
    // Inserting text and splitting paragraphs
    TEST_16: {
        description: 'insertText into single paragraph',
        tooltip: 'A two clients test for inserting content into one paragraph',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['1', '2', '3'] },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 3000, width: 3000, height: 1000 } }] } },
                    { keyboardevents: ['4', '5', '6'] },
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: 'MNOPQR' }] },
                    { selection: { start: [0, 1, 0, 5], end: [0, 1, 0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1, 0, 1], end: [0, 1, 0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'h', 'ENTER'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 30,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'SPACE',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'h', 'SPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            },
            CLIENT_2: {
                // expectedParagraphCount: 23
            }
        }
    },

    TEST_17: {
        description: 'Two clients grouping and ungrouping ',
        tooltip: 'A two clients test for grouping and ungrouping drawings on the same slide',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME ESCAPE CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 1000, top: 1000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 3000, top: 1000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 5000, top: 1000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 7000, top: 1000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 9000, top: 1000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 1000, top: 3000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 3000, top: 3000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 5000, top: 3000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 7000, top: 3000, width: 1000, height: 1000 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 9000, top: 3000, width: 1000, height: 1000 }] } },
                    { timeout: 100 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 100 },
                    { functioncall: { functionname: 'setMultiDrawingSelectionByPosition', object: 'selection', params: [[[0, 0], [0, 1], [0, 3], [0, 5], [0, 7], [0, 9]]] } },
                    { timeout: 100 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'setMultiDrawingSelectionByPosition', object: 'selection', params: [[[0, 0], [0, 2], [0, 3], [0, 4], [0, 6], [0, 8]]] } },
                    { timeout: 100 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 5000,
                    commandDelay: 100
                },
                orders: [
                    { activatetoolbar: 'drawing' },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 20
                },
                orders: [
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'setSlideSelection', object: 'selection' } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'selectAll', object: 'selection' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_17A: {
        description: 'Two clients, one is grouping and ungrouping and the other clients sets drawing attributes',
        tooltip: 'A two clients test for grouping and ungrouping drawings and assigning attributes',
        clients: 2,
        autotest: false, // because of colliding operations
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 1000, top: 1000, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 5500, top: 3300, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 10000, top: 5600, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 14500, top: 7900, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 19000, top: 10200, width: 3500, height: 2500 }] } },
                    { timeout: 100 },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 100
                },
                orders: [
                    { activatetoolbar: 'drawing' },
                    { timeout: 50 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 900 },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/ungroup' } }, { timeout: 900 },

                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 20
                },
                orders: [
                    { activatetoolbar: 'drawing' },
                    { timeout: 50 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="purple"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="purple"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE' }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_17B: {
        description: 'Two clients, one is grouping and ungrouping (with undo/redo) and the other clients sets drawing attributes',
        tooltip: 'A two clients test for grouping and ungrouping drawings and assigning attributes. In this scenario the undo stack is cleared, but no reload!',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 1000, top: 1000, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 5500, top: 3300, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 10000, top: 5600, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 14500, top: 7900, width: 3500, height: 2500 }] } },
                    { timeout: 50 },
                    { functioncall: { functionname: 'insertShape', params: ['ellipse', { left: 19000, top: 10200, width: 3500, height: 2500 }] } },
                    { timeout: 100 },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 100
                },
                orders: [
                    { activatetoolbar: 'drawing' },
                    { timeout: 50 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'drawing/group' } }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 20
                },
                orders: [
                    { activatetoolbar: 'drawing' },
                    { timeout: 50 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="purple"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="red"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="green"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="light-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="dark-blue"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="purple"]' } },
                    { timeout: 200 },
                    { keyboardevents: 'ESCAPE' }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    // Inserting and deleting rows in tables against inserting text into a row below (using insertRow/deleteRow button)
    TEST_18A: {
        description: 'insertText into a table below a removed and inserted row using insertRow/deleteRow button',
        tooltip: 'A two clients test for inserting content into a permanently modified table using insertRow/deleteRow button',
        clients: 2,
        autotest: true,
        idleTime: 10000,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '5*RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: [0, 0], end: [0, 0], attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB b 2*DOWN_ARROW' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 5000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 100
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: [
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    // two clients
    // Inserting and deleting rows in tables against inserting text into a row below (using insertRow/deleteRow button)
    TEST_18B: {
        description: 'insertText into a grid table below a removed and inserted row using insertRow/deleteRow button',
        tooltip: 'A two clients test for inserting content into a permanently modified grid table using insertRow/deleteRow button',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '5*RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { functioncall: { functionname: 'setAttributes', params: ['table', { styleId: '{5940675A-B579-460E-94D1-54222C63F5DA}' }, { clear: true }] } },
                    { timeout: 100 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: [0, 0], end: [0, 0], attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB a 2*DOWN_ARROW' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 5000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 100
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { start: [0, 0, 0, 0, 0, 0] } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: [
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    // two clients
    // Inserting and deleting rows in tables against inserting text into a row below (using undo/redo for table row)
    TEST_18C: {
        description: 'insertText into a table below a removed and inserted row using undo/redo',
        tooltip: 'A two clients test for inserting content into a permanently modified table using undo/redo',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '5*RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: [0, 0], end: [0, 0], attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] },
                    { keyboardevents: ['1', 'SPACE', '2', 'SPACE', '3', 'SPACE', '4', 'SPACE', '5', 'SPACE', '6', 'SPACE', '7', 'SPACE', '8', 'SPACE', '9', 'SPACE', '0'] } // some 'buffer' operations for undo to avoid deletion of table
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB a 2*DOWN_ARROW' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 5000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 100
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: [
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_18D: {
        description: 'Demonstrating a test without any logical position in its definition and complete preparation',
        tooltip: 'A two clients test for inserting content into a permanently modified table using insertRow/deleteRow button',
        clients: 2,
        autotest: true,
        idleTime: 10000,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '2*RIGHT_ARROW DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { timeout: 100 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 0 } }, attrs: { drawing: { left: 1000, top: 10000 } } }] },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '5*RIGHT_ARROW 3*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1 } }, attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] },
                    { activatetoolbar: 'table' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [2, 1, 0, 0] } } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 5000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 100
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 100 },
                    { selection: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1, appendStart: [0, 0, 0, 0] } } },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 100 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: [
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER', 'TAB',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'W', 'o', 'r', 'l', 'd', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'E', 'a', 'r', 't', 'h', 'ENTER',
                        'H', 'e', 'l', 'l', 'o', 'SPACE', 'M', 'o', 'o', 'n', 'i', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_18E: {
        description: 'Testing insertRow and deleteRow together with their undo operations against a client that deletes and restores the complete table (DOCS-3113)',
        tooltip: 'A two clients test for inserting and deleting rows in a table that is completely removed and restored via undo (DOCS-3113)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '7*RIGHT_ARROW 5*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1 } }, attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] },
                    { timeout: 100 },
                    { keyboardevents: [
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        '2*ESCAPE'
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB a DOWN_ARROW' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 3000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { timeout: 2000 }, // let second user insert a row
                    { keyboardevents: [{ key: 'a', ctrl: true }, 'DELETE'] }, // selecting all drawings and deleting them
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 2 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 3 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 4 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 5 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 6 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 7 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 8 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 9 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 0 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 1 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 2 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 3 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 4 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 5 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 6 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 7 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 8 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 9 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 2 0 DOWN_ARROW' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/row' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_18F: {
        description: 'Testing insertColumn and deleteColumns together with their undo operations against a client that deletes and restores the complete table (DOCS-3113)',
        tooltip: 'A two clients test for inserting and deleting columns in a table that is completely removed and restored via undo (DOCS-3113)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '7*RIGHT_ARROW 5*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1 } }, attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] },
                    { timeout: 100 },
                    { keyboardevents: [
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        '2*ESCAPE'
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB a DOWN_ARROW' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 3000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { timeout: 2000 }, // let second user insert a column
                    { keyboardevents: [{ key: 'a', ctrl: true }, 'DELETE'] }, // selecting all drawings and deleting them
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 4000 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 2 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 3 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 4 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 5 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 6 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 7 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 8 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 9 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 0 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 1 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 2 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 3 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 4 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 5 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 6 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 7 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 8 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 9 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 2 0 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_18G: {
        description: 'Testing insertColumn and deleteColumns together with their undo operations against a client that inserts and deletes text into the same table (DOCS-3114)',
        tooltip: 'A two clients test for inserting and deleting columns in a table in that another client adds and removes text (DOCS-3114)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '7*RIGHT_ARROW 5*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1 } }, attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] },
                    { timeout: 100 },
                    { keyboardevents: [
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        '2*ESCAPE'
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB a DOWN_ARROW' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 3000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { timeout: 500 }, // let second user insert a row
                    { keyboardevents: ['TAB 1 5*TAB SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1'] }, // going to right part of the table for editing
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 2'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 2'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 2'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 3'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 3'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 3'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 4'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 4'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 4'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 5'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 5'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 5'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 6'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 6'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 6'] }, { timeout: 300 },
                    { keyboardevents: ['2*RIGHT_ARROW 5*UP_ARROW'] },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 7'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 7'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 7'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 2 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 3 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 4 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 5 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 6 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 7 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 8 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 9 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 0 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 1 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 2 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 3 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'TAB 1 4 SHIFT+TAB' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    // modifying a table, so that cells are removed. In OX Presentation this is not allowed. In OX Text there is
    // test TEST_53, in which no reload is triggered, when table cells are not restored.
    // This test causes very reliable a reload during the text execution.
    TEST_18H: {
        description: 'Testing deleteColumn together with their undo operations against a client that deletes rows in the same table',
        tooltip: 'A two clients test for deleting columns and rows together with their undo operations in one table',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // clicking into mainframe, generating new slide, if required
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, // forcing focus into first slide
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '5*RIGHT_ARROW 5*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { operations: [{ name: Op.SET_ATTRIBUTES, start: { description: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing[data-type="table"]', index: 1 } }, attrs: { drawing: { left: 1000, top: 1000, width: 23000, height: 10000 } } }] },
                    { timeout: 100 },
                    { keyboardevents: [
                        '1 TAB 2 TAB 3 TAB SHIFT+h e l l o SPACE 4 TAB 5 TAB 6 TAB',
                        '7 TAB 8 TAB 9 TAB SHIFT+h e l l o SPACE 1 0 TAB 1 1 TAB 1 2 TAB',
                        '1 3 TAB 1 4 TAB 1 5 TAB SHIFT+h e l l o SPACE  1 6 TAB 1 7 TAB 1 8 TAB',
                        'SHIFT+h e l l o SPACE 1 9 TAB SHIFT+h e l l o SPACE 2 0 TAB SHIFT+h e l l o SPACE 2 1 TAB SHIFT+h e l l o SPACE  2 2 TAB SHIFT+h e l l o SPACE 2 3 TAB SHIFT+h e l l o SPACE 2 4 TAB',
                        '2 5 TAB 2 6 TAB 2 7 TAB SHIFT+h e l l o SPACE  2 8 TAB 2 9 TAB 3 0 TAB',
                        '3 1 TAB 3 2 TAB 3 3 TAB SHIFT+h e l l o SPACE  3 4 TAB 3 5 TAB 3 6',
                        '2*UP_ARROW 5*SHIFT+TAB' // deleting the 4th of the 6 rows
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB a 3*TAB b' }, // deleting the 4th of the 6 columns
                    { timeout: 500 },
                    { activatetoolbar: 'table' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 2500, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { timeout: 500 }, // let second user insert a row
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 }
                ]
            },
            CLIENT_3: {} // a watcher
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_19: {
        description: 'inserting text into an existing text frame',
        tooltip: 'A single client that inserts permanently text into an existing text frame',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 8 } } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_20: {
        description: 'inserting text into an existing text frame and splitting it into several paragraphs',
        tooltip: 'A single client that inserts permanently text into an existing text frame and then generates many splitParagraph operations',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 1000, top: 1000, width: 6000, height: 2000 } }] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        '50*LEFT_ARROW',
                        'RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER',
                        'RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER',
                        'RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER',
                        'RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER',
                        'RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER',
                        'RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER RIGHT_ARROW ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_21: {
        description: 'inserting a shape into the slide using the GUI and resizing the new inserted shape',
        tooltip: 'A single user test test for using the selection box to insert a shape and to resize it',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: []
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    // inserting the shape via the GUI
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 8 } } },
                    { timeout: 100 },
                    // adding content into the shape
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] },
                    // changing the size of the shape
                    { timeout: 100 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 7 } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.4, startY: 0.4, stepX: -0.05, stepY: -0.05, stepCount: 7 } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 7 } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.4, startY: 0.4, stepX: -0.05, stepY: -0.05, stepCount: 7 } } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_22: {
        description: 'Two clients inserting and selecting shapes ',
        tooltip: 'A two clients test for inserting and selecting shapes on the same slide',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: []
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 8 } } },
                    // adding content into the shape
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] },
                    // selecting drawing with the selection box
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'setselection', startX: 0.0, startY: 0.0, stepX: 0.05, stepY: 0.05, stepCount: 10 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="ellipse"]' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.55, startY: 0.55, stepX: 0.05, stepY: 0.05, stepCount: 8 } } },
                    // adding content into the shape
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] },
                    // selecting drawing with the selection box
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'setselection', startX: 0.5, startY: 0.5, stepX: 0.05, stepY: 0.05, stepCount: 10 } } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_23: {
        description: 'Two clients resizing and moving a text frame and inserting content into the text frame',
        tooltip: 'A two clients test for resizing and moving a text frame and inserting content into the text frame',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a text frame with the GUI
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'textframe', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 10 } } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB' } // selecting the first drawing
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, // setting new background color
                    { timeout: 500 },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    // moving the shape
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    // resizing the shape
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // two clients, one moving and resizing a shape and the other inserts and deletes slides
    TEST_24: {
        description: 'Two clients resizing and moving a shape and inserting and deleting slides',
        tooltip: 'A two clients test for resizing and moving a shape and inserting and deleting slides',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, // inserting a new slide
                    { timeout: 200 },
                    { keyboardevents: [{ key: 'a', ctrl: true }, 'DELETE'] }, // selecting all drawings and deleting them
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 8 } } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, // setting new background color
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.5, startY: 0.5, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .slide:not(".invisibleslide") > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.05, stepCount: 9 } } },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, // inserting a new slide
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 },
                    { buttonclick: 'undo' },
                    { timeout: 1000 },
                    { buttonclick: 'redo' },
                    { timeout: 1000 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_25: {
        description: 'Two clients rotating a shape and inserting content into the shape',
        tooltip: 'A two clients test for rotating a shape and inserting content into the shape',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.25, startY: 0.25, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.toolbar:not(".hidden") > .group-container:not(".hidden") > .group:not(".hidden") > a[data-original-title="More options"]', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="autofit"]' } },
                    { timeout: 500 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { buttonclick: { selector: '.toolbar:not(".hidden") > .group-container:not(".hidden") > .group:not(".hidden") > a[data-original-title="Paragraph alignment"]', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: '.group[data-key="drawing/verticalalignment"]:not(".hidden") > a[data-value="top"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB' } // selecting the first drawing
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="red"]' } }, // setting new background color
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.1, startY: 0.05, stepX: 0.05, stepY: 0.02, stepCount: 7 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.02, stepCount: 7 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.1, startY: 0.05, stepX: 0.05, stepY: 0.02, stepCount: 7 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.02, stepCount: 7 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_26: {
        description: 'Two clients adjusting a shape and inserting content into the shape',
        tooltip: 'A two clients test for adjusting a shape and inserting content into the shape',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 9 } } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.toolbar:not(".hidden") > .group-container:not(".hidden") > .group:not(".hidden") > a[data-original-title="More options"]', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="autofit"]' } },
                    { timeout: 500 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { buttonclick: { selector: '.toolbar:not(".hidden") > .group-container:not(".hidden") > .group:not(".hidden") > a[data-original-title="Paragraph alignment"]', index: 1 } },
                    { timeout: 100 },
                    { buttonclick: { selector: '.group[data-key="drawing/verticalalignment"]:not(".hidden") > a[data-value="top"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB' } // selecting the first drawing
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="purple"]' } }, // setting new background color
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.02, stepCount: 7 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.02, stepCount: 7 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.02, stepCount: 7 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.02, stepCount: 7 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_27: {
        description: 'Inserting several slides and running undo via operations (DOCS-2033)',
        tooltip: 'Inserting slide from bottom to top and deleting very fast in undo after the replacement slides are inserted (DOCS-2033)',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: 'SHIFT+s l i d e SPACE 1' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 2' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 3' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 2000 },
                    { keyboardevents: '2*UP_ARROW' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 2000 },
                    { keyboardevents: '2*UP_ARROW' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 15000 }, // waiting long time, so that replacement slides are inserted
                    // simulating very fast undo of all three slides
                    { operations: [
                        { name: Op.DELETE, start: [1, 1, 0] },
                        { name: Op.DELETE, start: [1, 1] },
                        { name: Op.DELETE, start: [1, 0, 0] },
                        { name: Op.DELETE, start: [1, 0] },
                        { name: Op.DELETE, start: [1] },
                        { name: Op.DELETE, start: [2, 1, 0] },
                        { name: Op.DELETE, start: [2, 1] },
                        { name: Op.DELETE, start: [2, 0, 0] },
                        { name: Op.DELETE, start: [2, 0] },
                        { name: Op.DELETE, start: [2] },
                        { name: Op.DELETE, start: [3, 1, 0] },
                        { name: Op.DELETE, start: [3, 1] },
                        { name: Op.DELETE, start: [3, 0, 0] },
                        { name: Op.DELETE, start: [3, 0] },
                        { name: Op.DELETE, start: [3] }
                    ] },
                    { timeout: 200 },
                    { functioncall: { functionname: 'changeToFirstSlideInView' } }

                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_28: {
        description: 'Single client, handling with new slides and undo and waiting for replacement slides',
        tooltip: 'A single client test that can be used as sparring partner handling with slides and waiting for replacement slides',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: 'SHIFT+s l i d e SPACE 1' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 2' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 3' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 2000 },
                    { keyboardevents: '2*UP_ARROW' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 2000 },
                    { keyboardevents: '2*UP_ARROW' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 15000 }, // waiting long time, so that replacement slides are inserted
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' }

                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_29: {
        description: 'Single client, deleting several slides and using undo and redo',
        tooltip: 'A single client test that can be used as sparring partner for deleting and restoring several slides',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' },
                    { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: 'SHIFT+s l i d e SPACE 1' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 2' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 3' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide', index: 0 } },
                    { timeout: 200 },
                    { keyboardevents: 'TAB SHIFT+s l i d e SPACE 4' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 200 },
                    { keyboardevents: '6*SHIFT+DOWN_ARROW' },
                    { timeout: 1000 },
                    { keyboardevents: 'DELETE' },
                    { timeout: 5000 },
                    { buttonclick: 'undo' },
                    { timeout: 5000 },
                    { buttonclick: 'redo' },
                    { timeout: 5000 },
                    { buttonclick: 'undo' },
                    { timeout: 5000 },
                    { buttonclick: 'redo' }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_30: {
        description: 'Two clients, inserting text into a shape while another client reloads the document',
        tooltip: 'A two client that inserts permanently text into a shape while the second client reloads the document several times',
        clients: 2,
        autotest: false, // because of reload
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.05, stepCount: 15 } } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { functioncall: { functionname: 'changeToFirstSlideInView' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 200
                },
                orders: [
                    { timeout: 1000 },
                    { keyboardevents: 'TAB 1 2 3' },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: 'TAB 4 5 6' },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: 'TAB 7 8 9' },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: 'TAB 0 1 2' }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_31: {
        description: 'Sparring partner test for copying and pasting a lot of content',
        tooltip: 'Testing long running process like pasting by the sparring partner',
        clients: 1, // sparring partner test
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 0, 2], end: [0, 0, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 1, 2], end: [0, 0, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 2, 2], end: [0, 0, 2, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 }, // selecting the complete content
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 }, // { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { keyboardevents: 'CTRL+v' }, { timeout: 2000 }, // { functioncall: { functionname: 'pasteInternalClipboard' } }
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_31B: {
        description: 'Sparring partner test for moving slides',
        tooltip: 'Testing permanent move of slides',
        clients: 1, // sparring partner test
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a DELETE' }, // deleting all drawings
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 10000, height: 1000 } }] } }, { keyboardevents: '1' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 500 },
                    { activatetoolbar: 'drawing' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="red"]' } }, { timeout: 500 },
                    { keyboardevents: '2*ESCAPE' }, { timeout: 500 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 10000, height: 1000 } }] } }, { keyboardevents: '2' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, { timeout: 500 },
                    { keyboardevents: 'ESCAPE' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, { timeout: 200 }, // using the side pane!
                    { keyboardevents: 'HOME' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_31C: {
        description: 'Sparring partner test for inserting text',
        tooltip: 'Testing permanent text insertion',
        clients: 1, // sparring partner test
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 3000, width: 18000, height: 1000 } }] } }, { keyboardevents: '1' }, { timeout: 100 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_31D: {
        description: '4 Client test: Combination of the sparring partner tests 31 to 31C and additionally one client as a watcher',
        tooltip: 'A 4 client test as combination of the sparring partner tests 31 to 31C and additionally one client as a watcher',
        clients: 4,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 10000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 0, 2], end: [0, 0, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 1, 2], end: [0, 0, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 2, 2], end: [0, 0, 2, 592], attrs: { character: { underline: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 3] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 3, 0], text: '3 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 3, 2], end: [0, 0, 3, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 4] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 4, 0], text: '4 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 4, 2], end: [0, 0, 4, 592], attrs: { character: { underline: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 5] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 5, 0], text: '5 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 5, 2], end: [0, 0, 5, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 6] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 6, 0], text: '6 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 6, 2], end: [0, 0, 6, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 7] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 7, 0], text: '7 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 7, 2], end: [0, 0, 7, 592], attrs: { character: { underline: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 8] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 8, 0], text: '8 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 8, 2], end: [0, 0, 8, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 9] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 9, 0], text: '9 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 6, 2], end: [0, 0, 6, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 10] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 10, 0], text: '10 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 10, 3], end: [0, 0, 10, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'ESCAPE' },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 10000, height: 1000 } }] } }, { keyboardevents: '2' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, { timeout: 500 },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_3: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 3000, width: 18000, height: 1000 } }] } }, { keyboardevents: '1' }, { timeout: 100 }
                ]
            },
            CLIENT_4: {} // a watcher
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 }, // selecting the complete content
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 }, // { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { keyboardevents: 'CTRL+v' }, { timeout: 2000 }, // { functioncall: { functionname: 'pasteInternalClipboard' } }
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, { timeout: 200 }, // using the side pane!
                    { keyboardevents: 'HOME' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] }
                ]
            },
            CLIENT_4: {} // watcher
        }
    },

    TEST_32: {
        description: 'Two client test for copying and pasting a lot of content',
        tooltip: 'Testing long running process and checking the results by a remote client',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 0, 2], end: [0, 0, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 1, 2], end: [0, 0, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 2, 2], end: [0, 0, 2, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_33: {
        description: 'Two client test for copying and pasting a lot of content while the other client is typing',
        tooltip: 'Testing long running process paste processes while another client is typing into a textframe',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 10000, top: 2000, width: 14000, height: 3000 } }] } },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 1, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [0, 1, 0, 2], end: [0, 1, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 1, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 1, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 1, 1, 2], end: [0, 1, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 1, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 1, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 1, 2, 2], end: [0, 1, 2, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME TAB' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z',
                        'ESCAPE ESCAPE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_34: {
        description: 'Two client test for copying and pasting a lot of content while the other client changes the slide layout',
        tooltip: 'Testing long running process paste processes while another client is changing the layout of the active slide',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 0, 2], end: [0, 0, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 1, 2], end: [0, 0, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 2, 2], end: [0, 0, 2, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[0] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[1] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[2] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[3] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[4] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[5] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[0] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[1] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[2] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[3] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[4] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 2000 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[5] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_35: {
        description: 'Two client test for copying and pasting a lot of content while the other client moves the slides',
        tooltip: 'Testing long running process paste processes while another client is moving some slides',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 100 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all slides
                    { timeout: 200 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // creating one slide
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { keyboardevents: 'SHIFT+s l i d e SPACE 1' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, // inserting a new slide
                    { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 5000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [1, 0, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [1, 0, 0, 2], end: [1, 0, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [1, 0, 1] },
                        { name: Op.TEXT_INSERT, start: [1, 0, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [1, 0, 1, 2], end: [1, 0, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [1, 0, 2] },
                        { name: Op.TEXT_INSERT, start: [1, 0, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [1, 0, 2, 2], end: [1, 0, 2, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } }, { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } }, { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } }, { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } }, { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } }, { timeout: 2000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 200 },
                    { keyboardevents: 'HOME' }, { timeout: 1000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 2000 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 2000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_35B: {
        description: 'Two client test for changing the slide layout while the other client moves the slides',
        tooltip: 'Testing long running process of changing slide layout while another client is moving some slides',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 100 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all slides
                    { timeout: 200 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // creating one slide
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[0] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'TAB TAB SHIFT+S l i d e SPACE 1 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 2 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 3 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 4 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="blue"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 5 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="red"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 6 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 7 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="blue"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 8 SPACE o f SPACE 8' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="red"]' } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { assert: { id: 10, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.slide' }, { name: 'length', isProperty: true }], result: 8 }, text: 'Unexpected number of slide nodes.' } },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 7 }, text: 'Unexpected active slide index.' } },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 200 },
                    { keyboardevents: 'HOME' },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+UP_ARROW' },
                    { timeout: 200 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'redo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { buttonclick: 'redo' },
                    { timeout: 200 },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { buttonclick: 'redo' },
                    { timeout: 200 },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { buttonclick: 'undo' },
                    { timeout: 200 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { timeout: 100 },
                    { activatetoolbar: 'file' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=portrait]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=landscape]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=portrait]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=landscape]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=portrait]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=landscape]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=portrait]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'document/pagesettings' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .page-orientation .btn' } }, { timeout: 100 },
                    { buttonclick: { selector: '.smart-dropdown-container [data-value=landscape]' } }, { timeout: 100 },
                    { buttonclick: { selector: '.io-ox-office-dialog .modal-footer .btn[data-action="ok"]' } }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'redo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 1000 },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_35C: {
        description: 'Two client test for setAttributes operations for many drawings on a slide while the other client moves the slides',
        tooltip: 'Testing multiple setAttributes operations while another client is moving some slides',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 100 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all slides
                    { timeout: 200 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // creating one slide
                    { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all drawings
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 4000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '2' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 6000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '3' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '4' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 10000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '5' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 12000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '6' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 14000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '7' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 16000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '8' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 18000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '9' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 20000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 0' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 1' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 4000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 2' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 6000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 3' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 4' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 10000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 5' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 12000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 6' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 14000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 7' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 16000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 8' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 18000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '1 9' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 20000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: '2 0' }, { timeout: 500 },
                    // one more drawing, so that setAttributes operation on wrong slide crashes
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 10000, top: 9000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'SHIFT+x' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 500 },
                    { activatetoolbar: 'drawing' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="yellow"]' } }, { timeout: 500 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'a' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 4000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'b' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 6000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'c' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'd' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 10000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'e' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 12000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'f' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 14000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'g' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 16000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'h' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 18000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'i' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 20000, top: 1000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'j' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'k' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 4000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'l' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 6000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'm' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 8000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'n' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 10000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'o' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 12000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'p' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 14000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'q' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 16000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 'r' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 18000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 's' }, { timeout: 100 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 20000, top: 5000, width: 1500, height: 1000 } }] } }, { keyboardevents: 't' }, { timeout: 500 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="green"]' } }, { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 200 },
                    { keyboardevents: 'HOME' },
                    { assert: { id: 10, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' },
                    { timeout: 500 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, // selecting all drawings on the slide
                    { timeout: 1000 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 }, // moving all drawings
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW' }, { timeout: 100 },
                    { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { timeout: 1000 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_35D: {
        description: 'Two client test for insertText operations while the other client moves the active slide',
        tooltip: 'Testing insertText operations while another client is moving the active slide',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 100 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all slides
                    { timeout: 200 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // creating one slide
                    { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all drawings
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 20000, height: 1000 } }] } },
                    { keyboardevents: 'SHIFT+s l i d e SPACE t o SPACE t y p e SPACE a n d SPACE m o v e' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 500 },
                    { activatetoolbar: 'drawing' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, { timeout: 500 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 20000, height: 1000 } }] } },
                    { keyboardevents: 'SHIFT+s l i d e SPACE w i t h o u t SPACE t y p e SPACE a n d SPACE w i t h o u t SPACE m o v e' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } }, { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 200 },
                    { keyboardevents: 'HOME' }, { timeout: 500 },
                    { assert: { id: 10, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 }, // moving that slide, on which the other client types content
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, // selecting the drawing on the slide
                    { timeout: 1000 },
                    { keyboardevents: [
                        'SPACE ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] },
                    { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_35E: {
        description: 'Two client test for insertText operations while the other client moves the non-active slide',
        tooltip: 'Testing insertText operations while another client is moving the non-active slide',
        clients: 2, // second client only for checksum
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 100 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all slides
                    { timeout: 200 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, // creating one slide
                    { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all drawings
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 20000, height: 1000 } }] } },
                    { keyboardevents: 'SHIFT+s l i d e SPACE t o SPACE t y p e' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 500 },
                    { activatetoolbar: 'drawing' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, { timeout: 500 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 20000, height: 1000 } }] } },
                    { keyboardevents: 'SHIFT+s l i d e SPACE t o SPACE m o v e' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="orange"]' } }, { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 200 },
                    { keyboardevents: 'HOME' }, { timeout: 500 },
                    { assert: { id: 10, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'DOWN_ARROW' }, { timeout: 500 }, // not moving slide, but changing to the second slide to move that
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, // selecting the drawing on the slide
                    { timeout: 1000 },
                    { keyboardevents: [
                        'SPACE ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] },
                    { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { timeout: 500 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 },
                    { buttonclick: 'undo' }, { timeout: 50 },
                    { buttonclick: 'redo' }, { timeout: 50 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_35F: { // for DOCS-4066
        description: 'Two client test for insertText operations while the other client inserts and removes some slides',
        tooltip: 'Testing insertText operations while another client inserts and removes some slides',
        clients: 3,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, // using the side pane!
                    { timeout: 100 },
                    { keyboardevents: 'CTRL+a DELETE' }, // deleting all slides
                    { timeout: 200 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 20000, height: 1000 } }] } },
                    { keyboardevents: 'SHIFT+s l i d e SPACE t o SPACE t y p e' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE ESCAPE' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, // selecting the drawing on the slide
                    { timeout: 1000 },
                    { keyboardevents: [
                        'SPACE ENTER',
                        'a b c d e x y z 1 ENTER',
                        'a b c d e x y z 2 ENTER',
                        'a b c d e x y z 3 ENTER',
                        'a b c d e x y z 4 ENTER',
                        'a b c d e x y z 5 ENTER',
                        'a b c d e x y z 6 ENTER',
                        'a b c d e x y z 7 ENTER',
                        'a b c d e x y z 8 ENTER',
                        'a b c d e x y z 9 ENTER',
                        'a b c d e x y z 0 ENTER',
                        'a b c d e x y z A ENTER',
                        'a b c d e x y z B ENTER',
                        'a b c d e x y z C ENTER',
                        'a b c d e x y z D ENTER',
                        'a b c d e x y z E ENTER',
                        'a b c d e x y z F ENTER',
                        'a b c d e x y z G ENTER',
                        'a b c d e x y z H ENTER',
                        'a b c d e x y z I ENTER',
                        'a b c d e x y z J ENTER',
                        'a b c d e x y z K ENTER',
                        'a b c d e x y z L ENTER',
                        'a b c d e x y z M ENTER',
                        'a b c d e x y z N ENTER',
                        'a b c d e x y z O ENTER',
                        'a b c d e x y z P ENTER',
                        'a b c d e x y z Q ENTER',
                        'a b c d e x y z R ENTER',
                        'a b c d e x y z S ENTER',
                        'a b c d e x y z T ENTER',
                        'a b c d e x y z U ENTER',
                        'a b c d e x y z V ENTER',
                        'a b c d e x y z W ENTER',
                        'a b c d e x y z X ENTER',
                        'a b c d e x y z Y ENTER',
                        'a b c d e x y z Z ENTER'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['UP_ARROW'] },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['UP_ARROW'] },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['UP_ARROW'] }

                ]
            },
            CLIENT_3: {} // a watcher
        },
        result: {
            CLIENT_1: {}
        }
    },

    // single client, presentation mode
    TEST_36: {
        description: 'Simple single presentation mode',
        tooltip: 'Single client presentation mode',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' } // twice 'delete' to remove regenerated placeholder drawings, too
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { timeout: 100 },
                    { activatetoolbar: 'format' },
                    { timeout: 500 },
                    { assert: { id: 2, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.slide' }, { name: 'length', isProperty: true }], result: 1 }, text: 'Unexpected number of slide nodes.' } },
                    { assert: { id: 4, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.slide' }, { name: 'children', param: '.drawing' }, { name: 'length', isProperty: true }], result: 0 }, text: 'Unexpected number of drawings on slide.' } },
                    { buttonclick: { dataKey: 'layoutslidepicker/changelayout', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="' + LAYOUT_SLIDE_IDS[0] + '"]' } },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { assert: { id: 6, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.slide' }, { name: 'children', param: '.drawing' }, { name: 'length', isProperty: true }], result: 2 }, text: 'Unexpected number of drawings on slide.' } },
                    { keyboardevents: 'TAB TAB SHIFT+S l i d e SPACE 1' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="orange"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 2' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="yellow"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 3' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="green"]' } },
                    { timeout: 500 },
                    { keyboardevents: 'ESCAPE' },
                    { timeout: 100 },
                    { assert: { id: 10, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.slide' }, { name: 'length', isProperty: true }], result: 3 }, text: 'Unexpected number of slide nodes.' } },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } },
                    { timeout: 500 },
                    { keyboardevents: 'TAB SHIFT+S l i d e SPACE 4' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="blue"]' } },
                    { timeout: 500 },
                    { assert: { id: 20, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.slide' }, { name: 'length', isProperty: true }], result: 4 }, text: 'Unexpected number of slide nodes.' } },
                    { assert: { id: 30, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 3 }, text: 'Unexpected active slide index.' } },
                    { activatetoolbar: 'present' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'present/fullscreenpresentation' } }, // no fullscreen mode in testrunner test allowed
                    { timeout: 500 },
                    { assert: { id: 40, expect: { type: 'function', context: 'model', function: 'isPresentationMode', result: false }, text: 'Unexpected presentation mode.' } },
                    { buttonclick: { dataKey: 'present/startpresentation' } },
                    { timeout: 200 },
                    { assert: { id: 50, expect: { type: 'function', context: 'model', function: 'isPresentationMode', result: true }, text: 'Unexpected presentation mode.' } },
                    { assert: { id: 55,  expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'RIGHT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 60, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'RIGHT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 70, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 2 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'RIGHT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 80, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 3 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'LEFT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 90, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 2 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'LEFT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 100, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 1 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'LEFT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 110, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'END' },
                    { timeout: 200 },
                    { assert: { id: 120, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 3 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'HOME' },
                    { timeout: 200 },
                    { assert: { id: 130, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 0 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'END' },
                    { timeout: 200 },
                    { assert: { id: 140, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 3 }, text: 'Unexpected active slide index.' } },
                    { keyboardevents: 'RIGHT_ARROW' },
                    { timeout: 200 },
                    { assert: { id: 150, expect: { type: 'function', context: 'model', function: 'getActiveSlideIndex', result: 3 }, text: 'Unexpected active slide index.' } },
                    { assert: { id: 160, expect: { type: 'function', context: 'model', function: 'isPresentationMode', result: true }, text: 'Unexpected presentation mode.' } },
                    { keyboardevents: 'RIGHT_ARROW' },
                    { assert: { id: 170, expect: { type: 'function', context: 'model', function: 'isPresentationMode', result: false }, text: 'Unexpected presentation mode.' } }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 0], end: [0, 0] }
            }
        }
    }
};
