/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import * as Op from '@/io.ox/office/textframework/utils/operations';

// constants ==============================================================

const LOREM_IPSUM_STRING = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Presentation.
 */
export default {

    TEST_37: {
        description: 'Strong 4 client test: Combination of the sparring partner tests 31 to 31C and additionally one client as a watcher',
        tooltip: 'A strong 4 client test as combination of the sparring partner tests 31 to 31C and additionally one client as a watcher',
        clients: 4,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 10000, height: 3000 } }] } },
                    { timeout: 100 },
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: '0 ' + LOREM_IPSUM_STRING },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 0, 2], end: [0, 0, 0, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 1] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 1, 2], end: [0, 0, 1, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 2] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 2, 2], end: [0, 0, 2, 592], attrs: { character: { underline: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 3] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 3, 0], text: '3 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 3, 2], end: [0, 0, 3, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 4] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 4, 0], text: '4 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 4, 2], end: [0, 0, 4, 592], attrs: { character: { underline: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 5] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 5, 0], text: '5 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 5, 2], end: [0, 0, 5, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 6] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 6, 0], text: '6 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 6, 2], end: [0, 0, 6, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 7] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 7, 0], text: '7 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 7, 2], end: [0, 0, 7, 592], attrs: { character: { underline: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 8] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 8, 0], text: '8 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 8, 2], end: [0, 0, 8, 592], attrs: { character: { bold: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 9] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 9, 0], text: '9 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 6, 2], end: [0, 0, 6, 592], attrs: { character: { italic: true } } },
                        { name: Op.PARA_INSERT, start: [0, 0, 10] },
                        { name: Op.TEXT_INSERT, start: [0, 0, 10, 0], text: '10 ' + LOREM_IPSUM_STRING  },
                        { name: Op.SET_ATTRIBUTES, start: [0, 0, 10, 3], end: [0, 0, 10, 592], attrs: { character: { underline: true } } }
                    ] },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'ESCAPE' },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: 'CTRL+a DELETE' }, { timeout: 500 }, // deleting all drawings
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 10000, height: 1000 } }] } }, { keyboardevents: '2' }, { timeout: 100 },
                    { keyboardevents: '2*ESCAPE CTRL+a' }, { timeout: 1000 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, { timeout: 500 },
                    { keyboardevents: 'ESCAPE' }
                ]
            },
            CLIENT_3: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 500 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 3000, width: 18000, height: 1000 } }] } }, { keyboardevents: '1' }, { timeout: 100 }
                ]
            },
            CLIENT_4: {} // a watcher
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 }, // selecting the complete content
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 }, // { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { keyboardevents: 'CTRL+v' }, { timeout: 2000 }, // { functioncall: { functionname: 'pasteInternalClipboard' } }
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } },
                    { keyboardevents: 'ESCAPE CTRL+a' }, { timeout: 100 },
                    { keyboardevents: 'CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }, { keyboardevents: 'ESCAPE TAB CTRL+c' }, { timeout: 1000 },
                    { functioncall: { functionname: 'pasteInternalClipboard' } }, { timeout: 2000 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, { timeout: 200 }, // using the side pane!
                    { keyboardevents: 'HOME' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }, { timeout: 500 },
                    { buttonclick: 'undo' }, { timeout: 500 }, { buttonclick: 'redo' }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    ] }
                ]
            },
            CLIENT_4: {} // watcher
        }
    },

    TEST_38: {
        description: '4 client test: Shifting multiple slides, inserting text, setting attributes and a watcher (DOCS-4284)',
        tooltip: 'Shifting multiple slides, inserting text, setting attributes and a watcher',
        clients: 4,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } },
                    { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } },
                    { timeout: 100 },
                    { keyboardevents: 'HOME CTRL+a DELETE CTRL+a DELETE' },
                    { timeout: 200 },
                    { functioncall: { functionname: 'insertTextFrame', params: [{ rectangle: { left: 2000, top: 1000, width: 15000, height: 3000 } }] } },
                    { timeout: 500 },
                    { keyboardevents: ['1'] }, { timeout: 500 },
                    { activatetoolbar: 'drawing' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-red"]' } }, { timeout: 500 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 2'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 100 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="red"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 3'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 100 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="orange"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 4'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="yellow"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 5'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-green"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 6'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="green"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 7'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-blue"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 8'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="blue"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 9'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-blue"]' } }, { timeout: 100 },
                    { activatetoolbar: 'insert' }, { timeout: 500 },
                    { buttonclick: { dataKey: 'layoutslidepicker/insertslide' } }, { timeout: 500 },
                    { keyboardevents: ['TAB 1 0'] }, { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="purple"]' } }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, { timeout: 100 },
                    { keyboardevents: ['TAB a'] }
                ]
            },
            CLIENT_3: {
                timers: {
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { target: 'APP_CONTENT_ROOT' } } }, { timeout: 100 },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'ACTIVE_SLIDE' } } }, { timeout: 500 },
                    { keyboardevents: ['TAB b'] }
                ]
            },
            CLIENT_4: {} // a watcher
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 10
                },
                orders: [
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.slide-pane > .slide-pane-container > .slide-container:first' } } }, { timeout: 200 }, // using the side pane!
                    { keyboardevents: '10*DOWN_ARROW' },
                    { keyboardevents: 'SHIFT+UP_ARROW SHIFT+UP_ARROW SHIFT+UP_ARROW SHIFT+UP_ARROW' }, { timeout: 500 }, // slide multi selection
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+UP_ARROW' }, { timeout: 500 },
                    { keyboardevents: 'CTRL+DOWN_ARROW' }, { timeout: 500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 10
                },
                orders: [
                    { activatetoolbar: 'format' }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="orange"]' } }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="yellow"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-green"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="green"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-blue"]' } }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="purple"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { blockexternalops: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="orange"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { blockexternalops: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="yellow"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { blockexternalops: 1000 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-green"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="green"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-blue"]' } }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="purple"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="orange"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="yellow"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-green"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="green"]' } }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="light-blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-blue"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { blockexternalops: 500 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="purple"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="dark-red"]' } }, { timeout: 300 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, { timeout: 300 },
                    { buttonclick: { selector: '[data-section="standard"] a[data-value="red"]' } }, { timeout: 300 }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: [
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER',
                        'BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE BACKSPACE',
                        'a b c d e f g h i j k l m n o p q r s t u v w x y z SPACE a b c d e f g h i j k l m n o p q r s t u v w x y z ENTER'
                    ] }
                ]
            },
            CLIENT_4: {} // watcher
        }
    }

};
