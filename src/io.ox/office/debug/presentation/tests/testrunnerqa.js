/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import * as Op from '@/io.ox/office/textframework/utils/operations';

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Presentation (QA).
 */
export default {

    // two clients, with commands, adding content into the same paragraph
    TEST_0001_QA: {
        description: 'Writing content in different placeholder drawings',
        tooltip: 'A two clients test with text input into two different placeholder drawings',
        clients: 2,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0, 0, 0], text: 'abc' }
                    ] },
                    { selection: { start: [0, 0, 0, 1], end: [0, 0, 0, 3] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 2] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', '7', '8'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 0, 0, 9], end: [0, 0, 0, 9] }
            },
            CLIENT_2: {
                expectedSelection: { start: [0, 1, 0, 8], end: [0, 1, 0, 8] }
            }
        }
    }
};
