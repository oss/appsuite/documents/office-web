/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun, jpromise } from "@/io.ox/office/tk/algorithms";

import { getFlag, setValue, getDebugFlag, resetAllDebugFlags, USE_LOCAL_STORAGE, REALTIME_DEBUGGING } from "@/io.ox/office/editframework/utils/editconfig";
import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import { handleMinifiedActions } from "@/io.ox/office/editframework/utils/operationutils";
import type { CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import { otLogger } from "@/io.ox/office/editframework/utils/otutils";
import type { EditView } from "@/io.ox/office/editframework/view/editview";
import type { DebugProblemKey } from "@/io.ox/office/editframework/app/rtconnection";
import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

import { type DebugQuitErrorKind, importDebugAppModule, registerUserConfigItem } from "@/io.ox/office/debug/common/app/apputils";
import { AppDebugContext } from "@/io.ox/office/debug/common/app/debugcontext";
import { overlayConsole } from "@/io.ox/office/debug/common/view/pane/overlayconsole";

import "@/io.ox/office/debug/common/view/debugstyles.less";

// globals ====================================================================

// storage for additional debug data stored per application
const debugContextStorage = new Map<EditApplication, AppDebugContext>();

// private functions ==========================================================

/**
 * Sends an invalid operation to the server. Depending on the options different
 * invalid operations can be created and sent to the server.
 *
 * @param docApp
 *  The document application to be used.
 *
 * @param operation
 *  The document operation to be sent to the server.
 */
function sendInvalidOperation(docApp: EditApplication, operation: Operation): MaybeAsync {

    // add OSN and OPL to the operation
    const osn = docApp.docModel.getOperationStateNumber();
    operation = { ...operation, osn, opl: 1 };
    docApp.docModel.setOperationStateNumber(osn + 1);

    // the actions to be sent to the server
    // DOCS-892: clone and minify operations before sending to server
    const actionsBuffer = handleMinifiedActions([{ operations: [operation] }]);

    // send the actions buffer directly via the RT connection
    return docApp.rtConnection?.sendActions(actionsBuffer);
}

/**
 * Updates the GUI according to the current state of debug user settings.
 *
 * @param docApp
 *  The aplication instance to be extended with debug actions.
 *
 * @param _debugContext
 *  The storage for additional debug data per application.
 */
function updateDebugSettings(docApp: EditApplication, _debugContext: AppDebugContext): void {
    docApp.getWindowNode().toggleClass("debug-highlight", getDebugFlag("office:debug-dom"));
}

/**
 * Registers all debug action items in the passed document controller.
 *
 * @param docApp
 *  The aplication instance to be extended with debug actions.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
function registerDebugItems(docApp: EditApplication, debugContext: AppDebugContext): void {

    // the application components
    const { docModel, docController } = docApp;
    const { replayWorker, testRunner } = debugContext;

    // item keys for drop-down menus (automated testing)
    docController.registerItem("view/debug/actions/menu", {});
    docController.registerItem("view/debug/options/menu", {});
    docController.registerItem("view/debug/loggers/menu", {});

    // simple browser reload
    docController.registerItem<void>("debug/reload", {
        set() { window.location.reload(); }
    });

    // clear the saved debug settings to restore defaults
    docController.registerItem<void>("debug/restoredefaults", {
        set: resetAllDebugFlags
    });

    // trigger a specific RT event
    docController.registerItem<DebugProblemKey>("debug/realtime/trigger", {
        enable() { return REALTIME_DEBUGGING; },
        set(type) { docApp.rtConnection?.debugTriggerProblem(type); }
    });

    // apply a document operation at the document model
    docController.registerItem<Operation>("debug/operations/apply", {
        parent: "document/editable",
        set(operation) { docModel.applyOperations(operation); }
    });

    // send operation via RT channel
    docController.registerItem<Operation>("debug/realtime/sendop", {
        parent: "document/editable",
        set(operation) { return sendInvalidOperation(docApp, operation); }
    });

    // send operations via RT channel in read-only mode
    docController.registerItem<Operation>("debug/realtime/sendop/readonly", {
        parent: "document/readonly",
        set(operation) { return sendInvalidOperation(docApp, operation); }
    });

    // send operation to add character attributes to the current selection
    docController.registerItem<CharacterAttributes>("debug/operations/applycharacterattrs", {
        parent: "document/editable",
        set(attrs) { docModel.addCharacterAttributes(attrs); }
    });

    // start busy screen with and without Cancel button
    docController.registerItem<boolean>("debug/view/busy", {
        set(cancelable) {
            const def = jpromise.deferred();
            return cancelable ? docApp.createAbortablePromise(def) : def.promise();
        },
        async: "cancelable"
    });

    // try to quit the application with unsaved changes
    docController.registerItem<void>("debug/quit/unsaved", {
        async set() {
            try {
                // temporarily overwrite the method `hasUnsavedChangesOffice`
                docApp.hasUnsavedChangesOffice = fun.true;
                await docApp.quit();
            } catch {
                // quit was canceled: back to class prototype method
                delete (docApp as unknown as Dict).hasUnsavedChangesOffice;
            }
        }
    });

    // trigger a document reload with 3 seconds delay to be able
    // to setup a specific UI state manually before the reload happens
    docController.registerItem<void>("debug/quit/reload", {
        shortcut: { keyCode: "R", ctrlOrMeta: true, shift: true, alt: true },
        set() {
            docController.setTimeout(() => {
                jpromise.floating(docApp.reloadDocument(null, true));
            }, 3000);
        }
    });

    // try to quit the application with a type error in the quit handlers
    docController.registerItem<DebugQuitErrorKind>("debug/quit/typeerror", {
        set(errorKind) {
            const typeErrorFn = (): void => { throw new TypeError("debug type error"); };
            switch (errorKind) {
                case "beforeQuitHandlerSync":
                    docApp.on("docs:edit:beforequithandler", typeErrorFn);
                    break;
                case "quitHandlerSync":
                    docApp.on("docs:edit:quithandler", typeErrorFn);
                    break;
                case "quitHandlerAsync":
                    docApp.on("docs:edit:quithandler:after", typeErrorFn);
                    break;
            }
            // do not return the promise, otherwise the controller would wait for its own destruction
            jpromise.floating(docApp.quit());
        }
    });

    // downloads the buffered debug log as text file
    docController.registerItem<void>("debug/downloadlog", {
        set() { docApp.downloadFrontendLog(); }
    });

    // enables/disables the server-side slow-save mode
    docController.registerItem<boolean>("debug/option/slowsave", {
        enable() { return getFlag("module/debugslowsave"); },
        get() { return getFlag("module/debugslowsave") && getFlag("module/debugslowsaveuser"); },
        set(state) { return setValue("module/debugslowsaveuser", state); }
    });

    // OT DEMO
    docController.registerItem<boolean>("debug/ot/blockingops", {
        parent: "document/editable",
        get() { return docApp.isOTBlockingOpsMode(); },
        set(state) { docApp.setOTBlockingOpsMode(state); }
    });

    // write some statistics to the OT logger
    docController.registerItem<void>("debug/ot/statistics", {
        set() {
            const statistics = docApp.getOTStatistics();
            otLogger.info(`Checksum of the current document: ${docModel.getDocumentChecksum()}`);
            otLogger.info(`External operations handled by OT: ${statistics.extOpCount}`);
            otLogger.info(`Number of transformations: ${statistics.otCount}`);
        }
    });

    // starts a test of the UI test runner
    docController.registerItem<string>("debug/testrunner/start", {
        parent: "document/editable",
        enable() { return !replayWorker.running && !testRunner.isTestRunning(); },
        set(testId) { return testRunner.start(testId); },
        async: "background"
    });

    // stops the current test of the UI test runner
    docController.registerItem<void>("debug/testrunner/stop", {
        parent: "document/editable",
        enable() { return testRunner.isTestRunning(); },
        set() { testRunner.stop(); }
    });

    // server-side user configuration
    registerUserConfigItem(docApp, "debug/option/localstorage", "debugUseLocalStorage", USE_LOCAL_STORAGE, {
        enable() { return !docApp.isOTEnabled(); }
    });

    registerUserConfigItem(docApp, "debug/option/fastload", "useFastLoad", true);

    // initialize all debug settings
    updateDebugSettings(docApp, debugContext);

    // update all debug settings when the URL hash changes
    docController.listenToGlobal("change:config:debug", () => {
        jpromise.floating(docController.update());
        updateDebugSettings(docApp, debugContext);
    });
}

// public functions ===========================================================

/**
 * Main entry point for debug initialization of a Documents application.
 *
 * Automatically calls special initialization code defined in the same function
 * exported from the module `io.ox/office/debug/<app>/main`.
 *
 * @param docApp
 *  The application instance to be extended with debug functionality.
 */
export async function initializeApp(docApp: EditApplication): Promise<void> {

    // storage for additional debug data for this application
    const debugContext = new AppDebugContext(docApp);
    debugContextStorage.set(docApp, debugContext);

    // remove debug data from global storage
    docApp.on("docs:destroying", () => {
        debugContextStorage.delete(docApp);
        debugContext.destroy();
    });

    // register common controller items for debugging
    registerDebugItems(docApp, debugContext);

    // load application-specific initialization code
    await importDebugAppModule(docApp, module => module.initializeApp?.(docApp, debugContext));
}

/**
 * Main entry point for debug initialization of a Documents view.
 *
 * Automatically calls special initialization code defined in the same function
 * exported from the module `io.ox/office/debug/<app>/main`.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 */
export async function initializeView(docView: EditView): Promise<void> {

    // storage for additional debug data for this application
    const debugContext = debugContextStorage.get(docView.docApp);
    if (!debugContext) { return; }

    // initialize additional context members
    debugContext.initializeView(docView);

    // load application-specific initialization code
    await importDebugAppModule(docView.docApp, module => module.initializeView?.(docView, debugContext));
}

/**
 * Main entry point for debug initialization of the debug tool bar in the
 * Documents view.
 *
 * Automatically calls special initialization code defined in the same function
 * exported from the module `io.ox/office/debug/<app>/main`.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 */
export async function initializeToolPane(docView: EditView): Promise<void> {

    // storage for additional debug data for this application
    const debugContext = debugContextStorage.get(docView.docApp);
    if (!debugContext) { return; }

    // initialize additional context members
    debugContext.initializeToolPane(docView);

    // load application-specific initialization code
    await importDebugAppModule(docView.docApp, module => module.initializeToolPane?.(docView, debugContext));
}

// static initilaization ======================================================

overlayConsole.initialize();
