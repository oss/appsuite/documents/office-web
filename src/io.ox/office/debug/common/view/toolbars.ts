/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { getInt } from "@/io.ox/office/tk/config";
import { Button } from "@/io.ox/office/tk/controls";

import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import { CLIPBOARD_CUT_OPTIONS, CLIPBOARD_COPY_OPTIONS, CLIPBOARD_PASTE_OPTIONS } from "@/io.ox/office/editframework/view/editlabels";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import { DebugActionsPicker } from "@/io.ox/office/debug/common/view/control/debugactionspicker";
import { DebugOptionsPicker } from "@/io.ox/office/debug/common/view/control/debugoptionspicker";
import { DebugLoggersPicker } from "@/io.ox/office/debug/common/view/control/debugloggerspicker";
import { TestRunPicker } from "@/io.ox/office/debug/common/view/control/testrunpicker";
import { ThemeColorPicker } from "@/io.ox/office/debug/common/view/control/themecolorpicker";
import type { AppDebugContext } from "@/io.ox/office/debug/common/app/debugcontext";

// re-exports =================================================================

export * from "@/io.ox/office/debug/common/view/replaytoolbar";

// constants ==================================================================

const SLOW_SAVE = getInt("module/debugslowsavetime");

// class DebugToolBar =========================================================

export class DebugToolBar extends ToolBar {

    readonly actionsPicker: DebugActionsPicker;
    readonly optionsPicker: DebugOptionsPicker;
    readonly loggersPicker: DebugLoggersPicker;

    // constructor ------------------------------------------------------------

    constructor(docView: EditView) {

        // base constructor
        super(docView);

        // properties
        this.actionsPicker = new DebugActionsPicker(docView);
        this.optionsPicker = new DebugOptionsPicker(docView);
        this.loggersPicker = new DebugLoggersPicker(docView);

        // actions and options
        this.addSection("actions");
        this.addControl("view/debug/actions/menu", this.actionsPicker);
        this.addControl("view/debug/options/menu", this.optionsPicker);

        // debug logging
        this.addSection("loggers");
        this.addControl("view/debug/loggers/menu", this.loggersPicker);
        this.addControl("debug/downloadlog", new Button({ icon: "bi:life-preserver", tooltip: _.noI18n("Download debug log as text file") }));

        // UI theming
        this.addSection("theme");
        this.addControl(null, new ThemeColorPicker());

        // other controls (extendable by applications)
        this.addSection("custom");
        this.addControl("debug/option/slowsave", new Button({ icon: "bi:disc", tooltip: _.noI18n(`Slow down saving document (${SLOW_SAVE}s)`), toggle: true }));
    }
}

// class ClipboardToolBar =====================================================

export class ClipboardToolBar extends ToolBar {

    constructor(docView: EditView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("document/cut",            new Button({ ...CLIPBOARD_CUT_OPTIONS,   label: null }));
        this.addControl("document/copy",           new Button({ ...CLIPBOARD_COPY_OPTIONS,  label: null }));
        this.addControl("document/paste/internal", new Button({ ...CLIPBOARD_PASTE_OPTIONS, label: null }));
    }
}

// class OTToolBar ============================================================

export class OTToolBar extends ToolBar {

    constructor(docView: EditView, debugContext: AppDebugContext) {

        // base constructor
        super(docView, { visibleKey: "document/ot/enabled" });

        // OT controls
        this.addSection("ot");
        this.addControl("debug/ot/blockingops", new Button({ icon: "bi:x-octagon", toggle: true, tooltip: _.noI18n("OT: Block sending operations to the server") }));
        this.addControl("debug/ot/statistics", new Button({ icon: "bi:calculator", tooltip: _.noI18n("OT: Write statistics of current document to console") }));

        // test runner controls
        this.addSection("testrunner");
        this.addControl("debug/testrunner/start", new TestRunPicker(debugContext.testRunner));
        // this.addControl("debug/testrunner/stop", new Button({ icon: "bi:stop", tooltip: _.noI18n("Stop current test run") }));
    }
}
