/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { is, debug } from "@/io.ox/office/tk/algorithms";
import { getDebugFlag } from "@/io.ox/office/tk/config";
import { isJQuery, createElement, createSpan, matchKeyCode, isEscapeKey, isSelectAllKeyEvent } from "@/io.ox/office/tk/dom";

import { BasePane } from "@/io.ox/office/baseframework/view/pane/basepane";

import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import "@/io.ox/office/debug/common/view/pane/clipboardpane.less";

// private functions ==========================================================

/**
 * Creates a `<table>` element for the passed JSON document operations.
 *
 * @param operations
 *  The JSON document operations to be rendered.
 *
 * @returns
 *  The `<table>` element filled with the passed operations.
 */
function createOperationsTable(operations: Operation[]): HTMLTableElement {

    // create the <table> element with header row
    const tableNode = createElement("table");
    const theadNode = tableNode.appendChild(createElement("thead"));
    const hrowNode = theadNode.appendChild(createElement("tr"));
    hrowNode.appendChild(createElement("th"));
    hrowNode.appendChild(createElement("th", { label: "Name" }));
    hrowNode.appendChild(createElement("th", { label: "Parameters" }));

    // add table rows for all operations
    const tbodyNode = tableNode.appendChild(createElement("tbody"));
    operations.forEach((operation, index) => {
        const { name, osn, opl, ...props } = operation;
        const rowNode = tbodyNode.appendChild(createElement("tr"));
        rowNode.appendChild(createElement("td", { label: String(index) }));
        rowNode.appendChild(createElement("td", { label: name }));
        rowNode.appendChild(createElement("td", { label: debug.stringify(props) }));
    });

    return tableNode;
}

// class ClipboardPane ========================================================

/**
 * A view pane attached to the bottom border of the application view that
 * displays the HTML markup contained in the browser clipboard.
 *
 * @param docView
 *  The document view containing this pane instance.
 */
export class ClipboardPane extends BasePane<EditView> {

    /** The container element for the HTML markup of the clipboard contents. */
    readonly $container: JQuery;

    // constructor ------------------------------------------------------------

    constructor(docView: EditView) {

        // base constructor
        super(docView, {
            position: "bottom",
            classes: "clipboard-pane noI18n",
            size: 150,
            resizable: true,
            minSize: 100,
            maxSize: 400,
            landmark: false
        });

        // the DOM node containing the clipboard data
        this.$container = $('<div class="clip-container" tabindex="0">').append(createSpan("Clipboard debug window"));
        this.$el.append(this.$container);

        // update visibility of this view pane
        this.listenToGlobal("change:config:debug", this.#updateVisibility);
        this.#updateVisibility();

        // keyboard shortcuts
        this.listenTo(this.$container, "keydown", this.#keyDownHandler);

        // listen to "debug:clipboard" events
        this.listenTo(this.docModel, "debug:clipboard", this.#setClipboardContent);
    }

    // private methods --------------------------------------------------------

    #updateVisibility(): void {
        this.toggle(getDebugFlag("office:clipboard-pane"));
    }

    #setClipboardContent(content: unknown): void {
        this.$container.empty();
        if (is.array(content) && (content.length > 0)) {
            this.$container.append(createOperationsTable(content as Operation[]));
        } else if (isJQuery(content) && (content.contents().length > 0)) {
            this.$container.append(createSpan({ label: content.html() }));
        } else if (is.string(content) && (content.length > 0)) {
            this.$container.append(createSpan({ label: content }));
        } else {
            this.$container.append(createSpan({ label: "Clipboard does not contain data." }));
        }
    }

    #keyDownHandler(event: JTriggeredEvent): void | false {

        if (isSelectAllKeyEvent(event)) {
            const target = event.delegateTarget as Element;
            const docRange = window.document.createRange();
            docRange.setStart(target, 0);
            docRange.setEnd(target, 1);
            window.getSelection()?.removeAllRanges();
            window.getSelection()?.addRange(docRange);
            return false;
        }

        if (matchKeyCode(event, "DELETE")) {
            this.$container.empty();
            return false;
        }

        if (isEscapeKey(event)) {
            this.docView.grabFocus();
            return false;
        }
    }
}
