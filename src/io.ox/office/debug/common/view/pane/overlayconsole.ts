/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import moment from "$/moment";

import { is, set, dict } from "@/io.ox/office/tk/algorithms";
import { onDebugFlag } from "@/io.ox/office/tk/config";
import type { IconId, CreateButtonOptions } from "@/io.ox/office/tk/dom";
import { detachChildren, createElement, createDiv, createSpan, createIcon, createButton } from "@/io.ox/office/tk/dom";
import { EObject } from "@/io.ox/office/tk/objects";

import { ObjectInspector } from "@/io.ox/office/debug/common/view/objectinspector";

import "@/io.ox/office/debug/common/view/pane/overlayconsole.less";

// types ======================================================================

type ToggleFn = (state: boolean) => void;

type ConsoleFn = FuncType<void, unknown[]>;

type ConsoleFnKey = "log" | "info" | "warn" | "error";

// constants ==================================================================

// icon names for different log levels
const ICONS: PtRecord<ConsoleFnKey, IconId> = {
    info: "bi:info-circle-fill",
    warn: "bi:exclamation-triangle-fill",
    error: "bi:x-circle-fill"
};

// globals ====================================================================

// the console as dictionary
const consoleDict = window.console as unknown as Dict<ConsoleFn>;

// the console methods to be hooked
const consoleFuncs: Record<ConsoleFnKey, ConsoleFn> = {
    log: window.console.log,
    info: window.console.info,
    warn: window.console.warn,
    error: window.console.error
};

// the states of the toggle buttons
const states = new Set<string>();

// class OverlayConsole =======================================================

class OverlayConsole extends EObject {

    // the root element of the DOM console window
    readonly #panel = createDiv({ id: "io-ox-office-console", classes: "io-ox-office-main noI18n collapsed" });
    // the popup header
    readonly #header = this.#panel.appendChild(createDiv("header"));
    // the message list
    readonly #output = this.#panel.appendChild(createDiv("output"));
    // the button toolbar
    readonly #buttons = this.#panel.appendChild(createDiv("buttons"));
    // inspector for objects/array logged into the console
    readonly #inspector = this.member(new ObjectInspector(this.#panel));

    // constructor ------------------------------------------------------------

    constructor() {
        super({ _kind: "singleton" });

        // show console when clicking on the "expand" icon
        this.#panel.appendChild(createButton({
            type: "bare",
            action: "expand",
            icon: "bi:bug-fill",
            tooltip: "Expand overlay console",
            click: () => {
                this.#panel.classList.remove("collapsed");
                this.#scrollDown();
            }
        }));

        // create header contents
        this.#header.append(
            createDiv({ classes: "title flex-grow", label: "Overlay Console" }),
            createButton({
                type: "bare",
                action: "move-left",
                icon: "bi:box-arrow-left",
                tooltip: "Move to left border",
                click: () => this.#panel.classList.remove("right-border")
            }),
            createButton({
                type: "bare",
                action: "move-right",
                icon: "bi:box-arrow-right",
                tooltip: "Move to right border",
                click: () => this.#panel.classList.add("right-border")
            }),
            createButton({
                type: "bare",
                action: "collapse",
                icon: "bi:box-arrow-in-down-left",
                tooltip: "Collapse overlay console",
                click: () => this.#panel.classList.add("collapsed")
            })
        );

        // create and prepare buttons
        this.#createCheckBox("time", true, { icon: "bi:watch", label: "Show time", tooltip: "Show timestamps for all messages" }, state => this.#output.classList.toggle("show-time", state));
        this.#createCheckBox("font", false, { icon: "bi:zoom-in", label: "Larger", tooltip: "Show messages with larger font size" }, state => this.#output.classList.toggle("large-font", state));
        this.#createCheckBox("error", true, { icon: "bi:exclamation-octagon", label: "Auto errors", tooltip: "Automatically show the console on uncaught JS exceptions" });
        this.#createCheckBox("scroll", true, { icon: "bi:chevron-double-down", label: "Auto scroll", tooltip: "Automatically scroll down after new messages" });
        this.#createButton({ icon: "bi:clipboard-plus", label: "Select all", tooltip: "Select all messages", click: this.#selectAll });
        this.#createButton({ icon: "bi:trash", label: "Clear", tooltip: "Remove all messages", click: () => detachChildren(this.#output) });
    }

    // public methods ---------------------------------------------------------

    initialize(): void {
        // toggle the console when the debug configuration has been changed
        onDebugFlag("office:debug-console", () => this.#enable(), () => this.#disable());
    }

    // private methods --------------------------------------------------------

    #scrollDown(): void {
        if (states.has("scroll")) {
            this.#output.scrollTop = this.#output.scrollHeight;
        }
    }

    #writeToConsole(type: ConsoleFnKey, ...args: unknown[]): void {
        const rowNode = this.#output.appendChild(createElement("p", type));
        rowNode.appendChild(createSpan({ classes: "time", label: moment().format("HH:mm:ss.SSS") }));
        rowNode.appendChild(createIcon(ICONS[type] ?? "png:none", { classes: "msg-icon", size: 12 }));
        if (is.string(args[0])) {
            const arg0 = args[0].replace(/%c/g, "");
            const tokens = (args[0].length - arg0.length) / 2;
            if (tokens > 0) {
                args = args.slice(tokens + 1);
                args.unshift(arg0);
            }
        }
        args.forEach(msg => {
            if (is.nullish(msg)) {
                rowNode.append(`<span style="font-style:italic">${msg}</span>`);
            } else if (is.dict(msg) || is.object(msg)) {
                // handle plain dictionaries a.k.a. Object.create(null) correctly
                // eslint-disable-next-line @typescript-eslint/no-base-to-string
                const label = is.function(msg.toString) ? String(msg) : "[object Dict]";
                rowNode.appendChild(this.#inspector.createAnchor(msg, { label }));
            } else {
                rowNode.appendChild(createSpan({ label: String(msg) }));
            }
        });
        if (states.has("error") && (type === "error")) {
            this.#panel.classList.remove("collapsed");
        }
        this.#scrollDown();
    }

    #selectAll(): void {
        const docRange = window.document.createRange();
        docRange.setStart(this.#output, 0);
        docRange.setEnd(this.#output, this.#output.childElementCount);
        window.getSelection()!.removeAllRanges();
        window.getSelection()!.addRange(docRange);
    }

    #createButton(options: CreateButtonOptions): HTMLButtonElement {
        const button = this.#buttons.appendChild(createButton({ type: "bare", ...options, click: options.click?.bind(this) }));
        button.addEventListener("dblclick", event => event.preventDefault());
        return button;
    }

    #updateCheckBox(button: HTMLButtonElement, action: string, state: boolean, toggleFn?: ToggleFn): void {
        set.toggle(states, action, state);
        button.classList.toggle("selected", state);
        toggleFn?.call(this, state);
    }

    #createCheckBox(action: string, state: boolean, options: CreateButtonOptions, toggleFn?: ToggleFn): HTMLButtonElement {
        const button = this.#createButton({ ...options, click: () => this.#updateCheckBox(button, action, !states.has(action), toggleFn) });
        this.#updateCheckBox(button, action, state, toggleFn);
        return button;
    }

    #enable(): void {

        // nothig to do, if already enabled
        if (this.#panel.parentElement) { return; }

        // replace implementations of browser console methods
        dict.forEach(consoleFuncs, (method, key) => {
            consoleDict[key] = (...args) => {
                method.apply(window.console, args);
                this.#writeToConsole(key, ...args);
            };
        });

        // global exception handler to forward unhandled exceptions to the DOM console
        this.listenTo(window, "error", event => this.#writeToConsole("error", event));

        // insert the root element into the DOM
        document.body.appendChild(this.#panel);
    }

    #disable(): void {

        // nothing to do, if already disabled
        if (!this.#panel.parentElement) { return; }

        // remove the console root node from the DOM
        this.#panel.remove();

        // remove global exception handler
        this.stopListeningTo(window, "error");

        // restore original console methods
        dict.forEach(consoleFuncs, (method, key) => (consoleDict[key] = method));
    }
}

// singletons =================================================================

export const overlayConsole = new OverlayConsole();
