/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { is, to, str, pick, json } from "@/io.ox/office/tk/algorithms";
import { getDebugFlag } from "@/io.ox/office/tk/config";
import { type NodeOrJQuery, setElementLabel, createElement, createDiv, createSpan, createIcon, createButton, createInput, isEscapeKey, isSelectAllKeyEvent } from "@/io.ox/office/tk/dom";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { DataPipeline } from "@/io.ox/office/tk/workers";

import { BasePane } from "@/io.ox/office/baseframework/view/pane/basepane";
import { handleMinifiedObject } from "@/io.ox/office/editframework/utils/operationutils";

import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import type { OTTestRunner } from "@/io.ox/office/debug/common/app/ottestrunner";
import type { OperationTracer } from "@/io.ox/office/debug/common/app/operationtracer";
import { ModelObjectInspector } from "@/io.ox/office/debug/common/view/objectinspector";

import "@/io.ox/office/debug/common/view/pane/operationspane.less";

const { floor } = Math;

// types ======================================================================

/**
 * Entry for a single document operation in the rendering pipeline.
 */
interface PipelineEntry {
    operation: Operation;
    external: boolean;
    removed?: boolean;
}

/**
 * Entry for a rendered table row of a document operation.
 */
interface OperationLogEntry {
    operation: Operation;
    rowNode: HTMLTableRowElement;
    osn1Node: HTMLTableCellElement;
    osn2Node: HTMLTableCellElement;
    osn3Node: HTMLTableCellElement;
    oplNode: HTMLTableCellElement;
    op1Node: HTMLTableCellElement;
    op2Node: HTMLTableCellElement;
    contents?: string[];
}

// constants ==================================================================

/**
 * Specifies whether the minified operations shall be displayed in the
 * operations pane.
 */
const SHOW_SHORT_OPERATIONS = getDebugFlag("office:short-operations");

// private functions ==========================================================

/**
 * Removes all control properties (operation name, OSN, OPL, ...) from the
 * passed JSON document operation, leaving only the data properties to be shown
 * in the "Properties" column of the output table.
 */
function reduceOperation(operation: Operation): Dict {
    const { name, n, osn, opl, dbg_op_id, dbg_user_id, dbg_merged, ...props } = operation as unknown as Dict;
    return props;
}

// class OperationsPane =======================================================

/**
 * A view pane attached to the bottom border of the application view that
 * contains the operations log, and other custom debug information defined
 * by the application.
 *
 * @param docView
 *  The document view containing this pane instance.
 *
 * @param testRunner
 *  The OT test runner. Used to log the result messages of finished tests.
 *
 * @param operationTracer
 *  The operation tracer that updates the operation pane asynchronously. The
 *  operation tracer contains more information about each operation, so that
 *  the operations pane can show more content. This is especially important in
 *  the OT context.
 */
export class OperationsPane extends BasePane<EditView> {

    // the operation tracer keeping track of OT additions
    readonly tracer: OperationTracer;

    // an inspector popup menu for details about operations and attribute sets
    readonly inspector: ModelObjectInspector;

    // background pipeline for logging all applied operations
    readonly #pipeline: DataPipeline<PipelineEntry>;
    // fragment for rendering multiple operations before inserting them into the DOM
    readonly #fragment = document.createDocumentFragment();

    // the operations log table
    readonly #$opTable: JQuery;

    // the debug info table
    readonly #$infoTable: JQuery;

    // entries for all operations already logged
    readonly #opLog: OperationLogEntry[] = [];
    readonly #opMap = new Map<number, OperationLogEntry>();

    // the text field for operations filter
    readonly #opFilterField: HTMLInputElement;
    #opFilterTokens: string[] = [];

    // delete all logged operations
    readonly #trashButton: HTMLButtonElement;

    // constructor ------------------------------------------------------------

    constructor(docView: EditView, testRunner: OTTestRunner, operationTracer: OperationTracer) {

        // base constructor
        super(docView, {
            position: "bottom",
            classes: "operations-pane noI18n",
            size: 300,
            resizable: true,
            minSize: 100,
            maxSize: 500
        });

        // public properties
        this.tracer = operationTracer;
        this.inspector = this.member(new ModelObjectInspector(docView, this.$el));

        // private properties
        this.#pipeline = this.member(new DataPipeline(entry => this.#logOperation(entry), { delay: 1000, slice: 100, interval: 500, paused: true }));

        // create the operations log table
        this.#$opTable = $('<table class="debug-table">').append(
            $("<colgroup>").append(
                '<col width="35px" span="6">',
                '<col width="100px">'
            ),
            $("<thead>").append($("<tr>").append(
                $('<th title="Operation sequence index">').text("Idx"),
                $('<th class="small" title="Operation State Number (Apply for internal OPs / current OSN when receiving external OPs)">').text("OSN1"),
                $('<th class="small" title="Operation State Number (Sent for internal OPs / apply for external OPs - OSN sent from server)">').text("OSN2"),
                $('<th class="small" title="Operation State Number (Sent with ACK)">').text("ACK"),
                $('<th class="small" title="Operation Length">').text("OPL"),
                $('<th class="small" title="User ID">').text("UID"),
                $('<th title="Long operation name">').text("Name"),
                $('<th title="Operation contents applied locally in the document">').text("Local"),
                $('<th title="Operation contents received from server or sent to server">').text("Remote")
            ))
        );

        // create the debug info table
        this.#$infoTable = $('<table class="debug-table">').append($("<colgroup>").append('<col width="70px">', "<col>"));

        // add the operations table and the info table to the debug pane
        [{ $table: this.#$opTable, classes: "ops" }, { $table: this.#$infoTable, classes: "info" }].forEach(({ $table, classes }) => {
            const scrollNode = $('<div class="scroll-container" tabindex="0">').append($table.append("<tbody>"));
            this.$el.append($(`<div class="output-panel ${classes}">`).append(scrollNode));
        });

        // create filter bar in operations pane
        const filterBar = createDiv("filter-bar");
        this.#$opTable.parent().before(filterBar);
        this.#opFilterField = filterBar.appendChild(createInput({ classes: "form-control", placeholder: "Filter..." }));
        this.listenTo(this.#opFilterField, ["input", "change"], this.#filterAllLogEntries);
        this.#trashButton = filterBar.appendChild(createButton({ icon: "bi:trash", tooltip: "Clear log" }));
        this.listenTo(this.#trashButton, "click", this.#clearOpLog);

        // create the output nodes in the debug pane
        this.addDebugInfoHeader("application", "Application");
        this.addDebugInfoNode("application", "doc-uid", "Document unique ID");
        this.addDebugInfoNode("application", "client-uid", "Client unique ID");
        this.addDebugInfoNode("application", "load-mode", "Mode used to load document content");
        this.addDebugInfoNode("application", "state", "Application state");
        this.addDebugInfoNode("application", "osn", "Operation state number");
        this.addDebugInfoNode("application", "undo", "Undo/redo stack state");
        this.addDebugInfoNode("application", "error", "Error description");
        this.addDebugInfoNode("application", "checksum", "The checksum of the current document state");

        // update visibility of this view pane
        this.listenToGlobal("change:config:debug", this.#updateVisibility);
        this.#updateVisibility();

        // log the application state, always show the pane when an internal application error occurs
        this.listenTo(this.docApp, "docs:state", state => {
            this.setDebugInfo("application", "state", state);
            if (state === "error") { this.show(); }
        });

        // log the operation state number of the document (for OT the server OSN is logged)
        this.listenTo(this.docModel, "change:osn", osn => {
            this.setDebugInfo("application", "osn", String(osn));
        });

        // log the checksum of the document
        this.listenTo(this.docModel, "update:checksum", checksum => {
            this.setDebugInfo("application", "checksum", checksum);
        });

        // log size of undo/redo stacks
        this.listenTo(docView, "controller:update", () => {
            const undoCount = docView.getControllerItemValue("document/undo");
            const redoCount = docView.getControllerItemValue("document/redo");
            this.setDebugInfo("application", "undo", `undo=${undoCount}`, `redo=${redoCount}`);
        });

        // collect and log all operations
        this.listenTo(this.docModel, "operations:after", (operations, external) => {
            if (!this.docApp.isOperationsBlockActive()) {
                operations.forEach(operation => {
                    this.#pipeline.pushValue({ operation, external });
                });
            }
        });

        // write message of internal errors
        this.listenTo(this.docApp, "docs:state:error", errorCode => {
            if (errorCode) {
                this.setDebugInfo("application", "error", errorCode.getDescription());
            }
        });

        // write messages of test runner
        this.listenTo(testRunner, "message", message => {
            this.setDebugInfo("application", "error", message);
        });

        // initialization after document import
        this.waitForImport(() => {
            this.setDebugInfo("application", "client-uid", this.docApp.getClientId());
            this.setDebugInfo("application", "doc-uid", this.docApp.getDocUID());
            const loadInfo = this.docApp.getLoadDocInfo();
            this.setDebugInfo("application", "load-mode", `mode=${this.docApp.getLoadMode()}`, `version=${loadInfo.fileVersion}`, `document-osn=${loadInfo["document-osn"]}`);
            // start logging collected operations after the document is imported
            if (this.isVisible()) { this.#pipeline.resume(); }
            // suspend operations logging while operations pane is hidden
            this.on("pane:hide", () => this.#pipeline.pause());
            this.on("pane:show", () => this.#pipeline.resume());
        });

        // Mark the last operations in the operations table with orange background color. This means,
        // that they were not executed, not send to the server and they do not influence the OSN for
        // the following operations. This scenario happens for example, if a paste process is cancelled
        // and an existing selection was already removed before the pasting started.
        this.listenTo(this.docApp, "docs:operations:remove", count => {

            // mark operations not yet logged
            const pendingCount = this.#pipeline.values.length;
            for (let index = Math.max(0, pendingCount - count); index < pendingCount; index += 1) {
                this.#pipeline.values[index].removed = true;
            }

            // mark operations already logged
            if (pendingCount < count) {
                const $rowNodes = this.#$opTable.find("tbody").children();
                for (let i = count - pendingCount, l = $rowNodes.length; i < l; i += 1) {
                    $rowNodes[i].classList.add("removed");
                }
            }
        });

        // listener for delayed update from the operation tracer. The missing content from internal
        // operations is updated after the server sent the ack OSN.
        this.listenTo(operationTracer, "refresh:operation", this.#refreshOperationHandler);

        // keyboard shortcuts
        this.listenTo(this.$el, "keydown", this.#keyDownHandler, { delegated: "[tabindex]" });
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds a new header row to the information window.
     *
     * @param header
     *  The name of the header row, used to identify data rows shown under the
     *  new header row.
     *
     * @param title
     *  The title label shown in the header row.
     *
     * @param [tooltip]
     *  A tooltip for the entire header row.
     */
    addDebugInfoHeader(header: string, title: string, tooltip?: string): void {
        const $headRow = $(`<tr data-header="${header}">`).attr("title", tooltip || null).append($('<th colspan="2">').text(title));
        this.#$infoTable.append($headRow);
    }

    /**
     * Adds a new data row to the information window.
     *
     * @param header
     *  The name of the header row to associate this data row to.
     *
     * @param label
     *  The name of the data row, used to identify the row when inserting text
     *  contents, and as label text.
     *
     * @param [tooltip]
     *  A tooltip for the first cell of the data row (the row label).
     */
    addDebugInfoNode(header: string, label: string, tooltip?: string): void {
        const $infoRow = $(`<tr data-header="${header}">`).append(
            $("<td>").attr("title", tooltip || null).text(label),
            $(`<td data-label="${label}">`)
        );
        this.#$infoTable.find(`tr[data-header="${header}"]`).last().after($infoRow);
    }

    /**
     * Changes the text shown in a specific data row of the information window.
     *
     * @param header
     *  The name of the header containing the data row to be changed.
     *
     * @param label
     *  The name of the data row to be changed.
     *
     * @param contents
     *  The new text contents, or DOM contents, shown in the data row.
     */
    setDebugInfo(header: string, label: string, ...contents: Array<NodeOrJQuery | string>): void {
        const $cell = this.#$infoTable.find(`tr[data-header="${header}"] td[data-label="${label}"]`).empty();
        contents.filter(Boolean).forEach((item, idx) => {
            if (idx) { $cell.append(createSpan({ label: ", " })); }
            $cell.append(is.string(item) ? createSpan({ label: item }) : item);
        });
    }

    // private methods --------------------------------------------------------

    #updateVisibility(): void {
        this.toggle(getDebugFlag("office:operations-pane"));
    }

    #createAnchor(value: Dict): HTMLAnchorElement {
        return this.inspector.createAnchor(value, { hideIcon: true });
    }

    /**
     * Renders a document operation.
     */
    #logOperation(entry: PipelineEntry): void {

        // this operation was removed by the optimize operation handler and merged into the previous operation
        if (entry.operation.dbg_merged) { return; }

        const { tracer } = this;

        let op1 = json.deepClone(entry.operation);

        if (SHOW_SHORT_OPERATIONS) {
            handleMinifiedObject(op1);
        }

        // operation identifier used in operation tracer (keeps track of operation transformations)
        const opId = op1.dbg_op_id || 0;
        const useTracer = (opId > 0) && tracer.containsTrace(opId);

        const name = op1.name || pick.string(op1, "n") || "";
        // the start OSN: For internal OP, when the OP was generated, for external OP, when the OP was received
        const osn1 = useTracer ? tracer.getStartOSN(opId) : floor(to.number(op1.osn, -1));
        // the final OSN: For internal OP, when the OP was sent to the server, for external OP, when it was applied
        const osn2 = useTracer ? tracer.getFinalOSN(opId) : -1;
        // the ack OSN, only for internal OPs: The OSN received from the server in the ACK for the sent operations
        const osn3 = (useTracer && !entry.external) ? tracer.getAckOSN(opId) : -1;
        // the OPL (number of originating operations merged into one operation)
        const opl = floor(to.number(op1.opl, -1));
        const userId = floor(to.number(op1.dbg_user_id, -1));

        if (useTracer) { op1 = tracer.getStartOperation(opId) ?? op1; }
        const op2 = useTracer ? tracer.getFinalOperation(opId) : undefined;

        // create the table row element
        const opCount = this.#opLog.length + 1;
        const source = entry.external ? "external" : "internal";
        const error = this.docModel.getOperationsCount() < opCount;
        const classes = { [source]: true, error, removed: !!entry.removed };
        const rowNode = createElement("tr", { classes });
        if (useTracer && !entry.external) { rowNode.dataset.opId = String(opId); }

        // create the table cells
        rowNode.appendChild(createElement("td", { label: String(opCount) }));
        const osn1Node = rowNode.appendChild(createElement("td", { classes: "text-right osn1", label: (osn1 >= 0) ? String(osn1) : null }));
        const osn2Node = rowNode.appendChild(createElement("td", { classes: "text-right osn2", label: (osn2 >= 0) ? String(osn2) : null }));
        const osn3Node = rowNode.appendChild(createElement("td", { classes: "text-right osn3", label: (osn3 >= 0) ? String(osn3) : null }));
        const oplNode = rowNode.appendChild(createElement("td", { classes: "text-right opl", label: (opl > 0) ? String(opl) : null }));
        rowNode.appendChild(createElement("td", { classes: "text-right", label: (userId >= 0) ? String(userId) : null }));
        rowNode.appendChild(createElement("td", { label: name, tooltip: name }));

        const op1Node = rowNode.appendChild(createElement("td", { classes: "op1" }));
        const op2Node = rowNode.appendChild(createElement("td", { classes: "op2" }));
        const props1 = reduceOperation(op1);
        if (useTracer) {
            const props2 = op2 ? reduceOperation(op2) : undefined;
            if (entry.external) {
                if (props2) {
                    op1Node.appendChild(this.#createAnchor(props2));
                }
                op2Node.appendChild(createIcon("bi:cloud-download"));
                op2Node.appendChild(this.#createAnchor(props1));
            } else {
                op1Node.appendChild(this.#createAnchor(props1));
                if (props2) {
                    op2Node.appendChild(createIcon("bi:cloud-upload"));
                    op2Node.appendChild(this.#createAnchor(props2));
                }
            }
        } else {
            op1Node.appendChild(this.#createAnchor(props1));
        }

        if (useTracer) {
            if (!entry.external) {
                // setting marker at operation tracer that the row has been generated
                // -> but this is often not the final state for internal OPs, that still wait for sending to the server and receiving the ACK osn
                // -> if the state is not final yet, the row must be updated later
                if (osn2 === -1) { tracer.updateFinalOSN(opId); }
                if (osn3 === -1) { tracer.updateAckOSN(opId); }
                if (!op2) { tracer.updateFinalOperation(opId); }
            }

            // informing the operation tracer about this registration
            tracer.operationPaneRegistered(opId);
        }

        // insert table row into internal fragment, trigger debounced DOM expansion
        this.#fragment.append(rowNode);
        this.#appendOperationsFragment();

        // create a new log entry
        const logEntry: OperationLogEntry = { operation: entry.operation, rowNode, osn1Node, osn2Node, osn3Node, oplNode, op1Node, op2Node };
        this.#opLog.push(logEntry);
        this.#opMap.set(opId, logEntry);
        this.#filterLogEntry(logEntry);
    }

    @debounceMethod({ delay: "animationframe" })
    #appendOperationsFragment(): void {
        // append all table rows from fragment to operations table (this empties the fragment)
        this.#$opTable.append(this.#fragment);
        // update scroll position of the operations table
        const scrollNode = this.#$opTable.parent()[0];
        scrollNode.scrollTop = scrollNode.scrollHeight;
    }

    #refreshOperationHandler(opId: number, osn2?: number, operation?: Operation, osn3?: number, opl?: number): void {
        const entry = this.#opMap.get(opId);
        if (!entry) { return; }
        if (is.number(osn2)) { setElementLabel(entry.osn2Node, String(osn2)); }
        if (is.number(osn3)) { setElementLabel(entry.osn3Node, String(osn3)); }
        if (operation) {
            entry.operation = operation;
            const props = reduceOperation(operation);
            entry.op2Node.querySelector("a")?.remove();
            entry.op2Node.append(this.#createAnchor(props));
            // if the merge of operations happened after the first logging of operations, it is possible,
            // that the operation length (opl) needs to be updated, too.
            setElementLabel(entry.oplNode, (opl && opl > 1) ? String(opl) : "");
            // refresh filter state
            delete entry.contents;
            this.#filterLogEntry(entry);
        }
    }

    #keyDownHandler(event: JTriggeredEvent): void | false {

        if (isSelectAllKeyEvent(event)) {
            const target = event.delegateTarget as Element;
            const docRange = window.document.createRange();
            docRange.setStart(target, 0);
            docRange.setEnd(target, 1);
            window.getSelection()?.removeAllRanges();
            window.getSelection()?.addRange(docRange);
            return false;
        }

        if (isEscapeKey(event)) {
            this.docView.grabFocus();
            return false;
        }
    }

    #filterLogEntry(entry: OperationLogEntry): void {
        const contents = (entry.contents ??= [entry.operation.name, entry.op1Node.textContent, entry.op2Node.textContent].map(text => text?.toLowerCase() || ""));
        const matches = this.#opFilterTokens.every(token => contents.some(text => text.includes(token)));
        entry.rowNode.classList.toggle("hidden", !matches);
    }

    @debounceMethod({ delay: 250, maxDelay: 1000 })
    #filterAllLogEntries(): void {
        this.#opFilterTokens = str.splitTokens(this.#opFilterField.value.toLowerCase());
        for (const entry of this.#opLog) { this.#filterLogEntry(entry); }
    }

    #clearOpLog(): void {
        this.#pipeline.abort();
        this.#$opTable.find("tbody").empty();
        this.#opLog.length = 0;
        this.#opMap.clear();
    }
}
