/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { DEBUG_AVAILABLE } from "@/io.ox/office/editframework/utils/editconfig";
import type { EditView } from "@/io.ox/office/editframework/view/editview";
import { DebugBasePicker } from "@/io.ox/office/debug/common/view/control/debugbasepicker";

// class DebugOptionsPicker ===================================================

export class DebugOptionsPicker extends DebugBasePicker {

    constructor(docView: EditView) {

        // base constructor
        super(docView, {
            icon: "bi:gear",
            label: _.noI18n("Options"),
            tooltip: _.noI18n("Set debug options")
        });

        // debug view panes
        this.addSection("panes", _.noI18n("Debug view panes"));
        this.registerOption("office:debug-console", "Toggle the DOM overlay console.");
        this.registerOption("office:operations-pane", "Show the operations view pane.");
        this.registerOption("office:clipboard-pane", "Toggle the clipboard contents pane.");

        // Core debug options
        this.addSection("core", _.noI18n("Core settings"));
        this.registerOption("tabmode", "Open all documents in the main browser tab instead of new standalone browser tabs.", { enable: "single", disable: "multi", hash: true, reload: true });
        this.registerOption("debug-i18n", "Highlight all untranslated text in the GUI.", { hash: true, reload: true });

        // general debug options
        this.addSection("options", _.noI18n("General settings"));
        this.registerOption("office:enable-debug", "Enables the local debug mode (loggers, debug toolbar, ...).", { default: DEBUG_AVAILABLE, reload: true });
        this.registerOption("office:simple-filestore", "Simulate a file store that uses the file name as unique key.");
        this.registerOption("office:prevent-logErrorData", "Prevent log file creation in Drive and sending client errors to the server log on this client.", { reload: true });
        this.registerOption("office:install-webfonts", "Always install replacement webfonts for Office fonts.", { reload: true });
        this.registerOption("office:testautomation", "Test automation mode used in E2E tests (DOM state markup, ...).", { reload: true });

        // view settings
        this.addSection("view", _.noI18n("View settings"));
        this.registerOption("office:sticky-popups", "Keep all popup menus open (no automatic close when clicking somewhere, please click again on the opener button to close it again).");
        this.registerOption("office:block-toolpane-init", "Whether to block initialization of invisible toolpane contents in a background idle loop.");
        this.registerOption("office:panes-combined", "Combine toppane and main toolpane into a single toolpane (small device mode).", { reload: true });
        this.registerOption("office:short-operations", "Show minified operations in the operations pane.", { reload: true });
        this.registerOption("office:debug-dom", "Highlight specific elements in the GUI (view panes, toolbars, form controls).");
        this.registerOption("office:collaborator-menu", "Allow to open the collaborators menu without other users.");
        this.registerOption("office:small-color-picker", "Switch color picker controls to reduced mode with a slider for scheme colors instead of the full palette.", { reload: true });

        // welcome tours
        this.addSection("tours", _.noI18n("Welcome tours"));
        this.registerOption("office:show-tours", "Always show the welcome tour when launching a Documents application.");
        this.registerOption("office:full-tours", "Show the complete welcome tours of Documents applications. Default behaviour: Show a shortened version if another Documents tour has been run before.");
    }
}
