/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { Button, CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";
import type { EditView } from "@/io.ox/office/editframework/view/editview";
import { TEXT_INSERT } from "@/io.ox/office/textframework/utils/operations";

// class DebugActionsPicker ===================================================

export class DebugActionsPicker extends CompoundButton<EditView> {

    constructor(docView: EditView) {

        // base constructor
        super(docView, {
            icon: "bi:wrench",
            label: _.noI18n("Actions"),
            tooltip: _.noI18n("Additional debug actions")
        });

        // debug actions to change UI states
        this.addSection("offline", _.noI18n("Simulate UI states"));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Switch UI to offline mode (simulated - network is still online)"), value: "offline" }));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Switch UI to online mode"), value: "online" }));
        this.addControl("debug/view/busy",        new Button({ label: _.noI18n("Show permanent busy screen"), value: false }));
        this.addControl("debug/view/busy",        new Button({ label: _.noI18n("Show busy screen with Cancel button"), value: true }));

        // realtime debug actions
        this.addSection("rt", _.noI18n("Realtime connection"));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Simulate internal error (UNKNOWN_REALTIME_FAILURE)"), value: "error" }));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Simulate a broken server->client connection (block incoming rt2 msgs)"), value: "blockmsg" }));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Simulate a broken send ACK client"), value: "disablesendack" }));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Simulate a type error thrown & caught in rt2 layer"), value: "typeerror" }));
        this.addControl("debug/realtime/trigger", new Button({ label: _.noI18n("Simulate a session invalid error on the client"), value: "sessioninvalid" }));

        // debug actions for operations
        this.addSection("ops", _.noI18n("Operations"));
        this.addControl("debug/operations/apply",         new Button({ label: _.noI18n("Locally apply operation with invalid name"), value: { name: "_invalid_operation_" } }));
        this.addControl("debug/operations/apply",         new Button({ label: _.noI18n("Locally apply operation with invalid position"), value: { name: TEXT_INSERT, start: [987654321, 0], text: "test" } }));
        this.addControl("debug/realtime/sendop",          new Button({ label: _.noI18n("Send operation with invalid name via RT"), value: { name: "_invalid_operation_" } }));
        this.addControl("debug/realtime/sendop",          new Button({ label: _.noI18n('Send "createError" operation via RT'), value: { name: "createError", start: [987654321, 0] } }));
        this.addControl("debug/realtime/sendop/readonly", new Button({ label: _.noI18n("Send operation as viewer via RT (requires read-only mode)"), value: { name: TEXT_INSERT, start: [0, 0], text: "test" } }));

        if (docView.docApp.isTextApp()) {
            this.addControl("debug/operations/applycharacterattrs", new Button({ label: _.noI18n("Apply character attributes (paragraph or drawing, red)"), value: { bold: true, italic: true, fontSize: 20, color: { type: "rgb", value: "FF0000" } } }));
            this.addControl("debug/operations/applycharacterattrs", new Button({ label: _.noI18n("Apply character attributes (paragraph or drawing, green)"), value: { underline: true, bold: true, italic: true, fontSize: 20, color: { type: "rgb", value: "00FF00" } } }));
        }

        // debug actions for application quit
        this.addSection("quit", _.noI18n("Quit"));
        this.addControl("debug/quit/unsaved",   new Button({ label: _.noI18n("Close document with unsaved changes") }));
        this.addControl("debug/quit/typeerror", new Button({ label: _.noI18n('Close document with script error in "EditApp#beforeQuitHandler"'), value: "beforeQuitHandlerSync" }));
        this.addControl("debug/quit/typeerror", new Button({ label: _.noI18n('Close document with script error in "EditApp#quitHandler"'), value: "quitHandlerSync" }));
        this.addControl("debug/quit/typeerror", new Button({ label: _.noI18n('Close document with script error in "EditApp#quitHandler (async)"'), value: "quitHandlerAsync" }));
        this.addControl("debug/quit/reload",    new Button({ label: _.noI18n("Trigger a document reload in 3 seconds") }));
    }
}
