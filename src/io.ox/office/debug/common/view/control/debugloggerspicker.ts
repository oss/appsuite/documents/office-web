/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import type { IconId } from "@/io.ox/office/tk/dom";
import { LogLevel } from "@/io.ox/office/tk/utils/logger";
import { createCaptionNode, setToolTip } from "@/io.ox/office/tk/forms";
import { RadioGroup } from "@/io.ox/office/tk/controls";

import type { EditView } from "@/io.ox/office/editframework/view/editview";

import { registerLoggerItem } from "@/io.ox/office/debug/common/app/apputils";
import type { DebugReloadOptions } from "@/io.ox/office/debug/common/view/control/debugbasepicker";
import { createCaptionOptions, addReloadIcon, DebugBasePicker } from "@/io.ox/office/debug/common/view/control/debugbasepicker";

import "@/io.ox/office/debug/common/view/control/debugloggerspicker.less";

// types ======================================================================

export interface DebugLoggerOptions extends DebugReloadOptions {

    /**
     * The server-side default log level if the logger has not been configured
     * locally. default value is `LogLevel.WARN`.
     */
    default?: LogLevel;
}

// constants ==================================================================

interface LogLevelSettings {
    logLevel: LogLevel;
    icon: IconId;
    color: string;
    tooltip: string;
}

const LOG_LEVEL_CONFIGS: LogLevelSettings[] = [
    { logLevel: LogLevel.OFF,   icon: "bi:dash-circle",               color: "var(--text-gray)", tooltip: "OFF" },
    { logLevel: LogLevel.ERROR, icon: "bi:x-circle-fill",             color: "red",              tooltip: "ERROR" },
    { logLevel: LogLevel.WARN,  icon: "bi:exclamation-triangle-fill", color: "orange",           tooltip: "WARN" },
    { logLevel: LogLevel.INFO,  icon: "bi:info-circle-fill",          color: "blue",             tooltip: "INFO" },
    { logLevel: LogLevel.DEBUG, icon: "bi:bug-fill",                  color: "brown",            tooltip: "DEBUG" },
    { logLevel: LogLevel.TRACE, icon: "bi:megaphone-fill",            color: "var(--text)",      tooltip: "TRACE" }
];

// class LogLevelGroup ========================================================

class LogLevelGroup extends RadioGroup<LogLevel> {

    constructor(configKey: string, tooltip: string, config?: DebugLoggerOptions) {

        super();

        this.$el.addClass("log-level-group inline-button-group");

        LOG_LEVEL_CONFIGS.forEach(option => {
            this.addOption(option.logLevel, {
                icon: option.icon,
                iconStyle: { color: option.color },
                tooltip: _.noI18n(option.tooltip),
                attributes: { class: `inline level${option.logLevel}` }
            });
        });

        const captionOptions = createCaptionOptions({ label: _.noI18n(configKey), tooltip, ...config, default: undefined });
        const $caption = addReloadIcon(createCaptionNode(captionOptions), captionOptions);
        setToolTip($caption, _.noI18n(captionOptions.tooltip));
        this.$el.append($caption);
    }
}

// class DebugLoggersPicker ===================================================

export class DebugLoggersPicker extends DebugBasePicker {

    constructor(docView: EditView) {

        // base constructor
        super(docView, {
            icon: "bi:bug-fill",
            label: _.noI18n("Loggers"),
            tooltip: _.noI18n("Configure debug loggers")
        });

        // logger options
        this.addSection("options", _.noI18n("Logger settings"));
        this.registerOption("office:simple-log", "Disables formatted tags and badges in the browser console in favour of plain-text output.", { reload: true });

        // tooltit loggers
        this.addSection("tk", _.noI18n("Toolkit loggers"));
        this.registerLogger("office:log-global", "The global unspecific logger.", { default: LogLevel.TRACE });
        this.registerLogger("office:log-focus", "Logger for browser focus change events.");
        this.registerLogger("office:log-control", "Logger for the GUI layout engine (form controls).");
        this.registerLogger("office:log-popup", "Logger for the GUI layout engine (pop-up menus).");
        this.registerLogger("office:log-layout", "Logger for the GUI layout engine (view panes, toolbars, ...).");
        this.registerLogger("office:log-tracking", "Logger for the mouse/touch tracking engine.");
        this.registerLogger("office:log-locale", "Logger for handling of locale data and resources.");
        this.registerLogger("office:log-tabs", "Logger for browser tab handling.");
        this.registerLogger("office:log-a11y", "Logger for A11Y announcements.");

        // application loggers
        this.addSection("app", _.noI18n("Application loggers"));
        this.registerLogger("office:log-rt2", "Shows details for low-level RT2 communication layer.", { reload: true });
        this.registerLogger("office:log-realtime", "Shows details about the application's realtime communication.", { default: LogLevel.TRACE });
        this.registerLogger("office:log-perf", "Shows performance data about long-running processes.", { default: LogLevel.TRACE });
        this.registerLogger("office:log-metrics", "Shows metrics events about portal document actions.");
        this.registerLogger("office:log-ops", "Shows all document operations applied locally.");
        this.registerLogger("office:log-ot", "Show details for operation transformations.");
        this.registerLogger("office:log-edittransfer", "Shows details for transferring edit rights between clients.");
        this.registerLogger("office:log-textutils", "Shows details for editing of text contents in all applications.");
        this.registerLogger("office:log-yell", "Logger for tracking yell show and hide.");
        this.registerLogger("office:log-controller", "Shows details about the application controller.");
        this.registerLogger("office:log-search", 'Shows details about "Search and Replace" actions.');
        this.registerLogger("office:log-tours", 'Logger for the "Welcome tours".');
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a selector control for a console logger that is bound to a
     * specific log level configuration item.
     *
     * @param configKey
     *  The identifier of the configuration item used to control the log level.
     *
     * @param tooltip
     *  The tool tip to be inserted into the selector control.
     *
     * @param [options]
     *  Optional parameters:
     *  - {LogLevel} [options.default=LogLevel.WARN]
     *    The server-side default log level if the logger has not been
     *    configured locally.
     *  - {boolean} [options.reload=false]
     *    Whether changing the log level takes effect only after a browser
     *    reload.
     */
    registerLogger(configKey: string, tooltip: string, options?: DebugLoggerOptions): void {

        // register a new controller item
        const itemKey = registerLoggerItem(this.docApp, configKey, options?.default);

        // create the new logger group control
        const logLevelGroup = new LogLevelGroup(configKey, tooltip);
        this.addControl(itemKey, logLevelGroup, { ...options, sticky: true });
    }
}
