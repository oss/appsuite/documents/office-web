/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { dict } from "@/io.ox/office/tk/algorithms";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

import type { OTTestDefinition, OTTestRunner } from "@/io.ox/office/debug/common/app/ottestrunner";

// private functions ==========================================================

function getTestTitle(testId: string, testDef: OTTestDefinition): string {
    let title = `${testId}: ${testDef.description}`;
    const clients = testDef.clients ?? 1;
    if (clients > 1) { title += ` (${clients} clients)`; }
    if (testDef.autotest) { title += " - autotest"; }
    return _.noI18n(title);
}

// class TestRunPicker ========================================================

/**
 * A combo-box control used to select an OT test.
 */
export class TestRunPicker extends RadioList<string> {

    constructor(testRunner: OTTestRunner) {

        // the collection of all runtime test definitions
        const testDefs = testRunner.getTestDefinitions();

        // base constructor
        super({
            width: "20em",
            tooltip: _.noI18n("OT Tests"),
            highlight: false,
            splitValue: dict.anyKey(testDefs),
            updateSplitValue: true,
            shrinkWidth: "7em"
        });

        // create the menu entries for the test contained in the test file description
        dict.forEach(testDefs, (testDef, testId) => {
            this.addOption(testId, {
                label: getTestTitle(testId, testDef),
                tooltip: _.noI18n(testDef.tooltip || "")
            });
        });
    }
}
