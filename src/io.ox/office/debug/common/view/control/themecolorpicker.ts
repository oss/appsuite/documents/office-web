/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { createSpan, getSchemeColorCount } from "@/io.ox/office/tk/dom";
import { RadioList } from "@/io.ox/office/baseframework/view/basecontrols";

// class ThemeColorPicker =====================================================

export class ThemeColorPicker extends RadioList<string> {

    constructor() {

        // base constructor
        super({
            icon: "bi:palette",
            tooltip: _.noI18n("Show colors of current UI theme"),
            listLayout: "grid",
            gridColumns: 12,
            updateCaptionMode: "none"
        });

        // add marker class for additional CSS formatting
        this.menu.el.classList.add("color-picker");

        this.addSection("main", { label: _.noI18n("Main colors"), gridColumns: 2 });
        this.#addColor("--accent", "Accent");
        this.#addColor("--text", "Text");
        this.#addColor("--background", "Background");
        this.#addColor("--border", "Border");

        this.addSection("text", _.noI18n("Text"));
        this.#addColor("--text");
        this.#addColor("--text-accent");
        this.#addColor("--text-gray");
        this.#addColor("--text-gray-on-gray");
        this.#addColor("--text-disabled");
        this.#addColor("--text-gray-hover");
        this.#addColor("--text-dark");
        this.#addColor("--toolbar");
        this.#addColor("--link");
        this.#addColor("--link-hover");
        this.#addColor("--outline");

        this.addSection("background", _.noI18n("Background shades"));
        this.#addColor("--background-5");
        this.#addColor("--background-10");
        this.#addColor("--background-50");
        this.#addColor("--background-100");
        this.#addColor("--background-200");
        this.#addColor("--background-300");
        this.#addColor("--shade-a5");
        this.#addColor("--shade-a10");
        this.#addColor("--shade-a20");
        this.#addColor("--shade-a30");
        this.#addColor("--shade-a50");

        this.addSection("border", _.noI18n("Border shades"));
        this.#addColor("--border");
        this.#addColor("--border-bright");

        this.addSection("focus", _.noI18n("Focus shades"));
        this.#addColor("--focus");
        this.#addColor("--focus-100");
        this.#addColor("--focus-200");
        this.#addColor("--focus-300");
        this.#addColor("--focus-400");
        this.#addColor("--focus-500");
        this.#addColor("--focus-600");
        this.#addColor("--focus-700");

        this.addSection("primary", _.noI18n("Primary button"));
        this.#addColor("--btn-primary");
        this.#addColor("--btn-primary-background");
        this.#addColor("--btn-primary-background-hover");
        this.#addColor("--btn-primary-outline");

        this.addSection("circular", _.noI18n("Circular button"));
        this.#addColor("--button-circular");
        this.#addColor("--button-circular-background");
        this.#addColor("--button-circular-border");
        this.#addColor("--button-circular-focus");
        this.#addColor("--button-circular-background-focus");
        this.#addColor("--button-circular-border-focus");
        this.#addColor("--button-circular-hover");
        this.#addColor("--button-circular-background-hover");
        this.#addColor("--button-circular-border-hover");

        this.addSection("checkbox", _.noI18n("Checkbox"));
        this.#addColor("--checkbox-border");
        this.#addColor("--checkbox-color");
        this.#addColor("--checkbox-background");

        this.addSection("topbar", _.noI18n("Top bar"));
        this.#addColor("--topbar-background");
        this.#addColor("--topbar-icon");
        this.#addColor("--topbar-hover");
        this.#addColor("--topbar-search-background");
        this.#addColor("--topbar-search-placeholder");

        this.addSection("selection", _.noI18n("List selection"));
        this.#addColor("--selected");
        this.#addColor("--selected-focus");
        this.#addColor("--selected-gray-focus");
        this.#addColor("--selected-background");
        this.#addColor("--selected-background-hover");
        this.#addColor("--selected-background-shade");
        this.#addColor("--selected-background-focus");
        this.#addColor("--selected-background-focus-shade");
        this.#addColor("--selected-background-focus-hover");

        this.addSection("marker", _.noI18n("Marker colors"));
        this.#addColor("--unseen");
        this.#addColor("--today");
        this.#addColor("--missed");
        this.#addColor("--attention");
        this.#addColor("--favorite");
        this.#addColor("--upsell");
        this.#addColor("--encrypted");
        this.#addColor("--low");
        this.#addColor("--legacy-blue");

        this.addSection("app", _.noI18n("Application colors"));
        this.#addColor("--app-color-mail");
        this.#addColor("--app-color-chat");
        this.#addColor("--app-color-calendar");
        this.#addColor("--app-color-contacts");
        this.#addColor("--app-color-tasks");
        this.#addColor("--app-color-files");
        this.#addColor("--app-color-portal");
        this.#addColor("--app-color-text");
        this.#addColor("--app-color-spreadsheet");
        this.#addColor("--app-color-presentation");
        this.#addColor("--app-color-fallback");

        this.addSection("user", _.noI18n("Documents user colors"));
        for (let index = 1, count = getSchemeColorCount(); index <= count; index += 1) {
            this.#addColor(`--scheme-color-${index}`);
        }

        this.addSection("accent", _.noI18n("Accent shades"));
        this.#addColor("--accent-10");
        this.#addColor("--accent-20");
        this.#addColor("--accent-50");
        this.#addColor("--accent-100");
        this.#addColor("--accent-200");
        this.#addColor("--accent-300");
        this.#addColor("--accent-400");
        this.#addColor("--accent-500");
        this.#addColor("--accent-600");
        this.#addColor("--accent-700");
        this.#addColor("--accent-800");
        this.#addColor("--accent-900");

        this.addSection("gray", _.noI18n("Gray shades"));
        this.#addColor("--gray-5");
        this.#addColor("--gray-10");
        this.#addColor("--gray-50");
        this.#addColor("--gray-100");
        this.#addColor("--gray-200");
        this.#addColor("--gray-300");
        this.#addColor("--gray-400");
        this.#addColor("--gray-500");
        this.#addColor("--gray-600");
        this.#addColor("--gray-700");
        this.#addColor("--gray-800");
        this.#addColor("--gray-900");

        this.addSection("indigo", _.noI18n("Indigo shades"));
        this.#addColor("--indigo-20");
        this.#addColor("--indigo-50");
        this.#addColor("--indigo-100");
        this.#addColor("--indigo-200");
        this.#addColor("--indigo-300");
        this.#addColor("--indigo-400");
        this.#addColor("--indigo-500");
        this.#addColor("--indigo-600");
        this.#addColor("--indigo-700");
        this.#addColor("--indigo-800");
        this.#addColor("--indigo-900");

        this.addSection("green", _.noI18n("Green shades"));
        this.#addColor("--green-20");
        this.#addColor("--green-50");
        this.#addColor("--green-100");
        this.#addColor("--green-200");
        this.#addColor("--green-300");
        this.#addColor("--green-400");
        this.#addColor("--green-500");
        this.#addColor("--green-600");
        this.#addColor("--green-700");
        this.#addColor("--green-800");
        this.#addColor("--green-900");

        this.addSection("red", _.noI18n("Red shades"));
        this.#addColor("--red-20");
        this.#addColor("--red-50");
        this.#addColor("--red-100");
        this.#addColor("--red-200");
        this.#addColor("--red-300");
        this.#addColor("--red-400");
        this.#addColor("--red-500");
        this.#addColor("--red-600");
        this.#addColor("--red-700");
        this.#addColor("--red-800");
        this.#addColor("--red-900");
    }

    // private methods --------------------------------------------------------

    #addColor(name: string, label?: string): void {
        const $button = this.addOption(name, { label, tooltip: _.noI18n(name) });
        $button.prepend(createSpan({ classes: "color-box", style: { background: `var(${name})` } }));
    }
}
