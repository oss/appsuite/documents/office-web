/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { str, jpromise } from "@/io.ox/office/tk/algorithms";
import { createIcon } from "@/io.ox/office/tk/dom";
import type { DebugFlagOptions } from "@/io.ox/office/tk/config";

import type { CaptionControlConfig, CheckBoxConfig, CompoundButtonConfig } from "@/io.ox/office/baseframework/view/basecontrols";
import { CheckBox, CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";
import type { ToolBarAddControlOptions } from "@/io.ox/office/baseframework/view/pane/toolbar";

import type { EditView } from "@/io.ox/office/editframework/view/editview";

import { registerDebugFlagItem } from "@/io.ox/office/debug/common/app/apputils";

// types ======================================================================

export interface DebugReloadOptions {

    /**
     * Whether changing the option takes effect only after a browser reload.
     * Default value is `false`.
     */
    reload?: boolean;
}

/**
 * Configuration options for registering a checkbox in a debug dropdown menu.
 */
export interface DebugCheckBoxConfig extends CheckBoxConfig, ToolBarAddControlOptions, DebugFlagOptions, DebugReloadOptions { }

// public functions ===========================================================

/**
 * Adds options lor labels and icons according to whether the passed options
 * contain the "reload" flag.
 */
export function createCaptionOptions<OptT extends CaptionControlConfig<any> & DebugFlagOptions & DebugReloadOptions>(options?: OptT): OptT {

    // create a copy of the options
    options = { ...options } as unknown as OptT;

    // prepare tooltip for reload option
    if (options.reload) {
        options.tooltip = str.concatTokens(options.tooltip, "Needs browser reload!");
    }

    if (options.label) {
        if (options.enable) {
            options.label += `=${options.enable.split("|")[0]}`;
        }
        options.label = _.noI18n(options.label);
    }

    if (options.tooltip) {
        options.tooltip = _.noI18n(options.tooltip);
    }

    return options;
}

/**
 * Adds a "Reaload" icon to the passed caption node.
 */
export function addReloadIcon($caption: JQuery, options?: DebugReloadOptions): JQuery {
    // prepare icon and tooltip for reload option
    if (options?.reload) {
        $caption.append(createIcon("bi:arrow-repeat", "text-danger"));
    }
    return $caption;
}

// class DebugBasePicker ======================================================

/**
 * A compound drop-down control that adds a footer with a "Reload" button
 * reloading the browser tab.
 */
export class DebugBasePicker extends CompoundButton<EditView> {

    // flag whether this menu contains at least one option the needs browser reload
    #hasReloadOption = false;

    // action button that reloads the browser tab
    readonly #$reloadButton: JQuery;

    // constructor ------------------------------------------------------------

    protected constructor(docView: EditView, config?: CompoundButtonConfig) {

        // base constructor
        super(docView, {
            ...config,
            cancelButton: _.noI18n("Close")
        });

        // action button that restores all debug configuration items
        this.addActionButton({
            action: "restore",
            type: "warning",
            icon: "bi:skip-backward-fill",
            label: _.noI18n("Reset all"),
            tooltip: _.noI18n("Reset all debug options and log levels to their defaults."),
            click: () => {
                // change "reload" button to type "danger" when selecting an option requiring a reload
                this.#changeReloadButtonType();
                jpromise.floating(docView.executeControllerItem("debug/restoredefaults"));
            }
        });

        // action button that reloads the browser tab
        this.#$reloadButton = this.addActionButton({
            action: "reload",
            type: "warning",
            icon: "bi:arrow-repeat",
            label: _.noI18n("Reload"),
            tooltip: _.noI18n("Reload the browser tab (required to activate/deactivate some of the options)."),
            click: () => jpromise.floating(docView.executeControllerItem("debug/reload"))
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a checkbox for a specific debug option.
     *
     * @param itemKey
     *  The key of the controller item.
     *
     * @param [options]
     *  Optional parameters.
     */
    createCheckBox(itemKey: string, options?: DebugCheckBoxConfig): void {

        // create the checkbox
        const checkBox = new CheckBox(createCaptionOptions(options));
        addReloadIcon(checkBox.$caption, options);
        this.addControl(itemKey, checkBox, { ...options, sticky: true });

        // change "reload" button to type "danger" when selecting an option needing reload
        if (options?.reload) {
            this.#hasReloadOption = true;
            this.listenTo(checkBox, "group:commit", this.#changeReloadButtonType);
        }
    }

    /**
     * Creates a checkbox for a specific debug option.
     *
     * @param configKey
     *  The identifier of the debug option.
     *
     * @param tooltip
     *  The tool tip to be inserted into the checkbox.
     *
     * @param [options]
     *  Optional parameters.
     */
    registerOption(configKey: string, tooltip: string, options?: DebugCheckBoxConfig): void {

        // register a new controller item
        const itemKey = registerDebugFlagItem(this.docApp, configKey, options);

        // create the new checkbox
        this.createCheckBox(itemKey, { label: configKey, tooltip, ...options });
    }

    // private methods --------------------------------------------------------

    #changeReloadButtonType(): void {
        if (this.#hasReloadOption) {
            this.#$reloadButton.removeClass("btn-warning").addClass("btn-danger");
        }
    }
}
