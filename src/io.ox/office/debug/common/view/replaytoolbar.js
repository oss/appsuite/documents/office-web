/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, to } from '@/io.ox/office/tk/algorithms';
import { Button, TextField, SpinField } from '@/io.ox/office/tk/controls';
import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';

// class ReplayToolBar ====================================================

export class ReplayToolBar extends ToolBar {

    constructor(docView, debugContext) {

        // base constructor ---------------------------------------------------
        super(docView);

        // the application instance
        var docApp = docView.docApp;

        // the document model instance
        var docModel = docView.docModel;

        // the asynchronous worker that replays the recorded operations
        var replayWorker = debugContext.replayWorker;

        // the UI test runner (needed to disable manual replay during tests)
        var testRunner = debugContext.testRunner;

        // button to start/stop recording operations
        var recordButton = new Button({ icon: 'bi:record-fill', iconStyle: { color: "red" }, tooltip: _.noI18n('Start/stop recording operations'), toggle: true });

        // button to start/stop replaying operations
        var replayButton = new Button({ icon: 'bi:play-fill', iconStyle: { color: "green" }, tooltip: _.noI18n('Start/stop replaying operations'), toggle: true });

        // operations to replay (as JSON string)
        var opsTextField = new TextField({ placeholder: _.noI18n('Operations'), tooltip: _.noI18n('Paste the JSON operations array here.'), width: "10em", select: true, clearButton: true, shrinkWidth: "5em" });

        // maximum OSN for debug operations for replay
        var osnSpinField = new SpinField({ icon: 'bi:skip-end-fill', tooltip: _.noI18n('The OSN of the last operation to be applied. Leave empty or set to 0 to disable limiting per OSN.'), width: "6.5em", shrinkWidth: "5.5em", min: 0 });

        // delay between every replayed operation
        var delaySpinField = new SpinField({ icon: 'bi:clock', tooltip: _.noI18n('The delay in milliseconds between applied operations. Leave empty or set to 0 for fast mode.'), width: "6.5em", shrinkWidth: "5.5em", min: 0, max: 3000, smallStep: 10, largeStep: 100 });

        // whether operations are currently recorded
        var isRecording = false;

        // metadata object for replay worker
        var replayData = { ops: debugContext.rescueOperations || [], osn: 0, delay: 0, expand: true };

        // private methods ----------------------------------------------------

        function operationsEventHandler(operations, external) {

            // bug 29601: count-down for operations optimized away but still existing in the array
            var oplCountDown = 1;

            // process all new operations
            for (const operation of operations) {

                // bug 29601: check if this operation was merged by the 'optimizeOperationHandler'
                // (can only happen for internal operations)
                if (!external) {
                    var opl = to.number(operation.opl, 0);
                    if (opl > 1) {
                        oplCountDown = opl;
                    } else if (oplCountDown > 1) {
                        oplCountDown -= 1;
                        continue;
                    }
                }

                replayData.ops.push(operation);
            }
        }

        // initialization -----------------------------------------------------

        // controller item to start/stop recording all applied operations
        docApp.docController.registerItem('debug/replay/record', {
            parent: 'app/imported/success',
            enable() { return !replayWorker.running; },
            get() { return isRecording; },
            set(state) {
                if (isRecording !== state) {
                    isRecording = state;
                    if (isRecording) {
                        docView.listenTo(docModel, 'operations:after', operationsEventHandler);
                    } else {
                        docView.stopListeningTo(docModel, 'operations:after', operationsEventHandler);
                    }
                }
            }
        });

        // parent item that is enabled if operations can be replayed
        docApp.docController.registerItem('debug/replay/enabled', {
            parent: 'app/imported/success',
            enable() { return !isRecording && !testRunner.isTestRunning(); }
        });

        // controller item to start/stop replaying all recorded operations
        docApp.docController.registerItem('debug/replay/start', {
            parent: 'debug/replay/enabled',
            enable() {
                return ((replayData.ops.length > 0) && (docApp.isEditable() || docApp.isRescueDocMode()));
            },
            get() { return replayWorker.running; },
            set(state) {
                return state ? replayWorker.start(replayData) : replayWorker.abort();
            },
            async: 'background'
        });

        // controller item for the operations text field
        docApp.docController.registerItem('debug/replay/ops', {
            parent: 'debug/replay/enabled',
            enable() { return !replayWorker.running; },
            get() { return replayData.ops.length ? JSON.stringify(replayData.ops) : ''; },
            set(text) {
                try {
                    var jsonData = text ? JSON.parse(text) : null;
                    replayData.ops = to.array(is.dict(jsonData) ? jsonData.operations : jsonData, true);
                } catch {
                    replayData.ops.length = 0;
                }
            }
        });

        // controller item for the "maximum OSN" spin field
        docApp.docController.registerItem('debug/replay/osn', {
            parent: 'debug/replay/enabled',
            get() { return replayData.osn; },
            set(value) { replayData.osn = value; }
        });

        // controller item for the "delay" spin field
        docApp.docController.registerItem('debug/replay/delay', {
            parent: 'debug/replay/enabled',
            get() { return replayData.delay; },
            set(value) { replayData.delay = value; }
        });

        // add the controls to the toolbar
        this.addControl('debug/replay/record', recordButton);
        this.addControl('debug/replay/start', replayButton);
        this.addControl('debug/replay/ops', opsTextField);
        this.addControl('debug/replay/osn', osnSpinField);
        this.addControl('debug/replay/delay', delaySpinField);

        // collect rescue operations triggered after document has been imported
        docApp.on('docs:rescueoperations', rescueOperations => {
            replayData.ops = rescueOperations;
            docApp.docController.update();
        });
    }
}
