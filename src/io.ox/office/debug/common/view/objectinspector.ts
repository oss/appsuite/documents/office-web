/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str, itr, ary, map, json, debug } from "@/io.ox/office/tk/algorithms";
import type { NodeOrJQuery, IconId } from "@/io.ox/office/tk/dom";
import { replaceChildren, createText, createElement, createSpan, createAnchor, createFragment, createIcon, createButton } from "@/io.ox/office/tk/dom";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { ToolTip } from "@/io.ox/office/tk/popup/tooltip";

import type { OpColor } from "@/io.ox/office/editframework/utils/color";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import "@/io.ox/office/debug/common/view/objectinspector.less";

// types ======================================================================

/**
 * Options for creating an anchor element for an `ObjectInspector` instance.
 */
export interface InspectorAnchorOptions {

    /**
     * A plain-text label to be inserted into the anchor node. If omitted, the
     * inspected value itself will be stringified.
     */
    label?: string;

    /**
     * If set to `true`, the "expand" icon will not be appended to the anchor
     * label.
     */
    hideIcon?: boolean;
}

export interface InspectorContextEntry<VT = unknown> {

    /**
     * The name of the property, or the index of an array element.
     */
    name: string | number;

    /**
     * The value of the property or array element.
     */
    value: VT;
}

/**
 * A predicate callback function to select properties to be formatted with a
 * formatter callback function (see `InspectorFormatterFn`).
 *
 * @param context
 *  The property context (path of properties) of the property to be mached.
 *
 * @returns
 *  Whether the property matches (will be formatted with a formatter callback
 *  function).
 */
export type InspectorMatcherFn = (this: ObjectInspector, context: InspectorContext) => boolean;

/**
 * A callback function to create custom HTML for a JSON value.
 *
 * @param context
 *  The property context (path of properties) of the property to be mached.
 *
 * @param simple
 *  A document fragment with simple text shown in the table cell (usually a few
 *  `<span>` elements). Initially, will contain a single `<span>` element with
 *  a default representation of the JSON value.
 *
 * @param complex
 *  A document fragment with additional more complex contents that can be
 *  collapsed. Initially, will be empty for atomic values, but will contain a
 *  `<table>` element for JSON dictionaries and JSON arrays.
 *
 * @returns
 *  May return the type identifier of the value, which will be used instead of
 *  the standard "Dict" or "Array" badge.
 */
export type InspectorFormatterFn = (this: ObjectInspector, context: InspectorContext, simple: DocumentFragment, complex: DocumentFragment) => string | void;

/**
 * Contents to be inserted into a table cell shown in the object inspector.
 * Strings will be converted to `<span>` elements, `null` will create an empty
 * table cell.
 */
export type InspectorTableCellArg = null | string | Node | Array<null | string | Node>;

/**
 * Array of cell contents to be inserted into a table row shown in the object
 * inspector. Strings will be converted to `<span>` elements.
 */
export type InspectorTableRowArg = InspectorTableCellArg[];

// private functions ==========================================================

function getSortedKeys(value: object): string[] {
    return LOCALE_DATA.getCollator({ numeric: true }).sort(Object.keys(value));
}

function isComplex(value: unknown): value is object {
    return is.dict(value) || is.object(value);
}

function colorAttrMatcher(context: InspectorContext): boolean {
    return context.isDictEntry(0, /color/i) && context.isDictEntry(1);
}

function borderAttrMatcher(context: InspectorContext): boolean {
    return context.isDictEntry(0, /border/i) && context.isDictEntry(1);
}

function getCollapseIconId(collapse?: boolean): IconId {
    return collapse ? "bi:chevron-right" : "bi:chevron-down";
}

function createTableRow(tagName: "th" | "td", cellArgs: InspectorTableRowArg): HTMLTableRowElement {
    const rowNode = createElement("tr");
    for (const cellArg of cellArgs) {
        const cellNode = rowNode.appendChild(createElement(tagName));
        for (const entry of ary.wrap(cellArg)) {
            if (entry) {
                cellNode.appendChild(is.string(entry) ? createSpan({ label: entry }) : entry);
            }
        }
    }
    return rowNode;
}

/**
 * Renders the passed value into a document fragment.
 *
 * @param fragment
 *  The document fragment to be rendered into.
 *
 * @param value
 *  The JSON value to be rendered. Numbers will be rendered with violet text
 *  color. Strings will be rendered with blue text color. Boolean values will
 *  be rendered with green or red text color according to the value.
 *
 * @param [short]
 *  If set to `true`, dictionaries and arrays will be rendered with simple
 *  placeholder text `{...}` resp. `[...]`, instead with the property values or
 *  array element values.
 */
function renderValue(fragment: DocumentFragment, value: unknown, short?: boolean): DocumentFragment {
    if (is.number(value)) {
        fragment.appendChild(createSpan({ classes: "t-num", label: String(value) }));
    } else if (is.string(value)) {
        const text = (short && value.length > 20) ? `${value.slice(0, 20)}${str.ELLIPSIS_CHAR}` : value;
        const tooltip = (value !== text) ? value : null;
        fragment.appendChild(createSpan({ classes: "t-str", label: json.safeStringify(text).replace(/ /g, "\xa0"), tooltip }));
    } else if (is.boolean(value)) {
        fragment.appendChild(createSpan({ classes: `t-${value}`, label: String(value) }));
    } else if (value === null) {
        fragment.appendChild(createSpan({ classes: "t-null", label: "null" }));
    } else if (is.dict(value)) {
        const keys = getSortedKeys(value);
        const count = keys.length;
        fragment.appendChild(createSpan({ classes: "t-id", label: `Dict[${count}]` }));
        if (!short) {
            fragment.appendChild(createText(" { "));
            for (let i = 0, l = Math.min(3, count); i < l; i += 1) {
                const key = keys[i];
                const text = `${(i > 0) ? ", " : ""}${key}: `;
                fragment.appendChild(createText(text));
                renderValue(fragment, value[key], true);
            }
            fragment.appendChild(createText(`${(count > 3) ? `, ${str.ELLIPSIS_CHAR}` : ""} }`));
        }
    } else if (is.array(value)) {
        const count = value.length;
        fragment.appendChild(createSpan({ classes: "t-id", label: `Array[${count}]` }));
        if (!short) {
            fragment.appendChild(createText(" [ "));
            for (let i = 0, l = Math.min(3, count); i < l; i += 1) {
                if (i > 0) { fragment.appendChild(createText(", ")); }
                renderValue(fragment, value[i], true);
            }
            fragment.appendChild(createText(`${(count > 3) ? `, ${str.ELLIPSIS_CHAR}` : ""} ]`));
        }
    } else {
        fragment.appendChild(createSpan({ classes: "t-id", label: "unknown" }));
        fragment.appendChild(createSpan({ classes: "t-unknown", label: debug.stringify(value) }));
    }
    return fragment;
}

// class InspectorContext =====================================================

/**
 * The specification of a property to be shown in the object inspector.
 */
export class InspectorContext {

    readonly guard = new Set<unknown>();

    readonly chain: Array<Opt<InspectorContextEntry>> = [];

    // public methods ---------------------------------------------------------

    isNamedEntry(index: number, key?: string | RegExp, matcher?: (value: unknown) => boolean): boolean {
        const entry = this.chain[index];
        return !!entry && is.string(entry.name) && (is.string(key) ? (entry.name === key) : key ? key.test(entry.name) : true) && (!matcher || matcher(entry.value));
    }

    isNumberEntry(index: number, key?: string | RegExp): boolean {
        return this.isNamedEntry(index, key, is.number);
    }

    isStringEntry(index: number, key?: string | RegExp): boolean {
        return this.isNamedEntry(index, key, is.string);
    }

    isBooleanEntry(index: number, key?: string | RegExp): boolean {
        return this.isNamedEntry(index, key, is.boolean);
    }

    isDictEntry(index: number, key?: string | RegExp, matcher?: (value: Dict) => boolean): boolean {
        return this.isNamedEntry(index, key, is.dict) && (!matcher || matcher(this.chain[index]!.value as Dict));
    }

    isArrayEntry(index: number, key?: string, matcher?: (value: unknown[]) => boolean): boolean {
        return this.isNamedEntry(index, key, is.array) && (!matcher || matcher(this.chain[index]!.value as unknown[]));
    }
}

// class ObjectInspector ======================================================

/**
 * A popup menu for inspecting a JSON data value.
 */
export class ObjectInspector extends ToolTip {

    // properties -------------------------------------------------------------

    /** Container for the header label with the entire value. */
    readonly #headerLabel = createSpan("flex-grow");

    /** Registry for formatters. */
    readonly #formatters = new Map<string, InspectorFormatterFn>();

    /** Registry for formatter matchers. */
    readonly #formatMatchers = new Map<string, InspectorMatcherFn[]>();

    /** Registry for collapsed properties. */
    readonly #collapseMatchers: InspectorMatcherFn[] = [];

    // constructor ------------------------------------------------------------

    constructor(rootNode: NodeOrJQuery) {

        super({
            classes: "object-inspector",
            anchorBox: rootNode,
            anchorBorder: ["top", "bottom"],
            textSelect: true
        });

        // initialize the header element
        this.$header.append(this.#headerLabel, createButton({
            type: "bare",
            action: "close",
            icon: "bi:x-lg",
            click: () => { this.hide(); }
        }));

        // register a click handler for all collapse buttons
        this.$body.on("click", '[data-action="collapse"]', event => {
            const buttonNode = event.currentTarget as HTMLButtonElement;
            const collapsed = !buttonNode.classList.contains("i-collapse");
            buttonNode.classList.toggle("i-collapse", collapsed);
            replaceChildren(buttonNode, createIcon(getCollapseIconId(collapsed)));
            this.refresh({ immediate: true });
            return false;
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates an anchor node to be bound to an object inspector menu.
     *
     * @param value
     *  The JSON dictionary or array to be shown in the inspector menu.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The anchor node carrying the passed JSON data.
     */
    createAnchor(value: unknown, options?: InspectorAnchorOptions): HTMLAnchorElement {
        const label = options?.label || debug.stringify(value);
        const anchorNode = createAnchor("#", { classes: "object-inspector-anchor", label });
        if (!options?.hideIcon) {
            anchorNode.appendChild(createIcon("bi:box-arrow-up-right"));
        }
        anchorNode.addEventListener("click", event => {
            this.#renderValue(anchorNode, value);
            event.stopPropagation();
            event.preventDefault();
        });
        return anchorNode;
    }

    /**
     * Registers a formatter callback function.
     *
     * @param formatterKey
     *  A unique key that will be used later for registering matchers.
     *
     * @param formatterFn
     *  The callback function that generates DOM content for a property.
     */
    registerFormatter(formatterKey: string, formatterFn: InspectorFormatterFn): void {
        this.#formatters.set(formatterKey, formatterFn);
    }

    /**
     * Registers a formatter callback function for specific data types and/or
     * property paths.
     *
     * @param formatterKey
     *  The key of the formatter callback function.
     *
     * @param matcherFns
     *  The predicate callback functions to select properties to be formatted.
     */
    addFormatMatcher(formatterKey: string, ...matcherFns: InspectorMatcherFn[]): void {
        map.upsert(this.#formatMatchers, formatterKey, () => []).push(...matcherFns);
    }

    /**
     * Registers specific data types and/or property paths to be initially
     * collapsed when showing the popup menu.
     *
     * @param matcherFns
     *  The predicate callback functions to select properties to be collapsed.
     */
    addCollapseMatcher(...matcherFns: InspectorMatcherFn[]): void {
        this.#collapseMatchers.push(...matcherFns);
    }

    /**
     * Creates a `<table>` element, and creates and fills the cells with the
     * passed contents.
     *
     * @param headArgs
     *  The cell contents for a table header row (creates `<th>` elements in a
     *  `<thead>` element). If set to `null`, no header row will be created.
     *
     * @param dataArgs
     *  The cell contents for all table data rows (creates `<td>` elements in a
     *  `<tbody>` element).
     */
    createTable(headArgs: InspectorTableRowArg | null, dataArgs: InspectorTableRowArg[]): HTMLTableElement {

        const tableNode = createElement("table");

        if (headArgs) {
            const headNode = tableNode.appendChild(createElement("thead"));
            headNode.appendChild(createTableRow("th", headArgs));
        }

        const bodyNode = tableNode.appendChild(createElement("tbody"));
        for (const rowArgs of dataArgs) {
            bodyNode.appendChild(createTableRow("td", rowArgs));
        }

        return tableNode;
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a `<table>` element containing the passed JSON dictionary or
     * array.
     */
    #createTableForJSON(context: InspectorContext, value: object): HTMLTableElement {

        // protection against recursive data structures
        context.guard.add(value);

        // create array of entries for all object property or array elements
        const entries: InspectorContextEntry[] = is.array(value) ?
            value.map((v, i) => ({ name: i, value: v })) :
            getSortedKeys(value).map(k => ({ name: k, value: (value as Dict)[k] }));

        // create a `<table>` element
        const tableCells: InspectorTableRowArg[] = [];

        // create a table row for each property/element
        for (const entry of entries) {

            // add the current entry to the context property chain (in front for easy access in tester functions)
            context.chain.unshift(entry);

            // the simple/complex fragments to be passed to formatter callbacks
            const simpleFragment = renderValue(createFragment(), entry.value);
            const complexFragment = createFragment();

            // create a `<table>` element for dictionaries and arrays
            if (isComplex(entry.value)) {
                if (context.guard.has(entry.value)) {
                    simpleFragment.append(createSpan({ label: " " }), createSpan({ classes: "t-id t-false", label: "recursive" }));
                } else {
                    complexFragment.appendChild(this.#createTableForJSON(context, entry.value));
                }
            }

            // invoke custom formatter
            const formatterKey = itr.mapFirst(this.#formatMatchers, ([key, fns]) => fns.some(fn => fn.call(this, context)) ? key : undefined);
            const typeId = formatterKey ? this.#formatters.get(formatterKey)?.call(this, context, simpleFragment, complexFragment) : undefined;

            // replace type identifier in simple fragment
            if (typeId) {
                simpleFragment.querySelector("span.t-id")?.remove();
                simpleFragment.prepend(createSpan({ classes: "t-id", label: typeId }));
            }

            const hasComplex = complexFragment.hasChildNodes();
            const collapse = hasComplex && this.#collapseMatchers.some(matcherFn => matcherFn.call(this, context));
            const buttonNode = hasComplex ? createButton({ type: "bare", action: "collapse", classes: collapse ? "i-collapse" : "", icon: getCollapseIconId(collapse) }) : null;
            tableCells.push([String(entry.name), [buttonNode, simpleFragment, complexFragment]]);

            // remove the current entry from the context property chain
            context.chain.shift();
        }

        return this.createTable(null, tableCells);
    }

    /**
     * Renders the contents of this tooltip from the settings of the clicked
     * DOM element.
     */
    #renderValue(anchorNode: HTMLAnchorElement, value: unknown): void {

        // generate the header label
        replaceChildren(this.#headerLabel, renderValue(createFragment(), value));

        // generate and insert the HTML <table> element
        this.$body.empty();
        if (isComplex(value)) {
            this.$body.append(this.#createTableForJSON(new InspectorContext(), value));
        }

        // register target node as current anchor node, and show the menu
        this.setAnchor(anchorNode);
        this.show();
    }
}

// class ObjectInspector ======================================================

/**
 * A popup menu for inspecting a JSON data value with document access.
 */
export class ModelObjectInspector extends ObjectInspector {

    // properties -------------------------------------------------------------

    /** The document view containing this inspector instance. */
    docView: EditView;

    /** The document model contained by the view. */
    docModel: EditModel;

    // constructor ------------------------------------------------------------

    constructor(docView: EditView, rootNode: NodeOrJQuery) {

        super(rootNode);

        // properties
        this.docView = docView;
        this.docModel = docView.docModel;

        // register a formatter for color attributes (add a color box)
        this.registerFormatter("color", (context, simple) => {
            const color = context.chain[0]!.value as Dict;
            const type = context.isNamedEntry(1, "character") ? "text" : "fill";
            simple.prepend(this.createColorBox(color, type));
            return "Color";
        });
        this.addFormatMatcher("color", colorAttrMatcher);

        // register a formatter for border attributes (add a color box)
        this.registerFormatter("border", (context, simple) => {
            const border = context.chain[0]!.value as Dict;
            const color = (border.style === "none") ? null : border.color;
            simple.prepend(this.createColorBox(color, "line"));
            return "Border";
        });
        this.addFormatMatcher("border", borderAttrMatcher);

        // initially collapse document positions in operations
        this.addCollapseMatcher(context => context.isArrayEntry(0, undefined, value => value.every(is.number)));

        // initially collapse JSON colors, and JSON borders
        this.addCollapseMatcher(colorAttrMatcher, borderAttrMatcher);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a `<span>` element for a JSON color value.
     *
     * @param value
     *  The JSON color value, or a CSS color string, to be formatted.
     *
     * @param type
     *  The context type for the automatic color.
     *
     * @returns
     *  The `<span>` element for the passed color value.
     */
    createColorBox(value: unknown, type: string): HTMLSpanElement {
        const color = is.string(value) ? value : is.dict(value) ? this.docModel.getCssColor(value as unknown as OpColor, type) : null;
        const spanNode = createSpan({ classes: "t-color", label: color ? "\xa0" : null, tooltip: color || "(no color)" });
        if (color) { spanNode.style.backgroundColor = color; } else { spanNode.appendChild(createIcon("bi:x")); }
        return spanNode;
    }
}
