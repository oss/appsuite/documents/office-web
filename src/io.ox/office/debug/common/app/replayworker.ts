/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { breakLoop, AsyncWorker } from "@/io.ox/office/tk/workers";

import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import { handleMinifiedObject } from "@/io.ox/office/editframework/utils/operationutils";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// types ======================================================================

export interface ReplayWorkerData {
    ops: Operation[];
    osn?: number;
    delay?: number;
    expand?: boolean;
}

// class ReplayWorker =========================================================

export class ReplayWorker extends AsyncWorker<ReplayWorkerData> {

    readonly #docApp: EditApplication;
    readonly #docModel: EditModel;

    #operations!: Operation[];
    #osn!: number;
    #expand!: boolean;

    // constructor ------------------------------------------------------------

    constructor(docModel: EditModel) {
        super();

        this.#docApp = docModel.docApp;
        this.#docModel = docModel;

        // define the iteration loop step that processes the operations
        this.steps.addStep(worker => worker.iterate(
            this.#operations,
            operation => this.#applyOperation(operation)
        ));

        // no merging of operation in optimization handler
        this.on("worker:start", () => this.#docApp.useOperationOptimization(false));
        this.on("worker:finish", () => this.#docApp.useOperationOptimization(true));
    }

    // public methods ---------------------------------------------------------

    override start(data: ReplayWorkerData): Promise<void> {
        this.#operations = data.ops;
        this.#osn = data.osn ?? 0;
        this.#expand = !!data.expand;
        this.configure(data.delay ? { slice: 0, interval: data.delay } : undefined);
        return super.start(data);
    }

    // private methods --------------------------------------------------------

    #applyOperation(operation: Operation): JPromise {
        // DOCS-892: expand operations before applying them
        if (this.#expand) {
            handleMinifiedObject(operation, "", true);
        }
        // exit loop early if maximum OSN has been reached
        if (this.#osn && operation.osn && (this.#osn < operation.osn)) {
            breakLoop();
        }
        // apply operation, fail on error
        return this.#docModel.applyOperationsAsync(operation);
    }
}
