/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import { locationHash } from '$/url';

import { BREAK } from '@/io.ox/office/tk/utils';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { BaseObject } from '@/io.ox/office/tk/objects';
import { NOOP } from '@/io.ox/office/editframework/utils/operations';
import { otLogger } from '@/io.ox/office/editframework/utils/otutils';
import { PARAGRAPH_NODE_SELECTOR } from '@/io.ox/office/textframework/utils/dom';
import { CommandHandler } from '@/io.ox/office/debug/common/app/commandhandler';

// constants ==================================================================

/**
 * The default delay before starting the next test cycle if the test will
 * be executed repeatedly.
 */
const DEFAULT_REPEAT_DELAY = 2000;

/**
 * The idle time without operation handling.
 */
const IDLE_TME = 30000;

/**
 * The name of the master client.
 */
const MASTER_CLIENT_NAME = getClientName(1);

/**
 * The default name of a joined client.
 */
const DEFAULT_JOINED_CLIENT_NAME = getClientName(2);

/**
 * The orders for activating OT Logging in verbose mode in the preparation phase.
 * -> this causes trouble when debug console is open while the test is running.
 */
const PREPARE_OT_LOGGING = {
    orders: [
        { activatetoolbar: 'review' },
        { timeout: 200 },
        { buttonclick: { selector: '.main-pane .group[data-key="document/onlinespelling"] > a.button[data-checked="true"]' } }, // disabling spellchecking, only click button, if data-value is true!
        { timeout: 500 }
    ]
};

// private functions ==========================================================

/**
 * Returns the full name for the specified test client identifier.
 *
 * @param {number} clientId
 *  The identofier of a test client.
 *
 * @returns {string}
 *  The full name for the specified test client identifier.
 */
function getClientName(clientId) {
    return 'CLIENT_' + clientId;
}

// class OTTestRunner =========================================================

/**
 * An instance of this class represents the test runner for the Operational
 * Transformation (OT).
 */
export class OTTestRunner extends BaseObject {

    // the document model
    #docModel;
    // definitions of all tests
    #testDefinitions = {};
    // whether a test is currently running
    #isTestRunning = false;
    // the number of detected OT collisions
    #otCollisions = 0;
    // the checksum of the local document
    #localChecksum = null;
    // whether this client is the master client for the test
    #isMasterClient = false;
    // the number of joined clients for this test
    #joinedClients = 0;
    // a collector for the checksums of all clients
    #allChecksums = [];
    // a collector for all finished clients
    #allFinishedClients = [];
    // a collector for the client IDs
    #allClientIDs = {};
    // whether a client received the result request from the master client
    #receivedResultRequest = false;
    // the id that remote clients can use to join to this test
    #clientRegistrationId = null;
    // the user ID that a joined client received from the master for the current test
    #localTestUserId = null;
    // the user name that a joined client received from the master for the current test
    #localTestUserName = null;
    // the command handler
    #commandHandler = null;
    // the collector for the assertion errors
    #assertionCollector = [];

    constructor(docApp, replayWorker) {

        // base constructor
        super();

        this.#docModel = docApp.docModel;
        this.#commandHandler = this.member(new CommandHandler(docApp, replayWorker));

        // initialization -----------------------------------------------------

        // register a handler for the 'operations:after' event
        this.listenTo(this.#docModel, 'operations:after', (operations, external) => {

            if (!external) { return; }

            operations.forEach(op => {

                // get special test data from "noOp" operation
                const testData = (op.name === NOOP) ? op.testData : null;
                if (!_.isObject(testData)) { return; }

                // listening to registration and results of joined clients
                if (this.#isMasterClient) {
                    if (this.#clientRegistrationId === testData.joinedTestId) {
                        this.#registerOneJoinedClient(testData);
                    }
                    if (testData.clientChecksum) {
                        this.#registerClientResult(testData.clientChecksum);
                    }
                    if (testData.finishedTest) {
                        this.#registerFinishedClient(testData.clientName);
                    }
                }

                // listening to a starting test and to the assigend client user id
                if (!this.#isMasterClient) {
                    if (testData.startTestId) {
                        this.#join(testData.startTestId);
                    }
                    if (testData.assignedIds) {
                        this.#assignUserIdToJoinedClient(testData.assignedIds);
                    }
                    if (testData.sendResult) {
                        this.#receivedResultRequest = true;
                    }
                }
            });
        });

        // count runtime OT collisions notified by a document model event
        this.listenTo(this.#docModel, 'ot:collision', () => { this.#otCollisions += 1; });

        // if a reload happens inside a test, the client might need to proceed with the test
        this.listenTo(docApp, 'revealApp', () => {

            // checking whether a test need to proceed after a reload
            const testOptions = docApp.getLaunchOption('testRunnerSettings');

            if (_.isObject(testOptions)) {
                // trying to proceed the test
                // globalLogger.info(' Trying to proceed the test: ', testOptions);
                this.#sendReloadOperationAfter(testOptions); // informing other clients, that reload is done
                // restore global variables
                this.#restoreGlobalVariables(testOptions);
                // reset run parameters
                const testId = testOptions.reloadContainer.testId;
                const testDefinition = testOptions.reloadContainer.testDefinition;
                const clientName = testOptions.reloadContainer.clientName;
                const counter = testOptions.reloadContainer.counter;
                const commands = testOptions.commands;

                // proceeding the test commands
                this.#proceedTest(testId, clientName, testDefinition, counter, commands);
                return;
            }

            // check the URL for a test definition
            const urlTestId = locationHash('office:run-test');

            if (urlTestId) {
                this.start(urlTestId).catch(err => {
                    globalLogger.exception(err, 'could not start test "' + urlTestId + '" specified in the URL');
                });
            }
        });
    }

    // public methods -----------------------------------------------------

    /**
     * Adds new test definitions to the internal map.
     *
     * @param {Dict<object>} definitions
     *  The collection of test definitions to be registered.
     */
    registerDefinitions(definitions) {
        _.extend(this.#testDefinitions, definitions);
    }

    /**
     * Returns the collection of all test definitions, mapped by the unique
     * test identifiers.
     *
     * @returns {Dict<object>}
     *  The collection of all test definitions.
     */
    getTestDefinitions() {
        return this.#testDefinitions;
    }

    /**
     * Returns whether a test is currently running.
     *
     * @returns {boolean}
     *  Whether a test is currently running.
     */
    isTestRunning() {
        return this.#isTestRunning;
    }

    /**
     * Running the test with the specified ID.
     *
     * @param {String} testId
     *  The ID of the test to be executed.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved or rejected after the test has finished.
     *  The return value in the promise is the number of errors. If this is 0, the
     *  promise will be resolved, otherwise it will be rejected.
     */
    start(testId) {
        return this.#startTestRun(testId, true);
    }

    /**
     * Stops the test currently running.
     */
    stop() {
        otLogger.warn('$badge{TestRunner} method "stop" not implemented');
    }

    // private methods ----------------------------------------------------

    // drawing a line of stars
    #infoLine() {
        otLogger.info('****************************************************');
    }

    // helper function to empty the complete document
    #emptyDocument() {
        this.#docModel.getSelection().selectAll();
        return this.#docModel.deleteSelected();
    }

    // helper function to get the checksum of the local document
    #getChecksum() {
        return this.#docModel.getDocumentChecksum();
    }

    // helper function to reset all global variables after a test
    #resetGlobalSettingsAfterTest() {
        this.#joinedClients = 0;
        this.#isTestRunning = false;
        this.#isMasterClient = false;
        this.#receivedResultRequest = false;
        this.#clientRegistrationId = null;
        this.#localTestUserId = null;
        this.#localTestUserName = null;
        this.#allChecksums = [];
        this.#allFinishedClients = [];
        this.#assertionCollector = [];
    }

    // saving the global variables before reloading the document
    #getGlobalVariables() {
        return {
            isMasterClient: this.#isMasterClient,
            receivedResultRequest: this.#receivedResultRequest,
            clientRegistrationId: this.#clientRegistrationId,
            localTestUserId: this.#localTestUserId,
            localTestUserName: this.#localTestUserName,
            allChecksums: this.#allChecksums,
            allFinishedClients: this.#allFinishedClients,
            allClientIDs: this.#allClientIDs,
            assertionCollector: this.#assertionCollector
        };
    }

    // restoring the global variables after reloading the document
    #restoreGlobalVariables(obj) {
        this.#isMasterClient = obj.isMasterClient;
        this.#receivedResultRequest = obj.receivedResultRequest;
        this.#clientRegistrationId = obj.clientRegistrationId;
        this.#localTestUserId = obj.localTestUserId;
        this.#localTestUserName = obj.localTestUserName;
        this.#allChecksums = obj.allChecksums;
        this.#allFinishedClients = obj.allFinishedClients;
        this.#allClientIDs = obj.allClientIDs;
        this.#assertionCollector = obj.assertionCollector;
    }

    #getClientId() {
        return this.#docModel.getApp().getClientId();
    }

    // check idle time after receiving an external operation
    #createIdlePromise() {

        const idlePromise = this.createDeferred();

        const idleTimeChecker = this.debounce(() => { idlePromise.resolve(); }, { delay: IDLE_TME });

        this.listenTo(this.#docModel, 'operations:after', operations => {
            if (operations.length === 1 && operations[0].name === NOOP) { return; } // not counting noops (result operations should not reset the timer)
            idleTimeChecker();
        }, { while: idlePromise });

        idleTimeChecker(); // starting process

        return idlePromise;
    }

    #sendTestData(testData) {
        const operation = { name: NOOP, testData: _.extend({ clientId: this.#getClientId() }, testData) };
        return this.#commandHandler.runCommands({ orders: [{ operations: [operation] }] });
    }

    // helper function triggered by the master client to send to all listening clients the info about the test, that will be started
    #sendTestStart(testId, testDefinition) {
        const isMultiClientTest = testDefinition && testDefinition.clients && testDefinition.clients > 1;
        if (!this.#isMasterClient || !isMultiClientTest) { return $.when(); } // only the master clients sends this call
        this.#clientRegistrationId = testId; // saving id to check that client join with correct test id
        return this.#sendTestData({ startTestId: testId });
    }

    // helper function to inform other clients about a finished reload
    #sendReloadOperationAfter(options) {
        return this.#sendTestData({ oldClientId: options.oldClientId, afterReloadDocument: true });
    }

    // helper function triggered by the client to register itself at the master client
    #registerClient(testId) {
        if (this.#isMasterClient) { return $.when(); } // only the joined clients register at the master client
        return this.#sendTestData({ joinedTestId: testId });
    }

    // helper function triggered by the master client to inform all joined clients about their assigned temporary test IDs
    #prepareJoinedClients(testId) {
        // waiting two seconds for the joined clients
        if (!this.#isMasterClient) { return $.when(); } // only the master prepares the remote clients

        return this.executeDelayed(() => {
            return this.#joinedClients ? this.#sendTestData({ testId, assignedIds: this.#allClientIDs }) : null;
        }, 1000); // the clients have 1 second to register
    }

    // helper function needed for all joined clients to receive the assigned id for this test
    #receiveAssignedTestClientId(testDefinition) {
        // waiting for the operation from the server with the assigned test IDs -> checking the value in 'localTestUserId'
        if (this.#isMasterClient) { return $.when(); } // only the joined clients wait for the assigned test user ID

        const receiveAssignedId = () => {
            if (this.#localTestUserId) {
                otLogger.info('$badge{TestRunner} Receiving test user id from the master client. Received test user ID: ' + this.#localTestUserId);
                const testClientName = getClientName(this.#localTestUserId);
                const clientCommands = testDefinition.commands && testDefinition.commands[testClientName];
                // do use the assigned ID from the server only, if there exist client specific commands in the definition (otherwise work with default client name: 'CLIENT_2')
                if (clientCommands) { this.#localTestUserName = testClientName; }
                return BREAK;
            }
        };

        // waiting until all results are received, but not endlessly
        return this.repeatDelayed(receiveAssignedId, { repeatDelay: 10, cycles: 200 });
    }

    // collecting the information about all registered clients at the master, after sending the start operation
    #registerOneJoinedClient(data) {
        const clientId = data.clientId;
        this.#joinedClients++;
        this.#allClientIDs[clientId] = this.#joinedClients + 1; // the first joined client needs to get the ID 2
    }

    // every joined client gets a user Id assigned for the current test
    #assignUserIdToJoinedClient(allAssignedIds) {
        const clientId = this.#getClientId();
        this.#localTestUserId = allAssignedIds[clientId]; // the test user id for the joined client
    }

    // helper function to collect the results of the joined clients
    #registerClientResult(clientChecksum) {
        this.#allChecksums.push(clientChecksum); // collecting the checksums of the joined clients
    }

    // helper function to collect the finished clients
    #registerFinishedClient(clientName) {
        this.#allFinishedClients.push(clientName);
    }

    // the (synchronous) function that compares the document with the expectations in the definition file
    #checkResults(result) {

        const resultPromise = this.createDeferred();
        let errorCount = 0;
        let errorText = '';

        // checking the expected selection
        if (result.expectedSelection) {
            const start = this.#docModel.getSelection().getStartPosition();
            const end = this.#docModel.getSelection().getEndPosition();
            const expectedStart = result.expectedSelection.start;
            const expectedEnd = result.expectedSelection.end;

            this.#infoLine();

            if (_.isEqual(start, expectedStart) && _.isEqual(end, expectedEnd)) {
                otLogger.info('$badge{TestRunner} Selection is correct.');
            } else {
                errorText = 'Selection is not correct!';
                otLogger.info('$badge{TestRunner} ' + errorText);
                otLogger.info(' Expected: ' + JSON.stringify(expectedStart) + ', ' + JSON.stringify(expectedEnd));
                otLogger.info(' Found: ' + JSON.stringify(start) + ', ' + JSON.stringify(end));
                errorCount++;
            }

            this.#infoLine();
        }

        // checking the expected document content
        if (result.expectedTextContent) {
            const content = this.#docModel.getNode().find(PARAGRAPH_NODE_SELECTOR).text();

            this.#infoLine();

            if (content === result.expectedTextContent) {
                otLogger.info('$badge{TestRunner} Document content is correct.');
            } else {
                errorText = 'Document content is not correct.';
                otLogger.info('$badge{TestRunner} ' + errorText);
                otLogger.info(' Expected: ' + result.expectedTextContent);
                otLogger.info(' Found: ' + content);
                errorCount++;
            }

            this.#infoLine();
        }

        // checking the expected number of paragraphs
        if (result.expectedParagraphCount) {
            const paragraphCount = this.#docModel.getNode().find(PARAGRAPH_NODE_SELECTOR).length;

            this.#infoLine();

            if (paragraphCount === result.expectedParagraphCount) {
                otLogger.info('$badge{TestRunner} Number of paragraphs is correct.');
            } else {
                errorText = 'Number of paragraphs is not correct.';
                otLogger.info('$badge{TestRunner} ' + errorText);
                otLogger.info(' Expected: ' + result.expectedParagraphCount);
                otLogger.info(' Found: ' + paragraphCount);
                errorCount++;
            }

            this.#infoLine();
        }

        // checking the assertion collector
        if (this.#assertionCollector.length) {
            errorCount += this.#assertionCollector.length;
            this.#assertionCollector.forEach(errorMessage => { otLogger.info('$badge{TestRunner} ' + errorMessage); });
        }

        if (errorCount) {
            resultPromise.reject({ errorCount, errorText });
        } else {
            resultPromise.resolve();
        }

        return resultPromise;
    }

    // collecting some settings required for a reload
    #getReloadSettings(reloadContainer) {
        const reloadSettings = this.#getGlobalVariables();
        reloadSettings.oldClientId = this.#getClientId();
        reloadSettings.reloadContainer = reloadContainer;
        return reloadSettings;
    }

    // preparing the test environment with the specified commands
    #prepareTest(clientName, testDefinition) {

        const usedClientName = this.#localTestUserName || clientName; // if a global user name is specified, it will be used

        // in multi user tests, spellchecking must be deactivated for the checksum calculation
        if (testDefinition.clients && testDefinition.clients > 1 && !testDefinition.avoidAdditionalPreparation) {
            if (testDefinition.preparation && testDefinition.preparation[usedClientName]) {
                if (!testDefinition.preparation[usedClientName].otLoggingAdded) {
                    if (testDefinition.preparation[usedClientName].orders) {
                        testDefinition.preparation[usedClientName].orders = PREPARE_OT_LOGGING.orders.concat(testDefinition.preparation[usedClientName].orders);
                    } else {
                        testDefinition.preparation[usedClientName].orders = PREPARE_OT_LOGGING.orders;
                    }
                }
            } else {
                testDefinition.preparation = testDefinition.preparation || {};
                testDefinition.preparation[usedClientName] = PREPARE_OT_LOGGING;
            }
            testDefinition.preparation[usedClientName].otLoggingAdded = true; // setting a marker to add it only once
        }

        // running the specified preparation
        if (!testDefinition.preparation || !testDefinition.preparation[usedClientName]) { return $.when(); }

        return this.#commandHandler.runCommands(_.copy(testDefinition.preparation[usedClientName], true));
    }

    // running the specified test commands
    #runTest(clientName, testDefinition, testId, counter, commands) {

        let runPromise = null;
        const usedClientName = this.#localTestUserName || clientName; // if a global user name is specified, it will be used
        const reloadSettings = this.#getReloadSettings({ clientName, testDefinition, testId, counter });

        // running the specified commands
        if (commands) {
            runPromise = this.#commandHandler.runCommands(_.copy(commands, true), reloadSettings); // only used after reloading the document
        } else if (testDefinition.commands && testDefinition.commands[usedClientName]) {
            runPromise = this.#commandHandler.runCommands(_.copy(testDefinition.commands[usedClientName], true), reloadSettings);
        } else {
            runPromise = $.when();
        }

        return runPromise;
    }

    // joined clients have to send a finish signal to the master client
    #sendFinishSignal(clientName, testId) {
        if (this.#isMasterClient) { return $.when(); } // only the joined clients have to send the finish signal
        const usedClientName = this.#localTestUserName || clientName; // if a global user name is specified, it will be used
        return this.#sendTestData({ testId, clientName: usedClientName, finishedTest: true }); // the master client collects the external operations with checksums
    }

    // the master send the result request to all clientsfunction for joined clients to send the finish result to the master client
    async #sendResultRequest(testId) {
        if (!this.#isMasterClient || !this.#joinedClients) { return $.when(); } // only the master client sends the request for the results to the clients
        await new Promise(resolve => { this.setTimeout(resolve, 1000); }); // wait one second, so that all clients have all operations applied before calculating the checksum
        return this.#sendTestData({ testId, sendResult: true });
    }

    // helper function for joined clients to determine the checksum and send the value to the master client
    #sendResults(clientName, testId) {
        const usedClientName = this.#localTestUserName || clientName; // if a global user name is specified, it will be used
        return this.#sendTestData({ testId, clientName: usedClientName, clientChecksum: this.#getChecksum() }); // the master client collects the external operations with checksums
    }

    // checking the test results
    #checkTest(clientName, testDefinition) {

        const usedClientName = this.#localTestUserName || clientName; // if a global user name is specified, it will be used

        if ((!testDefinition.result || !testDefinition.result[usedClientName]) && this.#assertionCollector.length === 0) { return $.when(); }
        return this.#checkResults(testDefinition.result[usedClientName]);
    }

    // the master client must wait for the finish signal from all clients
    #waitForAllFinishSignals() {

        if (!this.#isMasterClient || !this.#joinedClients) { return $.when(); } // only the master client collects the finish state of the clients

        const receiveResults = () => {
            otLogger.info('$badge{TestRunner} Waiting for the finish signals of joined clients. Received: ' + this.#allFinishedClients.length + ' of ' + this.#joinedClients);
            if (this.#allFinishedClients.length === this.#joinedClients) { return BREAK; }
        };

        const receiveSignalsPromise = this.repeatDelayed(receiveResults, { delay: 1000, repeatDelay: 1000 });

        // waiting until all clients have finished the test, but not endlessly (idlePromise is required for crash of client(s))
        return Promise.any([receiveSignalsPromise, this.#createIdlePromise()]);
    }

    // the clients have to wait for the result request from the master client, before they send their own result to the master
    #waitForResultRequest() {

        if (this.#isMasterClient) { return $.when(); } // only the clients wait for the result request from the master

        const waitForResultRequestFromMaster = () => {
            otLogger.info('$badge{TestRunner} Waiting for the results request from the master.');
            if (this.#receivedResultRequest) { return BREAK; }
        };

        const masterRequestPromise = this.repeatDelayed(waitForResultRequestFromMaster, { delay: 1000, repeatDelay: 1000 });

        // waiting for the result request from the master, but not endlessly (idlePromise is required for crash of master)
        return Promise.any([masterRequestPromise, this.#createIdlePromise()]);
    }

    // the master client collects the results of all joined clients
    #collectResults() {

        if (!this.#isMasterClient || !this.#joinedClients) { return $.when(); } // only the master client collects the results

        const receiveResults = () => {
            otLogger.info('$badge{TestRunner} Waiting for the results of joined clients. Received: ' + this.#allChecksums.length + ' of ' + this.#joinedClients);
            if (this.#allChecksums.length === this.#joinedClients) { return BREAK; }
        };

        // waiting until all results are received, but not endlessly
        return this.repeatDelayed(receiveResults, { delay: 1000, repeatDelay: 1000, cycles: 40 });
    }

    // the (synchronous) function that compares all checksums of all remote clients
    #checkRemoteResults() {

        if (!this.#isMasterClient || !this.#joinedClients) { return $.when(); } // only the master client evaluates the results

        const resultPromise = this.createDeferred();
        let errorCount = 0;
        let errorText = '';

        this.#infoLine();

        this.#localChecksum = this.#getChecksum(); // getting the local checksum for the master client

        if (this.#joinedClients) {
            // for every client a result must be available
            if (this.#allChecksums.length === this.#joinedClients) {
                this.#allChecksums.forEach(checksum => { if (checksum !== this.#localChecksum) { errorCount++; } });
                if (errorCount) {
                    otLogger.info('$badge{TestRunner} Error: Number of remote clients: ' + this.#joinedClients);
                    errorText = 'Different checksums!';
                    otLogger.info('$badge{TestRunner} ' + errorText);
                } else {
                    otLogger.info('$badge{TestRunner} Success: Number of remote clients: ' + this.#joinedClients);
                    otLogger.info('$badge{TestRunner} All clients have the same checksum: ' + this.#localChecksum);
                }
            } else {
                errorText = 'Missing results from clients!';
                otLogger.info('$badge{TestRunner} Error: Did not get results from all clients:');
                otLogger.info('$badge{TestRunner} Number of registered clients: ' + this.#joinedClients);
                otLogger.info('$badge{TestRunner} Number of results: ' + this.#allChecksums.length);
                errorCount = 1;
            }
        } else {
            otLogger.info('$badge{TestRunner} No client joined the test.');
        }

        this.#infoLine();

        if (errorCount) {
            resultPromise.reject({ errorCount, errorText });
        } else {
            resultPromise.resolve();
        }

        return resultPromise;
    }

    // doing the work after the test has finished
    #finishTest(testId, testDefinition, counter, result) {

        if (this.#docModel.destroyed) { return; } // this might happen if reload happens inside the test

        if (!this.#localChecksum) { this.#localChecksum = this.#getChecksum(); } // only required for single client tests
        const statistics = this.#docModel.getApp().getOTStatistics();

        this.#infoLine();
        otLogger.info('$badge{TestRunner} Finished test with ID ' + testId);
        if (this.#isMasterClient) { otLogger.info('$badge{TestRunner} Number of joined clients: ' + this.#joinedClients); }
        otLogger.info('$badge{TestRunner} Number of detected collisions ' + this.#otCollisions);
        otLogger.info('$badge{TestRunner} External operations handled by OT: ' + statistics.extOpCount);
        otLogger.info('$badge{TestRunner} Number of transformations: ' + statistics.otCount);
        if (_.isNumber(counter)) { otLogger.info('$badge{TestRunner} Run ' + counter + ' of ' + testDefinition.repeat); }
        if (!counter || counter >= testDefinition.repeat) { this.#otCollisions = 0; }
        otLogger.info('$badge{TestRunner} Checksum of document ' + this.#localChecksum);

        if (result) {
            let message = '';
            if (_.isNumber(result.errorCount)) {
                otLogger.error('$badge{TestRunner} Number of errors: ' + result.errorCount);
                message = this.#assertionCollector.length ? '' : result.errorCount + ':' + result.errorText;
            } else {
                otLogger.error('$badge{TestRunner} Number of errors: ' + result);
                message = this.#assertionCollector.length ? '' : result;
            }
            message = testId + ': ' + message + " " + this.#assertionCollector.join(" ");
            this.trigger('message', message);
        } else {
            otLogger.info('$badge{TestRunner} No errors');
            this.trigger('message', '');
        }

        if (this.#assertionCollector.length) {
            otLogger.info('$badge{TestRunner} Failed assertions:');
            this.#assertionCollector.forEach(errorMessage => { otLogger.info('$badge{TestRunner} ' + errorMessage); });
        }

        this.#infoLine();

        // resetting global variables
        this.#resetGlobalSettingsAfterTest();

        // setting marker class at the page
        this.#docModel.getNode().addClass('finishedTest');
    }

    // proceeding with a test after a document reload, only specified commands available
    #proceedTest(testId, clientName, testDefinition, counter, commands) {

        this.#isTestRunning = true;
        let testPromise = $.when();

        // running the test
        testPromise = testPromise.then(this.#runTest.bind(this, clientName, testDefinition, testId, counter, commands));

        // sending info about finished test to the master client
        testPromise = testPromise.then(this.#sendFinishSignal.bind(this, clientName, testId));

        // waiting until the master client got signal about finished test from all clients
        testPromise = testPromise.then(this.#waitForAllFinishSignals.bind(this, clientName, testId));

        // the master client sends the request for results
        testPromise = testPromise.then(this.#sendResultRequest.bind(this, testId));

        // wait for result request from the master client
        testPromise = testPromise.then(this.#waitForResultRequest.bind(this, testId));

        // sending the result via operation to the master client
        testPromise = testPromise.then(this.#sendResults.bind(this, clientName, testId));

        // collecting the results from joined clients
        testPromise = testPromise.then(this.#collectResults.bind(this));

        // compare the results from joined clients
        testPromise = testPromise.then(this.#checkRemoteResults.bind(this));

        // checking the test with the specified expectations in the test definitions and the assertionCollector
        testPromise = testPromise.then(this.#checkTest.bind(this, clientName, testDefinition));

        // log (and ignore) all exceptions
        testPromise = testPromise.catch(cause => {
            if (cause instanceof Error) {
                otLogger.exception(cause);
            } else {
                otLogger.warn('$badge{TestRunner} test aborted:', cause);
            }
            return cause; // additional parameter of 'finishTest'
        });

        // finishing the test
        testPromise = testPromise.then(this.#finishTest.bind(this, testId, testDefinition, counter));

        return testPromise;
    }

    // this function executes the test once
    #runTestOnce(testId, clientName, testDefinition, counter) {

        let testPromise = $.when();

        // the master prepares the test
        if (this.#isMasterClient) {
            testPromise = testPromise.then(this.#prepareTest.bind(this, clientName, testDefinition));
        }

        // sending the info about the started tests to listening clients (if this is a multi client test)
        testPromise = testPromise.then(this.#sendTestStart.bind(this, testId, testDefinition));

        // sending the registration of a joined client via operation to the master client
        testPromise = testPromise.then(this.#registerClient.bind(this, testId));

        // the master waits for the client registration and assigns the local test IDs
        testPromise = testPromise.then(this.#prepareJoinedClients.bind(this, testId));

        // the non-master client waits for receiving the client specific test id, so that he can join the test with a valid id
        testPromise = testPromise.then(this.#receiveAssignedTestClientId.bind(this, testDefinition));

        // the joined clients prepare the test -> for this the client needs its specific client name
        if (!this.#isMasterClient) {
            testPromise = testPromise.then(this.#prepareTest.bind(this, clientName, testDefinition));
        }

        // running the test
        testPromise = testPromise.then(this.#runTest.bind(this, clientName, testDefinition, testId, counter));

        // sending info about finished test to the master client
        testPromise = testPromise.then(this.#sendFinishSignal.bind(this, clientName, testId));

        // waiting until the master client got signal about finished test from all clients
        testPromise = testPromise.then(this.#waitForAllFinishSignals.bind(this, clientName, testId));

        // the master client sends the request for results
        testPromise = testPromise.then(this.#sendResultRequest.bind(this, testId));

        // wait for result request from the master client
        testPromise = testPromise.then(this.#waitForResultRequest.bind(this, testId));

        // sending the result via operation to the master client
        testPromise = testPromise.then(this.#sendResults.bind(this, clientName, testId));

        // collecting the results from joined clients
        testPromise = testPromise.then(this.#collectResults.bind(this));

        // compare the results from joined clients
        testPromise = testPromise.then(this.#checkRemoteResults.bind(this));

        // checking the test with the specified expectations in the test definitions and the assertion collector
        testPromise = testPromise.then(this.#checkTest.bind(this, clientName, testDefinition));

        // log (and ignore) all exceptions
        testPromise = testPromise.catch(cause => {
            if (cause instanceof Error) {
                otLogger.exception(cause);
            } else {
                otLogger.warn('$badge{TestRunner} test aborted: ' + cause);
            }
            return cause; // additional parameter of 'finishTest'
        });

        // finishing the test
        testPromise = testPromise.then(this.#finishTest.bind(this, testId, testDefinition, counter));

        return testPromise;
    }

    // this function calls a test more than once. It can only be executed by the main client.
    #runTestRepeated(testId, clientName, testDefinition, repeat) {

        let counter = 0;
        let singleTestPromise = null;
        const allTestPromise = this.createDeferred();
        const repeatDelay = testDefinition.repeatDelay || DEFAULT_REPEAT_DELAY;
        const runAllTestsAsMaster = this.#isMasterClient; // this needs to be stored for all tests circles

        const repeatTest = () => {
            if (counter < repeat) {
                const emptyPromise = this.executeDelayed(this.#emptyDocument, repeatDelay); // all clients must have finished the test
                emptyPromise.then(() => {
                    return this.executeDelayed(runTestInLoop, 500);
                });
            } else {
                allTestPromise.resolve(); // TODO: Not always resolving the promise
            }
        };

        const runTestInLoop = () => {
            this.#isTestRunning = true;
            counter++;
            if (runAllTestsAsMaster) { this.#isMasterClient = true; } // restoring the info for the master client
            singleTestPromise = this.#runTestOnce(testId, clientName, testDefinition, counter);
            singleTestPromise.then(repeatTest, repeatTest);
        };

        runTestInLoop();

        return allTestPromise;
    }

    // the main test runner function
    #startTestRun(testId, asMaster) {

        const testDefinition = this.#testDefinitions[testId];
        if (!testDefinition) { return $.Deferred().reject(new Error('invalid test ID')); }

        this.#isTestRunning = true;
        this.#isMasterClient = asMaster;
        const clientName = asMaster ? MASTER_CLIENT_NAME : DEFAULT_JOINED_CLIENT_NAME;

        // informing the tester about the number of required clients if this is a multi user test
        if (_.isNumber(testDefinition.clients) && (testDefinition.clients > 1)) {
            otLogger.info('$badge{TestRunner} Be sure to start the expected number of clients: ' + testDefinition.clients);
        }

        this.#infoLine();
        otLogger.info('$badge{TestRunner} Running test with ID: ' + testId);
        this.#infoLine();

        // checking, if the test shall be repeated
        const repeat = (asMaster && testDefinition.repeat) || 1;

        return (repeat > 1) ?
            this.#runTestRepeated(testId, clientName, testDefinition, repeat) :
            this.#runTestOnce(testId, clientName, testDefinition);
    }

    /**
     * Joining a test as external client.
     *
     * @param {String} testId
     *  The ID of the test to be executed.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved or rejected after the test has finished.
     *  The return value in the promise is the number of errors. If this is 0, the
     *  promise will be resolved, otherwise it will be rejected.
     */
    #join(testId) {

        // do not join a test if another test is currently running
        if (this.#isTestRunning) { return $.when(); }

        // start with default name for joined clients
        return this.#startTestRun(testId, false);
    }
}
