/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { debug } from "@/io.ox/office/tk/algorithms";
import { type DebugFlagOptions, registerDebugFlag, getDebugFlag, setDebugFlag } from "@/io.ox/office/tk/config";
import { type LogLevel, getLogLevel, setLogLevel } from "@/io.ox/office/tk/utils/logger";

import type { ItemDefinition } from "@/io.ox/office/baseframework/controller/controlleritem";

import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

import type { AppDebugContext } from "@/io.ox/office/debug/common/app/debugcontext";

// types ======================================================================

/**
 * Shape of the debug main module with all registration entry points for a
 * Documents application.
 */
export interface DebugAppMainModule<DocAppT extends EditApplication> {
    initializeApp(docApp: DocAppT, debugContext: AppDebugContext): MaybeAsync;
    initializeView(docView: DocAppT["docView"], debugContext: AppDebugContext): MaybeAsync;
    initializeToolPane(docView: DocAppT["docView"], debugContext: AppDebugContext): MaybeAsync;
}

export type DebugQuitErrorKind = "beforeQuitHandlerSync" | "quitHandlerSync" | "quitHandlerAsync";

// public functions ===========================================================

/**
 * Loads the main debug module for the passed application. Does not fail if the
 * module does not exist.
 *
 * @param docApp
 *  The application instance to load a debug module for. Uses the type
 *  specifier in `docApp.appType` to build the full path to the debug module.
 *
 * @param successFn
 *  A callback function what will be invoked with the module contents.
 *
 * @returns
 *  A promise that will *always* be fulfilled regardless of the result.
 */
export async function importDebugAppModule<DocAppT extends EditApplication>(docApp: DocAppT, successFn: (module: DebugAppMainModule<DocAppT>) => MaybeAsync): Promise<void> {
    try {
        const module = await import(`../../${docApp.appType}/main.ts`) as DebugAppMainModule<DocAppT>;
        await successFn(module);
    } catch (err) {
        debug.logScriptError(err, "importDebugAppModule: importing debug application module failed");
    }
}

/**
 * Registers a controller item in the passed application that reads and writes
 * a value in the server-side user settings.
 *
 * @param docApp
 *  The application instance to be extended with the controller item.
 *
 * @param itemKey
 *  The key of the new controller item.
 *
 * @param configKey
 *  The key of the server-side configuration item.
 *
 * @param defValue
 *  The default value to be returned if the configuration item does not exist.
 *
 * @param [itemConfig]
 *  Additional configuration settings for the controller item, e.g. the
 *  properties "parent" or "enable".
 */
export function registerUserConfigItem(docApp: EditApplication, itemKey: string, configKey: string, defValue: unknown, itemConfig?: ItemDefinition): void {
    docApp.docController.registerItem(itemKey, {
        ...itemConfig,
        get() { return docApp.getUserSettingsValue(configKey, defValue); },
        set(value) { return docApp.setUserSettingsValue(configKey, value); },
        async: "background"
    });
}

/**
 * Registers a controller item in the passed application that represents the
 * boolean state of a debug option that will be persisted in the local storage.
 *
 * @param docApp
 *  The application instance to be extended with the controller item.
 *
 * @param configKey
 *  The name of the debug configuration option, as used in the local storage.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The key of the new controller item.
 */
export function registerDebugFlagItem(docApp: EditApplication, configKey: string, options?: DebugFlagOptions): string {

    // register options for the debug flag
    registerDebugFlag(configKey, options);

    // create a controller item for the debug flag
    const itemKey = `debug/option/${configKey}`;
    docApp.docController.registerItem<boolean>(itemKey, {
        get() { return getDebugFlag(configKey); },
        set(state) { setDebugFlag(configKey, state); }
    });

    return itemKey;
}

/**
 * Registers a controller item in the passed application that represents the
 * log level of a console logger that will be persisted in the local storage.
 *
 * @param docApp
 *  The application instance to be extended with the controller item.
 *
 * @param configKey
 *  The name of the debug configuration option, as used in the local storage.
 *
 * @param [defLevel=LogLevel.WARN]
 *  The server-side default log level if the logger has not been configured
 *  locally.
 *
 * @returns
 *  The key of the new controller item.
 */
export function registerLoggerItem(docApp: EditApplication, configKey: string, defLevel?: LogLevel): string {

    // create a controller item for the debug logger
    const itemKey = `debug/loggers/${configKey}`;
    docApp.docController.registerItem<LogLevel>(itemKey, {
        get() { return getLogLevel(configKey, defLevel); },
        set(logLevel) { setLogLevel(configKey, logLevel, defLevel); }
    });

    return itemKey;
}
