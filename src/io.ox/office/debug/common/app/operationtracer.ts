/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun, json } from "@/io.ox/office/tk/algorithms";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { otLogger } from "@/io.ox/office/editframework/utils/otutils";
import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import type { OperationAction } from "@/io.ox/office/editframework/utils/operationutils";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// interfaces =================================================================

/**
 * Type mapping for the events emitted by `OperationTracer` instances.
 */
export interface OperationTracerEventMap {
    "refresh:operation": [uid: number, finalOSN: Opt<number>, finalOperation: Opt<Operation>, ackOSN: Opt<number>, finalOPL: Opt<number>];
}

// private types --------------------------------------------------------------

/**
 * The description of an OperationTrace, that is used to collect more information for each operation.
 * Such an OperationTrace is generated for each operation and is stored in the property
 * "_operationContainer". The key in this container is the generated operation uid and the value
 * is the OperationTrace object.
 */
interface OperationTrace {

    /**
     * Whether this is an internal or external operation.
     */
    internal: boolean;

    /**
     * The OSN that is used, when the OperationTrace is generated. For internal operations this
     * happens just after the operation was locally applied. For external operations this
     * happens, when the operation was received. In both cases, no OT was applied to the
     * operation.
     */
    startOSN: number;

    /**
     * The OSN that is valid, when the operation might have been modified by OT. For internal
     * operations this is the case, when the operations are sent to the server. For external
     * operations, when they were locally applied. In both cases, the operations might have
     * been modified by OT.
     */
    finalOSN?: number;

    /**
     * The OSN that was sent from the server in the ACK for the locally sent operations. This
     * is the OSN that the server used to integrate the sent operations into the document.
     */
    ackOSN?: number;

    /**
     * The state of the operation, when the OperationTrace is generated. For internal operations
     * this happens just after the operation was locally applied. For external operations this
     * happens, when the operation was received. In both cases, no OT was applied to the
     * operation.
     */
    startOperation: Operation;

    /**
     * The state of the operation, when the operation might have been modified by OT. For internal
     * operations this is the case, when the operations are sent to the server. For external
     * operations, when they were locally applied. In both cases, the operations might have
     * been modified by OT.
     */
    finalOperation?: Operation;

    /**
     * Whether an update of the final OSN in the operations pane is required. This can happen only
     * for internal operations, because it is not clear, whether the operations were sent to the
     * server before or after logging into the operations pane.
     */
    updateFinalOSN?: boolean;

    /**
     * Whether an update of the ACK OSN in the operations pane is required. This can happen only
     * for internal operations, because it is not clear, whether the receival of the ACK OSN from
     * the server happens before or after logging into the operations pane.
     */
    updateAckOSN?: boolean;

    /**
     * The final operation length OPL, when an operation is sent to the server. This must be saved
     * for operations, that were already logged in the operations pane, because an update of the
     * operation length might be required. This can happen only for internal operations, because
     * it is not clear, whether the merge of operations happens before or after logging into the
     * operations pane.
     *
     * The operation length needs to be updated for those operations, whose length is increased.
     * Different is the behavior for those operations, that are merged into other operations:
     *
     * - If they are already logged in the operations pane, before being merged, they are shown
     *   only with the first OSN in the operations pane. No following update happens anymore. But
     *   the OPL of the leading merge operaiton must be updated later.
     *
     * - If they are not logged in the operations pane before merging, they will not appear in
     *   the operations pane. And a further update of the leading merge operation is also not
     *   required.
     */
    finalOPL?: number;

    /**
     * Whether an update of the final operation state in the operations pane is required. This can
     * happen only for internal operations, because it is not clear, whether the operations were sent
     * to the server before or after logging into the operations pane.
     */
    updateFinalOperation?: boolean;

    /**
     * Whether this operation (with its OperationTrace) was already registered in the operations pane.
     */
    operationPaneRegistered?: boolean;

    /**
     * Whether this operation (with its OperationTrace) already received the ACK OSN.
     */
    ackOSNReceived?: boolean;
}

// private functions ==========================================================

/**
 * Generates a unique serial number that can be assigned to an internal or
 * external operation.
 */
const generateOpId = fun.do(() => {
    let opIid = 1000;
    return () => (opIid += 1);
});

// class OperationTracer ======================================================

/**
 * An instance of this class represents the operation tracer. This object keeps
 * track of the states of all operations to help debugging, especially in the
 * context of OT.
 */
export class OperationTracer extends ModelObject<EditModel, OperationTracerEventMap> {

    // properties -------------------------------------------------------------

    // a collector for all operation traces. The key is a unique ID that is set as a marker at the operations.
    readonly #operationMap = new Map<number, OperationTrace>();

    // a collector for all operation UIDs that are waiting for an ack from the server
    readonly #waitingForAckMap = new Map<number, number[]>();

    // constructor ------------------------------------------------------------

    /**
     * @param docApp
     *  The application object.
     */
    constructor(docApp: EditApplication) {

        super(docApp.docModel);

        // registering the handler for applying external operations
        this.listenTo(this.docModel, "operationtrace:apply:external", this.#registerApplyOfExternalOperation);
        // registering the handler for logging when operations are applied
        this.listenTo(this.docModel, "operation:apply:before", (...args) => this.#registerApplyOperation(...args, true));
        this.listenTo(this.docModel, "operation:apply:after", (...args) => this.#registerApplyOperation(...args, false));
        // registering the handler for applying external operations
        this.listenTo(this.docModel, "operationtrace:merged:internal", this.#registerMergeOfInternalOperation);

        // registering the handler for receiving new external operations
        this.listenTo(this.docApp, "docs:operationtrace:new:external", this.#registerExternalOperations);
        // registering the handler for sending internal operations
        this.listenTo(this.docApp, "docs:operationtrace:send:internal", this.#registerSendingOfActionsBuffer);
        // registering the handler for applying new internal operations
        this.listenTo(this.docApp, "docs:operationtrace:new:internal", this.#registerInternalOperations);
        // registering the handler for new received ACK OSNs
        this.listenTo(this.docApp, "docs:operationtrace:ackosn", this.#registerAckOSN);
    }

    // public methods ---------------------------------------------------------

    /**
     * Whether there is an operation trace for the specified UID in the collector.
     *
     * @param uid
     *  The operation UID to be checked.
     *
     * @returns
     *  Whether there is an operation trace for the specified UID in the collector.
     */
    containsTrace(uid: number): boolean {
        return this.#operationMap.has(uid);
    }

    /**
     * Getting the operation in the start state for a specified UID. This is the state,
     * when an internal operation was applied or an external operation was received.
     *
     * @param uid
     *  The operation UID.
     *
     * @returns
     *  The operation in the start state with the specified UID.
     */
    getStartOperation(uid: number): Opt<Operation> {
        return this.#operationMap.get(uid)?.startOperation;
    }

    /**
     * Getting the operation in the final state for a specified UID. This is the state,
     * when an internal operation was sent to the server or an external operation was
     * applied.
     *
     * @param uid
     *  The operation UID.
     *
     * @returns
     *  The operation in the final state with the specified UID.
     */
    getFinalOperation(uid: number): Opt<Operation> {
        return this.#operationMap.get(uid)?.finalOperation;
    }

    /**
     * Getting the start OSN for a specified UID. This is the OSN, that was valid, when
     * an internal operation was applied or an external operation was received.
     *
     * @param uid
     *  The operation UID.
     *
     * @returns
     *  The OSN of the start state for the operation with the specified UID. Or -1, if it cannot
     *  be found in the collector.
     */
    getStartOSN(uid: number): number {
        return this.#operationMap.get(uid)?.startOSN ?? -1;
    }

    /**
     * Getting the final OSN for a specified UID. This is the OSN, that was valid, when
     * an internal operation was sent to the server or an external operation was applied.
     *
     * @param uid
     *  The operation UID.
     *
     * @returns
     *  The OSN of the final state for the operation with the specified UID. Or -1, if it cannot
     *  be found in the collector.
     */
    getFinalOSN(uid: number): number {
        return this.#operationMap.get(uid)?.finalOSN ?? -1;
    }

    /**
     * Getting the ACK OSN for a specified UID. This is the OSN, that the server sends back
     * with an ACK, after the client sent the operation to the filter. This is the OSN, that
     * the server used to integrate the operations into the document.
     *
     * This ACK OSN is only available for internal operations.
     *
     * @param uid
     *  The operation UID.
     *
     * @returns
     *  The OSN of the ACK received from the server for the operation with the specified UID.
     *  Or -1, if it cannot be found in the collector.
     */
    getAckOSN(uid: number): number {
        return this.#operationMap.get(uid)?.ackOSN ?? -1;
    }

    /**
     * Setting a marker, that that value of the final OSN needs to be updated later. This is set
     * in the operations pane, when an internal operation is logged, but the final OSN was not
     * available during logging into the operations pane.
     *
     * In this case the final OSN is updated in the operations pane, after the server sent the
     * ACK OSN.
     *
     * @param uid
     *  The operation UID, that requires a later update. The UID must be specified, because there
     *  is a marker class at the specific row in the operations pane, so that this row can be
     *  updated later.
     */
    updateFinalOSN(uid: number): void {
        const opTrace = this.#operationMap.get(uid);
        if (opTrace) { opTrace.updateFinalOSN = true; }
    }

    /**
     * Setting a marker, that that value of the final operation needs to be updated later. This is
     * set in the operations pane, when an internal operation is logged, but the final operation
     * (that is the state before the internal operations are sent to the server) was not available
     * during logging into the operations pane.
     *
     * In this case the final operation is updated in the operations pane, after the server sent the
     * ACK OSN.
     *
     * @param uid
     *  The operation UID, that requires a later update. The UID must be specified, because there
     *  is a marker class at the specific row in the operations pane, so that this row can be
     *  updated later.
     */
    updateFinalOperation(uid: number): void {
        const opTrace = this.#operationMap.get(uid);
        if (opTrace) { opTrace.updateFinalOperation = true; }
    }

    /**
     * Setting a marker, that that value of the ACK OSN needs to be updated later. This is set
     * in the operations pane, when an internal operation is logged, but the ACK OSN was not
     * available during logging into the operations pane.
     *
     * In this case the ACK OSN is updated in the operations pane, after the server sent the
     * ACK OSN.
     *
     * @param uid
     *  The operation UID, that requires a later update. The UID must be specified, because there
     *  is a marker class at the specific row in the operations pane, so that this row can be
     *  updated later.
     */
    updateAckOSN(uid: number): void {
        const opTrace = this.#operationMap.get(uid);
        if (opTrace) { opTrace.updateAckOSN = true; }
    }

    /**
     * The operation is registered in the operations pane. If it is also complete in the logfile,
     * the operation trace can be deleted now.
     *
     * External operations are already applied and logged. The operationsPane is updated asynchronously
     * after 'operations:after'. Therefore external operations can be deleted from operationTracer,
     * when they are registered in the operations pane.
     *
     * The logging state of internal operations is different. When they are registered in the operations
     * pane it is possible, that the ACK OSN was already received. In this case the internal logging
     * is done the row in the operations pane does not require an update. Therefore the operation
     * trace can be deleted immediately.
     *
     * If the ACK OSN was not already received, the operation trace can only be deleted after receiving
     * the ACK OSN. In this case an update of the operations pane is also required. Therefore deleting
     * of the operation trace is triggered after the update of the operations pane.
     *
     * If the internal operation was merged, the operation trace can also be removed immediately after
     * the operation was logged into the operations pane.
     *
     * @param uid
     *  The operation UID of the operation registered in the operations pane.
     */
    operationPaneRegistered(uid: number): void {
        const opTrace = this.#operationMap.get(uid);
        if (opTrace) {
            if (!opTrace.internal || opTrace.ackOSNReceived) {
                this.#deleteOperationTrace(uid);
            } else {
                opTrace.operationPaneRegistered = true; // this trace can be deleted after the update with the ack osn
            }
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Registering an operation in the collector, where a clone of the operation is inserted and the UID
     * is used as key.
     * The registration process happens, when an internal operation was applied (in 'registerAction' in the
     * editApplication) and when an external operation was received (in 'applyRemoteOperations' in the
     * editApplication).
     * These two states are the 'start' states of the operations. In both cases, no OT was done with this
     * operations.
     *
     * @param isInternal
     *  Whether this is an internal or an external operation.
     *
     * @param op
     *  The operation that will be marked, cloned and stored.
     *
     * @param startOSN
     *  The current serverOSN when this function is called.
     */
    #registerOperation(isInternal: boolean, op: Operation, startOSN: number): void {
        const uid = generateOpId();
        op.dbg_op_id = uid; // setting marker at the operation
        this.#operationMap.set(uid, { internal: isInternal, startOSN, startOperation: json.deepClone(op) });
        otLogger.info(`$badge{OperationTracer} Registering operation (start): ${uid} ${isInternal ? "internal" : "external"} OSN: ${startOSN} OP: `, op); // logging into the OT Logger
    }

    /**
     * Register the state, if an internal operation is sent to the server.
     *
     * @param op
     *  The operation in the state before it is sent to the server.
     *
     * @param finalOSN
     *  The current locally available serverOSN when the operation is sent to the server.
     */
    #registerSendingOfOneOperation(op: Operation, finalOSN: number): Opt<number> {

        // the uid of the operation
        const uid = op.dbg_op_id;

        const opTrace = uid ? this.#operationMap.get(uid) : undefined;
        if (!opTrace) { return undefined; }

        opTrace.finalOperation = json.deepClone(op);
        opTrace.finalOSN = finalOSN;
        otLogger.info(`$badge{OperationTracer} Sending operation: ${uid} OSN: ${finalOSN} OP: `, op); // logging into the OT Logger

        // if this operation was already registered, it might be necessary to update the OPL in the operations pane
        // -> this happens, if the merge of operations is done after the registration in the operations pane
        if (op.opl && op.opl > 1 && opTrace.operationPaneRegistered) { opTrace.finalOPL = op.opl;  }

        return uid;
    }

    /**
     * Registering the received ack OSN for all waiting operations.
     *
     * @param ackOSN
     *  The ACK OSN that was sent from the server for the operations that were sent before.
     *
     * @param sendOSN
     *  The send OSN that was valid, when the operations were sent to the server.
     */
    #registerAckOSNAtWaitingOPs(ackOSN: number, sendOSN: number): void {

        const uids = this.#waitingForAckMap.get(sendOSN);
        if (uids) {
            uids.forEach(uid => {
                const opTrace = this.#operationMap.get(uid);
                if (opTrace) {
                    opTrace.ackOSN = ackOSN;
                    if (opTrace.operationPaneRegistered) {
                        this.#refreshOperation(uid); // now the operation pane can be updated with the final information for internal operaitons
                        this.#deleteOperationTrace(uid); // the operation trace is no longer required and can be deleted now
                    } else {
                        opTrace.ackOSNReceived = true; // setting marker, so that the trace can be deleted after registration in operation pane
                    }
                }
            });
            otLogger.info(`$badge{OperationTracer} Received ACK OSN: ${ackOSN} for sent OSN: ${sendOSN}`); // logging into the OT Logger
            this.#waitingForAckMap.delete(sendOSN);
        }
    }

    /**
     * Updating the operations pane, after the ACK OSN was received for internal operations.
     *
     * @param uid
     *  The UID of the operation trace that can be updated.
     *
     * @param operationTrace
     *  The operation trace itself, that contains all information collected for one operation UID.
     */
    #refreshOperation(uid: number): void {

        const opTrace = this.#operationMap.get(uid);
        if (!opTrace) { return; }

        const finalOSN = opTrace.updateFinalOSN ? opTrace.finalOSN : undefined;
        const ackOSN = opTrace.updateAckOSN ? opTrace.ackOSN : undefined;
        const finalOperation = (opTrace.updateFinalOperation && opTrace.finalOperation) ? { ...opTrace.finalOperation } : undefined;

        if (finalOSN || ackOSN || finalOperation) {
            this.trigger("refresh:operation", uid, finalOSN, finalOperation, ackOSN, opTrace.finalOPL);
        }

        delete opTrace.updateFinalOSN;
        delete opTrace.updateFinalOperation;
        delete opTrace.updateAckOSN;
    }

    /**
     * Register the state of an external operation after it was applied. In this state, the OT might
     * be done with this operation. This is the final state for an external operation.
     *
     * @param op
     *  The external operation in the state after it was applied.
     *
     * @param finalOSN
     *  The OSN that was locally valid, when the operation was applied.
     */
    #registerApplyOfExternalOperation(op: Operation, finalOSN: number): void {
        const uid = op.dbg_op_id;
        if (uid) {
            const opTrace = this.#operationMap.get(uid);
            if (opTrace) {
                opTrace.finalOperation = json.deepClone(op);
                opTrace.finalOSN = finalOSN;
                otLogger.info(`$badge{OperationTracer} Applying external operation: ${uid} OSN: ${finalOSN} OP: `, op);
            }
        }
    }

    /**
     * Register when an operation is applied. This function is called, before and after applying
     * an operation.
     *
     * @param op
     *  The external operation in the state after it was applied.
     *
     * @param finalOSN
     *  The OSN that was locally valid, when the operation was applied.
     *
     * @param external
     *  Whether the operation is external or internal.
     */
    #registerApplyOperation(op: Operation, finalOSN: number, external: boolean, before: boolean): void {
        const uid = op.dbg_op_id || "UID not set. ";
        const externalSate = external ? "external" : "internal";
        const state = before ? "Before" : "After";
        otLogger.info(`$badge{OperationTracer} ${state} applying ${externalSate} operation: ${uid} OSN: ${finalOSN} OP: `, op);
    }

    /**
     * After merging operations before sending them to the server, this regisration is important to
     * check, when the operation trace can be removed.
     *
     * @param op
     *  The internal operation that was merged.
     */
    #registerMergeOfInternalOperation(op: Operation): void {
        const uid = op.dbg_op_id;
        if (uid) {
            const opTrace = this.#operationMap.get(uid);
            if (opTrace) {
                // otLogger.info(`$badge{OperationTracer} Merged internal operation: ${uid} OP: `, op); // logging into the OT Logger -> useful information?
                // This operation trace can be deleted, because it will not be sent to the server
                this.#deleteOperationTrace(uid);
            }
        }
    }

    /**
     * Register the state of an external operation directly after it was received. In this
     * state no OT was done with this operation.
     *
     * @param operations
     *  The external operations in the state after it was received.
     *
     * @param startOSN
     *  The current locally available serverOSN when the external operation was received.
     *
     * @param operationServerOSN
     *  The OSN that was sent from the server together with the external operation.
     */
    #registerExternalOperations(operations: Operation[], startOSN: number, operationServerOSN: number): void {
        otLogger.info(`$badge{OperationTracer} Registering external operations received with OSN sent from the server: ${operationServerOSN}. Current local OSN: ${startOSN}.`);
        operations.forEach(op => this.#registerOperation(false, op, startOSN));
    }

    /**
     * Register the states of the internal operations included in the actionsBuffer just
     * before they are sent to the server. In this state, the OT might be done with this
     * internal operations. This is the final state for an internal operation.
     *
     * @param actions
     *  An array with all actions that will be sent to the server.
     *
     * @param finalOSN
     *  The OSN that is locally valid, when the operation are sent to the server.
     */
    #registerSendingOfActionsBuffer(actions: OperationAction[], finalOSN: number): void {

        // a collector for all operation UIDs waiting for the ack from the server
        const opUids: number[] = [];

        actions.forEach(oneAction => {
            oneAction.operations?.forEach(op => {
                const uid = this.#registerSendingOfOneOperation(op, finalOSN);
                if (uid) { opUids.push(uid); }
            });
        });

        if (opUids.length > 0) {
            this.#waitingForAckMap.set(finalOSN, opUids); // saving the UIDs of the waiting operations
        }
    }

    /**
     * Register the state of an internal operation directly after it was applied. In this
     * state no OT was done with this operation.
     *
     * @param operations
     *  The internal operations in the state after it was applied.
     *
     * @param startOSN
     *  The current locally available serverOSN when the operation was applied.
     */
    #registerInternalOperations(operations: Operation[], startOSN: number): void {
        operations.forEach(op => this.#registerOperation(true, op, startOSN));
    }

    /**
     * Registering the ACK OSN that was received from the server as answer to the operations
     * that were sent to the server before. The server added this operations with the specified
     * ACK OSN into the document.
     *
     * @param ackOSN
     *  The OSN that is sent from the server for the operations that were sent before.
     *
     * @param sendOSN
     *  The OSN that is locally valid, when the operation were sent to the server.
     */
    #registerAckOSN(ackOSN: number, sendOSN: number): void {
        // registering the ack OSN at the 'waiting' operations
        this.#registerAckOSNAtWaitingOPs(ackOSN, sendOSN);
    }

    /**
     * Deleting the operation trace with the specified UID from the collector.
     *
     * @param uid
     *  The operation UID, whose operation trace will be removed.
     */
    #deleteOperationTrace(uid: number): void {
        this.#operationMap.delete(uid);
    }
}
