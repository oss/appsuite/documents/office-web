/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { BaseObject } from "@/io.ox/office/tk/objects";
import { EditApplication } from "@/io.ox/office/editframework/app/editapplication";
import { ReplayWorker } from "@/io.ox/office/debug/common/app/replayworker";

// types ======================================================================

export interface OTTestDefinition {
    description: string;
    tooltip?: string;
    clients?: number;
    autotest?: boolean;
}

/**
 * Type mapping for the events emitted by `OTTestRunner` instances.
 */
export interface OTTestRunnerEventMap {
    message: [message: string];
}

// class OTTestRunner =========================================================

export class OTTestRunner extends BaseObject<OTTestRunnerEventMap> {

    constructor(docApp: EditApplication, replayWorker: ReplayWorker);

    registerDefinitions(definitions: Dict<OTTestDefinition>): void;
    getTestDefinitions(): Dict<OTTestDefinition>;

    isTestRunning(): boolean;
    start(testId: string): JPromise;
    stop(): void;
}
