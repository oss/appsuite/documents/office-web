/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, dict, jpromise } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { BaseObject } from '@/io.ox/office/tk/objects';

import { ActivateToolBarCommand } from '@/io.ox/office/debug/common/app/commands/activatetoolbarcommand';
import { AssertCommand } from '@/io.ox/office/debug/common/app/commands/assertcommand';
import { ButtonClickCommand } from '@/io.ox/office/debug/common/app/commands/buttonclickcommand';
import { FunctionCallCommand } from '@/io.ox/office/debug/common/app/commands/functioncallcommand';
import { KeyboardEventsCommand } from '@/io.ox/office/debug/common/app/commands/keyboardeventscommand';
import { OperationsCommand } from '@/io.ox/office/debug/common/app/commands/operationscommand';
import { SelectionCommand } from '@/io.ox/office/debug/common/app/commands/selectioncommand';
import { BlockExternalOpsCommand } from '@/io.ox/office/debug/common/app/commands/blockexternalopscommand';
import { TimeoutCommand } from '@/io.ox/office/debug/common/app/commands/timeoutcommand';
import { TriggerEventCommand } from '@/io.ox/office/debug/common/app/commands/triggereventcommand';

// constants ==================================================================

/**
 * The default initial delay before starting the test.
 */
const DEFAULT_INITIAL_DELAY = 200;

/**
 * The default delay between executing two commands.
 */
const DEFAULT_COMMAND_DELAY = 500;

// class CommandHandler =======================================================

/**
 * An instance of this class represents the command handler for all commands
 * supported by the (OT) TestRunner.
 */
export class CommandHandler extends BaseObject {

    constructor(docApp, replayWorker) {
        super();

        // the container for all command handler functions
        this.handlers = {};

        // register the commands
        this.#registerCommand(new ActivateToolBarCommand(docApp));
        this.#registerCommand(new AssertCommand(docApp));
        this.#registerCommand(new BlockExternalOpsCommand(docApp));
        this.#registerCommand(new ButtonClickCommand(docApp));
        this.#registerCommand(new FunctionCallCommand(docApp));
        this.#registerCommand(new KeyboardEventsCommand(docApp));
        this.#registerCommand(new OperationsCommand(docApp, replayWorker));
        this.#registerCommand(new SelectionCommand(docApp));
        this.#registerCommand(new TimeoutCommand(docApp));
        this.#registerCommand(new TriggerEventCommand(docApp));
    }

    // public methods ---------------------------------------------------------

    /**
     * The function that executes all commands.
     *
     * @param {Object} commands
     *  The commands object as specified in the test definition files.
     *
     * @param {Object} reloadSettings
     *  The settings that are required to continue the test after a reload
     *  of the document.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when all orders specified in
     *  the command are done.
     */
    runCommands(commands, reloadSettings) {

        var timers = commands.timers || null;
        var testStartDelay = is.number(timers?.arbitraryTestStartDelayMax) ? Math.floor(timers.arbitraryTestStartDelayMax * Math.random()) : 0;
        testStartDelay = is.number(timers?.testStartDelay) ? timers.testStartDelay : testStartDelay; // specifying a precise start delay for the test
        var allOrders = commands.orders || [];
        var context = {
            initialDelay: is.number(timers?.initialDelay) ? timers.initialDelay : DEFAULT_INITIAL_DELAY,
            commandDelay: is.number(timers?.commandDelay) ? timers.commandDelay : DEFAULT_COMMAND_DELAY,
            commands,
            reloadSettings
        };

        // iterating asynchronously over all specified orders
        return this.iterateArraySliced(allOrders, order => {

            const handler = dict.find(this.handlers, handler => handler.name in order);
            if (handler) {
                return jpromise.invoke(() => handler.runCommand(order, context));
            }

            globalLogger.error('Unknown command: ', order);

        }, { delay: testStartDelay });
    }

    // private methods --------------------------------------------------------

    /**
     * Registering a handler function for a specific command.
     *
     * @param {object} command
     *  The command name, for that the function will be registered.
     */
    #registerCommand(command) {

        const { name } = command;

        if (this.handlers[name]) {
            globalLogger.warn('There is already a handler for this command specified (will be overwritten): ', name);
        }

        this.handlers[name] = this.member(command);
    }
}
