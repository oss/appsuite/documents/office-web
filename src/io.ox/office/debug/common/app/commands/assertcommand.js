/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';

import { is } from '@/io.ox/office/tk/algorithms';
import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';
import { equalPositions } from '@/io.ox/office/editframework/utils/operations';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';

// class AssertCommand ========================================================

export class AssertCommand extends BaseCommand {

    constructor(docApp) {

        // base constructor
        super(docApp, 'assert');
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for the command 'assert'.
     *
     * Important: The assertion handling is not supported in the preparation phase of a test.
     *
     * @param {Object} order
     *  The order object specifying an 'assert' command.
     *
     * @param {Object} context
     *  The context object with the properties:
     *  - commandDelay {Number}: The duration between the commands.
     *  - initialDelay {Number}: The duration before starting the first command.
     *  - commands {Object}: The commands object as specified in the test definition files.
     *  - reloadSettings {Object}: The settings that are required to continue the test after a reload of the document.
     */
    /*override*/ runCommand(order, context) {

        const assert = order.assert;
        let type = null;

        if (assert && assert.expect && context && context.reloadSettings && context.reloadSettings.assertionCollector) {

            type = assert.expect.type;

            switch (type) {
                case 'selection':
                    this.#checkSelection(assert, context.reloadSettings.assertionCollector);
                    break;
                case 'text':
                    this.#checkText(assert, context.reloadSettings.assertionCollector);
                    break;
                case 'function':
                    this.#checkFunction(assert, context.reloadSettings.assertionCollector);
                    break;
                case 'call':
                    this.#checkCall(assert, context.reloadSettings.assertionCollector);
                    break;
            }
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Helper function to add the assertion message into the assertion collector.
     *
     * @param {String} assertMessage
     *  The assert message that will be included into the assertion collector.
     *
     * @param {String} assertId
     *  If specified, this ID will be prepended to the specified assertMessage.
     *
     * @param {Array} assertionCollector
     *  The collector for the failed assertions. This collector will be filled
     *  within this function.
     */
    #addAssertMessage(assertMessage, assertId, assertionCollector) {
        if (assertId) { assertMessage = 'Assert ID ' + assertId + ': ' + assertMessage; }
        assertionCollector.push(assertMessage);
    }

    /**
     * Replacing the string representation of a parameter by its 'real' value.
     *
     * @param {String} param
     *  The string representation of a parameter.
     *
     * @returns {String|jQuery}
     *  The jQueryfied node that can be used instead of the string representation.
     */
    #replaceOneParam(param) {

        let newParam = param;

        if (param === 'CURRENT_ROOT_NODE') {
            newParam = this.docModel.getCurrentRootNode();
        }

        return newParam;
    }

    /**
     * Replacing the string representation of a parameter or an array of string
     * representations of a parameter by their 'real' values.
     *
     * @param {String|String[]} params
     *  The string representation of a parameter or an array filled with the
     *  string representations.
     *
     * @returns {String|jQuery}
     *  The optionally modified string parameter or array of string parameters,
     *  that might contain replacement nodes.
     */
    #replaceStringParameter(params) {
        return is.array(params) ? params.map(parameter => this.#replaceOneParam(parameter)) : this.#replaceOneParam(params);
    }

    /**
     * Checking a specified selection.
     *
     * @param {Object} assert
     *  An object describing the assertion with the expected selection.
     *
     * @param {Array} assertionCollector
     *  The collector for the failed assertions.
     */
    #checkSelection(assert, assertionCollector) {

        // Example: { assert: { expect: { type: 'selection', start: [0, 3], end: [0, 3] }, text: 'Unexpected selection.' } },

        const expect = assert.expect;
        const selection = this.docModel.getSelection();
        const realStartPos = selection.getStartPosition();
        const realEndPos = selection.getEndPosition();
        const expectedStartPos = expect.start;
        const expectedEndPos = expect.end;
        let assertMessage = '';

        if (!equalPositions(realStartPos, expectedStartPos)) {
            assertMessage = assert.text + 'Expected start position: '  + JSON.stringify(expectedStartPos) + ', found start position: ' + JSON.stringify(realStartPos);
        } else if (expectedEndPos && !equalPositions(realEndPos, expectedEndPos)) {
            assertMessage = assert.text + 'Expected end position: '  + JSON.stringify(expectedEndPos)  + ', found end position: ' + JSON.stringify(realEndPos);
        } else if (!expectedStartPos) {
            assertMessage = assert.text + ' No start position specified in assertion! Example: { assert: { expect: { type: "selection", start: [0, 3], end: [0, 3] }, text: "Unexpected selection." } }';
        }

        if (assertMessage) {
            this.#addAssertMessage(assertMessage, assert.id, assertionCollector);
        }
    }

    /**
     * Checking a specified text content.
     *
     * @param {Object} assert
     *  An object describing the assertion with the expected text content.
     *
     * @param {Array} assertionCollector
     *  The collector for the failed assertions.
     */
    #checkText(assert, assertionCollector) {

        // Example { assert: { expect: { type: 'text', position: [0], content: 'abc' }, text: 'Unexpected text content.' } }

        const expect = assert.expect;
        const paragraphPos = expect.position;
        let paraPoint = null;
        let realText = null;
        const expectedText = expect.content;
        let assertMessage = '';

        if (paragraphPos) {
            paraPoint = Position.getDOMPosition(this.docModel.getCurrentRootNode(), paragraphPos, true);

            if (paraPoint && paraPoint.node && DOM.isParagraphNode(paraPoint.node)) {
                realText = $(paraPoint.node).text();

                if (realText !== expectedText) { // TODO: This is problematic for whitespaces
                    assertMessage = assert.text + 'Expected text: "' + expectedText + '", found text: "' + realText + '"';
                }
            } else {
                assertMessage = assert.text + ' No paragraph at specified position ' + JSON.stringify(paragraphPos);
            }
        } else {
            assertMessage = assert.text + ' No position specified in assertion! Example: { assert: { expect: { type: "text", position: [0], content: "abc" }, text: "Unexpected text content." } }';
        }

        if (assertMessage) {
            this.#addAssertMessage(assertMessage, assert.id, assertionCollector);
        }
    }

    /**
     * Checking a specified function call return value.
     *
     * @param {Object} assert
     *  An object describing the assertion with the expected function call result.
     *
     * @param {Array} assertionCollector
     *  The collector for the failed assertions.
     */
    #checkFunction(assert, assertionCollector) {

        // Example { assert: { expect: { type: 'function', context: 'model', function: 'getActiveSlideId', result: 'slide_1' }, text: 'Unexpected active slide ID.' } },
        const exampleString = 'Example: { assert: { expect: { type: "function", context: "model", function: "getActiveSlideId", result: "slide_4" }, text: "Unexpected active slide ID." } }';
        let assertMessage = '';

        try {

            const expect = assert.expect;
            let callObject = null;
            const functionName = is.string(expect.function) ? expect.function : null;
            const functionList = is.array(expect.function) ? expect.function : null;
            const expectedResult = expect.result;
            let result = null;
            let params = null;

            switch (expect.context) {
                case 'app':
                    callObject = this.docApp;
                    break;
                case 'model':
                    callObject = this.docModel;
                    break;
                case 'selection':
                    callObject = this.docModel.getSelection();
                    break;
                case 'view':
                    callObject = this.docView;
                    break;
                case 'DOM':
                    callObject = DOM;
                    break;
                case 'Position':
                    callObject = Position;
                    break;
            }

            if (callObject && (functionName || functionList)) {

                if (functionName) {
                    if (callObject[functionName]) {
                        result = callObject[functionName]();
                    } else {
                        assertMessage =  assert.text + ' Function ' + functionName + ' does not exist on object! ' + exampleString;
                    }
                } else if (functionList) {
                    functionList.forEach(functionCall => {
                        if (callObject[functionCall.name] !== undefined) {
                            if (functionCall.isProperty) {
                                callObject = callObject[functionCall.name];
                            } else {
                                if (functionCall.param === undefined) {
                                    callObject = callObject[functionCall.name]();
                                } else if (is.string(functionCall.param)) {
                                    params = this.#replaceStringParameter(functionCall.param);
                                    callObject = callObject[functionCall.name](params);
                                } else if (is.array(functionCall.param)) {
                                    params = this.#replaceStringParameter(functionCall.param);
                                    callObject = callObject[functionCall.name](...params);
                                }
                            }
                        } else {
                            assertMessage = assert.text + ' Function ' + functionCall.name + ' does not exist on object! ' + exampleString;
                        }
                    });

                    result = callObject;
                }

                if (result !== expectedResult) {
                    assertMessage = assert.text + 'Expected: ' + expectedResult + ', found: ' + result;
                }
            } else {
                assertMessage = assert.text + ' No object or function name specfied in assertion! ' + exampleString;
            }
        } catch (error) {
            assertMessage = assert.text + ' Failure in function call! Message: ' + error.message + ' '  + exampleString;
        }

        if (assertMessage) {
            this.#addAssertMessage(assertMessage, assert.id, assertionCollector);
        }
    }

    /**
     * Checking a specified return value received from a direct function call.
     *
     * @param {Object} assert
     *  An object describing the assertion with the expected function call result.
     *
     * @param {Array} assertionCollector
     *  The collector for the failed assertions.
     */
    #checkCall(assert, assertionCollector) {

        let assertMessage = '';

        try {

            const expect = assert.expect;
            const func = expect.function;
            const expectedResult = expect.result;
            let result = null;

            if (func) {

                result = func(this.docApp);

                if (result !== expectedResult) {
                    assertMessage = assert.text + 'Expected: ' + expectedResult + ', found: ' + result;
                }
            } else {
                assertMessage = assert.text + ' No function specfied in assertion!';
            }
        } catch (error) {
            assertMessage = assert.text + ' Failure in function call! Message: ' + error.message;
        }

        if (assertMessage) {
            this.#addAssertMessage(assertMessage, assert.id, assertionCollector);
        }
    }
}
