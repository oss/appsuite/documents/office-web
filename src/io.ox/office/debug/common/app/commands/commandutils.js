/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';

import { is } from '@/io.ox/office/tk/algorithms';
import { getOxoPosition } from '@/io.ox/office/textframework/utils/position';

// public functions =======================================================

/**
 * Helper function to find a selection with logical positions for start and end
 * from a description with a selector.
 *
 * @param {Object} docApp
 *  The application object.
 *
 * @param {Object} desc
 *  The description object that is used to calculate a logical position.
 *  -> the description object supports the following properties:
 *    - 'selector' (String) to find the node in the dom
 *    - 'target', (String) optional, to define the logical position (if not specified the empty target is used)
 *    - 'index', (Number) optional, to specify the 0-based index of found nodes, that shall be used
 *    - 'appendStart' (Number[]), optional, to append further numbers to the logical start position
 *    - 'appendEnd' (Number[]), optional, to append further numbers to the logical end position
 *
 * @returns {Object}
 *  An object with the properties 'start' and 'end' containing the logical positions.
 */
export function getSelectionFromDescription(docApp, desc) {

    let selectionNode = null;
    let rootPos = null;
    let start = null;
    let end = null;

    if (desc.selector) {
        selectionNode = $(desc.selector);
        if (selectionNode.length > 1 && is.number(desc.index)) { selectionNode = selectionNode[desc.index]; }
        rootPos = getOxoPosition(docApp.docModel.getRootNode(desc.target), selectionNode, 0);
        start = is.array(desc.appendStart) ? rootPos.concat(desc.appendStart) : rootPos;
        end = is.array(desc.appendEnd) ? rootPos.concat(desc.appendEnd) : undefined;
    }

    return { start, end };
}
