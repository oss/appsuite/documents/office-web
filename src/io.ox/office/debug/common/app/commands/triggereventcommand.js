/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { ary, is } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE } from '@/io.ox/office/tk/dom';
import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';

// constants ==================================================================

// the map of the mouse events
const MOUSE_EVENTS = {
    mousedown: 1,
    mousemove: 1,
    mouseup: 1
};

// the map of the supported custom events
const CUSTOM_EVENTS = {
    selectionBox: 1,
    move: 1,
    resize: 1,
    rotate: 1,
    adjust: 1,
    nodeClick: 1
};

// class TriggerEventCommand ==================================================

export class TriggerEventCommand extends BaseCommand {

    constructor(docApp) {
        super(docApp, 'triggerevent');
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for the command 'triggerevent'.
     *
     * @param {Object} order
     *  The order object specifying a 'triggerevent' command.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    /*override*/ runCommand(order) {

        if (is.string(order.triggerevent.type) && CUSTOM_EVENTS[order.triggerevent.type]) {
            const generatedOrders = this.#generateCustomEventOrder(order.triggerevent.type, _.copy(order.triggerevent.definition, true));
            return this.iterateArraySliced(generatedOrders, singleOrder => this.#runSingleCommand(singleOrder), { delay: 250 });
        }

        return this.#runSingleCommand(order);
    }

    // private methods --------------------------------------------------------

    /**
     * Helper function to specify the default button in the event definitions,
     * if it is not already set.
     *
     * @param {String} eventType
     *  The event type.
     *
     * @param {Object} def
     *  The event definition object required to generate the jQuery event.
     *  This might be modified within this function.
     */
    #addDefaultButtonToMouseEvent(eventType, def) {
        if (MOUSE_EVENTS[eventType] && !is.number(def.button)) { def.button = 0; } // setting the default button for mouse events
    }

    /**
     * Helper function to calculate relative event positions specified
     * in the event definitions.
     *
     * @param {Object} def
     *  The event definition object required to generate the jQuery event.
     *  This might be modified within this function.
     */
    #adaptRelativeEventPositions(def) {

        const pageX = def.pageX;
        const pageY = def.pageY;
        let zoomFactor = 1;
        let referenceNode = null;

        if (this.docApp.isPresentationApp()) {
            referenceNode = this.docModel.getActiveSlide();
        } else if (this.docApp.isTextApp()) {
            referenceNode = this.docModel.getNode();
        } else if (this.docApp.isSpreadsheetApp) {
            referenceNode = $('#io-ox-core');
        }

        // definition: { target: 'ACTIVE_SLIDE', pageX: { relative: 0.05 }, pageY: { relative: 0.05 } } // adding 5% of slide width and slide height to upper left corner

        if (referenceNode && is.dict(pageX) && is.number(pageX.relative)) {
            zoomFactor = this.docView.getZoomFactor();
            const baseX = referenceNode.offset().left;
            const width = referenceNode.width();
            def.pageX = baseX + pageX.relative * width * zoomFactor;
        }

        if (referenceNode && is.dict(pageY) && is.number(pageY.relative)) {
            zoomFactor = this.docView.getZoomFactor();
            const baseY = referenceNode.offset().top;
            const height = referenceNode.height();
            def.pageY = baseY + pageY.relative * height * zoomFactor;
        }

    }

    /**
     * Generating one trigger event object.
     *
     * @param {String} type
     *  The event type.
     *
     * @param {String} target
     *  The event type.
     *
     * @param {String} selector
     *  A selector to specify the event target.
     *
     * @param {Number} pageX
     *  The horizontal (relative) event position.
     *
     * @param {Number} pageY
     *  The vertical (relative) event position.
     *
     * @param {Boolean|undefined} forceTestSelection
     *  Whether a test browser selection must to be generated.
     *  This is only evaluated by 'nodeClick' custom events.
     *
     * @param {String|undefined} textposition
     *  The text position inside a paragraph specified by a selector.
     *  This is only evaluated by 'nodeClick' custom events.
     *  Allowed values are 'start' (the default), 'middle' or 'end'.
     *
     * @returns {Object}
     *  A generated trigger event object.
     */
    #generateOneTriggerEventObject(type, target, selector, pageX, pageY, forceTestSelection, textposition) {
        return {
            triggerevent: {
                type,
                definition: {
                    target,
                    selector,
                    pageX: { relative: pageX },
                    pageY: { relative: pageY },
                    textposition,
                    forceTestSelection
                }
            }
        };
    }

    /**
     * In OX Text a mousedown or mouseup event with target 'PAGE' needs to be simulated.
     * The browser selection cannot be used, because the simulated event is not trusted.
     * Instead the event target is set to the end of the last paragraph in the document.
     *
     * @param {Object} def
     *  The triggerevent definition object required to generate the jQuery events.
     */
    #simulateClickAtDocumentEnd(def) {
        def.target = null;
        def.selector = '.app-content > .page > .pagecontent > .p:last-of-type';
        def.textposition = 'end';
        def.forceTestSelection = true;
    }

    /**
     * Generating the array of required trigger events from the specified custom
     * event. One custom event typically leads to a multitude of 'known' events.
     *
     * @param {String} eventType
     *  The event type.
     *
     * @param {Object} def
     *  The triggerevent definition object required to generate the jQuery events.
     *
     * @returns {Object[]}
     *  An array with the generated events.
     */
    #generateCustomEventOrder(eventType, def) {

        const commandOrder = [];
        let mouseDownTarget = '';
        let mouseMoveTarget = '';
        let mouseUpTarget = '';
        let mouseDownSelector = '';
        const mouseMoveSelector = '';
        let mouseUpSelector = '';
        let additionalMouseUpTarget = '';

        if (eventType === 'selectionBox') {

            if (this.docApp.isSpreadsheetApp()) {
                // support for all modes: 'shape' and 'setselection'
                mouseDownSelector = def.selector;
                mouseMoveTarget = 'TRACKING_ROOT_NODE';
                mouseUpTarget = 'TRACKING_ROOT_NODE';
                // }
            } else {
                mouseDownTarget = this.docApp.isTextApp() ? 'PAGE' : 'ACTIVE_SLIDE';
                mouseMoveTarget = 'WINDOW';
                additionalMouseUpTarget = 'WINDOW';

                if (def.mode === 'shape' || def.mode === 'textframe') {
                    mouseUpTarget = 'CANVAS_IN_SELECTION_BOX';
                } else {
                    // 'setselection' is the default mode
                    if (def.mode !== 'setselection') {} // TODO
                    mouseUpTarget = 'SELECTION_BOX';
                }
            }

        } else if (eventType === 'move') {

            mouseDownTarget = '';
            mouseMoveTarget = 'TRACKER_NODE';
            mouseUpTarget = 'TRACKER_NODE';
            mouseDownSelector = def.selector;

        } else if (eventType === 'resize' || eventType === 'rotate' || eventType === 'adjust') {

            mouseDownTarget = '';
            mouseMoveTarget = 'TRACKING_ROOT_NODE';
            mouseUpTarget = 'TRACKING_ROOT_NODE';
            mouseDownSelector = def.selector;

        } else if (eventType === 'nodeClick') {

            // Convenience for OX Text -> a click on the page is simulated as click at the end of the document
            // -> a click on the slide in OX Presentation leads already in mousedown to a valid slide selection
            if (this.docApp.isTextApp() && def.target === 'PAGE') {
                this.#simulateClickAtDocumentEnd(def);
            }

            // simulating 'mousedown' and 'mouseup' on the specified node
            def.startX = 0;
            def.startY = 0;
            def.stepCount = 0;
            def.stepX = 0;
            def.stepY = 0;
            mouseDownTarget = def.target;
            mouseUpTarget = def.target;
            mouseDownSelector = def.selector;
            mouseUpSelector = def.selector;

            if (this.docApp.isPresentationApp()) { additionalMouseUpTarget = 'WINDOW'; } // TODO (only required in OX Presentation caused by the selection box)

        } else {
            // Not supported, TODO
            return [];
        }

        const startEventName = TOUCH_DEVICE ? 'touchstart' : 'mousedown';
        const moveEventName = TOUCH_DEVICE ? 'touchmove' : 'mousemove';
        const endEventName = TOUCH_DEVICE ? 'touchend' : 'mouseup';

        // generating the mousedown operation
        commandOrder.push(this.#generateOneTriggerEventObject(startEventName, mouseDownTarget, mouseDownSelector, def.startX, def.startY, def.forceTestSelection, def.textposition));

        // generating the mousemove operation(s)
        for (let i = 1; i <= def.stepCount; i++) {
            commandOrder.push(this.#generateOneTriggerEventObject(moveEventName, mouseMoveTarget, mouseMoveSelector, def.startX + i * def.stepX, def.startY + i * def.stepY, false, undefined));
        }

        // generating the mouseup operation(s)
        commandOrder.push(this.#generateOneTriggerEventObject(endEventName, mouseUpTarget, mouseUpSelector, def.startX + def.stepCount * def.stepX, def.startY + def.stepCount * def.stepY, def.forceTestSelection, def.textposition));

        // the selection box (and the nodeClick) requires one mouseup on the 'WINDOW' and one additional 'mouseup' on the target node
        if (additionalMouseUpTarget) {
            commandOrder.push(this.#generateOneTriggerEventObject(endEventName, additionalMouseUpTarget, mouseUpSelector, def.startX + def.stepCount * def.stepX, def.startY + def.stepCount * def.stepY, def.forceTestSelection, def.textposition));
        }

        return commandOrder;
    }

    /**
     * Running one keyboard command with the specified event definition.
     *
     * @param {String} eventType
     *  The event type.
     *
     * @param {Object} def
     *  The event definition object required to generate the jQuery event.
     *
     * @returns {jQuery.Promise | void} // TODO: check-void-return
     *  A promise that will be resolved after a fixed duration.
     */
    #triggerOneEvent(eventType, def) {

        let triggerNode = null;
        let event = null;

        this.#addDefaultButtonToMouseEvent(eventType, def);
        this.#adaptRelativeEventPositions(def);

        if (def.target === 'WINDOW') {

            event = new MouseEvent(eventType, {
                bubbles: true,
                cancelable: true,
                view: window,
                detail: 1,
                screenX: def.pageX,
                screenY: def.pageY,
                clientX: def.pageX,
                clientY: def.pageY
            });

            window.dispatchEvent(event);

        } else {

            if (def.target === 'PAGE') {
                triggerNode = this.docModel.getNode();
            } else if (def.target === 'FIRST_HEADER') {
                triggerNode = this.docModel.getNode().find('.header.first');
            } else if (def.target === 'APP_CONTENT_ROOT') {
                triggerNode = this.docView.getContentRootNode();
            } else if (def.target === 'TRACKING_ROOT_NODE') {
                triggerNode = $('#io-ox-core');
            } else if (def.target === 'TRACKER_NODE') {
                triggerNode = $('div.tracker');
            } else if (def.target === 'SELECTION_BOX') {
                triggerNode = $('.selectionbox');
            } else if (def.target === 'CANVAS_IN_SELECTION_BOX') {
                triggerNode = this.docView.getContentRootNode().children().children('canvas.canvasgeometry');
            } else if (def.target === 'ACTIVE_SLIDE') {
                triggerNode = this.docModel.getActiveSlide();
            } else if (def.selector) {
                triggerNode = $(def.selector);
            }

            if (!triggerNode || triggerNode.length === 0) { return; } // TODO

            const triggerEvent = {
                bubbles: true,
                cancelable: true,
                view: window,
                detail: 1
            };

            if (is.number(def.pageX)) {
                triggerEvent.screenX = def.pageX;
                triggerEvent.clientX = def.pageX;
            }

            if (is.number(def.pageY)) {
                triggerEvent.screenY = def.pageY;
                triggerEvent.clientY = def.pageY;
            }

            if (is.number(def.button)) {
                triggerEvent.button = def.button;
            }

            triggerNode[0].dispatchEvent(
                new MouseEvent(eventType, triggerEvent)
            );
        }

        return this.executeDelayed(_.noop, 100); // resolving the promise with a litte delay
    }

    /**
     * Running one single command 'triggerevent'. Typically only one command is allowed inside
     * a 'triggerevent' order. But in the case of a custom event, like 'rotate', 'resize', ...
     * an array of commands is generated.
     *
     * @param {Object} order
     *  The order object specifying a 'triggerevent' command.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    #runSingleCommand(order) {
        const allEventtypes = ary.wrap(order.triggerevent.type);
        return this.iterateArraySliced(allEventtypes, eventType => this.#triggerOneEvent(eventType, _.copy(order.triggerevent.definition, true)), { delay: 200 });
    }
}
