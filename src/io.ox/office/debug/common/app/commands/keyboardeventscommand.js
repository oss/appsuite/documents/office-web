/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { KeyCode, KeyUpDownOnly } from '@/io.ox/office/tk/dom';

import { isCommentTextframeNode } from "@/io.ox/office/textframework/utils/dom";

import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';

// constants ==================================================================

// the list of supported meta keys
const META_KEYS = ['shift', 'ctrl', 'alt'];
// the container of all keys that are used in copy and paste events
const COPY_PASTE_KEYS = { c: 'copy', C: 'copy', v: 'paste', V: 'paste' };

// class KeyboardEventsCommand ================================================

export class KeyboardEventsCommand extends BaseCommand {

    constructor(docApp) {
        super(docApp, 'keyboardevents');
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for the command 'keyboardevents'.
     *
     * @param {Object} order
     *  The order object specifying a 'keyboardevents' command.
     *
     * @param {Object} context
     *  The context object with the properties:
     *  - commandDelay {Number}: The duration between the commands.
     *  - initialDelay {Number}: The duration before starting the first command.
     *  - commands {Object}: The commands object as specified in the test definition files.
     *  - reloadSettings {Object}:  The settings that are required to continue the test after a reload of the document.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    /*override*/ runCommand(order, context) {

        let allKeyboardEvents = order.keyboardevents;
        if (_.isString(allKeyboardEvents)) { allKeyboardEvents = [allKeyboardEvents]; }
        const allEvents = allKeyboardEvents.reduce((array, event) => {
            return array.concat(this.#convertStringToKeyboardEventArray(event));
        }, []);

        if (allEvents.length === 0) { return jpromise.resolve(); }

        return this.repeatDelayed(index => this.#runNextKeyboardCommand(allEvents[index]), {
            delay: context.initialDelay,
            repeatDelay: context.commandDelay,
            cycles: allEvents.length
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Converting an event that is for convenience reasons specified as 'string' to an
     * object, so that it can be handled in the following process.
     *
     * @param {String} oneEventString
     *  The description of the keyboard event as string.
     *
     * @returns {Object}
     *  The description of the keyboard event as object.
     */
    #convertOneStringEvent(oneEventString) {

        let oneEvent;

        if (oneEventString.length === 1) {
            oneEvent = { key: oneEventString };
        } else {
            const allParts = oneEventString.split('+');
            if (allParts.length === 1) {
                oneEvent = { key: oneEventString };
            } else {
                oneEvent = { key: allParts[allParts.length - 1] };
                for (const metaKey of META_KEYS) {
                    if (allParts.find(part => part.toLowerCase() === metaKey)) {
                        oneEvent[metaKey] = true;
                    }
                }
            }
        }

        return oneEvent;
    }

    /**
     * For convenience reasons all keyboard events can be specified in a string
     * and also multiple events in a string separated by whitespaces: '1 2 3'.
     * In this case it is necessary to convert the string definitions into objects
     * and an array of the objects, before the keyboard events can be executed.
     *
     * @param {String|Object} eventDescription
     *  The description of the keyboard events as string or already as an object.
     *
     * @returns {Object[]}
     *  An array with the keyboard events converted to objects.
     */
    #convertStringToKeyboardEventArray(eventDescription) {

        const allEvents = [];

        if (_.isString(eventDescription)) {

            _.each(eventDescription.split(' '), oneKeyboardEventString => {
                // checking for a multiplicator in the string event definition
                const allMultiParts = oneKeyboardEventString.split('*');
                // whether a multiplicator is specified
                const multiplicatorSpecified = allMultiParts.length > 1;
                // the multiplicator for the event
                const multiplicator = multiplicatorSpecified ? allMultiParts[0] : 1;
                // the event string without multiplicator
                const eventString = multiplicatorSpecified ? allMultiParts[1] : oneKeyboardEventString;
                // converting the string to the event object
                const eventObject = this.#convertOneStringEvent(eventString);

                if (multiplicator > 1) {
                    _.times(multiplicator, () => { allEvents.push(_.copy(eventObject)); });
                } else {
                    allEvents.push(eventObject);
                }
            });
        } else {
            allEvents.push(eventDescription); // the event is already the required object
        }

        return allEvents;
    }

    /**
     * Getting the 'default' focus node for each application.
     *
     * @returns {HTMLElement | void} // TODO: check-void-return
     *  The default focus node for the application.
     */
    #getDefaultFocusNode() {
        if (this.docApp.isTextApp()) {
            return this.docModel.getNode()[0];
        } else if (this.docApp.isSpreadsheetApp()) {
            return this.#getActiveClipboardNode()[0];
        } else if (this.docApp.isPresentationApp()) {
            return this.docModel.getSelection().getClipboardNode()[0];
        }
    }

    /**
     * Helper function to get the focused clipboard node in spreadsheet app.
     *
     * @returns {jQuery}
     *  The jQueryfied focused clipboard node
     */
    #getActiveClipboardNode() {
        return this.docView.getContentRootNode().find('.grid-pane.focused').children('.clipboard');
    }

    /**
     * Special handling for Ctrl-C and Ctrl-V events.
     *
     * Info: The returned promises are not evaluated inside this keyboard events
     *       class. Therefore these special copy/paste handling should only be
     *       specified with some timeout following.
     *
     * @param {Object} eventDefinition
     *  The event definition object required to generate the copy/paste function call.
     */
    #handleCopyPasteCommand(eventDefinition) {
        if (COPY_PASTE_KEYS[eventDefinition.key] === 'copy') {
            this.docModel.copy(null, { forceOperationGeneration: true });
        } else if (COPY_PASTE_KEYS[eventDefinition.key] === 'paste') {
            this.docModel.pasteInternalClipboard();
        }
    }

    /**
     * Execution one keyboard command with the specified event definition,
     * that is used to generate a jQuery event.
     *
     * @param {Object} eventDefinition
     *  The event definition object required to generate the jQuery event.
     */
    #executeKeyboardCommand(eventDefinition) {

        let triggerNode = document.activeElement;

        // special handling for for Ctrl-C and Ctrl-V events -> calling model functions directly
        if (eventDefinition.ctrlKey && COPY_PASTE_KEYS[eventDefinition.key]) {
            return this.#handleCopyPasteCommand(eventDefinition);
        }

        if (this.docApp.isSpreadsheetApp() && ($(triggerNode).is('textarea') || $(triggerNode).is('input.form-control'))) {
            const isEnter = eventDefinition.keyCode === 13;
            if (!KeyUpDownOnly[eventDefinition.key] && !isEnter) {
                triggerNode.value += eventDefinition.key; // simulating text input into text area, after its activation or into dialog input field
                return;
            }
        } else if (isCommentTextframeNode(triggerNode)) {
            const $triggerNode = $(triggerNode);
            if (!KeyUpDownOnly[eventDefinition.key]) {
                const $inputNode = $triggerNode.children('div').length > 0 ? $triggerNode.children().last() : $triggerNode;
                if (eventDefinition.key === 'SPACE') { eventDefinition.key = ' '; }
                if (eventDefinition.key === 'ENTER') {
                    $triggerNode.append(document.createElement("div")); // new "div" for the new paragraph
                    return;
                }
                $inputNode.text($inputNode.text() + eventDefinition.key); // simulating text input into comment (TODO, not reliable yet)
                $triggerNode.parent().parent().find(".btn-primary").removeAttr("disabled"); // activating the "send" button
                // cursor to the end
                return;
            }
        }

        if ($(triggerNode).is('body')) { triggerNode = this.#getDefaultFocusNode(); }
        eventDefinition.target = triggerNode;
        // the originalEvent is only required for the handling in core UI after commit 7cc561420e
        eventDefinition.originalEvent = { composedPath: () => [triggerNode] };
        eventDefinition.preventDefault = () => {};
        eventDefinition.stopPropagation = () => {};
        $(triggerNode).trigger($.Event('keydown', eventDefinition));
        delete eventDefinition.originalEvent;
        delete eventDefinition.preventDefault;
        delete eventDefinition.stopPropagation;

        // triggerNode = document.activeElement;
        // eventDefinition.target = triggerNode;
        if (!eventDefinition.ctrl && !KeyUpDownOnly[eventDefinition.key]) {
            $(triggerNode).trigger($.Event('keypress', eventDefinition));
        }

        // triggerNode = document.activeElement;
        // eventDefinition.target = triggerNode;
        $(triggerNode).trigger($.Event('keyup', eventDefinition));
    }

    /**
     * Running one keyboard command with the specified event definition.
     *
     * @param {Object} oneEvent
     *  The description of the keyboard event.
     */
    #runNextKeyboardCommand(oneEvent) {

        if (oneEvent.key) {
            let keyCode = KeyCode[oneEvent.key]; // { key: 'Q', shift: false, ctrl: false, alt: false, target: 'PAGE' }
            if (oneEvent.shift) { oneEvent.shiftKey = true; } // 'shift' and 'shiftKey' must be provided
            if (oneEvent.ctrl) { oneEvent.ctrlKey = true; } // 'ctrl' and 'ctrlKey' must be provided
            if (keyCode === undefined) { keyCode = KeyCode[oneEvent.key.toUpperCase()]; } // allowing to specify 'q' instead of 'Q'
            oneEvent.keyCode = keyCode;
            oneEvent.charCode = keyCode;
            if (!oneEvent.shift && oneEvent.charCode >= 65 && oneEvent.charCode <= 90) { oneEvent.charCode += 32; } // simple helper for small letters
        }

        this.#executeKeyboardCommand(oneEvent);
    }
}
