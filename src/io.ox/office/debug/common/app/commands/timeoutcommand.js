/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';

// constants ==================================================================

// the default timeout time
const DEFAULT_TIME = 1000;

// class TimeoutCommand =======================================================

export class TimeoutCommand extends BaseCommand {

    constructor(docApp) {
        super(docApp, 'timeout');
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for the command 'timeout'.
     *
     * @param {Object} order
     *  The order object specifying a 'timeout' command.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    /*override*/ runCommand(order) {

        let time;

        if (_.isNumber(order.timeout)) {
            time = order.timeout;
        } else if (_.isObject(order.timeout)) {
            time = order.timeout.time || DEFAULT_TIME;
        } else {
            time = DEFAULT_TIME;
        }
        return this.executeDelayed(_.noop, time); // resolving the promise with the specified delay
    }
}
