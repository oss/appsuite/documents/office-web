/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';
import { getSelectionFromDescription } from '@/io.ox/office/debug/common/app/commands/commandutils';

// class OperationsCommand ====================================================

export class OperationsCommand extends BaseCommand {

    constructor(docApp, replayWorker) {
        super(docApp, 'operations');
        this._replayWorker = replayWorker;
    }


    // public methods ---------------------------------------------------------

    /**
     * The command runner for this command.
     *
     * @param {Object} order
     *  The order object specifying an 'operations' command.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when all orders specified in
     *  the command are done.
     */
    /*override*/ runCommand(order) {
        // Info: In this tests it is always better to call the function that generates the operation, not the operation directly.
        this.#setActiveTargetInOperations(order.operations);
        this.#setPositionsInOperations(order.operations);
        return this._replayWorker.start({ ops: order.operations, delay: 10 });
    }

    // private methods --------------------------------------------------------

    /**
     * Replacing the marker 'ACTIVE_TARGET' in the operations.
     *
     * @param {Object[]} ops
     *  The operations container, that might be modified within
     *  this function.
     */
    #setActiveTargetInOperations(ops) {
        for (const op of ops) {
            if (op.target === 'ACTIVE_TARGET') {
                op.target = this.docModel.getActiveTarget();
            }
        }
    }

    /**
     * Setting the start and end positions inside the operation corresponding to the description.
     * The description is evaluated in the function 'getSelectionFromDescription'.
     *
     * Example:
     * start: { description: { selector: '.page > .pagecontent > .slide > .drawing[data-type="table"]', index: 1 } }
     *
     * @param {Object[]} ops
     *  The operations container, that might be modified within
     *  this function.
     */
    #setPositionsInOperations(ops) {
        for (const op of ops) {
            let selectionObject = null;
            if (op.start && op.start.description) { // using a selection description instead of a fixed number array
                selectionObject = getSelectionFromDescription(this.docApp, op.start.description);
                if (selectionObject && selectionObject.start) { op.start = selectionObject.start; }
            }
            if (op.end && op.end.description) { // using a selection description instead of a fixed number array
                selectionObject = getSelectionFromDescription(this.docApp, op.end.description);
                if (selectionObject && selectionObject.start) { op.end = selectionObject.start; }
            }
        }
    }
}
