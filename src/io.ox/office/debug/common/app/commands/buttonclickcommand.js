/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';

import { is } from '@/io.ox/office/tk/algorithms';
import { isDeeplyVisibleNode, isEnabledNode } from '@/io.ox/office/tk/forms';
import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';

// class ButtonClickCommand ===================================================

export class ButtonClickCommand extends BaseCommand {

    constructor(docApp) {
        super(docApp, 'buttonclick');
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for the command 'buttonclick'.
     *
     * @param {Object} order
     *  The order object specifying a 'buttonclick' command.
     *
     * @param {Object} context
     *  The context object with the properties:
     *  - commandDelay {Number}: The duration between the commands.
     *  - initialDelay {Number}: The duration before starting the first command.
     *  - commands {Object}: The commands object as specified in the test definition files.
     *  - reloadSettings {Object}:  The settings that are required to continue the test after a reload of the document.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    /*override*/ runCommand(order, context) {

        return this.executeDelayed(() => {

            if (is.string(order.buttonclick)) {
                this.#convertStringToOrderObject(order);
            }

            const selector = this.#getButtonSelector(order);
            const button = $(selector);
            const groupNode = button.parent();
            const triggerEvent = order.buttonclick.triggerevent || 'click'; // click is the default button trigger event;
            this.#checkButtonEvent(order);
            // check if the button is unique, visible and enabled
            let promise2 = null;
            if (button.length >= 1 && isDeeplyVisibleNode(button) && isEnabledNode(groupNode)) {
                if (order.buttonclick.event) {
                    promise2 = this.docModel.waitForEvent(order.buttonclick.triggerobject, order.buttonclick.event, 15000); // waiting for specified event, rejecting after 15 seconds
                    // the reject after 15 seconds is necessary, so that for example tests with an undo/redo OT Error do not run endlessly.
                } else {
                    promise2 = this.executeDelayed(() => { }, 100); // a litte delay (no event)
                }
                if (button.length === 1) {
                    button.trigger(triggerEvent);
                } else {
                    if (order.buttonclick.index) {
                        button.eq(order.buttonclick.index).trigger(triggerEvent); // for example in a splitbutton
                    } else {
                        button.first().trigger(triggerEvent); // for example in a splitbutton
                    }
                }
            }
            return promise2;
        }, context.commandDelay);
    }

    // private methods --------------------------------------------------------

    /**
     * Helper function to find a selector for a button from a specified order.
     *
     * @param {Object} order
     *  The order object specifying a 'buttonclick' command.
     *
     * @returns {String | Null}
     *  The selector string that can be used to find the button. Or null, if
     *  no selector string could be determined.
     */
    #getButtonSelector(order) {
        let selector = null;
        if (order.buttonclick.selector) {
            selector = order.buttonclick.selector;
        } else if (order.buttonclick.dataKey) {
            selector = '.toolbar:not(".hidden") > [data-section]:not(".hidden") > .group-container:not(".hidden") > .group[data-key="' + order.buttonclick.dataKey + '"]:not(".hidden") > a';
        } else if (order.buttonclick.undo) {
            selector = '.top-pane .group[data-key="document/undo"] > a';
        } else if (order.buttonclick.redo) {
            selector = '.top-pane .group[data-key="document/redo"] > a';
        }
        return selector;
    }

    /**
     * Setting optionally the 'event' and the 'triggerobject' to the specified
     * order object for the 'buttonclick' command.
     *
     * @param {Object} order
     *  The order object specifying a 'buttonclick' command. This might be
     *  modified within this function.
     */
    #checkButtonEvent(order) {
        if (order.buttonclick.event) { // event already defined
            if (!order.buttonclick.triggerobject) { order.buttonclick.triggerobject = this.docModel; } // defaulting to the model
            return;
        }

        if (order.buttonclick.undo) {
            order.buttonclick.event = 'undo:after';
            order.buttonclick.triggerobject = this.docModel.getUndoManager();
        } else if (order.buttonclick.redo) {
            order.buttonclick.event = 'redo:after';
            order.buttonclick.triggerobject = this.docModel.getUndoManager();
        }
    }

    /**
     * Converting a buttonclick order specified as string to the typically used
     * buttonclick order object.
     * This string definition is currently supported for the undo and the redo
     * button, but there might be more buttons in the future.
     *
     * @param {Object} order
     *  The order object specifying a 'buttonclick' command. This is modified
     *  within this function.
     */
    #convertStringToOrderObject(order) {

        const newEventObject = {};
        newEventObject[order.buttonclick] = 1;
        order.buttonclick = newEventObject;
    }
}
