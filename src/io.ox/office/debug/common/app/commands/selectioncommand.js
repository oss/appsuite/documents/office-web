/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';
import { getSelectionFromDescription } from '@/io.ox/office/debug/common/app/commands/commandutils';
import { Address } from '@/io.ox/office/spreadsheet/utils/address';

// class SelectionCommand =====================================================

export class SelectionCommand extends BaseCommand {

    constructor(docApp) {
        super(docApp, 'selection');
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for the command 'selection'.
     *
     * @param {Object} order
     *  The order object specifying a 'selection' command.
     *
     * @param {Object} context
     *  The context object with the properties:
     *  - commandDelay {Number}: The duration between the commands.
     *  - initialDelay {Number}: The duration before starting the first command.
     *  - commands {Object}: The commands object as specified in the test definition files.
     *  - reloadSettings {Object}:  The settings that are required to continue the test after a reload of the document.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    /*override*/ runCommand(order, context) {
        return this.executeDelayed(() => this.#setSelection(order.selection), context.commandDelay);
    }

    // private methods --------------------------------------------------------

    /**
     * Setting a selection according to the specified selection object.
     *
     * @param {Object} selection
     *  The selection object as described in the order.
     */
    #setSelection(selection) {
        if (this.docApp.isSpreadsheetApp()) {
            this.docView.selectionEngine.selectCell(new Address(selection.col, selection.row));
        } else {
            const selectionObject = selection.description ? getSelectionFromDescription(this.docApp, selection.description) : null;
            if (selectionObject) {
                if (selectionObject.start) { selection.start = selectionObject.start; }
                if (selectionObject.end) { selection.end = selectionObject.end; }
            }
            if (!selection.end) { selection.end = _.copy(selection.start); }
            this.docModel.getSelection().setTextSelection(selection.start, selection.end);
        }
    }
}
