/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { ViewObject } from '@/io.ox/office/baseframework/view/viewobject';

// class BaseCommand ==========================================================

export /*abstract*/ class BaseCommand extends ViewObject {

    constructor(docApp, commandName) {

        // base constructor
        super(docApp.docView);

        // properties
        this.name = commandName;
    }

    // public methods ---------------------------------------------------------

    /**
     * The command runner for this command. Must be overwritten in the
     * subclasses.
     *
     * @param {object} order
     *  The order object.
     *
     * @param {object} context
     *  The context object with the properties:
     *  - commandDelay {Number}: The duration between the commands.
     *  - initialDelay {Number}: The duration before starting the first command.
     *  - commands {Object}: The commands object as specified in the test definition files.
     *  - reloadSettings {Object}:  The settings that are required to continue the test after a reload of the document.
     *
     * @returns {MaybeAsync}
     *  Optionally, a promise that will fulfil when all orders specified in
     *  the command are done.
     */
    /*abstract*/ runCommand(/*order, context*/) {
    }
}
