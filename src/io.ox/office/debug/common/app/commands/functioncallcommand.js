/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { BaseCommand } from '@/io.ox/office/debug/common/app/commands/basecommand';

// class FunctionCallCommand ==================================================

export class FunctionCallCommand extends BaseCommand {

    constructor(docApp) {
        super(docApp, 'functioncall');
    }

    // public methods --------------------------------------------------------

    /**
     * The command runner for the command 'functioncall'.
     *
     * @param {Object} order
     *  The order object specifying a 'functioncall' command.
     *
     * @param {Object} context
     *  The context object with the properties:
     *  - commandDelay {Number}: The duration between the commands.
     *  - initialDelay {Number}: The duration before starting the first command.
     *  - commands {Object}: The commands object as specified in the test definition files.
     *  - reloadSettings {Object}:  The settings that are required to continue the test after a reload of the document.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when all orders specified in the command are done.
     */
    /*override*/ runCommand(order, context) {

        return this.executeDelayed(() => {

            let callPromise = null;
            let name = null;
            let object = null;
            let params = null;

            switch (order.functioncall.type) {
                case 'call':
                    callPromise = this.#makeCall(order);
                    break;
                default:
                    name = order.functioncall.functionname;
                    object = this.docModel;
                    params = order.functioncall.params || [];
                    if (order.functioncall.object === 'app') {
                        object = this.docApp;
                    } else if (order.functioncall.object === 'controller') {
                        object = this.docApp.getController();
                    } else if (order.functioncall.object === 'view') {
                        object = this.docView;
                    } else if (order.functioncall.object === 'fieldmanager') {
                        object = this.docModel.getFieldManager();
                    } else if (order.functioncall.object === 'selection') {
                        object = this.docModel.getSelection();
                    }

                    if (name === 'reloadDocument') {
                        params = [{
                            testRunnerSettings: this.#addCommandsToReloadSettings(context.reloadSettings, context.commands, order.functioncall.marker)
                        }];
                    }
                    callPromise = object[name](...params);
            }

            return callPromise ? callPromise : $.when();

        }, context.commandDelay);
    }

    // private methods --------------------------------------------------------

    /**
     * Filtering those commands from the array of commands, that still need to
     * be executed after a reload with the specified marker.
     *
     * @param {Object} commands
     *  The commands object as specified in the test definition files.
     *
     * @param {String} marker
     *  The marker for the reload.
     *
     * @returns {Object}
     *  The remaining commands after the reload.
     */
    #filterMissingOrders(commands, marker) {

        const newCommands = commands; // _.copy(commands, true);
        let foundMarker = false;
        const missingOrders = [];
        const orders = newCommands.orders;

        orders.forEach(order => {
            if (foundMarker) { missingOrders.push(_.copy(order, true)); }
            if (!foundMarker && order.functioncall && order.functioncall.marker && order.functioncall.marker === marker) { foundMarker = true; }
        });

        newCommands.orders = missingOrders;
        return newCommands;
    }

    /**
     * Helper function to add the remaining commands to the object with the
     * reload settings.
     *
     * @param {Object} settings
     *  The object with the reload settings.
     *
     * @param {Object} commands
     *  The commands object as specified in the test definition files.
     *
     * @param {String} marker
     *  The marker for the reload.
     *
     * @returns {Object}
     *  The expanded object with the reload settings.
     */
    #addCommandsToReloadSettings(settings, commands, marker) {
        settings.commands = this.#filterMissingOrders(commands, marker);
        return settings;
    }

    /**
     * Making a direct function call. The type must be specified as 'call'.
     *
     * @param {Object} order
     *  An object describing the function call.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the function call is done.
     */
    #makeCall(order) {

        try {
            const func = order.functioncall.function;

            if (func) {
                return func(this.docApp);
            } else {
                return $.when();
            }
        } catch {
            return $.when();
        }
    }
}
