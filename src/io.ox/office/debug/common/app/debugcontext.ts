/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/objects";

import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import type { EditView } from "@/io.ox/office/editframework/view/editview";
import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

import { OperationsPane } from "@/io.ox/office/debug/common/view/pane/operationspane";
import { ClipboardPane } from "@/io.ox/office/debug/common/view/pane/clipboardpane";
import { DebugToolBar, ReplayToolBar, OTToolBar } from "@/io.ox/office/debug/common/view/toolbars";
import { ReplayWorker } from "@/io.ox/office/debug/common/app/replayworker";
import { OTTestRunner } from "@/io.ox/office/debug/common/app/ottestrunner";
import { OperationTracer } from "@/io.ox/office/debug/common/app/operationtracer";

// class AppDebugContext ======================================================

/**
 * Additional debug data for a Documents application instance.
 */
export class AppDebugContext extends EObject {

    readonly replayWorker: ReplayWorker;
    readonly testRunner: OTTestRunner;
    readonly operationTracer: OperationTracer;
    operationsPane!: OperationsPane;
    clipboardPane!: ClipboardPane;
    debugToolBar!: DebugToolBar;
    rescueOperations?: Operation[];

    // constructor ------------------------------------------------------------

    constructor(docApp: EditApplication) {
        super({ _kind: "root" });

        // create the operations replay worker
        this.replayWorker = this.member(new ReplayWorker(docApp.docModel));
        // create the UI test runner
        this.testRunner = this.member(new OTTestRunner(docApp, this.replayWorker));
        // create the operation tracer to improve debug options
        this.operationTracer = this.member(new OperationTracer(docApp));

        // collect rescue operations triggered after document has been imported
        this.listenTo(docApp, "docs:rescueoperations", rescueOperations => {
            this.rescueOperations = rescueOperations;
        });
    }

    // public methods ---------------------------------------------------------

    initializeView(docView: EditView): void {
        // create and insert the operations pane
        this.operationsPane = docView.addPane(new OperationsPane(docView, this.testRunner, this.operationTracer), { outer: true });
        // create and insert the clipboard pane
        this.clipboardPane = docView.addPane(new ClipboardPane(docView), { outer: true });
    }

    initializeToolPane(docView: EditView): void {
        // create the standard debug toolbar
        this.debugToolBar = docView.insertToolBar(new DebugToolBar(docView));
        // create other toolbars
        docView.insertToolBar(new ReplayToolBar(docView, this));
        docView.insertToolBar(new OTToolBar(docView, this));
    }
}
