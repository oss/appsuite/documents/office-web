/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { Rectangle } from '@/io.ox/office/tk/dom';
import * as Op from '@/io.ox/office/spreadsheet/utils/operations';

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Spreadsheet.
 */
export default {

    TEST_01: {
        description: 'Single client reference test',
        tooltip: 'The reference test for OX Spreadsheet',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { selection: { col: 0, row: 0  } },
                    { operations: [
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { B2: 'abc' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { C2: 'def' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { D2: 'ghi' } }
                    ] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: '1 2 3 4 TAB ENTER ENTER DOWN_ARROW 3 LEFT_ARROW 4 5 6 7 8 9 ESCAPE 4 UP_ARROW 5 RIGHT_ARROW 6 UP_ARROW 7 TAB TAB' },
                    { selection: { col: 5, row: 6  } },
                    { keyboardevents: 'SHIFT+a TAB' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 200 },
                    { keyboardevents: '1 2 TAB 2*ENTER DOWN_ARROW 3 LEFT_ARROW 4 UP_ARROW 5 RIGHT_ARROW 6 UP_ARROW 7 TAB TAB' },
                    { selection: { col: 5, row: 6  } },
                    { keyboardevents: 'SHIFT+b TAB' },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet (second button)
                    { selection: { col: 5, row: 7  } },
                    { keyboardevents: 'a TAB b TAB c TAB' },
                    { buttonclick: { dataKey: 'document/insertsheet' } }, // adding a third sheet (behind the first sheet)
                    { timeout: 200 },
                    { keyboardevents: '1 2 TAB 2*ENTER DOWN_ARROW 3 LEFT_ARROW 4 UP_ARROW 5 RIGHT_ARROW 6 UP_ARROW 7 2*TAB' },
                    { selection: { col: 5, row: 6  } },
                    { keyboardevents: 'SHIFT+c TAB' },
                    { timeout: 200 },
                    { functioncall: { type: 'call', function: docApp => docApp.docModel.setActiveSheet(2) } }, // switching to the last sheet (not recommended way, better use 'buttonclick' command)
                    { timeout: 200 },
                    { selection: { col: 1, row: 6  } },
                    { keyboardevents: [
                        '1 2 TAB 2*ENTER DOWN_ARROW 3 LEFT_ARROW 4 UP_ARROW 5 RIGHT_ARROW 6 UP_ARROW 7 2*TAB',
                        '4*SHIFT+TAB',
                        'SHIFT+SPACE DELETE RIGHT_ARROW', // selecting complete row and deleting content
                        'CTRL+SPACE DELETE LEFT_ARROW', // selecting complete column and deleting content
                        'RIGHT_ARROW', '4*SHIFT+UP_ARROW'
                    ] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'cell/fillcolor', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="green"]' } }, // setting new background color
                    { timeout: 200 },
                    { keyboardevents: 'TAB 1 2 3 TAB 4 5 6 TAB SHIFT+r e l o a d TAB' },
                    { timeout: 200 },
                    { functioncall: { type: 'call', function: docApp => docApp.reloadDocument(), marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: [
                        '1 2 3 4 5 6 RIGHT_ARROW',
                        'RIGHT_ARROW 1 UP_ARROW 2 UP_ARROW 3 UP_ARROW 4 UP_ARROW 5',
                        '4*SHIFT+DOWN_ARROW'
                    ] },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'cell/autoformula' } },
                    { keyboardevents: '2*RIGHT_ARROW 1 2 3 4 UP_ARROW' },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { activatetoolbar: 'colrow' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 100 },
                    { keyboardevents: '2*LEFT_ARROW' },
                    { buttonclick: { dataKey: 'column/delete' } },
                    { timeout: 100 },
                    { keyboardevents: 'DOWN_ARROW 1 DOWN_ARROW 2 DOWN_ARROW 3 DOWN_ARROW 4 DOWN_ARROW 5 2*UP_ARROW' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 100 },
                    { keyboardevents: '2*UP_ARROW' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'row/delete' } },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'document/editable' } }, // start deleting a sheet
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'document/deletesheet' } },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.modal-footer .btn-primary' } }, // end of deleting a sheet
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'document/editable' } }, // start copying a sheet
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'document/copysheet/dialog' } },
                    { timeout: 500 },
                    { keyboardevents: '_ c o p y' },
                    { timeout: 500 },
                    { buttonclick: { selector: '.modal-footer .btn-primary' } }, // end of copying a sheet
                    { timeout: 200 },
                    { activatetoolbar: 'comments' },
                    { timeout: 1000 },
                    { buttonclick: { selector: '.group[data-key="threadedcomment/insert"] > a.button:not(".app-tooltip")', index: 0 } },
                    { timeout: 100 },
                    { keyboardevents: '1 2 3 4 5 6 7 8 9' },
                    { timeout: 100 },
                    { buttonclick: { selector: '.commentEditor button[data-action="send"]' } }
                ]
            }
        },
        result: {}
    },

    TEST_01A: {
        description: 'Debug test for keyboard events',
        tooltip: 'Debug test for keyboard events',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { selection: { col: 0, row: 0  } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: [
                        'DOWN_ARROW RIGHT_ARROW',
                        'a', 'b', 'c', 'TAB', { key: 'd', shift: true }, 'TAB', 'e', 'TAB', { key: 'f', shift: true }, 'TAB',
                        '2*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+g', 'TAB', 'h', 'TAB', 'SHIFT+i', 'TAB', 'j', 'TAB', 'SHIFT+RIGHT_ARROW', 'SHIFT+RIGHT_ARROW', 'SHIFT+DOWN_ARROW', 'SHIFT+k', 'CTRL+SHIFT+ENTER',
                        '3*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+l TAB m TAB SHIFT+n TAB o TAB 8*SHIFT+RIGHT_ARROW 3*SHIFT+DOWN_ARROW SHIFT+p CTRL+SHIFT+ENTER',
                        '5*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+q TAB r TAB SHIFT+s TAB t TAB 8*SHIFT+RIGHT_ARROW 3*SHIFT+DOWN_ARROW SHIFT+u CTRL+SHIFT+ENTER',
                        '5*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+v TAB w TAB SHIFT+x TAB y TAB 8*SHIFT+RIGHT_ARROW 3*SHIFT+DOWN_ARROW SHIFT+z CTRL+SHIFT+ENTER'
                    ] },
                    { keyboardevents: '5*DOWN_ARROW 4*LEFT_ARROW' } // just a string, no array
                ]
            }
        },
        result: {}
    },

    TEST_02: {
        description: 'Single client, handling with new sheets and undo',
        tooltip: 'A single client test that can be used as sparring partner handling with sheets',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { selection: { col: 0, row: 0  } },
                    { operations: [
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A1: 'Running' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A2: 'the' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A3: 'preparation' } }
                        // { name: Operations.INSERT_AUTOSTYLE, styleId: 'a1', attrs: { character: { fontName: '+mn-lt', fontSize: 11, bold: false, italic: false, underline: false, strike: 'none', color: Color.DARK1, apply: { font: false, fill: true, border: false, align: false, number: false, protect: false }, cell: { alignHor: 'auto', alignVert: 'bottom', wrapText: false, formatId: 0, formatCode: 'General', fillType: 'solid', fillColor: Color.RED, foreColor: Color.AUTO, pattern: 'solid', borderLeft: { style: 'none' }, borderRight: { style: 'none' }, borderTop: { style: 'none' }, borderBottom: { style: 'none' }, borderDown: { style: 'none' }, borderUp: { style: 'none' }, unlocked: false, hidden: false }, styleId: 'Standard' } },
                        // { name: Operations.CHANGE_CELLS, sheet: 0, contents: { A1: { s: 'a1' } } },
                        // { name: Operations.CHANGE_CELLS, sheet: 0, contents: { A2: { s: 'a1' } } },
                        // { name: Operations.CHANGE_CELLS, sheet: 0, contents: { A3: { s: 'a1' } } }
                    ] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet (second button)
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet (second button)
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet (second button)
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } } // switching to the first sheet
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_03: {
        description: 'Two clients, handling with new sheets and undo/redo',
        tooltip: 'A multi client test handling with inserting and deleting many sheets',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { selection: { col: 0, row: 0  } },
                    { operations: [
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A1: 'Running' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A2: 'the' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A3: 'preparation' } }
                    ] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', '6', 'ENTER', '7', 'ENTER', '8', 'ENTER', '9', 'ENTER', '0'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } } // switching to the first sheet
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'a', 'TAB', 'b', 'TAB', 'c', 'TAB', 'd', 'TAB', 'e', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', 'f', 'ENTER', 'g', 'ENTER', 'h', 'ENTER', 'i', 'ENTER', 'j'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'undo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 300 },
                    { buttonclick: 'redo' },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', 'a', 'TAB', 'b', 'TAB', 'c', 'TAB', 'd', 'TAB', 'e', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', 'f', 'ENTER', 'g', 'ENTER', 'h', 'ENTER', 'i', 'ENTER', 'j'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'a', 'TAB', 'b', 'TAB', 'c', 'TAB', 'd', 'TAB', 'e', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', 'f', 'ENTER', 'g', 'ENTER', 'h', 'ENTER', 'i', 'ENTER', 'j'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'a', 'TAB', 'b', 'TAB', 'c', 'TAB', 'd', 'TAB', 'e', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', 'f', 'ENTER', 'g', 'ENTER', 'h', 'ENTER', 'i', 'ENTER', 'j'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="document/insertsheet"] > a' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: 'redo' },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { keyboardevents: ['TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'TAB', 'TAB', '1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB'] },
                    { timeout: 200 },
                    { keyboardevents: ['ENTER', 'f', 'ENTER', 'g', 'ENTER', 'h', 'ENTER', 'i', 'ENTER', 'j'] },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['TAB'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'cell/fillcolor' } },
                    { timeout: 200 },
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } } // switching to the first sheet
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_04: {
        description: 'Single client, handling with inserting and deleting a column',
        tooltip: 'A single client test that can be used as sparring partner handling with columns',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { selection: { col: 0, row: 0  } },
                    { operations: [
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A1: 'A1' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { B1: 'B1' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { C1: 'C1' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { D1: 'D1' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { E1: 'E1' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { F1: 'F1' } }
                    ] },
                    { keyboardevents: ['TAB', 'TAB', 'TAB'] }
                    // { timeout: 200 },
                    // { buttonclick: { selector: '.header-pane[data-orientation="columns"].focused .cell[data-index="4"]', index: 0, triggerevent: 'mousedown' } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { activatetoolbar: 'colrow' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'column/insert' } },
                    { timeout: 500 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_05: {
        description: 'Single client, handling with inserting and deleting a row',
        tooltip: 'A single client test that can be used as sparring partner handling with rows',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { selection: { col: 0, row: 0  } },
                    { operations: [
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A1: 'A1' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A2: 'A2' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A3: 'A3' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A4: 'A4' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A5: 'A5' } },
                        { name: Op.CHANGE_CELLS, sheet: 0, contents: { A6: 'A6' } }
                    ] },
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER'] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { activatetoolbar: 'colrow' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 500 }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_06: {
        description: 'inserting text into an existing shape',
        tooltip: 'A single client that inserts permanently text into an existing shape',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { type: 'call', function: docApp => docApp.docController.execInsertShape('rect', new Rectangle(300, 100, 200, 250)) } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'TAB'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_07: {
        description: 'inserting text into an existing shape and splitting it into several paragraphs',
        tooltip: 'A single client that inserts permanently text into an existing shape and then generates many splitParagraph operations',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { functioncall: { type: 'call', function: docApp => docApp.docController.execInsertShape('rect', new Rectangle(300, 100, 200, 250)) } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_08: {
        description: 'Two clients, generating colliding operations with formulas',
        tooltip: 'A two client test handling with conflicting formulas and insertRow/deleteRow',
        clients: 2,
        autotest: false,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 80
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'document/insertsheet' } }, // inserting a fresh sheet for the test
                    { keyboardevents: ['DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW'] }, // 5th row
                    { keyboardevents: ['1', 'TAB', '2', 'TAB', '3', 'TAB', '4', 'TAB', '5', 'TAB', '6', 'TAB', '7', 'TAB', '8', 'TAB', '9', 'TAB', '1', '0', 'TAB', '1', '1', 'TAB', '1', '2', 'TAB'] },
                    { keyboardevents: ['LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { keyboardevents: ['DOWN_ARROW', 'DOWN_ARROW'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 60
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 1 } }, // switching to second sheet
                    { timeout: 200 },
                    { keyboardevents: ['a', 'DOWN_ARROW', 'b', 'DOWN_ARROW', 'c', 'DOWN_ARROW', 'd', 'UP_ARROW', 'UP_ARROW'] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'a', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'b', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'c', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'd', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'e', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'f', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'g', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'h', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'i', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'j', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'k', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'l', '5', '+', '5', 'TAB'] },
                    { keyboardevents: ['DOWN_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'a', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'b', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'c', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'd', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'e', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'f', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'g', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'h', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'i', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'j', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'k', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'l', '5', '+', '6', 'TAB'] },
                    { keyboardevents: ['DOWN_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'a', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'b', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'c', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'd', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'e', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'f', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'g', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'h', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'i', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'j', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'k', '5', '+', '7', 'TAB'] },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', 'l', '5', '+', '7', 'TAB'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { activatetoolbar: 'colrow' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_09: {
        description: 'Two clients, generating colliding operations with matrices',
        tooltip: 'A two client test handling with conflicting matrices and insertRow/deleteRow',
        clients: 2,
        autotest: false, // because of colliding operations
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 80
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'document/insertsheet' } }, // inserting a fresh sheet for the test
                    { keyboardevents: ['DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 60
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 1 } }, // switching to second sheet
                    { timeout: 200 },
                    { keyboardevents: ['a', 'DOWN_ARROW', 'b', 'DOWN_ARROW', 'c', 'DOWN_ARROW', 'd', 'UP_ARROW', 'UP_ARROW'] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 80
                },
                orders: [
                    { keyboardevents: [
                        'RIGHT_ARROW', { key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true },
                        'MOZ_EQUAL_SIGN', '1', { key: 'ENTER', ctrl: true, shift: true },
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'RIGHT_ARROW', { key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true },
                        'MOZ_EQUAL_SIGN', '2', { key: 'ENTER', ctrl: true, shift: true },
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'RIGHT_ARROW', { key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true },
                        'MOZ_EQUAL_SIGN', '3', { key: 'ENTER', ctrl: true, shift: true },
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'RIGHT_ARROW', { key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true },
                        'MOZ_EQUAL_SIGN', '4', { key: 'ENTER', ctrl: true, shift: true },
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'RIGHT_ARROW', { key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true },
                        'MOZ_EQUAL_SIGN', '5', { key: 'ENTER', ctrl: true, shift: true },
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW',
                        'RIGHT_ARROW', { key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true }, { key: 'DOWN_ARROW', shift: true },
                        'MOZ_EQUAL_SIGN', '6', { key: 'ENTER', ctrl: true, shift: true },
                        'DOWN_ARROW', 'DOWN_ARROW', 'DOWN_ARROW'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 50
                },
                orders: [
                    { activatetoolbar: 'colrow' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'row/insert' } }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    TEST_10: {
        description: 'Single client test for inserting, resizing, moving, adjusting and rotating shapes',
        tooltip: 'Single client test for inserting, resizing, moving, adjusting and rotating shapes',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'selectionBox', definition: { selector: '.grid-pane.focused .grid-layer-root', startX: 0.2, startY: 0.3, stepX: 0.02, stepY: 0.02, stepCount: 10 } } },
                    { timeout: 100 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e'] },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0, startY: 0, stepX: 0.02, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0, startY: 0, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'move', definition: { selector: '.pagecontent > .sheet > .drawing.selected > .textframecontent', startX: 0, startY: 0, stepX: 0.02, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'move', definition: { selector: '.pagecontent > .sheet > .drawing.selected > .textframecontent', startX: 0, startY: 0, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0, startY: 0, stepX: 0.02, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0, startY: 0, stepX: 0.05, stepY: 0.02, stepCount: 10 } } },
                    { timeout: 500 },
                    { buttonclick: 'undo' },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { selector: '.grid-pane.focused .grid-layer-root', startX: 0.18, startY: 0.28, stepX: 0.02, stepY: 0.02, stepCount: 14 } } },
                    { timeout: 200 },
                    { activatetoolbar: 'format' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'cell/fillcolor', index: 1 } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="yellow"]' } }, // setting new background color
                    { timeout: 200 },
                    { keyboardevents: ['MOZ_EQUAL_SIGN', '1', { key: 'ENTER', ctrl: true, shift: true }] } // setting matrix
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_11: {
        description: 'Two clients modifying a shape and inserting content into the shape',
        tooltip: 'A two clients test for modifying a shape and inserting content into the shape',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'document/insertsheet' } }, // inserting a fresh sheet for the test
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'selectionBox', definition: { selector: '.grid-pane.focused .grid-layer-root', startX: 0.2, startY: 0.3, stepX: 0.02, stepY: 0.02, stepCount: 10 } } },
                    { timeout: 100 }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 60
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 1 } }, // switching to second sheet
                    { timeout: 200 },
                    { functioncall: { type: 'call', function: docApp => docApp.docView.selectionEngine.selectDrawing([0], { toggle: false }) } } // selecting the first drawing
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0, startY: 0, stepX: 0.02, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0, startY: 0, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'move', definition: { selector: '.pagecontent > .sheet > .drawing.selected > .textframecontent', startX: 0, startY: 0, stepX: 0.02, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'move', definition: { selector: '.pagecontent > .sheet > .drawing.selected > .textframecontent', startX: 0, startY: 0, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0, startY: 0, stepX: 0.02, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0, startY: 0, stepX: 0.05, stepY: 0.02, stepCount: 10 } } },
                    { timeout: 500 },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_12: {
        description: 'Two clients modifying a shape and inserting/deleting a sheet',
        tooltip: 'A two clients test for modifying a shape and inserting and deleting a previous sheet',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'document/insertsheet' } }, // inserting a fresh sheet for the test
                    { activatetoolbar: 'insert' },
                    { timeout: 100 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 100 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 100 },
                    { triggerevent: { type: 'selectionBox', definition: { selector: '.grid-pane.focused .grid-layer-root', startX: 0.2, startY: 0.3, stepX: 0.02, stepY: 0.02, stepCount: 10 } } },
                    { timeout: 100 }
                ]
            },
            CLIENT_2: {
                timers: {
                    commandDelay: 60
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 1 } }, // switching to second sheet
                    { timeout: 200 },
                    { functioncall: { type: 'call', function: docApp => docApp.docView.selectionEngine.selectDrawing([0], { toggle: false }) } } // selecting the first drawing
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 50
                },
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' },
                    { timeout: 400 },
                    { buttonclick: { dataKey: 'document/insertsheet' } },
                    { timeout: 400 },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    }
};
