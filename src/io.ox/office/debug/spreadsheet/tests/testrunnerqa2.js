/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import * as Op from '@/io.ox/office/spreadsheet/utils/operations';

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Spreadsheet (QA2).
 */
export default {

    TEST_0001_QA2: {
        description: 'Debug test for keyboard events',
        tooltip: 'Debug test for keyboard events',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { buttonclick: { selector: '.group[data-key="view/sheet/active"] > a.option-button', index: 0 } }, // switching to the first sheet
                    { operations: [{ name: Op.CHANGE_CELLS, sheet: 0, contents: { B2: '123' } }] },
                    { selection: { col: 0, row: 0  } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: [
                        'DOWN_ARROW RIGHT_ARROW',
                        'a', 'b', 'c', 'TAB', { key: 'd', shift: true }, 'TAB', 'e', 'TAB', { key: 'f', shift: true }, 'TAB',
                        '2*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+g', 'TAB', 'h', 'TAB', 'SHIFT+i', 'TAB', 'j', 'TAB', 'SHIFT+RIGHT_ARROW', 'SHIFT+RIGHT_ARROW', 'SHIFT+DOWN_ARROW', 'SHIFT+k', 'CTRL+SHIFT+ENTER',
                        '3*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+l TAB m TAB SHIFT+n TAB o TAB 8*SHIFT+RIGHT_ARROW 3*SHIFT+DOWN_ARROW SHIFT+p CTRL+SHIFT+ENTER',
                        '5*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+q TAB r TAB SHIFT+s TAB t TAB 8*SHIFT+RIGHT_ARROW 3*SHIFT+DOWN_ARROW SHIFT+u CTRL+SHIFT+ENTER',
                        '5*DOWN_ARROW 4*LEFT_ARROW',
                        'SHIFT+v TAB w TAB SHIFT+x TAB y TAB 8*SHIFT+RIGHT_ARROW 3*SHIFT+DOWN_ARROW SHIFT+z CTRL+SHIFT+ENTER'
                    ] },
                    { keyboardevents: '5*DOWN_ARROW 4*LEFT_ARROW' } // just a string, no array
                ]
            }
        },
        result: {}
    }
};
