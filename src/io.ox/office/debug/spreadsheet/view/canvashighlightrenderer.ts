/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PointList, Canvas2DContext } from "@/io.ox/office/tk/canvas";
import { getDebugFlag } from "@/io.ox/office/tk/config";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { renderLogger, getCanvasSize, getCanvasCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// private functions ==========================================================

function getDebugHighlightState(): boolean {
    return getDebugFlag("spreadsheet:highlight-formulas");
}

// class CanvasHighlightRenderer ==============================================

/**
 * A cell canvas renderer for highlight markers in formula cells.
 */
export class CanvasHighlightRenderer extends CanvasBaseRenderer {

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasHighlightRenderer} render", { when: getDebugHighlightState })
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        if (!getDebugHighlightState()) { return; }

        const { cellCollection, dvRuleCollection } = cache.sheetModel;

        const c2 = getCanvasCoord(2);
        const c3 = getCanvasCoord(3);
        const c8 = getCanvasCoord(8);
        const s15 = getCanvasSize(1.5);

        interface MarkerOptions {
            bottom?: boolean;
            label?: string;
            boundary?: Range;
        }

        const drawMarker = (anchor: Address, color: string, options?: MarkerOptions): void => {
            const rect = cache.getCanvasRectForCell(anchor);
            const l = rect.left, t = rect.top, b = rect.bottom();
            const bottom = options?.bottom;
            const marker: PointList = bottom ? [[l, b], [l + c8, b], [l, b - c8]] : [[l, t], [l + c8, t], [l, t + c8]];
            context.setFillStyle(color).drawPolygon(marker, "fill");
            if (options?.label) {
                context.drawText(String(options.label), l + c3, bottom ? (b - c8 - c3) : (t + c3), "fill");
            }
            if (options?.boundary) {
                if (!options.boundary.single()) {
                    context.setFillStyle("white").drawCircle(l + c2 + 0.5, bottom ? (b - c3 - 0.5) : (t + c2 + 0.5), s15, "fill");
                }
                context.setLineStyle({ style: color, width: 1, pattern: 1, patternOffset: 0.5 });
                context.drawRect(cache.getCanvasRectForRange(options.boundary).translateSelf(-0.5, -0.5), "stroke");
            }
        };

        // set font style for marker labels
        context.setFontStyle({ font: `${c8}pt monospace`, align: "left", baseline: "top" });

        // process all formula cells in the rendering cache
        for (const { cellModel } of cache.yieldCellModels(settings.renderRange, { origins: true })) {

            if (!cellModel) { continue; }

            // visualize shared formula cells
            if (cellModel.sf) {
                const boundary = cellModel.isSharedAnchor() ? cellModel.sf.boundRange : undefined;
                drawMarker(cellModel.a, "#080", { label: String(cellModel.sf.index), boundary });
                continue;
            }

            // visualize normal formula cells
            if (cellModel.f && !cellModel.sf && !cellModel.mr) {
                drawMarker(cellModel.a, "#00a", { boundary: new Range(cellModel.a) });
                continue;
            }

            // resolve token descriptor for all cells covered by a matrix formula
            const tokenDesc = cellCollection.getTokenArray(cellModel.a);
            if (tokenDesc?.matrixRange) {
                const style = tokenDesc.dynamicMatrix ? "#840" : "#a00";
                drawMarker(tokenDesc.matrixRange.a1, style, { label: tokenDesc.matrixRange.toOpStr(), boundary: tokenDesc.matrixRange });
            }
        }

        // process all data validation rules in the rendered range
        for (const ruleModel of dvRuleCollection.yieldRuleModels(settings.renderRange)) {
            for (const boundary of ruleModel.getTargetRanges()) {
                drawMarker(boundary.a1, "#a0a", { bottom: true, label: "dv", boundary });
            }
        }
    }
}
