/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { dict, json, debug } from "@/io.ox/office/tk/algorithms";
import { createSpan, getInputSelection } from "@/io.ox/office/tk/dom";
import { IntervalRunner } from "@/io.ox/office/tk/workers";

import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { ParsedSection } from "@/io.ox/office/editframework/model/formatter/parsedsection";

import type { SelectionDirection } from "@/io.ox/office/textframework/selection/selection";

import { CellAnchorType, CellAnchor } from "@/io.ox/office/spreadsheet/utils/cellanchor";
import type { CellAttributes } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import type { ColRowDescriptor } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

import type { InspectorContext, InspectorTableRowArg } from "@/io.ox/office/debug/common/view/objectinspector";
import type { OperationsPane } from "@/io.ox/office/debug/common/view/pane/operationspane";

// private functions ==========================================================

function resolvePos(lengths: number[], pos: number): Position {
    let para = 0;
    for (; lengths[para] < pos; para += 1) {
        pos -= (lengths[para] + 1);
    }
    return [para, pos];
}

function formatCodeAttrMatcher(context: InspectorContext): boolean {
    return context.isStringEntry(0, "formatCode") && context.isDictEntry(1, "cell");
}

function drawingAnchorAttrMatcher(context: InspectorContext): boolean {
    return context.isStringEntry(0, "anchor") && context.isDictEntry(1, "drawing");
}

function changeCellsEntryMatcher(context: InspectorContext): boolean {
    return (context.chain.length === 2) && context.isDictEntry(1, "contents");
}

function dateGroupEntryMatcher(context: InspectorContext): boolean {
    return context.isArrayEntry(1, "entries") && context.isDictEntry(2, "filter") && context.isDictEntry(3, "attrs");
}

function colorScaleColorMatcher(context: InspectorContext): boolean {
    return context.isDictEntry(0, "c") && context.isArrayEntry(2, "colorScale");
}

function dataBarColorMatcher(context: InspectorContext): boolean {
    return context.isDictEntry(0, /^(n?b?c|ac)$/) && context.isDictEntry(1, "dataBar");
}

// public functions ===========================================================

/**
 * Registers all event handling and logging for the debug operations pane.
 */
export function initializeOperationsPane(docView: SpreadsheetView, operationsPane: OperationsPane): void {

    // shortcuts to properties
    const { docModel } = docView;
    const { inspector } = operationsPane;

    // logs information about the active sheet (index, name, used area)
    function logSheetInfo(): void {
        const sheetModel: Opt<SheetModel> = docView.sheetModel;
        const sheetIndex = docView.activeSheet;
        const sheetName = sheetModel?.getName() || "";
        const usedRange = sheetModel?.getUsedRange() ?? "<empty>";
        operationsPane.setDebugInfo("selection", "sheet", `index=${sheetIndex}`, `name="${sheetName}"`, `used=${usedRange}`);
    }

    // displays the passed text selection state
    function logTextSelection(): void;
    function logTextSelection(start: Position, end: Position, dir: SelectionDirection): void;
    // implementation
    function logTextSelection(start?: Position, end?: Position, dir?: SelectionDirection): void {
        operationsPane.setDebugInfo("text", "start", start?.join(", ") || "");
        operationsPane.setDebugInfo("text", "end", end?.join(", ") || "");
        operationsPane.setDebugInfo("text", "dir", dir || "");
    }

    // create the output nodes in the operations pane
    operationsPane.addDebugInfoHeader("selection", "Sheet Selection");
    operationsPane.addDebugInfoNode("selection", "sheet", "Active (visible) sheet");
    operationsPane.addDebugInfoNode("selection", "ranges", "Selected cell ranges");
    operationsPane.addDebugInfoNode("selection", "border", "Mixed border attributes");
    operationsPane.addDebugInfoNode("selection", "draw", "Selected drawing objects");
    operationsPane.addDebugInfoNode("selection", "pane", "Active (focused) grid pane");
    operationsPane.addDebugInfoNode("selection", "auto", "Auto-fill range");

    operationsPane.addDebugInfoHeader("active", "Active Cell", "Formatting of the active cell");
    operationsPane.addDebugInfoNode("active", "col", "Explicit attributes of column containing active cell");
    operationsPane.addDebugInfoNode("active", "row", "Explicit attributes of row containing active cell");
    operationsPane.addDebugInfoNode("active", "cell", "Explicit attributes of active cell");
    operationsPane.addDebugInfoNode("active", "url", "Hyperlink URL of active cell");

    operationsPane.addDebugInfoHeader("text", "Text Selection");
    operationsPane.addDebugInfoNode("text", "edit", "Text edit mode identifier");
    operationsPane.addDebugInfoNode("text", "start", "Start position of text selection");
    operationsPane.addDebugInfoNode("text", "end", "End position of text selection");
    operationsPane.addDebugInfoNode("text", "dir", "Direction");

    // active sheet (index, name, used area)
    operationsPane.listenTo(docView, ["change:activesheet", "change:usedarea"], logSheetInfo);
    operationsPane.listenTo(docModel, ["transform:sheets", "rename:sheet"], logSheetInfo);

    // log all selection events
    operationsPane.listenTo(docView, "change:sheetprops", event => {
        const { selection, activePane } = event.newProps;
        if (selection) {
            const { ranges, active, address, origin, drawings } = selection;
            operationsPane.setDebugInfo("selection", "ranges", `count=${ranges.length}`, `ranges=${ranges}`, `active=${active}`, `cell=${address}`, origin ? `origin=${origin}` : "");
            operationsPane.setDebugInfo("selection", "draw", `count=${drawings.length}`, `frames=${drawings.length ? `[${drawings.map(pos => json.safeStringify(pos)).join()}]` : "<none>"}`);
        }
        if (activePane) {
            operationsPane.setDebugInfo("selection", "pane", activePane);
        }
    });

    // log auto-fill state
    operationsPane.listenToProp(docView.propSet, "autoFillData", autoFillData => {
        if (autoFillData) {
            operationsPane.setDebugInfo("selection", "auto", `dir=${autoFillData.direction}`, `count=${autoFillData.count}`);
        } else {
            operationsPane.setDebugInfo("selection", "auto", "<none>");
        }
    });

    // log formatting of the active cell, and the entire selection
    operationsPane.listenTo(docView, "update:selection:data", () => {

        function renderAutoStyle(styleId: string): HTMLAnchorElement {
            const attrSet = docModel.autoStyles.getMergedAttributeSet(styleId);
            return inspector.createAnchor(attrSet, { label: `style=${styleId || "<default>"}` });
        }

        function renderColRowAttrs<KT extends "column" | "row">(desc: ColRowDescriptor<KT>, family: KT): HTMLAnchorElement {
            const attrSet = { [family]: desc.merged };
            return inspector.createAnchor(attrSet, { label: `attrs=${debug.stringify(desc.explicit)}` });
        }

        const activeCell = docView.selectionEngine.getActiveCell();
        const { colCollection, rowCollection, cellCollection } = docView;
        const colDesc = colCollection.getEntry(activeCell.c);
        const rowDesc = rowCollection.getEntry(activeCell.r);
        const cellModel = cellCollection.getCellModel(activeCell);

        operationsPane.setDebugInfo("selection", "border", inspector.createAnchor({ cell: docView.getBorderAttributes() }, { label: "mixed-borders" }));
        operationsPane.setDebugInfo("active", "col", renderAutoStyle(colDesc.style), renderColRowAttrs(colDesc, "column"));
        operationsPane.setDebugInfo("active", "row", renderAutoStyle(rowDesc.style), renderColRowAttrs(rowDesc, "row"));
        operationsPane.setDebugInfo("active", "cell", `exists=${!!cellModel}`, renderAutoStyle(cellCollection.getStyleId(activeCell)));
        operationsPane.setDebugInfo("active", "url", cellCollection.getEffectiveURL(activeCell) || "<none>");
    });

    // log settings for text edit mode
    operationsPane.listenTo(docView, "textedit:enter", editMode => {
        operationsPane.setDebugInfo("text", "edit", editMode);
    });
    operationsPane.listenTo(docView, "textedit:leave", () => {
        operationsPane.setDebugInfo("text", "edit", "");
    });

    // the interval runner that logs the text selection for cell edit mode
    const textSelectionLogger = new IntervalRunner(() => {
        const textArea = docView.$appContentNode.find("textarea:focus")[0] as Opt<HTMLTextAreaElement>;
        if (textArea) {
            const paraLengths = textArea.value.split(/\n/).map(txt => txt.length);
            const { start, end } = getInputSelection(textArea);
            const dir = (start === end) ? "cursor" : (start < end) ? "forwards" : "backwards";
            logTextSelection(resolvePos(paraLengths, start), resolvePos(paraLengths, end), dir);
        } else {
            logTextSelection();
        }
    }, { interval: 200, _kind: "private" });

    // track the text selection during drawing edit mode
    operationsPane.listenTo(docView, "textedit:enter", editMode => {
        if (editMode !== "cell") { return; }
        textSelectionLogger.start();
        operationsPane.listenTo(docView, "textedit:leave", () => {
            textSelectionLogger.abort();
            logTextSelection();
        }, { once: true });
    });

    // the text selection engine (from text framework)
    const textSelection = docModel.getSelection();
    operationsPane.listenTo(textSelection, "change", () => {
        logTextSelection(textSelection.getStartPosition(), textSelection.getEndPosition(), textSelection.getDirection());
    });

    // hide text selection settings when switching to cell selection
    operationsPane.listenTo(docView, "change:selection", selection => {
        if (selection.drawings.length === 0) { logTextSelection(); }
    });

    // register an attribute formatter for number formats
    inspector.registerFormatter("formatCode", (context, simple, complex) => {

        const cellAttrs = context.chain[1]!.value as Partial<CellAttributes>;
        const parsedFormat = docModel.numberFormatter.getParsedFormatForAttributes(cellAttrs);
        simple.appendChild(createSpan({ label: `\xa0(category:\xa0${parsedFormat.category})` }));

        function createSectionCells(label: string, section: ParsedSection): InspectorTableRowArg {
            const color = section.color ? [inspector.createColorBox(section.color.css, "text"), section.color.key] : "";
            const range = section.range ? `${section.range.op}${section.range.arg}` : "";
            return [label, section.category, color, range];
        }

        const tableCells = parsedFormat.numberSections.map((section, i) => createSectionCells(`Num${i + 1}`, section));
        if (parsedFormat.textSection) { tableCells.push(createSectionCells("Text", parsedFormat.textSection)); }

        complex.appendChild(inspector.createTable(["Section", "Category", "Color", "Range"], tableCells));

        return "NumberFormat";
    });
    inspector.addFormatMatcher("formatCode", formatCodeAttrMatcher);

    // register an attribute formatter for drawing anchor strings
    inspector.registerFormatter("drawingAnchor", (context, _simple, complex) => {
        const cellAnchor = CellAnchor.parseOpStr(context.chain[0]!.value as string);
        const typeName = dict.keyOf(CellAnchorType, cellAnchor.type);
        complex.appendChild(inspector.createTable(null, [
            ["type",   typeName || "invalid"],
            ["point1", cellAnchor.point1.toString()],
            ["point2", cellAnchor.point2.toString()]
        ]));
        return "CellAnchor";
    });
    inspector.addFormatMatcher("drawingAnchor", drawingAnchorAttrMatcher);
    inspector.addCollapseMatcher(drawingAnchorAttrMatcher);

    // collapse cell entries in "changeCells" operations
    // collapse date group entries in table filter operations
    inspector.addCollapseMatcher(changeCellsEntryMatcher, dateGroupEntryMatcher);

    // collapse colors in conditional formatting rules
    inspector.addFormatMatcher("color", colorScaleColorMatcher, dataBarColorMatcher);

    // destroy instances
    docView.docApp.on("docs:beforequit", () => textSelectionLogger.destroy());
}
