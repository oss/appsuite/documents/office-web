/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { getDebugFlag } from "@/io.ox/office/tk/config";
import { Button } from "@/io.ox/office/tk/controls";

import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";
import type SpreadsheetApp from "@/io.ox/office/spreadsheet/app/application";

import type { AppDebugContext } from "@/io.ox/office/debug/common/app/debugcontext";

import { initializeOperationsPane } from "@/io.ox/office/debug/spreadsheet/view/operationspane";
import { CanvasHighlightRenderer } from "@/io.ox/office/debug/spreadsheet/view/canvashighlightrenderer";

import testDefinitions from "@/io.ox/office/debug/spreadsheet/tests/testrunner";
import testDefinitionsqa from "@/io.ox/office/debug/spreadsheet/tests/testrunnerqa";
import testDefinitionsqa2 from "@/io.ox/office/debug/spreadsheet/tests/testrunnerqa2";

import "@/io.ox/office/debug/spreadsheet/view/debugstyles.less";

// private functions ==========================================================

function getDebugHighlightState(): boolean {
    return getDebugFlag("spreadsheet:highlight-formulas");
}

/**
 * Repaints all cell contents in all grid panes.
 */
function invalidateCellLayers(docView: SpreadsheetView): void {
    for (const gridPane of docView.getGridPanes()) {
        gridPane.cellRenderer.invalidateLayerRange();
    }
}

// public functions ===========================================================

/**
 * Entry point for debug initialization of a spreadsheet application.
 *
 * @param _docApp
 *  The application instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeApp(_docApp: SpreadsheetApp, debugContext: AppDebugContext): void {

    // register definitions for spreadsheet UI tests
    debugContext.testRunner.registerDefinitions(testDefinitions);
    debugContext.testRunner.registerDefinitions(testDefinitionsqa);
    debugContext.testRunner.registerDefinitions(testDefinitionsqa2);
}

/**
 * Entry point for debug initialization of a spreadsheet view.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeView(docView: SpreadsheetView, debugContext: AppDebugContext): void {

    // initialize the operations pane
    initializeOperationsPane(docView, debugContext.operationsPane);

    // register the highlight renderer for formula cells
    docView.registerCanvasRenderer(new CanvasHighlightRenderer(docView));

    // update formula cell highlighting when changing the configuration
    let debugHighlightState = getDebugHighlightState();
    docView.listenToGlobal("change:config:debug", () => {
        const newHighlightState = getDebugHighlightState();
        if (debugHighlightState !== newHighlightState) {
            debugHighlightState = newHighlightState;
            invalidateCellLayers(docView);
        }
    });
}

/**
 * Entry point for initialization of the debug toolbar in the spreadsheet
 * view.
 *
 * @param _docView
 *  The view instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeToolPane(_docView: SpreadsheetView, debugContext: AppDebugContext): void {

    const { debugToolBar } = debugContext;

    debugToolBar.optionsPicker.addSection("spreadsheet", _.noI18n("Spreadsheet settings"));
    debugToolBar.optionsPicker.registerOption("spreadsheet:failing-import", "Simulates a failure in the next imported spreadsheet document, leaving the document model without any sheets.\nAfter the simulated failure, this option will be disabled automatically.");
    debugToolBar.optionsPicker.registerOption("spreadsheet:debug-formulas", "Disables timeout detection in the formula interpreter for better debugging.");
    debugToolBar.optionsPicker.registerOption("spreadsheet:highlight-formulas", "Renders highlight markers into all formula cells (normal formulas, matrix formulas, and shared formulas).");
    debugToolBar.optionsPicker.registerOption("spreadsheet:slow-dependencies", "Slows down the background worker of the formula dependency manager.");

    debugToolBar.loggersPicker.addSection("spreadsheet", _.noI18n("Spreadsheet loggers"));
    debugToolBar.loggersPicker.registerLogger("spreadsheet:log-models", "Logger of the document model.");
    debugToolBar.loggersPicker.registerLogger("spreadsheet:log-formulas", "Logger of the formula engine (parser, compiler, interpreter).");
    debugToolBar.loggersPicker.registerLogger("spreadsheet:log-deps", "Logger of the dependency manager.");
    debugToolBar.loggersPicker.registerLogger("spreadsheet:log-renderer", "Logger of the sheet rendering engine.");
    debugToolBar.loggersPicker.registerLogger("spreadsheet:log-editor", "Logger for the text editors (cells and drawing objects).");

    debugToolBar.addSection("formulas");
    debugToolBar.addControl("document/formula/rcstyle",    new Button({ icon: "bi:link", label: _.noI18n("R1C1"), tooltip: _.noI18n("Show cell references in formulas in A1 or R1C1 style"), toggle: true }));
    debugToolBar.addControl("document/formula/recalc/all", new Button({ icon: "bi:arrow-repeat", label: _.noI18n("Recalculate all"), tooltip: _.noI18n("Recalculate all formulas in the document") }));
}
