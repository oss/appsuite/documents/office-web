/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { type ResourceConfig, EditResourceManager } from "@/io.ox/office/editframework/resource/resourcemanager";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type SpreadsheetApp from "@/io.ox/office/spreadsheet/app/application";

// constants ==================================================================

const RESOURCE_MAP = {
    cellstylenames: { module: "spreadsheet", name: "cellstylenames", merge: true }
} as const satisfies Dict<ResourceConfig>;

// class SpreadsheetResourceManager ===========================================

export class SpreadsheetResourceManager extends EditResourceManager<SpreadsheetApp, typeof RESOURCE_MAP> {

    /**
     * The factory for spreadsheet formula resources providing the resources
     * for different file formats and locales.
     */
    readonly formulaResourceFactory!: FormulaResourceFactory;

    // constructor ------------------------------------------------------------

    constructor(docApp: SpreadsheetApp) {
        super(docApp, RESOURCE_MAP);
    }

    // public functions -------------------------------------------------------

    /**
     * Loads all registered resources, and initializes the formula resource
     * factory.
     */
    override async loadResources(): Promise<void> {

        // load all registered resources and the formula resource factory (in parallel)
        const [formulaResourceFactory] = await Promise.all([
            FormulaResourceFactory.create(this.docApp.getFileFormat(), LOCALE_DATA),
            super.loadResources()
        ]);

        (this as Writable<this>).formulaResourceFactory = formulaResourceFactory;
    }
}
