/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { type StyleChangeSet, SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import { type SheetCacheConfig, type SheetCacheFlushResult, SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// re-exports =================================================================

export type { StyleChangeSet } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
export { createBorderChangeSet, createVisibleBorderChangeSet } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
export type { CellChangeSet, AddressChangeSet } from "@/io.ox/office/spreadsheet/model/operation/cellcache";

// types ======================================================================

/**
 * The changed settings in the document after applying all changes collected by
 * an `OperationBuilder`.
 */
export interface OperationBuilderFlushResult {

    /**
     * The generator filled with all operations (either passed into the method
     * `flushOperations()`, or a new instance if omitted).
     */
    generator: SheetOperationGenerator;

    /**
     * The changed settings of all processed `SheetCache` instances, mapped by
     * the sheet models.
     */
    sheetResults: Map<SheetModel, SheetCacheFlushResult>;
}

// public functions ===========================================================

/**
 * Creates a flat clone of the passed change set, and adds a globally unique
 * cache key that causes internal auto-style caching.
 *
 * @param changeSet
 *  The change set to be cloned and extended with a unique cache key.
 *
 * @returns
 *  The extended clone of the passed change set.
 */
export function cloneWithCacheKey<ChangeSetT extends StyleChangeSet>(changeSet: ChangeSetT): ChangeSetT {
    return { ...changeSet, cacheKey: DObject.makeUid() };
}

// class OperationBuilder =====================================================

/**
 * A helper class wrapping multiple operation generators and caches.
 *
 * An instance contains a `SheetStyleCache` that implements generation and
 * caching of cell auto-styles, style sheets, and indexed number formats to be
 * shared across the document; and multiple `SheetCache` instances for
 * generating contents and formatting for multiple sheets (e.g. while updating
 * formula expressions in the entire document).
 *
 * At the end of the operation generation process, the generated document
 * operations and undo operations will be pushed in the correct order into a
 * resulting operation generator.
 */
export class OperationBuilder extends DObject {

    /**
     * The document model this operation builder is working on.
     */
    readonly docModel: SpreadsheetModel;

    /**
     * Main generator for global document operations to be applied before the
     * sheet operations.
     */
    readonly headGenerator: SheetOperationGenerator;

    /**
     * Main generator for global document operations to be applied after the
     * sheet operations.
     */
    readonly tailGenerator: SheetOperationGenerator;

    // caches for sheet contents, mapped by sheet UIDs
    readonly #styleCache: SheetStyleCache;

    // caches for sheet contents, mapped by sheet model
    readonly #sheetCaches = this.member(new Map<SheetModel, SheetCache>());

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super();
        this.docModel = docModel;
        this.headGenerator = new SheetOperationGenerator(docModel);
        this.tailGenerator = new SheetOperationGenerator(docModel);
        this.#styleCache = new SheetStyleCache(docModel);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a new sheet cache used to store sheet operations for the passed
     * sheet model. This method expects that the specified sheet cache has not
     * been created before, and throws otherwise.
     *
     * @param sheetModel
     *  The sheet model to return the sheet cache for.
     *
     * @param [config]
     *  Configuration options for the `SheetCache` instance.
     *
     * @returns
     *  The sheet cache for the passed sheet model.
     */
    createSheetCache(sheetModel: SheetModel, config?: SheetCacheConfig): SheetCache {
        return map.update(this.#sheetCaches, sheetModel, sheetCache => {
            OperationError.assertCause(!sheetCache, "operation", "sheet cache cannot be recreated");
            return new SheetCache(sheetModel, this.#styleCache, config);
        });
    }

    /**
     * Returns the sheet cache used to store sheet operations for the passed
     * sheet model. If the sheet cache does not exist yet, it will be created
     * with default settings.
     *
     * @param sheetModel
     *  The sheet model to return the sheet cache for.
     *
     * @returns
     *  The sheet cache for the passed sheet model.
     */
    getSheetCache(sheetModel: SheetModel): SheetCache {
        return map.upsert(this.#sheetCaches, sheetModel, () => new SheetCache(sheetModel, this.#styleCache));
    }

    /**
     * Copies the collected document operations from all internal generators
     * into the passed operation generator.
     *
     * @param [generator]
     *  The operation generator to be filled with all document operations. If
     *  omitted, a new generator will be created and put into the result
     *  object returned from this method.
     *
     * @returns
     *  The result object with the collected changes in the document.
     */
    flushOperations(generator?: SheetOperationGenerator): OperationBuilderFlushResult {

        // the operation generator to be used
        generator ??= new SheetOperationGenerator(this.docModel);

        // the result object to be returned
        const builderResult: OperationBuilderFlushResult = { generator, sheetResults: new Map() };

        // finalize all sheet caches (this may generate more style operations in the own `_styleCache`)
        for (const sheetCache of this.#sheetCaches.values()) {
            const sheetResult = sheetCache.flushOperations(generator);
            builderResult.sheetResults.set(sheetCache.sheetModel, sheetResult);
        }

        // operations from `headGenerator` in front of sheet operations
        generator.prependOperations(this.headGenerator);
        // style operations in front of all other operations
        generator.prependOperations(this.#styleCache);
        // operations from `tailGenerator` after the sheet operations
        generator.appendOperations(this.tailGenerator);

        // undo operations from `headGenerator` in front of sheet operations
        generator.prependOperations(this.headGenerator, { undo: true });
        // undo operations from `tailGenerator` after the sheet operations
        generator.appendOperations(this.tailGenerator, { undo: true });
        // style undo operations after all other operations
        generator.appendOperations(this.#styleCache, { undo: true });

        // return the result object
        return builderResult;
    }
}
