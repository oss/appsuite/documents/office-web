/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { type Relation, is, ary, map, dict, pick, json } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { ChangeIntervalsProps } from "@/io.ox/office/spreadsheet/utils/operations";
import * as Op from "@/io.ox/office/spreadsheet/utils/operations";

import type { GeneratorOptions, SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { StyleChangeSet, SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import type { SheetCacheConfig } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { type AddressTransformer, TransformMode } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { type PtColRowAttributeSet, type ColModel, containsResizeAttr } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import type { ColRowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * A descriptor with all properties needed to change contents and formatting of
 * columns and rows.
 */
export interface ColRowChangeSet extends StyleChangeSet {

    /**
     * The (incomplete) column/row attribute set to be merged with the existing
     * attribute set.
     */
    attrs?: PtColRowAttributeSet;

    /**
     * If set to `true`, the size attribute (either "width" or "height") in the
     * property `attrs` is expected to be in native operation units according
     * to the file format (OOXML: standard digit count for column width, font
     * points for height; ODF: 1/100 mm). By default, the size attribute is
     * always expected in 1/100 of millimeters, and will be converted to the
     * native operation units.
     */
    nativeSize?: boolean;
}

/**
 * A function that returns the identifier of the effective auto-style of the
 * column/row with the specified index.
 *
 * @param index
 *  The column/row index.
 *
 * @returns
 *  The effective identifier of the auto-style that will be used for the index
 *  (as string); or `null`, if a row does not use, or removes its old
 *  auto-style (by having set its row attribute "customFormat" to `false`).
 */
export type ResolveIndexStyleFn = (index: number) => string | null;

/**
 * The result data for generating all change operations for column/row
 * intervals.
 */
export interface ColRowCacheFlushResult {

    /**
     * The (transformed) index intervals that have changed somehow.
     */
    changedIntervals: IntervalArray;

    /**
     * A function that returns the new auto-style for the specified column/row.
     */
    resolveStyleFn: ResolveIndexStyleFn;

    /**
     * Whether the effective size of the columns/rows will be changed by the
     * operations (either by changing the size, or by hiding/unhiding).
     */
    resizeEntries: boolean;
}

// private types --------------------------------------------------------------

/** Changeset with column/row interval to be stored in sorted arrays. */
interface IntervalEntry {
    interval: Interval;
    changeSet: ColRowChangeSet;
    txOffset?: number;
    resetStyle?: boolean;
}

// private functions ==========================================================

function compareEntries(entry1: IntervalEntry, entry2: IntervalEntry): Relation {
    return Interval.compare(entry1.interval, entry2.interval);
}

/**
 * Inserts a new entry with the passed changeset into a sorted entry array.
 */
function insertChangeSet(entries: IntervalEntry[], interval: Interval, changeSet: ColRowChangeSet, txOffset?: number, resetStyle?: boolean): void {
    if (!is.empty(changeSet)) {
        ary.insertSorted(entries, { interval, changeSet, txOffset, resetStyle }, compareEntries, { unique: true });
    }
}

/**
 * Returns the half open (!) interval of array indexes of all changeset entries
 * overlapping with the passed index interval.
 */
function getOverlapSliceIndexes(entries: IntervalEntry[], interval: Interval): Pair<number> {
    return [
        ary.fastFindFirstIndex(entries, entry => interval.first <= entry.interval.last),
        ary.fastFindFirstIndex(entries, entry => interval.last < entry.interval.first)
    ];
}

/**
 * Returns whether any of the elements in the sorted entry array overlaps with
 * the passed index interval.
 */
function hasOverlapEntry(entries: IntervalEntry[], interval: Interval): boolean {
    const entry = ary.fastFindFirstValue(entries, entry => interval.first <= entry.interval.last);
    return !!entry && (entry.interval.first <= interval.last);
}

// class ColRowCache ==========================================================

/**
 * A helper class to generate operations and the related undo operations for
 * the formatting of columns and rows in a sheet.
 *
 * @param sheetModel
 *  The model of the sheet this instance is associated to.
 *
 * @param styleCache
 *  The parent style cache needed to generate the cell auto-styles.
 *
 * @param [config]
 *  Configuration options.
 */
export class ColRowCache extends DObject {

    // properties -------------------------------------------------------------

    // the document model
    readonly docModel: SpreadsheetModel;

    // the style cache needed to generate cell auto-styles
    readonly styleCache: SheetStyleCache;

    // the configuration settings passed to the constructor
    readonly #config: SheetCacheConfig;

    // the address transformer for moving interval indexes
    readonly #transformer?: AddressTransformer;

    // the interval collection this builder works on
    readonly #collection: ColRowCollection;

    // the array with all collected changesets for columns/rows (with original indexes!)
    readonly #entries: IntervalEntry[] = [];

    // whether file format is OOXML (special handling for column widths)
    readonly #oox: boolean;

    // whether this is the column cache
    readonly #columns: boolean;

    // the family name of the attributes
    readonly #family: "column" | "row";

    // the name of the column/row size attribute
    readonly #sizeAttr: string;

    // helper function that converts column/row size from 1/100mm to operation units
    readonly #convertSizeToUnit: (size: number) => number;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, styleCache: SheetStyleCache, columns: boolean, config?: SheetCacheConfig) {
        super();

        this.docModel = sheetModel.docModel;
        this.styleCache = styleCache;

        this.#config = { ...config };
        this.#transformer = ((columns === config?.transformer?.columns) && !config.moveCells) ? config.transformer : undefined;
        this.#collection = columns ? sheetModel.colCollection : sheetModel.rowCollection;
        this.#oox = this.docModel.docApp.isOOXML();
        this.#columns = columns;
        this.#family = columns ? "column" : "row";
        this.#sizeAttr = columns ? "width" : "height";
        const { fontMetrics } = this.docModel;
        this.#convertSizeToUnit = columns ? (size => fontMetrics.convertColWidthToUnit(size)) : (size => fontMetrics.convertRowHeightToUnit(size));
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a changeset to be applied to one or more row/column intervals
     * in the sheet associated to this instance.
     *
     * @param interval
     *  The current (original) index interval to be modified with the passed
     *  changeset. If the configuration of this instance contains an address
     *  transformer, the operations will be generated with the transformed
     *  interval, but the undo operations will always be generated with this
     *  original interval.
     *
     * @param changeSet
     *  The changeset to be applied to the specified intervals.
     */
    changeInterval(interval: Interval, changeSet: ColRowChangeSet): void {

        // update the array entries covered by the passed interval
        const [begin, end] = getOverlapSliceIndexes(this.#entries, interval);
        const updateEntries = this.#entries.splice(begin, end - begin);
        updateEntries.forEach(entry => {
            // reinsert leading uncovered part of the existing entry
            if (entry.interval.first < interval.first) {
                insertChangeSet(this.#entries, new Interval(entry.interval.first, interval.first - 1), entry.changeSet);
            }
            // reinsert trailing uncovered part of the existing entry
            if (interval.last < entry.interval.last) {
                insertChangeSet(this.#entries, new Interval(interval.last + 1, entry.interval.last), entry.changeSet);
            }
            // merge the changesets
            const newChangeSet = { ...entry.changeSet, ...changeSet };
            insertChangeSet(this.#entries, interval.intersect(entry.interval)!, newChangeSet);
        });

        // insert the passed changeset for all gaps in the array
        const updateIntervals = IntervalArray.map(updateEntries, entry => entry.interval);
        const gapIntervals = new IntervalArray(interval).difference(updateIntervals);
        gapIntervals.forEach(gapInterval => insertChangeSet(this.#entries, gapInterval, changeSet));
    }

    /**
     * Registers column/row attributes to be applied to one or more row/column
     * intervals in the sheet associated to this instance.
     *
     * @param interval
     *  The current (original) index interval to be modified with the passed
     *  column/row attributes. If the configuration of this instance contains
     *  an address transformer, the operations will be generated with the
     *  transformed interval, but the undo operations will always be generated
     *  with this original interval.
     *
     * @param attrs
     *  The column/row attributes to be applied to the specified interval.
     */
    changeAttributes(interval: Interval, attrs: Dict): void {
        this.changeInterval(interval, { attrs: { [this.#family]: attrs } });
    }

    /**
     * Generates the change operations for all columns/rows collected by this
     * instance, and inserts all collected operations into the passed operation
     * generator.
     *
     * @param generator
     *  The operation generator to be filled with all sheet operations.
     *
     * @returns
     *  The result object with all data needed for further processing, e.g. for
     *  cell operations.
     */
    @modelLogger.profileMethod("$badge{ColRowCache} flushOperations")
    flushOperations(generator: SheetOperationGenerator): ColRowCacheFlushResult {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;

        // whether to generate regular or undo operation
        const processOps = !this.#config.skipOps;
        const processUndo = !this.#config.skipUndo;
        // whether the columns/rows will be resized

        // process all collected interval entries
        const operEntries: IntervalEntry[] = [];
        const undoEntries: IntervalEntry[] = [];
        let resizeEntries = this.#processIntervalEntries(operEntries, undoEntries);

        // add changesets for new intervals (inserted columns/rows)
        if (processOps && this.#transformer?.insert && this.#processInsertIntervals(this.#transformer, operEntries)) {
            resizeEntries = true;
        }

        // add changesets for deleted intervals
        if (processUndo && this.#transformer) {
            this.#processRestoreIntervals(this.#transformer, undoEntries);
        }

        // generate operations for the collected interval data
        if (processOps && operEntries.length) {
            this.#generateIntervalOperations(generator, operEntries, false);
        }

        // generate the undo operations
        if (processUndo && undoEntries.length) {
            this.#generateIntervalOperations(generator, undoEntries, true);
        }

        // the intervals of all changed columns/rows
        const changedIntervals = IntervalArray.map(operEntries, entry => entry.interval).merge();
        // a fast lookup cache for auto-style identifiers
        const styleIdCache = new Map<number, string | null>();
        // the auto-style resolver function to be returned in the result
        const resolveStyleFn: ResolveIndexStyleFn = newIndex => map.upsert(styleIdCache, newIndex, () => {
            const entry = ary.fastFindFirstValue(operEntries, entry => newIndex <= entry.interval.last);
            const styleId = (entry && (entry.interval.first <= newIndex)) ? (entry.resetStyle ? null : entry.changeSet.s) : undefined;
            if (styleId !== undefined) { return styleId; }
            const oldIndex = (entry && is.number(entry.txOffset)) ? (newIndex - entry.txOffset) : undefined;
            return is.number(oldIndex) ? this.#collection.getModel(oldIndex).getAutoStyleId() : defStyleId;
        });

        // return the result object
        return { changedIntervals, resolveStyleFn, resizeEntries };
    }

    // private methods --------------------------------------------------------

    /**
     * Reduces the collected changesets to the properties that will change
     * according to the current state of the column/row interval in the
     * document, and appends the reduced changesets and the appropriate undo
     * changesets into the passed buffers.
     *
     * @returns
     *  Whether any of the intervals has changed its pixel size (by resizing,
     *  or by hiding/unhiding).
     */
    #processIntervalEntries(operEntries: IntervalEntry[], undoEntries: IntervalEntry[]): boolean {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;
        // the address transformer
        const transformer = this.#transformer;
        // whether the effective size of the collection entries will be changed by the operations
        let resizeEntries = false;

        // process all collected changesets
        for (const { interval, changeSet } of this.#entries) {

            // the column/row attributes
            const attrs = { ...pick.dict(changeSet.attrs, this.#family, true) };
            // whether the row auto-style will be reset
            const resetStyle = !this.#columns && ((attrs.customFormat === false) || (attrs.customFormat === null));
            // special handling needed to show hidden columns
            const showHiddenOox = this.#oox && this.#columns && (attrs.visible === true) && !("width" in attrs);

            // convert column/row size in 1/100mm to operation units according to file format
            const newSize = attrs[this.#sizeAttr];
            if (is.number(newSize) && !changeSet.nativeSize) {
                attrs[this.#sizeAttr] = this.#convertSizeToUnit(newSize);
            }

            // process all column/row models covered by the current interval
            for (const { model, interval: oldInterval } of this.#collection.yieldBandModels(interval)) {

                // the new transformed index interval (ignore registered changes for deleted intervals)
                let newIntervals = new IntervalArray(oldInterval);
                if (this.#transformer) { newIntervals = this.#transformer.transformIntervals(newIntervals, { transformMode: TransformMode.SPLIT }); }
                if (newIntervals.empty()) { continue; }

                // the resulting changeset for the interval
                const intSet = dict.createPartial<ColRowChangeSet>();
                // the resulting undo changeset
                const undoSet = dict.createPartial<ColRowChangeSet>();

                // the original style identifier of the interval auto-style
                const oldStyleId = model.getAutoStyleId();
                // flat copy of the attributes to be modified individually for each column/row model
                const newAttrs = { ...attrs };
                // whether to reset the row auto-style for the current model
                let newResetStyle = false;

                // special case: reset the custom row auto-style
                if (resetStyle) {
                    // nothing to do if the row does not have an auto-style
                    if (oldStyleId !== null) {
                        intSet.s = defStyleId;
                        undoSet.s = oldStyleId;
                        newResetStyle = true;
                    }
                } else {
                    // get the new auto-style (merge old auto-style with settings in the changeset)
                    const effOldStyleId = oldStyleId ?? defStyleId;
                    const newStyleId = this.styleCache.generateAutoStyleOperations(effOldStyleId, changeSet);
                    if (oldStyleId !== newStyleId) {
                        intSet.s = newStyleId;
                        undoSet.s = effOldStyleId;
                        if (oldStyleId !== null) { newAttrs.customFormat = true; }
                    }
                }

                // bug 56758: OOXML needs the default column width when showing hidden columns
                if (showHiddenOox && (((model as ColModel).getMergedEntryAttributes()).width === 0)) {
                    newAttrs.width = this.#convertSizeToUnit(this.#collection.defaultModel.sizeHmm);
                    newAttrs.customWidth = false;
                }

                // update the new (partial) attribute set
                let newAttrSet: PtColRowAttributeSet = changeSet.attrs ? { ...changeSet.attrs } : dict.create();
                if (is.empty(newAttrs)) {
                    delete newAttrSet[this.#family];
                } else {
                    newAttrSet[this.#family] = newAttrs;
                }

                // create the resulting attribute set to be applied at the current entry model
                newAttrSet = model.getReducedAttributeSet(newAttrSet);
                if (!is.empty(newAttrSet)) {
                    intSet.attrs = newAttrSet;
                    undoSet.attrs = model.getUndoAttributeSet(newAttrSet);
                    // detect resized intervals
                    resizeEntries ||= containsResizeAttr(newAttrSet, this.#columns);
                }

                // insert the changesets into the passed buffers
                for (const newInterval of newIntervals) {
                    // DOCS-2716: calculate reverse interval for undo, that does not overlap with restore intervals
                    const revInterval = transformer ? transformer.transformIntervals([newInterval], { reverse: true })[0] : newInterval;
                    insertChangeSet(operEntries, newInterval, intSet, newInterval.first - revInterval.first, newResetStyle);
                    insertChangeSet(undoEntries, revInterval, undoSet);
                }
            }
        }

        return resizeEntries;
    }

    /**
     * Creates the changesets needed to format new inserted column/row.
     *
     * @returns
     *  Whether any of the intervals has changed its pixel size (by unhiding).
     */
    #processInsertIntervals(transformer: AddressTransformer, operEntries: IntervalEntry[]): boolean {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;
        // whether the effective size of the collection entries will be changed by the operations
        let resizeEntries = false;

        // process all inserted columns/rows
        transformer.targetIntervals.forEach((targetInterval, index) => {

            // check that the inserted interval will not be modified otherwise
            if (hasOverlapEntry(operEntries, targetInterval)) {
                OperationError.throwCause("operation", `$badge{ColRowCache} processInsertIntervals: found changeset for inserted interval ${targetInterval.toOpStr(this.#columns)}`);
            }

            // expand formatting of the preceding column/row
            const sourceIndex = transformer.sourceIntervals[index].first;
            if (sourceIndex === 0) { return; }

            // row/column models (row entries without custom style will contain the default auto-style)
            const prevModel = this.#collection.getModel(sourceIndex - 1);
            const thisModel = this.#collection.getModel(sourceIndex);

            // the changeset for the new columns/rows
            const changeSet = dict.createPartial<ColRowChangeSet>();

            // create the auto-style with correct border settings (do not apply the default style)
            const prevStyleId = prevModel.getEffectiveAutoStyleId();
            const thisStyleId = thisModel.getEffectiveAutoStyleId();
            const newStyleId = this.styleCache.generateInsertedAutoStyleOperations(prevStyleId, thisStyleId, this.#columns);
            if (newStyleId !== defStyleId) { changeSet.s = newStyleId; }

            // ensure that the inserted entries are always visible
            let attrSet = prevModel.getExplicitAttributeSet(true);
            if (!prevModel.getMergedEntryAttributes().visible) {
                attrSet = json.deepClone(attrSet);
                (attrSet[this.#family] ??= dict.create()).visible = true;
                resizeEntries = true;
            }
            if (!is.empty(attrSet)) { changeSet.attrs = attrSet; }

            // insert the changeset into the array (interval is already transformed)
            insertChangeSet(operEntries, targetInterval, changeSet);
        });

        return resizeEntries;
    }

    /**
     * Creates the changesets needed to restore the deleted columns/rows.
     */
    #processRestoreIntervals(transformer: AddressTransformer, undoEntries: IntervalEntry[]): void {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;

        // process all deleted intervals in the address transformer (intervals are distinct and sorted)
        for (const deleteInterval of transformer.deleteIntervals) {

            // check that the restored interval has not been modified otherwise
            if (hasOverlapEntry(undoEntries, deleteInterval)) {
                OperationError.throwCause("operation", `$badge{ColRowCache} processRestoreIntervals: found changeset for deleted interval ${deleteInterval.toOpStr(this.#columns)}`);
            }

            // process all intervals to be restored on undo
            for (const { model, interval } of this.#collection.yieldBandModels(deleteInterval)) {

                // the current entry model
                const undoSet = dict.createPartial<ColRowChangeSet>();

                // restore non-default auto-styles
                const styleId = model.getEffectiveAutoStyleId();
                if (styleId !== defStyleId) { undoSet.s = styleId; }

                // restore explicit column/row attributes
                const attrSet = model.getExplicitAttributeSet();
                if (!is.empty(attrSet)) { undoSet.attrs = attrSet; }

                // create a changeset, if an auto-style or explicit attributes need to be restored
                insertChangeSet(undoEntries, interval.clone(), undoSet);
            }
        }
    }

    /**
     * Generates the change operations for all intervals contained in the
     * passed array of changesets.
     */
    #generateIntervalOperations(generator: SheetOperationGenerator, entries: IntervalEntry[], undo: boolean): void {

        // an entry for the following map that collects all intervals with the same operation properties
        interface GroupEntry {
            intervals: IntervalArray;
            props: ChangeIntervalsProps;
        }

        // create groups of intervals with equal attributes and auto-style
        const intervalGroups = new Map<string, GroupEntry>();
        for (const { interval, changeSet } of entries) {
            const props: ChangeIntervalsProps = _.pick(changeSet, "s", "attrs");
            if (is.empty(props)) { continue; }
            const key = json.tryStringify(props, { sortKeys: true });
            map.update(intervalGroups, key, intervalGroup => {
                intervalGroup ??= { intervals: new IntervalArray(), props };
                intervalGroup.intervals.push(interval);
                return intervalGroup;
            });
        }

        // create a change operation for every group with equal formatting
        const opName = this.#columns ? Op.CHANGE_COLUMNS : Op.CHANGE_ROWS;
        const genOptions: GeneratorOptions = { undo, filter: this.#config.filter };
        for (const intervalGroup of intervalGroups.values()) {
            generator.generateIntervalsOperation(opName, intervalGroup.intervals, this.#columns, intervalGroup.props, genOptions);
        }
    }
}
