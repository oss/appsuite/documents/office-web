/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type GeneratorConfig, type GeneratorOptions, OperationGenerator } from "@/io.ox/office/editframework/model/operation/generator";

import type { Position, CellDataDict, SheetOperation } from "@/io.ox/office/spreadsheet/utils/operations";
import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import type { MergeMode, Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/model/operation/generator";

// types ======================================================================

/**
 * Configuration options for for the class `SheetOperationGenerator`.
 */
export interface SheetGeneratorConfig extends GeneratorConfig {

    /**
     * The initial zero-based sheet index to be inserted into all sheet
     * operations. Default value is `0`.
     */
    sheet?: number;
}

// class SheetOperationGenerator ==============================================

/**
 * An operations generator specifically for spreadsheet documents. Provides
 * additional methods to generate document operations, and undo operations, for
 * a specific sheet model. However, the generator is not restricted to a single
 * sheet. The sheet index used to generate sheet operations can be changed at
 * any time.
 *
 * @param docModel
 *  The document model this operations generator will be created for.
 *
 * @param [config]
 *  Initial configuration.
 */
export class SheetOperationGenerator extends OperationGenerator<SpreadsheetModel> {

    // properties -------------------------------------------------------------

    /**
     * The sheet index to be inserted into generated operations with property
     * `sheet`. This property can be changed at any time while generating
     * operations.
     */
    sheet: number;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, config?: SheetGeneratorConfig) {
        super(docModel, config);
        this.sheet = config?.sheet ?? 0;
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed JSON position of a drawing object inside a sheet to
     * a global position includig the leading sheet index, as used in generic
     * drawing and text operations.
     *
     * @param localPos
     *  The local position of a drawing object (without leading sheet index).
     *
     * @returns
     *  The global position of the drawing object (with leading sheet index).
     */
    getDrawingPosition(localPos: Position): Op.Position {
        return [this.sheet, ...localPos];
    }

    /**
     * Creates a new empty operation generator with the same settings as this
     * generator.
     *
     * @returns
     *  A new empty operation generator with the same settings.
     */
    override createSubGenerator(): SheetOperationGenerator {
        return new SheetOperationGenerator(this.docModel, {
            applyImmediately: this.isImmediateApplyMode(),
            sheet: this.sheet
        });
    }

    /**
     * Creates and appends a new operation to the operations array, which will
     * contain a property `sheet` set to the current sheet index of this
     * generator.
     *
     * @param name
     *  The name of the operation.
     *
     * @param [props]
     *  Additional properties that will be stored in the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateSheetOperation(name: string, props?: Dict, options?: GeneratorOptions): SheetOperation {
        props = { sheet: this.sheet, ...props };
        return this.generateOperation(name, props, options) as SheetOperation;
    }

    /**
     * Creates and appends a new operation with a property `intervals` to the
     * operations array addressing column or row intervals.
     *
     * @param name
     *  The name of the operation.
     *
     * @param intervals
     *  The column/row intervals to be inserted into the operation.
     *
     * @param columns
     *  Whether to generate column intervals (`true`), or row intervals
     *  (`false`).
     *
     * @param [props]
     *  Additional properties that will be stored in the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateIntervalsOperation(name: string, intervals: IntervalSource, columns: boolean, props?: Opt<Op.ChangeIntervalsProps>, options?: GeneratorOptions): SheetOperation {
        const intStr = IntervalArray.cast(intervals).toOpStr(columns);
        return this.generateSheetOperation(name, { intervals: intStr, ...props }, options);
    }

    /**
     * Creates and appends a new operation with a string property `table` to
     * the operations array addressing a table range.
     *
     * @param name
     *  The name of the operation.
     *
     * @param tableName
     *  The name of a table range. The empty string addresses the anonymous
     *  table range used to store filter settings for the standard auto filter
     *  of the sheet (the property `table` will not be inserted into the
     *  generated operation).
     *
     * @param [props]
     *  Additional properties that will be stored in the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateTableOperation(name: string, tableName: string, props?: Dict, options?: GeneratorOptions): Op.TableOperation {
        if (tableName.length > 0) { props = { table: tableName, ...props }; }
        return this.generateSheetOperation(name, props, options) as Op.TableOperation;
    }

    /**
     * Creates and appends a new operation with the properties `table` and
     * `range` to the operations array addressing a table range.
     *
     * @param name
     *  The name of the operation.
     *
     * @param tableName
     *  The name of a table range. The empty string addresses the anonymous
     *  table range used to store filter settings for the standard auto filter
     *  of the sheet (the property `table` will not be inserted into the
     *  generated operation).
     *
     * @param tableRange
     *  The location of the table range. Will be inserted as property `range`
     *  into the operation.
     *
     * @param [props]
     *  Additional properties that will be stored in the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateTableRangeOperation(name: string, tableName: string, tableRange: Range, props?: Dict, options?: GeneratorOptions): Op.TableOperation {
        return this.generateTableOperation(name, tableName, { range: tableRange.toOpStr(), ...props }, options);
    }

    /**
     * Creates and appends a new operation with a property `start` to the
     * operations array addressing a drawing object.
     *
     * @param name
     *  The name of the operation.
     *
     * @param position
     *  The local position of the drawing object in the sheet (without leading
     *  sheet index).
     *
     * @param [props]
     *  Additional properties that will be stored in the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateDrawingOperation(name: string, position: Position, props?: Dict, options?: GeneratorOptions): Op.DrawingOperation {
        props = { start: this.getDrawingPosition(position), ...props };
        return this.generateOperation(name, props, options) as Op.DrawingOperation;
    }

    /**
     * Creates and appends a new operation for a cell note or comment with a
     * property `anchor` to the operations array.
     *
     * @param name
     *  The name of the operation.
     *
     * @param anchor
     *  The address of the anchor cell.
     *
     * @param [props]
     *  Additional properties that will be stored in the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateAnchorOperation(name: string, anchor: Address, props?: Dict, options?: GeneratorOptions): Op.CellAnchorOperation {
        props = { anchor: anchor.toOpStr(), ...props };
        return this.generateSheetOperation(name, props, options) as Op.CellAnchorOperation;
    }

    // explicit operations ----------------------------------------------------

    /**
     * Creates and appends a new `changeCells` operation.
     *
     * @param contents
     *  The cell contents to be applied with the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateChangeCellsOperation(contents: CellDataDict, options?: GeneratorOptions): Op.ChangeCellsOperation {
        return this.generateSheetOperation(Op.CHANGE_CELLS, { contents }, options) as Op.ChangeCellsOperation;
    }

    /**
     * Creates and appends a new `changeCells` operation that clears all cells
     * in the passed cell range (values, formulas, and auto-styles).
     *
     * @param range
     *  The address of the cell range to be cleared.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateClearCellsOperation(range: Range, options?: GeneratorOptions): Op.ChangeCellsOperation {
        return this.generateChangeCellsOperation({ [range.toOpStr()]: { u: true } }, options);
    }

    /**
     * Creates and appends a new `moveCells` operation.
     *
     * @param range
     *  The address of the cell range addressed by the operation.
     *
     * @param direction
     *  The move mode to be inserted into the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateMoveCellsOperation(range: Range, direction: Direction, options?: GeneratorOptions): Op.MoveCellsOperation {
        return this.generateSheetOperation(Op.MOVE_CELLS, { range: range.toOpStr(), dir: direction }, options) as Op.MoveCellsOperation;
    }

    /**
     * Creates and appends a new `mergeCells` operation.
     *
     * @param ranges
     *  The addresses of the cell ranges addressed by the operation. If this
     *  value resolves to an empty list, no operation will be generated.
     *
     * @param mergeMode
     *  The merge mode to be inserted into the operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object; or `null`, if the passed cell range
     *  source is empty.
     */
    generateMergeCellsOperation(ranges: RangeSource, mergeMode: MergeMode, options?: GeneratorOptions): Op.MergeCellsOperation | null {
        const rangesStr = RangeArray.cast(ranges).toOpStr();
        return rangesStr ? this.generateSheetOperation(Op.MERGE_CELLS, { ranges: rangesStr, type: mergeMode }, options) as Op.MergeCellsOperation : null;
    }

    /**
     * Creates and appends a new `insertHyperlink` or `deleteHyperlink`
     * operation.
     *
     * @param ranges
     *  The addresses of the cell ranges addressed by the operation. If this
     *  value resolves to an empty list, no operation will be generated.
     *
     * @param url
     *  The URL of the hyperlink to be attached to the cell range. If set to
     *  the empty string or null, existing hyperlinks will be removed instead.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object; or `null`, if the passed cell range
     *  source is empty.
     */
    generateHlinkOperation(ranges: RangeSource, url: "" | null, options?: GeneratorOptions): Op.DeleteHlinkOperation | null;
    generateHlinkOperation(ranges: RangeSource, url: string, options?: GeneratorOptions): Op.InsertHlinkOperation | null;
    // implementation
    generateHlinkOperation(ranges: RangeSource, url: string | null, options?: GeneratorOptions): Op.InsertHlinkOperation | Op.DeleteHlinkOperation | null {

        const rangesStr = RangeArray.cast(ranges).toOpStr();
        if (!rangesStr) { return null; }

        const name = url ? Op.INSERT_HYPERLINK : Op.DELETE_HYPERLINK;
        const props: Dict = { ranges: rangesStr };
        if (url) { props.url = url; }
        return this.generateSheetOperation(name, props, options) as Op.InsertHlinkOperation | Op.DeleteHlinkOperation;
    }

    /**
     * Creates and appends a new `moveDrawing` operation.
     *
     * @param fromPos
     *  The current position of the drawing object in the sheet.
     *
     * @param toPos
     *  The new position for the drawing object in the sheet.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateMoveDrawingOperation(fromPos: Position, toPos: Position, options?: GeneratorOptions): Op.MoveDrawingOperation {
        return this.generateDrawingOperation(Op.MOVE_DRAWING, fromPos, { to: this.getDrawingPosition(toPos) }, options) as Op.MoveDrawingOperation;
    }
}
