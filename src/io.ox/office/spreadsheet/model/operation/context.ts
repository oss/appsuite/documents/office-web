/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str } from "@/io.ox/office/tk/algorithms";

import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import { OperationContext } from "@/io.ox/office/editframework/model/operation/context";

import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// exports ====================================================================

// re-export base classes and types
export * from "@/io.ox/office/editframework/model/operation/context";

// class SheetOperationContext ================================================

/**
 * A small wrapper for a JSON document operation object providing useful helper
 * methods, passed to the operation handlers of spreadsheet documents.
 */
export class SheetOperationContext extends OperationContext<SpreadsheetModel> {

    // properties -------------------------------------------------------------

    readonly #addressFactory: AddressFactory;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, operation: Operation, external?: boolean, importing?: boolean) {
        super(docModel, operation, external, importing);
        this.#addressFactory = docModel.addressFactory;
    }

    // public methods ---------------------------------------------------------

    /**
     * Parses a column/row interval from a string in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @param columns
     *  Whether to parse a column interval (`true`, e.g. "A:C"), or a row
     *  interval (`false`, e.g. "1:3").
     *
     * @returns
     *  The parsed column/row interval.
     *
     * @throws
     *  An `OperationError`, if the passed string cannot be parsed to a valid
     *  column/row interval.
     */
    parseInterval(parseStr: string, columns: boolean): Interval {
        const interval = this.#addressFactory.parseInterval(parseStr, columns);
        this.ensure(interval, `invalid index interval "${parseStr}"`);
        return interval;
    }

    /**
     * Returns the value of a required column/row interval property (as string
     * in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param columns
     *  Whether to parse a column interval (`true`, e.g. "A:C"), or a row
     *  interval (`false`, e.g. "1:3").
     *
     * @returns
     *  The column/row interval of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  column/row interval property with the specified name.
     */
    getInterval(propName: string, columns: boolean): Interval {
        return this.parseInterval(this.getStr(propName), columns);
    }

    /**
     * Returns the value of an optional column/row interval property (as string
     * in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param columns
     *  Whether to parse a column interval (`true`, e.g. "A:C"), or a row
     *  interval (`false`, e.g. "1:3").
     *
     * @returns
     *  The column/row interval of the specified operation property; or
     *  `undefined`, if the property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid column/row
     *  interval.
     */
    optInterval(propName: string, columns: boolean): Opt<Interval> {
        return this.has(propName) ? this.getInterval(propName, columns) : undefined;
    }

    /**
     * Parses a cell address from a string in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The parsed cell address.
     *
     * @throws
     *  An `OperationError`, if the passed string cannot be parsed to a valid
     *  cell address.
     */
    parseAddress(parseStr: string): Address {
        const address = this.#addressFactory.parseAddress(parseStr);
        this.ensure(address, `invalid cell address "${parseStr}"`);
        return address;
    }

    /**
     * Returns the value of a required cell address property (as string in A1
     * notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The cell address of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  cell address property with the specified name.
     */
    getAddress(propName: string): Address {
        return this.parseAddress(this.getStr(propName));
    }

    /**
     * Returns the value of an optional cell address property (as string in A1
     * notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The cell address of the specified operation property; or `undefined`,
     *  if the property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid cell address.
     */
    optAddress(propName: string): Opt<Address> {
        return this.has(propName) ? this.getAddress(propName) : undefined;
    }

    /**
     * Parses a cell range address from a string in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The parsed cell range address.
     *
     * @throws
     *  An `OperationError`, if the passed string cannot be parsed to a valid
     *  cell range address.
     */
    parseRange(parseStr: string): Range {
        const range = this.#addressFactory.parseRange(parseStr);
        this.ensure(range, `invalid cell range address "${parseStr}"`);
        return range;
    }

    /**
     * Returns the value of a required cell range address property (as string
     * in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The cell range address of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  cell range address property with the specified name.
     */
    getRange(propName: string): Range {
        return this.parseRange(this.getStr(propName));
    }

    /**
     * Returns the value of an optional cell range address property (as string
     * in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The cell range address of the specified operation property; or
     *  `undefined`, if the property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid cell range
     *  address.
     */
    optRange(propName: string): Opt<Range> {
        return this.has(propName) ? this.getRange(propName) : undefined;
    }

    /**
     * Parses a column/row interval list from a string of space-separated
     * column/row intervals in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @param columns
     *  Whether to parse column intervals (`true`, e.g. "A:C E:F"), or row
     *  intervals (`false`, e.g. "1:3 5:6").
     *
     * @returns
     *  The list of column/row intervals.
     *
     * @throws
     *  An `OperationError`, if the passed string cannot be parsed to a valid
     *  column/row interval list.
     */
    parseIntervalList(parseStr: string, columns: boolean): IntervalArray {
        // the own method `parseInterval()` will throw for any single invalid token; the
        // method `AddressFactory.parseIntervalList()` would silently filter invalid tokens
        const intervals = IntervalArray.map(str.splitTokens(parseStr), token => this.parseInterval(token, columns));
        this.ensure(!intervals.empty(), "unexpected empty index interval list");
        return intervals;
    }

    /**
     * Returns the value of a required column/row interval list property (as
     * string of space-separated column/row intervals in A1 notation) of the
     * wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param columns
     *  Whether to parse column intervals (`true`, e.g. "A:C E:F"), or row
     *  intervals (`false`, e.g. "1:3 5:6").
     *
     * @returns
     *  The list of column/row intervals of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  column/row interval list property with the specified name.
     */
    getIntervalList(propName: string, columns: boolean): IntervalArray {
        return this.parseIntervalList(this.getStr(propName), columns);
    }

    /**
     * Returns the value of an optional column/row interval list property (as
     * string of space-separated column/row intervals in A1 notation) of the
     * wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param columns
     *  Whether to parse column intervals (`true`, e.g. "A:C E:F"), or row
     *  intervals (`false`, e.g. "1:3 5:6").
     *
     * @returns
     *  The list of column/row intervals of the specified operation property;
     *  or `undefined`, if the property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid column/row
     *  interval list.
     */
    optIntervalList(propName: string, columns: boolean): Opt<IntervalArray> {
        return this.has(propName) ? this.getIntervalList(propName, columns) : undefined;
    }

    /**
     * Parses a cell address list from a string of space-separated cell
     * addresses in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The list of cell addresses.
     *
     * @throws
     *  An `OperationError`, if the passed string cannot be parsed to a valid
     *  cell address list.
     */
    parseAddressList(parseStr: string): AddressArray {
        // the own method `parseAddress()` will throw for any single invalid token; the
        // method `AddressFactory.parseAddressList()` would silently filter invalid tokens
        const addresses = AddressArray.map(str.splitTokens(parseStr), token => this.parseAddress(token));
        this.ensure(!addresses.empty(), "unexpected empty cell address list");
        return addresses;
    }

    /**
     * Returns the value of a required cell address list property (as string of
     * space-separated addresses in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The list of cell addresses of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  cell address list property with the specified name.
     */
    getAddressList(propName: string): AddressArray {
        return this.parseAddressList(this.getStr(propName));
    }

    /**
     * Returns the value of an optional cell address list property (as string
     * of space-separated addresses in A1 notation) of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The list of cell addresses of the specified operation property; or
     *  `undefined`, if the property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid cell address
     *  list.
     */
    optAddressList(propName: string): Opt<AddressArray> {
        return this.has(propName) ? this.getAddressList(propName) : undefined;
    }

    /**
     * Parses a cell range list from a string of space-separated addresses in
     * A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The list of cell range addresses.
     *
     * @throws
     *  An `OperationError`, if the passed string cannot be parsed to a valid
     *  cell range list.
     */
    parseRangeList(parseStr: string): RangeArray {
        // the own method `parseRange()` will throw for any single invalid token; the
        // method `AddressFactory.parseRangeList()` would silently filter invalid tokens
        const ranges = RangeArray.map(str.splitTokens(parseStr), token => this.parseRange(token));
        this.ensure(!ranges.empty(), "unexpected empty cell range list");
        return ranges;
    }

    /**
     * Returns the value of a required cell address list property (as string of
     * space-separated addresses in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The list of cell range addresses of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  cell range list property with the specified name.
     */
    getRangeList(propName: string): RangeArray {
        return this.parseRangeList(this.getStr(propName));
    }

    /**
     * Returns the value of an optional cell range list property (as string of
     * space-separated addresses in A1 notation) of the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The list of cell range addresses of the specified operation property;
     *  or `undefined`, if the property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid cell range
     *  list.
     */
    optRangeList(propName: string): Opt<RangeArray> {
        return this.has(propName) ? this.getRangeList(propName) : undefined;
    }
}
