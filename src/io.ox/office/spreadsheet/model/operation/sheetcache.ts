/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import { ColRowCache } from "@/io.ox/office/spreadsheet/model/operation/colrowcache";
import { type ResolveColRowStyleFn, CellCache } from "@/io.ox/office/spreadsheet/model/operation/cellcache";
import { DrawingCache } from "@/io.ox/office/spreadsheet/model/operation/drawingcache";
import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * Configuration options for instances of the class `SheetCache`.
 */
export interface SheetCacheConfig {

    /**
     * The address transformer representing a move operation in the sheet
     * (insertion/deletion of rows or columns, or a moved cell range). The
     * column, row, and cell cache will initialize themselves, e.g. will
     * register styles and formatting for inserted columns and rows which are
     * needed later to generate the correct auto-styles for cells inside these
     * ranges.
     */
    transformer?: AddressTransformer;

    /**
     * If set to `true`, the address transformer in the option `transformer`
     * represents a "moveCells" operation. The column and row cache will not be
     * initialized with the transformer in this case, i.e. no column/row move
     * operations will be generated.
     */
    moveCells?: boolean;

    /**
     * The index of a sheet to be inserted into all sheet operations using the
     * methods of the class `SheetOperationGenerator`. Default is the index of
     * the shet associated to the `SheetCache` instance.
     */
    targetSheet?: number;

    /**
     * Whether to skip creating the document operations. Useful as performance
     * optimization, when only the undo operations are of interest. Default
     * value is `false`.
     */
    skipOps?: boolean;

    /**
     * Whether to skip creating the undo operations. Useful as performance
     * optimization, when undo operations are not of interest. Default value is
     * `false`.
     */
    skipUndo?: boolean;

    /**
     * If set to `true`, operations for export filters will be created (remote
     * editor clients will ignore the operations). Default value is `false`.
     */
    filter?: boolean;
}

/**
 * The changed columns, rows, and cells after applying all collected changes of
 * a `SheetCache` have been generated.
 */
export interface SheetCacheFlushResult {

    /**
     * The intervals of all columns that have been changed.
     */
    changedCols: IntervalArray;

    /**
     * The intervals of all columns that have been changed.
     */
    changedRows: IntervalArray;

    /**
     * The addresses of all cell ranges containing changed cells.
     */
    changedCells: RangeArray;

    /**
     * Whether the effective size of the columns/rows will be changed by the
     * operations (either by changing the size, or by hiding/unhiding).
     */
    resizeEntries: boolean;
}

// class SheetCache ===========================================================

/**
 * A helper class to generate operations and the related undo operations for
 * contents and formatting of columns, rows, and cells in a sheet.
 *
 * @param sheetModel
 *  The model of the sheet this instance is associated to.
 *
 * @param builder
 *  The parent operation builder needed to generate the cell auto-styles.
 *
 * @param [config]
 *  Configuration options.
 */
export class SheetCache extends SheetOperationGenerator implements Destroyable {

    // properties -------------------------------------------------------------

    // the related sheet model
    readonly sheetModel: SheetModel;

    // the style cache needed to generate cell auto-styles
    readonly styleCache: SheetStyleCache;

    // the column cache needed to generate column formatting operations
    readonly colCache: ColRowCache;

    // the row cache needed to generate row formatting operations
    readonly rowCache: ColRowCache;

    // the cell cache needed to generate cell content operations
    readonly cellCache: CellCache;

    // the drawing cache needed to generate drawing operations
    readonly drawingCache: DrawingCache;

    // the configuration settings passed to the constructor
    readonly #config: SheetCacheConfig;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, styleCache: SheetStyleCache, config?: SheetCacheConfig) {
        super(sheetModel.docModel, { sheet: config?.targetSheet ?? sheetModel.getIndex() });
        this.sheetModel = sheetModel;
        this.styleCache = styleCache;
        this.colCache = new ColRowCache(sheetModel, styleCache, true, config);
        this.rowCache = new ColRowCache(sheetModel, styleCache, false, config);
        this.cellCache = new CellCache(sheetModel, styleCache, config);
        this.drawingCache = new DrawingCache(sheetModel, config);
        this.#config = { ...config };
    }

    // public methods ---------------------------------------------------------

    destroy(): void {
        this.drawingCache.destroy();
        this.cellCache.destroy();
        this.rowCache.destroy();
        this.colCache.destroy();
    }

    /**
     * Returns the specified cache for columns or rows.
     *
     * @param columns
     *  Whether to return the column cache (`true`), or the row cache (`false`).
     *
     * @returns
     *  The specified cache for columns or rows.
     */
    getColRowCache(columns: boolean): ColRowCache {
        return columns ? this.colCache : this.rowCache;
    }

    /**
     * Generates the `changeCells` operations for all cells collected by this
     * instance, and inserts all collected operations into the passed operation
     * generator.
     *
     * @param generator
     *  The operation generator to be filled with all sheet operations.
     *
     * @returns
     *  The addresses of all cell ranges containing changed cells.
     */
    @modelLogger.profileMethod("$badge{SheetCache} flushOperations")
    flushOperations(generator: SheetOperationGenerator): SheetCacheFlushResult {

        // flush all column content operations
        const colResult = this.colCache.flushOperations(this);
        const resolveColStyleId = colResult.resolveStyleFn;

        // flush all row content operations
        const rowResult = this.rowCache.flushOperations(this);
        const resolveRowStyleId = colResult.resolveStyleFn;

        // the resolver function for the default auto-style of the column/row below a cell
        const resolveStyleId: ResolveColRowStyleFn = address => resolveRowStyleId(address.r) ?? resolveColStyleId(address.c) ?? this.styleCache.defStyleId;
        // flush all cell content operations
        const changedCells = this.cellCache.flushOperations(this, resolveStyleId);

        // append collected regular operations
        if (!this.#config.skipOps) {
            generator.appendOperations(this);
        }

        // append collected undo operations
        if (!this.#config.skipUndo) {
            generator.appendOperations(this, { undo: true });
        }

        // return the result object
        return {
            changedCols: colResult.changedIntervals,
            changedRows: rowResult.changedIntervals,
            changedCells,
            resizeEntries: colResult.resizeEntries || rowResult.resizeEntries
        };
    }
}
