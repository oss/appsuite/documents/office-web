/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, to, dict } from "@/io.ox/office/tk/algorithms";

import { type OpBorder, NO_BORDER, isVisibleBorder, equalBorders } from "@/io.ox/office/editframework/utils/border";
import { cloneAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { GeneratorConfig, GeneratorUndoOptions } from "@/io.ox/office/editframework/model/operation/generator";
import { StyleCache } from "@/io.ox/office/editframework/model/operation/stylecache";

import type { CellAttributeSet, PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import type { OuterBorderKey, OuterBorderName, AttrBorderKey, AttrBorderName } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { ATTR_BORDER_KEYS, getBorderName, getOuterBorderKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { FormatCodeSpec } from "@/io.ox/office/spreadsheet/model/numberformatter";
import type { AutoStyleBorders } from "@/io.ox/office/spreadsheet/model/style/cellautostylecollection";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * A descriptor with all properties needed to change the settings of a cell
 * auto-style.
 */
export interface StyleChangeSet {

    /**
     * The style identifier of a new auto-style. The empty string can be used
     * for the default auto-style of the document.
     */
    s?: string;

    /**
     * An incomplete attribute set to be merged with the existing auto-style.
     */
    a?: PtCellAttributeSet;

    /**
     * The identifier of a number format, or an explicit format code.
     */
    format?: FormatCodeSpec;

    /**
     * An attribute set resolved form a table style sheet, to be merged "under"
     * the explicit cell attributes and cell style.
     */
    table?: Dict;

    /**
     * A hash key for the formatting attributes, used to improve performance
     * when processing the same original cell auto-styles multiple times.
     */
    cacheKey?: string;

    /**
     * A callback function that allows to modify the cell attribute set before
     * the operation for a new auto-style will be generated.
     *
     * @param attrSet
     *  A deep copy of the resulting cell attribute set to be merged with an
     *  auto-style. SHALL be modified in-place by the callback function.
     *
     * @param mergedAttrSet
     *  The current merged attribute set of the auto-style. MUST NOT be changed
     *  by the callback function.
     */
    processFn?(attrSet: Dict, mergedAttrSet: Readonly<Dict>): void;
}

/**
 * Specifier for a border type in a `BorderFillData` structure.
 */
export type BorderFillDataCacheType = "outer" | "inner" | "clear";

/**
 * Fill data for border attribute generators. The operation builder will merge
 * multiple fill data structures for cells with overlapping border ranges.
 */
export interface BorderFillData {
    key: OuterBorderKey;
    cacheType: BorderFillDataCacheType;
    border?: OpBorder;
    keepBorder?: OpBorder;
}

/**
 * Fill data for border attribute generators manipulating the appearance of
 * existing visible borders (new line style, line color, etc.). The operation
 * builder will merge multiple fill data structures for cells with overlapping
 * border ranges.
 */
export interface VisibleBorderFillData {
    keys: readonly AttrBorderKey[];
}

// constants ==================================================================

// cache key for an outer, inner, and deleted border line (to be combined with `CACHE_KEY_MULTIPLIERS`)
const BORDER_CACHE_FLAGS: Record<BorderFillDataCacheType, number> = { outer: 1, inner: 2, clear: 3 };

// factors used to build unique cache keys for generating border auto-styles
const CACHE_KEY_MULTIPLIERS: Record<OuterBorderKey, number> = { t: 1, b: 4, l: 16, r: 64 };

// bit flags used to build unique cache keys for generating border auto-styles
const VISIBLE_BORDER_CACHE_FLAGS: Record<AttrBorderKey, number> = { t: 1, b: 2, l: 4, r: 8, d: 16, u: 32 };

// private functions ==========================================================

/**
 * Helper callback function used to finalize the attribute set for a new
 * auto-style, intended to be added into a contents object for an interval or
 * cell range.
 */
function processBorderAttributes(newAttrSet: Dict, mergedAttrSet: Readonly<Dict>, keepBorderAttrs: Dict<OpBorder>): void {

    // shortcut to the current cell attributes of the auto-style
    const oldBorderAttrs = mergedAttrSet.cell as Dict;
    // shortcut to the new border attributes
    const newBorderAttrs = newAttrSet.cell as Dict;

    // update the existing borders of the auto-style, delete other border attributes from the attribute set
    ATTR_BORDER_KEYS.forEach(borderKey => {

        // nothing to do without an explicit new border attribute
        const borderName = getBorderName(borderKey);
        if (!(borderName in newBorderAttrs)) { return; }

        // the old and new border attribute
        const oldBorder = oldBorderAttrs[borderName];
        const newBorder = newBorderAttrs[borderName];

        // do not apply invisible borders (do not set the 'border' apply flag to the auto-style), if the old
        // border is already invisible, or if the old border is equal to the border attribute passed in the
        // parameter 'keepBorderAttrs' (do not change existing borders of adjacent cells outside the ranges)
        if (!isVisibleBorder(newBorder)) {
            if (!isVisibleBorder(oldBorder) || equalBorders(oldBorder, keepBorderAttrs[borderName])) {
                delete newBorderAttrs[borderName];
            }
        }
    });
}

/**
 * Helper callback function used to finalize the attribute set for a new
 * auto-style, intended to be added into a contents object for an interval or
 * cell range.
 */
function processVisibleBorderAttributes(newAttrSet: Dict, mergedAttrSet: Readonly<Dict>): void {

    // shortcut to the current cell attributes of the auto-style
    const oldBorderAttrs = mergedAttrSet.cell as Dict;
    // shortcut to the new border attributes
    const newBorderAttrs = newAttrSet.cell as Dict;

    // update the existing borders of the auto-style, delete other border attributes from the attribute set
    ATTR_BORDER_KEYS.forEach(borderKey => {

        // nothing to do without an explicit new border attribute
        const borderName = getBorderName(borderKey);
        if (!(borderName in newBorderAttrs)) { return; }

        // the old and new border attribute
        const oldBorder = oldBorderAttrs[borderName];
        let newBorder = newBorderAttrs[borderName];

        // extend visible borders of the auto-style only, ignore other borders (by removing them from the attribute set)
        if (isVisibleBorder(oldBorder)) {
            newBorder = is.dict(newBorder) ? { ...oldBorder, ...newBorder } : { ...oldBorder };
            newBorderAttrs[borderName] = isVisibleBorder(newBorder) ? newBorder : NO_BORDER;
        } else {
            delete newBorderAttrs[borderName];
        }
    });
}

function getStyleCacheKey(styleId: string, cacheKey?: string): Opt<string> {
    return cacheKey ? `${cacheKey}\0${styleId}` : undefined;
}

// public functions ===========================================================

/**
 * Helper callback function for the column, row, and cell collections, used to
 * create the change sets for cell ranges or column/row intervals expected by
 * the content generators, while adding or removing border lines inside a cell
 * selection.
 *
 * @param fillDataArray
 *  An array of fill data objects created by the operation generators for cell
 *  formatting, or column/row formatting.
 *
 * @param cacheKey
 *  The root cache key used to optimize creation of new cell auto-styles,
 *  intended to be reused in different generators (columns, rows, and cells)
 *  while creating the operations for the same border settings.
 *
 * @returns
 *  The change set with the converted border settings.
 */
export function createBorderChangeSet(fillDataArray: readonly BorderFillData[], cacheKey: string): StyleChangeSet {

    // fill data objects, divided by border keys
    const fillDataMap = new Map<OuterBorderKey, BorderFillData>();
    // the resulting border attributes to be inserted into the contents object
    const borderAttrs = dict.createRec<OuterBorderName, OpBorder>();
    // border attributes to be kept while deleting adjacent outer borders
    const keepBorderAttrs = dict.createRec<OuterBorderName, OpBorder>();
    // a bitmask for the auto-style cache, according to which borders will be changed
    let cacheFlags = 0;

    // collect the fill data objects separated by border key
    fillDataArray.forEach(fillData => {
        const destFillData = fillDataMap.get(fillData.key);
        if (destFillData) {
            Object.assign(destFillData, fillData);
        } else {
            fillDataMap.set(fillData.key, { ...fillData });
        }
    });

    // collect the resulting border attributes for each border key
    for (const [key, borderData] of fillDataMap) {
        const borderName = getBorderName(key);
        const multiplier = CACHE_KEY_MULTIPLIERS[key];
        if (borderData.border) {
            borderAttrs[borderName] = borderData.border;
            cacheFlags |= multiplier * BORDER_CACHE_FLAGS[borderData.cacheType];
        } else if (borderData.keepBorder) {
            borderAttrs[borderName] = NO_BORDER;
            keepBorderAttrs[borderName] = borderData.keepBorder;
            cacheFlags |= multiplier * BORDER_CACHE_FLAGS.clear;
        }
    }

    // return the contents object with border properties, auto-style cache key, and attribute set processor
    return {
        a: { cell: borderAttrs },
        cacheKey: `${cacheKey}:${cacheFlags}`,
        processFn(newAttrSet, mergedAttrSet): void {
            processBorderAttributes(newAttrSet, mergedAttrSet, keepBorderAttrs);
        }
    };
}

/**
 * Helper callback function for the column, row, and cell collections, used to
 * create the change sets for cell ranges or column/row intervals expected by
 * the content generators, while modifying the properties of existing borders
 * inside a cell selection.
 *
 * @param fillDataArray
 *  An array of fill data objects created by the operation generators for cell
 *  formatting, or column/row formatting. Each object must contain a string
 *  property `keys` specifying which cell borders to be changed with the passed
 *  border properties.
 *
 * @param border
 *  The border properties to be changed with the generated operations. All
 *  properties contained in this object (color, line style, line width) will be
 *  set at existing visible borders. Omitted border properties will not be
 *  changed.
 *
 * @param cacheKey
 *  The root cache key used to optimize creation of new auto-styles, intended
 *  to be reused in different generators (columns, rows, and cells) while
 *  creating the operations for the same border settings.
 *
 * @returns
 *  The change set with the converted border settings.
 */
export function createVisibleBorderChangeSet(fillDataArray: readonly VisibleBorderFillData[], border: OpBorder, cacheKey: string): StyleChangeSet {

    // the border attributes for all cell borders to be changed
    const borderAttrs = dict.createRec<AttrBorderName, OpBorder>();
    // a bitmask for the auto-style cache, according to which borders will be changed
    let cacheFlags = 0;

    // add all borders that will be changed accoding to the different fill data objects
    fillDataArray.forEach(fillData => {
        fillData.keys.forEach(key => {
            borderAttrs[getBorderName(key)] = border;
            cacheFlags |= VISIBLE_BORDER_CACHE_FLAGS[key];
        });
    });

    // return the contents object with border properties, auto-style cache key, and attribute set processor
    return {
        a: { cell: borderAttrs },
        cacheKey: `${cacheKey}:${cacheFlags}`,
        processFn: processVisibleBorderAttributes
    };
}

// class SheetStyleCache ======================================================

/**
 * A helper class for generating and caching operations for auto-styles, style
 * sheets, and indexed number formats in spreadsheets.
 */
export class SheetStyleCache extends StyleCache<SpreadsheetModel> {

    // properties -------------------------------------------------------------

    /** Cache for number format attributes that have been generated already. */
    readonly numFmtAttrCache = new Map<string, Dict>();

    /** The style identifier of the default auto-style. */
    readonly defStyleId: string;

    /** Next free number format identifier. */
    nextFormatId?: number;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, config?: GeneratorConfig) {
        super(docModel, config);
        this.defStyleId = docModel.autoStyles.getDefaultStyleId();
    }

    // public methods ---------------------------------------------------------

    /**
     * Generates an operation for an indexed number format.
     *
     * @param name
     *  The name of the operation.
     *
     * @param formatId
     *  The format identifier.
     *
     * @param props
     *  Additional properties to be inserted into the document operation.
     *
     * @param [options]
     *  Optional parameters.
     */
    generateNumberFormatOperation(name: string, formatId: number, props?: Dict, options?: GeneratorUndoOptions): void {
        this.generateOperation(name, { id: formatId, ...props }, options);
    }

    /**
     * Returns the style identifier of the auto-style containing the merged
     * cell formatting attributes of the passed auto-style, and the attribute
     * set contained in the contents object. Tries to resolve a cached
     * auto-style identifier that has been generated in a previous invocation.
     *
     * @param oldStyleId
     *  The style identifier of the original auto-style to be merged with new
     *  cell formatting attributes.
     *
     * @param changeSet
     *  The formatting properties to be merged with the specified auto-style.
     *
     * @returns
     *  The style identifier of the resulting auto-style containing the merged
     *  cell formatting attributes of the original auto-style, and the
     *  formatting settings in the contents object.
     */
    generateAutoStyleOperations(oldStyleId: string, changeSet: StyleChangeSet): string {

        // try to resolve an existing cached auto-style identifier
        const cachedStyleId = this.#getCachedStyleId(oldStyleId, changeSet.cacheKey);
        if (cachedStyleId) { return cachedStyleId; }

        // changeset may contain an auto-style identifier that replaces the auto-style `oldStyleId`
        const styleId = is.string(changeSet.s) ? changeSet.s : oldStyleId;
        // the attribute set with the new attributes to be inserted into the auto-style
        let attrSet = is.dict(changeSet.a) ? cloneAttributeSet(changeSet.a) : undefined;

        // shortcuts to document collections
        const { numberFormatter, autoStyles } = this.docModel;

        // resolve custom number format to formatting attributes
        const formatAttrs = (changeSet.format === undefined) ? undefined : numberFormatter.generateNumberFormatOperations(this, changeSet.format);
        if (formatAttrs) {
            attrSet ??= dict.create();
            attrSet.cell ??= dict.create();
            Object.assign(attrSet.cell, formatAttrs);
        }

        // the merged attribute set of the original auto-style
        const mergedAttrSet = autoStyles.getMergedAttributeSet(oldStyleId);

        // merge the table style attributes below the auto-style and explicit attributes
        if (changeSet.table) {
            const tmpAttrSet = autoStyles.extendAttrSet(mergedAttrSet, attrSet, { clone: true }) as CellAttributeSet;
            attrSet = autoStyles.extendWithTableAttributeSet(to.dict(attrSet, true), tmpAttrSet, changeSet.table);
        }

        // nothing to do without any new custom attributes
        if (is.empty(attrSet)) {
            return this.#setCachedStyleId(styleId, styleId, changeSet.cacheKey);
        }

        // callback function wants to preprocess the attributes
        changeSet.processFn?.(attrSet!, mergedAttrSet);

        // merge explicit formatting attributes to the resulting auto-style
        const newStyleId = autoStyles.generateAutoStyleOperations(this, styleId, attrSet!);

        // put the new auto-style identifier into the cache
        return this.#setCachedStyleId(oldStyleId, newStyleId, changeSet.cacheKey);
    }

    /**
     * Returns the style identifier of an auto-style containing the formatting
     * that results when inserting content between two objects with the
     * specified auto-styles (e.g. when inserting columns or rows, or shifting
     * cells in the sheet to the right or down).
     *
     * @param styleId1
     *  The style identifier of the auto-style of the leading object.
     *
     * @param styleId2
     *  The style identifier of the auto-style of the trailing object.
     *
     * @param horizontal
     *  Whether the leading object containing the first auto-style is located
     *  left of the trailing object with the second auto-style (`true`), or on
     *  top of the trailing object (`false`).
     *
     * @returns
     *  The style identifier of the auto-style to be set to the objects
     *  inserted between the existing objects with the passed auto-styles.
     */
    generateInsertedAutoStyleOperations(styleId1: string, styleId2: string, horizontal: boolean): string {

        // nothing to do if styles are equal
        const { autoStyles } = this.docModel;
        if (autoStyles.areEqualStyleIds(styleId1, styleId2)) { return styleId1; }

        // try the cached auto-styles first
        const cacheKey = `${styleId1}\0${styleId2}`;
        const newStyleId = this.#getCachedStyleId(styleId1, cacheKey);
        if (newStyleId) { return newStyleId; }

        // early exit if none of the auto-styles contains any visible borders
        const borderMap1 = autoStyles.getBorderMap(styleId1);
        const borderMap2 = autoStyles.getBorderMap(styleId2);
        if (is.empty(borderMap1) && is.empty(borderMap2)) { return styleId1; }

        // returns whether the passed cell attribute map contains two visible and equal border attributes
        const equalBordersForKeys = (borderMap: AutoStyleBorders, key1: OuterBorderKey, key2: OuterBorderKey): boolean => {
            const border1 = borderMap[key1], border2 = borderMap[key2];
            return !!border1 && !!border2 && equalBorders(border1, border2);
        };

        // returns the border attribute, if it is visible and equal in both border maps, otherwise null
        const getMergedBorder = (key1: OuterBorderKey, key2: OuterBorderKey): OpBorder | null => {
            const border1 = borderMap1[key1], border2 = borderMap2[key2];
            return (border1 && border2 && equalBorders(border1, border2)) ? border1 : null;
        };

        // the new border attributes to be applied over the leading auto-style
        const borderAttrs = dict.create<OpBorder | null>();

        // copy leading borders along move direction if equal in both objects (e.g. top borders of two adjacent cells when inserting columns)
        const alongKey1 = getOuterBorderKey(!horizontal, true);
        borderAttrs[getBorderName(alongKey1)] = getMergedBorder(alongKey1, alongKey1);

        // copy trailing borders along move direction if equal in both objects (e.g. bottom borders of two adjacent cells when inserting columns)
        const alongKey2 = getOuterBorderKey(!horizontal, false);
        borderAttrs[getBorderName(alongKey2)] = getMergedBorder(alongKey2, alongKey2);

        // copy the inner overlapping border of the objects, but only if *both* (leading and trailing) borders in either of the objects are equal
        const acrossKey1 = getOuterBorderKey(horizontal, true);
        const acrossKey2 = getOuterBorderKey(horizontal, false);
        const innerBorder = getMergedBorder(acrossKey2, acrossKey1);
        const areEqual = innerBorder && (equalBordersForKeys(borderMap1, acrossKey1, acrossKey2) || equalBordersForKeys(borderMap2, acrossKey1, acrossKey2));
        borderAttrs[getBorderName(acrossKey1)] = borderAttrs[getBorderName(acrossKey2)] = areEqual ? innerBorder : null;

        // create and cache the new auto-style
        return this.generateAutoStyleOperations(styleId1, { a: { cell: borderAttrs }, cacheKey });
    }

    // private methods --------------------------------------------------------

    /**
     * Tries to resolve a cached auto-style identifier.
     *
     * @param oldStyleId
     *  The style identifier of the old auto-style.
     *
     * @param [cacheKey]
     *  The unique key used to look-up a cached auto-style identifier for the
     *  specified auto-style. If this parameter has been omitted, no cache
     *  lookup will be performed.
     *
     * @returns
     *  The cached style identifier of a auto-style, if available; otherwise
     *  `undefined`.
     */
    #getCachedStyleId(oldStyleId: string, cacheKey?: string): Opt<string> {
        const key = getStyleCacheKey(oldStyleId, cacheKey);
        return key ? this.autoStyleIdCache.get(key) : undefined;
    }

    /**
     * Inserts an auto-style identifier into the internal cache.
     *
     * @param oldStyleId
     *  The style identifier of the old auto-style for which the new auto-style
     *  identifier will be cached.
     *
     * @param newStyleId
     *  The style identifier of the new auto-style to be inserted into the
     *  internal cache.
     *
     * @param [cacheKey]
     *  The unique key used to cache the new auto-style identifier. If this
     *  parameter has been omitted, the cache will not be modified.
     *
     * @returns
     *  The parameter `newStyleId`.
     */
    #setCachedStyleId(oldStyleId: string, newStyleId: string, cacheKey?: string): string {
        const key = getStyleCacheKey(oldStyleId, cacheKey);
        if (key) { this.autoStyleIdCache.set(key, newStyleId); }
        return newStyleId;
    }
}
