/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DObject } from "@/io.ox/office/tk/objects";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";

import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { type CellAnchorPoint, AnchorMode, SheetDrawingAnchor } from "@/io.ox/office/spreadsheet/utils/cellanchor";
import type { SheetCacheConfig } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { ColRowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SheetDrawingModelType } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";

const { min, floor } = Math;

// types ======================================================================

/**
 * The type of the result of transforming a drawing anchor for a move operation
 * in a sheet.
 *
 * - The value `undefined`, if the drawing object does not need to be updated;
 * - The anchor attributes that need to be applied at the drawing object to
 *   update its location;
 * - The string "delete" to indicate that the drawing object will be deleted
 *   from the sheet by the move operation.
 */
export type TransformAnchorResult = Opt<Dict> | "delete";

// private types --------------------------------------------------------------

/**
 * Data about an (inserted or deleted) column/row interval.
 */
interface MoveInfo {

    /**
     * The position of the interval.
     */
    interval: Interval;

    /**
     * The start offset of the transformed interval, in 1/100mm.
     */
    offset: number;

    /**
     * The absolute size of the entire interval, in 1/100mm.
     */
    size: number;
}

// class AnchorTransformer ====================================================

/**
 * Helper class for transforming drawing anchors for move operations. Will be
 * created on demand, and caches prepared data that will be reused for multiple
 * drawing objects.
 */
class AnchorTransformer {

    // properties -------------------------------------------------------------

    // the address transformer representing the move operation
    readonly #transformer: AddressTransformer;

    // the column/row collection used to convert between indexes and offsets
    readonly #collection: ColRowCollection;

    // total move size of all target intervals in the sheet
    readonly #moveInfos: MoveInfo[] = [];

    // positions of new interval inserted at the end of the sheet in deletion mode
    readonly #trailingInfo?: MoveInfo;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, transformer: AddressTransformer) {

        this.#transformer = transformer;
        const collection = this.#collection = transformer.columns ? sheetModel.colCollection : sheetModel.rowCollection;

        // the function to determine the size of an inserted/deleted source interval
        const getSizeFn: FuncType<number, [Interval]> = transformer.insert ?
            // the size of inserted columns/rows is equal to the size of the preceding column/row (ignoring hidden state)
            interval => {
                const model = (interval.first > 0) ? collection.getModel(interval.first - 1) : collection.defaultModel;
                return model.rawSizeHmm * interval.size();
            } :
            // the size of deleted columns/rows will be obtained from the collection directly
            interval => collection.getIntervalPosition(interval).size;

        // create the array entries for all source intervals
        const moveSign = transformer.insert ? 1 : -1;
        const totalSize = transformer.sourceIntervals.reduce((moveSize, sourceInterval, index) => {
            const interval = transformer.targetIntervals[index];
            const offset = collection.getEntryOffsetHmm(sourceInterval.first, 0) + moveSize * moveSign;
            const size = getSizeFn(sourceInterval);
            moveSize += size;
            this.#moveInfos.push({ interval, offset, size });
            return moveSize;
        }, 0);

        // create an entry for the trailing inserted insertval for deletion mode
        if (!transformer.insert) {
            const interval = transformer.insertIntervals[0];
            const offset = collection.getTotalSize() - totalSize;
            const size = collection.defaultModel.rawSizeHmm * interval.size();
            this.#trailingInfo = { interval, offset, size };
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Calculates the cell anchor attributes needed to update the location of
     * the passed top-level drawing object, when cells will be moved in the
     * sheet.
     *
     * @param drawingModel
     *  The model of the drawing object to be updated.
     *
     * @returns
     *  The resulting anchor attributes.
     */
    generateMovedAnchorAttributes(drawingModel: SheetDrawingModelType): TransformAnchorResult {

        // the complete anchor descriptor with current settings
        const sheetAnchor = drawingModel.getSheetAnchor({ displayMode: true });
        const oldCellAnchor = sheetAnchor.cellAnchor;

        // transform the cell anchor object (may return `undefined` except for notes/comments which will never be deleted)
        const newCellAnchor = this.#transformer.transformCellAnchor(oldCellAnchor, { move: drawingModel.drawingType === DrawingType.NOTE });
        if (!newCellAnchor) { return "delete"; }

        // move direction
        const { columns } = this.#transformer;
        // the resulting location of the drawing object, in 1/100 mm
        const newRectHmm = sheetAnchor.rectHmm.clone();

        // calculate the missing properties of the top-left corner
        switch (sheetAnchor.anchorMode) {

            // calculate the new top-left anchor cell for current absolute position
            case AnchorMode.ABSOLUTE: {
                const offset1 = columns ? newRectHmm.left : newRectHmm.top;
                this.#updateAnchorPoint(newCellAnchor.point1, offset1);
                break;
            }

            // calculate the new absolute position for the new anchor cell
            case AnchorMode.ONE_CELL:
            case AnchorMode.TWO_CELL: {
                const offset1 = this.#calcAbsOffset(newCellAnchor.point1);
                newRectHmm[columns ? "left" : "top"] = offset1;
                break;
            }
        }

        // calculate the missing properties of the bottom-right corner
        switch (sheetAnchor.anchorMode) {

            // calculate the new bottom-right anchor cell from transformed absolute position
            case AnchorMode.ABSOLUTE:
            case AnchorMode.ONE_CELL: {
                const offset2 = columns ? newRectHmm.right() : newRectHmm.bottom();
                this.#updateAnchorPoint(newCellAnchor.point2, offset2);
                break;
            }

            // calculate the new absolute size
            case AnchorMode.TWO_CELL: {
                const offset1 = columns ? newRectHmm.left : newRectHmm.top;
                const offset2 = this.#calcAbsOffset(newCellAnchor.point2);
                newRectHmm[columns ? "width" : "height"] = offset2 - offset1;
                break;
            }
        }

        // check if the anchor attributes do change at all
        const anchorAttrs = SheetDrawingAnchor.createAttributes(newCellAnchor, newRectHmm);
        return drawingModel.getChangedAnchorAttributes(anchorAttrs);
    }

    // private methods --------------------------------------------------------

    /**
     * Calculates the new absolute offset for the passed transformed anchor
     * point.
     *
     * @param anchorPoint
     *  The transformed anchor point to be converted to the absolute offset.
     *
     * @returns
     *  The transformed absolute offset for the passed anchor point.
     */
    #calcAbsOffset(anchorPoint: CellAnchorPoint): number {

        // shortcuts to transformer properties
        const { columns, insert } = this.#transformer;

        // transform passed column/row index back to its current position
        const newIndex = anchorPoint.a.get(columns);
        const oldIndex = this.#transformer.transformIndex(newIndex, { reverse: true, move: true });

        // a predicate that decides whether a `MoveInfo` affects the passed anchor point, depending on move direction
        const isAffected: (moveInfo: MoveInfo) => boolean = insert ?
            moveInfo => moveInfo.interval.first <= newIndex :
            moveInfo => moveInfo.interval.last < oldIndex;

        // transform the current absolute offset in the column/row collection
        let absOffset = this.#collection.getEntryOffsetHmm(oldIndex, columns ? anchorPoint.x : anchorPoint.y);
        const moveSign = insert ? 1 : -1;
        for (const moveInfo of this.#moveInfos) {
            if (!isAffected(moveInfo)) { break; }
            absOffset += moveSign * moveInfo.size;
        }
        return absOffset;
    }

    /**
     * Calculates and updates the transformed cell index and offset for the
     * passed absolute offset in the passed anchor point structure.
     *
     * @param anchorPoint
     *  The anchor point to be updated in-place.
     *
     * @param absOffset
     *  The absolute offset to be converted to a cell index/offset pair.
     */
    #updateAnchorPoint(anchorPoint: CellAnchorPoint, absOffset: number): void {

        // shortcuts to transformer properties
        const { columns, insert, maxIndex } = this.#transformer;

        // Find current location of the new anchor cell by shifting the passed offset back (insertion mode) or ahead
        // (deletion mode) until it lands behind an inserted/deleted interval (because the move operation will shift
        // that cell to the desired absolute offset afterwards).
        let totalSize = 0;
        for (const moveInfo of this.#moveInfos) {

            // exit loop, if the interval is located behind the offset
            if (absOffset < moveInfo.offset) { break; }

            // Determine whether the point will land inside a new blank interval (also for deletion mode where the
            // offset might land inside the new interval inserted at the end of the sheet).
            const insertInfo = this.#trailingInfo ?? moveInfo;
            if ((insertInfo.offset <= absOffset) && (absOffset < insertInfo.offset + insertInfo.size)) {
                // all entries in inserted intervals have the same size
                const relOffset = absOffset - insertInfo.offset;
                const size = insertInfo.size / insertInfo.interval.size();
                const index = min(maxIndex, insertInfo.interval.first + floor(relOffset / size));
                anchorPoint.set(index, relOffset % size, columns);
                return;
            }

            // sum up the distance needed to move the offset
            totalSize += moveInfo.size;
        }

        // Shift the offset in reverse direction to make it point to the *current* column/row that will be moved to the
        // offset by the address transformer. This column/row index can be transformed then using the address
        // transformer to find the resulting anchor position.
        const oldOffset = absOffset + (insert ? -totalSize : totalSize);
        const entryDesc = this.#collection.getEntryByOffset(oldOffset);
        const index = this.#transformer.transformIndex(entryDesc.index, { move: true });
        anchorPoint.set(index, entryDesc.relOffsetHmm, columns);
    }
}

// class DrawingCache =========================================================

/**
 * A helper class to generate operations and the related undo operations for
 * the formatting attributes of drawing objects in a sheet.
 *
 * @param sheetModel
 *  The model of the sheet this instance is associated to.
 *
 * @param [config]
 *  Configuration options.
 */
export class DrawingCache extends DObject {

    // properties -------------------------------------------------------------

    // the sheet model
    readonly #sheetModel: SheetModel;

    // the address transformer for moving interval indexes
    readonly #transformer?: AddressTransformer;

    // helper for transforming drawing anchors for move operations
    #anchorTransformer?: AnchorTransformer;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, config?: SheetCacheConfig) {
        super();

        this.#sheetModel = sheetModel;
        this.#transformer = config?.transformer;
    }

    // public methods ---------------------------------------------------------

    /**
     * Calculates the cell anchor attributes needed to update the location of
     * the passed top-level drawing object, when cells will be moved in the
     * sheet.
     *
     * @param drawingModel
     *  The model of the drawing object to be updated.
     *
     * @returns
     *  The resulting anchor attributes.
     */
    generateMovedAnchorAttributes(drawingModel: SheetDrawingModelType): TransformAnchorResult {

        // nothing to do for embedded drawing objects (in groups), or without address transformer (no move operation)
        if (drawingModel.isEmbedded || !this.#transformer) { return undefined; }

        // create the anchor transformer on demand
        this.#anchorTransformer ??= new AnchorTransformer(this.#sheetModel, this.#transformer);
        return this.#anchorTransformer.generateMovedAnchorAttributes(drawingModel);
    }
}
