/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, to, str, fun, ary, dict, pick, json } from "@/io.ox/office/tk/algorithms";

import { clonePosition } from "@/io.ox/office/editframework/utils/operations";
import type { OperationAction } from "@/io.ox/office/editframework/utils/operationutils";
import {
    type OTSortVector, type OTMoveResult,
    isOperationRemoved, setOperationRemoved,
    copyProperties, reduceProperties, transformAttribute, reduceOperationAttributes,
    transformIndexInsert, transformIndexDelete, transformIndexMove, transformIndexSort,
    transformIndexInsertInsert, transformIndexInsertDelete, transformIndexInsertMove, transformIndexInsertCopy, transformIndexInsertSort,
    transformIndexDeleteDelete, transformIndexDeleteMove, transformIndexDeleteCopy, transformIndexDeleteSort,
    transformIndexMoveMove, transformIndexMoveCopy, transformIndexMoveSort,
    transformIndexCopyCopy, transformIndexCopySort, transformIndexSortSort,
    transformPositionInsert, transformPositionDelete, transformPositionMove
} from "@/io.ox/office/editframework/utils/otutils";

import {
    type TransformOpsResult,
    OT_AUTOSTYLE_ALIAS, OT_CHART_ALIAS,
    OT_INSERT_CHAR_ALIAS, OT_INSERT_COMP_ALIAS, OT_MERGE_COMP_ALIAS, OT_SPLIT_COMP_ALIAS,
    TextBaseOTManager
} from "@/io.ox/office/textframework/model/otmanager";

import { MAX_SHEET_COUNT } from "@/io.ox/office/spreadsheet/utils/config";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { type OptSheet, SheetType, Direction, MergeMode, isSupportedSheetType, isVerticalDir } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import type { Position, Operation, OptAttrsOperation } from "@/io.ox/office/spreadsheet/utils/operations";
import { equalPositions, isCellData } from "@/io.ox/office/spreadsheet/utils/operations";
import * as Op from "@/io.ox/office/spreadsheet/utils/operations";

import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import type { TransformAddressOptions, TransformRangeOptions } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { TransformMode, AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { DocumentSnapshot } from "@/io.ox/office/spreadsheet/model/modelsnapshot";
import { containsResizeAttr } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

import type { FormulaType } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import type { TransformFormulaResultFn, FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { DeleteSheetUpdateTask, RenameSheetUpdateTask, MoveCellsUpdateTask, RelabelNameUpdateTask, RenameTableUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import type { FormulaParser } from "@/io.ox/office/spreadsheet/model/formula/parser/formulaparser";
import type { RangeListParser } from "@/io.ox/office/spreadsheet/model/formula/parser/rangelistparser";
import { BaseTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

const { min, max } = Math;

const { LEFT, RIGHT, UP, DOWN } = Direction;
const { RESIZE, EXPAND, STRICT, SPLIT, FIXED } = TransformMode;

// types ======================================================================

/**
 * Extends an instance of `AddressTransformer` with a sheet index property that
 * is needed in various transformation implementations.
 */
interface OTAddressTransformer extends AddressTransformer {
    sheetIndex: number;
}

/**
 * Generic shape of spreadsheet operations with (optional) sheet index (e.g.
 * `insertName`), intended to be used with `OT_SHEET_INDEX_ALIAS`.
 */
interface AnySheetOperation extends Operation {
    sheet?: number;
}

/**
 * Generic shape of spreadsheet operations with (optional) sheet name.
 */
interface AnySheetNameOperation extends Operation {
    sheetName?: string;
}

/**
 * Generic base interface for table operations.
 */
interface AnyTableOperation extends Op.TableOperation {
    range?: string;
}

/**
 * Base interface for spreadsheet operations with a range list.
 */
interface AnyRangeListOperation extends Op.SheetOperation {
    ranges?: string;
}

/**
 * Base interface for spreadsheet operations with a reference formula.
 */
interface AnyRefOperation extends Op.SheetOperation {
    ref?: string;
}

/**
 * Base interface for spreadsheet operations with optional CF rule properties.
 */
interface AnyCFRuleOperation extends Op.SheetOperation, Op.CFRuleAttributes {
}

/**
 * Generic shape of drawing operations and text shape operations with optional
 * text positions (property `end` used e.g. in `setAttributes` and `delete`,
 * and property `to` used e.g. in `moveDrawing`) intended to be used with
 * `OT_DRAWING_POS_ALIAS`.
 */
interface AnyDrawingOperation extends Op.DrawingOperation, OptAttrsOperation {
    end?: Position;
    to?: Position;
}

/**
 * A generator for an operation that deletes the object targeted by the passed
 * JSON operation.
 */
type GenerateDeleteOpFn<OpT extends Operation> = (operation: OpT) => Opt<Operation>;

/**
 * A generator for a delete operation for an object with cell anchor property.
 */
type GenerateDeleteAnchorFn = (sheet: number, anchor: Address) => Op.CellAnchorOperation;

/**
 * Implementation of a document snapshot transformation function.
 *
 * @param operation
 *  The document operation transforming the document snapshot.
 *
 * @param snapshot
 *  The document snapshot to be transformed in-place.
 */
type TransformSnapshotFn<OpT extends Operation> = (operation: OpT, snapshot: DocumentSnapshot) => void;

/**
 * Implementation of a formula transformation function.
 *
 * @param formulaOp
 *  The document operation with formula expressions to be transformed.
 *
 * @param updateTask
 *  The formula update task created from a document operation that causes
 *  transformation of formula expressions in other operations.
 */
type TransformFormulaFn<OpT extends Operation> = (formulaOp: OpT, updateTask: FormulaUpdateTask) => void;

/**
 * Settings for formula transformation.
 */
interface FormulaTransformConfig {
    updateTask: FormulaUpdateTask;
    formulaType: FormulaType;
    refSheet: OptSheet;
    refAddress?: Address; // missing reference address for unimplemented transformations
}

/**
 * Parsed properties of a valid "moveNotes" or "moveComments" operation.
 */
interface MoveAnchorsParts {
    fromAnchors: AddressArray;
    toAnchors: AddressArray;
}

/**
 * Parsed properties of a valid "moveDrawing" operation.
 */
interface MoveDrawingParts extends OTMoveResult {
    sheetIdx: number;
    fromPos: Position;
    toPos: Position;
    arrIdx: number;
}

// constants ==================================================================

/**
 * Synthetic alias name for drawing operations containing a `start` property
 * with a document position. The first element of that position is the sheet
 * index that needs to be transformed.
 */
const OT_DRAWING_ALIAS = "$sheetDrawing";

/**
 * Synthetic alias name for text framework operations targeting drawing objects
 * containing a `start` property with a logical document position. The first
 * element of that position is the sheet index that needs to be transformed.
 */
const OT_DRAWING_TEXT_ALIAS = "$sheetDrawingText";

/**
 * Synthetic alias name for drawing and text operations containing a `start`
 * property with a document position. The first element of that position is the
 * sheet index that needs to be transformed.
 */
const OT_DRAWING_POS_ALIAS = "$sheetDrawingPos";

/**
 * Synthetic alias name for all chart operations containing a source link
 * formula expression that needs to be transformed.
 */
const OT_CHART_SOURCELINK_ALIAS = "$chartSourceLink";

/**
 * Synthetic alias name for all operations that modify the collection of number
 * format codes.
 */
const OT_NUMBER_FORMAT_ALIAS = "$sheetNumberFormat";

/**
 * Synthetic alias name for all operations that modify the sheet collection
 * (number of sheets, or order of sheets).
 */
const OT_SHEETS_ALIAS = "$sheetCollection";

/**
 * Synthetic alias name for all operations that modify the column collection of
 * a sheet (insert/delete/change columns).
 */
const OT_SHEET_COLS_ALIAS = "$sheetCols";

/**
 * Synthetic alias name for all operations that modify the row collection of a
 * sheet (insert/delete/change rows).
 */
const OT_SHEET_ROWS_ALIAS = "$sheetRows";

/**
 * Synthetic alias name for all operations that modify the cell collection of a
 * sheet (change/move/merge cells).
 */
const OT_SHEET_CELLS_ALIAS = "$sheetCells";

/**
 * Synthetic alias name for all operations that modify the hyperlink collection
 * of a sheet.
 */
const OT_SHEET_LINKS_ALIAS = "$sheetLinks";

/**
 * Synthetic alias name for all operations that modify the defined names of the
 * document.
 */
const OT_SHEET_NAMES_ALIAS = "$sheetNames";

/**
 * Synthetic alias name for all operations that modify the table ranges of a
 * sheet.
 */
const OT_SHEET_TABLES_ALIAS = "$sheetTables";

/**
 * Synthetic alias name for all operations that modify the collection of data
 * validation rules of a sheet.
 */
const OT_SHEET_DVRULES_ALIAS = "$sheetDVRules";

/**
 * Synthetic alias name for all operations that modify the collection of
 * conditional formatting rules of a sheet.
 */
const OT_SHEET_CFRULES_ALIAS = "$sheetCFRules";

/**
 * Synthetic alias name for all operations that modify the note collection of a
 * sheet.
 */
const OT_SHEET_NOTES_ALIAS = "$sheetNotes";

/**
 * Synthetic alias name for all operations that modify the comment collection
 * of a sheet.
 */
const OT_SHEET_COMMENTS_ALIAS = "$sheetComments";

/**
 * Synthetic alias name for spreadsheet operations containing a `sheet` index
 * property that will be transformed by "insertSheet", "deleteSheet",
 * "moveSheet", etc.
 */
const OT_SHEET_INDEX_ALIAS = "$sheetIndex";

/**
 * Synthetic alias name for all operations containing a `ranges` property with
 * a list of cell range addresses that will be transformed by "insertColumns",
 * "deleteRows", etc.
 */
const OT_SHEET_RANGES_ALIAS = "$sheetRanges";

/**
 * Synthetic alias name for all operations containing an `anchor` cell address
 * property (notes and comments) that will be transformed by "insertColumns",
 * "deleteRows", etc.
 */
const OT_CELL_ANCHOR_ALIAS = "$sheetCellAnchor";

/**
 * The names of all drawing operations containing a `start` property with a
 * logical document position (aliased by `OT_DRAWING_ALIAS`).
 */
const DRAWING_OPS = [
    Op.INSERT_DRAWING,
    Op.DELETE_DRAWING,
    Op.CHANGE_DRAWING,
    Op.MOVE_DRAWING,
    OT_CHART_ALIAS,
    Op.POSITION
];

/**
 * The names of all text framework operations targeting drawing objects
 * containing a `start` property with a logical document position (aliased by
 * `OT_DRAWING_TEXT_ALIAS`).
 */
const DRAWING_TEXT_OPS = [
    OT_INSERT_CHAR_ALIAS,
    OT_INSERT_COMP_ALIAS,
    OT_MERGE_COMP_ALIAS,
    OT_SPLIT_COMP_ALIAS,
    Op.SET_ATTRIBUTES,
    Op.DELETE
];

/**
 * The names of all operations containing a source link formula expression that
 * needs to be transformed. Aliased by `OT_CHART_SOURCELINK_ALIAS`.
 */
const CHART_SOURCELINK_OPS = [
    Op.INSERT_CHART_SERIES,
    Op.CHANGE_CHART_SERIES,
    Op.CHANGE_CHART_TITLE
];

/**
 * The names of all operations that modify the sheet collection (number of
 * sheets, or order of sheets, aliased by `OT_SHEETS_ALIAS`).
 */
const SHEETS_OPS = [
    Op.INSERT_SHEET,
    Op.DELETE_SHEET,
    Op.MOVE_SHEET,
    Op.COPY_SHEET,
    Op.MOVE_SHEETS
];

/**
 * The names of all operations containing a `sheet` index property that will be
 * transformed (aliased by `OT_SHEET_INDEX_ALIAS`).
 */
const SHEET_INDEX_OPS = [
    Op.CHANGE_SHEET,
    OT_SHEET_COLS_ALIAS,
    OT_SHEET_ROWS_ALIAS,
    OT_SHEET_CELLS_ALIAS,
    OT_SHEET_LINKS_ALIAS,
    OT_SHEET_NAMES_ALIAS,
    OT_SHEET_TABLES_ALIAS,
    OT_SHEET_DVRULES_ALIAS,
    OT_SHEET_CFRULES_ALIAS,
    OT_SHEET_NOTES_ALIAS,
    OT_SHEET_COMMENTS_ALIAS,
    Op.SHEET_SELECTION
];

/**
 * Configuration for all operations containing a `ranges` property transformed
 * against column/row/cell move operations.
 */
const SHEET_RANGES_CONFIG: Dict<TransformRangeOptions> = {
    [Op.INSERT_HYPERLINK]: { transformMode: RESIZE },
    [Op.DELETE_HYPERLINK]: { transformMode: RESIZE },
    [Op.INSERT_DVRULE]:    { transformMode: EXPAND },
    [Op.CHANGE_DVRULE]:    { transformMode: EXPAND },
    [Op.INSERT_CFRULE]:    { transformMode: EXPAND },
    [Op.CHANGE_CFRULE]:    { transformMode: EXPAND }
};

/**
 * The names of all operations containing an `anchor` property transformed
 * against column/row/cell move operations.
 */
const CELL_ANCHOR_OPS = [
    Op.INSERT_NOTE,
    Op.DELETE_NOTE,
    Op.CHANGE_NOTE,
    Op.INSERT_COMMENT,
    Op.DELETE_COMMENT,
    Op.CHANGE_COMMENT
];

/**
 * The names of all deprecated properties in data validation rule operations.
 */
const DVRULE_VAL_PROPS: Array<keyof Op.DVRuleAttributes> = ["type", "compare", "value1", "value2"];

/**
 * The names of all properties in data validation rule operations.
 */
const DVRULE_PROPS: Array<keyof Op.ChangeDVRuleOperation> = ["ranges", ...DVRULE_VAL_PROPS, "showInfo", "infoTitle", "infoText", "showError", "errorTitle", "errorText", "errorType", "showDropDown", "ignoreEmpty"];

/**
 * The names of all deprecated properties in conditional formatting rule
 * operations.
 */
const CFRULE_VAL_PROPS: Array<keyof Op.CFRuleAttributes> = ["type", "value1", "value2", "colorScale", "dataBar", "iconSet"];

/**
 * The names of all properties in conditional formatting rule operations.
 */
const CFRULE_PROPS: Array<keyof Op.ChangeCFRuleOperation> = ["ranges", ...CFRULE_VAL_PROPS, "priority", "stop", "attrs"];

/**
 * The names of all position properties in drawing operations.
 */
const DRAWING_POS_PROPS: Array<keyof AnyDrawingOperation> = ["start", "end", "to"];

// generic reference address for formula transformation
const A1 = new Address(0, 0);

// private functions ==========================================================

function parseIndex(text: string): number {
    return /^(0|[1-9]\d*)$/.test(text) ? parseInt(text, 10) : -1;
}

function isHideSheet(sheetOp: Op.ChangeSheetOperation): boolean {
    return pick.dict(sheetOp.attrs, "sheet")?.visible === false;
}

/**
 * Returns whether both name operations refer to the same defined name.
 */
function isSameName(op1: Op.NameOperation, op2: Op.NameOperation): boolean {
    return (op1.sheet === op2.sheet) && str.equalsICC(op1.label, op2.label);
}

/**
 * Returns whether both table operations refer to the same table range (ignores
 * sheet index except for auto-filters which can appear on every sheet).
 */
function isSameTable(op1: Op.TableOperation, op2: Op.TableOperation): boolean {
    return (op1.table && op2.table && str.equalsICC(op1.table, op2.table)) || (!op1.table && !op2.table && (op1.sheet === op2.sheet));
}

/**
 * Returns whether both operations refer to the same anchor cell.
 */
function isSameAnchor(op1: Op.CellAnchorOperation, op2: Op.CellAnchorOperation): boolean {
    return (op1.sheet === op2.sheet) && (op1.anchor === op2.anchor);
}

/**
 * Assigns the indexes of an `OTMoveResult` structure to a "moveSheet" or a
 * "copySheet" operation.
 */
function assignMoveSheet(sheetOp: Op.TargetSheetOperation, moveRes: Opt<OTMoveResult>): void {
    if (moveRes) {
        sheetOp.sheet = moveRes.fromIdx;
        sheetOp.to = moveRes.toIdx;
    } else {
        setOperationRemoved(sheetOp);
    }
}

/**
 * Assigns the passed sort vector to a "moveSheets" operation, and checks that
 * the sort vector is not a no-op.
 */
function assignMoveSheets(sortOp: Op.MoveSheetsOperation, sortVec: Opt<OTSortVector>): void {
    if (sortVec) {
        sortOp.sheets = sortVec;
    } else {
        setOperationRemoved(sortOp);
    }
}

/**
 * Assigns an interval list to an operation, and checks whether the operation
 * becomes a no-op.
 */
function assignIntervalList(intervalsOp: Op.IntervalListOperation, intervals: IntervalArray, columns: boolean): void {
    if (intervals.empty()) {
        setOperationRemoved(intervalsOp);
    } else {
        intervalsOp.intervals = intervals.toOpStr(columns);
    }
}

/**
 * Assigns a cell range list to an operation, and checks whether the operation
 * becomes a no-op.
 */
function assignRangeList(rangesOp: AnyRangeListOperation, ranges: RangeArray): void {
    if (ranges.empty()) {
        setOperationRemoved(rangesOp);
    } else {
        rangesOp.ranges = ranges.toOpStr();
    }
}

/**
 * Assigns the passed address lists to the properties of a "moveNotes" or
 * "moveComments" operation.
 */
function assignMoveAnchors(moveOp: Op.MoveAnchorsOperation, fromAnchors: AddressArray, toAnchors: AddressArray): void {
    if (fromAnchors.empty() || toAnchors.empty()) {
        setOperationRemoved(moveOp);
    } else {
        moveOp.from = fromAnchors.toOpStr();
        moveOp.to = toAnchors.toOpStr();
    }
}

/**
 * Inserts a sheet index into all position properties of a drawing operation.
 */
function assignDrawingSheetIndex(drawingOp: AnyDrawingOperation, sheet: number): void {
    DRAWING_POS_PROPS.forEach(key => {
        if (drawingOp[key]) { (drawingOp[key] as Position)[0] = sheet; }
    });
}

/**
 * Assigns the indexes of an `OTMoveResult` structure to a specific element in
 * the positions of a "moveDrawing" operation.
 */
function assignMoveDrawing(drawingOp: Op.MoveDrawingOperation, arrIdx: number, moveRes: Opt<OTMoveResult>): void {
    if (moveRes) {
        drawingOp.start[arrIdx] = moveRes.fromIdx;
        drawingOp.to[arrIdx] = moveRes.toIdx;
    } else {
        setOperationRemoved(drawingOp);
    }
}

function transformSheetInDrawingPositions(drawingOp: AnyDrawingOperation, transformFn: (sheet: number) => Opt<number>): void {
    DRAWING_POS_PROPS.forEach(key => {
        const xfPos = drawingOp[key] as Opt<Position>;
        if (xfPos) {
            const sheet = transformFn(xfPos[0]);
            if (is.number(sheet)) { xfPos[0] = sheet; } else { setOperationRemoved(drawingOp); }
        }
    });
}

function transformInsertDrawingPositions(drawingOp: AnyDrawingOperation, insPos: Position): void {
    DRAWING_POS_PROPS.forEach(key => {
        const xfPos = drawingOp[key];
        if (is.array(xfPos)) { transformPositionInsert(xfPos, insPos, 1); }
    });
}

function transformDeleteDrawingPositions(drawingOp: AnyDrawingOperation, delPos: Position): void {
    DRAWING_POS_PROPS.forEach(key => {
        const xfPos = drawingOp[key];
        if (is.array(xfPos) && !transformPositionDelete(xfPos, delPos, 1)) {
            setOperationRemoved(drawingOp);
        }
    });
}

function transformDrawingPositionsForMove(drawingOp: AnyDrawingOperation, moveParts: MoveDrawingParts): void {
    DRAWING_POS_PROPS.forEach(key => {
        const xfPos = drawingOp[key];
        if (is.array(xfPos)) { transformPositionMove(xfPos, moveParts.fromPos, 1, moveParts.toIdx); }
    });
}

function checkChangeSheetNoOp(changeOp: Op.ChangeSheetOperation): void {
    if (!changeOp.attrs && !changeOp.sheetName) { setOperationRemoved(changeOp); }
}

function checkChangeIntervalsNoOp(changeOp: Op.ChangeIntervalsOperation): boolean {
    const removed = !changeOp.attrs && !changeOp.s;
    if (removed) { setOperationRemoved(changeOp); }
    return removed;
}

function checkChangeCellsNoOp(changeOp: Op.ChangeCellsOperation): void {
    if (is.empty(changeOp.contents)) { setOperationRemoved(changeOp); }
}

function checkChangeNameNoOp(changeOp: Op.ChangeNameOperation): void {
    // remove properties that depend on "formula"
    if (!changeOp.formula) {
        delete changeOp.ref;
        delete changeOp.isExpr;
    }
    if (!changeOp.newLabel && !("formula" in changeOp)) { setOperationRemoved(changeOp); }
}

function checkChangeTableNoOp(changeOp: Op.ChangeTableOperation): void {
    if (!changeOp.attrs && !changeOp.newName && !changeOp.range && !changeOp.headers) { setOperationRemoved(changeOp); }
}

function checkChangeDVRuleNoOp(changeOp: Op.ChangeDVRuleOperation): void {
    if (!DVRULE_PROPS.some(key => key in changeOp) && !changeOp.ref) { setOperationRemoved(changeOp); }
}

function checkChangeCFRuleNoOp(changeOp: Op.ChangeCFRuleOperation): void {
    if (!CFRULE_PROPS.some(key => key in changeOp) && !changeOp.ref) { setOperationRemoved(changeOp); }
}

function checkChangeNoteNoOp(changeOp: Op.ChangeNoteOperation): void {
    if (!changeOp.attrs && !("text" in changeOp)) { setOperationRemoved(changeOp); }
}

function checkChangeCommentNoOp(changeOp: Op.ChangeCommentOperation): void {
    if (!changeOp.attrs && !("text" in changeOp) && !changeOp.mentions) { setOperationRemoved(changeOp); }
}

function changeOperationType<OpType extends Operation>(op: Operation, name: OpType["name"]): OpType {
    const convOp = op as unknown as OpType;
    convOp.name = name;
    return convOp;
}

function reduceMergedRanges(lclRanges: RangeArray, extRanges: RangeArray, mergeMode?: MergeMode): RangeArray {

    // reduce all external ranges that overlap with local ranges (local ranges win over external ranges)
    switch (mergeMode) {

        case MergeMode.HORIZONTAL:
        case MergeMode.VERTICAL: {
            const columns = mergeMode === MergeMode.VERTICAL;
            return RangeArray.map(extRanges, extRange => {
                const resRanges = new RangeArray(extRange);
                lclRanges.forEach(lclRange => {
                    if (!extRange.overlaps(lclRange)) { return; }
                    const srcRanges = resRanges.clone();
                    resRanges.clear();
                    srcRanges.forEach(srcRange => {
                        if (srcRange.getStart(columns) < lclRange.getStart(columns)) {
                            const resRange = srcRange.clone();
                            resRange.setEnd(min(srcRange.getEnd(columns), lclRange.getStart(columns) - 1), columns);
                            resRanges.push(resRange);
                        }
                        if (lclRange.getEnd(columns) < srcRange.getEnd(columns)) {
                            const resRange = srcRange.clone();
                            resRange.setStart(max(srcRange.getStart(columns), lclRange.getEnd(columns) + 1), columns);
                            resRanges.push(resRange);
                        }
                    });
                });
                return resRanges;
            });
        }

        case MergeMode.UNMERGE:
            return extRanges.difference(lclRanges);

        default:
            return extRanges.reject(extRange => lclRanges.overlaps(extRange));
    }
}

/**
 * Converts the passed absolute document position of (or in) a drawing object
 * to a document position inside the containing sheet (by removing the first
 * array element containing the sheet index).
 *
 * @param docPos
 *  The document drawing position to be converted to a sheet drawing position.
 *
 * @returns
 *  The sheet drawing position.
 */
function getSheetPos(docPos: Position): Position {
    return docPos.slice(1) as Position;
}

// class SnapshotTokenArray ===================================================

class SnapshotTokenArray extends BaseTokenArray {

    readonly #refSheet: OptSheet;

    constructor(
        snapshot: DocumentSnapshot,
        parser: FormulaParser,
        type: FormulaType,
        formula: string,
        refSheet: OptSheet,
        refAddress: Address
    ) {
        super(snapshot, type);
        this.#refSheet = refSheet;

        // parse the formula expression
        const parseTokens = parser.parse(snapshot, formula, refAddress, this.extendWithOwnOptions());
        this.setTokens(parseTokens.map(parseToken => parseToken.token));
    }

    /**
     * Returns the index of the sheet that contains this token array. This is a
     * required implementation of the abstract method declared by the base
     * class `BaseTokenArray`.
     */
    getRefSheet(): OptSheet {
        return this.#refSheet;
    }
}

// class SpreadsheetOTManager =================================================

export class SpreadsheetOTManager extends TextBaseOTManager {

    // properties -------------------------------------------------------------

    // the address factory containing the sheet limits (column/row count)
    readonly #addressFactory: AddressFactory;

    // the formula grammar needed to transform formula expressions
    readonly #formulaGrammar: FormulaGrammar;

    // the formula parser needed to parse formula expressions
    readonly #formulaParser: FormulaParser;

    // the range list parser needed to parse cell range address lists with sheet names
    readonly #rangeListParser: RangeListParser;

    // registry for document snapshot transformation implementations
    readonly #deleteOpGeneratorMap: Map<string, GenerateDeleteOpFn<any>>;

    // registry for document snapshot transformation implementations
    readonly #snapshotTransformMap: Map<string, TransformSnapshotFn<any>>;

    // registry for formula transformation implementations
    readonly #formulaTransformMap: Map<string, TransformFormulaFn<any>>;

    // whether operations with formulas use a custom reference address in a "ref" property
    readonly #customRefAddress: boolean;

    // whether data validation rules use the required "index" property
    readonly #indexedDVRules: boolean;

    // whether conditional formatting rules use the property "id" as array index
    readonly #indexedCFRules: boolean;

    // the document snapshot for the operations currently transformed
    #docSnapshot: Opt<DocumentSnapshot>;

    // deep copy of the original local operation being transformed (needed for snapshot transformation)
    #origLclOp: Opt<Operation>;

    // deep copy of the original local operation being transformed (needed for snapshot transformation)
    #origExtOps: Opt<Array<Opt<Operation>>>;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {

        // special handling for file formats
        const oox = docModel.docApp.isOOXML();
        const odf = docModel.docApp.isODF();

        // super constructor
        super({
            defaultAutoStyleType: "cell",
            indexedAutoStylePrefixes: oox ? { cell: "a" } : undefined
        });

        // initialize properties
        this.#addressFactory = docModel.addressFactory;
        this.#formulaGrammar = docModel.formulaGrammarOP;
        this.#formulaParser = docModel.formulaParserOP;
        this.#rangeListParser = docModel.rangeListParserOP;

        // unregister table operations implemented in base class
        this.unregisterOperationNames(Op.INSERT_ROWS, Op.DELETE_COLUMNS, Op.INSERT_TABLE);

        // other configuration options
        this.#customRefAddress = odf;
        this.#indexedDVRules = oox;
        this.#indexedCFRules = odf;

        // register alias names
        this.registerAliasName(OT_NUMBER_FORMAT_ALIAS,    Op.INSERT_NUMBER_FORMAT, Op.DELETE_NUMBER_FORMAT);
        this.registerAliasName(OT_SHEETS_ALIAS,           SHEETS_OPS);
        this.registerAliasName(OT_SHEET_COLS_ALIAS,       Op.INSERT_COLUMNS, Op.DELETE_COLUMNS, Op.CHANGE_COLUMNS);
        this.registerAliasName(OT_SHEET_ROWS_ALIAS,       Op.INSERT_ROWS, Op.DELETE_ROWS, Op.CHANGE_ROWS);
        this.registerAliasName(OT_SHEET_CELLS_ALIAS,      Op.CHANGE_CELLS, Op.MOVE_CELLS, Op.MERGE_CELLS);
        this.registerAliasName(OT_SHEET_LINKS_ALIAS,      Op.INSERT_HYPERLINK, Op.DELETE_HYPERLINK);
        this.registerAliasName(OT_SHEET_NAMES_ALIAS,      Op.INSERT_NAME, Op.DELETE_NAME, Op.CHANGE_NAME);
        this.registerAliasName(OT_SHEET_TABLES_ALIAS,     Op.INSERT_TABLE, Op.DELETE_TABLE, Op.CHANGE_TABLE, Op.CHANGE_TABLE_COLUMN);
        this.registerAliasName(OT_SHEET_DVRULES_ALIAS,    Op.INSERT_DVRULE, Op.DELETE_DVRULE, Op.CHANGE_DVRULE);
        this.registerAliasName(OT_SHEET_CFRULES_ALIAS,    Op.INSERT_CFRULE, Op.DELETE_CFRULE, Op.CHANGE_CFRULE);
        this.registerAliasName(OT_SHEET_NOTES_ALIAS,      Op.INSERT_NOTE, Op.DELETE_NOTE, Op.CHANGE_NOTE, Op.MOVE_NOTES);
        this.registerAliasName(OT_SHEET_COMMENTS_ALIAS,   Op.INSERT_COMMENT, Op.DELETE_COMMENT, Op.CHANGE_COMMENT, Op.MOVE_COMMENTS);
        this.registerAliasName(OT_SHEET_INDEX_ALIAS,      SHEET_INDEX_OPS);
        this.registerAliasName(OT_SHEET_RANGES_ALIAS,     Object.keys(SHEET_RANGES_CONFIG));
        this.registerAliasName(OT_DRAWING_ALIAS,          DRAWING_OPS);
        this.registerAliasName(OT_DRAWING_TEXT_ALIAS,     DRAWING_TEXT_OPS);
        this.registerAliasName(OT_DRAWING_POS_ALIAS,      OT_DRAWING_ALIAS, OT_DRAWING_TEXT_ALIAS);
        this.registerAliasName(OT_CHART_SOURCELINK_ALIAS, CHART_SOURCELINK_OPS);
        this.registerAliasName(OT_CELL_ANCHOR_ALIAS,      CELL_ANCHOR_OPS);

        // register generator functions for delete operations
        this.#deleteOpGeneratorMap = new Map();
        this.#deleteOpGeneratorMap.set(Op.INSERT_DVRULE, this.#generateDeleteDVRule);
        this.#deleteOpGeneratorMap.set(Op.CHANGE_DVRULE, this.#generateDeleteDVRule);
        this.#deleteOpGeneratorMap.set(Op.INSERT_CFRULE, this.#generateDeleteCFRule);
        this.#deleteOpGeneratorMap.set(Op.CHANGE_CFRULE, this.#generateDeleteCFRule);
        this.#deleteOpGeneratorMap.set(Op.INSERT_NOTE, this.#generateDeleteNote);
        this.#deleteOpGeneratorMap.set(Op.CHANGE_NOTE, this.#generateDeleteNote);
        this.#deleteOpGeneratorMap.set(Op.INSERT_COMMENT, this.#generateDeleteComment);
        this.#deleteOpGeneratorMap.set(Op.CHANGE_COMMENT, this.#generateDeleteComment);

        // transform the document snapshot against every external and local operation
        this.registerActionPrepareHandler(this.#initializeActionSnapshot);
        this.registerActionFinalizeHandler(this.#transformActionSnapshot);
        this.registerTransformPrepareHandler(this.#initializeOpsSnapshot);
        this.registerTransformFinalizeHandler(this.#transformOpsSnapshot);

        // register the snapshot transformation functions
        this.#snapshotTransformMap = new Map();
        this.#snapshotTransformMap.set(Op.INSERT_SHEET, this.#transform_insertSheet_docSnapshot);
        this.#snapshotTransformMap.set(Op.DELETE_SHEET, this.#transform_deleteSheet_docSnapshot);
        this.#snapshotTransformMap.set(Op.MOVE_SHEET, this.#transform_moveSheet_docSnapshot);
        this.#snapshotTransformMap.set(Op.MOVE_SHEETS, this.#transform_moveSheets_docSnapshot);
        this.#snapshotTransformMap.set(Op.COPY_SHEET, this.#transform_copySheet_docSnapshot);
        this.#snapshotTransformMap.set(Op.CHANGE_SHEET, this.#transform_changeSheet_docSnapshot);
        this.#snapshotTransformMap.set(Op.INSERT_NAME, this.#transform_insertName_docSnapshot);
        this.#snapshotTransformMap.set(Op.DELETE_NAME, this.#transform_deleteName_docSnapshot);
        this.#snapshotTransformMap.set(Op.CHANGE_NAME, this.#transform_changeName_docSnapshot);
        this.#snapshotTransformMap.set(Op.INSERT_TABLE, this.#transform_insertTable_docSnapshot);
        this.#snapshotTransformMap.set(Op.DELETE_TABLE, this.#transform_deleteTable_docSnapshot);
        this.#snapshotTransformMap.set(Op.CHANGE_TABLE, this.#transform_changeTable_docSnapshot);

        // register formula transformation functions
        this.#formulaTransformMap = new Map();
        this.#formulaTransformMap.set(Op.CHANGE_CELLS, this.#transform_changeCells_updateTask);
        this.#formulaTransformMap.set(Op.INSERT_NAME, this.#transform_definedName_updateTask);
        this.#formulaTransformMap.set(Op.CHANGE_NAME, this.#transform_definedName_updateTask);
        this.#formulaTransformMap.set(Op.INSERT_DVRULE, this.#transform_dvRule_updateTask);
        this.#formulaTransformMap.set(Op.CHANGE_DVRULE, this.#transform_dvRule_updateTask);
        this.#formulaTransformMap.set(Op.INSERT_CFRULE, this.#transform_cfRule_updateTask);
        this.#formulaTransformMap.set(Op.CHANGE_CFRULE, this.#transform_cfRule_updateTask);
        this.#formulaTransformMap.set(Op.INSERT_DRAWING, this.#transform_textLink_updateTask);
        this.#formulaTransformMap.set(Op.CHANGE_DRAWING, this.#transform_textLink_updateTask);
        this.#formulaTransformMap.set(Op.INSERT_CHART_SERIES, this.#transform_chartSeries_updateTask);
        this.#formulaTransformMap.set(Op.CHANGE_CHART_SERIES, this.#transform_chartSeries_updateTask);
        this.#formulaTransformMap.set(Op.CHANGE_CHART_TITLE, this.#transform_textLink_updateTask);

        // skip auto-style operations with other unrelated operations
        this.registerBidiTransformation(OT_AUTOSTYLE_ALIAS, OT_NUMBER_FORMAT_ALIAS, null);
        this.registerBidiTransformation(OT_AUTOSTYLE_ALIAS, OT_SHEETS_ALIAS, null);
        this.registerBidiTransformation(OT_AUTOSTYLE_ALIAS, OT_SHEET_INDEX_ALIAS, null);
        this.registerBidiTransformation(OT_AUTOSTYLE_ALIAS, OT_DRAWING_POS_ALIAS, null);

        // insertAutoStyle
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.INSERT_COLUMNS, this.#transform_moveAutoStyle_changeIntervals.bind(this, true, false));
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.CHANGE_COLUMNS, this.#transform_moveAutoStyle_changeIntervals.bind(this, true, true));
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.INSERT_ROWS, this.#transform_moveAutoStyle_changeIntervals.bind(this, true, false));
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.CHANGE_ROWS, this.#transform_moveAutoStyle_changeIntervals.bind(this, true, true));
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.CHANGE_CELLS, this.#transform_moveAutoStyle_changeCells.bind(this, true));

        // deleteAutoStyle
        this.registerBidiTransformation(Op.DELETE_AUTOSTYLE, Op.INSERT_COLUMNS, this.#transform_moveAutoStyle_changeIntervals.bind(this, false, false));
        this.registerBidiTransformation(Op.DELETE_AUTOSTYLE, Op.CHANGE_COLUMNS, this.#transform_moveAutoStyle_changeIntervals.bind(this, false, true));
        this.registerBidiTransformation(Op.DELETE_AUTOSTYLE, Op.INSERT_ROWS, this.#transform_moveAutoStyle_changeIntervals.bind(this, false, false));
        this.registerBidiTransformation(Op.DELETE_AUTOSTYLE, Op.CHANGE_ROWS, this.#transform_moveAutoStyle_changeIntervals.bind(this, false, true));
        this.registerBidiTransformation(Op.DELETE_AUTOSTYLE, Op.CHANGE_CELLS, this.#transform_moveAutoStyle_changeCells.bind(this, false));

        // skip chart operations with text operations (but check for shapes embedded in charts)
        this.registerBidiTransformation(OT_CHART_ALIAS, OT_DRAWING_TEXT_ALIAS, this.#transform_anyChart_drawingText);
        this.registerBidiTransformation(OT_CHART_ALIAS, Op.POSITION, null);

        // skip number format operations with other unrelated operations
        this.registerBidiTransformation(OT_NUMBER_FORMAT_ALIAS, OT_SHEETS_ALIAS, null);
        this.registerBidiTransformation(OT_NUMBER_FORMAT_ALIAS, OT_SHEET_INDEX_ALIAS, null);
        this.registerBidiTransformation(OT_NUMBER_FORMAT_ALIAS, OT_DRAWING_POS_ALIAS, null);

        // insertNumberFormat
        this.registerSelfTransformation(Op.INSERT_NUMBER_FORMAT, this.#transform_insertNumFmt_insertNumFmt);
        this.registerBidiTransformation(Op.INSERT_NUMBER_FORMAT, Op.DELETE_NUMBER_FORMAT, this.#transform_insertNumFmt_deleteNumFmt);

        // deleteNumberFormat
        this.registerSelfTransformation(Op.DELETE_NUMBER_FORMAT, this.#transform_deleteNumFmt_deleteNumFmt);

        // insertSheet
        this.registerSelfTransformation(Op.INSERT_SHEET, this.#transform_insertSheet_insertSheet);
        this.registerBidiTransformation(Op.INSERT_SHEET, Op.DELETE_SHEET, this.#transform_insertSheet_deleteSheet);
        this.registerBidiTransformation(Op.INSERT_SHEET, Op.MOVE_SHEET, this.#transform_insertSheet_moveSheet);
        this.registerBidiTransformation(Op.INSERT_SHEET, Op.COPY_SHEET, this.#transform_insertSheet_copySheet);
        this.registerBidiTransformation(Op.INSERT_SHEET, Op.MOVE_SHEETS, this.#transform_insertSheet_moveSheets);
        this.registerBidiTransformation(Op.INSERT_SHEET, Op.CHANGE_SHEET, this.#transform_insertSheet_changeSheet);
        this.registerBidiTransformation(Op.INSERT_SHEET, OT_SHEET_INDEX_ALIAS, this.#transform_insertSheet_sheetIndex);
        this.registerBidiTransformation(Op.INSERT_SHEET, OT_DRAWING_POS_ALIAS, this.#transform_insertSheet_drawingPos);

        // deleteSheet
        this.registerSelfTransformation(Op.DELETE_SHEET, this.#transform_deleteSheet_deleteSheet);
        this.registerBidiTransformation(Op.DELETE_SHEET, Op.MOVE_SHEET, this.#transform_deleteSheet_moveSheet);
        this.registerBidiTransformation(Op.DELETE_SHEET, Op.COPY_SHEET, this.#transform_deleteSheet_copySheet);
        this.registerBidiTransformation(Op.DELETE_SHEET, Op.MOVE_SHEETS, this.#transform_deleteSheet_moveSheets);
        this.registerBidiTransformation(Op.DELETE_SHEET, Op.CHANGE_SHEET, this.#transform_deleteSheet_changeSheet);
        this.registerBidiTransformation(Op.DELETE_SHEET, OT_SHEET_INDEX_ALIAS, this.#transform_deleteSheet_sheetIndex);
        this.registerBidiTransformation(Op.DELETE_SHEET, OT_DRAWING_POS_ALIAS, this.#transform_deleteSheet_drawingPos);

        // moveSheet
        this.registerSelfTransformation(Op.MOVE_SHEET, this.#transform_moveSheet_moveSheet);
        this.registerBidiTransformation(Op.MOVE_SHEET, Op.COPY_SHEET, this.#transform_moveSheet_copySheet);
        this.registerBidiTransformation(Op.MOVE_SHEET, Op.MOVE_SHEETS, this.#transform_moveSheet_moveSheets);
        this.registerBidiTransformation(Op.MOVE_SHEET, OT_SHEET_INDEX_ALIAS, this.#transform_moveSheet_sheetIndex);
        this.registerBidiTransformation(Op.MOVE_SHEET, OT_DRAWING_POS_ALIAS, this.#transform_moveSheet_drawingPos);

        // copySheet
        this.registerSelfTransformation(Op.COPY_SHEET, this.#transform_copySheet_copySheet);
        this.registerBidiTransformation(Op.COPY_SHEET, Op.MOVE_SHEETS, this.#transform_copySheet_moveSheets);
        this.registerBidiTransformation(Op.COPY_SHEET, Op.CHANGE_SHEET, this.#transform_copySheet_changeSheet);
        this.registerBidiTransformation(Op.COPY_SHEET, OT_SHEET_INDEX_ALIAS, this.#transform_copySheet_sheetIndex);
        this.registerBidiTransformation(Op.COPY_SHEET, OT_DRAWING_POS_ALIAS, this.#transform_copySheet_drawingPos);

        // moveSheets
        this.registerSelfTransformation(Op.MOVE_SHEETS, this.#transform_moveSheets_moveSheets);
        this.registerBidiTransformation(Op.MOVE_SHEETS, OT_SHEET_INDEX_ALIAS, this.#transform_moveSheets_sheetIndex);
        this.registerBidiTransformation(Op.MOVE_SHEETS, OT_DRAWING_POS_ALIAS, this.#transform_moveSheets_drawingPos);

        // changeSheet
        this.registerSelfTransformation(Op.CHANGE_SHEET, this.#transform_changeSheet_changeSheet);
        this.registerBidiTransformation(Op.CHANGE_SHEET, OT_SHEET_INDEX_ALIAS, this.#transform_changeSheet_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_SHEET, Op.INSERT_NOTE, this.#transform_changeSheet_drawingAnchor);
        this.registerBidiTransformation(Op.CHANGE_SHEET, Op.CHANGE_NOTE, this.#transform_changeSheet_drawingAnchor);
        this.registerBidiTransformation(Op.CHANGE_SHEET, OT_DRAWING_POS_ALIAS, this.#transform_changeSheet_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_SHEET, Op.INSERT_DRAWING, this.#transform_changeSheet_drawingAnchor);
        this.registerBidiTransformation(Op.CHANGE_SHEET, Op.CHANGE_DRAWING, this.#transform_changeSheet_drawingAnchor);

        // column collection
        this.registerBidiTransformation(OT_SHEET_COLS_ALIAS, OT_SHEET_ROWS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_COLS_ALIAS, Op.DELETE_NAME, null);
        this.registerBidiTransformation(OT_SHEET_COLS_ALIAS, Op.DELETE_TABLE, null);
        this.registerBidiTransformation(OT_SHEET_COLS_ALIAS, Op.DELETE_DVRULE, null);
        this.registerBidiTransformation(OT_SHEET_COLS_ALIAS, Op.DELETE_CFRULE, null);
        this.registerBidiTransformation(OT_SHEET_COLS_ALIAS, OT_DRAWING_POS_ALIAS, null);

        // insertColumns
        this.registerSelfTransformation(Op.INSERT_COLUMNS, this.#transform_insertIntervals_insertIntervals.bind(this, true));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.DELETE_COLUMNS, this.#transform_insertIntervals_deleteIntervals.bind(this, true));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.CHANGE_COLUMNS, this.#transform_moveIntervals_changeIntervals.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.CHANGE_CELLS, this.#transform_moveIntervals_changeCells.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.MERGE_CELLS, this.#transform_moveIntervals_mergeCells.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.INSERT_NAME, this.#transform_moveIntervals_definedName.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.CHANGE_NAME, this.#transform_moveIntervals_definedName.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.INSERT_TABLE, this.#transform_moveIntervals_tableRange.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.CHANGE_TABLE, this.#transform_moveIntervals_tableRange.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.CHANGE_TABLE_COLUMN, this.#transform_moveIntervals_changeTableCol.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, OT_SHEET_RANGES_ALIAS, this.#transform_moveIntervals_rangeList.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, OT_CELL_ANCHOR_ALIAS, this.#transform_moveIntervals_cellAnchor.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.MOVE_NOTES, this.#transform_moveIntervals_moveNotes.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.MOVE_COMMENTS, this.#transform_moveIntervals_moveComments.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.INSERT_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.CHANGE_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, OT_CHART_SOURCELINK_ALIAS, this.#transform_moveIntervals_sourceLinks.bind(this, RIGHT));
        this.registerBidiTransformation(Op.INSERT_COLUMNS, Op.SHEET_SELECTION, this.#transform_moveIntervals_sheetSelection.bind(this, RIGHT));

        // deleteColumns
        this.registerSelfTransformation(Op.DELETE_COLUMNS, this.#transform_deleteIntervals_deleteIntervals.bind(this, true));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.CHANGE_COLUMNS, this.#transform_moveIntervals_changeIntervals.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.CHANGE_CELLS, this.#transform_moveIntervals_changeCells.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.MERGE_CELLS, this.#transform_moveIntervals_mergeCells.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.INSERT_NAME, this.#transform_moveIntervals_definedName.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.CHANGE_NAME, this.#transform_moveIntervals_definedName.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.INSERT_TABLE, this.#transform_moveIntervals_tableRange.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.CHANGE_TABLE, this.#transform_moveIntervals_tableRange.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.CHANGE_TABLE_COLUMN, this.#transform_moveIntervals_changeTableCol.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, OT_SHEET_RANGES_ALIAS, this.#transform_moveIntervals_rangeList.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, OT_CELL_ANCHOR_ALIAS, this.#transform_moveIntervals_cellAnchor.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.MOVE_NOTES, this.#transform_moveIntervals_moveNotes.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.MOVE_COMMENTS, this.#transform_moveIntervals_moveComments.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.INSERT_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.CHANGE_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, OT_CHART_SOURCELINK_ALIAS, this.#transform_moveIntervals_sourceLinks.bind(this, LEFT));
        this.registerBidiTransformation(Op.DELETE_COLUMNS, Op.SHEET_SELECTION, this.#transform_moveIntervals_sheetSelection.bind(this, LEFT));

        // changeColumns
        this.registerSelfTransformation(Op.CHANGE_COLUMNS, this.#transform_changeIntervals_changeIntervals.bind(this, true));
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_CELLS_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_LINKS_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_NAMES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_TABLES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_DVRULES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, Op.INSERT_NOTE, this.#transform_changeIntervals_drawingAnchor.bind(this, true));
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, Op.CHANGE_NOTE, this.#transform_changeIntervals_drawingAnchor.bind(this, true));
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, Op.INSERT_DRAWING, this.#transform_changeIntervals_drawingAnchor.bind(this, true));
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, Op.CHANGE_DRAWING, this.#transform_changeIntervals_drawingAnchor.bind(this, true));
        this.registerBidiTransformation(Op.CHANGE_COLUMNS, Op.SHEET_SELECTION, null);

        // row collection
        this.registerBidiTransformation(OT_SHEET_ROWS_ALIAS, Op.DELETE_NAME, null);
        this.registerBidiTransformation(OT_SHEET_ROWS_ALIAS, Op.DELETE_TABLE, null);
        this.registerBidiTransformation(OT_SHEET_ROWS_ALIAS, Op.DELETE_DVRULE, null);
        this.registerBidiTransformation(OT_SHEET_ROWS_ALIAS, Op.DELETE_CFRULE, null);
        this.registerBidiTransformation(OT_SHEET_ROWS_ALIAS, OT_DRAWING_POS_ALIAS, null);

        // insertRows
        this.registerSelfTransformation(Op.INSERT_ROWS, this.#transform_insertIntervals_insertIntervals.bind(this, false));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.DELETE_ROWS, this.#transform_insertIntervals_deleteIntervals.bind(this, false));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.CHANGE_ROWS, this.#transform_moveIntervals_changeIntervals.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.CHANGE_CELLS, this.#transform_moveIntervals_changeCells.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.MERGE_CELLS, this.#transform_moveIntervals_mergeCells.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.INSERT_NAME, this.#transform_moveIntervals_definedName.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.CHANGE_NAME, this.#transform_moveIntervals_definedName.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.INSERT_TABLE, this.#transform_moveIntervals_tableRange.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.CHANGE_TABLE, this.#transform_moveIntervals_tableRange.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.CHANGE_TABLE_COLUMN, this.#transform_moveIntervals_changeTableCol.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, OT_SHEET_RANGES_ALIAS, this.#transform_moveIntervals_rangeList.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, OT_CELL_ANCHOR_ALIAS, this.#transform_moveIntervals_cellAnchor.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.MOVE_NOTES, this.#transform_moveIntervals_moveNotes.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.MOVE_COMMENTS, this.#transform_moveIntervals_moveComments.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.INSERT_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.CHANGE_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, OT_CHART_SOURCELINK_ALIAS, this.#transform_moveIntervals_sourceLinks.bind(this, DOWN));
        this.registerBidiTransformation(Op.INSERT_ROWS, Op.SHEET_SELECTION, this.#transform_moveIntervals_sheetSelection.bind(this, DOWN));

        // deleteRows
        this.registerSelfTransformation(Op.DELETE_ROWS, this.#transform_deleteIntervals_deleteIntervals.bind(this, false));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.CHANGE_ROWS, this.#transform_moveIntervals_changeIntervals.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.CHANGE_CELLS, this.#transform_moveIntervals_changeCells.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.MERGE_CELLS, this.#transform_moveIntervals_mergeCells.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.INSERT_NAME, this.#transform_moveIntervals_definedName.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.CHANGE_NAME, this.#transform_moveIntervals_definedName.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.INSERT_TABLE, this.#transform_moveIntervals_tableRange.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.CHANGE_TABLE, this.#transform_moveIntervals_tableRange.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.CHANGE_TABLE_COLUMN, this.#transform_moveIntervals_changeTableCol.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, OT_SHEET_RANGES_ALIAS, this.#transform_moveIntervals_rangeList.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, OT_CELL_ANCHOR_ALIAS, this.#transform_moveIntervals_cellAnchor.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.MOVE_NOTES, this.#transform_moveIntervals_moveNotes.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.MOVE_COMMENTS, this.#transform_moveIntervals_moveComments.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.INSERT_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.CHANGE_DRAWING, this.#transform_moveIntervals_drawingAnchor.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, OT_CHART_SOURCELINK_ALIAS, this.#transform_moveIntervals_sourceLinks.bind(this, UP));
        this.registerBidiTransformation(Op.DELETE_ROWS, Op.SHEET_SELECTION, this.#transform_moveIntervals_sheetSelection.bind(this, UP));

        // changeRows
        this.registerSelfTransformation(Op.CHANGE_ROWS, this.#transform_changeIntervals_changeIntervals.bind(this, false));
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_CELLS_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_LINKS_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_NAMES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_TABLES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_DVRULES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, Op.INSERT_NOTE, this.#transform_changeIntervals_drawingAnchor.bind(this, false));
        this.registerBidiTransformation(Op.CHANGE_ROWS, Op.CHANGE_NOTE, this.#transform_changeIntervals_drawingAnchor.bind(this, false));
        this.registerBidiTransformation(Op.CHANGE_ROWS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_ROWS, Op.INSERT_DRAWING, this.#transform_changeIntervals_drawingAnchor.bind(this, false));
        this.registerBidiTransformation(Op.CHANGE_ROWS, Op.CHANGE_DRAWING, this.#transform_changeIntervals_drawingAnchor.bind(this, false));
        this.registerBidiTransformation(Op.CHANGE_ROWS, Op.SHEET_SELECTION, null);

        // cells
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_SHEET_LINKS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_SHEET_NAMES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, Op.DELETE_TABLE, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, Op.CHANGE_TABLE_COLUMN, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_SHEET_DVRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CELLS_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);

        // changeCells
        this.registerSelfTransformation(Op.CHANGE_CELLS, this.#transform_changeCells_changeCells);
        this.registerBidiTransformation(Op.CHANGE_CELLS, Op.MOVE_CELLS, this.#transform_changeCells_moveCells);
        this.registerBidiTransformation(Op.CHANGE_CELLS, Op.MERGE_CELLS, this.#transform_changeCells_mergeCells);
        this.registerBidiTransformation(Op.CHANGE_CELLS, Op.CHANGE_NAME, this.#transform_changeCells_changeName);
        this.registerBidiTransformation(Op.CHANGE_CELLS, Op.INSERT_TABLE, this.#transform_changeCells_tableRange);
        this.registerBidiTransformation(Op.CHANGE_CELLS, Op.CHANGE_TABLE, this.#transform_changeCells_tableRange);
        this.registerBidiTransformation(Op.CHANGE_CELLS, Op.SHEET_SELECTION, null);

        // moveCells
        this.registerBidiTransformation(Op.MOVE_CELLS, Op.INSERT_NAME, this.#transform_moveCells_formulaExpressions);
        this.registerBidiTransformation(Op.MOVE_CELLS, Op.CHANGE_NAME, this.#transform_moveCells_formulaExpressions);
        this.registerBidiTransformation(Op.MOVE_CELLS, Op.INSERT_DVRULE, this.#transform_moveCells_formulaExpressions);
        this.registerBidiTransformation(Op.MOVE_CELLS, Op.CHANGE_DVRULE, this.#transform_moveCells_formulaExpressions);
        this.registerBidiTransformation(Op.MOVE_CELLS, Op.INSERT_CFRULE, this.#transform_moveCells_formulaExpressions);
        this.registerBidiTransformation(Op.MOVE_CELLS, Op.CHANGE_CFRULE, this.#transform_moveCells_formulaExpressions);
        this.registerBidiTransformation(Op.MOVE_CELLS, OT_DRAWING_POS_ALIAS, this.#transform_moveCells_formulaExpressions);

        // mergeCells
        this.registerSelfTransformation(Op.MERGE_CELLS, this.#transform_mergeCells_mergeCells);
        this.registerBidiTransformation(Op.MERGE_CELLS, Op.INSERT_TABLE, this.#transform_mergeCells_tableRange);
        this.registerBidiTransformation(Op.MERGE_CELLS, Op.CHANGE_TABLE, this.#transform_mergeCells_tableRange);
        this.registerBidiTransformation(Op.MERGE_CELLS, Op.SHEET_SELECTION, null);

        // hyperlinks
        this.registerSelfTransformation(OT_SHEET_LINKS_ALIAS, this.#transform_changeHyperlink_changeHyperlink);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_SHEET_NAMES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_SHEET_TABLES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_SHEET_DVRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_LINKS_ALIAS, Op.SHEET_SELECTION, null);

        // defined names
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, OT_SHEET_TABLES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, OT_SHEET_DVRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NAMES_ALIAS, Op.SHEET_SELECTION, null);

        // insertName
        this.registerSelfTransformation(Op.INSERT_NAME, this.#transform_insertName_insertName);
        this.registerBidiTransformation(Op.INSERT_NAME, Op.DELETE_NAME, this.#transform_insertName_deleteName);
        this.registerBidiTransformation(Op.INSERT_NAME, Op.CHANGE_NAME, this.#transform_insertName_changeName);
        this.registerBidiTransformation(Op.INSERT_NAME, Op.INSERT_TABLE, this.#transform_insertName_insertTable);
        this.registerBidiTransformation(Op.INSERT_NAME, Op.CHANGE_TABLE, this.#transform_insertName_changeTable);

        // deleteName
        this.registerSelfTransformation(Op.DELETE_NAME, this.#transform_deleteName_deleteName);
        this.registerBidiTransformation(Op.DELETE_NAME, Op.CHANGE_NAME, this.#transform_deleteName_changeName);

        // changeName
        this.registerSelfTransformation(Op.CHANGE_NAME, this.#transform_changeName_changeName);
        this.registerBidiTransformation(Op.CHANGE_NAME, Op.INSERT_TABLE, this.#transform_changeName_insertTable);
        this.registerBidiTransformation(Op.CHANGE_NAME, Op.CHANGE_TABLE, this.#transform_changeName_changeTable);
        this.registerBidiTransformation(Op.CHANGE_NAME, Op.INSERT_DVRULE, this.#transform_changeName_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_NAME, Op.CHANGE_DVRULE, this.#transform_changeName_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_NAME, Op.INSERT_CFRULE, this.#transform_changeName_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_NAME, Op.CHANGE_CFRULE, this.#transform_changeName_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_NAME, OT_DRAWING_POS_ALIAS, this.#transform_changeName_formulaExpressions);

        // table ranges
        this.registerBidiTransformation(OT_SHEET_TABLES_ALIAS, OT_SHEET_DVRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_TABLES_ALIAS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_TABLES_ALIAS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_TABLES_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_TABLES_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_TABLES_ALIAS, Op.SHEET_SELECTION, null);

        // insertTable
        this.registerSelfTransformation(Op.INSERT_TABLE, this.#transform_insertTable_insertTable);
        this.registerBidiTransformation(Op.INSERT_TABLE, Op.DELETE_TABLE, this.#transform_insertTable_deleteTable);
        this.registerBidiTransformation(Op.INSERT_TABLE, Op.CHANGE_TABLE, this.#transform_insertTable_changeTable);
        this.registerBidiTransformation(Op.INSERT_TABLE, Op.CHANGE_TABLE_COLUMN, this.#transform_insertTable_changeTableCol);

        // deleteTable
        this.registerSelfTransformation(Op.DELETE_TABLE, this.#transform_deleteTable_deleteTable);
        this.registerBidiTransformation(Op.DELETE_TABLE, Op.CHANGE_TABLE, this.#transform_deleteTable_changeTable);
        this.registerBidiTransformation(Op.DELETE_TABLE, Op.CHANGE_TABLE_COLUMN, this.#transform_deleteTable_changeTableCol);

        // changeTable
        this.registerSelfTransformation(Op.CHANGE_TABLE, this.#transform_changeTable_changeTable);
        this.registerBidiTransformation(Op.CHANGE_TABLE, Op.CHANGE_TABLE_COLUMN, this.#transform_changeTable_changeTableCol);
        this.registerBidiTransformation(Op.CHANGE_TABLE, Op.INSERT_DVRULE, this.#transform_changeTable_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_TABLE, Op.CHANGE_DVRULE, this.#transform_changeTable_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_TABLE, Op.INSERT_CFRULE, this.#transform_changeTable_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_TABLE, Op.CHANGE_CFRULE, this.#transform_changeTable_formulaExpressions);
        this.registerBidiTransformation(Op.CHANGE_TABLE, OT_DRAWING_POS_ALIAS, this.#transform_changeTable_formulaExpressions);

        // changeTableColumn
        this.registerSelfTransformation(Op.CHANGE_TABLE_COLUMN, this.#transform_changeTableCol_changeTableCol);

        // data validation
        this.registerBidiTransformation(OT_SHEET_DVRULES_ALIAS, OT_SHEET_CFRULES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_DVRULES_ALIAS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_DVRULES_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_DVRULES_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_DVRULES_ALIAS, Op.SHEET_SELECTION, null);

        // insertDVRule
        this.registerSelfTransformation(Op.INSERT_DVRULE, this.#transform_insertDVRule_insertDVRule);
        this.registerBidiTransformation(Op.INSERT_DVRULE, Op.DELETE_DVRULE, this.#transform_insertDVRule_deleteDVRule);
        this.registerBidiTransformation(Op.INSERT_DVRULE, Op.CHANGE_DVRULE, this.#transform_insertDVRule_changeDVRule);

        // deleteDVRule
        this.registerSelfTransformation(Op.DELETE_DVRULE, this.#transform_deleteDVRule_deleteDVRule);
        this.registerBidiTransformation(Op.DELETE_DVRULE, Op.CHANGE_DVRULE, this.#transform_deleteDVRule_changeDVRule);

        // changeDVRule
        this.registerSelfTransformation(Op.CHANGE_DVRULE, this.#transform_changeDVRule_changeDVRule);

        // conditional formatting
        this.registerBidiTransformation(OT_SHEET_CFRULES_ALIAS, OT_SHEET_NOTES_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CFRULES_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CFRULES_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_CFRULES_ALIAS, Op.SHEET_SELECTION, null);

        // insertCFRule
        this.registerSelfTransformation(Op.INSERT_CFRULE, this.#transform_insertCFRule_insertCFRule);
        this.registerBidiTransformation(Op.INSERT_CFRULE, Op.DELETE_CFRULE, this.#transform_insertCFRule_deleteCFRule);
        this.registerBidiTransformation(Op.INSERT_CFRULE, Op.CHANGE_CFRULE, this.#transform_insertCFRule_changeCFRule);

        // deleteCFRule
        this.registerSelfTransformation(Op.DELETE_CFRULE, this.#transform_deleteCFRule_deleteCFRule);
        this.registerBidiTransformation(Op.DELETE_CFRULE, Op.CHANGE_CFRULE, this.#transform_deleteCFRule_changeCFRule);

        // changeCFRule
        this.registerSelfTransformation(Op.CHANGE_CFRULE, this.#transform_changeCFRule_changeCFRule);

        // cell notes
        this.registerBidiTransformation(OT_SHEET_NOTES_ALIAS, OT_SHEET_COMMENTS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NOTES_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_NOTES_ALIAS, Op.SHEET_SELECTION, null);

        // insertNote
        this.registerSelfTransformation(Op.INSERT_NOTE, this.#transform_insertNote_insertNote);
        this.registerBidiTransformation(Op.INSERT_NOTE, Op.DELETE_NOTE, this.#transform_insertNote_deleteNote);
        this.registerBidiTransformation(Op.INSERT_NOTE, Op.CHANGE_NOTE, this.#transform_insertNote_changeNote);
        this.registerBidiTransformation(Op.INSERT_NOTE, Op.MOVE_NOTES, this.#transform_insertNote_moveNotes);
        this.registerBidiTransformation(Op.INSERT_NOTE, Op.INSERT_COMMENT, this.#transform_insertNote_insertComment);
        this.registerBidiTransformation(Op.INSERT_NOTE, Op.MOVE_COMMENTS, this.#transform_insertNote_moveComments);

        // deleteNote
        this.registerSelfTransformation(Op.DELETE_NOTE, this.#transform_deleteNote_deleteNote);
        this.registerBidiTransformation(Op.DELETE_NOTE, Op.CHANGE_NOTE, this.#transform_deleteNote_changeNote);
        this.registerBidiTransformation(Op.DELETE_NOTE, Op.MOVE_NOTES, this.#transform_deleteNote_moveNotes);

        // changeNote
        this.registerSelfTransformation(Op.CHANGE_NOTE, this.#transform_changeNote_changeNote);
        this.registerBidiTransformation(Op.CHANGE_NOTE, Op.MOVE_NOTES, this.#transform_changeNote_moveNotes);

        // moveNotes
        this.registerSelfTransformation(Op.MOVE_NOTES, this.#transform_moveNotes_moveNotes);
        this.registerBidiTransformation(Op.MOVE_NOTES, Op.INSERT_COMMENT, this.#transform_moveNotes_insertComment);

        // comment threads
        this.registerBidiTransformation(OT_SHEET_COMMENTS_ALIAS, OT_DRAWING_POS_ALIAS, null);
        this.registerBidiTransformation(OT_SHEET_COMMENTS_ALIAS, Op.SHEET_SELECTION, null);

        // insertComment
        this.registerSelfTransformation(Op.INSERT_COMMENT, this.#transform_insertComment_insertComment);
        this.registerBidiTransformation(Op.INSERT_COMMENT, Op.DELETE_COMMENT, this.#transform_insertComment_deleteComment);
        this.registerBidiTransformation(Op.INSERT_COMMENT, Op.CHANGE_COMMENT, this.#transform_insertComment_changeComment);
        this.registerBidiTransformation(Op.INSERT_COMMENT, Op.MOVE_COMMENTS, this.#transform_changeNote_moveNotes);

        // deleteComment
        this.registerSelfTransformation(Op.DELETE_COMMENT, this.#transform_deleteComment_deleteComment);
        this.registerBidiTransformation(Op.DELETE_COMMENT, Op.CHANGE_COMMENT, this.#transform_deleteComment_changeComment);
        this.registerBidiTransformation(Op.DELETE_COMMENT, Op.MOVE_COMMENTS, this.#transform_deleteComment_moveComments);

        // changeComment
        this.registerSelfTransformation(Op.CHANGE_COMMENT, this.#transform_changeComment_changeComment);
        this.registerBidiTransformation(Op.CHANGE_COMMENT, Op.MOVE_COMMENTS, this.#transform_changeNote_moveNotes);

        // moveComments
        this.registerSelfTransformation(Op.MOVE_COMMENTS, this.#transform_moveNotes_moveNotes);

        // insertDrawing
        this.registerSelfTransformation(Op.INSERT_DRAWING, this.#transform_insertDrawing_insertDrawing);
        this.registerBidiTransformation(Op.INSERT_DRAWING, Op.DELETE_DRAWING, this.#transform_insertDrawing_deleteDrawing);
        this.registerBidiTransformation(Op.INSERT_DRAWING, Op.MOVE_DRAWING, this.#transform_insertDrawing_moveDrawing);
        this.registerBidiTransformation(Op.INSERT_DRAWING, OT_DRAWING_POS_ALIAS, this.#transform_insertDrawing_drawingPos);
        this.registerBidiTransformation(Op.INSERT_DRAWING, Op.SHEET_SELECTION, this.#transform_insertDrawing_sheetSelection);

        // deleteDrawing
        this.registerSelfTransformation(Op.DELETE_DRAWING, this.#transform_deleteDrawing_deleteDrawing);
        this.registerBidiTransformation(Op.DELETE_DRAWING, Op.MOVE_DRAWING, this.#transform_deleteDrawing_moveDrawing);
        this.registerBidiTransformation(Op.DELETE_DRAWING, OT_DRAWING_POS_ALIAS, this.#transform_deleteDrawing_drawingPos);
        this.registerBidiTransformation(Op.DELETE_DRAWING, Op.SHEET_SELECTION, this.#transform_deleteDrawing_sheetSelection);

        // changeDrawing
        this.registerSelfTransformation(Op.CHANGE_DRAWING, this.#transform_changeDrawing_changeDrawing);
        this.registerBidiTransformation(Op.CHANGE_DRAWING, OT_CHART_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_DRAWING, OT_DRAWING_TEXT_ALIAS, null);
        this.registerBidiTransformation(Op.CHANGE_DRAWING, Op.SHEET_SELECTION, null);
        this.registerBidiTransformation(Op.CHANGE_DRAWING, Op.POSITION, null);

        // moveDrawing
        this.registerSelfTransformation(Op.MOVE_DRAWING, this.#transform_moveDrawing_moveDrawing);
        this.registerBidiTransformation(Op.MOVE_DRAWING, OT_DRAWING_POS_ALIAS, this.#transform_moveDrawing_drawingPos);
        this.registerBidiTransformation(Op.MOVE_DRAWING, Op.SHEET_SELECTION, this.#transform_moveDrawing_sheetSelection);

        // sheetSelection
        this.registerSelfTransformation(Op.SHEET_SELECTION, 'INTERNAL ERROR: "sheetSelection" cannot be self-transformed');
        this.registerBidiTransformation(Op.SHEET_SELECTION, OT_CHART_ALIAS, null);
        this.registerBidiTransformation(Op.SHEET_SELECTION, OT_DRAWING_TEXT_ALIAS, null);
        this.registerBidiTransformation(Op.SHEET_SELECTION, Op.POSITION, null);
    }

    // public methods ---------------------------------------------------------

    /**
     * Transforms the passed sheet selection against all document operations.
     *
     * @param selectState
     *  The sheet selection state to be transformed.
     *
     * @param actions
     *  The operation actions used to transform the sheet selection state.
     *
     * @returns
     *  Whether the transformed sheet selection state remains valid (`true`);
     *  or has become invalid (e.g. if the selected sheet has been deleted) and
     *  needs to be dropped (`false`).
     */
    override transformSelectionState(selectState: Dict, actions: OperationAction[]): boolean {

        // extract the spreadsheet-specific selection properties
        const sheet = to.number(selectState.sheet, -1);
        const selection = selectState.selection;
        if ((sheet < 0) || !(selection instanceof SheetSelection)) { return true; }

        // create a synthetic "sheetSelection" operation that will be transformed
        const selectOp: Op.SheetSelectionOperation = {
            name: Op.SHEET_SELECTION,
            sheet,
            ranges: selection.ranges.toOpStr(),
            index: selection.active,
            active: selection.address.toOpStr(),
            origin: selection.origin?.toOpStr(),
            drawings: json.deepClone(selection.drawings)
        };

        // transform the synthetic operation
        const resultOps = this.transformOperation(selectOp, actions);

        // sheet may have been deleted
        if (resultOps.length === 0) { return false; }

        // check that the transformation handlers did not generate other operations
        if ((resultOps.length > 1) || (resultOps[0].name !== Op.SHEET_SELECTION)) {
            throw new Error("invalid transformation result, expected single selection operation");
        }

        // update the properties in the selection state object
        const resultOp = resultOps[0] as Op.SheetSelectionOperation;
        selectState.sheet = resultOp.sheet;
        selectState.selection = this.#parseSheetSelection(resultOp);

        // selection state transformed successfully
        return true;
    }

    // private methods --------------------------------------------------------

    /**
     * Parses the passed string as a cell address, throws on error.
     */
    #parseAddress(parseStr: string): Address {
        const address = this.#addressFactory.parseAddress(parseStr);
        this.ensure(address, "OP ERROR: cannot parse cell address");
        return address;
    }

    /**
     * Parses the passed string as a list of cell addresses, throws on error.
     */
    #parseAddressList(parseStr: string): AddressArray {
        const addresses = this.#addressFactory.parseAddressList(parseStr);
        this.ensure(addresses, "OP ERROR: cannot parse cell address list");
        return addresses;
    }

    /**
     * Parses the passed string as a range address, throws on error.
     */
    #parseRange(parseStr: string): Range {
        const range = this.#addressFactory.parseRange(parseStr);
        this.ensure(range, "OP ERROR: cannot parse range address");
        return range;
    }

    /**
     * Parses the passed string as a list of index intervals, throws on error.
     */
    #parseIntervalList(parseStr: string, columns: boolean): IntervalArray {
        const intervals = this.#addressFactory.parseIntervalList(parseStr, columns);
        this.ensure(intervals, "OP ERROR: cannot parse interval list");
        return intervals;
    }

    /**
     * Parses the passed string as a list of range addresses, throws on error.
     */
    parseRangeList(parseStr: string): RangeArray {
        const ranges = this.#addressFactory.parseRangeList(parseStr);
        this.ensure(ranges, "OP ERROR: cannot parse cell range list");
        return ranges;
    }

    /**
     * Throws an `OTError`, if both operations contain the same sheet name.
     */
    #ensureUniqueSheetNames(lclOp: AnySheetNameOperation, extOp: AnySheetNameOperation): void {
        if (lclOp.sheetName && extOp.sheetName) {
            this.ensure(!str.equalsICC(lclOp.sheetName, extOp.sheetName), "NOT IMPLEMENTED: different sheets cannot receive the same name");
        }
    }

    /**
     * Ensures that there are not too many sheets after inserting two sheets.
     */
    #ensureValidSheetCountForInsert(): void {
        if (MAX_SHEET_COUNT <= 0) { return; }
        this.ensure(this.#docSnapshot, "OT ERROR: missing document snapshot");
        this.ensure(this.#docSnapshot.getSheetCount() + 2 <= MAX_SHEET_COUNT, "OT ERROR: too many sheets in document");
    }

    /**
     * Ensures that there is a visible sheet left when deleting sheets.
     */
    #ensureValidSheetCountForDelete(...skipSheets: number[]): void {
        this.ensure(this.#docSnapshot, "OT ERROR: missing document snapshot");
        this.ensure(this.#docSnapshot.hasVisibleSheet(...skipSheets), "OT ERROR: cannot delete all sheets in document");
    }

    /**
     * Throws an `OTError`, if both operations contain overlapping cell range
     * addresses.
     */
    #ensureUniqueTargetRanges(lclOp: AnyRangeListOperation, extOp: AnyRangeListOperation): void {
        if (lclOp.ranges && extOp.ranges) {
            const lclRanges = this.parseRangeList(lclOp.ranges);
            const extRanges = this.parseRangeList(extOp.ranges);
            this.ensure(!lclRanges.overlaps(extRanges), "NOT IMPLEMENTED: cannot insert objects with overlapping target ranges");
        }
    }

    /**
     * Creates an address transformer for the passed index intervals operation.
     */
    createIntervalTransformer(intervalOp: Op.IntervalListOperation, dir: Direction): OTAddressTransformer {
        const intervals = this.#parseIntervalList(intervalOp.intervals, !isVerticalDir(dir));
        const transformer = AddressTransformer.fromIntervals(this.#addressFactory, intervals, dir) as OTAddressTransformer;
        transformer.sheetIndex = intervalOp.sheet;
        return transformer;
    }

    /**
     * Creates an address transformer for the passed "moveCells" "operation.
     */
    #createRangeTransformer(moveOp: Op.MoveCellsOperation): OTAddressTransformer {
        const range = this.#parseRange(moveOp.range);
        const transformer = AddressTransformer.fromRange(this.#addressFactory, range, moveOp.dir) as OTAddressTransformer;
        transformer.sheetIndex = moveOp.sheet;
        return transformer;
    }

    /**
     * Checks that both passed DV rule operations contain an "index" property.
     */
    #parseDVRuleIndexes(lclOp: Op.DVRuleOperation, extOp: Op.DVRuleOperation): { lclIdx: number; extIdx: number } {
        this.ensure(is.number(lclOp.index) && is.number(extOp.index), "OP ERROR: missing rule index in DV rule operation");
        return { lclIdx: lclOp.index, extIdx: extOp.index };
    }

    /**
     * Parses the target range addresses of both DV rule operations.
     */
    #parseDVRuleRanges(lclOp: Op.DVRuleOperation, extOp: Op.DVRuleOperation): { lclRanges: RangeArray; extRanges: RangeArray; equals: boolean; overlaps: boolean } {
        this.ensure(lclOp.ranges && extOp.ranges, "OP ERROR: missing target ranges in DV rule operation");
        const lclRanges = this.parseRangeList(lclOp.ranges).merge();
        const extRanges = this.parseRangeList(extOp.ranges).merge();
        const equals = lclRanges.deepEquals(extRanges);
        const overlaps = equals || lclRanges.overlaps(extRanges);
        return { lclRanges, extRanges, equals, overlaps };
    }

    /**
     * Checks that both passed CF rule operations contain integral "id"
     * properties.
     */
    #parseCFRuleIndexes(lclOp: Op.CFRuleOperation, extOp: Op.CFRuleOperation): { lclIdx: number; extIdx: number } {
        const lclIdx = parseIndex(lclOp.id), extIdx = parseIndex(extOp.id);
        this.ensure((lclIdx >= 0) && (extIdx >= 0), "OP ERROR: missing rule index in CF rule operation");
        return { lclIdx, extIdx };
    }

    /**
     * Transforms the drawing anchor property in the passed operation with
     * drawing attributes (drawing objects, notes, comments) according to the
     * settings in the address transformer.
     */
    #transformDrawingAnchor(drawingOp: OptAttrsOperation, transformer: AddressTransformer, options?: TransformAddressOptions): boolean {
        let deleted = false;
        transformAttribute(drawingOp, "drawing", "anchor", anchorStr => {
            if (!is.string(anchorStr)) { return undefined; }
            const cellAnchor = this.#addressFactory.parseCellAnchor(anchorStr);
            const newCellAnchor = transformer.transformCellAnchor(cellAnchor, options);
            this.warnIf(newCellAnchor, "NOT IMPLEMENTED: cannot transform absolute drawing anchor (moved cells)");
            deleted = !newCellAnchor;
            return newCellAnchor?.toOpStr();
        });
        return deleted;
    }

    /**
     * Extracts different parts of a "moveNotes" or "moveComments" operation.
     */
    #parseMoveAnchors(moveOp: Op.MoveAnchorsOperation): MoveAnchorsParts {
        const fromAnchors = this.#parseAddressList(moveOp.from);
        const toAnchors = this.#parseAddressList(moveOp.to);
        this.ensure(fromAnchors.length === toAnchors.length, `OP ERROR: different number of addresses in "${moveOp.name}" operation`);
        return { fromAnchors, toAnchors };
    }

    /**
     * Extracts different parts of a "moveDrawing" operation.
     */
    #parseMoveDrawing(moveOp: Op.MoveDrawingOperation, splitSheet?: boolean): MoveDrawingParts {

        // check validity of the operation
        const fromPos = moveOp.start, toPos = moveOp.to;
        const arrIdx = fromPos.length - 1;
        this.ensure(arrIdx >= 1, 'OP ERROR: start position in "moveDrawing" operation too short');
        this.ensure((arrIdx === toPos.length - 1) && equalPositions(fromPos, toPos, arrIdx), 'OP ERROR: "moveDrawing" target position needs same parent');

        // return the position parts
        return {
            sheetIdx: fromPos[0],
            fromPos: splitSheet ? getSheetPos(fromPos) : fromPos,
            toPos: splitSheet ? getSheetPos(toPos) : toPos,
            arrIdx: splitSheet ? (arrIdx - 1) : arrIdx,
            fromIdx: fromPos[arrIdx],
            toIdx: toPos[arrIdx]
        };
    }

    /**
     * Parses a "sheetSelection" operation to a `SheetSelection` object.
     */
    #parseSheetSelection(selectOp: Op.SheetSelectionOperation): SheetSelection {
        const selection = new SheetSelection(this.parseRangeList(selectOp.ranges), selectOp.index, this.#parseAddress(selectOp.active), selectOp.drawings);
        if (selectOp.origin) { selection.origin = this.#parseAddress(selectOp.origin); }
        return selection;
    }

    // operation generators ---------------------------------------------------

    #generateDeleteDVRule(ruleOp: Op.DVRuleOperation): Op.DeleteDVRuleOperation {
        return Op.deleteDVRule(ruleOp.sheet, this.#indexedDVRules ? ruleOp.index! : ruleOp.ranges!);
    }

    #generateDeleteCFRule(ruleOp: Op.CFRuleOperation): Op.DeleteCFRuleOperation {
        return Op.deleteCFRule(ruleOp.sheet, ruleOp.id);
    }

    #generateDeleteNote(noteOp: Op.CellAnchorOperation): Op.DeleteNoteOperation {
        return Op.deleteNote(noteOp.sheet, noteOp.anchor);
    }

    #generateDeleteComment(commentOp: Op.CommentOperation): Opt<Op.DeleteCommentOperation> {
        return (commentOp.index === 0) ? Op.deleteComment(commentOp.sheet, commentOp.anchor) : undefined;
    }

    #generateDeleteOperation(operation: Operation): Opt<Operation> {
        return this.#deleteOpGeneratorMap.get(operation.name)?.call(this, operation);
    }

    // document snapshots -----------------------------------------------------

    #transform_insertSheet_docSnapshot(insertOp: Op.InsertSheetOperation, snapshot: DocumentSnapshot): void {
        const supported = isSupportedSheetType(insertOp.type ?? SheetType.WORKSHEET);
        const visible = pick.boolean(pick.dict(insertOp.attrs, "sheet"), "visible", true);
        snapshot.insertSheet(insertOp.sheet, insertOp.sheetName, supported, visible);
    }

    #transform_deleteSheet_docSnapshot(deleteOp: Op.DeleteSheetOperation, snapshot: DocumentSnapshot): void {
        snapshot.deleteSheet(deleteOp.sheet);
    }

    #transform_moveSheet_docSnapshot(moveOp: Op.MoveSheetOperation, snapshot: DocumentSnapshot): void {
        snapshot.moveSheet(moveOp.sheet, moveOp.to);
    }

    #transform_moveSheets_docSnapshot(moveOp: Op.MoveSheetsOperation, snapshot: DocumentSnapshot): void {
        snapshot.moveSheets(moveOp.sheets);
    }

    #transform_copySheet_docSnapshot(copyOp: Op.CopySheetOperation, snapshot: DocumentSnapshot): void {
        snapshot.copySheet(copyOp.sheet, copyOp.to, copyOp.sheetName, copyOp.tableNames);
    }

    #transform_changeSheet_docSnapshot(changeOp: Op.ChangeSheetOperation, snapshot: DocumentSnapshot): void {
        const visible = pick.boolean(pick.dict(changeOp.attrs, "sheet"), "visible");
        snapshot.changeSheet(changeOp.sheet, changeOp.sheetName, visible);
    }

    #transform_insertName_docSnapshot(insertOp: Op.InsertNameOperation, snapshot: DocumentSnapshot): void {
        snapshot.insertName(insertOp.sheet ?? null, insertOp.label);
    }

    #transform_deleteName_docSnapshot(deleteOp: Op.DeleteNameOperation, snapshot: DocumentSnapshot): void {
        snapshot.deleteName(deleteOp.sheet ?? null, deleteOp.label);
    }

    #transform_changeName_docSnapshot(changeOp: Op.ChangeNameOperation, snapshot: DocumentSnapshot): void {
        if (changeOp.newLabel) {
            snapshot.changeName(changeOp.sheet ?? null, changeOp.label, changeOp.newLabel);
        }
    }

    #transform_insertTable_docSnapshot(insertOp: Op.InsertTableOperation, snapshot: DocumentSnapshot): void {
        // ignore auto-filter
        if (insertOp.table) {
            const range = this.#parseRange(insertOp.range);
            const attrs = pick.dict(insertOp.attrs, "table");
            const header = attrs?.headerRow === true;
            const footer = attrs?.footerRow === true;
            snapshot.insertTable(insertOp.sheet, insertOp.table, range, header, footer, insertOp.headers);
        }
    }

    #transform_deleteTable_docSnapshot(deleteOp: Op.DeleteTableOperation, snapshot: DocumentSnapshot): void {
        // ignore auto-filter
        if (deleteOp.table) {
            snapshot.deleteTable(deleteOp.sheet, deleteOp.table);
        }
    }

    #transform_changeTable_docSnapshot(changeOp: Op.ChangeTableOperation, snapshot: DocumentSnapshot): void {
        // ignore auto-filter
        if (changeOp.table) {
            const attrs = pick.dict(changeOp.attrs, "table");
            snapshot.changeTable(changeOp.sheet, changeOp.table, {
                name: changeOp.newName,
                range: changeOp.range ? this.#parseRange(changeOp.range) : undefined,
                header: pick.boolean(attrs, "headerRow"),
                footer: pick.boolean(attrs, "footerRow"),
                columns: pick.array(attrs, "columns") as Opt<string[]>
            });
        }
    }

    #transform_anyOperation_docSnapshot(operation: Operation, docSnapshot: DocumentSnapshot): void {
        try {
            this.#snapshotTransformMap.get(operation.name)?.call(this, operation, docSnapshot);
        } catch (err) {
            const msg = (err instanceof Error) ? err.message : "unknown error";
            this.error(`OT ERROR: cannot transform document snapshot: ${msg}`);
        }
    }

    #initializeActionSnapshot(extOps: Operation[], userData: Opt<object>): void {

        // use a clone of the snapshot to transform the formula expressions
        // (it will be transformed against all local operations)
        this.#docSnapshot = (userData instanceof DocumentSnapshot) ? userData.clone() : undefined;

        // rescue original operations for snapshot transformation afterwards
        this.#origExtOps = extOps.map(extOp => this.#snapshotTransformMap.has(extOp.name) ? json.deepClone(extOp) : undefined);
    }

    #transformActionSnapshot(_extOps: Operation[], userData: Opt<object>): void {

        // transform the original snapshot against the external operations
        if (this.#origExtOps && (userData instanceof DocumentSnapshot)) {
            this.#origExtOps.forEach(extOp => extOp && this.#transform_anyOperation_docSnapshot(extOp, userData));
        }
    }

    #initializeOpsSnapshot(lclOp: Operation): void {
        // rescue original operation for snapshot transformation afterwards
        this.#origLclOp = this.#snapshotTransformMap.has(lclOp.name) ? json.deepClone(lclOp) : undefined;
    }

    #transformOpsSnapshot(): void {
        if (this.#origLclOp && this.#docSnapshot) {
            this.#transform_anyOperation_docSnapshot(this.#origLclOp, this.#docSnapshot);
        }
    }

    // formula expressions ----------------------------------------------------

    /**
     * Returns the optional reference address of the operation (the value of
     * the property "ref").
     */
    #parseRefAddress(op: AnyRefOperation, def?: Address): Opt<Address> {

        // missing property: return default
        if (!this.#customRefAddress || !is.string(op.ref)) { return def; }

        // ODF: parse formula expression with sheet name
        this.ensure(this.#docSnapshot, "OT ERROR: missing document snapshot for reference address");
        const ranges = this.#rangeListParser.parse(this.#docSnapshot, op.ref, { skipSheets: true });
        const range = (ranges?.length === 1) ? ranges[0] : undefined;
        this.ensure(range?.single(), "OP ERROR: invalid formula expression for reference address");
        return range?.a1;
    }

    /**
     * Returns the reference address of the operation (the top-left cell of the
     * bounding range of all cell range addresses in the property "ranges", or
     * the value of the property "ref").
     */
    #parseRangesRefAddress(rangesOp: AnyRangeListOperation): Opt<Address> {

        // ODF: parse formula expression with sheet name
        if (this.#customRefAddress) { return this.#parseRefAddress(rangesOp); }

        // OOXML: use the top-left cell of the bounding range
        const ranges = rangesOp.ranges ? this.parseRangeList(rangesOp.ranges) : undefined;
        return ranges?.boundary()?.a1;
    }

    #transformFormula(formula: unknown, config: FormulaTransformConfig, failMsg: string, resultFn?: TransformFormulaResultFn): Opt<string> {

        // check validity of input data
        if (!is.string(formula) || !formula) { return undefined; }
        this.ensure(this.#docSnapshot, `OT ERROR: missing document snapshot for ${failMsg}`);
        this.ensure(config.refAddress, `NOT IMPLEMENTED: missing reference address for ${failMsg}`);

        // parse the formula expression
        const tokenArray = new SnapshotTokenArray(this.#docSnapshot, this.#formulaParser, config.formulaType, formula, config.refSheet, config.refAddress);

        // generate the new formula expression
        return config.updateTask.transformFormula(tokenArray, this.#formulaGrammar, resultFn);
    }

    #transformFmlaAttributes(attrOp: OptAttrsOperation, config: FormulaTransformConfig, failMsg: string, family: string, ...keys: string[]): void {
        keys.forEach(key => transformAttribute(attrOp, family, key, formula => this.#transformFormula(formula, config, `${failMsg} (family="${family}" key="${key}")`)));
    }

    #transformFmlaProperties(data: unknown, config: FormulaTransformConfig, failMsg: string, ...keys: string[]): void {
        if (is.dict(data)) {
            keys.forEach(key => this.#transformFormula(data[key], config, `${failMsg} (key="${key}")`, newExpr => (data[key] = newExpr)));
        }
    }

    #transform_changeCells_updateTask(cellsOp: Op.ChangeCellsOperation, updateTask: FormulaUpdateTask): void {
        const config: FormulaTransformConfig = { updateTask, formulaType: "cell", refSheet: cellsOp.sheet };
        dict.forEach(cellsOp.contents, (data, key) => {
            config.refAddress = this.#parseRange(key).a1;
            this.#transformFmlaProperties(data, config, "cell formula", "f");
        });
    }

    #transform_definedName_updateTask(nameOp: AnyRefOperation, updateTask: FormulaUpdateTask): void {
        const config: FormulaTransformConfig = { updateTask, formulaType: "name", refSheet: nameOp.sheet ?? null, refAddress: this.#parseRefAddress(nameOp, A1) };
        this.#transformFmlaProperties(nameOp, config, "defined name", "formula");
    }

    #transform_dvRule_updateTask(ruleOp: Op.DVRuleOperation, updateTask: FormulaUpdateTask): void {
        const config: FormulaTransformConfig = { updateTask, formulaType: "rule", refSheet: ruleOp.sheet, refAddress: this.#parseRangesRefAddress(ruleOp) };
        this.#transformFmlaProperties(ruleOp, config, "data validation", "value1", "value2");
    }

    #transform_cfRule_updateTask(ruleOp: AnyCFRuleOperation, updateTask: FormulaUpdateTask): void {
        const config: FormulaTransformConfig = { updateTask, formulaType: "rule", refSheet: ruleOp.sheet, refAddress: this.#parseRangesRefAddress(ruleOp) };
        this.#transformFmlaProperties(ruleOp, config, "conditional formatting", "value1", "value2");
        if (is.array(ruleOp.colorScale)) {
            ruleOp.colorScale.forEach(colorStep => this.#transformFmlaProperties(colorStep, config, "conditional formatting (color step)", "v"));
        }
        const dataBar = pick.dict(ruleOp, "dataBar");
        if (dataBar) {
            this.#transformFmlaProperties(dataBar.r1, config, "conditional formatting (data bar)", "v");
            this.#transformFmlaProperties(dataBar.r2, config, "conditional formatting (data bar)", "v");
        }
    }

    #transform_textLink_updateTask(drawingOp: AnyDrawingOperation, updateTask: FormulaUpdateTask): void {
        const config: FormulaTransformConfig = { updateTask, formulaType: "link", refSheet: drawingOp.start[0], refAddress: A1 };
        this.#transformFmlaAttributes(drawingOp, config, "drawing source link", "text", "link");
    }

    #transform_chartSeries_updateTask(chartOp: AnyDrawingOperation, updateTask: FormulaUpdateTask): void {
        const config: FormulaTransformConfig = { updateTask, formulaType: "link", refSheet: chartOp.start[0], refAddress: A1 };
        this.#transformFmlaAttributes(chartOp, config, "chart data source link", "series", "title", "values", "names", "bubbles");
    }

    #transform_formulaOp_updateTask(fmlaOp: Operation, updateTask: FormulaUpdateTask): void {
        this.#formulaTransformMap.get(fmlaOp.name)?.call(this, fmlaOp, updateTask);
    }

    // auto-styles ------------------------------------------------------------

    #transform_moveAutoStyle_changeIntervals(insert: boolean, checkNoOp: boolean, styleOp: Op.AutoStyleOperation, changeOp: Op.ChangeIntervalsOperation): void {
        if (this.transformAutoStyleProperty(changeOp, "s", "cell", styleOp, insert) && !changeOp.s && checkNoOp) {
            checkChangeIntervalsNoOp(changeOp);
        }
    }

    #transform_moveAutoStyle_changeCells(insert: boolean, styleOp: Op.AutoStyleOperation, changeOp: Op.ChangeCellsOperation): void {
        const { contents } = changeOp;
        dict.forEach(contents, (data, key) => {
            if (isCellData(data) && this.transformAutoStyleProperty(data, "s", "cell", styleOp, insert) && is.empty(data)) {
                delete contents[key];
            }
        });
        checkChangeCellsNoOp(changeOp);
    }

    // charts -----------------------------------------------------------------

    #transform_anyChart_drawingText(chartOp: Op.DrawingOperation, textOp: Op.DrawingOperation): void {
        this.ensure(!equalPositions(chartOp.start, textOp.start, chartOp.start.length), "NOT IMPLEMENTED: shapes embedded in charts not supported");
    }

    // insertNumberFormat -----------------------------------------------------

    #transform_insertNumFmt_insertNumFmt(lclOp: Op.InsertNumberFormatOperation, extOp: Op.InsertNumberFormatOperation): void {
        // both operations have inserted the same number format: do not repeat that on either side
        if ((lclOp.id === extOp.id) && (lclOp.code === extOp.code)) {
            setOperationRemoved(lclOp, extOp);
        } else {
            // inserting number formats is very rare, shifting identifiers through style sheet operations not implemented
            this.error("NOT IMPLEMENTED: cannot transform different new number formats");
        }
    }

    #transform_insertNumFmt_deleteNumFmt(insertOp: Op.InsertNumberFormatOperation, deleteOp: Op.DeleteNumberFormatOperation): void {
        // remove delete operation if a number format will be inserted with the same identifier
        if (insertOp.id === deleteOp.id) {
            setOperationRemoved(deleteOp);
        }
    }

    // deleteNumberFormat -----------------------------------------------------

    #transform_deleteNumFmt_deleteNumFmt(lclOp: Op.DeleteNumberFormatOperation, extOp: Op.DeleteNumberFormatOperation): void {
        // both operations have deleted the same number format: do not repeat that on either side
        if (lclOp.id === extOp.id) {
            setOperationRemoved(lclOp, extOp);
        }
    }

    // insertSheet ------------------------------------------------------------

    #transform_insertSheet_insertSheet(lclOp: Op.InsertSheetOperation, extOp: Op.InsertSheetOperation): void {

        // fail if both sheets get the same name, or if inserting sheets exceeds the configured limit
        this.#ensureUniqueSheetNames(lclOp, extOp);
        this.#ensureValidSheetCountForInsert();

        const result = transformIndexInsertInsert(lclOp.sheet, extOp.sheet);
        lclOp.sheet = result.lclIdx;
        extOp.sheet = result.extIdx;
    }

    #transform_insertSheet_deleteSheet(insertOp: Op.InsertSheetOperation, deleteOp: Op.DeleteSheetOperation): void {
        const result = transformIndexInsertDelete(insertOp.sheet, deleteOp.sheet);
        insertOp.sheet = result.lclIdx;
        deleteOp.sheet = result.extIdx;
    }

    #transform_insertSheet_moveSheet(insertOp: Op.InsertSheetOperation, moveOp: Op.MoveSheetOperation): void {
        const result = transformIndexInsertMove(insertOp.sheet, moveOp.sheet, moveOp.to);
        insertOp.sheet = result.shiftIdx;
        assignMoveSheet(moveOp, result.moveRes);
    }

    #transform_insertSheet_copySheet(insertOp: Op.InsertSheetOperation, copyOp: Op.CopySheetOperation): void {

        // fail if both sheets get the same name, or if inserting sheets exceeds the configured limit
        this.#ensureUniqueSheetNames(insertOp, copyOp);
        this.#ensureValidSheetCountForInsert();

        const result = transformIndexInsertCopy(insertOp.sheet, copyOp.sheet, copyOp.to);
        insertOp.sheet = result.shiftIdx;
        assignMoveSheet(copyOp, result.moveRes);
    }

    #transform_insertSheet_moveSheets(insertOp: Op.InsertSheetOperation, sortOp: Op.MoveSheetsOperation): void {
        const result = transformIndexInsertSort(insertOp.sheet, sortOp.sheets);
        insertOp.sheet = result.shiftIdx;
        assignMoveSheets(sortOp, result.sortVec);
    }

    #transform_insertSheet_changeSheet(insertOp: Op.InsertSheetOperation, changeOp: Op.ChangeSheetOperation): void {
        this.#ensureUniqueSheetNames(insertOp, changeOp);
        this.#transform_insertSheet_sheetIndex(insertOp, changeOp);
    }

    #transform_insertSheet_sheetIndex(insertOp: Op.InsertSheetOperation, sheetOp: AnySheetOperation): void {
        if (is.number(sheetOp.sheet)) {
            sheetOp.sheet = transformIndexInsert(sheetOp.sheet, insertOp.sheet, 1);
        }
    }

    #transform_insertSheet_drawingPos(insertOp: Op.InsertSheetOperation, drawingOp: AnyDrawingOperation): void {
        transformSheetInDrawingPositions(drawingOp, xfSheet => transformIndexInsert(xfSheet, insertOp.sheet, 1));
    }

    // deleteSheet ------------------------------------------------------------

    #transform_deleteSheet_deleteSheet(lclOp: Op.DeleteSheetOperation, extOp: Op.DeleteSheetOperation): void {

        // set both operations to "removed" state, if they delete the same sheet
        const result = transformIndexDeleteDelete(lclOp.sheet, extOp.sheet);
        if (!result) { setOperationRemoved(lclOp, extOp); return; }

        // fail if the deleted sheets are the last two sheets in the document
        this.#ensureValidSheetCountForDelete(lclOp.sheet, extOp.sheet);

        lclOp.sheet = result.lclIdx;
        extOp.sheet = result.extIdx;
    }

    #transform_deleteSheet_moveSheet(deleteOp: Op.DeleteSheetOperation, moveOp: Op.MoveSheetOperation): void {
        const result = transformIndexDeleteMove(deleteOp.sheet, moveOp.sheet, moveOp.to);
        deleteOp.sheet = result.shiftIdx;
        assignMoveSheet(moveOp, result.moveRes);
    }

    #transform_deleteSheet_copySheet(deleteOp: Op.DeleteSheetOperation, copyOp: Op.CopySheetOperation): Opt<TransformOpsResult> {
        const result = transformIndexDeleteCopy(deleteOp.sheet, copyOp.sheet, copyOp.to);
        deleteOp.sheet = result.shiftIdx;
        assignMoveSheet(copyOp, result.moveRes);
        // delete the cloned sheet, if source sheet has been deleted
        return is.number(result.delToIdx) ? { localOpsAfter: Op.deleteSheet(result.delToIdx) } : undefined;
    }

    #transform_deleteSheet_moveSheets(deleteOp: Op.DeleteSheetOperation, sortOp: Op.MoveSheetsOperation): void {
        const result = transformIndexDeleteSort(deleteOp.sheet, sortOp.sheets);
        deleteOp.sheet = result.shiftIdx;
        assignMoveSheets(sortOp, result.sortVec);
    }

    #transform_deleteSheet_changeSheet(deleteOp: Op.DeleteSheetOperation, changeOp: Op.ChangeSheetOperation): void {

        // DOCS-2142: do not delete the last visible sheet, if another sheet will be hidden
        if ((deleteOp.sheet !== changeOp.sheet) && isHideSheet(changeOp)) {
            this.#ensureValidSheetCountForDelete(deleteOp.sheet, changeOp.sheet);
        }

        // generic transformation of sheet index
        this.#transform_deleteSheet_sheetIndex(deleteOp, changeOp);
    }

    #transform_deleteSheet_formulaExpressions(deleteOp: Op.DeleteSheetOperation, fmlaOp: Operation): void {
        if (!isOperationRemoved(fmlaOp)) {
            const updateTask = new DeleteSheetUpdateTask(deleteOp.sheet);
            this.#transform_formulaOp_updateTask(fmlaOp, updateTask);
        }
    }

    #transform_deleteSheet_sheetIndex(deleteOp: Op.DeleteSheetOperation, sheetOp: AnySheetOperation): void {
        if (is.number(sheetOp.sheet)) {
            sheetOp.sheet = transformIndexDelete(sheetOp.sheet, deleteOp.sheet, 1);
            if (!is.number(sheetOp.sheet)) { setOperationRemoved(sheetOp); return; }
        }
        this.#transform_deleteSheet_formulaExpressions(deleteOp, sheetOp);
    }

    #transform_deleteSheet_drawingPos(deleteOp: Op.DeleteSheetOperation, drawingOp: AnyDrawingOperation): void {
        transformSheetInDrawingPositions(drawingOp, xfSheet => transformIndexDelete(xfSheet, deleteOp.sheet, 1));
        this.#transform_deleteSheet_formulaExpressions(deleteOp, drawingOp);
    }

    // moveSheet --------------------------------------------------------------

    #transform_moveSheet_moveSheet(lclOp: Op.MoveSheetOperation, extOp: Op.MoveSheetOperation): void {
        // transform and reassign all sheet indexes (either operation may become a no-op)
        const result = transformIndexMoveMove(lclOp.sheet, lclOp.to, extOp.sheet, extOp.to);
        assignMoveSheet(lclOp, result.lclRes);
        assignMoveSheet(extOp, result.extRes);
    }

    #transform_moveSheet_copySheet(moveOp: Op.MoveSheetOperation, copyOp: Op.CopySheetOperation): void {
        // transform and reassign all sheet indexes (move operation may become a no-op)
        const result = transformIndexMoveCopy(moveOp.sheet, moveOp.to, copyOp.sheet, copyOp.to);
        assignMoveSheet(moveOp, result.moveRes);
        assignMoveSheet(copyOp, result.copyRes);
    }

    #transform_moveSheet_moveSheets(moveOp: Op.MoveSheetOperation, sortOp: Op.MoveSheetsOperation): void {
        // transform and reassign all sheet indexes (move operation may become a no-op)
        const result = transformIndexMoveSort(moveOp.sheet, moveOp.to, sortOp.sheets);
        assignMoveSheet(moveOp, result.moveRes);
        assignMoveSheets(sortOp, result.sortVec);
    }

    #transform_moveSheet_sheetIndex(moveOp: Op.MoveSheetOperation, sheetOp: AnySheetOperation): void {
        if (is.number(sheetOp.sheet)) {
            sheetOp.sheet = transformIndexMove(sheetOp.sheet, moveOp.sheet, 1, moveOp.to);
        }
    }

    #transform_moveSheet_drawingPos(moveOp: Op.MoveSheetOperation, drawingOp: AnyDrawingOperation): void {
        transformSheetInDrawingPositions(drawingOp, xfSheet => transformIndexMove(xfSheet, moveOp.sheet, 1, moveOp.to));
    }

    // copySheet --------------------------------------------------------------

    #transform_copySheet_copySheet(lclOp: Op.CopySheetOperation, extOp: Op.CopySheetOperation): void {

        // fail if both sheets get the same name, or if inserting sheets exceeds the configured limit
        this.#ensureUniqueSheetNames(lclOp, extOp);
        this.#ensureValidSheetCountForInsert();

        const result = transformIndexCopyCopy(lclOp.sheet, lclOp.to, extOp.sheet, extOp.to);
        assignMoveSheet(lclOp, result.lclRes);
        assignMoveSheet(extOp, result.extRes);
    }

    #transform_copySheet_moveSheets(copyOp: Op.MoveSheetOperation, sortOp: Op.MoveSheetsOperation): void {
        const result = transformIndexCopySort(copyOp.sheet, copyOp.to, sortOp.sheets);
        assignMoveSheet(copyOp, result.moveRes);
        assignMoveSheets(sortOp, result.sortVec);
    }

    #transform_copySheet_changeSheet(copyOp: Op.CopySheetOperation, changeOp: Op.ChangeSheetOperation): Opt<TransformOpsResult> {
        this.#ensureUniqueSheetNames(copyOp, changeOp);
        return this.#transform_copySheet_sheetIndex(copyOp, changeOp);
    }

    #transform_copySheet_formulaExpressions(copyOp: Op.CopySheetOperation, fmlaOp: Operation): void {
        if (!isOperationRemoved(fmlaOp)) {
            const updateTask = new RenameSheetUpdateTask(copyOp.sheet, copyOp.sheetName, copyOp.tableNames);
            this.#transform_formulaOp_updateTask(fmlaOp, updateTask);
        }
    }

    #transform_copySheet_sheetIndex(copyOp: Op.CopySheetOperation, sheetOp: AnySheetOperation): Opt<TransformOpsResult> {

        // local "copySheet": replicate external operation locally in the new sheet
        // external "copySheet": replicate local operation externally in the new sheet (for simplicity,
        // otherwise it would be needed to generate matching reversed operations for each local operation)
        let result: Opt<TransformOpsResult>;
        if (copyOp.sheet === sheetOp.sheet) {

            // create a deep clone of the sheet index operation
            const cloneOp = json.deepClone(sheetOp);
            cloneOp.sheet = copyOp.to;

            // special behavior for "changeSheet": do not clone a new sheet name (to prevent name collision)
            if (cloneOp.name === Op.CHANGE_SHEET) {
                delete (cloneOp as Op.ChangeSheetOperation).sheetName;
                checkChangeSheetNoOp(cloneOp as Op.ChangeSheetOperation);
            }

            // cloned table operations need new unused table name
            this.ensure(!this.hasAliasName(cloneOp, OT_SHEET_TABLES_ALIAS), "NOT IMPLEMENTED: cloned table operations need unused table name");

            // transform formula expressions in the cloned operation
            this.#transform_copySheet_formulaExpressions(copyOp, cloneOp);

            // add cloned operation to the result object
            if (!isOperationRemoved(cloneOp)) {
                result = { externalOpsAfter: cloneOp };
            }
        }

        // transform sheet index according to position of the clone
        if (sheetOp.sheet) {
            sheetOp.sheet = transformIndexInsert(sheetOp.sheet, copyOp.to, 1);
        }
        return result;
    }

    #transform_copySheet_drawingPos(copyOp: Op.CopySheetOperation, drawingOp: AnyDrawingOperation): Opt<TransformOpsResult> {

        // local "copySheet": replicate external operation locally in the new sheet
        // external "copySheet": replicate local operation externally in the new sheet (for simplicity,
        // otherwise it would be needed to generate matching reversed operations for each local operation)
        let result: Opt<TransformOpsResult>;
        if (copyOp.sheet === drawingOp.start[0]) {

            // create a deep clone of the drawing operation
            const cloneOp = json.deepClone(drawingOp);
            assignDrawingSheetIndex(cloneOp, copyOp.to);

            // transform formula expressions in the cloned operation
            this.#transform_copySheet_formulaExpressions(copyOp, cloneOp);

            // add cloned operation to the result object
            if (!isOperationRemoved(cloneOp)) {
                result = { externalOpsAfter: cloneOp };
            }
        }

        // transform sheet index according to position of the clone
        transformSheetInDrawingPositions(drawingOp, xfSheet => transformIndexInsert(xfSheet, copyOp.to, 1));
        return result;
    }

    // moveSheets -------------------------------------------------------------

    #transform_moveSheets_moveSheets(lclOp: Op.MoveSheetsOperation, extOp: Op.MoveSheetsOperation): void {
        this.ensure(lclOp.sheets.length === extOp.sheets.length, 'OP ERROR: sort vectors of "moveSheets" operations have different length');
        // transform and reassign sort vectors (either operation may become a no-op)
        const result = transformIndexSortSort(lclOp.sheets, extOp.sheets);
        assignMoveSheets(lclOp, result.lclSortVec);
        assignMoveSheets(extOp, result.extSortVec);
    }

    #transform_moveSheets_sheetIndex(sortOp: Op.MoveSheetsOperation, sheetOp: AnySheetOperation): void {
        if (is.number(sheetOp.sheet)) {
            sheetOp.sheet = transformIndexSort(sheetOp.sheet, sortOp.sheets);
        }
    }

    #transform_moveSheets_drawingPos(sortOp: Op.MoveSheetsOperation, drawingOp: AnyDrawingOperation): void {
        transformSheetInDrawingPositions(drawingOp, xfSheet => transformIndexSort(xfSheet, sortOp.sheets));
    }

    // changeSheet ------------------------------------------------------------

    #transform_changeSheet_changeSheet(lclOp: Op.ChangeSheetOperation, extOp: Op.ChangeSheetOperation): void {

        // operations for different sheets are independent (except if they want to set the same sheet name!)
        if (lclOp.sheet !== extOp.sheet) {

            // DOCS-2142: do not hide the last visible sheets
            if (isHideSheet(lclOp) && isHideSheet(extOp)) {
                this.#ensureValidSheetCountForDelete(lclOp.sheet, extOp.sheet);
            }

            // two sheets cannot be set to the same name
            this.#ensureUniqueSheetNames(lclOp, extOp);
            return;
        }

        // process the formatting attributes
        reduceOperationAttributes(lclOp, extOp, { deleteEqual: true });

        // ignore externally renamed sheet, if it has been renamed locally
        reduceProperties(lclOp, extOp, "sheetName", { deleteEqual: true });

        // ignore the entire operation, if all changes have been discarded
        checkChangeSheetNoOp(lclOp);
        checkChangeSheetNoOp(extOp);
    }

    #transform_changeSheet_formulaExpressions(changeOp: Op.ChangeSheetOperation, fmlaOp: Operation): void {
        if (changeOp.sheetName) {
            const updateTask = new RenameSheetUpdateTask(changeOp.sheet, changeOp.sheetName);
            this.#transform_formulaOp_updateTask(fmlaOp, updateTask);
        }
    }

    #transform_changeSheet_drawingAnchor(changeOp: Op.ChangeSheetOperation, drawingOp: OptAttrsOperation): void {

        // transform absolute position of the drawing object according to new column/row default size
        const drawingAttrs = pick.dict(drawingOp.attrs, "drawing");
        if (drawingAttrs && is.string(drawingAttrs.anchor) && changeOp.attrs) {
            this.warnIf(containsResizeAttr(changeOp.attrs, true), "cannot transform absolute drawing anchor (changed column size)");
            this.warnIf(containsResizeAttr(changeOp.attrs, false), "cannot transform absolute drawing anchor (changed row size)");
        }

        // transform formula expression linking the shape to a cell
        this.#transform_changeSheet_formulaExpressions(changeOp, drawingOp);
    }

    // addressTansform --------------------------------------------------------

    #transform_addressTransform_formulaExpressions(transformer: OTAddressTransformer, fmlaOp: Operation): void {
        if (!isOperationRemoved(fmlaOp)) {
            const updateTask = new MoveCellsUpdateTask(transformer.sheetIndex, transformer);
            this.#transform_formulaOp_updateTask(fmlaOp, updateTask);
        }
    }

    // moveIntervals ----------------------------------------------------------

    #transform_insertIntervals_insertIntervals(columns: boolean, lclOp: Op.IntervalListOperation, extOp: Op.IntervalListOperation): void {

        // operations for different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // read the interval lists to be transformed from the operations
        const lclIntervals = this.#parseIntervalList(lclOp.intervals, columns);
        const extIntervals = this.#parseIntervalList(extOp.intervals, columns);

        // transform external operation first
        const dir = columns ? RIGHT : DOWN;
        const lclTransformer = AddressTransformer.fromIntervals(this.#addressFactory, lclIntervals, dir);
        const newExtIntervals = lclTransformer.transformIntervals(extIntervals, { transformMode: FIXED });
        assignIntervalList(extOp, newExtIntervals, columns);

        // transform local operation with the transformed external operation
        if (!newExtIntervals.empty()) {
            const extTransformer = AddressTransformer.fromIntervals(this.#addressFactory, newExtIntervals, dir);
            const newLclIntervals = extTransformer.transformIntervals(lclIntervals, { transformMode: FIXED });
            assignIntervalList(lclOp, newLclIntervals, columns);
        }
    }

    #transform_insertIntervals_deleteIntervals(columns: boolean, insertOp: Op.IntervalListOperation, deleteOp: Op.IntervalListOperation): void {

        // operations for different sheets are independent
        if (insertOp.sheet !== deleteOp.sheet) { return; }

        // read the interval lists to be transformed from the operations
        const insertIntervals = this.#parseIntervalList(insertOp.intervals, columns);
        const deleteIntervals = this.#parseIntervalList(deleteOp.intervals, columns);

        // transform the insert operation according to deleted intervals
        const deleteTransformer = AddressTransformer.fromIntervals(this.#addressFactory, deleteIntervals, columns ? LEFT : UP);
        const newInsertIntervals = deleteTransformer.transformIntervals(insertIntervals, { transformMode: FIXED }); // never empty
        assignIntervalList(insertOp, newInsertIntervals, columns);

        // transform the delete operation according to inserted intervals
        const insertTransformer = AddressTransformer.fromIntervals(this.#addressFactory, insertIntervals, columns ? RIGHT : DOWN);
        const newDeleteIntervals = insertTransformer.transformIntervals(deleteIntervals, { transformMode: SPLIT });
        assignIntervalList(deleteOp, newDeleteIntervals, columns);
    }

    #transform_deleteIntervals_deleteIntervals(columns: boolean, lclOp: Op.IntervalListOperation, extOp: Op.IntervalListOperation): void {

        // operations for different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // read the interval lists to be transformed from the operations
        const lclIntervals = this.#parseIntervalList(lclOp.intervals, columns);
        const extIntervals = this.#parseIntervalList(extOp.intervals, columns);

        // transform local operation according to externally deleted intervals
        const dir = columns ? LEFT : UP;
        const extTransformer = AddressTransformer.fromIntervals(this.#addressFactory, extIntervals, dir);
        assignIntervalList(lclOp, extTransformer.transformIntervals(lclIntervals).merge(), columns);

        // transform external operation according to locally deleted intervals
        const lclTransformer = AddressTransformer.fromIntervals(this.#addressFactory, lclIntervals, dir);
        assignIntervalList(extOp, lclTransformer.transformIntervals(extIntervals).merge(), columns);
    }

    #transform_moveIntervals_changeIntervals(dir: Direction, intervalsOp: Op.IntervalListOperation, changeOp: Op.ChangeIntervalsOperation): void {

        // operations for different sheets are independent
        if (intervalsOp.sheet !== changeOp.sheet) { return; }

        // create the address transformer
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        const { columns } = transformer;

        // read the interval list to be transformed from the operation
        const intervals = this.#parseIntervalList(changeOp.intervals, columns);

        // transform the index intervals (ignore the operation, if the intervals
        // will be deleted or shifted outside the sheet)
        const newIntervals = transformer.transformIntervals(intervals, { transformMode: SPLIT });
        assignIntervalList(changeOp, newIntervals, columns);
    }

    #transform_moveIntervals_changeCells(dir: Direction, intervalsOp: Op.IntervalListOperation, changeOp: Op.ChangeCellsOperation): void {
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        this.#transform_changeCells_addressTransform(changeOp, transformer);
    }

    #transform_moveIntervals_mergeCells(dir: Direction, intervalsOp: Op.IntervalListOperation, mergeOp: Op.MergeCellsOperation): void {

        // operations for different sheets are independent
        if (intervalsOp.sheet !== mergeOp.sheet) { return; }

        // read the range list to be transformed from the operation
        const ranges = this.parseRangeList(mergeOp.ranges);

        // transform the cell range addresses (split when inserting between single stripes)
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        const stripes = mergeOp.type === (transformer.columns ? MergeMode.VERTICAL : MergeMode.HORIZONTAL);
        const newRanges = transformer.transformRanges(ranges, { transformMode: stripes ? SPLIT : RESIZE });

        // delete ranges that will not be merged (according to merge mode)
        const predicate = fun.do(() => {
            switch (mergeOp.type) {
                case MergeMode.HORIZONTAL: return (range: Range) => range.singleCol();
                case MergeMode.VERTICAL:   return (range: Range) => range.singleRow();
                case MergeMode.UNMERGE:    return undefined; // always allow to unmerge on single cells
                default:                   return (range: Range) => range.single();
            }
        });
        if (predicate) {
            ary.deleteAllMatching(newRanges, predicate);
        }

        // ignore the operation, if the ranges will be deleted or shifted outside the sheet
        assignRangeList(mergeOp, newRanges);
    }

    #transform_moveIntervals_definedName(dir: Direction, intervalsOp: Op.IntervalListOperation, nameOp: Op.NameOperation): void {

        // transform formula expressions in all operations (regardless of sheet index)
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        this.#transform_addressTransform_formulaExpressions(transformer, nameOp);
    }

    #transform_moveIntervals_tableRange(dir: Direction, intervalsOp: Op.IntervalListOperation, tableOp: AnyTableOperation): Opt<TransformOpsResult> {

        // operations for different sheets are independent, ignore missing table range
        if ((intervalsOp.sheet !== tableOp.sheet) || !is.string(tableOp.range)) { return undefined; }

        // transform the table range
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        const tableRange = this.#parseRange(tableOp.range);
        const newRange = transformer.transformRanges(tableRange, { transformMode: STRICT }).first();

        // create "deleteTable" operation for implicit deletion of the table
        if (!newRange) {
            setOperationRemoved(tableOp);
            return { localOpsBefore: Op.deleteTable(tableOp.sheet, tableOp.table) };
        }

        // do not allow to expand/shrink columns (TODO: needs update of `headers` property)
        if (!isVerticalDir(dir) && (tableRange.cols() !== newRange.cols())) {
            this.error("NOT IMPLEMENTED: cannot insert/delete columns in table range");
        }

        // deleting header row or footer row of the table range is not supported
        if (dir === UP) {
            this.ensure(!transformer.transformRanges(tableRange.headerRow()).empty(), "NOT IMPLEMENTED: cannot delete table header row");
            this.ensure(!transformer.transformRanges(tableRange.footerRow()).empty(), "NOT IMPLEMENTED: cannot delete table footer row");
        }

        // update the table range in the table operation
        tableOp.range = newRange.toOpStr();
        return undefined;
    }

    #transform_moveIntervals_changeTableCol(dir: Direction, intervalsOp: Op.IntervalListOperation, tableOp: Op.ChangeTableColumnOperation): void {

        // operations for different sheets are independent, ignore missing table range
        if (intervalsOp.sheet !== tableOp.sheet) { return; }

        // table column index (relative to table range) cannot be transformed with column operations
        this.ensure(isVerticalDir(dir), "NOT IMPLEMENTED: cannot transform table column index when inserting/deleting columns");
    }

    #transform_moveIntervals_rangeList(dir: Direction, intervalsOp: Op.IntervalListOperation, rangesOp: AnyRangeListOperation): Opt<TransformOpsResult> {

        // transform formula expressions in all operations (regardless of sheet index)
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        this.#transform_addressTransform_formulaExpressions(transformer, rangesOp);

        // operations for different sheets are independent
        if (!rangesOp.ranges || (intervalsOp.sheet !== rangesOp.sheet)) { return undefined; }

        // resolve transformation configuration
        const config = SHEET_RANGES_CONFIG[rangesOp.name];
        this.ensure(config, `INTERNAL ERROR: missing configuration for ranges transformation of operation ${rangesOp.name}`);

        // read the range list to be transformed from the operation
        const ranges = this.parseRangeList(rangesOp.ranges);

        // transform the cell range addresses
        const newRanges = transformer.transformRanges(ranges, config);

        // set the operation to "removed", if the ranges will be deleted or shifted outside the sheet
        assignRangeList(rangesOp, newRanges);
        if (!newRanges.empty()) { return undefined; }

        // generate a "delete" operation, if the ranges will be deleted or shifted outside the sheet
        const deleteOp = this.#generateDeleteOperation(rangesOp);
        return deleteOp ? { localOpsBefore: deleteOp } : undefined;
    }

    #transform_moveIntervals_sourceLinks(dir: Direction, intervalsOp: Op.IntervalListOperation, drawingOp: AnyDrawingOperation): void {

        // transform formula expressions in all operations (regardless of sheet index)
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        this.#transform_addressTransform_formulaExpressions(transformer, drawingOp);
    }

    #transform_moveIntervals_cellAnchor(dir: Direction, intervalsOp: Op.IntervalListOperation, anchorOp: Op.CellAnchorOperation): Opt<TransformOpsResult> {

        // operations for different sheets are independent
        if (intervalsOp.sheet !== anchorOp.sheet) { return undefined; }

        // parse the addresses from the operations
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        const anchor = this.#parseAddress(anchorOp.anchor);

        // transform the anchor address of the comment (ignore the comment operation,
        // if the comment will be deleted or shifted outside the sheet)
        const newAnchor = transformer.transformAddress(anchor);
        if (newAnchor) {
            anchorOp.anchor = newAnchor.toOpStr();
            // transform the anchor drawing attribute (position of the text frame); never delete the text frame
            this.#transformDrawingAnchor(anchorOp, transformer, { move: true });
            return undefined;
        }

        // generate a "delete" operation, if the anchor cell will be deleted or shifted outside the sheet
        setOperationRemoved(anchorOp);
        const deleteOp = this.#generateDeleteOperation(anchorOp);
        return deleteOp ? { localOpsBefore: deleteOp } : undefined;
    }

    #transform_moveIntervals_moveAnchors(dir: Direction, intervalsOp: Op.IntervalListOperation, moveOp: Op.MoveAnchorsOperation, genDeleteFn: GenerateDeleteAnchorFn): Opt<TransformOpsResult> {

        // operations for different sheets are independent
        if (intervalsOp.sheet !== moveOp.sheet) { return undefined; }

        // parse source and target addresses
        const { fromAnchors, toAnchors } = this.#parseMoveAnchors(moveOp);

        // transformed anchor addresses
        const newFromAnchors = new AddressArray();
        const newToAnchors = new AddressArray();

        // additional operations for notes/comments to be deleted
        const lclDelOps: Operation[] = [];
        const extDelOps: Operation[] = [];

        // transform the anchor addresses
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        fromAnchors.forEach((fromAnchor, index) => {

            // transform the anchor addresses
            const toAnchor = toAnchors[index];
            const newFromAnchor = transformer.transformAddress(fromAnchor);
            const newToAnchor = transformer.transformAddress(toAnchor);

            // push addresses, if the note/comment will not be deleted
            if (newFromAnchor && newToAnchor) {
                newFromAnchors.push(newFromAnchor);
                newToAnchors.push(newToAnchor);
                return;
            }

            // if note/comment will be deleted at source or target address, create a local "delete" operation
            // with original target address to be applied remotely before the "moveIntervals" operation
            lclDelOps.push(genDeleteFn(moveOp.sheet, toAnchor));

            // if note/comment will be deleted at target address only, create an external "delete" operation
            // with tarnsformed source address to be applied locally before the "move" operation
            if (newFromAnchor) {
                extDelOps.push(genDeleteFn(moveOp.sheet, newFromAnchor));
            }
        });

        // delete the "move" operation, if all notes/comments have been deleted
        assignMoveAnchors(moveOp, newFromAnchors, newToAnchors);

        // return the additional delete operations for deleted notes/comments
        return { localOpsBefore: lclDelOps, externalOpsBefore: extDelOps };
    }

    #transform_moveIntervals_moveNotes(dir: Direction, intervalsOp: Op.IntervalListOperation, moveOp: Op.MoveNotesOperation): Opt<TransformOpsResult> {
        return this.#transform_moveIntervals_moveAnchors(dir, intervalsOp, moveOp, Op.deleteNote);
    }

    #transform_moveIntervals_moveComments(dir: Direction, intervalsOp: Op.IntervalListOperation, moveOp: Op.MoveCommentsOperation): Opt<TransformOpsResult> {
        return this.#transform_moveIntervals_moveAnchors(dir, intervalsOp, moveOp, Op.deleteComment);
    }

    #transform_moveIntervals_drawingAnchor(dir: Direction, intervalsOp: Op.IntervalListOperation, drawingOp: AnyDrawingOperation): Opt<TransformOpsResult> {

        // transform formula expressions in all operations (regardless of sheet index)
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        this.#transform_addressTransform_formulaExpressions(transformer, drawingOp);

        // operations for different sheets are independent
        if (intervalsOp.sheet !== drawingOp.start[0]) { return undefined; }

        // transform the anchor drawing attribute (position of the drawing frame)
        const deleted = this.#transformDrawingAnchor(drawingOp, transformer);
        if (!deleted) { return undefined; }

        // generate a "deleteDrawing" operation, if the drawing objects will be deleted
        setOperationRemoved(drawingOp);
        return { localOpsBefore: Op.deleteDrawing(drawingOp.start) };
    }

    #transform_moveIntervals_sheetSelection(dir: Direction, intervalsOp: Op.IntervalListOperation, selectOp: Op.SheetSelectionOperation): void {

        // operations for different sheets are independent
        if (intervalsOp.sheet !== selectOp.sheet) { return; }

        // parse the operations
        const transformer = this.createIntervalTransformer(intervalsOp, dir);
        const oldSel = this.#parseSheetSelection(selectOp);

        // transform the selection, update the operation
        const newSel = transformer.transformSheetSelection(oldSel);
        selectOp.ranges = newSel.ranges.toOpStr();
        selectOp.index = newSel.active;
        selectOp.active = newSel.address.toOpStr();
        if (newSel.origin) { selectOp.origin = newSel.origin.toOpStr(); } else { delete selectOp.origin; }
    }

    // changeIntervals --------------------------------------------------------

    #transform_changeIntervals_changeIntervals(columns: boolean, lclOp: Op.ChangeIntervalsOperation, extOp: Op.ChangeIntervalsOperation): Opt<TransformOpsResult> {

        // operations for different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return undefined; }

        // read the column/row intervals from both operations
        let lclIntervals = this.#parseIntervalList(lclOp.intervals, columns);
        let extIntervals = this.#parseIntervalList(extOp.intervals, columns);

        // nothing to do if the intervals do not overlap
        const overlapIntervals = lclIntervals.intersect(extIntervals);
        if (overlapIntervals.empty()) { return undefined; }

        // create the reduced local and external operations for the overlapping parts
        const lclOverlapOp = json.deepClone(lclOp);
        const extOverlapOp = json.deepClone(extOp);
        const reducedStyle = reduceProperties(lclOverlapOp, extOverlapOp, "s", { deleteEqual: true });
        const reducedAttrs = reduceOperationAttributes(lclOverlapOp, extOverlapOp, { deleteEqual: true });

        // if the operations do not influence each other, just use them as they are
        if (!reducedStyle && !reducedAttrs) { return undefined; }

        // set the "removed" flag in the new overlap operations, if all changes have been reduced
        const lclRemoved = checkChangeIntervalsNoOp(lclOverlapOp);
        const extRemoved = checkChangeIntervalsNoOp(extOverlapOp);

        // reduce the intervals to the distinct parts
        lclIntervals = lclIntervals.difference(overlapIntervals);
        extIntervals = extIntervals.difference(overlapIntervals);

        // process the local operation
        if (lclRemoved && lclIntervals.empty()) {
            // remove the local operation, if it has been covered by the external operation, and does not add any change
            setOperationRemoved(lclOp);
        } else if (lclRemoved && !lclIntervals.empty()) {
            // local operation does not change external formatting: reduce to uncovered intervals
            assignIntervalList(lclOp, lclIntervals, columns);
        } else if (!lclRemoved && lclIntervals.empty()) {
            // local operation covered by external operation: use reduced attributes only
            copyProperties(lclOp, lclOverlapOp, "attrs", "s");
        }

        // process the external operation
        let result: Opt<TransformOpsResult>;
        if (extRemoved && extIntervals.empty()) {
            // remove the external operation, if it has been covered by the local operation, and does not add any change
            setOperationRemoved(extOp);
        } else if (extRemoved && !extIntervals.empty()) {
            // external operation does not change local formatting: reduce to uncovered intervals
            assignIntervalList(extOp, extIntervals, columns);
        } else if (!extRemoved && extIntervals.empty()) {
            // external operation covered by local operation: use reduced attributes only
            copyProperties(extOp, extOverlapOp, "attrs", "s");
        } else {
            // split external operation: all attributes in uncovered intervals, reduced attributes in covered intervals
            assignIntervalList(extOp, extIntervals, columns);
            assignIntervalList(extOverlapOp, overlapIntervals, columns);
            result = { externalOpsAfter: extOverlapOp };
        }

        return result;
    }

    #transform_changeIntervals_drawingAnchor(columns: boolean, intervalsOp: Op.ChangeIntervalsOperation, drawingOp: AnyDrawingOperation): void {

        const drawingAttrs = pick.dict(drawingOp.attrs, "drawing");
        if (drawingAttrs && is.string(drawingAttrs.anchor) && intervalsOp.attrs) {
            this.warnIf(containsResizeAttr(intervalsOp.attrs, columns), "cannot transform absolute drawing anchor (changed column/row size)");
        }
    }

    // changeCells ------------------------------------------------------------

    #transform_changeCells_addressTransform(changeOp: Op.ChangeCellsOperation, transformer: OTAddressTransformer): void {

        // transform formula expressions in all operations (regardless of sheet index)
        this.#transform_addressTransform_formulaExpressions(transformer, changeOp);

        // operations for different sheets are independent
        if (transformer.sheetIndex !== changeOp.sheet) { return; }

        // transform the range addresses (keys of the contents dictionary)
        // TODO: handle shared formulas
        const contents = dict.create<Op.CellDataOrValue>();
        dict.forEach(changeOp.contents, (data, key) => {

            // immediately fail to touch shared formulas
            if (isCellData(data)) {
                this.ensure(is.nullish(data.si) && is.nullish(data.sr), "NOT IMPLEMENTED: shared formulas cannot be modified");
            }

            // transform the range address: insert gaps when inserting columns/rows inbetween
            const oldRange = this.#parseRange(key);
            const newRanges = transformer.transformRanges(oldRange, { transformMode: SPLIT });
            if (newRanges.empty()) { return; }

            // create a new entry for all transformed ranges
            newRanges.forEach(newRange => (contents[newRange.toOpStr()] = data));

            // transform matrix range (its size must not change, but complete deletion is allowed)
            if (isCellData(data) && data.mr) {
                const oldMatRange = this.#parseRange(data.mr);
                const newMatRanges = transformer.transformRanges(oldMatRange);
                this.ensure((newMatRanges.length === 1) && oldMatRange.equalSize(newMatRanges[0]), "NOT IMPLEMENTED: matrix formula modified partially");
                data.mr = newMatRanges[0].toOpStr();
            }
        });

        // insert the new contents dictionary into the operation, delete operation on demand
        changeOp.contents = contents;
        checkChangeCellsNoOp(changeOp);
    }

    #transform_changeCells_changeCells(lclOp: Op.ChangeCellsOperation, extOp: Op.ChangeCellsOperation): void {

        // operations for different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // helper interface to store parsed range addresses and cell data objects in arrays
        interface RangeAndCellData {
            range: Range;
            data: Op.CellData;
        }

        class ContentsContainer {
            readonly cellContents = new Array<RangeAndCellData>();
            readonly matrixRanges = new RangeArray();
        }

        // put contents and ranges into arrays to prevent re-parsing range addresses
        const [lclContainer, extContainer] = [lclOp, extOp].map(changeOp => {
            const container = new ContentsContainer();
            const { cellContents, matrixRanges } = container;
            dict.forEach(changeOp.contents, (data, key) => {
                const range = this.#parseRange(key);
                if (isCellData(data)) {
                    cellContents.push({ range, data });
                    if ("mr" in data) {
                        matrixRanges.push(data.mr ? this.#parseRange(data.mr) : range);
                    }
                } else {
                    cellContents.push({ range, data: { v: data } });
                }
            });
            return container;
        });

        // matrix formulas must not collide with any other formulas (without transformation for simplicity; this should rarely happen)
        [[lclContainer, extContainer], [extContainer, lclContainer]].forEach(([container1, container2]) => {
            const { matrixRanges } = container2;
            if (matrixRanges.empty()) { return; }
            container1.cellContents.forEach(({ data, range }) => {
                if (("f" in data) || ("si" in data) || ("sr" in data) || ("mr" in data)) {
                    this.ensure(!matrixRanges.overlaps(range), "NOT IMPLEMENTED: matrix formula overlaps with other formula");
                }
            });
        });

        // shortcuts to container properties
        const lclContentsArr = lclContainer.cellContents;
        const extContentsArr = extContainer.cellContents;

        // clear the contents dictionaries in the operations (will be re-filled in the loops below)
        const lclContents = lclOp.contents = dict.create();
        const extContents = extOp.contents = dict.create();

        // inserts an entry into a contents dictionary
        const insertCellData = (contents: Dict<Op.CellDataOrValue>, range: Range, data: Op.CellData): void => {
            if (!is.empty(data)) {
                contents[range.toOpStr()] = (("v" in data) && dict.singleKey(data)) ? data.v! : data;
            }
        };

        // process all cell data entries in the local operation (pop from front, push new to end)
        while (lclContentsArr.length) {

            // decompose the array element into range address and cell data
            const lclEntry = lclContentsArr.shift()!;
            const { range: lclRange, data: lclData } = lclEntry;

            // find an overlapping entry in the external operation
            let isectRange: Range | null = null;
            const extIdx = extContentsArr.findIndex(extEntry => !!(isectRange = lclRange.intersect(extEntry.range)));

            // no overlapping range: use local data unmodified, proceed with next local cell data
            if (!isectRange) {
                insertCellData(lclContents, lclRange, lclData);
                continue;
            }

            // remove found entry from array of external cell data entries;
            // decompose the array element into range address and cell data
            const extEntry = ary.deleteAt(extContentsArr, extIdx)!;
            const { range: extRange, data: extData } = extEntry;

            // immediately fail to transform different shared formulas (for simplicity; this should rarely happen)
            if (("sr" in lclData) && (("f" in extData) || ("si" in extData) || ("sr" in extData))) {
                this.error("NOT IMPLEMENTED: shared formula overlaps with other formula");
            }
            if (("sr" in extData) && (("f" in lclData) || ("si" in lclData))) {
                this.error("NOT IMPLEMENTED: shared formula overlaps with other formula");
            }

            // create new cell data objects to be filled with the effective cell properties
            const lclData2 = dict.createPartial<Op.CellData>();
            const extData2 = dict.createPartial<Op.CellData>();

            // copy the delete property if it exists on one side only
            if (lclData.u && !extData.u) { lclData2.u = true; }
            if (!lclData.u && extData.u) { extData2.u = true; }

            // helper function to process a single property in all cell data objects
            const processProperty = <KT extends keyof Op.CellData>(...propKeys: KT[]): void => {

                // whether the property exists in the original cell data objects
                const lclKey = propKeys.find(propKey => propKey in lclData);
                const extKey = propKeys.find(propKey => propKey in extData);

                // send local property to server (write into `newLclData`), if it exists, and:
                // - it differs from the external property (or no external property is set), or
                // - the local operation deletes the cell (property "u")
                if (lclKey && (lclData.u || (lclKey !== extKey) || (lclData[lclKey] !== extData[lclKey]))) {
                    lclData2[lclKey] = lclData[lclKey];
                }

                // re-apply local property locally (write into `newExtData`), if external operation
                // will delete the cell, but local operation has not deleted the cell
                if (lclKey && !lclData.u && extData.u) {
                    extData2[lclKey] = lclData[lclKey];
                }

                // apply external property locally (write into `newExtData`), if it exists, and if the
                // local operation neither defines the property by itself nor deletes the cell entirely
                if (extKey && !lclKey && !lclData.u) {
                    extData2[extKey] = extData[extKey];
                }

                // force to delete the cell externally, if an external property has been set but
                // locally the property was not modified
                if (lclData.u && !lclKey && extKey) {
                    lclData2.u = true;
                }
            };

            // process all properties of the cell data objects ("sr" and "mr" will not collide, see above)
            processProperty("v", "e");
            processProperty("f");
            processProperty("si");
            processProperty("s");

            // insert the filled cell data into the contents dictionaries
            insertCellData(lclContents, isectRange, lclData2);
            insertCellData(extContents, isectRange, extData2);

            // append remaining uncovered parts of the ranges to the contents arrays
            // (either of them may overlap with other unprocessed cell ranges)
            if (lclRange.differs(extRange)) {
                for (const range of new RangeArray(lclRange).difference(extRange)) {
                    lclContentsArr.push({ range, data: lclData });
                }
                for (const range of new RangeArray(extRange).difference(lclRange)) {
                    extContentsArr.push({ range, data: extData });
                }
            }
        }

        // remaining entries in `extContentsArr` are not covered by local contents
        extContentsArr.forEach(extEntry => insertCellData(extContents, extEntry.range, extEntry.data));

        // mark empty operations as "removed"
        checkChangeCellsNoOp(lclOp);
        checkChangeCellsNoOp(extOp);
    }

    #transform_changeCells_moveCells(changeOp: Op.ChangeCellsOperation, moveOp: Op.MoveCellsOperation): void {
        const transformer = this.#createRangeTransformer(moveOp);
        this.#transform_changeCells_addressTransform(changeOp, transformer);
    }

    #transform_changeCells_mergeCells(changeOp: Op.ChangeCellsOperation, mergeOp: Op.MergeCellsOperation): void {

        // operations for different sheets are independent, unmerging is always allowed
        if ((changeOp.sheet !== mergeOp.sheet) || (mergeOp.type === MergeMode.UNMERGE)) { return; }

        // compare merged ranges against matrix ranges
        const mergedRanges = this.parseRangeList(mergeOp.ranges);
        dict.forEach(changeOp.contents, data => {
            if (isCellData(data) && data.mr) {
                const matRange = this.#parseRange(data.mr);
                this.ensure(!mergedRanges.overlaps(matRange), "NOT IMPLEMENTED: merged range overlaps with matrix formula");
            }
        });
    }

    #transform_changeCells_changeName(cellsOp: Op.ChangeCellsOperation, nameOp: Op.ChangeNameOperation): void {
        this.#transform_changeName_formulaExpressions(nameOp, cellsOp);
    }

    #transform_changeCells_tableRange(cellsOp: Op.ChangeCellsOperation, tableOp: Op.ChangeTableOperation): void {

        // update new table name in cell formulas
        this.#transform_changeTable_formulaExpressions(tableOp, cellsOp);

        // operations for different sheets are independent, ignore missing table range
        if ((cellsOp.sheet !== tableOp.sheet) || !tableOp.range) { return; }

        // parse the table range
        const tableRange = this.#parseRange(tableOp.range);

        // check that the table header will not be changed, and matrix formulas do not overlap with the table
        dict.forEach(cellsOp.contents, (data, key) => {

            // fail to change value or formula of the table header cells
            if (!isCellData(data) || ("v" in data) || ("f" in data) || ("si" in data) || ("sr" in data)) {
                const range = this.#parseRange(key);
                this.ensure(!range.overlaps(tableRange.headerRow()), "NOT IMPLEMENTED: header row of table range cannot be changed");
            }

            // fail for overlapping matrix formulas
            if (isCellData(data) && data.mr) {
                const matrixRange = this.#parseRange(data.mr);
                this.ensure(!matrixRange.overlaps(tableRange), "NOT IMPLEMENTED: matrix range cannot overlap with table range");
            }
        });
    }

    // moveCells --------------------------------------------------------------

    #transform_moveCells_formulaExpressions(moveOp: Op.MoveCellsOperation, fmlaOp: Operation): void {
        const transformer = this.#createRangeTransformer(moveOp);
        this.#transform_addressTransform_formulaExpressions(transformer, fmlaOp);
    }

    // mergeCells -------------------------------------------------------------

    #transform_mergeCells_mergeCells(lclOp: Op.MergeCellsOperation, extOp: Op.MergeCellsOperation): void {

        // operations for different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // parse local and external merged ranges
        const lclRanges = this.parseRangeList(lclOp.ranges);
        const extRanges = this.parseRangeList(extOp.ranges);

        // reduce all external ranges that overlap with local ranges (local ranges win over external ranges)
        const newExtRanges = reduceMergedRanges(lclRanges, extRanges, extOp.type);

        // assign transformed range lists to operations, set to "removed" state on demand
        //assignRangeList(lclOp, newLclRanges);
        assignRangeList(extOp, newExtRanges);
    }

    #transform_mergeCells_tableRange(mergeOp: Op.MergeCellsOperation, tableOp: Op.ChangeTableOperation): Opt<TransformOpsResult> {

        // operations for different sheets are independent, ignore unmerge, ignore missing table range
        if ((mergeOp.sheet !== tableOp.sheet) || (mergeOp.type === MergeMode.UNMERGE) || !tableOp.range) { return undefined; }

        // merged ranges must not overlap with table range
        const mergedRanges = this.parseRangeList(mergeOp.ranges);
        const tableRange = this.#parseRange(tableOp.range);

        // reduce merged ranges that overlap with table range
        const newMergedRanges = reduceMergedRanges(new RangeArray(tableRange), mergedRanges, mergeOp.type);
        assignRangeList(mergeOp, newMergedRanges);

        // add an unmerge operation if the merged ranges have been reduced
        const changed = !mergedRanges.deepEquals(newMergedRanges);
        return changed ? { externalOpsBefore: Op.mergeCells(tableOp.sheet, tableOp.range, MergeMode.UNMERGE) } : undefined;
    }

    // hyperlinks -------------------------------------------------------------

    #transform_changeHyperlink_changeHyperlink(lclOp: Op.RangeListOperation, extOp: Op.RangeListOperation): void {

        // operations for different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // parse local and external hyperlink ranges
        const lclRanges = this.parseRangeList(lclOp.ranges);
        const extRanges = this.parseRangeList(extOp.ranges);

        // reduce extenal ranges so that they do not overwrite local hyperlinks
        assignRangeList(extOp, extRanges.difference(lclRanges));
    }

    // insertName -------------------------------------------------------------

    #transform_insertName_insertName(lclOp: Op.InsertNameOperation, extOp: Op.InsertNameOperation): void {
        // cannot insert an existing name again
        this.ensure(!isSameName(lclOp, extOp), "NOT IMPLEMENTED: cannot insert existing name again");
    }

    #transform_insertName_deleteName(insertOp: Op.InsertNameOperation, deleteOp: Op.DeleteNameOperation): void {
        if (isSameName(insertOp, deleteOp)) { setOperationRemoved(insertOp); }
    }

    #transform_insertName_changeName(insertOp: Op.InsertNameOperation, changeOp: Op.ChangeNameOperation): void {

        // cannot insert an existing name again
        this.ensure(!isSameName(insertOp, changeOp), "OP ERROR: label of inserted name already used");

        // cannot insert name and change the label of another name to the same label
        if (changeOp.newLabel && (insertOp.sheet === changeOp.sheet)) {
            this.ensure(!str.equalsICC(insertOp.label, changeOp.newLabel), "OP ERROR: label of inserted name already used");
        }

        // update new label in all formula expressions
        this.#transform_changeName_formulaExpressions(changeOp, insertOp);
    }

    #transform_insertName_insertTable(nameOp: Op.InsertNameOperation, tableOp: Op.InsertTableOperation): void {
        if (tableOp.table) {
            this.ensure(!str.equalsICC(nameOp.label, tableOp.table), "NOT IMPLEMENTED: defined name and table cannot have same label");
        }
    }

    #transform_insertName_changeTable(nameOp: Op.InsertNameOperation, tableOp: Op.ChangeTableOperation): void {

        // cannot insert a name for an existing table
        if (tableOp.table) {
            this.ensure(!str.equalsICC(nameOp.label, tableOp.table), "OP ERROR: label of inserted name already used");
        }

        // cannot insert a name and change a table name to the same label
        if (tableOp.newName) {
            this.ensure(!str.equalsICC(nameOp.label, tableOp.newName), "NOT IMPLEMENTED: defined name and table cannot have same label");
        }

        // update new table name in all formula expressions of the defined name
        this.#transform_changeTable_formulaExpressions(tableOp, nameOp);
    }

    // deleteName -------------------------------------------------------------

    #transform_deleteName_deleteName(lclOp: Op.DeleteNameOperation, extOp: Op.DeleteNameOperation): void {
        if (isSameName(lclOp, extOp)) { setOperationRemoved(lclOp, extOp); }
    }

    #transform_deleteName_changeName(deleteOp: Op.DeleteNameOperation, changeOp: Op.ChangeNameOperation): void {

        // different names are independent
        if (!isSameName(deleteOp, changeOp)) { return; }

        // delete operation wins over change operation
        setOperationRemoved(changeOp);

        // update label in delete operation
        if (changeOp.newLabel) { deleteOp.label = changeOp.newLabel; }
    }

    // changeName -------------------------------------------------------------

    #transform_changeName_formulaExpressions(nameOp: Op.ChangeNameOperation, fmlaOp: Operation): void {
        if (nameOp.newLabel) {
            const updateTask = new RelabelNameUpdateTask(nameOp.sheet ?? null, nameOp.label, nameOp.newLabel);
            this.#transform_formulaOp_updateTask(fmlaOp, updateTask);
        }
    }

    #transform_changeName_changeName(lclOp: Op.ChangeNameOperation, extOp: Op.ChangeNameOperation): void {

        if (isSameName(lclOp, extOp)) {

            // update new label in operations (before reducing the property)
            if (lclOp.newLabel) { extOp.label = lclOp.newLabel; }
            if (extOp.newLabel) { lclOp.label = extOp.newLabel; }

            // transform the operation properties
            reduceProperties(lclOp, extOp, ["newLabel", "formula"], { deleteEqual: true });

            // set no-ops to "removed" state
            checkChangeNameNoOp(lclOp);
            checkChangeNameNoOp(extOp);
        }

        // cannot change different names in same parent to the same label
        if (lclOp.newLabel && extOp.newLabel && (lclOp.sheet === extOp.sheet)) {
            this.ensure(!str.equalsICC(lclOp.newLabel, extOp.newLabel), "NOT IMPLEMENTED: cannot set same label to different names");
        }

        // update new label in formula expressions
        this.#transform_changeName_formulaExpressions(lclOp, extOp);
        this.#transform_changeName_formulaExpressions(extOp, lclOp);
    }

    #transform_changeName_insertTable(nameOp: Op.ChangeNameOperation, tableOp: Op.InsertTableOperation): void {

        // cannot insert a table for an existing name
        if (tableOp.table) {
            this.ensure(!str.equalsICC(nameOp.label, tableOp.table), "OP ERROR: name of inserted table already used");
        }

        // cannot insert a table and change a name to the same label
        if (nameOp.newLabel && tableOp.table) {
            this.ensure(!str.equalsICC(nameOp.newLabel, tableOp.table), "NOT IMPLEMENTED: table and defined name cannot have same label");
        }
    }

    #transform_changeName_changeTable(nameOp: Op.ChangeNameOperation, tableOp: Op.ChangeTableOperation): void {

        // cannot change name label to existing table
        if (nameOp.newLabel && tableOp.table) {
            this.ensure(!str.equalsICC(nameOp.newLabel, tableOp.table), "OP ERROR: name label already used by table");
        }

        // cannot change table name to existing defined name
        if (tableOp.newName) {
            this.ensure(!str.equalsICC(nameOp.label, tableOp.newName), "OP ERROR: table name already used by defined name");
        }

        // cannot change both labels to the same string
        if (nameOp.newLabel && tableOp.newName) {
            this.ensure(!str.equalsICC(nameOp.newLabel, tableOp.newName), "NOT IMPLEMENTED: table and defined name cannot have same label");
        }

        // update new table name in all formula expressions of the defined name
        this.#transform_changeTable_formulaExpressions(tableOp, nameOp);
    }

    // insertTable ------------------------------------------------------------

    #transform_insertTable_insertTable(lclOp: Op.InsertTableOperation, extOp: Op.InsertTableOperation): void {

        // tables must not have same name (regardless of sheet)
        this.ensure(!isSameTable(lclOp, extOp), "NOT IMPLEMENTED: cannot insert table with same name twice");

        // tables in the same sheet must not overlap
        if (lclOp.sheet === extOp.sheet) {
            const lclRange = this.#parseRange(lclOp.range);
            const extRange = this.#parseRange(extOp.range);
            this.ensure(!lclRange.overlaps(extRange), "NOT IMPLEMENTED: table ranges cannot overlap");
        }
    }

    #transform_insertTable_deleteTable(insertOp: Op.InsertTableOperation, deleteOp: Op.DeleteTableOperation): void {
        // cannot insert an existing table again (regardless of sheet)
        this.ensure(!isSameTable(insertOp, deleteOp), "OP ERROR: cannot insert existing table");
    }

    #transform_insertTable_changeTable(insertOp: Op.InsertTableOperation, changeOp: Op.ChangeTableOperation): void {

        // cannot insert an existing table again (regardless of sheet)
        this.ensure(!isSameTable(insertOp, changeOp), "OP ERROR: cannot insert existing table");

        // cannot insert table and change another table to the same name
        if (insertOp.table && changeOp.newName) {
            this.ensure(!str.equalsICC(insertOp.table, changeOp.newName), "NOT IMPLEMENTED: label of inserted name already used");
        }
    }

    #transform_insertTable_changeTableCol(insertOp: Op.InsertTableOperation, changeOp: Op.ChangeTableColumnOperation): void {
        // cannot insert an existing table again (regardless of sheet)
        this.ensure(!isSameTable(insertOp, changeOp), "OP ERROR: cannot insert existing table");
    }

    // deleteTable ------------------------------------------------------------

    #transform_deleteTable_deleteTable(lclOp: Op.DeleteTableOperation, extOp: Op.DeleteTableOperation): void {

        // different tables are independent
        if (!isSameTable(lclOp, extOp)) { return; }

        // operations must target the same sheet
        this.ensure(lclOp.sheet === extOp.sheet, "OP ERROR: same table name in different sheets");

        // both operations become no-ops
        setOperationRemoved(lclOp, extOp);
    }

    #transform_deleteTable_changeTable(deleteOp: Op.DeleteTableOperation, changeOp: Op.ChangeTableOperation): void {

        // different tables are independent
        if (!isSameTable(deleteOp, changeOp)) { return; }

        // operations must target the same sheet
        this.ensure(deleteOp.sheet === changeOp.sheet, "OP ERROR: same table name in different sheets");

        // delete operation wins over change operation
        setOperationRemoved(changeOp);

        // update table name in delete operation
        if (changeOp.newName) { deleteOp.table = changeOp.newName; }
    }

    #transform_deleteTable_changeTableCol(deleteOp: Op.DeleteTableOperation, changeOp: Op.ChangeTableColumnOperation): void {

        // different tables are independent
        if (!isSameTable(deleteOp, changeOp)) { return; }

        // operations must target the same sheet
        this.ensure(deleteOp.sheet === changeOp.sheet, "OP ERROR: same table name in different sheets");

        // delete operation wins over change operation
        setOperationRemoved(changeOp);
    }

    // changeTable ------------------------------------------------------------

    #transform_changeTable_formulaExpressions(tableOp: Op.ChangeTableOperation, fmlaOp: Operation): void {
        if (tableOp.table && tableOp.newName) {
            const updateTask = new RenameTableUpdateTask(tableOp.table, tableOp.newName);
            this.#transform_formulaOp_updateTask(fmlaOp, updateTask);
        }
    }

    #transform_changeTable_changeTable(lclOp: Op.ChangeTableOperation, extOp: Op.ChangeTableOperation): void {

        if (isSameTable(lclOp, extOp)) {

            // operations must target the same sheet
            this.ensure(lclOp.sheet === extOp.sheet, "OP ERROR: same table name in different sheets");

            // update table name in operations (before reducing the property)
            if (lclOp.newName) { extOp.table = lclOp.newName; }
            if (extOp.newName) { lclOp.table = extOp.newName; }

            // transform the formatting attributes and other properties
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true });
            reduceProperties(lclOp, extOp, ["newName", "range", "headers"], { deleteEqual: true });

            // set no-ops to "removed" state
            checkChangeTableNoOp(lclOp);
            checkChangeTableNoOp(extOp);
        }

        // cannot change different tables to the same name
        if (lclOp.newName && extOp.newName) {
            this.ensure(!str.equalsICC(lclOp.newName, extOp.newName), "NOT IMPLEMENTED: cannot set same name to different tables");
        }
    }

    #transform_changeTable_changeTableCol(changeOp: Op.ChangeTableOperation, colOp: Op.ChangeTableColumnOperation): void {

        if (isSameTable(changeOp, colOp)) {

            // operations must target the same sheet
            this.ensure(changeOp.sheet === colOp.sheet, "OP ERROR: same table name in different sheets");

            // cannot change table range (TODO: needs transformation or validity checking of table column index)
            this.ensure(!changeOp.range, "NOT IMPLEMENTED: table range cannot be changed when changing columns");

            // update table name in column operation
            if (changeOp.newName) { colOp.table = changeOp.newName; }

        } else if (changeOp.newName && colOp.table) {
            // cannot change table name to a used name
            this.ensure(!str.equalsICC(changeOp.newName, colOp.table), "OP ERROR: table name already used");
        }
    }

    // changeTableCol ---------------------------------------------------------

    #transform_changeTableCol_changeTableCol(lclOp: Op.ChangeTableColumnOperation, extOp: Op.ChangeTableColumnOperation): void {

        if (isSameTable(lclOp, extOp) && (lclOp.col === extOp.col)) {

            // operations must target the same sheet
            this.ensure(lclOp.sheet === extOp.sheet, "OP ERROR: same table name in different sheets");

            // do not interfere with multi-column sort operation
            this.ensure(!lclOp.sort && !extOp.sort, "NOT IMPLEMENTED: multi-column sort operation in table");

            // transform the formatting attributes and other properties
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true });
        }
    }

    // insertDVRule -----------------------------------------------------------

    #transform_insertDVRule_insertDVRule(lclOp: Op.InsertDVRuleOperation, extOp: Op.InsertDVRuleOperation): void {

        // rules in different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // transform the rule indexes (OOXML only)
        if (this.#indexedDVRules) {

            // rule ranges must not overlap
            this.#ensureUniqueTargetRanges(lclOp, extOp);

            // transform the "index" properties of the operations
            const { lclIdx, extIdx } = this.#parseDVRuleIndexes(lclOp, extOp);
            const result = transformIndexInsertInsert(lclIdx, extIdx);
            lclOp.index = result.lclIdx;
            extOp.index = result.extIdx;

        } else {

            // TODO: process partially overlapping rules (ODF)
            const { overlaps } = this.#parseDVRuleRanges(lclOp, extOp);
            this.ensure(!overlaps, "NOT IMPLEMENTED: partially overlapping DV rules");
        }
    }

    #transform_insertDVRule_deleteDVRule(insertOp: Op.InsertDVRuleOperation, deleteOp: Op.DeleteDVRuleOperation): void {

        // rules in different sheets are independent
        if (insertOp.sheet !== deleteOp.sheet) { return; }

        // transform the rule indexes (OOXML only)
        if (this.#indexedDVRules) {

            // transform the "index" properties of the operations
            const { lclIdx, extIdx } = this.#parseDVRuleIndexes(insertOp, deleteOp);
            const result = transformIndexInsertDelete(lclIdx, extIdx);
            insertOp.index = result.lclIdx;
            deleteOp.index = result.extIdx;

        } else {

            // TODO: process partially overlapping rules (ODF)
            const { overlaps } = this.#parseDVRuleRanges(insertOp, deleteOp);
            this.ensure(!overlaps, "NOT IMPLEMENTED: partially overlapping DV rules");
        }
    }

    #transform_insertDVRule_changeDVRule(insertOp: Op.InsertDVRuleOperation, changeOp: Op.ChangeDVRuleOperation): void {

        // rules in different sheets are independent
        if (insertOp.sheet !== changeOp.sheet) { return; }

        // transform the rule index (OOXML only)
        if (this.#indexedDVRules) {

            // rule ranges must not overlap
            this.#ensureUniqueTargetRanges(insertOp, changeOp);

            // transform index of the change operation
            const { lclIdx: insertIdx, extIdx: changeIdx } = this.#parseDVRuleIndexes(insertOp, changeOp);
            changeOp.index = transformIndexInsert(changeIdx, insertIdx, 1);

        } else {

            // TODO: process partially overlapping rules (ODF)
            const { overlaps } = this.#parseDVRuleRanges(insertOp, changeOp);
            this.ensure(!overlaps, "NOT IMPLEMENTED: partially overlapping DV rules");
        }
    }

    // deleteDVRule -----------------------------------------------------------

    #transform_deleteDVRule_deleteDVRule(lclOp: Op.DeleteDVRuleOperation, extOp: Op.DeleteDVRuleOperation): void {

        // rules in different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // transform the rule indexes (OOXML only)
        if (this.#indexedDVRules) {

            // set both operations to "removed" state, if they delete the same rule
            const { lclIdx, extIdx } = this.#parseDVRuleIndexes(lclOp, extOp);
            const result = transformIndexDeleteDelete(lclIdx, extIdx);
            if (result) {
                lclOp.index = result.lclIdx;
                extOp.index = result.extIdx;
            } else {
                setOperationRemoved(lclOp, extOp);
            }

        } else {

            // TODO: process partially overlapping rules
            const { equals } = this.#parseDVRuleRanges(lclOp, extOp);
            if (equals) { setOperationRemoved(lclOp, extOp); }
        }
    }

    #transform_deleteDVRule_changeDVRule(deleteOp: Op.DeleteDVRuleOperation, changeOp: Op.ChangeDVRuleOperation): void {

        // rules in different sheets are independent
        if (deleteOp.sheet !== changeOp.sheet) { return; }

        // transform the index of the change operation (OOXML only)
        if (this.#indexedDVRules) {

            const { lclIdx: deleteIdx, extIdx: changeIdx } = this.#parseDVRuleIndexes(deleteOp, changeOp);
            const index = transformIndexDelete(changeIdx, deleteIdx, 1);
            if (is.number(index)) { changeOp.index = index; } else { setOperationRemoved(changeOp);  }

        } else {

            const { equals, overlaps } = this.#parseDVRuleRanges(deleteOp, changeOp);
            if (equals) {
                setOperationRemoved(changeOp);
            } else if (overlaps) {
                // TODO: process partially overlapping rules
                this.error("NOT IMPLEMENTED: partially overlapping DV rules");
            }
        }
    }

    // changeDVRule -----------------------------------------------------------

    #transform_changeDVRule_changeDVRule(lclOp: Op.ChangeDVRuleOperation, extOp: Op.ChangeDVRuleOperation): void {

        // rules in different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // check whether the same rule is modified by both operations
        if (this.#indexedDVRules) {
            const { lclIdx, extIdx } = this.#parseDVRuleIndexes(lclOp, extOp);
            // ranges of different rules must not overlap
            if (lclIdx !== extIdx) { this.#ensureUniqueTargetRanges(lclOp, extOp); return; }
        } else {
            const { equals, overlaps } = this.#parseDVRuleRanges(lclOp, extOp);
            if (!overlaps) { return; }
            // TODO: process partially overlapping rules
            this.ensure(equals, "NOT IMPLEMENTED: partially overlapping DV rules");
        }

        // reduce operation properties
        reduceProperties(lclOp, extOp, DVRULE_PROPS, { deleteEqual: true });

        // TODO: change rule properties to single opaque "rule" property, handle "ref" property
        const hasLclVals = DVRULE_VAL_PROPS.some(key => key in lclOp);
        const hasExtVals = DVRULE_VAL_PROPS.some(key => key in extOp);
        this.ensure(!hasLclVals && !hasExtVals, "NOT IMPLEMENTED: cannot partially change type/value settings of data validation rule");

        // set no-ops to "removed" state
        checkChangeDVRuleNoOp(lclOp);
        checkChangeDVRuleNoOp(extOp);
    }

    // insertCFRule -----------------------------------------------------------

    #transform_insertCFRule_insertCFRule(lclOp: Op.InsertCFRuleOperation, extOp: Op.InsertCFRuleOperation): void {

        // rules in different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // indexed mode: transform rule indexes
        if (this.#indexedCFRules) {

            // transform the "id" properties of the operations
            const { lclIdx, extIdx } = this.#parseCFRuleIndexes(lclOp, extOp);
            const result = transformIndexInsertInsert(lclIdx, extIdx);
            lclOp.id = String(result.lclIdx);
            extOp.id = String(result.extIdx);

        } else if (lclOp.id === extOp.id) {

            // same rule identifier: convert operations to "changeCFRule" and reduce properties
            const lclChangeOp = changeOperationType<Op.ChangeCFRuleOperation>(lclOp, Op.CHANGE_CFRULE);
            const extChangeOp = changeOperationType<Op.ChangeCFRuleOperation>(extOp, Op.CHANGE_CFRULE);
            this.#transform_changeCFRule_changeCFRule(lclChangeOp, extChangeOp);
        }
    }

    #transform_insertCFRule_deleteCFRule(insertOp: Op.InsertCFRuleOperation, deleteOp: Op.DeleteCFRuleOperation): void {

        // rules in different sheets are independent
        if (insertOp.sheet !== deleteOp.sheet) { return; }

        // indexed mode: transform rule indexes
        if (this.#indexedCFRules) {

            // transform the "id" properties of the operations
            const { lclIdx, extIdx } = this.#parseCFRuleIndexes(insertOp, deleteOp);
            const result = transformIndexInsertDelete(lclIdx, extIdx);
            insertOp.id = String(result.lclIdx);
            deleteOp.id = String(result.extIdx);

        } else {

            // should not happen in real-life: preceding insert collision was handled in "insertCFRule" self transformation
            this.ensure(insertOp.id !== deleteOp.id, "OP ERROR: cannot insert an existing CF rule");
        }
    }

    #transform_insertCFRule_changeCFRule(insertOp: Op.InsertCFRuleOperation, changeOp: Op.ChangeCFRuleOperation): void {

        // rules in different sheets are independent
        if (insertOp.sheet !== changeOp.sheet) { return; }

        // indexed mode: transform rule indexes
        if (this.#indexedCFRules) {

            // transform "id" property of the change operation
            const { lclIdx: insertIdx, extIdx: changeIdx } = this.#parseCFRuleIndexes(insertOp, changeOp);
            changeOp.id = String(transformIndexInsert(changeIdx, insertIdx, 1));

        } else {

            // should not happen in real-life: preceding insert collision was handled in "insertCFRule" self transformation
            this.ensure(insertOp.id !== changeOp.id, "OP ERROR: cannot insert an existing CF rule");
        }
    }

    // deleteCFRule -----------------------------------------------------------

    #transform_deleteCFRule_deleteCFRule(lclOp: Op.DeleteCFRuleOperation, extOp: Op.DeleteCFRuleOperation): void {

        // rules in different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // indexed mode: transform rule indexes
        if (this.#indexedCFRules) {

            // transform the "id" properties of the operations
            const { lclIdx, extIdx } = this.#parseCFRuleIndexes(lclOp, extOp);
            const result = transformIndexDeleteDelete(lclIdx, extIdx);
            if (result) {
                lclOp.id = String(result.lclIdx);
                extOp.id = String(result.extIdx);
            } else {
                // set both operations to "removed" state, if they delete the same rule
                setOperationRemoved(lclOp, extOp);
            }
        } else if (lclOp.id === extOp.id) {

            // set both operations to "removed" state, if they delete the same rule
            setOperationRemoved(lclOp, extOp);
        }
    }

    #transform_deleteCFRule_changeCFRule(deleteOp: Op.DeleteCFRuleOperation, changeOp: Op.ChangeCFRuleOperation): void {

        // rules in different sheets are independent
        if (deleteOp.sheet !== changeOp.sheet) { return; }

        // indexed mode: transform rule indexes
        if (this.#indexedCFRules) {

            // transform the "id" property of the change operation
            const { lclIdx: deleteIdx, extIdx: changeIdx } = this.#parseCFRuleIndexes(deleteOp, changeOp);
            const index = transformIndexDelete(changeIdx, deleteIdx, 1);
            if (is.number(index)) { changeOp.id = String(index); } else { setOperationRemoved(changeOp);  }

        } else if (deleteOp.id === changeOp.id) {
            setOperationRemoved(changeOp);
        }
    }

    // changeCFRule -----------------------------------------------------------

    #transform_changeCFRule_changeCFRule(lclOp: Op.ChangeCFRuleOperation, extOp: Op.ChangeCFRuleOperation): void {

        // different rules are independent
        if ((lclOp.sheet !== extOp.sheet) || (lclOp.id !== extOp.id)) { return; }

        // reduce operation properties
        reduceProperties(lclOp, extOp, CFRULE_PROPS, { deleteEqual: true });

        // TODO: change rule properties to single opaque "rule" property, handle "ref" property
        const hasLclVals = CFRULE_VAL_PROPS.some(key => key in lclOp);
        const hasExtVals = CFRULE_VAL_PROPS.some(key => key in extOp);
        this.ensure(!hasLclVals && !hasExtVals, "NOT IMPLEMENTED: cannot partially change type/value settings of conditional formatting rule");

        // set no-ops to "removed" state
        checkChangeCFRuleNoOp(lclOp);
        checkChangeCFRuleNoOp(extOp);
    }

    // insertNote -------------------------------------------------------------

    #transform_insertNote_insertNote(lclOp: Op.InsertNoteOperation, extOp: Op.InsertNoteOperation): void {

        // different notes are independent
        if (!isSameAnchor(lclOp, extOp)) { return; }

        // two notes in same cell: convert operations to "changeNote" and reduce properties
        const lclChangeOp = changeOperationType<Op.ChangeNoteOperation>(lclOp, Op.CHANGE_NOTE);
        const extChangeOp = changeOperationType<Op.ChangeNoteOperation>(extOp, Op.CHANGE_NOTE);
        this.#transform_changeNote_changeNote(lclChangeOp, extChangeOp);
    }

    #transform_insertNote_deleteNote(insertOp: Op.InsertNoteOperation, deleteOp: Op.DeleteNoteOperation): void {
        // should not happen in real-life: preceding insert collision was handled in "insertNote" self transformation
        this.ensure(!isSameAnchor(insertOp, deleteOp), "OP ERROR: cannot insert a note twice into the same cell");
    }

    #transform_insertNote_changeNote(insertOp: Op.InsertNoteOperation, changeOp: Op.ChangeNoteOperation): void {
        // should not happen in real-life: preceding insert collision was handled in "insertNote" self transformation
        this.ensure(!isSameAnchor(insertOp, changeOp), "OP ERROR: cannot insert a note twice into the same cell");
    }

    #transform_insertNote_moveNotes(insertOp: Op.InsertNoteOperation, moveOp: Op.MoveNotesOperation): Opt<TransformOpsResult> {

        // notes in different sheets are independent
        if (insertOp.sheet !== moveOp.sheet) { return undefined; }

        // parse cell anchors of all note operations
        const anchor = this.#parseAddress(insertOp.anchor);
        const { fromAnchors, toAnchors } = this.#parseMoveAnchors(moveOp);

        // should not happen in real-life: preceding insert collision was handled in "insertNote" self transformation
        this.ensure(!fromAnchors.contains(anchor), "OP ERROR: cannot insert a note twice into the same cell");

        // note moved into the cell of the new note: delete external note before sending locally inserted note, ignore external move locally
        const arrIdx = toAnchors.indexOf(anchor);
        if (arrIdx >= 0) {
            fromAnchors.deleteAt(arrIdx);
            toAnchors.deleteAt(arrIdx);
            assignMoveAnchors(moveOp, fromAnchors, toAnchors);
            return { localOpsBefore: Op.deleteNote(insertOp.sheet, insertOp.anchor) };
        }

        return undefined;
    }

    #transform_insertNote_insertComment(noteOp: Op.InsertNoteOperation, commentOp: Op.InsertCommentOperation): Opt<TransformOpsResult> {

        // different cells are independent
        if (!isSameAnchor(noteOp, commentOp)) { return undefined; }

        // note and comment in same cell: delete note before sending inserted comment, ignore inserted note
        setOperationRemoved(noteOp);
        return { externalOpsBefore: Op.deleteNote(noteOp.sheet, noteOp.anchor) };
    }

    #transform_insertNote_moveComments(insertOp: Op.InsertNoteOperation, moveOp: Op.MoveCommentsOperation): Opt<TransformOpsResult> {

        // different sheets are independent
        if (insertOp.sheet !== moveOp.sheet) { return undefined; }

        // parse cell anchors of all note operations
        const anchor = this.#parseAddress(insertOp.anchor);
        const { fromAnchors, toAnchors } = this.#parseMoveAnchors(moveOp);

        // should not happen in real-life: preceding insert collision was handled in "insertComment" transformation
        this.ensure(!fromAnchors.contains(anchor), "OP ERROR: cannot insert a note into a comment cell");

        // comment moved into the cell of the new note: delete note before moving comments
        const arrIdx = toAnchors.indexOf(anchor);
        if (arrIdx >= 0) {
            setOperationRemoved(insertOp);
            return { externalOpsBefore: Op.deleteNote(insertOp.sheet, insertOp.anchor) };
        }

        return undefined;
    }

    // deleteNote -------------------------------------------------------------

    #transform_deleteNote_deleteNote(lclOp: Op.DeleteNoteOperation, extOp: Op.DeleteNoteOperation): void {
        if (isSameAnchor(lclOp, extOp)) { setOperationRemoved(lclOp, extOp); }
    }

    #transform_deleteNote_changeNote(deleteOp: Op.DeleteNoteOperation, changeOp: Op.ChangeNoteOperation): void {
        if (isSameAnchor(deleteOp, changeOp)) { setOperationRemoved(changeOp); }
    }

    #transform_deleteNote_moveNotes(deleteOp: Op.CellAnchorOperation, moveOp: Op.MoveAnchorsOperation): void {

        // notes in different sheets are independent
        if (deleteOp.sheet !== moveOp.sheet) { return; }

        // parse cell anchors of all note operations
        const anchor = this.#parseAddress(deleteOp.anchor);
        const { fromAnchors, toAnchors } = this.#parseMoveAnchors(moveOp);

        // move anchor in delete operation if deleted note was moved; and
        // remove the deleted note from the move operation
        const arrIdx = fromAnchors.indexOf(anchor);
        if (arrIdx >= 0) {
            deleteOp.anchor = anchor.assign(toAnchors[arrIdx]).toOpStr();
            fromAnchors.deleteAt(arrIdx);
            toAnchors.deleteAt(arrIdx);
            assignMoveAnchors(moveOp, fromAnchors, toAnchors);
        }

        // fail when trying to move a note over another note (with transformed `anchor`)
        this.ensure(!toAnchors.contains(anchor), "OP ERROR: cannot move a note to another note");
    }

    // changeNote -------------------------------------------------------------

    #transform_changeNote_changeNote(lclOp: Op.ChangeNoteOperation, extOp: Op.ChangeNoteOperation): void {

        // different notes are independent
        if (!isSameAnchor(lclOp, extOp)) { return; }

        // transform the formatting attributes and note text
        reduceOperationAttributes(lclOp, extOp, { deleteEqual: true });
        reduceProperties(lclOp, extOp, "text", { deleteEqual: true });

        // ignore the entire operation, if all changes have been discarded
        checkChangeNoteNoOp(lclOp);
        checkChangeNoteNoOp(extOp);
    }

    #transform_changeNote_moveNotes(changeOp: Op.CellAnchorOperation, moveOp: Op.MoveAnchorsOperation): void {

        // notes/comments in different sheets are independent
        if (changeOp.sheet !== moveOp.sheet) { return; }

        // parse cell anchors of all operations
        const anchor = this.#parseAddress(changeOp.anchor);
        const { fromAnchors, toAnchors } = this.#parseMoveAnchors(moveOp);

        // move changed anchor if necessary
        const arrIdx = fromAnchors.indexOf(anchor);
        if (arrIdx >= 0) {
            changeOp.anchor = anchor.assign(toAnchors[arrIdx]).toOpStr();
        }
    }

    // moveNotes --------------------------------------------------------------

    #transform_moveNotes_moveNotes(lclOp: Op.MoveAnchorsOperation, extOp: Op.MoveAnchorsOperation): void {

        // notes/comments in different sheets are independent
        if (lclOp.sheet !== extOp.sheet) { return; }

        // parse cell anchors of all operations
        const { fromAnchors: lclFromAnchors, toAnchors: lclToAnchors } = this.#parseMoveAnchors(lclOp);
        const { fromAnchors: extFromAnchors, toAnchors: extToAnchors } = this.#parseMoveAnchors(extOp);

        // process all local note moves
        lclFromAnchors.forEachReverse((lclFrom, lclIdx) => {
            const lclTo = lclToAnchors[lclIdx];

            // handle moving same note in both oerations (ignore both if it is the same target)
            let extIdx = extFromAnchors.indexOf(lclFrom);
            if (extIdx >= 0) {
                const extTo = extToAnchors[extIdx];
                if (lclTo.equals(extTo)) {
                    lclFromAnchors.deleteAt(lclIdx);
                    lclToAnchors.deleteAt(lclIdx);
                } else {
                    lclFromAnchors[lclIdx] = extTo;
                }
                extFromAnchors.deleteAt(extIdx);
                extToAnchors.deleteAt(extIdx);
                return;
            }

            // handle moving two notes to the same target address (local move wins, needs to overrule external move)
            extIdx = extToAnchors.indexOf(lclTo);
            if (extIdx >= 0) {
                lclFromAnchors.insertAt(lclIdx + 1, extToAnchors[extIdx]);
                lclToAnchors.insertAt(lclIdx + 1, extFromAnchors[extIdx]);
                extFromAnchors.deleteAt(extIdx);
                extToAnchors.deleteAt(extIdx);
            }
        });

        // write back all transformed anchor addresses
        assignMoveAnchors(lclOp, lclFromAnchors, lclToAnchors);
        assignMoveAnchors(extOp, extFromAnchors, extToAnchors);
    }

    #transform_moveNotes_insertComment(moveOp: Op.MoveNotesOperation, insertOp: Op.InsertCommentOperation): Opt<TransformOpsResult> {

        // different sheets are independent
        if (moveOp.sheet !== insertOp.sheet) { return undefined; }

        // parse cell anchors of all note operations
        const anchor = this.#parseAddress(insertOp.anchor);
        const { fromAnchors, toAnchors } = this.#parseMoveAnchors(moveOp);

        // should not happen in real-life: preceding insert collision was handled in "insertNote" transformation
        this.ensure(!fromAnchors.contains(anchor), "OP ERROR: cannot insert a comment into a note cell");

        // note moved into the cell of the new comment: delete note before creating the comment
        const arrIdx = toAnchors.indexOf(anchor);
        if (arrIdx >= 0) {
            fromAnchors.deleteAt(arrIdx);
            toAnchors.deleteAt(arrIdx);
            assignMoveAnchors(moveOp, fromAnchors, toAnchors);
            return { externalOpsBefore: Op.deleteNote(insertOp.sheet, insertOp.anchor) };
        }

        return undefined;
    }

    // insertComment ----------------------------------------------------------

    #transform_insertComment_insertComment(lclOp: Op.InsertCommentOperation, extOp: Op.InsertCommentOperation): void {

        // different comments are independent
        if (!isSameAnchor(lclOp, extOp)) { return; }

        // transform the "index" properties of the operations
        const result = transformIndexInsertInsert(lclOp.index, extOp.index);
        lclOp.index = result.lclIdx;
        extOp.index = result.extIdx;
    }

    #transform_insertComment_deleteComment(insertOp: Op.InsertCommentOperation, deleteOp: Op.DeleteCommentOperation): void {

        // different comments are independent
        if (!isSameAnchor(insertOp, deleteOp)) { return; }

        // ignore insert operations, if the entire thread will be deleted
        if (deleteOp.index === 0) { setOperationRemoved(insertOp); return; }

        // transform the "index" properties of the operations
        const result = transformIndexInsertDelete(insertOp.index, deleteOp.index);
        insertOp.index = result.lclIdx;
        deleteOp.index = result.extIdx;
    }

    #transform_insertComment_changeComment(insertOp: Op.InsertCommentOperation, changeOp: Op.ChangeCommentOperation): void {

        // different comments are independent
        if (!isSameAnchor(insertOp, changeOp)) { return; }

        // transform change operation with insert index
        changeOp.index = transformIndexInsert(changeOp.index, insertOp.index, 1);
    }

    // deleteComment ----------------------------------------------------------

    #transform_deleteComment_deleteComment(lclOp: Op.DeleteCommentOperation, extOp: Op.DeleteCommentOperation): void {

        // different comments are independent
        if (!isSameAnchor(lclOp, extOp)) { return; }

        // set both operations to "removed" state, if they delete the same comment
        const result = transformIndexDeleteDelete(lclOp.index, extOp.index);
        if (!result) { setOperationRemoved(lclOp, extOp); return; }

        // if one operation deletes the entire thread, set the other operation to "removed" state
        if (lclOp.index === 0) {
            setOperationRemoved(extOp);
        } else if (extOp.index === 0) {
            setOperationRemoved(lclOp);
        } else {
            lclOp.index = result.lclIdx;
            extOp.index = result.extIdx;
        }
    }

    #transform_deleteComment_changeComment(deleteOp: Op.DeleteCommentOperation, changeOp: Op.ChangeCommentOperation): void {

        // different comments are independent
        if (!isSameAnchor(deleteOp, changeOp)) { return; }

        // set change operation to "removed" state, if the entire thread will be deleted
        if (deleteOp.index === 0) { setOperationRemoved(changeOp); return; }

        // transform the index of the change operation
        const index = transformIndexDelete(changeOp.index, deleteOp.index, 1);
        if (is.number(index)) { changeOp.index = index; } else { setOperationRemoved(changeOp);  }
    }

    #transform_deleteComment_moveComments(deleteOp: Op.DeleteCommentOperation, moveOp: Op.MoveCommentsOperation): void {

        // deleting an entire thread works like delete+move notes (delete the move operation);
        // deleting a single reply works like change+move notes (retain the move operation)
        if (deleteOp.index === 0) {
            this.#transform_deleteNote_moveNotes(deleteOp, moveOp);
        } else {
            this.#transform_changeNote_moveNotes(deleteOp, moveOp);
        }
    }

    // changeComment ----------------------------------------------------------

    #transform_changeComment_changeComment(lclOp: Op.ChangeCommentOperation, extOp: Op.ChangeCommentOperation): void {

        // different comments or replies are independent
        if (!isSameAnchor(lclOp, extOp) || (lclOp.index !== extOp.index)) { return; }

        // transform the formatting attributes and note text
        reduceOperationAttributes(lclOp, extOp, { deleteEqual: true });
        reduceProperties(lclOp, extOp, ["text", "mentions", "done"], { deleteEqual: true });

        // ignore the entire operation, if all changes have been discarded
        checkChangeCommentNoOp(lclOp);
        checkChangeCommentNoOp(extOp);
    }

    // insertDrawing ----------------------------------------------------------

    #transform_insertDrawing_insertDrawing(lclOp: Op.InsertDrawingOperation, extOp: Op.InsertDrawingOperation): void {
        // first, shift local position away (external operation wins)
        transformInsertDrawingPositions(lclOp, extOp.start);
        transformInsertDrawingPositions(extOp, lclOp.start);
    }

    #transform_insertDrawing_deleteDrawing(insertOp: Op.InsertDrawingOperation, deleteOp: Op.DeleteDrawingOperation): void {
        // first, shift away delete position according to insert position (new drawing cannot collide with existing drawing)
        transformInsertDrawingPositions(deleteOp, insertOp.start);
        // transform insert operation with the new deletion index
        transformDeleteDrawingPositions(insertOp, deleteOp.start);
    }

    #transform_insertDrawing_moveDrawing(insertOp: Op.InsertDrawingOperation, moveOp: Op.MoveDrawingOperation): void {

        // check validity of "moveDrawing" operation, and extract position parts
        const moveParts = this.#parseMoveDrawing(moveOp);
        // array index of the position of the inserted shape
        const arrIdx = insertOp.start.length - 1;

        // parent shape inserted: transform indexes of move operation
        if (arrIdx < moveParts.arrIdx) {
            transformInsertDrawingPositions(moveOp, insertOp.start);
            return;
        }

        // embedded shape inserted: transform indexes of delete operation
        if (arrIdx > moveParts.arrIdx) {
            transformDrawingPositionsForMove(insertOp, moveParts);
            return;
        }

        // shapes on same level: transform indexes if shapes are inside the same parent component
        if (equalPositions(insertOp.start, moveParts.toPos, arrIdx)) {
            const result = transformIndexInsertMove(insertOp.start[arrIdx], moveParts.fromIdx, moveParts.toIdx);
            insertOp.start[arrIdx] = result.shiftIdx;
            assignMoveDrawing(moveOp, arrIdx, result.moveRes);
        }
    }

    #transform_insertDrawing_drawingPos(insertOp: Op.InsertDrawingOperation, drawingOp: AnyDrawingOperation): void {
        transformInsertDrawingPositions(drawingOp, insertOp.start);
    }

    #transform_insertDrawing_sheetSelection(insertOp: Op.InsertDrawingOperation, selectOp: Op.SheetSelectionOperation): void {
        if (selectOp.drawings && (insertOp.start[0] === selectOp.sheet)) {
            const insPos = getSheetPos(insertOp.start);
            selectOp.drawings.forEach(xfPos => transformPositionInsert(xfPos, insPos, 1));
        }
    }

    // deleteDrawing ----------------------------------------------------------

    #transform_deleteDrawing_deleteDrawing(lclOp: Op.DeleteDrawingOperation, extOp: Op.DeleteDrawingOperation): void {
        const origExtPos = clonePosition(extOp.start);
        // transform both positions with the original (untransformed) positions
        transformDeleteDrawingPositions(extOp, lclOp.start);
        transformDeleteDrawingPositions(lclOp, origExtPos);
    }

    #transform_deleteDrawing_moveDrawing(deleteOp: Op.DeleteDrawingOperation, moveOp: Op.MoveDrawingOperation): void {

        // check validity of "moveDrawing" operation, and extract position parts
        const moveParts = this.#parseMoveDrawing(moveOp);
        // array index of the position of the deleted shape
        const arrIdx = deleteOp.start.length - 1;

        // parent shape deleted: transform indexes of move operation
        if (arrIdx < moveParts.arrIdx) {
            transformDeleteDrawingPositions(moveOp, deleteOp.start);
            return;
        }

        // embedded shape deleted: transform indexes of delete operation
        if (arrIdx > moveParts.arrIdx) {
            transformDrawingPositionsForMove(deleteOp, moveParts);
            return;
        }

        // shapes on same level: transform indexes if shapes are inside the same parent component
        if (equalPositions(deleteOp.start, moveParts.toPos, arrIdx)) {
            const result = transformIndexDeleteMove(deleteOp.start[arrIdx], moveParts.fromIdx, moveParts.toIdx);
            deleteOp.start[arrIdx] = result.shiftIdx;
            assignMoveDrawing(moveOp, arrIdx, result.moveRes);
        }
    }

    #transform_deleteDrawing_drawingPos(deleteOp: Op.DeleteDrawingOperation, drawingOp: AnyDrawingOperation): void {
        transformDeleteDrawingPositions(drawingOp, deleteOp.start);
    }

    #transform_deleteDrawing_sheetSelection(deleteOp: Op.DeleteDrawingOperation, selectOp: Op.SheetSelectionOperation): void {
        if (selectOp.drawings && (deleteOp.start[0] === selectOp.sheet)) {
            const delPos = getSheetPos(deleteOp.start);
            ary.deleteAllMatching(selectOp.drawings, xfPos => !transformPositionDelete(xfPos, delPos, 1));
            if (selectOp.drawings.length === 0) { delete selectOp.drawings; }
        }
    }

    // changeDrawing ----------------------------------------------------------

    #transform_changeDrawing_changeDrawing(lclOp: Op.ChangeDrawingOperation, extOp: Op.ChangeDrawingOperation): void {
        // reduce both attribute sets, set empty operations to "removed" state
        if (equalPositions(lclOp.start, extOp.start)) {
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true });
        }
    }

    // moveDrawing ------------------------------------------------------------

    #transform_moveDrawing_moveDrawing(lclOp: Op.MoveDrawingOperation, extOp: Op.MoveDrawingOperation): void {

        // check validity of "moveDrawing" operations, and extract position parts
        const lclParts = this.#parseMoveDrawing(lclOp);
        const extParts = this.#parseMoveDrawing(extOp);

        // different position level: transform move operation in embedded shape only
        const { arrIdx } = lclParts;
        if (arrIdx < extParts.arrIdx) {
            transformDrawingPositionsForMove(extOp, lclParts);
        } else if (arrIdx > extParts.arrIdx) {
            transformDrawingPositionsForMove(lclOp, extParts);
        } else if (equalPositions(lclParts.toPos, extParts.toPos, arrIdx)) {
            // transform and reassign all indexes (either move operation may become a no-op)
            const result = transformIndexMoveMove(lclParts.fromIdx, lclParts.toIdx, extParts.fromIdx, extParts.toIdx);
            assignMoveDrawing(lclOp, arrIdx, result.lclRes);
            assignMoveDrawing(extOp, arrIdx, result.extRes);
        }
    }

    #transform_moveDrawing_drawingPos(moveOp: Op.MoveDrawingOperation, drawingOp: AnyDrawingOperation): void {
        // check validity of "moveDrawing" operation, and extract position parts
        const moveParts = this.#parseMoveDrawing(moveOp);
        // transform all position properties of the drawing operation
        transformDrawingPositionsForMove(drawingOp, moveParts);
    }

    #transform_moveDrawing_sheetSelection(moveOp: Op.MoveDrawingOperation, selectOp: Op.SheetSelectionOperation): void {
        // check validity of "moveDrawing" operation, and extract position parts
        const moveParts = this.#parseMoveDrawing(moveOp, true);
        // transform all drawing positions in the selection
        if (selectOp.drawings && (moveParts.sheetIdx === selectOp.sheet)) {
            selectOp.drawings.forEach(xfPos => transformPositionMove(xfPos, moveParts.fromPos, 1, moveParts.toIdx));
        }
    }
}
