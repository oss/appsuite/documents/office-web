/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, fun, itr, ary, map, set, dict, jpromise } from "@/io.ox/office/tk/algorithms";
import { MAX_JSON_KEYS } from "@/io.ox/office/tk/config";
import { EObject } from "@/io.ox/office/tk/objects";

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import type { CellData, CellDataOrValue, CellDataDict } from "@/io.ox/office/spreadsheet/utils/operations";
import { mergeCellData } from "@/io.ox/office/spreadsheet/utils/operations";
import { type ScalarType, isErrorCode, isScalar } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { modelLogger, mapKey, getTableColName } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { GeneratorOptions, SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { StyleChangeSet, SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import type { SheetCacheConfig } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { type AddressTransformer, TransformMode } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { CellType, getModelValue } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { SharedFormulaModel } from "@/io.ox/office/spreadsheet/model/formula/sharedformulamodel";
import { type FormulaUpdateTask, MoveCellsUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

const { ceil, max } = Math;

// types ======================================================================

/**
 * A descriptor with all properties needed to change contents and formatting of
 * cells.
 */
export interface CellChangeSet extends StyleChangeSet {

    /**
     * If set to `true`, an existing cell must be undefined (i.e., physically
     * removed) first, before applying any other property of this object.
     */
    u?: true;

    /**
     * The new value (or formula result) of the cell.
     */
    v?: ScalarType;

    /**
     * The new formula expression for the cell, or `null` to remove an existing
     * formula.
     */
    f?: string | null;

    /**
     * The new index of a shared formula for the cell, or `null` to remove an
     * existing index.
     */
    si?: number | null;

    /**
     * The new bounding range of a shared formula for the cell, or `null` to
     * remove an existing bounding range from the anchor cell.
     */
    sr?: Range | null;

    /**
     * The new bounding range of a matrix formula starting at the cell, or
     * `null` to remove an existing bounding range from the cell.
     */
    mr?: Range | null;

    /**
     * The new dynamic flag of a matrix formula starting at the cell.
     */
    md?: boolean;

    /**
     * If set to a non-empty string, a hyperlink will be created for the cells.
     * If set to an empty string, an existing URL will be removed.
     */
    url?: string;
}

/**
 * A descriptor with all properties needed to change contents and formatting of
 * a specific cell, including its address. Intended to be used in arrays to
 * carry the changes for multiple cells.
 */
export interface AddressChangeSet extends CellChangeSet {

    /**
     * The address of the cell to be changed.
     */
    address: Address;
}

/**
 * A function that returns the identifier of the effective default auto-style
 * (used by the underlying row or column) for the cell with the specified
 * address.
 *
 * @param address
 *  The address of a cell.
 *
 * @returns
 *  The identifier of the effective default row auto-style if available (i.e.
 *  if the row has its attribute "customFormat" set), otherwise the effective
 *  default column auto-style (which is always available).
 */
export type ResolveColRowStyleFn = (address: Address) => string;

// private types --------------------------------------------------------------

/** Changeset with cell address. */
interface AddressEntry {
    address: Address;
    changeSet: CellChangeSet;
}

/** Changeset with cell range address. */
interface RangeEntry {
    range: Range;
    changeSet: CellChangeSet;
}

// constants ==================================================================

/**
 * DOCS-1662: The maximum number of keys allowed in the `contents` dictionary
 * of "changeCells" document operations (restricted by server JSON libraries).
 *
 * Reduce the maximum available value by 10% to prevent that transformations of
 * "changeCells" with itself exceeds the limit when inserting one or a few new
 * entries into the dictionary (e.g. when splitting partially overlapping cell
 * ranges).
 */
const MAX_CONTENTS_SIZE = (MAX_JSON_KEYS === 0) ? Infinity : ceil(MAX_JSON_KEYS * 0.9);

// private functions ==========================================================

function insertChangeSet(entryMap: Map<string, AddressEntry>, address: Address, changeSet: CellChangeSet): void {
    if (!is.empty(changeSet)) {
        entryMap.set(address.key, { address, changeSet });
    }
}

function updateChangeSet(entryMap: Map<string, AddressEntry>, address: Address, changeSet: CellChangeSet, unlessUndef?: boolean): void {
    if (!is.empty(changeSet)) {
        map.update(entryMap, address.key, entry => {
            if (!entry) { return { address, changeSet }; }
            // skip modifying changeset if it undefines the cell
            if (!unlessUndef || !entry.changeSet.u) {
                // do not modify original entries (may be inserted repeatedly for a cell range)
                entry.changeSet = { ...entry.changeSet, ...changeSet };
            }
            return entry;
        });
    }
}

/**
 * Returns whether the passed cell range addresses are equal, or are both
 * falsy.
 *
 * @param range1
 *  The first cell range address.
 *
 * @param range2
 *  The second cell range address.
 *
 * @returns
 *  Whether the passed cell range addresses are equal, or are both falsy.
 */
function equalRanges(range1: Range | null, range2: Range | null): boolean {
    return (!range1 && !range2) || !!(range1 && range2 && range1.equals(range2));
}

/**
 * Returns whether the passed changesets are equal. Ignores all internal
 * properties that do not qualify for the test (e.g. `cacheKey`).
 *
 * @param changeSet1
 *  The first changeset to be tested.
 *
 * @param changeSet2
 *  The second changeset to be tested.
 */
function equalChangeSets(changeSet1: CellChangeSet, changeSet2: CellChangeSet): boolean {
    return dict.equals(changeSet1, changeSet2, ["s", "format", "v", "f", "si", "md", "u"])  // strict comparison of primitive values
        && dict.equals(changeSet1, changeSet2, ["a", "table"], _.isEqual)                   // deep comparison of attribute sets
        && dict.equals(changeSet1, changeSet2, ["sr", "mr"], equalRanges);                  // compare cell range addresses
}

// class CellCache ============================================================

/**
 * A helper class to generate operations and the related undo operations for
 * contents and formatting of cells in a sheet.
 *
 * @param sheetModel
 *  The model of the sheet this instance is associated to.
 *
 * @param styleCache
 *  The parent style cache needed to generate the cell auto-styles.
 *
 * @param [config]
 *  Configuration options.
 */
export class CellCache extends EObject {

    // properties -------------------------------------------------------------

    // the document model
    readonly docModel: SpreadsheetModel;

    // the sheet model
    readonly sheetModel: SheetModel;

    // the style cache needed to generate cell auto-styles
    readonly styleCache: SheetStyleCache;

    // the configuration settings passed to the constructor
    readonly #config: SheetCacheConfig;

    // the address transformer for moving cell addresses
    readonly #transformer?: AddressTransformer;

    // the configuration for the formula grammar used in operations
    readonly #grammar: FormulaGrammar;

    // the cell collection this builder works on
    readonly #collection: CellCollection;

    // the map with all collected changesets for existing cells
    readonly #changeMap = new Map<string, AddressEntry>();

    // whether a move task has been processed (no need to process shared formulas)
    #skipShared = false;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, styleCache: SheetStyleCache, config?: SheetCacheConfig) {
        super();

        this.sheetModel = sheetModel;
        this.docModel = sheetModel.docModel;
        this.styleCache = styleCache;

        this.#config = { ...config };
        this.#transformer = config?.transformer;
        this.#grammar = sheetModel.docModel.formulaGrammarOP;
        this.#collection = sheetModel.cellCollection;
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a cell changeset to be applied to an existing cell in the
     * sheet associated to this instance.
     *
     * @param address
     *  The current (original) address of the cell to be modified with the
     *  passed changeset. If the configuration of this instance contains an
     *  address transformer, the cell operation will be generated with the
     *  transformed address, but the undo operation will always be generated
     *  with this original address.
     *
     * @param changeSet
     *  The changeset to be applied to the specified cell.
     */
    changeCell(address: Address, changeSet: CellChangeSet): void {
        updateChangeSet(this.#changeMap, address, changeSet);
    }

    /**
     * Registers a cell changeset to be applied to multiple existing cells in
     * the sheet associated to this instance.
     *
     * @param ranges
     *  The current (original) addresses of the cell ranges to be modified with
     *  the passed changeset. If the configuration of this instance contains an
     *  address transformer, the cell operation will be generated with the
     *  transformed addresses, but the undo operation will always be generated
     *  with this original address.
     *
     * @param changeSet
     *  The changeset to be applied to the specified cells.
     */
    changeCells(ranges: RangeSource, changeSet: CellChangeSet): void {
        for (const address of RangeArray.cast(ranges).addresses()) {
            this.changeCell(address, changeSet);
        }
    }

    /**
     * Generates the changesets needed to update or restore all cell formulas
     * (including shared and matrix formulas) for the passed update task.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all changesets have been generated.
     */
    @modelLogger.profileMethod("$badge{CellCache} updateFormulas")
    updateFormulas(updateTask: FormulaUpdateTask): JPromise {

        // whether the passed update task moves the cells (in any sheet)
        const isMoveTask = updateTask instanceof MoveCellsUpdateTask;
        // when processing a move task, shared formulas do not need to be processed afterwards
        this.#skipShared = isMoveTask;

        // nothing to do without any cells
        const usedRange = this.#collection.getUsedRange();
        if (!usedRange) { return jpromise.resolve(); }

        // additional handling for moving cells in the sheet (transform cell addresses)
        const transformer = this.#transformer;
        // a helper token array used to generate relocated formula expressions for all shared cells
        const tokenArray = this.sheetModel.createCellTokenArray();

        // iterator providing sorted list of index intervals for existing shared formulas
        const siIntervals = this.#collection.sharedFormulas.intervals();
        // the first interval of shared formula indexes
        let siInterval = itr.shift(siIntervals);
        // next free model index for a new shared formula
        let nextFreeSi = (siInterval && (siInterval[0] === 0)) ? (siInterval[1] + 1) : 0;

        // visit all formula anchor cells (cell formulas, matrix formulas, shared formulas)
        return this.asyncForEach(this.#collection.cellEntries(usedRange, { type: CellType.ANCHOR }), cellEntry => {

            // the current formula cell to be processed
            const cellModel = cellEntry.model!;
            const oldAnchor = cellEntry.address;

            // when moving cells in any sheet, transform the member cells of a shared formula individually (each
            // member cell of the shared formula may result in a different relative formula expression)
            const sharedModel = cellModel.sf;
            if (isMoveTask && sharedModel && cellModel.isSharedAnchor()) {

                // nothing to do, if all shared formulas will be deleted by the transformer
                if (transformer?.transformRanges(sharedModel.boundRange).empty()) { return; }

                // In the following, the different steps will be illustrated with a specific example. A shared
                // formula may be located in the cells B2:B5 (anchor cell is B2), and the formula expression in the
                // anchor cell is =SUM($C$1:$C3) with relative end row reference "3". The formula expressions in
                // each cell will look as following:
                //  B2: =SUM($C$1:$C3)
                //  B3: =SUM($C$1:$C4)
                //  B4: =SUM($C$1:$C5)
                //  B5: =SUM($C$1:$C6)
                //
                // Now, a new row will be inserted between row 4 and row 5. This means that cell B5 will be moved
                // one row down to B6, and that the formula expressions will look as following:
                //  B2: =SUM($C$1:$C3)
                //  B3: =SUM($C$1:$C4)
                //  B4: =SUM($C$1:$C6) (row reference "5" transformed to row reference "6")
                //  B5: <blank>
                //  B6: =SUM($C$1:$C7) (row reference "6" transformed to row reference "7")

                // helper interface for grouping shared formulas
                interface SharedEntryGroup {
                    oldAddresses: Address[];
                    anchorFormula: string;
                    newBoundRange: Range;
                }

                // group the member cells by different transformed anchor formula expressions
                const entryGroups = new Map<string, SharedEntryGroup>();

                // Calculate the resulting transformed formula expression for all cells of the shared formula, and
                // group the cells by their resulting anchor formula expression. In the example above, the
                // transformed formula expressions of the (resulting) cells B2, B3, B4, and B6 will be relocated
                // back to the old anchor address in order to find equivalent formulas, which results in the
                // following transformed anchor formulas:
                //  B2: =SUM($C$1:$C3) (relative to B2)
                //  B3: =SUM($C$1:$C3) (relative to B2)
                //  B4: =SUM($C$1:$C4) (relative to B2)
                //  B6: =SUM($C$1:$C3) (relative to B2, original cell address was B5)
                //
                // Therefore, two different groups of cells will be created: The first group contains the cell
                // addresses B2, B3, and B6; and the second group contains the address B4. From these two groups,
                // two different shared formulas will be created, the first shared formula with anchor address B2,
                // and the second with anchor address B4.

                // process the member cells of the shared formula row-by-row from top to bottom, so that the first
                // element in each array in `entryGroups` becomes the reference cell of a (new) shared formula with
                // the correct formula expression stored in the property `anchorFormula`
                for (const oldAddress of sharedModel.addressSet.ordered()) {

                    // find the new position of the formula cell (ignore formula cells that will be deleted)
                    const newAddress = transformer ? transformer.transformAddress(oldAddress) : oldAddress;
                    if (!newAddress) { continue; }

                    // parse and transform the formula expression for the current cell
                    const oldFormula = sharedModel.getFormulaOp(oldAddress);
                    tokenArray.parseFormula("op", oldFormula, oldAddress);
                    const newFormula = updateTask.transformFormula(tokenArray, this.#grammar) || oldFormula;

                    // create the key for the entry group (a formula expression relative to cell A1 with option
                    // `wrapReferences` in order to prevent #REF! errors for transformed references)
                    tokenArray.parseFormula("op", newFormula, newAddress);
                    const groupKey = tokenArray.getFormula("op", newAddress, Address.A1, { wrapReferences: true });

                    // collect the entries in distinct groups according to the relocated anchor formula expressions
                    map.update(entryGroups, groupKey, entryGroup => {
                        if (!entryGroup) {
                            entryGroup = { oldAddresses: [], anchorFormula: newFormula, newBoundRange: new Range(newAddress) };
                        } else {
                            entryGroup.newBoundRange = entryGroup.newBoundRange.expand(newAddress);
                        }
                        entryGroup.oldAddresses.push(oldAddress);
                        return entryGroup;
                    });
                }

                // Create the changesets for all cells that need to be updated. Each address group will result in a
                // shared formula, except for groups with only one address which will result in normal formula cells.
                // In the example above, the group with the addresses B2, B3, and B5 will result in the shared formula
                // at anchor address B2, and the anchor formula =SUM($C$1:$C3). The second group contains address B4
                // only, this will result in the normal formula cell with the formula expression =SUM($C$1:$C6).
                //
                let firstGroup = true;
                entryGroups.forEach(({ oldAddresses, anchorFormula, newBoundRange }) => {

                    // break the shared formula, if only a single formula cell will remain in the group
                    if (oldAddresses.length === 1) {
                        this.changeCell(oldAddresses[0], { f: anchorFormula, si: null, sr: null });
                        return;
                    }

                    // create the changesets for all member cells in the shared formula
                    oldAddresses.forEach((address, index) => {
                        // the resulting changeset
                        const changeSet = dict.createPartial<CellChangeSet>();
                        // write a new shared index (first group sticks to old shared index)
                        if (!firstGroup) { changeSet.si = nextFreeSi; }
                        // create the additional properties for the anchor cell
                        if (index === 0) {
                            changeSet.f = anchorFormula;
                            changeSet.sr = newBoundRange;
                        }
                        // store the new change set, keep track of processed shared formulas
                        this.changeCell(address, changeSet);
                    });

                    // find next unused index for shared formulas
                    if (!firstGroup) {
                        nextFreeSi += 1;
                        if (siInterval && (nextFreeSi === siInterval[0])) {
                            nextFreeSi = siInterval[1] + 1;
                            siInterval = itr.shift(siIntervals);
                        }
                    }
                    firstGroup = false;
                });

                // processing of shared formula done (no processing for cell or matrix formula needed)
                return;
            }

            // the resulting cell changeset
            const changeSet = dict.createPartial<CellChangeSet>();

            // special handling for moving a matrix formula in this sheet
            if (transformer && cellModel.mr) {

                // get the transformed range address, nothing to do for deleted matrixes
                const newRange = transformer.transformRanges(cellModel.mr).first();
                if (!newRange) { return; }

                // fail for partially modified matrix formulas
                OperationError.assertCause(cellModel.mr.equalSize(newRange), transformer.insert ? "formula:matrix:insert" : "formula:matrix:delete");

                // update the bounding range of the matrix formula
                if (cellModel.mr.a1.differs(newRange.a1)) {
                    changeSet.mr = newRange;
                }
            }

            // ignore formula cells that will be deleted
            if (!transformer || transformer.transformAddress(oldAnchor)) {
                // calculate the new formula expression
                updateTask.transformOpProperty(cellModel.t!, this.#grammar, "f", changeSet);
                // store the new change set
                this.changeCell(oldAnchor, changeSet);
            }
        });
    }

    /**
     * Generates the "changeCells" operations for all cells collected by this
     * instance, and inserts all collected operations into the passed operation
     * generator.
     *
     * @param generator
     *  The operation generator to be filled with all sheet operations.
     *
     * @returns
     *  The addresses of all cell ranges containing changed cells.
     */
    @modelLogger.profileMethod("$badge{CellCache} flushOperations")
    flushOperations(generator: SheetOperationGenerator, resolveStyleFn: ResolveColRowStyleFn): RangeArray {

        // whether to generate regular or undo operation
        const processOps = !this.#config.skipOps;
        const processUndo = !this.#config.skipUndo;

        // process all collected cell entries
        const operMap = new Map<string, AddressEntry>();
        const undoMap = new Map<string, AddressEntry>();
        this.#processCellEntries(operMap, undoMap, resolveStyleFn);

        // add changesets for new cells (inserted columns/rows)
        if (processOps && this.#transformer?.insert) {
            this.#processInsertCells(this.#transformer, operMap);
        }

        // add changesets for deleted cells
        if (processUndo && this.#transformer) {
            this.#processRestoreCells(this.#transformer, undoMap);
        }

        // generate operations for the collected cell data
        if (processOps && operMap.size) {
            this.#generateCellOperations(generator, operMap, false);
        }

        // generate the undo operations
        if (processUndo && undoMap.size) {
            this.#generateCellOperations(generator, undoMap, true);
        }

        // return the addresses of all changed ranges
        return RangeArray.mergeAddresses(operMap.values(), entry => entry.address);
    }

    // private methods --------------------------------------------------------

    /**
     * Reduces the collected changesets to the properties that will change
     * according to the current state of the cells in the document, and appends
     * the reduced changesets and the appropriate undo changesets to the passed
     * buffers.
     */
    #processCellEntries(operMap: Map<string, AddressEntry>, undoMap: Map<string, AddressEntry>, resolveStyleFn: ResolveColRowStyleFn): void {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;
        // the address transformer
        const transformer = this.#transformer;

        // entries for dirty shared formulas
        class DirtySharedEntry {
            sharedModel: Opt<SharedFormulaModel>;
            addressSet: AddressSet;
            constructor(sharedModel?: SharedFormulaModel) {
                this.sharedModel = sharedModel;
                this.addressSet = new AddressSet(sharedModel?.addressSet);
            }
        }

        // collect all dirty shared formulas that need updated bounding range
        const dirtySharedMap = this.#skipShared ? undefined : new Map<number, DirtySharedEntry>();

        // helper function to remove a member cell of a shared formula in the dirty cache
        const removeSharedCell = dirtySharedMap ? (sharedModel: SharedFormulaModel, address: Address): void => {
            map.update(dirtySharedMap, sharedModel.index, sharedEntry => {
                sharedEntry ??= new DirtySharedEntry(sharedModel);
                sharedEntry.addressSet.delete(address);
                return sharedEntry;
            });
        } : fun.undef;

        // helper function to insert a member cell of a shared formula in the dirty cache
        const insertSharedCell = dirtySharedMap ? (index: number, address: Address): void => {
            map.update(dirtySharedMap, index, sharedEntry => {
                sharedEntry ??= new DirtySharedEntry(this.#collection.sharedFormulas.get(index));
                sharedEntry.addressSet.add(address);
                return sharedEntry;
            });
        } : fun.undef;

        // process all collected cell entries
        this.#changeMap.forEach(entry => {

            // shortcuts to the cache entry properties
            const { address: oldAddress, changeSet } = entry;

            // the new transformed cell address
            const newAddress = transformer ? transformer.transformAddress(oldAddress) : oldAddress;
            if (!newAddress) {
                OperationError.throwCause("operation", `$badge{CellCache} processCellEntries: found changeset for deleted cell ${oldAddress}`);
            }

            // the model of the cell to be manipulated
            const cellModel = this.#collection.getCellModel(oldAddress);
            // whether to delete the cell model explicitly
            let deleteCell = changeSet.u === true;
            // the resulting changeset for the cell
            let cellSet = dict.createPartial<CellChangeSet>();
            // the resulting undo changeset
            let undoSet = dict.createPartial<CellChangeSet>();

            // the original value or formula result of the cell
            const oldValue = getModelValue(cellModel);
            // the original formula expression of the cell
            const oldFormula = cellModel?.f ?? null;
            // the original index and bounding range of a shared formula
            const oldSharedModel = cellModel?.sf;
            const oldSharedIndex = oldSharedModel?.index ?? null;
            const oldSharedRange = (cellModel?.isSharedAnchor() && oldSharedModel?.boundRange) || null;
            // the original bounding range of a matrix formula
            const oldMatrixRange = cellModel?.mr ?? null;
            const oldDynamicMatrix = !!cellModel?.md;
            // the style identifier of the cell auto-style
            const oldStyleId = cellModel ? this.docModel.autoStyles.resolveStyleId(cellModel.suid) : null;

            // create the properties in the JSON data that will really change the cell model
            if (!deleteCell) {

                // destructure the changeset
                const { v: newValue, f: newFormula, si: newSharedIndex, sr: newSharedRange, mr: newMatrixRange, md: newDynamicMatrix } = changeSet;

                // copy the cell value to the JSON data (unless it does not change)
                if ((newValue !== undefined) && (newValue !== oldValue)) {
                    cellSet.v = newValue;
                    undoSet.v = oldValue;
                }

                // copy the cell formula to the JSON data (unless it does not change)
                if ((newFormula !== undefined) && (newFormula !== oldFormula)) {
                    cellSet.f = newFormula;
                    undoSet.f = oldFormula;
                }

                // copy the shared formula index to the JSON data (unless it does not change)
                if ((newSharedIndex !== undefined) && (newSharedIndex !== oldSharedIndex)) {
                    cellSet.si = newSharedIndex;
                    undoSet.si = oldSharedIndex;
                    // bounding ranges of shared formulas need to be updated when changing the shared index
                    if (oldSharedModel) { removeSharedCell(oldSharedModel, oldAddress); }
                    if (is.number(newSharedIndex)) { insertSharedCell(newSharedIndex, oldAddress); }
                }

                // copy the shared formula range to the JSON data (unless it does not change)
                if ((newSharedRange !== undefined) && !equalRanges(newSharedRange, oldSharedRange)) {
                    cellSet.sr = newSharedRange;
                    undoSet.sr = oldSharedRange;
                }

                // copy the matrix formula range to the JSON data (unless it does not change)
                if ((newMatrixRange !== undefined) && !equalRanges(newMatrixRange, oldMatrixRange)) {
                    cellSet.mr = newMatrixRange;
                    undoSet.mr = oldMatrixRange;
                }

                // copy the dynamic matrix flag to the JSON data (unless it does not change)
                if (is.boolean(newDynamicMatrix) && (newDynamicMatrix !== oldDynamicMatrix)) {
                    cellSet.md = newDynamicMatrix;
                    undoSet.md = oldDynamicMatrix;
                }

                // the default column/row auto-style identifier for undefined cells, with current column/row defaults
                const crStyleId = resolveStyleFn(newAddress);
                // the identifier of the base auto-style used to create the new cell auto-style
                const baseStyleId = oldStyleId ?? crStyleId;
                // the resulting auto-style identifier (try to get a cached identifier from a previous call)
                const newStyleId = this.styleCache.generateAutoStyleOperations(baseStyleId, changeSet);

                // When changing an existing cell model, add the auto-style if it changes, also if set from or to the default style.
                if (oldStyleId && (oldStyleId !== newStyleId)) {
                    cellSet.s = newStyleId;
                    undoSet.s = oldStyleId;
                }

                // When the cell does not exist, the auto-style must be set if it differs from the default column/row style
                // (also set the default style to override an existing row style); or when creating the cell due to a new
                // value or formula, and the auto-style does not remain the default auto-style.
                if (!cellModel && ((newStyleId !== crStyleId) || (!is.empty(cellSet) && (newStyleId !== defStyleId)))) {
                    cellSet.s = newStyleId;
                    // undo needs to delete the cell, this will be done below in the next "if" statement
                }

                // the cell simply needs to be deleted on undo, if it does not exist yet and will be created by `cellSet`
                if (!cellModel && !is.empty(cellSet)) {
                    undoSet = { u: true };
                }

                // Check whether to undefine (delete) an existing cell model implicitly: It must be (or become) blank, it must not
                // contain a formula, and its auto-style must be (or become) equal to the default auto-style of the row or column.
                deleteCell = !!cellModel &&
                    (crStyleId === newStyleId) &&
                    ((changeSet.v === undefined) ? (oldValue === null) : (changeSet.v === null)) &&
                    ((changeSet.f === undefined) ? (oldFormula === null) : (changeSet.f === null)) &&
                    ((cellSet.si === undefined) ? (oldSharedIndex === null) : (cellSet.si === null)) &&
                    ((cellSet.sr === undefined) ? (oldSharedRange === null) : (cellSet.sr === null)) &&
                    ((cellSet.mr === undefined) ? (oldMatrixRange === null) : (cellSet.mr === null));
            }

            // create simple changeset when an existing cell model will be deleted
            // (the flag `deleteCell` may have been set in the previous if-block)
            if (deleteCell && cellModel) {

                // the property `u` marks a cell to be deleted (undefined)
                cellSet = { u: true };

                // restore the cell value, formula, and auto-style
                undoSet = dict.createPartial();
                if (oldValue !== null) { undoSet.v = oldValue; }
                if (oldFormula !== null) { undoSet.f = oldFormula; }
                if (oldSharedIndex !== null) { undoSet.si = oldSharedIndex; }
                if (oldSharedRange) { undoSet.sr = oldSharedRange; }
                if (oldMatrixRange) { undoSet.mr = oldMatrixRange; }
                if (oldDynamicMatrix) { undoSet.md = true; }
                if (oldStyleId && (oldStyleId !== defStyleId)) { undoSet.s = oldStyleId; }

                // update shared formula after deleting one of its cells
                if (oldSharedModel) { removeSharedCell(oldSharedModel, oldAddress); }
            }

            // insert the changesets into the passed data buffers
            insertChangeSet(operMap, newAddress, cellSet);
            insertChangeSet(undoMap, oldAddress, undoSet);
        });

        // update bounding ranges of shared formulas with deleted cells
        dirtySharedMap?.forEach(sharedEntry => {

            // the new set of addresses of the shared formula
            const newAddressSet = sharedEntry.addressSet;
            if (!newAddressSet.size) { return; }

            // the current and new anchor address and bounding range
            const { sharedModel } = sharedEntry;
            const oldAnchor = sharedModel?.anchorAddress;
            const newAnchor = newAddressSet.first()!;
            const oldBoundRange = sharedModel?.boundRange;
            const newBoundRange = newAddressSet.boundary()!;

            // no current shared formula model available (newly generated): add the bounding range
            if (!sharedModel || !oldAnchor || !oldBoundRange) {
                OperationError.assertCause(operMap.get(newAnchor.key)?.changeSet.f, "operation", `missing formula expression for new shared formula in ${newAnchor}`);
                updateChangeSet(operMap, newAnchor, { sr: newBoundRange });
                updateChangeSet(undoMap, newAnchor, { sr: null }, true);
                return;
            }

            // the cell model at the new anchor address (needed for undo changeset)
            const cellModel = this.#collection.getCellModel(newAnchor);

            // anchor address changed: copy formula expression and bounding range to the new cell
            if (oldAnchor.differs(newAnchor)) {
                const formula = sharedModel.getFormulaOp(newAnchor);
                updateChangeSet(operMap, newAnchor, { f: formula, sr: newBoundRange });
                updateChangeSet(undoMap, newAnchor, { f: cellModel?.f ?? null, sr: cellModel?.sf?.boundRange ?? null }, true);
                return;
            }

            // bounding range changed: set and restore bounding range at the anchor cell
            if (oldBoundRange.differs(newBoundRange)) {
                updateChangeSet(operMap, oldAnchor, { sr: newBoundRange });
                updateChangeSet(undoMap, oldAnchor, { sr: cellModel?.sf?.boundRange ?? null }, true);
            }
        });
    }

    /**
     * Processes the changeset needed to create new cells.
     */
    #processInsertCells(transformer: AddressTransformer, operMap: Map<string, AddressEntry>): void {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;
        // move direction
        const columns = transformer.columns;
        // function to create a range in a target interval
        const createRangeFn = columns ? Range.fromColInterval : Range.fromRowInterval;

        // create auto-styles for inserted cells (special handling for border formatting)
        transformer.targetIntervals.forEach((targetInterval, index) => {

            // nothing to do when inserting at the beginning of the sheet
            const sourceIndex = transformer.sourceIntervals[index].first;
            if (sourceIndex === 0) { return; }

            // Create a parallel iterator that visits all columns/rows with at least one defined cell before or
            // after the insertion line. Example: When inserting columns C:E, the existing cells in column B need
            // to be extended into the new blank columns C, D, and E, with special border processing of the cells
            // in columns B (not moved) and C (moved to F).
            const prevLine = transformer.createBandLineRange(sourceIndex - 1);
            const nextLine = transformer.createBandLineRange(sourceIndex);
            const addressIt = this.#collection.parallelAddresses([prevLine, nextLine], { type: CellType.DEFINED });

            // collect the range addresses for inserting defined cells into the new blank ranges
            for (const addresses of addressIt) {

                // get the identifiers of the auto-styles before and after the new blank range
                const styleId1 = this.#collection.getStyleId(addresses[0]);
                const styleId2 = this.#collection.getStyleId(addresses[1]);

                // create new entries in the sheet cache for all new cells
                const lineIndex = addresses[0].get(!columns);
                const targetRange = createRangeFn(targetInterval, lineIndex);
                for (const address of targetRange.addresses()) {

                    // calculate the new auto-style to be inserted into the blank cells
                    const newStyleId = this.styleCache.generateInsertedAutoStyleOperations(styleId1, styleId2, columns);

                    // insert the changeset into the map (without address transformation!)
                    if (newStyleId !== defStyleId) {
                        insertChangeSet(operMap, address, { s: newStyleId });
                    }
                }
            }
        });

        // generate labels for new header cells when inserting columns inside table ranges
        if (transformer.columns && transformer.insert) {
            for (const tableModel of this.sheetModel.tableCollection.yieldTableModels()) {

                // the header range of the table (nothing to do for tables without header row)
                const headerRange = tableModel.getHeaderRange();
                if (!headerRange) { continue; }

                // the new transformed ranges of the header cells (with gaps)
                const newHeaderRanges = transformer.transformRanges(headerRange, { transformMode: TransformMode.SPLIT });
                if (newHeaderRanges.length <= 1) { continue; }

                // the existing original column names of the table
                const cellIterator = this.#collection.cellEntries(headerRange);
                const colNames = Array.from(cellIterator, cellEntry => is.string(cellEntry.value) ? cellEntry.value : "");

                // a set with the uppercase column keys
                const colSet = set.from(colNames, mapKey);

                // generate all missing column names
                let colIdx = 0;
                newHeaderRanges.forEach((range1, index) => {

                    // the following new header range address to obtain the gap size
                    const range2 = newHeaderRanges[index + 1];
                    if (!range2) { return; }

                    // the name of the column before the current gap (`colIdx` points to beginning of `range1`)
                    colIdx += range1.cols();
                    const oldColName = colNames[colIdx - 1];

                    // try to split the header label of the preceding column into a base name and a trailing index
                    const matches = /^(.*?)(\d+)$/.exec(oldColName);
                    const baseName = matches?.[1] ?? getTableColName();
                    const baseKey = mapKey(baseName);
                    let nameIdx = matches ? (parseInt(matches[2], 10) + 1) : 1;

                    // generate labels for all cells in the gap
                    for (let col1 = range1.a2.c + 1, col2 = range2.a1.c; col1 < col2; col1 += 1) {

                        // generate an unused column name by increasing the training index repeatedly
                        while (colSet.has(`${baseKey}${nameIdx}`)) { nameIdx += 1; }
                        colSet.add(`${baseKey}${nameIdx}`);

                        // store the new column name in the cell cache
                        const address = new Address(col1, headerRange.a1.r);
                        updateChangeSet(operMap, address, { v: `${baseName}${nameIdx}` });
                    }
                });
            }
        }
    }

    /**
     * Creates the changesets needed to restore all deleted cells.
     */
    #processRestoreCells(transformer: AddressTransformer, undoMap: Map<string, AddressEntry>): void {

        // the document's default auto-style
        const { defStyleId } = this.styleCache;

        // visit all existing cell models that will be deleted by the transformer
        const deleteRanges = transformer.createBandRanges(transformer.deleteIntervals);
        for (const { address, model } of this.#collection.cellEntries(deleteRanges, { type: CellType.DEFINED })) {

            // check that the restored cell has not been modified otherwise
            if (undoMap.has(address.key)) {
                OperationError.throwCause("operation", `$badge{CellCache} processRestoreCells: found changeset for restored cell ${address}`);
            }

            // the model of the cell to be manipulated (exists always due to "defined" iterator)
            const cellModel = model!;

            // restore the cell value, formula, and auto-style
            const undoSet = dict.createPartial<CellChangeSet>();
            if (cellModel.v !== null) { undoSet.v = cellModel.v; }
            if (cellModel.f !== null) { undoSet.f = cellModel.f; }
            if (cellModel.sf) {
                undoSet.si = cellModel.sf.index;
                if (cellModel.isSharedAnchor()) {
                    undoSet.sr = cellModel.sf.boundRange;
                }
            }
            if (cellModel.mr) { undoSet.mr = cellModel.mr; }
            if (cellModel.md) { undoSet.md = true; }
            const oldStyleId = this.docModel.autoStyles.resolveStyleId(cellModel.suid);
            if (oldStyleId !== defStyleId) { undoSet.s = oldStyleId; }

            // insert the changeset into the passed undo buffer
            insertChangeSet(undoMap, address, undoSet);
        }
    }

    /**
     * Generates the "changeCells" operations for all cells contained in the
     * passed map of changesets.
     */
    #generateCellOperations(generator: SheetOperationGenerator, entryMap: Map<string, AddressEntry>, undo: boolean): void {

        // sort the entries row-by-row as preparation for merging to ranges with equal changesets
        const sheetRange = this.docModel.addressFactory.getSheetRange();
        const sortedEntries = ary.sortFrom(entryMap.values(), entry => sheetRange.indexAt(entry.address));

        // split into rows, try to merge adjacent cells in each row
        const rowBuffer: RangeEntry[][] = [];
        let currRow = -1;
        sortedEntries.forEach(({ address, changeSet }) => {

            // create a new entry vector, if the row index changes
            if (currRow < address.r) {
                rowBuffer.push([]);
                currRow = address.r;
            }

            // extend last entry in row vector if possible, otherwise create a new entry
            const rowVector = ary.at(rowBuffer, -1)!;
            const lastEntry = ary.at(rowVector, -1);
            if (lastEntry && (lastEntry.range.a2.c + 1 === address.c) && equalChangeSets(changeSet, lastEntry.changeSet)) {
                lastEntry.range.a2.c += 1;
            } else {
                rowVector.push({ range: new Range(address.clone()), changeSet });
            }
        });

        // try to merge entries from adjacent row vectors
        for (let irow = 1; irow < rowBuffer.length; irow += 1) {

            // the row vectors to be merged
            const rowVector1 = rowBuffer[irow - 1];
            const rowVector2 = rowBuffer[irow];

            // rows must adjoin, otherwise nothing can be merged
            if (!rowVector1.length || (rowVector1[0].range.a2.r + 1 !== rowVector2[0].range.a1.r)) {
                continue;
            }

            // try to merge each entry from `rowVector2` with an entry from `rowVector1`
            for (let i1 = 0, i2 = 0, l2 = rowVector2.length; i2 < l2; i2 += 1) {

                // the entry from `rowVector2` to be merged with an entry from `rowVector1`
                const { range: range2, changeSet: changeSet2 } = rowVector2[i2];

                // search for an entry in `rowVector1` that does not start before `entry2`
                const l1 = rowVector1.length;
                const startCol = range2.a1.c;
                while ((i1 < l1) && (rowVector1[i1].range.a1.c < startCol)) { i1 += 1; }

                // try to merge the two entries (expand range of `entry2`, and remove `entry1` from array)
                const entry1 = rowVector1[i1];
                if (entry1?.range.equalCols(range2) && equalChangeSets(entry1.changeSet, changeSet2)) {
                    range2.a1.r = entry1.range.a1.r;
                    ary.deleteAt(rowVector1, i1);
                }
            }
        }

        // the formula grammar needed to convert error codes to strings
        const grammar = this.#grammar;
        // the `contents` dictionary for the "changeCells" operations
        const contents = dict.create<CellDataOrValue>();
        // count the entries in `contents`
        let contentSize = 0;

        // extends an existing in, or inserts a new entry into the `contents` dictionary
        const addCellData = (range: Range, data: CellDataOrValue): void => {
            const key = range.toOpStr();
            const oldData = contents[key];
            if (oldData === undefined) {
                contents[key] = data;
                contentSize += 1;
            } else {
                contents[key] = mergeCellData(oldData, data);
            }
        };

        // create the JSON dictionary for the "changeCells" operation
        rowBuffer.forEach(rowVector => rowVector.forEach(({ range, changeSet }) => {

            // destructure the cell changeset
            const { u, v, f, si, sr, mr, md, s } = changeSet;

            // reduce all entries with single `v` property to simple values
            const value = (isScalar(v) && !isErrorCode(v)) ? v : undefined;
            if ((value !== undefined) && dict.singleKey(changeSet)) {
                addCellData(range, value);
                return;
            }

            // create a `CellData` object
            const cellData = dict.createPartial<CellData>();
            if (u) { cellData.u = true; }
            if (value !== undefined) {
                cellData.v = value;
            } else if (isErrorCode(v)) {
                cellData.e = grammar.getErrorName(v)!;
            }
            if (f !== undefined) { cellData.f = f; }
            if (si !== undefined) { cellData.si = si; }
            if (sr !== undefined) { cellData.sr = sr?.toOpStr() ?? null; }
            if (mr !== undefined) { cellData.mr = mr?.toOpStr() ?? null; }
            if (md !== undefined) { cellData.md = md; }
            if (s !== undefined) { cellData.s = s; }

            // insert or merge with existing entry
            addCellData(range, cellData);
        }));

        // DOCS-1662: split the contents dictionary into multiple parts to keep the size in the configured limit
        const opCount = max(1, ceil(contentSize / MAX_CONTENTS_SIZE));
        const sizePerOp = ceil(contentSize / opCount);
        const contentsArray: CellDataDict[] = [];
        for (let i = 1, j = 0; i < opCount; i += 1, j = 0) {
            const newContents = dict.create<CellDataOrValue>();
            contentsArray.push(newContents);
            for (const key in contents) {
                newContents[key] = contents[key];
                delete contents[key];
                if (++j === sizePerOp) { break; }
            }
        }
        contentsArray.push(contents);

        // generate the "changeCells" operations
        const genOptions: GeneratorOptions = { undo, filter: this.#config.filter };
        contentsArray.forEach(data => generator.generateChangeCellsOperation(data, genOptions));
    }
}
