/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, ary, map, dict } from "@/io.ox/office/tk/algorithms";
import { MAX_JSON_KEYS } from "@/io.ox/office/tk/config";

import type { GeneratorUndoOptions, GeneratorOptions } from "@/io.ox/office/editframework/model/operation/generator";

import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { isErrorCode, isScalar } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";
import type { CellData, CellDataOrValue, CellDataDict } from "@/io.ox/office/spreadsheet/utils/operations";
import { mergeCellData } from "@/io.ox/office/spreadsheet/utils/operations";

import { getModelValue } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

import type { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import type { CellChangeSet } from "@/io.ox/office/spreadsheet/model/operation/cellcache";

import type { CellAutoStyleCollection } from "@/io.ox/office/spreadsheet/model/style/cellautostylecollection";

import type { SharedFormulaModel } from "@/io.ox/office/spreadsheet/model/formula/sharedformulamodel";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

const { ceil, max } = Math;

// types ======================================================================

/**
 * A enumeration specifying which cells will be processed when formatting cell
 * ranges with the `CellOperationBuilder`.
 */
export enum ContentRangeMode {

    /**
     * Process all cells covered by a cell range. Used for regular cell
     * formatting that will be merged over column and row default styles.
     */
    CELL_MERGE = "cell:merge",

    /**
     * Process existing cells only (do not create new cells). Used when
     * formatting entire rows with own active default style.
     */
    ROW_DEF = "row:def",

    /**
     * Merge attributes for undefined cells with column default styles. Used
     * when formatting entire rows without own active default style.
     */
    ROW_MERGE = "row:merge",

    /**
     * Merge attributes for undefined cells with active row default styles.
     * Used when formatting entire columns.
     */
    COL_MERGE = "col:merge",

    /**
     * Skip the formatting of the range.
     */
    SKIP = "skip"
}

export interface CellChangeSet2 extends CellChangeSet {

    /**
     * Internal helper property used for formatting entire columns and rows.
     * Specifies which cells need to be processed in a cell range. Default
     * value is `CELL_MERGE` for regular processing of cells.
     */
    rangeMode?: ContentRangeMode;
}

/**
 * Configuration options for instances of the class `CellOperationBuilder`.
 */
export interface CellOperationBuilderConfig {

    /**
     * Whether to skip updating the shared formulas after generating the cell
     * operations. Should be used by code that handles updating the shared
     * formulas internally. Default value is `false`.
     */
    skipShared?: boolean;

    /**
     * Whether to skip creating the document operations for the changed cells.
     * Useful when only the undo operations are of intereset. Default value is
     * `false`.
     */
    skipOps?: boolean;

    /**
     * Whether to skip creating the undo operations for the changed cells.
     * Default value is `false`.
     */
    skipUndo?: boolean;

    /**
     * If set to `true`, operations for export filters will be created (remote
     * editor clients will ignore the operations). Default value is `false`.
     */
    filter?: boolean;
}

// private types --------------------------------------------------------------

interface BufferCellEntry {
    address: Address;
    changeSet: CellChangeSet;
}

interface BufferRangeEntry {
    range: Range;
    changeSet: CellChangeSet;
}

interface DirtySharedEntry {
    sharedModel: SharedFormulaModel;
    anchorAddress: Address;
    boundRange: Range;
}

// constants ==================================================================

/**
 * DOCS-1662: The maximum number of keys allowed in the `contents` dictionary
 * of "changeCells" document operations (restricted by server JSON libraries).
 *
 * Reduce the maximum available value by 10% to prevent that transformations of
 * "changeCells" with itself exceeds the limit when inserting one or a few new
 * entries into the dictionary (e.g. when splitting partially overlapping cell
 * ranges).
 */
const MAX_CONTENTS_SIZE = (MAX_JSON_KEYS === 0) ? Infinity : ceil(MAX_JSON_KEYS * 0.9);

// private functions ==========================================================

/**
 * Returns whether the passed cell range addresses are equal, or are both
 * falsy.
 *
 * @param range1
 *  The first cell range address.
 *
 * @param range2
 *  The second cell range address.
 *
 * @returns
 *  Whether the passed cell range addresses are equal, or are both falsy.
 */
function equalRanges(range1: Range | null, range2: Range | null): boolean {
    return (!range1 && !range2) || !!(range1 && range2 && range1.equals(range2));
}

/**
 * Returns whether the passed content change sets are equal. Ignores all
 * internal properties that do not qualify for the test (e.g. `cacheKey`).
 *
 * @param changeSet1
 *  The first change set to be tested.
 *
 * @param changeSet2
 *  The second change set to be tested.
 */
function equalChangeSets(changeSet1: CellChangeSet, changeSet2: CellChangeSet): boolean {
    return dict.equals(changeSet1, changeSet2, ["s", "format", "v", "f", "si", "md", "u"])  // strict comparison of primitive values
        && dict.equals(changeSet1, changeSet2, ["a", "table"], _.isEqual)                   // deep comparison of attribute sets
        && dict.equals(changeSet1, changeSet2, ["sr", "mr"], equalRanges);                  // compare cell range addresses
}

// public functions ===========================================================

/**
 * Returns the effective (stronger) range mode of the passed range modes.
 *
 * @param rangeMode1
 *  The first range mode to be compared with the second.
 *
 * @param rangeMode2
 *  The second range mode to be compared with the first.
 *
 * @returns
 *  The effective (stronger) range mode of the passed range modes.
 */
export function mergeRangeMode(rangeMode1: ContentRangeMode, rangeMode2: ContentRangeMode): ContentRangeMode {

    // short-cut: equal range modes
    if (rangeMode1 === rangeMode2) { return rangeMode1; }

    // ignore range mode `SKIP` (fall-back to other range mode)
    if (rangeMode1 === ContentRangeMode.SKIP) { return rangeMode2; }
    if (rangeMode2 === ContentRangeMode.SKIP) { return rangeMode1; }

    // needing to process all cells in a range always wins over every other range mode
    // (cell formatting wins over column and row default formatting)
    if ((rangeMode1 === ContentRangeMode.CELL_MERGE) || (rangeMode2 === ContentRangeMode.CELL_MERGE)) {
        return ContentRangeMode.CELL_MERGE;
    }

    // row formatting wins over column formatting
    return (rangeMode1 === ContentRangeMode.COL_MERGE) ? rangeMode2 : rangeMode1;
}

// private class ChangeSetBuffer ==============================================

/**
 * A helper class to store the change sets for multiple cells, and to generate
 * a `changeCells` operation from all collected contents.
 *
 * @param sheetModel
 *  The model of the sheet this instance is associated to.
 *
 * @param [config]
 *  Configuration options.
 */
class ChangeSetBuffer {

    // properties -------------------------------------------------------------

    // the configuration for the formula grammar used in operations
    readonly #grammar: FormulaGrammar;

    // the configuration settings passed to the constructor
    readonly #config?: CellOperationBuilderConfig;

    // the cached change sets and cell addresses
    readonly #entries = new Array<BufferCellEntry>();

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, config?: CellOperationBuilderConfig) {
        this.#grammar = sheetModel.docModel.formulaGrammarOP;
        this.#config = config;
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts a cell change set into this buffer.
     *
     * @param address
     *  The address of a single cell to be filled with the passed cell data.
     *
     * @param changeSet
     *  The cell properties to be applied to the specified cell.
     */
    insertCell(address: Address, changeSet: CellChangeSet): void {
        this.#entries.push({ address, changeSet });
    }

    /**
     * Generates a `changeCells` operation for all cells collected by this
     * instance.
     */
    generateOperation(generator: SheetOperationGenerator, options?: GeneratorUndoOptions): void {

        // sort the array row-by-row as preparation for merging to ranges with equal change sets
        this.#entries.sort((entry1, entry2) => Address.compare(entry1.address, entry2.address));

        // split into rows, try to merge adjacent cells in each row
        const rowBuffer: BufferRangeEntry[][] = [];
        let currRow = -1;
        this.#entries.forEach(({ address, changeSet }) => {

            // create a new entry vector, if the row index changes
            if (currRow < address.r) {
                rowBuffer.push([]);
                currRow = address.r;
            }

            // extend last entry in row vector if possible, otherwise create a new entry
            const rowVector = ary.at(rowBuffer, -1)!;
            const lastEntry = ary.at(rowVector, -1);
            if (lastEntry && (lastEntry.range.a2.c + 1 === address.c) && equalChangeSets(changeSet, lastEntry.changeSet)) {
                lastEntry.range.a2.c += 1;
            } else {
                rowVector.push({ range: new Range(address.clone()), changeSet });
            }
        });

        // try to merge entries from adjacent row vectors
        for (let irow = 1; irow < rowBuffer.length; irow += 1) {

            // the row vectors to be merged
            const rowVector1 = rowBuffer[irow - 1];
            const rowVector2 = rowBuffer[irow];

            // rows must adjoin, otherwise nothing can be merged
            if (!rowVector1.length || (rowVector1[0].range.a2.r + 1 !== rowVector2[0].range.a1.r)) {
                continue;
            }

            // try to merge each entry from `rowVector2` with an entry from `rowVector1`
            for (let i1 = 0, i2 = 0, l2 = rowVector2.length; i2 < l2; i2 += 1) {

                // the entry from `rowVector2` to be merged with an entry from `rowVector1`
                const { range: range2, changeSet: changeSet2 } = rowVector2[i2];

                // search for an entry in `rowVector1` that does not start before `entry2`
                const l1 = rowVector1.length;
                const startCol = range2.a1.c;
                while ((i1 < l1) && (rowVector1[i1].range.a1.c < startCol)) { i1 += 1; }

                // try to merge the two entries (expand range of `entry2`, and remove `entry1` from array)
                const entry1 = rowVector1[i1];
                if (entry1?.range.equalCols(range2) && equalChangeSets(entry1.changeSet, changeSet2)) {
                    range2.a1.r = entry1.range.a1.r;
                    ary.deleteAt(rowVector1, i1);
                }
            }
        }

        // the formula grammar needed to convert error codes to strings
        const grammar = this.#grammar;
        // the `contents` dictionary for the "changeCells" operations
        const contents = dict.create<CellDataOrValue>();
        // count the entries in `contents`
        let contentSize = 0;

        // extends an existing in, or inserts a new entry into the `contents` dictionary
        const addCellData = (range: Range, data: CellDataOrValue): void => {
            const key = range.toOpStr();
            const oldData = contents[key] as Opt<CellDataOrValue>;
            if (oldData === undefined) {
                contents[key] = data;
                contentSize += 1;
            } else {
                contents[key] = mergeCellData(oldData, data);
            }
        };

        // create the JSON dictionary for the `changeCells` operation
        rowBuffer.forEach(rowVector => rowVector.forEach(({ range, changeSet }) => {

            // destructure the cell change set
            const { u, v, f, si, sr, mr, md, s } = changeSet;

            // reduce all entries with single `v` property to simple values
            const value = (isScalar(v) && !isErrorCode(v)) ? v : undefined;
            if ((value !== undefined) && dict.singleKey(changeSet)) {
                addCellData(range, value);
                return;
            }

            // create a `CellData` object
            const cellData = dict.createPartial<CellData>();
            if (u) { cellData.u = true; }
            if (value !== undefined) {
                cellData.v = value;
            } else if (isErrorCode(v)) {
                cellData.e = grammar.getErrorName(v)!;
            }
            if (f !== undefined) { cellData.f = f; }
            if (si !== undefined) { cellData.si = si; }
            if (sr !== undefined) { cellData.sr = sr?.toOpStr() ?? null; }
            if (mr !== undefined) { cellData.mr = mr?.toOpStr() ?? null; }
            if (md !== undefined) { cellData.md = md; }
            if (s !== undefined) { cellData.s = s; }

            // insert or merge with existing entry
            addCellData(range, cellData);
        }));

        // DOCS-1662: split the contents dictionary into multiple parts to keep the size in the configured limit
        const opCount = max(1, ceil(contentSize / MAX_CONTENTS_SIZE));
        const sizePerOp = ceil(contentSize / opCount);
        const contentsArray: CellDataDict[] = [];
        for (let i = 1, j = 0; i < opCount; i += 1, j = 0) {
            const newContents = dict.create<CellDataOrValue>();
            contentsArray.push(newContents);
            for (const key in contents) {
                newContents[key] = contents[key];
                delete contents[key];
                if (++j === sizePerOp) { break; }
            }
        }
        contentsArray.push(contents);

        // generate the `changeCells` operations
        const genOptions: GeneratorOptions = { ...options, filter: this.#config?.filter };
        contentsArray.forEach(data => generator.generateChangeCellsOperation(data, genOptions));
    }
}

// class CellOperationBuilder =================================================

/**
 * A builder to generate cell operations and undo operations for multiple cells
 * in a cell collection.
 *
 * @param sheetModel
 *  The model of the sheet this instance is associated to.
 *
 * @param [config]
 *  Configuration options.
 */
export class CellOperationBuilder {

    // properties -------------------------------------------------------------

    // the sheet model this builder works on
    readonly #sheetModel: SheetModel;

    // the external generator
    readonly #generator: SheetOperationGenerator;

    // the configuration settings passed to the constructor
    readonly #config?: CellOperationBuilderConfig;

    // the parent builder needed to generate cell auto-styles
    readonly #styleCache: SheetStyleCache;

    // the collection of cell auto-styles of the document
    readonly #autoStyles: CellAutoStyleCollection;

    // the cell collection this builder works on
    readonly #cellCollection: CellCollection;

    // the addresses of all cells with a change set
    readonly #addresses = new AddressSet();

    // the JSON data for the cell operations
    readonly #cellBuffer: ChangeSetBuffer;

    // the JSON data for the undo operations
    readonly #undoBuffer: ChangeSetBuffer;

    // whether shared formulas need to be updated after changing formula properties
    readonly #dirtySharedMap = new Map<number, DirtySharedEntry>();

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, generator: SheetOperationGenerator, config?: CellOperationBuilderConfig) {
        this.#sheetModel = sheetModel;
        this.#generator = generator;
        this.#config = config;
        this.#styleCache = new SheetStyleCache(sheetModel.docModel, { applyImmediately: generator.isImmediateApplyMode() });
        this.#autoStyles = sheetModel.docModel.autoStyles;
        this.#cellCollection = sheetModel.cellCollection;
        this.#config = config;
        this.#cellBuffer = new ChangeSetBuffer(sheetModel, config);
        this.#undoBuffer = new ChangeSetBuffer(sheetModel, config);
    }

    // public methods ---------------------------------------------------------

    /**
     * Processes the passed cell properties for a single cell, and stores all
     * data needed to be inserted into the resulting cell operations.
     *
     * @param address
     *  The address of a single cell to be changed with the passed cell data.
     *
     * @param changeSet
     *  The cell properties to be applied to the specified cell.
     *
     * @returns
     *  Whether an entry has been created for the cell (i.e., whether the
     *  passed change set actually causes to change something).
     */
    insertCell(address: Address, changeSet: CellChangeSet2): boolean {

        // multiple changes of the same cell not supported (yet)
        if (this.#addresses.has(address)) {
            modelLogger.error(`$badge{CellOperationBuilder} insertCell: cannot change cell ${address} twice`);
            return false;
        }

        // shortcuts to the collections
        const autoStyles = this.#autoStyles;
        const cellCollection = this.#cellCollection;

        // the model of the cell to be manipulated
        const cellModel = cellCollection.getCellModel(address);
        // whether to delete the cell model explicitly
        let deleteCell = changeSet.u === true;
        // the resulting JSON contents object
        let cellSet = dict.createPartial<CellChangeSet>();
        // the resulting JSON contents object for the undo operation
        let undoSet = dict.createPartial<CellChangeSet>();

        // the original value or formula result of the cell
        const oldValue = getModelValue(cellModel);
        // the original formula expression of the cell
        const oldFormula = cellModel?.f ?? null;
        // the original index and bounding range of a shared formula
        const oldSharedModel = cellModel?.sf;
        const oldSharedIndex = oldSharedModel?.index ?? null;
        const oldSharedRange = (cellModel?.isSharedAnchor() && oldSharedModel?.boundRange) || null;
        // the original bounding range of a matrix formula
        const oldMatrixRange = cellModel?.mr ?? null;
        const oldDynamicMatrix = !!cellModel?.md;

        // create the properties in the JSON data that will really change the cell model
        if (!deleteCell) {

            // destructure the change set
            const { v: newValue, f: newFormula, si: newSharedIndex, sr: newSharedRange, mr: newMatrixRange, md: newDynamicMatrix } = changeSet;

            // copy the cell value to the JSON data (unless it does not change)
            if ((newValue !== undefined) && (newValue !== oldValue)) {
                cellSet.v = newValue;
                undoSet.v = oldValue;
            }

            // copy the cell formula to the JSON data (unless it does not change)
            if ((newFormula !== undefined) && (newFormula !== oldFormula)) {
                cellSet.f = newFormula;
                undoSet.f = oldFormula;
            }

            // copy the shared formula index to the JSON data (unless it does not change)
            if ((newSharedIndex !== undefined) && (newSharedIndex !== oldSharedIndex)) {
                cellSet.si = newSharedIndex;
                undoSet.si = oldSharedIndex;
            }

            // copy the shared formula range to the JSON data (unless it does not change)
            if ((newSharedRange !== undefined) && !equalRanges(newSharedRange, oldSharedRange)) {
                cellSet.sr = newSharedRange;
                undoSet.sr = oldSharedRange;
            }

            // copy the matrix formula range to the JSON data (unless it does not change)
            if ((newMatrixRange !== undefined) && !equalRanges(newMatrixRange, oldMatrixRange)) {
                cellSet.mr = newMatrixRange;
                undoSet.mr = oldMatrixRange;
            }

            // copy the dynamic matrix flag to the JSON data (unless it does not change)
            if (is.boolean(newDynamicMatrix) && (newDynamicMatrix !== oldDynamicMatrix)) {
                cellSet.md = newDynamicMatrix;
                undoSet.md = oldDynamicMatrix;
            }

            // add more cell properties to detach the cell from a shared formula or matrix formula
            if (!this.#config?.skipShared && (newFormula !== undefined)) {
                // reset the shared index if it has not been changed explicitly
                if ((oldSharedIndex !== null) && (newSharedIndex === undefined)) {
                    cellSet.si = null;
                    undoSet.si = oldSharedIndex;
                }
                // reset the bounding range if the current cell is the anchor cell of the shared formula
                if (oldSharedRange && (newSharedRange === undefined)) {
                    cellSet.sr = null;
                    undoSet.sr = oldSharedRange;
                }
                // reset the bounding range if the current cell is the anchor cell of the matrix formula
                if (oldMatrixRange && (newMatrixRange === undefined)) {
                    cellSet.mr = null;
                    undoSet.mr = oldMatrixRange;
                }
            }

            // bounding ranges of shared formulas need to be updated when changing the shared index
            if (cellSet.si !== undefined) {
                this.#registerDirtySharedFormula(oldSharedModel);
                this.#registerDirtySharedFormula(cellCollection.sharedFormulas.get(cellSet.si));
            }

            // the default column/row auto-style identifier for undefined cells, with current column/row defaults
            const defStyleId = cellCollection.getDefaultStyleId(address);
            // range mode `ROW_MERGE`: ignore the row style that has been set during this operation cycle when creating the cell auto-style
            const ignoreRowStyle = changeSet.rangeMode === ContentRangeMode.ROW_MERGE;
            // the style identifier of the base cell auto-style
            const cellStyleId = cellModel ? autoStyles.resolveStyleId(cellModel.suid) : null;
            // the identifier of the base auto-style used to create the new cell auto-style
            const baseStyleId = cellStyleId ?? (ignoreRowStyle ? cellCollection.getDefaultStyleId(address, { ignoreRowStyle: true }) : defStyleId);
            // the resulting auto-style identifier (try to get a cached identifier from a previous call)
            const newStyleId = this.#styleCache.generateAutoStyleOperations(baseStyleId, changeSet);

            // When changing an existing cell model, add the auto-style if it changes, also if set from or to the default style.
            if (is.string(cellStyleId) && !autoStyles.areEqualStyleIds(cellStyleId, newStyleId)) {
                cellSet.s = newStyleId;
                undoSet.s = cellStyleId;
            }

            // When the cell does not exist, the auto-style must be set if it differs from the default column/row
            // style (also set the default style to override an existing row style); or when creating the cell due
            // to a new value or formula, and the auto-style does not remain the default auto-style.
            if (!cellModel && (!autoStyles.areEqualStyleIds(defStyleId, newStyleId) || (!is.empty(cellSet) && !autoStyles.isDefaultStyleId(newStyleId)))) {
                cellSet.s = newStyleId;
                // undo needs to delete the cell, this will be done below in the next "if" statement
            }

            // the cell simply needs to be deleted on undo, if it does not exist yet and will be created
            if (!cellModel && !is.empty(cellSet)) {
                undoSet = { u: true };
            }

            // Check whether to undefine (delete) an existing cell model implicitly: It must be (or become) blank, it must not
            // contain a formula, and its auto-style must be (or become) equal to the default auto-style of the row or column.
            deleteCell = !!cellModel &&
                ((changeSet.v === undefined) ? (oldValue === null) : (changeSet.v === null)) &&
                ((changeSet.f === undefined) ? (oldFormula === null) : (changeSet.f === null)) &&
                ((cellSet.si === undefined) ? (oldSharedIndex === null) : (cellSet.si === null)) &&
                ((cellSet.sr === undefined) ? (oldSharedRange === null) : (cellSet.sr === null)) &&
                ((cellSet.mr === undefined) ? (oldMatrixRange === null) : (cellSet.mr === null)) &&
                ((cellSet.mr === undefined) ? (oldMatrixRange === null) : (cellSet.mr === null)) &&
                autoStyles.areEqualStyleIds(defStyleId, newStyleId);
        }

        // create simple contents objects when an existing cell model will be deleted
        // (the flag 'deleteCell' may have been set in the previous if-block)
        if (deleteCell && cellModel) {

            // the property `u` marks a cell to be deleted (undefined)
            cellSet = { u: true };

            // restore the cell value, formula, and auto-style
            undoSet = dict.createPartial();
            if (oldValue !== null) { undoSet.v = oldValue; }
            if (oldFormula !== null) { undoSet.f = oldFormula; }
            if (oldSharedIndex !== null) { undoSet.si = oldSharedIndex; }
            if (oldSharedRange) { undoSet.sr = oldSharedRange; }
            if (oldMatrixRange) { undoSet.mr = oldMatrixRange; }
            if (oldDynamicMatrix) { undoSet.md = true; }
            const oldCellStyleId = autoStyles.resolveStyleId(cellModel.suid);
            if (!autoStyles.isDefaultStyleId(oldCellStyleId)) { undoSet.s = oldCellStyleId; }

            // update shared formula after deleting one of its cells
            this.#registerDirtySharedFormula(oldSharedModel);
        }

        // insert the change sets into the data buffers, update addresses
        const hasChanged = !is.empty(cellSet);
        if (hasChanged) {
            this.#addresses.add(address);
            this.#cellBuffer.insertCell(address, cellSet);
            this.#undoBuffer.insertCell(address, undoSet);
        }
        return hasChanged;
    }

    /**
     * Generates the document operations and the undo operations for all cells
     * collected by this builder instance, and invalidates the entire builder
     * instance.
     *
     * @param generator
     *  The operation generator to copy the collected cell operations to.
     *
     * @returns
     *  The addresses of all cell ranges containing changed cells.
     */
    @modelLogger.profileMethod("$badge{CellOperationBuilder} finalizeOperations")
    finalizeOperations(): RangeArray {

        // nothing more to do if this instance is empty (all cell changes were no-ops)
        if (!this.#addresses.size) { return new RangeArray(); }

        // shortcut to own configuration
        const config = this.#config;
        // create the operations for all cells collected by this instance
        const subGenerator = this.#generator.createSubGenerator();

        // generate the undo operations
        if (!config?.filter && !config?.skipUndo) {
            this.#undoBuffer.generateOperation(subGenerator, { undo: true });
        }

        // generate operations for the collected cell data
        if (!config?.skipOps) {
            this.#cellBuffer.generateOperation(subGenerator);
        }

        // Update the bounding ranges of all affected shared formulas in a second pass.
        // TODO: Cannot be done in a single pass with the current implementation which depends on immediate
        // execution of the leading `changeCells` operation to detect changed/deleted anchor cells of shared formulas.
        if (!this.#config?.skipShared && this.#dirtySharedMap.size) {

            // the cell collection
            const cellCollection = this.#sheetModel.cellCollection;

            // process all shared formulas in the cell collection
            this.#dirtySharedMap.forEach(dirtyEntry => {

                // nothing to do for completely deleted shared formulas
                const { sharedModel } = dirtyEntry;
                if (!sharedModel.addressSet.size) { return; }

                // get the current anchor cell (may be missing)
                const oldAnchor = dirtyEntry.anchorAddress;
                const oldBoundRange = dirtyEntry.boundRange;
                const oldAnchorModel = cellCollection.getCellModel(oldAnchor);

                // get the new anchor cell and bounding range of the shared formula
                const newAnchor = sharedModel.anchorAddress;
                const newBoundRange = sharedModel.boundRange;
                const newAnchorModel = cellCollection.getCellModel(newAnchor);

                // cell properties to be applied for the old and new anchor cell
                const oldOperSet = dict.createPartial<CellData>();
                const oldUndoSet = dict.createPartial<CellData>();
                const newOperSet = dict.createPartial<CellData>();
                const newUndoSet = dict.createPartial<CellData>();

                // remove the bounding range from the old anchor cell (e.g. after shrinking/expanding the shared formula)
                const movedAnchor = !oldAnchorModel || (oldAnchorModel.sf !== sharedModel) || oldAnchor.differs(newAnchor);
                if (movedAnchor) {
                    oldOperSet.sr = null;
                    oldUndoSet.sr = oldBoundRange.toOpStr();
                    newOperSet.f = sharedModel.tokenArray.getFormula("op", oldAnchor, newAnchor);
                    newUndoSet.f = newAnchorModel?.f ?? null;
                }

                // update the bounding range and formula expression of the shared formula in the new anchor cell
                if (movedAnchor || oldBoundRange.differs(newBoundRange)) {
                    newOperSet.sr = newBoundRange.toOpStr();
                    newUndoSet.sr = newAnchorModel?.sf?.boundRange.toOpStr() ?? null;
                }

                // create the cell contents objects, if something will be changed
                const operContents = dict.create<CellDataOrValue>();
                const undoContents = dict.create<CellDataOrValue>();
                if (!is.empty(oldOperSet)) {
                    operContents[oldAnchor.toOpStr()] = oldOperSet;
                    undoContents[oldAnchor.toOpStr()] = oldUndoSet;
                }
                if (!is.empty(newOperSet)) {
                    operContents[newAnchor.toOpStr()] = newOperSet;
                    undoContents[newAnchor.toOpStr()] = newUndoSet;
                }

                // insert the operations into the generator
                if (!is.empty(operContents)) {
                    subGenerator.generateChangeCellsOperation(operContents);
                    subGenerator.generateChangeCellsOperation(undoContents, { undo: true, prepend: true });
                }
            });
        }

        // push the operations in the right order into the passed external generator
        this.#generator.appendOperations(this.#styleCache);
        this.#generator.appendOperations(subGenerator);
        this.#generator.appendOperations(subGenerator, { undo: true });
        this.#generator.appendOperations(this.#styleCache, { undo: true });

        // return the addresses of all changed ranges
        return RangeArray.mergeAddresses(this.#addresses);
    }

    // private methods --------------------------------------------------------

    #registerDirtySharedFormula(sharedModel?: SharedFormulaModel): void {
        if (sharedModel) {
            map.upsert(this.#dirtySharedMap, sharedModel.index, () => ({
                sharedModel,
                anchorAddress: sharedModel.anchorAddress.clone(),
                boundRange: sharedModel.boundRange.clone()
            }));
        }
    }
}
