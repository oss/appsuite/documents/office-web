/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, to, math, itr, ary, dict, pick, jpromise } from "@/io.ox/office/tk/algorithms";
import { onceMethod, debounceMethod } from "@/io.ox/office/tk/objects";

import type { Position, Operation, PositionOperation } from "@/io.ox/office/editframework/utils/operations";
import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import type { ExtAttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";
import type { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import type { DocumentAttributes, CreateAndApplyOperationsOptions } from "@/io.ox/office/editframework/model/editmodel";

import DrawingStyleCollection from "@/io.ox/office/drawinglayer/model/drawingstylecollection";

import CharacterStyleCollection from "@/io.ox/office/textframework/format/characterstyles";
import type TableRowStyleCollection from "@/io.ox/office/textframework/format/tablerowstyles";
import type { TextBaseAttributePoolMap, TextBaseModelEventMap } from "@/io.ox/office/textframework/model/editor";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";
import ListHandlerMixin from "@/io.ox/office/textframework/model/listhandlermixin";
import UpdateListsMixin from "@/io.ox/office/textframework/model/updatelistsmixin";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { type OptSheet, modelLogger, inverseSortVector } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { type Range3DSource, Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { type SelectionState, SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";

import { ModelPropertySet } from "@/io.ox/office/spreadsheet/model/modelpropset";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";
import type { RangeContentOptions, CellContents, CellContentsArray } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import { type NameIteratorOptions, NameCollection } from "@/io.ox/office/spreadsheet/model/namecollection";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import { SortListCollection } from "@/io.ox/office/spreadsheet/model/sortlistcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { MatchSheetOptions, SheetIteratorOptions, SheetCollectionEventMap } from "@/io.ox/office/spreadsheet/model/sheetcollection";
import { SheetCollection } from "@/io.ox/office/spreadsheet/model/sheetcollection";
import { DocumentSnapshot } from "@/io.ox/office/spreadsheet/model/modelsnapshot";

import { SheetAttributePool } from "@/io.ox/office/spreadsheet/model/style/sheetattributepool";
import { type PageAttributes, PageStyleCollection } from "@/io.ox/office/spreadsheet/model/style/pagestylecollection";
import { CellStyleCollection } from "@/io.ox/office/spreadsheet/model/style/cellstylecollection";
import { ParagraphStyleCollection } from "@/io.ox/office/spreadsheet/model/style/paragraphstylecollection";
import { TableStyleCollection } from "@/io.ox/office/spreadsheet/model/style/tablestylecollection";
import { CellAutoStyleCollection } from "@/io.ox/office/spreadsheet/model/style/cellautostylecollection";
import { FontMetrics } from "@/io.ox/office/spreadsheet/model/style/fontmetrics";

import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { type OperationBuilderFlushResult, OperationBuilder } from "@/io.ox/office/spreadsheet/model/operation/builder";

import type { GrammarType } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import type { DefinedNameToken, TableToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import type { DefNameRef, TableRef, IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import type { FormulaResource, FormulaResourceFactory, FormulaResourceType } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { type FormulaGrammarConfig, FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { FormulaParser } from "@/io.ox/office/spreadsheet/model/formula/parser/formulaparser";
import { RangeListParser } from "@/io.ox/office/spreadsheet/model/formula/parser/rangelistparser";
import { FormulaCompiler } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacompiler";
import { FormulaInterpreter } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulainterpreter";
import { DependencyManager } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencymanager";

import { type SheetDrawingAttributes, type SheetDrawingModel, DRAWING_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/drawing/drawingmodel";
import { SheetChartModel } from "@/io.ox/office/spreadsheet/model/drawing/chart/chartmodel";
import type { SheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/drawingcollection";
import type { CommentModel } from "@/io.ox/office/spreadsheet/model/drawing/commentmodel";
import { FieldManager } from "@/io.ox/office/spreadsheet/model/drawing/text/fieldmanager";

import type SpreadsheetApp from "@/io.ox/office/spreadsheet/app/application";

// re-exports =================================================================

export { SHEET_FORWARD_EVENTS } from "@/io.ox/office/spreadsheet/model/sheetcollection";

// types ======================================================================

export interface SheetDocumentAttributes extends DocumentAttributes {

    /**
     * Number of columns available in each sheet of the document.
     */
    cols: number;

    /**
     * Number of rows available in each sheet of the document.
     */
    rows: number;

    /**
     * Specifies whether all formulas in the document shall be relaculated once
     * after import.
     */
    calcOnLoad: boolean;

    /**
     * The null date (the date represented by the serial value 0) of the
     * workbook. Must be a date in ISO format YYYY-MM-DD.
     */
    nullDate: string;

    // view attributes

    /**
     * Index of the active (displayed) sheet.
     */
    activeSheet: number;

    /**
     * Whether to show cell references in formulas in R1C1 notation.
     */
    rcStyle: boolean;
}

export interface DocAttributePoolMap extends TextBaseAttributePoolMap {
    document: [SheetDocumentAttributes];
    page:     [PageAttributes];
    drawing:  [SheetDrawingAttributes];
}

export type BuildOperationsOptions = Omit<CreateAndApplyOperationsOptions<SheetOperationGenerator>, "generator">;

/**
 * Type mapping for the events emitted by `SpreadsheetModel` instances.
 */
export interface SpreadsheetModelEventMap extends TextBaseModelEventMap<DocAttributePoolMap>, SheetCollectionEventMap {

    /**
     * Will be emitted after the UI locale was changed, and the document model
     * has updated itself.
     */
    "change:locale": [];

    // for compatibility with editframework
    "insert:threadedcomment": [commentModel: CommentModel];
    "new:threadedcomment:thread": [commentModel: CommentModel];
    "new:threadedcomment:comment": [commentModel: CommentModel];
    "delete:threadedcomment:end": [commentThread: CommentModel[]];
}

// constants ==================================================================

const FORMULA_GRAMMAR_CONFIGS: Record<Exclude<GrammarType, "ui">, FormulaGrammarConfig & { resource: FormulaResourceType }> = {
    op:         { resource: "std",  localized: false,   rcStyle: false },
    "op:oox":   { resource: "oox",  localized: false,   rcStyle: false },
    "ui:a1":    { resource: "std",  localized: true,    rcStyle: false },
    "ui:rc":    { resource: "std",  localized: true,    rcStyle: true },
    "en:a1":    { resource: "en",   localized: true,    rcStyle: false },
    "en:rc":    { resource: "en",   localized: true,    rcStyle: true }
};

// additional attributes for global document configuration
const DOCUMENT_ATTRIBUTES: ExtAttributeConfigBag<SheetDocumentAttributes, DocumentAttributes> = {
    cols:        { def: 1024 },
    rows:        { def: 1048576 },
    calcOnLoad:  { def: false },
    nullDate:    { def: "1899-12-30" },
    activeSheet: { def: 0 },
    rcStyle:     { def: false }
};

// the names of document operations that modify text contents in drawing objects
const DRAWING_TEXT_OPERATIONS = new Set([
    Op.SET_ATTRIBUTES,
    Op.DELETE,
    Op.MOVE,
    Op.TEXT_INSERT,
    Op.TAB_INSERT,
    Op.HARDBREAK_INSERT,
    Op.PARA_INSERT,
    Op.PARA_SPLIT,
    Op.PARA_MERGE,
    Op.INSERT_LIST,
    Op.DELETE_LIST
]);

// private functions ==========================================================

function makeTableRef(tableModel: TableModel): TableRef {
    return {
        name: tableModel.name,
        sheet: tableModel.sheetModel.getIndex(),
        range: tableModel.getRange(),
        header: tableModel.hasHeaderRow(),
        footer: tableModel.hasFooterRow(),
        model: tableModel,
        // lazy access to the column keys
        get columns(): string[] { return tableModel.getColumnKeys(); }
    };
}

// class SpreadsheetModel =====================================================

// declaration merging to add all properties of base interfaces automatically
export interface SpreadsheetModel extends IDocumentAccess { }

/**
 * Represents a "workbook", the root document model of a spreadsheet document.
 * Implements execution of all supported JSON document operations.
 */
export class SpreadsheetModel extends TextBaseModel<DocAttributePoolMap, SpreadsheetModelEventMap> implements IDocumentAccess {

    /**
     * The application instance containing this document model.
     */
    declare readonly docApp: SpreadsheetApp;

    /**
     * Runtime properties.
     */
    readonly propSet: ModelPropertySet;

    /**
     * The address factory wrapping the current size of the sheets.
     */
    readonly addressFactory: AddressFactory;

    /**
     * Font metrics helper with methods for font-size dependent lengths.
     */
    readonly fontMetrics: FontMetrics;

    /**
     * The number formatter of this document.
     */
    override readonly numberFormatter: NumberFormatter;

    /**
     * The special attribute pool for cell formatting attributes used by the
     * spreadsheet model. Used to be able to distinguish cell character
     * attributes from drawing character attributes.
     */
    readonly sheetAttrPool: SheetAttributePool;

    /**
     * The stylesheet collection for cells, columns, and rows.
     */
    readonly cellStyles: CellStyleCollection;

    /**
     * The stylesheet collection for table ranges.
     */
    readonly tableStyles: TableStyleCollection;

    /**
     * The stylesheet collection for drawing objects.
     */
    readonly drawingStyles: DrawingStyleCollection<SpreadsheetModel>;

    /**
     * The stylesheet collection for page settings.
     */
    readonly pageStyles: PageStyleCollection;

    /**
     * The collection of character stylesheets used by the text framework for
     * text contents in drawing objects. These attributes will *not* be used
     * for character formatting in cells!
     */
    readonly characterStyles: CharacterStyleCollection<SpreadsheetModel>;

    /**
     * The collection of paragraph stylesheets used by the text framework for
     * text contents in drawing objects.
     */
    readonly paragraphStyles: ParagraphStyleCollection;

    // Added to fulfil base class constraints, but not existing at runtime.
    // Used by text framework to implement top-level table operations (tables
    // in text document, tables in drawing objects), both are not supported by
    // Spreadsheet applications.
    declare readonly tableRowStyles: TableRowStyleCollection<SpreadsheetModel>;
    declare readonly tableCellStyles: StyleCollection<SpreadsheetModel, TextBaseAttributePoolMap, Empty>;

    /**
     * The cell autostyle collection of this document.
     */
    readonly autoStyles: CellAutoStyleCollection;

    /**
     * The collection of globally-defined names.
     */
    readonly nameCollection: NameCollection;

    /**
     * The collection of globally-defined sort lists.
     */
    readonly sortListCollection: SortListCollection;

    /**
     * The collection with all sheet models.
     */
    readonly sheetCollection: SheetCollection;

    /**
     * The formula resource containing all localized strings used in formula
     * expressions for the current UI language. This instance may change during
     * lifetime of the document (according to changed locale data settings) and
     * _MUST NOT_ be cached in other class instances.
     */
    readonly formulaResource: FormulaResource;

    /**
     * The formula grammar used for formula expressions contained in JSON
     * document operations.
     */
    readonly formulaGrammarOP: FormulaGrammar;

    /**
     * The formula grammar used for formula expressions shown in the GUI. This
     * instance may change during lifetime of the document (according to
     * changed locale data settings, or the A1/RC reference style) and *MUST
     * NOT* be cached in other class instances.
     */
    readonly formulaGrammarUI: FormulaGrammar;

    /**
     * The parser for formula expressions used in JSON document operations.
     */
    readonly formulaParserOP: FormulaParser;

    /**
     * The parser/formatter for range list expressions used in JSON document
     * operations.
     */
    readonly rangeListParserOP: RangeListParser;

    /**
     * The formula compiler.
     */
    readonly formulaCompiler: FormulaCompiler;

    /**
     * The formula interpreter.
     */
    readonly formulaInterpreter: FormulaInterpreter;

    /**
     * The dependency manager.
     */
    readonly dependencyManager: DependencyManager;

    /**
     * The global factory for locale-dependent formula resources.
     */
    #resourceFactory: FormulaResourceFactory;

    // constructor ------------------------------------------------------------

    /**
     * @param docApp
     *  The application containing this document model.
     */
    constructor(docApp: SpreadsheetApp) {

        // base constructor
        super(docApp, {
            defaultStyleFamily: "cell",
            slideMode: true,
            disableRedoWithOT: true // DOCS-1968: disable OT in "redo" as temporary workaround until DOCS-1969 is implemented
        });

        // text framework mixin classes
        ListHandlerMixin.call(this);
        UpdateListsMixin.call(this, docApp);

        // public properties
        this.sheetAttrPool = this.member(new SheetAttributePool(this));
        this.propSet = this.member(new ModelPropertySet(this));
        this.addressFactory = new AddressFactory(1, 1);
        this.fontMetrics = this.member(new FontMetrics(this));
        this.numberFormatter = this.member(new NumberFormatter(this));

        // create and register spreadsheet stylesheet collections
        this.cellStyles = this.addStyleCollection(new CellStyleCollection(this));
        this.tableStyles = this.addStyleCollection(new TableStyleCollection(this));
        this.drawingStyles = this.addStyleCollection(new DrawingStyleCollection(this));
        this.pageStyles = this.addStyleCollection(new PageStyleCollection(this));

        // create style collections needed for text in shapes
        this.characterStyles = this.addStyleCollection(new CharacterStyleCollection(this));
        this.paragraphStyles = this.addStyleCollection(new ParagraphStyleCollection(this));

        // autostyle collections
        this.autoStyles = this.addAutoStyleCollection(new CellAutoStyleCollection(this));

        // resources and formulas
        this.#resourceFactory = docApp.resourceManager.formulaResourceFactory;
        this.formulaResource = this.#resourceFactory.getResource("std");
        this.formulaGrammarOP = this.getFormulaGrammar("op");
        this.formulaGrammarUI = this.getFormulaGrammarUI();
        this.formulaParserOP = this.getFormulaParser("op");
        this.rangeListParserOP = RangeListParser.create(this.formulaGrammarOP);
        this.formulaCompiler = this.member(new FormulaCompiler(this));
        this.formulaInterpreter = this.member(new FormulaInterpreter(this));
        this.dependencyManager = this.member(new DependencyManager(this));

        // global document collections
        this.nameCollection = this.member(new NameCollection(this));
        this.sortListCollection = this.member(new SortListCollection(this));
        this.sheetCollection = this.member(new SheetCollection(this));

        // register additional attributes
        this.defAttrPool.registerAttrFamily("document", DOCUMENT_ATTRIBUTES);
        this.defAttrPool.registerAttrFamily("drawing", DRAWING_ATTRIBUTES);

        // further helper classes expected by the text framework used for text in shapes
        this.setFieldManager(new FieldManager());

        // register operation handlers for cells
        this.#registerSheetOperationHandler(Op.CHANGE_CELLS, (context, sheetModel) => sheetModel.cellCollection.applyChangeCellsOperation(context));
        this.#registerSheetOperationHandler(Op.MOVE_CELLS,   (context, sheetModel) => sheetModel.cellCollection.applyMoveCellsOperation(context));
        this.#registerSheetOperationHandler(Op.MERGE_CELLS,  (context, sheetModel) => sheetModel.mergeCollection.applyMergeCellsOperation(context));

        // register operation handlers for hyperlinks
        this.#registerSheetOperationHandler(Op.INSERT_HYPERLINK, (context, sheetModel) => sheetModel.hlinkCollection.applyInsertOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_HYPERLINK, (context, sheetModel) => sheetModel.hlinkCollection.applyDeleteOperation(context));

        // register operation handlers for column collections
        this.#registerSheetOperationHandler(Op.INSERT_COLUMNS, (context, sheetModel) => sheetModel.colCollection.applyInsertOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_COLUMNS, (context, sheetModel) => sheetModel.colCollection.applyDeleteOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_COLUMNS, (context, sheetModel) => sheetModel.colCollection.applyChangeOperation(context));

        // register operation handlers for row collections
        this.#registerSheetOperationHandler(Op.INSERT_ROWS, (context, sheetModel) => sheetModel.rowCollection.applyInsertOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_ROWS, (context, sheetModel) => sheetModel.rowCollection.applyDeleteOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_ROWS, (context, sheetModel) => sheetModel.rowCollection.applyChangeOperation(context));

        // register operation handlers for defined names (allow operations to address global names as well as sheet names)
        this.#registerNameOperationHandler(Op.INSERT_NAME, (context, nameCollection) => nameCollection.applyInsertOperation(context));
        this.#registerNameOperationHandler(Op.DELETE_NAME, (context, nameCollection) => nameCollection.applyDeleteOperation(context));
        this.#registerNameOperationHandler(Op.CHANGE_NAME, (context, nameCollection) => nameCollection.applyChangeOperation(context));

        // register operation handlers for table ranges
        this.#registerSheetOperationHandler(Op.INSERT_TABLE,        (context, sheetModel) => sheetModel.tableCollection.applyInsertOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_TABLE,        (context, sheetModel) => sheetModel.tableCollection.applyDeleteOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_TABLE,        (context, sheetModel) => sheetModel.tableCollection.applyChangeOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_TABLE_COLUMN, (context, sheetModel) => sheetModel.tableCollection.applyChangeColumnOperation(context));

        // register operation handlers for data validation rules
        this.#registerSheetOperationHandler(Op.INSERT_DVRULE, (context, sheetModel) => sheetModel.dvRuleCollection.applyInsertOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_DVRULE, (context, sheetModel) => sheetModel.dvRuleCollection.applyDeleteOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_DVRULE, (context, sheetModel) => sheetModel.dvRuleCollection.applyChangeOperation(context));

        // register operation handlers for conditional formatting rules
        this.#registerSheetOperationHandler(Op.INSERT_CFRULE, (context, sheetModel) => sheetModel.cfRuleCollection.applyInsertOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_CFRULE, (context, sheetModel) => sheetModel.cfRuleCollection.applyDeleteOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_CFRULE, (context, sheetModel) => sheetModel.cfRuleCollection.applyChangeOperation(context));

        // register operation handlers for note collections
        this.#registerSheetOperationHandler(Op.INSERT_NOTE, (context, sheetModel) => sheetModel.noteCollection.applyInsertNoteOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_NOTE, (context, sheetModel) => sheetModel.noteCollection.applyDeleteNoteOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_NOTE, (context, sheetModel) => sheetModel.noteCollection.applyChangeNoteOperation(context));
        this.#registerSheetOperationHandler(Op.MOVE_NOTES,  (context, sheetModel) => sheetModel.noteCollection.applyMoveNotesOperation(context));

        // register operation handlers for comment collections
        this.#registerSheetOperationHandler(Op.INSERT_COMMENT, (context, sheetModel) => sheetModel.commentCollection.applyInsertCommentOperation(context));
        this.#registerSheetOperationHandler(Op.DELETE_COMMENT, (context, sheetModel) => sheetModel.commentCollection.applyDeleteCommentOperation(context));
        this.#registerSheetOperationHandler(Op.CHANGE_COMMENT, (context, sheetModel) => sheetModel.commentCollection.applyChangeCommentOperation(context));
        this.#registerSheetOperationHandler(Op.MOVE_COMMENTS,  (context, sheetModel) => sheetModel.commentCollection.applyMoveCommentsOperation(context));

        // register operation handlers for drawing collections
        this.#registerDrawingOperationHandler(Op.INSERT_DRAWING, (context, drawingCollection, position) => drawingCollection.applyInsertDrawingOperation(context, position));
        this.#registerDrawingOperationHandler(Op.DELETE_DRAWING, (context, drawingCollection, position) => drawingCollection.applyDeleteDrawingOperation(context, position));
        this.#registerDrawingOperationHandler(Op.MOVE_DRAWING,   (context, drawingCollection, position) => drawingCollection.applyMoveDrawingOperation(context, position));

        // register generic operation handlers for drawing models
        this.#registerDrawingModelOperationHandler(Op.CHANGE_DRAWING, (context, drawingModel) => drawingModel.applyChangeOperation(context));

        // register operation handlers for chart models
        this.#registerChartModelOperationHandler(Op.INSERT_CHART_SERIES, (context, chartModel) => chartModel.applyInsertSeriesOperation(context));
        this.#registerChartModelOperationHandler(Op.DELETE_CHART_SERIES, (context, chartModel) => chartModel.applyDeleteSeriesOperation(context));
        this.#registerChartModelOperationHandler(Op.CHANGE_CHART_SERIES, (context, chartModel) => chartModel.applyChangeSeriesOperation(context));
        this.#registerChartModelOperationHandler(Op.DELETE_CHART_AXIS, (context, chartModel) =>   chartModel.applyDeleteAxisOperation(context));
        this.#registerChartModelOperationHandler(Op.CHANGE_CHART_AXIS, (context, chartModel) =>   chartModel.applyChangeAxisOperation(context));
        this.#registerChartModelOperationHandler(Op.CHANGE_CHART_GRID, (context, chartModel) =>   chartModel.applyChangeGridOperation(context));
        this.#registerChartModelOperationHandler(Op.CHANGE_CHART_TITLE, (context, chartModel) =>  chartModel.applyChangeTitleOperation(context));
        this.#registerChartModelOperationHandler(Op.CHANGE_CHART_LEGEND, (context, chartModel) => chartModel.applyChangeLegendOperation(context));

        // further initialization after import (also if import has failed)
        this.waitForImport(() => {

            // nothing to do without any sheet models
            const sheetCount = this.sheetCollection.getSheetCount();
            if (sheetCount === 0) { return; }

            // initialize dynamic sheet properties from the imported attributes and launch options
            for (const sheetModel of this.yieldSheetModels()) {
                sheetModel.initializePropSet();
            }

            // DOCS-1931: prefer view settings from (reload) launch options
            const launchAttrs = pick.dict(docApp.launchConfig, "docAttrs", true);

            // activate the nearest visible sheet
            const activeSheet = to.number(launchAttrs.activeSheet, this.globalConfig.activeSheet);
            const visibleSheet = this.sheetCollection.findVisibleSheet(math.clamp(activeSheet, 0, sheetCount - 1));
            if (visibleSheet >= 0) { this.setActiveSheet(visibleSheet); }

            // initialize A1/RC reference style (this also updates property `formulaGrammarUI`)
            const rcStyle = to.boolean(launchAttrs.rcStyle, this.globalConfig.rcStyle);
            this.propSet.set("rcStyle", rcStyle);
        });

        // forward all events of the sheet and name collection
        this.listenToAllEvents(this.sheetCollection, this.trigger);
        this.listenToAllEvents(this.nameCollection,  this.trigger);

        // set the column/row count, listen to document attribute changes
        const updateSheetSize = (): void => this.addressFactory.updateSheetSize(this.globalConfig.cols, this.globalConfig.rows);
        updateSheetSize();
        this.on("change:config", updateSheetSize);

        // activate another sheet, if the active sheet has been hidden
        this.on("change:sheetattrs", event => {
            const activeSheet = this.getActiveSheet();
            if ((event.sheet === activeSheet) && !event.sheetModel.isVisible()) {
                const visSheet = this.sheetCollection.findVisibleSheet(activeSheet);
                if (visSheet >= 0) { this.setActiveSheet(visSheet); }
            }
        });

        // send changed selection to remote clients after activating another sheet
        this.listenToProp(this.propSet, "activeSheet", () => this.#sendActiveSelectionDebounced());

        // update the UI formula grammar property for new A1/RC reference style
        this.listenToProp(this.propSet, "rcStyle", rcStyle => {
            const self = this as Writable<this>;
            self.formulaGrammarUI = this.getFormulaGrammarUI(rcStyle);
            this.trigger("change:locale");
        });

        // send changed selection in the active sheet to remote clients
        this.on("change:sheetprops", event => {
            if (("selection" in event.newProps) && (event.sheet === this.getActiveSheet())) {
                this.#sendActiveSelectionDebounced();
            }
        });

        // update formula resources and grammars when some locale data have changed
        this.listenTo(docApp.resourceManager, "change:locale", () => {
            const self = this as Writable<this>;
            this.#resourceFactory = docApp.resourceManager.formulaResourceFactory;
            self.formulaResource = this.#resourceFactory.getResource("std");
            self.formulaGrammarUI = this.getFormulaGrammarUI();
            this.trigger("change:locale");
        });

        // Notify listeners about changed text contents in drawing objects. For performance,
        // this document model will evaluate the operations array once, and will trigger the
        // events at the correct drawing collections containing the changed drawing objects.
        this.on("operations:success", operations => {

            // nothing to do without operations that modify drawing text contents
            if (!operations.some(operation => DRAWING_TEXT_OPERATIONS.has(operation.name))) { return; }

            // an array with sheet models that will be transformed in reverse order with sheet collection operations
            const sheetModels = Array.from<SheetModel | null>(this.yieldSheetModels());
            // the map with all changed drawing models with text contents
            const drawingModels = new Set<SheetDrawingModel>();

            // process the operations in reversed order to be able to reconstruct the sheet collection
            ary.forEach(operations, operation => {

                // collect the drawing models targeted by text content operations
                if (DRAWING_TEXT_OPERATIONS.has(operation.name)) {
                    // operation must contain a start position with at least 3 elements (drawing position and paragraph)
                    const { start } = operation as PositionOperation;
                    if (!is.array(start) || (start.length < 3)) { return; }
                    // the sheet model must exist, after all operations have been applied (no trailing "deleteSheet")
                    const sheetModel = sheetModels[start[0]];
                    if (!sheetModel) { return; }
                    // resolve the drawing model (position may target a child object in a group object)
                    const modelInfo = sheetModel.drawingCollection.getModelInfo(start.slice(1) as Position);
                    if (!modelInfo?.remaining) { return; }
                    // finally, the drawing model exists and contains text
                    drawingModels.add(modelInfo.model);
                    return;
                }

                // reconstruct the sheet model array to the state before applying sheet collection operations
                switch (operation.name) {
                    case Op.INSERT_SHEET:
                        // remove an inserted sheet model from the array
                        ary.deleteAt(sheetModels, (operation as Op.InsertSheetOperation).sheet);
                        break;
                    case Op.DELETE_SHEET:
                        // insert a gap for the deleted sheet (nothing to collect for this sheet)
                        ary.insertAt(sheetModels, (operation as Op.DeleteSheetOperation).sheet, null);
                        break;
                    case Op.MOVE_SHEET:
                        // move the sheet model back to its old position
                        ary.insertAt(sheetModels, (operation as Op.MoveSheetOperation).sheet, ary.deleteAt(sheetModels, (operation as Op.MoveSheetOperation).to)!);
                        break;
                    case Op.MOVE_SHEETS:
                        // move the sheet models back to their old position
                        ary.permute(sheetModels, inverseSortVector((operation as Op.MoveSheetsOperation).sheets));
                        break;
                    case Op.COPY_SHEET:
                        // remove the cloned sheet model from the array
                        ary.deleteAt(sheetModels, (operation as Op.CopySheetOperation).to);
                        break;
                }
            }, { reverse: true });

            // notify all changed drawing models
            for (const drawingModel of drawingModels) {
                drawingModel.parentCollection.trigger("change:drawing", drawingModel, "text");
            }
        });

        // register the mousedown handler on the page (required for text shapes)
        this.listenTo(this.getNode()[0], ["mousedown", "touchstart"], () => this.setActiveMouseDownEvent(true));
    }

    protected override destructor(): void {
        // bug 52790: explicitly disconnect the dependency manager before starting to destroy the document (performance/safety)
        this.dependencyManager.disconnect();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates an operation context (wrapper object for the JSON data).
     */
    override createOperationContext(operation: Operation, external: boolean, importing: boolean): SheetOperationContext {
        return new SheetOperationContext(this, operation, external, importing);
    }

    /**
     * Returns the current selection state of the document.
     *
     * @returns
     *  The current selection state of this document, if available.
     */
    override getSelectionState(): Opt<SelectionState> {
        const sheet = this.getActiveSheet();
        const sheetModel = this.getSheetModel(sheet);
        return sheetModel && { sheet, selection: sheetModel.getSelection() };
    }

    /**
     * Changes the selection state of the document.
     *
     * @param selectionState
     *  The new selection state to be set at this document.
     */
    override setSelectionState(selectionState: object): void {
        const sheet = pick.number(selectionState, "sheet");
        const selection = pick.prop(selectionState, "selection");
        const sheetModel = is.number(sheet) ? this.getSheetModel(sheet) : undefined;
        if (sheetModel && (selection instanceof SheetSelection)) {
            this.setActiveSheet(sheet!);
            sheetModel.setSelection(selection);
        } else {
            modelLogger.warn("$badge{SpreadsheetModel} setSelectionState: invalid selection data");
        }
    }

    /**
     * Returns all data needed to be passed as launch options to the new
     * document after it has been reloaded.
     *
     * @returns
     *  The launch options needed to be passed to the reloaded document.
     */
    createReloadSettings(): Dict {
        return {
            recalcFormulas: this.dependencyManager.needsCalcOnLoad(),
            docAttrs: this.propSet.all(),
            sheetAttrsArray: Array.from(this.yieldSheetModels(), sheetModel => sheetModel.getChangedViewAttributes())
        };
    }

    /**
     * Returns the formula grammar for the specified grammar identifier.
     *
     * @param grammarType
     *  The identifier of the formula grammar.
     *
     * @returns
     *  The formula grammar for the passed grammar identifier.
     */
    getFormulaGrammar(grammarType: GrammarType): FormulaGrammar {
        // grammar for type `UI` already cached (depends on view attribute "rcStyle")
        if (grammarType === "ui") { return this.formulaGrammarUI; }
        const config = FORMULA_GRAMMAR_CONFIGS[grammarType];
        const formulaResource = this.#resourceFactory.getResource(config.resource);
        return FormulaGrammar.create(formulaResource, config);
    }

    /**
     * Returns the UI formula grammar for a specific A1/RC reference style.
     *
     * @param rcStyle
     *  The reference style to be used by the returned formula grammar. The
     *  value `false` returns the grammar for A1 style (e.g. the reference
     *  "$B$3"); the value `true` returns the grammar for RC style (e.g. the
     *  reference "R3C2"). If omitted, uses the value of the runtime property
     *  "rcStyle" of this instance.
     *
     * @returns
     *  The UI formula grammar for the passed reference style.
     */
    getFormulaGrammarUI(rcStyle?: boolean): FormulaGrammar {
        rcStyle ??= this.propSet.get("rcStyle");
        return FormulaGrammar.create(this.formulaResource, { localized: true, rcStyle });
    }

    /**
     * Returns the formula parser for the specified grammar identifier.
     *
     * @param grammarType
     *  The identifier of the formula grammar.
     *
     * @returns
     *  The formula parser for the passed grammar identifier.
     */
    getFormulaParser(grammarType: GrammarType): FormulaParser {
        const formulaGrammar = this.getFormulaGrammar(grammarType);
        return FormulaParser.create(formulaGrammar);
    }

    // sheets -----------------------------------------------------------------

    /**
     * Returns the maximum length of a sheet name according to the current file
     * format.
     *
     * @returns
     *  The maximum length of a sheet name.
     */
    getMaxSheetNameLength(): number {
        return this.docApp.isOOXML() ? 31 : 65535;
    }

    /**
     * Returns the exact name of the sheet at the specified index. This method
     * is part of the interface `IDocumentAccess`.
     *
     * @param sheet
     *  The zero-based index of the sheet.
     *
     * @returns
     *  The name of the sheet at the passed index; or `null`, if the passed
     *  index is invalid.
     */
    getSheetName(sheet: number): string | null {
        return this.sheetCollection.getSheetName(sheet);
    }

    /**
     * Returns the current index of the sheet with the specified name. This
     * method is part of the interface `IDocumentAccess`.
     *
     * @param sheetName
     *  The case-insensitive sheet name.
     *
     * @returns
     *  The current zero-based index of the sheet with the specified name; or
     *  `-1`, if a sheet with the passed name does not exist.
     */
    getSheetIndex(sheetName: string): number {
        return this.sheetCollection.getSheetIndex(sheetName);
    }

    /**
     * Returns the model instance of the sheet at the specified index.
     *
     * @param sheet
     *  The zero-based index of the sheet.
     *
     * @returns
     *  The model instance for the sheet at the passed index; or `undefined`,
     *  if the passed index is invalid.
     */
    getSheetModel(sheet: number): Opt<SheetModel> {
        return this.sheetCollection.getSheetModel(sheet);
    }

    /**
     * Returns the number of sheets in the spreadsheet document.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The number of sheets in the document.
     */
    getSheetCount(options?: MatchSheetOptions): number {
        return this.sheetCollection.getSheetCount(options);
    }

    /**
     * Creates an iterator that visits the sheet models in this document.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The matching sheet models.
     */
    *yieldSheetModels(options?: SheetIteratorOptions): IterableIterator<SheetModel> {
        yield* this.sheetCollection.yieldSheetModels(options);
    }

    /**
     * Returns the zero-based index of the active (visible) sheet in this
     * document (the value of the view attribute "activeSheet").
     *
     * @returns
     *  The zero-based index of the active sheet.
     */
    getActiveSheet(): number {
        return this.propSet.get("activeSheet");
    }

    /**
     * Changes the active (visible) sheet in this document (the value of the
     * view attribute "activeSheet").
     *
     * @param sheet
     *  The zero-based index of the sheet to be activated.
     */
    setActiveSheet(sheet: number): void {
        modelLogger.takeTime(`$badge{SpreadsheetModel} activating sheet ${sheet}`, () => {
            this.propSet.set("activeSheet", sheet);
        });
    }

    /**
     * Returns whether this document contains a comment thread with unsaved
     * changes in any sheet.
     *
     * @returns
     *  Whether any comment thread contains unsaved changes.
     */
    override hasUnsavedComments(): boolean {
        return itr.some(this.yieldSheetModels(), sheetModel => sheetModel.hasUnsavedComment());
    }

    /**
     * Deletes unsaved changes in all comment threads in this document.
     */
    override deleteUnsavedComments(): void {
        for (const sheetModel of this.yieldSheetModels()) {
            sheetModel.deleteUnsavedComment();
        }
    }

    // defined names and table ranges -----------------------------------------

    /**
     * Returns whether the document contains the specified defined name.
     *
     * @param sheet
     *  The index of the sheet to look for a sheet-local name. If set to
     *  `null`, looks for a globally defined name.
     *
     * @param label
     *  The case-insensitive label of the defined name to look for.
     *
     * @returns
     *  Whether the document contains the specified defined name.
     */
    hasName(sheet: OptSheet, label: string): boolean {
        return !!this.getNameModel(sheet, label);
    }

    /**
     * Returns the model of the specified defined name.
     *
     * @param sheet
     *  The index of the sheet to look for a sheet-local name. If set to
     *  `null`, looks for a globally defined name.
     *
     * @param label
     *  The case-insensitive label of the defined name to look for.
     *
     * @returns
     *  The model of the defined name, if existing; otherwise `undefined`.
     */
    getNameModel(sheet: OptSheet, label: string): Opt<NameModel> {
        const parentModel = (sheet === null) ? this : this.getSheetModel(sheet);
        return parentModel?.nameCollection.getNameModel(label);
    }

    /**
     * Resolves data for the specified defined name. This method is part of the
     * interface `IDocumentAccess`.
     *
     * @param sheet
     *  The index of the sheet to look for a sheet-local name; or `null` to
     *  look for a globally defined name.
     *
     * @param label
     *  The case-insensitive label of the defined name to look for.
     *
     * @returns
     *  The reference to the defined name, if existing; otherwise `undefined`.
     */
    resolveName(sheet: OptSheet, label: string): Opt<DefNameRef> {
        const nameModel = this.getNameModel(sheet, label);
        return nameModel ? { model: nameModel, sheet, label: nameModel.label } : undefined;
    }

    /**
     * Returns the model of the defined name referred by the passed formula
     * token.
     *
     * @param nameToken
     *  The formula token referring to a defined name.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve sheet-local names
     *  without sheet reference. If set to `null`, and the name token does not
     *  contain its own sheet reference, this method looks for globally defined
     *  names only.
     *
     * @returns
     *  The model of an existing defined name; otherwise `undefined`.
     */
    resolveNameModel(nameToken: DefinedNameToken, refSheet: OptSheet): Opt<NameModel> {
        return nameToken.resolveName(refSheet)?.model;
    }

    /**
     * Creates an iterator that visits the models of all globally and
     * sheet-locally defined names in the document.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The models of all globally and sheet-locally defined names.
     */
    *yieldNameModels(options?: NameIteratorOptions): IterableIterator<NameModel> {

        // visit the global names
        yield* this.nameCollection.yieldNameModels(options);

        // visit all sheet-local names
        for (const sheetModel of this.yieldSheetModels({ supported: true })) {
            yield* sheetModel.nameCollection.yieldNameModels(options);
        }
    }

    /**
     * Returns whether the specified table range exists in any sheet of this
     * document.
     *
     * @param tableName
     *  The name of the table to be checked. MUST NOT be the empty string
     *  (addresses the anonymous table range used to store filter settings for
     *  the standard auto filter of each sheet).
     *
     * @returns
     *  Whether the specified table exists in the document.
     */
    hasTable(tableName: string): boolean {
        return !!this.sheetCollection.getTableModel(tableName);
    }

    /**
     * Returns the model of the table range with the specified name.
     *
     * @param tableName
     *  The name of the table. MUST NOT be the empty string (addresses the
     *  anonymous table range used to store filter settings for the standard
     *  autofilter of each sheet).
     *
     * @returns
     *  The model of the table with the specified name; or `undefined`, if no
     *  table exists with that name.
     */
    getTableModel(tableName: string): Opt<TableModel> {
        return this.sheetCollection.getTableModel(tableName);
    }

    /**
     * Resolves data for the specified table range. This method is part of the
     * interface `IDocumentAccess`.
     *
     * @param tableName
     *  The name of the table to be checked. MUST NOT be the empty string
     *  (addresses the anonymous table range used to store filter settings for
     *  the standard autofilter of each sheet).
     *
     * @returns
     *  The reference to the table range, if existing.
     */
    resolveTable(tableName: string): Opt<TableRef> {
        const tableModel = this.getTableModel(tableName);
        return tableModel ? makeTableRef(tableModel) : undefined;
    }

    /**
     * Resolves data for a table range at the specified position. This method
     * is part of the interface `IDocumentAccess`.
     *
     * @param sheet
     *  The index of the sheet expected to contain the table range.
     *
     * @param address
     *  The address of a cell in the specified sheet.
     *
     * @returns
     *  The reference to the table range, if existing.
     */
    resolveTableAt(sheet: number, address: Address): Opt<TableRef> {
        const tableModel = this.getSheetModel(sheet)?.tableCollection.getTableModelAt(address);
        return tableModel ? makeTableRef(tableModel) : undefined;
    }

    /**
     * Returns the model of the table range referred by the passed formula
     * token.
     *
     * @param tableToken
     *  The formula token referring to a table range.
     *
     * @returns
     *  The model of an existing defined name; otherwise `undefined`.
     */
    resolveTableModel(tableToken: TableToken): Opt<TableModel> {
        return tableToken.resolveTable()?.model;
    }

    /**
     * Creates an iterator that visits the models of all table ranges (except
     * the autofilters) in the document.
     *
     * @yields
     *  The table models of all sheets in the document except autofilters.
     */
    *yieldTableModels(): IterableIterator<TableModel> {
        yield* this.sheetCollection.yieldTableModels();
    }

    /**
     * Whether this model supports date groups in discrete table filters.
     */
    @onceMethod
    hasDateGroupFilterSupport(): boolean {
        return this.docApp.isOOXML() && (this.getFilterVersion() >= 2);
    }

    /**
     * Converts the passed cell value to a date for discrete table filters with
     * date groups.
     *
     * @param value
     *  The cell value to be converted to a date.
     *
     * @param format
     *  The parsed number format of the cell.
     *
     * @returns
     *  The date represented by the passed cell value, if it is a number that
     *  can be converted to a date (according to null date of the document
     *  etc.), *and* if the passed number format is a date or date/time format
     *  (but not time-only formats), *and* if date groups are supported by the
     *  current file format and middleware; otherwise `null`.
     */
    getDateForDateGroupFilter(value: ScalarType, format: ParsedFormat): Date | null {
        return (this.hasDateGroupFilterSupport() && is.number(value) && format.isAnyDate()) ? this.numberFormatter.convertNumberToDate(value) : null;
    }

    // range contents ---------------------------------------------------------

    /**
     * Returns the contents of all cells in the passed cell ranges.
     *
     * @param ranges
     *  An array of range addresses, or a single cell range address, with sheet
     *  indexes, whose cell contents will be returned. Each range MUST refer to
     *  a single sheet only (no sheet intervals!), but different ranges in an
     *  array may refer to different sheets.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The contents of the cells in the passed ranges. The result will be an
     *  array of cell content objects, with the additional property "count"
     *  representing the total number of cells contained in the result (this
     *  value will be different to the length of the array in compressed mode).
     *  See method `CellCollection#getRangeContents` for details.
     */
    getRangeContents(ranges: Range3DSource, options?: RangeContentOptions): CellContentsArray {

        // the maximum number of cells in the result
        const maxCount = options?.maxCount;
        // the result array returned by this method
        let contents: CellContents[] = [];
        // the number of cells already inserted into the result (differs to array length in compressed mode)
        let count = 0;

        // check sheet indexes contained in the ranges
        ranges = Range3DArray.filter(ranges, range => {
            if (this.sheetCollection.getSheetModel(range.sheet1) && range.singleSheet()) { return true; }
            modelLogger.warn("$badge{SpreadsheetModel} getRangeContents: invalid sheet index in range address");
            return false;
        });

        // create a clone of the options to be able to decrease the maximum count per cell range
        options = { ...options };

        // collect cell contents of all ranges from the respective cell collections
        for (const range3d of ranges) {

            // the cell collection to fetch the cell data from
            const { cellCollection } = this.getSheetModel(range3d.sheet1)!;

            // 3D ranges must be converted to simple 2D ranges without sheet indexes, otherwise cell collection
            // uses the wrong instance methods of class Range3D (no better solution for that in JavaScript...)
            const range = range3d.toRange();

            // fetch and concatenate the new cell data
            const newContents = cellCollection.getRangeContents(range, options);
            contents = contents.concat(newContents);
            count += newContents.count;

            // early exit, if the specified limit has been reached; otherwise decrease the maximum
            // count in the options for the next iteration
            if (maxCount === count) { break; }
            if (maxCount) { options.maxCount! -= newContents.count; }
        }

        // add total count as new property
        (contents as CellContentsArray).count = count;
        return contents as CellContentsArray;
    }

    // operation generators ---------------------------------------------------

    /**
     * Creates a new operation generator for spreadsheet operations and undo
     * operations, invokes the callback function, applies all operations
     * contained in the generator, sends them to the server, and creates an
     * undo action with the undo operations that have been generated by the
     * callback function.
     *
     * @param callback
     *  The callback function to be invoked. May return a promise to defer
     *  applying and sending the operations until the promise will fulfil.
     *
     * @param [options]
     *  Optional parameters. The option "generator" will be set internally to
     *  an instance of Spreadsheet's own generator class
     *  `SheetOperationGenerator`.
     *
     * @returns
     *  A promise that will fulfil after the operations have been applied and
     *  sent successfully (with the result of the callback function); or
     *  rejected, if the callback has returned a rejected promise (with the
     *  result of that promise), or if applying the operations has failed (with
     *  the object `{cause:"operation"}`).
     */
    @modelLogger.profileMethod("$badge{SpreadsheetModel} createAndApplySheetOperations")
    createAndApplySheetOperations<T>(callback: (this: this, generator: SheetOperationGenerator) => MaybeAsync<T>, options?: BuildOperationsOptions): JPromise<T> {

        // It is required to stop the text framework from fiddling around with the browser selection
        // when immediately applying spreadsheet operations.
        //
        // When applying undo/redo operations, the event handlers for "undo:after" and "redo:after"
        // of the text framework want to set the browser selection. This can be suppressed by
        // passing the option preventSelectionChange:true to the generated undo/redo operations.
        //
        // When creating empty paragraphs in a new shape drawing object, text frameworks wants to
        // update the paragraph formatting and set the browser selection afterwards. This can be
        // prevented by using the "BlockKeyboardEvent" mode of the text framework.

        // the effective options passed to the method of the base class EditModel
        const coreOptions: CreateAndApplyOperationsOptions<SheetOperationGenerator> = {
            preventSelectionChange: true, // suppress browser selection in text framework
            ...options,
            generator: new SheetOperationGenerator(this, options)
        };

        // create a document snapshot for undo manager in concurrent editing mode
        if (this.docApp.isOTEnabled()) {
            coreOptions.afterApplyHandler = generator => {
                generator.setUserData(DocumentSnapshot.fromModel(this), { undo: true });
            };
        }

        // suppress browser selection in text framework
        this.setBlockKeyboardEvent(true);

        // create and apply the operations by invoking the callback function with additional
        // preparation and post-processing specific for spreadsheet documents
        const promise = this.createAndApplyOperations(generator => {

            // store the current document snapshot for OT before doing any changes
            if (this.docApp.isOTEnabled()) {
                generator.setUserData(DocumentSnapshot.fromModel(this));
            }

            // invoke the generator callback function
            return callback.call(this, generator);
        }, coreOptions);

        // DOCS-2043: final cleanup after operations processing is completely done
        this.onSettled(promise, () => this.setBlockKeyboardEvent(false));

        return promise;
    }

    /**
     * Creates a new operation builder for spreadsheet operations and undo
     * operations, invokes the callback function, applies all operations
     * collected in the builder, sends them to the server, and creates an undo
     * action with the undo operations that have been collected in the builder.
     *
     * @param callback
     *  The callback function to be invoked. May return a promise to defer
     *  applying and sending the operations until the promise will fulfil.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil after the operations have been applied and
     *  sent successfully; or reject, if the callback has thrown an exception,
     *  or returned a rejected promise, or if applying the operations has
     *  failed.
     */
    buildOperations(callback: (this: this, generator: OperationBuilder) => MaybeAsync, options?: BuildOperationsOptions): JPromise<OperationBuilderFlushResult> {

        // It is required to stop the text framework from fiddling around with the browser selection when applying
        // spreadsheet operations.
        //
        // When applying undo/redo operations, the event handlers for "undo:after" and "redo:after" of the text
        // framework want to set the browser selection. This can be suppressed by setting the option
        // `preventSelectionChange` for the generated undo/redo operations.
        //
        // When creating empty paragraphs in a new shape drawing object, text frameworks wants to update the
        // paragraph formatting and set the browser selection afterwards. This can be prevented by using the
        // "BlockKeyboardEvent" mode of the text framework.

        // create the operation builder
        const builder = new OperationBuilder(this);
        OperationError.assertCause(!options?.applyImmediately, "operation", 'unexpected "applyImmediately" mode');

        // the effective options passed to the method of the base class `EditModel`
        const coreOptions: CreateAndApplyOperationsOptions<SheetOperationGenerator> = {
            preventSelectionChange: true, // suppress browser selection in text framework
            ...options,
            generator: new SheetOperationGenerator(this, options)
        };

        // create a document snapshot for undo manager in concurrent editing mode
        if (this.docApp.isOTEnabled()) {
            coreOptions.afterApplyHandler = generator => {
                generator.setUserData(DocumentSnapshot.fromModel(this), { undo: true });
            };
        }

        // suppress browser selection in text framework
        this.setBlockKeyboardEvent(true);

        // create and apply the operations by invoking the callback function with additional
        // preparation and post-processing specific for spreadsheet documents
        const promise = this.createAndApplyOperations(generator => {

            // store the current document snapshot for OT before doing any changes
            if (this.docApp.isOTEnabled()) {
                generator.setUserData(DocumentSnapshot.fromModel(this));
            }

            // invoke the generator callback function
            return jpromise.fastChain(
                () => callback.call(this, builder),
                () => builder.flushOperations(generator)
            );
        }, coreOptions);

        // DOCS-2043: final cleanup after operations processing is completely done
        this.onSettled(promise, () => {
            builder.destroy();
            this.setBlockKeyboardEvent(false);
        });

        return promise;
    }

    /**
     * Generates the operations and undo operations to update or restore the
     * formulas of all cells, defined names, data validation rules, formatting
     * rules, and drawing objects in this document.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{SpreadsheetModel} generateUpdateTaskOperations")
    generateUpdateTaskOperations(builder: OperationBuilder, updateTask: FormulaUpdateTask): JPromise {
        return jpromise.fastChain(
            // update the globally defined names
            () => this.nameCollection.generateUpdateTaskOperations(builder.headGenerator, updateTask),
            // update the formula expressions for all sheets in the document
            () => this.sheetCollection.generateUpdateTaskOperations(builder, updateTask)
        );
    }

    /**
     * Returns whether the document contains local pending changes that need to
     * be sent back to the server via specific document operations.
     *
     * @returns
     *  Whether the document contains pending changes that need to be sent back
     *  to the server.
     */
    hasUnsavedPendingChanges(): boolean {

        // immediately return `true` if the "calcOnLoad" flag has changed
        if (is.boolean(this.#getChangedCalcOnLoad())) { return true; }

        // detect if a sheet contains "important" changed view attributes
        const sheetPool = this.sheetAttrPool.getFamilyPool("sheet");
        for (const sheetModel of this.yieldSheetModels()) {
            const sheetAttrs = sheetModel.getChangedViewAttributes();
            for (const { entry } of sheetPool.yieldAttrs(sheetAttrs)) {
                if (entry.config.sendAlways) { return true; }
            }
        }

        return false;
    }

    /**
     * Generates the JSON document operations needed to update all view
     * attributes that have been changed locally, amongst other pending
     * settings to be saved.
     *
     * @param generator
     *  The operation generator to be filled with the operations.
     */
    generatePendingChangesOperations(generator: SheetOperationGenerator): void {

        // the global document properties to be changed
        const docConfig = dict.create();

        // add the changed "calcOnLoad" flag
        const calcOnLoad = this.#getChangedCalcOnLoad();
        if (is.boolean(calcOnLoad)) {
            docConfig.calcOnLoad = calcOnLoad;
        }

        // add the changed active sheet index
        const activeSheet = this.getActiveSheet();
        if (activeSheet !== this.globalConfig.activeSheet) {
            docConfig.activeSheet = activeSheet;
        }

        // create the document operation
        if (!is.empty(docConfig)) {
            generator.generateChangeConfigOperation(docConfig);
        }

        // create operations for the changed view attributes of all sheets
        for (const sheetModel of this.yieldSheetModels()) {
            const sheetAttrs = sheetModel.getChangedViewAttributes();
            if (!is.empty(sheetAttrs)) {
                generator.sheet = sheetModel.getIndex();
                sheetModel.generateChangeOperations(generator, { sheet: sheetAttrs });
            }
        }
    }

    // internal public methods ------------------------------------------------

    /**
     * Post-processing of the document, after all import operations have been
     * applied successfully.
     *
     * _Attention:_ Called from the application import process. MUST NOT be
     * called from external code.
     *
     * @internal
     *
     * @returns
     *  A promise that will fulfil when the document has been postprocessed
     *  successfully; or reject when the document is invalid, or any other
     *  error has occurred.
     */
    async postProcessImport(): Promise<void> {

        // document must contain at least one sheet (returning a rejected Deferred
        // object will cause invocation of the prepareInvalidDocument() method)
        if (this.sheetCollection.getSheetCount() === 0) {
            throw new Error("missing sheets");
        }

        // create all built-in cell styles not imported from the file
        this.cellStyles.createMissingStyles();

        // bug 46465: parse all cell formulas in a timer loop to prevent script warnings
        await this.asyncForEach(this.sheetCollection.yieldSheetModels(), async sheetModel => {
            await sheetModel.postProcessImport();
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the "calcOnLoad" flag to be saved if it has changed.
     *
     * @returns
     *  The "calcOnLoad" flag as boolean value, if it has changed and needs to
     * be saved, otherwise `null`.
     */
    #getChangedCalcOnLoad(): boolean | null {

        // DOCS-3161: in ODF, the "calcOnLoad" flag is a GUI feature and must not be changed
        if (this.docApp.isODF()) { return null; }

        // compare imported "calcOnLoad" flag with current state from dependency manager
        const oldCalcOnLoad = this.globalConfig.calcOnLoad;
        const newCalcOnLoad = this.dependencyManager.needsCalcOnLoad();
        return (oldCalcOnLoad === newCalcOnLoad) ? null : newCalcOnLoad;
    }

    /**
     * Sends the own selection settings to remote clients.
     */
    @debounceMethod({ delay: 250 })
    #sendActiveSelectionDebounced(): void {

        // do not send anything while settings are invalid
        const selectionState = this.getSelectionState();
        if (!selectionState) { return; }

        // convert selected ranges to strings
        this.docApp.updateUserData({
            sheet: selectionState.sheet,
            ranges: selectionState.selection.ranges.toOpStr(),
            drawings: selectionState.selection.drawings
        });
    }

    /**
     * Registers an operation handler that targets a specific sheet model. The
     * operation must contain a property "sheet" with the index of an existing
     * sheet.
     *
     * @param name
     *  The name of a document operation to be handled by a sheet model.
     *
     * @param handler
     *  The callback handler for the sheet operation.
     */
    #registerSheetOperationHandler(
        name: string,
        handler: (this: this, context: SheetOperationContext, sheetModel: SheetModel) => void
    ): void {
        this.registerOperationHandler(name, (context: SheetOperationContext) => {
            const sheetModel = this.getSheetModel(context.getInt("sheet"));
            context.ensure(sheetModel, "invalid sheet index");
            handler.call(this, context, sheetModel);
        });
    }

    /**
     * Registers an operation handler that targets a specific collection for
     * defined names. The operation may contain a property "sheet" with the
     * index of an existing sheet to address sheet-local names. Without this
     * property, the operation will address a global name.
     *
     * @param name
     *  The name of a document operation to be handled by a name collection.
     *
     * @param handler
     *  The callback handler for the name operation.
     */
    #registerNameOperationHandler(
        name: string,
        handler: (this: this, context: SheetOperationContext, nameCollection: NameCollection) => void
    ): void {
        this.registerOperationHandler(name, (context: SheetOperationContext) => {
            const sheet = context.has("sheet") ? context.getInt("sheet") : undefined;
            const targetModel = is.number(sheet) ? this.getSheetModel(sheet) : this;
            context.ensure(targetModel, "invalid sheet index");
            handler.call(this, context, targetModel.nameCollection);
        });
    }

    /**
     * Registers a callback function for the specified document operation
     * addressing a drawing model in a specific sheet of this spreadsheet
     * document. The operation MUST contain a property "start" (non-empty array
     * of integers) with the position of the drawing object in the document.
     * The position may point to a non-existing drawing (e.g. to insert a new
     * drawing), but it must point to an existing sheet (the first array
     * element is the sheet index).
     *
     * @param name
     *  The name of a document operation for drawing objects to be handled by
     *  the passed callback function.
     *
     * @param handler
     *  The callback function that will be invoked for every operation with the
     *  specified name, and a property "start" referring to an existing sheet.
     *  Receives the drawing collection of the sheet targeted by the operation,
     *  and the position of the drawing object (the value of the operation
     *  property "start" without the first array element which was the sheet
     *  index).
     */
    #registerDrawingOperationHandler(
        name: string,
        handler: (this: this, context: SheetOperationContext, drawingCollection: SheetDrawingCollection, position: Position) => void
    ): void {
        this.registerOperationHandler(name, (context: SheetOperationContext) => {

            // the entire start position of the drawing object, and its (optional) embedded target
            const position = context.getPos("start");
            context.ensure(position.length >= 2, "invalid drawing start position");

            // resolve the sheet model (first array element of the position)
            const sheetModel = this.getSheetModel(position[0]);
            context.ensure(sheetModel, "invalid sheet index");

            // invoke the operation handler with shortened start position (no leading sheet index)
            handler.call(this, context, sheetModel.drawingCollection, position.slice(1) as Position);
        });
    }

    /**
     * Registers a callback function for the specified document operation
     * addressing a drawing model in a specific sheet of this spreadsheet
     * document.
     *
     * @param name
     *  The name of a document operation for drawing objects to be handled by
     *  the passed callback function.
     *
     * @param handler
     *  The callback function that will be invoked for every operation with the
     *  specified name, and a property "start" referring to an existing sheet.
     *  Receives the drawing model targeted by the operation.
     */
    #registerDrawingModelOperationHandler(
        name: string,
        handler: (this: this, context: SheetOperationContext, drawingModel: SheetDrawingModel) => void
    ): void {
        this.#registerDrawingOperationHandler(name, (context: SheetOperationContext, collection, position) => {

            // find the drawing model in the drawing collection
            const modelInfo = collection.getModelInfo(position);
            context.ensure(modelInfo, "invalid drawing position");

            // check that the operation does not address embedded text contents
            context.ensure(!modelInfo.remaining, "invalid drawing start position");

            // call the operation handler (instance method of the drawing model)
            handler.call(this, context, modelInfo.model);
        });
    }

    /**
     * Registers a callback function for the specified document operation
     * addressing a chart model in a specific sheet of this spreadsheet
     * document.
     *
     * @param name
     *  The name of a document operation for charts to be handled by the passed
     *  callback function.
     *
     * @param handler
     *  The callback function that will be invoked for every operation with the
     *  specified name, and a property "start" referring to an existing sheet.
     *  Receives the chart model targeted by the operation.
     */
    #registerChartModelOperationHandler(
        name: string,
        handler: (this: this, context: SheetOperationContext, chartModel: SheetChartModel) => void
    ): void {
        this.#registerDrawingModelOperationHandler(name, (context: SheetOperationContext, drawingModel) => {
            context.ensure(drawingModel instanceof SheetChartModel, "invalid drawing type");
            handler.call(this, context, drawingModel);
        });
    }
}
