/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, map } from "@/io.ox/office/tk/algorithms";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";
import { type Direction, type MergeMode, FindMatchType, getAdjacentRange, isVerticalDir, isLeadingDir } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { consumeArraySource } from "@/io.ox/office/spreadsheet/utils/arraybase";

import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { CellType } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { MoveCellsEvent } from "@/io.ox/office/spreadsheet/model/cellcollection";

// types ======================================================================

/**
 * Event object emitted by an instance of `HlinkCollection`.
 */
export interface HlinkEvent extends SheetEventBase {

    /**
     * The addresses of the cell ranges with a hyperlink.
     */
    ranges: RangeArray;
}

export interface InsertHlinkEvent extends HlinkEvent {

    /**
     * The URL of the hyperlink.
     */
    url: string;
}

/**
 * Type mapping for the events emitted by `HlinkCollection` instances.
 */
export interface HlinkCollectionEventMap {

    /**
     * Will be emitted after new cell ranges with a hyperlink have been
     * inserted into a hyperlink collection.
     *
     * @param event
     *  Event object with the model of the new validation rule.
     */
    "insert:hlink": [event: InsertHlinkEvent];

    /**
     * Will be emitted before cell ranges with hyperlink will be removed from a
     * hyperlink collection.
     *
     * @param event
     *  Event object with the model of the new validation rule.
     */
    "delete:hlink": [event: HlinkEvent];
}

// private types --------------------------------------------------------------

interface LinkRange extends Range {
    url: string;
}

// private functions ==========================================================

function toLinkRange(range: Range, url: string): LinkRange {
    (range as LinkRange).url = url;
    return range as LinkRange;
}

/**
 * Transforms the specified hyperlink range.
 *
 * @param transformer
 *  An address transformer that specifies how to transform the passed
 *  hyperlink range.
 *
 * @param range
 *  The hyperlink range to be transformed.
 *
 * @param [reverse=false]
 *  If set to true, the opposite move operation will be used to transform the
 *  hyperlink range.
 *
 * @returns
 *  The transformed hyperlink range if available, otherwise `undefined`.
 */
function transformLinkRange(transformer: AddressTransformer, range: LinkRange, reverse = false): Opt<LinkRange> {
    // transform the passed range without expanding the end of the range
    const newRange = transformer.transformRanges(range, { reverse }).first();
    return newRange ? toLinkRange(newRange, range.url) : undefined;
}

// class HlinkCollection ======================================================

/**
 * Collects information about all hyperlinks of a single sheet in a spreadsheet
 * document.
 */

export class HlinkCollection extends SheetChildModel<HlinkCollectionEventMap> {

    // all hyperlink ranges (may overlap), as Range objects with additional "url" properties
    #linkRangeSet = new RangeSet<LinkRange>();

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this collection.
     */
    constructor(sheetModel: SheetModel) {

        // base constructor
        super(sheetModel);

        // update hyperlinks after moving cells, or inserting/deleting columns or rows
        this.listenTo(sheetModel.cellCollection, "move:cells", this.#moveCellsHandler);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed range overlaps with any hyperlink range.
     *
     * @param range
     *  The address of the cell range to checked.
     *
     * @param [matchType]
     *  Specifies which hyperlink ranges from this collection will match.
     *
     * @returns
     *  Wether the passed range overlaps with any hyperlink range.
     */
    coversAnyLinkRange(range: Range, matchType?: FindMatchType): boolean {
        return !!this.#linkRangeSet.findOne(range, matchType);
    }

    /**
     * Returns all hyperlink ranges that overlap with the passed range.
     *
     * @param range
     *  The address of the cell range to checked.
     *
     * @param [matchType]
     *  Specifies which hyperlink ranges from this collection will match.
     *
     * @returns
     *  All hyperlink ranges that overlap with the passed cell range. Each
     *  array element contains the additional property "url".
     */
    getLinkRanges(range: Range, matchType?: FindMatchType): RangeArray {
        // `Array.from` is not polymorphic but needs explicit downcast
        return RangeArray.from(this.#linkRangeSet.yieldMatching(range, matchType)) as unknown as RangeArray;
    }

    /**
     * Returns the URL of a hyperlink range covering the specified address.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  The URL of a hyperlink attached to the specified cell; or `null`, if
     *  the cell does not contain a hyperlink.
     */
    getCellURL(address: Address): string | null {
        return this.#linkRangeSet.findOneByAddress(address)?.url ?? null;
    }

    /**
     * Returns the addresses of all ranges containing a specific URL.
     *
     * @param url
     *  The URL to be looked up
     *
     * @returns
     *  The addresses of all ranges containing the specified URL.
     */
    getRangesForURL(url: string): RangeArray {
        const linkRanges = new RangeArray();
        for (const linkRange of this.#linkRangeSet) {
            if (linkRange.url === url) {
                linkRanges.append(linkRange);
            }
        }
        return linkRanges;
    }

    // generator methods ------------------------------------------------------

    /**
     * Generates the operations and undo operations to insert or delete
     * hyperlinks for the specified cell ranges.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address to
     *  generate the operations for.
     *
     * @param url
     *  The URL of the hyperlink to be attached to the cell range. If set to
     *  the empty string, existing hyperlinks will be removed instead.
     */
    generateHlinkOperations(generator: SheetOperationGenerator, ranges: RangeSource, url: string): void {

        // whether to generate "deleteHyperlink" operations
        const deleteHyperlinks = url.length === 0;

        // remove ranges that are completely covered by other ranges
        const targetRanges = RangeArray.cast(ranges).filterCovered();

        // search matching hyperlink range for deleting single cell
        if (deleteHyperlinks) {
            for (const [index, targetRange] of targetRanges.entries()) {
                const newTargetRange = targetRange.single() ? this.#linkRangeSet.findOne(targetRange) : undefined;
                if (newTargetRange) { targetRanges[index] = newTargetRange; }
            }
        }

        // undo: delete the new hyperlink ranges before restoring the old ranges
        if (!deleteHyperlinks) {
            generator.generateHlinkOperation(targetRanges, null, { undo: true });
        }

        // delete the covered parts of all affected hyperlink ranges
        this.#generateReduceLinkRangesOperations(generator, targetRanges);

        // create the new hyperlink range
        if (!deleteHyperlinks) {
            generator.generateHlinkOperation(targetRanges, url);
        }
    }

    /**
     * Generates the operations, and the undo operations, to repeatedly copy
     * the hyperlink ranges from the source range into the specified direction.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param range
     *  The address of the source cell range to be copied.
     *
     * @param direction
     *  The direction in which the specified cell range will be expanded.
     *
     * @param count
     *  The number of columns/rows to extend the cell range into the specified
     *  direction.
     */
    generateAutoFillOperations(generator: SheetOperationGenerator, range: Range, direction: Direction, count: number): void {

        // the target range to be filled
        const targetRange = getAdjacentRange(range, direction, count);
        // get source hyperlink ranges (reduced to the source range)
        const linkRanges = RangeArray.from(this.#linkRangeSet.yieldMatching(range), linkRange => toLinkRange(linkRange.intersect(range)!, linkRange.url));

        // first step for undo: delete the hyperlink ranges generated by auto-fill
        if (linkRanges.length) {
            generator.generateHlinkOperation(targetRange, null, { undo: true });
        }

        // delete the covered parts of all existing hyperlink ranges in the target range
        this.#generateReduceLinkRangesOperations(generator, targetRange);

        // nothing more to do, if no hyperlink ranges exist in the source range
        if (!linkRanges.length) { return; }

        // number of repetitions
        const columns = !isVerticalDir(direction);
        const size = range.size(columns);
        const cycles = Math.ceil(count / size);

        // create all hyperlink ranges for the target range
        const newLinkRangesMap = new Map<string, RangeArray>();
        const sign = isLeadingDir(direction) ? -1 : 1;
        for (let cycle = 1; cycle <= cycles; cycle += 1) {
            const diff = sign * cycle * size;
            for (const linkRange of linkRanges) {
                const newLinkRange = linkRange.clone().moveBoth(diff, columns).intersect(targetRange);
                if (newLinkRange) {
                    map.upsert(newLinkRangesMap, linkRange.url, () => new RangeArray()).push(newLinkRange);
                }
            }
        }

        // generate the hyperlink operations
        for (const [url, ranges] of newLinkRangesMap) {
            generator.generateHlinkOperation(ranges, url);
        }
    }

    /**
     * Generates the undo operations needed to restore the hyperlink in this
     * collection that would not be restored automatically with the reverse
     * operation of the passed update task.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     */
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): void {

        // only process move operations in the own sheet
        if (!this.sheetModel.isOwnMoveTask(updateTask)) { return; }

        // collect all hyperlinks that cannot be restored by applying the reversed move operation
        const { transformer } = updateTask;
        const restoreRangeGroups = new Map<string, RangeArray>();
        for (const linkRange of this.#linkRangeSet) {

            // transform the hyperlinks back and forth to decide whether it can be restored implicitly
            const transformRange = transformLinkRange(transformer, linkRange);
            const restoredRange = transformRange ? transformLinkRange(transformer, transformRange, true) : undefined;

            // collect all ranges that cannot be restored implicitly
            if (!restoredRange?.equals(linkRange)) {
                map.upsert(restoreRangeGroups, linkRange.url, () => new RangeArray()).push(linkRange);
            }
        }

        // add all hyperlinks to the undo generator that need to be restored
        for (const [url, ranges] of restoreRangeGroups.entries()) {
            sheetCache.generateHlinkOperation(ranges, url, { undo: true });
        }
    }

    /**
     * Generates the operations and undo operations to merge cells which covers
     * or overlaps hyperlink ranges
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address to
     *  generate the operations for.
     *
     * @param _mergeMode
     *  The merge mode for the specified ranges.
     */
    generateMergeCellsOperations(generator: SheetOperationGenerator, ranges: RangeSource, _mergeMode: MergeMode): JPromise {

        // cell collection used to resolve value cells in to-be-merged ranges
        const { cellCollection } = this.sheetModel;

        // collect all hyperlink ranges to be created, grouped by URL
        const linkRangeGroups = ary.group(RangeArray.cast(ranges), range => {

            // the address to extract a hyperlink URL from (prefer value cell that will be moved to the top-left corner)
            const address = cellCollection.findFirstCell(range, { type: CellType.VALUE }) ?? range.a1;
            // use the first hyperlink covering the address
            const linkRange = this.#linkRangeSet.findOneByAddress(address);
            // empty string as URL deletes the hyperlinks
            return linkRange?.url ?? "";
        });

        // generate the hyperlink operations for all collected ranges
        return this.asyncForEach(linkRangeGroups, ([url, linkRanges]) => this.generateHlinkOperations(generator, linkRanges, url));
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * hyperlinks from the passed collection into this collection.
     *
     * @param _context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyCopySheetOperation(_context: SheetOperationContext, fromCollection: HlinkCollection): void {

        // clone the contents of the source collection
        for (const linkRange of fromCollection.#linkRangeSet) {
            this.#linkRangeSet.add(toLinkRange(linkRange.clone(), linkRange.url));
        }
    }

    /**
     * Callback handler for the document operation "insertHyperlink" that
     * attaches a URL to a cell range.
     *
     * @param context
     *  A wrapper representing the "insertHyperlink" document operation.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyInsertOperation(context: SheetOperationContext): void {

        // the new hyperlink ranges
        const ranges = context.getRangeList("ranges");

        // remove all hyperlink ranges completely covered by the new ranges
        for (const range of ranges) {
            this.#linkRangeSet.deleteMatching(range, FindMatchType.CONTAIN);
        }

        // insert the new hyperlink ranges
        const url = context.getStr("url");
        for (const range of ranges) {
            this.#linkRangeSet.add(toLinkRange(range, url));
        }

        // notify all change listeners
        this.trigger("insert:hlink", this.createSheetEvent({ ranges, url }, context));
    }

    /**
     * Callback handler for the document operation "deleteHyperlink" that
     * removes all covered hyperlink ranges.
     *
     * @param context
     *  A wrapper representing the "deleteHyperlink" document operation.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyDeleteOperation(context: SheetOperationContext): void {

        // the ranges to be cleared
        const ranges = context.getRangeList("ranges");

        // remove the hyperlink range overlapping with the passed ranges
        for (const range of ranges) {
            this.#linkRangeSet.deleteMatching(range);
        }

        // notify all change listeners
        this.trigger("delete:hlink", this.createSheetEvent({ ranges }, context));
    }

    // private methods --------------------------------------------------------

    /**
     * Generates the operations needed to remove parts of the hyperlink ranges.
     */
    #generateReduceLinkRangesOperations(generator: SheetOperationGenerator, removeRanges: RangeSource): void {

        // collect all affected hyperlink ranges before removing elements from the set
        const coveredRangeSet = new Set<LinkRange>();
        consumeArraySource(removeRanges, removeRange => {
            for (const linkRange of this.#linkRangeSet.yieldMatching(removeRange)) {
                coveredRangeSet.add(linkRange);
            }
        });

        // delete the existing hyperlink ranges covered by the passed ranges
        const coveredRanges = Array.from(coveredRangeSet);
        generator.generateHlinkOperation(coveredRanges, null);

        // undo: restore the hyperlink ranges (will remove the smaller range parts generated below automatically)
        const coveredRangeGroups = ary.group(coveredRanges, range => range.url);
        for (const [url, ranges] of coveredRangeGroups) {
            generator.generateHlinkOperation(ranges, url, { undo: true });
        }

        // collect the remaining ranges by their URL
        const remainRangeGroups = new Map<string, RangeArray>();
        for (const coveredRange of coveredRanges) {
            for (const remainRange of new RangeArray(coveredRange).difference(removeRanges)) {
                map.upsert(remainRangeGroups, coveredRange.url, () => new RangeArray()).push(remainRange);
            }
        }

        // regenerate remaining parts of hyperlink range
        for (const [url, ranges] of remainRangeGroups) {
            generator.generateHlinkOperation(ranges, url);
        }
    }

    /**
     * Recalculates the position of all hyperlink ranges, after cells have been
     * moved (including inserted/deleted columns or rows) in the sheet.
     */
    #moveCellsHandler({ transformer }: MoveCellsEvent): void {
        const transformFn = transformLinkRange.bind(null, transformer);
        this.#linkRangeSet = RangeSet.from(this.#linkRangeSet, transformFn);
    }
}
