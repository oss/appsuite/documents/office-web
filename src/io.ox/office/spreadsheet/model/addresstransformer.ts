/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ReverseOptions } from "@/io.ox/office/tk/algorithms";

import { type Direction, isVerticalDir, isLeadingDir } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { consumeArraySource } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { type IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { type CellAnchor, AnchorMode } from "@/io.ox/office/spreadsheet/utils/cellanchor";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";

import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";

const { min, max } = Math;

// types ======================================================================

/**
 * Special options on top of `ReverseOptions` for transforming cell addresses.
 *
 * If the option "reverse" is set to `true`, the passed cell address will be
 * transformed with the reversed operation. If the address transformer
 * represents an insert operation, the address will be transformed according to
 * a delete operation, and vice versa.
 */
export interface TransformAddressOptions extends ReverseOptions {

    /**
     * If set to `true`, a cell address will never be deleted but will be moved
     * according to the transformation. Insert operations will shift the
     * address to (but not off) the sheet limits, delete operations will move
     * the (deleted) address to the left border of the deleted interval.
     * Default value is `false`.
     */
    move?: boolean;
}

/**
 * Specifies how to transform index intervals and cell range addresses when
 * inserting or deleting intervals inside them.
 */
export enum TransformMode {

    /**
     * The size of the index intervals and cell ranges will be increased when
     * inserting an interval inside, and decreased when deleting intervals
     * inside, but will NOT increased when inserting intervals directly after.
     *
     * Example: Inserting columns `C:E` will transform interval `A:D` to `A:G`.
     * Deleting columns `C:E` will transform interval `A:D` to `A:B`.
     */
    RESIZE = 0,

    /**
     * Similar to `RESIZE`, but the end position of the index interval or cell
     * range will be expanded, if a new interval will be inserted directly
     * after the interval or range. Behaves equally to `RESIZE` when deleting
     * intervals.
     *
     * Example: Inserting columns `C:E` will transform interval `A:D` to `A:G`,
     * and interval `A:B` to `A:E` (directly after). Deleting columns `C:E`
     * will transform interval `A:D` to `A:B`.
     */
    EXPAND = 1,

    /**
     * Similar to `RESIZE`, but an index interval or cell range will be deleted
     * (instead of being shortened), if its end position would be shifted
     * beyond the maximum index. Behaves equally to `RESIZE` when deleting
     * intervals.
     *
     * Example: With sheet limit at column Z, inserting single column `C` will
     * transform interval `X:Y` to `Y:Z`, but will delete interval `Y:Z`
     * (instead of transforming it to interval `Z`) because it would be shifted
     * outside the sheet partially.
     */
    STRICT = 2,

    /**
     * Index intervals and cell ranges will be split into parts when inserting
     * intervals inside. Behaves equally to `RESIZE` when deleting intervals.
     *
     * Example: Inserting columns `C:E` will transform interval `A:D` to the
     * interval list `A:B F:G`. Deleting columns `C:E` will transform interval
     * `A:D` to `A:B`.
     */
    SPLIT = 3,

    /**
     * Index intervals and cell ranges will not be modified when inserting or
     * deleting intervals inside (fixed size). Only insertions and deletions
     * before an interval or cell range will cause to shift it.
     *
     * Example: Inserting columns `A:C` will transform interval `A:D` to `D:G`,
     * but inserting columns `C:E` will not transform interval `A:D`. Deleting
     * columns `B:C` will transform intervals `B:C`, `C:D`, and `D:E` to `B:C`
     * (intervals will be pulled to the beginning of the deleted interval, but
     * will not be shortened).
     */
    FIXED = 4
}

/**
 * Special options on top of `ReverseOptions` for transforming index intervals
 * and cell range addresses.
 *
 * If the option "reverse" is set to `true`, the interval will be transformed
 * with the reversed operation. If the address transformer represents an insert
 * operation, the interval will be transformed according to a delete operation,
 * and vice versa.
 */
export interface TransformIntervalOptions extends ReverseOptions {

    /**
     * Specifies how to transform index intervals or cell range addresses when
     * inserting or deleting intervals inside. Default value is `RESIZE`.
     */
    transformMode?: TransformMode;
}

/**
 * Special options on top of `TransformIntervalOptions` for transforming cell
 * range addresses.
 */
export interface TransformRangeOptions extends TransformIntervalOptions {

    /**
     * If set to `true`, cell range addresses partially covered by the band
     * interval of the address transformer will be split, the covered part will
     * be moved and transformed, while the uncovered part will be kept
     * unmodified. By default, partially covered cell range addresses will not
     * be transformed at all. Default value is `false`.
     */
    splitPartial?: boolean;
}

// private types --------------------------------------------------------------

// callback function to collect transformed intervals from private methods
type ResultFn = (first: number, last: number) => void;

// predicate function taking a single index interval
type IntervalPredicateFn = (targetInt: Interval) => boolean;

// shortcuts for transformation modes
const { RESIZE, EXPAND, STRICT, SPLIT, FIXED } = TransformMode;

// private functions ==========================================================

/**
 * Transforms a column/row index according to the move intervals in insertion
 * mode.
 *
 * @param moveIntervals
 *  The move intervals used to transform the index.
 *
 * @param xfIndex
 *  The column/row index to be transformed.
 *
 * @param maxIndex
 *  Maximum column/row index allowed in a sheet.
 *
 * @param [moveMode]
 *  Whether to keep the index valid (at the maximum index) when it would be
 *  moved off.
 *
 * @returns
 *  The transformed column/row index; or `undefined`, if the index has been
 *  moved beyond the maximum column/row index in the sheet (unless the
 *  `moveMode` flag has been passed).
 */
function transformIndexInsert(moveIntervals: IntervalArray, xfIndex: number, maxIndex: number, moveMode?: boolean): Opt<number> {

    // transform the column/row index as long as it remains valid
    for (const moveInterval of moveIntervals) {

        // early exit, if the target intervals are located behind the index (array is sorted)
        if (xfIndex < moveInterval.first) { break; }

        // shift index ahead
        xfIndex += moveInterval.size();

        // early exit if the index becomes invalid (moved beyond the maximum index)
        if (xfIndex > maxIndex) {
            return moveMode ? maxIndex : undefined;
        }
    }

    return xfIndex;
}

/**
 * Transforms a column/row index according to the move intervals in deletion
 * mode.
 *
 * @param moveIntervals
 *  The move intervals used to transform the index.
 *
 * @param xfIndex
 *  The column/row index to be transformed.
 *
 * @param _maxIndex
 *  Maximum column/row index allowed in a sheet.
 *
 * @param [moveMode]
 *  Whether to keep the index valid (at the interval start index) when it would
 *  be deleted by an interval.
 *
 * @returns
 *  The transformed column/row index; or `undefined`, if the index was part of
 *  a target interval and has therefore been deleted (unless the `moveMode`
 *  flag has been passed).
 */
function transformIndexDelete(moveIntervals: IntervalArray, xfIndex: number, _maxIndex: number, moveMode?: boolean): Opt<number> {

    // transform the column/row index as long as it remains valid (in reversed order!)
    for (let ai = moveIntervals.length - 1; ai >= 0; ai -= 1) {

        // nothing to do, if the target intervals are located behind the index
        const moveInterval = moveIntervals[ai];
        if (xfIndex < moveInterval.first) { continue; }

        // transform the index (special handling if it is contained in the target interval)
        if (xfIndex <= moveInterval.last) {
            if (!moveMode) { return undefined; }
            xfIndex = moveInterval.first;
        } else {
            xfIndex -= moveInterval.size();
        }
    }

    return xfIndex;
}

/**
 * Transforms an index interval according to the move intervals in insertion
 * mode.
 *
 * @param moveIntervals
 *  The move intervals used to transform the interval.
 *
 * @param xfInterval
 *  The index interval to be transformed.
 *
 * @param xfMode
 *  The resolved transformation mode.
 *
 * @param maxIndex
 *  Maximum column/row index allowed in a sheet.
 *
 * @param resultFn
 *  A callback function that will be invoked for each transformed index
 *  interval. May be invoked multiple times, e.g. for `SPLIT` mode.
 */
function transformIntervalInsert(moveIntervals: IntervalArray, xfInterval: Interval, xfMode: TransformMode, maxIndex: number, resultFn: ResultFn): void {

    // the interval indexes to be transformed
    let { first, last } = xfInterval;

    // special behavior for specific transformation modes
    const strictLimit = xfMode === STRICT;
    const splitMode = xfMode === SPLIT;

    // predicate function returning whether the interval will be transformed by a target interval
    // - `FIXED` mode: interval will be shifted if target interval starts before
    // - `EXPAND` mode: interval will be expanded if target interval starts directly after
    // - otherwise: interval will be expanded if target interval starts inside
    const isAffected: IntervalPredicateFn =
        (xfMode === FIXED) ? (targetInt => targetInt.first <= first) :
        (xfMode === EXPAND) ? (targetInt => targetInt.first <= last + 1) :
        (targetInt => targetInt.first <= last);

    // transform the interval as long as it remains valid
    for (const moveInterval of moveIntervals) {

        // early exit, if the remaining (sorted) target intervals will not transform the interval anymore
        if (!isAffected(moveInterval)) { break; }

        // split leading part from interval
        if (splitMode && (first < moveInterval.first)) {
            resultFn(first, moveInterval.first - 1);
            first = moveInterval.first;
        }

        // shift start position ahead, if the interval starts inside or behind the target interval;
        // exit the loop early if the start index becomes invalid (moved beyond the maximum index)
        const moveSize = moveInterval.size();
        if (moveInterval.first <= first) {
            first += moveSize;
            if (first > maxIndex) { return; }
        }

        // shift end position ahead (in `EXPAND` mode: also, if it ends exactly before the target
        // interval), keep the end position inside the limits of the sheet (interval may shrink;
        // but in `STRICT` mode, the interval will be dropped instead)
        last += moveSize;
        if (last > maxIndex) {
            if (strictLimit) { return; }
            last = maxIndex;
        }
    }

    // invoke the callback function with the new interval
    resultFn(first, last);
}

/**
 * Transforms an index interval according to the move intervals in deletion
 * mode.
 *
 * @param moveIntervals
 *  The move intervals used to transform the interval.
 *
 * @param xfInterval
 *  The index interval to be transformed.
 *
 * @param xfMode
 *  The resolved transformation mode.
 *
 * @param _maxIndex
 *  Maximum column/row index allowed in a sheet.
 *
 * @param resultFn
 *  A callback function that will be invoked for each transformed index
 *  interval. Will be invoked exactly once, if the interval has been
 *  transformed; or never, if the interval was covered by a target interval and
 *  has therefore been deleted completely.
 */
function transformIntervalDelete(moveIntervals: IntervalArray, xfInterval: Interval, xfMode: TransformMode, _maxIndex: number, resultFn: ResultFn): void {

    // the interval indexes to be transformed
    let { first, last } = xfInterval;

    // special behavior for specific transformation modes
    const fixedMode = xfMode === FIXED;

    // predicate function returning whether the interval will be transformed by a target interval
    // - `FIXED` mode: interval will be shifted if target interval starts before
    // - otherwise: interval will be transformed if target interval starts inside
    const isAffected: IntervalPredicateFn = fixedMode ?
        (targetInt => targetInt.first < first) :
        (targetInt => targetInt.first <= last);

    // transform the interval index as long as it remains valid (in reversed order!)
    for (let ai = moveIntervals.length - 1; ai >= 0; ai -= 1) {

        // nothing to do, if the target intervals are located behind the interval
        const moveInterval = moveIntervals[ai];
        if (!isAffected(moveInterval)) { continue; }

        // shift start position of the interval back to start of target interval
        const moveSize = moveInterval.size();
        if (moveInterval.first < first) {
            first = max(moveInterval.first, first - moveSize);
        }

        // shift end position of the interval back to start of target interval;
        // exit the loop early if the interval becomes invalid
        if (!fixedMode) {
            last = max(last - moveSize, moveInterval.first - 1);
            if (last < first) { return; }
        }
    }

    // invoke the callback function with the new interval
    resultFn(first, fixedMode ? (first + xfInterval.size() - 1) : last);
}

// class AddressTransformer ===================================================

/**
 * Builds the resulting shifted insertion or deletion intervals, and other
 * useful intervals, needed to generate document operations to insert or delete
 * cells in a worksheet, including inserting/deleting entire columns or rows.
 *
 * **Example 1 (Insertion Mode):**
 *
 * The columns `C:D` (two columns) and `F:I` (4 columns) will be passed to this
 * class in insertion mode (insert two columns between `B` and `C`, and four
 * columns between `E` and `F`). The sheet contains 256 columns (maximum column
 * `IV`).
 *
 * After applying the insert operations, columns `C:D` and `H:K` will be blank.
 * The latter interval `F:I` has been shifted by two columns to `H:K` due to
 * the preceding insertion interval `C:D`. The following intervals will be
 * generated and collected:
 *
 * - The unshifted insertion intervals to be inserted before applying the
 *   insert operations (property `sourceIntervals` with columns `C:D F:I`).
 *
 * - The shifted insertion intervals that will be blank after applying the
 *   insert operations (property `targetIntervals` with columns `C:D H:K`).
 *
 * - The original position of the column intervals that will be moved to the
 *   end of the sheet (property `moveFromIntervals` with columns `C:E F:IP`).
 *   The latter interval ends six columns before the end of the sheet (column
 *   `IV`) so that it will not be shifted outside the sheet.
 *
 * - The position of the column intervals after they have been moved to the end
 *   of the sheet (property `moveToIntervals` with columns `E:G L:IV`).
 *
 * - The new intervals that will be created in the sheet (property
 *  `insertIntervals` with columns `C:D H:K`). In insertion mode, these will
 *   always be equal to `targetIntervals`.
 *
 * - The deleted intervals that will be shifted outside the sheet (property
 *   `deleteIntervals` with columns `IQ:IV`).
 *
 * **Example 2 (Deletion Mode):**
 *
 * The columns `C:D` (two columns) and `F:I` (4 columns) will be passed to this
 * class in deletion mode (delete columns `B` and `C`, and columns `F` to `I`).
 * The sheet contains 256 columns (maximum column `IV`).
 *
 * After applying the delete operations, the specified columns have been
 * deleted, and the remaining entries have been shifted towards the beginning
 * of the sheet. The following intervals will be generated and collected:
 *
 * - The deletion intervals that will be deleted from the sheet (properties
 *   `sourceIntervals` and `targetIntervals` with columns `C:D F:I`).
 *
 * - The original position of the column intervals that will be moved to the
 *   beginning of the sheet (property `moveFromIntervals` with columns
 *   `E J:IV`).
 *
 * - The position of the column intervals after they have been moved to the
 *   beginning of the sheet (property `moveToIntervals` with columns `C D:IP`).
 *
 * - The new intervals that will be created at the end of the sheet (property
 *   `insertIntervals` with columns `IQ:IV`).
 *
 * - The intervals deleted from the sheet. In deletion mode, these will always
 *   be copies of the target intervals (property `deleteIntervals` with columns
 *   `C:D F:I`).
 *
 * @param addressFactory
 *  The address factory used to resolve the maximum column/row index.
 */
export class AddressTransformer {

    // static functions -------------------------------------------------------

    /**
     * Creates an address transformer for one or more index intervals (entire
     * columns or rows in the sheet, suitable e.g. for the document operations
     * "insertColumns", "deleteRows", etc.).
     *
     * @param addressFactory
     *  The address factory used to resolve the maximum column/row index in the
     *  document.
     *
     * @param moveIntervals
     *  The index intervals of the columns or rows to be inserted or deleted in
     *  the sheet.
     *
     * @param direction
     *  The direction to move existing columns/rows to.
     */
    static fromIntervals(addressFactory: AddressFactory, moveIntervals: IntervalSource, direction: Direction): AddressTransformer {
        const columns = !isVerticalDir(direction);
        return new AddressTransformer(addressFactory, addressFactory.getFullInterval(!columns), moveIntervals, direction);
    }

    /**
     * Creates an address transformer for a single cell range (suitable e.g.
     * for the document operation "moveCells").
     *
     * @param addressFactory
     *  The address factory used to resolve the maximum column/row index in the
     *  document.
     *
     * @param moveRange
     *  The address of the cell range to be inserted or deleted in the sheet.
     *  In insertion mode, this cell range will become empty, and the contents
     *  starting from its leading border will be shifted towards the end of the
     *  sheet. In deletion mode, this cell range will be deleted, and the
     *  contents starting outside its trailing border will be shifted towards
     *  its leading border.
     *
     * @param direction
     *  The direction to move existing cells to.
     */
    static fromRange(addressFactory: AddressFactory, moveRange: Range, direction: Direction): AddressTransformer {
        const columns = !isVerticalDir(direction);
        return new AddressTransformer(addressFactory, moveRange.interval(!columns), moveRange.interval(columns), direction);
    }

    // properties -------------------------------------------------------------

    /**
     * The index interval in the crossing direction that restricts the moved
     * interval, that can be used to convert the target intervals to cell range
     * addresses.
     */
    readonly bandInterval: Interval;

    /**
     * The sorted and shortened (NOT shifted, insertion mode), and merged
     * (deletion mode) move intervals, that represent the index intervals to be
     * inserted, or the still existing index intervals to be deleted.
     */
    readonly sourceIntervals: IntervalArray;

    /**
     * The sorted, shifted and shortened (insertion mode), and merged (deletion
     * mode) move intervals, that represent the new inserted index intervals,
     * or the still existing index intervals to be deleted.
     */
    readonly targetIntervals: IntervalArray;

    /**
     * The move intervals needed to exactly reverse the operation. In insertion
     * mode, this property equals `targetIntervals` needed to delete the
     * (already shifted) insertion intervals. In deletion mode, this property
     * contains the pack-pulled target intervals (to compensate that the target
     * intervals have been shifted forwards).
     */
    readonly reverseIntervals: IntervalArray;

    /**
     * The index intervals containing the source position of the cells before
     * they will be moved towards the end (insertion), or to the beginning
     * (deletion) of the sheet.
     */
    readonly moveFromIntervals = new IntervalArray();

    /**
     * The index intervals containing the final position of the cells after
     * they have been moved towards the end (insertion), or to the beginning
     * (deletion) of the sheet. The intervals in this array will have the same
     * size as the corresponding original intervals from `moveFromIntervals`.
     */
    readonly moveToIntervals = new IntervalArray();

    /**
     * The index intervals containing the position of all entries that will be
     * inserted: Either the target intervals themselves on insertion, or the
     * trailing entries that will be shifted inside the sheet on deletion.
     */
    readonly insertIntervals: IntervalArray;

    /**
     * The index intervals containing the position of all entries that will be
     * deleted: Either trailing entries that will be shifted outside the sheet
     * on insertion, or the target intervals themselves on deletion.
     */
    readonly deleteIntervals: IntervalArray;

    /**
     * The address of the cell range that will be modified by the move
     * operation represented by this transformer. Includes all cells that will
     * be moved, inserted, or deleted.
     */
    readonly dirtyRange: Range;

    /**
     * The maximum column/row index allowed in the specified move direction.
     */
    readonly maxIndex: number;

    /**
     * If `true`, the cells will be moved through columns (i.e. to the left or
     * right), otherwise through rows (i.e. up or down).
     */
    readonly columns: boolean;

    /**
     * If set to `true`, the cells will be moved to the end of the sheet (new
     * blank cells will be inserted), otherwise to the beginning of the sheet
     * (existing cells will be deleted).
     */
    readonly insert: boolean;

    // constructor ------------------------------------------------------------

    protected constructor(addressFactory: AddressFactory, bandInterval: Interval, moveIntervals: IntervalSource, direction: Direction) {

        const columns = !isVerticalDir(direction);
        const insert = !isLeadingDir(direction);
        const maxIndex = addressFactory.getMaxIndex(columns);

        this.bandInterval = bandInterval;
        this.maxIndex = maxIndex;
        this.columns = columns;
        this.insert = insert;

        // preparations for insertion or deletion mode
        if (insert) {

            // shift the following intervals to the end, according to the size of the preceding insertion intervals,
            // shorten or delete the trailing intervals that will be shifted outside the maximum column/row index
            const boundInterval = new Interval(0, maxIndex);
            let insertOffset = 0;
            this.sourceIntervals = new IntervalArray();
            this.targetIntervals = new IntervalArray();
            new IntervalArray(moveIntervals).sort().some(interval => {
                const targetInterval = interval.clone().move(insertOffset).intersect(boundInterval);
                if (!targetInterval) { return true; }
                insertOffset += interval.size();
                this.sourceIntervals.push(interval.clone());
                this.targetIntervals.push(targetInterval);
                return false;
            });

            // the reverse operation (for deletion) must operate on the shifted target intervals
            this.reverseIntervals = this.insertIntervals = this.targetIntervals;

            // the trailing indexes at the end of the sheet will be shifted outside (a single index interval)
            const deleteSize = this.targetIntervals.size();
            this.deleteIntervals = new IntervalArray(new Interval(maxIndex - deleteSize + 1, maxIndex));

        } else {

            // deletion mode: merge the intervals (the result array will be sorted)
            this.sourceIntervals = this.targetIntervals = this.deleteIntervals = IntervalArray.merge(moveIntervals);

            // the reverse intervals (for insertion) need to be pulled back
            let deleteOffset = 0;
            this.reverseIntervals = this.targetIntervals.clone(interval => {
                const undoInterval = interval.clone().move(deleteOffset);
                deleteOffset -= interval.size();
                return undoInterval;
            });

            // the trailing indexes at the end of the sheet will be shifted inside (a single index interval)
            const deleteSize = this.targetIntervals.size();
            this.insertIntervals = new IntervalArray(new Interval(maxIndex - deleteSize + 1, maxIndex));
        }

        // calculate the move intervals between the resulting target intervals
        const shiftedIntervals = insert ? this.moveToIntervals : this.moveFromIntervals;
        const originalIntervals = insert ? this.moveFromIntervals : this.moveToIntervals;
        let moveOffset = 0;
        this.targetIntervals.some((targetInterval, ai) => {

            // exit if the outer border of the sheet has been reached
            if (targetInterval.last >= maxIndex) { return true; }

            // the interval between the current and next target interval
            const nextInterval = this.targetIntervals[ai + 1];
            let moveInterval = new Interval(targetInterval.last + 1, nextInterval ? (nextInterval.first - 1) : maxIndex);
            // the interval is the position after moving (insertion mode); or before moving (deletion mode)
            shiftedIntervals.push(moveInterval);

            // move the interval back by the total size of preceding target intervals
            moveOffset -= targetInterval.size();
            moveInterval = moveInterval.clone().move(moveOffset);
            originalIntervals.push(moveInterval);
            return false;
        });

        // calculate the dirty range covered by this transformer
        this.dirtyRange = this.createBandRange(new Interval(this.targetIntervals.first()!.first, maxIndex));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed cell address is contained in the cell range
     * covered by the band interval of this address transformer.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether the passed cell address is contained in the cell range covered
     *  by the band interval of this address transformer.
     */
    bandContainsAddress(address: Address): boolean {
        return this.bandInterval.containsIndex(address.get(!this.columns));
    }

    /**
     * Returns whether the passed range is completely contained in the cell
     * range covered by the band interval of this address transformer.
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  Whether the passed range is completely contained in the cell range
     *  covered by the band interval of this address transformer.
     */
    bandContainsRange(range: Range): boolean {
        return this.bandInterval.contains(range.interval(!this.columns));
    }

    /**
     * Returns whether the passed range overlaps with the cell range covered by
     * the band interval of this address transformer.
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  Whether the passed range overlaps with the cell range covered by the
     *  band interval of this address transformer.
     */
    bandOverlapsRange(range: Range): boolean {
        return this.bandInterval.overlaps(range.interval(!this.columns));
    }

    /**
     * Creates a cell range address located in the band interval of this
     * address transformer, and covering a single column or row in that band.
     *
     * Example: If the address transformer represents the row band `1:3` (move
     * cells to the left or right inside these rows), this method will return
     * the cell range address `E1:E3` for index `4` (column `E`) passed to it.
     *
     * @param index
     *  The column/row index to be used to create the cell range address.
     *
     * @returns
     *  The cell range address located in the band interval of this address
     *  transformer, and covering the specified column/row index.
     */
    createBandLineRange(index: number): Range {
        return this.columns ? Range.fromRowInterval(this.bandInterval, index) : Range.fromColInterval(this.bandInterval, index);
    }

    /**
     * Creates a cell range address located in the band interval of this
     * address transformer, and covering the specified index interval in that
     * band.
     *
     * Example: If the address transformer represents the row band `1:3` (move
     * cells to the left or right inside these rows), this method will return
     * the cell range address `E1:F3` for the column interval `E:F` passed to
     * it.
     *
     * @param interval
     *  The interval inside the band covered by this address transformer.
     *
     * @returns
     *  The cell range address located in the band interval of this address
     *  transformer, and covering the specified index interval.
     */
    createBandRange(interval: Interval): Range {
        return this.columns ? Range.fromIntervals(interval, this.bandInterval) : Range.fromIntervals(this.bandInterval, interval);
    }

    /**
     * Creates an array of cell range addresses located in the band interval of
     * this address transformer, and covering the specified index intervals in
     * that band.
     *
     * @param intervals
     *  An array of intervals, or a single interval inside the band covered by
     *  this address transformer.
     *
     * @returns
     *  The cell range addresses located in the band interval of this address
     *  transformer, and covering the specified index intervals.
     */
    createBandRanges(intervals: IntervalSource): RangeArray {
        return RangeArray.map(intervals, interval => this.createBandRange(interval));
    }

    /**
     * Transforms the passed column/row index according to the settings of this
     * address transformer, but ignores its band interval.
     *
     * @param index
     *  The column/row index to be transformed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed index; or `undefined`, if the index will be deleted.
     */
    transformIndex(index: number, options: TransformAddressOptions & { move: true }): number;
    transformIndex(index: number, options?: TransformAddressOptions): Opt<number>;
    // implementation
    transformIndex(index: number, options?: TransformAddressOptions): Opt<number> {

        // the helper function that implements transformation of an index interval
        const transformFn = this.isInsertMode(options) ? transformIndexInsert : transformIndexDelete;

        // transform the index according to move direction
        return transformFn(this.targetIntervals, index, this.maxIndex, options?.move);
    }

    /**
     * Transforms the passed index intervals according to the settings of this
     * address transformer, but ignores its band interval.
     *
     * @param intervals
     *  The index intervals to be transformed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed index intervals. Untransformed index intervals will be
     *  cloned. Index intervals that would be deleted completely while deleting
     *  columns or rows, or would be shifted outside the sheet while inserting
     *  columns or rows, will not be included into the result. With the
     *  insertion mode `SPLIT`, the result array may contain multiple index
     *  intervals for a single source interval.
     */
    transformIntervals(intervals: Iterable<Interval>, options?: TransformIntervalOptions): IntervalArray {

        // shortcuts to own properties
        const { maxIndex } = this;

        // how to insert/delete intervals inside intervals
        const xfMode = options?.transformMode ?? RESIZE;
        // the helper function that implements transformation of an index interval
        const transformFn = this.isInsertMode(options) ? transformIntervalInsert : transformIntervalDelete;
        // the transformed result intervals
        const resultIntervals = new IntervalArray();

        // transform all intervals in the passed data source
        const appendFn: ResultFn = (first, last) => resultIntervals.push(new Interval(first, last));
        for (const interval of intervals) {
            transformFn(this.targetIntervals, interval, xfMode, maxIndex, appendFn);
        }

        return resultIntervals;
    }

    /**
     * Transforms the passed cell address according to the settings of this
     * address transformer.
     *
     * @param address
     *  The cell address to be transformed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed cell address. An untransformed cell address will be
     *  cloned. If the cell would be deleted completely while deleting columns
     *  or rows, or would be shifted outside the sheet while inserting columns
     *  or rows, `undefined` will be returned instead (except if `move` mode is
     *  passed in the options).
     */
    transformAddress(address: Address, options: TransformAddressOptions & { move: true }): Address;
    transformAddress(address: Address, options?: TransformAddressOptions): Opt<Address>;
    // implementation
    transformAddress(address: Address, options?: TransformAddressOptions): Opt<Address> {

        // nothing to do, if the address is not covered by this transformer
        if (!this.bandContainsAddress(address)) { return address.clone(); }

        // transform the correct index of the passed address, according to move direction
        const index = this.transformIndex(address.get(this.columns), options);
        // create a new address with adjusted column/row index, or return undefined to indicate deleted cell
        return (index === undefined) ? undefined : address.clone().set(index, this.columns);
    }

    /**
     * Transforms the passed cell addresses according to the settings of this
     * address transformer.
     *
     * @param addresses
     *  The cell addresses to be transformed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed cell addresses. Untransformed cell addresses will be
     *  cloned. Cells that would be deleted completely while deleting columns
     *  or rows, or would be shifted outside the sheet while inserting columns
     *  or rows, will not be included into the result.
     */
    transformAddresses(addresses: Iterable<Address>, options?: TransformAddressOptions): AddressArray {
        const transformed = new AddressArray();
        for (const address of addresses) {
            const result = this.transformAddress(address, options);
            if (result) { transformed.push(result); }
        }
        return transformed;
    }

    /**
     * Transforms the passed cell range addresses according to the settings of
     * this address transformer.
     *
     * @param ranges
     *  The cell range addresses to be transformed, as array of cell range
     *  addresses, or as single cell range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed cell range addresses. Untransformed range addresses
     *  will be cloned. Cell ranges that would be deleted completely while
     *  deleting columns or rows, or would be shifted outside the sheet while
     *  inserting columns or rows, will not be included into the result. With
     *  the insertion mode `SPLIT` or the option `splitPartial`, the result
     *  array may contain multiple cell ranges for a single source range.
     */
    transformRanges(ranges: RangeSource, options?: TransformRangeOptions): RangeArray {

        // shortcuts to own properties
        const { maxIndex, columns } = this;

        // the effective move operation
        const insert = this.isInsertMode(options);
        // how to insert/delete intervals inside ranges
        const xfMode = options?.transformMode ?? RESIZE;
        // whether to split partially covered ranges
        const splitPartial = options?.splitPartial;
        // the helper function that implements transformation of an index interval
        const transformFn = insert ? transformIntervalInsert : transformIntervalDelete;
        // the transformed result ranges
        const resultRanges = new RangeArray();

        // the minimum column/row index that will be transformed
        const minIndex = this.dirtyRange.a1.get(columns) - ((insert && (xfMode === EXPAND)) ? 1 : 0);
        // returns whether the passed move interval covers the entire sheet
        const isFullInterval = (interval: Interval): boolean => (interval.first === 0) && (interval.last === maxIndex);

        // builds and appends a cell range from the passed transformed interval and band interval
        const createFn = columns ? Range.fromRowInterval : Range.fromColInterval;
        const appendFn = (bandInt: Interval, first: number, last: number): void => { resultRanges.push(createFn(bandInt, first, last)); };

        // transform all ranges in the passed data source
        consumeArraySource(ranges, range => {

            // the column/row interval of the range in the band interval
            const bandInterval = range.interval(!columns);
            // the column/row interval of the range to be transformed
            const moveInterval = range.interval(columns);
            // whether the range overlaps and/or is contained in the band interval
            const overlaps = this.bandInterval.overlaps(bandInterval);
            const contains = overlaps && this.bandInterval.contains(bandInterval);

            // nothing to do, if the range is not covered by this transformer;
            // do not transform full column/row intervals
            if (!overlaps || (!contains && !splitPartial) || (moveInterval.last < minIndex) || isFullInterval(moveInterval)) {
                resultRanges.push(range.clone());
                return;
            }

            // split partially covered ranges (insert the outer parts of the range's band interval into the result)
            if (!contains) {
                const outerIntervals = new IntervalArray(bandInterval).difference(this.bandInterval);
                outerIntervals.forEach(bandInt => appendFn(bandInt, moveInterval.first, moveInterval.last));
                bandInterval.assign(bandInterval.intersect(this.bandInterval)!);
            }

            // transform the correct interval of the passed range, according to move direction
            transformFn(this.targetIntervals, moveInterval, xfMode, maxIndex, (first, last) => appendFn(bandInterval, first, last));
        });

        return resultRanges;
    }

    /**
     * Transforms the passed cell anchor of a drawing object according to the
     * settings of this address transformer.
     *
     * @param cellAnchor
     *  The cell anchor of a drawing object to be transformed.
     *
     * @returns
     *  The transformed cell anchor. An untransformed cell anchor will be
     *  cloned deeply. If the drawing object would be deleted completely while
     *  deleting columns or rows, `undefined` will be returned instead (except
     *  if `move` mode is passed in the options).
     */
    transformCellAnchor(cellAnchor: CellAnchor, options: TransformAddressOptions & { move: true }): CellAnchor;
    transformCellAnchor(cellAnchor: CellAnchor, options?: TransformAddressOptions): Opt<CellAnchor>;
    // implementation
    transformCellAnchor(cellAnchor: CellAnchor, options?: TransformAddressOptions): Opt<CellAnchor> {

        // create and modify a clone of the passed cell anchor
        cellAnchor = cellAnchor.clone();

        // use the display anchor mode for runtime transformations
        const anchorMode = cellAnchor.getAnchorMode(true);
        // whether the position of the drawing object may change (one-cell and two-cell mode)
        const moveAnchor = anchorMode !== AnchorMode.ABSOLUTE;
        // whether the size of the drawing object may change (two-cell mode only)
        const resizeAnchor = anchorMode === AnchorMode.TWO_CELL;

        // absolutely positioned drawing objects will not change at all
        if (!moveAnchor) { return cellAnchor; }

        // the cell range covered by the cell anchor
        const drawingRange = cellAnchor.getRange();
        // nothing to do, if the transformer does not cover the drawing object completely
        if (!this.bandContainsRange(drawingRange)) { return cellAnchor; }

        // shortcuts to the anchor, range, and transformer properties
        const { columns, maxIndex } = this;
        const { point1, point2 } = cellAnchor;
        const addr1 = drawingRange.a1;
        const addr2 = drawingRange.a2;

        // whether to keep deleted drawings with zero-size
        const moveMode = options?.move;

        // calculate new column/row indexes
        if (this.isInsertMode(options)) {

            // insertion mode: process the target intervals from first to last
            for (const interval of this.targetIntervals) {

                // the number of columns/rows to be inserted
                const moveSize = interval.size();
                // the column/row interval covered by the drawing model
                const index1 = addr1.get(columns);
                const index2 = addr2.get(columns);
                // the maximum number of columns/rows the drawing can be moved
                const maxMoveSize = min(moveSize, maxIndex - index2);

                // exit the loop early, if the drawing object cannot be moved anymore
                if (maxMoveSize === 0) { break; }

                // move end position, if the columns/rows are inserted inside the drawing object
                if (interval.first <= index2) {
                    point2.a.move(maxMoveSize, columns);
                    addr2.move(maxMoveSize, columns);
                    // move completely, if the columns/rows are inserted before the drawing object
                    if (interval.first <= index1) {
                        point1.a.move(maxMoveSize, columns);
                        addr1.move(maxMoveSize, columns);
                    }
                }
            }

            return cellAnchor;
        }

        // deletion mode: process the target intervals in reversed order
        const deleteDrawing = this.targetIntervals.someReverse(interval => {

            // the number of columns/rows to be inserted
            const moveSize = interval.size();
            // the column/row interval covered by the drawing model
            const index1 = addr1.get(columns);
            const index2 = addr2.get(columns);

            // adjust end position of drawings overlapping or following the deleted interval
            if (resizeAnchor) {

                // delete drawing object, if it is contained in the deleted column/row interval,
                // or if it ends at offset zero in the first column/row after the interval
                if ((interval.first <= index1) && (index2 <= interval.last)) {
                    point1.set(interval.first, 0, columns);
                    point2.set(interval.first, 0, columns);
                    addr1.set(interval.first, columns);
                    addr2.set(interval.first, columns);
                    return !moveMode; // early-exit the loop when deleted
                }

                if (interval.last < index2) {
                    // move end position, if the columns/rows are deleted before the end
                    point2.a.move(-moveSize, columns);
                    addr2.move(-moveSize, columns);
                } else if (interval.first <= index2) {
                    // delete drawing if it shrinks towards the left border of the sheet
                    if ((interval.first === 0) && !moveMode) { return true; }
                    // cut end of drawing object at the deleted interval
                    point2.set(interval.first, 0, columns);
                    addr2.set(max(0, interval.first - 1), columns);
                }
            }

            // adjust start position of drawings overlapping or following the deleted interval
            if (interval.last < index1) {
                // move start position, if the columns/rows are deleted before the start
                point1.a.move(-moveSize, columns);
                addr1.move(-moveSize, columns);
            } else if (interval.first <= index1) {
                // cut start of drawing object at the deleted interval
                point1.set(interval.first, 0, columns);
                addr1.set(interval.first, columns);
            }

            // continue with the next target interval
            return false;
        });

        return deleteDrawing ? undefined : cellAnchor;
    }

    /**
     * Transforms the passed sheet selection object according to the settings
     * of this address transformer.
     *
     * @param selection
     *  The sheet selection object to be transformed.
     *
     * @returns
     *  The transformed sheet selection. An untransformed sheet selection will
     *  be cloned deeply.
     */
    transformSheetSelection(selection: SheetSelection): SheetSelection {

        // transform the selection ranges
        const oldRanges = selection.ranges;
        const optRanges = Array.from(oldRanges, range => this.transformRanges(range).first());
        const newRanges = new RangeArray(...optRanges);

        // transform the active cell
        const oldActive = selection.address;
        const newActive = this.transformAddress(oldActive, { move: true });

        // entire selection deleted: create selection for transformed active cell
        if (newRanges.empty()) {
            return SheetSelection.createFromAddress(newActive, selection.drawings);
        }

        // active range deleted: create selection with active cell in first range
        if (!optRanges[selection.active]) {
            return new SheetSelection(newRanges, 0, newRanges[0].a1, selection.drawings);
        }

        // transform array index of active range
        let newIndex = selection.active;
        for (let i = newIndex - 1; i > 0; i -= 1) {
            if (!optRanges[i]) { newIndex -= 1; }
        }

        // create the transformed selection object, transform origin address
        const newSelection = new SheetSelection(newRanges, newIndex, newActive, selection.drawings);
        if (selection.origin) { newSelection.origin = this.transformAddress(selection.origin, { move: true }); }
        return newSelection;
    }

    // protected methods ------------------------------------------------------

    /**
     * Resolves the effective move direction for the passed options.
     */
    protected isInsertMode(options: Opt<ReverseOptions>): boolean {
        return this.insert === !options?.reverse;
    }
}
