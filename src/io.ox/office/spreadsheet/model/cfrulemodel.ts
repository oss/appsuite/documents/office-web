/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, math, fun, ary, map, dict, json } from "@/io.ox/office/tk/algorithms";

import { type OpColor, AutoColorType, Color } from "@/io.ox/office/editframework/utils/color";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";
import type { ThemeModel } from "@/io.ox/office/editframework/model/themecollection";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { type ScalarType, type ScalarTypeNE, CompareResult, compareNumbers, ErrorCode, isErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { type TransformSheetVector, MAX_LENGTH_STANDARD_FORMULA, modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { type SheetModel, SheetAttrsModel } from "@/io.ox/office/spreadsheet/model/sheetattrsmodel";
import type { CFRuleBucket } from "@/io.ox/office/spreadsheet/model/cfrulebucket";
import type { GeneratorOptions, SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { ScalarMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { getLocalTodayAsUTC, countDays } from "@/io.ox/office/spreadsheet/model/formula/utils/dateutils";
import { getPercentileInclusive } from "@/io.ox/office/spreadsheet/model/formula/utils/statutils";
import { type FormulaUpdateTask, type RenameSheetUpdateTask, MoveCellsUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { type SheetTokenArray, TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

// types ======================================================================

export interface CFRangeRuleItem {
    t: string;
    v?: string;
}

export interface CFColorStep extends CFRangeRuleItem {
    c: OpColor;
}

export type CFColorScale = CFColorStep[];

export interface CFDataBarRule extends CFRangeRuleItem {
}

export interface CFDataBar {
    r1: CFDataBarRule;
    r2: CFDataBarRule;
    c: OpColor;
    s?: boolean;
}

export interface CFIconSetRule extends CFRangeRuleItem {
    is?: string;
    id?: number;
    gte?: boolean;
}

export interface CFIconSet {
    is: string;
    ir: CFIconSetRule[];
    s?: boolean;
    c?: boolean;
    r?: boolean;
}

export interface CFRuleAttributes {

    /**
     * The type of the formatting rule. Supports all types used in the document
     * operation "insertCFRule".
     */
    type: string;

    /**
     * The first value, used by most conditional rules. Depending on the rule
     * type, must either be a formula expression as string (without the leading
     * equality sign), or a constant number (for "average" rules only).
     */
    value1: ScalarTypeNE;

    /**
     * The second value, used by interval rules of type "between" and
     * "notBetween" only. Must be a formula expression as string, without the
     * leading equality sign.
     */
    value2: ScalarTypeNE;

    /**
     * A data structure that describes the start point, the end point, and an
     * arbitrary number of additional points inbetween a color scale rule.
     */
    colorScale: CFColorScale;

    /**
     * A data structure that describes a data bar rule.
     */
    dataBar: CFDataBar;

    /**
     * A data structure that describes an icon set rule.
     */
    iconSet: CFIconSet;

    /**
     * The sheet-global priority of this rule among all formatting rules in the
     * sheet. The lower this value, the higher the priority. Rules with equal
     * priority will be applied in insertion order.
     */
    priority: number;

    /**
     * Specifies whether to stop evaluating other formatting rules matching a
     * specific cell, if the conditions of this rule has matched.
     */
    stop: boolean;

    /**
     * An incomplete formatting attribute set containing cell attributes,
     * character attributes, and the identifier of a style sheet. If the
     * conditions of the rule are matching, these attributes will be used to
     * render in the respective cell, above its own attributes.
     */
    attrs: PtCellAttributeSet;
}

export interface CFRuleAttributeSet {
    cfrule: CFRuleAttributes;
}

/**
 * Shared configuration for conditional formatting rules in a collection.
 */
export interface CFRuleSharedConfig {

    /**
     * If set to `true`, integer array indexes will be used to address
     * formatting rules in operations (operation property "id" with an index
     * converted to a string), and when inserting or deleting rules, the
     * following indexes need to be adjusted.
     *
     * Otherwise, the identifier (operation property "id") will be used as
     * constant unique map key to identify the rule.
     */
    indexedOps: boolean;

    /**
     * If set to `true`, the operations will contain a custom reference address
     * (operation property "ref").
     *
     * Otherwise, the reference address will be inferred automatically from the
     * top-left cell of the bounding range of the target ranges.
     */
    customRef: boolean;

    /**
     * If set to `true`, the rules refer to cell stylesheets instead of
     * explicit attribute sets.
     */
    cellStyles: boolean;

    /**
     * If set to `true`, the property operation property "ranges" will contain
     * the name of the sheet containing the rule. By default, the property
     * "ranges" is expected to be a regular range list.
     */
    sheetNames: boolean;

    /**
     * Specifies whether the character case of strings will be ignored during
     * comparisons and pattern matching.
     */
    ignoreCase: boolean;

    /**
     * Specifies whether date rules for years are supported.
     */
    yearRules: boolean;

    /**
     * Specifies whether average rules with standard deviation are supported.
     */
    stdDevRules: boolean;
}

/**
 * Resolved settings for rendering a solid color from a conditional formatting
 * rule of type "colorScale".
 */
export interface CFColorScaleRenderProps {

    /**
     * The CSS fill color for the cell.
     */
    color: string;
}

/**
 * Resolved settings for rendering a data bar from a conditional formatting
 * rule.
 */
export interface CFDataBarRenderProps {

    /**
     * The data bar value represented by the left border of the cell.
     */
    min: number;

    /**
     * The data bar value represented by the right border of the cell.
     */
    max: number;

    /**
     * The value represented by the end of the data bar (cell value).
     */
    num: number;

    /**
     * The CSS fill color for the inner area of the data bar.
     */
    color1: string;

    /**
     * The CSS end color for a color gradient if available.
     */
    color2: string | null;
}

/**
 * Resolved settings for rendering a specific icon from an icon set in a
 * conditional formatting rule.
 */
export interface CFIconSetRenderProps {

    /**
     * The identifier of an icon set.
     */
    id: string;

    /**
     * The zero-based index of the icon from the icon set.
     */
    index: number;
}

export interface CFRuleRenderProps {

    /**
     * Settings for rendering a color from a "colorScale" rule into the cell,
     * according to a numeric cell value.
     */
    colorScale?: CFColorScaleRenderProps;

    /**
     * Settings for rendering a data bar into the cell, according to a numeric
     * cell value.
     */
    dataBar?: CFDataBarRenderProps;

    /**
     * Settings for rendering an icon into the cell, according to a numeric
     * cell value.
     */
    iconSet?: CFIconSetRenderProps;

    /**
     * If set to `true`, only the data bar or icon will be rendered, and the
     * cell value will be hidden.
     */
    hideValue?: boolean;
}

/**
 * Result of resolving a formatting rule for a specific cell address.
 */
export interface CFRuleFormatResult {

    /**
     * The attribute set with all character and cell formatting attributes to
     * be applied to the cell. Will be omitted for a range rule that returns
     * rendering properties only.
     */
    cellAttrSet?: PtCellAttributeSet;

    /**
     * Additional optional rendering properties, e.g. for data bars.
     */
    renderProps?: CFRuleRenderProps;

    /**
     * Whether to abort the evalution of following formatting rules.
     */
    stop?: boolean;
}

/**
 * Required and optional parameters for updating the formula expressions of
 * conditional formatting rules.
 */
export interface CFRuleFormulaUpdateConfig {

    /**
     * The string representation of the current target ranges of this rule to
     * be added to the undo operation.
     */
    oldRangesStr: string;

    /**
     * The new string representation of the target ranges of this rule to be
     * added to the document operation, or `null` for rules to be deleted.
     */
    newRangesStr: string | null;

    /**
     * Whether the target ranges have changed indirectly. Default value is
     * `false`.
     */
    updateRanges?: boolean;

    /**
     * The current implicit reference address of the target ranges.
     */
    oldRefAddress: Address;

    /**
     * The new transformed implicit reference address of the target ranges.
     * Omitted, if the update task is not a "moveCells" task.
     */
    newRefAddress?: Address;

    /**
     * The restored implicit reference address of all target ranges, after
     * undoing the transformations of the target ranges. Omitted, if the update
     * task is not a "moveCells" task.
     */
    undoRefAddress?: Address;
}

/**
 * A descriptor for a formatting rule to be pasted from clipboard.
 */
export interface CFRulePasteItem {

    /**
     * The original identifier of the formatting rule.
     */
    opId: string;

    /**
     * The cell ranges of the formatting rule, intersected with the copy range,
     * relocated to cell A1.
     */
    sourceRanges: RangeArray;

    /**
     * The reference address of the original target ranges, as used for the
     * relative references in all formula expressions in "ruleAttrs".
     */
    refAddress: Address;

    /**
     * The plain attributes of the formatting rule (rule type, conditions, cell
     * formatting attributes).
     */
    ruleAttrs: Partial<CFRuleAttributes>;
}

/**
 * Maps type keys for formula metadata to index types used to access the
 * respective formulas of formatting rules.
 */
interface CFRuleFormulaMetaTypeMap {
    /** Value type formulas (rule attributes "value1" and "value"). */
    value: "value1" | "value2";
    /** Formulas in "colorScale" rules (unlimited number of color steps). */
    color: number;
    /** Formulas in "dataBar" rules (minimum and maximum). */
    data: "r1" | "r2";
    /** Formulas in "iconSet" rules (unlimited number of thresholds). */
    icon: number;
}

/**
 * Type shape of formula metadata provided by formula iterators of a formatting
 * rule (see function `yieldCFRuleFormulas`). The resulting type is a union of
 * interfaces for the different formula types. Each matching metadata object
 * will contains the following properties:
 *
 * - `propType`: The type identifier for the formula (one of the keys of the
 *   interface `CFRuleFormulaMetaTypeMap`).
 *
 * - `propKey`: The name of the property, or the array index, used to store the
 *   formula expression in the formatting rule attributes (one of the property
 *   types of the interface `CFRuleFormulaMetaTypeMap`).
 *
 * - `tokenKey`: The internal map key of the formula token array.
 *
 * - `formula`: The actual formula expression.
 */
export type CFRuleFormulaMeta = dict.ValOf<{
    [PropType in keyof CFRuleFormulaMetaTypeMap]: {
        propType: PropType;
        propKey: CFRuleFormulaMetaTypeMap[PropType];
        tokenKey: `${PropType}:${CFRuleFormulaMetaTypeMap[PropType]}`;
        formula: string;
    };
}>;

/**
 * Identifiers for all token arrays contained in a `CFRuleModel`.
 */
export type CFRuleTokenKey = CFRuleFormulaMeta["tokenKey"];

// private types --------------------------------------------------------------

/**
 * Type categories of conditional formatting rules.
 */
const enum CFRuleCategory {

    /**
     * The rule uses one or two formula expressions to compare the value of
     * the formatted target cell.
     */
    VALUE = 1,

    /**
     * The rule evaluates the value of the formatted target cell without
     * formula expression.
     */
    SELF = 2,

    /**
     * The rule evaluates the values of all cells in the target ranges.
     */
    RANGE = 3
}

// constants ==================================================================

export const CFRULE_ATTRIBUTES: AttributeConfigBag<CFRuleAttributes> = {
    type: { def: "formula" },
    value1: { def: "" },
    value2: { def: "" },
    colorScale: { def: [] },
    dataBar: { def: { r1: { t: "min" }, r2: { t: "max" }, c: Color.AUTO } },
    iconSet: { def: { is: "", ir: [] } },
    priority: { def: 0 },
    stop: { def: false },
    attrs: { def: {} }
};

// private constants ----------------------------------------------------------

// limit types (for colorScale, dataBar, and iconSet) that depend on a formula
const DYNAMIC_LIMIT_TYPES = new Set(["formula", "percent", "percentile"]);

// maps all supported rule types to type categories
const RULE_TYPE_CATEGORIES = fun.do(() => {
    const catMap = new Map<string, CFRuleCategory>();
    const fill = (cat: CFRuleCategory, ...types: string[]): void => {
        for (const type of types) { catMap.set(type, cat); }
    };
    fill(CFRuleCategory.VALUE, "formula", "between", "notBetween", "equal", "notEqual", "less", "lessEqual", "greater", "greaterEqual", "contains", "notContains", "beginsWith", "endsWith");
    fill(CFRuleCategory.SELF,  "yesterday", "today", "tomorrow", "last7Days", "lastWeek", "thisWeek", "nextWeek", "lastMonth", "thisMonth", "nextMonth", "lastYear", "thisYear", "nextYear", "blank", "notBlank", "error", "noError");
    fill(CFRuleCategory.RANGE, "aboveAverage", "atLeastAverage", "belowAverage", "atMostAverage", "topN", "bottomN", "topPercent", "bottomPercent", "unique", "duplicate", "colorScale", "dataBar", "iconSet");
    return catMap;
});

// all properties that need to be written together in a change operation
const RULE_TYPE_PROPERTIES = fun.do(() => {
    const propsMap = new Map<string, Array<KeysOf<CFRuleAttributes>>>();
    const fill = (props: Array<KeysOf<CFRuleAttributes>>, ...types: string[]): void => {
        props = ["type", ...props];
        for (const type of types) { propsMap.set(type, props); }
    };
    fill(["value1"],           "formula", "equal", "notEqual", "less", "lessEqual", "greater", "greaterEqual", "contains", "notContains", "beginsWith", "endsWith", "aboveAverage", "atLeastAverage", "belowAverage", "atMostAverage", "topN", "bottomN", "topPercent", "bottomPercent");
    fill(["value1", "value2"], "between", "notBetween");
    fill(["colorScale"],       "colorScale");
    fill(["dataBar"],          "dataBar");
    fill(["iconSet"],          "iconSet");
    return propsMap;
});

// public functions ===========================================================

/**
 * Creates an iterator that visits all existing formula expressions in the
 * passed rule attribute bag.
 *
 * @param ruleAttrs
 *  The formatting rule attributs to be processed.
 *
 * @yields
 *  The existing formula expressions.
 */
export function *yieldCFRuleFormulas(ruleAttrs: Partial<CFRuleAttributes>): IterableIterator<CFRuleFormulaMeta> {

    // attributes "value1" and "value2" for regular rules with formula expressions
    if (RULE_TYPE_CATEGORIES.get(ruleAttrs.type ?? "formula") === CFRuleCategory.VALUE) {
        for (const propKey of ["value1", "value2"] as const) {
            const value = ruleAttrs[propKey];
            if (is.string(value)) {
                yield { propType: "value", propKey, tokenKey: `value:${propKey}`, get formula() { return value; }, set formula(f) { ruleAttrs[propKey] = f; } };
            }
        }
    }

    // attribute "colorScale" contains an unlimited number of color stops
    if (ruleAttrs.colorScale) {
        for (const [propKey, scaleStep] of ruleAttrs.colorScale.entries()) {
            if (scaleStep.v) {
                yield { propType: "color", propKey, tokenKey: `color:${propKey}`, get formula() { return scaleStep.v!; }, set formula(f) { scaleStep.v = f; } };
            }
        }
    }

    // attribute "dataBar" contains two formula expressions for minimum/maximum
    if (ruleAttrs.dataBar) {
        for (const propKey of ["r1", "r2"] as const) {
            const barRule = ruleAttrs.dataBar[propKey];
            if (barRule.v) {
                yield { propType: "data", propKey, tokenKey: `data:${propKey}`, get formula() { return barRule.v!; }, set formula(f) { barRule.v = f; } };
            }
        }
    }

    // attribute "iconSet" contains up to 4 formula expressions
    if (ruleAttrs.iconSet) {
        for (const [propKey, iconRule] of ruleAttrs.iconSet.ir.entries()) {
            if (iconRule.v) {
                yield { propType: "icon", propKey, tokenKey: `icon:${propKey}`, get formula() { return iconRule.v!; }, set formula(f) { iconRule.v = f; } };
            }
        }
    }
}

// class CFRuleModel ==========================================================

/**
 * Stores settings for a single formatting rule of conditionally formatted cell
 * ranges.
 */
export class CFRuleModel extends SheetAttrsModel<CFRuleAttributeSet> {

    /**
     * The parent rule bucket containing this rule.
     */
    ruleBucket: CFRuleBucket;

    /**
     * The rule identifier for operations (may change in indexed mode).
     */
    opId: string;

    // shared collection/model configuration
    readonly #config: Readonly<CFRuleSharedConfig>;
    // the theme of the spreadsheet document
    readonly #themeModel: ThemeModel;

    // bug 56646: (ODF) explicit reference address for formulas with relative references
    #expRefAddress: Opt<Address>;
    // the token array representing the formula expressions of the various conditions
    readonly #tokenArrayMap = this.member(new Map<CFRuleTokenKey, SheetTokenArray>());
    // the category of the rule type
    #ruleCategory: Opt<CFRuleCategory>;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The parent sheet model containing this formatting rule.
     *
     * @param ruleBucket
     *  The parent rule bucket containing this rule.
     *
     * @param opId
     *  The identifier of this formatting rule used in JSON operations.
     *
     * @param ruleAttrs
     *  The initial rule attributes.
     *
     * @param refFormula
     *  The custom reference address, as formula expression.
     *
     * @param config
     *  The shared configuration of the parent collection.
     *
     * @param cloneModel
     *  An existing formatting rule model model to be cloned.
     */
    constructor(sheetModel: SheetModel, ruleBucket: CFRuleBucket, opId: string, ruleAttrs: Partial<CFRuleAttributes>, refFormula: string | null, config: Readonly<CFRuleSharedConfig>);
    constructor(sheetModel: SheetModel, ruleBucket: CFRuleBucket, cloneModel: CFRuleModel);
    // implementation
    constructor(sheetModel: SheetModel, ruleBucket: CFRuleBucket, cloneModelOrOpId: string | CFRuleModel, ruleAttrs?: Partial<CFRuleAttributes>, refFormula?: string | null, config?: Readonly<CFRuleSharedConfig>) {

        // whether this constructor needs to clone an existing rule model
        const clone = cloneModelOrOpId instanceof CFRuleModel;
        const cloneModel = clone ? cloneModelOrOpId : undefined;
        const opId = clone ? undefined : cloneModelOrOpId;

        // base constructor
        if (cloneModel) { ruleAttrs = cloneModel.#getRuleAttrs(true); }
        super(sheetModel, ["cfrule"], { cfrule: ruleAttrs });

        // public properties
        this.ruleBucket = ruleBucket;
        this.opId = cloneModel ? cloneModel.opId : opId!;

        // shared configuration of the parent collection
        this.#config = cloneModel ? cloneModel.#config : config!;

        // private properties
        this.#themeModel = this.docModel.getThemeModel();

        // calculate the reference address
        if (this.#config.customRef) {
            if (cloneModel) {
                const expRefAddress = cloneModel.#expRefAddress;
                refFormula = expRefAddress ? cloneModel.#generateRefFormula(expRefAddress, sheetModel.getName()) : null;
            }
            this.setRefFormula(refFormula || "");
        }

        // parse the initial formula expressions
        this.#parseFormulas(this.#getRuleAttrs());

        // parse the formula expressions after changing the attributes
        this.on("change:attributes", (newAttrSet, oldAttrSet) => {
            this.#parseFormulas(newAttrSet.cfrule, oldAttrSet.cfrule);
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the priority of this rule model. The less this value, the
     * earlier this rule model will be evaluated compared to all other rule
     * models in a conditional formatting collection.
     *
     * @returns
     *  The priority of this rule model.
     */
    getPriority(): number {
        return this.#getRuleAttrs().priority;
    }

    /**
     * Returns the addresses of all target ranges covered by this rule.
     *
     * @returns
     *  The addresses of all target ranges.
     */
    getTargetRanges(): RangeArray {
        return this.ruleBucket.ranges.deepClone();
    }

    /**
     * Returns the reference addresses of the target ranges covered by this
     * instance. The reference address is the address of the top-left cell of
     * the bounding range of all target cell ranges.
     *
     * @returns
     *  The address of the reference cell of the target ranges.
     */
    getRefAddress(): Address {
        // bug 56646: use explicit reference address (ODF), fall-back to top-left address of target ranges
        return (this.#expRefAddress ?? this.ruleBucket.refAddress).clone();
    }

    /**
     * Returns whether this rule needs all its covered cells for evaluating if
     * it applies to a specific cell value. For example, "average" rules use
     * the arithmetic mean of all covered number cells. After changing one of
     * these cells, all other cells covered by this rule need to be checked
     * whether they apply to the new arithmetic mean.
     *
     * @returns
     *  Whether this rule needs all its covered cells for evaluating if it
     *  applies to a specific cell value.
     */
    isRangeRule(): boolean {
        return this.#ruleCategory === CFRuleCategory.RANGE;
    }

    /**
     * Returns the target position to be used to resolve the dependencies of
     * the formulas contained in this rule model. For range rules (see method
     * `isRangeRule` for details), this is the reference address of the parent
     * formatting model (the formulas will always be evaluated relative to the
     * reference address), otherwise the target ranges of the formatting model
     * will be returned (the formulas will be evaluated relative to each cell
     * in the target ranges).
     *
     * @returns
     *  The target position to resolve the formula dependencies, according to
     *  the type of this rule.
     */
    getDependencyTarget(): Address | RangeArray {
        return this.isRangeRule() ? this.getRefAddress() : this.getTargetRanges();
    }

    /**
     * Returns an iterator that visits all token arrays of this rule.
     *
     * @returns
     *  The new token array iterator. The first element of the result value is
     *  the internal map key of the token array.
     */
    tokenArrays(): KeyedIterator<SheetTokenArray> {
        return this.#tokenArrayMap.entries();
    }

    /**
     * Returns the explicit formatting attributes contained in this rule model
     * that will be applied to a cell if this rule matches its value.
     *
     * @param [direct]
     *  If set to `true`, the returned attribute set will be a reference to the
     *  original map stored in this instance, which MUST NOT be modified! By
     *  default, a deep clone of the attribute set will be returned that can be
     *  freely modified.
     *
     * @returns
     *  The explicit formatting attributes that will be applied if this rule
     *  matches the cell value.
     */
    getMatchAttributeSet(direct = false): PtCellAttributeSet {
        const attrSet = this.#getRuleAttrs().attrs;
        return direct ? attrSet : json.deepClone(attrSet);
    }

    /**
     * Returns the formatting attributes to be applied at the specified target
     * cell, if the conditions of this rule are matching.
     *
     * @param address
     *  The address of the cell to calculate the attributes for. MUST be a cell
     *  that is covered by the parent formatting model.
     *
     * @returns
     *  The result object for a match.
     */
    resolveRule(address: Address): Opt<CFRuleFormatResult> {

        // the settings of this rule
        const ruleAttrs = this.#getRuleAttrs();
        // the current value of the specified cell
        let value = this.sheetModel.cellCollection.getValue(address);
        // whether the cell value matches the conditions (always false for the advanced range rules colorScale, DataBar, and iconSet)
        let matches = false;
        // the cell formatting attributes to be applied at the cell for this rule
        let attrSet: Opt<PtCellAttributeSet>;
        // additional rendering properties, e.g. for data bars
        let renderProps: Opt<CFRuleRenderProps>;

        // process all supported rule types
        switch (ruleAttrs.type) {

            // formula expression: rule matches if result is truthy
            case "formula":
                value = this.#interpretCondition("value:value1", address);
                matches = !!this.docModel.numberFormatter.convertScalarToBoolean(value);
                break;

            // interval rules: value must be inside or outside an interval
            case "between":
                matches = this.#matchesInterval(value, address, true);
                break;
            case "notBetween":
                matches = this.#matchesInterval(value, address, false);
                break;

            // comparison rules: compare cell value with formula expression in "value1"
            case "equal":
            case "notEqual":
            case "less":
            case "lessEqual":
            case "greater":
            case "greaterEqual":
                matches = this.#matchesValue(value, address, "value:value1", ruleAttrs.type);
                break;

            // text rules
            case "contains":
                // TODO: pattern matching
                matches = this.#matchesString(value, address, (text, comp) => text.includes(comp));
                break;
            case "notContains":
                // TODO: pattern matching
                matches = this.#matchesString(value, address, (text, comp) => !text.includes(comp));
                break;
            case "beginsWith":
                matches = this.#matchesString(value, address, (text, comp) => text.startsWith(comp));
                break;
            case "endsWith":
                matches = this.#matchesString(value, address, (text, comp) => text.endsWith(comp));
                break;

            // date rules
            case "yesterday":
                matches = this.#matchesDate(value, "day", -1);
                break;
            case "today":
                matches = this.#matchesDate(value, "day", 0);
                break;
            case "tomorrow":
                matches = this.#matchesDate(value, "day", 1);
                break;
            case "last7Days":
                matches = this.#matchesDate(value, "7days", 0);
                break;
            case "lastWeek":
                matches = this.#matchesDate(value, "week", -1);
                break;
            case "thisWeek":
                matches = this.#matchesDate(value, "week", 0);
                break;
            case "nextWeek":
                matches = this.#matchesDate(value, "week", 1);
                break;
            case "lastMonth":
                matches = this.#matchesDate(value, "month", -1);
                break;
            case "thisMonth":
                matches = this.#matchesDate(value, "month", 0);
                break;
            case "nextMonth":
                matches = this.#matchesDate(value, "month", 1);
                break;
            case "lastYear":
                matches = this.#matchesDate(value, "year", -1);
                break;
            case "thisYear":
                matches = this.#matchesDate(value, "year", 0);
                break;
            case "nextYear":
                matches = this.#matchesDate(value, "year", 1);
                break;

            // above/below average rules
            case "aboveAverage":
                matches = this.#matchesAverage(value, ruleAttrs.value1, false, false);
                break;
            case "atLeastAverage":
                matches = this.#matchesAverage(value, ruleAttrs.value1, false, true);
                break;
            case "belowAverage":
                matches = this.#matchesAverage(value, ruleAttrs.value1, true, false);
                break;
            case "atMostAverage":
                matches = this.#matchesAverage(value, ruleAttrs.value1, true, true);
                break;

            // top/bottom rules
            case "topN":
                matches = this.#matchesRank(value, ruleAttrs.value1, false, true);
                break;
            case "topPercent":
                matches = this.#matchesRank(value, ruleAttrs.value1, true, true);
                break;
            case "bottomN":
                matches = this.#matchesRank(value, ruleAttrs.value1, false, false);
                break;
            case "bottomPercent":
                matches = this.#matchesRank(value, ruleAttrs.value1, true, false);
                break;

            // quantity rules
            case "unique":
                matches = this.#matchesQuantity(value, true);
                break;
            case "duplicate":
                matches = this.#matchesQuantity(value, false);
                break;

            // cell type rules
            case "blank":
                matches = this.#matchesBlank(value, true);
                break;
            case "notBlank":
                matches = this.#matchesBlank(value, false);
                break;
            case "error":
                matches = this.#matchesError(value, true);
                break;
            case "noError":
                matches = this.#matchesError(value, false);
                break;

            // complex range rules (do not set "matches", range rules cannot stop rule evaluation)
            case "colorScale":
                renderProps = this.#getColorScaleProperties(value);
                break;
            case "dataBar":
                renderProps = this.#getDataBarProperties(value);
                break;
            case "iconSet":
                renderProps = this.#getIconSetProperties(value);
                break;
        }

        // no match, no result
        if (!matches && !renderProps) { return undefined; }

        // if a regular rule matches the passed cell value, return the formatting attributes of this rule
        if (matches) {
            attrSet = this.getMatchAttributeSet(); // returns a deep copy of the attributes
        }

        // ODF: resolve the explicit attributes of a style sheet (but do not add the default attribute values)
        if (attrSet && ("styleId" in attrSet)) {
            if (this.#config.cellStyles) {
                attrSet = this.docModel.cellStyles.getMergedAttributes(attrSet, { skipDefaults: true });
            }
            delete attrSet.styleId;
        }

        // create and return the result object
        const ruleResult = dict.createPartial<CFRuleFormatResult>();
        if (matches && ruleAttrs.stop) { ruleResult.stop = true; }
        if (attrSet) { ruleResult.cellAttrSet = attrSet; }
        if (renderProps) { ruleResult.renderProps = renderProps; }
        return ruleResult;
    }

    /**
     * Changes all formula expressions stored in the rule attributes, after the
     * sheet collection of the document has been changed.
     */
    transformSheets(xfVector: TransformSheetVector): void {
        for (const tokenArray of this.#tokenArrayMap.values()) {
            tokenArray.transformSheets(xfVector);
        }
    }

    /**
     * Clears the cached formula results in all token arrays of this formatting
     * rule.
     */
    clearResultCache(): void {
        for (const tokenArray of this.#tokenArrayMap.values()) {
            tokenArray.clearResultCache();
        }
    }

    /**
     * Refreshes the target ranges of this rule after the source data referred
     * by the conditions has been changed. This method will be called
     * automatically by the dependency manager of the spreadsheet document.
     */
    refreshFormulas(): void {
        this.sheetModel.refreshRanges(this.ruleBucket.ranges);
    }

    /**
     * Changes the reference address of this rule model.
     *
     * @param refFormula
     *  A formula expression that contains a single reference with a single
     *  cell address.
     */
    setRefFormula(refFormula: string): void {

        // parse the passed reference address expression
        const tokenArray = new TokenArray(this.sheetModel, "rule");
        tokenArray.parseFormula("op", refFormula, Address.A1);

        // try to convert to a single reference address (do not pass a reference
        // sheet index, sheet name is expected to exist in the formula expression)
        const ranges = tokenArray.resolveRangeList(Address.A1, Address.A1);

        // if the expression resolves to a single cell address, use it as reference address
        const isSingle = (ranges.length === 1) && ranges[0].single();
        this.#expRefAddress = isSingle ? ranges[0].a1 : undefined;
    }

    /**
     * Tries to replace unresolved sheet names in the token arrays with
     * existing sheet indexes. Intended to be used after document import to
     * refresh all token arrays that refer to sheets that did not exist during
     * their creation.
     */
    refreshAfterImport(): void {
        const refAddress = this.getRefAddress();
        for (const tokenArray of this.#tokenArrayMap.values()) {
            tokenArray.refreshAfterImport(refAddress);
        }
    }

    /**
     * Returns a plain JSON object for this formatting rule that can be used in
     * clipboard markup.
     *
     * @param copyRange
     *  The address of the cell range to be copied to clipboard.
     *
     * @returns
     *  The clipboard paste item, if this rule overlaps with the specified copy
     *  range.
     */
    serializePasteItems(copyRange: Range): Opt<CFRulePasteItem> {

        // calculate the intersection ranges
        const sourceRanges = this.ruleBucket.ranges.intersect(copyRange);
        if (sourceRanges.empty()) { return undefined; }

        // relocate the ranges to cell A1
        for (const sourceRange of sourceRanges) {
            sourceRange.move(-copyRange.a1.c, -copyRange.a1.r);
        }

        // the rule attributes (deep copy to be able to modify formulas below)
        const ruleAttrs: Partial<CFRuleAttributes> = json.deepClone(this.#getRuleAttrs());

        // replace stylesheet reference with explicit attributes (ODF)
        if (ruleAttrs.attrs?.styleId) {
            ruleAttrs.attrs = this.docModel.cellStyles.getStyleAttributeSet(ruleAttrs.attrs.styleId);
        }

        // convert all formula expressions to "OP_OOX" grammar used in clipboard
        const [newRuleAttrs] = this.#transformFormulas(tokenArray => tokenArray.getFormula("op:oox", Address.A1, Address.A1));
        Object.assign(ruleAttrs, newRuleAttrs);

        // delete unused properties (have been default-initialized)
        if (ruleAttrs.value1 === "") { delete ruleAttrs.value1; }
        if (ruleAttrs.value2 === "") { delete ruleAttrs.value2; }
        if (!ruleAttrs.priority) { delete ruleAttrs.priority; }
        if (!ruleAttrs.stop) { delete ruleAttrs.stop; }
        if (ruleAttrs.type !== "colorScale") { delete ruleAttrs.colorScale; }
        if (ruleAttrs.type !== "dataBar") { delete ruleAttrs.dataBar; }
        if (ruleAttrs.type !== "iconSet") { delete ruleAttrs.iconSet; }
        if (is.empty(ruleAttrs.attrs)) { delete ruleAttrs.attrs; }

        // create the clipboard paste item
        return {
            opId: this.opId,
            sourceRanges,
            refAddress: this.ruleBucket.refAddress,
            ruleAttrs: Object.assign(ruleAttrs, newRuleAttrs)
        };
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the document operation needed to restore this formatting rule
     * after it has been deleted.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param rangesStr
     *  The string representation of the target ranges of this rule, as used in
     *  document operations.
     *
     * @param [options]
     *  Optional parameters.
     */
    generateRestoreOperations(generator: SheetOperationGenerator, rangesStr: string, options?: GeneratorOptions): void {

        // initial operation properties
        const operProps: Dict = { id: this.opId, ranges: rangesStr };

        // create the formula expression for the reference address
        if (this.#expRefAddress) {
            operProps.ref = this.#generateRefFormula(this.#expRefAddress);
        }

        // add the rule attributes that are different to the defaults
        const ruleAttrs = this.#getRuleAttrs(true);
        if (ruleAttrs) {
            const ruleDefAttrs = this.attrPool.getDefaultValues("cfrule");
            dict.forEach(ruleAttrs, (ruleAttr, attrName) => {
                if (!_.isEqual(ruleAttr, ruleDefAttrs[attrName])) {
                    operProps[attrName] = ruleAttr;
                }
            });
        }

        generator.generateSheetOperation(Op.INSERT_CFRULE, operProps, options);
    }

    /**
     * Generates the operations and undo operations to update and restore the
     * formula expressions of this formatting rule.
     *
     * @param generator
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @param updateConfig
     *  Descriptor with changed target ranges and reference addresses.
     */
    generateUpdateTaskOperations(generator: SheetOperationGenerator, updateTask: FormulaUpdateTask, updateConfig: CFRuleFormulaUpdateConfig): void {

        // generate operation for deleted rules
        if (!updateConfig.newRangesStr) {
            generator.generateSheetOperation(Op.DELETE_CFRULE, { id: this.opId });
            this.generateRestoreOperations(generator, updateConfig.oldRangesStr, { undo: true, prepend: true });
            return;
        }

        // the formula grammar used for formula expression transformations
        const grammar = this.docModel.formulaGrammarOP;

        // the properties for the document and undo operation
        const operProps = dict.create();
        const undoProps = dict.create();
        // the transformed explicit reference address
        let newRefAddress = this.#expRefAddress;

        // update the reference address, if the moved cells are located in the own sheet
        if (this.sheetModel.isOwnMoveTask(updateTask)) {

            // shortcut to the address transformer
            const transformer = updateTask.transformer;
            const oldRefAddress = updateConfig.oldRefAddress;
            const undoRefAddress = updateConfig.undoRefAddress;

            // transform the explicit reference address
            if (this.#expRefAddress) {
                newRefAddress = transformer.transformAddress(this.#expRefAddress, { move: true });
            } else if (undoRefAddress && oldRefAddress.differs(undoRefAddress)) {
                // if the reference address will be deleted, relocate the relative references in the formulas to the
                // reference address that remains valid during range transformation (top-left cell of restored ranges)
                updateTask = new MoveCellsUpdateTask(updateTask.sheet, transformer, oldRefAddress, undoRefAddress);
            }
        }

        // update formula expressions according to the current rule type
        const [newRuleAttrs, oldRuleAttrs] = this.#transformFormulas(tokenArray => updateTask.transformFormula(tokenArray, grammar));
        Object.assign(operProps, newRuleAttrs);
        Object.assign(undoProps, oldRuleAttrs);

        // DOCS-2754: add all dependent properties that describe the rule settings
        const ruleAttrs = this.#getRuleAttrs();
        const propNames = RULE_TYPE_PROPERTIES.get(ruleAttrs.type);
        if (propNames && !is.empty(operProps)) {
            for (const propName of propNames) {
                if (!(propName in operProps)) {
                    operProps[propName] = undoProps[propName] = ruleAttrs[propName];
                }
            }
        }

        // DOCS-1825: always put target ranges (also unchanged) into the operation (needed for OT)
        if (updateConfig.updateRanges || !is.empty(operProps)) {
            operProps.ranges = updateConfig.newRangesStr;
            undoProps.ranges = updateConfig.oldRangesStr;
        }

        // ODF: update the formula expression for the reference address for renamed sheets
        const renameSheet = this.#expRefAddress && this.sheetModel.isOwnRenameTask(updateTask);
        if (this.#expRefAddress && newRefAddress && (renameSheet || this.#expRefAddress.differs(newRefAddress) || !is.empty(operProps))) {
            const newSheetName = renameSheet ? (updateTask as RenameSheetUpdateTask).sheetName : undefined;
            operProps.ref = this.#generateRefFormula(newRefAddress, newSheetName);
            undoProps.ref = this.#generateRefFormula(this.#expRefAddress);
        }

        // generate the operations (undo operation must be prepended to retain array indexes used as identifiers)
        if (!is.empty(operProps)) {
            operProps.id = undoProps.id = this.opId;
            generator.generateSheetOperation(Op.CHANGE_CFRULE, operProps);
            generator.generateSheetOperation(Op.CHANGE_CFRULE, undoProps, { undo: true, prepend: true });
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the merged or explicit rule attributes.
     */
    #getRuleAttrs(explicit: true): Opt<Partial<CFRuleAttributes>>;
    #getRuleAttrs(): CFRuleAttributes;
    // implementation
    #getRuleAttrs(explicit?: boolean): Opt<Partial<CFRuleAttributes>> {
        return (explicit ? this.getExplicitAttributeSet(true) : this.getMergedAttributeSet(true)).cfrule;
    }

    /**
     * Returns a formula expression with sheet name representing the passed
     * reference address.
     *
     * @param refAddress
     *  The reference address of a formatting rule.
     *
     * @param [sheetName]
     *  A custom sheet name to be inserted into the reference. If omitted, the
     *  name of the own parent sheet will be used.
     *
     * @returns
     *  The passed reference cell, as formula expression with sheet name.
     */
    #generateRefFormula(refAddress: Address, sheetName?: string): string {
        const tokenArray = new TokenArray(this.sheetModel, "rule");
        tokenArray.appendAddress(refAddress, { sheet: sheetName || this.sheetModel.getIndex(), relSheet: true });
        return tokenArray.getFormulaOp();
    }

    /**
     * Returns the smallest number covered by the target ranges.
     *
     * @param autoZero
     *  If set to `true`, and the minimum is positive, zero will be returned.
     *
     * @returns
     *  The smallest number of the covered cells; or null if the target ranges
     *  do not contain any numbers.
     */
    #getMinNumber(autoZero: boolean): number | null {
        const min = this.ruleBucket.valueCache.getMin();
        return !Number.isFinite(min) ? null : autoZero ? Math.min(0, min) : min;
    }

    /**
     * Returns the largest number covered by the target ranges.
     *
     * @param autoZero
     *  If set to `true`, and the maximum is negative, zero will be returned.
     *
     * @returns
     *  The largest number of the covered cells; or null if the target ranges
     *  do not contain any numbers.
     */
    #getMaxNumber(autoZero: boolean): number | null {
        const max = this.ruleBucket.valueCache.getMax();
        return !Number.isFinite(max) ? null : autoZero ? Math.max(0, max) : max;
    }

    /**
     * Returns a number between smallest and largest number in the cells of the
     * target ranges.
     *
     * @param percent
     *  The percentage.
     *
     * @returns
     *  The resulting number; or `null` if the target ranges do not contain any
     *  numbers.
     */
    #getPercentNumber(percent: number): number | null {
        const rangeCache = this.ruleBucket.valueCache;
        const min = rangeCache.getMin(), max = rangeCache.getMax();
        const number = (max - min) * (percent / 100) + min;
        return Number.isFinite(number) ? number : null;
    }

    /**
     * Returns a number between smallest and largest number in the cells of the
     * target ranges.
     *
     * @param percentile
     *  The percentile.
     *
     * @returns
     *  The resulting number; or `null` if the target ranges do not contain any
     *  numbers.
     */
    #getPercentileNumber(percentile: number): number | null {
        try {
            const numbers = this.ruleBucket.valueCache.getSortedNumbers();
            return getPercentileInclusive(numbers, percentile / 100, true);
        } catch {
            return null;
        }
    }

    /**
     * Parses the passed formula expression into a token array.
     *
     * @param tokenKey
     *  The map key of a specific token array.
     *
     * @param [value]
     *  The attribute value. Strings will be parsed as formula expression into
     *  the specified token array. All other values will cause to destroy and
     *  remove the token array from the internal map.
     */
    #parseRuleValue(tokenKey: CFRuleTokenKey, value?: ScalarType): void {
        if (is.string(value) && (value.length > 0)) {
            map.upsert(this.#tokenArrayMap, tokenKey, () => new TokenArray(this.sheetModel, "rule")).parseFormula("op", value, this.getRefAddress());
        } else {
            map.remove(this.#tokenArrayMap, tokenKey)?.destroy();
        }
    }

    /**
     * Parses the passed formula expression into a token array.
     *
     * @param tokenKey
     *  The map key of a specific token array.
     *
     * @param item
     *  A range rule item with type specifier and optional formula expression.
     */
    #parseRangeRuleItem(tokenKey: CFRuleTokenKey, item: CFRangeRuleItem): void {
        // only specific limit types depend on a formula, e.g. "min" and "max" do not
        const value = DYNAMIC_LIMIT_TYPES.has(item.t) ? item.v : undefined;
        this.#parseRuleValue(tokenKey, value);
    }

    /**
     * Updates internal settings after the rule attributes of this rule model
     * have been changed.
     */
    #parseFormulas(newRuleAttrs: CFRuleAttributes, oldRuleAttrs?: CFRuleAttributes): void {

        // whether the rule type has changed
        const typeChanged = newRuleAttrs.type !== oldRuleAttrs?.type;

        // delete all token arrays, if the rule type has changed, and detect whether this rule is based on all covered cells
        if (typeChanged) {
            map.destroy(this.#tokenArrayMap);
            this.#ruleCategory = RULE_TYPE_CATEGORIES.get(newRuleAttrs.type);
        }

        // use formula expressions in "value1" and "value2" for simple rules
        if (this.#ruleCategory === CFRuleCategory.VALUE) {
            for (const propName of ["value1", "value2"] as const) {
                if (typeChanged || (newRuleAttrs[propName] !== oldRuleAttrs?.[propName])) {
                    this.#parseRuleValue(`value:${propName}`, newRuleAttrs[propName]);
                }
            }
            return;
        }

        // parse formula expressions according to the current rule type
        switch (newRuleAttrs.type) {

            // attribute "colorScale" contains an unlimited number of color stops
            case "colorScale": {
                const { colorScale } = newRuleAttrs;
                if (typeChanged || !_.isEqual(colorScale, oldRuleAttrs.colorScale)) {
                    map.destroy(this.#tokenArrayMap);
                    for (const [stepIdx, scaleStep] of colorScale.entries()) {
                        this.#parseRangeRuleItem(`color:${stepIdx}`, scaleStep);
                    }
                }
                break;
            }

            // attribute "dataBar" contains two formula expressions for minimum/maximum
            case "dataBar": {
                const { dataBar } = newRuleAttrs;
                if (typeChanged || !_.isEqual(dataBar, oldRuleAttrs.dataBar)) {
                    map.destroy(this.#tokenArrayMap);
                    for (const propName of ["r1", "r2"] as const) {
                        this.#parseRangeRuleItem(`data:${propName}`, dataBar[propName]);
                    }
                }
                break;
            }

            // attribute "iconSet" contains up to 4 formula expressions
            case "iconSet": {
                const { iconSet } = newRuleAttrs;
                if (typeChanged || !_.isEqual(iconSet, oldRuleAttrs.iconSet)) {
                    map.destroy(this.#tokenArrayMap);
                    for (const [ruleIdx, iconRule] of iconSet.ir.entries()) {
                        this.#parseRangeRuleItem(`icon:${ruleIdx}`, iconRule);
                    }
                }
                break;
            }
        }
    }

    /**
     * Returns the interpreted result of the specified formula expression.
     *
     * @param tokenKey
     *  The map key of a token array.
     *
     * @param [targetAddress]
     *  The target address needed to interpret the formula (the address of the
     *  cell to interpret the token array for). If omitted, the reference
     *  address of the parent formatting model will be used (for range rules,
     *  such as colorScale, dataBar, and iconSet).
     *
     * @returns
     *  The scalar result of the condition.
     */
    #interpretCondition(tokenKey: CFRuleTokenKey, targetAddress?: Address): ScalarType {

        // get the specified token array, fall-back to the #N/A error code
        const tokenArray = this.#tokenArrayMap.get(tokenKey);
        if (!tokenArray) { return ErrorCode.NA; }

        // calculate the result relative to the target address (bug 49355: evaluate in matrix context)
        const refAddress = this.getRefAddress();
        const { value } = tokenArray.interpretFormula("mat", refAddress, targetAddress ?? refAddress, {
            cacheResult: true // results will be cached internally for repeated usage in multiple cells
        });

        // return the value regardless of the result type (will be an error code in case of internal errors etc.)
        // bug 49355: result is a matrix because formula has been evaluated in matrix context, but return value is a scalar
        return (value as ScalarMatrix).get(0, 0);
    }

    /**
     * Implementation for comparison rules. Returns whether the passed cell
     * value and the comparison value resolved from the formula expression of
     * the rule property "value1" or "value2" are related to each other as
     * expected.
     */
    #matchesValue(value: ScalarType, address: Address, tokenKey: CFRuleTokenKey, type: string): boolean {
        const result = this.docModel.formulaInterpreter.compareScalars(value, this.#interpretCondition(tokenKey, address));
        switch (type) {
            case "equal":        return result === CompareResult.EQUAL;
            case "notEqual":     return result !== CompareResult.EQUAL;
            case "less":         return result === CompareResult.LESS;
            case "lessEqual":    return result <= CompareResult.EQUAL;
            case "greater":      return result === CompareResult.GREATER;
            case "greaterEqual": return result >= CompareResult.EQUAL;
        }
        return false;
    }

    /**
     * Implementation for interval rules. Returns whether the passed cell value
     * is (not) located in the interval formed by the values resolved from the
     * formula expressions of the rule properties "value1" and "value2".
     */
    #matchesInterval(value: ScalarType, address: Address, matchInside: boolean): boolean {
        const isInside = this.#matchesValue(value, address, "value:value1", "greaterEqual") && this.#matchesValue(value, address, "value:value2", "lessEqual");
        return matchInside === isInside;
    }

    /**
     * Implementation for text rules. Returns whether the passed cell value and
     * the comparison string resolved from the formula expression of the rule
     * property "value1" are matching according to the predicate function.
     */
    #matchesString(value: ScalarType, address: Address, predicate: FuncType<boolean, Pair<string>>): boolean {
        let textValue = this.docModel.numberFormatter.convertScalarToString(value, MAX_LENGTH_STANDARD_FORMULA);
        if (textValue === null) { return false; }
        let compValue = this.#interpretCondition("value:value1", address);
        compValue = this.docModel.numberFormatter.convertScalarToString(compValue, MAX_LENGTH_STANDARD_FORMULA);
        if ((compValue !== null) && (compValue.length > 0)) {
            if (this.#config.ignoreCase) {
                textValue = textValue.toLowerCase();
                compValue = compValue.toLowerCase();
            }
            return predicate(textValue, compValue);
        }
        return false;
    }

    /**
     * Implementation for date rules. Returns whether the passed cell value is
     * matching according to the exact rule type.
     */
    #matchesDate(value: ScalarType, type: string, diff: number): boolean {

        // convert the cell value to a date (returns null on error)
        const date = this.docModel.numberFormatter.convertScalarToDate(value, true);
        if (!date) { return false; }

        const compDate = getLocalTodayAsUTC();
        switch (type) {
            case "day":
                if (diff !== 0) { compDate.setUTCDate(compDate.getUTCDate() + diff); }
                return (compDate.getUTCFullYear() === date.getUTCFullYear()) && (compDate.getUTCMonth() === date.getUTCMonth()) && (compDate.getUTCDate() === date.getUTCDate());
            case "7days":
                diff = countDays(date, compDate);
                return (diff >= 0) && (diff < 7);
            case "week":
                compDate.setUTCDate(compDate.getUTCDate() - compDate.getUTCDay());
                if (diff !== 0) { compDate.setUTCDate(compDate.getUTCDate() + 7 * diff); }
                diff = countDays(compDate, date);
                return (diff >= 0) && (diff < 7);
            case "month":
                if (diff !== 0) { compDate.setUTCMonth(compDate.getUTCMonth() + diff); }
                return (compDate.getUTCFullYear() === date.getUTCFullYear()) && (compDate.getUTCMonth() === date.getUTCMonth());
            case "year":
                if (diff !== 0) { compDate.setUTCFullYear(compDate.getUTCFullYear() + diff); }
                return compDate.getUTCFullYear() === date.getUTCFullYear();
        }
        return false;
    }

    /**
     * Implementation for average rules. Returns whether the passed cell value
     * matches the average of all numbers covered by this rule.
     */
    #matchesAverage(value: ScalarType, count: ScalarType, subtract: boolean, equal: boolean): boolean {

        // try to convert the passed value to a number
        const number = this.docModel.numberFormatter.convertScalarToNumber(value);
        if (number === null) { return false; }

        // get the arithmetic mean of the target ranges
        const rangeCache = this.ruleBucket.valueCache;
        const mean = rangeCache.getMean();
        if (!Number.isFinite(mean)) { return false; }

        // add/subtract the standard deviations
        count = is.number(count) ? Math.floor(Math.max(count, 0)) : 0;
        const devsq = (count === 0) ? 0 : Math.sqrt(rangeCache.getDeviation());
        const average = mean + devsq * (subtract ? -count : count);
        if (!Number.isFinite(average)) { return false; }

        // compare the resulting average with the passed number
        const comp = compareNumbers(number, average);
        return (comp === CompareResult.EQUAL) ? equal : (comp === (subtract ? CompareResult.LESS : CompareResult.GREATER));
    }

    /**
     * Implementation for rank rules. Returns whether the passed cell value
     * matches the rank in the numbers covered by this rule.
     */
    #matchesRank(value: ScalarType, rank: ScalarType, percent: boolean, top: boolean): boolean {

        // rank rule applies to numbers only, other cells will never match
        if (!is.number(value) || !is.number(rank)) { return false; }

        // the sorted numbers in the cell ranges
        const numbers = this.ruleBucket.valueCache.getSortedNumbers();
        // the count of the numbers
        const count = numbers.length;

        // calculate a percentual rank (round down, but at least 1, regardless of exact result)
        rank = percent ? Math.max(1, Math.floor(rank / 100 * count)) : Math.floor(rank);

        // compare passed value with the n-th array element
        if (rank < 1) { return false; }
        if (rank > count) { return true; }
        return top ? (numbers[count - rank] <= value) : (value <= numbers[rank - 1]);
    }

    /**
     * Implementation for quantity rules. Returns whether the passed cell is
     * (not) unique in the cells covered by this rule.
     */
    #matchesQuantity(value: ScalarType, matchUnique: boolean): boolean {

        // blank cells are neither unique nor duplicate
        if (value === null) { return false; }

        // empty strings (as formula result) are considered to be duplicate to blank cells
        const rangeCache = this.ruleBucket.valueCache;
        if ((value === "") && (rangeCache.getBlankCount() > 0)) { return !matchUnique; }

        // the value cache has counted all scalar values
        const isUnique = rangeCache.countScalar(value) === 1;
        return matchUnique === isUnique;
    }

    /**
     * Implementation for blank rules. Returns whether the passed cell value is
     * (not) blank. Strings consisting of space characters only are considered
     * to be blank too.
     */
    #matchesBlank(value: ScalarType, matchBlank: boolean): boolean {
        const isBlank = (value === null) || (is.string(value) && /^ *$/.test(value));
        return matchBlank === isBlank;
    }

    /**
     * Implementation for error rules. Returns whether the passed cell value is
     * (not) an error code.
     */
    #matchesError(value: ScalarType, matchError: boolean): boolean {
        const isError = isErrorCode(value);
        return matchError === isError;
    }

    /**
     * Returns the effective number for the specified token array, and a
     * specific result type, for a limiting value in range rules.
     *
     * @param tokenKey
     *  The map key of a token array.
     *
     * @param type
     *  One of the following type specifiers:
     *  - "formula": The passed value will be converted to a number, and
     *    returned.
     *  - "min": The minimum of the numbers of the parent formatting model will
     *    be returned. The passed cell value will be ignored.
     *  - "max": The maximum of the numbers of the parent formatting model will
     *    be returned. The passed cell value will be ignored.
     *  - "percent": The passed cell value will be interpreted as percent, and
     *    a scaled number between the minimum and maximum of the parent
     *    formatting model will be returned.
     *  - "percentile": The passed cell value will be interpreted as
     *    percentile, and a scaled number between the minimum and maximum of
     *    the parent formatting model will be returned.
     *
     * @returns
     *  The resulting number according to the specified type; or `null`, if no
     *  valid number could be resolved.
     */
    #getLimitForType(tokenKey: CFRuleTokenKey, type: string): number | null {

        // ignore the passed cell value for minimum/maximum
        switch (type) {
            case "min":     return this.#getMinNumber(false);
            case "max":     return this.#getMaxNumber(false);
            case "automin": return this.#getMinNumber(true);
            case "automax": return this.#getMaxNumber(true);
        }

        // try to convert the passed scalar to a number
        const value = this.#interpretCondition(tokenKey);
        const number = this.docModel.numberFormatter.convertScalarToNumber(value);
        if (number === null) { return null; }

        switch (type) {
            case "formula":    return number;
            case "percent":    return this.#getPercentNumber(number);
            case "percentile": return this.#getPercentileNumber(number);
        }

        modelLogger.error(`$badge{CFRuleModel} getLimitForType: invalid type "${type}"`);
        return null;
    }

    /**
     * Returns the effective formatting attributes for a scalar cell value, and
     * the passed color scale settings, according to the numbers covered by the
     * parent formatting rule.
     *
     * @param value
     *  A scalar cell value of any type.
     *
     * @returns
     *  The cell attribute with the effective fill color for the passed cell
     *  value and color scale properties; or `undefined`, if no fill color
     *  could be calculated.
     */
    #getColorScaleProperties(value: ScalarType): Opt<CFRuleRenderProps> {

        // creates the `CFRuleRenderProps` from an operation JSON color
        const createRenderProps = (color: OpColor): Opt<CFRuleRenderProps> => {
            const colorDesc = Color.parseJSON(color).resolve(AutoColorType.FILL, this.docModel.getThemeModel());
            return { colorScale: { color: colorDesc.css } };
        };

        // cell value must be a real number (no type conversion)
        if (!is.number(value)) { return undefined; }

        // check validity of the color scale properties
        const { colorScale } = this.#getRuleAttrs();
        if (colorScale.length < 2) { return undefined; }

        // calculate the effective limits of all color steps
        const limits = colorScale.map((colorStep, index) => this.#getLimitForType(`color:${index}`, colorStep.t));

        // exit if a single limit could not be calculated
        if (limits.some(limit => limit === null)) { return undefined; }

        // sort the limits (without reordering the color stops!)
        const limits2 = limits as number[];
        limits2.sort(math.compare);

        // return end color, if the cell value is greater than or equal to the highest limit
        // (bug 53248: test maximum first, end color wins in case minimum and maximum are equal)
        if (value >= ary.last(limits2)!) { return createRenderProps(ary.last(colorScale)!.c); }

        // return start color, if the cell value is less than or equal to the lowest limit
        if (value <= limits2[0]) { return createRenderProps(colorScale[0].c); }

        // find the index of the first color stop that follows or equals the cell value
        // (result is a valid array index greater than 0 because of the limit checks above)
        const index = ary.fastFindFirstIndex(limits2, limit => value <= limit);

        // return the color stop value, if the cell value is exactly equal to the limit
        if (limits2[index] === value) { return createRenderProps(colorScale[index].c); }

        // create a mixed color, according to the start and end of the interval
        const ratio = (value - limits2[index - 1]) / (limits2[index] - limits2[index - 1]);
        const color1 = Color.parseJSON(colorScale[index - 1].c);
        const color2 = Color.parseJSON(colorScale[index].c);
        const mixedColor = color1.resolveMixed(color2, ratio, AutoColorType.FILL, this.#themeModel);
        return { colorScale: { color: mixedColor.css } };
    }

    /**
     * Returns the properties to render a data bar into a cell.
     *
     * @param value
     *  A scalar cell value of any type.
     *
     * @returns
     *  If the passed cell value is a number, the rendering properties for a
     *  data bar will be returned (properties `dataBar` and `hideValue` will be
     *  set); otherwise `undefined`.
     */
    #getDataBarProperties(value: ScalarType): Opt<CFRuleRenderProps> {

        // data bars only for number cells (no type conversion)
        if (!is.number(value)) { return undefined; }

        // the rule settings containing the original data bar properties
        const { dataBar } = this.#getRuleAttrs();
        if (!is.dict(dataBar) || !is.dict(dataBar.r1) || !is.dict(dataBar.r2)) { return undefined; }

        // resolve the interval of the color scale
        const limit1 = this.#getLimitForType("data:r1", dataBar.r1.t);
        const limit2 = this.#getLimitForType("data:r2", dataBar.r2.t);
        if ((limit1 === null) || (limit2 === null)) { return undefined; }

        // create an instance of the Color class for further processing
        const color = Color.parseJSON(dataBar.c);
        // the CSS value of the solid color or start color of a gradient
        const cssColor1 = color.resolve(AutoColorType.FILL, this.#themeModel).css;
        // the CSS value of the end color of a gradient
        const cssColor2 = dataBar.g ? color.transform("lumMod", 30000).transform("lumOff", 70000).resolve(AutoColorType.FILL, this.#themeModel).css : null;

        // the resulting data bar properties
        return {
            hideValue: !dataBar.s,
            dataBar: {
                min: Math.min(limit1, limit2),
                max: Math.max(limit1, limit2),
                num: value,
                color1: cssColor1,
                color2: cssColor2
            }
        };
    }

    /**
     * Returns the properties to render an icon of an icon set into a cell.
     *
     * @param value
     *  A scalar cell value of any type.
     *
     * @returns
     *  If the passed cell value is a number, the rendering properties for an
     *  icon of an icon set will be returned (properties `iconSet` and
     *  `hideValue` will be set); otherwise `undefined`.
     */
    #getIconSetProperties(value: ScalarType): Opt<CFRuleRenderProps> {

        // icon sets only for number cells (no type conversion)
        if (!is.number(value)) { return undefined; }

        // the rule settings containing the original icon set properties
        const { iconSet } = this.#getRuleAttrs();

        // find the first matching icon rule (start at last array element)
        let iconIndex = -1;
        for (const [ruleIdx, iconRule] of ary.entries(iconSet.ir, { reverse: true })) {

            // fail completely if a rule is invalid (e.g. limit is not a number)
            const limit = this.#getLimitForType(`icon:${ruleIdx}`, iconRule.t);
            if (limit === null) { break; }

            // check the limit, and exit the loop for the first matching icon rule
            if (iconRule.gte ? (value >= limit) : (value > limit)) {
                iconIndex = ruleIdx;
                break;
            }
        }

        // no matching icon rule found
        if (iconIndex < 0) { return undefined; }

        // the identifier of the default icon set
        let iconSetId = iconSet.is;
        // the matching icon rule
        const iconRule = iconSet.ir[iconIndex];

        // further adjustments according to the icon set rule
        if (iconSet.c && is.string(iconRule.is) && iconRule.is && is.number(iconRule.id)) {
            // use a custom icon if specified by the icon set rule
            iconSetId = iconRule.is;
            iconIndex = iconRule.id;
        } else if (iconSet.r) {
            // reverse the order of the icons in the default icon set
            iconIndex = iconSet.ir.length - iconIndex - 1;
        }

        // the resulting icon set properties
        return {
            hideValue: !iconSet.s,
            iconSet: {
                id: iconSetId.toLowerCase(),
                index: iconIndex
            }
        };
    }

    /**
     * Transforms all formula expressions.
     *
     * @param transform
     *  The transformation callback function. Receives the token array of a
     *  formula attribute, and returns the transformed formula expression, or
     *  `undefined` to indicate that the formula will not change.
     *
     * @returns
     *  A pair containing the changed rule attributes, and the corresponding
     *  original rule attributes.
     */
    #transformFormulas(transform: (tokenArray: SheetTokenArray) => Opt<string>): Pair<Partial<CFRuleAttributes>> {

        // the current complete rule attributes
        const ruleAttrs = this.#getRuleAttrs();
        // attributes with transformed formula expressions
        const newRuleAttrs = dict.createPartial<CFRuleAttributes>();
        // attributes with original formula expressions (neded to undo the changes)
        const oldRuleAttrs = dict.createPartial<CFRuleAttributes>();

        // process token arrays of all existing formula expressions
        for (const { propType, propKey, tokenKey } of yieldCFRuleFormulas(ruleAttrs)) {

            // try to transform existing token array of the visited property
            const tokenArray = this.#tokenArrayMap.get(tokenKey);
            const newExpr = tokenArray && transform(tokenArray);
            if (!newExpr) { continue; }

            // create old and new entries in the rule attribute bags
            switch (propType) {
                case "value":
                    newRuleAttrs[propKey] = newExpr;
                    oldRuleAttrs[propKey] = ruleAttrs[propKey];
                    break;
                case "color":
                    newRuleAttrs.colorScale ??= json.deepClone(ruleAttrs.colorScale);
                    newRuleAttrs.colorScale[propKey].v = newExpr;
                    oldRuleAttrs.colorScale ??= ruleAttrs.colorScale;
                    break;
                case "data":
                    newRuleAttrs.dataBar ??= json.deepClone(ruleAttrs.dataBar);
                    newRuleAttrs.dataBar[propKey].v = newExpr;
                    oldRuleAttrs.dataBar ??= ruleAttrs.dataBar;
                    break;
                case "icon":
                    newRuleAttrs.iconSet ??= json.deepClone(ruleAttrs.iconSet);
                    newRuleAttrs.iconSet.ir[propKey].v = newExpr;
                    oldRuleAttrs.iconSet ??= ruleAttrs.iconSet;
                    break;
            }
        }

        return [newRuleAttrs, oldRuleAttrs];
    }
}
