/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, map } from "@/io.ox/office/tk/algorithms";
import { EObject } from "@/io.ox/office/tk/objects";

import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";

import { CellType } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { CellEntry, CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

const { min, max } = Math;

// types ======================================================================

/**
 * Type mapping for the events emitted by `CellValueCache` instances.
 */
export interface CellValueCacheEventMap {

    /**
     * Will be emitted after the value of at least one cell covered by the cell
     * value cache has been changed in the document (cache invalidation event).
     * The cache has invalidated itself already before emitting the event.
     */
    invalidate: [];
}

// private types --------------------------------------------------------------

interface ValueCacheEntry {
    countMap: Map<ScalarType, number>;
    valueCount: number;
    blankCount: number;
    numbers: number[];
    sum: number;
    product: number;
    min: number;
    max: number;
    mean: number;
}

// private functions ==========================================================

function scalarKey(value: ScalarType): ScalarType {
    return is.string(value) ? value.toLowerCase() : value;
}

// class CellValueCache =======================================================

/**
 * A cache for the values, and specific subtotal results, of the cells covered
 * by specific cell ranges in a sheet.
 *
 * The cache will collect all necessary information lazily on first access. It
 * will register itself as a change listener at the cell collection, and will
 * invalidate itself whenever the values in the covered cell ranges have been
 * changed.
 *
 * Will emit an "invalidate" event after the value of at least one cell covered
 * by this cache has been changed in the document (cache invalidation event).
 * This cache has invalidated itself already before emitting the event.
 */
export class CellValueCache extends EObject<CellValueCacheEventMap> {

    // a map used as cache for various results
    readonly #cache = new Map<string, unknown>();

    // the cell collection observed for cell value changes
    readonly #collection: CellCollection;

    // the current target ranges in the cell collection
    #ranges: RangeArray;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model containing the cells to be cached by this instance.
     *
     * @param [targetRanges]
     *  The cell ranges to be cached by this instance. If omitted, the cache
     *  will not cover any ranges. New target ranges can be set later with the
     * method `setRanges`.
     */
    constructor(sheetModel: SheetModel, targetRanges?: RangeSource) {
        super();

        // the cell collection to be observed
        this.#collection = sheetModel.cellCollection;

        // create a clone of the passed cell range addresses
        this.#ranges = RangeArray.cast(targetRanges).deepClone();

        // clear the cached subtotals when the values of the covered cells have changed
        this.listenTo(this.#collection, "change:cells", event => {

            // nothing to do, if the cache is empty (do not waste time to evaluate the passed addresses),
            // or if no cell values have been changed (e.g. formatting only)
            const { valueCells } = event;
            if (this.#ranges.empty() || !this.#cache.size || valueCells.empty()) { return; }

            // check if the cells covered by this formatting model have changed their values (ignore formatting)
            const boundary = valueCells.boundary()!;
            if (this.#ranges.some(range => range.overlaps(boundary) && valueCells.some(address => range.containsAddress(address)))) {
                this.invalidate();
            }
        });

        // invalidate the cache, if cells are moved around
        this.listenTo(this.#collection, "move:cells", event => {
            if (this.#ranges.overlaps(event.transformer.dirtyRange)) {
                this.invalidate();
            }
        });
    }

    protected override destructor(): void {
        this.#cache.clear();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the number of non-blank cells covered by the target ranges.
     *
     * @returns
     *  The number of non-blank cells covered by the target ranges.
     */
    getValueCount(): number {
        return this.#collectSettings().valueCount;
    }

    /**
     * Returns the number of blank cells covered by the target ranges.
     *
     * @returns
     *  The number of blank cells covered by the target ranges.
     */
    getBlankCount(): number {
        return this.#collectSettings().blankCount;
    }

    /**
     * Returns the number of occurrences of the specified scalar value.
     *
     * @param value
     *  The scalar value to be counted.
     *
     * @returns
     *  The number of occurrences of the specified scalar value.
     */
    countScalar(value: ScalarType): number {
        const settings = this.#collectSettings();
        return (value === null) ? settings.blankCount : (settings.countMap.get(scalarKey(value)) || 0);
    }

    /**
     * Returns the numbers from all cells covered by this cache as array.
     *
     * @returns
     *  The numbers from all cells covered by this cache as array.
     */
    getNumbers(): readonly number[] {
        return this.#collectSettings().numbers;
    }

    /**
     * Returns all numbers covered by this model as sorted array.
     *
     * @returns
     *  All numbers covered by this model as sorted array.
     */
    getSortedNumbers(): readonly number[] {
        return this.#upsert("sorted", () => this.getNumbers().slice().sort(math.compare));
    }

    /**
     * Returns all numbers covered by this model as unified and sorted array.
     *
     * @returns
     *  All numbers covered by this model as unified and sorted array.
     */
    getUniqueNumbers(): readonly number[] {
        return this.#upsert("unified", () => Array.from(new Set(this.getSortedNumbers())));
    }

    /**
     * Returns the sum of all covered numbers.
     *
     * @returns
     *  The sum of all covered numbers.
     */
    getSum(): number {
        return this.#collectSettings().sum;
    }

    /**
     * Returns the product of all covered numbers.
     *
     * @returns
     *  The product of all covered numbers.
     */
    getProduct(): number {
        return this.#collectSettings().product;
    }

    /**
     * Returns the smallest of the covered numbers.
     *
     * @returns
     *  The smallest of the covered numbers; or `Infinity`, if the covered
     *  cells do not contain any numbers.
     */
    getMin(): number {
        return this.#collectSettings().min;
    }

    /**
     * Returns the largest of the covered numbers.
     *
     * @returns
     *  The largest of the covered numbers; or `-Infinity`, if the covered
     *  cells do not contain any numbers.
     */
    getMax(): number {
        return this.#collectSettings().max;
    }

    /**
     * Returns the arithmetic mean of all covered numbers.
     *
     * @returns
     *  The arithmetic mean of all covered numbers; or `NaN`, if the covered
     *  cells do not contain any numbers.
     */
    getMean(): number {
        return this.#collectSettings().mean;
    }

    /**
     * Returns the standard deviation of all covered numbers.
     *
     * @returns
     *  The standard deviation of all covered numbers; or `NaN`, if the covered
     *  cells do not contain any numbers.
     */
    getDeviation(): number {
        return this.#upsert("deviation", () => {
            const { numbers, mean } = this.#collectSettings();
            if (numbers.length === 0) { return Number.NaN; }
            return numbers.reduce((sum, num) => {
                const diff = num - mean;
                return sum + diff * diff;
            }, 0) / numbers.length;
        });
    }

    /**
     * Returns a custom cache entry. If the cache entry does not exist yet, it
     * will be created by invoking the passed callback function.
     *
     * @param key
     *  A unique key for the custom cache entry.
     *
     * @param fn
     *  A callback function that will be invoked if this cache does not contain
     *  an entry with the specified custom key. Receives a cell iterator that
     *  will visit all (non-blank) value cells of the target ranges covered by
     *  this value cache. The return value of the callback function will be
     *  inserted into this cache.
     *
     * @returns
     *  The cached entry for the specified custom key.
     */
    getCustom<T>(key: string, fn: (iterator: IterableIterator<CellEntry>) => T): T {
        return this.#upsert(`custom:${key}`, () => fn(this.#yieldCells()));
    }

    /**
     * Invalidates this cache. The next invocation of an accessor method will
     * collect the covered cell values again.
     */
    invalidate(): void {
        this.#cache.clear();
        this.trigger("invalidate");
    }

    /**
     * Changes the addresses of the cell ranges covered by this instance, and
     * invalidates the cache.
     *
     * @param [ranges]
     *  The new target ranges for this cache. Can be omitted or set to `null`
     * to remove the current target ranges.
     */
    setRanges(ranges: RangeSource): void {
        this.#ranges = RangeArray.cast(ranges).deepClone();
        this.invalidate();
    }

    // private methods --------------------------------------------------------

    #upsert<T>(key: string, fn: (key: string) => T): T {
        return map.upsert(this.#cache, key, fn) as T;
    }

    #yieldCells(): IterableIterator<CellEntry> {
        return this.#collection.cellEntries(this.#ranges, { type: CellType.VALUE });
    }

    #collectSettings(): ValueCacheEntry {
        return this.#upsert("settings", () => {

            // initialization
            const countMap = new Map<ScalarType, number>();
            let valueCount = 0;
            const numbers: number[] = [];
            let sum = 0;
            let product = 1;
            let minV = Infinity;
            let maxV = -Infinity;

            // collect the numbers of all covered cells
            for (const { value } of this.#yieldCells()) {
                map.add(countMap, scalarKey(value), 1);
                valueCount += 1;
                if (is.number(value)) {
                    numbers.push(value);
                    sum += value;
                    product *= value;
                    minV = min(minV, value);
                    maxV = max(maxV, value);
                }
            }

            // return the cache entry structure
            return {
                countMap,
                valueCount,
                blankCount: this.#ranges.cells() - valueCount,
                numbers,
                sum,
                product,
                min: minV,
                max: maxV,
                mean: (numbers.length > 0)  ? (sum / numbers.length) : Number.NaN
            };
        });
    }
}
