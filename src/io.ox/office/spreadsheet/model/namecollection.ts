/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map, dict, jpromise } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { modelLogger, mapKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { type OptSheetEventBase, createSheetEvent } from "@/io.ox/office/spreadsheet/model/sheetevents";
import type { NameFormulaSpec, NameModelConfig } from "@/io.ox/office/spreadsheet/model/namemodel";
import { getFormulaProperties, validateLabel, NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import { OperationBuilder } from "@/io.ox/office/spreadsheet/model/operation/builder";
import type { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { type FormulaParentModel, resolveParentModel } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Optional parameters to control which defined names will be selected by a
 * model iterator.
 */
export interface NameIteratorOptions {

    /**
     * If set to true, hidden defined names will not be visited. Default value
     * is `false`.
     */
    skipHidden?: boolean;
}

export interface NameEvent extends OptSheetEventBase {

    /**
     * The model instance of the defined name causing the event.
     */
    nameModel: NameModel;
}

export interface ChangeNameEvent extends NameEvent {

    /**
     * The old and new label of the defined name, if it has been changed.
     */
    label?: Pair<string>;

    /**
     * The old and new formula expression of the defined name, if it has been
     * changed.
     */
    formula?: Pair<string>;
}

/**
 * Type mapping for the events emitted by `NameCollection` instances.
 */
export interface NameCollectionEventMap {

    /**
     * Will be emitted before a new defined name will be inserted into a name
     * collection.
     *
     * @param event
     *  Event object with the model instance of the new defined name.
     */
    "insert:name:before": [event: NameEvent];

    /**
     * Will be emitted after a new defined name has been inserted into a name
     * collection.
     *
     * @param event
     *  Event object with the model instance of the new defined name.
     */
    "insert:name": [event: NameEvent];

    /**
     * Will be emitted before a defined name will be deleted from a name
     * collection.
     *
     * @param event
     *  Event object with the model instance of the defined name about to be
     *  deleted.
     */
    "delete:name:before": [event: NameEvent];

    /**
     * Will be emitted after a defined name has been deleted from a name
     * collection.
     *
     * @param event
     *  Event object with the model instance of the deleted defined name.
     */
    "delete:name": [event: NameEvent];

    /**
     * Will be emitted before the label, or the formula definition of a defined
     * name will be modified.
     *
     * @param event
     *  The event object with the model instance of the defined name that will
     *  be changed.
     */
    "change:name:before": [event: ChangeNameEvent];

    /**
     * Will be emitted after the label, or the formula definition of a defined
     * name has been modified.
     *
     * @param event
     *  The event object with the model instance of the defined name that has
     *  been changed.
     */
    "change:name": [event: ChangeNameEvent];
}

// class NameCollection =======================================================

/**
 * Collects all global defined names of the spreadsheet document, or the
 * defined names contained in a single sheet of the document.
 */
export class NameCollection extends ModelObject<SpreadsheetModel, NameCollectionEventMap> {

    /**
     * The parent document model of global names, or sheet model of sheet-local
     * names.
     */
    readonly parentModel: FormulaParentModel;

    /**
     * The parent sheet model of sheet-local names.
     */
    readonly sheetModel: Opt<SheetModel>;

    // the models of all defined names, mapped by upper-case name (map has ownership)
    readonly #nameModelMap = this.member(new Map<string, NameModel>());
    // special behavior for OOXML files
    readonly #oox: boolean;
    // special behavior for ODF files
    readonly #odf: boolean;

    // constructor ------------------------------------------------------------

    /**
     * @param parentModel
     *  The spreadsheet document model for global names, or the sheet model for
     *  sheet-local names containing this instance.
     */
    constructor(parentModel: FormulaParentModel) {

        const { docModel, sheetModel } = resolveParentModel(parentModel);
        super(docModel);

        this.parentModel = parentModel;
        this.sheetModel = sheetModel;

        this.#oox = this.docApp.isOOXML();
        this.#odf = this.docApp.isODF();

        // additional processing and event handling after the document has been imported
        this.waitForImportSuccess(alreadyImported => {

            // refresh all formulas after import is finished (formulas may refer to sheets not yet imported)
            if (!alreadyImported) {
                for (const nameModel of this.yieldNameModels()) {
                    nameModel.tokenArray.refreshAfterImport(Address.A1);
                }
            }

            // update the sheet indexes after the sheet collection has been manipulated
            this.listenTo(this.docModel, "transform:sheets", xfVector => {
                for (const nameModel of this.yieldNameModels()) {
                    nameModel.tokenArray.transformSheets(xfVector);
                }
            });
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this collection contains a defined name with the passed
     * label.
     *
     * @param label
     *  The (case-insensitive) label of a defined name.
     *
     * @returns
     *  Whether this collection contains a defined name with the passed label.
     */
    hasName(label: string): boolean {
        return this.#nameModelMap.has(mapKey(label));
    }

    /**
     * Returns the model of the specified defined name.
     *
     * @param label
     *  The (case-insensitive) label of the defined name to be returned.
     *
     * @returns
     *  The model of the specified defined name, if existing.
     */
    getNameModel(label: string): Opt<NameModel> {
        return this.#nameModelMap.get(mapKey(label));
    }

    /**
     * Creates an iterator that visits the models of all defined names in this
     * collection.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The models of the defined names.
     */
    *yieldNameModels(options?: NameIteratorOptions): IterableIterator<NameModel> {
        if (options?.skipHidden) {
            for (const nameModel of this.#nameModelMap.values()) {
                if (!nameModel.hidden) {
                    yield nameModel;
                }
            }
        } else {
            yield* this.#nameModelMap.values();
        }
    }

    /**
     * Returns whether the passed label exists already, either as defined name
     * in this collection, or as name of a table range anywhere in the
     * document.
     *
     * @param label
     *  The label for a defined name to be checked.
     *
     * @returns
     *  Whether the passed label exists already.
     */
    isLabelUsed(label: string): boolean {
        return this.hasName(label) || this.docModel.hasTable(label);
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations, and the undo operations, to insert a new
     * defined name into this name collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param label
     *  The exact label for the new defined name.
     *
     * @param fmlaSpec
     *  Configuration for the formula expression to be bound to the new defined
     *  name.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated, or
     *  that will reject with an object with "cause" property set to one of the
     *  following error codes:
     *  - "name:used": A defined name, or a table range, with the passed label
     *    exists already.
     *  - "name:empty": The specified label is empty.
     *  - "name:invalid": The specified label contains invalid characters.
     *  - "name:address": The specified label would be valid, but conflicts
     *    with the representation of a relative cell reference in A1 notation,
     *    or a cell reference in R1C1 notation (either English, e.g. "R1C1", or
     *    according to current UI language, e.g. "Z1S1" in German).
     *  - "formula:invalid": The passed formula expression is invalid.
     */
    generateInsertNameOperations(builder: OperationBuilder, label: string, fmlaSpec: NameFormulaSpec, options?: NameModelConfig): JPromise {

        // the label must be valid
        validateLabel(this.docModel, label);

        // check that the defined name does not exist yet (check table ranges in the entire document too)
        OperationError.assertCause(!this.isLabelUsed(label), "name:used");

        // parse and validate the formula expression
        const operProps = getFormulaProperties(this.parentModel, fmlaSpec) as Opt<Dict>;
        OperationError.assertCause(operProps, "formula:invalid");
        const undoProps = dict.create();

        // add the label, and optionally the sheet index, to the operation properties
        operProps.label = undoProps.label = label;
        if (this.sheetModel) {
            operProps.sheet = undoProps.sheet = this.sheetModel.getIndex();
        }

        // additional properties
        if (options?.hidden) {
            operProps.hidden = true;
        }

        // generate the "insertName" operation, and the undo operation
        const generator = builder.headGenerator;
        generator.generateOperation(Op.INSERT_NAME, operProps);
        generator.generateOperation(Op.DELETE_NAME, undoProps, { undo: true });
        return jpromise.resolve();
    }

    /**
     * Generates the operations, and the undo operations, to delete an existing
     * defined name from this name collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param label
     *  The label of the defined name to be deleted.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateDeleteNameOperations(builder: OperationBuilder, label: string): JPromise {

        // check that the name exists
        const nameModel = this.getNameModel(label);
        OperationError.assertCause(nameModel, "operation");

        // generate the "deleteName" operation, and the undo operation
        nameModel.generateDeleteOperations(builder);
        return jpromise.resolve();
    }

    /**
     * Generates the operations, and the undo operations, to change the label
     * or formula definition of an existing defined name in this name
     * collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param label
     *  The label of the defined name to be changed.
     *
     * @param newLabel
     *  The new label for the defined name. If set to `null`, the label of the
     *  defined name will not be changed.
     *
     * @param fmlaSpec
     *  Configuration for the formula expression to be bound to the defined
     *  name. If set to `null`, the formula expression of the defined name will
     *  not be changed (an invalid formula expression in the defined name will
     *  be retained).
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated, or
     *  that will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - "name:used": A defined name, or a table range, with the passed label
     *    exists already.
     *  - "name:empty": The new label is empty.
     *  - "name:invalid": The new label contains invalid characters.
     *  - "name:address": The new label would be valid, but conflicts with the
     *    representation of a relative cell reference in A1 notation, or a cell
     *    reference in R1C1 notation (either English, e.g. "R1C1", or according
     *    to current UI language, e.g. "Z1S1" in German).
     *  - "formula:invalid": The passed formula expression is invalid.
     *  - "operation": Internal error while creating the operations.
     */
    generateChangeNameOperations(builder: OperationBuilder, label: string, newLabel: string | null, fmlaSpec: NameFormulaSpec | null): JPromise {

        // check that the name exists
        const nameModel = this.getNameModel(label);
        OperationError.assertCause(nameModel, "operation");

        // generate the "changeName" operation, and the undo operation
        return nameModel.generateChangeOperations(builder, newLabel, fmlaSpec);
    }

    /**
     * Generates the operations and undo operations to create or update a
     * defined name referring to a specific cell range in the own sheet. This
     * method will fail if called for a global name collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param label
     *  The label of the defined name to be created or updated.
     *
     * @param range
     *  The cell range address to be referred by the defined name.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated, or
     *  that will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - "name:empty": The new label is empty.
     *  - "name:invalid": The new label contains invalid characters.
     *  - "name:address": The new label would be valid, but conflicts with the
     *    representation of a relative cell reference in A1 notation, or a cell
     *    reference in R1C1 notation (either English, e.g. "R1C1", or according
     *    to current UI language, e.g. "Z1S1" in German).
     *  - "operation": Internal error while creating the operations.
     */
    generateNamedRangeOperations(builder: OperationBuilder, label: string, range: Range, options?: NameModelConfig): JPromise {

        // sheet-local names only
        OperationError.assertCause(this.sheetModel, "operation");
        const sheet = this.sheetModel.getIndex();

        // create the formula expression for the passed cell range
        const tokenArray = new TokenArray(this.parentModel, "name");
        tokenArray.appendRange(range, { sheet, abs: true });
        const formula = tokenArray.getFormulaOp();

        // create the formula configuration data
        const fmlaSpec: NameFormulaSpec = { grammarType: "op", formula, refSheet: sheet, refAddress: Address.A1 };

        // update an existing name, or create a new name
        return this.hasName(label) ?
            this.generateChangeNameOperations(builder, label, null, fmlaSpec) :
            this.generateInsertNameOperations(builder, label, fmlaSpec, options);
    }

    /**
     * Generates the operations to create or update the special defined name
     * representing the auto-filter of the own sheet in OOXML documents. This
     * method will fail if called for a global name collection.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param range
     *  The address of the cell range covered by the auto-filter.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateFilterDatabaseOperations(generator: SheetOperationGenerator, range: Range): JPromise {
        // TODO: called from table collection which needs to pass an operation builder!
        const builder = new OperationBuilder(this.docModel);
        let promise = this.#oox ? this.generateNamedRangeOperations(builder, "_xlnm._FilterDatabase", range, { hidden: true }) : jpromise.resolve();
        promise = promise.then(() => {
            const result = builder.flushOperations();
            generator.appendOperations(result.generator);
            generator.appendOperations(result.generator, { undo: true });
        });
        promise.always(() => builder.destroy());
        return promise;
    }

    /**
     * Generates the operations and undo operations to update or restore the
     * formula expressions of the defined names in this collection.
     *
     * @param generator
     *  The operation generator to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{NameCollection} generateUpdateTaskOperations")
    generateUpdateTaskOperations(generator: SheetOperationGenerator, updateTask: FormulaUpdateTask): JPromise {
        return this.asyncForEach(this.yieldNameModels(), nameModel => {
            nameModel.generateUpdateTaskOperations(generator, updateTask);
        });
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * defined names from the passed collection into this collection.
     *
     * @param context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws
     *  An `OperationError`, if applying the operation fails, e.g. if a
     *  required property is missing in the operation.
     */
    applyCopySheetOperation(context: SheetOperationContext, fromCollection: NameCollection): void {
        context.ensure(this.sheetModel, "copySheet operation not supported for global names");

        // clone the name models of the source collection
        for (const nameModel of fromCollection.yieldNameModels()) {
            this.#insertNameModel(context, nameModel.cloneFor(this.sheetModel));
        }
    }

    /**
     * Callback handler for the document operation "insertName". Creates and
     * stores a new defined name, and triggers an "insert:name:before" event
     * and an "insert:name" event.
     *
     * @param context
     *  A wrapper representing the "insertName" document operation.
     */
    applyInsertOperation(context: SheetOperationContext): void {

        // get the label from the operation
        const label = context.getStr("label");
        context.ensure(!this.hasName(label), "defined name exists");

        // get formula expression (may be empty)
        const formula = context.getStr("formula", { empty: true });

        // create a new name model (formula expression may be empty)
        const nameModel = new NameModel(this.parentModel, label, formula, {
            hidden: context.optBool("hidden"),
            ref: this.#odf ? context.getStr("ref") : undefined
        });

        // insert the model into the map, notify listeners
        this.#insertNameModel(context, nameModel);
    }

    /**
     * Callback handler for the document operation "deleteName". Deletes an
     * existing defined name from this collection, and (before) triggers a
     * "delete:name:before" event and a "delete:name" event.
     *
     * @param context
     *  A wrapper representing the "deleteName" document operation.
     */
    applyDeleteOperation(context: SheetOperationContext): void {

        // get the name model addressed by the operation
        const label = context.getStr("label");
        const nameModel = this.#nameModelMap.get(mapKey(label));
        context.ensure(nameModel, "missing defined name");

        // delete the name model and notify listeners
        const event: NameEvent = this.#createNameEvent(context, nameModel);
        this.trigger("delete:name:before", event);
        this.#nameModelMap.delete(nameModel.key);
        this.trigger("delete:name", event);
        nameModel.destroy();
    }

    /**
     * Callback handler for the document operation "changeName". Changes the
     * label or formula definition of an existing defined name, and emits a
     * "change:name:before", and a "change:name" event.
     *
     * @param context
     *  A wrapper representing the "changeName" document operation.
     */
    applyChangeOperation(context: SheetOperationContext): void {

        // get the name model addressed by the operation
        const label = context.getStr("label");
        const nameModel = this.#nameModelMap.get(mapKey(label));
        context.ensure(nameModel, "missing defined name");

        // get new label (will be checked to be non-empty)
        const oldLabel = nameModel.label;
        const newLabel = context.optStr("newLabel") || oldLabel;
        const oldKey = nameModel.key;
        const labelChanged = oldLabel !== newLabel;
        if (labelChanged) {
            const newKey = mapKey(newLabel);
            // DOCS-3304: allow to change character case of existing name
            context.ensure((oldKey === newKey) || !this.#nameModelMap.has(newKey), "name exists");
        }

        // get new formula expression
        const formulaChanged = context.has("formula");
        const defFormula = formulaChanged ? context.getStr("formula", { empty: true }) : undefined;
        const refFormula = (this.#odf && formulaChanged) ? context.optStr("ref") : undefined;

        // early exit if nothing will change
        if (!labelChanged && !formulaChanged) { return; }

        // notify listeners
        const event: ChangeNameEvent = this.#createNameEvent(context, nameModel, {
            label: labelChanged ? [oldLabel, newLabel] : undefined,
            formula: formulaChanged ? [nameModel.getFormula("op"), defFormula!] : undefined
        });
        this.trigger("change:name:before", event);

        // move the model in the map, if the label has been changed
        if (labelChanged) {
            nameModel.setLabel(newLabel);
            map.move(this.#nameModelMap, oldKey, nameModel.key);
        }

        // set a new formula expression for the model if specified
        if (formulaChanged) {
            nameModel.setFormula(defFormula!, refFormula);
        }

        // notify listeners
        this.trigger("change:name", event);
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a `NameEvent` event object with optional additional properties.
     *
     * @param context
     *  The context wrapper of a document operation.
     *
     * @param nameModel
     *  The model of a defined name.
     *
     * @param eventData
     *  The additional properties for the event object.
     *
     * @returns
     *  The `NameEvent` event object with the passed properties.
     */
    #createNameEvent(context: SheetOperationContext, nameModel: NameModel): NameEvent;
    #createNameEvent<DataT extends object>(context: SheetOperationContext, nameModel: NameModel, eventData: DataT): NameEvent & DataT;
    // implementation
    #createNameEvent(context: SheetOperationContext, nameModel: NameModel, eventData?: object): NameEvent {
        const sheet = this.sheetModel?.getIndex() ?? null;
        return createSheetEvent(this.sheetModel, sheet, { nameModel, ...eventData }, context);
    }

    /**
     * Inserts the passed name model into this collection, and triggers an
     * "insert:name:before" and an "insert:name" event.
     */
    #insertNameModel(context: SheetOperationContext, nameModel: NameModel): void {
        const event: NameEvent = this.#createNameEvent(context, nameModel);
        this.trigger("insert:name:before", event);
        this.#nameModelMap.set(nameModel.key, nameModel);
        this.trigger("insert:name", event);
    }
}
