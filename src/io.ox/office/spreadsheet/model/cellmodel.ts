/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import type { DisplayText, ParsedFormat } from "@/io.ox/office/editframework/model/formatter/baseformatter";

import { type ScalarType, isErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";

import type { SharedFormulaModel } from "@/io.ox/office/spreadsheet/model/formula/sharedformulamodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";

import type { CellAutoStyleCollection } from "@/io.ox/office/spreadsheet/model/style/cellautostylecollection";
import type { GrammarType } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

// types ======================================================================

/**
 * Specifies which type of cells will be visited by a cell iterator.
 */
export enum CellType {

    /**
     * Visit all (defined and undefined) cells in a cell range.
     */
    ALL = "all",

    /**
     * Visit all defined cells that really exist in the sheet (i.e., that have
     * a `CellModel` instance), regardless of their content value (including
     * blank but formatted cells).
     */
    DEFINED = "defined",

    /**
     * Visit all non-blank cells (with any value), regardless if the value is
     * given literally, or is the result of a formula.
     */
    VALUE = "value",

    /**
     * Visit all formula cells (i.e., all normal formula cells, all cells that
     * are part of shared formulas, and the anchor cells of matrix formulas,
     * but not the remaining cells of the matrix formulas).
     */
    FORMULA = "formula",

    /**
     * Visit all formula cells with an actual formula definition (i.e., all
     * normal formula cells, and the anchor cells of shared formulas and matrix
     * formulas, but not the remaining cells of shared formulas and matrix
     * formulas).
     */
    ANCHOR = "anchor"
}

// public functions ===========================================================

/**
 * Returns the value of the passed cell model; or `null` for undefined cells.
 *
 * @param cellModel
 *  The cell model to be tested. Will return `null` for nullish values (i.e.
 *  undefined cells).
 *
 * @returns
 *  The value of the passed cell model.
 */
export function getModelValue(cellModel: Nullable<CellModel>): ScalarType {
    return cellModel?.v ?? null;
}

/**
 * Returns whether the passed cell model is blank (either undefined, or with
 * cell value `null`).
 *
 * @param cellModel
 *  The cell model to be tested. Will return `true` for nullish values (i.e.
 *  undefined cells).
 *
 * @returns
 *  Whether the passed cell model is blank.
 */
export function isBlankCell(cellModel: Nullable<CellModel>): boolean {
    return !cellModel || cellModel.isBlank();
}

// class CellModel ============================================================

/**
 * Light-weight representation of a defined cell in a cell collection.
 *
 * @param address
 *  The address of the cell model.
 */
export class CellModel extends DObject implements Keyable {

    /**
     * The address of the cell represented by this instance.
     */
    a: Address;

    /**
     * The typed value, or formula result, of the cell. The value `null` is
     * used to represent a blank (but formatted) cell.
     */
    v: ScalarType = null;

    /**
     * The native formula expression of the cell (as used in document JSON
     * operations, without the leading equality sign); or `null` for simple
     * value cells without a formula.
     */
    f: string | null = null;

    /**
     * The model of a shared formula this cell refers to.
     */
    sf?: SharedFormulaModel;

    /**
     * The bounding range of the matrix formula starting at this cell; or
     * `null` otherwise.
     */
    mr: Range | null = null;

    /**
     * Whether the cell contains a dynamic matrix formula.
     */
    md = false;

    /**
     * The unique object identifier of the auto-style for the cell.
     */
    suid = "";

    /**
     * The parsed number format for the cell, according to the number format
     * attribute of this cell's auto-style. This is a calculated property
     * depending on the property `suid`.
     */
    pf!: ParsedFormat; // MUST be initialized by caller after construction!

    /**
     * The formatted display string of the cell, according to its current value
     * and number format. May be `null`, if the value cannot be formatted with
     * the current number format. In this case, the cell renderer will show a
     * "railroad track" error (repeated hash marks) in the cell. This is a
     * calculated property depending on the properties `v` and `suid`.
     */
    d: DisplayText = "";

    /**
     * The token array containing the parsed formula expression of this cell.
     * This is a calculated property depending on the property `f`.
     */
    t?: TokenArray;

    /**
     * The dynamic URL returned by a cell formula (HYPERLINK function). This is
     * a calculated property depending on the property `f`.
     */
    url?: string;

    // constructor ------------------------------------------------------------

    constructor(address: Address) {
        super();
        this.a = address.clone();
        // property `pf` MUST be initialized by caller after construction
    }

    protected override destructor(): void {
        this.t?.destroy();
        super.destructor();
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns a unique key for the cell address of this cell model. Intended
     * to be used as map key.
     *
     * @returns
     *  A unique key for the cell address of this cell model.
     */
    get key(): string {
        return this.a.key;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this cell model is blank (no value, and no formula).
     *
     * @returns
     *  Whether this cell model is blank (no value, and no formula).
     */
    isBlank(): boolean {
        return (this.v === null) && (this.f === null);
    }

    /**
     * Returns whether this cell model contains a number value.
     *
     * @returns
     *  Whether this cell model contains a number value.
     */
    isNumber(): boolean {
        return is.number(this.v);
    }

    /**
     * Returns whether this cell model contains a string value.
     *
     * @returns
     *  Whether this cell model contains a string value.
     */
    isText(): boolean {
        return is.string(this.v);
    }

    /**
     * Returns whether this cell model contains a boolean value.
     *
     * @returns
     *  Whether this cell model contains a boolean value.
     */
    isBoolean(): boolean {
        return is.boolean(this.v);
    }

    /**
     * Returns whether the cell contains an error code.
     *
     * @returns
     *  Whether the cell contains an error code.
     */
    isError(): boolean {
        return isErrorCode(this.v);
    }

    /**
     * Returns whether this cell model represents a normal formula cell, i.e.
     * the cell contains a formula expression, but is neither part of a shared
     * formula nor a matrix formula.
     *
     * @returns
     *  Whether this cell model represents a normal formula cell.
     */
    isNormalFormula(): boolean {
        return !!this.f && !this.sf && !this.mr;
    }

    /**
     * Returns whether this cell model is part of a shared formula, regardless
     * whether the cell is the anchor of the shared formula.
     *
     * @returns
     *  Whether this cell model is part of a shared formula.
     */
    isSharedFormula(): boolean {
        return !!this.sf;
    }

    /**
     * Returns whether this cell model contains a formula, either as normal
     * formula cell, or as part of a shared formula, or as the anchor cell of a
     * matrix formula.
     *
     * @returns
     *  Whether this cell model represents a formula cell.
     */
    isAnyFormula(): boolean {
        return !!this.f || !!this.sf || !!this.mr;
    }

    /**
     * Returns whether this cell model is the anchor cell of a shared formula.
     *
     * @returns
     *  Whether this cell model is the anchor cell of a shared formula.
     */
    isSharedAnchor(): boolean {
        return !!this.sf?.anchorAddress.equals(this.a);
    }

    /**
     * Returns whether this cell model is the anchor cell of a matrix formula.
     *
     * @returns
     *  Whether this cell model is the anchor cell of a matrix formula.
     */
    isMatrixAnchor(): boolean {
        return !!this.mr;
    }

    /**
     * Returns whether this cell model contains an actual formula expression,
     * either as normal formula cell, or as anchor cell of a shared formula or
     * matrix formula.
     *
     * @returns
     *  Whether this cell model contains an actual formula expression.
     */
    isAnyAnchor(): boolean {
        return !!this.f || this.isSharedAnchor() || !!this.mr;
    }

    /**
     * Returns the formula expression of this cell model.
     *
     * @param grammarType
     *  The identifier of the formula grammar for the formula expression.
     *
     * @param [targetAddress]
     *  The target address used to relocate the formula expression. If omitted,
     *  the formula will be generated for the own address of this cell model.
     *
     * @returns
     *  The formula expression of this cell model; or `null`, if the cell model
     *  does not contain a formula expression.
     */
    getFormula(grammarType: GrammarType, targetAddress?: Address): string | null {

        // resolve shared formula via its model instance
        if (this.sf) {
            return this.sf.getFormula(grammarType, targetAddress ?? this.a);
        }

        // no formula exists
        if (!this.t) { return null; }

        // shortcut for operation grammar without relocation: return the property `f` directly
        return ((grammarType === "op") && !targetAddress) ? this.f :
            this.t.getFormula(grammarType, this.a, targetAddress ?? this.a);
    }

    /**
     * Returns the index of a shared formula this cell is member of.
     *
     * @returns
     *  The index of the shared formula this cell is member of; otherwise
     *  `null`.
     */
    getSharedIndex(): number | null {
        return this.sf?.index ?? null;
    }

    /**
     * Changes the value of this cell model, and returns whether the cell value
     * has changed.
     *
     * @param value
     *  The new value for the cell.
     *
     * @returns
     *  Whether the cell value has changed.
     */
    setValue(value: ScalarType): boolean {
        if (this.v === value) { return false; }
        this.v = value;
        return true;
    }

    /**
     * Changes the formula expression of this cell model, and returns whether
     * the formula expression has changed.
     *
     * @param sheetModel
     *  The sheet model containing this cell model, needed to create a new
     *  token array instance on demand.
     *
     * @param formula
     *  The new formula expression for the cell. The value `null` will delete
     *  the formula expression, and the cell becomes a simple value cell.
     *
     * @param [importing=false]
     *  INTERNAL! If set to `true`, the token array of the cell model will NOT
     *  be created (this will be done later during post-processing phase of the
     *  import process to improve performance).
     *
     * @returns
     *  Whether the formula expression has changed.
     */
    setFormula(sheetModel: SheetModel, formula: string | null, importing?: boolean): boolean {
        formula = formula || null;
        if (this.f === formula) { return false; }
        this.f = formula;
        if (!importing) { this.updateTokenArray(sheetModel); }
        return true;
    }

    /**
     * Adds the address and formula expression of this cell to the model of a
     * shared formula if existing (in property `sf`).
     */
    registerSharedFormula(): void {
        this.sf?.registerCell(this.a, this.f);
    }

    /**
     * Removes the address of this cell from the model of a shared formula if
     * existing (in property `sf`).
     */
    unregisterSharedFormula(): void {
        this.sf?.unregisterCell(this.a);
    }

    /**
     * Changes the bounding range of the matrix formula starting at this cell.
     *
     * @param range
     *  The new bounding range of a matrix formula, or `null` to remove the
     *  bounding range from this cell model.
     *
     * @returns
     *  Whether the bounding range of the matrix formula has changed.
     */
    setMatrixRange(range: Range | null): boolean {
        if ((!this.mr && !range) || (this.mr && range && this.mr.equals(range))) { return false; }
        this.mr = range?.clone() ?? null;
        return true;
    }

    /**
     * Changes the dynamic flag of the matrix formula starting at this cell.
     *
     * @param dynamic
     *  The new state of the dynamic flag for a matrix formula.
     *
     * @returns
     *  Whether the dynamic flag of the matrix formula has changed.
     */
    setDynamicMatrix(dynamic: boolean): boolean {
        if (this.md === dynamic) { return false; }
        this.md = dynamic;
        return true;
    }

    /**
     * Changes the dynamic URL of this cell model, and returns whether the cell
     * URL has changed.
     *
     * @param url
     *  The new dynamic URL for the cell, or null to remove the current URL.
     *
     * @returns
     *  Whether the dynamic cell URL has changed.
     */
    setURL(url: string | null): boolean {
        if (!this.url && !url) { return false; }
        if (this.url && url && (this.url === url)) { return false; }
        if (url) { this.url = url; } else { delete this.url; }
        return true;
    }

    /**
     * Updates the parsed number format (property `pf`) of this cell model,
     * according to the current number format.
     *
     * @param autoStyles
     *  The collection of cell auto-styles of the spreadsheet document.
     */
    updateParsedFormat(autoStyles: CellAutoStyleCollection): void {
        this.pf = autoStyles.getParsedFormat(this.suid, true);
    }

    /**
     * Updates the display string of this cell model, according to the current
     * value and number format.
     *
     * @param numberFormatter
     *  The number formatter of the spreadsheet document.
     */
    updateDisplayString(numberFormatter: NumberFormatter): void {
        this.d = numberFormatter.formatValue(this.pf, this.v).text;
    }

    /**
     * Creates, updates, or removes the token array stored in the property `t`
     * of this cell model, according to the current formula expression stored
     * in the property `f`.
     *
     * @param sheetModel
     *  The model of the sheet containing this cell, needed to create a new
     *  token array instance.
     */
    updateTokenArray(sheetModel: SheetModel): void {
        if (this.f) {
            const tokenArray = this.t ??= sheetModel.createCellTokenArray();
            tokenArray.parseFormula("op", this.f, this.a);
        } else if (this.t) {
            this.t.destroy();
            delete this.t;
        }
    }

    /**
     * Returns a copy of this cell model, to be inserted into the passed sheet
     * model (used by the "copySheet" operation).
     *
     * @param sheetModel
     *  The target sheet model that will contain this cell model.
     *
     * @param [sharedModel]
     *  The model of the shared formula the copied cell shall be member of.
     *  This shared formula model must be part of the passed `SheetModel`!
     *
     * @returns
     *  A copy of this cell model for the specified sheet model.
     */
    cloneFor(sheetModel: SheetModel, sharedModel?: SharedFormulaModel): CellModel {
        const cloneModel = new CellModel(this.a);
        cloneModel.v = this.v;
        cloneModel.f = this.f;
        if (sharedModel) { cloneModel.sf = sharedModel; }
        cloneModel.mr = this.mr?.clone() ?? null;
        cloneModel.md = this.md;
        cloneModel.suid = this.suid;
        cloneModel.pf = this.pf;
        cloneModel.d = this.d;
        if (this.url) { cloneModel.url = this.url; }
        cloneModel.updateTokenArray(sheetModel);
        return cloneModel;
    }
}
