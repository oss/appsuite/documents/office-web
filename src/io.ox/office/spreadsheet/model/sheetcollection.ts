/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { type ReverseOptions, is, fun, itr, ary, map, dict, jpromise } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";
import { detachChildren, insertChildren } from "@/io.ox/office/tk/dom";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";

import { type OTSortVector, transformIndexInsert, transformIndexDelete, transformIndexSort } from "@/io.ox/office/editframework/utils/otutils";
import type { AttributedModelEventMap } from "@/io.ox/office/editframework/model/attributedmodel";
import { getPageContentNode } from "@/io.ox/office/textframework/utils/dom";

import { MAX_SHEET_COUNT, getDebugFlag, setDebugFlag } from "@/io.ox/office/spreadsheet/utils/config";
import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { type TransformSheetVector, SheetType, modelLogger, mapKey, generateSheetName, getTableName, inverseSortVector } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { createSheetEvent } from "@/io.ox/office/spreadsheet/model/sheetevents";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { OperationBuilder } from "@/io.ox/office/spreadsheet/model/operation/builder";
import type { ISheetProps } from "@/io.ox/office/spreadsheet/model/sheetpropset";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import type { SheetAttributeSet, SheetModelEventMap } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

import { type FormulaUpdateTask, DeleteSheetUpdateTask, RenameSheetUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

// types ======================================================================

/**
 * Enumeration of methods to find a visible sheet in the collection.
 */
export enum FindVisibleSheetMethod {

    /**
     * If the specified sheet is hidden, searches for the nearest following
     * visible sheet. All sheets preceding the specified sheet will be ignored.
     */
    NEXT = "next",

    /**
     * If the specified sheet is hidden, searches for the nearest preceding
     * visible sheet. All sheets following the specified sheet will be ignored.
     */
    PREV = "prev",

    /**
     * If the specified sheet is hidden, first searches with the method `NEXT`,
     * and if this fails, with the method `PREV`.
     */
    NEAREST = "nearest"
}

/**
 * Options to match (e.g. iterate, count) sheets.
 */
export interface MatchSheetOptions {

    /**
     * If set to `true`, only sheets with a type supported by the spreadsheet
     * application (according to `isSupportedSheetType()`); if set to `false`,
     * only unsupported sheets will match. By default, all sheets will match
     * regardless if they are supported.
     */
    supported?: boolean;

    /**
     * If set to `true`, only sheets that contain cells (worksheets, macro
     * sheets); if set to `false`, only sheets that do not contain cells (chart
     * sheets, dialog sheets) will match. By default, all sheets will match
     * regardless of their type.
     */
    cells?: boolean;

    /**
     * If set to `true`, only visible sheets; if set to `false`, only hidden
     * sheets will match. By default, all sheets will match regardless of their
     * visibility.
     */
    visible?: boolean;
}

/**
 * Options to create sheet iterators for a spreadsheet document model.
 */
export interface SheetIteratorOptions extends ReverseOptions, MatchSheetOptions {

    /**
     * The index of the first sheet model to be visited. If omitted, uses the
     * first sheet in the document.
     */
    first?: number;

    /**
     * The index of the last sheet model to be visited. If omitted, uses the
     * last sheet in the document.
     */
    last?: number;
}

/**
 * Optional settings for sheet name validation.
 */
interface ValidateSheetNameOptions {

    /**
     * If specified, the zero-based index of the sheet that is allowed to
     * contain the passed name (used while renaming an existing sheet).
     */
    skipSheet?: number;

    /**
     * Whether the sheet name originates form an imported "insertSheet"
     * operation. In this case, sheet names may be checked differently in order
     * to workaround bugs in external spreadsheet applications. Default value
     * is `false`.
     */
    importing?: boolean;
}

/**
 * Will be emitted by `SheetCollection` instances.
 */
export interface SheetEvent extends SheetEventBase {

    /**
     * The name of the sheet (the _new_ sheet name for "rename:sheet:before"
     * events, the _old_ sheet name for "rename:sheet" events).
     */
    sheetName: string;
}

/**
 * Will be emitted by "change:sheetattrs" events after the formatting
 * attributes of a sheet have been changed.
 */
export interface ChangeSheetAttrsEvent extends SheetEventBase {
    newAttrSet: SheetAttributeSet;
    oldAttrSet: SheetAttributeSet;
}

/**
 * Will be emitted by "change:sheetprops" events after the dynamic property set
 * of a sheet has been changed.
 */
export interface ChangeSheetPropsEvent extends SheetEventBase {
    newProps: Readonly<Partial<ISheetProps>>;
    oldProps: Readonly<Partial<ISheetProps>>;
}

/**
 * Type mapping for the events emitted by `SheetCollection` instances.
 */
export interface SheetCollectionEventMap extends Omit<SheetModelEventMap, keyof AttributedModelEventMap<any>> {

    /**
     * Will be emitted before a new sheet will be inserted into a sheet
     * collection.
     *
     * @param event
     *  Event object with the sheet model instance, and the zero-based index of
     *  the new sheet in the collection.
     */
    "insert:sheet:before": [event: SheetEvent];

    /**
     * Will be emitted after a new sheet has been inserted into a sheet
     * collection.
     *
     * @param event
     *  Event object with the sheet model instance, and the zero-based index of
     *  the new sheet in the collection.
     */
    "insert:sheet": [event: SheetEvent];

    /**
     * Will be emitted before a sheet will be removed from a sheet collection.
     *
     * @param sheet
     *  The zero-based index of the sheet in the collection.
     *
     * @param sheetModel
     *  Event object with the sheet model instance, and the zero-based index of
     *  the sheet to be removed.
     */
    "delete:sheet:before": [event: SheetEvent];

    /**
     * Will be emitted after a sheet has been removed from a sheet collection.
     *
     * @param sheet
     *  Event object with the sheet model instance, and the zero-based index of
     *  the deleted sheet.
     */
    "delete:sheet": [event: SheetEvent];

    /**
     * Will be emitted before a sheet will be renamed.
     *
     * @param event
     *  Event object with the model of the sheet to be renamed.
     */
    "rename:sheet:before": [event: SheetEvent];

    /**
     * Will be emitted after a sheet has been renamed.
     *
     * @param event
     *  Event object with the model of the renamed sheet.
     */
    "rename:sheet": [event: SheetEvent];

    /**
     * Will be emitted after the formatting attributes of a sheet have been
     * changed (forwards the "change:attributes" event of the sheet).
     */
    "change:sheetattrs": [event: ChangeSheetAttrsEvent];

    /**
     * Will be emitted after the dynamic property set of a sheet has been
     * changed (forwards the "change:props" event of the sheet's property set).
     */
    "change:sheetprops": [event: ChangeSheetPropsEvent];

    /**
     * Will be emitted before one or more sheets will be moved to a new
     * position.
     *
     * @param xfVector
     *  The transformation vector for the sheet indexes.
     */
    "move:sheets:before": [xfVector: TransformSheetVector];

    /**
     * Will be emitted after one or more sheets have been moved to a new
     * position.
     */
    "move:sheets": [xfVector: TransformSheetVector];

    /**
     * Will be emitted after one of the events "insert:sheet", "delete:sheet",
     * or "move:sheets".
     *
     * @param xfVector
     *  The transformation vector for the sheet indexes.
     */
    "transform:sheets": [xfVector: TransformSheetVector];
}

// constants ==================================================================

/**
 * Names of all sheet events based on `SheetEventBase` to be forwarded as
 * collection events.
 */
export const SHEET_FORWARD_EVENTS = [
    "change:sheetattrs", "change:sheetprops", "refresh:ranges",
    "change:columns", "move:columns", "change:rows", "move:rows",
    "change:usedarea", "change:cells", "move:cells", "merge:cells",
    "insert:hlink", "delete:hlink",
    "insert:name:before", "insert:name", "delete:name:before", "delete:name", "change:name:before", "change:name",
    "insert:table:before", "insert:table", "delete:table:before", "delete:table",
    "change:table:before", "change:table", "change:table:attrs", "change:table:column",
    "insert:cfrule", "delete:cfrule", "change:cfrule",
    "insert:dvrule", "delete:dvrule", "change:dvrule",
    "insert:drawing", "delete:drawing", "change:drawing", "move:drawing",
    "insert:note", "delete:note", "change:note", "move:notes", "move:comments"
] as const satisfies ReadonlyArray<keyof SheetCollectionEventMap>;

// private constants ----------------------------------------------------------

// regular expression matching invalid (!) sheet names
const INVALID_SHEETNAME_RE = /^'|[\x00-\x1f\x80-\x9f[\]*?:/\\]|'$/;

// regular expression matching invalid (!) sheet names imported from OOXML
// DOCS-1642: allow trailing apostrophes as workaround fix
// DOCS-4490: allow to import all ASCII control characters except NUL
const INVALID_SHEETNAME_IMPORT_RE = /^'|[\0[\]*?:/\\]/;

// class SheetInfo ============================================================

/**
 * A structure containing all data for a single sheet in the collection.
 */
class SheetInfo extends DObject {

    /**
     * The model instance of the sheet.
     */
    readonly model: SheetModel;

    /**
     * The DOM container node for the DOM models of all drawing objects.
     */
    readonly node: HTMLElement;

    /**
     * The name of the sheet with correct character case.
     */
    name: string;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, containerNode: HTMLElement, sheetName: string) {
        super();
        this.model = this.member(sheetModel);
        this.node = containerNode;
        this.name = sheetName;
    }

    // public getters ---------------------------------------------------------

    /**
     * The case-insensitive map key of the sheet name.
     */
    get key(): string {
        return mapKey(this.name);
    }
}

// class SheetCollection ======================================================

/**
 * The collection of all sheet models in a spreadsheet document.
 */
export class SheetCollection extends ModelObject<SpreadsheetModel, SheetCollectionEventMap> {

    /**
     * The collected models of all table ranges in all sheets, mapped by the
     * table keys (upper-case table names).
     */
    readonly #tableModels = new Map<string, TableModel>();

    /** All sheet models, mapped by UID. */
    readonly #sheetMap = new Map<SheetModel, SheetInfo>();

    /** All sheet info structures in correct order. */
    readonly #sheetArr = this.member<SheetInfo[]>([]);

    /** Sheet indexes, mapped by sheet key (uppercase sheet name). */
    readonly #indexKeyMap = new Map<string, number>();

    /** Sheet indexes, mapped by sheet model. */
    readonly #indexUidMap = new Map<SheetModel, number>();

    /** The content node of the page for all drawing models. */
    readonly #drawingRootNode: HTMLElement;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super(docModel);

        // root container node for all drawing collections
        // (bug 53493: do not remove "contenteditable" attribute)
        this.#drawingRootNode = getPageContentNode(docModel.getNode())[0];
        detachChildren(this.#drawingRootNode);

        // register operation handlers for manipulating the sheet collection
        docModel.registerOperationHandler(Op.INSERT_SHEET, this.#applyInsertSheetOperation.bind(this));
        docModel.registerOperationHandler(Op.DELETE_SHEET, this.#applyDeleteSheetOperation.bind(this));
        docModel.registerOperationHandler(Op.CHANGE_SHEET, this.#applyChangeSheetOperation.bind(this));
        docModel.registerOperationHandler(Op.MOVE_SHEET,   this.#applyMoveSheetOperation.bind(this));
        docModel.registerOperationHandler(Op.MOVE_SHEETS,  this.#applyMoveSheetsOperation.bind(this));
        docModel.registerOperationHandler(Op.COPY_SHEET,   this.#applyCopySheetOperation.bind(this));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the exact name of the sheet at the specified index.
     *
     * @param sheet
     *  The zero-based index of the sheet.
     *
     * @returns
     *  The name of the sheet at the passed index; or `null`, if the passed
     *  index is invalid.
     */
    getSheetName(sheet: number): string | null {
        return this.#sheetArr[sheet]?.name ?? null;
    }

    /**
     * Returns the current index of the sheet with the specified name.
     *
     * @param sheetName
     *  The case-insensitive sheet name.
     *
     * @returns
     *  The current zero-based index of the sheet with the specified name; or
     *  `-1`, if a sheet with the passed name does not exist.
     */
    getSheetIndex(sheetName: string): number {
        // initialize the lookup map on demand
        return map.upsert(this.#indexKeyMap, mapKey(sheetName), key => this.#sheetArr.findIndex(sheetInfo => sheetInfo.key === key));
    }

    /**
     * Returns the current sheet index of the passed sheet model instance.
     *
     * @param sheetModel
     *  The sheet model whose index will be returned.
     *
     * @returns
     *  The current zero-based sheet index of the sheet model; or `-1`, if the
     *  sheet model does not exist in this collection.
     */
    getSheetIndexOfModel(sheetModel: SheetModel): number {
        // initialize the lookup map on demand
        return map.upsert(this.#indexUidMap, sheetModel, () => this.#sheetArr.findIndex(sheetInfo => sheetInfo.model === sheetModel));
    }

    /**
     * Returns the model instance of the sheet at the specified index.
     *
     * @param sheet
     *  The zero-based index of the sheet.
     *
     * @returns
     *  The model instance for the sheet at the passed index; or `undefined`,
     *  if the passed index is invalid.
     */
    getSheetModel(sheet: number): Opt<SheetModel> {
        return this.#sheetArr[sheet]?.model;
    }

    /**
     * Returns the number of sheets in this collection.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The number of sheets in this collection.
     */
    getSheetCount(options?: MatchSheetOptions): number {
        return is.empty(options) ? this.#sheetArr.length : itr.size(this.yieldSheetModels(options));
    }

    /**
     * Creates an iterator that visits the sheet models in this document.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The matching sheet models.
     */
    *yieldSheetModels(options?: SheetIteratorOptions): IterableIterator<SheetModel> {

        // the index of the first sheet to be visited
        const firstSheet = options?.first ?? 0;
        // the index of the last sheet to be visited
        const lastSheet = options?.last ?? (this.#sheetArr.length - 1);
        // whether to iterate in reversed order
        const reverse = options?.reverse;

        // the effective half-open interval indexes
        const begin = reverse ? lastSheet : firstSheet;
        const end = reverse ? (firstSheet - 1) : (lastSheet + 1);

        // filter for (un)supported sheet types
        const supported = options?.supported;
        const supportedFn = is.boolean(supported) ? ((sheetModel: SheetModel) => supported === sheetModel.isSupported) : fun.true;

        // filter for sheets with/without cells
        const cells = options?.cells;
        const cellsFn = is.boolean(cells) ? ((sheetModel: SheetModel) => cells === sheetModel.isCellType) : fun.true;

        // filter for visible/hidden sheets
        const visible = options?.visible;
        const visibleFn = is.boolean(visible) ? ((sheetModel: SheetModel) => visible === sheetModel.isVisible()) : fun.true;

        // transform the iterator result (sheet collection entries to sheet models), filter for sheets if specified
        for (const { model: sheetModel } of ary.values(this.#sheetArr, { begin, end, reverse })) {
            if (supportedFn(sheetModel) && cellsFn(sheetModel) && visibleFn(sheetModel)) {
                yield sheetModel;
            }
        }
    }

    /**
     * Returns the index of the nearest visible sheet next to the specified
     * sheet.
     *
     * @param sheet
     *  The zero-based index of a sheet. If this sheet is visible, its index
     *  will be returned. Otherwise, another visible sheet will be searched.
     *
     * @param [method=NEAREST]
     *  The method how to find a visible sheet.
     *
     * @returns
     *  The zero-based index of a visible sheet; or `-1`, if no visible sheet
     *  has been found.
     */
    findVisibleSheet(sheet: number, method?: FindVisibleSheetMethod): number {

        // NEAREST method: first try to find a following sheet, then a preceding
        const sheetModel =
            (method === FindVisibleSheetMethod.NEXT) ? this.#findNextVisibleSheetModel(sheet) :
            (method === FindVisibleSheetMethod.PREV) ? this.#findPrevVisibleSheetModel(sheet) :
            (this.#findNextVisibleSheetModel(sheet) ?? this.#findPrevVisibleSheetModel(sheet));

        return sheetModel?.getIndex() ?? -1;
    }

    /**
     * Returns whether it is possible to insert a new sheet into this
     * collection according to the server configuration.
     *
     * @returns
     *  Whether it is possible to insert a new sheet into this document.
     */
    canInsertSheet(): boolean {
        return (MAX_SHEET_COUNT < 1) || (this.#sheetArr.length < MAX_SHEET_COUNT);
    }

    /**
     * Creates a localized sheet name that is not yet used in this collecion.
     *
     * @returns
     *  A sheet name not yet used in this collecion.
     */
    generateUnusedSheetName(): string {

        // the new sheet name
        let sheetName = "";
        // one-based index for the new sheet name
        let index = this.#sheetArr.length;

        // generate a valid name
        while ((sheetName.length === 0) || (this.getSheetIndex(sheetName) >= 0)) {
            sheetName = generateSheetName(index);
            index += 1;
        }

        return sheetName;
    }

    /**
     * Returns the model of a table range from any sheet.
     *
     * @param tableName
     *  The case-insensitive name of a table range.
     *
     * @returns
     *  The model of a table range with the specified name.
     */
    getTableModel(tableName: string): Opt<TableModel> {
        return this.#tableModels.get(mapKey(tableName));
    }

    /**
     * Creates an iterator that visits the models of all table ranges (except
     * the autofilters) in the document.
     *
     * @yields
     *  The table models of all sheets in the document except autofilters.
     */
    *yieldTableModels(): IterableIterator<TableModel> {
        yield* this.#tableModels.values();
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations and undo operations to update or restore the
     * formulas of all sheets in this collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateUpdateTaskOperations(builder: OperationBuilder, updateTask: FormulaUpdateTask): JPromise {

        // the deleted sheet to be skipped completely
        const deletedModel = (updateTask instanceof DeleteSheetUpdateTask) ? this.getSheetModel(updateTask.sheet) : undefined;

        // visit all sheet models in a sliced loop
        return this.asyncForEach(this.yieldSheetModels(), sheetModel => {
            const sheetCache = (sheetModel === deletedModel) ? undefined : builder.getSheetCache(sheetModel);
            return sheetCache ? sheetModel.generateUpdateTaskOperations(sheetCache, updateTask) : undefined;
        });
    }

    /**
     * Generates the operations, and the undo operations, to insert a new sheet
     * into this collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param sheet
     *  The zero-based insertion index of the new sheet.
     *
     * @param sheetType
     *  The type identifier of the new sheet.
     *
     * @param sheetName
     *  The name of the new sheet. Must not be empty. Must not be equal to
     *  the name of any existing sheet.
     *
     * @param [attrSet]
     *  Initial formatting attributes for the new sheet.
     */
    generateInsertSheetOperations(builder: OperationBuilder, sheet: number, sheetType: SheetType, sheetName: string, attrSet?: Dict): void {

        // first check the passed sheet name
        OperationError.throwCauseIf(this.#validateSheetName(sheetName));

        // the properties for the 'insertSheet' operation (add optional attributes)
        const props: Dict = { sheet, sheetName };
        if (sheetType !== SheetType.WORKSHEET) { props.type = sheetType; }
        if (attrSet) { props.attrs = attrSet; }

        // generate the operations
        const generator = builder.headGenerator;
        generator.generateOperation(Op.INSERT_SHEET, props);
        generator.generateOperation(Op.DELETE_SHEET, { sheet }, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to delete an existing
     * sheet from this collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param sheet
     *  The zero-based index of the sheet to be deleted.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateDeleteSheetOperations(builder: OperationBuilder, sheet: number): JPromise {

        // first, generate the undo operations for the deleted sheet
        const sheetModel = this.#ensureSheetModel(sheet);
        const sheetCache = builder.createSheetCache(sheetModel);
        let promise = sheetModel.generateRestoreOperations(sheetCache);

        // update the formula expressions for all sheets (except the sheet to be deleted)
        promise = promise.then(() => {
            const updateTask = new DeleteSheetUpdateTask(sheet);
            return this.docModel.generateUpdateTaskOperations(builder, updateTask);
        });

        // finally, generate the operation to delete the sheet (after all updates)
        return promise.then(() => { builder.tailGenerator.generateOperation(Op.DELETE_SHEET, { sheet }); });
    }

    /**
     * Generates the operations, and the undo operations, to move a sheet in
     * this collection to a new position.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param fromSheet
     *  The zero-based index of the sheet to be moved.
     *
     * @param toSheet
     *  The zero-based index of the new position of the sheet.
     */
    generateMoveSheetOperations(builder: OperationBuilder, fromSheet: number, toSheet: number): void {
        const generator = builder.headGenerator;
        generator.generateOperation(Op.MOVE_SHEET, { sheet: fromSheet, to: toSheet });
        generator.generateOperation(Op.MOVE_SHEET, { sheet: toSheet, to: fromSheet }, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to move all sheets in
     * this collection to new positions.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param sortVector
     *  The new order of the sheets. MUST contain the same number of elements
     *  as sheets exist in this collection. The array element values represent
     *  the old sheet indexes (element value) at their new position (array
     *  index).
     */
    generateMoveSheetsOperations(builder: OperationBuilder, sortVector: OTSortVector): void {
        const generator = builder.headGenerator;
        generator.generateOperation(Op.MOVE_SHEETS, { sheets: sortVector });
        generator.generateOperation(Op.MOVE_SHEETS, { sheets: inverseSortVector(sortVector) }, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to create and insert
     * a complete clone of a sheet in this collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param fromSheet
     *  The zero-based index of the sheet to be copied.
     *
     * @param toSheet
     *  The zero-based insertion index of the new sheet.
     *
     * @param sheetName
     *  The name of the new sheet. Must not be empty. Must not be equal to
     *  the name of any existing sheet.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateCopySheetOperations(builder: OperationBuilder, fromSheet: number, toSheet: number, sheetName: string): JPromise {

        // first check the passed sheet name
        OperationError.throwCauseIf(this.#validateSheetName(sheetName));

        // collect the names of all existing tables in a set
        const oldTableKeys = new Set(this.#tableModels.keys());

        // generate unused names for all new table ranges in the cloned sheet
        const sheetModel = this.#ensureSheetModel(fromSheet);
        const newTableNames = dict.create<string>();
        for (const tableModel of sheetModel.tableCollection.yieldTableModels()) {

            // try to split the table name into a base name and a trailing integer (fall-back to translated 'Table')
            const oldTableName = tableModel.name;
            const matches = /^(.*?)(\d+)$/.exec(oldTableName);
            const baseName = matches?.[1] || getTableName();
            let nameIndex = matches ? (parseInt(matches[2], 10) + 1) : 1;

            // generate an unused table name by increasing the trailing index repeatedly
            while (oldTableKeys.has(mapKey(`${baseName}${nameIndex}`))) { nameIndex += 1; }
            const newTableName = `${baseName}${nameIndex}`;
            oldTableKeys.add(mapKey(newTableName));
            newTableNames[oldTableName] = newTableName;
        }

        // first, generate the "copySheet" operation
        const generator = builder.headGenerator;
        const props: Dict = { sheet: fromSheet, to: toSheet, sheetName };
        if (!is.empty(newTableNames)) { props.tableNames = newTableNames; }
        generator.generateOperation(Op.COPY_SHEET, props);
        generator.generateOperation(Op.DELETE_SHEET, { sheet: toSheet }, { undo: true });

        // insert index of the *new* sheet into all following formula update operations, and ignore all undo operations
        // generated above while updating the formulas (the copied sheet will just be deleted on undo)
        const sheetCache = builder.createSheetCache(sheetModel, { targetSheet: toSheet, skipUndo: true });

        // update the formula expressions in the new sheet (insert the index of the *new* sheet into all following
        // formula update operations; insert name of the new sheet where the old sheet name has been used; same for
        // cloned table references)
        const updateTask = new RenameSheetUpdateTask(fromSheet, sheetName, newTableNames);
        const promise = sheetModel.generateUpdateTaskOperations(sheetCache, updateTask);

        // bug 52800: (workaround) delete all unsupported charts in the new sheet
        return promise.then(() => {
            const iterator = sheetModel.drawingCollection.yieldModels({ deep: true, reverse: true });
            return this.asyncForEach(iterator, drawingModel => {
                if ((drawingModel.drawingType === DrawingType.CHART) && !drawingModel.isRestorable()) {
                    sheetCache.generateDrawingOperation(Op.DELETE_DRAWING, drawingModel.getPosition());
                }
            });
        });
    }

    /**
     * Generates the operations, and the undo operations, to rename a sheet in
     * this collection.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param sheet
     *  The zero-based index of the sheet to be renamed.
     *
     * @param sheetName
     *  The new name for the sheet. Must not be empty. Must not be equal to the
     *  name of any existing sheet.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateRenameSheetOperations(builder: OperationBuilder, sheet: number, sheetName: string): JPromise {

        // do not generate an operation, if passed name matches the old name
        const oldSheetName = this.getSheetName(sheet);
        if (oldSheetName === sheetName) { return jpromise.resolve(); }

        // check the passed sheet name
        OperationError.throwCauseIf(this.#validateSheetName(sheetName, { skipSheet: sheet }));

        // generate and apply the operations to rename the sheet
        const generator = builder.headGenerator;
        generator.generateOperation(Op.CHANGE_SHEET, { sheet, sheetName });
        generator.generateOperation(Op.CHANGE_SHEET, { sheet, sheetName: oldSheetName }, { undo: true });

        // update the formula expressions for all sheets in the document
        const updateTask = new RenameSheetUpdateTask(sheet, sheetName);
        return this.docModel.generateUpdateTaskOperations(builder, updateTask);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the specified sheet model, or throws an exception.
     *
     * @param sheet
     *  The zero-based index of the sheet.
     *
     * @returns
     *  The sheet model.
     *
     * @throws
     *  A `RangeError` exception, if the passed document operation does not
     *  address an existing sheet.
     */
    #ensureSheetModel(sheet: number): SheetModel {
        const sheetModel = this.getSheetModel(sheet);
        if (!sheetModel) { throw new RangeError("invalid sheet index"); }
        return sheetModel;
    }

    /**
     * Returns a visible sheet at the specified index. If the sheet is not
     * visible, returns the nearest preceding visible sheet.
     *
     * @param sheet
     *  The zero-based index of a sheet.
     *
     * @returns
     *  The model of a visible sheet if available.
     */
    #findPrevVisibleSheetModel(sheet: number): Opt<SheetModel> {
        return itr.shift(this.yieldSheetModels({ last: sheet, reverse: true, supported: true, visible: true }));
    }

    /**
     * Returns a visible sheet at the specified index. If the sheet is not
     * visible, returns the nearest following visible sheet.
     *
     * @param sheet
     *  The zero-based index of a sheet.
     *
     * @returns
     *  The model of a visible sheet if available.
     */
    #findNextVisibleSheetModel(sheet: number): Opt<SheetModel> {
        return itr.shift(this.yieldSheetModels({ first: sheet, supported: true, visible: true }));
    }

    /**
     * Returns the sheet info structure addressed by the property "sheet" of
     * the specified operation.
     *
     * @param context
     *  The wrapper representing a sheet collection operation.
     *
     * @returns
     *  The sheet info structure addressed by the passed operation.
     *
     * @throws
     *  An `OperationError`, if the passed document operation does not address
     *  an existing sheet.
     */
    #resolveSheetInfo(context: SheetOperationContext): { sheet: number; sheetInfo: SheetInfo } {
        const sheet = context.getInt("sheet");
        const sheetInfo = this.#sheetArr[sheet];
        context.ensure(sheetInfo, "invalid sheet index %d", sheet);
        return { sheet, sheetInfo };
    }

    /**
     * Returns an error code if the passed sheet name is not valid (empty, or
     * with invalid characters, or already existing in the document).
     *
     * @param sheetName
     *  The desired name of the sheet (case-insensitive).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The empty string, if the sheet name is valid; otherwise one of the
     *  following error codes:
     *  - "sheet:name:empty": The passed sheet name is empty.
     *  - "sheet:name:invalid": The passed sheet name contains invalid
     *    characters.
     *  - "sheet:name:used": The passed sheet name is already used in the
     *    document.
     */
    #validateSheetName(sheetName: string, options?: ValidateSheetNameOptions): string {

        // name must not be empty
        if (sheetName.length === 0) { return "sheet:name:empty"; }

        // Name must not start nor end with an apostrophe (embedded apostrophes are allowed), and must not contain any
        // NPCs or other invalid characters.
        //
        // DOCS-1642: OOXML imported sheet names may end with an apostrophe. Excel365 seems to contain a bug that
        // allows to rename a sheet in such a way in its GUI, but immediately fails to load its own files containing
        // these sheet names. As a workaround, we allow to import and work with these files, but we DO NOT allow to
        // rename sheets in our own GUI.
        //
        const invalidRE = (options?.importing && this.docApp.isOOXML()) ? INVALID_SHEETNAME_IMPORT_RE : INVALID_SHEETNAME_RE;
        if (invalidRE.test(sheetName)) { return "sheet:name:invalid"; }

        // name must not be used already for another sheet
        const sheet = this.getSheetIndex(sheetName);
        if ((sheet >= 0) && (sheet !== options?.skipSheet)) { return "sheet:name:used"; }

        // empty string indicates valid sheet name
        return "";
    }

    /**
     * Returns whether the passed sheet name is valid (non-empty, no invalid
     * characters, and not existing in the document).
     *
     * @param sheetName
     *  The desired name of the sheet (case-insensitive).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  Whether the sheet name is valid and not used.
     */
    #isValidSheetName(sheetName: string, options?: ValidateSheetNameOptions): boolean {
        return !this.#validateSheetName(sheetName, options);
    }

    /**
     * Throws an `OperationError` via the passed operation context, if the
     * passed sheet name is not valid (empty, or with invalid characters, or
     * already existing in the document).
     *
     * @param context
     *  The operation wrapper object.
     *
     * @param sheetName
     *  The desired name of the sheet (case-insensitive).
     *
     * @param [options]
     *  Optional parameters.
     */
    #ensureValidSheetName(context: SheetOperationContext, sheetName: string, options?: ValidateSheetNameOptions): void {
        if (context.importing) { options = { importing: true, ...options }; }
        context.ensure(this.#isValidSheetName(sheetName, options), "invalid sheet name");
    }

    /**
     * Creates and inserts a new empty sheet into this collection, and triggers
     * the appropriate events.
     *
     * @param context
     *  A wrapper representing one of the document operations "insertSheet" or
     *  "copySheet".
     *
     * @param sheet
     *  The insertion index of the new sheet. MUST be valid (not negative, and
     *  not greater than the number of existing sheets).
     *
     * @param sheetType
     *  The type identifier of the new sheet.
     *
     * @param sheetName
     *  The name of the new sheet. MUST NOT be used yet in this document.
     *
     * @param [attrSet]
     *  The initial sheet attributes.
     *
     * @returns
     *  The model instance of the new sheet; or `undefined`, if no more sheets
     *  can be inserted into this document.
     */
    #createSheetAndTrigger(context: SheetOperationContext, sheet: number, sheetType: SheetType, sheetName: string, attrSet?: Dict): Opt<SheetModel> {

        // first, check maximum sheet count
        if (!this.canInsertSheet()) { return undefined; }

        // create the new sheet model
        const sheetModel = new SheetModel(this.docModel, sheetType, attrSet);
        // the DOM container node for drawing frames
        const containerNode = sheetModel.drawingCollection.getContainerNode();

        // notify all listeners before insertion
        const sheetEvent: SheetEvent = createSheetEvent(sheetModel, sheet, { sheetName }, context);
        this.trigger("insert:sheet", sheetEvent);

        // calculate the new active sheet index
        const oldActiveSheet = this.docModel.getActiveSheet();
        const newActiveSheet = transformIndexInsert(oldActiveSheet, sheet, 1);
        const activeChanged = oldActiveSheet !== newActiveSheet;

        // invalidate active sheet index while inserting a sheet before it
        if (activeChanged) { this.docModel.setActiveSheet(-1); }

        // insert the sheet model descriptor into the collections
        const sheetInfo = new SheetInfo(sheetModel, containerNode, sheetName);
        ary.insertAt(this.#sheetArr, sheet, sheetInfo);
        this.#sheetMap.set(sheetModel, sheetInfo);

        // insert the DOM container node for drawing frames into the page node
        insertChildren(this.#drawingRootNode, sheet, containerNode);

        // clear the sheet index cache unless the sheet has been appended
        const sheetCount = this.#sheetArr.length;
        if (sheet === sheetCount) {
            this.#indexKeyMap.set(sheetInfo.key, sheet);
            this.#indexUidMap.set(sheetModel, sheet);
        } else {
            this.#indexKeyMap.clear();
            this.#indexUidMap.clear();
        }

        // update active sheet index, if the sheet has been inserted before
        if (activeChanged) { this.docModel.setActiveSheet(newActiveSheet); }

        // insert all new table models into the global map
        this.listenTo(sheetModel.tableCollection, "insert:table", ({ tableModel }) => {
            if (!tableModel.isAutoFilter()) { this.#tableModels.set(tableModel.key, tableModel); }
        });

        // remove all deleted table models from the global map
        this.listenTo(sheetModel.tableCollection, "delete:table:before", ({ tableModel }) => {
            if (!tableModel.isAutoFilter()) { this.#tableModels.delete(tableModel.key); }
        });

        // remove old key of all renamed table models from the global map
        this.listenTo(sheetModel.tableCollection, "change:table:before", event => {
            if (event.name) { this.#tableModels.delete(event.tableModel.key); }
        });

        // insert new key of all renamed table models into the global map
        this.listenTo(sheetModel.tableCollection, "change:table", event => {
            if (event.name) { this.#tableModels.set(event.tableModel.key, event.tableModel); }
        });

        // notify all listeners after insertion
        this.trigger("insert:sheet", sheetEvent);

        // transform sheet indexes in formulas
        const xfVector: TransformSheetVector = ary.generate(sheetCount - 1, index => transformIndexInsert(index, sheet, 1));
        this.trigger("transform:sheets", xfVector);

        // forward all sheet events based on `SheetEventBase` event objects
        for (const eventType of SHEET_FORWARD_EVENTS) {
            this.listenTo(sheetModel, eventType as "refresh:ranges", event => {
                // fake type of "eventType" to fixed string to prevent type error with mixed types of "event"
                this.trigger(eventType as "refresh:ranges", event);
            });
        }

        // forward "change:attributes" events as "change:sheetattrs" events
        this.listenTo(sheetModel, "change:attributes", (newAttrSet, oldAttrSet) => {
            this.trigger("change:sheetattrs", sheetModel.createSheetEvent({ newAttrSet, oldAttrSet }));
        });

        // forward sheet property events as "change:sheetprops" events
        this.listenTo(sheetModel.propSet, "change:props", (newProps, oldProps) => {
            this.trigger("change:sheetprops", sheetModel.createSheetEvent({ newProps, oldProps }));
        });

        return sheetModel;
    }

    /**
     * Resorts all sheets according to the passed sort vector, and triggers the
     * appropriate document events.
     *
     * @param context
     *  The operation wrapper object.
     *
     * @param sortVector
     *  The new order of all sheets.
     */
    #sortSheetsAndTrigger(context: SheetOperationContext, sortVector: OTSortVector): void {

        // notify all listeners before moving (DOCS-2733: the transformation vector used by the formula engine
        // uses reversed logic (old sheet index as array value)
        const xfVector: TransformSheetVector = inverseSortVector(sortVector);
        this.trigger("move:sheets:before", xfVector);

        // calculate the new active sheet index
        const oldActiveSheet = this.docModel.getActiveSheet();
        const newActiveSheet = transformIndexSort(oldActiveSheet, sortVector);
        const activeChanged = oldActiveSheet !== newActiveSheet;

        // invalidate active sheet index while moving the sheets
        if (activeChanged) { this.docModel.setActiveSheet(-1); }

        // reorder the sheets collection
        ary.permute(this.#sheetArr, sortVector);

        // reorder the DOM container nodes for drawing frames (DOCS-1630: explicitly
        // insert at correct position to keep the dummy empty paragraph at the end)
        try {
            this.#sheetArr.forEach((sheetInfo, sheet) => {
                insertChildren(this.#drawingRootNode, sheet, sheetInfo.node);
            });
        } catch (err) {
            modelLogger.exception(err);
            // DOCS-2055: generate a more useful error than IE11's "unknown error"
            context.error("failed to reorder drawing page nodes");
        }

        // clear the sheet index caches
        this.#indexKeyMap.clear();
        this.#indexUidMap.clear();

        // notify all listeners after moving
        this.trigger("move:sheets", xfVector);
        this.trigger("transform:sheets", xfVector);

        // update the active sheet index
        if (activeChanged) { this.docModel.setActiveSheet(newActiveSheet); }
    }

    /**
     * Callback handler for the document operation "insertSheet". Creates and
     * inserts a new sheet in this spreadsheet document.
     *
     * @param context
     *  A wrapper representing the document operation "insertSheet".
     */
    #applyInsertSheetOperation(context: SheetOperationContext): void {

        // the sheet index passed in the operation
        const sheet = context.getInt("sheet");
        // the sheet name passed in the operation
        const sheetName = context.getStr("sheetName");
        // the sheet type passed in the operation
        const sheetType = context.optEnum("type", SheetType, SheetType.WORKSHEET);
        // the sheet attributes passed in the operation (optional)
        const attrSet = context.optDict("attrs");

        // let the "insertSheet" operation fail ONCE on purpose if specified in URL options
        if (getDebugFlag("spreadsheet:failing-import")) {
            setDebugFlag("spreadsheet:failing-import", false);
            context.error("debug mode: insert sheet fails");
        }

        // check the sheet index, name (must not exist yet), and type
        context.ensure((sheet >= 0) && (sheet <= this.#sheetArr.length), "invalid sheet index");
        this.#ensureValidSheetName(context, sheetName);

        // create and insert the new sheet
        const sheetModel = this.#createSheetAndTrigger(context, sheet, sheetType, sheetName, attrSet);
        context.ensure(sheetModel, "cannot create sheet");
    }

    /**
     * Callback handler for the document operation "deleteSheet". Deletes an
     * existing sheet from this spreadsheet document.
     *
     * @param context
     *  A wrapper representing the document operation "deleteSheet".
     */
    #applyDeleteSheetOperation(context: SheetOperationContext): void {

        // resolve the target sheet to be deleted
        const { sheet, sheetInfo } = this.#resolveSheetInfo(context);
        const sheetModel = sheetInfo.model;

        // there must remain at least one sheet in the document
        context.ensure(this.#sheetArr.length > 1, "cannot delete last sheet");

        // notify all listeners before deletion
        const sheetEvent: SheetEvent = createSheetEvent(sheetModel, sheet, { sheetName: sheetInfo.name }, context);
        this.trigger("delete:sheet:before", sheetEvent);

        // calculate the new active sheet index
        const oldActiveSheet = this.docModel.getActiveSheet();
        let newActiveSheet = transformIndexDelete(oldActiveSheet, sheet, 1);
        const activeChanged = oldActiveSheet !== newActiveSheet;

        // invalidate active sheet index while deleting itself, or a sheet before it
        if (activeChanged) { this.docModel.setActiveSheet(-1); }

        // clear the sheet index cache unless the sheet was the last
        if (sheet + 1 === this.#sheetArr.length) {
            this.#indexKeyMap.delete(sheetInfo.key);
            this.#indexUidMap.delete(sheetModel);
        } else {
            this.#indexKeyMap.clear();
            this.#indexUidMap.clear();
        }

        // remove the DOM container node for drawing frames from the page node
        sheetInfo.node.remove();

        // unregister all table models in the global map
        for (const tableModel of sheetModel.tableCollection.yieldTableModels()) {
            this.#tableModels.delete(tableModel.key);
        }

        // remove the sheet from the collection storage
        this.#sheetMap.delete(sheetModel);
        ary.deleteAt(this.#sheetArr, sheet);

        // update active sheet index, if a sheet before the active sheet has been deleted
        // (find a visible sheet, if the sheet at the specified position is hidden)
        if (activeChanged) {
            if (!is.number(newActiveSheet)) {
                newActiveSheet = Math.min(sheet, this.#sheetArr.length - 1);
            }
            this.docModel.setActiveSheet(this.findVisibleSheet(newActiveSheet));
        }

        // notify all listeners after deletion
        this.trigger("delete:sheet", sheetEvent);

        // transform sheet indexes in formulas
        const xfVector: TransformSheetVector = ary.generate(this.#sheetArr.length + 1, index => transformIndexDelete(index, sheet, 1) ?? null);
        this.trigger("transform:sheets", xfVector);

        // destroy the sheet model
        sheetModel.destroy();
    }

    /**
     * Callback handler for the document operation "changeSheet". Renames an
     * existing sheet in this collection, and changes its attributes.
     *
     * @param context
     *  A wrapper representing the document operation "changeSheet".
     */
    #applyChangeSheetOperation(context: SheetOperationContext): void {

        // resolve the target sheet to be changed
        const { sheet, sheetInfo } = this.#resolveSheetInfo(context);
        const sheetModel = sheetInfo.model;

        // the sheet name passed in the operation (no other sheet must contain this name)
        const sheetName = context.optStr("sheetName");
        if (sheetName) {
            this.#ensureValidSheetName(context, sheetName, { skipSheet: sheet });

            // rename the sheet (no event, if passed name matches old name)
            if (sheetInfo.name !== sheetName) {
                this.trigger("rename:sheet:before", sheetModel.createSheetEvent({ sheetName }));
                this.#indexKeyMap.delete(sheetInfo.key); // key depends on name
                const oldName = sheetInfo.name;
                sheetInfo.name = sheetName;
                this.#indexKeyMap.set(sheetInfo.key, sheet);
                this.trigger("rename:sheet", sheetModel.createSheetEvent({ sheetName: oldName }));
            }
        }

        // the formatting attributes to be updated
        const attrSet = context.optDict("attrs");
        if (attrSet) { sheetModel.setAttributes(attrSet); }
    }

    /**
     * Callback handler for the document operation "moveSheet" Moves an
     * existing sheet in this collection to a new position.
     *
     * @param context
     *  A wrapper representing the document operation "moveSheet".
     */
    #applyMoveSheetOperation(context: SheetOperationContext): void {

        // resolve the target sheet to be moved
        const { sheet: fromSheet } = this.#resolveSheetInfo(context);

        // the target position of the sheet
        const toSheet = context.getInt("to");
        context.ensure((toSheet >= 0) && (toSheet < this.#sheetArr.length), "invalid target index %d", toSheet);
        if (fromSheet === toSheet) { return; }

        // prepare the sort vector for sheet indexes
        const sortVector = ary.generate(this.#sheetArr.length, fun.identity);
        ary.deleteAt(sortVector, fromSheet);
        ary.insertAt(sortVector, toSheet, fromSheet);

        // reorder the sheets and trigger
        this.#sortSheetsAndTrigger(context, sortVector);
    }

    /**
     * Callback handler for the document operation "moveSheets". Moves all
     * existing sheets in this collection to a new position.
     *
     * @param context
     *  A wrapper representing the document operation "moveSheets".
     */
    #applyMoveSheetsOperation(context: SheetOperationContext): void {

        // the target positions of the sheets
        const sortVector = context.getPos("sheets");
        const sheetCount = this.#sheetArr.length;
        context.ensure(sortVector.length === sheetCount, "invalid length of transformation vector");

        // check that all sheet indexes occur exactly once in the array
        const sortedIndexes = _.unique(_.sortBy(sortVector), true);
        context.ensure(sortedIndexes.length === sheetCount, "duplicate sheet indexes in transformation vector");
        context.ensure((sortedIndexes[0] === 0) && (ary.at(sortedIndexes, -1) === sheetCount - 1), "invalid sheet indexes in transformation vector");

        // reorder the sheets and trigger
        this.#sortSheetsAndTrigger(context, sortVector);
    }

    /**
     * Callback handler for the document operation "copySheet". Creates a copy
     * of an existing sheet in this collection, and inserts that sheet to a new
     * position.
     *
     * @param context
     *  A wrapper representing the document operation "copySheet".
     */
    #applyCopySheetOperation(context: SheetOperationContext): void {

        // resolve the source sheet to be copied
        const { sheetInfo } = this.#resolveSheetInfo(context);
        const sheetModel = sheetInfo.model;

        // the target position for the new sheet
        const toSheet = context.getInt("to");
        context.ensure((toSheet >= 0) && (toSheet <= this.#sheetArr.length), "invalid target index");

        // the new sheet name passed in the operation (no existing sheet must contain this name)
        const sheetName = context.getStr("sheetName");
        this.#ensureValidSheetName(context, sheetName);

        // insert a new sheet model into the collection
        const cloneModel = this.#createSheetAndTrigger(context, toSheet, sheetModel.sheetType, sheetName);
        context.ensure(cloneModel, "cannot create sheet");

        // clone all contents from the existing sheet
        cloneModel.applyCopySheetOperation(context, sheetModel);
    }
}
