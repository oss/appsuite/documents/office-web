/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { itr, map, pick, jpromise } from "@/io.ox/office/tk/algorithms";

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { MergeMode, mapKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";

import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import type { MoveCellsEvent } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { TableColumnAttributeSet } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import { type TableAttributeSet, type PtTableAttributeSet, TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

// types ======================================================================

/**
 * Optional parameters to control which table models will be visited by a model
 * iterator.
 */
export interface TableIteratorOptions {

    /**
     * Restricts the iterator to table models overlapping with that range.
     */
    overlapRange?: Range;

    /**
     * If set to `true`, the table model for the auto-filter will be visited if
     * existing. By default (`false`), only real (named) tables will be
     * selected.
     */
    autoFilter?: boolean;
}

/**
 * Event object emitted by `TableCollection` instances.
 */
export interface TableEvent extends SheetEventBase {

    /**
     * The model instance of the defined name causing the event.
     */
    tableModel: TableModel;
}

export interface ChangeTableEvent extends TableEvent {

    /**
     * The old ane new cell range address of the table range, if it has been
     * changed.
     */
    range?: Pair<Range>;

    /**
     * The old and new name of the table range, if it has been changed.
     */
    name?: Pair<string>;
}

export interface ChangeTableAttrsEvent extends TableEvent {

    /**
     * The old formatting attributes of the table range.
     */
    oldAttrSet: TableAttributeSet;

    /**
     * The new formatting attributes of the table range.
     */
    newAttrSet: TableAttributeSet;
}

export interface ChangeTableColumnEvent extends TableEvent {

    /**
     * The zero-based index of the changed table column model.
     */
    tableCol: number;

    /**
     * The old formatting attributes of the table column model.
     */
    oldAttrSet: TableColumnAttributeSet;

    /**
     * The new formatting attributes of the table column model.
     */
    newAttrSet: TableColumnAttributeSet;
}

/**
 * Type mapping for the events emitted by `TableCollection` instances.
 */
export interface TableCollectionEventMap {

    /**
     * Will be emitted before a new table range will be inserted into a table
     * collection.
     *
     * @param event
     *  Event object with the model instance of the new table range.
     */
    "insert:table:before": [event: TableEvent];

    /**
     * Will be emitted after a new table range has been inserted into a table
     * collection.
     *
     * @param event
     *  Event object with the model instance of the new table range.
     */
    "insert:table": [event: TableEvent];

    /**
     * Will be emitted before a table range will be deleted from a table
     * collection.
     *
     * @param event
     *  Event object with the model instance of the table range to be deleted.
     */
    "delete:table:before": [event: TableEvent];

    /**
     * Will be emitted after a table range has been deleted from a table
     * collection.
     *
     * @param event
     *  Event object with the model instance of the deleted table range.
     */
    "delete:table": [event: TableEvent];

    /**
     * Will be emitted before the range address or name of an existing table
     * range will be changed.
     *
     * @param event
     *  Event object with the model instance of the table range to be changed.
     */
    "change:table:before": [event: ChangeTableEvent];

    /**
     * Will be emitted after the range address or name of an existing table
     * range has been changed.
     *
     * @param event
     *  Event object with the model instance of the changed table range.
     */
    "change:table": [event: ChangeTableEvent];

    /**
     * Will be emitted after the formatting attributes of an existing table
     * range have been changed.
     *
     * @param event
     *  Event object with the model instance of the changed table range.
     */
    "change:table:attrs": [event: ChangeTableAttrsEvent];

    /**
     * Will be emitted after the formatting attributes of a column of an
     * existing table range have been changed.
     *
     * @param event
     *  Event object with the model instance of the changed table range.
     */
    "change:table:column": [event: ChangeTableColumnEvent];
}

// class TableCollection ======================================================

/**
 * Stores settings for table ranges in a specific sheet. Table ranges contain
 * filter settings, sorting settings, and specific cell formatting.
 */
export class TableCollection extends SheetChildModel<TableCollectionEventMap> {

    // all table models, mapped by table key (uppercase table name)
    readonly #tableModelMap = this.member(new Map<string, TableModel>());

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this collection.
     */
    constructor(sheetModel: SheetModel) {

        // base constructor
        super(sheetModel);

        // additional processing and event handling after the document has been imported
        this.waitForImportSuccess(() => {

            // update table ranges after moving cells, or inserting/deleting columns or rows
            // TODO: generate explicit operations to update the tables (DOCS-2158)
            this.listenTo(sheetModel.cellCollection, "move:cells", this.#moveCellsHandler);
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the specified table exists in this collection.
     *
     * @param tableName
     *  The name of the table to check for. The empty string addresses the
     *  anonymous table range used to store filter settings for the standard
     *  autofilter of the sheet.
     *
     * @returns
     *  Whether the specified table exists in this collection.
     */
    hasTable(tableName: string): boolean {
        return this.#tableModelMap.has(mapKey(tableName));
    }

    /**
     * Returns whether this collection contains an autofilter range.
     *
     * @returns
     *  Whether this collection contains an autofilter range.
     */
    hasAutoFilter(): boolean {
        return this.#tableModelMap.has("");
    }

    /**
     * Returns the model of the table with the specified name.
     *
     * @param tableName
     *  The name of the table. The empty string addresses the anonymous table
     *  range used to store filter settings for the standard autofilter of the
     *  sheet.
     *
     * @returns
     *  The model of the table with the specified name; or `undefined`, if no
     *  table exists with that name.
     */
    getTableModel(tableName: string): Opt<TableModel> {
        return this.#tableModelMap.get(mapKey(tableName));
    }

    /**
     * Returns the model of the table representing the autofilter range.
     *
     * @returns
     *  The model of the table representing the autofilter range; or
     *  `undefined`, if this collection does not contain an autofilter.
     */
    getAutoFilterModel(): Opt<TableModel> {
        return this.#tableModelMap.get("");
    }

    /**
     * Returns the model of the table covering the specified cell.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  The model of the table at the specified position; or `null`, if the
     *  cell is not part of a table.
     */
    getTableModelAt(address: Address): Opt<TableModel> {
        return map.find(this.#tableModelMap, tableModel => tableModel.containsCell(address));
    }

    /**
     * Creates an iterator that visits the models of all table ranges in this
     * collection.
     *
     * @param [options]
     *  Optional parameters for selecting the tables to be included.
     *
     * @yields
     *  The matching table models.
     */
    *yieldTableModels(options?: TableIteratorOptions): IterableIterator<TableModel> {

        const autoFilter = options?.autoFilter;
        const overlapRange = options?.overlapRange;

        for (const tableModel of this.#tableModelMap.values()) {
            // skip autofilter unless it has to be included
            if (autoFilter || !tableModel.isAutoFilter()) {
                // restrict iterator to tables overlapping with the specified range
                if (!overlapRange || tableModel.overlapsRange(overlapRange)) {
                    yield tableModel;
                }
            }
        }
    }

    /**
     * Returns whether this collection contains at least one table model with
     * active filter rules.
     *
     * @returns
     *  Whether this collection contains at least one table model with active
     *  filter rules.
     */
    hasFilteredTables(): boolean {
        return itr.some(this.#tableModelMap.values(), tableModel => tableModel.isFiltered());
    }

    /**
     * Returns whether the passed ranges can be deleted with the tables in this
     * collection. The tables cannot be restored if they will be deleted
     * completely.
     *
     * @param ranges
     *  The addresses of the cell ranges to be deleted.
     *
     * @returns
     *  Whether the cell ranges can be deleted safely.
     */
    canRestoreDeletedRanges(ranges: Range[]): boolean {
        return itr.every(this.#tableModelMap.values(), tableModel => tableModel.canRestoreDeletedRanges(ranges));
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations, and the undo operations, to insert a new table
     * range into this collection.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param tableName
     *  The name for the new table to be inserted. The empty string refers to
     *  the anonymous table range used to store filter settings for the
     *  standard autofilter of the sheet.
     *
     * @param tableRange
     *  The address of the cell range covered by the new table.
     *
     * @param [attrSet]
     *  The initial attribute set for the table.
     *
     * @returns
     *  A promise that will fulfil when the operations have been generated, or
     *  that will reject with an object with property `cause` set to one of the
     *  following error codes:
     *  - "table:duplicate": The sheet already contains a table with the passed
     *    name.
     *  - "table:overlap": The passed cell range covers an existing table.
     *  - "operation": Internal error while applying the operation.
     */
    generateInsertTableOperations(sheetCache: SheetCache, tableName: string, tableRange: Range, attrSet?: PtTableAttributeSet): JPromise {

        // check existence of the table range (must be unique in entire document except for autofilter)
        const autoFilter = tableName.length === 0;
        const tableExists = autoFilter ? this.hasAutoFilter() : this.docModel.hasTable(tableName);
        OperationError.assertCause(!tableExists, "table:duplicate");

        // check that the range does not overlap with an existing table
        const tableModel = itr.shift(this.yieldTableModels({ overlapRange: tableRange, autoFilter: true }));
        OperationError.assertCause(!tableModel, "table:overlap");

        // "real" tables must not contain merged cells (neither in header, footer, nor data rows)
        let promise = autoFilter ? jpromise.resolve() : this.sheetModel.mergeCollection.generateMergeCellsOperations(sheetCache, tableRange, MergeMode.UNMERGE);

        // create the "insertTable" operation, and the appropriate undo operation
        promise = promise.then(() => {
            const props: Dict = { range: tableRange.toOpStr() };
            if (attrSet) { props.attrs = attrSet; }
            sheetCache.generateTableOperation(Op.DELETE_TABLE, tableName, undefined, { undo: true });
            sheetCache.generateTableOperation(Op.INSERT_TABLE, tableName, props);
        });

        // special handling for autofilters
        if (autoFilter) {

            // Bug 36152: Autofilter will hide dropdown buttons in header cells covered by merged ranges, but
            // only if the cells were already merged before creating the autofilter.
            promise = promise.then(() => {
                const mergedRanges = this.sheetModel.mergeCollection.yieldMergedRanges(tableRange.headerRow());
                return this.asyncForEach(mergedRanges, mergedRange => {
                    const col1 = Math.max(mergedRange.a1.c, tableRange.a1.c);
                    const col2 = Math.min(mergedRange.a2.c, tableRange.a2.c);
                    // the dropdown button in the LAST column of a merged range remains visible
                    const colAttrSet = { filter: { hideButton: true } };
                    for (let col = col1; col < col2; col += 1) {
                        // no undo operations necessary (the table will be deleted completely on undo)
                        sheetCache.generateTableOperation(Op.CHANGE_TABLE_COLUMN, tableName, { col: col - tableRange.a1.c, attrs: colAttrSet });
                    }
                });
            });

            // bug 56336: create or update the internal defined name
            promise = promise.then(() => this.sheetModel.nameCollection.generateFilterDatabaseOperations(sheetCache, tableRange));
        }

        return promise;
    }

    /**
     * Generates the operations and undo operations to update the table ranges
     * in this collection.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise {

        // only process move operations in the own sheet
        if (!this.sheetModel.isOwnMoveTask(updateTask)) { return jpromise.resolve(); }

        // process all table models
        return this.asyncForEach(this.#tableModelMap.values(), tableModel => tableModel.generateUpdateTaskOperations(sheetCache, updateTask));
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * table ranges from the passed collection into this collection.
     *
     * @param context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyCopySheetOperation(context: SheetOperationContext, fromCollection: TableCollection): void {

        // the new names for cloned table ranges (table names must be unique in the entire document)
        const tableNames = context.optDict("tableNames");

        // clone the contents of the source collection
        for (const tableModel of fromCollection.#tableModelMap.values()) {
            const oldTableName = tableModel.name;
            let newTableName: Opt<string> = oldTableName;
            if (!tableModel.isAutoFilter()) {
                newTableName = pick.string(tableNames, oldTableName);
                context.ensure(newTableName, 'missing replacement name for table "%s"', oldTableName);
            }
            this.#insertTableModel(context, new TableModel(this.sheetModel, newTableName, tableModel));
        }
    }

    /**
     * Callback handler for the document operation "insertTable". Creates and
     * inserts a new table into this collection.
     *
     * @param context
     *  A wrapper representing the "insertTable" document operation.
     */
    applyInsertOperation(context: SheetOperationContext): void {

        // get table name and map key from operation (table must not exist, throws on error)
        const tableName = this.#getModelData(context, true);

        // create and insert the new table model, notify listeners
        const tableRange = context.getRange("range");
        const tableModel = new TableModel(this.sheetModel, tableName, tableRange, context.optDict("attrs"));
        this.#insertTableModel(context, tableModel);
    }

    /**
     * Callback handler for the document operation "deleteTable". Deletes an
     * existing table from this collection.
     *
     * @param context
     *  A wrapper representing the "deleteTable" document operation.
     */
    applyDeleteOperation(context: SheetOperationContext): void {

        // resolve the table model addressed by the operation (throws on error)
        const tableModel = this.#getModelData(context);

        // delete the table model and notify listeners
        this.#deleteTableModel(context, tableModel);
    }

    /**
     * Callback handler for the document operation "changeTable". Changes the
     * position and/or attributes of an existing table.
     *
     * @param context
     *  A wrapper representing the "changeTable" document operation.
     */
    applyChangeOperation(context: SheetOperationContext): void {

        // resolve the table model addressed by the operation (throws on error)
        const tableModel = this.#getModelData(context);

        // get new table range
        const oldRange = tableModel.getRange();
        const newRange = context.optRange("range") ?? oldRange;
        const rangeChanged = oldRange.differs(newRange);

        // get new table name
        const oldName = tableModel.name;
        const newName = context.optStr("newName") || oldName;
        const nameChanged = oldName !== newName;
        if (nameChanged) {
            // not allowed to rename the autofilter
            context.ensure(!tableModel.isAutoFilter(), "autofilter cannot be renamed");
            // check that the name is not used in the entire document
            context.ensure(!this.docModel.hasTable(newName), "table name already used");
        }

        // change range and/or name, notify listeners
        if (rangeChanged || nameChanged) {

            const event: ChangeTableEvent = this.createSheetEvent({
                tableModel,
                range: rangeChanged ? [oldRange, newRange.clone()] : undefined,
                name: nameChanged ? [oldName, newName] : undefined
            }, context);
            this.trigger("change:table:before", event);

            if (rangeChanged) {
                tableModel.setRange(newRange);
            }

            if (nameChanged) {
                this.#tableModelMap.delete(tableModel.key);
                tableModel.setName(newName);
                this.#tableModelMap.set(tableModel.key, tableModel);
            }

            this.trigger("change:table", event);
        }

        // change all other table attributes (change events will be forwarded)
        if (context.has("attrs")) {
            tableModel.setAttributes(context.getDict("attrs"));
        }
    }

    /**
     * Callback handler for the document operation "changeTableColumn". Changes
     * the attributes of a column in an existing table.
     *
     * @param context
     *  A wrapper representing the "changeTableColumn" document operation.
     */
    applyChangeColumnOperation(context: SheetOperationContext): void {
        this.#getModelData(context).applyChangeOperation(context);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a descriptor for an existing table model addressed by the passed
     * operation context.
     *
     * @param context
     *  A wrapper representing a document operation for tables.
     *
     * @param [insert]
     *  If set to `true`, the table model addressed by the operation must not
     *  exist in this table collection. By default, the table model must exist
     *  in the collection.
     *
     * @returns
     *  If parameter "insert" has been omitted, the model of an existing table.
     *  If parameter "insert" has been set to `true`, the original table name
     *  for a new table as contained in the operation.
     *
     * @throws
     *  An `OperationException` if the operation does not address an existing
     *  table model, or if it addresses an existing table model that must not
     *  exist (see option "insert" above).
     */
    #getModelData(context: SheetOperationContext): TableModel;
    #getModelData(context: SheetOperationContext, insert: true): string;
    // implementation
    #getModelData(context: SheetOperationContext, insert?: true): TableModel | string {

        // get the table name from the operation (missing/empty name addresses autofilter)
        const tableName = context.optStr("table", { empty: true });
        const tableModel = this.#tableModelMap.get(mapKey(tableName));

        // check absence of the table model
        if (insert) {
            context.ensure(!tableModel && (!tableName || !this.docModel.hasTable(tableName)), "table exists");
            return tableName;
        }

        // check existence of the table model
        context.ensure(tableModel, "missing table");
        return tableModel;
    }

    /**
     * Inserts the passed table model into this collection, triggers an
     * "insert:table:before" event and a "insert:table" event, and registers a
     * listener for all events of the table model that will be forwarded to the
     * listeners of this collection.
     */
    #insertTableModel(context: SheetOperationContext, tableModel: TableModel): void {

        // insert the passed table model, notify listeners
        const event: TableEvent = this.createSheetEvent({ tableModel }, context);
        this.trigger("insert:table:before", event);
        this.#tableModelMap.set(tableModel.key, tableModel);
        this.trigger("insert:table", event);

        // forward attribute change events of the table model
        this.listenTo(tableModel, "change:attributes", (newAttrSet, oldAttrSet) => {
            this.trigger("change:table:attrs", this.createSheetEvent({ tableModel, oldAttrSet, newAttrSet }));
        });

        // forward attribute change events of the table column models
        this.listenTo(tableModel, "change:column:attrs", (tableCol, newAttrSet, oldAttrSet) => {
            this.trigger("change:table:column", this.createSheetEvent({ tableModel, tableCol, oldAttrSet, newAttrSet }));
        });
    }

    /**
     * Deletes an existing table model from this collection, and triggers a
     * "delete:table:before" event, and a "delete:table" event.
     */
    #deleteTableModel(context: SheetOperationContext, tableModel: TableModel): void {
        const event: TableEvent = this.createSheetEvent({ tableModel }, context);
        this.trigger("delete:table:before", event);
        this.#tableModelMap.delete(tableModel.key);
        this.trigger("delete:table", event);
        tableModel.destroy();
    }

    /**
     * Recalculates the target ranges of all table ranges, after cells have
     * been moved (including inserted/deleted columns or rows) in the sheet.
     */
    #moveCellsHandler({ transformer }: MoveCellsEvent): void {
        // TODO: remove when implementing DOCS-2158
        for (const tableModel of this.#tableModelMap.values()) {
            tableModel.transformRange(transformer);
        }
    }
}
