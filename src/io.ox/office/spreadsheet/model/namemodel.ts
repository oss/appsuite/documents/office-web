/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict, jpromise } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import type { NameOperationFormulaProps } from "@/io.ox/office/spreadsheet/utils/operations";
import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { type OptSheet, mapKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { OperationBuilder } from "@/io.ox/office/spreadsheet/model/operation/builder";
import type { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { FormulaParentModel, GrammarType } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { resolveParentModel } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { type FormulaUpdateTask, RenameSheetUpdateTask, RelabelNameUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface NameModelConfig {

    /**
     * The address of the reference cell used to interpret relative cell
     * references in the formula expression. The reference cell must be a
     * formula expression by itself that results in a single cell address with
     * sheet name.
     */
    ref?: string;

    /**
     * Whether the defined name should be hidden in the user interface. Default
     * value is `false`.
     */
    hidden?: boolean;
}

/**
 * Configuration for a formula expression to be bound to a defined name.
 */
export interface NameFormulaSpec {

    /**
     * The identifier of the formula grammar to be used to parse the formula
     * expression.
     */
    grammarType: GrammarType;

    /**
     * The formula expression to be bound to the new defined name.
     */
    formula: string;

    /**
     * The index of the reference sheet the formula is related to.
     */
    refSheet: number;

    /**
     * The address of the reference cell used to interpret relative cell
     * references in the formula expression.
     */
    refAddress: Address;
}

// public functions ===========================================================

/**
 * Helper function for the ODF file format. Returns whether the passed formula
 * array contains a complex expression, or a simple cell reference.
 *
 * @param tokenArray
 *  A token array with a formula.
 *
 * @returns
 *  Whether the passed token array represents a complex formula expression
 *  (`true`), or a simple cell reference (`false`).
 */
export function isComplexExpression(tokenArray: TokenArray): boolean {
    const ranges = tokenArray.resolveRangeList(Address.A1, Address.A1);
    return !ranges || (ranges.length !== 1);
}

/**
 * Creates the operation properties for the passed formula expression, intended
 * to be used in the document operations "insertName" and "changeName". The
 * formula expression will be checked for syntax and semantic errors, and it
 * will be relocated for usage in document operations.
 *
 * @param parentModel
 *  The spreadsheet document model for globally defined names, or the sheet
 *  model for sheet-local names.
 *
 * @param fmlaSpec
 *  Configuration for the formula expression.
 *
 * @returns
 *  On success, an object with an existing property `formula` containing the
 *  relocated formula expression, and an optional property `ref` with the
 *  passed reference address (as formula expression) for ODF documents.
 */
export function getFormulaProperties(parentModel: FormulaParentModel, fmlaSpec: NameFormulaSpec): Opt<NameOperationFormulaProps> {

    // parse the formula expression (bug 40293: add sheet name to references without sheet)
    const tokenArray = new TokenArray(parentModel, "name");
    tokenArray.parseFormula(fmlaSpec.grammarType, fmlaSpec.formula, Address.A1, { autoCorrect: true, refSheet: fmlaSpec.refSheet, extendSheet: parentModel.docApp.isOOXML() });

    // interpret the formula to find syntax errors or semantic errors
    const formulaResult = tokenArray.interpretFormula("any", Address.A1, Address.A1);
    if (formulaResult.type === "error") { return undefined; }

    // build the formula descriptor according to the file format
    if (parentModel.docApp.isODF()) {
        // ODF: leave formula expression unmodified, pass reference cell with operation
        const props: NameOperationFormulaProps = { formula: tokenArray.getFormulaOp(), isExpr: isComplexExpression(tokenArray) };
        // store reference cell as formula expression with sheet name
        tokenArray.clearTokens();
        tokenArray.appendAddress(fmlaSpec.refAddress, { sheet: fmlaSpec.refSheet, abs: true });
        props.ref = tokenArray.getFormulaOp();
        return props;
    }

    // OOXML: relocate the formula expression from reference cell to A1
    return { formula: tokenArray.getFormula("op", fmlaSpec.refAddress, Address.A1) };
}

/**
 * Ensures that the passed name label is valid according to the document
 * model's formula grammar.
 *
 * @param docModel
 *  The spreadsheet document model.
 *
 * @param label
 *  The name label to be checked.
 *
 * @throws
 *  An `OperationError` if the passed label is valid for usage in formulas.
 */
export function validateLabel(docModel: SpreadsheetModel, label: string): void {
    OperationError.throwCauseIf(docModel.formulaGrammarUI.validateNameLabel(docModel, label));
}

// class NameModel ============================================================

/**
 * Stores settings for a single defined name in a specific sheet, or for a
 * global defined name in the document.
 */
export class NameModel extends ModelObject<SpreadsheetModel> implements Keyable {

    /**
     * The parent document model of global names, or sheet model of sheet-local
     * names.
     */
    readonly parentModel: FormulaParentModel;

    /**
     * The parent sheet model of sheet-local names.
     */
    readonly sheetModel: Opt<SheetModel>;

    /**
     * The token array representing the definition of this name.
     */
    readonly tokenArray: TokenArray;

    // the current UI label of the defined name
    #label: string;
    // whether the defined name is hidden in the GUI
    readonly #hidden: boolean;
    // special behavior for ODF files
    readonly #odf: boolean;
    // the token array representing the reference address for ODF
    readonly #refTokenArray: TokenArray;

    // constructor ------------------------------------------------------------

    /**
     * @param parentModel
     *  The spreadsheet document model for global names, or the sheet model for
     *  sheet-local names containing this instance.
     *
     * @param label
     *  The exact label of this defined name, with correct character case as
     *  used in formula expressions.
     *
     * @param formula
     *  The initial formula expression bound to the defined name.
     */
    constructor(parentModel: FormulaParentModel, label: string, formula: string, config?: NameModelConfig) {

        const { docModel, sheetModel } = resolveParentModel(parentModel);
        super(docModel);

        this.parentModel = parentModel;
        this.sheetModel = sheetModel;
        this.tokenArray = new TokenArray(parentModel, "name");

        this.#label = label;
        this.#hidden = !!config?.hidden;
        this.#odf = this.docApp.isODF();
        this.#refTokenArray = new TokenArray(parentModel, "name");

        // parse the initial formula definition
        this.setFormula(formula, this.#odf ? config?.ref : undefined);
    }

    // public accessors -------------------------------------------------------

    /**
     * The zero-based index of the parent sheet model of a sheet-local name; or
     * `null` for a global name.
     */
    get sheet(): OptSheet {
        return this.sheetModel?.getIndex() ?? null;
    }

    /**
     * The exact label of this name model, with correct character case as used
     * in formula expressions.
     */
    get label(): string {
        return this.#label;
    }

    /**
     * The unique map key of this name model (the uppercase label of the name).
     */
    get key(): string {
        return mapKey(this.#label);
    }

    /**
     * Whether the defined name should be hidden in the user interface.
     */
    get hidden(): boolean {
        return this.#hidden;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the formula expression of this name model, relative to the
     * passed target address.
     *
     * @param grammarType
     *  The identifier of the formula grammar to be used to generate the
     *  formula expression.
     *
     * @param [targetAddress]
     *  The target address to relocate the formula expression to. If omitted,
     *  the current non-relocated formula expression according to the internal
     *  reference address will be returned.
     *
     * @returns
     *  The formula expression of this name model.
     */
    getFormula(grammarType: GrammarType, targetAddress?: Address): string {
        return this.tokenArray.getFormula(grammarType, Address.A1, targetAddress ?? Address.A1);
    }

    /**
     * Returns the formula expression of the reference address of this name
     * model. Used for the ODF format only.
     *
     * @param grammarType
     *  The identifier of the formula grammar to be used to generate the
     *  formula expression.
     *
     * @returns
     *  The formula expression of the reference address of this name model.
     */
    getRefFormula(grammarType: GrammarType): string {
        return this.#refTokenArray.getFormula(grammarType, Address.A1, Address.A1);
    }

    /**
     * Changes the label of this defined name.
     *
     * @param label
     *  The new label for this defined name.
     */
    setLabel(label: string): void {
        this.#label = label;
    }

    /**
     * Changes the formula definition of this name model.
     *
     * @param defFormula
     *  The new formula expression bound to the name model.
     *
     * @param [refFormula]
     *  The address of the reference cell used to interpret relative cell
     *  references in the formula expression. The reference cell must be a
     *  formula expression by itself that results in a single cell address with
     *  sheet name.
     */
    setFormula(defFormula: string, refFormula?: string): void {

        // parse the formula expression
        this.tokenArray.parseFormula("op", defFormula, Address.A1);

        // relocate formula according to reference address (used by ODF only, OOXML always uses A1)
        if (refFormula) {

            // parse the passed reference address expression
            this.#refTokenArray.parseFormula("op", refFormula, Address.A1);

            // try to convert to a single reference address (do not pass a reference
            // sheet index, sheet name is expected to exist in the formula expression)
            const ranges = this.#refTokenArray.resolveRangeList(Address.A1, Address.A1);

            // if the expression resolves to a single cell address, use it to relocate the formula
            if ((ranges.length === 1) && ranges[0].single()) {
                this.tokenArray.relocateRanges(ranges[0].a1, Address.A1);
            }
        }
    }

    /**
     * Creates and returns a clone of this name model.
     *
     * @param parentModel
     *  The target parent model that will contain the clone, either the
     *  spreadsheet document model for global names, or a sheet model for
     *  sheet-local names.
     */
    cloneFor(parentModel: FormulaParentModel): NameModel {
        // use `Required` type to ensure to clone all configuration options
        const config: Required<NameModelConfig> = {
            ref: this.#refTokenArray.getFormulaOp(),
            hidden: this.#hidden,
        };
        return new NameModel(parentModel, this.#label, this.tokenArray.getFormulaOp(), config);
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations, and the undo operations, to delete this
     * defined name.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     */
    generateDeleteOperations(builder: OperationBuilder): void {

        // the properties for the operation, and the undo operation
        const operProps: Dict = { label: this.#label };
        const undoProps: Dict = { label: this.#label, formula: this.getFormula("op", Address.A1) };

        // add special undo properties for ODF
        if (this.#odf) {
            undoProps.isExpr = isComplexExpression(this.tokenArray);
            undoProps.ref = this.getRefFormula("op");
        }

        // add the sheet index to the operation properties
        const sheet = this.sheet;
        if (sheet !== null) {
            operProps.sheet = undoProps.sheet = sheet;
        }

        // more undo properties
        if (this.#hidden) { undoProps.hidden = true; }

        // generate the "deleteName" operation, and the undo operation
        const generator = builder.headGenerator;
        generator.generateOperation(Op.DELETE_NAME, operProps);
        generator.generateOperation(Op.INSERT_NAME, undoProps, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to change the label
     * or formula definition of this defined name.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param newLabel
     *  The new label for the defined name. If set to `null`, the label of the
     *  defined name will not be changed.
     *
     * @param fmlaSpec
     *  Configuration for the formula expression to be bound to the defined
     *  name. If set to `null`, the formula expression of the defined name will
     *  not be changed (an invalid formula expression in the defined name will
     *  be retained).
     *
     * @returns
     *  A promise that will fulfil when the operations have been generated, or
     *  that will be rejected with an object with property "cause" set to one
     *  of the following error codes:
     *  - "name:used": A defined name, or a table range, with the passed label
     *    exists already.
     *  - "name:empty": The new label is empty.
     *  - "name:invalid": The new label contains invalid characters.
     *  - "name:address": The new label would be valid, but conflicts with the
     *    representation of a relative cell reference in A1 notation, or a cell
     *    reference in R1C1 notation (either English, e.g. "R1C1", or according
     *    to current UI language, e.g. "Z1S1" in German).
     *  - "formula:invalid": The passed formula expression is invalid.
     *  - "operation": Internal error while creating the operations.
     */
    generateChangeOperations(builder: OperationBuilder, newLabel: string | null, fmlaSpec: NameFormulaSpec | null): JPromise {

        // the properties for the "changeName" operation
        const operProps = dict.create();
        const undoProps = dict.create();

        // check the passed new label (ignore unchanged existing invalid labels, e.g. from import filter)
        newLabel = (is.string(newLabel) && (this.#label !== newLabel)) ? newLabel : null;
        if (newLabel) {

            // the label must be valid
            validateLabel(this.docModel, newLabel);

            // new label must not exist yet in the document (but allow to change case of an existing label)
            const { nameCollection } = this.parentModel;
            if ((this.key !== mapKey(newLabel)) && nameCollection.isLabelUsed(newLabel)) {
                OperationError.throwCause("name:used");
            }

            // add the new label to the operation properties
            operProps.newLabel = newLabel;
            undoProps.newLabel = this.#label;
        }

        // parse and validate the formula expression
        if (fmlaSpec) {
            Object.assign(operProps, getFormulaProperties(this.parentModel, fmlaSpec));
            OperationError.assertCause("formula" in operProps, "formula:invalid");

            // add the current formula expression to the undo properties (ODF: current reference address as formula too)
            undoProps.formula = this.getFormula("op");
            if (this.#odf) {
                undoProps.isExpr = isComplexExpression(this.tokenArray);
                undoProps.ref = this.getRefFormula("op");
            }
        }

        // nothing to do without any changed properties
        if (is.empty(operProps)) { return jpromise.resolve(); }

        // add the label to the operation properties (use new label to address the name in undo)
        const oldLabel = operProps.label = this.#label;
        undoProps.label = newLabel ?? oldLabel;

        // add the sheet index to the operation properties
        const sheet = this.sheet;
        if (sheet !== null) {
            operProps.sheet = undoProps.sheet = sheet;
        }

        // generate the "changeName" operation, and the undo operation
        const generator = builder.headGenerator;
        generator.generateOperation(Op.CHANGE_NAME, undoProps, { undo: true });
        generator.generateOperation(Op.CHANGE_NAME, operProps);

        // nothing more to do, if the label has not been changed
        if (!newLabel) { return jpromise.resolve(); }

        // update all formula expressions in the document
        const updateTask = new RelabelNameUpdateTask(sheet, oldLabel, newLabel);
        return this.docModel.generateUpdateTaskOperations(builder, updateTask);
    }

    /**
     * Generates the operations and undo operations to update or restore the
     * formula expression of this defined name.
     *
     * @param generator
     *  The operation generator to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     */
    generateUpdateTaskOperations(generator: SheetOperationGenerator, updateTask: FormulaUpdateTask): void {

        // the formula grammar used for formula expression transformations
        const grammar = this.docModel.formulaGrammarOP;

        // the properties for the change operation, and the undo operation
        const operProps = dict.create();
        const undoProps = dict.create();

        // resolve the formula expression for the definition of this name
        updateTask.transformOpProperty(this.tokenArray, grammar, "formula", operProps, undoProps);

        // ODF only: update the formula expression for the reference address for renamed sheets
        // TODO: move reference address to another existing sheet when deleting a sheet
        if (this.#odf && (updateTask instanceof RenameSheetUpdateTask)) {
            updateTask.transformOpProperty(this.#refTokenArray, grammar, "ref", operProps, undoProps);
        }

        // generate the operation to change this name model
        if (!is.empty(operProps)) {
            operProps.label = undoProps.label = this.#label;
            // use the sheet index contained in the generator (may differ from own sheet index, e.g. when
            // generating operations for a copied sheet)
            if (this.sheetModel) { operProps.sheet = undoProps.sheet = generator.sheet; }
            generator.generateOperation(Op.CHANGE_NAME, operProps);
            generator.generateOperation(Op.CHANGE_NAME, undoProps, { undo: true });
        }
    }
}
