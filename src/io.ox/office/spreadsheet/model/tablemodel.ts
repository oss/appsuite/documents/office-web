/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, itr, ary, map, set, jpromise } from "@/io.ox/office/tk/algorithms";
import { BREAK } from "@/io.ox/office/tk/utils";

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";
import type { PtAttrSet, StyledAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { type AttributeConfigBag, AttributeScope } from "@/io.ox/office/editframework/model/attributefamilypool";
import type { AttributedModelEventMap } from "@/io.ox/office/editframework/model/attributedmodel";
import type { TableStyleFlags } from "@/io.ox/office/editframework/model/tablestylecollection";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { type SortRule, mapKey, modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";

import { type SheetModel, SheetAttrsModel } from "@/io.ox/office/spreadsheet/model/sheetattrsmodel";
import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { GeneratorOptions, SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { type TableColumnAttributeSet, type PtTableColumnAttributeSet, TableColumnModel } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import { CellValueCache } from "@/io.ox/office/spreadsheet/model/cellvaluecache";
import type { RowAttributes } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import type { ChangeSetInterval } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import type { MoveCellsUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

// types ======================================================================

export interface TableAttributes {

    /**
     * Whether the drop-down buttons of all columns will be hidden.
     */
    hideButtons: boolean;

    /**
     * Whether the first row of the table is a header row. Header rows will not
     * be filtered or sorted.
     */
    headerRow: boolean;

    /**
     * Whether the last row of the table is a footer row. Footer rows will not
     * be filtered or sorted.
     */
    footerRow: boolean;

    /**
     * Whether to show special formatting for the cells in the first table
     * column, according to the current table style sheet.
     */
    firstCol: boolean;

    /**
     * Whether to show special formatting for the cells in the last table
     * column, according to the current table style sheet.
     */
    lastCol: boolean;

    /**
     * Whether to show alternating row formatting for the data cells, according
     * to the current table style sheet.
     */
    bandsHor: boolean;

    /**
     * Whether to show alternating column formatting for the data cells,
     * according to the current table style sheet.
     */
    bandsVert: boolean;

    /**
     * Whether the string values in the table range will be sorted regarding
     * character case.
     */
    caseSensitive: boolean;
}

/**
 * The type shape of attribute sets supported by table ranges.
 */
export interface TableAttributeSet extends StyledAttributeSet {
    table: TableAttributes;
}

/**
 * Partial table attribute set (all families and attributes are optional).
 */
export type PtTableAttributeSet = PtAttrSet<TableAttributeSet>;

/**
 * Resolved settings needed to format a cell in a table range.
 */
export interface TableCellFormatResult {
    cellAttrSet: PtCellAttributeSet;
    attrSetFlags: number;
}

export interface CellContents {
    value: ScalarType;
    style: string;
    attributes: Dict;
    format: ParsedFormat;
    display: string;
    count: number;
}

/**
 * Options for querying filetred cells of a table range.
 */
export interface QueryFilterOptions {

    /**
     * The zero-based indexes of all table columns that will be excluded from
     * filtering (its filter settings will be ignored).
     */
    skipColumns?: number[];
}

/**
 * Options for generating operations to refresh a table range.
 */
export interface RefreshTableOptions extends QueryFilterOptions {

    /**
     * If set to `true`, the visibility of the data rows will be updated
     * according to the current filter rules. Default value is `false`.
     */
    updateFilter?: boolean;

    /**
     * If set to `true`, the order of the data rows will be updated according
     * to the current sorting rules. Default value is `false`.
     */
    updateSort?: boolean;
}

export interface GenerateChangeColumnOperationsOptions {

    /**
     * If set to `true`, the sort settings of all other columns will be reset.
     * Default value is `false`.
     */
    resetSort?: boolean;
}

/**
 * Type mapping for the events emitted by `TableModel` instances.
 */
export interface TableModelEventMap extends AttributedModelEventMap<TableAttributeSet> {

    /**
     * Will be emitted after the formatting attributes of a table column have
     * been changed.
     *
     * @param tableCol
     *  The zero-based index of the table column.
     *
     * @param newAttrSet
     *  The current (new) merged attribute set.
     *
     * @param oldAttrSet
     *  The previous (old) merged attribute set.
     */
    "change:column:attrs": [tableCol: number, newAttrSet: TableColumnAttributeSet, oldAttrSet: TableColumnAttributeSet];
}

// constants ==================================================================

export const TABLE_ATTRIBUTES: AttributeConfigBag<TableAttributes> = {
    hideButtons:    { def: false, scope: AttributeScope.ELEMENT },
    headerRow:      { def: false, scope: AttributeScope.ELEMENT },
    footerRow:      { def: false, scope: AttributeScope.ELEMENT },
    firstCol:       { def: false, scope: AttributeScope.ELEMENT },
    lastCol:        { def: false, scope: AttributeScope.ELEMENT },
    bandsHor:       { def: false, scope: AttributeScope.ELEMENT },
    bandsVert:      { def: false, scope: AttributeScope.ELEMENT },
    caseSensitive:  { def: false, scope: AttributeScope.ELEMENT }
};

// private functions ==========================================================

function createChangeSetIntervals(intervals: Interval[], attrs: Partial<RowAttributes>): ChangeSetInterval[] {
    const changeIntervals = intervals as ChangeSetInterval[];
    for (const interval of changeIntervals) { interval.fillData = { attrs }; }
    return changeIntervals;
}

// class TableModel ===========================================================

/**
 * Stores settings for a single table range in a specific sheet.
 *
 */
export class TableModel extends SheetAttrsModel<TableAttributeSet, TableModelEventMap> implements Keyable {

    // the name of the table (preserving character case)
    #tableName: string;
    // the cell range covered by the table (including header/footer row)
    #tableRange: Range;

    // all existing column models (with active filter or sorting settings), mapped by column index
    readonly #columnModelMap = this.member(new Map<number, TableColumnModel>());
    // column indexes, mapped by column models
    readonly #colIndexMap = new WeakMap<TableColumnModel, number>();
    // sorting order of table columns (using the fact that sets maintain elements in insertion order by definition)
    readonly #sortOrderSet = new Set<TableColumnModel>();
    // default column model for all unmodified columns
    readonly #defColumnModel: TableColumnModel;
    // the cache for header cell labels (used as column names)
    readonly #headerCache: CellValueCache;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this table.
     *
     * @param tableName
     *  The name of this table.
     *
     * @param tableRange
     *  The address of the cell range covered by this table.
     *
     * @param [attrSet]
     *  The initial table attribute set (style sheet reference, and explicit
     *  table attributes).
     *
     * @param cloneModel
     *  A table column model to be cloned.
     */
    constructor(sheetModel: SheetModel, tableName: string, tableRange: Range, attrSet?: PtTableAttributeSet);
    constructor(sheetModel: SheetModel, tableName: string, cloneModel: TableModel);
    // implementation
    constructor(sheetModel: SheetModel, tableName: string, cloneModelOrTableRange: Range | TableModel, attrSet?: PtTableAttributeSet) {

        // whether constructor is used as clone constructor
        const clone = cloneModelOrTableRange instanceof TableModel;
        const cloneModel = clone ? cloneModelOrTableRange : undefined;
        const tableRange = clone ? undefined : cloneModelOrTableRange;

        // base constructor
        if (cloneModel) { attrSet = cloneModel.getExplicitAttributeSet(true); }
        super(sheetModel, [], attrSet, { styleFamily: "table", autoClear: true });

        // initialize private properties
        this.#tableName = tableName;
        this.#tableRange = (cloneModel ? cloneModel.#tableRange : tableRange!).clone();
        this.#defColumnModel = this.member(new TableColumnModel(sheetModel));
        this.#headerCache = this.member(new CellValueCache(sheetModel, this.getHeaderRange()));

        // clone column models of passed source table model
        if (cloneModel) {
            for (const [tableCol, columnModel] of cloneModel.#columnModelMap) {
                this.#insertColumnModel(tableCol, new TableColumnModel(sheetModel, columnModel));
            }
        }

        // update column models after changing table attributes
        this.on("change:attributes", (newAttrSet, oldAttrSet) => {
            // reset the header cache if the existence of the table header changes
            // TODO: filter needs to send column names for tables without header row
            if (newAttrSet.table.headerRow !== oldAttrSet.table.headerRow) {
                this.#headerCache.setRanges(this.getHeaderRange());
            }
        });
    }

    // public accessors -------------------------------------------------------

    /**
     * The name of this table.
     */
    get name(): string {
        return this.#tableName;
    }

    /**
     * The unique map key of this table (the uppercase name of the table).
     */
    get key(): string {
        return mapKey(this.#tableName);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this table is the special autofilter range of the sheet.
     *
     * @returns
     *  Whether this table is the special autofilter range of the sheet.
     */
    isAutoFilter(): boolean {
        return this.#tableName.length === 0;
    }

    /**
     * Returns the address of the cell range covered by this table.
     *
     * @returns
     *  The address of the cell range covered by this table.
     */
    getRange(): Range {
        return this.#tableRange.clone();
    }

    /**
     * Returns whether the passed cell address is covered by this table.
     *
     * @param address
     *  The address of the cell to be checked.
     *
     * @returns
     *  Whether the specified cell is covered by this table.
     */
    containsCell(address: Address): boolean {
        return this.#tableRange.containsAddress(address);
    }

    /**
     * Returns whether the passed cell range address overlaps with the cell
     * range of this table.
     *
     * @param range
     *  The address of the cell range to be checked.
     *
     * @returns
     *  Whether the passed cell range address overlaps with the cell range of
     *  this table.
     */
    overlapsRange(range: Range): boolean {
        return this.#tableRange.overlaps(range);
    }

    /**
     * Returns whether this table range contains a header row. Cells in the
     * header row (the top-most row) will neither be filtered nor sorted.
     *
     * @returns
     *  Whether this table range contains a header row.
     */
    hasHeaderRow(): boolean {
        return this.getMergedAttributeSet(true).table.headerRow;
    }

    /**
     * Returns whether this table range contains a footer row. Cells in the
     * footer row (the bottom-most row) will neither be filtered nor sorted.
     *
     * @returns
     *  Whether this table range contains a footer row.
     */
    hasFooterRow(): boolean {
        return this.getMergedAttributeSet(true).table.footerRow;
    }

    /**
     * Returns the identifier of the table style sheet used to format the cells
     * covered by this table range.
     *
     * @returns
     *  The identifier of the table style sheet used to format the cells
     *  covered by this table range; or `null`, if the table is not formatted
     *  by a table style sheet.
     */
    getStyleId(): string | null {
        return this.getMergedAttributeSet(true).styleId || null;
    }

    /**
     * Returns the table style flags used to decide which conditioal parts of a
     * table style sheet need to be applied to the cells covered by this table
     * range.
     *
     * @returns
     *  A set of flags determining which parts of a table stylesheet will be
     *  used to format the cells of a table range.
     */
    getStyleFlags(): TableStyleFlags {
        const tableAttrs = this.#getTableAttrs();
        return {
            firstRow: tableAttrs.headerRow,
            lastRow: tableAttrs.footerRow,
            firstCol: tableAttrs.firstCol,
            lastCol: tableAttrs.lastCol,
            bandsHor: tableAttrs.bandsHor,
            bandsVert: tableAttrs.bandsVert
        };
    }

    /**
     * Return the attributes for a specific table cell, depending on its
     * position in the table.
     *
     * @param address
     *  The address of the cell to resolve the table attributes for. This
     *  address MUST be located inside the range of the passed table model.
     *
     * @param [rawStyleAttrSet]
     *  The resolved raw attribute set of a table stylesheet to be used. If
     *  omitted, the table stylesheet referred by the own attributes will be
     *  used instead.
     *
     * @returns
     *  The resulting resolved attribute set for the specified cell.
     */
    resolveCellAttributeSet(address: Address, rawStyleAttrSet?: object): Opt<TableCellFormatResult> {

        // resolve raw style attribute map from table style identifier
        if (!rawStyleAttrSet) {
            const styleId = this.getStyleId();
            if (!styleId) { return undefined; }
            rawStyleAttrSet = this.docModel.tableStyles.getStyleSheetAttributeMap(styleId, true);
        }

        // convert passed cell address to relative column/row indexes in the table range
        const range = this.#tableRange;
        const colIdx = address.c - range.a1.c;
        const rowIdx = address.r - range.a1.r;

        // resolve the attribute set of the table cell
        const { cellAttrSet, attrSetFlags } = this.docModel.tableStyles.resolveCellAttributeSet(rawStyleAttrSet, colIdx, rowIdx, range.cols(), range.rows(), this.getStyleFlags());

        // remove table attributes (TODO: already in `resolveCellAttributeSet`?)
        delete cellAttrSet.table;
        if (is.empty(cellAttrSet)) { return undefined; }

        // create the Spreadsheet specific result object
        return { cellAttrSet: cellAttrSet as PtCellAttributeSet, attrSetFlags };
    }

    /**
     * Returns whether the filter/sort dropdown buttons will be displayed for
     * this table range.
     *
     * @returns
     *  Whether the filter/sort dropdown button will be displayed for this
     *  table range.
     */
    areButtonsVisible(): boolean {
        const tableAttrs = this.#getTableAttrs();
        return tableAttrs.headerRow && !tableAttrs.hideButtons;
    }

    /**
     * Returns whether this table model contains at least one column with
     * active filter rules.
     *
     * @returns
     *  Whether this table model contains at least one column with active
     *  filter rules.
     */
    isFiltered(): boolean {
        return itr.some(this.#columnModelMap.values(), columnModel => columnModel.isFiltered());
    }

    /**
     * Returns whether this table model contains at least one column with
     * active sort rules.
     *
     * @returns
     *  Whether this table model contains at least one column with active sort
     *  rules.
     */
    isSorted(): boolean {
        return itr.some(this.#columnModelMap.values(), columnModel => columnModel.isSorted());
    }

    /**
     * Returns the current rules for sorting.
     *
     * @returns
     *  An array of sorting rules, in order of sort precedence (always with
     *  absolute column indexes, NOT table-relative column indexes).
     */
    getSortRules(): SortRule[] {
        const startCol = this.#tableRange.a1.c;
        return Array.from(this.#sortOrderSet, columnModel => ({
            index: startCol + this.#colIndexMap.get(columnModel)!,
            descending: columnModel.isDescending()
        }));
    }

    /**
     * Returns the address of the header row of this table.
     *
     * @returns
     *  The address of the header row of this table; or `undefined`, if this
     *  table does not contain a header row.
     */
    getHeaderRange(): Opt<Range> {
        return this.hasHeaderRow() ? this.#tableRange.headerRow() : undefined;
    }

    /**
     * Returns the address of the footer row of this table.
     *
     * @returns
     *  The address of the footer row of this table; or `undefined`, if this
     *  table does not contain a footer row.
     */
    getFooterRange(): Opt<Range> {
        return this.hasFooterRow() ? this.#tableRange.footerRow() : undefined;
    }

    /**
     * Returns the address of the data range in this table (the cell range
     * without the header and footer rows). For the special autofilter table,
     * the data range will be expanded to the bottom to include all existing
     * content cells following the real data range.
     *
     * @returns
     *  The address of the data range of this table; or `undefined`, if no data
     *  rows are available (e.g., if the table consists of a header row only).
     */
    getDataRange(): Opt<Range> {

        // whether the table contains header and/or footer rows
        const hasHeader = this.hasHeaderRow();
        const hasFooter = this.hasFooterRow();

        // the resulting data range
        let dataRange = this.#tableRange.clone();

        // expand data range of autofilter to the bottom (before removing
        // the header row from the range, to not invalidate the range address)
        if (!hasFooter && this.isAutoFilter()) {
            dataRange = this.sheetModel.cellCollection.findContentRange(dataRange, { directions: "down" });

            // restrict expansion to the next table range below the autofilter
            if (this.#tableRange.a2.r < dataRange.a2.r) {
                const boundRange = Range.fromIndexes(this.#tableRange.a1.c, this.#tableRange.a2.r + 1, this.#tableRange.a2.c, this.docModel.addressFactory.maxRow);
                for (const tableModel of this.sheetModel.tableCollection.yieldTableModels({ overlapRange: boundRange })) {
                    dataRange.a2.r = Math.min(dataRange.a2.r, tableModel.getRange().a1.r - 1);
                }
            }
        }

        // exclude header/footer rows from returned range
        if (hasHeader) { dataRange.a1.r += 1; }
        if (hasFooter) { dataRange.a2.r -= 1; }

        // check existence of data rows
        return (dataRange.a1.r <= dataRange.a2.r) ? dataRange : undefined;
    }

    /**
     * Returns the relative intervals of all visible rows in the data range of
     * this table.
     *
     * @returns
     *  The visible row intervals in the data range of this table; or an empty
     *  array, if the table does not contain any data rows. The properties
     *  "first" and "last" of each index interval are relative to the data
     *  range of the table (the index 0 refers to the first data row of this
     *  table!).
     */
    getVisibleDataRows(): IntervalArray {

        // the address of the data range
        const dataRange = this.getDataRange();

        // return empty interval array, if data range is missing
        if (!dataRange) { return new IntervalArray(); }

        // the absolute row intervals
        const rowIntervals = this.sheetModel.rowCollection.getVisibleIntervals(dataRange.rowInterval());

        // transform to relative row intervals
        return rowIntervals.move(-dataRange.a1.r);
    }

    /**
     * Returns whether the passed ranges can be deleted with this table. The
     * table cannot be restored if it will be deleted completely (unless this
     * is the autofilter).
     *
     * @param ranges
     *  The addresses of the cell ranges to be deleted.
     *
     * @returns
     *  Whether the cell ranges can be deleted safely.
     */
    canRestoreDeletedRanges(ranges: Range[]): boolean {
        return this.isAutoFilter() || ranges.every(range => !range.contains(this.#tableRange));
    }

    /**
     * Changes the name of this table.
     *
     * @param tableName
     *  The new name of the table.
     */
    setName(tableName: string): void {
        this.#tableName = tableName;
    }

    /**
     * Changes the address of the cell range covered by this table.
     *
     * @param tableRange
     *  The address of the new cell range.
     */
    setRange(tableRange: Range): void {

        // store the new range (unless nothing will change)
        if (this.#tableRange.equals(tableRange)) { return; }

        // change the table range
        this.#tableRange = tableRange.clone();

        // reset the map used to resolve column names to their indexes (will be created on demand)
        this.#headerCache.setRanges(this.getHeaderRange());

        // delete column models outside the new range
        const colCount = this.#tableRange.cols();
        for (const tableCol of this.#columnModelMap.keys()) {
            if (tableCol >= colCount) {
                this.#destroyColumnModel(tableCol);
            }
        }
    }

    /**
     * Transforms the cell range address of this table, after cells have been
     * moved in the sheet, including inserted/deleted columns or rows.
     *
     * @param transformer
     *  An address transformer that specifies how to transform the target range
     *  of this table model.
     */
    transformRange(transformer: AddressTransformer): void {

        // nothing to do, if transformer does not cover the table range completely in move direction
        if (!transformer.bandContainsRange(this.#tableRange)) { return; }

        // preprocessing when deleting rows in the table range
        if (!transformer.insert && !transformer.columns) {

            // bug 36226: delete tables whose header row has been deleted
            // bug 39869: deleting header rows prevented by UI except for autofilter
            const headerRange = this.getHeaderRange();
            if (headerRange && !transformer.transformRanges(headerRange).length) {
                return;
            }
        }

        // rescue first column index of the old table range (bug 65280: before transformation)
        const startCol = this.#tableRange.a1.c;

        // transform the table range (nothing more to do, if the range does not change at all)
        const newTableRange = transformer.transformRanges(this.#tableRange).first();
        if (!newTableRange) {
            modelLogger.error(`$badge{TableModel} transformRange: unexpected deleted table, range=${this.#tableRange}`);
            return;
        }
        if (this.#tableRange.equals(newTableRange)) { return; }

        // delete or remap all affected column models
        if (transformer.columns) {

            // copy models to temporary maps to prevent interfering with existing map entries
            const oldColumnModelMap = new Map(this.#columnModelMap);
            this.#columnModelMap.clear();

            // create a new model map to prevent interfering with existing map entries
            for (const [tableCol, columnModel] of oldColumnModelMap) {

                // transform the absolute column index according to the operation
                const oldAddress = new Address(startCol + tableCol, this.#tableRange.a1.r);
                const newAddress = transformer.transformAddress(oldAddress);

                // delete column model of deleted columns, remap remaining column models
                if (newAddress) {
                    const newTableCol = newAddress.c - newTableRange.a1.c;
                    this.#columnModelMap.set(newTableCol, columnModel);
                    this.#colIndexMap.set(columnModel, newTableCol);
                } else {
                    this.#destroyColumnModel(tableCol);
                }
            }
        }
    }

    /**
     * Returns an array with all column keys (upper-case column names).
     *
     * @returns
     *  An array with all column keys (upper-case column names).
     */
    getColumnKeys(): string[] {
        return this.#headerCache.getCustom("colKeys",
            cellIt => Array.from(cellIt, ({ value }) => is.string(value) ? mapKey(value) : "")
        );
    }

    /**
     * Returns the specified table column model, if it exists.
     *
     * @param tableCol
     *  The zero-based index of the table column, relative to the cell range
     *  covered by this table.
     *
     * @returns
     *  The specified table column model. If no explicit column model exists
     *  for the specified table column, the internal column model with default
     *  settings will be returned instead.
     */
    getColumnModel(tableCol: number): TableColumnModel {
        return this.#columnModelMap.get(tableCol) ?? this.#defColumnModel;
    }

    /**
     * Returns the address of the data range of a single column in this table,
     * without the header and footer cell.
     *
     * @param tableCol
     *  The zero-based index of the column, relative to the cell range covered
     *  by this table.
     *
     * @returns
     *  The address of the data range in the specified table column; or
     *  `undefined`, if no data rows are available (e.g., if the table consists
     *  of a header row only), or if the passed column index is invalid.
     */
    getColumnDataRange(tableCol: number): Opt<Range> {

        // the data range of the entire table
        const dataRange = this.getDataRange();
        // the resulting absolute column index
        const absCol = this.#tableRange.a1.c + tableCol;

        // check validity of passed column index, and existence of data rows
        if (!dataRange || !this.#tableRange.containsCol(absCol)) {
            return undefined;
        }

        // adjust column indexes in the result range
        return dataRange.setBoth(absCol, true);
    }

    /**
     * Returns the cell contents of the data range of a single column in this
     * table.
     *
     * @param tableCol
     *  The zero-based index of the column, relative to the cell range
     *  covered by this table.
     *
     * @param [rowIntervals]
     *  If specified, an array of row intervals, or a single row interval, in
     *  the data range of this table to query the cell data for. The properties
     *  "first" and "last" of each interval object are relative to the data
     *  range of the table (the index 0 refers to the first data row of this
     *  table!). If omitted, returns the cell contents of all data cells in the
     *  column.
     *
     * @returns
     *  A compressed array of cell contents in the specified table column,
     *  containing the cell values, and the formatted display strings. See
     *  method `CellCollection#getRangeContents` for more details.
     */
    getColumnContents(tableCol: number, rowIntervals?: IntervalArray): CellContents[] {

        // the address of the data range in the specified column
        const dataRange = this.getColumnDataRange(tableCol);

        // nothing to do, if no data range is available (e.g., header/footer rows only)
        if (!dataRange) { return []; }

        // transform the passed relative intervals to absolute row intervals
        const boundInterval = dataRange.rowInterval();
        if (rowIntervals) {
            rowIntervals = rowIntervals.deepClone().move(boundInterval.first).intersect(boundInterval);
        } else {
            rowIntervals = new IntervalArray(boundInterval);
        }

        // nothing to do, if the passed relative intervals are outside the data range
        if (rowIntervals.empty()) { return []; }

        // build the data range addresses to be queried for
        const dataRanges = RangeArray.fromRowIntervals(rowIntervals, dataRange.a1.c);

        // fetch the cell contents from the cell collection
        return this.sheetModel.cellCollection.getRangeContents(dataRanges, { blanks: true, attributes: true, display: true, compressed: true }) as CellContents[];
    }

    /**
     * Returns whether this table contains any columns with active settings
     * (filter rules or sort options) that can be used to refresh the table
     * range.
     *
     * @returns
     *  Whether this table contains any columns with active settings.
     */
    isRefreshable(): boolean {
        return itr.some(this.#columnModelMap.values(), columnModel => columnModel.isRefreshable());
    }

    /**
     * Builds and returns a list of row intervals containing all visible rows
     * not filtered by the current filter settings. For performance reasons (to
     * prevent blocked browser UI and its respective error messages), the
     * implementation of this method may run asynchronously.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil with the visible data row intervals. The
     *  properties "first" and "last" of each row interval are relative to the
     *  data range of the table (the index 0 always refers to the first data
     *  row of this table!).
     */
    queryFilterResult(options?: QueryFilterOptions): JPromise<IntervalArray> {

        // start with the entire data range
        const dataRange = this.getDataRange();

        // no data rows available
        if (!dataRange) { return this.createResolvedPromise(new IntervalArray()); }

        // table column to be excluded from filtering
        const skipColsSet = new Set(options?.skipColumns);
        // the entire row interval of the data range (absolute row indexes)
        const dataRowInterval = dataRange.rowInterval();
        // the remaining visible data intervals for the next column (relative to the data range)
        let visibleIntervals = new IntervalArray(new Interval(0, dataRowInterval.size() - 1));

        // collect the column indexes of all filtered columns
        const filterCols = ary.from(this.#columnModelMap, ([tableCol, columnModel]) => {
            return (columnModel.isFiltered() && !skipColsSet.has(tableCol)) ? tableCol : undefined;
        });

        // process the table columns in a background loop to prevent unresponsive browser
        const promise = this.iterateSliced(filterCols, (tableCol): typeof BREAK | void => {

            // early exit, if no visible data rows are left for filtering
            if (visibleIntervals.empty()) { return BREAK; }

            // the filter matcher for the current column
            // (ignore columns with unknown/unsupported filter settings)
            const matcher = this.getColumnModel(tableCol).getFilterMatcher();
            if (!matcher.active()) { return; }

            // query the cell contents of all remaining visible rows
            const cellArray = this.getColumnContents(tableCol, visibleIntervals);
            // the resulting row intervals which remain visible
            const newIntervals = new IntervalArray();
            // index of the current data row
            let dataRow = 0;
            // index of the current row interval, and offset inside the interval
            let intervalIndex = 0;

            // pushes a new row interval to the "newRowIntervals" array (tries to expand last interval)
            const pushVisibleInterval = (cellCount: number): void => {

                // end position of the new interval (start position is "dataRow")
                const lastDataRow = dataRow + cellCount - 1;
                // last pushed interval for expansion
                const lastInterval = newIntervals.last();

                // try to expand the last interval
                if (lastInterval && (lastInterval.last + 1 === dataRow)) {
                    lastInterval.last = lastDataRow;
                } else {
                    newIntervals.push(new Interval(dataRow, lastDataRow));
                }
            };

            // simultaneously iterate through the row intervals and compressed result array
            for (const cellData of cellArray) {

                // the number of remaining cell to process for the current cell data object
                let remainingCells = cellData.count;
                // whether the current cell data results in visible rows (the filter matches)
                const visible = matcher.test(cellData.value, cellData.format, cellData.display);

                // process as many remaining data rows as covered by the current cell data entry
                while (remainingCells > 0) {

                    // the current row interval
                    const visibleInterval = visibleIntervals[intervalIndex];
                    // update "dataRow" if a new row interval has been started
                    dataRow = Math.max(dataRow, visibleInterval.first);
                    // number of cells that can be processed for the current row interval
                    const cellCount = Math.min(remainingCells, visibleInterval.last - dataRow + 1);

                    // if the cell data matches the filter rule, add the row interval to the result
                    if (visible) { pushVisibleInterval(cellCount); }

                    // decrease number of remaining cells for the current cell data object
                    remainingCells -= cellCount;
                    // update data row index
                    dataRow += cellCount;
                    // if end of current row interval has been reached, go to next row interval
                    if (visibleInterval.last < dataRow) { intervalIndex += 1; }
                }
            }

            // use the resulting filtered row intervals for the next filtered column
            visibleIntervals = newIntervals;
        });

        // return a promise that resolves with the row intervals
        return promise.then(() => visibleIntervals);
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations, and the undo operations, to refresh this table
     * range according to its current filter settings, by showing or hiding the
     * affected rows in the sheet.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil when the operations have been generated, or
     *  that will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - "rows:overflow": The current filter settings of the table would
     *    modify (show or hide) too many rows at once.
     *  - "operation": Internal error while applying the operation.
     */
    generateRefreshOperations(generator: SheetOperationGenerator, options?: RefreshTableOptions): JPromise {

        // start with the entire data range; do nothing, if no data rows are available
        const dataRange = this.getDataRange();
        if (!dataRange) { return this.createResolvedPromise(); }

        // the effective table range, e.g. after expanding autofilter data range
        const effectiveRange = this.#tableRange.boundary(dataRange);

        // the promise chain
        let promise = this.createResolvedPromise();

        // update the effective table range (data range may have been expanded)
        if (this.#tableRange.differs(effectiveRange)) {
            generator.generateTableRangeOperation(Op.CHANGE_TABLE, this.#tableName, this.#tableRange, undefined, { undo: true });
            generator.generateTableRangeOperation(Op.CHANGE_TABLE, this.#tableName, effectiveRange);

            // bug 56336: create or update the internal defined name
            if (this.isAutoFilter()) {
                promise = promise.then(() => this.sheetModel.nameCollection.generateFilterDatabaseOperations(generator, effectiveRange));
            }
        }

        // local operation generators for correct order of undo operations for filtering/sorting
        const filterGenerator = this.sheetModel.createOperationGenerator({ applyImmediately: true });
        const sortGenerator = this.sheetModel.createOperationGenerator({ applyImmediately: true });

        // update state of row visibility from filter rules
        if (options?.updateFilter) {
            promise = promise
                .then(() => this.queryFilterResult(options))
                .then(rowIntervals => {

                    // the entire row interval of the data range
                    const dataRowInterval = dataRange.rowInterval();

                    // the new visible row intervals (queryFilterResult() returns relative row indexes)
                    const visibleIntervals = createChangeSetIntervals(rowIntervals.deepClone().move(dataRowInterval.first), { visible: true, filtered: false });
                    // the new hidden row intervals
                    const hiddenIntervals = createChangeSetIntervals(new IntervalArray(dataRowInterval).difference(visibleIntervals), { visible: false, filtered: true });

                    // create the operations to update all affected rows
                    const intervals = visibleIntervals.concat(hiddenIntervals);
                    return this.sheetModel.rowCollection.generateIntervalOperations(filterGenerator, intervals);
                });
        }

        // update row order from sorting rules
        if (options?.updateSort) {
            const sortRules = this.getSortRules();
            if (sortRules.length > 0) {
                promise = promise.then(() => this.sheetModel.cellCollection.generateSortOperations(sortGenerator, dataRange, true, sortRules));
            }
        }

        // Bug 61696: Sort operations must be undone before filter operations (otherwise all cell comments
        // moved by filtering will be accessed with wrong anchor addresses before restoring the sort order).
        //
        // TODO: Collect changes for drawing objects and apply them after filtering AND sorting, to prevent
        // changing the same drawing objects twice.
        //
        promise.done(() => {
            generator.appendOperations(filterGenerator);
            generator.appendOperations(sortGenerator);
            generator.appendOperations(sortGenerator, { undo: true });
            generator.appendOperations(filterGenerator, { undo: true });
        });

        return promise;
    }

    /**
     * Generates the operations, and the undo operations, to modify the
     * attributes of this table range.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param attrSet
     *  An (incomplete) attribute set with new attributes to be applied at the
     *  table range.
     *
     * @returns
     *  A promise that will fulfil when the operations have been generated.
     */
    generateChangeOperations(generator: SheetOperationGenerator, attrSet: PtTableAttributeSet): JPromise {

        // generate a "changeTable" operation (this modifies the table attributes immediately)
        generator.generateTableOperation(Op.CHANGE_TABLE, this.#tableName, { attrs: this.getUndoAttributeSet(attrSet) }, { undo: true });
        generator.generateTableOperation(Op.CHANGE_TABLE, this.#tableName, { attrs: attrSet });

        // refresh the table with current filter settings applied above
        return this.generateRefreshOperations(generator);
    }

    /**
     * Generates the operations, and the undo operations, to change the table
     * attributes of the specified table column, and refreshes all affected
     * settings in the sheet, such as filtered rows.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param tableCol
     *  The zero-based index of the table column to be modified, relative
     *  to the cell range covered by the table.
     *
     * @param attrSet
     *  The attributes to be set for the specified table column.
     *
     * @param [options]
     *  Optional parameters:
     *  - {boolean} [options.resetSort=false]
     *    If set to `true`, the sort settings of all other columns will be
     *    reset.
     *
     * @returns
     *  A promise that will fulfil when the operations have been generated, or
     *  that will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - "rows:overflow": The passed filter settings would modify (show or
     *    hide) too many rows at once.
     *  - "operation": Internal error while applying the operation.
     */
    generateChangeColumnOperations(generator: SheetOperationGenerator, tableCol: number, attrSet: PtTableColumnAttributeSet, options?: GenerateChangeColumnOperationsOptions): JPromise {

        // reset all existing column sort settings (except the specified column)
        if (options?.resetSort) {
            this.#generateResetSortSettings(generator, new Set([tableCol]));
        }

        // generate the undo attributes also for a missing table column
        // (back to defaults, this will delete the column - TODO: new "deleteTableColumn" operation!)
        const undoAttrSet = this.getColumnModel(tableCol).getUndoAttributeSet(attrSet);

        // generate a "changeTableColumn" operation (this modifies the column attributes immediately)
        generator.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, { col: tableCol, sort: "sort" in undoAttrSet, attrs: undoAttrSet }, { undo: true });
        generator.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, { col: tableCol, sort: "sort" in attrSet, attrs: attrSet });

        // always refresh the filter and sorting (also if no attributes have been changed,
        // the cell contents may have changed in the meantime, resulting in new filtered rows)
        return this.generateRefreshOperations(generator, { updateFilter: "filter" in attrSet, updateSort: "sort" in attrSet });
    }

    /**
     * Generates the operations, and the undo operations, to delete this table
     * range from the document. Additionally, if the table contains active
     * filter rules, all operations will be generated that make the affected
     * hidden rows visible again.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param [options]
     *  Optional parameters.
     */
    generateDeleteOperations(sheetCache: SheetCache, options?: GeneratorOptions): void {

        // make all rows visible that are filtered by this table range
        const changeSet = { attrs: { row: { visible: true, filtered: false } } };
        sheetCache.rowCache.changeInterval(this.#tableRange.rowInterval(), changeSet);

        // restore the entire table with its position and attributes
        const undoProps = { attrs: this.getExplicitAttributeSet(true) };
        sheetCache.generateTableRangeOperation(Op.INSERT_TABLE, this.#tableName, this.#tableRange, undoProps, { undo: true });

        // restore all existing columns
        for (const [tableCol, columnModel] of this.#columnModelMap) {
            const colProps = { col: tableCol, attrs: columnModel.getExplicitAttributeSet(true) };
            sheetCache.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, colProps, { undo: true });
        }

        // generate the operation to delete the table
        sheetCache.generateTableOperation(Op.DELETE_TABLE, this.#tableName, undefined, options);

        // TODO: delete and restore the "_FilterDatabase" defined name when deleting autofilters?
    }

    /**
     * Generates the operations and undo operations to update this table range.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param moveTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateUpdateTaskOperations(sheetCache: SheetCache, moveTask: MoveCellsUpdateTask): JPromise {

        // the address transformer used to transform the table range
        const transformer = moveTask.transformer;
        // whether the transformer deletes rows
        const deleteRows = !transformer.insert && !transformer.columns;
        // bug 36226: delete tables whose header row has been deleted
        const deleteTable = deleteRows && this.hasHeaderRow() && transformer.deleteIntervals.containsIndex(this.#tableRange.a1.r);

        // transform the table range, generate operation to delete the entire table on demand
        const newTableRange = deleteTable ? null : transformer.transformRanges(this.#tableRange).first();
        if (!newTableRange) {
            this.generateDeleteOperations(sheetCache, { prepend: true });
            return jpromise.resolve();
        }

        // nothing to do without transformation of table range
        if (this.#tableRange.equals(newTableRange)) { return jpromise.resolve(); }

        // check whether the table range can be moved legally
        OperationError.assertCause(this.#canMoveCells(transformer), "table:move");

        // the properties for the change operation
        // DOCS-1825: transform table range manually (no implicit transformation anymore, needed for OT)
        const operProps: Dict = { range: newTableRange.toOpStr() };
        const undoProps: Dict = { range: this.#tableRange.toOpStr() };

        // update table attribute "footerRow" when deleting rows
        if (!transformer.insert && !transformer.columns && this.hasFooterRow() && transformer.deleteIntervals.containsIndex(this.#tableRange.a2.r)) {
            operProps.attrs = { table: { footerRow: false } };
            undoProps.attrs = { table: { footerRow: true } };
        }

        // generate the change operations
        sheetCache.generateTableOperation(Op.CHANGE_TABLE, this.#tableName, operProps);
        sheetCache.generateTableOperation(Op.CHANGE_TABLE, this.#tableName, undoProps, { undo: true });

        // the promise chain
        let promise = jpromise.resolve();

        // restore the column models of all deleted table columns
        if (transformer.columns && this.#columnModelMap.size) {

            // generate the undo operations to restore all deleted table columns
            const startCol = this.#tableRange.a1.c;
            const startRow = this.#tableRange.a1.r;
            const deletedColumns = ary.from(this.#columnModelMap, ([tableCol, columnModel]) => {
                // transform the absolute column index according to the operation
                const address = new Address(startCol + tableCol, startRow);
                if (transformer.transformAddress(address)) { return undefined; }
                // generate restore operation for deleted table
                const colProps = { col: tableCol, attrs: columnModel.getExplicitAttributeSet(true) };
                sheetCache.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, colProps, { undo: true });
                return tableCol;
            });

            // refresh the visible/hidden rows (deleted filter settings will cause to show hidden rows)
            promise = this.generateRefreshOperations(sheetCache, { skipColumns: deletedColumns });
        }

        return promise;
    }

    /**
     * Generates operations to sort one or some column(s)
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param sortRules
     *  The sorting rules to be applied.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateSortOperations(generator: SheetOperationGenerator, sortRules: SortRule[]): JPromise {

        // collect (relative) table column indexes that will be sorted
        const startCol = this.#tableRange.a1.c;
        const sortCols = set.from(sortRules, sortRule => sortRule.index - startCol);

        // reset all existing column sort settings (except the columns that will be resorted anyway)
        this.#generateResetSortSettings(generator, sortCols);

        // update the sort attributes for every column specified in the passed rules
        for (const sortRule of sortRules) {

            // the relative index of the sorted column
            const tableCol = sortRule.index - startCol;
            // prepare sort attributes
            const sortAttrSet: PtTableColumnAttributeSet = { sort: { type: "value", descending: !!sortRule.descending } };
            // prepare undo attributes (with, or without existing columnModel)
            const undoAttrSet = this.getColumnModel(tableCol).getUndoAttributeSet(sortAttrSet);

            // generate operations
            generator.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, { sort: true, col: tableCol, attrs: undoAttrSet }, { undo: true });
            generator.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, { sort: true, col: tableCol, attrs: sortAttrSet });
        }

        // generate all operations to refresh (sort) the table range
        return this.generateRefreshOperations(generator, { updateSort: true });
    }

    // operation handlers -----------------------------------------------------

    /**
     * Callback handler for the document operation "changeTableColumn". Changes
     * the attributes of a column in this table model.
     *
     * @param context
     *  A wrapper representing the "changeTableColumn" document operation.
     */
    applyChangeOperation(context: SheetOperationContext): void {

        // check that the column index exists in the table
        const tableCol = context.getInt("col");
        context.ensure((tableCol >= 0) && (tableCol < this.#tableRange.cols()), "invalid table column");

        // change the column attributes
        const hasModel = this.#columnModelMap.has(tableCol);
        const attrSet = context.getDict("attrs");
        const columnModel = this.#upsertColumnModel(tableCol);
        columnModel.setAttributes(attrSet);

        // model can be deleted, if it does not contain any active settings
        if (!columnModel.hasExplicitAttributes()) {
            this.#destroyColumnModel(tableCol);
            return;
        }

        // DOCS-4446: maintain sort order (also if column model does not trigger change events,
        // e.g. when changing sort order of columns, but not the sorting attributes)
        if (!hasModel || context.optBool("sort")) {
            // move existing model to the end of the set by deleting from and re-adding to the set
            this.#sortOrderSet.delete(columnModel);
            if (columnModel.isSorted()) {
                this.#sortOrderSet.add(columnModel);
            }
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the merged table attributes.
     */
    #getTableAttrs(explicit: true): Opt<Partial<TableAttributes>>;
    #getTableAttrs(): TableAttributes;
    // implementation
    #getTableAttrs(explicit?: boolean): Opt<Partial<TableAttributes>> {
        return (explicit ? this.getExplicitAttributeSet(true) : this.getMergedAttributeSet(true)).table;
    }

    /**
     * Returns whether the move operations represented by the passed move
     * descriptor can be applied with this table. The table cannot be moved
     * partly, the header row of the table cannot be deleted (unless it is the
     * autofilter), and at least one data row must remain in the table.
     *
     * @param transformer
     *  An address transformer representing the move operation.
     *
     * @returns
     *  Whether the move operations can be applied.
     */
    #canMoveCells(transformer: AddressTransformer): boolean {

        // the part of the table range that will be moved somehow
        const moveRange = this.#tableRange.intersect(transformer.dirtyRange);
        if (!moveRange) { return true; }

        // it is not possible to move parts of the table (e.g. only the lower part to the right etc.)
        const tableInterval = this.#tableRange.interval(!transformer.columns);
        const moveInterval = moveRange.interval(!transformer.columns);
        if (tableInterval.differs(moveInterval)) { return false; }

        // entire columns/rows can be inserted, entire columns can be deleted,
        // all rows in an autofilter can be deleted
        if (transformer.insert || transformer.columns || this.isAutoFilter()) { return true; }

        // returns `true`, if the passed range is `null` or if it exists and will not be deleted by the transformer
        const rangeRemainsValid = (range: Opt<Range>): boolean => !range || !transformer.transformRanges(range).empty();

        // the header/footer row cannot be deleted while keeping other parts of
        // the table; at least one data row must remain in the table
        return rangeRemainsValid(this.getHeaderRange()) && rangeRemainsValid(this.getFooterRange()) && rangeRemainsValid(this.getDataRange());
    }

    /**
     * Inserts the passed table column model into this table, and registers a
     * listener for attribute change events that will be forwarded to the
     * listeners of this table.
     */
    #insertColumnModel(tableCol: number, columnModel: TableColumnModel): void {

        // store the column model as map entry (needed for fast direct access by column index)
        this.#columnModelMap.set(tableCol, columnModel);
        this.#colIndexMap.set(columnModel, tableCol);

        // listen to attribute change events, forward to own listeners
        this.listenTo(columnModel, "change:attributes", (newAttrSet, oldAttrSet) => {

            // resolve current column index (do NOT use the original column index passed above)
            const currTableCol = this.#colIndexMap.get(columnModel)!;

            // forward to listeners of this table model
            this.trigger("change:column:attrs", currTableCol, newAttrSet, oldAttrSet);
        });
    }

    /**
     * Creates a missing table column model, and returns the model instance.
     * The passed column index will not be checked for validity.
     */
    #upsertColumnModel(tableCol: number): TableColumnModel {

        // find an existing column model
        let columnModel = this.#columnModelMap.get(tableCol);

        // create missing column model
        if (!columnModel) {
            columnModel = new TableColumnModel(this.sheetModel);
            this.#insertColumnModel(tableCol, columnModel);
        }

        return columnModel;
    }

    /**
     * Removes a column model from all internal containers, and destroys it.
     */
    #destroyColumnModel(tableCol: number): void {
        map.visit(this.#columnModelMap, tableCol, columnModel => {
            this.#columnModelMap.delete(tableCol);
            this.#colIndexMap.delete(columnModel);
            this.#sortOrderSet.delete(columnModel);
            columnModel.destroy();
        });
    }

    /**
     * Generates operations to reset the sort attributes of all columns, but
     * does not generate the table refresh operations.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param [skipCols]
     *  The zero-based indexes of all table columns that will keep their sort
     *  settings.
     */
    #generateResetSortSettings(generator: SheetOperationGenerator, skipCols?: Set<number>): void {

        // reset all existing column sort settings
        for (const [tableCol, columnModel] of this.#columnModelMap) {

            // skip unsorted columns, and the specified columns
            if (!columnModel.isSorted() || skipCols?.has(tableCol)) { return; }

            const sortAttrSet: PtTableColumnAttributeSet = { sort: { type: "none", descending: false } };
            const undoAttrSet = columnModel.getUndoAttributeSet(sortAttrSet);

            generator.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, { col: tableCol, attrs: undoAttrSet }, { undo: true });
            generator.generateTableOperation(Op.CHANGE_TABLE_COLUMN, this.#tableName, { col: tableCol, attrs: sortAttrSet });
        }
    }
}
