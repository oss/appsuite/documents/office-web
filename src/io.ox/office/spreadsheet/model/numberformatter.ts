/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, map, dict } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import type { MixedAttrBag } from "@/io.ox/office/editframework/utils/attributeutils";
import { NumberParser } from "@/io.ox/office/editframework/model/formatter/parse/numberparser";
import type { DisplayText } from "@/io.ox/office/editframework/model/formatter/autoformat";
import type { FormatCodeSpec, PreparedFormatValue, ParseFormatOptions } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { PresetCodeId, PRESET_ID_USER_START, CURRENCY_FORMAT_IDS, ParsedFormat } from "@/io.ox/office/editframework/model/formatter/baseformatter";

import { NumberFormatter as TextNumberFormatter } from "@/io.ox/office/textframework/model/numberformatter";

import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { MAX_LENGTH_STANDARD_CELL, modelLogger, isErrorCode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { DELETE_NUMBER_FORMAT, INSERT_NUMBER_FORMAT } from "@/io.ox/office/spreadsheet/utils/operations";
import type { CellAttributes } from "@/io.ox/office/spreadsheet/utils/cellattrs";

import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";

import { Complex } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import type { OperandValType } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// re-exports =================================================================

export type { FormatCodeSpec, ParsedFormat };

// types ======================================================================

/**
 * Optional parameters for parsing formatted strings to scalar values.
 */
export interface ParseScalarOptions {

    /**
     * If set to `true`, the empty string will be treated as blank cell, and
     * the property "value" of the result descriptor will be set to `null`.
     * Default value is `false`.
     */
    blank?: boolean;

    /**
     * If set to `true`, a leading apostrophe in the passed text (used to
     * protect a string from conversion to other data types) will be kept in
     * the property "value" of the result descriptor. by default, the leading
     * apostrophe will be removed from the string.
     */
    keepApos?: boolean;
}

/**
 * A descriptor with the scalar value, and a preset format identifier,
 * representing a parsed text.
 */
export interface ParseScalarResult {

    /**
     * The resulting value with correct data type.
     */
    value: ScalarType;

    /**
     * The identifier of the best matching preset format code, or an explicit
     * format code as string, if the passed text has been converted to a
     * floating-point number in a specific significant format; or 0 (standard
     * number format) for simple floating-point numbers, or any other value
     * type. May not fit exactly to the text, e.g. if the text represents a
     * number in scientific notation, the identifier of the standard format
     * code for scientific numbers "0.00E+00" will always be returned.
     */
    format: string | number;
}

// private types --------------------------------------------------------------

/**
 * Representation of a user-defined number format.
 */
interface NumberFormatDescriptor {

    /**
     * The number format code.
     */
    code: string;

    /**
     * Whether the number format is persistent, i.e. whether it has been
     * created by an "insertNumberFormat" document operation (see property
     * "transient" for more details).
     */
    persistent: boolean;

    /**
     * The number of temporary usages of the number format without defining it
     * with an "insertNumberFormat" document operation, e.g. in the attribute
     * set of a conditional formatting rule which contains both attributes
     * "formatId" and "formatCode".
     */
    transient: number;
}

type UpdateTransientFormatFn = (formatId: number, formatCode: string, formatDesc: Opt<NumberFormatDescriptor>) => void;

// constants ==================================================================

// identifiers of predefined formats that can be overridden by user-defined formats
const REPLACEABLE_FORMAT_ID_SET = new Set(CURRENCY_FORMAT_IDS);

// class NumberFormatter ======================================================

/**
 * A number formatter for a spreadsheet document. Supports the format code
 * syntax used in document operations, and allows to deal with all data types
 * of a spreadsheet document: numbers, strings, Boolean values, and error
 * codes.
 *
 * @param {SpreadsheetModel} docModel
 *  The document model containing this instance.
 */
export class NumberFormatter extends TextNumberFormatter<SpreadsheetModel> {

    /**
     * The user-defined format codes, and the overridden preset codes, mapped
     * by identifiers.
     */
    readonly #formatTable = new Map<number, NumberFormatDescriptor>();

    /**
     * The number format identifiers, mapped by format codes.
     */
    readonly #reverseTable = new Map<string, number>();

    /**
     * The first unused number format identifier (as used in OOXML).
     */
    #firstFreeId = PRESET_ID_USER_START;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {

        // base constructor
        super(docModel, { stdLen: MAX_LENGTH_STANDARD_CELL });

        // OOXML: register operations for number format codes
        if (docModel.docApp.isOOXML()) {
            docModel.registerOperationHandler(INSERT_NUMBER_FORMAT, this.#applyInsertOperation.bind(this));
            docModel.registerOperationHandler(DELETE_NUMBER_FORMAT, this.#applyDeleteOperation.bind(this));
        }

        // change the null date via document operation
        this.listenTo(docModel, "change:config", changed => {
            const matches = changed.nullDate ? /^(\d{4})-(\d{2})-(\d{2})$/.exec(changed.nullDate) : null;
            if (matches) {
                const nullDate = Date.UTC(parseInt(matches[1], 10), parseInt(matches[2], 10) - 1, parseInt(matches[3], 10));
                this.configure({ nullDate });
            }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the parsed number format descriptor for the specified number
     * format. Adds support for predefined format identifiers to the base class
     * method.
     *
     * @param formatSpec
     *  The format code to be parsed, or the identifier of a predefined format
     *  code.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  The parsed number format descriptor of a number format code.
     */
    override getParsedFormat(formatSpec: FormatCodeSpec, options?: ParseFormatOptions): ParsedFormat {
        // try to resolve user-defined number format codes
        const formatDesc = is.number(formatSpec) ? this.#formatTable.get(formatSpec) : undefined;
        return super.getParsedFormat(formatDesc ? formatDesc.code : formatSpec, options);
    }

    /**
     * Returns the parsed number format descriptor for the specified cell
     * formatting attributes.
     *
     * @param cellAttrs
     *  The cell formatting attributes, expected to contain the attributes
     *  "formatId" and "formatCode".
     *
     * @returns
     *  The parsed number format descriptor for the passed cell attributes.
     */
    getParsedFormatForAttributes(cellAttrs: Partial<CellAttributes>): ParsedFormat {

        switch (this.fileFormat) {

            // OOXML: use the format identifier only
            case FileFormatType.OOX:
                return this.getParsedFormat(cellAttrs.formatId);

            // ODF: use the format code only
            case FileFormatType.ODF:
                return this.getParsedFormat(cellAttrs.formatCode);

            default:
                modelLogger.error(`$badge{NumberFormatter} parseAttributesFormat: unknown file format "${this.fileFormat}"`);
                return this.standardFormat;
        }
    }

    /**
     * Converts the specified number format to the identifier of an existing
     * predefined or user-defined number format.
     *
     * @param formatSpec
     *  The identifier of a number format as integer, or a format code as
     *  string, or a parsed format code.
     *
     * @returns
     *  If a format identifier as integer has been passed, it will be returned
     *  immediately. A format code (as string) will be resolved to a
     *  user-defined or preset number format, and its identifier will be
     *  returned, if available. Otherwise, `null` will be returned.
     */
    override resolveFormatId(formatSpec: FormatCodeSpec): number | null {

        // immediately return passed format identifiers
        if (is.number(formatSpec)) { return formatSpec; }

        // try to resolve identifier of a user-defined number format
        const formatCode = (formatSpec instanceof ParsedFormat) ? formatSpec.formatCode : formatSpec;
        const formatId = is.string(formatCode) ? this.#reverseTable.get(formatCode) : undefined;
        if (is.number(formatId)) { return formatId; }

        // try to resolve a predefined number format, but DO NOT return the identifier
        // if the predefined format has been overridden by a user-defined number format
        const presetId = super.resolveFormatId(formatCode);
        return (is.number(presetId) && !this.#formatTable.has(presetId)) ? presetId : null;
    }

    /**
     * Converts the passed complex number to a string. The real and imaginary
     * coefficients will be formatted with the standard number format using the
     * passed maximum number of digits.
     *
     * @param complex
     *  The complex number to be converted to a string.
     *
     * @param stdLen
     *  The maximum number of characters allowed for the absolute part of a
     *  coefficient, including the decimal separator and the complete exponent
     *  in scientific notation, but without the sign character. MUST be
     *  positive.
     *
     * @returns
     *  The converted complex number.
     */
    convertComplexToString(complex: Complex, stdLen: number): DisplayText {

        // the real coefficient, as string
        const real = this.autoFormatNumber(complex.real, { stdLen }).text;
        if (!real) { return null; }

        // leave out imaginary part if zero
        if (complex.imag === 0) { return real; }

        // the imaginary coefficient, as string
        const imag = this.autoFormatNumber(complex.imag, { stdLen }).text;
        if (!imag) { return null; }

        // fall-back to unit "i"
        const unit = complex.unit || "i";

        // leave out real part if missing; do not add single "1" for imaginary part
        return ((complex.real === 0) ? "" : real) + (((complex.real !== 0) && (complex.imag > 0)) ? "+" : "") + (/^-?1$/.test(imag) ? "" : imag) + unit;
    }

    /**
     * Converts the passed text to a complex number.
     *
     * @param text
     *  The string to be converted to a complex number.
     *
     * @returns
     *  The complex number represented by the passed string; or `null`, if the
     *  string cannot be parsed to a complex number.
     */
    convertStringToComplex(text: string): Complex | null {

        // do not accept empty strings
        if (!text) { return null; }

        // string may be a single imaginary unit without coefficients: i, +i, -i (same for j)
        let matches = /^([-+]?)([ij])$/.exec(text);
        if (matches) {
            return new Complex(0, (matches[1] === "-") ? -1 : 1, matches[2]);
        }

        // pull leading floating-point number from the string
        let parseResult = NumberParser.parseNumber(text);
        if (!parseResult) { return null; }

        // check for simple floating-point number without imaginary coefficient: a, +a, -a
        const real = parseResult.number;
        const rest = parseResult.remaining;
        if (!rest) {
            return new Complex(real, 0);
        }

        // check for imaginary number without real coefficient: bi, +bi, -bi
        if ((rest === "i") || (rest === "j")) {
            return new Complex(0, real, rest);
        }

        // check for following imaginary unit without coefficients, but with sign: a+i, a-i
        if ((matches = /^([-+])([ij])$/.exec(rest))) {
            return new Complex(real, (matches[1] === "-") ? -1 : 1, matches[2]);
        }

        // pull trailing floating-point number from the string: a+bi, a-bi (sign is required here, something like "abi" is not valid)
        parseResult = NumberParser.parseNumber(rest);
        if (!parseResult?.sign) { return null; }

        // remaining text must be the imaginary unit
        const rest2 = parseResult.remaining;
        if ((rest2 === "i") || (rest2 === "j")) {
            return new Complex(real, parseResult.number, rest2);
        }

        return null;
    }

    /**
     * Tries to convert the passed scalar value to a floating-point number.
     *
     * @param value
     *  A scalar value to be converted to a number. Dates will be converted to
     *  their serial numbers. Strings that represent a valid number (according
     *  to the current GUI language) will be converted to the number. The
     *  boolean value `false` will be converted to 0, the boolean value `true`
     *  will be converted to 1. The special value `null` (representing a blank
     *  cell) will be converted to 0. Other values (error codes, other strings,
     *  infinite numbers) cannot be converted to a number and result in
     *  returning the value `null`.
     *
     * @param toInt
     *  If set to `true`, the number will be rounded down to the next integer
     *  (negative numbers will be rounded down too, i.e. away from zero!).
     *
     * @returns
     *  The floating-point number, if available; otherwise `null`.
     */
    convertScalarToNumber(value: OperandValType, toInt?: boolean): number | null {

        // convert strings and booleans to numbers
        if (is.string(value)) {
            value = this.parseScalarValue(value).value;
        } else if (is.boolean(value)) {
            value = value ? 1 : 0;
        } else if (value instanceof Date) {
            value = this.convertDateToNumber(value); // returns null for invalid dates
        } else if (value === null) {
            value = 0;
        }

        // the resulting value must be a finite floating-point number
        return Number.isFinite(value) ? (toInt ? Math.floor(value as number) : value as number) : null;
    }

    /**
     * Tries to convert the passed scalar value to a UTC date.
     *
     * @param value
     *  A scalar value to be converted to a date. Strings that represent a
     *  valid number (according to the current GUI language) will be converted
     *  to a date representing that serial number. The boolean value `false`
     *  will be converted to the null date of this formatter, the boolean value
     *  `true` will be converted to the day following the null date. The
     *  special value `null` (representing a blank cell) will be converted to
     *  the null date too. Other values (error codes, other strings, infinite
     *  numbers) cannot be converted to a date and result in returning the
     *  value `null`.
     *
     * @param toInt
     *  If set to `true`, the time components of the resulting date will be
     *  removed (the time will be set to midnight).
     *
     * @returns
     *  The UTC date, if available; otherwise `null`.
     */
    convertScalarToDate(value: OperandValType, toInt?: boolean): Date | null {

        // convert scalar values to a date via conversion to a number
        if (!(value instanceof Date)) {
            // convert the value to a number (may return null)
            value = this.convertScalarToNumber(value, toInt);
            if (value === null) { return null; }
            // convert the number to a date object (may return null)
            value = this.convertNumberToDate(value);
        }

        // the resulting value must be a valid Date object
        return ((value instanceof Date) && this.isValidDate(value)) ? value : null;
    }

    /**
     * Tries to convert the passed scalar value to a string.
     *
     * @param value
     *  A scalar value to be converted to a string. Numbers and dates will be
     *  converted to decimal or scientific notation (according to the current
     *  GUI language), boolean values will be converted to their localized text
     *  representation. The special value `null` (representing a blank cell)
     *  will be converted to the empty string. Other values (error codes,
     *  infinite numbers) cannot be converted to a string and result in
     *  returning the value `null`.
     *
     * @param stdLen
     *  The maximum number of characters allowed for the absolute part of a
     *  floating-point number converted to a string, including the decimal
     *  separator and the complete exponent in scientific notation, but without
     *  a leading minus sign. MUST be positive.
     *
     * @returns
     *  The string representation of the value, if available; otherwise `null`.
     */
    convertScalarToString(value: OperandValType, stdLen: number): string | null {

        // treat date objects as plain unformatted numbers
        if (value instanceof Date) {
            value = this.convertScalarToNumber(value);
            if (value === null) { return null; } // do not convert invalid date to empty string
        }

        // convert numbers and booleans to strings
        if (Number.isFinite(value)) {
            value = this.autoFormatNumber(value as number, { stdLen }).text;
        } else if (is.boolean(value)) {
            value = this.docModel.formulaGrammarUI.getBooleanName(value); // always localized
        } else if (value === null) {
            value = "";
        }

        // the resulting value must be a string
        return is.string(value) ? value : null;
    }

    /**
     * Tries to convert the passed scalar value to a boolean value.
     *
     * @param value
     *  A scalar value to be converted to a boolean. Floating-point numbers
     *  will be converted to `true` if not zero, otherwise `false`. The null
     *  date will be converted to `false`, all other dates will be converted to
     *  `true` (if they are valid according to the configuration of this
     *  formatter). Strings containing the exact translated name of the `true`
     *  or `false` values (case-insensitive) will be converted to the
     *  respective boolean value. The special value `null` (representing a
     *  blank cell) will be converted to `false`. Other values (error codes,
     *  other strings, infinite numbers) cannot be converted to a boolean and
     *  result in returning the value `null`.
     *
     * @returns
     *  The boolean value, if available; otherwise `null`.
     */
    convertScalarToBoolean(value: OperandValType): boolean | null {

        // treat date objects as plain unformatted numbers
        if (value instanceof Date) {
            value = this.convertScalarToNumber(value);
            if (value === null) { return null; } // do not convert invalid date to false
        }

        // convert numbers and strings to booleans
        if (Number.isFinite(value)) {
            value = !math.isZero(value as number);
        } else if (is.string(value)) {
            // always localized, returns null on failure
            value = this.docModel.formulaGrammarUI.getBooleanValue(value);
        } else if (value === null) {
            value = false;
        }

        // the resulting value must be a boolean
        return is.boolean(value) ? value : null;
    }

    /**
     * Tries to convert the passed text to a scalar cell value. Supports
     * parsing to floating-point numbers (as implemented by the baseclass
     * method `BaseFormatter::parseFormattedValue`), boolean values, and error
     * code literals. Leading apostrophes (parsing suppression markers) will be
     * removed from the string, e.g. the original string "'0" results in the
     * string "0", not in the number 0.
     *
     * @param text
     *  The string to be converted to a scalar cell value.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  A result descriptor with the resulting scalar cell value, and a preset
     *  format identifier representing the original formatted text.
     */
    parseScalarValue(text: string, options?: ParseScalarOptions): ParseScalarResult {

        // try to parse boolean literals (with original untrimmed string)
        const boolValue = this.docModel.formulaGrammarUI.getBooleanValue(text);
        if (is.boolean(boolValue)) { return { value: boolValue, format: PresetCodeId.STANDARD }; }

        // try to parse error code literals (with original untrimmed string)
        const errorCode = this.docModel.formulaGrammarUI.getErrorCode(text);
        if (errorCode) { return { value: errorCode, format: PresetCodeId.STANDARD }; }

        // special handling for the (untrimmed) empty string
        if (!text && options?.blank) { return { value: null, format: PresetCodeId.STANDARD }; }

        // try to parse the text to a floating-point number
        const result = this.parseFormattedValue(text);

        // remove leading apostroph (unless option "keepApos" has been passed)
        if (!options?.keepApos && is.string(result.value) && result.value.startsWith("'")) {
            result.value = result.value.slice(1);
        }

        return result;
    }

    // operation generators ---------------------------------------------------

    /**
     * Returns the cell attributes for a number format to be inserted into an
     * auto-style or a style sheet. If required, an operation may be generated
     * that defines a new user-defined number format, according to the current
     * file format.
     *
     * @param styleCache
     *  The operation cache to be filled with the operations.
     *
     * @param formatSpec
     *  The identifier of a number format as integer, or a format code as
     *  string.
     *
     * @returns
     *  The resulting cell attributes.
     */
    generateNumberFormatOperations(styleCache: SheetStyleCache, formatSpec: FormatCodeSpec): MixedAttrBag<CellAttributes> {

        // try to resolve format code attributes from cache
        const formatCode = this.getParsedFormat(formatSpec).formatCode;
        return map.upsert(styleCache.numFmtAttrCache, formatCode, () => {

            // the effective identifier of an existing number format
            let formatId = this.resolveFormatId(formatCode);
            // the resulting formatting attributes
            const cellAttrs: MixedAttrBag<CellAttributes> = dict.create();

            // different behavior depending on file format
            switch (this.fileFormat) {

                // OOXML: check existence of the format code, create a user-defined format code on demand
                case FileFormatType.OOX: {

                    // no number format found (neither preset nor user-defined): create a new number format entry
                    if (formatId === null) {
                        formatId = styleCache.nextFormatId ?? this.#firstFreeId;
                        styleCache.nextFormatId = formatId + 1;
                    }

                    // do not create an explicit number format for preset formats, but create an operation for existing
                    // user-defined but transient format codes, created e.g. from a conditional formatting rule
                    if (formatId >= PRESET_ID_USER_START) {
                        const formatDesc = this.#formatTable.get(formatId);
                        if (!formatDesc?.persistent) {
                            // DOCS-1966: In concurrent editing, the number format may be reused by other users, which
                            // would cause errors if this user deletes the number format with an undo action afterwards.
                            // Therefore, generated number formats will never be deleted from now on. This implies also,
                            // that a generated number format must not be re-created from a REDO action of the undo
                            // manager. To achieve this, a special marker flag will be added to the operation which
                            // will cause to filter the operation when creating the undo/redo action.
                            styleCache.generateNumberFormatOperation(INSERT_NUMBER_FORMAT, formatId, { code: formatCode, _SKIP_REDO_: true });
                            // styleCache.generateNumberFormatOperation(DELETE_NUMBER_FORMAT, formatId, null, { undo: true, prepend: true });
                        }
                    }
                    cellAttrs.formatId = formatId;
                    break;
                }

                // ODF: do not create any additional operations, always use the number format property,
                // but add the preset format identifiers for further usage in the export filters
                // bug 49610: clear the "formatId" attribute (set to `null`) for user-defined format codes
                case FileFormatType.ODF: {
                    const isPredef = (formatId !== null) && (formatId < PRESET_ID_USER_START);
                    cellAttrs.formatId = isPredef ? formatId : null;
                    cellAttrs.formatCode = formatCode;
                    break;
                }
            }

            // return the resulting cell attributes for the format code
            return cellAttrs;
        });
    }

    /**
     * Registers a transient number format, i.e. a number format that is used
     * in attribute sets without having created it before with an
     * "insertNumberFormat" document operation.
     *
     * @param cellAttrs
     *  An incomplete attribute map with the cell attributes "formatId" and
     *  "formatCode".
     */
    registerTransientFormat(cellAttrs: Partial<CellAttributes>): void {
        this.#updateTransientFormat(cellAttrs, (formatId, formatCode, formatDesc) => {

            // create a new entry in the format table on demand (existing format code should not change, but do not fail)
            if (!formatDesc) {
                formatDesc = this.#createNumberFormat(formatId, formatCode);
            } else if (formatDesc.code !== formatCode) {
                modelLogger.warn(`$badge{NumberFormatter} registerTransientFormat: format code mismatch: id: ${formatId}, old code: ${formatDesc.code}, new code: ${formatCode}`);
            }

            // update the transient counter
            formatDesc.transient += 1;
        });
    }

    /**
     * Unregisters a transient number format that has been registered with
     * the method `registerTransientFormat` before.
     *
     * @param cellAttrs
     *  An incomplete attribute map with the cell attributes "formatId" and
     *  "formatCode".
     */
    unregisterTransientFormat(cellAttrs: Partial<CellAttributes>): void {
        this.#updateTransientFormat(cellAttrs, (formatId, _formatCode, formatDesc) => {

            // get the descriptor for the number format from the map (number format should exist, but do not fail)
            if (!formatDesc) {
                modelLogger.warn(`$badge{NumberFormatter} unregisterTransientFormat: missing format code: id: ${formatId}`);
                return;
            }

            // update the transient counter
            formatDesc.transient -= 1;

            // remove the number format from the map only, if it is not persistent
            if (!formatDesc.persistent && (formatDesc.transient <= 0)) {
                this.#removeNumberFormat(formatId);
            }
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Adds support for all scalar value types to be formatted by the base
     * formatter.
     *
     * @param value
     *  The value to be converted.
     *
     * @returns
     *  A result object with the converted value, and an optional format code
     *  override.
     */
    protected override prepareFormatValue(value: unknown): PreparedFormatValue {

        // `null` represents blank cells
        if (value === null) {
            return { value: "", format: this.standardFormat };
        }

        // error codes will never be formatted
        if (isErrorCode(value)) {
            return { value: this.docModel.formulaGrammarUI.getErrorName(value), format: this.standardFormat };
        }

        // format boolean values like strings (with text format section and color)
        return { value: is.boolean(value) ? this.docModel.formulaGrammarUI.getBooleanName(value) : value };
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a new entry in the number format table.
     *
     * @param formatId
     *  The identifier for the new number format.
     *
     * @param formatCode
     *  The format code to be associated with the specified identifier.
     *
     * @returns
     *  The descriptor object representing the new number format.
     */
    #createNumberFormat(formatId: number, formatCode: string): NumberFormatDescriptor {

        // create and insert a new descriptor for the number format
        const formatDesc = map.toggle(this.#formatTable, formatId, {
            code: formatCode,
            persistent: false,
            transient: 0,
        });

        // update the first free identifier for user-defined formats
        this.#firstFreeId = Math.max(this.#firstFreeId, formatId + 1);

        // insert the format code into the reverse table
        this.#reverseTable.set(formatCode, formatId);

        // return the new format descriptor
        return formatDesc;
    }

    /**
     * Removes an existing entry from the number format table.
     *
     * @param formatId
     *  The identifier for the number format to be removed.
     */
    #removeNumberFormat(formatId: number): void {

        // remove the format descriptor from the map
        const formatDesc = map.remove(this.#formatTable, formatId);
        if (formatDesc) {

            // update the first free identifier for user-defined formats
            if (formatId + 1 === this.#firstFreeId) {
                while ((this.#firstFreeId > PRESET_ID_USER_START) && !this.#formatTable.has(this.#firstFreeId - 1)) {
                    this.#firstFreeId -= 1;
                }
            }

            // remove the format code from the reverse table
            this.#reverseTable.delete(formatDesc.code);
        }
    }

    /**
     * Extracts the attributes "formatId" and "formatCode" from the passed cell
     * attributes, and invokes the callback function if both attributes exist.
     */
    #updateTransientFormat(cellAttrs: Partial<CellAttributes>, updateHandler: UpdateTransientFormatFn): void {

        // nothing to do for non-OOXML files
        if (this.fileFormat !== FileFormatType.OOX) { return; }

        // extract format identifier and the explicit format code from the attributes
        const formatId = cellAttrs.formatId;
        const formatCode = cellAttrs.formatCode;

        // invoke the callback function with identifier and format code
        if (is.number(formatId) && is.string(formatCode)) {
            updateHandler(formatId, formatCode, this.#formatTable.get(formatId));
        }
    }

    /**
     * Callback handler for the document operation "insertNumberFormat".
     *
     * @param context
     *  A wrapper representing the "insertNumberFormat" document operation.
     */
    #applyInsertOperation(context: SheetOperationContext): void {

        // format identifier must not be negative
        const formatId = context.getInt("id");
        context.ensure(formatId >= 0, "unexpected negative format identifier");

        // do not replace predefined number formats (except for currency formats), but do not fail in this case
        if ((formatId < PRESET_ID_USER_START) && !REPLACEABLE_FORMAT_ID_SET.has(formatId)) {
            modelLogger.warn(`$badge{NumberFormatter} applyInsertOperation: attempted to replace predefined number format ${formatId}`);
            return;
        }

        // get the new format code from the operation (bug 46211: may be an empty string)
        const formatCode = context.getStr("code", { empty: true });

        // insert the format code into the internal map, or update an existing map entry
        let formatDesc = this.#formatTable.get(formatId);
        if (formatDesc) {
            this.#reverseTable.delete(formatDesc.code);
            formatDesc.code = formatCode;
            this.#reverseTable.set(formatCode, formatId);
        } else {
            formatDesc = this.#createNumberFormat(formatId, formatCode);
        }

        // mark the number format to be persistent
        formatDesc.persistent = true;
    }

    /**
     * Callback handler for the document operation "deleteNumberFormat".
     *
     * @param context
     *  A wrapper representing the "deleteNumberFormat" document operation.
     */
    #applyDeleteOperation(context: SheetOperationContext): void {

        // format identifier must refer to a user-defined number format
        const formatId = context.getInt("id");
        context.ensure(formatId >= PRESET_ID_USER_START, "invalid format identifier");

        // reset the persistent flag, but do not remove the number format, if it is
        // still in use, e.g. in the attribute set of a conditional formatting rule
        const formatDesc = this.#formatTable.get(formatId);
        context.ensure(formatDesc?.persistent, "invalid format identifier");
        if (formatDesc.transient <= 0) {
            this.#removeNumberFormat(formatId);
        } else {
            formatDesc.persistent = false;
        }
    }
}
