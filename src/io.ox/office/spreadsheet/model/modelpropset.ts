/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { PropertySet } from "@/io.ox/office/tk/containers";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * All runtime properties for a spreadsheet document model.
 */
export interface IModelProps {

    /**
     * Index of the active (visible) sheet in the document; or -1 to deactivate
     * the current sheet temporarily (only used internally while switching the
     * active sheet).
     */
    activeSheet: number;

    /**
     * Whether to show cell references in formulas in R1C1 notation.
     */
    rcStyle: boolean;
}

// class ModelPropertySet =====================================================

/**
 * A runtime property set for a spreadsheet document model (dynamic properties
 * that are not backed by document operations).
 */
export class ModelPropertySet extends PropertySet<IModelProps> {
    constructor(docModel: SpreadsheetModel) {
        super({
            activeSheet: {
                def: -1,
                validate(sheet) {
                    return ((sheet >= -1) && (sheet < docModel.getSheetCount())) ? sheet : undefined;
                }
            },
            rcStyle: {
                def: false
            }
        });
    }
}
