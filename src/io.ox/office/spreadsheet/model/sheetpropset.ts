/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { PropertySet } from "@/io.ox/office/tk/containers";

import { Color } from "@/io.ox/office/editframework/utils/color";

import { SplitMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { type PaneSide, PanePos } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * All runtime properties for a sheet model.
 */
export interface ISheetProps {

    /**
     * Sheet selection (cells and drawing objects). The array of cell range
     * addresses MUST NOT be empty.
     */
    selection: SheetSelection;

    /**
     * Current zoom factor in the sheet. The value `1` represents a zoom factor
     * of 100%.
     */
    zoom: number;

    /**
     * Whether to display the grid lines between all cells.
     */
    showGrid: boolean;

    /**
     * The color of the grid lines between the cells.
     */
    gridColor: Color;

    /**
     * The type of the current view split. Has no effect, if both properties
     * "splitWidth" and "splitHeight" are set to the value zero (view is not
     * split at all).
     */
    splitMode: SplitMode;

    /**
     * The inner width of the left panes in view split mode. In dynamic split
     * mode, this value is interpreted in 1/100 of millimeters. In frozen split
     * mode, this value specifies the number of columns shown in the frozen
     * panes. Must not be negative. If this value is zero, the view is not
     * split into left and right panes (but may be split into upper and lower
     * panes).
     */
    splitWidth: number;

    /**
     * The inner height of the upper panes in view split mode. In dynamic split
     * mode, this value is interpreted in 1/100 of millimeters. In frozen split
     * mode, this value specifies the number of rows shown in the frozen panes.
     * Must not be negative. If this value is zero, the view is not split into
     * top and bottom panes (but may be split into left and right panes).
     */
    splitHeight: number;

    /**
     * The identifier of the active pane in split mode.
     */
    activePane: PanePos;

    /**
     * The identifier of an entire active pane side. Used for example while
     * selecting entire columns or rows.
     */
    activePaneSide: PaneSide | null;

    /**
     * The horizontal scroll position in the left view panes. The integral part
     * represents the zero-based index of the first column visible in the
     * panes. The fractional part represents the ratio of the width of that
     * column where the exact scroll position (the first visible pixel) is
     * located.
     */
    anchorLeft: number;

    /**
     * The horizontal scroll position in the right view panes. The integral
     * part represents the zero-based index of the first column visible in the
     * panes. The fractional part represents the ratio of the width of that
     * column where the exact scroll position (the first visible pixel) is
     * located.
     */
    anchorRight: number;

    /**
     * The vertical scroll position in the upper view panes. The integral part
     * represents the zero-based index of the first row visible in the panes.
     * The fractional part represents the ratio of the height of that row where
     * the exact scroll position (the first visible pixel) is located.
     */
    anchorTop: number;

    /**
     * The vertical scroll position in the lower view panes. The integral part
     * represents the zero-based index of the first row visible in the panes.
     * The fractional part represents the ratio of the height of that row where
     * the exact scroll position (the first visible pixel) is located.
     */
    anchorBottom: number;
}

// private functions ==========================================================

function equals<T extends Equality<T>>(value1: T, value2: T): boolean {
    return value1.equals(value2);
}

// class SheetPropertySet =====================================================

/**
 * A runtime property set for a sheet model (dynamic properties that are not
 * backed by document operations).
 */
export class SheetPropertySet extends PropertySet<ISheetProps> {
    constructor(_sheetModel: SheetModel) {
        super({
            selection:      { def: SheetSelection.createFromAddress(Address.A1), equals },
            zoom:           { def: 1 },
            showGrid:       { def: true },
            gridColor:      { def: Color.auto() },
            splitMode:      { def: SplitMode.SPLIT },
            splitWidth:     { def: 0 },
            splitHeight:    { def: 0 },
            activePane:     { def: PanePos.BR },
            activePaneSide: { def: null },
            anchorLeft:     { def: 0 },
            anchorRight:    { def: 0 },
            anchorTop:      { def: 0 },
            anchorBottom:   { def: 0 }
        });
    }
}
