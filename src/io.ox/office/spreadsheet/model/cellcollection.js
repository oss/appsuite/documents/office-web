/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import moment from '$/moment';

import { addProperty } from '@/io.ox/office/tk/utils';
import { is, math, str, fun, itr, ary, map, dict, jpromise } from '@/io.ox/office/tk/algorithms';
import { MSEC_PER_DAY } from '@/io.ox/office/tk/utils/dateutils';

import { NO_BORDER, isVisibleBorder, equalBorders } from '@/io.ox/office/editframework/utils/border';
import { matchesAttributesSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { checkForHyperlink } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { OperationError } from '@/io.ox/office/editframework/utils/operationerror';

import {
    MAX_FILL_CELL_COUNT, ATTR_BORDER_KEYS, MAX_LENGTH_STANDARD_EDIT,
    Direction, MergeMode, ErrorCode,
    Interval, Range, IntervalArray, AddressArray, RangeArray, RangeSet,
    FindMatchType, modelLogger, makeRejected,
    isVerticalDir, isLeadingDir, getTextOrientation,
    getOuterBorderKey, getInnerBorderKey, getBorderName, splitFormulaParts, getAdjacentRange
} from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { stringifyIndex, Address } from '@/io.ox/office/spreadsheet/utils/address';
import { SubtotalResult } from '@/io.ox/office/spreadsheet/utils/subtotalresult';
import { iterateParallelSynchronized } from '@/io.ox/office/spreadsheet/utils/iterator';
import { consumeArraySource, someInArraySource } from "@/io.ox/office/spreadsheet/utils/arraybase";

import { AddressTransformer } from '@/io.ox/office/spreadsheet/model/addresstransformer';
import { UsedRangeCollection } from '@/io.ox/office/spreadsheet/model/usedrangecollection';
import { getModelValue, isBlankCell, CellModel } from '@/io.ox/office/spreadsheet/model/cellmodel';
import { CellModelMatrix } from '@/io.ox/office/spreadsheet/model/cellmodelmatrix';
import { createBorderChangeSet, createVisibleBorderChangeSet } from '@/io.ox/office/spreadsheet/model/operation/stylecache';
import { cloneWithCacheKey, OperationBuilder } from '@/io.ox/office/spreadsheet/model/operation/builder';
import { ContentRangeMode, mergeRangeMode, CellOperationBuilder } from '@/io.ox/office/spreadsheet/model/operation/celloperationbuilder';

import { slope } from '@/io.ox/office/spreadsheet/model/formula/utils/mathutils';
import { CompareNullMode, getCellValue, isErrorCode, isScalar, compareScalars } from '@/io.ox/office/spreadsheet/model/formula/formulautils';
import { CutPasteUpdateTask } from '@/io.ox/office/spreadsheet/model/formula/parser/updatetask';
import { SharedFormulaStorage } from '@/io.ox/office/spreadsheet/model/formula/sharedformulastorage';

// constants ==================================================================

// regular expression to match a leading integer in a string
const LEADING_INT_RE = /^(\d+)(.*)$/i;

// regular expression to match a trailing integer in a string
const TRAILING_INT_RE = /^(.*?)(\d+)$/i;

// private functions ==========================================================

/**
 * Returns the visibility flags for columns and rows according to the
 * option 'visible' supported by various methods of a cell collection.
 *
 * @param {object} [options]
 *  Optional parameters passed to a method of class CellCollection with a
 *  'visible' option that can be a boolean, or one of the strings 'columns'
 *  or 'rows'.
 *
 * @returns {object}
 *  An object with the boolean flags 'cols' and 'rows' specifying whether
 *  to visit visible columns and/or visible rows only.
 */
function getVisibleMode(options) {
    const mode = options && options.visible;
    return {
        cols: (mode === true) || (mode === 'columns'),
        rows: (mode === true) || (mode === 'rows')
    };
}

/**
 * Returns a predicate function that maches cell models for specific type
 * specifiers.
 *
 * @param {string} type
 *  The cell type specifier.
 *
 * @returns {(model: CellModel | null) => boolean}
 *  A predicate function that receives either an instance of `CellModel` or
 *  `null`, and returns whether it matches the type specifier.
 */
const getCellTypePredicate = fun.do(() => {

    const PREDICATE_FUNCS = {
        any:     fun.true,
        defined: fun.identity,
        value:   cellModel => !isBlankCell(cellModel),
        formula: cellModel => !!cellModel?.isAnyFormula(),
        anchor:  cellModel => !!cellModel?.isAnyAnchor()
    };

    return type => PREDICATE_FUNCS[type] || PREDICATE_FUNCS.any;
});

// class CellCollection =======================================================

/**
 * Collects cell contents and formatting attributes for a single sheet.
 * To save memory and to improve performance, instances of this class store
 * specific parts of the sheet only. More cell data will be fetched from
 * the server on demand.
 */
export class CellCollection extends UsedRangeCollection {

    // the shared formulas in this sheet, mapped by shared index
    sharedFormulas = this.member(new SharedFormulaStorage(this.sheetModel));

    // document model containers
    #numberFormatter = this.docModel.numberFormatter;
    #addressFactory = this.docModel.addressFactory;
    #autoStyles = this.docModel.autoStyles;
    #sortListCollection = this.docModel.sortListCollection;

    // the column/row collections of the sheet
    #colCollection = this.sheetModel.colCollection;
    #rowCollection = this.sheetModel.rowCollection;

    // all cell models mapped by address key
    #modelMap = this.member(new Map/*<string, CellModel>*/());
    // all cells, as sorted array of sorted columns
    #colMatrix = this.member(new CellModelMatrix(this.sheetModel, true));
    // all cells, as sorted array of sorted columns
    #rowMatrix = this.member(new CellModelMatrix(this.sheetModel, false));

    // the range addresses of all matrix formulas
    #matrixRangeSet = new RangeSet();

    // constructor ------------------------------------------------------------

    /**
     * @param {SheetModel} sheetModel
     *  The sheet model instance containing this collection.
     */
    constructor(sheetModel) {

        // base constructor
        super(sheetModel);

        // additional processing and event handling after the document has been imported
        sheetModel.waitForImportSuccess(() => {

            // update the sheet indexes after the sheet collection has been manipulated
            this.listenTo(this.docModel, "transform:sheets", xfVector => {
                this.#modelMap.forEach(cellModel => cellModel.t?.transformSheets(xfVector));
            });
        });

        // update the formatted display string of all cell models after changing the UI locale
        this.listenTo(this.docModel, "change:locale", () => {
            for (const cellModel of this.#modelMap.values()) {
                cellModel.updateParsedFormat(this.#autoStyles);
                cellModel.updateDisplayString(this.#numberFormatter);
            }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the current zero-based index of the sheet containing this cell
     * collection.
     *
     * @returns {number}
     *  The current zero-based index of the sheet containing this cell
     *  collection.
     */
    getSheetIndex() {
        return this.sheetModel.getIndex();
    }

    /**
     * Returns the style identifier for the auto-style of the row or column to
     * be used for an undefined cell at the specified position.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.useUid=false]
     *    If set to `true`, the unique object identifier of an auto-style (the
     *    property `uid` of the auto-style model) will be returned instead.
     *  - {boolean} [options.ignoreRowStyle=false]
     *    If set to `true`, an existing active row auto-style will be ignored,
     *    and this method effectively returns the default column auto-style of
     *    the specified cell.
     *
     * @returns {string}
     *  The style identifier of the row auto-style, if the specified row
     *  contains the `customFormat` flag (and the ignore flag is not set),
     *  otherwise the style identifier of the column auto-style.
     */
    getDefaultStyleId(address, options) {
        const rowDesc = options?.ignoreRowStyle ? null : this.#rowCollection.getEntry(address.r);
        const effDesc = rowDesc?.merged.customFormat ? rowDesc : this.#colCollection.getEntry(address.c);
        return options?.useUid ? effDesc.suid : effDesc.style;
    }

    /**
     * Returns whether the specified cell is visible (located in a visible
     * column, AND a visible row).
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {boolean}
     *  Whether the specified cell is visible.
     */
    isVisibleCell(address) {
        return this.#colCollection.isVisibleIndex(address.c) && this.#rowCollection.isVisibleIndex(address.r);
    }

    /**
     * Returns the internal cell model with the specified cell address from the
     * collection.
     *
     * _Attention:_ The method is provided for optimized access to internal
     * cell data. The cell model MUST NOT BE CHANGED in order to retain the
     * internal consistency of the cell collection!
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {CellModel|null}
     *  The cell model; or `null`, if the cell does not exist in this cell
     *  collection. MUST NOT be changed!
     */
    getCellModel(address) {
        return this.#modelMap.get(address.key) || null;
    }

    /**
     * Returns whether the specified cell is blank (no result value). A blank
     * cell may contain formatting attributes.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {boolean}
     *  Whether the specified cell is blank.
     */
    isBlankCell(address) {
        return isBlankCell(this.getCellModel(address));
    }

    /**
     * Returns the value (or formula result) of the specified cell.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {ScalarType}
     *  The current value (or formula result) of the specified cell.
     */
    getValue(address) {
        return getModelValue(this.getCellModel(address));
    }

    /**
     * Returns the default display string of the specified cell.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {string|null}
     *  The default display string of the specified cell (regardless of the
     *  column width); or null, if the cell cannot display its value (e.g. due
     *  to an invalid number format).
     */
    getDisplayString(address) {
        // display string of blank cells is the empty string (`null` reserved for format errors!)
        const cellModel = this.getCellModel(address);
        return cellModel ? cellModel.d : "";
    }

    /**
     * Returns whether the specified cell is a normal formula cell, or part of
     * a shared formula or matrix formula.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {boolean}
     *  Whether the specified cell is a formula cell.
     */
    isFormulaCell(address) {
        const cellModel = this.getCellModel(address);
        if (cellModel?.isAnyFormula()) { return true; }
        return this.#matrixRangeSet.containsAddress(address);
    }

    /**
     * Returns whether the specified cell contains an actual formula
     * expression, either as normal formula cell, or as anchor cell of a shared
     * formula or matrix formula.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {boolean}
     *  Whether the specified cell contains an actual formula expression.
     */
    isAnchorCell(address) {
        const cellModel = this.getCellModel(address);
        return !!cellModel && cellModel.isAnyAnchor();
    }

    /**
     * Returns the token array of the specified formula cell.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.fullMatrix=false]
     *    If set to `true`, returns the token array of a matrix formula for
     *    every cell covered by the matrix. By default, the token array will be
     *    returned for the anchor cell of the matrix formula only.
     *  - {string} [options.grammarType]
     *    If specified, must be the identifier of a formula grammar. The
     *    descriptor returned by this method will contain an additional
     *    property "formula" with the formula expression of the cell as string,
     *    according to the formula grammar. If omitted, the formula expression
     *    will not be generated (better performance).
     *
     * @returns {object|null}
     *  A result object with the token array and the reference address of the
     *  specified cell; or null, if the cell does not contain a formula. The
     *  result object will contain the following properties:
     *  - {string} type
     *    The formula type specifier. The value 'normal' represents a normal
     *    formula cell. The value 'shared' represents a cell that is part of a
     *    shared formula. The value 'matrix' represents a cell that is part of
     *    a matrix formula.
     *  - {TokenArray} tokenArray
     *    The token array with the parsed formula expression used to calculate
     *    the result value of the cell.
     *  - {Address} refAddress
     *    The reference address of the token array. For normal formula cells,
     *    this address is equal to the passed cell address. For shared
     *    formulas, this is the anchor address of the shared formula (the
     *    address of the cell containing the definition of the entire shared
     *    formula).
     *  - {Range|null} matrixRange
     *    The bounding range of a matrix formula; or null, if the cell is not
     *    part of a matrix formula.
     *  - {boolean} dynamicMatrix
     *    Whether the matrix formula is a dynamic matrix.
     *  - {string|null} formula
     *    The formula expression for the specified cell, if the option
     *    `grammarType` has been passed; otherwise null.
     */
    getTokenArray(address, options) {

        // the cell model (may be null for a cell inside a matrix formula)
        let cellModel = this.getCellModel(address);
        // the grammar identifier to generate the formula expression for
        const grammarType = options?.grammarType;

        // use the model of a shared formula as result object
        if (cellModel?.sf) {
            return {
                tokenArray: cellModel.sf.tokenArray,
                refAddress: cellModel.sf.anchorAddress,
                sharedIndex: cellModel.sf.index,
                matrixRange: null,
                dynamicMatrix: false,
                formula: grammarType ? cellModel.sf.getFormula(grammarType, address) : null
            };
        }

        // resolve matrix range for other cells in a matrix formula
        let refAddress = address;
        if (!cellModel?.t && options?.fullMatrix) {
            const matrixRange = this.#matrixRangeSet.findOneByAddress(address);
            refAddress = matrixRange?.a1 ?? null;
            cellModel = refAddress ? this.getCellModel(refAddress) : null;
        }

        // resolve the token array of a normal formula cell, and for a matrix anchor cell
        return cellModel?.t ? {
            tokenArray: cellModel.t,
            refAddress: refAddress.clone(),
            sharedIndex: null,
            matrixRange: cellModel.mr?.clone() ?? null,
            dynamicMatrix: cellModel.md,
            formula: grammarType ? cellModel.getFormula(grammarType) : null
        } : null;
    }

    /**
     * Returns the formula expression of the cell at the specified address.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @param {GrammarType} grammarType
     *  The identifier of the formula grammar for the formula expression.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {boolean} [options.fullMatrix=false]
     *    If set to `true`, returns the formula expression of a matrix formula
     *    for every cell covered by the matrix. By default, the formula
     *    expression will be returned for the anchor cell of the matrix formula
     *    only.
     *
     * @returns {string|null}
     *  The formula expression of the specified cell, if it is a formula cell;
     *  otherwise `null`.
     */
    getFormula(address, grammarType, options) {
        const tokenDesc = this.getTokenArray(address, { ...options, grammarType });
        return tokenDesc?.formula ?? null;
    }

    /**
     * Returns the bounding range of the matrix formula the specified cell is
     * located in.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {range|null}
     *  The bounding range of the matrix formula the specified cell is located
     *  in; or `null`, if the cell is not part of a matrix formula.
     */
    getMatrixRange(address) {
        const cellModel = this.getCellModel(address);
        if (cellModel?.mr) { return cellModel.mr.clone(); }
        const matrixRange = this.#matrixRangeSet.findOneByAddress(address);
        return matrixRange?.clone() ?? null;
    }

    /**
     * Returns whether any cell in the passed cell range addresses is part of a
     * matrix formula.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, ot the address of a single cell
     *  range.
     *
     * @param {FindMatchType} [matchType]
     *  Specifies which matrix ranges from this collection will match.
     *
     * @returns {Boolean}
     *  Whether any cell in the passed cell range addresses is part of a matrix
     *  formula.
     */
    coversAnyMatrixRange(ranges, matchType) {
        return someInArraySource(ranges, range => {
            return !!this.#matrixRangeSet.findOne(range, matchType);
        });
    }

    /**
     * Returns the identifier of the effective auto-style used by the cell at
     * the specified address.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @param {boolean} [useUid=false]
     *  If set to `true`, the unique object identifier of an auto-style (the
     *  property `uid` of the auto-style model) will be returned instead.
     *
     * @returns {string}
     *  The style identifier of the cell auto-style, if the cell is defined,
     *  otherwise of the default auto-style of the row or column.
     */
    getStyleId(address, useUid) {
        const cellModel = this.getCellModel(address);
        return cellModel ? this.#resolveStyleId(cellModel, useUid) : this.getDefaultStyleId(address, { useUid });
    }

    /**
     * Returns the effective merged attribute set of the specified cell.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {object}
     *  The merged attribute set of the cell, if it is defined, otherwise the
     *  attribute set of the default auto-style of the row or column.
     */
    getAttributeSet(address) {
        return this.#autoStyles.getMergedAttributeSet(this.getStyleId(address));
    }

    /**
     * Returns the effective parsed number format of the cell at the specified
     * address.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {ParsedFormat}
     *  The parsed number format of the cell, if it is defined, otherwise the
     *  parsed format of the default auto-style of the row or column.
     */
    getParsedFormat(address) {
        return this.#autoStyles.getParsedFormat(this.getStyleId(address));
    }

    /**
     * Returns the effective text orientation settings of the cell at the
     * specified address.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {object}
     *  A descriptor containing various text orientation properties. See
     *  function `getTextOrientation` for details.
     */
    getTextOrientation(address) {
        const cellModel = this.getCellModel(address);
        const styleId = this.getStyleId(address);
        const attrSet = this.#autoStyles.getMergedAttributeSet(styleId);
        const preferText = this.#autoStyles.getParsedFormat(styleId).isText();
        return getTextOrientation(getModelValue(cellModel), attrSet.cell.alignHor, cellModel ? cellModel.d : '', preferText);
    }

    /**
     * Returns whether the number format category of the specified cell is
     * "text", i.e. whether no automatic parsing of input text happens for that
     * cell.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {boolean}
     *  Whether the number format category of the specified cell is "text".
     */
    isTextFormat(address) {
        return this.getParsedFormat(address).isText();
    }

    /**
     * Returns the URL of a hyperlink as returned by a cell formula (via the
     * function HYPERLINK) at the specified address.
     *
     * @param {Address} address
     *  The address of a cell.
     *
     * @returns {string|null}
     *  The URL of a hyperlink as returned by a cell formula (via the function
     *  HYPERLINK) at the specified address.
     */
    getFormulaURL(address) {
        return this.getCellModel(address)?.url ?? null;
    }

    /**
     * Sets the URL of a hyperlink returned by the formula of the specified
     * cell (via the function HYPERLINK).
     *
     * @param {Address} address
     *  The address of a cell.
     *
     * @param {string|null} url
     *  The resulting URL returned by a HYPERLINK function contained in the
     *  cell formula.
     */
    setFormulaURL(address, url) {
        const cellModel = this.getCellModel(address);
        if (cellModel) { cellModel.setURL(url); }
    }

    /**
     * Returns the effective URL of a hyperlink at the specified address. If
     * the cell contains a regular hyperlink, and a cell formula with a
     * HYPERLINK function, the regular hyperlink will be preferred. See
     * description of the methods `getCellURL` and `getFormulaURL` for details.
     *
     * @param {Address} address
     *  The address of a cell.
     *
     * @returns {string|null}
     *  The effective URL of a hyperlink at the specified address.
     */
    getEffectiveURL(address) {
        const cellURL = this.sheetModel.hlinkCollection.getCellURL(address);
        return (cellURL === null) ? this.getFormulaURL(address) : cellURL;
    }

    // cell edit mode ---------------------------------------------------------

    /**
     * Returns the formula expression of the specified cell in `UI` grammar for
     * the cell edit mode, with a leading equality sign.
     *
     * @param {Address} address
     *  The address of a cell.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.matrixBraces=false]
     *    If set to `true`, and the specified cell contains a matrix formula,
     *    the resulting text will be enclosed in curly braces.
     *
     * @returns {string|null}
     *  The formula expression of the cell for cell edit mode; or null, if the
     *  cell does not contain a formula. If the cell contains the "hidden"
     *  attribute, and the sheet is locked, an empty string will be returned
     *  instead of the formula expression.
     */
    getEditFormula(address, options) {

        // get the raw formula expression, return null for non-formula cells
        const tokenDesc = this.getTokenArray(address, { grammarType: "ui", fullMatrix: true });
        if (!tokenDesc) { return null; }

        // hide formula if the cell contains the hidden attribute, and the sheet is locked
        if (this.sheetModel.isLocked() && this.getAttributeSet(address).cell.hidden) { return ''; }

        // add the leading equality sign, add curly braces for matrix formulas
        let formula = `=${tokenDesc.formula}`;
        if (tokenDesc.matrixRange && options?.matrixBraces) {
            // DOCS-4504: do not enclose single-cell dynamic matrix formula
            if (!tokenDesc.dynamicMatrix || !tokenDesc.matrixRange.single()) {
                formula = `{${formula}}`;
            }
        }

        return formula;
    }

    /**
     * Returns the text of the specified cell for the cell edit mode. Returns
     * the formula expression for formula cells (in `UI` grammar with the
     * leading equality sign), otherwise the cell value formatted with an
     * appropriate default number format.
     *
     * @param {Address} address
     *  The address of a cell.
     *
     * @param {object} [options]
     *  Optional parameters. Supports all options that are supported by the
     *  method `CellCollection#getEditFormula`.
     *
     * @returns {string}
     *  The text for the cell edit mode.
     */
    getEditString(address, options) {

        // prefer formula expression over formula result
        const formula = this.getEditFormula(address, options);
        if (formula !== null) { return formula; }

        // the result value of the cell to be formatted
        let value = this.getValue(address);

        // strings: use plain unformatted string for editing (bug 34421: add an apostrophe if necessary)
        if (is.string(value) && (value.length > 0)) {
            return ((/^['=]/).test(value) || !is.string(this.#numberFormatter.parseScalarValue(value).value)) ? `'${value}` : value;
        }

        // booleans: use plain boolean literal for editing
        if (is.boolean(value)) {
            return this.docModel.formulaGrammarUI.getBooleanName(value);
        }

        // error codes: use plain error code literal for editing
        if (isErrorCode(value)) {
            return this.docModel.formulaGrammarUI.getErrorName(value);
        }

        // numbers: use appropriate number representation according to number format category
        if (Number.isFinite(value)) {

            // the resulting formatted value
            let formatted = null;

            // process different format categories
            const category = this.getParsedFormat(address).category;
            switch (category) {

                // percent: multiply by 100, add percent sign without whitespace
                case 'percent':
                    value *= 100; // may become infinite, e.g. `1e307 * 100`
                    if (Number.isFinite(value)) {
                        formatted = this.#numberFormatter.autoFormatNumber(value, { stdLen: MAX_LENGTH_STANDARD_EDIT }).text + '%';
                    }
                    break;

                // automatically show date and/or time, according to the number
                case 'date':
                case 'time':
                case 'datetime': {
                    // number of milliseconds
                    const milliSecs = Math.round(value * MSEC_PER_DAY);
                    // number of days
                    const date = Math.floor(milliSecs / MSEC_PER_DAY);
                    // number of milliseconds in the day
                    const time = Math.floor(milliSecs % MSEC_PER_DAY);
                    // whether to add the date and time part to the result
                    const hideDate = (category === 'time') && (date === 0);
                    const hideTime = (category === 'date') || (time === 0);
                    // the resulting format code
                    const parsedFormat = this.#numberFormatter.getSystemDateTimeFormat({ hideDate, hideTime });
                    // the resulting formatted value (may be `null` for invalid dates)
                    // always use colon as time separator in strings to be parsed by the number formatter
                    formatted = this.#numberFormatter.formatValue(parsedFormat, value, { timeSep: ":" }).text;
                    break;
                }
            }

            // use standard number format for all other format codes
            return (formatted !== null) ? formatted : this.#numberFormatter.autoFormatNumber(value, { stdLen: MAX_LENGTH_STANDARD_EDIT }).text;
        }

        // empty cells
        return '';
    }

    // range iterators --------------------------------------------------------

    /**
     * Creates an iterator that generates the cell addresses for all, or
     * specific, cells contained in the passed cell ranges.
     *
     * @param {RangeSource} ranges
     *  An array of range addresses, or a single cell range address, used to
     * generate cell addresses from.
     *
     * @param {CellIteratorOptions} [options]
     *  Optional parameters.
     *
     * @yields {CellEntry}
     *  The cell entries.
     */
    *cellEntries(ranges, options) {

        // collection of merged ranges in the active sheet
        const { sheetModel } = this;
        const { mergeCollection } = sheetModel;
        // whether to visit visible columns and/or rows only
        const visible = getVisibleMode(options);
        // whether to look for merged ranges
        const skipCovered = options?.skipCovered && !mergeCollection.isEmpty();

        // the shrink method for merged ranges
        const shrinkMethod = visible.cols ? (visible.rows ? 'both' : 'columns') : (visible.rows ? 'rows' : undefined);

        // split and reorder the passed ranges according to the passed options (the new ranges will contain
        // the additional properties 'orig' and 'index'), and create an array iterator for these ranges
        const srcRanges = RangeArray.splitRanges(ranges, options);
        for (const srcRange of ary.values(srcRanges, { reverse: options?.reverse })) {

            // returns the merged ranges overlapping the current range on first invocation (for performance:
            // if the range does not contain any matching cells, the merged ranges will not be collected at all)
            const getMergedRanges = fun.once(() => {

                // the merged ranges covering the cell range currently iterated
                let mergedRanges = mergeCollection.getMergedRanges(srcRange);

                // shrink merged ranges to their visible parts in visible cells mode
                if (shrinkMethod && !mergedRanges.empty()) {
                    mergedRanges = RangeArray.map(mergedRanges, mergedRange => {
                        mergedRange = sheetModel.shrinkRangeToVisible(mergedRange, shrinkMethod);
                        // remove resulting single-cell ranges from the array
                        return (mergedRange && !mergedRange.single()) ? mergedRange : null;
                    });
                }

                return mergedRanges;
            });

            // visit the cell addresses in the range
            for (const cellEntry of this.#yieldCellsForRange(srcRange, options)) {

                // skip cells that are covered by merged ranges
                if (skipCovered) {
                    const { address } = cellEntry;
                    const mergedRange = getMergedRanges().findByAddress(address);
                    if (mergedRange && !mergedRange.startsAt(address)) { continue; }
                }

                cellEntry.orig = srcRange.orig;
                cellEntry.index = srcRange.index;
                yield cellEntry;
            }
        }
    }

    /**
     * Creates an iterator that generates the cell addresses for all, or
     * specific, cells contained in a single column or row, while moving away
     * from the start cell into the specified directions.
     *
     * @param {Address} address
     *  The address of the cell to start the iteration process from (the option
     *  `skipStart` can be used, if iteration shall start with the nearest
     *  available neighbor of the start cell, and not with the cell itself).
     *
     * @param {Direction|Direction[]} directions
     *  Specifies how to move to the next cell addresses while iterating.
     *  Multiple directions passed in an array will be processed in order.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {CellType} [options.type=ALL]
     *    Specifies which type of cells will be covered by the iterator.
     *  - {boolean|"columns"|"rows"} [options.visible=false]
     *    Specifies how to handle hidden columns and rows.
     *  - {boolean} [options.skipCovered=false]
     *    If set to `true`, covered cells in merged ranges will not be visited
     *    (this may become rather expensive). By default, all cells covered and
     *    hidden by a merge range will be visited.
     *  - {Range} [options.boundRange]
     *    If specified, the line iterator will be restricted to that cell range.
     *    If omitted, the entire area of the sheet will be used.
     *  - {boolean} [options.skipStart=false]
     *    If set to `true`, the iterator will start at the nearest neighbor of
     *    the cell specified in the parameter `address` instead of that cell
     *    (nearest visible cell, if option `visible` has been set).
     *
     * @yields {LinearCellEntry}
     *  The result objects with the following properties:
     *  - {Address} value.address
     *    The address of the cell currently visited.
     *  - {CellModel|null} value.model
     *    The model of an existing cell, or `null` for an undefined cell.
     *  - {ScalarType} value.value
     *    The value of the current cell.
     *  - {boolean} value.blank
     *    Whether the current cell is blank.
     *  - {Direction} value.direction
     *    The position of the current cell, relative to the start cell (one of
     *    the directions passed in the method parameter `direction`).
     */
    *linearCellEntries(address, directions, options) {

        // the bounding range to restrict the visited area
        const boundRange = options?.boundRange ?? this.#addressFactory.getSheetRange();
        // number of cells to eb skipped before iteration starts
        const skipCount = options?.skipStart ? 1 : 0;

        // process all specified directions
        for (const direction of ary.wrap(directions)) {

            // whether to iterate with variable column index
            const columns = !isVerticalDir(direction);
            // whether to iterate in reversed order (towards first column/row)
            const reverse = isLeadingDir(direction);
            // the entire column/row range containing the passed address
            let range = this.#addressFactory.getFullRange(address.get(!columns), !columns);

            // reduce range to leading or trailing part, according to start address and direction
            if (reverse) {
                range.setEnd(address.get(columns) - skipCount, columns);
            } else {
                range.setStart(address.get(columns) + skipCount, columns);
            }

            // skip the current direction, if the range becomes invalid (e.g. UP in first row)
            if (range.getEnd(columns) < range.getStart(columns)) { continue; }

            // reduce the range to the bounding range (this may invalidate the range too)
            range = boundRange.intersect(range);
            if (!range) { continue; }

            // visit all cells in the cell range
            for (const cellEntry of this.cellEntries(range, { ...options, reverse })) {
                cellEntry.direction = direction;
                yield cellEntry;
            }
        }
    }

    /**
     * Creates an iterator that returns an array of cell addresses (one address
     * for each source range, synchronized by offset in the ranges).
     *
     * @param {RangeArray} ranges
     *  The cell ranges to be visited in parallel by the iterator.
     *
     * @yields {Address[]}
     *  The arrays with one cell address per source range.
     */
    *parallelAddresses(ranges, options) {

        // create an address iterator for each cell range
        const iterators = ranges.map(range => this.#yieldCellsForRange(range, options));

        // visit the cells in the ranges in parallel (synchronize addresses by their linear offset in the source ranges)
        const iterator = iterateParallelSynchronized(iterators, (cellEntry, index) => ranges[index].indexAt(cellEntry.address));

        // map the parallel iterator result to an array of cell addresses
        for (const { offset } of iterator) {
            yield ranges.map(range => range.addressAt(offset));
        }
    }

    /**
     * Creates an iterator that visits the equally formatted parts in the
     * passed cell ranges.
     *
     * @param {RangeSource} ranges
     *  An array of range addresses, or a single cell range address, to be
     *  visited by the iterator.
     *
     * @param {Object} [options]
     *  Optional parameters. Supports all options supported by the method
     *  `RangeArray#split` to define a custom start position inside the passed
     *  cell ranges (i.e. the options `reverse`, `startIndex`, `startAddress`,
     *  `skipStart`, and `wrap`), and the following additional options:
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only visible cells (cells in visible columns, AND
     *    visible rows) will be covered by the iterator. By default, all
     *    visible and hidden cells will be covered.
     *  - {boolean} [options.skipCovered=false]
     *    If set to `true`, covered cells in merged ranges will not be visited
     *    (this may become rather expensive). By default, all cells covered and
     *    hidden by a merge range will be visited.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the ranges AND the cell addresses in each range
     *    will be processed in reversed order.
     *
     * @yields {CellStyleEntry}
     *  The result objects with the following properties:
     *  - {Range} value.range
     *    The current cell range address.
     *  - {string|null} value.style
     *    The style identifier of the auto-style for all cells in the range.
     *  - {Range} value.origRange
     *    The address of the original cell range (from the passed range array)
     *    containing the current cell range.
     *  - {number} value.origIndex
     *    The array index of the original cell range contained in the property
     *    `origRange`.
     */
    *styleRanges(ranges, options) {

        // collection of merged ranges in the active sheet
        const { sheetModel } = this;
        const { mergeCollection } = sheetModel;
        // whether to visit visible cells only
        const visible = options?.visible;
        // whether to visit cells covered (hidden) by merged ranges
        const skipCovered = options?.skipCovered;
        // whether to iterate in reversed order
        const reverse = options?.reverse;

        // context for local generator functions
        const self = this;

        // Generates a style entry for a single cell. Used as shortcut for single cells to improve
        // performance, to prevent to create all the column/row intervals, sub-iterators, etc.
        function *yieldStylesForAddress(address) {
            if (!(skipCovered && mergeCollection.isHiddenCell(address))) {
                yield {
                    range: new Range(address.clone()),
                    style: self.getStyleId(address)
                };
            }
        }

        // Creates an iterator that tries to combine existing equally formatted cells into sub-ranges.
        // The result values of the iterator will contain the following properties:
        // - {IntervalArray} colIntervals
        //      The column intervals of all equally formatted ranges of the iterator step; each
        //      interval in the array with the additional property 'style'.
        // - {Interval} rowInterval
        //      The row interval containing all equally formatted ranges of the iterator step.
        function *yieldIntervalsForExistingCells(range) {

            // creates a column interval for the cell in the passed value of a cell iterator
            const createColInterval = cellEntry => {
                const colInterval = new Interval(cellEntry.address.c);
                colInterval.style = self.#resolveStyleId(cellEntry.model);
                return colInterval;
            };

            // creates and initializes the property 'colIntervals' in the passed result if missing
            const initializeColIntervals = cellEntry => {
                return cellEntry.colIntervals || (cellEntry.colIntervals = new IntervalArray(createColInterval(cellEntry)));
            };

            // creates and initializes the property 'rowInterval' in the passed result if missing
            const initializeRowInterval = cellEntry => {
                return cellEntry.rowInterval || (cellEntry.rowInterval = new Interval(cellEntry.address.r));
            };

            // an iterator that visits all existing cells in the range
            let cellIterator = self.#yieldCellsForRange(range, { type: "defined", visible, reverse });

            // filter for existing cells with an auto-style that differs from its active column/row style
            cellIterator = itr.filter(cellIterator, cellEntry => self.#styleDiffersFromDefault(cellEntry.model));

            // a merge iterator that collapses all existing formatted cells of a single row into a column interval array
            // with auto-style identifiers (inserted as new property 'colIntervals' into the iterator result)
            cellIterator = itr.merge(cellIterator, (cellEntry1, cellEntry2) => {

                // shortcuts for addresses
                const address1 = cellEntry1.address;
                const address2 = cellEntry2.address;

                // immediately return if the new cell model is located in another row (the iterator will immediately step to "cellEntry1")
                if (address1.r !== address2.r) { return; }

                // get or create the column interval array (will be initialized with column index if missing)
                const colIntervals = initializeColIntervals(cellEntry1);

                // try to extend the interval array (adjacent cell with equal formatting)
                let colInterval = reverse ? colIntervals.first() : colIntervals.last();
                if (self.#autoStyles.areEqualStyleIds(colInterval.style, self.#resolveStyleId(cellEntry2.model))) {
                    if (!reverse && (colInterval.last + 1 === address2.c)) {
                        colInterval.last += 1;
                        return cellEntry1;
                    }
                    if (reverse && (address2.c + 1 === colInterval.first)) {
                        colInterval.first -= 1;
                        return cellEntry1;
                    }
                }

                // create an interval for the new cell (push or unshift according to direction)
                colInterval = createColInterval(cellEntry2);
                if (reverse) { colIntervals.unshift(colInterval); } else { colIntervals.push(colInterval); }

                return cellEntry1;
            });

            // a merge iterator that collapses consecutive row intervals with equally formatted existing cells
            cellIterator = itr.merge(cellIterator, (cellEntry1, cellEntry2) => {

                // shortcuts for addresses
                const address2 = cellEntry2.address;

                // initialize the row interval in the first iterator result if missing
                const rowInterval = initializeRowInterval(cellEntry1);

                // the second iterator result must represent a row that can extend the row interval
                if (reverse ? (address2.r + 1 !== rowInterval.first) : (rowInterval.last + 1 !== address2.r)) { return; }

                // create missing column intervals in both result objects for comparison
                const colIntervals1 = initializeColIntervals(cellEntry1);
                const colIntervals2 = initializeColIntervals(cellEntry2);

                // compare the positions and auto-style identifiers of both column interval arrays
                const equalIntervals = (colIntervals1.length === colIntervals2.length) && colIntervals1.every((colInterval1, index) => {
                    const colInterval2 = colIntervals2[index];
                    return colInterval1.equals(colInterval2) && self.#autoStyles.areEqualStyleIds(colInterval1.style, colInterval2.style);
                });

                // column intervals are equal: expand the row interval, and continue to search for adjacent rows
                if (equalIntervals) {
                    if (reverse) { rowInterval.first -= 1; } else { rowInterval.last += 1; }
                    return cellEntry1;
                }
            });

            // ensure that all iterator results contain column and row intervals (reduce iterators may skip single entries)
            for (const cellEntry of cellIterator) {
                initializeColIntervals(cellEntry);
                initializeRowInterval(cellEntry);
                yield { colIntervals: cellEntry.colIntervals, rowInterval: cellEntry.rowInterval };
            }
        }

        // returns an interval array for the passed column interval (or the visible parts) to be used for rows with
        // an own active auto-style (rows with 'customFormat' flag set to true), and caches it for the iterator
        const getColIntervals = fun.memoize((colInterval, rowStyleId) => {
            const colIntervals = visible ? this.#colCollection.getVisibleIntervals(colInterval) : new IntervalArray(colInterval.clone());
            return addProperty(colIntervals, 'style', rowStyleId);
        }, (colInterval, rowStyleId) => `${colInterval.key}:${rowStyleId}`);

        // collect the default column auto-styles of the column interval to be used for unformatted rows that show
        // the default column auto-styles (rows with 'customFormat' flag set to false), and caches it for the iterator
        const getColStyleIntervals = fun.memoize(colInterval => {
            return IntervalArray.from(this.#colCollection.styleIntervals(colInterval, { visible }), ({ interval, style }) => {
                interval.style = style;
                return interval;
            });
        }, colInterval => colInterval.key);

        // collect the row bands of the merged ranges covering the passed cell range
        const getMergedRowBands = range => {

            // get merged ranges in the current range
            const mergedRanges = mergeCollection.getMergedRanges(range);
            if (mergedRanges.empty()) { return null; }

            // skip all cells but the top-left cell of the merged ranges
            let skipRanges = new RangeArray();
            for (const mergedRange of mergedRanges) {
                const { start, end } = mergedRange;
                if (!mergedRange.singleCol()) { skipRanges.push(Range.fromIndexes(start.c + 1, start.r, end.c, start.r)); }
                if (!mergedRange.singleRow()) { skipRanges.push(Range.fromIndexes(start.c, start.r + 1, end.c, end.r)); }
            }

            // reduce the ranges to the visible areas if the "visible" flag is set, reduce to the passed cell range address
            if (visible) { skipRanges = RangeArray.map(skipRanges, range => sheetModel.shrinkRangeToVisible(range)); }
            skipRanges = skipRanges.intersect(range);
            if (skipRanges.empty()) { return null; }

            // return the row bands containing the column intervals
            return skipRanges.getRowBands({ intervals: true });
        };

        // reduce the passed column intervals with auto-style identifiers to the parts not covered by merged ranges
        const reduceStyleColIntervals = (styleColIntervals, skipColIntervals) => {
            const partColIntervals = styleColIntervals.concat(skipColIntervals).partition();
            return RangeArray.filter(partColIntervals, partColInterval => {
                return !partColInterval.coveredBy.some(origColInterval => {
                    if (!("style" in origColInterval)) { return true; }
                    partColInterval.style = origColInterval.style;
                    delete partColInterval.coveredBy;
                    return false;
                });
            });
        };

        // returns a style iterator for the passed equally formatted and completely visivle row interval,
        // and the entire column interval (with hidden columns that need to be ignored in visible mode)
        function *yieldStylesForRowInterval(colInterval, rowInterval, rowStyleId) {

            // shortcut for single cells (do not create all the column/row intervals etc.)
            if (colInterval.single() && rowInterval.single()) {
                yield* yieldStylesForAddress(new Address(colInterval.first, rowInterval.first));
                return;
            }

            // an iterator that provides row intervals with existing, equally-formatted cells (as column intervals)
            const range = Range.fromIntervals(colInterval, rowInterval);
            // the column intervals with auto-style identifiers (use column auto-styles, if the row interval is not formatted)
            const colIntervals = (rowStyleId === null) ? getColStyleIntervals(colInterval) : getColIntervals(colInterval, rowStyleId);
            // an iterator that provides row intervals with existing, equally-formatted cells (as column intervals)
            const intervalsIt = yieldIntervalsForExistingCells(range);
            // the iterator provides settings for a row interval with existing formatted cells
            let intervalsEntry = itr.shift(intervalsIt);
            // the row bands of the merged ranges covering the current range to be skipped
            const mergedRowBands = skipCovered ? getMergedRowBands(range) : null;
            // the current row index (for the gaps between row intervals of existing cells)
            let currRow = reverse ? rowInterval.last : rowInterval.first;

            // visit the row intervals with existing cells, and the gaps between these row intervals (blank rows)
            while (rowInterval.containsIndex(currRow)) {

                // the next row interval to be visited (either a gap between cells, or a row interval with cells)
                let currRowInterval = null;
                // the column intervals with style identifiers
                let styleColIntervals = null;

                // find the next row interval to be processed, and build the column intervals with auto-style identifiers
                if (!intervalsEntry) {
                    // no more formatted cells available: process the remaining rows to the end (reverse mode: to the beginning) of the entire row interval
                    currRowInterval = reverse ? new Interval(rowInterval.first, currRow) : new Interval(currRow, rowInterval.last);
                    styleColIntervals = colIntervals;
                } else if (!intervalsEntry.rowInterval.containsIndex(currRow)) {
                    // 'currRow' is above the next formatted cells (reverse mode: below the cells): process the gap of blank rows
                    currRowInterval = reverse ? new Interval(intervalsEntry.rowInterval.last + 1, currRow) : new Interval(currRow, intervalsEntry.rowInterval.first - 1);
                    styleColIntervals = colIntervals;
                } else {
                    // process a row interval that contains existing formatted cells
                    currRowInterval = reverse ? new Interval(intervalsEntry.rowInterval.first, currRow) : new Interval(currRow, intervalsEntry.rowInterval.last);
                    styleColIntervals = self.#colCollection.mergeStyleIntervals(colIntervals, intervalsEntry.colIntervals);
                }

                // shorten the current row interval according to the next merged row band
                const nextMergedRowBand = !mergedRowBands ? null : reverse ? ary.at(mergedRowBands, -1) : mergedRowBands[0];
                if (nextMergedRowBand) {
                    if (nextMergedRowBand.containsIndex(currRow)) {
                        // current row interval starts in a row band with merged ranges
                        if (reverse) {
                            currRowInterval.first = Math.max(currRowInterval.first, nextMergedRowBand.first);
                        } else {
                            currRowInterval.last = Math.min(currRowInterval.last, nextMergedRowBand.last);
                        }
                        // reduce the column intervals to the parts that are not covered by merged ranges
                        styleColIntervals = reduceStyleColIntervals(styleColIntervals, nextMergedRowBand.intervals);
                    } else {
                        // current row interval starts before a row band with merged ranges (reverse mode: after a merged row band)
                        if (reverse) {
                            currRowInterval.first = Math.max(currRowInterval.first, nextMergedRowBand.last + 1);
                        } else {
                            currRowInterval.last = Math.min(currRowInterval.last, nextMergedRowBand.first - 1);
                        }
                    }
                }

                // index of the next row to be processed
                currRow = reverse ? (currRowInterval.first - 1) : (currRowInterval.last + 1);

                // fetch next cell result, if the row interval of the current result has been processed
                if (intervalsEntry && (reverse ? (currRow < intervalsEntry.rowInterval.first) : (intervalsEntry.rowInterval.last < currRow))) {
                    intervalsEntry = itr.shift(intervalsIt);
                }

                // delete the row band for merged columns, if it has been processed
                if (nextMergedRowBand && (reverse ? (currRow < nextMergedRowBand.first) : (nextMergedRowBand.last < currRow))) {
                    if (reverse) { mergedRowBands.pop(); } else { mergedRowBands.shift(); }
                }

                // create and return the result for the iterator
                for (const styleColInterval of styleColIntervals.values({ reverse })) {
                    yield { range: Range.fromIntervals(styleColInterval, currRowInterval), style: styleColInterval.style };
                }
            }
        }

        // creates an iterator that visits all equally formatted sub-ranges in the passed range
        function *yieldStylesForRange(range) {

            // Reduce the passed range to the visible area if the "visible" flag is set. Continue with
            // the next cell range, if no more visible cells are left (the range shrinks to `null`).
            // The range may still contain hidden columns/rows in the middle, this will be handled later.
            if (visible && !(range = sheetModel.shrinkRangeToVisible(range))) {
                return;
            }

            // shortcut for single cells (do not create all the column/row intervals etc.)
            if (range.single()) {
                yield* yieldStylesForAddress(range.a1);
                return;
            }

            // create a nested iterator that returns the results of a style iterator for each row interval
            for (const { interval, style } of self.#rowCollection.styleIntervals(range.rowInterval(), options)) {
                yield* yieldStylesForRowInterval(range.colInterval(), interval, style);
            }
        }

        // split and reorder the passed ranges according to the passed options
        // (the new ranges will contain the additional properties "orig" and "index")
        for (const srcRange of RangeArray.splitRanges(ranges, options)) {
            for (const styleEntry of yieldStylesForRange(srcRange)) {
                styleEntry.origRange = srcRange.orig;
                styleEntry.origIndex = srcRange.index;
                yield styleEntry;
            }
        }
    }

    /**
     * Returns an iterator that provides the subtotal results of specific
     * subranges and/or cells contained in the passed ranges.
     *
     * @param {RangeArray|Range} ranges
     *  An array of range addresses, or a single cell range address, whose
     *  subtotals will be returned.
     *
     * @yields {SubtotalResult|ScalarType}
     *  The subtotal result (class `SubtotalResult`), or scalar cell values.
     */
    *generateSubtotals(ranges) {

        // restrict the used range to its visible part (subtotals are collected from visible cells only)
        const usedRange = this.getVisibleUsedRange();
        // restrict the passed ranges to the visible used range of this collection
        ranges = usedRange ? RangeArray.cast(ranges).intersect(usedRange) : null;

        // return immediately if the ranges do not cover any existing cells
        if (!ranges || ranges.empty()) {
            yield new SubtotalResult();
            return;
        }

        // get the column intervals of the ranges covering the used range vertically
        const colIntervals = RangeArray.filter(ranges, range => range.equalRows(usedRange)).colIntervals().merge();

        // get the row intervals of the ranges covering the used range horizontally
        const rowIntervals = RangeArray.filter(ranges, range => range.equalCols(usedRange)).rowIntervals().merge();

        // decide whether to use the cached subtotals of the column or row intervals (more covered cells)
        const coveredCellsByCols = colIntervals.size() * usedRange.rows();
        const coveredCellsByRows = rowIntervals.size() * usedRange.cols();
        if (coveredCellsByRows < coveredCellsByCols) {
            rowIntervals.clear();
            ranges = ranges.difference(RangeArray.fromIntervals(colIntervals, usedRange.rowInterval()));
        } else {
            colIntervals.clear();
            ranges = ranges.difference(RangeArray.fromIntervals(usedRange.colInterval(), rowIntervals));
        }

        // Get the subtotal results of the column/row intervals from the matrix (cached internally), and remove
        // the column or row ranges from the passed ranges (the remaining ranges will be iterated individually).
        if (!colIntervals.empty()) {
            yield* this.#colMatrix.yieldSubtotals(colIntervals);
            ranges = ranges.difference(RangeArray.fromIntervals(colIntervals, usedRange.rowInterval()));
        } else if (!rowIntervals.empty()) {
            yield* this.#rowMatrix.yieldSubtotals(rowIntervals);
            ranges = ranges.difference(RangeArray.fromIntervals(usedRange.colInterval(), rowIntervals));
        }

        // Create a cell iterator (do not collect the subtotals of a cell multiple times, always visit the cells
        // covered by merged ranges which is faster and matches the behavior of Excel where covered cells are
        // always empty, and OpenOffice where covered cells are always counted into the subtotals).
        if (!ranges.empty()) {
            for (const cellEntry of this.#yieldCellsForRanges(ranges.merge(), { type: 'value', visible: true })) {
                yield cellEntry.value;
            }
        }
    }

    // range data access ------------------------------------------------------

    /**
     * Finds the address of the first available cell of a specific type in the
     * passed cell ranges.
     *
     * @param {RangeSource} ranges
     *  An array of range addresses, or a single cell range address, to be
     *  searched.
     *
     * @param {CellIteratorOptions} [options]
     *  Optional parameters.
     *
     * @returns {Address|null}
     *  The address of the first available cell matching the specified cell
     *  type; or `null`, if no cell has been found.
     */
    findFirstCell(ranges, options) {
        // find the first cell in the search ranges
        const cellEntry = itr.shift(this.cellEntries(ranges, options));
        return cellEntry?.address ?? null;
    }

    /**
     * Returns the address of the next available cell of a specific type (any
     * blank cells, defined cells only, or content cells only) in the specified
     * direction.
     *
     * @param {Address} address
     *  The address of the cell whose nearest adjacent cell will be searched.
     *
     * @param {Direction|Direction[]} directions
     *  Specifies where to look for the next cell. Multiple directions passed
     *  in an array will be processed in order.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {CellType} [options.type=ALL]
     *    Specifies the type of the next cell to be searched.
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only a visible cell (in a visible column, AND a
     *    visible row) will be found. By default, all visible and hidden cells
     *    will be searched.
     *  - {Range} [options.boundRange]
     *    If specified, the search area will be restricted to that cell range.
     *    If omitted, the entire area of the sheet will be used.
     *
     * @returns {Address|null}
     *  The address of the next available cell matching the specified cell
     *  type; or `null`, if no cell has been found.
     */
    findFirstCellLinear(address, directions, options) {
        // get the first result object of a linear iterator into the specified direction
        const cellEntry = itr.shift(this.linearCellEntries(address, directions, { ...options, skipStart: true }));
        return cellEntry?.address ?? null;
    }

    /**
     * Returns whether all passed ranges are blank (no values). Ignores cell
     * formatting attributes.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns {boolean}
     *  Whether all cells in the passed ranges are blank.
     */
    areRangesBlank(ranges) {
        // return whether a cell iterator does not find a single value cell
        return !itr.shift(this.#yieldCellsForRanges(RangeArray.cast(ranges).merge(), { type: 'value' }));
    }

    /**
     * Returns whether all passed ranges are undefined (no values, AND no
     * formatting).
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns {boolean}
     *  Whether all cells in the passed ranges are undefined.
     */
    areRangesUndefined(ranges) {
        // return whether a cell iterator does not find a single defined cell
        return !itr.shift(this.#yieldCellsForRanges(RangeArray.cast(ranges).merge(), { type: 'defined' }));
    }

    /**
     * Returns the contents of all cells in the passed cell ranges.
     *
     * @param {RangeSource} ranges
     *  An array of range addresses, or a single cell range address, whose cell
     *  contents will be returned.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {boolean} [options.blanks=false]
     *    If set to `true`, all blank cells will be included in the result. By
     *    default, only non-blank cells will be collected.
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only visible cells (cells in visible columns, AND
     *    in visible rows) will be included in the result. Otherwise, all
     *    visible and hidden cells will be returned.
     *  - {boolean} [options.attributes=false]
     *    If set to `true`, the result will contain the identifier of the cell
     *    auto-style, the merged formatting attributes, and the parsed number
     *    format of the cells.
     *  - {boolean} [options.display=false]
     *    If set to `true`, the result will contain the formatted display
     *    strings of the cells in the property 'display'.
     *  - {boolean} [options.compressed=false]
     *    If set to `true`, the result array will be optimized: Consecutive
     *    cells with equal contents and formatting will be represented by a
     *    single array element with an additional property 'count'. Especially
     *    useful, if large ranges located in the unused area of the sheet are
     *    queried.
     *  - {number} [options.maxCount]
     *    If specified, the maximum number of cells that will be returned in
     *    the result, regardless how large the passed ranges are.
     *
     * @returns {object[]}
     *  The contents of the cells in the passed ranges. The result will be an
     *  array of cell content objects. Each cell content object will contain
     *  the following properties:
     *  - {ScalarType} value
     *    The cell value, or formula result (`null` for blank cells).
     *  - {string} [style]
     *    The identifier of the cell auto-style. Will only be set, if the
     *    option 'attributes' has been set to true (see above).
     *  - {object} [attributes]
     *    The merged formatting attributes of the cell, resolved from the cell
     *    auto-style. Will only be set, if the option 'attributes' has been set
     *    to `true` (see above).
     *  - {ParsedFormat} [format]
     *    The parsed number format code of the cell. Will only be set, if the
     *    option 'attributes' has been set to true (see above).
     *  - {string} [display]
     *    The display string of the cell, according to its current value and
     *    number format. If the value cannot be formatted with the current
     *    number format, the string '#######' (with exactly 7 characters) will
     *    be used instead. Will only be set, if the option 'display' has been
     *    set to true (see above).
     *  - {number} [count]
     *    Always set in compressed mode (see option 'compressed' above).
     *    Contains the number of consecutive cells with equal contents and
     *    formatting represented by this array element.
     *
     *  The cells in the array will be in order of the passed ranges. Cells in
     *  each single range will be collected row-by-row. The result array will
     *  contain the additional property 'count' that represents the total
     *  number of cells contained in the result (this value will be different
     *  to the length of the array in compressed mode).
     */
    getRangeContents(ranges, options) {

        // whether to include blank cells
        const blanks = options && options.blanks;
        // whether to skip cells of hidden columns/rows
        const visible = options && options.visible;
        // compressed mode
        const compressed = options && options.compressed;
        // the maximum number of cells to be included in the result
        const maxCount = options && options.maxCount;
        // the result array returned by this method
        const contents = [];
        // the number of cells already inserted into the result (differs to array length in compressed mode)
        contents.count = 0;

        // creates a new result entry with value property, and optional display property
        const makeEntry = options?.display ?
            (value, display) => ({ value, display: (display === null) ? '#######' : display }) :
            value => ({ value });

        // inserts the passed result entry into the array (handles compressed mode)
        const pushEntry = (entry, count) => {

            // reduce the passed count, if the limit will be reached
            if (maxCount) { count = Math.min(count, maxCount - contents.count); }
            if (count === 0) { return; }

            // update the total count of cells in the result array
            contents.count += count;

            // the preceding result, for compressed mode
            const prevEntry = compressed ? _.last(contents) : null;

            // increase count of previous entry, if the new entry is equal
            if (prevEntry) {
                entry.count = prevEntry.count;
                if ((prevEntry.value === entry.value) && (prevEntry.style === entry.style) && (prevEntry.display === entry.display)) {
                    prevEntry.count += count;
                    return;
                }
            }

            // insert the new entry into the array (prepare count for compressed mode)
            if (compressed) {
                entry.count = count;
                contents.push(entry);
            } else {
                for (; count > 0; count -= 1) { contents.push(entry); }
            }
        };

        // pushes result entries for blank cells without formatting attributes
        const pushBlanks = blanks ? (count => pushEntry(makeEntry(null, ''), count)) : fun.undef;

        // collects all cells in the passed range without formatting attributes
        const collectWithoutAttributes = range => {

            // linear index of next expected cell in the range (used to fill blank cells)
            let blankIndex = 0;

            // fill the result array with existing cells and preceding blank cells
            for (const { address, model } of this.cellEntries(range, { type: 'value', visible })) {

                // the linear index of the address in the range
                const index = range.indexAt(address);

                // insert blank cell entries before the current cell model, and the cell model
                pushBlanks(index - blankIndex);
                pushEntry(makeEntry(model.v, model.d), 1);
                blankIndex = index + 1;

                // early exit, if the limit has been reached
                if (maxCount === contents.count) { break; }
            }

            // fill up following blank cells
            pushBlanks(range.cells() - blankIndex);
        };

        // collects all cells in the passed range with formatting attributes
        const collectWithAttributes = range => {
            // TODO: use a cell iterator that collects equally formatted blank ranges
            for (const { address, value } of this.cellEntries(range, { type: blanks ? 'all' : 'value', visible })) {
                const entry = makeEntry(value, this.getDisplayString(address));
                entry.style = this.getStyleId(address);
                entry.attributes = this.#autoStyles.getMergedAttributeSet(entry.style);
                entry.format = this.#autoStyles.getParsedFormat(entry.style);
                pushEntry(entry, 1);
                // early exit, if the limit has been reached
                if (maxCount === contents.count) { break; }
            }
        };

        // collect all cell contents
        consumeArraySource(ranges, options?.attributes ? collectWithAttributes : collectWithoutAttributes);
        return contents;
    }

    /**
     * Returns the content range (the entire range containing consecutive
     * content cells) surrounding the specified cell range.
     *
     * @param {Range} range
     *  The address of the cell range whose content range will be searched.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.directions='up down left right']
     *    All directions the passed range will be expanded to, as space
     *    separated token list with the tokens 'up', 'down', 'left', and
     *    'right'. If omitted, the range will be expanded to all four
     *    directions.
     *
     * @returns {Range}
     *  The address of the content range of the specified cell range.
     */
    @modelLogger.profileMethod("$badge{CellCollection} findContentRange")
    findContentRange(range, options) {

        // collection of merged ranges in the active sheet
        const mergeCollection = this.sheetModel.mergeCollection;
        // the resulting content range (start by expanding to merged ranges)
        let currRange = mergeCollection.expandRangeToMergedRanges(range);
        // the result of the last iteration, to detect exiting the loop
        let lastRange = null;
        // all directions the range will be expanded to
        const directions = str.splitTokens(options?.directions || 'up down left right');

        // returns the next column/row index outside the passed cell range
        const getNextIndex = (srcRange, forward, columns) => {
            // index of the next column/row outside the range
            const startIndex = forward ? (srcRange.getEnd(columns) + 1) : (srcRange.getStart(columns) - 1);
            // return the index if it is valid, otherwise -1
            return (forward ? (startIndex > this.#addressFactory.getMaxIndex(columns)) : (startIndex < 0)) ? -1 : startIndex;
        };

        // array iterator function, must be defined outside the do-while loop
        const expandRangeBorder = (direction, index) => {

            // whether to move towards the end of the sheet
            const forward = /^(down|right)$/.test(direction);
            // whether to move through columns in the same row
            const columns = /^(left|right)$/.test(direction);
            // the descriptor of the next column/row
            let nextIndex = getNextIndex(currRange, forward, columns);

            // do nothing, if the range already reached the sheet borders
            if (nextIndex < 0) {
                directions.splice(index, 1);
                return;
            }

            // build a single column/row range next to the current range
            const boundRange = currRange.clone();
            boundRange.setBoth(nextIndex, columns);

            // expand sideways, unless these directions are not active anymore
            if (directions.includes(columns ? Direction.UP : Direction.LEFT)) {
                nextIndex = getNextIndex(boundRange, false, !columns);
                if (nextIndex >= 0) { boundRange.setStart(nextIndex, !columns); }
            }
            if (directions.includes(columns ? Direction.DOWN : Direction.RIGHT)) {
                nextIndex = getNextIndex(boundRange, true, !columns);
                if (nextIndex >= 0) { boundRange.setEnd(nextIndex, !columns); }
            }

            // find a content cell next to the current range
            const cellEntry = itr.shift(this.#yieldCellsForRange(boundRange, { type: 'value' }));
            if (!cellEntry) { return; }

            // expand the current range to the found content cell
            const currAddress = forward ? currRange.a2 : currRange.a1;
            currAddress.set(cellEntry.address.get(columns), columns);

            // performance: expand further into the direction as long as there are adjacent content cells
            for (const { address, blank } of this.linearCellEntries(cellEntry.address, direction, { skipStart: true })) {
                if (blank) { break; }
                currAddress.set(address.get(columns), columns);
            }
        };

        // expand in all four directions until the range does not change anymore
        do {

            // store result of last iteration for comparison below
            lastRange = currRange.clone();

            // expand current range into all remaining directions
            ary.forEach(directions, expandRangeBorder, { reverse: true });

            // expand the range to include merged ranges
            currRange = mergeCollection.expandRangeToMergedRanges(currRange);

        } while ((directions.length > 0) && currRange.differs(lastRange));

        // return the resulting range
        return currRange;
    }

    /**
     * Returns the address of the first or last cell with the specified
     * formatting attributes in the passed cell ranges, regardless whether the
     * cell is actually defined, or the attribute values have been derived from
     * column or row attributes. Uses an optimized search algorithm for the
     * empty areas between the defined cells, regarding intervals of equally
     * formatted columns and rows, and the position of merged ranges covering
     * these column/row default formatting.
     *
     * @param {RangeSource} ranges
     *  An array of range addresses, or a single cell range address, whose
     *  cells will be searched for the attributes.
     *
     * @param {Object} attrSet
     *  The incomplete attribute set to be matched against the formatting
     *  attributes in the passed cell ranges.
     *
     * @param {Object} [options]
     *  Optional parameters. Supports all options that are supported by the
     *  method `styleRanges`.
     *
     * @returns {Object|null}
     *  A result descriptor for the first (or last, depending on direction)
     *  cell with matching formatting attributes; or null, if no such cell
     *  exists in the passed cell ranges. The cell descriptor will contain the
     *  following properties:
     *  - {Address} address
     *    The address of the first cell with the matching attributes.
     *  - {Range} range
     *    The original range from the passed range array containing the cell
     *    with the matching attributes.
     *  - {number} index
     *    The array index of the cell range contained in the property 'range'.
     */
    @modelLogger.profileMethod("$badge{CellCollection} findCellWithAttributes")
    findCellWithAttributes(ranges, attrSet, options) {

        // create a style iterator that visits equally formatted cell ranges in row bands, search for a range with matching formatting attributes
        const styleEntry = itr.find(this.styleRanges(ranges, options), styleEntry => {
            const cellAttrSet = this.#autoStyles.getMergedAttributeSet(styleEntry.style);
            return matchesAttributesSet(cellAttrSet, attrSet);
        });
        if (!styleEntry) { return null; }

        // return a result descriptor on success
        const address = options?.reverse ? styleEntry.range.a2 : styleEntry.range.a1;
        return { address: address.clone(), range: styleEntry.origRange, index: styleEntry.origIndex };
    }

    /**
     * Returns whether the passed cell range contains a header row according to
     * the cell contents.
     *
     * @param {Range} range
     *  A single cell range address, whose cells will be sorted.
     *
     * @returns {boolean}
     *  Whether the passed cell range contains a header row according to the
     *  cell contents.
     */
    hasHeaderRow(range) {

        // bug 63153: single row range cannot have a header row
        if (range.singleRow()) { return false; }

        // return the type of the cell-value
        const detectValueType = cellModel => {
            if (!cellModel) { return null; }

            if (cellModel.isAnyFormula()) {
                return 'formula';
            }

            if (cellModel.isNumber()) {
                return cellModel.pf.isAnyDateTime() ? 'date' : 'number';
            }

            if (cellModel.isText()) {
                return this.#sortListCollection.hasList(cellModel.v) ? 'sortList' : 'string';
            }

            return cellModel.isBlank() ? '' : null;
        };

        // return an array of types from a range
        const typeDetection = range => Array.from(range.addresses(), address => detectValueType(this.getCellModel(address)));

        // decides whether a header-line is present, or not
        const hasHeadline = (headType, contentTypes) => {
            if (
                (headType === 'sortList'             && _.unique(contentTypes).length >= 1) ||
                (headType === 'date'                 && _.unique(contentTypes).length >= 1) ||
                (headType === null                   && _.unique(contentTypes).length >= 1) ||
                (_.unique(contentTypes).length === 1 && contentTypes[0] !== headType)
            ) {
                return 'yes';
            }

            if (
                (headType === 'number'  && _.unique(contentTypes).length >= 1) ||
                (headType === 'string'  && _.unique(contentTypes).length === 1 && contentTypes[0] === 'string') ||
                (headType === 'number'  && _.unique(contentTypes).length === 1 && contentTypes[0] === 'number') ||
                (headType === 'date'    && _.unique(contentTypes).length === 1 && contentTypes[0] === 'date') ||
                (headType === 'formula' && _.unique(contentTypes).length === 1 && contentTypes[0] === 'formula') ||
                (headType === 'formula' && _.unique(contentTypes).length >= 1)
            ) {
                return 'no';
            }

            return 'maybe';
        };

        const startCol = range.a1.c;
        const startRow = range.a1.r;

        const arrHeadTypes = [];
        let arrContentTypes = [];

        const states = { yes: 0, no: 0, maybe: 0 };

        // when the selection covers exactly one table, go on ...
        //   ... otherwise, it isn't relevant whether the tables have headerRows or not
        const tableIt = this.sheetModel.tableCollection.yieldTableModels({ overlapRange: range, autoFilter: true });
        const tableModel = itr.shift(tableIt);
        if (tableModel && !itr.shift(tableIt) && tableModel.getRange().equals(range)) {
            return tableModel.hasHeaderRow();
        }

        // creates an object which counts 'yes', 'no' and 'maybe' to decide whether
        // we have a headline or not
        for (const col of range.colIndexes()) {
            const currentRange = range.colRange(col - startCol);
            const contentRange = currentRange.clone().setStart(startRow + 1, false);

            const headType = detectValueType(this.getCellModel(currentRange.a1));
            arrHeadTypes.push(headType);

            const contentTypes = typeDetection(contentRange);
            arrContentTypes = arrContentTypes.concat(contentTypes);

            if (hasHeadline(headType, contentTypes) === 'yes') {
                states.yes++;

            } else if (hasHeadline(headType, contentTypes) === 'no') {
                states.no++;

            } else {
                states.maybe++;
            }
        }

        // if the leading line is empty, there is no header
        if (_.unique(arrHeadTypes).length === 1 && arrHeadTypes[0] === null) {
            return false;
        }

        // when 'yes' predominates, return 'true'
        if (states.yes > states.no) {
            return true;
        }

        // when 'no' predominates, check 'yes' + 'maybe' against 'no'
        if (states.no > states.yes) {
            return states.yes + states.maybe > states.no;
        }

        // otherwise, check 'maybe'
        return states.maybe > 0;
    }

    /**
     * Returns the source range for an automatically generated subtotal formula
     * for a single cell.
     *
     * @param {Address} address
     *  The address of the target cell for the subtotal formula.
     *
     * @returns {Range|null}
     *  The source range for a subtotal formula for the specified target cell;
     *  or `null`, if no such range could be found.
     */
    findAutoFormulaRange(address) {

        const top = this.#getAutoFormulaRange(address, Direction.UP);
        const left = this.#getAutoFormulaRange(address, Direction.LEFT);

        // prefer the range located nearer to the target cell
        if (top && left) {
            const preferLeft = Math.abs(address.c - left.address1.c) < Math.abs(address.r - top.address1.r);
            return preferLeft ? left.range : top.range;
        }

        if (top) { return top.range; }
        if (left) { return left.range; }

        return null;
    }

    // operation generators ---------------------------------------------------

    /**
     * Checks that the passed cell ranges are not oversized in order to
     * successfully generate document operations.
     *
     * @param {RangeArray|Range} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns {JPromise|null}
     *  The value `null`, if the passed cell ranges are not oversized;
     *  otherwise a promise that has been rejected with an object with property
     *  `cause` set to the error code `"cells:overflow"`.
     */
    checkMaxRangeSize(ranges) {
        return (ranges.cells() > MAX_FILL_CELL_COUNT) ? makeRejected('cells:overflow') : null;
    }

    /**
     * Converts the passed text to a cell contents object as expected by the
     * other operation generator methods of this class.
     *
     * @param {string} parseText
     *  The text to be parsed to a cell contents object. The text may represent
     *  a plain or formatted number, a boolean value, an error code, or a
     *  formula expression.
     *
     * @param {string} contextType
     *  The context type used to evaluate the formula result.
     *
     * @param {Address} refAddress
     *  The reference address used to interpret a formula expression.
     *
     * @param {Range} [matrixRange]
     *  The bounding range of a matrix formula. Used if the passed context type
     *  is 'mat'. If omitted, a 1x1 matrix formula is assumed. MUST contain the
     *  passed reference address.
     *
     * @returns {object}
     *  The result object, with the following properties:
     *  - {ScalarType} v
     *    The resulting value with correct data type, or the calculated result
     *    of a formula expression.
     *  - {string|null} f
     *    The formula expression, translated to the native formula grammar used
     *    in document operations, without leading equality sign; or `null`, if
     *    the passed text is not a formula expression.
     *  - {InterpreterResult|null} result
     *    The result descriptor of the formula interpreter; or `null`, if the
     *    passed text is not a formula expression.
     *  - {string|number} [format]
     *    A specific number format code (as string), or the identifier of a
     *    preset format code (as integer), associated to the parsed number, or
     *    the result of the formula. If omitted, the number format shall not be
     *    modified.
     *  - {string} [url]
     *    The trimmed URL, if the passed text represents a valid URL; or an
     *    empty string, if the cell currently contains a hyperlink, and the
     *    passed text is an empty string (causes to delete the existing
     *    hyperlink).
     */
    parseCellValue(parseText, contextType, refAddress, matrixRange) {

        // the resulting contents object
        const parseResult = { v: parseText, f: null, result: null };

        // do not parse the text for the number format category 'text'
        if (this.isTextFormat(refAddress)) { return parseResult; }

        // handle specific strings not to be recognized as formulas
        if (/^[-+=]+$/.test(parseText)) { parseText = '\'' + parseText; }

        // try to parse the text as (formatted) value (empty strings will result in value null)
        const parseScalarResult = this.#numberFormatter.parseScalarValue(parseText, { blank: true });
        parseResult.v = parseScalarResult.value;

        // the identifier or format code of a number format (may be changed via formula result)
        let parsedFormat = this.#numberFormatter.getParsedFormat(parseScalarResult.format);

        // If the parsed value is still a string (no conversion to numbers etc. possible), and the original
        // string starts with specific characters, parse and interpret a formula expression in UI grammar.
        // This check excludes simple numbers with leading sign, e.g. '-1234' (which have been converted to
        // numbers already), but includes other formula expressions starting with a sign character instead
        // of an equality sign, e.g. '-A1+A2'.
        const formulaParts = is.string(parseResult.v) ? splitFormulaParts(parseText) : undefined;
        if (formulaParts && (formulaParts.expr.length > 0)) {

            // parse the formula in UI grammar, and translate to native grammar
            const tokenArray = this.sheetModel.createCellTokenArray();
            tokenArray.parseFormula("ui", formulaParts.expr, refAddress, { autoCorrect: true });
            parseResult.f = tokenArray.getFormula("op", refAddress, refAddress);

            // calculate the formula result, handle formula errors
            const formulaResult = parseResult.result = tokenArray.interpretFormula(contextType, refAddress, refAddress, { matrixRange });
            parseResult.v = getCellValue(formulaResult.value);

            // the formula engine may provide an arbitrary number format with the result
            if (formulaResult.format) { parsedFormat = formulaResult.format; }
        }

        // add a formatting attribute for the number format, if the cell's category changes, but ignore
        // the standard number formats, and ignore changes inside any of the date/time categories
        // (e.g. do not change a date cell to a time cell)
        const formatCode = parsedFormat ? this.#getNewCellFormatCode(refAddress, parsedFormat) : null;
        if (formatCode) { parseResult.format = formatCode; }

        // try to parse a URL from the passed text
        if (parseResult.v === null) {
            // delete the URL explicitly (via an empty string), if text is cleared
            if (this.sheetModel.hlinkCollection.getCellURL(refAddress)) { parseResult.url = ''; }
        } else if (!parseResult.f && is.string(parseResult.v)) {
            const url = checkForHyperlink(parseResult.v);
            if (is.string(url) && (url.length > 0)) { parseResult.url = url; }
        }

        return parseResult;
    }

    /**
     * Generates the operations and undo operations to update or restore the
     * cell formulas in this collection.
     *
     * @param {SheetCache} sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param {FormulaUpdateTask} updateTask
     *  The update task to be executed.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateUpdateTaskOperations")
    generateUpdateTaskOperations(sheetCache, updateTask) {

        // additional handling for moving cells in this sheet
        const ownTransformer = this.sheetModel.isOwnMoveTask(updateTask) ? updateTask.transformer : undefined;

        // check that the move operation does not push cells outside the sheet
        if (ownTransformer?.insert) {
            const deleteRanges = ownTransformer.createBandRanges(ownTransformer.deleteIntervals);
            OperationError.assertCause(!this.findFirstCell(deleteRanges, { type: 'value' }), 'cells:pushoff');
        }

        // let the cell cache do all the dirty work
        return sheetCache.cellCache.updateFormulas(updateTask);
    }

    /**
     * Generates the operations to convert all shared formula cells in the
     * specified range to regular formula cells.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Range} range
     *  The address of the cell range where all shared formula cells will be
     *  converted to regular formula cells.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateBreakSharedFormulaOperations")
    generateBreakSharedFormulaOperations(generator, range) {

        // an operations builder for all affected shared formulas
        const cellBuilder = new CellOperationBuilder(this.sheetModel, generator);

        // visit all cells attached to a shared formula in the passed range
        const promise = this.asyncForEach(this.sharedFormulas.values(), sharedModel => {
            for (const address of sharedModel.addressSet) {
                if (range.containsAddress(address)) {
                    const cellModel = this.getCellModel(address);
                    const changeSet = cellModel.isSharedAnchor() ? { si: null, sr: null } : { f: sharedModel.getFormula("op", address) };
                    cellBuilder.insertCell(address, changeSet);
                }
            }
        });

        // finalize the cell operations, return the addresses of the changed range
        return promise.then(()  => cellBuilder.finalizeOperations());
    }

    /**
     * Generates the document operations and the undo operations to change the
     * specified contents for a single cell.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Address} address
     *  The address of the cell to be modified.
     *
     * @param {CellChangeSet} changeSet
     *  The new properties to be applied at the cell.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated.
     */
    generateCellOperations(generator, address, changeSet) {
        return this.generateCellsOperations(generator, [{ address, ...changeSet }]);
    }

    /**
     * Generates the cell operations, and the undo operations, to change the
     * specified contents for multiple cells in the sheet, and optionally the
     * appropriate hyperlink operations.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeChangeSet[]} changeSets
     *  The new properties be applied at the cells, and their addresses.
     *
     * @param {Object} [options]
     *  Configuration options for the cell operations builder that will be
     *  passed to the callback function. See class `CellOperationBuilder` for
     *  details.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateCellsOperations")
    generateCellsOperations(generator, changeSets, options) {
        modelLogger.trace('changeSets=', changeSets);

        // the cell operation builder
        const cellBuilder = new CellOperationBuilder(this.sheetModel, generator, options);
        // cell range arrays per URL, will be merged together below
        const linkRangesMap = new Map/*<string, RangeArray>*/();
        // the number of cells already processed, for overflow detection
        let changedCount = 0;

        // use a time-sliced loop to prevent freezing browser
        let promise = this.iterateArraySliced(changeSets, changeSet => {

            // the cell range covered by the change set (convert address to range)
            const range = (changeSet.address instanceof Address) ? new Range(changeSet.address) : changeSet.address;
            // the change flags specifying how the cell will be potentially changed
            const changeFlags = this.#getChangeFlags(changeSet);
            // special behavior when clearing values without new formatting (performance)
            const clearValues = changeFlags.blank && !changeFlags.style;

            // collect the URLs for hyperlinks
            if ('url' in changeSet) {
                map.upsert(linkRangesMap, changeSet.url, () => new RangeArray()).push(range);
            }

            // create an appropriate cell iterator for the change set (`null` if nothing needs to be done)
            const cellIterator = fun.yield(function *() {

                // undefine cells: visit all existing cell models, regardless of range mode (cells/columns/rows)
                if (changeFlags.undefine) {
                    yield* this.#yieldCellsForRange(range, { type: 'defined' });
                    return;
                }

                // removing values, but not the formulas, is an illegal operation
                if ((changeSet.v === null) && (changeSet.f !== null)) {
                    modelLogger.error('$badge{CellCollection} generateCellsOperations: illegal operation (clearing values without formulas)');
                    return;
                }

                // clear values without new formatting: visit value cells only
                if (clearValues) {
                    yield* this.#yieldCellsForRange(range, { type: 'value' });
                    return;
                }

                // when setting values or formulas, all cells must be visited (regardless of passed range mode)
                if (changeFlags.value || changeFlags.formula) {
                    yield* this.#yieldCellsForRange(range, { type: 'all' });
                    return;
                }

                // visit all cells for regular formatting
                const rangeMode = changeSet.rangeMode || ContentRangeMode.CELL_MERGE;
                if (changeFlags.style && (rangeMode === ContentRangeMode.CELL_MERGE)) {
                    yield* this.#yieldCellsForRange(range, { type: 'all' });
                    return;
                }

                // early exit, if nothing will be changed at all (remaining change flags are 'blank' and 'style')
                if (!changeFlags.blank && !changeFlags.style) { return; }

                // formatting of blank ranges specific to range mode
                switch (rangeMode) {

                    // create an iterator that visits existing cells only
                    case ContentRangeMode.ROW_DEF:
                        yield* this.#yieldCellsForRange(range, { type: 'defined' });
                        break;

                    // When changing the formatting of unformatted rows, the column intervals with default auto-style do
                    // not need to be merged with the new formatting attributes (the new row default auto-style will suffice),
                    // but column intervals with other auto-styles need to be merged with the new formatting attributes.
                    case ContentRangeMode.ROW_MERGE: {
                        const colIntervals1 = new IntervalArray();
                        const colIntervals2 = new IntervalArray();
                        for (const styleEntry of this.#colCollection.styleIntervals(range.colInterval())) {
                            const colIntervals = this.#autoStyles.isDefaultStyleId(styleEntry.style) ? colIntervals1 : colIntervals2;
                            colIntervals.push(styleEntry.interval);
                        }
                        // undefined cells in columns with the default style do not need to be processed
                        yield* this.#yieldCellsForIntervals(colIntervals1.merge(), range.rowInterval(), { type: 'defined' });
                        yield* this.#yieldCellsForIntervals(colIntervals2.merge(), range.rowInterval(), { type: 'all' });
                        break;
                    }

                    // When changing the formatting of columns, the row intervals without custom auto-style (row attribute
                    // "customFormat" set to false) do not need to be merged with the new formatting attributes (the new column
                    // default auto-style will suffice), but row intervals with custom formatting need to be merged with the
                    // new formatting attributes.
                    case ContentRangeMode.COL_MERGE: {
                        for (const styleEntry of this.#rowCollection.styleIntervals(range.rowInterval())) {
                            // if the current row interval is not formatted, create a cell iterator that visits existing cells only
                            const rowRange = Range.fromIntervals(range.colInterval(), styleEntry.interval);
                            const cellType = (styleEntry.style === null) ? 'defined' : 'all';
                            yield* this.#yieldCellsForRange(rowRange, { type: cellType });
                        }
                        break;
                    }

                    default:
                        modelLogger.error('$badge{CellCollection} generateCellsOperations: invalid range mode "' + rangeMode + '"');
                }
            }, this);

            // process the cells in the range covered by the change set
            return this.iterateSliced(cellIterator, cellEntry => {
                const hasChanged = cellBuilder.insertCell(cellEntry.address, changeSet);
                // count the changed cells, early exit on overflow
                if (hasChanged && !clearValues) {
                    changedCount += 1;
                    if (changedCount > MAX_FILL_CELL_COUNT) {
                        return makeRejected('cells:overflow');
                    }
                }
            });
        });

        // generate all operations, return the addresses of all changed cell ranges
        promise = jpromise.fastThen(promise, () => {

            const hlinkCollection = this.sheetModel.hlinkCollection;

            // first, create operations to remove hyperlinks
            map.visit(linkRangesMap, '', linkRanges => {
                hlinkCollection.generateHlinkOperations(generator, linkRanges.merge(), '');
                linkRangesMap.delete('');
            });

            // create new hyperlinks for the cells
            for (const [url, linkRanges] of linkRangesMap) {
                hlinkCollection.generateHlinkOperations(generator, linkRanges.merge(), url);
            }

            // generate the cell operations, return addresses of changed ranges to caller
            return cellBuilder.finalizeOperations();
        });

        return promise.done(changedRanges => {
            modelLogger.trace('changed=' + changedRanges);
        });
    }

    /**
     * Generates the cell operations, and the undo operations, to fill the
     * passed cell ranges in the sheet with some individual contents.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address. Each
     *  range object MUST contain an additional property `fillData` with cell
     *  properties to be applied at all cells in the respective range.
     *  Overlapping ranges MUST NOT contain the same cell properties with
     *  different values (e.g.: set value 1 or value 2 to the same cells in
     *  overlapping ranges), otherwise the result will be `undefined`. However,
     *  incomplete cell property objects of overlapping ranges will be merged
     *  together for the overlapping parts of the ranges (e.g.: setting the
     *  auto-style "a1" to the range A1:B2, and the value 2 to the range B2:C3
     *  will result in setting both the auto-style and the value to cell B2).
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated, or that
     *  will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - 'cells:overflow': The cell ranges contain too many filled cells.
     *  - 'cols:overflow': Too many entire columns in the cell ranges.
     *  - 'rows:overflow': Too many entire rows in the cell ranges.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateRangeOperations")
    generateRangeOperations(generator, ranges) {
        modelLogger.trace('ranges=' + ranges);

        // divide the original ranges into column ranges, row ranges, and regular cell ranges
        const rangeGroups = this.#addressFactory.getRangeGroups(ranges);
        // add the property `rangeMode` to the cell ranges
        const taggedRanges = this.#getTaggedRanges(ranges);

        // implementation callback for entire columns/rows
        const generateColRowOperations = (lclGenerator, collection, intervals) => {
            return collection.generateIntervalOperations(lclGenerator, intervals, { skipFiltered: true });
        };

        // implementation callback for cell ranges (merge cell content objects of overlapping ranges)
        const createCellContents = fillDataArray => fillDataArray.reduce(Object.assign, {});

        // process entire columns and rows, and simple cell ranges separately
        return this.#generateCombinedColRowCellOperations(generator, rangeGroups, taggedRanges, generateColRowOperations, createCellContents);
    }

    /**
     * Generates the cell operations, and the undo operations, to fill the same
     * cell contents into entire cell ranges in the sheet.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {CellChangeSet} changeSet
     *  The new properties to be applied at all cells.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated, or that
     *  will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - 'cells:overflow': The cell ranges contain too many filled cells.
     *  - 'cols:overflow': Too many entire columns in the cell ranges.
     *  - 'rows:overflow': Too many entire rows in the cell ranges.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateFillOperations")
    generateFillOperations(generator, ranges, changeSet) {
        modelLogger.trace('ranges=' + ranges + ' changes=', changeSet);

        // add an arbitrary cache key to the change set that causes internal auto-style caching
        // (all ranges will be formatted with the same new formatting attributes)
        changeSet = cloneWithCacheKey(changeSet);

        // divide the original ranges into column ranges, row ranges, and regular cell ranges
        const rangeGroups = this.#addressFactory.getRangeGroups(ranges);
        // add the 'rangeMode' property to the cell ranges
        const taggedRanges = this.#getTaggedRanges(ranges);
        // the change flags specifying how the cell will be potentially changed
        const changeFlags = this.#getChangeFlags(changeSet);

        // if ranges will be formatted, the cell ranges not covering entire columns or rows must not exceed a specific limit
        let checkPromise = (changeFlags.style && rangeGroups.cellRanges) ? this.checkMaxRangeSize(rangeGroups.cellRanges.merge()) : null;
        if (checkPromise) { return checkPromise; }

        // when filling with values/formulas, the entire ranges must not exceed a specific limit (but not when clearing or undefining ranges)
        checkPromise = (changeFlags.value || changeFlags.formula) ? this.checkMaxRangeSize(taggedRanges.merge()) : null;
        if (checkPromise) { return checkPromise; }

        // create or remove hyperlinks for the cell ranges
        if (is.string(changeSet.url)) {
            const url = changeSet.url;
            delete changeSet.url;
            this.sheetModel.hlinkCollection.generateHlinkOperations(generator, ranges, url);
        }

        // implementation callback for entire columns/rows
        const generateColRowOperations = (lclGenerator, collection, intervals) => {
            return collection.generateFillOperations(lclGenerator, intervals, changeSet, { skipFiltered: true });
        };

        // implementation callback for cell ranges (use the same change set for all ranges; it can
        // be recycled because `generateCombinedColRowCellOperations` creates a clone internally)
        const createCellContents = fun.const(changeSet);

        // process entire columns and rows, and simple cell ranges separately
        return this.#generateCombinedColRowCellOperations(generator, rangeGroups, taggedRanges, generateColRowOperations, createCellContents);
    }

    /**
     * Generates the cell operations, and the undo operations, to fill the
     * cells next to the specified range with automatically generated values,
     * formulas, and formatting.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Range} range
     *  The address of the source cell range with values, formulas, and
     *  formatting to be expanded into one direction.
     *
     * @param {Direction} direction
     *  The direction in which the specified cell range will be expanded.
     *
     * @param {number} count
     *  The number of columns/rows to extend the cell range into the specified
     *  direction (positive values), or to shrink the cell range (negative
     *  values).
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.copy=false]
     *    If set to `true`, source values will be copied, instead of
     *    incrementing the existing values automatically.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated, or that
     *  will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - 'cells:overflow': The cell ranges contain too many filled cells.
     *  - 'cols:overflow': Too many entire columns in the cell ranges.
     *  - 'rows:overflow': Too many entire rows in the cell ranges.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateAutoFillOperations")
    generateAutoFillOperations(generator, range, direction, count, options) {
        modelLogger.trace(() => `source=${range} dir=${direction} count=${count}`);

        // the used range in this collection
        const usedRange = this.getUsedRange();
        // the target range to be filled from the source range
        const targetRange = getAdjacentRange(range, direction, count);
        // a private undo generator for restoring deleted contents at the end
        const undoGenerator = this.sheetModel.createOperationGenerator({ applyImmediately: true });
        // DOCS-4185: restrict to visible cells, if the sheet contains a filtered table *anywhere*
        const skipHiddenCells = this.sheetModel.isFiltered();

        // first step for undo: delete the cells generated by auto-fill
        const hasCells = usedRange && !this.areRangesUndefined(range);
        const clearRange = hasCells ? usedRange.intersect(targetRange) : null;
        if (clearRange) {
            const clearRanges = skipHiddenCells ? this.sheetModel.getVisibleRanges(clearRange) : [clearRange];
            for (const range of clearRanges) {
                generator.generateClearCellsOperation(range, { undo: true });
            }
        }

        // clear all target cells (must be done first, before e.g. trying to merge cell ranges)
        let promise = this.generateFillOperations(undoGenerator, targetRange, { u: true });
        promise.done(() => generator.appendOperations(undoGenerator));

        // copy formatting of entire columns/rows
        const columns = !isVerticalDir(direction);
        if (this.#addressFactory.isFullRange(range, columns)) {
            const collection = columns ? this.#colCollection : this.#rowCollection;
            promise = promise.then(() => collection.generateAutoFillOperations(generator, range.interval(columns), direction, count));
        }

        // copy all merged ranges contained in the source range
        promise = promise.then(() => this.sheetModel.mergeCollection.generateAutoFillOperations(generator, range, direction, count));

        // copy all hyperlinks contained in the source range
        promise = promise.then(() => this.sheetModel.hlinkCollection.generateAutoFillOperations(generator, range, direction, count));

        // copy cell contents and styles
        if (hasCells) {
            promise = promise.then(() => this.#generateAutoFillOperations(generator, range, direction, count, options));
        }

        // append the undo operations to restore the cell contents at the end
        promise.done(() => generator.appendOperations(undoGenerator, { undo: true }));

        return promise;
    }

    /**
     * Generates the cell operations, and the undo operations, to fill the cell
     * ranges in the sheet with outer and/or inner borders.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeArray|Range} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {Object} borderAttributes
     *  The border attributes to be applied at all cells. May contain regular
     *  border attributes, as supported by the document operations
     *  ('borderTop', 'borderBottom', 'borderLeft', 'borderRight') which will
     *  be applied at the outer boundaries of the passed ranges; and the pseudo
     *  attributes 'borderInsideHor' and 'borderInsideVert', which will be
     *  applied to the inner cells of the cell ranges.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated, or that
     *  will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - 'cells:overflow': The cell ranges contain too many filled cells.
     *  - 'cols:overflow': Too many entire columns in the cell ranges.
     *  - 'rows:overflow': Too many entire rows in the cell ranges.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateBorderOperations")
    generateBorderOperations(generator, ranges, borderAttributes) {
        modelLogger.trace('ranges=' + ranges + ' attributes=', borderAttributes);

        // ensure an array, remove duplicates (but do not merge the ranges)
        ranges = RangeArray.cast(ranges).unify();
        // divide the original ranges into column ranges, row ranges, and regular cell ranges
        const rangeGroups = this.#addressFactory.getRangeGroups(ranges);

        // prepare the range arrays with the parts of the passed ranges that will get a new single border
        const rangesT = this.#getChangedBorderRanges(ranges, borderAttributes, false, true);
        const rangesB = this.#getChangedBorderRanges(ranges, borderAttributes, false, false);
        const rangesL = this.#getChangedBorderRanges(ranges, borderAttributes, true,  true);
        const rangesR = this.#getChangedBorderRanges(ranges, borderAttributes, true,  false);
        // add the 'rangeMode' property to the cell ranges
        const taggedRanges = this.#getTaggedRanges(new RangeArray(rangesT, rangesB, rangesL, rangesR));

        // root cache key for using the same auto-style cache keys across all generators
        const cacheKey = CellCollection.makeUid();

        // implementation callback for entire columns/rows (use the border attributes resolved above)
        const generateColRowOperations = (lclGenerator, collection, intervals) => {
            return collection.generateBorderOperations(lclGenerator, intervals, borderAttributes, cacheKey);
        };

        // implementation callback for cell ranges (use the border attributes resolved above)
        const createCellChangeSet = fillDataArray => createBorderChangeSet(fillDataArray, cacheKey);

        // process entire columns and rows, and simple cell ranges separately
        return this.#generateCombinedColRowCellOperations(generator, rangeGroups, taggedRanges, generateColRowOperations, createCellChangeSet);
    }

    /**
     * Generates the cell operations, and the undo operations, to change the
     * formatting of existing border lines of the passed cell ranges in the
     * sheet.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeArray|Range} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {Border} border
     *  A border attribute value, which may be incomplete. All properties
     *  contained in this object (color, line style, line width) will be set
     *  for all visible borders in the cell range. Omitted border properties
     *  will not be changed.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated, or that
     *  will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - 'cells:overflow': The cell ranges contain too many filled cells.
     *  - 'cols:overflow': Too many entire columns in the cell ranges.
     *  - 'rows:overflow': Too many entire rows in the cell ranges.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateVisibleBorderOperations")
    generateVisibleBorderOperations(generator, ranges, border) {
        modelLogger.trace(() => `ranges=${ranges} border=${JSON.stringify(border)}`);

        // ensure an array, remove duplicates (but do not merge the ranges)
        ranges = RangeArray.cast(ranges).unify();
        // divide the original ranges into column ranges, row ranges, and regular cell ranges
        const rangeGroups = this.#addressFactory.getRangeGroups(ranges);

        // all borders need to be changed inside the passed ranges
        const fillRanges = addProperty(ranges.deepClone(), 'fillData', { keys: ATTR_BORDER_KEYS });
        // the adjacent cells outside the ranges (the adjoining borders will be changed too)
        const rangesT = this.#getAdjacentBorderRanges(ranges, false, true);
        const rangesB = this.#getAdjacentBorderRanges(ranges, false, false);
        const rangesL = this.#getAdjacentBorderRanges(ranges, true,  true);
        const rangesR = this.#getAdjacentBorderRanges(ranges, true,  false);
        // add the 'rangeMode' property to the cell ranges
        const taggedRanges = this.#getTaggedRanges(new RangeArray(fillRanges, rangesT, rangesB, rangesL, rangesR));

        // root cache key for using the same auto-style cache keys across all generators
        const cacheKey = CellCollection.makeUid();

        // implementation callback for entire columns/rows (use the border attributes resolved above)
        const generateColRowOperations = (lclGenerator, collection, intervals) => {
            return collection.generateVisibleBorderOperations(lclGenerator, intervals, border, cacheKey);
        };

        // implementation callback for cell ranges (use the border attributes resolved above)
        const createCellChangeSet = fillDataArray => {
            return createVisibleBorderChangeSet(fillDataArray, border, cacheKey);
        };

        // process entire columns and rows, and simple cell ranges separately
        return this.#generateCombinedColRowCellOperations(generator, rangeGroups, taggedRanges, generateColRowOperations, createCellChangeSet);
    }

    /**
     * Generates the cell operations, and the undo operations, to merge the
     * specified cell ranges. The first value cell of each cell range will be
     * moved to the top-left corner of the cell range, and all other cells
     * covered by the ranges will be deleted. The creation of column or row
     * formatting for merged ranges covering entire columns or rows will be
     * handled automatically.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeArray|Range} ranges
     *  An array of cell range addresses, or a single cell range address.
     *  Multiple ranges MUST NOT overlap each other!
     *
     * @param {MergeMode} mergeMode
     *  The merge mode for the specified ranges.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil (with the addresses of the ranges that have
     *  really been changed) when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateMergeCellsOperations")
    generateMergeCellsOperations(generator, ranges, mergeMode) {
        modelLogger.trace(() => `ranges=${ranges} mode=${mergeMode.toString()}`);

        // nothing to do when unmerging cells
        if (mergeMode === MergeMode.UNMERGE) {
            return this.createResolvedPromise(new RangeArray());
        }

        // whether to merge single columns in the passed ranges
        const vertical = mergeMode === MergeMode.VERTICAL;
        // whether to merge single rows in the passed ranges
        const horizontal = mergeMode === MergeMode.HORIZONTAL;
        // the ranges to be processed, with additional information in 'fillData' property
        const fillRanges = new RangeArray();
        // the border ranges to be processed, with additional information in 'borderAttrs' property
        const borderRanges = new RangeArray();
        // a private generator for restoring all range borders
        const borderGenerator = this.sheetModel.createOperationGenerator({ applyImmediately: true });
        // the used area in this collection
        const usedRange = this.getUsedRange();

        // horizontal/vertical merging does not have any effect for single-column/row ranges
        ranges = RangeArray.filter(ranges, range => {
            return horizontal ? !range.singleCol() : vertical ? !range.singleRow() : !range.single();
        });

        // process each range for its own (ranges do not overlap)
        let promise = this.iterateArraySliced(ranges, range => {

            // number of subranges to be processed according to merge type
            const count = vertical ? range.cols() : horizontal ? range.rows() : 1;

            // process all columns/rows separately, or the entire range, according to merge type
            return this.repeatSliced(index => {

                // the subrange to be merged (search for a value cell, same formatting)
                const partRange = vertical ? range.colRange(index) : horizontal ? range.rowRange(index) : range.clone();
                fillRanges.push(partRange);

                // find the first cell model with a value in the current range
                const cellModel = itr.shift(this.#yieldCellsForRange(partRange, { type: 'value' }))?.model;

                // fill the entire range with the same formatting (either from first value cell, or from origin)
                const styleId = cellModel ? this.#resolveStyleId(cellModel) : this.getStyleId(partRange.a1);
                partRange.fillData = { s: styleId };

                // collect the current outer borders and diagonal borders of the merged range
                const rangeBorderMap = this.#getRangeBorders(partRange);

                // delete the inner borders in a multi-column or multi-row range
                const styleBorderMap = this.#autoStyles.getBorderMap(styleId);
                const styleBorderAttrs = {};
                if (!partRange.singleCol() && (styleBorderMap.l || styleBorderMap.r)) {
                    styleBorderAttrs.borderLeft = styleBorderAttrs.borderRight = NO_BORDER;
                }
                if (!partRange.singleRow() && (styleBorderMap.t || styleBorderMap.b)) {
                    styleBorderAttrs.borderTop = styleBorderAttrs.borderBottom = NO_BORDER;
                }
                for (const borderKey of "du") {
                    if ((rangeBorderMap[borderKey] === null) && (styleBorderMap[borderKey])) {
                        styleBorderAttrs[getBorderName(borderKey)] = NO_BORDER;
                    }
                }
                if (!_.isEmpty(styleBorderAttrs)) {
                    partRange.fillData.a = { cell: styleBorderAttrs };
                }

                // process the outer borders of the merged range separately
                const outerBorderAttrs = {};
                _.forEach(rangeBorderMap, (rangeBorder, borderKey) => {
                    if ((borderKey === 'd') || (borderKey === 'u')) { return; }
                    const borderName = getBorderName(borderKey);
                    if (rangeBorder === null) {
                        // hide outer border line, if it is ambiguous
                        outerBorderAttrs[borderName] = NO_BORDER;
                    } else if (isVisibleBorder(rangeBorder)) {
                        // set a visible border line as outer border
                        outerBorderAttrs[borderName] = rangeBorder;
                    } else if (styleBorderMap[borderKey]) {
                        // hide a visible border of the auto-style
                        outerBorderAttrs[borderName] = NO_BORDER;
                    }
                });
                if (!is.empty(outerBorderAttrs)) {
                    const borderRange = partRange.clone();
                    borderRange.borderAttrs = outerBorderAttrs;
                    borderRanges.push(borderRange);
                }

                // move contents of the first value cell to the origin of the merged range
                if (cellModel) {

                    // the range consisting only of the start address
                    const startRange = new Range(partRange.a1);

                    // move value and formula (but not the formatting, see below) to top-left corner
                    if (!partRange.startsAt(cellModel.a)) {
                        startRange.rangeMode = ContentRangeMode.SKIP;
                        startRange.fillData = { v: cellModel.v, f: cellModel.f };
                        fillRanges.push(startRange);
                    }

                    // delete values and formulas from all other cells of the range
                    const remainRanges = new RangeArray(partRange.intersect(usedRange)).difference(startRange);
                    for (const remainRange of remainRanges) {
                        remainRange.rangeMode = ContentRangeMode.SKIP;
                        remainRange.fillData = { v: null, f: null };
                        fillRanges.push(remainRange);
                    }
                }
            }, { cycles: count });
        });

        // generate the resulting fill operations
        promise = promise.then(() => this.generateRangeOperations(generator, fillRanges));

        // homogeneous border for each single edge
        promise = promise.then(() => this.iterateArraySliced(borderRanges, borderRange => {
            return this.generateBorderOperations(borderGenerator, borderRange, borderRange.borderAttrs);
        }));

        return promise.done(() => {
            generator.appendOperations(borderGenerator);
            generator.prependOperations(borderGenerator, { undo: true });
        });
    }

    /**
     * Inserts one or more formulas calculating subtotal results into or next
     * to the specified cell range.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {String} funcKey
     *  The resource key of the subtotal function to be inserted into the
     *  formulas.
     *
     * @param {Range} range
     *  The source range used to generate the formulas.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved with the address of the target range
     *  inserted into the generated formulas when all operations have been
     *  generated.
     */
    generateAutoFormulaOperations(generator, funcKey, range) {

        const tokenArray = this.sheetModel.createCellTokenArray();
        const changeSets = [];

        const pushChangeSet = (sourceRange, refAddress) => {

            // DOCS-1653: skip blank ranges
            if (this.areRangesBlank(sourceRange)) { return; }

            // generate the formula expression, and create a token array
            const formula = this.docModel.formulaGrammarOP.generateAutoFormula(this.docModel, funcKey, sourceRange, refAddress);
            tokenArray.parseFormula("op", formula, refAddress);

            // calculate the formula result, this will also return the resulting number format
            const result = tokenArray.interpretFormula('val', refAddress, refAddress);

            // create the change set, and add the number format (except standard formats)
            const changeSet = { address: refAddress.clone(), v: result.value, f: formula };
            const formatCode = result.format ? this.#getNewCellFormatCode(refAddress, result.format) : null;
            if (formatCode) { changeSet.format = formatCode; }

            changeSets.push(changeSet);
        };

        const findNextBlankAddress = (col, row, direction) => {
            const cellIt = this.linearCellEntries(new Address(col, row), direction);
            const blankEntry = itr.find(cellIt, cellEntry => cellEntry.blank);
            return blankEntry?.address ?? null;
        };

        let singleRow = range.singleRow();
        let targetRange = null;
        let startAddress = null;

        if (singleRow) {
            const firstAddress = this.findFirstCell(range, { type: 'value' });
            if (!firstAddress) {
                targetRange = range;
                singleRow = false;
                const firstCol = this.#getAutoFormulaRange(range.a1, Direction.UP)?.range;
                if (!firstCol) { return this.createResolvedPromise(range); }
                range = Range.fromIndexes(firstCol.a1.c, firstCol.a1.r, range.a2.c, range.a2.r);
            }
        }

        if (singleRow) {
            startAddress = findNextBlankAddress(range.a2.c, Math.max(range.a1.r, range.a2.r), Direction.RIGHT);
            if (!startAddress) { return this.createResolvedPromise(range); }

            const contentRange = Range.fromIndexes(range.a1.c, range.a1.r, Math.min(range.a2.c, startAddress.c - 1), range.a2.r);

            pushChangeSet(contentRange, startAddress);
            const targetX = startAddress.c <= range.a2.c ? range.a2.c : range.a2.c + 1;
            targetRange = Range.fromIndexes(range.a1.c, range.a1.r, targetX, range.a2.r);

        } else {

            let emptyRow = -1;
            for (const col of this.#colMatrix.yieldIndexes(range.colInterval())) {
                const addr = findNextBlankAddress(col, range.a1.r, Direction.DOWN);
                if (addr) { emptyRow = Math.max(emptyRow, addr.r); }
            }

            const targetY = range.a2.r + (emptyRow <= range.a2.r ? 0 : 1);
            startAddress = new Address(range.a1.c, Math.max(emptyRow, range.a2.r));
            const sourceRange = Range.fromIndexes(range.a1.c, range.a1.r, range.a1.c, targetY - 1);

            for (const col of this.#colMatrix.yieldIndexes(range.colInterval())) {
                sourceRange.setBoth(col, true);
                pushChangeSet(sourceRange, new Address(col, Math.max(emptyRow, range.a2.r)));
            }
            if (!targetRange) {
                targetRange = Range.fromIndexes(range.a1.c, range.a1.r, range.a2.c, targetY);
            }
        }

        // generate and apply the operations
        return this.generateCellsOperations(generator, changeSets).then(fun.const(targetRange));
    }

    /**
     * Generates the cell operations, and the undo operations, to sort the
     * specified cell range according to one or more columns/rows.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Range} sortRange
     *  The address of the cell range to be sorted, without header cells.
     *
     * @param {boolean} sortVert
     *  The direction in which to sort the cell range (`true` for vertical).
     *
     * @param {SortRules[]} sortRules
     *  The sorting rules to be applied.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved (with the addresses of the ranges that
     *  have really been changed) when all operations have been generated, or
     *  that will be rejected with an object with 'cause' property set to one
     *  of the following error codes:
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateSortOperations")
    generateSortOperations(generator, sortRange, sortVert, sortRules) {
        modelLogger.trace('range=' + sortRange);

        const { sheetModel } = this;
        const { mergeCollection, hlinkCollection, noteCollection, commentCollection } = sheetModel;

        // immediately exit if the range contains any merged cell ranges
        if (mergeCollection.coversAnyMergedRange(sortRange)) {
            return makeRejected('sort:merge:overlap');
        }

        // prepare the data array used for sorting (cell values in order of sort rules)
        const collection = sortVert ? this.#rowCollection : this.#colCollection;
        const intervals = collection.getVisibleIntervals(sortRange.interval(!sortVert));
        let sortLines = Array.from(intervals.indexes(), index => {
            const address = new Address(index, index);
            const values = sortRules.map(sortRule => {
                address.set(sortRule.index, sortVert);
                return this.getValue(address);
            });
            return { fromIndex: index, values };
        });

        // prepare scalar comparator options (empty cells always to the bottom of the range)
        const compareOptions = sortRules.map(sortRule => {
            return { nullMode: sortRule.descending ? CompareNullMode.NULL_MIN : CompareNullMode.NULL_MAX };
        });

        // sort the lines array by cell values
        sortLines.sort((sortLine1, sortLine2) => {
            const length = Math.min(sortLine1.values.length, sortLine2.values.length);
            for (let ai = 0; ai < length; ai += 1) {
                const result = compareScalars(sortLine1.values[ai], sortLine2.values[ai], compareOptions[ai]);
                if (result) { return sortRules[ai].descending ? -result : result; }
            }
        });

        // filter lines that will not change their position
        const indexIter = intervals.indexes();
        sortLines = sortLines.filter(sortLine => {
            sortLine.toIndex = indexIter.next().value;
            return sortLine.fromIndex !== sortLine.toIndex;
        });

        // nothing to do if the sort order does not change at all
        if (sortLines.length === 0) { return jpromise.resolve(); }

        // the builder used to generate the cell operations for all moved cells
        const cellBuilder = new CellOperationBuilder(sheetModel, generator, { skipShared: true });

        // hyperlink ranges to be sorted in the sort range
        const deletedLinkRanges = new RangeArray();
        const movedLinkRanges = new Map/*<string, RangeArray>*/();

        // collect all single-line hyperlink ranges inside the sorting range
        const allLinkRanges = hlinkCollection.getLinkRanges(sortRange, FindMatchType.CONTAIN);
        const singleLinkRanges = allLinkRanges.filter(linkRange => linkRange.singleLine(!sortVert));

        // group hyperlink ranges by line index (according to sorting direction)
        const linkRangeGroups = ary.group(singleLinkRanges, linkRange => linkRange.a1.get(!sortVert));

        // cell notes to be sorted in the sort range
        const notesFrom = new AddressArray();
        const notesTo = new AddressArray();

        // cell comment threads to be sorted in the sort range
        const commentsFrom = new AddressArray();
        const commentsTo = new AddressArray();

        // a private undo generator for restoring deleted contents at the end
        const undoGenerator = sheetModel.createOperationGenerator({ applyImmediately: true });

        // convert all shared formulas in the sorted range to regular formula cells
        let promise = this.generateBreakSharedFormulaOperations(undoGenerator, sortRange);

        // process the sort range, collect all contents to be sorted
        promise = promise.then(() => {

            // visit all cells (also in hidden columns/rows) in the sort line
            for (const sortLine of sortLines) {

                // process cell contents and comments for each cell in the sort line
                for (const index of sortRange.indexes(sortVert)) {

                    // initialize source/dest addresses
                    const fromAddress = Address.fromDir(sortVert, index, sortLine.fromIndex);
                    const toAddress = Address.fromDir(sortVert, index, sortLine.toIndex);

                    // create and collect change sets for moved cells
                    const cellModel = this.getCellModel(fromAddress);
                    const changeSet = cellModel ? {
                        v: cellModel.v,
                        f: cellModel.getFormula("op", toAddress),
                        s: this.#resolveStyleId(cellModel)
                    } : { u: true };
                    cellBuilder.insertCell(toAddress, changeSet);

                    // collect start and end addresses of all moved cell notes
                    const noteModel = noteCollection.getByAddress(fromAddress);
                    if (noteModel) {
                        notesFrom.push(fromAddress);
                        notesTo.push(toAddress);
                    }

                    // collect start and end addresses of all moved cell threaded comments
                    const commentModel = commentCollection.getByAddress(fromAddress);
                    if (commentModel) {
                        commentsFrom.push(fromAddress);
                        commentsTo.push(toAddress);
                        // the comment has a fallback note, its position needs to be updated too
                        notesFrom.push(fromAddress);
                        notesTo.push(toAddress);
                    }
                }

                // process all hyperlinks in the sort line
                const fromLinkRanges = linkRangeGroups.get(sortLine.fromIndex);
                if (fromLinkRanges) {
                    deletedLinkRanges.append(fromLinkRanges);
                    fromLinkRanges.forEach(fromLinkRange => {
                        const toLinkRange = fromLinkRange.clone().setBoth(sortLine.toIndex, !sortVert);
                        map.upsert(movedLinkRanges, fromLinkRange.url, () => new RangeArray()).push(toLinkRange);
                    });
                }
            }
        });

        // delete sorted hyperlinks from old cell positions (via undoGenerator, to restore them at the end)
        promise = promise.then(() => {
            if (!deletedLinkRanges.empty()) {
                hlinkCollection.generateHlinkOperations(undoGenerator, deletedLinkRanges, '');
            }
        });

        // create the cell operation that sorts the specified range
        promise = promise.then(() => {
            generator.appendOperations(undoGenerator);
            return cellBuilder.finalizeOperations();
        });

        // create the hyperlink operations for single-cell hyperlinks
        promise = promise.then(() => {
            return this.asyncForEach(movedLinkRanges, ([url, ranges]) => {
                hlinkCollection.generateHlinkOperations(generator, ranges, url);
            });
        });

        // create the drawing operations that relocate the cell comments
        promise = promise.then(() => {
            if (notesFrom.length > 0) {
                return noteCollection.generateMoveAnchorOperations(generator, notesFrom, notesTo);
            }
        });

        // create the drawing operations that relocate the cell comments
        promise = promise.then(() => {
            if (commentsFrom.length > 0) {
                return commentCollection.generateMoveAnchorOperations(generator, commentsFrom, commentsTo);
            }
        });

        // append the operations needed to restore deleted contents at the end of all other undo operations
        return promise.done(() => {
            generator.appendOperations(undoGenerator, { undo: true });
        });
    }

    /**
     * Generates the operations to update all formula expressions in the
     * document, after cells that have been cut from another sheet, and pasted
     * within this sheet.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {number} sourceSheet
     *  The index of the sheet containing the cells that have been cut.
     *
     * @param {Range} sourceRange
     *  The address of the cell range that has been cut.
     *
     * @param {Range} targetRange
     *  The address of the cell range the cells have been pasted into.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all operations have been generated.
     */
    generateCutPasteOperations(generator, sourceSheet, sourceRange, targetRange) {

        // update the formula expressions for all sheets in the document
        const builder = new OperationBuilder(this.docModel);
        const updateTask = new CutPasteUpdateTask(sourceSheet, sourceRange, targetRange, { afterChange: true });
        const promise = this.docModel.generateUpdateTaskOperations(builder, updateTask);

        // prepend the undo operations, and append the change operations
        return promise.done(() => {
            // TODO: do not flush directly into `generator` as long as it is in "applyImmediately" mode
            const result = builder.flushOperations();
            generator.appendOperations(result.generator);
            generator.prependOperations(result.generator, { undo: true });
        }).always(() => {
            builder.destroy();
        });
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * contents from the passed collection into this collection.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "copySheet".
     *
     * @param {CellCollection} fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyCopySheetOperation(context, fromCollection) {

        // clone the shared formula models of the source collection
        this.sharedFormulas = new SharedFormulaStorage(this.sheetModel, fromCollection.sharedFormulas);

        // clone the matrix ranges of the source collection
        this.#matrixRangeSet = RangeSet.from(fromCollection.#matrixRangeSet, range => range.clone());

        // clone the cell models of the source collection
        const options = { skipFormula: true };
        for (const cellModel of fromCollection.#modelMap.values()) {
            const sharedModel = cellModel.sf ? this.sharedFormulas.get(cellModel.sf.index) : undefined;
            this.#implInsertCellModel(cellModel.cloneFor(this.sheetModel, sharedModel), options);
        }

        // update the used range, and emit the "change:usedarea" event
        this.updateUsedRange(context);
    }

    /**
     * Callback handler for the document operation "changeCells". Changes the
     * contents and auto-styles of the cells in this collection, and emits a
     * "change:cells" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "changeCells".
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyChangeCellsOperation(context) {

        // optimizations for import process
        const importing = context.importing;
        // collect the change flags for each single cell in a map
        const changeFlagsMap = importing ? null : new Map/*<string, CellChangeFlags>*/();

        // inserts or updates the passed change flags into the map
        const updateChangeFlags = importing ? fun.undef : changeFlags => {
            map.update(changeFlagsMap, changeFlags.address.key, oldFlags => {
                if (!oldFlags) { return changeFlags; }
                oldFlags.model ||= changeFlags.model;
                oldFlags.value ||= changeFlags.value;
                oldFlags.formula ||= changeFlags.formula;
                oldFlags.style ||= changeFlags.style;
                return oldFlags;
            });
        };

        // process all entries in the contents dictionary
        dict.forEach(context.getDict('contents'), (jsonData, jsonKey) => {

            // parse the key to a cell range address (throws on error)
            const range = context.parseRange(jsonKey);

            // convert scalar to data object
            if (isScalar(jsonData)) { jsonData = { v: jsonData }; }
            context.ensure(is.dict(jsonData), 'cell data must be an object or a scalar');

            // explicitly delete existing cell models, if the property `u` is set
            if (jsonData.u === true) {
                // the range MUST be iterated in reversed order, to not disturb the cell matrix iterators while deleting the models
                for (const cellEntry of this.#yieldCellsForRange(range, { type: 'defined', reverse: true })) {
                    updateChangeFlags(this.#deleteCellModel(cellEntry.address));
                }
            }

            // create the change set expected by the method `updateCellModel`
            const changeSet = this.#createChangeSet(context, jsonData, range);
            if (!changeSet) { return; }

            // create/update the cell models, collect flags of all changed cells
            for (const address of range.addresses()) {
                updateChangeFlags(this.#updateCellModel(address, changeSet, importing));
            }
        });

        // refresh bounding ranges and reference addresses of dirty shared formulas
        this.sharedFormulas.refresh({
            // collect all formula cells that are part of a shared formula with moved anchor address or changed formula
            changed: sharedModel => {
                for (const address of sharedModel.addressSet) {
                    updateChangeFlags({ address, formula: true });
                }
            }
        });

        // notify change event listeners
        if (changeFlagsMap?.size) {
            const event = {
                valueCells: new AddressArray(),
                formulaCells: new AddressArray(),
                styleCells: new AddressArray(),
                allCells: new AddressArray()
            };
            changeFlagsMap.forEach(changeFlags => {
                const address = changeFlags.address;
                if (changeFlags.value) { event.valueCells.push(address); }
                if (changeFlags.formula) { event.formulaCells.push(address); }
                if (changeFlags.style) { event.styleCells.push(address); }
                if (changeFlags.value || changeFlags.formula || changeFlags.style) {
                    event.allCells.push(address);
                }
            });
            event.results = context.optStr('scope') === 'filter';
            this.trigger("change:cells", this.createSheetEvent(event, context));
        }

        // update the used range, and emit the "change:usedarea" event
        this.updateUsedRange(context);
    }

    /**
     * Callback handler for the document operation "moveCells". Moves all
     * affected cell models, and emits a "move:cells" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "moveCells".
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyMoveCellsOperation(context) {

        // the range to be inserted or cleared
        const range = context.getRange('range');
        // the move direction
        const direction = context.getEnum('dir', Direction);
        // the address transformer containing all needed data
        const transformer = AddressTransformer.fromRange(this.#addressFactory, range, direction);

        // transform the cell models, notify listeners
        this.implTransformCells(context, transformer);
    }

    // protected methods ------------------------------------------------------

    /**
     * Post-processing of the cell collection, after all import operations have
     * been applied successfully.
     *
     * _Attention:_ Called from the application import process. MUST NOT be
     * called from external code.
     *
     * @returns {Promise<void>}
     *  A promise that will be resolved when the cell collection has been
     *  post-processed successfully; or rejected when any error has occurred.
     */
    async postProcessImport() {

        // create the token arrays of all formula cells
        await this.asyncForEach(this.#modelMap.values(), cellModel => {
            cellModel.updateTokenArray(this.sheetModel);
        });
    }

    /**
     * Implementation helper for the operation "moveCells". Will also be called
     * from the implementations of the "move intervals" operations
     * ("insertColumns", "deleteColumns", "insertRows", and "deleteRows").
     *
     * @param {SheetOperationContext} context
     *  An operation wrapper, just used for event handling and exceptions.
     *  The contents of the wrapped operations will not be used.
     *
     * @param {AddressTransformer} transformer
     *  The prepared address transformer to be applied to the cell models.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is
     *  missing in the operation.
     */
    implTransformCells(context, transformer) {

        // whether to move the cells through columns
        const columns = transformer.columns;

        // first, delete all existing cell models in the deleted ranges
        const blankRanges = transformer.createBandRanges(transformer.deleteIntervals);
        for (const blankRange of blankRanges) {
            // the cells MUST be iterated in reversed order, to not disturb the cell matrix iterators while deleting the models
            for (const cellEntry of this.#yieldCellsForRange(blankRange, { type: 'defined', reverse: true })) {
                this.#implRemoveCellModel(cellEntry.model);
                cellEntry.model.destroy();
            }
        }

        // move all cell models into a temporary array (prevents overwriting existing models)
        const tempModelsStore = [];

        // process all remaining cell ranges that will be moved by the transformer
        transformer.moveFromIntervals.forEach((moveFromInterval, index) => {

            // push a new entry into the temporary array
            const storeEntry = {};
            tempModelsStore.push(storeEntry);

            // the source range containing cell models that need to be moved (may be empty)
            const fromRange = transformer.createBandRange(moveFromInterval);
            // the signed distance to modify the cell address index
            storeEntry.moveDist = transformer.moveToIntervals[index].first - moveFromInterval.first;

            // column matrix does not need to be updated, when moving cells up/down;
            // or when moving the cells in all rows of the entire used area to the left/right
            const usedRowInterval = columns ? this.#rowMatrix.getUsedInterval() : null;
            storeEntry.skipColMatrix = !usedRowInterval || fromRange.rowInterval().contains(usedRowInterval);

            // row matrix does not need to be updated, when moving cells to left/right;
            // or when moving the cells in all columns of the entire used area up/down
            const usedColInterval = !columns ? this.#colMatrix.getUsedInterval() : null;
            storeEntry.skipRowMatrix = !usedColInterval || fromRange.colInterval().contains(usedColInterval);

            // Remove ALL cells to be moved from the containers, before the addresses will be changed. The cells
            // MUST be iterated in reversed order, to not disturb the cell matrix iterator while removing the
            // models. Cell addresses MUST NOT be adjusted inside this loop, this would invalidate the index order
            // in the cell matrix.
            storeEntry.cellModels = Array.from(this.#yieldCellsForRange(fromRange, { type: 'defined', reverse: true }), cellEntry => {
                const cellModel = cellEntry.model;
                this.#implRemoveCellModel(cellModel, storeEntry);
                return cellModel;
            });
        });

        // insert all moved cell models back into the containers
        for (const storeEntry of tempModelsStore) {
            const moveDist = storeEntry.moveDist;
            for (const cellModel of storeEntry.cellModels) {
                // adjust the cell address before inserting the cell into the containers
                cellModel.a.move(moveDist, columns);
                this.#implInsertCellModel(cellModel, storeEntry);
            }
        }

        // refresh bounding ranges and reference addresses of dirty shared formulas
        this.sharedFormulas.refresh();

        // notify move event listeners
        this.trigger("move:cells", this.createSheetEvent({ transformer }, context));

        // update the used range, and emit the "change:usedarea" event
        this.updateUsedRange(context);
    }

    // protected methods ------------------------------------------------------

    /**
     * Implementation of abstract base method. Calculates the used range of
     * this collection (the bounding range containing all defined cells).
     */
    /*protected override*/ calculateUsedRange() {
        const usedCols = this.#colMatrix.getUsedInterval();
        const usedRows = this.#rowMatrix.getUsedInterval();
        return (usedCols && usedRows) ? Range.fromIntervals(usedCols, usedRows) : undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the style identifier of the auto-style referred by the passed
     * cell model.
     */
    #resolveStyleId(cellModel, useUid) {
        return useUid ? cellModel.suid : this.#autoStyles.resolveStyleId(cellModel.suid);
    }

    /**
     * Returns whether the auto-style of the passed cell model differs from the
     * default auto-style according to the column/row formatting of the
     * respective cell.
     */
    #styleDiffersFromDefault(cellModel) {
        return cellModel.suid !== this.getDefaultStyleId(cellModel.a, { useUid: true });
    }

    /**
     * Inserts the address of the passed cell model into the model of a shared
     * formula, if the cell refers to a shared formula; or registers the
     * bounding range of a matrix formula.
     */
    #implRegisterFormulaCell(cellModel) {
        // add the cell address and formula expression to the shared formula model
        cellModel.registerSharedFormula();
        // add range of matrix formula to the range set
        if (cellModel.mr) { this.#matrixRangeSet.add(cellModel.mr.clone()); }
    }

    /**
     * Removes the address of the passed cell model from the model of a shared
     * formula, if the cell refers to a shared formula; or removes the bounding
     * range of a matrix formula.
     */
    #implUnregisterFormulaCell(cellModel) {
        // remove the cell address from the shared formula model
        cellModel.unregisterSharedFormula();
        // remove range of matrix formula from the range set
        if (cellModel.mr) { this.#matrixRangeSet.delete(cellModel.mr); }
    }

    /**
     * Inserts the passed new cell model into the internal containers.
     */
    #implInsertCellModel(cellModel, options) {
        this.#modelMap.set(cellModel.key, cellModel);
        if (!options?.skipColMatrix) { this.#colMatrix.insertModel(cellModel); }
        if (!options?.skipRowMatrix) { this.#rowMatrix.insertModel(cellModel); }
        if (!options?.skipFormula) { this.#implRegisterFormulaCell(cellModel); }
    }

    /**
     * Removes the passed new cell model from the internal containers.
     */
    #implRemoveCellModel(cellModel, options) {
        if (!options?.skipFormula) { this.#implUnregisterFormulaCell(cellModel); }
        if (!options?.skipColMatrix) { this.#colMatrix.removeModel(cellModel); }
        if (!options?.skipRowMatrix) { this.#rowMatrix.removeModel(cellModel); }
        this.#modelMap.delete(cellModel.key);
    }

    /**
     * Creates a new blank cell model for this collection. The collection MUST
     * NOT contain an existing cell model at the specified address.
     *
     * @param {Address} address
     *  The address of the cell. The cell MUST be undefined.
     *
     * @returns {CellModel}
     *  A new blank cell model.
     */
    #createCellModel(address) {
        const cellModel = new CellModel(address);
        this.#implInsertCellModel(cellModel);
        return cellModel;
    }

    /**
     * Removes a cell model from this collection.
     *
     * @param {Address} address
     *  The address of the cell.
     *
     * @returns {CellChangeFlags}
     *  A flag set containing change flags for modified properties of the cell.
     *  A value of `true` means that the respective cell property was different
     *  from the default state of the undefined cell (non-blank value, existing
     *  formula expression, auto-style different from the default column/row
     *  auto-style).
     */
    #deleteCellModel(address) {

        // create the change flags
        const changeFlags = { address, model: null, value: false, formula: false, style: false };

        // delete an existing cell model from the containers, and destroy it
        const cellModel = this.getCellModel(address);
        if (cellModel) {
            changeFlags.value = cellModel.v !== null;
            changeFlags.formula = cellModel.isAnyFormula();
            changeFlags.style = this.#styleDiffersFromDefault(cellModel);
            this.#implRemoveCellModel(cellModel);
            cellModel.destroy();
        }

        return changeFlags;
    }

    /**
     * Helper function for applying the operation "changeCells". Converts the
     * passed raw JSON cell data of the operation to a cell change set as
     * expected by the method `updateCellModel`.
     *
     * @param {SheetOperationContext} context
     *  The operation context representing the operation "changeCells".
     *
     * @param {Dict} jsonData
     *  The JSON cell data to be parsed. Must be a dictionary! This method
     *  throws an `OperationError` if the contents of the dictionary are
     *  invalid.
     *
     * @param {Range} range
     *  The cell range to be modified.
     *
     * @returns {Opt<CellChangeSet>}
     *  The cell changeset intended to be passed to the private method
     *  `updateCellModel`; or `undefined` if nothing needs to be changed.
     */
    #createChangeSet(context, jsonData, range) {

        // the cell change set
        const changeSet = dict.create();

        // update cell value (special handling for error codes needed)
        if ('v' in jsonData) {
            context.ensure(!('e' in jsonData), 'unexpected cell error code');
            context.ensure(isScalar(jsonData.v), 'invalid cell value type');
            changeSet.v = jsonData.v;
        } else if ('e' in jsonData) {
            context.ensure(is.string(jsonData.e), 'invalid cell error code');
            changeSet.v = this.docModel.formulaGrammarOP.getErrorCode(jsonData.e) || ErrorCode.NA;
        }

        // update cell formula
        if ('f' in jsonData) {
            if (jsonData.f === null) {
                changeSet.f = null;
            } else {
                context.ensure(is.string(jsonData.f) && (jsonData.f.length > 0), 'invalid formula expression');
                changeSet.f = jsonData.f;
            }
        }

        // index of a shared formula
        if ('si' in jsonData) {
            if (jsonData.si === null) {
                changeSet.si = null;
            } else {
                context.ensure(is.number(jsonData.si) && (jsonData.si >= 0), 'invalid shared formula identifier');
                changeSet.si = Math.floor(jsonData.si);
            }
        }

        // check the bounding range of a shared formula (but do not use it, it will be calculated dynamically)
        if (('sr' in jsonData) && (jsonData.sr !== null)) {
            context.ensure(range.single(), 'shared formula range cannot be set to multiple cells');
            const boundRange = is.string(jsonData.sr) && this.#addressFactory.parseRange(jsonData.sr);
            context.ensure(boundRange, 'invalid shared formula range');
        }

        // bounding range of a matrix formula
        if ('mr' in jsonData) {
            if (jsonData.mr === null) {
                changeSet.mr = null;
            } else {
                context.ensure(range.single(), 'matrix formula range cannot be set to multiple cells');
                changeSet.mr = is.string(jsonData.mr) ? this.#addressFactory.parseRange(jsonData.mr) : undefined;
                context.ensure(changeSet.mr, 'invalid matrix formula range');
            }
        }

        // dynamic flag of a matrix formula
        if ('md' in jsonData) {
            context.ensure(is.boolean(jsonData.md), 'invalid dynamic matrix flag');
            changeSet.md = jsonData.md;
        }

        // update cell auto-style
        if ('s' in jsonData) {
            context.ensure(is.string(jsonData.s), 'invalid auto-style identifier');
            const styleUid = this.#autoStyles.resolveStyleUid(jsonData.s);
            context.ensure(styleUid !== null, 'invalid auto-style identifier "%s"', jsonData.s);
            changeSet.s = styleUid;
        }

        // nothing to do if change set remains empty
        if (is.empty(changeSet)) { return undefined; }

        // ODF import: warn but do not fail for formatted cells without contents
        // (workaround for bug 48015: use half of row size to ignore default row formatting)
        if (this.docApp.isODF() && (range.cells() >= this.#addressFactory.colCount / 2) && dict.singleKey(changeSet) && ('s' in changeSet)) {
            modelLogger.warn('$badge{CellCollection} createChangeSet: Overflow error! Skipping formatting of ' + range.cells() + ' cells.');
            return undefined;
        }

        // fail safety: check number of changed cells (prevent freezing browser)
        context.ensure(range.cells() <= MAX_FILL_CELL_COUNT, 'too many cells');
        return changeSet;
    }

    /**
     * Updates the value, formula, and auto-style of a cell model.
     *
     * @param {Address} address
     *  The address of the cell to be updated.
     *
     * @param {CellChangeSet} changeSet
     *  The properties of the cell to be changed.
     *
     * @param {boolean} [importing=false]
     *  If set to `true`, special behavior for the document import process will
     *  be actiavted (for formula cells, the token array of the cell model will
     *  NOT be created).
     *
     * @returns {CellChangeFlags}
     *  A flag set containing change flags for modified properties of the cell,
     *  and the additional property `model` containing the updated cell model.
     */
    #updateCellModel(address, changeSet, importing) {

        // the cell model to be updated
        let cellModel = this.getCellModel(address);
        // the old auto-style (default to column/row auto style)
        const oldStyleUid = cellModel ? cellModel.suid : this.getDefaultStyleId(address, { useUid: true });
        // the changed flags
        const changeFlags = { address, model: null, value: false, formula: false, style: false };

        // small helper function that creates the cell model on demand
        const getCellModel = () => (cellModel ??= this.#createCellModel(address));

        // update cell value
        if (('v' in changeSet) && getCellModel().setValue(changeSet.v)) {
            changeFlags.value = true;
        }

        // update cell formula
        if (('f' in changeSet) && getCellModel().setFormula(this.sheetModel, changeSet.f, importing)) {
            changeFlags.formula = true;
        }

        // remove the cell from the shared formula map and matrix range set here, and add it below after updating
        // all related properties for shared formulas and matrix range
        if (cellModel) { this.#implUnregisterFormulaCell(cellModel); }

        // update shared formula settings
        if ('si' in changeSet) {
            const si = changeSet.si;
            if ((si === null) && cellModel && cellModel.sf) {
                delete cellModel.sf;
                changeFlags.formula = true;
            } else if (is.number(si) && (!cellModel || !cellModel.sf || (cellModel.sf.index !== si))) {
                getCellModel().sf = this.sharedFormulas.upsert(si, address);
                changeFlags.formula = true;
            }
        }

        // update the bounding range of a matrix formula
        if ('mr' in changeSet) {
            // DOCS-4742: broken ODS files may contain matrix range without formula expression
            if (changeSet.mr && !cellModel?.f) {
                modelLogger.warn(`$badge{CellCollection} updateCellModel: ignoring matrix range without formula expression in ${this.sheetModel.getName()}!${address}`);
            } else if (getCellModel().setMatrixRange(changeSet.mr)) {
                changeFlags.formula = true;
            }
        }

        // update the dynamic flag of a matrix formula
        if (('md' in changeSet) && getCellModel().setDynamicMatrix(changeSet.md)) {
            changeFlags.formula = true;
        }

        // if the cell is part of a shared formula, add the address to the internal maps
        if (cellModel) { this.#implRegisterFormulaCell(cellModel); }

        // update cell auto-style
        if ('s' in changeSet) {
            getCellModel().suid = changeSet.s;
            changeFlags.style = oldStyleUid !== changeSet.s;
        }

        // update the parsed number format (also for new cell models without style change)
        if (cellModel && (changeFlags.style || !cellModel.pf)) {
            cellModel.updateParsedFormat(this.#autoStyles);
        }

        // update the display string, if value or attributes have changed
        // (performance: nothing to do if formula has changed, but the result has not)
        if (changeFlags.value || changeFlags.style) {
            cellModel.updateDisplayString(this.#numberFormatter);
        }

        // delete the cached column/row subtotal results, if the value has changed
        if (changeFlags.value) {
            this.#colMatrix.clearSubtotalResult(cellModel);
            this.#rowMatrix.clearSubtotalResult(cellModel);
        }

        // return the updated cell model with the change flags
        changeFlags.model = cellModel;
        return changeFlags;
    }

    /**
     * Creates an iterator that visits all or specific cells contained in the
     * passed column and row intervals. The cells will be visited row-by-row
     * across all passed column intervals.
     *
     * @param {IntervalSource} colIntervals
     *  An array of column intervals, or a single column interval.
     *
     * @param {IntervalSource} rowIntervals
     *  An array of row intervals, or a single row interval.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {CellType} [options.type=ALL]
     *    Specifies which type of cells will be covered by the iterator.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the row intervals AND the column intervals in each
     *    row will be visited in reversed order.
     *
     * @yields {BaseCellEntry}
     *  The cell entries.
     */
    *#yieldCellsForIntervals(colIntervals, rowIntervals, options) {

        // which cell entries (by type) will be visited
        const type = options?.type ?? 'all';
        // whether to iterate in reversed order
        const reverse = options?.reverse;
        // the matcher predicate function for the cell type
        const matcher = getCellTypePredicate(type);

        // use the column or row matrix to visit the existing cell models faster
        // (test by checking whether the matcher does not accept missing cell models)
        if (!matcher(null)) {

            // the model iterator created by the cell matrix
            // TODO: use a parallel iterator in the column matrix for a small number of columns
            const cellIterator = (colIntervals.size() === 1) ?
                this.#colMatrix.yieldModels(colIntervals, rowIntervals, reverse) :
                this.#rowMatrix.yieldModels(rowIntervals, colIntervals, reverse);

            // filter for specific cell types, and transform cell models to their addresses
            for (const cellModel of cellIterator) {
                if (matcher(cellModel)) {
                    yield {
                        address: cellModel.a.clone(),
                        model: cellModel,
                        value: cellModel.v,
                        blank: cellModel.isBlank()
                    };
                }
            }

        } else {

            rowIntervals = IntervalArray.cast(rowIntervals);
            colIntervals = IntervalArray.cast(colIntervals);

            // visit all rows and columns in the intervals
            for (const row of rowIntervals.indexes({ reverse })) {
                for (const col of colIntervals.indexes({ reverse })) {
                    const address = new Address(col, row);
                    const cellModel = this.getCellModel(address);
                    yield {
                        address,
                        model: cellModel,
                        value: getModelValue(cellModel),
                        blank: isBlankCell(cellModel)
                    };
                }
            }
        }
    }

    /**
     * Creates an iterator that visits all or specific cells contained in the
     * passed cell ranges.
     *
     * @param {Range} range
     *  The address of the cell range to be visited.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {CellType} [options.type=ALL]
     *    Specifies which type of cells will be covered by the iterator.
     *  - {boolean|"columns"|"rows"} [options.visible=false]
     *    Specifies how to handle hidden columns and rows.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the ranges AND the cell addresses in each range
     *    will be visited in reversed order.
     *
     * @returns {IterableIterator<BaseCellEntry>}
     *  The new cell iterator.
     */
    #yieldCellsForRange(range, options) {

        // whether to visit visible columns and/or rows only
        const visible = getVisibleMode(options);

        // the column intervals of the range to be visited (all columns, or visible columns only)
        const colIntervals = visible.cols ? this.#colCollection.getVisibleIntervals(range.colInterval()) : range.colInterval();
        // the row intervals of the range to be visited (all rows, or visible rows only)
        const rowIntervals = visible.rows ? this.#rowCollection.getVisibleIntervals(range.rowInterval()) : range.rowInterval();

        // create an iterator that visits the cells row-by-row through the column intervals
        return this.#yieldCellsForIntervals(colIntervals, rowIntervals, options);
    }

    /**
     * Creates an iterator that visits all or specific cells contained in the
     * passed cell ranges.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {CellType} [options.type=ALL]
     *    Specifies which type of cells will be covered by the iterator.
     *  - {boolean|"columns"|"rows"} [options.visible=false]
     *    Specifies how to handle hidden columns and rows.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the ranges AND the cell addresses in each range
     *    will be visited in reversed order.
     *
     * @yields {CellEntry}
     *  The cell entries.
     */
    *#yieldCellsForRanges(ranges, options) {

        // visit all cell ranges in the passed source
        for (const [index, range] of ary.entries(RangeArray.cast(ranges), { reverse: options?.reverse })) {

            // visit all cells in the current cell range
            for (const cellEntry of this.#yieldCellsForRange(range, options)) {
                cellEntry.orig = range;
                cellEntry.index = index;
                yield cellEntry;
            }
        }
    }

    /**
     * Returns the new format code for a cell to be applied while changing the
     * contents of that cell.
     *
     * @param {Address} address
     *  The address of the cell to be changed.
     *
     * @param {ParsedFormat} parsedFormat
     *  The parsed format intended to be applied for the cell, e.g. as received
     *  after parsing an edit string, or after calculating the result of a
     *  formula.
     *
     * @returns {string|null}
     *  The format code contained in the parsed format, if it needs to be
     *  applied to the specified cell; or null, if the cell's number format
     *  will be retained.
     */
    #getNewCellFormatCode(address, parsedFormat) {

        // ignore the standard number formats
        if (parsedFormat.isStandard()) { return null; }

        // ignore changes inside any of the date/time categories (e.g. do not change a date cell to a time cell)
        const oldParsedFormat = this.getParsedFormat(address);
        if (parsedFormat.hasEqualCategory(oldParsedFormat, { anyDateTime: true })) { return null; }

        // return the format code of the parsed format on success
        return parsedFormat.formatCode;
    }

    /**
     * Returns an object with various flags specifying how a cell will be
     * changed according to the passed cell content data.
     *
     * @param {CellChangeSet} changeSet
     *  The cell change set to be examined.
     *
     * @returns {Object}
     *  A result object with the following properties:
     *  - {boolean} undefine
     *    Whether the cell will be undefined (physically deleted from this
     *    collection).
     *  - {boolean} blank
     *    The cell will become blank (value and formula will be removed).
     *  - {boolean} value
     *    The cell value will be changed, and the cell may not be blank
     *    afterwards (this flag will only be set, if 'blank' is false).
     *  - {boolean} formula
     *    The cell formula will be changed, or the cell formula will be deleted
     *    without resulting in a blank cell (this flag will only be set, if
     *    "blank" is false).
     *  - {boolean} style
     *    The cell formatting attributes, or the auto-style will be changed.
     */
    #getChangeFlags(changeSet) {

        // whether the cell model will be deleted completely
        const undefine = changeSet.u === true;
        // whether the cell value becomes blank (cell deleted, or value and formula removed)
        const blank = undefine || ((changeSet.v === null) && (changeSet.f === null));

        return {
            undefine,
            blank,
            value: !blank && ('v' in changeSet),
            formula: !blank && (('f' in changeSet) || ('si' in changeSet) || ('sr' in changeSet)),
            style: !undefine && (('s' in changeSet) || ('a' in changeSet) || ('format' in changeSet) || ('table' in changeSet))
        };
    }

    /**
     * Tags the passed cell range addresses with an additional property
     * `rangeMode` specifying how to process the cell formatting of the cell
     * range.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns {RangeArray}
     *  An array with copies of all cell ranges, with the additional property
     *  `rangeMode` attached to each range copy, as expected e.g. by the method
     *  `generateCellsOperations`. The row ranges will be split into row ranges
     *  covering rows with and without active custom auto-styles.
     */
    #getTaggedRanges(ranges) {

        // group the ranges into column ranges, row ranges, and other cell ranges
        const rangeGroups = this.#addressFactory.getRangeGroups(ranges);
        // the resulting ranges to be filled, with additional 'rangeMode' property
        const taggedRanges = new RangeArray();
        // DOCS-4185: restrict to visible cells, if the sheet contains a filtered table *anywhere*
        const iterOptions = this.sheetModel.isFiltered() ? { visible: true } : undefined;

        // Split row ranges into rows with and without custom auto-style. In rows without custom
        // auto-style, the new formatting attributes must be merged with column default formatting
        // (range mode `ROW_MERGE`). Otherwise, only existing cells need to be processed in rows
        // that already contain an active custom auto-style (range mode ROW_DEF).
        if (rangeGroups.rowRanges) {

            // convert the ranges to row intervals, forward all existing fill data objects
            const rowIntervals = IntervalArray.map(rangeGroups.rowRanges, range => {
                const interval = range.rowInterval();
                interval.rangeMode = range.rangeMode;
                interval.fillData = range.fillData;
                return interval;
            });

            // split the rows into formatted and unformatted ranges, forward all existing fill data objects
            // DOCS-4185: reduce to visible rows, if the sheet contains a filtered table
            for (const { interval, style, origInterval } of this.#rowCollection.styleIntervals(rowIntervals, iterOptions)) {
                const taggedRange = this.#addressFactory.getRowRange(interval);
                taggedRange.rangeMode = origInterval.rangeMode || ((style === null) ? ContentRangeMode.ROW_MERGE : ContentRangeMode.ROW_DEF);
                taggedRange.fillData = origInterval.fillData;
                taggedRanges.push(taggedRange);
            }
        }

        // Add the column ranges. New formatting attributes must be merged over active custom
        // auto-styles of the rows (range mode `COL_MERGE`).
        if (rangeGroups.colRanges) {
            for (const colRange of rangeGroups.colRanges) {
                for (const colInterval of this.#colCollection.intervals(colRange.colInterval(), iterOptions)) {
                    const taggedRange = this.#addressFactory.getColRange(colInterval);
                    taggedRange.rangeMode = colRange.rangeMode || ContentRangeMode.COL_MERGE;
                    taggedRanges.push(taggedRange);
                }
            }
        }

        // Add the remaining ranges not covering entire columns or rows.
        if (rangeGroups.cellRanges) {
            for (const cellRange of rangeGroups.cellRanges) {
                for (const rowInterval of this.#rowCollection.intervals(cellRange.rowInterval(), iterOptions)) {
                    for (const colInterval of this.#colCollection.intervals(cellRange.colInterval(), iterOptions)) {
                        const taggedRange = Range.fromIntervals(colInterval, rowInterval);
                        taggedRange.fillData = cellRange.fillData;
                        taggedRanges.append(taggedRange);
                    }
                }
            }
        }

        return taggedRanges;
    }

    /**
     * Returns the range addresses of the adjacent cells of all passed cell
     * ranges.
     */
    #getAdjacentRanges(ranges, columns, leading) {

        // returns whether the passed range has an outer adjacent range (i.e. is not at sheet border)
        const maxIndex = this.#addressFactory.getMaxIndex(columns);
        const checkRange = leading ?
            range => range.getStart(columns) > 0 :
            range => range.getEnd(columns) < maxIndex;

        // returns a range with a single column/row depending on direction and position
        const makeRange = leading ?
            range => range.lineRange(columns, -1) :
            range => range.lineRange(columns, range.size(columns));

        return RangeArray.map(ranges, range => checkRange(range) ? makeRange(range) : null);
    }

    /**
     * Generates the cell operations, and the undo operations, to change the
     * contents for equally formatted or filled cell ranges in the sheet, and
     * optionally the appropriate hyperlink operations.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeArray} taggedRanges
     *  The cell ranges to generate operations for. Each range address in this
     * array may contain the following additional properties:
     *  - {string} [rangeMode=ContentRangeMode.CELL_MERGE]
     *    Specifies which cells will be processed in the range. Usually, the
     *    method `getTaggedRanges` should be used to create this property. See
     *    method `generateCellsOperations` for more details.
     *  - {object} [fillData]
     *    Additional formatting settings for the range. These data objects will
     *    be collected in an array, and will be passed to the callback function
     *    (see below).
     *
     * @param {Function} createChangeSetFn
     *  A callback function that will be invoked for each cell range of the
     *  partition of the passed cell ranges. Receives the fill data objects
     *  extracted from the original ranges covered by the current partition
     *  range as simple array in the first parameter. MUST return the contents
     *  change set for the range to be passed to the cell operations builder.
     *
     * @param {object} [options]
     *  Configuration options for the cell operations builder that will be
     *  passed to the callback function. See class `CellOperationBuilder` for
     *  details.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil with the addresses of the ranges that have
     *  really been changed when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{CellCollection} generateTaggedRangeOperations")
    _generateTaggedRangeOperations(generator, taggedRanges, createChangeSetFn, options) {
        modelLogger.trace('ranges=' + taggedRanges);

        // early exit for empty arrays
        if (taggedRanges.empty()) { return this.createResolvedPromise(new RangeArray()); }

        // do not waste time to collect the fill data of the original ranges, if the callback does not use them
        const collectFillData = createChangeSetFn.length > 0;

        // process the partition set of the passed tagged ranges
        const changeSets = taggedRanges.partition().map(partRange => {

            // determine the range mode to be used for the current range
            const rangeMode = partRange.coveredBy.reduce((rangeMode, taggedRange) => {
                return mergeRangeMode(rangeMode, taggedRange.rangeMode || ContentRangeMode.CELL_MERGE);
            }, ContentRangeMode.COL_MERGE);

            // collect the fill data of all tagged source ranges covered by the current partition range
            // (performance optimization: do not collect fill data, if callback function does not use them)
            const fillDataArray = collectFillData ? partRange.coveredBy.map(range => range.fillData).filter(Boolean) : null;

            // create the change set for the current range
            return { ...createChangeSetFn(fillDataArray), rangeMode, address: partRange };
        });

        // generate the cell operations from the change sets
        return this.generateCellsOperations(generator, changeSets, options);
    }

    /**
     * Generates the operations for modifying entire column ranges, entire row
     * ranges, and single cell ranges at once. Takes care about correct order
     * of undo operations for the different types of cell ranges.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeGroups} rangeGroups
     *  The cell ranges to be processed, divided into column ranges, row
     *  ranges, and cell ranges.
     *
     * @param {RangeArray} taggedRanges
     *  The cell ranges to generate operations for. See description of the
     *  method `generateCellRangeOperations` for details.
     *
     * @param {Function} generateColRowFn
     *  A callback function invoked separately for the column ranges, and for
     *  the row ranges passed in `rangeGroups` (if existing). Receives the
     *  following parameters:
     *  - {SheetOperationGenerator} lclGenerator
     *    A separate operation generator to be filled with column/row
     *    operations. Needed to ensure correct order of undo operations. DO NOT
     *    USE the global `generator` passed above.
     *  - {ColRowCollection} collection
     *    The column collection or row collection to be used to generate the
     *    operations.
     *  - {IntervalArray} intervals
     *    The column/row intervals to be modified.
     *
     * @param {Function} createChangeSetFn
     *  A callback function used to reduce an array of fill data objects. Will
     *  be passed directly to `generateCellRangeOperations`.
     *
     * @returns {JPromise<RangeArray>}
     *  A promise that will fulfil with the addresses of the ranges that have
     *  really been changed when all operations have been generated.
     */
    #generateCombinedColRowCellOperations(generator, rangeGroups, taggedRanges, generateColRowFn, createChangeSetFn) {

        // bug 66003: private generators for columns/rows (correct order of "deleteAutoStyle" in undo)
        const rowGenerator = this.sheetModel.createOperationGenerator({ applyImmediately: true });
        const colGenerator = this.sheetModel.createOperationGenerator({ applyImmediately: true });

        // transport the changed ranges through the promise chain
        let promise = this.createResolvedPromise(new RangeArray());

        // let the row/column collections generate the operations for entire formatted rows/columns
        for (const columns of [false, true]) {

            // the full column/row ranges from the passed range groups
            const fullRanges = columns ? rangeGroups.colRanges : rangeGroups.rowRanges;
            if (!fullRanges) { continue; }

            // create a new asynchronous subtask for the ranges
            promise = promise.then(changedRanges => {

                // the local operations generator
                const lclGenerator = columns ? colGenerator : rowGenerator;
                // the column/row collection to be used
                const collection = columns ? this.#colCollection : this.#rowCollection;
                // extract the column/row intervals from the ranges, copy the "fillData" property
                const intervals = IntervalArray.map(fullRanges, fullRange => {
                    const interval = fullRange.interval(columns);
                    interval.fillData = fullRange.fillData;
                    return interval;
                });

                // invoke the passed callback function that generates the column/row operations
                const promise2 = generateColRowFn(lclGenerator, collection, intervals);
                // concatenate and merge the resulting changed ranges
                return promise2.then(changedIntervals => {
                    generator.appendOperations(lclGenerator);
                    return changedRanges.append(this.#addressFactory.getFullRangeList(changedIntervals, columns));
                });
            });
        }

        // generate the cell operations for all remaining cell ranges
        promise = promise.then(changedRanges => {
            const promise2 = this._generateTaggedRangeOperations(generator, taggedRanges, createChangeSetFn);
            // concatenate and merge the resulting changed ranges
            return promise2.then(newChangedRanges => {
                changedRanges = changedRanges.append(newChangedRanges).merge();
                modelLogger.trace('changed=' + changedRanges);
                return changedRanges;
            });
        });

        // bug 66003: correct order of undo operations (after undoing cell operations,
        // so that the "deleteAutoStyle" operations remain in correct reversed order)
        return promise.done(() => {
            generator.appendOperations(colGenerator, { undo: true });
            generator.appendOperations(rowGenerator, { undo: true });
        });
    }

    #getAutoFormulaRange(address, direction) {

        let address1 = null;
        let address2 = null;
        const vertical = isVerticalDir(direction);

        // find the largest range next to the start cell containing numbers only
        for (const cellEntry of this.linearCellEntries(address, direction, { type: 'value', skipStart: true })) {

            if (!is.number(cellEntry.value)) {
                if (address1) { break; } else { continue; }
            }

            const address3 = cellEntry.address;
            if (address2 && (Math.abs(address2.get(!vertical) - address3.get(!vertical)) > 1)) {
                break;
            }

            if (!address1) { address1 = address3; }
            address2 = address3;
        }

        if (address1 && address2) {
            let startCol = address.c;
            let startRow = address.r;

            if (vertical) { startRow--; } else { startCol--; }

            return { address1, range: Range.fromIndexes(startCol, startRow, address2.c, address2.r) };
        }
    }

    // analyze a range and create a plan for the cell-operations for autofill
    #analyzeForAutoFill(lineIdx, sourceIntervals, vertical, leading, copy) {

        // analyze dedicated ranges and decides which type of autoFill should grab here (increase, copy, ...)
        const analyseStringNumberInterval = sourceEntry => {

            if (sourceEntry.sequence.size <= 2) {
                return 'increase';
            }

            const arrNr = [];
            let parts = null;
            let i = 0;
            const partIndex = (sourceEntry.valueType === 'stringNumber') ? 2 : 1;
            const regex = (sourceEntry.valueType === 'stringNumber') ? TRAILING_INT_RE : LEADING_INT_RE;
            let lastValue = null;
            let rise = null;

            for (const address of ary.values(sourceEntry.sequence.addresses, { reverse: leading })) {

                const cellModel = this.getCellModel(address);
                let nr = null;

                parts = regex.exec(cellModel.v);
                nr = parseInt(parts[partIndex], 10);
                arrNr.push(nr);

                if (i > 0) {
                    if (rise === null || rise === (nr - lastValue)) {
                        rise = nr - lastValue;
                    } else {
                        rise = false;
                    }
                }

                lastValue = nr;

                i++;
            }

            const slp = slope(arrNr).slope;
            return (rise !== false && rise === slp) ? 'increase' : 'copy';
        };

        const analyzeDateTimeInterval = sourceEntry => {

            const values = this.getRangeContents(sourceEntry.sequence.ranges, { blanks: true }).map(entry => entry.value);
            if (values.length === 1) {
                return { interval: 1, unit: (sourceEntry.valueType === 'time') ? 'hour' : 'day' };
            }

            let interval = null;
            let unit = null;

            let lastUnit = null;
            let lastInterval = null;
            let lastDateObj = null;

            const linear = values.every((number, i) => {
                const date = this.#numberFormatter.convertNumberToDate(number);
                const momentObj = moment({ year: date.getFullYear(), month: date.getMonth(), day: date.getDate(), hours: date.getHours(), minutes: date.getMinutes(), seconds: date.getSeconds(), milliseconds: date.getMilliseconds() }).utc();

                if (i > 0) {
                    const intervalMilli   = -lastDateObj.diff(momentObj, 'milliseconds', true);
                    const intervalSeconds = -lastDateObj.diff(momentObj, 'seconds', true);
                    const intervalMinutes = -lastDateObj.diff(momentObj, 'minutes', true);
                    const intervalHours   = -lastDateObj.diff(momentObj, 'hours', true);
                    const intervalDays    = -lastDateObj.diff(momentObj, 'days', true);
                    const intervalMonth   = -lastDateObj.diff(momentObj, 'month', true);
                    const intervalYears   = -lastDateObj.diff(momentObj, 'years', true);

                    if (Number.isInteger(intervalYears)) {
                        unit = 'years';         interval = intervalYears;
                    } else if (Number.isInteger(intervalMonth)) {
                        unit = 'months';        interval = intervalMonth;
                    } else if (Number.isInteger(intervalDays)) {
                        unit = 'days';          interval = intervalDays;
                    } else if (Number.isInteger(intervalHours)) {
                        unit = 'hours';         interval = intervalHours;
                    } else if (Number.isInteger(intervalMinutes)) {
                        unit = 'minutes';       interval = intervalMinutes;
                    } else if (Number.isInteger(intervalSeconds)) {
                        unit = 'seconds';       interval = intervalSeconds;
                    } else if (Number.isInteger(intervalMilli)) {
                        unit = 'milliseconds';  interval = intervalMilli;
                    }

                    if (lastUnit === null && lastInterval === null) {
                        lastUnit = unit;
                        lastInterval = interval;
                    } else if (lastUnit !== unit || lastInterval !== interval) {
                        return false;
                    }
                }

                lastDateObj = momentObj;
                return true;
            });

            return (linear && interval !== null && unit !== null) ? { interval, unit } : null;
        };

        const analyzeDefinedListInterval = sourceEntry => {

            const values = this.getRangeContents(sourceEntry.sequence.ranges, { blanks: true }).map(entry => entry.value);

            let maxList = null;
            //let maxListIdx = -1;
            const entryIdxMap = new Map/*<SortList, number[]>*/();

            let linear = null;
            let interval = null;

            if (values.length > 1) {
                // collect and count all list-indexes whose matching the given array-values
                const listCounts = new Map/*<SortList, number>*/();
                let maxCount = 0;
                for (let value of values) {
                    value = value.toLowerCase();
                    for (const sortList of this.#sortListCollection.sortLists()) {
                        const entryIdx = sortList.indexOf(value);
                        if (entryIdx >= 0) {
                            const newCount = map.add(listCounts, sortList, 1);
                            if (maxCount < newCount) {
                                maxCount = newCount;
                                maxList = sortList;
                            }
                            map.upsert(entryIdxMap, sortList, () => []).push(entryIdx);
                        }
                    }
                }

                let lastDistance = null;
                let lastIndex = null;

                const entryIdxArr = maxList && entryIdxMap.get(maxList);
                if (entryIdxArr && (entryIdxArr.length > 1)) {
                    linear = entryIdxArr.every(entryIdx => {
                        let currentDistance = null;
                        if (lastDistance === null) {
                            if (lastIndex === null) {
                                lastIndex = entryIdx;
                            } else {
                                lastDistance = math.modulo(entryIdx - lastIndex, maxList.length);
                                lastIndex = entryIdx;
                            }
                        } else {
                            currentDistance = math.modulo(entryIdx - lastIndex, maxList.length);
                            if (lastDistance !== currentDistance) {
                                return false;
                            }
                            lastDistance = currentDistance;
                            lastIndex = entryIdx;
                        }
                        return true;
                    });
                    interval = linear ? lastDistance : null;

                } else {
                    linear = true;
                    interval = 1;
                }

            } else {
                const value = values[0].toLowerCase();
                for (const sortList of this.#sortListCollection.sortLists()) {
                    const entryIdx = sortList.indexOf(value);
                    if (entryIdx >= 0) {
                        maxList = sortList;
                        map.upsert(entryIdxMap, sortList, () => []).push(entryIdx);
                        break;
                    }
                }

                linear = true;
                interval = 1;
            }

            return {
                sortList: maxList,
                indexArr: entryIdxMap.get(maxList),
                linear,
                interval
            };
        };

        // compares values of the last and current cell to find similar types
        const compareValues = (type, last, current) => {
            let partsLast = null;
            let partsLastColl = null;
            let partsCurrent = null;

            if (type === 'stringNumber') {
                partsLast = TRAILING_INT_RE.exec(last);
                partsCurrent = TRAILING_INT_RE.exec(current);
                return partsLast[1].toLowerCase() === partsCurrent[1].toLowerCase();

            } else if (type === 'numberString') {
                partsLast = LEADING_INT_RE.exec(last);
                partsCurrent = LEADING_INT_RE.exec(current);
                return partsLast[2].toLowerCase() === partsCurrent[2].toLowerCase();

            } else if (type === 'sortList') {
                partsCurrent = [];
                partsLastColl = [];
                for (const [listIdx, sortList] of this.#sortListCollection.sortListEntries()) {
                    if (sortList.includes(last.toLowerCase())) { partsLastColl.push(listIdx); }
                    if (sortList.includes(current.toLowerCase())) { partsCurrent.push(listIdx); }
                }

                if (partsLastColl.length > 1 && _.intersection(partsLastColl, partsCurrent).length > 0) {
                    partsLast = _.first(_.intersection(partsLastColl, partsCurrent));
                } else {
                    partsLast = _.first(partsLastColl);
                }

                if (lastList === null) {
                    lastList = partsLast;
                }

                if (partsCurrent.indexOf(partsLast) !== -1) {
                    partsCurrent = partsLast;
                }

                return (partsLast !== null && partsLast === partsCurrent && lastList === partsLast);

            }
            return false;
        };

        let lastList = null;
        let lastSeq = null;
        let lastValueType = null;
        let lastValue = null;
        const sequences = [];

        const sourceEntries = Array.from(sourceIntervals.indexes({ reverse: leading }), sourceIdx => {

            const address = Address.fromDir(vertical, lineIdx, sourceIdx);
            const cellModel = this.getCellModel(address);
            let valueType = null;

            if (cellModel) {

                if (cellModel.isAnyFormula()) {
                    valueType = 'formula';
                } else if (cellModel.isError()) {
                    valueType = 'error';
                } else if (cellModel.isNumber()) {
                    valueType = cellModel.pf.isAnyDateTime() ? cellModel.pf.category : 'number';
                } else if (cellModel.isText()) {
                    if (this.#sortListCollection.hasList(cellModel.v)) {
                        valueType = 'sortList';
                    } else if (TRAILING_INT_RE.test(cellModel.v)) {
                        valueType = 'stringNumber';
                    } else if (LEADING_INT_RE.test(cellModel.v)) {
                        valueType = 'numberString';
                    } else {
                        valueType = 'string';
                    }
                } else if (cellModel.isBlank()) {
                    valueType = '';
                }

                if (lastSeq && (lastValueType !== null)) {
                    if (valueType === lastValueType && ((valueType !== 'stringNumber' && valueType !== 'numberString' && valueType !== 'sortList') || (lastValue !== null && compareValues(valueType, lastValue, cellModel.v)))) {
                        lastSeq.addresses.push(address);
                    } else {
                        lastList = null;
                        lastSeq = null;
                        lastValueType = null;
                    }
                }

                if (!lastSeq) {
                    sequences.push(lastSeq = { addresses: [address] });
                }
                if (lastValueType === null) {
                    lastValueType = valueType;
                }

                lastValue = cellModel.v;
            } else {
                lastSeq = null;
                lastValueType = null;
                lastValue = null;
            }

            return {
                address,
                sequence: lastSeq,
                valueType
            };
        });

        // add missing properties to sequences (shared by multiple entries)
        for (const sequence of sequences) {
            sequence.size = sequence.addresses.length;
            sequence.ranges = RangeArray.mergeAddresses(sequence.addresses);
            sequence.index = 0;
        }

        // analyze the fill type (increase, copy, ...) for each found range
        const singleSource = sourceIntervals.size() === 1;
        for (const sourceEntry of sourceEntries) {
            sourceEntry.fillType = 'copy';

            if (sourceEntry.valueType === 'number') {
                // reversed logic for single numbers (auto-increase in copy mode) as in MSXL
                const increase = singleSource === copy;
                sourceEntry.fillType = increase ? 'increase' : 'copy';

            } else if (!copy) {

                switch (sourceEntry.valueType) {

                    case 'stringNumber':
                    case 'numberString':
                        sourceEntry.fillType = analyseStringNumberInterval(sourceEntry);
                        break;

                    case 'date':
                    case 'time':
                    case 'datetime': {
                        const result = analyzeDateTimeInterval(sourceEntry);
                        if (result) {
                            sourceEntry.fillType = 'increase';
                            sourceEntry.unit = result.unit;
                            sourceEntry.interval = result.interval;
                        }
                        break;
                    }

                    case 'formula':
                        sourceEntry.fillType = 'relocate';
                        break;

                    case 'sortList': {
                        const listInterval = analyzeDefinedListInterval(sourceEntry);
                        if (listInterval.linear) {
                            sourceEntry.fillType     = 'increase';
                            sourceEntry.sortList     = listInterval.sortList;
                            sourceEntry.interval     = listInterval.interval;
                            sourceEntry.rangeIndexes = listInterval.indexArr;
                        }
                        break;
                    }
                }
            }
        }

        modelLogger.trace(() => `autofill pattern for ${stringifyIndex(lineIdx, vertical)}\n${sourceEntries.map(entry => `address=${entry.address} valueType=${entry.valueType} fillType=${entry.fillType} sequence=${entry.sequence?.ranges}`).join("\n")}`);

        return sourceEntries;
    }

    // generates autofill operations for cells
    #generateAutoFillOperations(generator, range, direction, count, options) {

        // calculate the result for the autofill-operations
        const calculateResult = (address, sourceEntry) => {

            const fromModel = this.getCellModel(sourceEntry.address);
            let resultValue = null;
            let resultFormula = null;
            let string = null;

            // INCREASE (values like numbers, dates, defined lists, ...)
            if (sourceEntry.fillType === 'increase' && sourceEntry.sequence) {

                // count up an index for each sequence separately
                const seqSize = sourceEntry.sequence.size;
                const seqIdx = sourceEntry.sequence.index;
                sourceEntry.sequence.index += 1;

                if (sourceEntry.valueType === 'date' || sourceEntry.valueType === 'time' || sourceEntry.valueType === 'datetime') {
                    const cellDate = this.#numberFormatter.convertNumberToDate(this.getValue(sourceEntry.sequence.ranges[0].a1));
                    // DOCS-3363: correct handling of UTC date values (Spreadsheet always uses UTC internally!)
                    const momentObj = moment.utc({ year: cellDate.getUTCFullYear(), month: cellDate.getUTCMonth(), day: cellDate.getUTCDate(), hours: cellDate.getUTCHours(), minutes: cellDate.getUTCMinutes(), seconds: cellDate.getUTCSeconds(), milliseconds: cellDate.getUTCMilliseconds() });

                    if (leading) {
                        momentObj.subtract((seqIdx + 1) * sourceEntry.interval, sourceEntry.unit);
                    } else {
                        momentObj.add((seqSize + seqIdx) * sourceEntry.interval, sourceEntry.unit);
                    }

                    resultValue = this.#numberFormatter.convertDateToNumber(momentObj.toDate());

                } else if (sourceEntry.valueType === 'sortList') {
                    const sortList = sourceEntry.sortList;
                    let newIndex = null;

                    if (leading) {
                        newIndex = sourceEntry.rangeIndexes[0] - (seqIdx + 1) * sourceEntry.interval;
                        while (newIndex < 0) { newIndex += sortList.length; }

                    } else {
                        newIndex = ary.at(sourceEntry.rangeIndexes, -1) + (seqIdx + 1) * sourceEntry.interval;
                        if (newIndex >= sortList.length) { newIndex %= sortList.length; }
                    }

                    resultValue = sortList[newIndex];

                    // make the whole string uppercase, if the initial value is uppercase too
                    const firstStr = this.getValue(sourceEntry.sequence.ranges[0].a1);
                    if (firstStr.toUpperCase() === firstStr) {
                        resultValue = resultValue.toUpperCase();
                    // make first letter uppercase, if the first initial letter is uppercase too
                    } else if (firstStr[0].toUpperCase() === firstStr[0]) {
                        resultValue = str.capitalizeFirst(resultValue);
                    }

                } else {
                    const numberGetter = entry => entry.value;
                    const valueGetter = entry => {
                        const matches = regex.exec(entry.value);
                        const value = matches?.[nrIndex] ?? null;
                        return value ? parseInt(value, 10) : 0;
                    };

                    const startCellIndex  = (seqSize + 1) / 2,
                        multiplier      = seqSize + 1 - startCellIndex + seqIdx,

                        strIndex        = (sourceEntry.valueType === 'stringNumber') ? 1 : 2,
                        nrIndex         = (sourceEntry.valueType === 'stringNumber') ? 2 : 1,
                        regex           = (sourceEntry.valueType === 'stringNumber') ? TRAILING_INT_RE : LEADING_INT_RE,
                        getter          = (sourceEntry.valueType === 'number') ? numberGetter : valueGetter;

                    // calculate the average and slope of the numbers
                    const numbers = this.getRangeContents(sourceEntry.sequence.ranges, { blanks: true }).map(getter);
                    const slopeResult = (numbers.length < 2) ? { mean: numbers[0], slope: 1 } : slope(numbers);
                    if (leading) { slopeResult.slope *= -1; }

                    // calculate the new number
                    resultValue = slopeResult.mean + (multiplier * slopeResult.slope);

                    // when the source-value is a combination of string and number
                    if (sourceEntry.valueType === 'stringNumber' || sourceEntry.valueType === 'numberString') {
                        // get the string part of the first cell of the sequence
                        string = regex.exec(this.getCellModel(sourceEntry.sequence.ranges[0].a1).v)[strIndex]; // need to use the first cell, because of the case-sensitivity
                        // add the string to the result value
                        resultValue = (sourceEntry.valueType === 'stringNumber') ? (string + Math.abs(resultValue)) : (Math.abs(resultValue) + string);
                    }
                }

            // RELOCATE (for formulas and shared formulas)
            } else if (sourceEntry.fillType === 'relocate') {
                // resolve the formula expression
                resultFormula = fromModel.getFormula("op", address);

            // COPY (values)
            } else if (sourceEntry.sequence) {
                if (sourceEntry.valueType === 'formula') {
                    resultFormula = this.getFormula(fromModel.a, "op");
                }
                resultValue = getModelValue(fromModel);
            }

            // returns the result within an object
            return {
                model: fromModel,
                style: this.getStyleId(sourceEntry.address),
                value: (resultFormula && (resultValue === null) && fromModel) ? fromModel.v : resultValue,
                formula: resultFormula
            };
        };

        // whether to expand/delete columns or rows
        const vertical = isVerticalDir(direction);
        // whether to expand/shrink the leading or trailing border
        const leading = isLeadingDir(direction);
        // whether to auto-fill complete columns
        const completeCol = this.#addressFactory.isColRange(range);
        // whether to auto-fill complete rows
        const completeRow = this.#addressFactory.isRowRange(range);
        // copy mode or auto-increase
        const copy = !!options?.copy;

        // column row collections for main and child direction
        const lineCollection = vertical ? this.#colCollection : this.#rowCollection;
        const stepCollection = vertical ? this.#rowCollection : this.#colCollection;

        // DOCS-4185: restrict to visible cells, if the sheet contains a filtered table *anywhere*
        const iterOptions = this.sheetModel.isFiltered() ? { visible: true } : undefined;

        // source intervals (DOCS-4185: may be interrupted by hidden entries in filtered tables)
        const sourceIntervals = IntervalArray.from(stepCollection.intervals(range.interval(!vertical), iterOptions));
        const sourceSize = sourceIntervals.size();

        // target intervals (DOCS-4185: may be interrupted by hidden entries in filtered tables)
        const targetRange = getAdjacentRange(range, direction, count);
        const targetIntervals = IntervalArray.from(stepCollection.intervals(targetRange.interval(!vertical), iterOptions));

        // the change sets for the cells
        const changeSets = [];

        // if one or more complete col/row is selected for autofill
        if (completeRow || completeCol) {

            // generate ranges for the parallel iterator
            const iterator = (vertical ? this.#rowMatrix : this.#colMatrix).yieldIndexes(sourceIntervals);
            const parallelRanges = Array.from(iterator, completeRow ?
                index => Range.fromIndexes(0, index, range.a2.c, index) :
                index => Range.fromIndexes(index, 0, index, range.a2.r)
            );

            // generate change sets with the parallel iterator
            for (const addresses of this.parallelAddresses(parallelRanges, { type: 'defined' })) {

                // analyze autofill patterns in source cells
                const lineIdx = addresses[0].get(completeRow);
                const sourceEntries = this.#analyzeForAutoFill(lineIdx, sourceIntervals, vertical, leading, copy);

                // iterate over all single colums/rows to push new content to target-ranges
                for (const [seqIdx, targetIdx] of itr.indexed(targetIntervals.indexes({ reverse: leading }))) {

                    // position of the target cell to be filled
                    const address = Address.fromDir(vertical, lineIdx, targetIdx);

                    // calculate contents for the target cell
                    const sourceEntry = sourceEntries[seqIdx % sourceSize];
                    const result = calculateResult(address, sourceEntry);

                    // add cell changset to the result array
                    changeSets.push({
                        address,
                        v: result.value,
                        f: result.formula,
                        s: result.model ? this.#resolveStyleId(result.model) : null
                    });
                }
            }

        } else {

            // process each column (for up/down) or row (for left/right) in the source range
            // DOCS-4185: may be interrupted by hidden entries in filtered tables
            for (const lineIdx of lineCollection.indexes(range.interval(vertical), iterOptions)) {

                // analyze autofill patterns in source cells
                const sourceEntries = this.#analyzeForAutoFill(lineIdx, sourceIntervals, vertical, leading, copy);

                // step through all cells in the target line range
                for (const [seqIdx, targetIdx] of itr.indexed(targetIntervals.indexes({ reverse: leading }))) {

                    // position of the target cell to be filled
                    const address = Address.fromDir(vertical, lineIdx, targetIdx);

                    // calculate contents for the target cell
                    const sourceEntry = sourceEntries[seqIdx % sourceSize];
                    const result = calculateResult(address, sourceEntry);

                    // add cell changset to the result array
                    changeSets.push({
                        address: address.clone(),
                        v: result.value,
                        f: result.formula,
                        s: result.style ?? null
                    });
                }
            }
        }

        // generate the cell content operations
        return this.generateCellsOperations(generator, changeSets);
    }

    /**
     * Returns the parts of the passed ranges that will receive a new border
     * attribute. Parameters 'columns'/'leading' specify the position of the
     * cell border to be modified. The resulting ranges will contain an
     * additional property 'fillData' with border settings as expected by the
     * helper function `createBorderChangeSet`.
     */
    #getChangedBorderRanges(ranges, borderAttributes, columns, leading) {

        // the keys of the borders to be processed
        const outerKey = getOuterBorderKey(columns, leading);
        const innerKey = getInnerBorderKey(columns);

        // get the border attributes, nothing to do without outer nor inner border attribute
        const outerBorder = borderAttributes[getBorderName(outerKey)];
        const innerBorder = borderAttributes[getBorderName(innerKey)];
        if (!outerBorder && !innerBorder) { return null; }

        // the resulting ranges
        const resultRanges = new RangeArray();

        // adds the passed style settings to the passed ranges, and appends them to the resulting range array
        const appendRanges = (sourceRanges, mapRangeFn, fillData) => {
            for (const sourceRange of sourceRanges) {
                const resultRange = mapRangeFn(sourceRange);
                if (resultRange) {
                    resultRange.fillData = fillData;
                    resultRanges.push(resultRange);
                }
            }
        };

        // add the adjacent ranges whose opposite borders need to be deleted while setting a range border
        // (if the outer border will not be changed, the borders of the adjacent cells will not be modified neither)
        if (outerBorder) {
            // the adjacent ranges at the specified range border whose existing borders will be deleted
            const adjacentRanges = this.#getAdjacentRanges(ranges, columns, leading);
            // opposite borders will be deleted, e.g. the right border left of the original range (but not if
            // they are equal to the outer border set at the cell range, to reduce the amount of changed cells)
            const oppositeKey = getOuterBorderKey(columns, !leading);
            appendRanges(adjacentRanges, fun.identity, { key: oppositeKey, keepBorder: outerBorder });
        }

        // shortcut (outer and inner borders are equal): return entire ranges instead of splitting into outer/inner
        if (outerBorder && innerBorder && equalBorders(outerBorder, innerBorder)) {
            appendRanges(ranges, range => range.clone(), { key: outerKey, border: outerBorder, cacheType: 'outer' });
            return resultRanges;
        }

        // divide the ranges into ranges covering entire columns/rows and other ranges
        const [cellRanges, fullRanges] = ary.split(ranges, range => this.#addressFactory.isFullRange(range, !columns));

        // do not set outer borders at entire column/row ranges, use inner border for all cells
        if (innerBorder && fullRanges.length) {
            appendRanges(fullRanges, range => range.clone(), { key: outerKey, border: innerBorder, cacheType: 'outer' });
        }

        // outer border will be set to the outer cells only
        if (outerBorder && cellRanges.length) {
            const subRangeFn = columns ?
                (leading ? (range => range.leadingCol()) : (range => range.trailingCol())) :
                (leading ? (range => range.headerRow())  : (range => range.footerRow()));
            appendRanges(cellRanges, subRangeFn, { key: outerKey, border: outerBorder, cacheType: 'outer' });
        }

        // inner border will be set to the remaining cells
        if (innerBorder && cellRanges.length) {
            const offset = leading ? 1 : 0;
            const subRangeFn = range => range.singleLine(columns) ? null : range.lineRange(columns, offset, range.size(columns) - 1);
            appendRanges(cellRanges, subRangeFn, { key: outerKey, border: innerBorder, cacheType: 'inner' });
        }

        return resultRanges;
    }

    /**
     * Returns the adjacent ranges of the passed ranges whose border will be
     * changed additionally to the covered ranges. The resulting ranges will
     * contain an additional property 'fillData' with border settings as
     * expected by the helper function `createVisibleBorderChangeSet`.
     */
    #getAdjacentBorderRanges(ranges, columns, leading) {
        const adjacentRanges = this.#getAdjacentRanges(ranges, columns, leading);
        const oppositeKey = getOuterBorderKey(columns, !leading);
        return addProperty(adjacentRanges, 'fillData', { keys: [oppositeKey] });
    }

    #getRangeBorders(borderRange) {

        // the resulting range borders
        const resultBorderMap = {};

        const updateBorder = (styleBorderMap, borderKey) => {
            const styleBorder = styleBorderMap[borderKey];
            if (!(borderKey in resultBorderMap)) {
                resultBorderMap[borderKey] = styleBorder;
            } else if ((resultBorderMap[borderKey] !== null) && !equalBorders(resultBorderMap[borderKey], styleBorder)) {
                resultBorderMap[borderKey] = null;
            }
        };

        // collect all outer borders and diagonal borders of the range
        for (const { range, style } of this.styleRanges(borderRange)) {

            // the visible border attributes of the auto-style, mapped by border key
            const styleBorderMap = this.#autoStyles.getBorderMap(style);

            // update all border attributes according to the position of the current style range
            const styleRange = range;
            if (borderRange.a1.c === styleRange.a1.c) { updateBorder(styleBorderMap, 'l'); }
            if (borderRange.a1.r === styleRange.a1.r) { updateBorder(styleBorderMap, 't'); }
            if (borderRange.a2.c === styleRange.a2.c) { updateBorder(styleBorderMap, 'r'); }
            if (borderRange.a2.r === styleRange.a2.r) { updateBorder(styleBorderMap, 'b'); }
            updateBorder(styleBorderMap, 'd');
            updateBorder(styleBorderMap, 'u');
        }

        return resultBorderMap;
    }
}
