/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary } from "@/io.ox/office/tk/algorithms";
import { EObject } from "@/io.ox/office/tk/objects";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { type AddressIndexFn, Address } from "@/io.ox/office/spreadsheet/utils/address";
import { type IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { SubtotalResult } from "@/io.ox/office/spreadsheet/utils/subtotalresult";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { CellModelVector } from "@/io.ox/office/spreadsheet/model/cellmodelvector";
import type { ColRowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * List entry for the cell models of an entire column or row. Contains the
 * sorted model vector, and a cached subtotal result for all cells.
 */
interface CellVectorEntry {

    /**
     * Sorted list of all cell models in this column/row.
     */
    vector: CellModelVector;

    /**
     * Cached subtotal result for the entire column or row (all cell models in
     * the vector).
     */
    result: SubtotalResult;

    /**
     * Cache age for the subtotal result.
     */
    age: number;
}

// class CellModelMatrix ======================================================

/**
 * An instance of this class represents a sorted sparse matrix of cells in the
 * sheet for a specific orientation (cells in column vectors, or in row
 * vectors). The double-sorted data structure allows fast binary lookup, and
 * fast iterator implementations for existing cells in large cell ranges.
 *
 * Example: An instance of this class is used as row matrix. In this case, it
 * will contain several row vectors (instances of the class `CellModelVector`,
 * sorted by row index) that contain the cell models of all defined (existing)
 * cells in the respective rows (each sorted by column indexes).
 *
 * @param columns
 *  Whether this instance contains column vectors (`true`; vectors will be
 *  sorted by column index), or row vectors (`false`; vectors will be sorted by
 *  row index).
 */
export class CellModelMatrix extends EObject {

    // properties -------------------------------------------------------------

    // returns the column/row index of a cell model address used as vector index
    readonly #vecIndexFn: AddressIndexFn;

    // the sorted array of cell vectors, and cached subtotal results
    readonly #vectorEntries: CellVectorEntry[] = [];

    // maps all cell models to their matrix entries
    readonly #vectorEntryMap = new WeakMap<CellModel, CellVectorEntry>();

    // the column/row collection in main direction needed to skip hidden column/row vectors
    readonly #collection: ColRowCollection;

    // whether this is a column matrix, or a row matrix
    readonly #columns: boolean;

    // the cache age of subtotal results in the cell model vectors
    #cacheAge = 0;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, columns: boolean) {
        super();

        this.#vecIndexFn = Address.indexFn(columns);
        this.#collection = columns ? sheetModel.colCollection : sheetModel.rowCollection;
        this.#columns = columns;

        // invalidate all cached subtotal results if columns/rows will be hidden or shown
        // (just increase the cache age instead of looping over all vector entries)
        const collection = columns ? sheetModel.rowCollection : sheetModel.colCollection;
        this.listenTo(collection, "change:intervals", event => {
            if (event.visibility) { this.#cacheAge += 1; }
        });
    }

    protected override destructor(): void {
        this.#vectorEntries.length = 0;
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the smallest interval containing all cell vectors in this cell
     * matrix.
     *
     * @returns
     *  The smallest interval containing all cell vectors in this cell matrix;
     *  or `undefined`, if this matrix is empty.
     */
    getUsedInterval(): Opt<Interval> {
        if (!this.#vectorEntries.length) { return undefined; }
        const first = this.#vectorEntries[0].vector.index;
        const last = ary.at(this.#vectorEntries, -1)!.vector.index;
        return new Interval(first, last);
    }

    /**
     * Inserts a new cell model into this matrix.
     *
     * @param cellModel
     *  The new cell model to be inserted into this matrix. This matrix DOES
     *  NOT take ownership of the cell models; and it DOES NOT check whether it
     *  already contains a cell model with the same address as the passed cell
     *  model (it is up to the caller to ensure uniqueness of the cell models).
     */
    insertModel(cellModel: CellModel): void {

        // binary search for array position (first cell vector with index greater/equal to index of cell model)
        const index = this.#vecIndexFn(cellModel.a);
        const ai = ary.fastFindFirstIndex(this.#vectorEntries, elem => index <= elem.vector.index);
        let entry = this.#vectorEntries[ai] as Opt<CellVectorEntry>;

        // insert the cell model into the respective cell vector (create missing vector on demand)
        if (entry && (entry.vector.index === index)) {
            entry.vector.insertModel(cellModel);
            // adding a non-blank cell invalidates the cached subtotal result
            if (!cellModel.isBlank()) { entry.age = -1; }
        } else {
            // create and insert a new cell vector
            const cellVector = new CellModelVector(cellModel, this.#columns);
            entry = { vector: cellVector, result: new SubtotalResult(), age: this.#cacheAge };
            ary.insertAt(this.#vectorEntries, ai, entry);
        }

        // map cell model to cell vector for fast lookup
        this.#vectorEntryMap.set(cellModel, entry);
    }

    /**
     * Removes a cell model from this matrix.
     *
     * @param cellModel
     *  The cell model to be removed from this matrix. If this matrix does not
     *  contain a cell model at the address of the passed cell model, nothing
     *  will happen.
     */
    removeModel(cellModel: CellModel): void {

        // binary search for array position (first cell vector with index greater/equal to index of cell model)
        const index = this.#vecIndexFn(cellModel.a);
        const ai = ary.fastFindFirstIndex(this.#vectorEntries, elem => index <= elem.vector.index);
        const entry = this.#vectorEntries[ai] as Opt<CellVectorEntry>;

        // check that vector index is exactly equal (otherwise, there is no vector for the passed cell model)
        if (entry && (entry.vector.index === index)) {
            if (entry.vector.removeModel(cellModel)) {
                // remove entire vector entry after the last cell model has been removed
                ary.deleteAt(this.#vectorEntries, ai);
            } else if (!cellModel.isBlank()) {
                // removing a non-blank cell invalidates the cached subtotal result
                entry.age = -1;
            }
        } else {
            modelLogger.error(`$badge{CellVector} remove: cell model at ${cellModel.a} not found`);
        }
    }

    /**
     * Creates an iterator that visits the cell vectors inside the specified
     * intervals contained in this matrix.
     *
     * @param vectorIntervals
     *  An array of index intervals, or a single index interval.
     *
     * @param [reverse=false]
     *  If set to `true`, the passed intervals will be processed in reversed
     *  order, as well as the cell vectors in each interval.
     *
     * @yields
     *  The cell vectors.
     */
    *yieldVectors(vectorIntervals: IntervalSource, reverse?: boolean): IterableIterator<CellModelVector> {
        const iterator = ary.values(IntervalArray.cast(vectorIntervals), { reverse });
        for (const entry of this.#yieldEntries(iterator, reverse)) {
            yield entry.vector;
        }
    }

    /**
     * Creates an iterator that generates the column/row indexes of all cell
     * vectors inside the specified intervals contained in this matrix.
     *
     * @param vectorIntervals
     *  An array of index intervals, or a single index interval.
     *
     * @param [reverse=false]
     *  If set to `true`, the passed intervals will be processed in reversed
     *  order, as well as the indexes of the cell vectors in each interval.
     *
     * @yields
     *  The indexes of the cell vectors.
     */
    *yieldIndexes(vectorIntervals: IntervalSource, reverse?: boolean): IterableIterator<number> {
        for (const vector of this.yieldVectors(vectorIntervals, reverse)) {
            yield vector.index;
        }
    }

    /**
     * Creates an iterator that visits all existing cell models in this matrix
     * covered by the ranges formed by the passed index intervals. The cell
     * models will be visited vector-by-vector.
     *
     * @param vectorIntervals
     *  An array of index intervals, or a single index interval, specifying all
     *  cell model vectors to be visited.
     *
     * @param entryIntervals
     *  An array of index intervals, or a single index interval, specifying all
     *  cells to be visited in each cell model vector.
     *
     * @param [reverse=false]
     *  If set to `true`, the passed intervals will be processed in reversed
     *  order.
     *
     * @yields
     *  The existing cell models in the specified intervals.
     */
    *yieldModels(vectorIntervals: IntervalSource, entryIntervals: IntervalSource, reverse?: boolean): IterableIterator<CellModel> {
        const iterator = ary.values(IntervalArray.cast(vectorIntervals), { reverse });
        entryIntervals = IntervalArray.cast(entryIntervals);
        for (const { vector } of this.#yieldEntries(iterator, reverse)) {
            for (const entryInterval of ary.values(entryIntervals, { reverse })) {
                yield* vector.yieldInterval(entryInterval, reverse);
            }
        }
    }

    /**
     * Creates an iterator that visits the subtotal results of all cell model
     * vectors covered by the specified index intervals.
     *
     * @param vectorIntervals
     *  An array of index intervals, or a single index interval, specifying all
     *  cell model vectors to be visited.
     *
     * @yields
     *  The subtotals.
     */
    *yieldSubtotals(vectorIntervals: IntervalSource): IterableIterator<SubtotalResult> {

        // visit all covered vector entries
        const iterator = this.#collection.intervals(vectorIntervals, { visible: true });
        for (const entry of this.#yieldEntries(iterator)) {

            // return the cached subtotal result, if it is up-to-date
            if (this.#cacheAge === entry.age) {
                yield entry.result;
                continue;
            }

            // create a new subtotal result
            const result = entry.result = new SubtotalResult();

            // simultaneously iterate through the column/row collection to find hidden columns/rows efficiently
            const interval = this.#collection.getFullInterval();
            const indexIter = this.#collection.indexEntries(interval, { visible: true, unique: true });
            let iterResult = indexIter.next();

            // collect the subtotals directly in synchronous mode
            for (const cellModel of entry.vector) {

                // the column/row index of the cell model used as sorting index
                const index = entry.vector.sortIndex(cellModel);

                // find the next visible column/row interval that does not end before the current cell
                while (!iterResult.done && (iterResult.value.uniqueInterval.last < index)) {
                    iterResult = indexIter.next();
                }

                // skip the cell model, if it is located in a hidden column/row
                // (i.e., next visible column/row interval starts after the current cell model)
                if (!iterResult.done && (iterResult.value.uniqueInterval.first <= index)) {
                    result.addValue(cellModel.v);
                }
            }

            // update cache age, and yield the new result
            entry.age = this.#cacheAge;
            yield result;
        }
    }

    /**
     * Invalidates the cached column/row subtotal result related to the passed
     * cell model.
     */
    clearSubtotalResult(cellModel: CellModel): void {
        const entry = this.#vectorEntryMap.get(cellModel);
        if (entry) { entry.age = -1; }
    }

    // private methods --------------------------------------------------------

    /**
     * Creates an iterator that visits all existing cell vector entries inside
     * the specified index intervals contained in this matrix.
     *
     * @param intervals
     *  An iterable data source for index intervals.
     *
     * @param [reverse=false]
     *  If set to `true`, the passed intervals will be processed in reversed
     *  order, as well as the cell vectors in each interval.
     *
     * @yields
     *  The covered cell vector entries.
     */
    *#yieldEntries(intervals: Iterable<Interval>, reverse?: boolean): IterableIterator<CellVectorEntry> {

        // visit all intervals in the passed interval source
        for (const { first, last } of intervals) {

            // performance: completely separated loop for reverse mode to keep all if/else outside the loop body
            if (reverse) {
                // binary search for start position
                let ai = ary.fastFindLastIndex(this.#vectorEntries, entry => entry.vector.index <= last);
                // visit all vector entries inside the interval
                for (let entry = this.#vectorEntries[ai]; entry && (first <= entry.vector.index); ai -= 1, entry = this.#vectorEntries[ai]) {
                    yield entry;
                }
            } else {
                // binary search for start position
                let ai = ary.fastFindFirstIndex(this.#vectorEntries, entry => first <= entry.vector.index);
                // visit all vector entries inside the interval
                for (let entry = this.#vectorEntries[ai]; entry && (entry.vector.index <= last); ai += 1, entry = this.#vectorEntries[ai]) {
                    yield entry;
                }
            }
        }
    }
}
