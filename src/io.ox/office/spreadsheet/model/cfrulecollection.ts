/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, ary, map, dict, pick, uuid } from "@/io.ox/office/tk/algorithms";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import type { CFRangeRuleItem, CFColorScale, CFDataBar, CFIconSet, CFRuleAttributes, CFRuleSharedConfig, CFRuleRenderProps, CFRuleFormulaUpdateConfig, CFRulePasteItem } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import { yieldCFRuleFormulas, CFRuleModel } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import { CFRuleBucket } from "@/io.ox/office/spreadsheet/model/cfrulebucket";
import { type AddressTransformer, TransformMode } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { type FormulaUpdateTask, RelocateRangesUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

// types ======================================================================

/**
 * Result of resolving all formatting rules for a specific cell address.
 */
export interface CFRulesFormatResult {

    /**
     * An array with all matching formatting rules contributing to the merged
     * cell attribute set (without range rules, e.g. data bars).
     */
    ruleModels: CFRuleModel[];

    /**
     * The attribute set with all character and cell formatting attributes to
     * be applied to the cell. Will be omitted for range rules that return
     * rendering properties only.
     */
    cellAttrSet?: PtCellAttributeSet;

    /**
     * Additional optional rendering properties, e.g. for data bars.
     */
    renderProps?: CFRuleRenderProps;
}

/**
 * Event object emitted by an instance of `CFRuleCollection`.
 */
export interface CFRuleEvent extends SheetEventBase {

    /**
     * The model instance of the formatting rule causing the event.
     */
    ruleModel: CFRuleModel;
}

/**
 * Type mapping for the events emitted by `CFRuleCollection` instances.
 */
export interface CFRuleCollectionEventMap {

    /**
     * Will be emitted after a new rule has been inserted into a formatting
     * model of a rule collection.
     *
     * @param event
     *  Event object with the model of the new formatting rule.
     */
    "insert:cfrule": [event: CFRuleEvent];

    /**
     * Will be emitted before an existing rule will be deleted from a
     * formatting model in a rule collection.
     *
     * @param event
     *  Event object with the model of the formatting rule to be deleted.
     */
    "delete:cfrule": [event: CFRuleEvent];

    /**
     * Will be emitted after the attributes of a rule have been changed.
     *
     * @param event
     *  Event object with the model of the changed formatting rule.
     */
    "change:cfrule": [event: CFRuleEvent];
}

// constants ==================================================================

// replacement formulas for "year" rules (supported in ODF only)
const YEAR_REPLACEMENT_FORMULAS: Dict<string> = {
    lastYear: "YEAR(#A)=YEAR(TODAY())-1",
    thisYear: "YEAR(#A)=YEAR(TODAY())",
    nextYear: "YEAR(#A)=YEAR(TODAY())+1"
};

// replacement formulas for "standard deviation" rules (supported in OOXML only)
const STDDEV_REPLACEMENT_FORMULAS: Dict<string> = {
    aboveAverage:   "AND(ISNUMBER(#A),#A>AVERAGE(#R)+#N*STDEVP(#R))",
    belowAverage:   "AND(ISNUMBER(#A),#A<AVERAGE(#R)-#N*STDEVP(#R))",
    atLeastAverage: "AND(ISNUMBER(#A),#A>=AVERAGE(#R)+#N*STDEVP(#R))",
    atMostAverage:  "AND(ISNUMBER(#A),#A<=AVERAGE(#R)-#N*STDEVP(#R))"
};

// private functions ==========================================================

/**
 * Transforms the specified target ranges.
 *
 * @param transformer
 *  An address transformer specifying how to transform the passed target
 *  ranges.
 *
 * @param targetRanges
 *  The addresses of the target ranges to be transformed.
 *
 * @param [reverse]
 *  If set to `true`, the opposite move operation will be used to transform the
 *  target ranges.
 *
 * @returns
 *  The transformed target ranges.
 */
function transformTargetRanges(transformer: AddressTransformer, targetRanges: Range[], reverse = false): RangeArray {
    // transform the passed ranges, expand the end of the range when inserting cells
    return transformer.transformRanges(targetRanges, { transformMode: TransformMode.EXPAND, reverse });
}

function parseRangeRuleItem(context: SheetOperationContext, item: unknown, label: string): asserts item is CFRangeRuleItem {
    context.ensure(is.dict(item), `${label} must be a dictionary`);
    context.ensure(is.string(item.t), `${label} must contain a type`);
    if ("v" in item) { context.ensure(is.string(item.v), `${label} must contain a formula expression`); }
}

function parseColorScale(context: SheetOperationContext): Opt<CFColorScale> {
    const colorScale = context.optArr("colorScale");
    colorScale?.forEach(step => {
        parseRangeRuleItem(context, step, "colorscale step");
        context.ensure(pick.dict(step, "c"), "colorscale step must contain a color");
    });
    return colorScale as Opt<CFColorScale>;
}

function parseDataBar(context: SheetOperationContext): Opt<CFDataBar> {
    const dataBar = context.optDict("dataBar");
    if (dataBar && !is.empty(dataBar)) {
        context.ensure(is.dict(dataBar.c), "databar rule must contain a color");
        for (const rule of [dataBar.r1, dataBar.r2]) {
            parseRangeRuleItem(context, rule, "databar rule");
        }
    }
    return dataBar as Opt<CFDataBar>;
}

function parseIconSet(context: SheetOperationContext): Opt<CFIconSet> {
    const iconSet = context.optDict("iconSet");
    if (iconSet && !is.empty(iconSet)) {
        context.ensure(is.string(iconSet.is), "iconset must contain an identifier");
        context.ensure(is.array(iconSet.ir), "iconset must contain a rule array");
        for (const rule of iconSet.ir) {
            parseRangeRuleItem(context, rule, "iconset rule");
        }
    }
    return iconSet as Opt<CFIconSet>;
}

/**
 * Extracts the rule attributes from the passed document operation.
 *
 * @param context
 *  A wrapper representing a rule operation.
 *
 * @param defaults
 *  A map with default valus for missing attributes.
 *
 * @returns
 *  The rule attributes extracted from the passed document operation.
 */
function parseRuleAttrs(context: SheetOperationContext, defaults: Readonly<CFRuleAttributes>): CFRuleAttributes {
    return {
        type: context.optStr("type") || defaults.type,
        value1: context.optPrim("value1", { empty: true }) ?? defaults.value1,
        value2: context.optPrim("value2", { empty: true }) ?? defaults.value2,
        priority: context.optInt("priority", defaults.priority),
        stop: context.optBool("stop", defaults.stop),
        colorScale: parseColorScale(context) ?? defaults.colorScale,
        dataBar: parseDataBar(context) ?? defaults.dataBar,
        iconSet: parseIconSet(context) ?? defaults.iconSet,
        attrs: context.optDict("attrs") ?? defaults.attrs
    };
}

// class CFRuleCollection =====================================================

/**
 * Stores all conditional formatting rules in a specific sheet. Conditional
 * formatting can be used to modify specific formatting attributes of one
 * or more cell ranges in a sheet automatically.
 */
export class CFRuleCollection extends SheetChildModel<CFRuleCollectionEventMap> {

    // shared collection/model configuration
    readonly #config: Readonly<CFRuleSharedConfig>;

    // all range buckets, mapped by the range keys
    readonly #ruleBucketMap = this.member(new Map<string, CFRuleBucket>());
    // OOXML: rule models by operation identifier
    readonly #ruleModelMap = new Map<string, CFRuleModel>();
    // ODF (indexed mode): rule models in index order
    readonly #ruleModelArray: CFRuleModel[] = [];
    // counter for identifiers for generated style sheets for ODF
    #nextStyleIndex = 0;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this collection.
     */
    constructor(sheetModel: SheetModel) {

        // base constructor
        super(sheetModel);

        // file-format dependent configuration shared by this instance and all rule models
        this.#config = {
            indexedOps: this.docApp.isODF(),
            customRef: this.docApp.isODF(),
            cellStyles: this.docApp.isODF(),
            sheetNames: this.docApp.isODF(), // Bug 45877: ODF inserts sheet names into the range list
            ignoreCase: this.docApp.isOOXML(),
            yearRules: this.docApp.isODF(),
            stdDevRules: this.docApp.isOOXML()
        };

        // additional processing and event handling after the document has been imported
        this.waitForImportSuccess(alreadyImported => {

            // reparse all formulas after import is finished (formulas may refer to sheets not yet imported)
            if (!alreadyImported) {
                for (const ruleModel of this.#ruleModelArray) {
                    ruleModel.refreshAfterImport();
                }
            }

            // update the sheet indexes after the sheet collection has been manipulated
            this.listenTo(this.docModel, "transform:sheets", xfVector => {
                for (const ruleModel of this.#ruleModelArray) {
                    ruleModel.transformSheets(xfVector);
                }
            });
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the rule model with the specified identifier.
     *
     * @param opId
     *  The operation identifier of the formatting rule.
     *
     * @returns
     *  The specified rule model if existing.
     */
    getRuleModel(opId: string): Opt<CFRuleModel> {
        return this.#ruleModelMap.get(opId);
    }

    /**
     * Creates an iterator that visits the rule models in this collection,
     * optionally filtered by rule models covering a specific target range.
     *
     * @param [target]
     *  The address of the cell or cell range to get all formatting rules for.
     *  If omitted, all rule models will be visited.
     *
     * @yields
     *  The rule models of this collection.
     */
    *yieldRuleModels(target?: Address | Range): IterableIterator<CFRuleModel> {
        if (target instanceof Address) {
            for (const ruleModel of this.#ruleModelArray) {
                if (ruleModel.ruleBucket.ranges.containsAddress(target)) {
                    yield ruleModel;
                }
            }
        } else if (target) {
            for (const ruleModel of this.#ruleModelArray) {
                if (ruleModel.ruleBucket.ranges.overlaps(target)) {
                    yield ruleModel;
                }
            }
        } else {
            yield* this.#ruleModelArray;
        }
    }

    /**
     * Creates an iterator that visits the rule buckets in this collection,
     * optionally filtered by rule models covering a specific target range.
     *
     * @param [target]
     *  The address of the cell or cell range to get all rule buckets for. If
     *  omitted, all rule buckets will be visited.
     *
     * @yields
     *  The rule buckets of this collection.
     */
    *yieldRuleBuckets(target?: Address | Range): IterableIterator<CFRuleBucket> {
        if (target instanceof Address) {
            for (const ruleBucket of this.#ruleBucketMap.values()) {
                if (ruleBucket.ranges.containsAddress(target)) {
                    yield ruleBucket;
                }
            }
        } else if (target) {
            for (const ruleBucket of this.#ruleBucketMap.values()) {
                if (ruleBucket.ranges.overlaps(target)) {
                    yield ruleBucket;
                }
            }
        } else {
            yield* this.#ruleBucketMap.values();
        }
    }

    /**
     * Returns the formatting attributes to be applied at the specified cell,
     * and other settings, for all matching rules in this collection.
     *
     * @param address
     *  The address of the cell to calculate the attributes for.
     *
     * @returns
     *  A result object with formatting attributes and additional rendering
     *  properties; or `undefined` if no formatting rules exist for the passed
     *  cell adddress.
     */
    resolveRules(address: Address): Opt<CFRulesFormatResult> {

        // nothing to do in an empty collection
        // DOCS-1611: do not evaluate conditions during import (early rendering)
        if (!this.#ruleBucketMap.size || !this.isImportFinished()) { return undefined; }

        // collect all rule models covering the specified cell in an array
        const ruleModels = Array.from(this.yieldRuleModels(address));
        if (!ruleModels.length) { return undefined; }

        // sort the entire list of rule models by priority
        ary.sortBy(ruleModels, ruleModel => ruleModel.getPriority());

        // collect the settings of all matching rules
        const attrPool = this.docModel.sheetAttrPool;
        const mergedResult: CFRulesFormatResult = { ruleModels: [] };
        for (const ruleModel of ruleModels) {

            // resolve the current rule, nothing to do, if it does not match
            const ruleResult = ruleModel.resolveRule(address);
            if (!ruleResult) { continue; }

            // merge the attributes (prefer existing attributes already found before)
            if (ruleResult.cellAttrSet) {
                mergedResult.ruleModels.push(ruleModel);
                mergedResult.cellAttrSet = attrPool.extendAttrSet(ruleResult.cellAttrSet, mergedResult.cellAttrSet);
            }

            // merge the rendering properties (prefer existing properties already found before)
            if (ruleResult.renderProps) {
                mergedResult.renderProps = { ...ruleResult.renderProps, ...mergedResult.renderProps };
            }

            // abort evaluation of other rules if specified by the rule
            if (ruleResult.stop) { break; }
        }

        // do not return the result object, if no matching rule covers the passed address
        return (mergedResult.cellAttrSet || mergedResult.renderProps) ? mergedResult : undefined;
    }

    /**
     * Serializes the formatting rules to a plain JSON array that can be
     * serialized for clipboard markup.
     *
     * @param copyRange
     *  The address of the cell range to be copied to clipboard.
     *
     * @returns
     *  The JSON data of all rules that overlap with the specified copy range.
     */
    serializePasteItems(copyRange: Range): unknown[] {
        return ary.from(this.yieldRuleModels(copyRange), ruleModel => {
            const pasteItem = ruleModel.serializePasteItems(copyRange);
            return pasteItem && {
                id: pasteItem.opId,
                ranges: pasteItem.sourceRanges.toOpStr(),
                ref: pasteItem.refAddress.toOpStr(),
                attrs: pasteItem.ruleAttrs
            };
        });
    }

    /**
     * Tries to parses the passed JSON data to a clipboard paste item.
     */
    parsePasteItems(data: unknown): CFRulePasteItem[] {
        return is.array(data) ? ary.from(data, entry => {

            // pick properties from array elements
            const opId = pick.string(entry, "id");
            const ranges = pick.string(entry, "ranges");
            const ref = pick.string(entry, "ref");
            const ruleAttrs = pick.dict(entry, "attrs");
            if (!opId || !ranges || !ref || !ruleAttrs) { return undefined; }

            // parse target ranges and referemce address
            const { addressFactory } = this.docModel;
            const sourceRanges = addressFactory.parseRangeList(ranges);
            const refAddress = addressFactory.parseAddress(ref);
            if (!sourceRanges || sourceRanges.empty() || !refAddress) { return undefined; }

            // create and return the rule paste item
            return { opId, sourceRanges, refAddress, ruleAttrs };
        }) : [];
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations and undo operations to update and restore
     * the formula expressions of the conditional formatting rules in this
     * collection.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     */
    @modelLogger.profileMethod("$badge{CFRuleCollection} generateUpdateTaskOperations")
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): void {

        // transform the target ranges when moving cells in the sheet
        const moveCells = this.sheetModel.isOwnMoveTask(updateTask);
        // update the target ranges in ODF (stored with sheet name) when the sheet has been renamed
        const renameSheet = this.#config.customRef && this.sheetModel.isOwnRenameTask(updateTask);

        // build a map with the settings of all transformed range buckets
        const updateConfigMap = map.from(this.#ruleBucketMap.values(), ruleBucket => {

            // the original target ranges covered by the rules currently processed
            const targetRanges = ruleBucket.ranges;
            // the new transformed target ranges for this rule
            let newTargetRanges = targetRanges;
            // configuration to be passed to operation generator methods
            const updateConfig: CFRuleFormulaUpdateConfig = {
                oldRangesStr: "",
                newRangesStr: "",
                oldRefAddress: ruleBucket.refAddress
            };

            // transform the target ranges, if the moved cells are located in the own sheet
            if (moveCells) {

                // transform the current target ranges
                newTargetRanges = transformTargetRanges(updateTask.transformer, targetRanges);

                // nothing more to do, if the target ranges do not change at all
                updateConfig.updateRanges = !newTargetRanges.empty() && !targetRanges.deepEquals(newTargetRanges);
                if (updateConfig.updateRanges) {

                    // the new reference address after transformation
                    updateConfig.newRefAddress = newTargetRanges.refAddress();

                    // simulate what happens when undoing the change (restored ranges may differ from original)
                    const undoTargetRanges = transformTargetRanges(updateTask.transformer, newTargetRanges, true);
                    updateConfig.undoRefAddress = undoTargetRanges.refAddress();
                }
            }

            // generate the range lists (as strings) to be added to the operations for all rules
            const newSheetName = renameSheet ? updateTask.sheetName : undefined;
            updateConfig.newRangesStr = this.#formatTargetRanges(newTargetRanges, newSheetName);
            updateConfig.oldRangesStr = this.#formatTargetRanges(targetRanges);

            // create a map entry keyed by rule bucket
            return [ruleBucket, updateConfig];
        });

        // Generate the change operations for all formatting rules, using the bucket transformation data (possibly
        // shared by multiple formatting rules).
        // DOCS-1941: Always in reversed array order to retain correct array indexes (when deleting/restoring rules).
        for (const ruleModel of ary.values(this.#ruleModelArray, { reverse: true })) {
            const configEntry = updateConfigMap.get(ruleModel.ruleBucket)!;
            ruleModel.generateUpdateTaskOperations(sheetCache, updateTask, configEntry);
        }
    }

    /**
     * Generates and applies operations to paste new formatting rules from the
     * passed clipboard data.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param pasteRanges
     *  The addresses of the cell ranges to be pasted into. Needed to reduce
     *  the target ranges of existing formatting rules.
     *
     * @param pasteRefs
     *  The reference addresses of the inidividual repeated paste ranges,
     *  needed to generate the effective rule target ranges from the original
     *  ranges in the paste items (which are always relative to cell A1).
     *
     * @param pasteItems
     *  The rule paste items, as returned from `parsePasteItems`.
     *
     * @param sameDoc
     *  Whether the pasted rules originate from the same document containing
     *  this collection instance (but possibly from another sheet).
     *
     * @param sameSheet
     *  Whether the pasted rules originate from the same sheet containing this
     *  collection instance.
     */
    @modelLogger.profileMethod("$badge{CFRuleCollection} generatePasteOperations")
    generatePasteOperations(generator: SheetOperationGenerator, pasteRanges: RangeArray, pasteRefs: Address[], pasteItems: CFRulePasteItem[], sameDoc: boolean, sameSheet: boolean): void {

        // local generator for inserting new cell stylesheets for ODF
        const styleGenerator = this.sheetModel.createOperationGenerator();
        // local generator for inserting new rules to be able to generate operations in custom order
        const insertGenerator = this.sheetModel.createOperationGenerator();
        // local generator for updating rules to be able to generate operations in custom order
        const updateGenerator = this.sheetModel.createOperationGenerator();

        // locally generated style sheet identifiers
        const usedStyleIds = new Set<string>();

        // data of a rule paste item after processing the raw source data passed in "pasteItems"
        interface PreparedPasteItem {
            targetRanges: RangeArray;
            refAddress: Address;
            ruleAttrs: Partial<CFRuleAttributes>;
        }

        // preprocess the new formatting rules (restrict ranges to sheet, generate stylesheets for ODF,
        // generate compatibility replacement formulas, put all entries into a local map)
        const pasteItemMap: Map<string, PreparedPasteItem> = map.from(pasteItems, pasteItem => {

            // duplicate and relocate the source ranges of the rule item according to all target reference cells
            const { addressFactory } = this.docModel;
            let targetRanges = new RangeArray();
            for (const pasteRef of pasteRefs) {
                for (const sourceRange of pasteItem.sourceRanges) {
                    targetRanges.append(addressFactory.getCroppedMovedRange(sourceRange, pasteRef.c, pasteRef.r));
                }
            }
            if (!targetRanges.length) { return undefined; }
            targetRanges = targetRanges.merge();

            // prepare the rule attributes
            const ruleAttrs = { ...pasteItem.ruleAttrs };
            const { type = "", value1 } = ruleAttrs;

            // bug 49320: OOXML does not support year rules, use a replacement formula instead
            let formula = !this.#config.yearRules && YEAR_REPLACEMENT_FORMULAS[type];

            // replacement formulas for standard deviation rules when pasting to an ODF file
            const stdDevFormula = !this.#config.stdDevRules && STDDEV_REPLACEMENT_FORMULAS[type];
            const stdDevCount = (stdDevFormula && is.number(value1)) ? Math.floor(value1) : 0;
            if (stdDevFormula && (stdDevCount >= 1)) { formula = stdDevFormula.replaceAll("#N", String(stdDevCount)); }

            // replace the placeholder tokens in the formula expression, create a new attribute map
            let { refAddress } = pasteItem;
            if (formula) {
                // do not relocate the generated formulas
                refAddress = targetRanges.refAddress()!;
                // insert the reference address for all "#A" tokens
                formula = formula.replaceAll("#A", fun.once(() => {
                    const tokenArray = new TokenArray(this.sheetModel, "rule");
                    return tokenArray.appendAddress(refAddress).getFormulaOp();
                }));
                // insert the target ranges for all "#R" tokens
                formula = formula.replaceAll("#R", fun.once(() => {
                    const tokenArray = new TokenArray(this.sheetModel, "rule");
                    return tokenArray.appendRangeList(targetRanges, { abs: true }).getFormulaOp();
                }));
                // insert the formula expression into the attributes
                ruleAttrs.type = "formula";
                ruleAttrs.value1 = formula;
            }

            // prepare cell attribute set
            const srcAttrSet = ruleAttrs.attrs;
            const newAttrSet: PtCellAttributeSet = dict.create();
            if (!this.#config.cellStyles) {
                // paste into OOXML document: use the explicit formatting attributes, drop stylesheet
                Object.assign(newAttrSet, srcAttrSet);
                delete newAttrSet.styleId;
            } else if (sameDoc) {
                // paste into the same ODF document: re-use the stylesheet (drop explicit formatting attributes)
                if (is.string(srcAttrSet?.styleId)) {
                    newAttrSet.styleId = srcAttrSet.styleId;
                }
            } else {
                // paste into another ODF document: create a new stylesheet from explicit formatting attributes
                const { cellStyles } = this.docModel;
                let styleId: string;
                do {
                    styleId = `condformat${this.#nextStyleIndex}`;
                    this.#nextStyleIndex += 1;
                } while (cellStyles.containsStyleSheet(styleId) || usedStyleIds.has(styleId));
                usedStyleIds.add(styleId);
                newAttrSet.styleId = styleId;
                // generate the stylesheet operations
                const styleAttrSet = { ...srcAttrSet };
                delete styleAttrSet.styleId;
                styleGenerator.generateOperation(Op.INSERT_STYLESHEET, { styleId, attrs: styleAttrSet });
                styleGenerator.generateOperation(Op.DELETE_STYLESHEET, { styleId }, { undo: true });
            }

            // insert a new entry with preprocessed data into the local map
            return [pasteItem.opId, { targetRanges, refAddress, ruleAttrs }];
        });

        // generate the operations to change the target ranges of existing formatting rules
        // DOCS-1941: always in reversed array order to retain correct array indexes (when deleting/restoring rules)
        for (const ruleModel of ary.values(this.#ruleModelArray, { reverse: true })) {

            // the original target ranges covered by the current rule
            const targetRanges = ruleModel.ruleBucket.ranges;

            // the reduced target ranges (without paste ranges)
            const hasOverlap = targetRanges.overlaps(pasteRanges);
            let newTargetRanges = hasOverlap ? targetRanges.difference(pasteRanges) : targetRanges;

            // manipulate target ranges an existing rule copied inside the own sheet
            const pasteItem = sameSheet ? map.remove(pasteItemMap, ruleModel.opId) : undefined;
            if (pasteItem) { newTargetRanges = newTargetRanges.concat(pasteItem.targetRanges).merge(); }

            // comparison by identity is enough here (due to `hasOverlap` flag above)
            if (targetRanges !== newTargetRanges) {
                const oldRefAddress = pasteItem?.refAddress ?? ruleModel.ruleBucket.refAddress;
                const newRefAddress = newTargetRanges.refAddress()!;
                const updateTask = new RelocateRangesUpdateTask(oldRefAddress, newRefAddress);
                ruleModel.generateUpdateTaskOperations(updateGenerator, updateTask, {
                    newRangesStr: newTargetRanges.empty() ? null : this.#formatTargetRanges(newTargetRanges),
                    oldRangesStr: this.#formatTargetRanges(ruleModel.ruleBucket.ranges),
                    updateRanges: true,
                    oldRefAddress,
                    newRefAddress
                });
            }
        }

        // generator function for new unused rule identifiers
        const generateId = this.#createRuleIdGenerator();

        // generate insert operations for the remaining new pasted formatting rules
        const tokenArray = this.sheetModel.createCellTokenArray();
        for (const [origId, pasteItem] of pasteItemMap) {

            // generate an unused identifier for the new rule
            const opId = generateId(origId);

            // relocate all formula expressions according to the target paste ranges
            const refAddress = pasteItem.refAddress;
            const targetAddress = pasteItem.targetRanges.refAddress()!;
            for (const entry of yieldCFRuleFormulas(pasteItem.ruleAttrs)) {
                tokenArray.parseFormula("op:oox", entry.formula, refAddress);
                entry.formula = tokenArray.getFormula("op", refAddress, targetAddress);
            }

            // generate the insert operation, and the respective delete operation for undo
            const rangesStr = this.#formatTargetRanges(pasteItem.targetRanges);
            insertGenerator.generateSheetOperation(Op.DELETE_CFRULE, { id: opId }, { undo: true, prepend: true });
            insertGenerator.generateSheetOperation(Op.INSERT_CFRULE, { id: opId, ranges: rangesStr, ...pasteItem.ruleAttrs });
        }
        tokenArray.destroy();

        // first create new cell styles, then new formatting rules, then update existing rules
        // (the latter may delete some rules which changes array indexes)
        generator.appendOperations(styleGenerator);
        generator.appendOperations(insertGenerator);
        generator.appendOperations(updateGenerator);

        // undo: first restore existing rules (restores array indexes), then delete new pasted rules, then delete ODF styles
        generator.appendOperations(updateGenerator, { undo: true });
        generator.appendOperations(insertGenerator, { undo: true });
        generator.appendOperations(styleGenerator, { undo: true });
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * contents from the passed collection into this collection.
     *
     * @param _context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyCopySheetOperation(_context: SheetOperationContext, fromCollection: CFRuleCollection): void {

        // clone the contents of the source collection
        for (const [ruleIdx, ruleModel] of fromCollection.#ruleModelArray.entries()) {

            // create a new cache model for the target ranges on demand
            const ruleBucket = this.#upsertRuleBucket(ruleModel.ruleBucket.ranges);
            // clone the source rule model
            const cloneModel = new CFRuleModel(this.sheetModel, ruleBucket, ruleModel);

            // insert the rule model into *all* internal containers
            this.#insertRuleModel(cloneModel, ruleIdx);
        }
    }

    /**
     * Callback handler for the document operation "insertCFRule".
     *
     * @param context
     *  A wrapper representing the "insertCFRule" document operation.
     */
    applyInsertOperation(context: SheetOperationContext): void {

        // the rule identifier
        const opId = context.getStr("id");
        let ruleIdx = this.#ruleModelArray.length;

        // DOCS-1941: ODS uses array indexes
        if (this.#config.indexedOps) {
            context.ensure(/^(0|[1-9]\d*)$/.test(opId), 'conditional formatting rule identifier "%s" must be an index', opId);
            ruleIdx = parseInt(opId, 10);
            context.ensure(ruleIdx <= this.#ruleModelArray.length, "invalid conditional formatting rule index %d", ruleIdx);
        } else {
            // the rule identifier must not be used yet in the entire collection
            context.ensure(!this.#ruleModelMap.has(opId), 'conditional formatting rule "%s" exists already', opId);
        }

        // the target ranges (sorted to ensure that the map hits using range array keys)
        const targetRanges = this.#parseTargetRanges(context);
        // the custom reference address (as formula expression)
        const refFormula = this.#config.customRef ? context.optStr("ref") : null;
        // the attributes of the rule
        const ruleAttrs = parseRuleAttrs(context, this.docModel.sheetAttrPool.getDefaultValues("cfrule"));

        // create a new cache model for the target ranges on demand
        const ruleBucket = this.#upsertRuleBucket(targetRanges);
        // create the new rule model
        const ruleModel = new CFRuleModel(this.sheetModel, ruleBucket, opId, ruleAttrs, refFormula, this.#config);

        // insert the rule model into the internal containers
        this.#insertRuleModel(ruleModel, ruleIdx);

        // notify event listeners
        this.trigger("insert:cfrule", this.createSheetEvent({ ruleModel }, context));
        this.sheetModel.refreshRanges(ruleModel.ruleBucket.ranges);
    }

    /**
     * Callback handler for the operation "deleteCFRule".
     *
     * @param context
     *  A wrapper representing the "deleteCFRule" document operation.
     */
    applyDeleteOperation(context: SheetOperationContext): void {

        // resolve the formatting rule
        const opId = context.getStr("id");
        const ruleModel = this.getRuleModel(opId);
        context.ensure(ruleModel, 'missing conditional formatting rule "%s"', opId);

        // notify event listeners
        this.trigger("delete:cfrule", this.createSheetEvent({ ruleModel }, context));
        this.sheetModel.refreshRanges(ruleModel.ruleBucket.ranges);

        // remove the rule model from the internal containers
        this.#removeRuleModel(ruleModel, true);

        // finally, destroy the rule model
        ruleModel.destroy();
    }

    /**
     * Callback handler for the operation "changeCFRule".
     *
     * @param context
     *  A wrapper representing the "changeCFRule" document operation.
     */
    applyChangeOperation(context: SheetOperationContext): void {

        // resolve the formatting rule
        const opId = context.getStr("id");
        const ruleModel = this.getRuleModel(opId);
        context.ensure(ruleModel, 'missing conditional formatting rule "%s"', opId);

        // update internal settings for the rule depending on the rule attributes
        const ruleBucket = ruleModel.ruleBucket;
        ruleBucket.unregisterRuleModel(ruleModel);

        // the new cell formatting attributes of the rule
        const ruleAttrs = parseRuleAttrs(context, ruleModel.getMergedAttributeSet(true).cfrule);
        const attrsChanged = ruleModel.setAttributes({ cfrule: ruleAttrs });

        // update internal settings for the rule depending on the rule attributes
        ruleBucket.registerRuleModel(ruleModel);

        // update target ranges (optional operation property)
        const origRanges = ruleBucket.ranges;
        const targetRanges = this.#parseTargetRanges(context, true);
        const rangesChanged = targetRanges && this.#changeTargetRanges(ruleBucket, targetRanges, ruleModel);

        // resolve optional "ref" property (throws on error), change reference address
        const refFormula = this.#config.customRef ? context.optStr("ref") : null;
        if (refFormula) { ruleModel.setRefFormula(refFormula); }

        // notify event listeners
        if (attrsChanged || rangesChanged || refFormula) {
            this.trigger("change:cfrule", this.createSheetEvent({ ruleModel }, context));
        }
        if (rangesChanged) {
            this.sheetModel.refreshRanges(origRanges, targetRanges);
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns an existing range bucket of a formatting rule, or creates a new
     * range bucket.
     */
    #upsertRuleBucket(targetRanges: RangeArray): CFRuleBucket {
        return map.upsert(this.#ruleBucketMap, targetRanges.key, () => new CFRuleBucket(this.sheetModel, targetRanges));
    }

    /**
     * Updates the operation identifier of a rule model.
     */
    #rekeyRuleModel(ruleIdx: number, opId: string): void {
        const ruleModel = this.#ruleModelArray[ruleIdx];
        map.move(this.#ruleModelMap, ruleModel.opId, opId);
        ruleModel.opId = opId;
    }

    /**
     * Inserts the rule model into the passed range bucket.
     */
    #insertRuleModel(ruleModel: CFRuleModel, ruleIdx?: number): void {

        // insert the rule model into the range bucket (takes ownership)
        ruleModel.ruleBucket.insertRuleModel(ruleModel);

        // insert the rule model into the map and array
        if (is.number(ruleIdx)) {

            // DOCS-1941: indexed mode: update indexes of trailing rules
            if (this.#config.indexedOps) {
                // in reversed order for increasing rule indexes!
                for (let arrIdx = this.#ruleModelArray.length - 1; arrIdx >= ruleIdx; arrIdx -= 1) {
                    this.#rekeyRuleModel(arrIdx, String(arrIdx + 1));
                }
            }

            ary.insertAt(this.#ruleModelArray, ruleIdx, ruleModel);
            this.#ruleModelMap.set(ruleModel.opId, ruleModel);
        }
    }

    /**
     * Removes the rule model from the passed range bucket, but does not
     * destroy it. If the range bucket becomes empty, it will be destroyed
     * though.
     */
    #removeRuleModel(ruleModel: CFRuleModel, updateCont = false): void {

        // remove the rule model from the range bucket
        const ruleBucket = ruleModel.ruleBucket;
        ruleBucket.removeRuleModel(ruleModel);

        // remove and destroy the range bucket if no other rules remain
        if (!ruleBucket.ruleModelSet.size) {
            this.#ruleBucketMap.delete(ruleBucket.rangesKey);
            ruleBucket.destroy();
        }

        // remove the rule model from the map and array
        if (updateCont) {

            const ruleIdx = this.#ruleModelArray.indexOf(ruleModel);
            this.#ruleModelMap.delete(ruleModel.opId);
            ary.deleteAt(this.#ruleModelArray, ruleIdx);

            // DOCS-1941: indexed mode: update indexes of trailing rules
            if (this.#config.indexedOps) {
                for (let arrIdx = ruleIdx, arrLen = this.#ruleModelArray.length; arrIdx < arrLen; arrIdx += 1) {
                    this.#rekeyRuleModel(arrIdx, String(arrIdx));
                }
            }
        }
    }

    /**
     * Extracts the target ranges form the "ranges" property of a document
     * operation.
     *
     * @param context
     *  A wrapper representing a document operation.
     *
     * @param [optional]
     *  Whether the operation property is optional.
     *
     * @returns
     *  The array of cell range addresses, if available; or `undefined`, if the
     *  operation property is optional and missing.
     *
     * @throws
     *  An `OperationError` if the property is required but missing, or if the
     *  property value cannot be parsed to an array of range addresses.
     */
    #parseTargetRanges(context: SheetOperationContext): RangeArray;
    #parseTargetRanges(context: SheetOperationContext, optional: true): Opt<RangeArray>;
    // implementation
    #parseTargetRanges(context: SheetOperationContext, optional?: boolean): Opt<RangeArray> {

        // extract the formula expression for the range list from the operation
        const formula = optional ? context.optStr("ranges") : context.getStr("ranges");
        if (!formula) { return undefined; }

        // parse the expression to an array of cell range addresses
        // Bug 46080: LO exports range lists with commas or semicolons as separator (instead of spaces) to XLSX
        // (see https://bugs.documentfoundation.org/show_bug.cgi?id=99947 for details)
        // Bug 45877: ODF inserts sheet names into the range list, ignore them silently
        const ranges = this.docModel.rangeListParserOP.parse(this.docModel, formula, { extSep: true, skipSheets: this.#config.sheetNames });
        context.ensure(ranges, "invalid target ranges");
        return ranges;
    }

    /**
     * Returns the string representation of the passed target ranges, as used
     * in document operations.
     *
     * @param targetRanges
     *  The target ranges to be formatted.
     *
     * @param [sheetName]
     *  A custom sheet name to be added to the range list in ODF mode.
     *
     * @returns
     *  The string representation of the passed target ranges, as used in
     *  document operations.
     */
    #formatTargetRanges(targetRanges: Range[], sheetName?: string): string {
        // in ODF documents, the sheet name is part of the target ranges
        if (this.#config.sheetNames) { sheetName ||= this.sheetModel.getName(); }
        return this.docModel.rangeListParserOP.format(this.docModel, targetRanges, { sheetName });
    }

    /**
     * Changes the target ranges of the passed formatting rule.
     */
    #changeTargetRanges(ruleBucket: CFRuleBucket, targetRanges: RangeArray, ruleModel: CFRuleModel): boolean {

        // get or create the range bucket for the rule
        const newRuleBucket = this.#upsertRuleBucket(targetRanges);
        if (ruleBucket === newRuleBucket) { return false; }

        // move the rule model to the new range bucket
        this.#removeRuleModel(ruleModel);
        ruleModel.ruleBucket = newRuleBucket;
        this.#insertRuleModel(ruleModel);

        // bug 49542: invalidate all cached formula results in the rules
        ruleModel.clearResultCache();

        return true;
    }

    #createRuleIdGenerator(): (origId: string) => string {

        // indexed mode: append the new rules to the array
        if (this.#config.indexedOps) {
            let nextRuleIdx = this.#ruleModelArray.length;
            return () => {
                const opId = String(nextRuleIdx);
                nextRuleIdx += 1;
                return opId;
            };
        }

        // identifier mode: generate rule identifiers with random GUIDs
        const usedOpIds = new Set<string>();
        return origId => {
            // in OOXML, the rule identifier starts with an internal rule type character
            let opId: string;
            const idPrefix = /^[ABC]\{.+\}$/.test(origId) ? origId[0] : "A";
            // generate an unused rule identifier
            do {
                opId = idPrefix + uuid.random(true);
            } while (this.#ruleModelMap.has(opId) || usedOpIds.has(opId));
            usedOpIds.add(opId);
            return opId;
        };
    }
}
