/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ReverseOptions } from "@/io.ox/office/tk/algorithms";
import { IntervalPosition } from "@/io.ox/office/tk/dom";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { SheetPixelOptions } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import { ColAttributes, RowAttributes, ColRowAttributeSet, ColRowModel, ColRowDescriptor } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";

// types ======================================================================

export interface ModelIteratorOptions extends ReverseOptions {
    visible?: boolean;
}

export interface BandModelEntry<KT extends "column" | "row"> {
    model: ColRowModel<KT>;
    interval: Interval;
    offsetHmm: number;
    offset: number;
}

export interface EntryIteratorOptions extends ModelIteratorOptions {
    unique?: boolean;
}

export enum VisibleEntryMethod {
    EXACT = "exact",
    NEXT = "next",
    PREV = "prev",
    NEXT_PREV = "nextPrev",
    PREV_NEXT = "prevNext"
}

export interface EntryOffsetOptions extends SheetPixelOptions {
    outerHidden?: boolean;
}

export interface EntryOffsetDescriptor<KT extends "column" | "row"> extends ColRowDescriptor<KT> {
    relOffsetHmm: number;
    relOffset: number;
}

export interface ChangeSetInterval extends Interval {
    fillData: {
        attrs: Partial<ColAttributes | RowAttributes>;
    };
}

/**
 * Event data that will be fired after inserting, deleting, or changing columns
 * or rows in a `ColRowCollection`.
 */
export interface ColRowEvent extends SheetEventBase {

    /**
     * The sorted index intervals of the inserted, deleted, or changed
     * columns/rows.
     */
    intervals: IntervalArray;
}

/**
 * Event data that will be fired after changing columns or rows in a
 * `ColRowCollection`.
 */
export interface ChangeColRowEvent extends ColRowEvent {

    /**
     * Whether the explicit column/row attributes of the intervals have been
     * changed.
     */
    attributes: boolean;

    /**
     * Whether the effective size of a column/row in the intervals has been
     * changed, including changed visibility of the columns or rows (hidden
     * columns/rows have effective size of zero).
     */
    size: boolean;

    /**
     * Whether the visibility of a column/row in the intervals has been changed
     * (the property `size` of this object will also be `true` in this case,
     * see above).
     */
    visibility: boolean;

    /**
     * Whether the cell auto-style associated to all undefined cells in the
     * intervals has been changed.
     */
    style: boolean;
}

/**
 * Event data that will be fired after moving (inserting or deleting) columns
 * or rows in a `ColRowCollection`.
 */
export interface MoveColRowEvent extends ColRowEvent {

    /**
     * Whether the columns/rows have been inserted (`true`), or deleted
     * (`false`).
     */
    insert: boolean;
}

/**
 * Type mapping for the events emitted by `ColRowCollection` instances.
 */
export interface ColRowCollectionEventMap {

    /**
     * Will be emitted after the formatting attributes, or the autostyle, of
     * columns/rows have been changed.
     *
     * @param event
     *  An event object with the intervals of all changed columns/rows.
     */
    "change:intervals": [event: ChangeColRowEvent];

    /**
     * Will be emitted after columns/rows have been moved in the sheet.
     *
     * @param event
     *  An event object with the intervals of all moved columns/rows.
     */
    "move:intervals": [event: MoveColRowEvent];
}

// class ColRowCollection =====================================================

export abstract class ColRowCollection<KT extends "column" | "row" = "column" | "row"> extends SheetChildModel<ColRowCollectionEventMap> {

    readonly defaultModel: ColRowModel<KT>;
    readonly maxIndex: number;

    protected constructor(sheetModel: SheetModel, columns: boolean);

    getFullInterval(): Interval;
    getTotalSize(options?: SheetPixelOptions): number;

    getModel(index: number): ColRowModel<KT>;

    yieldBandModels(interval: Interval, options?: ModelIteratorOptions): IterableIterator<BandModelEntry<KT>>;
    intervals(bounds: IntervalSource, options?: ModelIteratorOptions): IterableIterator<Interval>;
    indexes(bounds: IntervalSource, options?: ModelIteratorOptions): IterableIterator<number>;
    countIndexes(bounds: IntervalSource, options?: ModelIteratorOptions): number;

    getMixedAttributes(intervals: IntervalSource): ColRowAttributeSet[KT];

    isVisibleIndex(index: number): boolean;
    findNextVisibleIndex(index: number, lastIndex?: number): Opt<number>;
    findPrevVisibleIndex(index: number, firstIndex?: number): Opt<number>;
    findVisibleIndex(index: number, method?: VisibleEntryMethod): Opt<number>;

    isIntervalHidden(interval: Interval): boolean;
    getVisibleIntervals(bounds: IntervalSource): IntervalArray;
    shrinkIntervalToVisible(interval: Interval): Interval | null;
    expandIntervalToHidden(interval: Interval): Interval;
    mergeAndShrinkIntervals(intervals: IntervalSource): IntervalArray;

    /** @deprecated */
    getEntry(index: number): ColRowDescriptor<KT>;
    /** @deprecated */
    indexEntries(interval: Interval, options?: EntryIteratorOptions): IterableIterator<ColRowDescriptor<KT>>;

    getEntryOffsetHmm(index: number, offsetHmm: number): number;
    getEntryOffset(index: number, offsetHmm: number): number;
    getEntryByOffset(offset: number, options?: EntryOffsetOptions): EntryOffsetDescriptor<KT>;
    getScrollAnchorByOffset(offset: number, options?: EntryOffsetOptions): number;
    convertOffsetToHmm(offset: number): number;
    convertOffsetToPixel(offsetHmm: number): number;
    convertScrollAnchorToHmm(scrollAnchor: number): number;
    convertScrollAnchorToPixel(scrollAnchor: number): number;
    getEntryPosition(index: number, options?: SheetPixelOptions): IntervalPosition;
    getIntervalPosition(interval: Interval, options?: SheetPixelOptions): IntervalPosition;

    applyCopySheetOperation(context: SheetOperationContext, fromCollection: this): void;
    applyInsertOperation(context: SheetOperationContext): void;
    applyDeleteOperation(context: SheetOperationContext): void;
    applyChangeOperation(context: SheetOperationContext): void;

    checkMaxIntervalSize(intervals: IntervalSource): JPromise | null;
    generateMoveOperations(sheetCache: SheetCache, transformer: AddressTransformer): void;
    generateIntervalOperations(generator: SheetOperationGenerator, intervals: ChangeSetInterval[]): JPromise;
}

// class ColCollection ========================================================

export class ColCollection extends ColRowCollection<"column"> {
    public constructor(sheetModel: SheetModel);
}

// class RowCollection ========================================================

export class RowCollection extends ColRowCollection<"row"> {
    public constructor(sheetModel: SheetModel);

    postProcessImport(): Promise<void>;
}
