/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";
import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";

import type { DocAttributePoolMap, SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface PageAttributes {

    /**
     * Total width of a single page, in 1/100 of millimeters.
     */
    width: number;

    /**
     * Total height of a single page, in 1/100 of millimeters.
     */
    height: number;
}

export interface PageAttributeSet {
    page: PageAttributes;
}

export type PtPageAttributeSet = PtAttrSet<PageAttributeSet>;

// constants ==================================================================

const PAGE_ATTRIBUTES: AttributeConfigBag<PageAttributes> = {
    width: { def: 21000 },
    height: { def: 29700 }
};

// class PageStyleCollection ==================================================

/**
 * A collection of style sheets for page attributes. Expected to exist by the
 * text framework that implements common text formatting on drawing objects.
 */
export class PageStyleCollection extends StyleCollection<SpreadsheetModel, DocAttributePoolMap, PageAttributeSet> {

    /**
     * @param docModel
     *  The spreadsheet document model containing this instance.
     */
    constructor(docModel: SpreadsheetModel) {

        // base constructor
        super(docModel, "page");

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily("page", PAGE_ATTRIBUTES);
    }
}
