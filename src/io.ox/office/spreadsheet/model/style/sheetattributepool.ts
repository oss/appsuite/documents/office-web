/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AttributePool } from "@/io.ox/office/editframework/model/attributepool";

import type { CharacterAttributes, CellAttributes, ApplyAttributes, ExtCellAttrConfig } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { CHARACTER_ATTRIBUTES, CELL_ATTRIBUTES, APPLY_ATTRIBUTES } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import type { GroupAttributes, ColAttributes, RowAttributes, ExtColRowAttrConfig } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import { GROUP_ATTRIBUTES, COL_ATTRIBUTES, ROW_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import { type TableAttributes, TABLE_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/tablemodel";
import type { TableFilterAttributes, TableSortAttributes } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import { FILTER_ATTRIBUTES, SORT_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import { type DVRuleAttributes, DVRULE_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/dvrulemodel";
import { type CFRuleAttributes, CFRULE_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import { type SheetAttributes, type ExtSheetAttrConfig, SHEET_ATTRIBUTES } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Type configuration map for all formatting attributes used in the secondary
 * attribute pool of spreadsheet documents (class `SheetAttributePool`).
 */
export interface SheetAttributePoolMap {
    character:  [CharacterAttributes, ExtCellAttrConfig];
    cell:       [CellAttributes,      ExtCellAttrConfig];
    apply:      [ApplyAttributes];
    group:      [GroupAttributes];
    column:     [ColAttributes,       ExtColRowAttrConfig];
    row:        [RowAttributes,       ExtColRowAttrConfig];
    table:      [TableAttributes];
    filter:     [TableFilterAttributes];
    sort:       [TableSortAttributes];
    dvrule:     [DVRuleAttributes];
    cfrule:     [CFRuleAttributes];
    sheet:      [SheetAttributes,     ExtSheetAttrConfig];
}

// class SheetAttributePool ===================================================

/**
 * An additional attribute pool for spreadsheet documents. Contains attribute
 * families with names colliding with the document's default attribute pool
 * used in the text framework.
 */
export class SheetAttributePool extends AttributePool<SheetAttributePoolMap> {

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super();

        this.registerAttrFamily("character", CHARACTER_ATTRIBUTES);
        this.registerAttrFamily("cell",      CELL_ATTRIBUTES);
        this.registerAttrFamily("apply",     APPLY_ATTRIBUTES);
        this.registerAttrFamily("group",     GROUP_ATTRIBUTES);
        this.registerAttrFamily("column",    COL_ATTRIBUTES);
        this.registerAttrFamily("row",       ROW_ATTRIBUTES);
        this.registerAttrFamily("table",     TABLE_ATTRIBUTES);
        this.registerAttrFamily("filter",    FILTER_ATTRIBUTES);
        this.registerAttrFamily("sort",      SORT_ATTRIBUTES);
        this.registerAttrFamily("dvrule",    DVRULE_ATTRIBUTES);
        this.registerAttrFamily("cfrule",    CFRULE_ATTRIBUTES);
        this.registerAttrFamily("sheet",     SHEET_ATTRIBUTES);

        // forward character pool defaults imported from document operation
        docModel.defAttrPool.on("change:defaults", changes => {
            if (changes.character) {
                this.getFamilyPool("character").changeDefaults(changes.character);
            }
        });
    }
}
