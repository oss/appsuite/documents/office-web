/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { SMALL_DEVICE, convertHmmToLength, convertLength, convertLengthToHmm, iterateDescendantNodes } from '@/io.ox/office/tk/utils';
import { NODE_SELECTOR, getDrawingNode, isAutoResizeHeightDrawingFrame, isNoWordWrapDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { LineHeight } from '@/io.ox/office/editframework/utils/lineheight';
import { isHardBreakNode, iterateTextSpans } from '@/io.ox/office/textframework/utils/dom';
import ParagraphStyles from '@/io.ox/office/textframework/format/paragraphstyles';

// constants ==================================================================

// definitions for paragraph attributes
const DEFINITIONS = {

    // list level
    level: { def: 0 },

    lineHeight: { def: LineHeight._115 },
    // workaround for bug 47960 & bug 45951

    listLabelHidden: { def: false },

    listStartValue: { def: -1 },

    outlineLevel: { def: 9 },

    bullet: { def: {} },

    bulletFont: { def: { followText: true } },

    bulletColor: { def: { followText: true } },

    bulletSize: { def: { type: 'followText', size: 0 } },

    // default size for tabulators (per paragraph in spreadsheets)
    defaultTabSize: { def: 2540 },

    spacingBefore: { def: 0 },

    spacingAfter: { def: 0 }
};

const PARAGRAPH_ALIGNMENT_MAPPING = {
    center:  'center',
    left:    'flex-start',
    right:   'flex-end',
    justify: 'stretch'
};

// class ParagraphStyleCollection =============================================

/**
 * Implementation helper for paragraph formatting in drawing objects. The CSS
 * formatting will be read from and written to DOM paragraph elements.
 */
export class ParagraphStyleCollection extends ParagraphStyles {

    /**
     * @param {SpreadsheetModel} docModel
     *  The spreadsheet document model containing this instance.
     */
    constructor(docModel) {

        // base constructor
        super(docModel, {
            families: ['character'],
            parentResolvers: { drawing: getDrawingNode }
        });

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('paragraph', DEFINITIONS);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updateParagraphFormatting);
    }

    // private methods --------------------------------------------------------

    /**
     * Will be called for every paragraph whose character attributes have been
     * changed.
     *
     * @param {JQuery} paragraph
     *  The paragraph node whose attributes have been changed.
     *
     * @param {Dict} mergedAttributes
     *  A complete attribute set containing the effective attribute values
     *  merged from style sheets and explicit attributes.
     */
    #updateParagraphFormatting(paragraph, mergedAttributes) {

        var // the paragraph attributes of the passed attribute map
            paragraphAttributes = mergedAttributes.paragraph,

            leftMargin = 0,
            rightMargin = 0,

            nextParagraph = paragraph.next(),
            collectedAttrs =  {},
            textIndent = 0;

        const calcSpacingHmm = lineHeight => {
            // fix for Bug 48442
            if (!lineHeight.value) { return 0; }

            var height = 0;
            var fontHeight = convertLength(mergedAttributes.character.fontSize, 'pt', 'px');

            if (lineHeight.type === 'fixed') {
                height = convertHmmToLength(lineHeight.value, 'px');
            } else if (lineHeight.type === 'percent') {
                height = fontHeight * lineHeight.value / 100;
            } else {
                globalLogger.warn('ParagraphStyles.calcSpacingHmm(): invalid line height type', lineHeight.type);
                return 0;
            }

            // use zoom level as workaround for bug 47960 & Bug 48476
            var fontDesc = this.docModel.getRenderFont(mergedAttributes.character, 10);
            var normalLineHeight = fontDesc.getNormalLineHeight() / 10;
            height -= (normalLineHeight - fontHeight);
            height = Math.max(0, height);
            return convertLengthToHmm(height, 'px');
        };

        // Bug 49446, Bug 52808 & Bug 52475
        var drawingNode = paragraph.closest(NODE_SELECTOR);
        if (this.docApp.isOOXML() && isNoWordWrapDrawingFrame(drawingNode) && !isAutoResizeHeightDrawingFrame(drawingNode)) {
            collectedAttrs.alignSelf = PARAGRAPH_ALIGNMENT_MAPPING[paragraphAttributes.alignment] || '';
        }

        // Always update character formatting of all child nodes which may
        // depend on paragraph settings, e.g. automatic text color which
        // depends on the paragraph fill color. Also visit all helper nodes
        // containing text spans, e.g. numbering labels.
        const { characterStyles } = this.docModel;
        iterateDescendantNodes(paragraph, node => {

            // visiting the span inside a hard break node
            // -> this is necessary for change tracking attributes
            if (isHardBreakNode(node)) {
                characterStyles.updateElementFormatting(node.firstChild);
            }

            iterateTextSpans(node, span => {
                characterStyles.updateElementFormatting(span, { baseAttributes: mergedAttributes });
            });
        }, undefined, { children: true });

        // update borders
        var leftPadding = 0;
        var topMargin = 0;
        var bottomMargin = 0;

        //calculate list indents
        //            var listLabel = $(paragraph).children(DOM.LIST_LABEL_NODE_SELECTOR);
        //            var listStyleId = paragraphAttributes.listStyleId;
        //            if (listStyleId.length) {
        //                var listLevel = paragraphAttributes.listLevel,
        //                    lists = docModel.getListCollection();
        //                if (listLevel < 0) {
        //                    // is a numbering level assigned to the current paragraph style?
        //                    listLevel = lists.findIlvl(listStyleId, mergedAttributes.styleId);
        //                }
        //                if (listLevel !== -1 && listLevel < 10) {
        //                    var listItemCounter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        //                    var listObject = lists.formatNumber(paragraphAttributes.listStyleId, listLevel, listItemCounter, 1);
        //
        //                    if (listObject.indent > 0) {
        //                        leftPadding += listObject.indent - listObject.firstLine;
        //                        leftMargin += listObject.firstLine;
        //                    }
        //                    if (listLabel.length) {
        //                        var listSpan = listLabel.children('span');
        //                        if (listObject.fontSize) {
        //                            listSpan.css('font-size', listObject.fontSize + 'pt');
        //                        }
        //                        if (listObject.color) {
        //                            listSpan.css('color', docModel.getCssTextColor(listObject.color, [paragraphAttributes.fillColor, listObject.fillColor]));
        //                        }
        //                    }
        //                } else {
        //                    //fix for Bug 37594 some list-helper dont disappear
        //                    listLabel.detach();
        //                }
        //            } else {
        //                //fix for Bug 37594 some list-helper dont disappear
        //                listLabel.detach();
        //            }

        // paragraph margin attributes - also applying to paragraphs in a list, if they are defined as explicit attribute
        // -> handle both cases correctly: 40792 and 41118
        // if (listStyleId === '') {
        leftMargin += paragraphAttributes.indentLeft ? paragraphAttributes.indentLeft : 0;
        rightMargin += paragraphAttributes.indentRight ? paragraphAttributes.indentRight : 0;
        textIndent = paragraphAttributes.indentFirstLine ? paragraphAttributes.indentFirstLine : 0;
        collectedAttrs.textIndent = textIndent / 100 + 'mm';
        //            } else {
        //                // but explicit attributes need to be handled (40792)
        //                explicitParaAttributes = AttributeUtils.getExplicitAttributeSet(paragraph);
        //                explicitParaAttributes = (explicitParaAttributes && explicitParaAttributes.paragraph) || {};
        //                if (explicitParaAttributes.indentLeft) { leftMargin += explicitParaAttributes.indentLeft; }
        //                if (explicitParaAttributes.indentRight) { rightMargin += explicitParaAttributes.indentRight; }
        //                if (explicitParaAttributes.indentFirstLine) {
        //                    textIndent += explicitParaAttributes.indentRight;
        //                    collectedAttrs.textIndent = textIndent / 100 + 'mm';
        //                }
        //            }

        if (textIndent < 0) {
            leftPadding -= textIndent;
            leftMargin += textIndent;
        }

        collectedAttrs.paddingLeft = (leftPadding / 100) + 'mm';
        collectedAttrs.marginLeft = (leftMargin / 100) + 'mm';
        collectedAttrs.marginRight = (rightMargin / 100) + 'mm';
        collectedAttrs.textIndent = (textIndent / 100) + 'mm';

        // Overwrite of margin left for lists in draft mode (conversion from fixed unit mm to %)
        if (SMALL_DEVICE && paragraph.data('draftRatio')) {
            collectedAttrs.marginLeft = (parseInt($(paragraph).css('margin-left'), 10) * paragraph.data('draftRatio')) + '%';
        }

        if (paragraphAttributes.spacingBefore) {
            topMargin += calcSpacingHmm(paragraphAttributes.spacingBefore);
        }
        if (paragraphAttributes.spacingAfter && nextParagraph.length) {
            bottomMargin += calcSpacingHmm(paragraphAttributes.spacingAfter);
        }

        collectedAttrs.marginTop = (topMargin / 100) + 'mm';
        collectedAttrs.marginBottom = (bottomMargin / 100) + 'mm';
        collectedAttrs.width = null;

        // apply collected attributes at the end
        paragraph.css(collectedAttrs);

        // update the size of all tab stops in this paragraph
        this.implUpdateTabStops(paragraph, mergedAttributes);
    }
}
