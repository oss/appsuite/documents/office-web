/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, dict } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { type OpBorder, isVisibleBorder } from "@/io.ox/office/editframework/utils/border";
import { cloneAttributeSet, insertAttribute } from "@/io.ox/office/editframework/utils/attributeutils";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import { type AutoStyleEventType, type AutoStyleModel, AutoStyleCollection } from "@/io.ox/office/editframework/model/autostylecollection";

import type { CellAttributes, ApplyAttributes, CellAttributeSet, PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { type AttrBorderKey, ATTR_BORDER_KEYS, getBorderName } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import type { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";
import type { SheetAttributePoolMap } from "@/io.ox/office/spreadsheet/model/style/sheetattributepool";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * The visible border attributes of an auto-style, as (incomplete) dictionary.
 * Each visible border is mapped by the one-character key for the border ("t",
 * "b", "l", "r", "d", and "u"). Invisible borders are not contained in the
 * dictionary (the dictionary may even be empty).
 */
export type AutoStyleBorders = PtRecord<AttrBorderKey, OpBorder>;

export type CellAutoStyleModel = AutoStyleModel<SpreadsheetModel, SheetAttributePoolMap, CellAttributeSet>;

// private types --------------------------------------------------------------

/**
 * Additional calculated data for an existing auto-style.
 */
interface AutoStyleMetaData {
    cellAttrs: CellAttributes;
    parsedFormat: ParsedFormat;
    borderMap: AutoStyleBorders;
}

// class CellAutoStyleCollection ==============================================

/**
 * Contains the auto-styles with cell/character formatting attributes for
 * all cells, columns, and rows of a spreadsheet document.
 *
 * @param docModel
 *  The document model containing this instance.
 */
export class CellAutoStyleCollection extends AutoStyleCollection<SpreadsheetModel, SheetAttributePoolMap, CellAttributeSet> {

    // properties -------------------------------------------------------------

    // the number formatter of the document
    readonly #numberFormatter: NumberFormatter;

    // a cache for additional calculated data for all existing auto-styles
    readonly #metaDataCache = new Map<string, AutoStyleMetaData>();

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {

        const ooxml = docModel.docApp.isOOXML();
        const standardPrefix = ooxml ? "a" : "ce";

        // super constructor
        super(docModel, "cell", { standardPrefix, indexedMode: ooxml, keepComplete: ooxml });

        // initialize properties
        this.#numberFormatter = docModel.numberFormatter;

        // update cached parsed number formats after changing UI locale
        this.listenTo(this.#numberFormatter, "change:presets", () => {
            this.#metaDataCache.forEach(metaData => {
                metaData.parsedFormat = this.#numberFormatter.getParsedFormatForAttributes(metaData.cellAttrs);
            });
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the parsed number format instance of an auto-style.
     *
     * @param styleId
     *  The style identifier of an auto-style. The empty string can be used for
     *  the default auto-style of this collection.
     *
     * @param [useUid=false]
     *  If set to `true`, the parameter `styleId` will be interpreted as the
     *  unique object identifier of an auto-style (the property `uid` of the
     *  auto-style model).
     *
     * @returns
     *  The parsed number format instance of the auto-style.
     */
    getParsedFormat(styleId: string, useUid?: boolean): ParsedFormat {
        const metaData = this.#getMetaData(styleId, useUid);
        return metaData ? metaData.parsedFormat : this.#numberFormatter.standardFormat;
    }

    /**
     * Returns the visible border attributes of an auto-style in a map with
     * short single-character border keys.
     *
     * @param styleId
     *  The style identifier of an auto-style. The empty string can be used for
     *  the default auto-style of this collection.
     *
     * @param [useUid=false]
     *  If set to `true`, the parameter `styleId` will be interpreted as the
     *  unique object identifier of an auto-style (the property `uid` of the
     *  auto-style model).
     *
     * @returns
     *  The visible border attributes of the auto-style, as (incomplete) map.
     *  Each visible border is mapped by the one-character key for the border
     *  ("t", "b", "l", "r", "d", and "u"). Invisible borders are not contained
     *  in the map (the dictionary may be empty).
     */
    getBorderMap(styleId: string, useUid?: boolean): AutoStyleBorders {
        const metaData = this.#getMetaData(styleId, useUid);
        return metaData ? metaData.borderMap : dict.create();
    }

    /**
     * Extends the passed attribute set in-place with formatting attributes
     * resolved from a table style sheet. These attributes will be merged
     * "under" the explicit attributes.
     *
     * @param attrSet
     *  The partial cell attribute set to be extended in-place with the passed
     *  table style attributes.
     *
     * @param mergedAttrSet
     *  The (complete) merged cell attribute set used to decide whether the
     *  passed table attributes will become part of the extended attribute set.
     *  Only the attributes where this merged attribute set contains the same
     *  values as the default auto-style can be extended with the table style
     *  attributes.
     *
     * @param tableAttrSet
     *  The attribute set resolved from a table style sheet to be merged into
     *  the passed partial attribute set.
     *
     * @returns
     *  The attribute set passed to this method, that has been extended
     *  in-place.
     */
    extendWithTableAttributeSet(attrSet: PtCellAttributeSet, mergedAttrSet: Readonly<CellAttributeSet>, tableAttrSet: Readonly<PtCellAttributeSet>): Dict {
        const defaultAttrSet = this.getDefaultAttributeSet();
        dict.forEach(tableAttrSet, (tableAttrs, family) => {
            const mergedAttrs = mergedAttrSet[family];
            const defaultAttrs = defaultAttrSet[family];
            if (is.dict(tableAttrs) && is.dict(mergedAttrs) && is.dict(defaultAttrs)) {
                const targetAttrs = is.dict(attrSet[family]) ? (attrSet[family] as Dict) : ((attrSet as Dict)[family] = dict.create());
                dict.forEach(tableAttrs, (value, name) => {
                    if ((name in mergedAttrs) && _.isEqual(mergedAttrs[name], defaultAttrs[name])) {
                        targetAttrs[name] = value;
                    }
                });
            }
        });
        return attrSet;
    }

    // operation generators ---------------------------------------------------

    /**
     * Returns the identifier of an auto-style that contains the formatting
     * attributes of an existing auto-style, merged with the passed attribute
     *  set. If the auto-style does not exist yet, it will be created, and the
     *  appropriate remote operations will be inserted into the passed
     *  generators.
     *
     * @param styleCache
     *  The operation cache to be filled with the operations.
     *
     * @param styleId
     *  The style identifier of an auto-style. The empty string can be used for
     *  the default auto-style of this collection.
     *
     * @param attrSet
     *  The partial attribute set with formatting attributes to be merged into
     *  the new auto-style.
     *
     * @returns
     *  The identifier of the resulting auto-style containing the merged
     *  formatting attributes.
     */
    override generateAutoStyleOperations(styleCache: SheetStyleCache, styleId: string, attrSet: PtCellAttributeSet): string {

        // the merged attribute set of the auto-style
        const mergedAttrSet = this.getMergedAttributeSet(styleId);
        // special behavior depending on file format
        const fileFormat = this.docModel.docApp.getFileFormat();

        // apply flags cannot be set by outer code (will be calculated below)
        attrSet = cloneAttributeSet(attrSet);
        delete attrSet.apply;

        // Update the "apply" flags in the explicit attribute set for OOXML files. The "apply" flags mark entire groups
        // of attributes that have been set explicitly, and override the attributes of the cell style sheet. In ODF
        // files, the "apply" flags are not used at all.
        //
        if (fileFormat === FileFormatType.OOX) {
            const applyFlags = dict.createPartial<ApplyAttributes>();
            for (const family of ["character", "cell"] as const) {
                const attrs = attrSet[family];
                if (attrs) {
                    const pool = this.attrPool.getFamilyPool(family);
                    for (const { entry } of pool.yieldAttrs(attrs)) {
                        applyFlags[entry.config.apply] = true;
                    }
                }
            }
            attrSet.apply = applyFlags;
        }

        // Apply a new style sheet. In OOXML files, the "apply" flags of the style sheet specify which explicit
        // attributes of the auto-style have to be removed in order to let the style sheet attributes take effect. In
        // ODF files, all explicit attributes will be removed, and the explicit attributes of the new style sheet will
        // take effect only.
        //
        if (is.string(attrSet.styleId)) {

            const styleAttrSet = this.styleSheets.getStyleAttributeSet(attrSet.styleId);

            switch (fileFormat) {

                case FileFormatType.OOX: {

                    const styleApplyAttrs = styleAttrSet.apply;
                    const mergedApplyAttrs = mergedAttrSet.apply;

                    // copy all formatting attributes with active "apply" flag from the style sheet to the attribute set
                    for (const family of ["character", "cell"] as const) {
                        const pool = this.attrPool.getFamilyPool(family);
                        for (const { name, value, entry } of pool.yieldAttrs(styleAttrSet[family])) {
                            if (styleApplyAttrs[entry.config.apply]) {
                                insertAttribute(attrSet, family, name, value);
                            }
                        }
                    }

                    // reset the explicit "apply" flags of the auto-style that are set in the style sheet
                    dict.forEach(styleApplyAttrs, (apply, name) => {
                        if (apply && mergedApplyAttrs[name]) {
                            // keep the existing flags added above for new explicit attributes
                            insertAttribute(attrSet, "apply", name, false, true);
                        }
                    });
                    break;
                }

                case FileFormatType.ODF: {

                    // ODF: clear all existing explicit attributes (set all missing attributes to null)
                    const expAttrSet = this.getExplicitAttributeSet(styleId);
                    dict.forEach(expAttrSet, (expAttrs, family) => {
                        if (this.styleSheets.isSupportedFamily(family) && is.dict(expAttrs)) {
                            for (const name in expAttrs) {
                                insertAttribute(attrSet, family, name, null, true);
                            }
                        }
                    });
                    break;
                }
            }
        } else {
            delete attrSet.styleId;
        }

        // invoke the base class method to create the auto-style
        return super.generateAutoStyleOperations(styleCache, styleId, attrSet);
    }

    // protected methods ------------------------------------------------------

    /**
     * Extends auto-styles of this collection with specific properties for
     * spreadsheet cells.
     */
    protected override updateHandler(styleUid: string, eventType: AutoStyleEventType): void {

        // when deleting an auto-style, remove the additional data from the cache
        if (eventType === "delete:autostyle") {
            this.#metaDataCache.delete(styleUid);
            return;
        }

        // the effective merged attributes of the auto-style (resolve by UID)
        const mergedAttrSet = this.getMergedAttributeSet(styleUid, true);
        const cellAttrs = mergedAttrSet.cell;

        // resolve the parsed number format for fast access
        const parsedFormat = this.#numberFormatter.getParsedFormatForAttributes(cellAttrs);

        // cache the visible border attributes in a map with short border keys
        const borderMap = dict.generate(ATTR_BORDER_KEYS, borderKey => {
            const border = cellAttrs[getBorderName(borderKey)];
            return isVisibleBorder(border) ? border : undefined;
        });

        // insert the data into the cache
        this.#metaDataCache.set(styleUid, { cellAttrs, parsedFormat, borderMap });
    }

    // private methods --------------------------------------------------------

    #getMetaData(styleId: string, useUid?: boolean): Opt<AutoStyleMetaData> {
        const styleUid = useUid ? styleId : this.resolveStyleUid(styleId);
        return styleUid ? this.#metaDataCache.get(styleUid) : undefined;
    }
}
