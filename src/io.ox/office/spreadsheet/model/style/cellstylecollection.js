/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { opRgbColor } from "@/io.ox/office/editframework/utils/color";
import { THIN_WIDTH_HMM, MEDIUM_WIDTH_HMM, THICK_WIDTH_HMM } from "@/io.ox/office/editframework/utils/border";
import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";

// class CellStyleCollection ==================================================

/**
 * Contains the cell style sheets of a spreadsheet document.
 */
export class CellStyleCollection extends StyleCollection {

    // the identifier of the built-in "Hyperlink" style
    #hyperlinkStyleId = "";

    // constructor ------------------------------------------------------------

    /**
     * @param {SpreadsheetModel} docModel
     *  The document model containing this instance.
     */
    constructor(docModel) {
        super(docModel, "cell", {
            attrPool: docModel.sheetAttrPool,
            families: ["character", "apply"]
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates all missing built-in cell styles.
     *
     * _Attention:_ Not intended to be called from any other code than the
     * post-process handler of the application.
     */
    createMissingStyles() {

        // the merged attribute set of the default style
        const attributes = this.getDefaultStyleAttributeSet();
        // the default font name of the document
        const defFontName = attributes.character.fontName;

        // JSON color helper functions
        const schemeColor = (id, shade) => this.docModel.createSchemeColor(id, shade);

        // predefined colors
        const DARK1 = schemeColor("dark1", 0);
        const DARK2 = schemeColor("dark2", 0);
        const LIGHT1 = schemeColor("light1", 0);
        const ACCENT1 = schemeColor("accent1", 0);
        const ACCENT1_40 = schemeColor("accent1", 40);

        // JSON border helper functions
        const singleBorder = (width, color) => ({ style: "single", width, color });
        const thinBorder = color => singleBorder(THIN_WIDTH_HMM, color);
        const mediumBorder = color => singleBorder(MEDIUM_WIDTH_HMM, color);
        const thickBorder = color => singleBorder(THICK_WIDTH_HMM, color);
        const doubleBorder = color => ({ style: "double", width: THICK_WIDTH_HMM, color });

        // predefined border values
        const LIGHT_GRAY_BORDER  = thinBorder(opRgbColor("B2B2B2"));
        const MEDIUM_GRAY_BORDER = thinBorder(opRgbColor("7F7F7F"));
        const DARK_GRAY_BORDER   = thinBorder(opRgbColor("3F3F3F"));
        const DOUBLE_GRAY_BORDER = doubleBorder(opRgbColor("3F3F3F"));

        // JSON attribute helper functions
        const charAttrs = (color, attrs) => ({ fontName: defFontName, color, ...attrs });
        const fillAttrs = (color, border) => ({ fillType: "solid", fillColor: color, ...(border ? { borderTop: border, borderBottom: border, borderLeft: border, borderRight: border } : null) });

        // predefined combinations for apply flags (default value for apply flags is true)
        const APPLY_FONT             = { fill: false, border: false, align: false, number: false, protect: false };
        const APPLY_FONT_FILL        = { border: false, align: false, number: false, protect: false };
        const APPLY_FONT_BORDER      = { fill: false, align: false, number: false, protect: false };
        const APPLY_FONT_FILL_BORDER = { align: false, number: false, protect: false };
        const APPLY_FILL_BORDER      = { font: false, align: false, number: false, protect: false };

        // standard styles
        this.#createStyleSheet("Standard", "markup", 1, {});
        this.#createStyleSheet("Positive", "markup", 2, { character: charAttrs(opRgbColor("006100")), cell: fillAttrs(opRgbColor("C6EFCE")) }, APPLY_FONT_FILL);
        this.#createStyleSheet("Neutral",  "markup", 3, { character: charAttrs(opRgbColor("9C6500")), cell: fillAttrs(opRgbColor("FFEB9C")) }, APPLY_FONT_FILL);
        this.#createStyleSheet("Negative", "markup", 4, { character: charAttrs(opRgbColor("9C0006")), cell: fillAttrs(opRgbColor("FFC7CE")) }, APPLY_FONT_FILL);
        this.#createStyleSheet("Note",     "markup", 5, { character: charAttrs(DARK1),                cell: fillAttrs(opRgbColor("FFFFCC"),    LIGHT_GRAY_BORDER) }, APPLY_FILL_BORDER);

        // heading styles
        this.#createStyleSheet("Title",     "headings", 1, { character: charAttrs(DARK2, { bold: true, fontSize: 18 }) }, APPLY_FONT);
        this.#createStyleSheet("Heading 1", "headings", 2, { character: charAttrs(DARK2, { bold: true, fontSize: 15 }), cell: { borderBottom: thickBorder(ACCENT1) } },     APPLY_FONT_BORDER);
        this.#createStyleSheet("Heading 2", "headings", 3, { character: charAttrs(DARK2, { bold: true, fontSize: 13 }), cell: { borderBottom: thickBorder(ACCENT1_40) } },  APPLY_FONT_BORDER);
        this.#createStyleSheet("Heading 3", "headings", 4, { character: charAttrs(DARK2, { bold: true, fontSize: 11 }), cell: { borderBottom: mediumBorder(ACCENT1_40) } }, APPLY_FONT_BORDER);
        this.#createStyleSheet("Heading 4", "headings", 5, { character: charAttrs(DARK2, { bold: true, fontSize: 11 }) }, APPLY_FONT);
        this.#createStyleSheet("Total",     "headings", 9, { character: charAttrs(DARK1, { bold: true, fontSize: 11 }), cell: { borderTop: thinBorder(ACCENT1), borderBottom: doubleBorder(ACCENT1) } }, APPLY_FONT_BORDER);

        // other content styles
        this.#createStyleSheet("Output",           "hidden", 1, { character: charAttrs(opRgbColor("3F3F3F"), { bold: true }), cell: fillAttrs(opRgbColor("F2F2F2"), DARK_GRAY_BORDER) },   APPLY_FONT_FILL_BORDER);
        this.#createStyleSheet("Calculation",      "hidden", 2, { character: charAttrs(opRgbColor("FA7D00"), { bold: true }), cell: fillAttrs(opRgbColor("F2F2F2"), MEDIUM_GRAY_BORDER) }, APPLY_FONT_FILL_BORDER);
        this.#createStyleSheet("Input",            "hidden", 3, { character: charAttrs(opRgbColor("3F3F76")),                 cell: fillAttrs(opRgbColor("FFCC99"), MEDIUM_GRAY_BORDER) }, APPLY_FONT_FILL_BORDER);
        this.#createStyleSheet("Explanatory Text", "hidden", 4, { character: charAttrs(opRgbColor("7F7F7F"), { italic: true }) }, APPLY_FONT);
        this.#createStyleSheet("Linked Cell",      "hidden", 5, { character: charAttrs(opRgbColor("FA7D00")), cell: { borderBottom: doubleBorder(opRgbColor("FA7D00")) } }, APPLY_FONT_BORDER);
        this.#createStyleSheet("Warning Text",     "hidden", 6, { character: charAttrs(opRgbColor("FF0000")) }, APPLY_FONT);
        this.#createStyleSheet("Check Cell",       "hidden", 7, { character: charAttrs(LIGHT1, { bold: true }), cell: fillAttrs(opRgbColor("A5A5A5"), DOUBLE_GRAY_BORDER) }, APPLY_FONT_FILL_BORDER);

        // accent styles
        for (let index = 0; index < 6; index += 1) {
            const styleName = `Accent ${index + 1}`;
            const colorName = `accent${index + 1}`;
            this.#createStyleSheet("20% - " + styleName, "themes", index,      { character: charAttrs(DARK1),  cell: fillAttrs(schemeColor(colorName, 80)) }, APPLY_FONT_FILL);
            this.#createStyleSheet("40% - " + styleName, "themes", index + 6,  { character: charAttrs(DARK1),  cell: fillAttrs(schemeColor(colorName, 60)) }, APPLY_FONT_FILL);
            this.#createStyleSheet("60% - " + styleName, "themes", index + 12, { character: charAttrs(LIGHT1), cell: fillAttrs(schemeColor(colorName, 40)) }, APPLY_FONT_FILL);
            this.#createStyleSheet(styleName,            "themes", index + 18, { character: charAttrs(LIGHT1), cell: fillAttrs(schemeColor(colorName, 0)) },  APPLY_FONT_FILL);
        }

        // hyperlink style
        this.#createStyleSheet("Hyperlink", "hidden", 99, { character: charAttrs(this.docModel.createHlinkColor(), { underline: true }) }, APPLY_FONT);
        this.#hyperlinkStyleId = this.getStyleIdByName("Hyperlink");
    }

    /**
     * Returns the identifier of the built-in "Hyperlink" cell style sheet.
     *
     * @returns {string}
     *  The identifier of the built-in "Hyperlink" cell style sheet.
     */
    getHyperlinkStyleId() {
        return this.#hyperlinkStyleId;
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a new style sheet, if not existing yet. Updates the category and
     * priority of existing and created style sheets.
     */
    #createStyleSheet(styleId, category, priority, attributeSet, applyFlags) {

        // update category and priority in existing style sheets
        if (this.containsStyleSheet(styleId)) {
            this.setStyleOptions(styleId, { category, priority, builtIn: true });
            return;
        }

        // add the apply flags for OOXML files only
        if (applyFlags && this.docApp.isOOXML()) {
            attributeSet = { ...attributeSet, apply: applyFlags };
        }

        // create a new (dirty) style sheet, use name as identifier
        this.createDirtyStyleSheet(styleId, styleId, attributeSet, {
            category,
            priority,
            hidden: category === "hidden",
            builtIn: true
        });
    }
}
