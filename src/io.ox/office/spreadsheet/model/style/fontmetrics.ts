/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, ary, map, dict } from "@/io.ox/office/tk/algorithms";
import { convertLength, convertHmmToLength, convertLengthToHmm } from "@/io.ox/office/tk/dom";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import type { CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";

import { getTextPadding, getTotalCellPadding } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// constants ==================================================================

/**
 * A map with lower-case font names to sequences of hexadecimal digits. The
 * first digit of each sequence specifies the row height (in pixels) used by
 * spreadsheets for font size "1px". Each following digit specifies the
 * _difference_ between the preceding row height and the row height of the next
 * font size (plus 1px).
 */
const PIXEL_ROW_HEIGHT_DIFFS: Dict<string> = {
    arial:             "701120131121221222031131131131131121222122121131131131121421222122121131131131211222122131121131131221232122131112131121221231121121222221122121131242121121141131121132121131131131121222121211133131131122121221221131131131122121221131131131322122121221131131131122121221221131131122122141221131131131122121221221131131122122121221",
    calibri:           "70112013113122122122212212212213113113113113113113122122122122212212213113113113113113113113122122122212212212213113113113113113113122122122122212212213113113113113113113113122122122212212212213113113113113113113122122122122212212213113113113113113113122122122122212212212213113113113113113113122122122122212212213113113111",
    cambria:           "70112012212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112212212122122113113113113112211",
    consolas:          "701120131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131131221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221221",
    "courier new":     "7011201312212212221221221311311311311312212212221221221311311311311311312212212221221311311311311311312212212221221221311311311311311312212221221221311311311311311312212212221221221311311311A113113122122213113113122122213113113122122213113113122122213113113122212213113113122212213113113122212213113113122212213113",
    "times new roman": "70112013113113113113112212212212212212212214122122122122122122122113113113113115113113113113113113113113113112210214212212212211122122122122122122122133113113113113113113113113112113113313110122113113113112212212122122115113113112212122122113113113112212214122113113113113112212122122113113133112212212122113113113112212212122"
};

// effective pixel row height for different fonts and font sizes used in OOXML,
// as number arrays mapped by font names
const PIXEL_ROW_HEIGHT_MAP = dict.mapDict(PIXEL_ROW_HEIGHT_DIFFS, diffMap => {
    const rowHeights: Array<number | null> = [null];
    let rowHeight = 0;
    // process each character in the string
    for (const diff of diffMap) {
        rowHeight += parseInt(diff, 16);
        rowHeights.push(rowHeight);
    }
    return rowHeights;
});

// class FontMetrics ==========================================================

/**
 * A mix-in class for the document model class `SpreadsheetModel` providing the
 * style sheet containers for all attribute families used in a spreadsheet
 * document.
 */
export class FontMetrics extends ModelObject<SpreadsheetModel> {

    // cache for default digit width, mapped by different zoom factors
    readonly #digitWidthCache = new Map<number, number>();
    // special behavior for OOXML documents
    readonly #ooxml: boolean;

    // the conversion factor for column widths (1 operation unit to 1/100 mm, initialized lazily)
    #colWidthFactor: Opt<number>;
    // the conversion factor for row heights (1 operation unit to 1/100 mm, initialized lazily)
    #rowHeightFactor: Opt<number>;
    // the maximum column width in 1/100 mm (initialized lazily)
    #maxColWidthHmm: Opt<number>;
    // the maximum wow height in 1/100 mm (initialized lazily)
    #maxRowHeightHmm: Opt<number>;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super(docModel);

        // OOXML: listen to changes of the default stylesheet, invalidate cached font measures for column width
        this.#ooxml = docModel.docApp.isOOXML();
        if (this.#ooxml) {
            this.waitForImportStart(() => {
                this.listenToAllEvents(docModel.cellStyles, (_type, styleId) => {
                    if (styleId === docModel.cellStyles.getDefaultStyleId()) {
                        this.#digitWidthCache.clear();
                        this.#colWidthFactor = this.#maxColWidthHmm = undefined;
                    }
                });
            });
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the maximum width of the digits in the specified font, according
     * to the passed zoom factor.
     *
     * @param charAttrs
     *  Character formatting attributes, as used in document operations.
     *
     * @param zoom
     *  The zoom factor to be applied to the font size.
     *
     * @returns
     *  The maximum width of the digits in the specified font, in pixels.
     */
    getDigitWidth(charAttrs: CharacterAttributes, zoom: number): number {
        return this.docModel.getRenderFont(charAttrs, zoom).getDigitMeasures().maxWidth;
    }

    /**
     * Calculates the effective horizontal padding between cell grid lines and
     * the text contents of the cell in pixels to be used for the passed
     * character attributes and zoom factor.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the padding, as used in
     *  document operations.
     *
     * @param zoom
     *  The zoom factor used to scale the font size in the passed character
     *  attributes.
     *
     * @returns
     *  The effective horizontal text padding for the passed character
     *  attributes, in pixels.
     */
    getTextPadding(charAttrs: CharacterAttributes, zoom: number): number {
        return getTextPadding(this.getDigitWidth(charAttrs, zoom));
    }

    /**
     * Returns the total size of all horizontal padding occupied in a cell that
     * cannot be used for the cell contents for the passed character attributes
     * and zoom factor. This value includes the text padding (twice, for left
     * and right border), and additional space needed for the grid lines.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the padding, as used in
     *  document operations.
     *
     * @returns
     *  The total size of the horizontal cell content padding, in pixels.
     */
    getTotalCellPadding(charAttrs: CharacterAttributes, zoom: number): number {
        return getTotalCellPadding(this.getDigitWidth(charAttrs, zoom));
    }

    /**
     * Returns the maximum width of the digits in the font of the current
     * default cell style, according to the passed zoom factor.
     *
     * @param zoom
     *  The zoom factor to be applied to the default font.
     *
     * @returns
     *  The maximum width of the digits in the font of the current default cell
     *  style, in pixels.
     */
    getDefaultDigitWidth(zoom: number): number {
        return map.upsert(this.#digitWidthCache, Math.round(zoom * 100), () => {

            // attributes of the default cell style (NOT the default auto-style!)
            const defAttrSet = this.docModel.cellStyles.getDefaultStyleAttributeSet();

            // the maximum pixel width of any of the digits of the default font
            return this.getDigitWidth(defAttrSet.character, zoom);
        });
    }

    /**
     * Converts the passed column width from 1/100 of millimeters to operation
     * units according to the file format of the edited document.
     *
     * @param colWidthHmm
     *  A column width in 1/100 of millimeters.
     *
     * @returns
     *  The column width in operation units.
     */
    convertColWidthToUnit(colWidthHmm: number): number {
        const colWidth = colWidthHmm / this.#getColWidthFactor();
        return this.#ooxml ? (Math.floor(colWidth * 256) / 256) : colWidth;
    }

    /**
     * Converts the passed column width from operation units according to the
     * file format of the edited document to 1/100 of millimeters.
     *
     * @param colWidth
     *  A column width in operation units.
     *
     * @returns
     *  The column width in 1/100 of millimeters.
     */
    convertColWidthFromUnit(colWidth: number): number {
        return Math.ceil(colWidth * this.#getColWidthFactor());
    }

    /**
     * Converts the passed row height from 1/100 of millimeters to operation
     * units according to the file format of the edited document.
     *
     * @param rowHeightHmm
     *  A row height in 1/100 of millimeters.
     *
     * @returns
     *  The row height in operation units.
     */
    convertRowHeightToUnit(rowHeightHmm: number): number {
        const rowHeight = rowHeightHmm / this.#getRowHeightFactor();
        return this.#ooxml ? math.roundp(rowHeight, 0.25) : rowHeight;
    }

    /**
     * Converts the passed row height from operation units according to the
     * file format of the edited document to 1/100 of millimeters.
     *
     * @param rowHeight
     *  A row height in operation units.
     *
     * @returns
     *  The row height in 1/100 of millimeters.
     */
    convertRowHeightFromUnit(rowHeight: number): number {
        return Math.round(rowHeight * this.#getRowHeightFactor());
    }

    /**
     * Returns the maximum width for columns, as supported by the file format
     * of the edited document.
     *
     * @returns
     *  The maximum width for columns.
     */
    getMaxColWidthHmm(): number {
        return (this.#maxColWidthHmm ??= this.#ooxml ? this.convertColWidthFromUnit(255) : 100000);
    }

    /**
     * Returns the maximum height for rows, as supported by the file format of
     * the edited document.
     *
     * @returns
     *  The maximum height for rows.
     */
    getMaxRowHeightHmm(): number {
        return (this.#maxRowHeightHmm ??= this.#ooxml ? this.convertRowHeightFromUnit(409.5) : 100000);
    }

    /**
     * Returns the maximum width for columns or the maximum height for rows, as
     * supported by the file format of the edited document.
     *
     * @param columns
     *  Whether to the maximum column width (`true`), or the maximum row height
     *  (`false`).
     *
     * @returns
     *  The maximum width for columns, or the maximum height for rows.
     */
    getMaxColRowSizeHmm(columns: boolean): number {
        return columns ? this.getMaxColWidthHmm() : this.getMaxRowHeightHmm();
    }

    /**
     * Calculates the row height in 1/100 of millimeters to be used for the
     * passed character attributes. The row height is about 10% larger than the
     * normal line height for the passed font settings, as returned e.g. by the
     * method `Font#getNormalLineHeight`.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the line height, as used in
     *  document operations.
     *
     * @returns
     *  The row height for the passed character attributes, in 1/100 mm.
     */
    getRowHeightHmm(charAttrs: CharacterAttributes): number {

        // bug 50743: use precalculated row heights if available, to get rid of browser-dependent "normal line height"
        const presetHeight = this.#ooxml ? this.#getPresetRowHeight(charAttrs) : null;
        if (presetHeight) { return convertLengthToHmm(presetHeight, "px"); }

        // calculate row height in 1/100mm
        return this.#calculateRowHeightHmm(charAttrs);
    }

    /**
     * Calculates the effective scaled row height in pixels to be used for the
     * passed character attributes and zoom factor.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the line height, as used in
     *  document operations.
     *
     * @param zoom
     *  The zoom factor used to scale the font size in the passed character
     *  attributes.
     *
     * @returns
     *  The row height for the passed character attributes, in pixels.
     */
    getRowHeight(charAttrs: CharacterAttributes, zoom: number): number {

        // bug 50743: use precalculated row heights if available, to get rid of browser-dependent "normal line height"
        const presetHeight = this.#ooxml ? this.#getPresetRowHeight(charAttrs) : null;
        if (presetHeight) { return Math.round(presetHeight * zoom); }

        // calculate row height, and convert it from 1/100mm to pixels
        return convertHmmToLength(this.#calculateRowHeightHmm(charAttrs) * zoom, "px", 1);
    }

    // private methods ----------------------------------------------------

    #getColWidthFactor(): number {
        return (this.#colWidthFactor ??= this.#ooxml ? (100 * convertLength(this.getDefaultDigitWidth(1), "px", "mm")) : 1);
    }

    #getRowHeightFactor(): number {
        return (this.#rowHeightFactor ??= this.#ooxml ? convertLength(100, "pt", "mm") : 1);
    }

    /**
     * Returns the precalculated row height for the passed font settings.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the row height, as used in
     *  document operations.
     *
     * @returns
     *  The precalculated row height for the passed font settings, in pixels;
     *  or `null`, if no row height is available for the passed font settings.
     */
    #getPresetRowHeight(charAttrs: CharacterAttributes): number | null {
        const fontName = this.docModel.resolveFontName(charAttrs.fontName);
        const presetRowHeights = PIXEL_ROW_HEIGHT_MAP[fontName.toLowerCase()];
        if (!presetRowHeights) { return null; }
        const fontSize = Math.round(charAttrs.fontSize);
        return presetRowHeights[fontSize] || ary.at(presetRowHeights, -1)!;
    }

    /**
     * Calculates the row height in 1/100 of millimeters to be used for the
     * passed character attributes.
     */
    #calculateRowHeightHmm(charAttrs: CharacterAttributes): number {

        // the normal line height according to the font settings
        let lineHeight = this.docModel.getRenderFont(charAttrs, 1).getNormalLineHeight();

        // use at least 125% of the font size, to compensate for browsers
        // reporting very small line heights (especially Chrome)
        // Bug 40718: increased 120% to 125% to cover the standard case "Arial 11pt".
        lineHeight = Math.max(lineHeight, convertLength(charAttrs.fontSize, "pt", "px") * 1.25);

        // enlarge the resulting "normal" line height by another 10% to add top/bottom cell padding
        return convertLengthToHmm(Math.floor(lineHeight * 1.1), "px");
    }
}
