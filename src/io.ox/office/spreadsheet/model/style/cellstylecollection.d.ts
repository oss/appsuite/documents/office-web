/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import { CellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { SheetAttributePoolMap } from "@/io.ox/office/spreadsheet/model/style/sheetattributepool";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// class CellStyleCollection ==================================================

export class CellStyleCollection extends StyleCollection<SpreadsheetModel, SheetAttributePoolMap, CellAttributeSet> {

    constructor(docModel: SpreadsheetModel);

    createMissingStyles(): void;
}
