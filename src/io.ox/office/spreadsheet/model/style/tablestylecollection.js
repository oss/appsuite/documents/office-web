/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, dict } from "@/io.ox/office/tk/algorithms";

import { THIN_WIDTH_HMM, MEDIUM_WIDTH_HMM, THICK_WIDTH_HMM } from "@/io.ox/office/editframework/utils/border";
import BaseTableStyleCollection from "@/io.ox/office/editframework/model/tablestylecollection";

// class PhColor ==============================================================

class PhColor {
    constructor(shading, index) {
        this.shading = shading;
        this.index = index || 0;
    }
}

// class TableStyleCollection =================================================

/**
 * Contains the style sheets for table formatting attributes. The CSS
 * formatting will be written to table elements and their rows and cells.
 */
export class TableStyleCollection extends BaseTableStyleCollection {

    /**
     * @param {SpreadsheetModel} docModel
     *  The document model containing this instance.
     */
    constructor(docModel) {

        super(docModel, {
            attrPool: docModel.sheetAttrPool,
            families: ["cell", "character"],
            rowBandsOverColBands: true, // row band attributes will be rendered over column band attributes
            expandColBandsToOuterCols: true, // merged column band attributes over active first/last columns
            restrictColBandsToInnerRows: true // column bands will not be drawn into header/footer rows
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Silently inserts all predefiend table style sheets that have not
     * been imported from the document file.
     */
    insertMissingTableStyles() {

        // JSON color helper functions
        const schemeColor = (id, shade) => this.docModel.createSchemeColor(id, shade);

        // predefined colors
        const DARK1 = schemeColor("dark1", 0);
        const DARK1_15 = schemeColor("dark1", 15);
        const DARK1_25 = schemeColor("dark1", 25);
        const DARK1_45 = schemeColor("dark1", 45);
        const LIGHT1 = schemeColor("light1", 0);
        const LIGHT1_15 = schemeColor("light1", -15);
        const LIGHT1_35 = schemeColor("light1", -35);
        const SCHEME = new PhColor(0);
        const SCHEME_DARK_25 = new PhColor(-25);
        const SCHEME_DARK_50 = new PhColor(-50);
        const SCHEME_LIGHT_40 = new PhColor(40);
        const SCHEME_LIGHT_60 = new PhColor(60);
        const SCHEME_LIGHT_80 = new PhColor(80);
        const SCHEME2 = new PhColor(0, 1);

        // JSON border helper functions
        const singleBorder = (width, color) => ({ style: "single", width, color });
        const thinBorder = color => singleBorder(THIN_WIDTH_HMM, color);
        const mediumBorder = color => singleBorder(MEDIUM_WIDTH_HMM, color);
        const thickBorder = color => singleBorder(THICK_WIDTH_HMM, color);
        const doubleBorder = color => ({ style: "double", width: THICK_WIDTH_HMM, color });

        // predefined border values
        const THIN_LIGHT = thinBorder(LIGHT1);
        const THIN_DARK = thinBorder(DARK1);
        const THIN_SCHEME = thinBorder(SCHEME);
        const THIN_SCHEME_40 = thinBorder(SCHEME_LIGHT_40);
        const MEDIUM_LIGHT = mediumBorder(LIGHT1);
        const MEDIUM_DARK = mediumBorder(DARK1);
        const MEDIUM_SCHEME = mediumBorder(SCHEME);
        const THICK_LIGHT = thickBorder(LIGHT1);
        const DOUBLE_DARK = doubleBorder(DARK1);
        const DOUBLE_SCHEME = doubleBorder(SCHEME);

        // predefined character attributes
        const CHARACTER_LIGHT = { color: LIGHT1 };
        const CHARACTER_DARK = { color: DARK1 };
        const CHARACTER_BOLD_LIGHT = { color: LIGHT1, bold: true };
        const CHARACTER_BOLD_DARK = { color: DARK1, bold: true };
        const CHARACTER_BOLD_SCHEME_25 = { color: SCHEME_DARK_25, bold: true };

        // border attribute helpers
        function outerBorderAttrs(border) { return { borderLeft: border, borderRight: border, borderTop: border, borderBottom: border }; }
        function outerAndHorBorderAttrs(border) { return Object.assign(outerBorderAttrs(border), { borderInsideHor: border }); }
        function allBorderAttrs(border) { return Object.assign(outerAndHorBorderAttrs(border), { borderInsideVert: border }); }

        // Light 1...7: without borders
        this.#createSchemeStyleSheets("Light", 1, 72, {
            wholeTable: { character: CHARACTER_DARK, table: { borderTop: THIN_DARK, borderBottom: THIN_DARK } },
            firstRow: { character: CHARACTER_BOLD_DARK, cell: { borderBottom: THIN_DARK } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: THIN_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: LIGHT1_15 } },
            band1Hor: { cell: { fillColor: LIGHT1_15 } }
        }, {
            wholeTable: { character: { color: SCHEME_DARK_25 }, table: { borderTop: THIN_SCHEME, borderBottom: THIN_SCHEME } },
            firstRow: { character: CHARACTER_BOLD_SCHEME_25, cell: { borderBottom: THIN_SCHEME } },
            lastRow: { character: CHARACTER_BOLD_SCHEME_25, cell: { borderTop: THIN_SCHEME } },
            firstCol: { character: CHARACTER_BOLD_SCHEME_25 },
            lastCol: { character: CHARACTER_BOLD_SCHEME_25 },
            band1Vert: { cell: { fillColor: SCHEME_LIGHT_80 } },
            band1Hor: { cell: { fillColor: SCHEME_LIGHT_80 } }
        });

        // Light 8...14: with borders in bands
        this.#createSchemeStyleSheets("Light", 8, 72, {
            wholeTable: { character: CHARACTER_DARK, table: outerBorderAttrs(THIN_DARK) },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1 } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { borderLeft: THIN_DARK, borderRight: THIN_DARK } },
            band1Hor: { cell: { borderTop: THIN_DARK, borderBottom: THIN_DARK } }
        }, {
            wholeTable: { character: CHARACTER_DARK, table: outerBorderAttrs(THIN_SCHEME) },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_SCHEME } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { borderLeft: THIN_SCHEME, borderRight: THIN_SCHEME } },
            band1Hor: { cell: { borderTop: THIN_SCHEME, borderBottom: THIN_SCHEME } }
        });

        // Light 15...21: with all borders
        this.#createSchemeStyleSheets("Light", 15, 72, {
            wholeTable: { character: CHARACTER_DARK, table: allBorderAttrs(THIN_DARK) },
            firstRow: { character: CHARACTER_BOLD_DARK, cell: { borderBottom: MEDIUM_DARK } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: LIGHT1_15 } },
            band1Hor: { cell: { fillColor: LIGHT1_15 } }
        }, {
            wholeTable: { character: CHARACTER_DARK, table: allBorderAttrs(THIN_SCHEME) },
            firstRow: { character: CHARACTER_BOLD_DARK, cell: { borderBottom: MEDIUM_SCHEME } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_SCHEME } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: SCHEME_LIGHT_80 } },
            band1Hor: { cell: { fillColor: SCHEME_LIGHT_80 } }
        });

        // Medium 1...7: with outer borders
        this.#createSchemeStyleSheets("Medium", 1, 71, {
            wholeTable: { character: CHARACTER_DARK, table: outerAndHorBorderAttrs(THIN_DARK) },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1 } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: LIGHT1_15 } },
            band1Hor: { cell: { fillColor: LIGHT1_15 } }
        }, {
            wholeTable: { character: CHARACTER_DARK, table: outerAndHorBorderAttrs(THIN_SCHEME_40) },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_SCHEME } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: SCHEME_LIGHT_80 } },
            band1Hor: { cell: { fillColor: SCHEME_LIGHT_80 } }
        });

        // Medium 8...14: with white borders
        this.#createSchemeStyleSheets("Medium", 8, 71, {
            wholeTable: { character: CHARACTER_DARK, cell: { fillColor: LIGHT1_15 }, table: { borderInsideHor: THIN_LIGHT, borderInsideVert: THIN_LIGHT } },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1, borderBottom: THICK_LIGHT } },
            lastRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1, borderTop: THICK_LIGHT } },
            firstCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1 } },
            lastCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1 } },
            band1Vert: { cell: { fillColor: LIGHT1_35 } },
            band1Hor: { cell: { fillColor: LIGHT1_35 } }
        }, {
            wholeTable: { character: CHARACTER_DARK, cell: { fillColor: SCHEME_LIGHT_80 }, table: { borderInsideHor: THIN_LIGHT, borderInsideVert: THIN_LIGHT } },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME, borderBottom: THICK_LIGHT } },
            lastRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME, borderTop: THICK_LIGHT } },
            firstCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME } },
            lastCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME } },
            band1Vert: { cell: { fillColor: SCHEME_LIGHT_60 } },
            band1Hor: { cell: { fillColor: SCHEME_LIGHT_60 } }
        });

        // Medium 15...21: without borders
        this.#createSchemeStyleSheets("Medium", 15, 71, {
            wholeTable: { character: CHARACTER_DARK, table: { borderLeft: THIN_DARK, borderRight: THIN_DARK, borderTop: MEDIUM_DARK, borderBottom: MEDIUM_DARK, borderInsideHor: THIN_DARK, borderInsideVert: THIN_DARK } },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1, borderBottom: THIN_DARK } },
            lastRow: { cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1 } },
            lastCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1 } },
            band1Vert: { cell: { fillColor: LIGHT1_15 } },
            band1Hor: { cell: { fillColor: LIGHT1_15 } }
        }, {
            wholeTable: { character: CHARACTER_DARK, table: { borderTop: MEDIUM_DARK, borderBottom: MEDIUM_DARK } },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME, borderBottom: MEDIUM_DARK } },
            lastRow: { cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME } },
            lastCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME } },
            band1Vert: { cell: { fillColor: LIGHT1_15 } },
            band1Hor: { cell: { fillColor: LIGHT1_15 } }
        });

        // Medium 22...28: with colored borders
        this.#createSchemeStyleSheets("Medium", 22, 71, {
            wholeTable: { character: CHARACTER_DARK, table: allBorderAttrs(THIN_DARK), cell: { fillColor: LIGHT1_15 } },
            firstRow: { character: CHARACTER_BOLD_DARK },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: MEDIUM_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: LIGHT1_35 } },
            band1Hor: { cell: { fillColor: LIGHT1_35 } }
        }, {
            wholeTable: { character: CHARACTER_DARK, table: allBorderAttrs(THIN_SCHEME_40), cell: { fillColor: SCHEME_LIGHT_80 } },
            firstRow: { character: CHARACTER_BOLD_DARK },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: MEDIUM_SCHEME } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: SCHEME_LIGHT_60 } },
            band1Hor: { cell: { fillColor: SCHEME_LIGHT_60 } }
        });

        // Dark 1...7: without borders
        this.#createSchemeStyleSheets("Dark", 1, 73, {
            wholeTable: { character: CHARACTER_LIGHT, cell: { fillColor: DARK1_45 } },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1, borderBottom: MEDIUM_LIGHT } },
            lastRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1_15, borderTop: MEDIUM_LIGHT } },
            firstCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1_25, borderRight: MEDIUM_LIGHT } },
            lastCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1_25, borderLeft: MEDIUM_LIGHT } },
            band1Vert: { cell: { fillColor: DARK1_25 } },
            band1Hor: { cell: { fillColor: DARK1_25 } }
        }, {
            wholeTable: { character: CHARACTER_LIGHT, cell: { fillColor: SCHEME } },
            firstRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: DARK1, borderBottom: MEDIUM_LIGHT } },
            lastRow: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME_DARK_50, borderTop: MEDIUM_LIGHT } },
            firstCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME_DARK_25, borderRight: MEDIUM_LIGHT } },
            lastCol: { character: CHARACTER_BOLD_LIGHT, cell: { fillColor: SCHEME_DARK_25, borderLeft: MEDIUM_LIGHT } },
            band1Vert: { cell: { fillColor: SCHEME_DARK_25 } },
            band1Hor: { cell: { fillColor: SCHEME_DARK_25 } }
        });

        // Dark 8...11: with colored header
        this.#createSchemeStyleSheets2("Dark", 8, 73, {
            wholeTable: { cell: { fillColor: LIGHT1_15 } },
            firstRow: { character: CHARACTER_LIGHT, cell: { fillColor: DARK1 } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: LIGHT1_35 } },
            band1Hor: { cell: { fillColor: LIGHT1_35 } }
        }, {
            wholeTable: { cell: { fillColor: SCHEME_LIGHT_80 } },
            firstRow: { character: CHARACTER_LIGHT, cell: { fillColor: SCHEME2 } },
            lastRow: { character: CHARACTER_BOLD_DARK, cell: { borderTop: DOUBLE_DARK } },
            firstCol: { character: CHARACTER_BOLD_DARK },
            lastCol: { character: CHARACTER_BOLD_DARK },
            band1Vert: { cell: { fillColor: SCHEME_LIGHT_60 } },
            band1Hor: { cell: { fillColor: SCHEME_LIGHT_60 } }
        });
    }

    // private methods --------------------------------------------------------

    #createStyleSheet(category, index, uiPriority, attrs) {
        const styleId = `TableStyle${category}${index}`;
        const styleName = `Table Style ${category} ${index}`;
        // check by style sheet name (they are always loaded in English from Excel, while the style IDs are localized)
        if (this.containsStyleSheetByName(styleName)) {
            this.setStyleOptions(styleId, { category, priority: uiPriority });
        } else {
            this.createDirtyStyleSheet(styleId, styleName, attrs, { category, priority: uiPriority });
        }
    }

    #resolvePlaceHolderAttrs(placeholderAttrs, scheme1Index, scheme2Index) {
        const colorIds = [`accent${scheme1Index}`, `accent${scheme2Index}`];
        const processValue = value => {
            if (value instanceof PhColor) { return this.docModel.createSchemeColor(colorIds[value.index], value.shading); }
            if (is.array(value)) { return value.map(processValue); }
            if (is.dict(value)) { return dict.mapDict(value, processValue); }
            return value;
        };
        return processValue(placeholderAttrs);
    }

    #createSchemeStyleSheets(category, startIndex, uiPriority, leadingAttrs, placeholderAttrs) {
        this.#createStyleSheet(category, startIndex, uiPriority, leadingAttrs);
        startIndex += 1;
        for (let schemeIdx = 1; schemeIdx <= 6; schemeIdx += 1, startIndex += 1) {
            this.#createStyleSheet(category, startIndex, uiPriority, this.#resolvePlaceHolderAttrs(placeholderAttrs, schemeIdx));
        }
    }

    #createSchemeStyleSheets2(category, startIndex, uiPriority, leadingAttrs, placeholderAttrs) {
        this.#createStyleSheet(category, startIndex, uiPriority, leadingAttrs);
        startIndex += 1;
        for (let schemeIdx = 1; schemeIdx <= 6; schemeIdx += 2, startIndex += 1) {
            this.#createStyleSheet(category, startIndex, uiPriority, this.#resolvePlaceHolderAttrs(placeholderAttrs, schemeIdx, schemeIdx + 1));
        }
    }
}
