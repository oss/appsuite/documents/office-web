/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, fun, ary, set, dict } from "@/io.ox/office/tk/algorithms";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { FindMatchType, RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { MoveCellsEvent } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { DVRuleAttributes, DVRuleSharedConfig, DVListResult } from "@/io.ox/office/spreadsheet/model/dvrulemodel";
import { DVRuleErrorType, DVRuleModel } from "@/io.ox/office/spreadsheet/model/dvrulemodel";
import type { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

// types ======================================================================

/**
 * A `Range` object with additional validation rule property.
 */
interface DVRuleRange extends Range {
    ruleModel: DVRuleModel;
}

/**
 * A pair with a rule model and its array index.
 */
type RuleModelEntry = [number, DVRuleModel];

/**
 * Result object for `DVRuleCollection#_getModelsByRanges`.
 */
interface RuleModelsAndRanges {

    /**
     * The non-empty array of cell range addresses contained in a document
     * operation.
     */
    sourceRanges: RangeArray;

    /**
     * The models of all validation rules overlapping with the target ranges,
     * and their array indexes. This array will be sorted by rule index
     * (original array index).
     */
    modelEntries: RuleModelEntry[];
}

/**
 * Event object emitted by an instance of `DVRuleCollection`.
 */
export interface DVRuleEvent extends SheetEventBase {

    /**
     * The model instance of the validation rule causing the event.
     */
    ruleModel: DVRuleModel;
}

/**
 * Type mapping for the events emitted by `DVRuleCollection` instances.
 */
export interface DVRuleCollectionEventMap {

    /**
     * Will be emitted after a new validation rule has been inserted into a
     * rule collection.
     *
     * @param event
     *  Event object with the model of the new validation rule.
     */
    "insert:dvrule": [event: DVRuleEvent];

    /**
     * Will be emitted before an existing validation rule will be deleted from
     * a rule collection.
     *
     * @param event
     *  Event object with the model of the validation rule to be deleted.
     */
    "delete:dvrule": [event: DVRuleEvent];

    /**
     * Will be emitted after the attributes of a validation rule have been
     * changed.
     *
     * @param event
     *  Event object with the model of the changed validation rule.
     */
    "change:dvrule": [event: DVRuleEvent];
}

// private functions ==========================================================

function parseRuleAttrs(context: SheetOperationContext): Partial<DVRuleAttributes> {

    const ruleAttrs = dict.createPartial<DVRuleAttributes>();

    // string attributes (must not be empty)
    for (const attrName of ["type", "compare"] as const) {
        if (context.has(attrName)) {
            ruleAttrs[attrName] = context.getStr(attrName);
        }
    }

    // string attributes (can be empty)
    for (const attrName of ["value1", "value2", "infoTitle", "infoText", "errorTitle", "errorText"] as const) {
        if (context.has(attrName)) {
            ruleAttrs[attrName] = context.getStr(attrName, { empty: true });
        }
    }

    // boolean attributes
    for (const attrName of ["showInfo", "showError", "showDropDown", "ignoreEmpty"] as const) {
        if (context.has(attrName)) {
            ruleAttrs[attrName] = context.getBool(attrName);
        }
    }

    // error type enum
    if (context.has("errorType")) {
        ruleAttrs.errorType = context.getEnum("errorType", DVRuleErrorType);
    }

    return ruleAttrs;
}

// class DVRuleCollection =====================================================

/**
 * Stores data validation settings in a specific sheet. Data validation can be
 * used to restrict the values allowed to be entered in specific cell ranges,
 * and to show tooltips and error messages.
 */
export class DVRuleCollection extends SheetChildModel<DVRuleCollectionEventMap> {

    // shared collection/model configuration
    readonly #config: DVRuleSharedConfig;
    // all validation rules as array (in operation/insertion order)
    readonly #ruleModels = this.member<DVRuleModel[]>([]);
    // all validation rules, mapped by target ranges
    readonly #targetRangeSet = new RangeSet<DVRuleRange>();

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this collection.
     */
    constructor(sheetModel: SheetModel) {

        // base constructor
        super(sheetModel);

        // file-format dependent configuration shared by this instance and all rule models
        this.#config = {
            indexedOps: this.docApp.isOOXML(),
            customRef: this.docApp.isODF()
        };

        // ODF: update target ranges after moving cells, or inserting/deleting columns or rows
        // OOXML: target ranges will always be changed by generated operations (needed for OT)
        if (!this.#config.indexedOps) {
            this.listenTo(sheetModel.cellCollection, "move:cells", this.#moveCellsHandler);
        }

        // additional processing and event handling after the document has been imported
        this.waitForImportSuccess(alreadyImported => {

            // refresh all formulas after import is finished (formulas may refer to sheets not yet imported)
            if (!alreadyImported) {
                for (const ruleModel of this.#ruleModels) {
                    ruleModel.refreshAfterImport();
                }
            }

            // update the sheet indexes after the sheet collection has been manipulated
            this.listenTo(this.docModel, "transform:sheets", xfVector => {
                for (const ruleModel of this.#ruleModels) {
                    ruleModel.transformSheets(xfVector);
                }
            });
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the rule model with the specified index.
     *
     * @param ruleIndex
     *  The operation index of the validation rule.
     *
     * @returns
     *  The specified rule model if existing.
     */
    getRuleModel(ruleIndex: number): Opt<DVRuleModel> {
        return this.#ruleModels[ruleIndex];
    }

    /**
     * Returns the validation rule that covers the specified cell.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  The model of the validation rule for the specified cell. If the cell is
     *  not validated at all, returns `undefined`.
     */
    getModelByAddress(address: Address): Opt<DVRuleModel> {
        return this.#targetRangeSet.findOneByAddress(address)?.ruleModel;
    }

    /**
     * Creates an iterator that visits the rules of the specified data
     * validations in this collection.
     *
     * @param [boundRange]
     *  If specified, the iterator will visit data validations overlapping with
     *  that cell range. By default, all data validations in this collection
     *  will be visited.
     *
     * @yields
     *  The matching rule models.
     */
    *yieldRuleModels(boundRange?: Range): IterableIterator<DVRuleModel> {

        // visit all rules
        if (!boundRange) {
            yield* this.#ruleModels;
            return;
        }

        // visit all validation rules covering the passed bounding range; prevent
        // multiple hits of the same rule model referred from different target ranges
        const visitedModels = new Set<DVRuleModel>();
        for (const { ruleModel } of this.#targetRangeSet.yieldMatching(boundRange)) {
            if (set.toggle(visitedModels, ruleModel, true)) {
                yield ruleModel;
            }
        }
    }

    /**
     * Returns whether to show a drop-down button next to the specified cell,
     * according to the validation type (`source` and `list` only).
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  Whether to show a drop-down button next to the specified cell.
     */
    showDropDownButton(address: Address): boolean {
        return !!this.getModelByAddress(address)?.showDropDownButton();
    }

    /**
     * Returns the list contents for a validation rule of type "list" or
     * "source" at the specified cell address.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  The contents of the cells, if the specified cell contains a validation
     *  rule of type "list" or "source"; otherwise an empty array.
     */
    queryListContents(address: Address): DVListResult[] {
        return this.getModelByAddress(address)?.queryListContents(address) ?? [];
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations and undo operations to update or restore the
     * formula expressions of all data validation rules.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{DVRuleCollection} generateUpdateTaskOperations")
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise {
        return this.asyncForEach(this.#ruleModels, (ruleModel, ruleIndex) => {
            ruleModel.generateUpdateTaskOperations(sheetCache, ruleIndex, updateTask);
        });
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * contents from the passed collection into this collection.
     *
     * @param _context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws
     *  An `OperationError`, if applying the operation fails, e.g. if a
     *  required property is missing in the operation.
     */
    applyCopySheetOperation(_context: SheetOperationContext, fromCollection: DVRuleCollection): void {

        // clone the contents of the source collection
        for (const ruleModel of fromCollection.#ruleModels) {
            const cloneModel = new DVRuleModel(this.sheetModel, ruleModel);
            this.#ruleModels.push(cloneModel);
            this.#insertTargetRanges(cloneModel);
        }
    }

    /**
     * Callback handler for the document operation "insertDVRule".
     *
     * @param context
     *  A wrapper representing the "insertDVRule" document operation.
     */
    applyInsertOperation(context: SheetOperationContext): void {

        // the target ranges for the data validation
        const targetRanges = context.getRangeList("ranges");
        // read all validation attributes
        const ruleAttrs = parseRuleAttrs(context);
        // the custom reference address (as formula expression)
        const refFormula = this.#config.customRef ? context.getStr("ref") : null;

        // the array insertion index for the new validation rule
        const ruleIndex = fun.do(() => {

            // OOXML: validation rule will be addressed by an index
            if (this.#config.indexedOps) {
                const index = context.getInt("index");
                context.ensure((index >= 0) && (index <= this.#ruleModels.length), "invalid index");
                return index;
            }

            // ODF: shorten target ranges of existing validation rules
            this.#reduceModelsByRanges(context);
            // use index hint if available (e.g. created by `generateRestoreOperations()` to restore the original
            // array order which is needed in generic unit tests that repeatedly use "undo" and expect the original
            // document state)
            return math.clamp(context.optInt("index", this.#ruleModels.length), 0, this.#ruleModels.length);
        });

        // create and insert the new validation rule
        const ruleModel = new DVRuleModel(this.sheetModel, targetRanges, ruleAttrs, refFormula, this.#config);
        ary.insertAt(this.#ruleModels, ruleIndex, ruleModel);
        this.#insertTargetRanges(ruleModel);

        // notify event listeners
        this.trigger("insert:dvrule", this.createSheetEvent({ ruleModel }, context));
    }

    /**
     * Callback handler for the document operation "deleteDVRule".
     *
     * @param context
     *  A wrapper representing the "deleteDVRule" document operation.
     */
    applyDeleteOperation(context: SheetOperationContext): void {

        // OOXML: delete an existing validation rule addressed by an index
        if (this.#config.indexedOps) {

            // resolve the validation rule addressed by the operation (throws on error)
            const [ruleIndex, ruleModel] = this.#getModelEntry(context);

            // destroy and remove the validation rule
            this.#removeTargetRanges(ruleModel);
            this.#deleteModel(ruleModel, ruleIndex, context);
            return;
        }

        // ODF: shorten target ranges of existing validation rules (throws on error)
        this.#reduceModelsByRanges(context);
    }

    /**
     * Callback handler for the document operation "changeDVRule".
     *
     * @param context
     *  A wrapper representing the "changeDVRule" document operation.
     */
    applyChangeOperation(context: SheetOperationContext): void {

        // the validation rule to be changed
        let ruleModel: Opt<DVRuleModel>;
        // whether the target ranges have been changed by the operation (OOXML only)
        let changedRanges = false;

        // OOXML: change an existing validation rule addressed by an index
        if (this.#config.indexedOps) {

            // resolve the validation rule addressed by the operation (throws on error)
            ruleModel = this.#getModelEntry(context)[1];

            // resolve optional `ranges` property (throws on error), change target ranges
            const newTargetRanges = context.optRangeList("ranges");
            if (newTargetRanges) {
                this.#removeTargetRanges(ruleModel);
                changedRanges = ruleModel.setTargetRanges(newTargetRanges);
                this.#insertTargetRanges(ruleModel);
            }

        } else {

            // ODF: range addresses in operation must refer exactly to an existing rule
            const { sourceRanges, modelEntries } = this.#getModelsByRanges(context, FindMatchType.EXACT);
            context.ensure(modelEntries.length, "no validations found at %s", sourceRanges);
            context.ensure(modelEntries.length === 1, "multiple validations covering %s", sourceRanges);
            ruleModel = modelEntries[0][1];

            // target ranges must be exactly equal
            const targetRanges = ruleModel.getTargetRanges();
            if (!targetRanges.deepEquals(sourceRanges)) {
                context.error("rule target ranges %s do not match operation ranges %s", targetRanges, sourceRanges);
            }
        }

        // resolve optional "ref" property (throws on error), change reference address
        let changedRef = false;
        const refFormula = this.#config.customRef ? context.optStr("ref") : null;
        if (refFormula) {
            ruleModel.setRefFormula(refFormula);
            changedRef = true;
        }

        // change validation attributes
        const ruleAttrs = parseRuleAttrs(context);
        const changedAttrs = ruleModel.setAttributes({ dvrule: ruleAttrs });

        // notify event listeners
        if (changedRanges || changedRef || changedAttrs) {
            this.trigger("change:dvrule", this.createSheetEvent({ ruleModel }, context));
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Inserts the target ranges of the passed validation rule into the
     * internal range set.
     *
     * @param ruleModel
     *  The validation rule whose target ranges will be inserted into the range
     *  set.
     */
    #insertTargetRanges(ruleModel: DVRuleModel): void {
        for (const range of ruleModel.getTargetRanges()) {
            const targetRange = range.clone() as DVRuleRange;
            targetRange.ruleModel = ruleModel;
            this.#targetRangeSet.add(targetRange);
        }
    }

    /**
     * Removes the target ranges of the passed validation rule from the
     * internal range set.
     *
     * @param ruleModel
     *  The validation rule whose target ranges will be removed from the range
     *  set.
     */
    #removeTargetRanges(ruleModel: DVRuleModel): void {
        for (const range of ruleModel.getTargetRanges()) {
            this.#targetRangeSet.delete(range);
        }
    }

    /**
     * Triggers a "delete:dvrule" event, removes the validation rule from the
     * internal containers, and destroys the model.
     */
    #deleteModel(ruleModel: DVRuleModel, ruleIndex: number, context?: SheetOperationContext): void {
        this.trigger("delete:dvrule", this.createSheetEvent({ ruleModel }, context));
        ary.deleteAt(this.#ruleModels, ruleIndex);
        ruleModel.destroy();
    }

    /**
     * Returns the validation rule addressed by the property "index" in the
     * passed document operation.
     *
     * @param context
     *  A wrapper representing a document operation for validations, expected
     *  to contain an integer property "index".
     *
     * @returns
     *  The validation rule and its array index.
     *
     * @throws
     *  An `OperationError`, if the operation does not contain a valid "index"
     *  property, or if the validation rule with such an index does not exist.
     */
    #getModelEntry(context: SheetOperationContext): RuleModelEntry {

        // get the index from the operation
        const ruleIndex = context.getInt("index");

        // check existence of the validation rule
        const ruleModel = this.#ruleModels[ruleIndex];
        context.ensure(ruleModel, "missing validation rule");

        return [ruleIndex, ruleModel];
    }

    /**
     * Returns the validation rules addressed by the property "ranges" in the
     * passed document operation.
     *
     * @param context
     *  A wrapper representing a document operation for validations, expected
     *  to contain a range array property "ranges".
     *
     * @returns
     *  The cell ranges and rule models covered by the document operation.
     *
     * @throws
     *  An `OperationError`, if the operation does not contain a valid "ranges"
     *  property.
     */
    #getModelsByRanges(context: SheetOperationContext, matchType?: FindMatchType): RuleModelsAndRanges {

        // the target rangesof the document operation
        const sourceRanges = context.getRangeList("ranges").merge();
        // the validation rules to be returned
        const ruleModelSet = new Set/*<DVRuleModel>*/();

        // process all target ranges contained in the operation
        for (const sourceRange of sourceRanges) {
            // get all ranges from the set covered by a validation rule
            for (const targetRange of this.#targetRangeSet.yieldMatching(sourceRange, matchType)) {
                ruleModelSet.add(targetRange.ruleModel);
            }
        }

        // construct the array of descriptors with rule and array index
        const modelEntries: RuleModelEntry[] = [];
        // do not process the array of all models, if there is no overlapping model
        if (ruleModelSet.size) {
            for (const modelEntry of this.#ruleModels.entries()) {
                if (ruleModelSet.has(modelEntry[1])) {
                    modelEntries.push(modelEntry);
                }
            }
        }

        return { sourceRanges, modelEntries };
    }

    /**
     * Reduces the target ranges of all existing validation rules so that they
     * do not overlap with the ranges in the passed document operation anymore.
     * If a validation rule is covered completely by the ranges in the
     * operation, it will be deleted from this collection.
     *
     * @param context
     *  A wrapper representing a document operation for validations, expected
     *  to contain a range array property "ranges".
     *
     * @returns
     *  The target ranges contained in the document operation.
     *
     * @throws
     *  An `OperationError` if the operation does not contain a valid "ranges"
     *  property, or if no validation rules overlapping with the ranges exist.
     */
    #reduceModelsByRanges(context: SheetOperationContext): RangeArray {

        const { sourceRanges, modelEntries } = this.#getModelsByRanges(context);
        for (const [ruleIndex, ruleModel] of ary.values(modelEntries, { reverse: true })) {

            // the current target ranges of the validation rule
            const oldTargetRanges = ruleModel.getTargetRanges();
            // the reminaing target ranges for the validation rule
            const newTargetRanges = oldTargetRanges.difference(sourceRanges);

            // delete the rule
            if (newTargetRanges.empty()) {
                this.#removeTargetRanges(ruleModel);
                this.#deleteModel(ruleModel, ruleIndex, context);
                continue;
            }

            // shorten the target ranges
            this.#removeTargetRanges(ruleModel);
            ruleModel.setTargetRanges(newTargetRanges);
            this.#insertTargetRanges(ruleModel);
            this.trigger("change:dvrule", this.createSheetEvent({ ruleModel }, context));
        }

        // return the range addresses from the operation
        return sourceRanges;
    }

    /**
     * Recalculates the target ranges of all validation rules, after cells have
     * been moved (including inserted/deleted columns or rows) in the sheet.
     */
    #moveCellsHandler({ transformer }: MoveCellsEvent): void {

        // simply fill a new target range set while transforming the ranges
        this.#targetRangeSet.clear();

        // process all validation rules in reversed order to be able to delete array elements
        for (const [ruleIndex, ruleModel] of ary.entries(this.#ruleModels, { reverse: true })) {
            const targetRanges = ruleModel.getTargetRanges(transformer);
            if (targetRanges.empty()) {
                this.#deleteModel(ruleModel, ruleIndex);
            } else {
                const changed = ruleModel.setTargetRanges(targetRanges);
                this.#insertTargetRanges(ruleModel);
                if (changed) { this.trigger("change:dvrule", this.createSheetEvent({ ruleModel })); }
            }
        }
    }
}
