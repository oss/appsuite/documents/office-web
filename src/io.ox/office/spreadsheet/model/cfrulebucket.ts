/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { pick } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";
import { CellValueCache } from "@/io.ox/office/spreadsheet/model/cellvaluecache";
import type { CFRuleModel } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// class CFRuleBucket =========================================================

/**
 * A small helper structure that stores the target ranges of one or more
 * formatting rules, as well as the formatting rules covering these target
 * ranges.
 */
export class CFRuleBucket extends DObject {

    /**
     * The number formatter of the document model.
     */
    readonly numberFormatter: NumberFormatter;

    /**
     * The target ranges for the formatting rules.
     */
    readonly ranges: RangeArray;

    /**
     * A unique key for the target ranges, intended to be used as map key.
     */
    readonly rangesKey: string;

    /**
     * The reference address of the target ranges, i.e. to top-left address of
     * the bounding range of all target ranges. Used as reference address for
     * formulas in formatting rules.
     */
    readonly refAddress: Address;

    /**
     * The cache containing all cell values covered by the target ranges.
     */
    readonly valueCache: CellValueCache;

    /**
     * All rule models covering the target ranges.
     */
    readonly ruleModelSet: Set<CFRuleModel>;

    /**
     * The number of range rules in the rule model map.
     */
    rangeRules = 0;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, targetRanges: RangeArray) {
        super();

        // public properties
        this.numberFormatter = sheetModel.docModel.numberFormatter;
        this.ranges = targetRanges.deepClone();
        this.rangesKey = targetRanges.key;
        this.refAddress = targetRanges.refAddress()!;
        this.valueCache = this.member(new CellValueCache(sheetModel, targetRanges));
        this.ruleModelSet = this.member(new Set());

        // refresh the target ranges after change events of the value cache (range rules only)
        this.valueCache.on("invalidate", () => {
            if (this.rangeRules) { sheetModel.refreshRanges(targetRanges); }
        });
    }

    protected override destructor(): void {
        for (const ruleModel of this.ruleModelSet) {
            this.unregisterRuleModel(ruleModel);
        }
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Updates further internal settings after the specified rule model has
     * been inserted into the conditional formatting collection.
     */
    registerRuleModel(ruleModel: CFRuleModel): void {
        // update the range rule count
        if (ruleModel.isRangeRule()) { this.rangeRules += 1; }
        // update the transient number formats
        const cellAttrs = pick.dict(ruleModel.getMatchAttributeSet(true), "cell");
        if (cellAttrs) { this.numberFormatter.registerTransientFormat(cellAttrs); }
    }

    /**
     * Updates further internal settings before the specified rule model will
     * be removed from the conditional formatting collection.
     */
    unregisterRuleModel(ruleModel: CFRuleModel): void {
        // update the range rule count
        if (ruleModel.isRangeRule()) { this.rangeRules -= 1; }
        // update the transient number formats
        const cellAttrs = pick.dict(ruleModel.getMatchAttributeSet(true), "cell");
        if (cellAttrs) { this.numberFormatter.unregisterTransientFormat(cellAttrs); }
    }

    /**
     * Inserts the passed rule model into this range descriptor.
     */
    insertRuleModel(ruleModel: CFRuleModel): void {
        // insert the rule model into the set
        this.ruleModelSet.add(ruleModel);
        // update internal settings for the rule depending on the rule attributes
        this.registerRuleModel(ruleModel);
    }

    /**
     * Removes the passed rule model from this range descriptor, but does not
     * destroy it.
     */
    removeRuleModel(ruleModel: CFRuleModel): void {
        // update internal settings for the rule depending on the rule attributes
        this.unregisterRuleModel(ruleModel);
        // remove the rule model from the set
        this.ruleModelSet.delete(ruleModel);
    }
}
