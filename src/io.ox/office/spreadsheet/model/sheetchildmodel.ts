/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { type SheetEventBase, createSheetEvent } from "@/io.ox/office/spreadsheet/model/sheetevents";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// re-exports =================================================================

export type { SheetModel };

// class SheetChildModel ======================================================

/**
 * Generic base class for models contained in a single sheet in a spreadsheet
 * document.
 *
 * Every `SheetChildModel` instance contains a back-reference to its parent
 * sheet model. The life time of these instances is bound to the sheet model,
 * i.e. they MUST be destroyed when the sheet model will be destroyed.
 */
export class SheetChildModel<EvtMapT = Empty> extends ModelObject<SpreadsheetModel, EvtMapT> {

    /**
     * The parent sheet model containing this instance.
     */
    readonly sheetModel: SheetModel;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model containing this instance.
     */
    constructor(sheetModel: SheetModel) {
        super(sheetModel.docModel);
        this.sheetModel = sheetModel;
    }

    // protected methods ------------------------------------------------------

    /**
     * Creates a `SheetEventBase` event object with additional properties.
     *
     * @param eventData
     *  The additional properties for the event object.
     *
     * @param [context]
     *  The context wrapper of a document operation.
     *
     * @returns
     *  The `SheetEventBase` event object with the passed properties.
     */
    protected createSheetEvent<DataT extends object>(eventData: DataT, context?: SheetOperationContext): SheetEventBase & DataT {
        return createSheetEvent(this.sheetModel, this.sheetModel.getIndex(), eventData, context);
    }
}
