/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary, map, dict } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import type { OTSortVector } from "@/io.ox/office/editframework/utils/otutils";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type OptSheet, mapKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { DefNameRef, TableRef, IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import type { NameCollection } from "@/io.ox/office/spreadsheet/model/namecollection";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import type { TableCollection } from "@/io.ox/office/spreadsheet/model/tablecollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface JSONTableSnapshot {
    name: string;
    range: string;
    header: boolean;
    footer: boolean;
    columns: string[];
}

export interface JSONBaseSnapshot {
    names?: string[];
    tables?: JSONTableSnapshot[];
}

export interface JSONSheetSnapshot extends JSONBaseSnapshot {
    name: string;
}

export interface JSONDocumentSnapshot extends JSONBaseSnapshot {
    sheets: JSONSheetSnapshot[];
}

// private functions ==========================================================

function joinStrings(...args: Array<string | OrArray<BaseSnapshot>>): string {
    return args.map(arg => is.array(arg) ? arg.map(String).filter(Boolean).join("\n") : String(arg)).filter(Boolean).join("\n");
}

// class SheetIndexMap ========================================================

/**
 * A map with sheet indexes and additional dirty flag.
 */
class SheetIndexMap {
    readonly map = new Map<string, number>();
    dirty = false;
}

// class TableSnapshot ========================================================

/**
 * A table reference structure with additional UID of the sheet snapshot for
 * dynamic resolution of the sheet index.
 */
class TableSnapshot implements Identifiable, TableRef {

    // static functions -------------------------------------------------------

    static fromModel(uid: string, tableModel: TableModel): TableSnapshot {
        return new TableSnapshot(uid, tableModel.name, tableModel.getRange(), tableModel.hasHeaderRow(), tableModel.hasFooterRow(), tableModel.getColumnKeys());
    }

    // properties -------------------------------------------------------------

    // interface `Identifiable`
    readonly uid: string;

    // interface `TableRef`
    name: string;
    sheet: number;
    range: Range;
    header: boolean;
    footer: boolean;
    columns: string[];

    // constructor ------------------------------------------------------------

    constructor(
        uid: string,
        name: string,
        range: Range,
        header: boolean,
        footer: boolean,
        columns?: string[]
    ) {
        this.uid = uid;
        this.name = name;
        this.sheet = 0; // will be updated on access
        this.range = range.clone();
        this.header = header;
        this.footer = footer;
        this.columns = columns?.slice() ?? [];
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a deep clone of this instance.
     */
    clone(uid: string, tableName?: string): TableSnapshot {
        return new TableSnapshot(uid, tableName ?? this.name, this.range, this.header, this.footer, this.columns);
    }

    /**
     * Stringifies this instance for debug logging.
     */
    toString(): string {
        return `${this.name}[${this.range}]`;
    }

    /**
     * Converts this instance to JSON for unit testing.
     */
    toJSON(): JSONTableSnapshot {
        return { name: this.name, range: this.range.toOpStr(), header: this.header, footer: this.footer, columns: this.columns.slice() };
    }
}

// class BaseSnapshot =========================================================

/**
 * Represents and conserves a specific state of a spreadsheet model object
 * (sheet or document). Provides access to the labels of all defined names, and
 * the table snapshots in that model object.
 */
class BaseSnapshot implements Identifiable {

    // properties -------------------------------------------------------------

    // interface `Identifiable`
    readonly uid = DObject.makeUid();

    protected readonly labels = new Map<string, string>();
    protected readonly tables = new Map<string, TableSnapshot>();

    // constructor ------------------------------------------------------------

    constructor(labels?: Map<string, string>) {
        map.assign(this.labels, labels);
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts the labels of all defined names into the own map.
     */
    initNames(nameCollection: NameCollection): void {
        for (const nameModel of nameCollection.yieldNameModels()) {
            this.labels.set(nameModel.key, nameModel.label);
        }
    }

    /**
     * Merges the name labels of the passed snapshot into the own map.
     */
    mergeNames(snapshot: BaseSnapshot): void {
        map.assign(this.labels, snapshot.labels);
    }

    /**
     * Inserts the label of a new defined name.
     */
    insertName(label: string): void {
        this.labels.set(mapKey(label), label);
    }

    /**
     * Removes the label of a deleted defined name.
     */
    deleteName(label: string): void {
        this.labels.delete(mapKey(label));
    }

    /**
     * Changes the label of a defined name.
     */
    changeName(label: string, newLabel: string): void {
        this.deleteName(label);
        this.insertName(newLabel);
    }

    /**
     * Returns whether a defined name with the passed label exists.
     */
    hasName(label: string): boolean {
        return this.labels.has(mapKey(label));
    }

    /**
     * Inserts the snapshots of all table ranges into the own map.
     */
    initTables(tableCollection: TableCollection): void {
        for (const tableModel of tableCollection.yieldTableModels()) {
            this.tables.set(tableModel.key, TableSnapshot.fromModel(this.uid, tableModel));
        }
    }

    /**
     * Merges the tables of the passed snapshot into the own map.
     */
    mergeTables(snapshot: BaseSnapshot): void {
        map.assign(this.tables, snapshot.tables);
    }

    /**
     * Inserts the passed table snapshot into the own map.
     */
    insertTable(snapshot: TableSnapshot): TableSnapshot {
        return map.toggle(this.tables, mapKey(snapshot.name), snapshot);
    }

    /**
     * Creates and inserts a new table snapshot into the own map.
     */
    createTable(name: string, range: Range, header: boolean, footer: boolean, columns?: string[]): TableSnapshot {
        return this.insertTable(new TableSnapshot(this.uid, name, range, header, footer, columns));
    }

    /**
     * Removes a table snapshot from the own map.
     */
    deleteTable(name: string): Opt<TableSnapshot> {
        return map.remove(this.tables, mapKey(name));
    }

    /**
     * Transforms this snapshot for a "changeTable" operation.
     */
    changeTable(name: string, changeSet: Partial<TableRef>): TableSnapshot {
        const key = mapKey(name);
        const snapshot = this.tables.get(key);
        if (!snapshot) { throw new Error(`SheetSnapshot.changeTable: table "${name}" does not exist`); }
        if (changeSet.name) {
            map.move(this.tables, key, mapKey(changeSet.name));
            snapshot.name = changeSet.name;
        }
        if (changeSet.range) { snapshot.range = changeSet.range.clone(); }
        if (is.boolean(changeSet.header)) { snapshot.header = changeSet.header; }
        if (is.boolean(changeSet.footer)) { snapshot.footer = changeSet.footer; }
        if (changeSet.columns) { snapshot.columns = changeSet.columns.slice(); }
        return snapshot;
    }

    /**
     * Returns the snapshot of the table with the passed name.
     */
    getTable(tableName: string): Opt<TableSnapshot> {
        return this.tables.get(mapKey(tableName));
    }

    /**
     * Returns the snapshot of the table covering the passed cell address.
     */
    getTableAt(address: Address): Opt<TableSnapshot> {
        return map.find(this.tables, snapshot => snapshot.range.containsAddress(address));
    }

    /**
     * Stringifies this instance for debug logging.
     */
    toString(tables?: boolean): string {
        return joinStrings(
            this.labels.size ? ` names: ${Array.from(this.labels.values()).join(", ")}` : "",
            (tables && this.tables.size) ? ` tables: ${Array.from(this.tables.values()).join(", ")}` : ""
        );
    }

    /**
     * Converts this instance to JSON for unit testing.
     */
    toJSON(tables?: boolean): JSONBaseSnapshot {
        const json = dict.createPartial<JSONBaseSnapshot>();
        if (this.labels.size) { json.names = Array.from(this.labels.values()).sort(); }
        if (tables && this.tables.size) {
            json.tables = ary.sortFrom(this.tables.values(), snapshot => snapshot.name).map(snapshot => snapshot.toJSON());
        }
        return json;
    }
}

// class SheetSnapshot ========================================================

/**
 * Represents and conservates a specific state of a sheet model.
 */
class SheetSnapshot extends BaseSnapshot implements Cloneable<SheetSnapshot> {

    // static functions -------------------------------------------------------

    static fromModel(sheetModel: SheetModel): SheetSnapshot {
        const snapshot = new SheetSnapshot(sheetModel.getName(), sheetModel.isSupported, sheetModel.isVisible());
        snapshot.initNames(sheetModel.nameCollection);
        snapshot.initTables(sheetModel.tableCollection);
        return snapshot;
    }

    // properties -------------------------------------------------------------

    key: string;
    name: string;
    readonly supported: boolean;
    visible: boolean;

    // constructor ------------------------------------------------------------

    constructor(name: string, supported: boolean, visible: boolean, labels?: Map<string, string>) {
        super(labels);
        this.key = mapKey(name);
        this.name = name;
        this.supported = supported;
        this.visible = visible;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a deep clone of this instance.
     */
    clone(): SheetSnapshot {
        const snapshot = new SheetSnapshot(this.name, this.supported, this.visible, this.labels);
        this.tables.forEach((tableRef, tableKey) => {
            snapshot.tables.set(tableKey, tableRef.clone(snapshot.uid));
        });
        return snapshot;
    }

    /**
     * Returns whether this sheet is visible (supported sheet type, and visible
     * attribute).
     */
    isVisible(): boolean {
        return this.supported && this.visible;
    }

    /**
     * Returns a copy of this instance with new sheet name and table names.
     */
    copySheet(name: string, tableNames?: Dict<string>): SheetSnapshot {
        const snapshot = new SheetSnapshot(name, this.supported, this.visible, this.labels);
        if (tableNames) {
            dict.forEach(tableNames, (newName, oldName) => {
                map.visit(this.tables, mapKey(oldName), tableRef => {
                    snapshot.tables.set(mapKey(newName), tableRef.clone(snapshot.uid, newName));
                });
            });
        }
        return snapshot;
    }

    /**
     * Changes the settings of this sheet snapshot.
     */
    changeSheet(name?: string, visible?: boolean): void {
        if (name) {
            this.name = name;
            this.key = mapKey(name);
        }
        if (is.boolean(visible)) {
            this.visible = visible;
        }
    }

    /**
     * Stringifies this instance for debug logging.
     */
    override toString(): string {
        return joinStrings(`SheetSnapshot: uid="${this.uid}" name="${this.name}"`, super.toString(true));
    }

    /**
     * Converts this instance to JSON for unit testing.
     */
    override toJSON(): JSONSheetSnapshot {
        return { ...super.toJSON(true), name: this.name };
    }
}

// class ModelSnapshot ========================================================

/**
 * Implements the `IDocumentAccess` interface representing and conserving a
 * specific state of a spreadsheet document.
 */
export class DocumentSnapshot implements Identifiable, Cloneable<DocumentSnapshot>, IDocumentAccess {

    // static functions -------------------------------------------------------

    /**
     * Creates a new instance with the current state of the passed spreadsheet
     * document model.
     */
    static fromModel(docModel: SpreadsheetModel): DocumentSnapshot {

        // create a new snapshot with all globally defined names
        const snapshot = new DocumentSnapshot(docModel.addressFactory);
        snapshot.#globals.initNames(docModel.nameCollection);

        // initialize all sheet models
        for (const sheetModel of docModel.yieldSheetModels()) {
            snapshot.#insertSnapshot(sheetModel.getIndex(), SheetSnapshot.fromModel(sheetModel));
        }

        return snapshot;
    }

    // properties -------------------------------------------------------------

    // interface `Identifiable`
    readonly uid = DObject.makeUid();

    /** The address factory of the document. */
    readonly addressFactory: AddressFactory;

    // globals snapshot (names and tables)
    readonly #globals = new BaseSnapshot();
    // all sheet snapshots
    readonly #snapshots: SheetSnapshot[] = [];
    // maps sheet snapshot UIDs to sheet indexes
    readonly #indexByUid = new SheetIndexMap();
    // maps sheet names to sheet indexes
    readonly #indexByKey = new SheetIndexMap();

    // constructor ------------------------------------------------------------

    constructor(addressFactory: AddressFactory) {
        this.addressFactory = addressFactory;
    }

    // `IDocumentAccess` methods ----------------------------------------------

    /**
     * Returns the exact name of the sheet at the specified index.
     */
    getSheetName(sheet: number): string | null {
        // sheet index is allowed to be invalid (e.g. negative)
        return this.#snapshots[sheet]?.name ?? null;
    }

    /**
     * Returns the current index of the sheet with the specified name.
     */
    getSheetIndex(sheetName: string): number {
        return this.#resolveSheet(this.#indexByKey, "key", mapKey(sheetName));
    }

    /**
     * Resolves data for the specified defined name.
     */
    resolveName(sheet: OptSheet, label: string): Opt<DefNameRef> {
        return this.#getSnapshot(sheet).hasName(label) ? { sheet, label } : undefined;
    }

    /**
     * Resolves data for the specified table range.
     */
    resolveTable(tableName: string): Opt<TableRef> {
        const tableRef = this.#globals.getTable(tableName);
        if (!tableRef) { return undefined; }
        // resolve current sheet index (-1 for deleted sheets)
        tableRef.sheet = this.#resolveSheet(this.#indexByUid, "uid", tableRef.uid);
        return (tableRef.sheet >= 0) ? tableRef : undefined;
    }

    /**
     * Resolves data for a table range at the specified position.
     */
    resolveTableAt(sheet: number, address: Address): Opt<TableRef> {
        // sheet index is allowed to be invalid (e.g. negative)
        const tableRef = this.#snapshots[sheet]?.getTableAt(address);
        if (tableRef) { tableRef.sheet = sheet; }
        return tableRef;
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a deep clone of this instance.
     */
    clone(): DocumentSnapshot {
        const snapshot = new DocumentSnapshot(this.addressFactory);
        snapshot.#globals.mergeNames(this.#globals);
        this.#snapshots.forEach((source, sheet) => snapshot.#insertSnapshot(sheet, source.clone()));
        return snapshot;
    }

    /**
     * Returns the number of sheets in the spreadsheet document.
     */
    getSheetCount(): number {
        return this.#snapshots.length;
    }

    /**
     * Returns whether a visible sheet remains in the document when deleting or
     * hiding the specified sheet(s).
     */
    hasVisibleSheet(...skipSheets: number[]): number {
        return this.#snapshots.reduce((count, snapshot, sheet) => count + ((snapshot.isVisible() && !skipSheets.includes(sheet)) ? 1 : 0), 0);
    }

    /**
     * Transforms this snapshot for an "insertSheet" operation.
     */
    insertSheet(sheet: number, name: string, supported: boolean, visible: boolean): void {
        this.#insertSnapshot(sheet, new SheetSnapshot(name, supported, visible));
    }

    /**
     * Transforms this snapshot for a "deleteSheet" operation.
     */
    deleteSheet(sheet: number): void {
        this.#removeSnapshot(sheet);
    }

    /**
     * Transforms this snapshot for a "moveSheet" operation.
     */
    moveSheet(from: number, to: number): void {
        // table snapshots remain as they are (sheet indexes will be resolved dynamically)
        this.#insertSnapshot(to, this.#removeSnapshot(from), { skipTables: true });
    }

    /**
     * Transforms this snapshot for a "moveSheets" operation.
     */
    moveSheets(sortVector: OTSortVector): void {
        if (this.#snapshots.length !== sortVector.length) {
            throw new Error(`DocumentSnapshot.moveSheets: invalid length of sort vector: passed=${sortVector.length}, expected=${this.#snapshots.length}`);
        }
        ary.permute(this.#snapshots, sortVector);
        this.#invalidateMaps();
        // table snapshots remain as they are (sheet indexes will be resolved dynamically)
    }

    /**
     * Transforms this snapshot for a "copySheet" operation.
     */
    copySheet(from: number, to: number, name: string, tableNames?: Dict<string>): void {
        this.#insertSnapshot(to, this.#getSnapshot(from).copySheet(name, tableNames));
    }

    /**
     * Transforms this snapshot for a "changeSheet" operation.
     */
    changeSheet(sheet: number, name?: string, visible?: boolean): void {
        const snapshot = this.#getSnapshot(sheet);
        const oldKey = snapshot.key;
        snapshot.changeSheet(name, visible);
        map.move(this.#indexByKey.map, oldKey, snapshot.key);
    }

    /**
     * Transforms this snapshot for an "insertName" operation.
     */
    insertName(sheet: OptSheet, label: string): void {
        this.#getSnapshot(sheet).insertName(label);
    }

    /**
     * Transforms this snapshot for a "deleteName" operation.
     */
    deleteName(sheet: OptSheet, label: string): void {
        this.#getSnapshot(sheet).deleteName(label);
    }

    /**
     * Transforms this snapshot for a "changeName" operation with a new label.
     */
    changeName(sheet: OptSheet, label: string, newLabel: string): void {
        this.#getSnapshot(sheet).changeName(label, newLabel);
    }

    /**
     * Transforms this snapshot for an "insertTable" operation.
     */
    insertTable(sheet: number, name: string, range: Range, header: boolean, footer: boolean, columns?: string[]): void {
        const snapshot = this.#getSnapshot(sheet).createTable(name, range, header, footer, columns);
        this.#globals.insertTable(snapshot);
    }

    /**
     * Transforms this snapshot for a "deleteTable" operation.
     */
    deleteTable(sheet: number, name: string): void {
        this.#getSnapshot(sheet).deleteTable(name);
        this.#globals.deleteTable(name);
    }

    /**
     * Transforms this snapshot for a "changeTable" operation.
     */
    changeTable(sheet: number, name: string, changeSet: Partial<TableRef>): void {
        const snapshot = this.#getSnapshot(sheet).changeTable(name, changeSet);
        if (changeSet.name) {
            this.#globals.deleteTable(name);
            this.#globals.insertTable(snapshot);
        }
    }

    /**
     * Stringifies this instance for debug logging.
     */
    toString(): string {
        return joinStrings(`DocumentSnapshot: uid="${this.uid}" sheets=${this.#snapshots.length}`, this.#globals, this.#snapshots);
    }

    /**
     * Converts this instance to JSON for unit testing.
     */
    toJSON(): JSONDocumentSnapshot {
        return { ...this.#globals.toJSON(), sheets: this.#snapshots.map(snapshot => snapshot.toJSON()) };
    }

    // private methods --------------------------------------------------------

    /**
     * Throws an exception for an invalid sheet index.
     */
    #throwSheetError(method: string, sheet: number): never {
        throw new Error(`DocumentSnapshot.${method}: invalid sheet index: passed=${sheet}, max=${this.#snapshots.length - 1}`);
    }

    /**
     * Returns a snapshot for the passed sheet index.
     */
    #getSnapshot(sheet: number): SheetSnapshot;
    #getSnapshot(sheet: OptSheet): BaseSnapshot;
    // implementation
    #getSnapshot(sheet: OptSheet): BaseSnapshot {
        if (sheet === null) { return this.#globals; }
        const snapshot = this.#snapshots[sheet];
        if (!snapshot) { this.#throwSheetError("getSnapshot", sheet); }
        return snapshot;
    }

    /**
     * Inserts the passed sheet snapshot object into this document snapshot,
     * and updates other internal settings.
     */
    #insertSnapshot(sheet: number, snapshot: SheetSnapshot, options?: { skipTables?: boolean }): SheetSnapshot {

        // invalidate index maps (will be refreshed on demand) unless the new sheet has been appended
        const sheets = this.#snapshots.length;
        if (sheet === sheets) {
            this.#indexByUid.map.set(snapshot.uid, sheet);
            this.#indexByKey.map.set(snapshot.key, sheet);
        } else if (sheet < sheets) {
            this.#invalidateMaps();
        } else {
            this.#throwSheetError("insertSnapshot", sheet);
        }

        // merge table references into global map
        if (!options?.skipTables) { this.#globals.mergeTables(snapshot); }

        // insert the sheet snapshot into the array
        return ary.insertAt(this.#snapshots, sheet, snapshot);
    }

    /**
     * Removes the sheet snapshot object with the specified sheet index from
     * this document snapshot, and updates other internal settings.
     */
    #removeSnapshot(sheet: number): SheetSnapshot {

        // remove the sheet snapshot from the array
        const snapshot = ary.deleteAt(this.#snapshots, sheet);
        if (!snapshot) { this.#throwSheetError("removeSnapshot", sheet); }

        // invalidate index maps (will be refreshed on demand) unless the last sheet has been deleted
        if (sheet === this.#snapshots.length) {
            this.#indexByUid.map.delete(snapshot.uid);
            this.#indexByKey.map.delete(snapshot.key);
        } else {
            this.#invalidateMaps();
        }

        // performance: do not waste time deleting all affected tables references in the
        // own map; they will be detected when trying to resolve the sheet index from UID

        return snapshot;
    }

    /**
     * Invalidates the sheet index maps. When resolving a sheet index the next
     * time, the respective map will be rebuilt once.
     */
    #invalidateMaps(): void {
        this.#indexByUid.dirty = this.#indexByKey.dirty = true;
    }

    /**
     * Returns the current index of the sheet with the specified key. Updates
     * the passed index map on demand.
     */
    #resolveSheet(indexes: SheetIndexMap, prop: "uid" | "key", key: string): number {
        if (indexes.dirty) {
            this.#snapshots.forEach((snapshot, sheet) => indexes.map.set(snapshot[prop], sheet));
            indexes.dirty = false;
        }
        return indexes.map.get(key) ?? -1;
    }
}
