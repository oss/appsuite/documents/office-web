/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { is, math, fun, itr, ary, map, dict, json, jpromise } from "@/io.ox/office/tk/algorithms";
import { BREAK, addProperty, convertHmmToLength } from "@/io.ox/office/tk/utils";
import { UNITTEST } from "@/io.ox/office/tk/config";

import { equalBorders } from "@/io.ox/office/editframework/utils/border";

import { CHANGE_COLUMNS, CHANGE_ROWS, DELETE_COLUMNS, DELETE_ROWS, INSERT_COLUMNS, INSERT_ROWS } from "@/io.ox/office/spreadsheet/utils/operations";
import {
    MIN_CELL_SIZE, MAX_CHANGE_COLS_COUNT, MAX_CHANGE_ROWS_COUNT, MAX_AUTOFILL_COL_ROW_COUNT, ATTR_BORDER_KEYS,
    Direction, Interval, IntervalArray, modelLogger, makeRejected,
    getTotalCellPadding, getTextPadding, isLeadingDir, getOuterBorderKey, getInnerBorderKey, getBorderName
} from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { containsResizeAttr, ColModel, RowModel, ColRowDescriptor } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import { createBorderChangeSet, createVisibleBorderChangeSet, SheetStyleCache } from "@/io.ox/office/spreadsheet/model/operation/stylecache";
import { cloneWithCacheKey } from "@/io.ox/office/spreadsheet/model/operation/builder";

// types ======================================================================

/**
 * An enumeration of methods to specify how to look for another column/row, if
 * the targeted column/row is hidden.
 */
export /*enum*/ const VisibleEntryMethod = {

    /**
     * Does not look for other columns/rows.
     */
    EXACT: "exact",

    /**
     * Looks for a visible column/row following the target column/row.
     */
    NEXT: "next",

    /**
     * Looks for a visible column/row preceding the target column/row.
     */
    PREV: "prev",

    /**
     * First, looks for visible column/row following the target column/row. If
     * there is none available, looks for a visible column/row preceding the
     * target column/row.
     */
    NEXT_PREV: "nextPrev",

    /**
     * First, looks for visible column/row preceding the target column/row. If
     * there is none available, looks for a visible column/row following the
     * target column/row.
     */
    PREV_NEXT: "prevNext"
};

// class ColRowCollection =====================================================

/**
 * Collects information about all columns or all rows of a single sheet in a
 * spreadsheet document.
 */
export /*abstract*/ class ColRowCollection/*<ModelT extends ColModel | RowModel>*/ extends SheetChildModel {

    // sorted sparse array of index ranges
    #entryModels = this.member/*<ModelT[]>*/([]);

    // the orientation flag
    #columns/*: boolean*/;

    // constructor ------------------------------------------------------------

    /**
     * @param {SheetModel} sheetModel
     *  The sheet model instance containing this collection.
     *
     * @param {boolean} columns
     *  Whether this collection contains column headers (`true`) or row headers
     *  (`false`).
     */
    /*protected*/ constructor(sheetModel, columns) {

        // base constructor
        super(sheetModel);

        // the largest valid column/row index
        this.maxIndex = this.docModel.addressFactory.getMaxIndex(columns);

        // default model for entries missing in the collection
        const defStyleId = this.docModel.autoStyles.getDefaultStyleId(true);
        this.defaultModel = this.member(this.implCreateModel(new Interval(0, this.maxIndex), null, defStyleId, true));

        // the orientation flag
        this.#columns = columns;

        // initialize column/row size of the default entry model
        this.#updateDefaultSize();

        // update entry sizes, after the default column/row size defined by the sheet has been changed
        this.listenTo(sheetModel, "change:attributes", () => {
            this.#updateDefaultSize(true);
        });

        // update entry sizes, after the zoom factor of the sheet has been changed
        this.listenToProp(sheetModel.propSet, "zoom", () => {
            this.#updateDefaultSize();
            this.#updateModelGeometry(0, true);
        });

        // update entry sizes after the document has been loaded (the collection may
        // be destroyed before import finishes via the operation "deleteSheet")
        this.waitForImportSuccess(alreadyImported => {
            if (!alreadyImported) { this.#updateDefaultSize(true); }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a new column/row interval covering the entire collection.
     *
     * @returns {Interval}
     *  A new column/row interval covering the entire collection.
     */
    getFullInterval() {
        return new Interval(0, this.maxIndex);
    }

    /**
     * Returns the model of the column/row with the specified index.
     *
     * @param {number} index
     *  The zero-based index of the column/row.
     *
     * @returns {ModelT}
     *  The model for the specified column/row. For gaps between existing entry
     *  models, the default entry model will be passed here.
     */
    getModel(index) {
        // the model of the covered or the *following* entry
        const entryModel = this.#entryModels[this.#getModelArrayIndex(index)];
        return (!entryModel || (index < entryModel.interval.first)) ? this.defaultModel : entryModel;
    }

    /**
     * Returns a descriptor object for the column/row with the specified index.
     *
     * @deprecated
     *
     * @param {number} index
     *  The zero-based index of the column/row.
     *
     * @returns {ColRowDescriptor}
     *  A descriptor object for the specified column/row.
     */
    getEntry(index) {

        // collection is empty: create a descriptor with default settings, covering the entire collection
        if (!this.#entryModels.length) {
            return new ColRowDescriptor(this.defaultModel, index, this.getFullInterval());
        }

        // the array index of the covered, or the following entry model
        const ai = this.#getModelArrayIndex(index);

        // entry points into gap before first entry: use default size
        if ((ai < 0) || ((ai === 0) && (index < this.#entryModels[0].interval.first))) {
            return new ColRowDescriptor(this.defaultModel, index, new Interval(0, this.#entryModels[0].interval.first - 1));
        }

        // index points into a gap between two entries, or to the gap after the last entry
        const entryModel = this.#entryModels[ai];
        if (!entryModel || (index < entryModel.interval.first)) {
            const prevModel = this.#entryModels[ai - 1];
            const gapInterval = new Interval(prevModel.interval.last + 1, entryModel ? (entryModel.interval.first - 1) : this.maxIndex);
            const entryDesc = new ColRowDescriptor(this.defaultModel, index, gapInterval);
            // adjust entry offsets (default entry is always relative to index 0 instead of start index of current gap)
            const relIndex = index - prevModel.interval.last - 1;
            entryDesc.offsetHmm = prevModel.getEndOffset() + entryDesc.sizeHmm * relIndex;
            entryDesc.offset = prevModel.getEndOffset({ pixel: true }) + entryDesc.size * relIndex;
            return entryDesc;
        }

        // index points into a collection entry
        return new ColRowDescriptor(entryModel, index);
    }

    /**
     * Creates an iterator that visits all existing header models that cover
     * the specified column/row interval.
     *
     * @param {Interval} interval
     *  The column/row interval to be visited by the iterator.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only visible header models will be covered by the
     *    iterator. If set to `false`, only hidden header models will be
     *    covered by the iterator. By default, all visible and hidden header
     *    models will be visited.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the header models will be visited in reversed
     *    order.
     *
     * @yields {ModelT}
     *  The existing header models.
     */
    *yieldModels(interval, options) {

        // whether to iterate in reversed order
        const reverse = options?.reverse;
        // visit only visible or hidden intervals if specified
        const visible = options?.visible;
        const visibleFn = is.boolean(visible) ? (entryModel => visible === !entryModel.hidden) : fun.true;

        // the array index of the header model containing the start index
        const index = reverse ? interval.last : interval.first;
        let ai = this.#getModelArrayIndex(index, reverse);

        // fail-safety: check for invalid source intervals
        if (reverse ? (index < 0) : (index > this.maxIndex)) {
            modelLogger.error(() => `$badge{ColRowCollection} yieldModels: invalid source interval: ${interval.toOpStr(this.#columns)}`);
            return;
        }

        // visit all header models covered by the source interval
        for (;;) {

            // the current header model; exit loop if source interval is left
            const headerModel = this.#entryModels[ai];
            if (!headerModel?.interval.overlaps(interval)) { break; }

            // visit the current header model if it matches visibility filter
            if (visibleFn(headerModel)) {
                yield headerModel;
            }

            // prepare array index for the following (reverse: preceding) entry model for next loop step
            if (reverse) { ai -= 1; } else { ai += 1; }
        }
    }

    /**
     * Creates an iterator that visits all band models that cover the specified
     * column/row interval.
     *
     * @param {Interval} interval
     *  The column/row interval to be visited by the iterator.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only visible band models will be covered by the
     *    iterator. If set to `false`, only hidden band models will be covered
     *    by the iterator. By default, all visible and hidden band models will
     *    be visited.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the band models will be visited in reversed order.
     *
     * @yields {BandModelEntry<ModelT>}
     *  The result objects with the following properties:
     *  - {ModelT} model
     *    The band model currently visited. For gaps between existing band
     *    models, the default band model will be passed here.
     *  - {Interval} interval
     *    The index interval actually covered by the band model, AND by the
     *    source interval passed to this method.
     *  - {number} offsetHmm
     *    The start position of the interval contained in the property
     *    "interval", in 1/100 of millimeters.
     *  - {number} offset
     *    The start position of the interval contained in the property
     *    "interval", in pixels.
     */
    *yieldBandModels(interval, options) {

        // the default model
        const defModel = this.defaultModel;
        // whether to iterate in reversed order
        const reverse = options?.reverse;
        // visit only visible or hidden intervals if specified
        const visible = options?.visible;
        const visibleFn = is.boolean(visible) ? (entryModel => visible === !entryModel.hidden) : fun.true;

        // current entry index during iteration
        let index = reverse ? interval.last : interval.first;
        // the array index of the entry model containing the start index
        let ai = this.#getModelArrayIndex(index, reverse);

        // creates a partially initialized iterator value with "model" and "interval" properties
        const updateIndex = stepInterval => {
            // move index to the next interval for the next iterator step
            index = reverse ? (stepInterval.first - 1) : (stepInterval.last + 1);
            // return whether index is still valid
            return interval.containsIndex(index);
        };

        // creates a partially initialized iterator value with "model" and "interval" properties
        const createBandEntry = (bandModel, stepInterval) => {
            // shorten the passed interval to the interval visited by the iterator
            return { model: bandModel, interval: stepInterval.intersect(interval) };
        };

        // returns the iterator result for the passed interval of a gap between entry models
        const createGapEntry = stepInterval => {

            // create an iterator value with the default entry model used for the gaps
            const bandEntry = createBandEntry(defModel, stepInterval);

            // start position of the gap is the end offset of the preceding model visited before
            // (reverse mode: of the next model to be visited)
            const gapOffsetModel = reverse ? this.#entryModels[ai] : this.#entryModels[ai - 1];

            // start position of the gap, as index, and in 1/100mm and pixels
            const gapIndex = gapOffsetModel ? (gapOffsetModel.interval.last + 1) : 0;
            const gapOffsetHmm = gapOffsetModel?.getEndOffset() ?? 0;
            const gapOffset = gapOffsetModel?.getEndOffset({ pixel: true }) ?? 0;

            // add the start positions of the interval actually covered by the result
            const relIndex = bandEntry.interval.first - gapIndex;
            bandEntry.offsetHmm = gapOffsetHmm + defModel.sizeHmm * relIndex;
            bandEntry.offset = gapOffset + defModel.size * relIndex;

            return bandEntry;
        };

        // fail-safety: check for invalid source intervals
        if (reverse ? (index < 0) : (index > this.maxIndex)) {
            modelLogger.error(() => `$badge{ColRowCollection} yieldBandModels: invalid source interval: ${interval.toOpStr(this.#columns)}`);
            return;
        }

        // visit all band models covered by the source interval, and the gaps inbetween
        for (;;) {

            // the current band model (`undefined` for leading/trailing gap, will be visited after this loop)
            const bandModel = this.#entryModels[ai];
            if (!bandModel) { break; }

            // visit the gap before (reverse: behind) the current band model
            const bandInterval = bandModel.interval;
            if (!bandInterval.containsIndex(index)) {
                const first = reverse ? (bandInterval.last + 1) : index;
                const last = reverse ? index : (bandInterval.first - 1);
                const gapInterval = new Interval(first, last);
                // visit the gap if the default model matches visibility filter
                if (visibleFn(defModel)) { yield createGapEntry(gapInterval); }
                // exit loop if `bandModel` is outside of source interval
                if (!updateIndex(gapInterval)) { break; }
            }

            // visit the current band model if it matches visibility filter
            if (visibleFn(bandModel)) {
                const bandEntry = createBandEntry(bandModel, bandInterval);
                // add the start positions of the interval actually covered by the result
                const relIndex = bandEntry.interval.first - bandInterval.first;
                bandEntry.offsetHmm = bandModel.offsetHmm + bandModel.sizeHmm * relIndex;
                bandEntry.offset = bandModel.offset + bandModel.size * relIndex;
                yield bandEntry;
            }

            // prepare array index for the following (reverse: preceding) entry model for next loop step
            if (reverse) { ai -= 1; } else { ai += 1; }

            // exit loop if source interval is done now (`bandModel` partially outside)
            if (!updateIndex(bandInterval)) { break; }
        }

        // visit the gap after the last (reverse: before the first) visited band model
        if (interval.containsIndex(index) && visibleFn(defModel)) {
            const first = (reverse || !this.#entryModels.length) ? 0 : (ary.at(this.#entryModels, -1).interval.last + 1);
            const last = (reverse && this.#entryModels.length) ? (this.#entryModels[0].interval.first - 1) : this.maxIndex;
            const gapInterval = new Interval(first, last);
            yield createGapEntry(gapInterval);
        }
    }

    /**
     * Creates an iterator that visits all collection entries in the specified
     * index intervals.
     *
     * @param {IntervalSource} intervals
     *  An array of index intervals, or a single index interval.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only visible collection entries will be covered by
     *    the iterator. By default, all visible and hidden entries will be
     *    visited.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the intervals AND the indexes in each interval will
     *    be processed in reversed order.
     *  - {boolean} [options.unique=false]
     *    If set to `true`, the iterator will visit only the first entry of
     *    multiple consecutive collection entries with the exact same autostyle
     *    and formatting attributes.
     *
     * @yields {ColRowDescriptor}
     *  The entry descriptor objects with the following additional properties:
     *  - {Interval} value.origInterval
     *    The original index interval (from the passed interval array)
     *    containing the current collection entry.
     *  - {number} value.origIndex
     *    The array index of the original interval contained in the property
     *    "origInterval".
     */
    *indexEntries(intervals, options) {

        const reverse = options?.reverse;
        const unique = options?.unique;

        // visit the intervals in the passed data source
        for (const [srcIndex, srcInterval] of ary.entries(IntervalArray.cast(intervals), { reverse })) {

            // visit the entry models in the current interval
            for (const { interval, model, offsetHmm, offset } of this.yieldBandModels(srcInterval, options)) {

                // size of one entry for the current model
                const { sizeHmm, size } = model;

                // unique mode: visit entire intervals in one step
                const indexes = unique ? [interval.first] : interval.indexes({ reverse });
                for (const index of indexes) {
                    const relIndex = index - interval.first;
                    const entryDesc = new ColRowDescriptor(model, index, interval);
                    entryDesc.offsetHmm = offsetHmm + sizeHmm * relIndex;
                    entryDesc.offset = offset + size * relIndex;
                    entryDesc.origInterval = srcInterval;
                    entryDesc.origIndex = srcIndex;
                    yield entryDesc;
                }
            }
        }
    }

    /**
     * Creates an iterator that visits the intervals with equal autostyle
     * covered by the passed intervals.
     *
     * @param {IntervalSource} intervals
     *  An array of index intervals, or a single index interval.
     *
     * @param {ModelIteratorOptions} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible=false]
     *    If set to `true`, only visible collection entries will be covered by
     *    the iterator. By default, all visible and hidden entries will be
     *    visited.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the intervals AND the equally formatted parts in
     *    each interval will be processed in reversed order.
     *
     * @yields {StyleIteratorValue}
     *  The result objects with the following properties:
     *  - {Interval} value.interval
     *    The current index interval.
     *  - {string|null} value.style
     *    The style identifier of the cell autostyle contained by all
     *    collection entries in the interval; or the value `null`, if a row
     *    interval has no active autostyle due to its formatting attribute
     *    "customStyle" set to `false`.
     *  - {Interval} value.origInterval
     *    The original index interval (from the passed interval array)
     *    containing the current collection entry.
     *  - {number} value.origIndex
     *    The array index of the original index interval contained in the
     *    property "origInterval".
     */
    *styleIntervals(intervals, options) {

        // visit each index interval
        for (const [index, interval] of ary.entries(IntervalArray.cast(intervals), { reverse: options?.reverse })) {

            // create a model iterator with additional style information
            let styleIterator = itr.map(this.yieldBandModels(interval, options), bandEntry => {
                bandEntry.suid = bandEntry.model.getAutoStyleId(true);
                bandEntry.style = bandEntry.model.getAutoStyleId();
                return bandEntry;
            });

            // merge adjecent intervals with equal autostyle
            styleIterator = itr.merge(styleIterator, (entry1, entry2) => {
                const int1 = entry1.interval, int2 = entry2.interval;
                if ((int1.last + 1 === int2.first) && (entry1.suid === entry2.suid)) {
                    int1.last = int2.last;
                    return entry1;
                }
            });

            // add original interval and index to the entries
            for (const styleEntry of styleIterator) {
                styleEntry.origInterval = interval;
                styleEntry.origIndex = index;
                yield styleEntry;
            }
        }
    }

    /**
     * Returns an iterator for column/row intervals matching specific criteria.
     *
     * @param {IntervalSource} bounds
     *  The bounding column/row intervals to be processed.
     *
     * @param {ModelIteratorOptions} [options]
     *  Optional parameters.
     *
     * @yields {Interval}
     *  The matching column/row intervals in the passed boundaries.
     */
    *intervals(bounds, options) {

        // visit all boundary intervals in the correct order
        for (const interval of ary.values(IntervalArray.cast(bounds), options)) {

            // visit the intervals of all entry models in the current interval
            const iterator = itr.map(this.yieldBandModels(interval, options), entry => entry.interval);

            // merge adjacent intervals
            yield* itr.merge(iterator, (i1, i2) => (i1.last + 1 === i2.first) ? new Interval(i1.first, i2.last) : undefined);
        }
    }

    /**
     * Returns an iterator for individual column/row indexes matching specific
     * criteria.
     *
     * @param {IntervalSource} bounds
     *  The bounding column/row intervals to be processed.
     *
     * @param {ModelIteratorOptions} [options]
     *  Optional parameters.
     *
     * @yields {number}
     *  The indexes of all matching columns/rows in the passed boundaries.
     */
    *indexes(bounds, options) {
        for (const interval of this.intervals(bounds, options)) {
            yield* interval.indexes(options);
        }
    }

    /**
     * Returns the number of individual columns or rows contained in the passed
     * boundary intervals matching specific criteria.
     *
     * @param {IntervalSource} bounds
     *  The bounding column/row intervals to be processed.
     *
     * @returns {number}
     *  The number of matching columns or rows contained in the passed
     *  boundaries.
     */
    countIndexes(bounds, options) {
        let count = 0;
        for (const interval of this.intervals(bounds, options)) {
            count += interval.size();
        }
        return count;
    }

    /**
     * Returns the mixed column/row attributes of all entries covered by the
     * passed index intervals.
     *
     * @param {IntervalSource} intervals
     *  An array of index intervals, or a single index interval.
     *
     * @returns {Dict}
     *  The mixed attributes of all columns/rows covered by the passed
     *  intervals, as simple object (NOT mapped as a "column" or "row"
     *  sub-object). The result does not contain any cell formatting
     *  attributes. All attributes that could not be resolved unambiguously
     *  will be set to the value null.
     */
    getMixedAttributes(intervals) {

        // the resulting mixed attributes
        let mixedAttrs = null;
        // whether the default model has been visited once (no need to process it repeatedly)
        let defaultVisited = false;

        // visit all equally formatted intervals, and build the mixed attributes
        IntervalArray.merge(intervals).some(interval => {
            return itr.some(this.yieldBandModels(interval), bandEntry => {

                // do not visit the default model representing all gaps in the collection multiple times
                const entryModel = bandEntry.model;
                if (entryModel === this.defaultModel) {
                    if (defaultVisited) { return; }
                    defaultVisited = true;
                }

                // the merged attributes of the current interval (type column/row only)
                const attrs = entryModel.getMergedEntryAttributes();
                // whether any attribute is still unambiguous
                let hasNonNull = false;

                // first visited interval: store initial attributes
                if (!mixedAttrs) {
                    mixedAttrs = json.deepClone(attrs);
                    return;
                }

                // mix the attributes of the visited interval into the result
                dict.forEach(attrs, (value, name) => {
                    if (_.isEqual(value, mixedAttrs[name])) {
                        hasNonNull = true;
                    } else {
                        mixedAttrs[name] = null;
                    }
                });

                // stop iteration, if all attributes are ambiguous
                return !hasNonNull;
            });
        });

        return mixedAttrs;
    }

    /**
     * Returns the total size of all columns/rows represented by this
     * collection; either in 1/100 of millimeters independent from the current
     * sheet zoom factor, or in pixels according to the current sheet zoom
     * factor.
     *
     * @param {SheetPixelOptions} [options]
     *  Optional parameters. The option "pixel" determines whether to return
     *  the total size in 1/100 of millimeters, or in zoom-dependent pixels.
     *
     * @returns {number}
     *  The total size of all columns/rows in the sheet, either in 1/100 of
     *  millimeters, or in pixels.
     */
    getTotalSize(options) {

        // the last existing entry model
        const lastModel = ary.at(this.#entryModels, -1);
        // the start index of the gap after the last entry model
        const index = lastModel ? (lastModel.interval.last + 1) : 0;
        // the end position of the last entry model
        const offset = lastModel ? lastModel.getEndOffset(options) : 0;

        // add the size of the gap following the last entry model, this becomes the total size
        return offset + this.defaultModel.getSize(options) * (this.maxIndex - index + 1);
    }

    /**
     * Returns whether the column/row with the specified index is visible.
     *
     * @param {number} index
     *  The zero-based index of a column/row.
     *
     * @returns {boolean}
     *  Whether the column/row is visible.
     */
    isVisibleIndex(index) {
        return !this.getModel(index).hidden;
    }

    /**
     * Returns the index of the nearest visible column/row (ascending search).
     *
     * @param {number} index
     *  The zero-based index of a column/row.
     *
     * @param {number} [lastIndex]
     *  If specified, searching for a visible column/row will stop after this
     *  index. The index MUST be equal or greater than the passed start index.
     *  If omitted, searches to the end of the sheet.
     *
     * @returns {Opt<number>}
     *  The index of the nearest visible column/row (a value equal to or
     *  greater than `index`); or `undefined`, if no more visible columns/rows
     *  are available.
     */
    findNextVisibleIndex(index, lastIndex = this.maxIndex) {
        if (index > this.maxIndex) { return undefined; }
        // use an iterator to visit the first visible model following the passed index
        const interval = new Interval(index, lastIndex);
        const bandEntry = itr.shift(this.yieldBandModels(interval, { visible: true }));
        return bandEntry?.interval.containsIndex(index) ? index : bandEntry?.interval.first;
    }

    /**
     * Returns the index of the nearest visible column/row (descending search).
     *
     * @param {number} index
     *  The zero-based index of a column/row.
     *
     * @param {number} [firstIndex]
     *  If specified, searching for a visible column/row will stop before this
     *  index. The index MUST be equal or less than the passed start index. If
     *  omitted, searches to the beginning of the sheet.
     *
     * @returns {Opt<number>}
     *  The index of the nearest visible column/row (a value equal to or less
     *  than `index`); or `undefined`, if no more visible columns/rows are
     *  available.
     */
    findPrevVisibleIndex(index, firstIndex = 0) {
        if (index < 0) { return undefined; }
        // use a reverse iterator to visit the first visible entry preceding the passed index
        const interval = new Interval(firstIndex, index);
        const bandEntry = itr.shift(this.yieldBandModels(interval, { visible: true, reverse: true }));
        return bandEntry?.interval.containsIndex(index) ? index : bandEntry?.interval.last;
    }

    /**
     * Returns the index of the nearest visible column/row, according to the
     * specified search method.
     *
     * @param {number} index
     *  The zero-based index of a column/row.
     *
     * @param {VisibleEntryMethod} [method]
     *  Specifies how to look for another column/row, if the column/row at
     *  `index` is hidden.
     *
     * @returns {Opt<number>}
     *  The index of the nearest visible column/row; or `undefined`, if no
     *  visible columns/rows are available.
     */
    findVisibleIndex(index, method = VisibleEntryMethod.EXACT) {
        switch (method) {
            case VisibleEntryMethod.EXACT:      return this.findNextVisibleIndex(index, index);
            case VisibleEntryMethod.NEXT:       return this.findNextVisibleIndex(index);
            case VisibleEntryMethod.PREV:       return this.findPrevVisibleIndex(index);
            case VisibleEntryMethod.NEXT_PREV:  return this.findNextVisibleIndex(index) ?? this.findPrevVisibleIndex(index);
            case VisibleEntryMethod.PREV_NEXT:  return this.findPrevVisibleIndex(index) ?? this.findNextVisibleIndex(index);
        }
    }

    /**
     * Returns whether all columns/rows in the passed interval are hidden.
     *
     * @param {Interval} interval
     *  The index interval to be checked.
     *
     * @returns {boolean}
     *  Whether all columns/rows in the passed interval are hidden.
     */
    isIntervalHidden(interval) {
        return this.getIntervalPosition(interval).size === 0;
    }

    /**
     * Returns the visible column/row intervals contained in the passed
     * interval.
     *
     * @param {IntervalSource} bounds
     *  The column/row intervals to be processed.
     *
     * @returns {IntervalArray}
     *  An array with all visible column/row intervals contained in the passed
     *  interval. If the passed interval is completely hidden, the returned
     *  array will be empty.
     */
    getVisibleIntervals(bounds) {
        return IntervalArray.from(this.intervals(bounds, { visible: true }));
    }

    /**
     * Returns a copy of the passed column/row interval. If the first entry of
     * the passed interval is hidden, the returned interval will start at the
     * first visible entry contained in the interval. Same applies to the last
     * entry of the passed interval. If the entire interval is hidden, `null`
     * will be returned.
     *
     * @param {Interval} interval
     *  The column/row interval to be processed.
     *
     * @returns {Interval|null}
     *  The visible part of the passed interval (but may contain inner hidden
     * columns/rows); or `null`, if the entire interval is hidden.
     */
    shrinkIntervalToVisible(interval) {

        // first visible collection entry
        const first = this.findNextVisibleIndex(interval.first, interval.last);
        // last visible collection entry
        const last = is.number(first) ? this.findPrevVisibleIndex(interval.last, interval.first) : undefined;

        // return the resulting interval
        return is.number(last) ? new Interval(first, last) : null;
    }

    /**
     * Returns a column/row interval that covers the passed interval, and that
     * has been expanded to all hidden columns/rows directly preceding and
     * following the passed interval.
     *
     * @param {Interval} interval
     *  The column/row interval to be expanded.
     *
     * @returns {Interval}
     *  An expanded column/row interval including all leading and trailing
     *  hidden columns/rows.
     */
    expandIntervalToHidden(interval) {

        // nearest visible collection entry preceding the interval
        const prevIndex = (interval.first > 0) ? this.findPrevVisibleIndex(interval.first - 1) : undefined;
        // nearest visible collection entry following the interval
        const nextIndex = (interval.last < this.maxIndex) ? this.findNextVisibleIndex(interval.last + 1) : undefined;

        // return the resulting interval
        const first = is.number(prevIndex) ? (prevIndex + 1) : interval.first;
        const last = is.number(nextIndex) ? (nextIndex - 1) : interval.last;
        return new Interval(first, last);
    }

    /**
     * Returns merged column/row intervals covering the visible parts of the
     * passed intervals. First, the intervals will be expanded to the leading
     * and trailing hidden columns/rows. This may reduce the number of
     * intervals in the result, if there are only hidden columns/rows between
     * the passed intervals.
     *
     * @param {IntervalSource} intervals
     *  An array of index intervals, or a single index interval.
     *
     * @returns {IntervalArray}
     *  The merged intervals, shrunken to leading/trailing visible columns/rows
     *  (but may contain inner hidden columns/rows).
     */
    mergeAndShrinkIntervals(intervals) {

        // expand passed intervals to hidden columns/rows
        intervals = IntervalArray.map(intervals, interval => this.expandIntervalToHidden(interval));

        // merge and sort the intervals (result may be smaller due to expansion to hidden columns/rows)
        intervals = intervals.merge();

        // reduce merged intervals to visible parts (filter out intervals completely hidden)
        return IntervalArray.map(intervals, interval => this.shrinkIntervalToVisible(interval));
    }

    /**
     * Returns the absolute sheet offset of a specific position inside a
     * column/row in 1/100 mm, independent from the current sheet zoom factor.
     *
     * @param {number} index
     *  The zero-based index of the column/row.
     *
     * @param {number} offsetHmm
     *  The relative offset inside the column/row, in 1/100 mm. If this offset
     *  is larger than the total size of the column/row, the resulting absolute
     *  sheet offset will be located at the end position of the column/row.
     *
     * @returns {number}
     *  The absolute position of the specified entry offset, in 1/100 mm.
     */
    getEntryOffsetHmm(index, offsetHmm) {
        const entryDesc = this.getEntry(index);
        return entryDesc.offsetHmm + math.clamp(offsetHmm, 0, entryDesc.sizeHmm);
    }

    /**
     * Returns the absolute sheet offset of a specific position inside a
     * column/row in pixels, according to the current sheet zoom factor.
     *
     * @param {number} index
     *  The zero-based index of the column/row.
     *
     * @param {number} offsetHmm
     *  The relative offset inside the column/row, in 1/100 mm. If this offset
     *  is larger than the total size of the column/row, the resulting absolute
     *  sheet offset will be located at the end position of the column/row.
     *
     * @returns {number}
     *  The absolute position of the specified entry offset, in pixels.
     */
    getEntryOffset(index, offsetHmm) {
        const entryDesc = this.getEntry(index);
        const offsetPx = this.sheetModel.convertHmmToPixel(offsetHmm);
        return entryDesc.offset + math.clamp(offsetPx, 0, entryDesc.size);
    }

    /**
     * Returns information about the column/row covering the passed offset in
     * the sheet in pixels according to the current sheet zoom factor, or in
     * 1/100 of millimeters.
     *
     * @param {number} offset
     *  The absolute offset in the sheet, in pixels, or in 1/100 mm, according
     *  to the "pixel" option (see below).
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.pixel=false]
     *    If set to `true`, the parameter "offset" is interpreted as length in
     *    pixels according to the current sheet zoom factor. Otherwise, the
     *    offset is interpreted in 1/100 mm.
     *  - {boolean} [options.outerHidden=false]
     *    If set to `true`, and the passed offset is outside the sheet area
     *    (less than 0, or greater than the last available offset in the
     *    sheet), returns a descriptor for the very first or very last
     *    column/row of the sheet, regardless if this column/row is hidden or
     *    visible.
     *
     * @returns {EntryOffsetDescriptor}
     *  A descriptor object for the column/row, with the additional properties
     *  "relOffsetHmm" and "relOffset" (relative offset inside the column/row).
     *  If the passed offset is less than 0, returns information about the
     *  first visible column/row in the sheet; if the passed offset is greater
     *  than the total size of the sheet, returns information about the last
     *  visible column/row in the sheet (unless the option "outerHidden" has
     *  been set, see above).
     */
    getEntryByOffset(offset, options) {

        // whether to use pixels instead of 1/100 mm
        const pixel = options?.pixel;
        // the property names for offset and size
        const OFFSET_NAME = pixel ? "offset" : "offsetHmm";
        const SIZE_NAME = pixel ? "size" : "sizeHmm";
        // the total size of the sheet
        const totalSize = this.getTotalSize(options);
        // the passed offset, restricted to the valid sheet dimension
        const currOffset = math.clamp(offset, 0, totalSize - 1);
        // the index of the first entry model after the offset
        const ai = _.sortedIndex(this.#entryModels, { [OFFSET_NAME]: currOffset + 1 }, OFFSET_NAME);
        // the current entry model
        const entryModel = this.#entryModels[ai - 1];
        // the end offset of the previous entry
        const endOffset = !entryModel ? 0 : entryModel.getEndOffset(options);

        // the column/row index, relative to the entry or the gap
        let relIndex = 0;
        // the resulting entry descriptor
        let entryDesc = null;

        // special handling if all entries are hidden: return first entry
        if (totalSize === 0) {
            entryDesc = this.getEntry(0);
            entryDesc.relOffset = entryDesc.relOffsetHmm = 0;
            return entryDesc;
        }

        // offset outside sheet limits: always return first/last entry model if specified
        if (options?.outerHidden) {
            if (offset < 0) {
                entryDesc = this.getEntry(0);
            } else if (offset >= totalSize) {
                entryDesc = this.getEntry(this.maxIndex);
            }
            if (entryDesc && (entryDesc.size === 0)) {
                entryDesc.relOffset = entryDesc.relOffsetHmm = 0;
                return entryDesc;
            }
        }

        // offset points into the previous entry
        if (entryModel && (currOffset < endOffset)) {
            relIndex = Math.floor((currOffset - entryModel[OFFSET_NAME]) / entryModel[SIZE_NAME]);
            entryDesc = new ColRowDescriptor(entryModel, entryModel.interval.first + relIndex);
        } else {
            // offset points into the gap before the entry model at `ai` (entryModel is located before the offset)
            const nextModel = this.#entryModels[ai];
            const gapInterval = new Interval(entryModel ? (entryModel.interval.last + 1) : 0, nextModel ? (nextModel.interval.first - 1) : this.maxIndex);
            relIndex = Math.floor((currOffset - endOffset) / this.defaultModel[SIZE_NAME]);
            entryDesc = new ColRowDescriptor(this.defaultModel, (entryModel ? (entryModel.interval.last + 1) : 0) + relIndex, gapInterval);
            // adjust offsets (default entry is relative to index 0, not to start index of current gap)
            entryDesc.offsetHmm = (entryModel ? entryModel.getEndOffset() : 0) + entryDesc.sizeHmm * relIndex;
            entryDesc.offset = (entryModel ? entryModel.getEndOffset({ pixel: true }) : 0) + entryDesc.size * relIndex;
        }

        // add relative offset properties
        if (pixel) {
            entryDesc.relOffset = math.clamp(offset - entryDesc.offset, 0, entryDesc.size);
            entryDesc.relOffsetHmm = math.clamp(this.sheetModel.convertPixelToHmm(entryDesc.relOffset), 0, entryDesc.sizeHmm);
        } else {
            entryDesc.relOffsetHmm = math.clamp(offset - entryDesc.offsetHmm, 0, entryDesc.sizeHmm);
            entryDesc.relOffset = math.clamp(this.sheetModel.convertHmmToPixel(entryDesc.relOffsetHmm), 0, entryDesc.size);
        }

        return entryDesc;
    }

    /**
     * Converts the passed offset to a scroll anchor position which contains
     * the entry index, and a floating-point ratio describing the exact
     * position inside the entry.
     *
     * @param {number} offset
     *  The absolute offset in the sheet, in pixels, or in 1/100 mm, according
     *  to the "pixel" option (see below).
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.pixel=false]
     *    If set to `true`, the parameter "offset" is interpreted as length in
     *    pixels according to the current sheet zoom factor. Otherwise, the
     *    offset is interpreted in 1/100 mm.
     *  - {boolean} [options.outerHidden=false]
     *    If set to `true`, and the passed offset is outside the sheet area
     *    (less than 0, or greater than the last available offset in the
     *    sheet), returns a scroll anchor for the very first or very last
     *    column/row of the sheet, regardless if this column/row is hidden or
     *    visible.
     *
     * @returns {number}
     *  A scroll anchor position, as floating-point number. The integral part
     *  represents the zero-based column/row index of the collection entry that
     *  contains the passed offset. The fractional part represents the ratio
     *  inside the collection entry.
     */
    getScrollAnchorByOffset(offset, options) {
        const entryDesc = this.getEntryByOffset(offset, options);
        return entryDesc.index + ((entryDesc.sizeHmm > 0) ? (entryDesc.relOffsetHmm / entryDesc.sizeHmm) : 0);
    }

    /**
     * Converts the passed absolute offset in the sheet given in pixels
     * (according to the current sheet zoom factor) to an absolute offset in
     * 1/100 mm (independent from the current sheet zoom factor).
     *
     * @param {number} offset
     *  An absolute offset in the sheet in pixels (according to the current
     *  sheet zoom factor).
     *
     * @returns {number}
     *  The absolute offset in the sheet in 1/100 mm (independent from the
     *  current sheet zoom factor).
     */
    convertOffsetToHmm(offset) {
        const entryDesc = this.getEntryByOffset(offset, { pixel: true });
        return entryDesc.offsetHmm + entryDesc.relOffsetHmm;
    }

    /**
     * Converts the passed absolute offset in the sheet given in 1/100 mm
     * (independent from the current sheet zoom factor) to an absolute offset
     * in pixels (according to the current sheet zoom factor).
     *
     * @param {number} offsetHmm
     *  An absolute offset in the sheet in 1/100 mm (independent from the
     *  current sheet zoom factor).
     *
     * @returns {number}
     *  The absolute offset in the sheet in pixels (according to the current
     *  sheet zoom factor).
     */
    convertOffsetToPixel(offsetHmm) {
        const entryDesc = this.getEntryByOffset(offsetHmm);
        return entryDesc.offset + entryDesc.relOffset;
    }

    /**
     * Calculates the absolute offset of the passed scroll anchor, in 1/100 of
     * millimeters.
     *
     * @param {number} scrollAnchor
     *  A valid scroll anchor value as floating-point number. See return value
     *  of the method `ColRowCollection#getScrollAnchorByOffset` for details.
     *
     * @returns {number}
     *  The absolute offset of the passed scroll anchor, in 1/100 mm.
     */
    convertScrollAnchorToHmm(scrollAnchor) {
        const entryDesc = this.getEntry(Math.floor(scrollAnchor));
        return entryDesc.offsetHmm + Math.round(entryDesc.sizeHmm * (scrollAnchor % 1));
    }

    /**
     * Calculates the absolute offset of the passed scroll anchor, in pixels
     * according to the current sheet zoom factor.
     *
     * @param {number} scrollAnchor
     *  A valid scroll anchor value as floating-point number. See return value
     *  of the method `ColRowCollection#getScrollAnchorByOffset` for details.
     *
     * @returns {number}
     *  The absolute offset of the passed scroll anchor, in pixels.
     */
    convertScrollAnchorToPixel(scrollAnchor) {
        const entryDesc = this.getEntry(Math.floor(scrollAnchor));
        return entryDesc.offset + Math.round(entryDesc.size * (scrollAnchor % 1));
    }

    /**
     * Returns the position and size of the specified column or row, either in
     * 1/100 mm independent from the current sheet zoom factor, or in pixels
     * according to the current sheet zoom factor.
     *
     * @param {number} index
     *  The index of the column or row.
     *
     * @param {SheetPixelOptions} [options]
     *  Optional parameters. The option "pixel" determines whether to return
     *  the position of the column/row in 1/100 mm, or in zoom-dependent
     *  pixels.
     *
     * @returns {IntervalPosition}
     *  The absolute position and size of the specified column or row.
     */
    getEntryPosition(index, options) {
        const entryDesc = this.getEntry(index);
        const pixel = options?.pixel;
        return {
            offset: pixel ? entryDesc.offset : entryDesc.offsetHmm,
            size: pixel ? entryDesc.size : entryDesc.sizeHmm
        };
    }

    /**
     * Returns the position and size of the specified column/row interval,
     * either in 1/100 mm independent from the current sheet zoom factor, or in
     * pixels according to the current sheet zoom factor.
     *
     * @param {Interval} interval
     *  The index interval of the columns/rows.
     *
     * @param {SheetPixelOptions} [options]
     *  Optional parameters. The option "pixel" determines whether to return
     *  the position of the interval in 1/100 mm, or in zoom-dependent pixels.
     *
     * @returns {IntervalPosition}
     *  The absolute position and size of the specified interval.
     */
    getIntervalPosition(interval, options) {

        // descriptor for the first column/row of the interval
        const firstEntryDesc = this.getEntry(interval.first);
        // descriptor for the first column/row following the interval
        const lastEntryDesc = this.getEntry(interval.last + 1);

        const pixel = options?.pixel;
        const offset1 = pixel ? firstEntryDesc.offset : firstEntryDesc.offsetHmm;
        const offset2 = pixel ? lastEntryDesc.offset : lastEntryDesc.offsetHmm;
        return { offset: offset1, size: offset2 - offset1 };
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * contents from the passed collection into this collection.
     *
     * @param {SheetOperationContext} _context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param {ColRowCollection} fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyCopySheetOperation(_context, fromCollection) {
        this.#entryModels = fromCollection.#entryModels.map(entryModel => entryModel.cloneFor(this.sheetModel));
    }

    /**
     * Callback handler for the document operations "insertColumns" and
     * "insertRows". Inserts new columns/rows into this collection, and emits a
     * "move:intervals" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyInsertOperation(context) {

        // shortcuts to document model properties
        const { addressFactory, autoStyles } = this.docModel;
        // the column/row intervals to be inserted
        const intervals = context.getIntervalList("intervals", this.#columns);
        // new formatting attributes for the inserted entries
        const attrSet = context.optDict("attrs");
        // the identifier of a new cell autostyle (may be an empty string for the default style)
        const styleId = context.optStr("s", { empty: true });
        const styleUid = autoStyles.resolveStyleUid(styleId);
        context.ensure(styleUid !== null, 'invalid autostyle identifier "%s"', styleId);
        // the address transformer for column/row index transformations
        const dir = this.#columns ? Direction.RIGHT : Direction.DOWN;
        const transformer = AddressTransformer.fromIntervals(addressFactory, intervals, dir);
        // the callback function for updating the model size properties
        const updateModelSizeFn = this.implCreateUpdateModelSizeFn();
        // maximum column/row index of this collection
        const maxIndex = this.maxIndex;

        // process the prepared target intervals (following intervals are
        // already shifted accordingly, e.g. from "2 4 6" to "2 5 8")
        for (const interval of transformer.targetIntervals) {

            // the array index of the first entry model to be moved
            let ai = this.#getModelArrayIndex(interval.first);
            // the current entry model
            let entryModel = this.#entryModels[ai];
            // the number of columns/rows to be inserted
            const delta = interval.size();

            // split entry model if it covers the first moved column/row
            if (entryModel && (entryModel.interval.first < interval.first)) {
                this.#splitModel(ai, interval.first - entryModel.interval.first);
                ai += 1;
            }

            // update intervals of following entries (length of array may change inside loop!)
            for (let ai2 = ai; ai2 < this.#entryModels.length; ai2 += 1) {

                // update index interval of the entry
                entryModel = this.#entryModels[ai2];
                entryModel.interval.first += delta;
                entryModel.interval.last += delta;

                // delete following entries moved outside the collection limits
                if (entryModel.interval.first > maxIndex) {
                    this.#deleteModels(ai2);
                } else if (entryModel.interval.last >= maxIndex) {
                    this.#deleteModels(ai2 + 1);
                    entryModel.interval.last = maxIndex;
                }
            }

            // create a new entry model, if formatting attributes or an autostyle have been set
            if (attrSet || !autoStyles.isDefaultStyleId(styleUid, true)) {

                // insert a new entry model, try to merge with adjacent entries
                const newModel = this.implCreateModel(interval, attrSet, styleUid, false);
                updateModelSizeFn(newModel);
                this.#insertModel(ai, newModel);
                this.#mergeModel(ai, { prev: true, next: true });
            }
        }

        // update pixel offsets of the entries
        this.#updateModelGeometry(intervals.boundary().first);

        // notify insert listeners (bug 38436: events must be triggered
        // during import, in order to update position of drawing objects)
        this.trigger("move:intervals", this.createSheetEvent({ intervals, insert: true }, context));

        // shift the cell models in the cell collection
        this.sheetModel.cellCollection.implTransformCells(context, transformer);
    }

    /**
     * Callback handler for the document operations "deleteColumns" and
     * "deleteRows". Removes existing columns/rows from this collection, and
     * emits a "move:intervals" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyDeleteOperation(context) {

        // the column/row interval to be deleted
        const intervals = context.getIntervalList("intervals", this.#columns);
        // the address transformer for column/row index transformations
        const dir = this.#columns ? Direction.LEFT : Direction.UP;
        const transformer = AddressTransformer.fromIntervals(this.docModel.addressFactory, intervals, dir);

        // process the prepared target intervals in *reversed* order
        for (const interval of ary.values(transformer.targetIntervals, { reverse: true })) {

            // the array index of the first entry model to be deleted
            let ai = this.#getModelArrayIndex(interval.first);
            // the current entry model
            let entryModel = this.#entryModels[ai];
            // the number of elements to be deleted from the array
            let deleteCount = 0;
            // the number of columns/rows to be removed
            const delta = interval.size();

            // split the part of the entry located before the interval
            if (entryModel && (entryModel.interval.first < interval.first)) {
                entryModel = this.#splitModel(ai, interval.first - entryModel.interval.first);
                ai += 1;
            }

            // remove and move the remaining entries
            for (let ai2 = ai; ai2 < this.#entryModels.length; ai2 += 1) {

                // update number of entries to be deleted, update index interval of moved entries
                entryModel = this.#entryModels[ai2];
                if (entryModel.interval.last <= interval.last) {
                    deleteCount += 1;
                } else {
                    entryModel.interval.first = Math.max(interval.first, entryModel.interval.first - delta);
                    entryModel.interval.last -= delta;
                }
            }

            // delete the array elements
            if (deleteCount > 0) {
                this.#deleteModels(ai, deleteCount);
            }

            // try to merge with previous entry
            this.#mergeModel(ai, { prev: true });
        }

        // update pixel offsets of the entries
        this.#updateModelGeometry(intervals.boundary().first);

        // notify delete listeners (bug 38436: events must be triggered during
        // import, in order to update position of drawing objects)
        this.trigger("move:intervals", this.createSheetEvent({ intervals, insert: false }, context));

        // shift the cell models in the cell collection
        this.sheetModel.cellCollection.implTransformCells(context, transformer);
    }

    /**
     * Callback handler for the document operations "changeColumns" and
     * "changeRows". Changes the formatting attributes of all entries covered
     * by the operation, and emits a "change:intervals" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyChangeOperation(context) {

        // the column/row intervals to be modified (merged and sorted)
        const intervals = context.getIntervalList("intervals", this.#columns).merge();
        // the column/row attributes to be set (or cleared)
        const attrSet = context.optDict("attrs");

        // the identifier of a new cell autostyle (may be an empty string for the default style)
        let styleUid = null;
        if (context.has("s")) {
            const styleId = context.getStr("s", { empty: true });
            styleUid = this.docModel.autoStyles.resolveStyleUid(styleId);
            context.ensure(styleUid, 'invalid autostyle identifier "%s"', styleId);
        }

        // nothing to do without attributes and autostyle
        if (attrSet || (styleUid !== null)) {
            this.implChangeModels(intervals, styleUid, attrSet, context);
        }
    }

    // operation generators ---------------------------------------------------

    /**
     * Checks that the passed index intervals are not oversized in order to
     * successfully generate document operations.
     *
     * @param {IntervalSource} intervals
     *  An array of index interval, or a single index interval.
     *
     * @returns {JPromise<void>|null}
     *  The value `null`, if the passed index intervals are not oversized;
     *  otherwise a promise that has been rejected with an object with property
     *  "cause" set to one of the following error codes:
     *  - "cols:overflow": Too many columns in the index intervals.
     *  - "rows:overflow": Too many rows in the index intervals.
     */
    checkMaxIntervalSize(intervals) {
        const MAX_CHANGE_COUNT = this.#columns ? MAX_CHANGE_COLS_COUNT : MAX_CHANGE_ROWS_COUNT;
        return (intervals.size() > MAX_CHANGE_COUNT) ? this.#makeOverflowRejected() : null;
    }

    /**
     * Merges the passed index intervals arrays. The interval objects may
     * contain additional style properties that need to be merged.
     *
     * @param {IntervalArray} sourceIntervals
     *  An array of index intervals. The intervals MUST be sorted, and they
     *  MUST NOT overlap each other (e.g. an array as returned by the methods
     *  `IntervalArray#merge` or `IntervalArray#partition`). The intervals may
     *  contain a property "style" with an autostyle identifier, and/or a
     *  property "attrs" with explicit formatting attributes (flat map of
     *  key/value pairs).
     *
     * @param {IntervalArray} targetIntervals
     *  An array of index intervals that will be merged over the old index
     *  intervals. The intervals MUST be sorted, and they MUST NOT overlap each
     *  other (see parameter "sourceIntervals").
     *
     * @returns {IntervalArray}
     *  A new array of index intervals (sorted, without overlapping intervals).
     *  The interval objects contain the merged settings for the properties
     *  "style" and "attrs" of the old and new index intervals.
     */
    mergeStyleIntervals(sourceIntervals, targetIntervals) {

        // returns a clone of the passed interval with style properties
        const cloneStyleInterval = interval => {
            const newInterval = interval.clone();
            if ("style" in interval) { newInterval.style = interval.style; }
            if ("attrs" in interval) { newInterval.attrs = interval.attrs; }
            return newInterval;
        };

        // the resulting intervals returned by this method
        const resultIntervals = new IntervalArray();

        // arrays will be modified in-place for simplicity, work on clones of cloned objects
        sourceIntervals = IntervalArray.map(sourceIntervals, cloneStyleInterval);
        targetIntervals = IntervalArray.map(targetIntervals, cloneStyleInterval);

        // merge the source intervals array over the target intervals array
        sourceIntervals.some((sourceInterval, sourceIndex) => {

            // index of last target interval completely preceding the current source interval
            const lastIndex = ary.fastFindLastIndex(targetIntervals, targetInterval => targetInterval.last < sourceInterval.first);

            // move all preceding target intervals into the result
            if (lastIndex >= 0) {
                resultIntervals.append(targetIntervals.splice(0, lastIndex + 1));
            }

            // merge all covering target intervals over the current source interval
            for (;;) {

                // no more intervals available in target array: append all remaining source intervals and exit the loop
                if (targetIntervals.length === 0) {
                    // push current interval separately (may be modified, see below)
                    resultIntervals.append(sourceInterval, sourceIntervals.slice(sourceIndex + 1));
                    return true; // exit the "some()" array loop
                }

                // first remaining interval in the target array
                const targetInterval = targetIntervals[0];
                // temporary interval, inserted into the result array
                let tempInterval = null;

                // entire source interval fits into the gap before the current target interval
                if (sourceInterval.last < targetInterval.first) {
                    resultIntervals.push(sourceInterval);
                    // continue the "some()" array loop with next source interval
                    return;
                }

                // one interval starts before the other interval: split the leading interval
                if (sourceInterval.first < targetInterval.first) {
                    tempInterval = cloneStyleInterval(sourceInterval);
                    tempInterval.last = targetInterval.first - 1;
                    resultIntervals.push(tempInterval);
                    sourceInterval.first = targetInterval.first;
                } else if (targetInterval.first < sourceInterval.first) {
                    tempInterval = cloneStyleInterval(targetInterval);
                    tempInterval.last = sourceInterval.first - 1;
                    resultIntervals.push(tempInterval);
                    targetInterval.first = sourceInterval.first;
                }

                // both intervals start at the same position now: create a new interval with merged style settings
                tempInterval = new Interval(sourceInterval.first, Math.min(sourceInterval.last, targetInterval.last));
                resultIntervals.push(tempInterval);

                // prefer autostyle of target interval over autostyle of source interval
                if ("style" in targetInterval) {
                    tempInterval.style = targetInterval.style;
                } else if ("style" in sourceInterval) {
                    tempInterval.style = sourceInterval.style;
                }

                // copy or merge the formatting attributes
                if (("attrs" in sourceInterval) || ("attrs" in targetInterval)) {
                    tempInterval.attrs = { ...sourceInterval.attrs, ...targetInterval.attrs };
                }

                // source and target intervals have equal size: continue with next source and target interval
                if (sourceInterval.last === targetInterval.last) {
                    targetIntervals.shift();
                    return;
                }

                // source interval ends before target interval: shorten the target interval
                if (sourceInterval.last < targetInterval.last) {
                    targetInterval.first = sourceInterval.last + 1;
                    return;
                }

                // otherwise: shorten source interval, continue with next target interval
                sourceInterval.first = targetInterval.last + 1;
                targetIntervals.shift();
            }
        });

        // append remaining target intervals
        resultIntervals.append(targetIntervals);

        // merge adjacent intervals with equal style settings
        return this.#mergeTaggedIntervals(resultIntervals);
    }

    /**
     * Generates the operations, and the undo operations, to fill the passed
     * intervals in this collection with some individual contents.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalSource} intervals
     *  An array of index interval, or a single index interval. Each interval
     *  object MUST contain an additional property "fillData" with properties
     *  to be applied at all entries in the respective interval. See method
     *  `ColRowCollection#generateFillOperations` for more details about the
     *  expected properties. Overlapping intervals MUST NOT contain the same
     *  properties with different values (e.g.: set autostyle "a1" or autostyle
     *  "a2" to the same entries in overlapping intervals), otherwise the
     *  result will be undefined. However, incomplete property objects of
     *  overlapping intervals will be merged together for the overlapping parts
     *  of the intervals (e.g.: setting the autostyle "a1" to the column
     *  interval A:B, and the column attribute `{visible:true}` to the column
     *  interval B:C will result in setting both the autostyle and the column
     *  attribute to column B).
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will fulfil with the changed index intervals, or that
     *  will be rejected on oversized index intervals (see method
     *  `checkMaxIntervalSize` for details).
     */
    generateIntervalOperations(generator, intervals, options) {

        // bug 34641: restrict maximum number of modified columns/rows
        const checkPromise = this.checkMaxIntervalSize(intervals);
        if (checkPromise) { return checkPromise; }

        // generate the operations for all intervals, merge content objects of overlapping intervals
        const fillDataFn = fillDataArray => Object.assign({}, ...fillDataArray);
        return this._generateChangeIntervalOperations(generator, intervals, fillDataFn, options);
    }

    /**
     * Generates the operations, and the undo operations, to fill the same
     * contents into the specified columns/rows.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalSource} intervals
     *  An array of index interval, or a single index interval.
     *
     * @param {ColRowChangeSet} changeSet
     *  The new properties to be applied at all index intervals.
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will fulfil with the changed index intervals, or that
     *  will reject on oversized index intervals (see method
     *  `checkMaxIntervalSize` for details).
     */
    generateFillOperations(generator, intervals, changeSet, options) {

        // ensure an array, merge the intervals
        intervals = IntervalArray.merge(intervals);

        // bug 34641: restrict maximum number of modified columns/rows
        const checkPromise = this.checkMaxIntervalSize(intervals);
        if (checkPromise) { return checkPromise; }

        // add an arbitrary cache key to the change set that causes internal autostyle caching
        // (all intervals will be formatted with the same new formatting attributes)
        changeSet = cloneWithCacheKey(changeSet);

        // process all intervals with the same contents object
        return this._generateChangeIntervalOperations(generator, intervals, fun.const(changeSet), options);
    }

    /**
     * Generates the interval operations, and the undo operations, to fill the
     * columns/rows in the sheet with outer and/or inner borders.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalSource} intervals
     *  An array of index interval, or a single index interval.
     *
     * @param {Dict} borderAttrs
     *  The border attributes to be applied at all intervals. May contain
     *  regular border attributes, as supported by the document operations
     *  ("borderTop", "borderBottom", "borderLeft", "borderRight") which will
     *  be applied at the outer boundaries of the passed intervals; and the
     *  pseudo attributes "borderInsideHor" or "borderInsideVert", which will
     *  be applied to the inner entries of the intervals.
     *
     * @param {string} cacheKey
     *  The root cache key used to optimize creation of new autostyles,
     *  intended to be reused in different generators (columns, rows, and
     *  cells) while creating the operations for the same border settings.
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will fulfil with the changed index intervals, or that
     *  will be rejected on oversized index intervals (see method
     *  `checkMaxIntervalSize` for details).
     */
    @modelLogger.profileMethod("$badge{ColRowCollection} generateBorderOperations")
    generateBorderOperations(generator, intervals, borderAttrs, cacheKey) {
        modelLogger.log(() => [`intervals=${intervals.toOpStr(this.#columns)} attributes=`, borderAttrs]);

        // single names of border attributes along the columns/rows
        const LEADING_BORDER_KEY = getOuterBorderKey(this.#columns, true);
        const TRAILING_BORDER_KEY = getOuterBorderKey(this.#columns, false);
        const INNER_BORDER_KEY = getInnerBorderKey(this.#columns);

        // border keys, and single names of border attributes in crossing direction
        const CROSS_LEADING_BORDER_KEY = getOuterBorderKey(!this.#columns, true);
        const CROSS_TRAILING_BORDER_KEY = getOuterBorderKey(!this.#columns, false);
        const CROSS_INNER_BORDER_KEY = getInnerBorderKey(!this.#columns);

        // Returns the parts of the passed intervals that will receive a new border attribute.
        // Parameter "leading" specifies the position of the cell border to be modified. The
        // resulting intervals will contain an additional property "fillData" with border settings
        // as expected by the helper function `createBorderChangeSet()`.
        const getChangedBorderIntervals = (intervals, borderAttributes, leading) => {

            // the resulting intervals with additional border information
            const resultIntervals = new IntervalArray();

            // adds the passed settings to the passed intervals, and appends them to the resulting interval array
            const appendIntervals = (newIntervals, fillData) => {
                resultIntervals.append(addProperty(newIntervals, "fillData", fillData));
            };

            // get the border attributes, nothing to do without any border attribute
            const outerKey = leading ? LEADING_BORDER_KEY : TRAILING_BORDER_KEY;
            const outerBorder = borderAttributes[getBorderName(outerKey)];
            const innerBorder = borderAttributes[getBorderName(INNER_BORDER_KEY)];
            const crossBorder = borderAttributes[getBorderName(CROSS_INNER_BORDER_KEY)];
            if (!outerBorder && !innerBorder && !crossBorder) { return null; }

            // the crossing inner border attribute to be set as both crossing outer borders
            if (crossBorder) {
                appendIntervals(intervals.deepClone(), { key: CROSS_LEADING_BORDER_KEY,  border: crossBorder, cacheType: "outer" });
                appendIntervals(intervals.deepClone(), { key: CROSS_TRAILING_BORDER_KEY, border: crossBorder, cacheType: "outer" });
            }

            // add the adjacent intervals whose opposite borders need to be deleted while setting an outer border
            // (if the outer border will not be changed, the borders of the adjacent entries will not be modified neither)
            if (outerBorder) {
                // the adjacent ranges at the specified range border whose existing borders will be deleted
                const adjacentIntervals = this.#getAdjacentIntervals(intervals, leading);
                // opposite borders will be deleted, e.g. the right border left of a column interval (but not if
                // they are equal to the outer border set at the column, to reduce the amount of changed entries)
                const oppositeKey = leading ? TRAILING_BORDER_KEY : LEADING_BORDER_KEY;
                appendIntervals(adjacentIntervals, { key: oppositeKey, keepBorder: outerBorder });
            }

            // shortcut (outer and inner borders are equal): return the entire intervals
            if (outerBorder && innerBorder && equalBorders(outerBorder, innerBorder)) {
                appendIntervals(intervals.deepClone(), { key: outerKey, border: outerBorder, cacheType: "outer" });
                return resultIntervals;
            }

            // outer border will be set to the outer columns/rows only
            if (outerBorder) {
                const indexProp = leading ? "first" : "last";
                const newIntervals = IntervalArray.map(intervals, interval => new Interval(interval[indexProp]));
                appendIntervals(newIntervals, { key: outerKey, border: outerBorder, cacheType: "outer" });
            }

            // inner border will be set to the remaining columns/rows
            if (innerBorder) {
                const offsetL = leading ? 1 : 0;
                const offsetT = leading ? 0 : -0;
                const newIntervals = IntervalArray.map(intervals, interval => interval.single() ? null : new Interval(interval.first + offsetL, interval.last + offsetT));
                appendIntervals(newIntervals, { key: outerKey, border: innerBorder, cacheType: "inner" });
            }

            return resultIntervals;
        };

        // ensure an array, remove duplicates (but do not merge the intervals)
        intervals = IntervalArray.cast(intervals).unify();

        // prepare the interval arrays with the parts of the passed intervals that will get a new single border
        const intervalsL = getChangedBorderIntervals(intervals, borderAttrs, true);
        const intervalsT = getChangedBorderIntervals(intervals, borderAttrs, false);

        // the intervals to be processed, as single array
        const fillIntervals = new IntervalArray(intervalsL, intervalsT);

        // bug 34641: restrict maximum number of modified columns/rows
        const checkPromise = this.checkMaxIntervalSize(fillIntervals.merge());
        if (checkPromise) { return checkPromise; }

        // generate the operations for all intervals, use the border attributes resolved above
        const fillDataFn = fillDataArray => createBorderChangeSet(fillDataArray, cacheKey);
        return this._generateChangeIntervalOperations(generator, fillIntervals, fillDataFn, { skipFiltered: true });
    }

    /**
     * Generates the operations, and the undo operations, to change the
     * formatting of existing border lines of columns/rows in the sheet.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalSource} intervals
     *  An array of index interval, or a single index interval.
     *
     * @param {OpBorder} border
     *  A border attribute value, which may be incomplete. All properties
     *  contained in this object (color, line style, line width) will be set
     *  for all visible borders in the columns/rows. Omitted border properties
     *  will not be changed.
     *
     * @param {string} cacheKey
     *  The root cache key used to optimize creation of new autostyles,
     *  intended to be reused in different generators (columns, rows, and
     *  cells) while creating the operations for the same border settings.
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will fulfil with the changed index intervals, or that
     *  will be rejected on oversized index intervals (see method
     *  `checkMaxIntervalSize` for details).
     */
    @modelLogger.profileMethod("$badge{ColRowCollection} generateVisibleBorderOperations")
    generateVisibleBorderOperations(generator, intervals, border, cacheKey) {
        modelLogger.log(() => [`intervals=${intervals.toOpStr(this.#columns)} border=`, border]);

        // border keys according to collection orientation
        const LEADING_BORDER_KEY = getOuterBorderKey(this.#columns, true);
        const TRAILING_BORDER_KEY = getOuterBorderKey(this.#columns, false);

        // Returns the adjacent intervals of the passed intervals whose border will be changed additionally to the
        // covered intervals. The resulting intervals will contain an additional property "fillData" with border
        // settings as expected by the helper function `createVisibleBorderChangeSet()`.
        const getAdjacentBorderIntervals = (intervals, leading) => {
            const adjacentIntervals = this.#getAdjacentIntervals(intervals, leading).difference(intervals);
            const oppositeKey = leading ? TRAILING_BORDER_KEY : LEADING_BORDER_KEY;
            return addProperty(adjacentIntervals, "fillData", { keys: [oppositeKey] });
        };

        // ensure an array, merge the intervals
        intervals = IntervalArray.merge(intervals);

        // bug 34641: restrict maximum number of modified columns/rows
        const checkPromise = this.checkMaxIntervalSize(intervals);
        if (checkPromise) { return checkPromise; }

        // all entries need to be changed inside the passed intervals
        const fillIntervals = addProperty(intervals.deepClone(), "fillData", { keys: ATTR_BORDER_KEYS });
        // the adjacent entries outside the intervals (the adjoining borders will be changed too)
        fillIntervals.append(getAdjacentBorderIntervals(intervals, true));
        fillIntervals.append(getAdjacentBorderIntervals(intervals, false));

        // generate the operations for all intervals, use the border attributes resolved above
        const fillDataFn = fillDataArray => createVisibleBorderChangeSet(fillDataArray, border, cacheKey);
        return this._generateChangeIntervalOperations(generator, fillIntervals, fillDataFn, { skipFiltered: true });
    }

    /**
     * Generates the operations, and the undo operations, to insert new, or to
     * delete existing columns/rows from this collection.
     *
     * @param {SheetCache} sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param {AddressTransformer} transformer
     *  An address transformer with information about the inserted or deleted
     *  index intervals. Its property "insert" specifies whether to insert new
     *  entries into, or delete entries from this collection.
     */
    generateMoveOperations(sheetCache, transformer) {

        // multiple intervals must be reversed due to buggy implementation of server-side generators (e.g., in the
        // operation "insertColumns B D", column D will not be shifted to E automatically when writing to the file)

        // the name of the document operation to insert new entries into the collection
        const INSERT_OPERATION_NAME = this.#columns ? INSERT_COLUMNS : INSERT_ROWS;
        // the name of the document operation to insert new entries into the collection
        const DELETE_OPERATION_NAME = this.#columns ? DELETE_COLUMNS : DELETE_ROWS;

        // create the operation for all intervals
        const opName = transformer.insert ? INSERT_OPERATION_NAME : DELETE_OPERATION_NAME;
        const operIntervals = transformer.sourceIntervals.clone().reverse();
        sheetCache.generateIntervalsOperation(opName, operIntervals, this.#columns);

        // create the reverse operation for undo
        const undoOpName = transformer.insert ? DELETE_OPERATION_NAME : INSERT_OPERATION_NAME;
        const undoIntervals = transformer.reverseIntervals.clone().reverse();
        sheetCache.generateIntervalsOperation(undoOpName, undoIntervals, this.#columns, undefined, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to automatically fill
     * complete columns or rows.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Interval} interval
     *  The column/row interval containing the source entries to be copied.
     *
     * @param {Direction} direction
     *  The direction in which the specified interval will be expanded. The
     *  values LEFT and UP will cause to auto-fill in front of the passed
     *  interval, regardless of the actual orientation of this collection.
     *
     * @param {number} count
     *  The number of columns/rows to be generated next to the specified source
     *  interval. MUST be small enough to not leave the available index space
     *  of this collection.
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will fulfil with the changed index intervals, or reject
     *  on oversized index intervals (see `checkMaxIntervalSize` for details).
     */
    @modelLogger.profileMethod("$badge{ColRowCollection} generateAutoFillOperations")
    generateAutoFillOperations(generator, interval, direction, count) {
        modelLogger.log(() => `source=${interval.toOpStr(this.#columns)} count=${count}`);

        // bug 34641: restrict maximum number of modified columns/rows
        if (count > MAX_AUTOFILL_COL_ROW_COUNT) {
            return this.#makeOverflowRejected();
        }

        // whether to expand/shrink the leading or trailing border
        const leading = isLeadingDir(direction);

        // special case: if the entire source intervals has equal formatting, simply fill the target interval
        const entryDesc = this.getEntry(interval.first);
        if (entryDesc.uniqueInterval.contains(interval)) {
            const targetInterval = leading ? new Interval(interval.first - count, interval.first - 1) : new Interval(interval.last + 1, interval.last + count);
            return this.generateFillOperations(generator, targetInterval, { attrs: entryDesc.merged, s: entryDesc.style, nativeSize: true }, { skipFiltered: true });
        }

        // an iterator that cycles through all models in the source interval in an endless loop
        const entryIt = fun.yield(function *() {
            for (;;) {
                yield* this.yieldBandModels(interval, { reverse: leading });
            }
        }, this);

        // build the settings to be generated for the target interval
        const targetIntervals = new IntervalArray();
        let index = leading ? (interval.first - 1) : (interval.last + 1);
        let promise = this.repeatSliced(() => {
            if (count === 0) { return BREAK; }

            // fetch the next source interval model
            const bandEntry = itr.shift(entryIt);
            const entryModel = bandEntry.model;

            // create the target interval
            const size = Math.min(count, bandEntry.interval.size());
            const targetInterval = leading ? new Interval(index - size + 1, index) : new Interval(index, index + size - 1);
            targetInterval.fillData = { attrs: entryModel.getMergedEntryAttributes(), s: entryModel.getEffectiveAutoStyleId(), nativeSize: true };
            targetIntervals.push(targetInterval);

            // prepare next iteration cycle
            count -= size;
            index = leading ? (targetInterval.first - 1) : (targetInterval.last + 1);
        });

        // generate the operations for the target interval
        promise = promise.then(() => {
            const fillDataFn = fillDataArray => fillDataArray[0];
            return this._generateChangeIntervalOperations(generator, targetIntervals, fillDataFn, { skipFiltered: true });
        });

        return promise;
    }

    // protected methods ------------------------------------------------------

    /**
     * Helper function that converts column/row size from 1/100mm to operation
     * units.
     */
    /*protected abstract*/ implConvertSizeToUnit(/*size*/) {
        throw new TypeError("ColRowCollection#implConvertSizeToUnit: missing implementation");
    }

    /**
     * Creates a new entry model instance for the specified interval.
     */
    /*protected abstract*/ implCreateModel(/*interval, attrSet, styleUid, listenToParent*/) {
        throw new TypeError("ColRowCollection#implCreateModel: missing implementation");
    }

    /**
     * Creates a callback function that takes an entry model, and updates its
     * size properties. The returned function is bound to the current state of
     * the sheet, and must be used as one-way function (create, use, drop).
     */
    /*protected abstract*/ implCreateUpdateModelSizeFn() {
        throw new TypeError("ColRowCollection#implCreateUpdateModelSizeFn: missing implementation");
    }

    /**
     * Changes the header models of specific column/row intervals. Inserts,
     * deletes, splits, merges, and changes header models as needed.
     *
     * @param {Iterable<Interval>} intervals
     *  The column/row intervals to be updated.
     *
     * @param {string|null} styleUid
     *  The UID of the cell autostyle to be set to the intervals, or `null` to
     *  leave the current autostyles unchanged.
     *
     * @param {PtColRowAttributeSet} attrSet
     *  The column/row attributes to be changed in the intervals, or `null` to
     *  leave the current formatting attributes unchanged.
     *
     * @param {SheetOperationContext} [context]
     *  The operation context with data to be added to the "change:intervals"
     *  event.
     */
    /*protected*/ implChangeModels(intervals, styleUid, attrSet, context) {

        // the callback function for updating the model size properties
        const updateModelSizeFn = this.implCreateUpdateModelSizeFn();
        // event data object passed to the event listeners
        const event = { intervals, attributes: false, size: false, visibility: false, style: false };
        // index of the first column/row with dirty pixel offset due to changed sizes
        let dirtyIndex = null;

        // applies the attributes and autostyle at the passed entry model
        const updateModel = (entryModel, lastIndex) => {

            // update the cell autostyle of the current entry model
            if (styleUid && (entryModel.suid !== styleUid)) {
                entryModel.suid = styleUid;
                event.style = true;
            }

            // rescue old size of the entry model, in 1/100 mm and in pixels
            const oldSizeHmm = entryModel.sizeHmm;
            const oldSizePx = entryModel.size;

            // set the column/row attributes; post-processing only if any attributes have changed
            if (attrSet && entryModel.setAttributes(attrSet)) {

                // update effective size of the entry model
                event.attributes = true;
                updateModelSizeFn(entryModel);

                // collect changed size and visibility
                const changedSize = (oldSizeHmm !== entryModel.sizeHmm) || (oldSizePx !== entryModel.size);
                event.size ||= changedSize;
                event.visibility ||= (oldSizePx === 0) !== (entryModel.size === 0);

                // set index of entries with dirty pixel offsets, if size has been changed
                if (changedSize && (dirtyIndex === null)) {
                    dirtyIndex = entryModel.interval.last + 1;
                }
            }

            // continue with next sub-interval
            return lastIndex + 1;
        };

        // process all intervals in the list
        for (const interval of intervals) {

            // current column/row index for the next invocation
            let index = interval.first;
            // array index of the next collection entry to be visited
            let ai = this.#getModelArrayIndex(index);

            // invokes the callback function for the gap before a collection entry
            const processGap = lastIndex => {

                // the preceding entry model, needed to calculate the offsets
                const prevModel = this.#entryModels[ai - 1];
                // relative start index in the gap
                const relIndex = prevModel ? (index - prevModel.interval.last - 1) : index;
                // start position of the gap, in 1/100mm and in pixels
                const gapOffsetHmm = prevModel ? prevModel.getEndOffset() : 0;
                const gapOffset = prevModel ? prevModel.getEndOffset({ pixel: true }) : 0;

                // initialize the new entry model for the gap
                const entryModel = this.defaultModel.cloneFor(this.sheetModel);
                entryModel.interval = Interval.create(index, lastIndex);
                entryModel.offsetHmm = gapOffsetHmm + entryModel.sizeHmm * relIndex;
                entryModel.offset = gapOffset + entryModel.size * relIndex;

                // insert new entry into array in modifying mode
                this.#insertModel(ai, entryModel);
                ai += 1;

                // set the formatting attributes and autostyle
                index = updateModel(entryModel, lastIndex);

                // remove entry from the collection, if it does not contain any explicit attributes
                if (this.#isDefaultFormatted(entryModel)) {
                    ai -= 1;
                    this.#deleteModels(ai, 1);
                    return;
                }

                // try to merge the new entry with its predecessor
                // (`ai` points behind the new entry, after merging, it has to be decreased)
                if (this.#mergeModel(ai - 1, { prev: true }).prev) {
                    ai -= 1;
                }
            };

            // split the first entry model not covered completely by the interval
            if ((ai < this.#entryModels.length) && (this.#entryModels[ai].interval.first < index)) {
                this.#splitModel(ai, index - this.#entryModels[ai].interval.first);
                ai += 1;
            }

            // process all existing collection entries and the gaps covered by the interval
            for (;;) {

                // the collection entry to be visited, exit the loop if the interval is done
                const entryModel = this.#entryModels[ai];
                if (!entryModel || (interval.last < entryModel.interval.first)) { break; }

                // visit the gap between current index and start of the next entry model
                if (index < entryModel.interval.first) {
                    processGap(entryModel.interval.first - 1);
                }

                // split the last entry model not covered completely by the interval
                const lastIndex = Math.min(interval.last, entryModel.interval.last);
                if (lastIndex < entryModel.interval.last) {
                    this.#splitModel(ai, interval.last - index + 1);
                }

                // set the formatting attributes and autostyle
                index = updateModel(entryModel, lastIndex);

                // Remove entry model from the collection, if it does not contain any explicit attributes
                // anymore. On success, do not change the array index (it already points to the next
                // entry model after deleting the current entry model).
                if (this.#isDefaultFormatted(entryModel)) {
                    this.#deleteModels(ai, 1);
                    continue;
                }

                // Try to merge the entry model with its predecessor. On success, do not change the
                // array index (it already points to the next entry model after merging the entries).
                if (this.#mergeModel(ai, { prev: true }).prev) {
                    continue;
                }

                // go to next array element
                ai += 1;
            }

            // visit the gap after the last existing entry model
            if (index <= interval.last) {
                processGap(interval.last);
            }

            // try to merge last visited entry model with its successor
            // (`ai` already points to the successor of the last visited entry)
            this.#mergeModel(ai, { prev: true });
        }

        // update pixel offsets of following entry models, if the size has been changed
        if (dirtyIndex !== null) {
            this.#updateModelGeometry(dirtyIndex);
        }

        // notify change listeners (bug 38436: events must be triggered during
        // import, in order to update position of drawing objects)
        if (event.attributes || event.style) {
            this.trigger("change:intervals", this.createSheetEvent(event, context));
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the array index of an entry model that contains, follows, or
     * precedes the passed column/row index.
     *
     * @param {number} index
     *  The zero-based column/row index. Must be located in the valid range
     *  covered by this collection.
     *
     * @param {boolean} [reverse=false]
     *  If set to `true`, and the passed index is located in a gap between two
     *  entry models, the array index of the preceding entry model will be
     *  returned, instead of the following entry model.
     *
     * @returns {number}
     *  The array index of the entry model containing the passed index. If the
     *  index is located in a gap between two entries, returns the array index
     *  of the entry model starting after (in reversed mode: before) the passed
     *  index. This may also be an array index pointing after the last existing
     *  entry (in reversed mode: array index before the first model, i.e. -1).
     */
    #getModelArrayIndex(index, reverse) {
        return reverse ?
            ary.fastFindLastIndex(this.#entryModels, entryModel => entryModel.interval.first <= index) :
            ary.fastFindFirstIndex(this.#entryModels, entryModel => index <= entryModel.interval.last);
    }

    /**
     * Updates the default column/row size according to the current formatting
     * attributes of the sheet model.
     *
     * @param {boolean} [notify=false]
     *  If set to `true`, and the default size has been changed, a change event
     *  will be fired.
     */
    #updateDefaultSize(notify) {

        // the default column/row model
        const defModel = this.defaultModel;
        // the old default size, in 1/100 mm, and in pixels
        const oldDefSizeHmm = defModel.sizeHmm;
        const oldDefSizePx = defModel.size;

        // calculate the new default size
        this.implCreateUpdateModelSizeFn()(defModel);

        // do nothing, if the effective size has not changed (check both sizes to catch any rounding errors)
        if (notify && ((defModel.sizeHmm !== oldDefSizeHmm) || (defModel.size !== oldDefSizePx))) {
            this.#updateModelGeometry(0, true);
            const intervals = new IntervalArray(this.getFullInterval());
            const visibility = (oldDefSizePx === 0) !== (defModel.size === 0);
            const event = { intervals, attributes: true, size: true, visibility, style: false };
            this.trigger("change:intervals", this.createSheetEvent(event));
        }
    }

    /**
     * Updates the dirty entry model offsets and sizes up to the end of the
     * collection, after changing the collection, or the sheet attributes.
     *
     * @param {number} startIndex
     *  The zero-based column/row index where updating the offsets starts.
     *
     * @param {boolean} [updateSize=false]
     *  If set to `true`, the size of the entries will be updated too. By
     *  default, only the offset positions of the entries will be updated.
     */
    #updateModelGeometry(startIndex, updateSize) {

        // the default model
        const defModel = this.defaultModel;
        // the callback function for updating the model size properties
        const updateModelSizeFn = this.implCreateUpdateModelSizeFn();
        // the array index of the first dirty entry model
        let ai = this.#getModelArrayIndex(startIndex);
        // the last valid entry model
        let prevModel = this.#entryModels[ai - 1];
        // the current entry model to be updated
        let currModel = null;

        // update start offsets of all dirty collection entries
        for (; ai < this.#entryModels.length; ai += 1) {

            // set offset of current entry to the end offset of the previous entry
            currModel = this.#entryModels[ai];
            currModel.offsetHmm = prevModel ? prevModel.getEndOffset() : 0;
            currModel.offset = prevModel ? prevModel.getEndOffset({ pixel: true }) : 0;

            // add size of the gap between previous and current entry
            const currFirst = currModel.interval.first;
            const prevLast = prevModel ? (prevModel.interval.last + 1) : 0;
            if ((prevLast === 0) || (prevLast < currFirst)) {
                currModel.offsetHmm += defModel.sizeHmm * (currFirst - prevLast);
                currModel.offset += defModel.size * (currFirst - prevLast);
            }

            // update size of the current entry model (first update the merged
            // attributes, parent sheet attributes may have been changed)
            if (updateSize) {
                currModel.refreshMergedAttributeSet();
                updateModelSizeFn(currModel);
            }

            prevModel = currModel;
        }
    }

    /**
     * Returns whether the passed entry model is not formatted differently than
     * the sheet default.
     */
    #isDefaultFormatted(entryModel) {
        return (entryModel.suid === this.defaultModel.suid) && !entryModel.hasExplicitAttributes();
    }

    /**
     * Inserts the passed entry model into the internal array.
     *
     * @param {number} ai
     *  The array index for the new entry model.
     *
     * @param {ModelT} entryModel
     *  The new entry model to be inserted.
     */
    #insertModel(ai, entryModel) {
        ary.insertAt(this.#entryModels, ai, entryModel);
    }

    /**
     * Deletes the specified entry models from the array.
     *
     * @param {number} ai
     *  The array index of the first entry model to be deleted.
     *
     * @param {number} [count]
     *  The number of entry models to be deleted. If omitted, all entry models
     *  up to the end of the array will be deleted.
     */
    #deleteModels(ai, count) {
        // call destructor for all deleted entries
        ary.destroy(this.#entryModels.splice(ai, count));
    }

    /**
     * Splits the entry model at the passed array index, and inserts the new
     * entry model after the existing entry model.
     *
     * @param {number} ai
     *  The array index of the entry model to be split.
     *
     * @param {number} leadingCount
     *  The new interval size of the existing entry model. Must be less than
     *  the current interval size of the entry model.
     *
     * @returns {ModelT}
     *  The new entry model inserted after the existing entry model.
     */
    #splitModel(ai, leadingCount) {

        // the existing collection entry
        const oldModel = this.#entryModels[ai];
        // the clone of the existing entry
        const newModel = oldModel.cloneFor(this.sheetModel);

        // adjust start and end position of the entries
        oldModel.interval.last = oldModel.interval.first + leadingCount - 1;
        newModel.interval.first = oldModel.interval.first + leadingCount;
        newModel.offsetHmm = oldModel.offsetHmm + leadingCount * oldModel.sizeHmm;
        newModel.offset = oldModel.offset + leadingCount * oldModel.size;

        // insert the new entry model into the collection
        this.#insertModel(ai + 1, newModel);
        return newModel;
    }

    /**
     * Tries to merge the entry model at the passed array index with its
     * predecessor and/or successor, if these entries are equal. If successful,
     * the preceding and/or following entry models will be removed from this
     * collection.
     *
     * @param {number} ai
     *  The array index of the entry model to be merged with its predecessor
     *  and/or successor. May be an invalid index (no entry exists in the
     *  collection), or may refer to the first or last entry model (no
     *  predecessor or successor exists in the collection).
     *
     * @param {object} [options]
     *  Optional parameters specifying which entry models will be merged with
     *  the specified entry model:
     *  - {boolean} [options.prev=false]
     *    If set to `true`, the entry and its predecessor will be tried to
     *     merge.
     *  - {boolean} [options.next=false]
     *    If set to `true`, the entry and its successor will be tried to merge.
     *
     * @returns {object}
     *  An object containing the boolean flags "prev" and "next" specifying
     *  whether the entry model has been merged with its predecessor and/or
     *  successor respectively.
     */
    #mergeModel(ai, options) {

        // tries to merge the entry with its predecessor, removes the preceding entry
        const tryMerge = currIndex => {

            // the preceding collection entry
            const prevModel = this.#entryModels[currIndex - 1];
            // the current collection entry
            const thisModel = this.#entryModels[currIndex];

            // check that the entries are equal and do not have a gap
            if (prevModel && thisModel && prevModel.canMergeWith(thisModel)) {
                thisModel.interval.first = prevModel.interval.first;
                thisModel.offsetHmm = prevModel.offsetHmm;
                thisModel.offset = prevModel.offset;
                this.#deleteModels(currIndex - 1, 1);
                return true;
            }
            return false;
        };

        // try to merge the entry with its successor/predecessor
        const next = !!options?.next && tryMerge(ai + 1);
        const prev = !!options?.prev && tryMerge(ai);
        return { prev, next };
    }

    /**
     * Creates and returns the single-index intervals representing the adjacent
     * columns/rows of all passed index intervals.
     *
     * @param {IntervalArray} intervals
     *  The interval array to be processed.
     *
     * @param {boolean} leading
     *  Whether to create an interval array for the leading adjacent index
     *  entries (`true`), or for the trailing index entries (`false`).
     *
     * @returns {IntervalArray}
     *  The intervals representing the adjacent columns/rows of all passed
     *  index intervals.
     */
    #getAdjacentIntervals(intervals, leading) {
        return IntervalArray.map(intervals, leading ?
            (interval => (interval.first > 0) ? new Interval(interval.first - 1) : null) :
            (interval => (interval.last < this.maxIndex) ? new Interval(interval.last + 1) : null)
        );
    }

    /**
     * Returns whether the passed tagged style intervals can be merged to a
     * single tagged style intervals. The intervals must lay exactly side by
     * side, and the otional style properties "s" and "attrs" must be equal (or
     * equally missing).
     *
     * @param {Interval} interval1
     *  The first interval to be compared with the second interval.
     *
     * @param {Interval} interval2
     *  The second interval to be compared with the first interval.
     *
     * @returns {boolean}
     *  Whether the first interval precedes the second interval without a gap,
     *  and the style properties "s" and "attrs" of both intervals are equal.
     */
    #canMergeTaggedIntervals(interval1, interval2) {
        return (interval1.last + 1 === interval2.first) &&
            this.docModel.autoStyles.areEqualStyleIds(interval1.s, interval2.s) &&
            _.isEqual(interval1.attrs, interval2.attrs);
    }

    /**
     * Extends the last tagged style interval in the array, if the passed style
     * interval follows directly, and the style settings (autostyle and
     * formatting attributes) of both intervals are equal. Otherwise, the
     * passed interval will be appended to the array.
     *
     * @param {TaggedInterval[]} intervals
     *  The sorted interval array to be extended with the passed interval. Each
     *  interval object in the array may contain the additional properties "s"
     *  and "attrs". The last array element will be modified in-place if
     *  possible.
     *
     * @param {TaggedInterval} interval
     *  The new interval to be inserted into the interval array. MUST be
     *  located behind the last interval in the array. May contain the
     *  additional properties "s" and "attrs". If neither of the properties
     *  exists, the interval will NOT be inserted into the array.
     *
     * @returns {TaggedInterval[]}
     *  The passed interval array, for convenience.
     */
    #appendTaggedInterval(intervals, interval) {
        if (("s" in interval) || ("attrs" in interval)) {
            const lastInterval = ary.at(intervals, -1);
            if (lastInterval && this.#canMergeTaggedIntervals(lastInterval, interval)) {
                lastInterval.last = interval.last;
            } else {
                intervals.push(interval);
            }
        }
        return intervals;
    }

    /**
     * Merges adjacent tagged style intervals with equal style settings in the
     * passed interval array if possible.
     *
     * @param {TaggedInterval[]} intervals
     *  The sorted interval array whose intervals will be merged in-place if
     *  possible. Each interval object in the array may contain the additional
     *  properties "s" and "attrs".
     *
     * @returns {TaggedInterval[]}
     *  The passed interval array, for convenience.
     */
    #mergeTaggedIntervals(intervals) {
        ary.forEach(intervals, (interval, index) => {
            const nextInterval = intervals[index + 1];
            if (nextInterval && this.#canMergeTaggedIntervals(nextInterval, interval)) {
                interval.last = nextInterval.last;
                ary.deleteAt(intervals, index + 1);
            }
        }, { reverse: true });
        return intervals;
    }

    /**
     * Creates new "changeColumns" or "changeRows" operations (according to the
     * orientation of this collection) to change the passed intervals.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the new operation.
     *
     * @param {TaggedInterval[]} intervals
     *  The tagged style intervals for the document operation, with the
     *  additional optional properties "s" and "attrs".
     *
     * @param {GeneratorOptions} [options]
     *  Optional parameters to be passed to the operations generator.
     */
    #createChangeIntervalsOperation(generator, intervals, options) {

        // create groups of intervals with equal attributes and autostyle
        const intervalGroups = ary.group(intervals, interval => {
            const props = interval.props = _.pick(interval, "s", "attrs");
            return is.empty(props) ? undefined : json.tryStringify(props, { sortKeys: true });
        });

        // create a change operation for every group with equal formatting
        const opName = this.#columns ? CHANGE_COLUMNS : CHANGE_ROWS;
        for (const intervalGroup of intervalGroups.values()) {
            generator.generateIntervalsOperation(opName, intervalGroup, this.#columns, intervalGroup[0].props, options);
        }
    }

    /**
     * Generates the operations, and the undo operations, to change the
     * autostyles and formatting attributes of multiple columns or rows.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalSource} intervals
     *  An array of index intervals, or a single index interval, to generate
     *  operations for. Each interval may contain the following additional
     *  properties:
     *  - {object} [fillData]
     *    Additional formatting settings for the interval. These data objects
     *    will be collected in an array, and will be passed to the callback
     *    function (see below).
     *
     * @param {Function} callback
     *  A callback function that will be invoked for each index interval of the
     *  partition of the passed index intervals. Receives the style data
     *  objects extracted from the original intervals covered by the current
     *  partition interval as simple array in the first parameter. MUST return
     *  the contents object for that interval with formatting attributes and
     *  other settings, as described in the method
     *  `ColRowCollection#generateFillOperations`. Additionally, the following
     *  internal properties are supported:
     *  - {string} [contents.cacheKey]
     *    A unique key for the formatting attributes and autostyle carried in
     *    this contents object. If set, an internal cache will be filled with
     *    the resulting autostyle identifiers, and will be used while
     *    processing the index intervals (performance optimization).
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will fulfil with the index intervals that have really
     *  been changed when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{ColRowCollection} generateChangeIntervalOperations")
    /*private*/ _generateChangeIntervalOperations(generator, intervals, callback, options) {
        modelLogger.log(() => `intervals=${intervals.toOpStr(this.#columns)}`);

        // ensure to work with an array, early exit for empty arrays
        intervals = IntervalArray.cast(intervals);
        if (intervals.empty()) { return this.createResolvedPromise(new IntervalArray()); }

        // the collected intervals with new formatting and/or autostyle
        const newIntervals = new IntervalArray();
        // the collected undo intervals to restore the old formatting and/or autostyle
        const undoIntervals = new IntervalArray();

        // the operation builder needed to generate the autostyle operations
        const styleCache = new SheetStyleCache(this.docModel, { applyImmediately: generator.isImmediateApplyMode() });
        // the embedded operation generator of the builder
        const subGenerator = generator.createSubGenerator();

        // create an iterator that visits the models in a (sorted) partition of the intervals,
        // and adds a "contents" property to the result objects
        const iterator = fun.yield(function *() {

            // do not waste time to collect the fill data of the original intervals, if the callback does not use them
            const collectFillData = callback.length > 0;
            // DOCS-4185: restrict to visible intervals, if the sheet contains a filtered table *anywhere*
            const iterOptions = (options?.skipFiltered && this.sheetModel.isFiltered()) ? { visible: true } : undefined;

            // visit all intervals of a sorted partition of the source intervals
            for (const interval of intervals.partition()) {

                // collect the fill data of all source ranges covered by the current row band and column interval
                // (performance optimization: do not collect fill data, if callback function does not use them)
                const fillDataArray = collectFillData ? interval.coveredBy.map(e => e.fillData).filter(Boolean) : null;

                // create the cell contents entry for the current partition interval
                const contents = callback.call(this, fillDataArray);

                // visit the models in the current interval
                for (const bandEntry of this.yieldBandModels(interval, iterOptions)) {
                    bandEntry.contents = contents;
                    yield bandEntry;
                }
            }
        }, this);

        // whether the effective size of the collection entries will be changed by the operations
        let resizeEntries = false;

        // create the style intervals (intervals with effective attributes and autostyle identifiers)
        let promise = this.iterateSliced(iterator, iterValue => {

            // the current entry model
            const entryModel = iterValue.model;
            // the interval object with new formatting attrtibutes and/or autostyle
            const newInterval = iterValue.interval.clone();
            // the undo interval object to restore the old state
            const undoInterval = iterValue.interval.clone();
            // the contents object to be applied to the current interval
            const contents = iterValue.contents;

            // find/create the autostyle to be applied to the current interval
            const oldStyleId = entryModel.getEffectiveAutoStyleId();
            const newStyleId = styleCache.generateAutoStyleOperations(oldStyleId, contents);

            // add autostyle to the interval objects, if changed
            if (oldStyleId !== newStyleId) {
                newInterval.s = newStyleId;
                undoInterval.s = oldStyleId;
            }

            // get the column/row attributes, add the "customFormat" row attribute, if the formatting changes
            let newAttrs = is.empty(contents.attrs) ? null : { ...contents.attrs };
            if (!this.#columns && ("s" in newInterval)) {
                (newAttrs ??= dict.create()).customFormat = true;
            }

            // convert column/row size in 1/100mm to operation units according to file format
            const SIZE_ATTR_NAME = this.#columns ? "width" : "height";
            if (newAttrs && !contents.nativeSize && is.number(newAttrs[SIZE_ATTR_NAME])) {
                newAttrs[SIZE_ATTR_NAME] = this.implConvertSizeToUnit(newAttrs[SIZE_ATTR_NAME]);
            }

            // create the resulting attribute set to be applied at the current entry model
            const FAMILY_NAME = this.#columns ? "column" : "row";
            const newAttrSet = is.empty(newAttrs) ? null : entryModel.getReducedAttributeSet({ [FAMILY_NAME]: newAttrs });

            // add formatting attributes to the interval objects, if changed
            if (!is.empty(newAttrSet)) {

                // bug 56758: OOXML needs the default column width when showing hidden columns
                const newColAttrs = (this.#columns && this.docApp.isOOXML()) ? newAttrSet.column : null;
                if (newColAttrs && (newColAttrs.visible === true) && !("width" in newColAttrs)) {
                    const oldColAttrs = entryModel.getMergedEntryAttributes();
                    if (oldColAttrs.width === 0) {
                        newColAttrs.width = this.implConvertSizeToUnit(this.defaultModel.sizeHmm);
                        newColAttrs.customWidth = false;
                    }
                }

                newInterval.attrs = newAttrSet;
                undoInterval.attrs = entryModel.getUndoAttributeSet(newAttrSet);

                // detect resized collection entries
                resizeEntries ||= containsResizeAttr(newAttrSet, this.#columns);
            }

            // add the style interval, if it contains changed formatting attributes, or a new autostyle
            this.#appendTaggedInterval(newIntervals, newInterval);
            this.#appendTaggedInterval(undoIntervals, undoInterval);
        });

        // generate the column/row operations, and simultaneously update the column/row collection
        promise = jpromise.fastThen(promise, () => {

            // generate the undo operations synchronously
            this.#createChangeIntervalsOperation(subGenerator, undoIntervals, { undo: true });

            // generate the column/row operations, and simultaneously update the column/row collection
            this.#createChangeIntervalsOperation(subGenerator, newIntervals);

            // update the positions of all drawing objects in the sheet, after the columns/rows have been resized
            if (resizeEntries) {
                return this.sheetModel.generateRefreshAnchorOperations(subGenerator);
            }
        });

        // return the positions of all changed intervals
        return jpromise.fastThen(promise, () => {
            generator.appendOperations(styleCache);
            generator.appendOperations(subGenerator);
            generator.appendOperations(subGenerator, { undo: true });
            generator.appendOperations(styleCache, { undo: true });
            modelLogger.log(() => `changed=${newIntervals.toOpStr(this.#columns)}`);
            return newIntervals;
        });
    }

    #makeOverflowRejected() {
        return makeRejected(this.#columns ? "cols:overflow" : "rows:overflow");
    }
}

// class ColCollection ========================================================

export class ColCollection extends ColRowCollection/*<ColModel>*/ {

    constructor(sheetModel) {
        super(sheetModel, true);
    }

    // protected methods ------------------------------------------------------

    /**
     * Converts a column width from 1/100mm to operation units.
     */
    /*protected override*/ implConvertSizeToUnit(width) {
        return this.docModel.fontMetrics.convertColWidthToUnit(width);
    }

    /**
     * Creates a new column model instance for the specified interval.
     */
    /*protected override*/ implCreateModel(interval, attrSet, styleUid, listenToParent) {
        return new ColModel(this.sheetModel, interval, attrSet, styleUid, listenToParent);
    }

    /**
     * Creates a callback function that takes a `ColModel`, and updates its
     * size properties. The returned function is bound to the current state of
     * the sheet (e.g. base column width etc.), and must be used as one-way
     * function (create, use, drop).
     */
    /*protected override*/ implCreateUpdateModelSizeFn() {

        // the current zoom factor of the sheet
        const effectiveZoom = this.sheetModel.getEffectiveZoom();
        // the padding added to the inner size available for text contents at 100% zoom
        const totalPadding100 = getTotalCellPadding(this.docModel.fontMetrics.getDefaultDigitWidth(1));
        // the padding added to the inner size available for text contents at current zoom
        const totalPaddingZoom = getTotalCellPadding(this.sheetModel.getDefaultDigitWidth());
        // the base column width (in number of digits) to be used for undefined columns
        const baseWidth = this.sheetModel.getMergedAttributeSet(true).sheet.baseColWidth;
        // the width of the digits of the default font
        const digitWidth = this.sheetModel.getDefaultDigitWidth();
        // the default text padding according to zoom and digit width
        const textPadding = getTextPadding(digitWidth);
        // default width in pixels (padding for base column width is a multiple of 8 pixels, depending on standard text padding)
        const defWidthPx = Math.ceil(digitWidth * baseWidth) + 8 * (Math.floor(textPadding / 4) + 1);
        // default width in 1/100mm
        const defWidthHmm = this.sheetModel.convertPixelToHmm(defWidthPx);

        // return the update function for column width
        return colModel => {

            // the merged attributes of the correct family
            const colAttrs = colModel.getMergedEntryAttributes();

            // start with the effective size of a single column, in 1/100 mm
            colModel.rawSizeHmm = Math.max(0, this.docModel.fontMetrics.convertColWidthFromUnit(colAttrs.width));

            // if the column width in the attributes is zero, use the default size provided by the sheet
            if (colModel.rawSizeHmm === 0) {
                colModel.rawSizeHmm = defWidthHmm;
                colModel.rawSize = defWidthPx;
            } else {
                // the inner size available for text contents at 100% zoom
                const innerSize = convertHmmToLength(colModel.rawSizeHmm, "px") - totalPadding100;
                // scale the available inner size according to the current zoom factor, and add current effective padding
                colModel.rawSize = Math.ceil(innerSize * effectiveZoom) + totalPaddingZoom;
            }

            // ensure a specific minimum pixel size to prevent rendering problems (e.g. clipping artifacts)
            colModel.rawSize = Math.max(MIN_CELL_SIZE, colModel.rawSize);

            // whether the entries appear hidden
            const hidden = colModel.hidden = !colAttrs.visible;
            // effective size of an entry
            colModel.sizeHmm = hidden ? 0 : colModel.rawSizeHmm;
            colModel.size = hidden ? 0 : colModel.rawSize;
        };
    }
}

// class RowCollection ========================================================

export class RowCollection extends ColRowCollection/*<RowModel>*/ {

    constructor(sheetModel) {
        super(sheetModel, false);
    }

    // public methods ---------------------------------------------------------

    /**
     * Post-processing of the column/row collection, after all import
     * operations have been applied successfully.
     *
     * _Attention:_ Called from the application import process. MUST NOT be
     * called from external code.
     *
     * @returns {Promise<void>}
     *  A promise that will be resolved when the column/row collection has been
     *  post-processed successfully; or rejected when any error has occurred.
     */
    @modelLogger.profileMethod("$badge{RowCollection} postProcessImport")
    async postProcessImport() {

        // DOCS-5187: GoogleDocs does not write effective row height for dynamic rows into ODS files
        // (do not run this code in unit tests for now, would need to adapt several existing tests)
        if (UNITTEST || !this.docApp.isODF()) { return; }

        // nothing to do without any existing cell models
        const { cellCollection } = this.sheetModel;
        const usedRows = cellCollection.getUsedRange()?.rowInterval();
        if (!usedRows) { return; }

        // the row indexes to be updated, mapped by new row height in 1/100 mm
        const rowIndexes = new Map/*<number, number[]>*/();

        // process all row header models in the range with existing cell models
        const { fontMetrics } = this.docModel;
        await this.asyncForEach(this.yieldBandModels(usedRows), entry => {

            // ignore explicit row height, process dynamic row height only
            if (entry.customHeight) { return; }

            // collect the maximum row height according to formatting attributes of existing cell models
            const rowHeights = new Map/*<number, number>*/();
            const rowRange = this.docModel.addressFactory.getRowRange(entry.interval);
            for (const { address } of cellCollection.cellEntries(rowRange, { type: "value" })) {
                map.update(rowHeights, address.r, height => {
                    const attrSet = cellCollection.getAttributeSet(address);
                    // for performance, do not care about multi-line cells
                    const cellHeight = fontMetrics.getRowHeightHmm(attrSet.character);
                    return Math.max(height ?? 0, cellHeight);
                });
            }

            // insert rows to be updated into `rowIndexes` (leave 5% tolerance)
            const rawHeight = 1.05 * entry.model.rawSizeHmm;
            for (const [row, height] of rowHeights) {
                if (height > rawHeight) {
                    map.upsert(rowIndexes, height, () => []).push(row);
                }
            }
        });

        // update the models of all collected rows
        await this.asyncForEach(rowIndexes, ([height, indexes]) => {
            const intervals = IntervalArray.mergeIndexes(indexes);
            this.implChangeModels(intervals, null, { row: { height } });
            modelLogger.trace(() => `updated outdated height of rows ${intervals.toOpStr(false)} to ${height / 100}mm`);
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Converts a column width from 1/100mm to operation units.
     */
    /*protected override*/ implConvertSizeToUnit(height) {
        return this.docModel.fontMetrics.convertRowHeightToUnit(height);
    }

    /**
     * Creates a new row model instance for the specified interval.
     */
    /*protected override*/ implCreateModel(interval, attrSet, styleUid, listenToParent) {
        return new RowModel(this.sheetModel, interval, attrSet, styleUid, listenToParent);
    }

    /**
     * Creates a callback function that takes a `RowModel`, and updates its
     * size properties. The returned function is bound to the current state of
     * the sheet (e.g. default font), and must be used as one-way function
     * (create, use, drop).
     */
    /*protected override*/ implCreateUpdateModelSizeFn() {

        // the character attributes of the default cell style sheet
        const defCharAttrs = this.docModel.cellStyles.getDefaultStyleAttributeSet().character;
        // default row height from the default character attributes, in pixels for sheet zoom factor
        const defHeightPx = this.sheetModel.getRowHeight(defCharAttrs);
        // default row height in 1/100mm, from the document (independent from zoom)
        const defHeightHmm = this.docModel.fontMetrics.getRowHeightHmm(defCharAttrs);

        // return the update function for row height
        return rowModel => {

            // the merged attributes of the correct family
            const rowAttrs = rowModel.getMergedEntryAttributes();

            // start with the effective size of a single row, in 1/100 mm
            rowModel.rawSizeHmm = Math.max(0, this.docModel.fontMetrics.convertRowHeightFromUnit(rowAttrs.height));

            // if the row height is zero, use the default height provided by the sheet
            if (rowModel.rawSizeHmm === 0) {
                rowModel.rawSizeHmm = defHeightHmm;
                rowModel.rawSize = defHeightPx;
            } else {
                // simple conversion to pixels for row height
                rowModel.rawSize = this.sheetModel.convertHmmToPixel(rowModel.rawSizeHmm);
            }

            // ensure a specific minimum pixel size to prevent rendering problems (e.g. clipping artifacts)
            rowModel.rawSize = Math.max(MIN_CELL_SIZE, rowModel.rawSize);

            // whether the entries appear hidden (filtered rows are hidden too)
            const hidden = rowModel.hidden = !rowAttrs.visible || !!rowAttrs.filtered;
            // effective size of an entry
            rowModel.sizeHmm = hidden ? 0 : rowModel.rawSizeHmm;
            rowModel.size = hidden ? 0 : rowModel.rawSize;
        };
    }
}
