/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";

// types ======================================================================

/**
 * Event object emitted by an instance of `UsedRangeCollection`.
 */
export interface UsedAreaEvent extends SheetEventBase {

    /**
     * The address of the bounding range of the collection; or `undefined`, if
     * the collection is empty.
     */
    range: Opt<Range>;
}

/**
 * Type mapping for the events emitted by `UsedRangeCollection` instances.
 */
export interface UsedRangeCollectionEventMap {

    /**
     * Will be emitted after the bounding range of this instance has changed.
     *
     * @param event
     *  An event object with the address of the bounding range of the
     *  collection.
     */
    "change:usedarea": [event: UsedAreaEvent];
}

// class UsedRangeCollection ==================================================

/**
 * The base class for sheet collections based on cells that want to track a
 * bounding range of their used area.
 */
export abstract class UsedRangeCollection<
    EvtMapT extends UsedRangeCollectionEventMap = UsedRangeCollectionEventMap
> extends SheetChildModel<EvtMapT> {

    /** The address of the cell range currently used by this collection. */
    #usedRange: Opt<Range>;

    // workaround for type errors with extensible event maps
    get #evt(): UsedRangeCollection { return this as UsedRangeCollection; }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this collection is empty.
     *
     * @returns
     *  Whether this collection is empty.
     */
    isUsedRangeEmpty(): boolean {
        return !this.#usedRange;
    }

    /**
     * Returns the address of the bounding range of the used area in this
     * collection.
     *
     * @returns
     *  The address of the bounding range of the used area in this collection;
     *  or `undefined`, if this collection is empty.
     */
    getUsedRange(): Opt<Range> {
        return this.#usedRange?.clone();
    }

    /**
     * Returns the address of the bounding range of the used area in this
     * collection, shrunken to its visible parts. See method
     * `SheetModel.shrinkRangeToVisible()` for details.
     *
     * @returns
     *  The address of the visible part of the bounding range of the used area
     *  in this collection; or `undefined`, if this collection is empty, or its
     *  used area is completely hidden.
     */
    getVisibleUsedRange(): Opt<Range> {
        return this.#usedRange ? this.sheetModel.shrinkRangeToVisible(this.#usedRange) : undefined;
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses have to implement this method and return their current used
     * range according to the collection contents.
     */
    protected abstract calculateUsedRange(): Opt<Range>;

    /**
     * Recalculates and stores the used range of this collection, and emits the
     * event "change:usedarea" if necessary.
     */
    protected updateUsedRange(context?: SheetOperationContext): void {

        // the new bounding range
        const newUsedRange = this.calculateUsedRange();
        // whether the used range does not change
        const equalRanges = (!this.#usedRange && !newUsedRange) || (newUsedRange && this.#usedRange?.equals(newUsedRange));

        // fire change event if the used range has changed, or if this collection was or becomes empty
        if (!equalRanges) {
            this.#usedRange = newUsedRange?.clone();
            this.#evt.trigger("change:usedarea", this.createSheetEvent({ range: newUsedRange }, context));
        }
    }
}
