/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict, pick } from "@/io.ox/office/tk/algorithms";

import type { StyledAttributeSet, PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

import type { SheetPixelOptions } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { type SheetModel, SheetAttrsModel } from "@/io.ox/office/spreadsheet/model/sheetattrsmodel";

// types ======================================================================

/**
 * Formatting attributes for collapsable groups of column or rows in a sheet.
 */
export interface GroupAttributes {

    /**
     * Specifies the grouping level of the column or row.
     */
    level: number;

    /**
     * Specifies whether the affected column/row groups are collapsed. Will be
     * a boolean value in OOXML documents, but an integral bit set in ODF
     * documents!
     */
    collapse: boolean;
}

/**
 * Base shape of column or row attribute sets.
 */
export interface GroupAttributeSet extends StyledAttributeSet {
    group: GroupAttributes;
}

/**
 * Formatting attributes for columns in a sheet.
 */
export interface ColAttributes {

    /**
     * Specifies whether the column is visible in the GUI.
     */
    visible: boolean;

    /**
     * The width of the column, in number of digits of default document font
     * (OOXML), or in 1/100 mm (ODF). The actual default column width will be
     * set dynamically per sheet.
     */
    width: number;

    /**
     * Specifies whether the width of the column has been set or changed in the
     * GUI and is fixed.
     */
    customWidth: boolean;
}

/**
 * Type shape of complete attribute sets for columns in a sheet.
 */
export interface ColAttributeSet extends GroupAttributeSet {
    column: ColAttributes;
}

/**
 * Type shape of partial attribute sets for columns in a sheet.
 */
export type PtColAttributeSet = PtAttrSet<ColAttributeSet>;

/**
 * Formatting attributes for rows in a sheet.
 */
export interface RowAttributes {

    /**
     * Specifies whether the row is visible in the GUI.
     */
    visible: boolean;

    /**
     * The height of the row, in points (OOXML), or in 1/100 mm (ODF). The
     * actual default row height will be set dynamically per sheet.
     */
    height: number;

    /**
     * Specifies whether the height of the row has been set or changed in the
     * GUI and is fixed. If set to false, the row height will be adjusted
     * automatically due to cell contents and font settings.
     */
    customHeight: boolean;

    /**
     * Specifies whether the row contains default formatting attributes for all
     * undefined cells. If set to false, undefined cells will be formatted
     * according to the default column formatting.
     */
    customFormat: boolean;

    /**
     * Specifies whether the row has been hidden implicitly by a filter
     * (auto-filter, or filtered table range). The row will be hidden although
     * the attribute 'visible' is true. Used by ODF only!
     */
    filtered: boolean;
}

/**
 * Type shape of complete attribute sets for rows in a sheet.
 */
export interface RowAttributeSet extends GroupAttributeSet {
    row: RowAttributes;
}

/**
 * Type shape of partial attribute sets for rows in a sheet.
 */
export type PtRowAttributeSet = PtAttrSet<RowAttributeSet>;

/**
 * Extended configuration options for column and row attributes.
 */
export interface ExtColRowAttrConfig {

    /**
     * If set to `true`, the formatting attribute influences the effective size
     * of the column or row when changed.
     */
    resize?: true;
}

export interface ColRowAttributeSet extends GroupAttributeSet {
    column: ColAttributes;
    row: RowAttributes;
}

export type PtColRowAttributeSet = PtAttrSet<ColRowAttributeSet>;

// constants ==================================================================

export const GROUP_ATTRIBUTES: AttributeConfigBag<GroupAttributes> = {
    level:      { def: 0 },
    collapse:   { def: false }
};

export const COL_ATTRIBUTES: AttributeConfigBag<ColAttributes, ExtColRowAttrConfig> = {
    visible:        { def: true,    resize: true },
    width:          { def: 0,       resize: true },
    customWidth:    { def: false }
};

export const ROW_ATTRIBUTES: AttributeConfigBag<RowAttributes, ExtColRowAttrConfig> = {
    visible:        { def: true,    resize: true },
    height:         { def: 0,       resize: true },
    customHeight:   { def: false },
    customFormat:   { def: false },
    filtered:       { def: false,   resize: true }
};

// public functions ===========================================================

/**
 * Returns whether the passed attribute set contains attributes that will
 * change the effective size of a column or row.
 *
 * @param attrSet
 *  The attribute set to be checked.
 *
 * @param columns
 *  Whether to check for column attribute names (`true`), or row attribute
 *  names (`false`).
 *
 * @returns
 *  Whether the passed column/row attribute bag contains at least one
 *  resizing attributes.
 */
export function containsResizeAttr(attrSet: Dict, columns: boolean): boolean {
    const configs = columns ? COL_ATTRIBUTES : ROW_ATTRIBUTES;
    const attrs = attrSet[columns ? "column" : "row"];
    return is.dict(attrs) && !is.empty(attrs) && dict.some(configs, (config, name) => !!config.resize && (name in attrs));
}

// class ColRowModelBase ======================================================

/**
 * Abstract model base class for an interval of equally sized and formatted
 * columns or rows.
 *
 * @template KT
 *  Whether the instances represent column models, or row models.
 */
export abstract class ColRowModel<KT extends "column" | "row"> extends SheetAttrsModel<ColRowAttributeSet> {

    /**
     * The column/row interval represented by this instance.
     */
    readonly interval: Interval;

    /**
     * Absolute position of the first column/row represented by this instance,
     * in 1/100 of millimeters.
     */
    offsetHmm = 0;

    /**
     * The effective size of a single column/row represented by this instance,
     * in 1/100 of millimeters; or zero for hidden columns/rows.
     */
    sizeHmm = 0;

    /**
     * The configured size of a single column/row represented by this instance,
     * in 1/100 of millimeters, independent from the hidden state.
     */
    rawSizeHmm = 0;

    /**
     * Absolute position of the first column/row represented by this instance,
     * in pixels.
     */
    offset = 0;

    /**
     * The effective size of a single column/row represented by this instance,
     * in pixels; or zero for hidden columns/rows.
     */
    size = 0;

    /**
     * The configured size of a single column/row represented by this instance,
     * in pixels, independent from the hidden state.
     */
    rawSize = 0;

    /**
     * Whether the columns/rows represented by this instance are currently
     * hidden.
     */
    hidden = false;

    /**
     * The unique object identifier of the cell auto-style used for all
     * undefined cells.
     */
    suid: string;

    // constructor ------------------------------------------------------------

    protected constructor(sheetModel: SheetModel, family: KT, interval: Interval, attrSet: Opt<PtAttrSet<ColRowAttributeSet>>, styleUid: string, listenToParent?: boolean) {

        super(sheetModel, ["group", family], attrSet, { parentModel: sheetModel, listenToParent, autoClear: true });

        this.interval = interval.clone();
        this.suid = styleUid;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the effective size of one column/row covered by this model,
     * either in 1/100 mm independent from the current sheet zoom factor, or in
     * pixels according to the current sheet zoom factor.
     *
     * @param [options]
     *  Optional parameters. The option `pixel` determines whether to return
     *  the column/row size in 1/100 of millimeters, or in zoom-dependent
     *  pixels.
     */
    getSize(options?: SheetPixelOptions): number {
        return options?.pixel ? this.size : this.sizeHmm;
    }

    /**
     * Returns the offset of the first column/row covered by this model, either
     * in 1/100 mm independent from the current sheet zoom factor, or in pixels
     * according to the current sheet zoom factor.
     *
     * @param [options]
     *  Optional parameters. The option `pixel` determines whether to return
     *  the column/row offset in 1/100 of millimeters, or in zoom-dependent
     *  pixels.
     */
    getStartOffset(options?: SheetPixelOptions): number {
        return options?.pixel ? this.offset : this.offsetHmm;
    }

    /**
     * Returns the offset behind the last column/row covered by this model,
     * either in 1/100 mm independent from the current sheet zoom factor, or in
     * pixels according to the current sheet zoom factor.
     *
     * @param [options]
     *  Optional parameters. The option `pixel` determines whether to return
     *  the column/row offset in 1/100 of millimeters, or in zoom-dependent
     *  pixels.
     */
    getEndOffset(options?: SheetPixelOptions): number {
        return this.getStartOffset(options) + this.interval.size() * this.getSize(options);
    }

    /**
     * Returns the offset of a specific column/row covered by this model,
     * either in 1/100 mm independent from the current sheet zoom factor, or in
     * pixels according to the current sheet zoom factor.
     *
     * @param index
     *  The absolute column/row index (*NOT* a relative index in the interval
     *  covered by this model).
     *
     * @param [options]
     *  Optional parameters. The option `pixel` determines whether to return
     *  the column/row offset in 1/100 of millimeters, or in zoom-dependent
     *  pixels.
     */
    getOffset(index: number, options?: SheetPixelOptions): number {
        return this.getStartOffset(options) + (index - this.interval.first) * this.getSize(options);
    }

    /**
     * Returns the effective merged column/row attributes of this model.
     *
     * @returns
     *  The merged attribute values of the "column" or "row" attribute family.
     */
    abstract getMergedEntryAttributes(): Readonly<ColRowAttributeSet[KT]>;

    /**
     * Returns the explicit column/row attributes of this model.
     *
     * @returns
     *  The explicit attribute values of the "column" or "row" attribute family
     *  (partial map of attributes).
     */
    abstract getExplicitEntryAttributes(): Readonly<Partial<ColRowAttributeSet[KT]>>;

    /**
     * Returns whether this model contains an active auto-style. The auto-style
     * of a column model is always active; the auto-style of a row model is
     * active, if the row attribute "customFormat" is set to `true`.
     *
     * @returns
     *  Whether this model contains an active auto-style.
     */
    abstract hasAutoStyle(): boolean;

    /**
     * Returns the style identifier of the cell auto-style referred by this
     * model. If this model does not contain an active auto-style (rows without
     * "customFormat" attribute), `null` will be returned.
     *
     * @param [useUid=false]
     *  If set to `true`, the unique object identifier of an auto-style (the
     *  property `uid` of the auto-style model) will be returned instead.
     *
     * @returns
     *  The style identifier of the cell auto-style; or `null`, if this model
     *  does not contain an active auto-style.
     */
    getAutoStyleId(useUid?: boolean): string | null {
        return !this.hasAutoStyle() ? null : useUid ? this.suid : this.docModel.autoStyles.resolveStyleId(this.suid);
    }

    /**
     * Returns the style identifier of the effective cell auto-style referred
     * by this model. If this model does not contain an active auto-style (rows
     * without "customFormat" attribute), the identifier of the default
     * auto-style will be returned.
     *
     * @param [useUid=false]
     *  If set to `true`, the unique object identifier of an auto-style (the
     *  property `uid` of the auto-style model) will be returned instead.
     *
     * @returns
     *  The style identifier of the effective cell auto-style.
     */
    getEffectiveAutoStyleId(useUid?: boolean): string {
        const styleId = this.getAutoStyleId(useUid);
        return styleId ?? this.docModel.autoStyles.getDefaultStyleId(useUid);
    }

    /**
     * Returns whether the interval of this model can be merged with the
     * interval of the passed model. The interval of the passed model must
     * start right after the end of the own interval, and must contain the same
     * formatting attributes and cell auto-style identifier.
     */
    canMergeWith(otherModel: this): boolean {
        return (this.interval.last + 1 === otherModel.interval.first) && (this.suid === otherModel.suid) && this.hasEqualAttributeSet(otherModel);
    }

    /**
     * Returns a copy of this model, to be inserted into the passed sheet model
     * (used e.g. by the "copySheet" operation).
     *
     * @returns
     *  A clone of this model, initialized for ownership by the passed sheet
     *  model.
     */
    cloneFor(sheetModel: SheetModel): this {
        const cloneModel = new (this.constructor as CtorType<this, any[]>)(sheetModel, this.interval, this.getExplicitAttributeSet(true), this.suid, false);
        cloneModel.offsetHmm = this.offsetHmm;
        cloneModel.sizeHmm = this.sizeHmm;
        cloneModel.rawSizeHmm = this.rawSizeHmm;
        cloneModel.offset = this.offset;
        cloneModel.size = this.size;
        cloneModel.rawSize = this.rawSize;
        cloneModel.hidden = this.hidden;
        return cloneModel;
    }
}

// class ColModel =============================================================

/**
 * The model of an equally sized and formatted column interval in a sheet.
 */
export class ColModel extends ColRowModel<"column"> {

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, interval: Interval, attrSet: Opt<PtColAttributeSet>, styleUid: string, listenToParent: Opt<boolean>) {
        super(sheetModel, "column", interval, attrSet, styleUid, listenToParent);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the effective merged column attributes of this model.
     *
     * @returns
     *  The merged attribute values of the "column" attribute family.
     */
    getMergedEntryAttributes(): Readonly<ColAttributes> {
        return this.getMergedAttributeSet(true).column;
    }

    /**
     * Returns the explicit column attributes of this model.
     *
     * @returns
     *  The explicit attribute values of the "column" attribute family.
     */
    getExplicitEntryAttributes(): Readonly<Partial<ColAttributes>> {
        return pick.dict(this.getExplicitAttributeSet(true), "column", true);
    }

    /**
     * Returns whether this model contains an active auto-style (the auto-style
     * of a column model is always active).
     *
     * @returns
     *  Whether this model contains an active auto-style.
     */
    hasAutoStyle(): true {
        return true;
    }
}

// class RowModel =============================================================

/**
 * The model of an equally sized and formatted row interval in a sheet.
 */
export class RowModel extends ColRowModel<"row"> {

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, interval: Interval, attrSet: Opt<PtRowAttributeSet>, styleUid: string, listenToParent?: boolean) {
        super(sheetModel, "row", interval, attrSet, styleUid, listenToParent);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the effective merged column/row attributes of this model.
     *
     * @returns
     *  The merged attribute values of the "column" attribute family, if this
     *  model represents columns, otherwise the merged attribute values of the
     *  "row" attribute family.
     */
    getMergedEntryAttributes(): Readonly<RowAttributes> {
        return this.getMergedAttributeSet(true).row;
    }

    /**
     * Returns the explicit column/row attributes of this model.
     *
     * @returns
     *  The explicit attribute values of the "column" attribute family, if this
     *  model represents columns, otherwise the explicit attribute values of
     *  the "row" attribute family.
     */
    getExplicitEntryAttributes(): Readonly<Partial<RowAttributes>> {
        return pick.dict(this.getExplicitAttributeSet(true), "row", true);
    }

    /**
     * Returns whether this model contains an active auto-style. The auto-style
     * of a row model is active, if the row attribute "customFormat" is set to
     * `true`.
     *
     * @returns
     *  Whether this model contains an active auto-style.
     */
    hasAutoStyle(): boolean {
        return this.getMergedEntryAttributes().customFormat;
    }
}

// class ColRowDescriptor =====================================================

/**
 * A simple descriptor object for a column or row, returned by the public
 * methods of the class `ColRowCollection`.
 *
 * @deprecated
 *  Bad performance. Use `ColModel` or `RowModel` directly instead.
 */
export class ColRowDescriptor<KT extends "column" | "row" = "column" | "row"> {

    // properties -------------------------------------------------------------

    /**
     * The zero-based index of the column or row.
     */
    readonly index: number;

    /**
     * The column/row interval containing this entry with the same formatting
     * attributes and auto-style.
     */
    readonly uniqueInterval: Interval;

    /**
     * The absolute position of the column/row, in 1/100 mm.
     */
    readonly offsetHmm: number;

    /**
     * The effective size (zero for hidden columns/rows), in 1/100 mm.
     */
    readonly sizeHmm: number;

    /**
     * The absolute position of the column/row, in pixels.
     */
    readonly offset: number;

    /**
     * The effective size (zero for hidden columns/rows), in pixels.
     */
    readonly size: number;

    /**
     * The unique object identifier of the auto-style containing the character
     * and cell formatting attributes for all undefined cells in the column or
     * row.
     */
    readonly suid: string;

    /**
     * The style identifier of the auto-style containing the character and cell
     * formatting attributes for all undefined cells in the column/row.
     */
    readonly style: string;

    /**
     * The merged attribute set of the cell auto-style. MUST NOT be changed!
     */
    readonly attributes: Readonly<Dict>;

    /**
     * The parsed number format code extracted from the cell auto-style.
     */
    readonly format: ParsedFormat;

    /**
     * The effective merged column/row attributes, as simple key/value map (NOT
     * mapped by the "column" or "row" family name). MUST NOT be changed!
     */
    readonly merged: Readonly<Partial<ColRowAttributeSet[KT]>>;

    /**
     * The explicit column/row attributes, as simple key/value map (NOT mapped
     * by the "column" or "row" family name). MUST NOT be changed!
     */
    readonly explicit: Readonly<Partial<ColRowAttributeSet[KT]>>;

    // constructor ------------------------------------------------------------

    constructor(entryModel: ColRowModel<KT>, index: number, uniqueInterval?: Interval) {

        // column/row index, and unique formatting interval
        this.index = index;
        this.uniqueInterval = uniqueInterval ?? entryModel.interval.clone();

        // position and size
        const relIndex = index - entryModel.interval.first;
        this.offsetHmm = entryModel.offsetHmm + entryModel.sizeHmm * relIndex;
        this.sizeHmm = entryModel.sizeHmm;
        this.offset = entryModel.offset + entryModel.size * relIndex;
        this.size = entryModel.size;

        // the cell auto-style, and the merged attributes
        this.suid = entryModel.getEffectiveAutoStyleId(true);
        this.style = entryModel.getEffectiveAutoStyleId();
        const { autoStyles } = entryModel.docModel;
        this.attributes = autoStyles.getMergedAttributeSet(this.style);
        this.format = autoStyles.getParsedFormat(this.style);

        // the merged and explicit column/row attributes, as simple object
        this.merged = entryModel.getMergedEntryAttributes();
        this.explicit = entryModel.getExplicitEntryAttributes();
    }
}
