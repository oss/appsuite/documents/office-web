/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import { CellAttributeSet, PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { MergeMode, DisplayText, Direction, SortRule } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeSource, SplitRangesOptions } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { CellType, CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { UsedRangeCollectionEventMap, UsedRangeCollection } from "@/io.ox/office/spreadsheet/model/usedrangecollection";
import { SheetTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import { SharedFormulaStorage } from "@/io.ox/office/spreadsheet/model/formula/sharedformulastorage";
import { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

export interface DefaultStyleIdOptions {
    useUid?: boolean;
    ignoreRowStyle?: boolean;
}

/**
 * A set of flags specifying what has changed in the cell.
 */
export interface CellChangeFlags {

    /**
     * The address of the changed cell.
     */
    address: Address;

    /**
     * The current cell model; or `null`, if the cell model has been deleted.
     */
    model: CellModel | null;

    /**
     * If set to `true`, the value (or formula result) of the cell has changed.
     */
    value: boolean;

    /**
     * If set to true, the formula expression of the cell has changed.
     */
    formula: boolean;

    /**
     * If set to true, the auto-style of the cell has changed.
     */
    style: boolean;
}

/**
 * Optional settings for creating a cell style iterator. Supports all options
 * that are supported by the method `RangeArray#split` to define a custom start
 * position inside the cell ranges.
 */
export interface CellStyleIteratorOptions extends SplitRangesOptions {

    /**
     * Specifies how to handle hidden columns and rows:
     * - `true`: Only visible cells (cells in visible columns, AND in visible
     *   rows) will be covered by the iterator.
     * - "columns": Cells in hidden columns will be skipped, but cells in
     *   visible columns and hidden rows will be visited.
     * - "rows": Cells in hidden rows will be skipped, but cells in visible
     *   rows and hidden columns will be visited.
     * - `false` (default): By default, all visible and hidden cells in the
     *   passed cell ranges will be covered.
     */
    visible?: boolean | "columns" | "rows";

    /**
     * If set to `true`, covered cells in merged ranges will not be visited
     * (this may become rather expensive). Default value is `false` (all cells
     * covered and hidden by a merge range will be visited).
     */
    skipCovered?: boolean;
}

/**
 * Optional settings for creating a cell iterator. Supports all options that
 * are supported by the method `RangeArray#split` to define a custom start
 * position inside the cell ranges.
 */
export interface CellIteratorOptions extends CellStyleIteratorOptions {

    /**
     * Specifies which type of cells will be covered by the iterator. Default
     * value is `ALL`.
     */
    type?: CellType;
}

/**
 * Common properties of the values of all cell iterators.
 */
export interface BaseCellEntry {

    /**
     * The address of the cell currently visited.
     */
    address: Address;

    /**
     * The model of an existing cell, or `null` for an undefined cell.
     */
    model: CellModel | null;

    /**
     * The value of the current cell (`null` for all blank cells).
     */
    value: ScalarType;

    /**
     * Whether the current cell is blank (regardless if defined).
     */
    blank: boolean;
}

/**
 * The result objects of a cell iterator as returned by the method
 *  `CellCollection::yieldCellEntries`.
 */
export interface CellEntry extends BaseCellEntry {

    /**
     * The address of the original cell range (from the passed range array)
     * containing the current cell address.
     */
    orig: Range;

    /**
     * The array index of the original cell range contained in the property
     * `orig`.
     */
    index: number;
}

export interface LinearCellEntry extends BaseCellEntry {
    direction: Direction;
}

export interface CellStyleEntry {
    range: Range;
    style: string | null;
    origRange: Range;
    origIndex: number;
}

export interface TokenArrayDescriptor {
    type: "normal" | "shared" | "matrix";
    tokenArray: SheetTokenArray;
    refAddress: Address;
    sharedIndex: number | null;
    matrixRange: Range | null;
    dynamicMatrix: boolean;
    formula: string | null;
}

export interface RangeContentOptions {
    blanks?: boolean;
    visible?: boolean;
    attributes?: boolean;
    display?: boolean;
    compressed?: boolean;
    maxCount?: number;
}

export interface CellContents {
    value: ScalarType;
    style?: string;
    attributes?: Dict;
    format?: ParsedFormat;
    display?: string;
    count?: number;
}

export interface CellWithAttrsEntry {
    address: Address;
    range: Range;
    index: number;
}

export interface CellContentsArray extends Array<CellContents> {
    count: number;
}

/**
 * Contains the addresses of all changed cells that will be collected during
 * applying "changeCells" document operations. Will be emitted by the cell
 * collection in "change:cells" events.
 */
export interface ChangeCellsEvent extends SheetEventBase {

    /**
     * The addresses of all cells with changed value (including cells that just
     * became blank). The cell addresses will be in no specific order.
     */
    valueCells: AddressArray;

    /**
     * The addresses of all cells with changed formula expression (including
     * cells where the formula has been deleted). The cell addresses will be in
     * no specific order.
     */
    formulaCells: AddressArray;

    /**
     * The addresses of all cells with changed auto-style. The cell addresses
     * will be in no specific order.
     */
    styleCells: AddressArray;

    /**
     * The addresses of all changed cells (value, formula expression, or
     * auto-style. The cell addresses will be in no specific order.
     */
    allCells: AddressArray;

    /**
     * Whether this change event has been caused by the dependency manager
     * while updating the results of formula cells.
     */
    results: boolean;
}

/**
 * Contains the address transformer representing a "moveCells" document
 * operation. Will be emitted by the cell collection in "move:cells" events.
 */
export interface MoveCellsEvent extends SheetEventBase {

    /**
     * The address transformer representing a "moveCells" document operation.
     */
    transformer: AddressTransformer;
}

/**
 * Type mapping for the events emitted by `CellCollection` instances.
 */
export interface CellCollectionEventMap extends UsedRangeCollectionEventMap {

    /**
     * Will be emitted after the value, formula expression, or autostyle of one
     * or more cells in the cell collection have been changed due to a
     * "changeCells" document operation.
     *
     * @param event
     *  An event object with the addresses of all changed cells.
     */
    "change:cells": [event: ChangeCellsEvent];

    /**
     * Will be emitted after some cells in the cell collection have been moved
     * to a new position due to a "moveCells", "insertColumns", "deleteColumns",
     * "insertRows", or "deleteRows" document operation.
     *
     * @param event
     *  An event object with the address transformer.
     */
    "move:cells": [event: MoveCellsEvent];
}

// class CellCollection =======================================================

export class CellCollection extends UsedRangeCollection<CellCollectionEventMap> {

    readonly sharedFormulas: SharedFormulaStorage;

    constructor(sheetModel: SheetModel);

    getCellModel(address: Address): CellModel | null;
    isBlankCell(address: Address): boolean;
    getValue(address: Address): ScalarType;
    getDisplayString(address: Address): DisplayText;
    getTokenArray(address: Address, options?: object): TokenArrayDescriptor | null;
    getStyleId(address: Address, useUid?: boolean): string;
    getDefaultStyleId(address: Address, options?: DefaultStyleIdOptions): string;
    getAttributeSet(address: Address): CellAttributeSet;
    getParsedFormat(address: Address): ParsedFormat;
    isTextFormat(address: Address): boolean;
    setFormulaURL(address: Address, url: string | null): void;
    getEffectiveURL(address: Address): string | null;

    cellEntries(ranges: RangeSource, options?: CellIteratorOptions): IterableIterator<CellEntry>;
    linearCellEntries(address: Address, directions: Direction | Direction[], options?: object): IterableIterator<LinearCellEntry>;
    parallelAddresses(ranges: Range[], options?: CellIteratorOptions): IterableIterator<Address[]>;

    findFirstCell(range: Range, options?: CellIteratorOptions): Address | null;
    findFirstCellLinear(address: Address, directions: Direction | Direction[], options?: object): Address | null;

    findContentRange(range: Range, options?: object): Range;

    getRangeContents(ranges: RangeSource, options?: RangeContentOptions): CellContentsArray;

    findCellWithAttributes(ranges: RangeSource, attrSet: PtCellAttributeSet, options?: CellStyleIteratorOptions): CellWithAttrsEntry | null;

    postProcessImport(): Promise<void>;
    applyCopySheetOperation(context: SheetOperationContext, fromCollection: CellCollection): void;
    applyChangeCellsOperation(context: SheetOperationContext): void;
    applyMoveCellsOperation(context: SheetOperationContext): void;

    checkMaxRangeSize(ranges: RangeSource): JPromise | null;
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise;
    generateMergeCellsOperations(generator: SheetOperationGenerator, rangeSource: RangeSource, mergeMode: MergeMode): JPromise;
    generateSortOperations(generator: SheetOperationGenerator, sortRange: Range, sortVert: boolean, sortRules: SortRule[]): JPromise;

    protected calculateUsedRange(): Opt<Range>;
}
