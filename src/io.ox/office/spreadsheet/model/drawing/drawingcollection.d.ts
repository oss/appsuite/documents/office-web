/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Position } from "@/io.ox/office/editframework/utils/operations";
import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { DrawingCollectionEventMap, DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";
import { IndexedCollection } from "@/io.ox/office/drawinglayer/model/indexedcollection";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { XSheetDrawingModel, SheetDrawingModelType } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { XSheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingcollection";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export type SheetDrawingCollectionType = DrawingCollection<SpreadsheetModel, XSheetDrawingModel>;

/**
 * Type mapping for the events emitted by `SheetDrawingCollection` instances.
 */
export type SheetDrawingCollectionEventMap = DrawingCollectionEventMap<SpreadsheetModel, XSheetDrawingModel>;

// class SheetDrawingCollection ===============================================

export interface SheetDrawingCollection<
    EvtMapT extends SheetDrawingCollectionEventMap = SheetDrawingCollectionEventMap
> extends IndexedCollection<SpreadsheetModel, XSheetDrawingModel, EvtMapT>, XSheetDrawingCollection { }

export class SheetDrawingCollection<
    EvtMapT extends SheetDrawingCollectionEventMap = SheetDrawingCollectionEventMap
> extends IndexedCollection<SpreadsheetModel, XSheetDrawingModel, EvtMapT> implements XSheetDrawingCollection {

    public constructor(sheetModel: SheetModel);

    getContainerNode(): HTMLDivElement;
    clientCreateModel(parentCollection: SheetDrawingCollection, drawingType: DrawingType, attrSet?: Dict): Opt<SheetDrawingModelType>;

    applyCopySheetOperation(context: SheetOperationContext, fromCollection: SheetDrawingCollection): void;
    applyInsertDrawingOperation(context: SheetOperationContext, position: Position): void;
    applyDeleteDrawingOperation(context: SheetOperationContext, position: Position): void;
    applyMoveDrawingOperation(context: SheetOperationContext, position: Position): void;

    canRestoreDeletedRanges(ranges: RangeArray): boolean;
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise;
    generateRefreshAnchorOperations(generator: SheetOperationGenerator): JPromise;
}
