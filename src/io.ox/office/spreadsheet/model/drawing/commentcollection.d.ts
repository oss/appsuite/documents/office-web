/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AbstractCommentCollection } from "@/io.ox/office/editframework/view/editview";
import { YieldIndexedDrawingModelsOptions } from "@/io.ox/office/drawinglayer/model/indexedcollection";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { RangeSource } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { CommentModel } from "@/io.ox/office/spreadsheet/model/drawing/commentmodel";
import { SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";

// types ======================================================================

export interface CommentModelIteratorOptions extends YieldIndexedDrawingModelsOptions {
    ranges?: RangeSource;
    vertical?: boolean;
    visibleCells?: boolean;
}

/**
 * Type mapping for the events emitted by `CommentCollection` instances.
 */
export interface CommentCollectionEventMap {
    "move:comments": [commentModels: CommentModel[]];
}

// class CommentCollection ====================================================

export class CommentCollection extends SheetChildModel<CommentCollectionEventMap> implements AbstractCommentCollection {

    hasUnsavedComment(): boolean;
    deleteUnsavedComment(): void;

    getByAddress(address: Address): Opt<CommentModel[]>;

    yieldCommentModels(options?: CommentModelIteratorOptions): IterableIterator<CommentModel>;

    applyCopySheetOperation(context: SheetOperationContext, fromCollection: CommentCollection): void;
    applyInsertCommentOperation(context: SheetOperationContext): void;
    applyDeleteCommentOperation(context: SheetOperationContext): void;
    applyChangeCommentOperation(context: SheetOperationContext): void;
    applyMoveCommentsOperation(context: SheetOperationContext): void;

    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise;
    generateRefreshAnchorOperations(generator: SheetOperationGenerator): JPromise;
    generateMergeCellsOperations(generator: SheetOperationGenerator, rangeSource: RangeSource, mergeMode: MergeMode): JPromise;
}
