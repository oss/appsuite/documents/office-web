/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { ary } from '@/io.ox/office/tk/algorithms';

import { DrawingType, DrawingOrderType, optimizeDrawingPositions } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { IndexedCollection } from '@/io.ox/office/drawinglayer/model/indexedcollection';

import { DELETE_DRAWING, INSERT_DRAWING } from '@/io.ox/office/spreadsheet/utils/operations';
import { XSheetDrawingCollection } from '@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingcollection';
import { SheetDrawingModel } from '@/io.ox/office/spreadsheet/model/drawing/drawingmodel';
import { SheetShapeModel } from '@/io.ox/office/spreadsheet/model/drawing/shapemodel';
import { SheetConnectorModel } from '@/io.ox/office/spreadsheet/model/drawing/connectormodel';
import { SheetImageModel } from '@/io.ox/office/spreadsheet/model/drawing/imagemodel';
import { SheetChartModel } from '@/io.ox/office/spreadsheet/model/drawing/chart/chartmodel';
import { SheetGroupModel } from '@/io.ox/office/spreadsheet/model/drawing/groupmodel';
import { SheetUndefinedModel } from '@/io.ox/office/spreadsheet/model/drawing/undefinedmodel';
import { copyTextFrame } from '@/io.ox/office/spreadsheet/model/drawing/text/textframeutils';

// class SheetDrawingCollection ===============================================

/**
 * Represents the drawing collection of a single sheet in a spreadsheet
 * document.
 *
 * @param {SheetModel} sheetModel
 *  The sheet model instance containing this collection.
 */
export class SheetDrawingCollection extends XSheetDrawingCollection.mixin(IndexedCollection) {

    constructor(sheetModel) {
        super(sheetModel, sheetModel.docModel);
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations, and the undo operations, to insert new drawing
     * objects into this collection.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Position} position
     *  The position of the first new drawing object. All drawing objects will
     *  be inserted subsequently.
     *
     * @param {object[]} drawingDescs
     *  An array of descriptors with the types and attribute sets for the
     *  drawing objects to be inserted. Each array element MUST be an object
     *  with the following properties:
     *  - {string} drawingDesc.type -- The type identifier of the drawing
     *    object.
     *  - {Dict} drawingDesc.attrs -- Initial attribute set for the drawing
     *    object. MUST contain the complete (!) anchor attributes specifying
     *    the physical position of the drawing object in the sheet.
     *
     * @param {Function} [callback]
     *  A callback function that will be invoked every time after a new
     *  "insertDrawing" operation has been generated for a drawing object.
     *  Allows to create additional operations for the drawing objects.
     *  Receives the following parameters:
     *  1. {SheetOperationGenerator} generator -- The operations generator
     *     (already containing the initial "insertDrawing" operation of the
     *     current drawing).
     *  2. {number} sheet -- The zero-based index of this sheet.
     *  3. {Position} position -- The effective document position of the
     *     current drawing object in the sheet, as inserted into the
     *     "insertDrawing" operation.
     *  4. {object} data -- The data element from the array parameter
     *     `drawingDescs` that is currently processed.
     *
     *  The callback function may return a promise to defer processing of
     *  subsequent drawing objects.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the operations have been generated
     *  successfully, or that will reject on any error.
     */
    generateInsertDrawingOperations(generator, position, drawingDescs, callback) {

        // the index of the sheet containing this collection
        var sheet = this.sheetModel.getIndex();

        // generate all operations for the new drawing objects, and the undo operations
        return this.asyncForEach(drawingDescs, drawingDesc => {

            // generate the undo operations that delete the new drawings (in reversed order)
            generator.generateDrawingOperation(DELETE_DRAWING, position, null, { undo: true, prepend: true });

            // create the 'insertDrawing' operation at the current position
            generator.generateDrawingOperation(INSERT_DRAWING, position, { type: drawingDesc.type, attrs: drawingDesc.attrs });
            // invoke callback function for more operations (may return a promise)
            var result = callback ? callback.call(this, generator, sheet, position.slice(), drawingDesc) : null;

            // adjust document position for the next drawing object
            position = position.slice();
            position[position.length - 1] += 1;

            // callback may want to defer processing the following drawings
            return result;
        });
    }

    /**
     * Generates the operations, and the undo operations, to delete the
     * specified drawing objects from this collection.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Position[]} positions
     *  An array with positions of the drawing objects to be deleted.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the operations have been generated
     *  successfully, or that will reject on any error.
     */
    generateDeleteDrawingOperations(generator, positions) {

        // sort by position, remove positions of embedded drawings whose parents will be deleted too
        positions = optimizeDrawingPositions(positions);

        // create the delete operations (in reversed order!), and the according undo operations
        const iterator = ary.values(positions, { reverse: true });
        return this.asyncForEach(iterator, position => {

            // the drawing model to be deleted
            var drawingModel = this.getModel(position);
            // a local generator to be able to prepend all undo operations at once
            var generator2 = this.sheetModel.createOperationGenerator();

            // generate the operations, and insert them into the passed generator
            drawingModel.generateDeleteOperations(generator2);
            generator.appendOperations(generator2);
            generator.prependOperations(generator2, { undo: true });
        });
    }

    /**
     * Generates the operations, and the undo operations, to change the
     * formatting attributes of the specified drawing objects individually.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {object[]} drawingDescs
     *  An array of descriptors with the positions and attribute sets for the
     *  drawing objects to be changed. Each descriptor MUST contain the
     *  following properties:
     *  - {Position} drawingDesc.position -- The document position of the
     *    drawing object to be changed.
     *  - {Dict} drawingDesc.attrs -- The (incomplete) attribute set to be
     *    applied at the drawing object.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the operations have been generated
     *  successfully.
     */
    generateChangeDrawingOperations(generator, drawingDescs) {
        return this.asyncForEach(drawingDescs, drawingDesc => {
            var drawingModel = this.getModel(drawingDesc.position);
            drawingModel.generateChangeOperations(generator, drawingDesc.attrs);
        });
    }

    /**
     * Generates the operations, and the undo operations, to apply the same
     * formatting attributes to multiple drawing objects.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Position[]} positions
     *  An array with positions of the drawing objects to be modified.
     *
     * @param {Dict} attrSet
     *  An (incomplete) attribute set to be applied at all drawing objects.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the operations have been generated
     *  successfully.
     */
    generateFormatDrawingOperations(generator, positions, attrSet) {
        return this.asyncForEach(positions, position => {
            var drawingModel = this.getModel(position);
            drawingModel.generateChangeOperations(generator, attrSet);
        });
    }

    /**
     * Generates the operations, and the undo operations, to change the Z
     * order of multiple drawing objects.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Position[]} positions
     *  An array with positions of the drawing objects to be modified.
     *
     * @param {DrawingOrderType} orderType
     *  The ordering direction for the drawing objects.
     */
    generateReorderDrawingOperations(generator, positions, orderType) {

        // sort by position, remove positions of embedded drawings whose parents will be deleted too
        positions = optimizeDrawingPositions(positions);

        // number of drawing objects in this collection
        var modelCount = this.getModelCount();
        // reduce to positions of top-level drawing objects (TODO: delegate to group objects?)
        var fromIndexes = positions.filter(pos => pos.length === 1).map(pos => pos[0]);

        // nothing to do, if none or all drawing models have been selected for moving
        if ((fromIndexes.length === 0) || (fromIndexes.length === modelCount)) { return; }

        // resolve the drawing models
        var moveModels = fromIndexes.map(fromIndex => this.getModel([fromIndex]));

        // process the different ordering types
        switch (orderType) {

            // all models to back (decrease Z oder indexes)
            case DrawingOrderType.BACK: {
                moveModels.forEach((moveModel, toIndex) => {
                    moveModel.generateReorderOperations(generator, toIndex);
                });
                break;
            }

            // all models to front (increase Z oder indexes)
            case DrawingOrderType.FRONT: {
                var toIndex = this.getModelCount() - 1;
                ary.forEach(moveModels, moveModel => {
                    moveModel.generateReorderOperations(generator, toIndex);
                    toIndex -= 1;
                }, { reverse: true });
                break;
            }

            // all models one step down (decrease Z oder indexes)
            case DrawingOrderType.BACKWARD: {
                var minIndex = 0;
                if (moveModels.length > 1) {
                    moveModels.forEach((moveModel, ai) => {
                        var toIndex = Math.max(minIndex, fromIndexes[ai] - 1);
                        moveModel.generateReorderOperations(generator, toIndex);
                        minIndex = toIndex + 1;
                    });
                } else {
                    // find nearest sibling overlapping drawing object
                    var thisRect1 = moveModels[0].getRectangleHmm();
                    for (minIndex = fromIndexes[0] - 1; minIndex >= 0; minIndex -= 1) {
                        if (this.getModel([minIndex]).getRectangleHmm().overlaps(thisRect1)) {
                            moveModels[0].generateReorderOperations(generator, minIndex);
                            break;
                        }
                    }
                }
                break;
            }

            // all models one step up (increase Z oder indexes)
            case DrawingOrderType.FORWARD: {
                var maxIndex = this.getModelCount() - 1;
                if (moveModels.length > 1) {
                    ary.forEach(moveModels, (moveModel, ai) => {
                        var toIndex = Math.min(maxIndex, fromIndexes[ai] + 1);
                        moveModel.generateReorderOperations(generator, toIndex);
                        maxIndex = toIndex - 1;
                    }, { reverse: true });
                } else {
                    // find nearest sibling overlapping drawing object
                    var thisRect2 = moveModels[0].getRectangleHmm();
                    for (maxIndex = fromIndexes[0] + 1; maxIndex < modelCount; maxIndex += 1) {
                        if (this.getModel([maxIndex]).getRectangleHmm().overlaps(thisRect2)) {
                            moveModels[0].generateReorderOperations(generator, maxIndex);
                            break;
                        }
                    }
                }
                break;
            }
        }
    }

    // operation handlers -----------------------------------------------------

    /**
     * Callback handler for the document operation 'copySheet'. Clones all
     * drawing objects from the passed collection into this collection.
     *
     * @param {SheetOperationContext} _context
     *  A wrapper representing the 'copySheet' document operation.
     *
     * @param {SheetDrawingCollection} fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyCopySheetOperation(_context, fromCollection) {

        // clone the DOM text contents of the drawing model, and all embedded child models
        const copyTextFrames = sourceModel => {

            // copy the DOM text frame of the specified model
            var sourceFrame = fromCollection.getDrawingFrameForModel(sourceModel);
            var targetFrame = this.getDrawingFrame(sourceModel.getPosition());
            copyTextFrame(sourceFrame, targetFrame);

            // copy the DOM text frames of the embedded models
            sourceModel.forEachChildModel(copyTextFrames);
        };

        // clone the contents of the source collection
        for (const [index, drawingModel] of fromCollection.yieldModelEntries()) {

            // clone the source drawing model and insert it into this collection
            // (the embedded drawing models of group objects will be cloned automatically)
            var newDrawingModel = drawingModel.clone(this.sheetModel, this);
            this.implInsertModel([index], newDrawingModel);

            // clone the DOM text contents of the drawing model, and all embedded child models
            copyTextFrames(drawingModel);
        }
    }

    /**
     * Callback handler for the document operation 'insertDrawing'.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the 'insertDrawing' document operation.
     *
     * @param {Array<Number>} position
     *  The insertion position of the new drawing object in this collection
     *  (without leading sheet index).
     */
    applyInsertDrawingOperation(context, position) {
        var result = this.insertModel(position, context.getStr('type'), context.optDict('attrs'));
        context.ensure(result, 'cannot insert drawing');
    }

    /**
     * Callback handler for the document operation 'deleteDrawing'.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the 'deleteDrawing' document operation.
     *
     * @param {Array<Number>} position
     *  The position of the drawing object to be deleted in this collection
     *  (without leading sheet index).
     */
    applyDeleteDrawingOperation(context, position) {
        var result = this.deleteModel(position);
        context.ensure(result, 'cannot delete drawing');
    }

    /**
     * Callback handler for the document operation 'moveDrawing'.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the 'moveDrawing' document operation.
     *
     * @param {Array<Number>} position
     *  The position of the drawing object to be moved in this collection
     *  (without leading sheet index).
     */
    applyMoveDrawingOperation(context, position) {

        // get the target position (must be located in the same sheet for now)
        var targetPos = context.getPos('to');
        context.ensure(targetPos[0] === this.sheetModel.getIndex(), 'cannot move drawing to another sheet');
        targetPos = targetPos.slice(1);

        // target position must be located in the same parent drawing object
        context.ensure(_.isEqual(position.slice(0, -1), targetPos.slice(0, -1)), 'invalid target position');
        var result = this.moveModel(position, _.last(targetPos));
        context.ensure(result, 'cannot move drawing');
    }

    // protected methods ------------------------------------------------------

    /**
     * Constructs a drawing model instance for the specified drawing type.
     *
     * @param {DrawingCollection} parentCollection
     *  The parent drawing collection that will contain the new drawing
     *  object.
     *
     * @param {DrawingType} drawingType
     *  The type of the drawing model to be created.
     *
     * @param {Dict} [attrSet]
     *  Initial formatting attributes for the drawing model.
     *
     * @returns {Opt<DrawingModel>}
     *  The new drawing model instance, if the passed type is supported.
     */
    /*protected*/ clientCreateModel(parentCollection, drawingType, attrSet) {

        // create an explicit model for supported drawing types
        switch (drawingType) {
            case DrawingType.SHAPE:
                return new SheetShapeModel(this.sheetModel, parentCollection, attrSet);
            case DrawingType.CONNECTOR:
                return new SheetConnectorModel(this.sheetModel, parentCollection, attrSet);
            case DrawingType.IMAGE:
                return new SheetImageModel(this.sheetModel, parentCollection, attrSet);
            case DrawingType.CHART:
                return new SheetChartModel(this.sheetModel, parentCollection, attrSet);
            case DrawingType.GROUP:
                return new SheetGroupModel(this.sheetModel, parentCollection, attrSet);
            case DrawingType.UNDEFINED:
                return new SheetUndefinedModel(this.sheetModel, parentCollection, attrSet);
            case DrawingType.DIAGRAM:
            case DrawingType.OLE:
                // create a placeholder model for known but unsupported drawing types
                return new SheetDrawingModel(this.sheetModel, parentCollection, drawingType, attrSet);
        }

        return undefined;
    }
}
