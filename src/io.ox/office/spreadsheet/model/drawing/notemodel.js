/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, math, dict, pick } from "@/io.ox/office/tk/algorithms";

import { DrawingModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { XSheetDrawingModel } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";

// class NoteModelConfig ======================================================

/**
 * All additional settings of a spreadsheet cell notes.
 */
export class NoteModelConfig {

    // static functions -------------------------------------------------------

    static parseJSON(anchor, data) {
        return new NoteModelConfig(anchor, pick.string(data, "author"), pick.string(data, "date"), pick.string(data, "text"));
    }

    // constructor ------------------------------------------------------------

    constructor(anchor, author, date, text) {
        this.anchor = anchor.clone();
        this.author = author || "";
        this.date = date || "";
        this.text = text || "";
    }

    // public methods ---------------------------------------------------------

    clone() {
        return new NoteModelConfig(this.anchor, this.author, this.date, this.text);
    }

    toJSON() {
        const data = dict.create();
        if (this.author) { data.author = this.author; }
        if (this.date) { data.date = this.date; }
        if (this.text) { data.text = this.text; }
        return data;
    }
}

// class NoteModel ============================================================

/**
 * The model of a cell note shape contained in a sheet.
 */
export class NoteModel extends XSheetDrawingModel.mixin(DrawingModel) {

    // constructor ------------------------------------------------------------

    /**
     * @param {SheetModel} sheetModel
     *  The sheet model instance containing this drawing object.
     *
     * @param {DrawingCollection} parentCollection
     *  The parent drawing collection that will contain this drawing object.
     *
     * @param {Dict} attrSet
     *  An attribute set with initial formatting attributes for the object.
     *
     * @param {NoteModelConfig} modelConfig
     *  Additional note configuration.
     */
    constructor(sheetModel, parentCollection, attrSet, modelConfig) {

        super(sheetModel, parentCollection, "note", attrSet, { families: ["shape", "fill", "line", "character", "paragraph"] });

        // the configuration of the cell note
        this.config = modelConfig.clone();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the address of the anchor cell of this note model.
     *
     * @returns {Address}
     *  The address of the anchor cell of this note model.
     */
    getAnchor() {
        return this.config.anchor;
    }

    /**
     * Returns the location of the anchor cell of this note model in the sheet,
     * in pixels according to the current sheet zoom factor.
     *
     * @returns {Rectangle}
     *  The location of the anchor cell in the sheet.
     */
    getAnchorRectangle() {
        return this.sheetModel.getAnchorRectangle(this.config.anchor);
    }

    /**
     * Returns the name of the author of this note model.
     *
     * @returns {string}
     *  The name of the author of this note model. May be an empty string.
     */
    getAuthor() {
        return this.config.author;
    }

    /**
     * Returns the creation date of this note model.
     *
     * @returns {Date}
     *  The creation of this note model.
     */
    getDate() {
        return this.config.date;
    }

    /**
     * Returns the text contents of this note model.
     *
     * @returns {string}
     *  The text contents of this note model.
     */
    getText() {
        return this.config.text;
    }

    /**
     * Moves this note model to a new anchor cell.
     *
     * @param {Address} anchor
     *  The address of the new anchor cell for this note model.
     *
     * @returns {boolean}
     *  Whether the anchor address has been changed.
     */
    setAnchor(anchor) {
        if (this.config.anchor.equals(anchor)) { return false; }
        this.config.anchor = anchor;
        return true;
    }

    /**
     * Changes the text contents of this note model.
     *
     * @param {string} text
     *  The new text contents for this note model.
     *
     * @returns {boolean}
     *  Whether the text contents have been changed.
     */
    setText(text) {
        if (this.config.text === text) { return false; }
        this.config.text = text;
        return true;
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations and the undo operations to change the attribute
     * set of this cell note.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Dict} attrSet
     *  The incomplete attribute set with all formatting attributes to be
     *  changed.
     *
     * @param {GeneratorOptions} [options]
     *  Additional options to be passed to the operation generator.
     */
    generateChangeOperations(generator, attrSet, options) {

        // create the undo attributes; nothing to do if no attribute actually changes
        const undoAttrSet = this.getUndoAttributeSet(attrSet);
        if (is.empty(undoAttrSet)) { return; }

        // bug 58272: change operation is generated after moving cells (for new anchor position);
        // but the undo operation needs to be generated for the original anchor position
        // TODO: remove this when removing the "applyImmediately" generator mode
        const transformer = options?.transformer;
        const undoAnchor = transformer ? transformer.transformAddress(this.config.anchor, { reverse: true }) : this.config.anchor;
        if (!undoAnchor) { return; }

        generator.generateAnchorOperation(Op.CHANGE_NOTE, this.config.anchor, { attrs: attrSet }, options);
        generator.generateAnchorOperation(Op.CHANGE_NOTE, undoAnchor, { attrs: undoAttrSet }, { undo: true });
    }

    /**
     * Generates the undo operations needed to restore this cell note.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the undo operations.
     */
    generateRestoreOperations(generator) {
        const operProps = { attrs: this.getExplicitAttributeSet(true), ...this.config.toJSON() };
        generator.generateAnchorOperation(Op.INSERT_NOTE, this.config.anchor, operProps, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to update the
     * position of the drawing frame of this cell note, while moving it to a
     * new anchor cell.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Address} anchor
     *  The new anchor position of this cell note.
     */
    generateMoveAnchorOperations(generator, anchor) {

        // resolve the current anchor rectangle (expand to merged ranges)
        const oldAnchorRect = this.sheetModel.getAnchorRectangle(this.config.anchor);
        // resolve the new anchor rectangle (expand to merged ranges)
        const newAnchorRect = this.sheetModel.getAnchorRectangle(anchor);
        // the difference of the anchor positions (top-right edges of the anchor cells)
        const moveX = newAnchorRect.right() - oldAnchorRect.right();
        const moveY = newAnchorRect.top - oldAnchorRect.top;

        // calculate the new location of the drawing frame
        const oldFrameRect = this.getRectangle();
        const newFrameRect = oldFrameRect.clone().translateSelf(moveX, moveY);

        // keep new position of the drawing frame inside sheet area
        const sheetRect = this.sheetModel.getSheetRectangle({ pixel: true });
        newFrameRect.left = math.clamp(newFrameRect.left, 0, sheetRect.right() - newFrameRect.width);
        newFrameRect.top = math.clamp(newFrameRect.top, 0, sheetRect.bottom() - newFrameRect.height);

        // generate the operations to update the location of the drawing frame
        const anchorAttrs = this.parentCollection.generateAnchorAttributesForRect(this, newFrameRect, { pixel: true });
        if (anchorAttrs) { this.generateChangeOperations(generator, { drawing: anchorAttrs }); }
    }

    // client API methods -----------------------------------------------------

    /**
     * Generates the operation to delete this cell note.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     */
    /*protected override*/ clientGenerateDeleteOperation(generator, options) {
        generator.generateAnchorOperation(Op.DELETE_NOTE, this.config.anchor, null, options);
    }

    /**
     * Returns the string "delete", if this note will be deleted by the passed
     * update task. The calling code will react and generate the appropriate
     * operations for deleting and restoring this note.
     */
    /*protected override*/ clientProcessUpdateTask(_sheetCache, updateTask) {
        const deletedAnchor = this.sheetModel.isOwnMoveTask(updateTask) && !updateTask.transformer.transformAddress(this.config.anchor);
        return deletedAnchor ? "delete" : undefined;
    }

    /**
     * Returns a clone of this note model for the specified sheet.
     */
    /*protected override*/ clientCloneModel(targetModel, targetCollection) {
        return new NoteModel(targetModel, targetCollection, this.getExplicitAttributeSet(true), this.config);
    }
}
