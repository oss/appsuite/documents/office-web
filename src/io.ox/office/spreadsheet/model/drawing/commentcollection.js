/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ox from '$/ox';

import { is, ary, jpromise } from '@/io.ox/office/tk/algorithms';
import { containsFocus } from '@/io.ox/office/tk/dom';
import { getNodePositionInPage } from '@/io.ox/office/tk/utils';

import { OX_PROVIDER_ID, calculateUserId } from '@/io.ox/office/editframework/utils/operationutils';

import { CHANGE_COMMENT, DELETE_COMMENT, INSERT_COMMENT, MOVE_COMMENTS } from '@/io.ox/office/spreadsheet/utils/operations';
import { modelLogger, Address, AddressSet, MergeMode, AnchorMode, RangeArray, isOriginCell } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import { CommentSettings, CommentModel } from '@/io.ox/office/spreadsheet/model/drawing/commentmodel';
import { XSheetDrawingCollection } from '@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingcollection';

// class CommentCollection ====================================================

export class CommentCollection extends XSheetDrawingCollection.mixin(SheetChildModel) {

    constructor(sheetModel) {

        // base constructor
        super(sheetModel, sheetModel);

        // properties ---------------------------------------------------------

        var self = this;

        // the anchor address of the comment
        // the document model instance
        var docModel = this.docModel;
        var docApp = this.docApp;
        var docView = docApp.docView;

        var unsavedComment = null;

        // all comment models, sorted by their anchor address (row by row)
        var sortedComments = this.member/*<CommentModel[]>*/([]);

        // all comment models, mapped by anchor cell address
        var addressSet = new AddressSet();

        // the name of the current user
        var userName = null;

        this.rootCollection = this;

        // private methods ----------------------------------------------------

        /**
         * Sort comparator callback function.
         */
        function modelSorter(commentModel1, commentModel2) {
            var c = Address.compare(commentModel1.getAnchor(), commentModel2.getAnchor());

            if (c === 0) {
                if (commentModel1.isReply() !== commentModel2.isReply()) {
                    return commentModel1.isReply() ? 1 : -1;
                }
                c =  commentModel1.getDateAsNumber() - commentModel2.getDateAsNumber();
                if (c === 0) {

                    var replies = addressSet.at(commentModel1.getAnchor()).children;

                    c = replies.indexOf(commentModel1) - replies.indexOf(commentModel2);
                }
            }

            return c;
        }

        function getIsoDate() {
            var date = new Date();
            date.setMilliseconds(0);

            // the date string in ISO format, without milliseconds
            return date.toISOString().replace('.000Z', 'Z');
        }

        function getCommentIndex(commentModel, index) {

            if (commentModel) {
                if (commentModel.isReply()) {
                    var replyIndex = addressSet.at(commentModel.getAnchor()).children.indexOf(commentModel);
                    if (replyIndex >= 0) {
                        return replyIndex + 1;
                    }
                } else {
                    return 0;
                }
            }

            return index;
        }

        function getReplyIndex(commentModel) {
            return  addressSet.at(commentModel.getAnchor()).children.indexOf(commentModel) + 1;
        }

        function generateInsertOperation(generator, commentModel, undo, paste) {

            var props = commentModel.getJSONSettings();
            var anchor = commentModel.getAnchor();

            if (!undo && !paste) {
                props.newcomment = true;
            }

            if (commentModel.isReply()) {
                if (undo) {
                    props.index = getReplyIndex(commentModel);
                } else {
                    props.index = addressSet.at(anchor).children.length + 1;
                }
            } else {
                props.index = 0;
            }

            generator.generateAnchorOperation(INSERT_COMMENT, anchor, props, { undo });
        }

        function generateDeleteOperation(generator, commentModel, undo) {
            var props = {};

            var anchor = commentModel.getAnchor();

            if (commentModel.isReply()) {
                if (undo) {
                    props.index = addressSet.at(anchor).children.length + 1;
                } else {
                    props.index = getReplyIndex(commentModel);
                }
            } else {
                props.index = 0;
            }

            generator.generateAnchorOperation(DELETE_COMMENT, anchor, props, { undo });
        }

        function generateChangeThreadOperations(generator, threadedCommentModel, commentInfo, drawing) {
            var changeString = commentInfo && is.string(commentInfo.text);

            if (!changeString && !drawing) {
                return this;
            }

            var generateChangeOperation = function (generator, commentModel, undo) {
                var anchor = commentModel.getAnchor();
                var props = {};
                var mentions = commentModel.getMentions();

                if (commentModel.isReply()) {
                    props.index = getReplyIndex(commentModel);
                } else {
                    props.index = 0;
                }

                if (changeString) {
                    props.text = commentModel.getText();
                }
                if (mentions) {
                    props.mentions = mentions;
                }

                if (drawing) {
                    props.attrs = { drwaing: commentModel.getMergedAttributeSet(true).drawing };
                }

                generator.generateAnchorOperation(CHANGE_COMMENT, anchor, props, { undo });
            };

            generateChangeOperation(generator, threadedCommentModel, true);

            if (changeString) {
                threadedCommentModel.setText(commentInfo.text);
                threadedCommentModel.setMentions(commentInfo.mentions);
            }
            if (drawing) {
                threadedCommentModel.setAttributes({ drawing });
            }

            generateChangeOperation(generator, threadedCommentModel, false);
            return this;
        }

        function triggerEvent(event, commentModel) {
            if (docModel.isImportFinished()) {
                docModel.trigger(event, commentModel);
            }
        }

        function getUserName() {
            if (userName !== null) {
                return jpromise.resolve(userName);
            }
            /**
             *  fixing #Bug 68174 - "Better 'user name' for external invitees / collaborators"
             *  [https://bugs.open-xchange.com/show_bug.cgi?id=68174]
             *
             *  This solutions uses the `info.guest` flag as switch of how to retrieve a user's/client's name.
             *  In order to not end up with a cryptic generated name for an external invitee like 'User 10314',
             *  as it would happen with `info.operationName` one uses `info.displayName` for an identified guest.
             */
            return docApp.getUserInfo(ox.user_id).then(function (info) {
                userName = ((info.guest && info.displayName) || info.operationName);
                return userName;
            });
        }

        function registerComment(commentModel, index) {

            var anchor = commentModel.getAnchor().clone();

            if (commentModel.isReply() && !addressSet.has(anchor)) {
                throw new Error('cannot insert reply comment, because threaded comment not exist');
            }

            if (!commentModel.isReply() && addressSet.has(anchor)) {
                throw new Error('cannot insert threaded comment, because a comment on this address exist');
            }

            ary.insertSorted(sortedComments, commentModel, modelSorter);

            if (commentModel.isReply()) {
                addressSet.at(anchor).children.splice(index, 0, commentModel);
            } else {
                anchor.model = commentModel;
                anchor.children = [];
                addressSet.add(anchor);
            }
        }

        /**
         * Unregister a comment and remove it from the comment lists.
         *
         * @param {Address} anchorAddress
         *  The linked cell of the comment.
         * @param {Integer} index
         *  The comment thread index of the comment to unregister. 0 = root comment and > 1 are comment replies.
         *  If the index is 0 the complete thread will be deleted otherwise delete a single thread.
         */
        function unregisterComment(anchorAddress, index) {
            var deletedComments = [];
            function remove(comment, index) {
                deletedComments.push({ model: comment, index });
            }

            var anchorToRemove;
            var anchor = addressSet.at(anchorAddress);
            if (anchor) {

                // delete complete thread
                if (index === 0) {
                    for (var idx = 0; idx < anchor.children.length; idx++) {
                        remove(anchor.children[idx], idx + 1);
                    }
                    anchor.children = [];
                    remove(anchor.model, 0);
                    anchorToRemove = anchor;

                } else { // delete reply comment
                    remove(anchor.children[index - 1], index);
                    anchor.children.splice(index - 1, 1);
                }
            }

            for (const { model } of deletedComments) {
                ary.deleteSorted(sortedComments, model, modelSorter);
            }

            if (anchorToRemove) {
                addressSet.delete(anchorToRemove);
            }

            return deletedComments;
        }

        /**
         * Recalculates the position of all cell comments, after cells have
         * been moved (including inserted/deleted columns or rows) in the
         * sheet.
         */
        function moveCellsHandler({ transformer }) {

            // collect comments to be moved or deleted (do not alter the container while iterating)
            var movedModels = [];
            var deletedModels = [];

            function getNewAnchor(address) {
                var newAnchor = transformer.transformAddress(address);
                if (!newAnchor) {
                    return false;
                } else if (newAnchor.differs(address)) {
                    return newAnchor;
                }
                return null;
            }

            if (unsavedComment && unsavedComment.isNewThread) {
                const commentAnchor = unsavedComment.model.getAnchor();
                const newAnchor = getNewAnchor(commentAnchor);
                if (newAnchor === false) {
                    triggerEvent('new:threadedcomment:thread:delete');
                } else if (newAnchor) {
                    unsavedComment.model.setAnchor(newAnchor);
                }
            }

            // no preparation work needed without comment models
            if (!addressSet.size) { return; }

            for (const element of addressSet.ordered({ reverse: transformer.insert })) {
                const newAnchor = getNewAnchor(element);
                if (newAnchor === false) {
                    deletedModels.push(element.model);
                } else if (newAnchor) {
                    movedModels.push({ model: element.model, target: newAnchor });
                }
            }

            // first, delete all models (do not move comments over comments to be deleted)
            deletedModels.forEach(function (commentModel) {
                unregisterComment(commentModel.getAnchor(), getCommentIndex(commentModel));
            });

            triggerEvent('delete:threadedcomment:end', deletedModels);

            // move all comment models to their new position
            movedModels.forEach(function (entry) {

                // update the anchor address, and the comment model containers
                var parentIndex = getCommentIndex(entry.model);
                var unregisteredComments = unregisterComment(entry.model.getAnchor(), parentIndex);

                var parentModel = entry.model;

                parentModel.setAnchor(entry.target);
                registerComment(parentModel, parentIndex);

                unregisteredComments.forEach(function (comment) {

                    var commentModel = comment.model;
                    if (commentModel.isReply()) {
                        commentModel.setAnchor(entry.target);
                        registerComment(commentModel, comment.index);
                    }
                });
            });
        }

        // protected methods --------------------------------------------------

        /**
         * Callback handler for the document operation "copySheet". Clones all
         * cell comments from the passed collection into this collection.
         *
         * @param {SheetOperationContext} _context
         *  A wrapper representing the document operation "copySheet".
         *
         * @param {CommentCollection} fromCollection
         *  The existing collection whose contents will be cloned into this new
         *  collection.
         *
         * @throws {OperationError}
         *  If applying the operation fails, e.g. if a required property is
         *  missing in the operation.
         */
        this.applyCopySheetOperation = function (_context, fromCollection) {

            // clone the contents of the source collection
            for (const anchor of fromCollection.threadModels()) {

                // clone the root comment
                var rootModel = anchor.model.clone(sheetModel, this);
                registerComment(rootModel, 0);
                triggerEvent('insert:threadedcomment', rootModel);

                // clone all replies
                for (const [index, fromModel] of anchor.children.entries()) {
                    const replyModel = fromModel.clone(sheetModel, this);
                    replyModel.setParent(rootModel);
                    registerComment(replyModel, index + 1);
                    triggerEvent('insert:threadedcomment', replyModel);
                }
            }
        };

        /**
         * Callback handler for the document operation "insertComment". Creates
         * and stores a new comment, and triggers an "insert:comment" event.
         *
         * @param {SheetOperationContext} context
         *  A wrapper representing the document operation "insertComment".
         */
        this.applyInsertCommentOperation = function (context) {

            // multiple comments must not be located in the same cell
            var anchor = context.getAddress('anchor');
            var index = context.getInt('index');

            var rootComment = addressSet.at(anchor);

            if (rootComment) {
                rootComment = rootComment.model;
            } else {
                context.ensure(index === 0, 'invalid comment index');
            }

            var attrs =  context.optDict('attrs');
            var drawing;
            if (attrs) {
                drawing = attrs.drawing;
            }

            // additional settings for the comment model
            var settings = new CommentSettings(
                anchor,
                rootComment,
                context.optStr('author'),
                context.optStr('authorId', { empty: true }),
                context.optStr('authorProvider', { empty: true }),
                context.optStr('date'),
                context.optStr('text', { empty: true }),
                context.optArr('mentions'),
                context.optBool('done'),
                drawing
            );

            var commentModel = new CommentModel(sheetModel, this, settings);
            registerComment(commentModel, index);

            if (context.optBool('newcomment')) {
                if (commentModel.isReply()) {
                    triggerEvent('new:threadedcomment:comment', commentModel);
                } else {
                    triggerEvent('new:threadedcomment:thread', commentModel);
                }
                delete context.operation.newcomment;
            } else {
                triggerEvent('insert:threadedcomment', commentModel);
            }
        };

        /**
         * Callback handler for the document operation "deleteComment". Deletes
         * an existing comment, and triggers a "delete:drawing" event.
         *
         * @param {SheetOperationContext} context
         *  A wrapper representing the document operation "deleteComment".
         */
        this.applyDeleteCommentOperation = function (context) {
            var anchorAddress = context.getAddress('anchor');
            var index = context.getInt('index');

            var deletedComments = unregisterComment(anchorAddress, index);
            context.ensure(deletedComments.length, 'cannot delete threaded comment from cell %s', anchorAddress);

            deletedComments = deletedComments.map(function (comment) { return comment.model; });

            if (this.hasUnsavedComment()) {
                var unsavedCommentModel =  this.getUnsavedComment().model;
                var editComment = deletedComments.find(function (comment) {
                    return (comment.equals(unsavedCommentModel) || (!comment.isReply() && unsavedCommentModel.isReply() && comment.getId() === unsavedCommentModel.getParentId()));
                });
                if (editComment) {
                    this.deleteUnsavedComment();
                }
            }
            triggerEvent('delete:threadedcomment:end', deletedComments);
        };

        this.applyChangeCommentOperation = function (context) {
            var anchor = context.getAddress('anchor');
            var index = context.getInt('index');

            var rootComment = addressSet.at(anchor);

            var comment;
            if (rootComment) {
                if (index === 0) {
                    comment = rootComment.model;
                } else {
                    comment = rootComment.children[index - 1];
                }
            }

            context.ensure(comment, 'cannot change threaded comment from cell %s', anchor);

            if (context.has('text')) {
                comment.setText(context.getStr('text', { empty: true }));

                if (context.has('mentions')) {
                    comment.setMentions(context.getArr('mentions'));
                }

                triggerEvent('change:threadedcomment', comment);
            }

            var attrSet = context.optDict('attrs');
            if (attrSet) {
                comment.setAttributes(attrSet);
            }
        };

        /**
         * Callback handler for the document operation "moveComments". Moves
         * multiple comments to new anchor positions, and triggers a
         * "move:comments" event.
         *
         * @param {SheetOperationContext} context
         *  A wrapper representing the document operation "moveComments".
         */
        this.applyMoveCommentsOperation = function (context) {

            // get the source and target anchor addresses
            var fromAnchors = context.getAddressList('from');
            var toAnchors = context.getAddressList('to');
            var moveComments = [];
            context.ensure(fromAnchors.length === toAnchors.length, 'anchor from/to count mismatch');

            fromAnchors.forEach(function (fromAnchor, index) {
                var commentModels = self.getByAddress(fromAnchor);
                if (commentModels) {
                    ary.forEach(commentModels, commentModel => {
                        var commentIndex = getCommentIndex(commentModel);
                        unregisterComment(fromAnchor, commentIndex);
                        commentModel.setAnchor(toAnchors[index]);
                        moveComments.push({ model: commentModel, index: commentIndex });
                    }, { reverse: true });
                }
            });

            var movedModels = [];
            ary.forEach(moveComments, comment => {
                registerComment(comment.model, comment.index);
                if (!comment.model.isReply()) { movedModels.push(comment.model); }
            }, { reverse: true });

            this.trigger('move:comments', movedModels);

            triggerEvent('after:move:threadedcomments');
        };

        // public methods -----------------------------------------------------

        this.getCommentModels = function () {
            return sortedComments;
        };

        this.hasComment = function (address) {
            return addressSet.has(address);
        };

        this.getStartCommentByAddress = function (address) {
            return addressSet.at(address)?.model ?? null;
        };

        /**
         * Returns the sorted threaded comment models anchored at the specified cell address.
         *
         * @param {Address} address
         *  The address of the comment's anchor cell.
         *
         * @returns {Opt<SheetCommentModel[]>}
         *  The models of the cell comment anchored at the specified cell, if
         *  existing; otherwise `undefined`.
         */
        this.getByAddress = function (address) {
            var anchor = addressSet.at(address);
            return anchor ? sortedComments.filter(function (commentModel) {
                return commentModel.getAnchor().equals(address);
            }) : undefined;
        };

        this.selectComment = function (comment) {
            if (comment && !docView.selectionEngine.getActiveCell().equals(comment.getAnchor())) {
                docView.selectionEngine.selectCell(comment.getAnchor());
                const commentFocused = containsFocus(docView.commentsPane.$el);
                docView.scrollToCell(comment.getAnchor(), { preserveFocus: commentFocused });
                return docApp.docController.update();
            }
            return jpromise.resolve();
        };

        this.setUnsavedComment = function (comment) {
            unsavedComment = comment || null;
        };

        this.getUnsavedComment = function () {
            return unsavedComment;
        };

        this.deleteUnsavedComment = function () {
            unsavedComment = null;
        };

        this.hasUnsavedComment = function () {
            return !!unsavedComment;
        };

        /**
         * Returns whether this collection contains any cell comments.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  - {Boolean} [options.visible]
         *      If set to a boolean value, only comments with a visibility
         *      state (formatting attribute) equal to the option value will be
         *      considered, regardless of their position in hidden or visible
         *      columns/rows. If omitted, the visibility state of the comments
         *      will be ignored.
         *  - {Boolean} [options.visibleCells=false]
         *      If set to true, only comments contained in visible columns and
         *      rows will be considered.
         *
         * @returns {Boolean}
         *  Whether this collection contains any cell comments.
         */
        this.hasComments = function (options) {
            return !this.threadModels(options).next().done;
        };

        this.threadModels = function *(options) {
            if (options?.ranges) {
                for (const range of ary.values(RangeArray.cast(options.ranges), options)) {
                    yield* addressSet.ordered({ ...options, range });
                }
            } else {
                yield* addressSet.ordered(options);
            }
        };

        this.yieldCommentModels = function *(options) {
            for (const threadModel of this.threadModels(options)) {
                yield threadModel.model;
            }
        };

        this.getSelectedThread = function () {
            return this.getStartCommentByAddress(docView.selectionEngine.getActiveCell());
        };

        /**
         * Returns the count of the threads of the sheet.
         */
        this.getThreadCount = function () {
            return addressSet.size;
        };

        this.generateMergeCellsOperations = function (generator, mergedRanges, mergeMode) {

            // nothing to do without comments, or when unmerging a cell range
            if (!addressSet.size || (mergeMode === MergeMode.UNMERGE)) {
                return this.createResolvedPromise();
            }

            // collect comments to be deleted (do not alter the container while iterating)
            var deletedModels = [];

            // process all passed merged ranges
            var promise = this.iterateArraySliced(RangeArray.cast(mergedRanges), function (mergedRange) {
                for (const element of addressSet.ordered({ range: mergedRange })) {
                    if (!isOriginCell(element, mergedRange, mergeMode)) {
                        deletedModels.push(element.model);
                    }
                }
            });

            // generate all operations to delete the comments
            return promise.then(function () {
                return self.iterateArraySliced(deletedModels, function (commentModel) {
                    self.generateDeleteCommentOperations(generator, commentModel);
                });
            });
        };

        /**
         * Generates the operations, and the undo operations, to move multiple
         * cell comments to new anchor cells. The positions of the drawing
         * frames will be updated relative to the new anchor cells.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {AddressArray} fromAddresses
         *  An array of cell addresses specifying the current anchor position
         *  of the cell comments.
         *
         * @param {AddressArray} toAddresses
         *  An array of cell addresses specifying the new anchor position of
         *  the cell comments. This array MUST contain exactly the same number
         *  of cell addresses as parameter "fromAddresses".
         */
        this.generateMoveAnchorOperations = function (generator, fromAddresses, toAddresses) {

            // the address properties for the "moveComments" operation
            var fromStr = fromAddresses.toOpStr();
            var toStr = toAddresses.toOpStr();

            // generate the "moveComments" operations
            generator.generateSheetOperation(MOVE_COMMENTS, { from: toStr, to: fromStr }, { undo: true });
            generator.generateSheetOperation(MOVE_COMMENTS, { from: fromStr, to: toStr });
        };

        /**
         * Generates the operations, and the undo operations, to delete a
         * single cell comment.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {CommentModel} commentModel
         *  The model of the comment to be deleted. It this model is a reply,
         *  it will be deleted from the thread. If it is a root comment, the
         *  entire thread will be deleted.
         */
        this.generateDeleteCommentOperations = function (generator, commentModel) {
            const restoreModels = commentModel.isReply() ? commentModel : this.getByAddress(commentModel.getAnchor());
            this.generateRestoreOperations(generator, restoreModels);
            generateDeleteOperation(generator, commentModel, false);
        };

        /**
         * Generates the operations, and the undo operations, to delete the
         * cell comment thread from the specified cell.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {Address} anchor
         *  The address of the anchor cell containing the comment thread to be
         *  deleted.
         */
        this.generateDeleteThreadOperations = function (generator, anchor) {
            const commentModel = addressSet.at(anchor)?.model;
            if (commentModel) {
                this.generateDeleteCommentOperations(generator, commentModel);
            }
        };

        /**
         * Generates the operations, and the undo operations, to delete all
         * cell comment threads (optionally located in specific ranges) from
         * this collection.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {RangeSource} [ranges]
         *  The cell ranges containing the cell comments to be deleted. If
         *  omitted, all existing cell comments will be deleted.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when the operations have been generated.
         */
        this.generateDeleteAllCommentsOperations = function (generator, ranges) {
            // bug 64896: delete in reversed order (uses an array iterator internally)
            var iterator = this.yieldCommentModels({ ranges, reverse: true });
            return this.asyncForEach(iterator, commentModel => {
                this.generateDeleteCommentOperations(generator, commentModel);
            });
        };

        /**
         * Generates the operations and undo operations to update and restore
         * the inactive anchor attributes of the comment text shapes, after the
         * size or visibility of the columns or rows in the sheet has been
         * changed.
         *
         * @param {OperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when all operations have been generated.
         */
        this.generateRefreshAnchorOperations = function (generator) {
            return this.asyncForEach(this.yieldCommentModels(), commentModel => {
                // recalculate the inactive anchor attributes according to the anchor type
                var anchorAttrs = this.generateAnchorAttributes(commentModel);
                if (anchorAttrs) {
                    generateChangeThreadOperations(generator, commentModel, null, anchorAttrs);
                }
            });
        };

        /**
         * Generates the operations and undo operations to update or restore
         * the anchors of the fallback note shapes in this collection.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operation cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when all operations have been generated.
         */
        this.generateUpdateTaskOperations = modelLogger.profileMethod('$badge{CommentCollection} generateUpdateTaskOperations', function (sheetCache, updateTask) {

            // In order to retain the array indexes in drawing positions, the operations to modify or delete the
            // conmments will be prepended to the sheet cache so that they will be processed in reversed order
            // effectively. The undo operations will be processed in regular (ascending) order, so that preceding
            // comments will be restored before following comments will be modified.

            // nothing to do unless the update task moves cells in the own sheet
            if (!sheetModel.isOwnMoveTask(updateTask)) { return jpromise.resolve(); }

            // update the anchor position of the fallback note shape when moving cells in the sheet
            return this.asyncForEach(this.threadModels(), anchor => {

                // delete the entire thread, if the comment's anchor cell will be deleted
                if (!updateTask.transformer.transformAddress(anchor)) {
                    sheetCache.generateAnchorOperation(DELETE_COMMENT, anchor, { index: 0 }, { prepend: true });
                    this.generateRestoreOperations(sheetCache, this.getByAddress(anchor));
                    return;
                }

                // update the drawing anchor of the fallback note shape
                var anchorAttrs = sheetCache.drawingCache.generateMovedAnchorAttributes(anchor.model);
                if (anchorAttrs) {
                    var attrSet = { drawing: anchorAttrs };
                    var operProps = { index: 0, attrs: attrSet };
                    var undoProps = { index: 0, attrs: anchor.model.getUndoAttributeSet(attrSet) };
                    sheetCache.generateAnchorOperation(CHANGE_COMMENT, anchor, operProps, { prepend: true });
                    sheetCache.generateAnchorOperation(CHANGE_COMMENT, anchor, undoProps, { undo: true });
                }
            });
        });

        /**
         * Generates the undo operations needed to restore this cell comment.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the undo operations.
         */
        this.generateRestoreOperations = function (generator, commentModels) {
            for (const commentModel of ary.wrap(commentModels)) {
                generateInsertOperation(generator, commentModel, true, false);
            }
        };

        this.insertThread = function (anchor, commentInfo) {
            // check that the comments in the active sheet are not locked
            var promise = docView.leaveTextEditMode();

            promise = promise.then(function () {
                return getUserName();
            });

            // generate and apply the operations
            promise = promise.then(function (userName) {
                return sheetModel.createAndApplyOperations(function (generator) {
                    self.generateInsertCommentOperation(generator, anchor, commentInfo.text, userName, String(calculateUserId(ox.user_id)), OX_PROVIDER_ID, getIsoDate(), false, false, commentInfo.mentions);
                    return this;
                }, { storeSelection: true });
            });

            // show warning alerts if necessary
            return docView.yellOnFailure(promise);
        };

        this.generateInsertCommentOperation = function (generator, anchor, text, author, authorId, authorProvider, date, done, paste, mentions) {

            var isReply = addressSet.has(anchor);
            var drawingAttrs = null;
            var commentId = null;

            if (isReply) {
                done = null;
            } else {

                var drawingAnchor = anchor.clone();
                var move = anchor.c >= docModel.addressFactory.maxCol ? -1 : 1;
                drawingAnchor.move(move, true);

                drawingAttrs = sheetModel.drawingCollection.getAnchorAttributesForCellAndSize(AnchorMode.ONE_CELL, drawingAnchor, 3810, 2090);
            }

            var settings = new CommentSettings(anchor, isReply ? addressSet.at(anchor).model : null, author, authorId, authorProvider, date ? date : getIsoDate(), text, mentions, done, drawingAttrs);

            var commentModel = new CommentModel(sheetModel, self, settings);

            // if it is a paste operation then only create a undo operation for the comment and not for the replies: DOCS-1631 Copy&paste a threaded comment- undo:=> internal error
            if (!paste || !isReply) {
                generateDeleteOperation(generator, commentModel, true);
            }

            // inform the users mentioned in a comment
            if (mentions) {
                commentId = String(new Date(date).getTime());
                docApp.sendCommentNotification(mentions, text, commentId);
            }

            generateInsertOperation(generator, commentModel, false, paste);
        };

        this.changeThread = function (commentModel, commentInfo) {
            // check that the comments in the active sheet are not locked
            var promise = docView.leaveTextEditMode();

            // generate and apply the operations
            promise = promise.then(function () {
                return sheetModel.buildOperations(function (sheetCache) {
                    return generateChangeThreadOperations(sheetCache, commentModel, commentInfo);
                }, { storeSelection: true });
            });

            // show warning alerts if necessary
            return docView.yellOnFailure(promise);
        };

        this.startNewThread = function (anchor) {

            return getUserName().then(function (userName) {
                var settings = new CommentSettings(anchor, null, userName, String(calculateUserId(ox.user_id)), '', '', '', null, false, {});
                var commentModel = new CommentModel(sheetModel, self, settings);

                var popupAnchor = function () {
                    var gridPane = docView.getTargetGridPane(anchor);
                    var cellRect = docView.getCellRectangle(anchor, { pixel: true });
                    var canvasPos = getNodePositionInPage(gridPane.layerRootNode);
                    var posInCanvas = gridPane.convertToLayerRectangle(cellRect);
                    cellRect.left = canvasPos.left + posInCanvas.left;
                    cellRect.top = canvasPos.top + posInCanvas.top;
                    return cellRect;
                };

                var options = {
                    model: commentModel,
                    popupOptions: {
                        anchor: popupAnchor,
                        anchorPadding: 4
                    }
                };

                triggerEvent('start:threadedcomment', options);
            });
        };

        // initialization -----------------------------------------------------

        // update position of cell anchors when moving cells in the sheet
        this.listenTo(sheetModel.cellCollection, "move:cells", moveCellsHandler);
    }
}
