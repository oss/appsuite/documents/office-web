/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { ImageModel } from '@/io.ox/office/drawinglayer/model/drawingmodel';
import { XSheetDrawingModel } from '@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel';

// class SheetImageModel ======================================================

/**
 * The model of a image/picture object contained in a sheet.
 *
 * @param {SheetModel} sheetModel
 *  The sheet model instance containing this image object.
 *
 * @param {DrawingCollection} parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param {Object} [initAttributes]
 *  An attribute set with initial formatting attributes for the object.
 */
export class SheetImageModel extends XSheetDrawingModel.mixin(ImageModel) {

    // protected methods ------------------------------------------------------

    /**
     * Returns a clone of this image model for the specified sheet.
     */
    /*protected*/ clientCloneModel(targetModel, targetCollection) {
        return new SheetImageModel(targetModel, targetCollection, this.getExplicitAttributeSet(true));
    }
}
