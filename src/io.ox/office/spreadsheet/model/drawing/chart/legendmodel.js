/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { handleCharacterProps } from '@/io.ox/office/drawinglayer/view/chartstyleutil';
import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';
import { CHANGE_CHART_LEGEND } from '@/io.ox/office/spreadsheet/utils/operations';

// class LegendModel ==========================================================

/**
 * Representation of a legend in a chart drawing object.
 */
export default class LegendModel extends AttributedModel {

    constructor(chartModel, attrs, dataLegend) {

        // base constructor
        super(chartModel.docModel, attrs, { families: ['legend', 'character'] });

        var self = this;

        // public methods -----------------------------------------------------

        this.refreshInfo = function () {
            var attributeSet = self.getMergedAttributeSet(true);
            var legendPos = attributeSet.legend.pos;
            switch (legendPos) {
                case 'bottom':
                case 'top':
                    dataLegend.verticalAlign = legendPos;
                    dataLegend.horizontalAlign = 'center';
                    break;
                case 'left':
                case 'right':
                    dataLegend.verticalAlign = 'center';
                    dataLegend.horizontalAlign = legendPos;
                    break;
                case 'topRight':
                    dataLegend.verticalAlign = 'center';
                    dataLegend.horizontalAlign = 'right';
                    break;
            }

            if (legendPos !== 'off') {
                handleCharacterProps(chartModel, attributeSet.character, dataLegend);
            }
        };

        // operation generators -----------------------------------------------

        /**
         * Generates the undo operations needed to restore this chart legend.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the undo operations.
         *
         * @param {Position} position
         *  The position of the parent chart object in the sheet.
         */
        this.generateRestoreOperations = function (generator, position) {
            if (this.hasExplicitAttributes()) {
                var properties = { attrs: this.getExplicitAttributeSet(true) };
                generator.generateDrawingOperation(CHANGE_CHART_LEGEND, position, properties, { undo: true });
            }
        };
    }
}
