/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { math } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { handleCharacterProps, handleLineProps } from '@/io.ox/office/drawinglayer/view/chartstyleutil';
import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';

import { CHANGE_CHART_AXIS } from '@/io.ox/office/spreadsheet/utils/operations';
import TitleModel from '@/io.ox/office/spreadsheet/model/drawing/chart/titlemodel';
import GridlineModel from '@/io.ox/office/spreadsheet/model/drawing/chart/gridlinemodel';

// class AxisModel ============================================================

/**
 * Representation of a simgle axis in a chart drawing object.
 */
export default class AxisModel extends AttributedModel {

    constructor(chartModel, axisType, axisId, axPos, crossAx) {

        // base constructor
        super(chartModel.docModel, null, { families: ['axis', 'line', 'character'] });

        var self = this;
        var titleModel = null;
        var gridModel = null;
        var format = null;

        var indexName = 'axis' + chartModel.getAxisTypeForDrawing(axisType[0]).toUpperCase() + 'Id';
        var dataAxis;
        if (axisType === 'z') {
            dataAxis = { axisId, axPos, crossAx };
        } else {
            dataAxis = { labelAutoFit: true, labelAngle: 0, axisId, axPos, crossAx };
        }

        chartModel.getModelData()['axis' + axisType.toUpperCase()].push(dataAxis);

        if (dataAxis) {
            titleModel = this.member(new TitleModel(chartModel, axisId, dataAxis, 'title', 'title'));
            gridModel = this.member(new GridlineModel(chartModel, axisId, {}, dataAxis));

            dataAxis.labelFormatter = function (info) {
                if (chartModel.isXYType()) {
                    return formatNumber(info.value);
                } else {
                    return info.label || formatNumber(info.value);
                }
            };
        }

        // private methods ----------------------------------------------------

        function formatNumber(value) {
            if (!chartModel) {
                globalLogger.warn('no chartmodel in AxisModel.formatNumber()');
                return;
            }
            var formatter = chartModel.docModel.numberFormatter;
            var realValue = value * (format && format.exp ? Math.pow(10, format.exp) : 1);
            var formattedNumber;
            if (format && format.format) {
                formattedNumber = formatter.formatValue(format.format, realValue).text;
            } else {
                formattedNumber = formatter.autoFormatNumber(realValue, { stdLen: 8 }).text;
            }
            return formattedNumber === null ? '' : formattedNumber;
        }

        function isHorizontal() {
            return chartModel.getAxisTypeForDrawing(axisType[0]) === 'x';
        }

        function refreshMinMax() {
            var data = chartModel.getModelData();
            if (!data.series.length) {
                return;
            }

            if (axisType[0] === 'x') {
                refreshAxisX(data);
            } else if (axisType[0] === 'y') {
                refreshAxisY(data);
            }
        }

        function makeMin(min, max) {
            if ((!format || !format.date) && (max === min || (min / max < 5 / 6))) {
                return 0;
            }
            return min - ((max - min) / 2);
        }

        function makeMax(min, max) {
            var res = max + 0.2 * (max - min);
            if (res < max) {
                res = max;
            }
            return res;
        }

        function distance(a, b) {
            return Math.abs(a - b);
        }

        function updateInterval(number) {
            var parts = math.splitNumber(number);

            var distances = [];
            distances.push({ value: 1, dist: distance(parts.mant, 1) });
            distances.push({ value: 2, dist: distance(parts.mant, 2) });
            distances.push({ value: 5, dist: distance(parts.mant, 5) });
            distances.push({ value: 10, dist: distance(parts.mant, 10) });

            var newMant = null;
            var minDist = 999999;

            distances.forEach(function (d) {
                if (d.dist < minDist) {
                    minDist = d.dist;
                    newMant = d.value;
                }
            });

            return newMant * Math.pow(10, parts.expn);
        }

        function updateMinMax(minValue, maxValue, precision, intervalCount) {
            if (minValue < maxValue) {
                var interval = (maxValue - minValue) / intervalCount;
                interval = round(interval, precision - 1);
                interval = updateInterval(interval);

                if (interval === 0) {
                    interval = null;
                    dataAxis.minimum = minValue;
                    dataAxis.maximum = maxValue;
                    dataAxis.interval = null;
                }
                // removed for Bug 52880
                setMinMax(minValue, maxValue, interval);
            }
        }

        function setMinMax(min, max, interval) {
            if (!interval || !format || !Number.isFinite(min) || !Number.isFinite(max)) {
                delete dataAxis.minimum;
                delete dataAxis.maximum;
                delete dataAxis.interval;
                return;
            }

            if (min < 0 && format && format.format && format.format.category === 'date') { min = 0; }

            dataAxis.minimum = min;
            dataAxis.maximum = max;
            dataAxis.interval = interval;

            if (chartModel.containsCurvedSeries()) {
                dataAxis.minimum -= dataAxis.interval * 2;
            }
            if (!isHorizontal()) {
                // workaround for Bug 49637
                dataAxis.minimum -= dataAxis.interval * 0.001;
                dataAxis.maximum += dataAxis.interval * 0.001;
            }
            if (chartModel.isXYType()) {
                // workaround for Bug 53190
                dataAxis.minimum -= dataAxis.interval * 0.1;
                dataAxis.maximum += dataAxis.interval * 0.1;
            }

        }

        function isBubbleChart() {
            return chartModel.getChartType().indexOf('bubble') === 0;
        }

        function isAreaChart() {
            return chartModel.getChartType().indexOf('area') === 0;
        }

        function refreshAxisX(data) {

            if (!data.linearX && (chartModel.isXYType() || isAreaChart())) {
                var minValue = Infinity;
                var maxValue = -Infinity;

                _.each(data.series, function (dd) {
                    if (dd[indexName] !== axisId) { return; }

                    _.each(dd.dps, function (dataPoint) {
                        var value = dataPoint[axisType[0]];
                        minValue = Math.min(minValue, value);
                        maxValue = Math.max(maxValue, value);
                    });
                });

                if (isBubbleChart()) {
                    var diff = maxValue - minValue;
                    maxValue +=  diff / 8;
                    minValue -=  diff / 8;
                }

                if (!Number.isFinite(minValue) || !Number.isFinite(maxValue)) {
                    setMinMax();
                    return;
                }
                if (minValue === maxValue) {
                    maxValue = minValue + 0.5;
                    minValue -= 0.5;
                }

                updateMinMax(minValue, maxValue, 2, 4);
            } else {
                setMinMax(null, null, null);
            }
        }

        function round(value, precision) {
            if ((precision > 0) && (precision < 20)) {
                return parseFloat(value.toPrecision(precision));
            }
            return value;
        }

        function refreshAxisY(data) {
            var chartAttrs = chartModel.getMergedAttributeSet(true).chart;
            var stacking = chartAttrs.stacking;

            var givenMin = null;
            var givenMax = null;

            var axisInfo = self.getMergedAttributeSet(true);
            if (axisInfo) {
                if (axisInfo.min !== 'auto') {
                    givenMin = axisInfo.min;
                }
                if (axisInfo.max !== 'auto') {
                    givenMax = axisInfo.max;
                }
            }

            var minValue = Infinity;
            var maxValue = -Infinity;

            if (stacking === 'percentStacked' || (stacking === 'stacked' && data.series.length > 1)) {
                var first = data.series[0];
                _.times(first.dps.length, function (j) {
                    var posValue = 0;
                    var negValue = 0;
                    _.each(data.series, function (dd) {
                        if (dd[indexName] !== axisId) { return; }

                        var jValue = dd.dps[j];
                        if (!jValue) { return; }
                        var useValue = jValue[axisType[0]];
                        if (useValue > 0) {
                            posValue += useValue;
                        } else if (useValue < 0) {
                            negValue += useValue;
                        }
                    });

                    // fix for Bug 52765
                    if (format && format.date) { negValue = posValue; }

                    minValue = Math.min(minValue, negValue);
                    maxValue = Math.max(maxValue, posValue);
                });
            } else {
                _.each(data.series, function (dd) {
                    if (dd['axis' + axisType.toUpperCase() + 'Id'] !== axisId) { return; }

                    _.each(dd.dps, function (dataPoint) {
                        var value = dataPoint[axisType[0]];
                        minValue = Math.min(minValue, value);
                        maxValue = Math.max(maxValue, value);
                    });
                });
            }

            var precision = 2;
            if (format && format.date) { precision = 0; }

            if (chartModel.isXYType()) {
                precision = 2;
                var diff = (maxValue - minValue) / 4;
                if (isBubbleChart()) { diff *= 1.5; }
                maxValue += diff * 2;
                minValue -= diff;
            } else if (stacking === 'percentStacked') {
                format = {
                    exp: -2,
                    format: chartModel.docModel.numberFormatter.getParsedFormat('0%')
                };

                if (maxValue >= 0 && minValue >= 0) {
                    // scenario 1
                    minValue = givenMin || 0;
                    maxValue = givenMax || 100;
                } else if (maxValue <= 0 && minValue <= 0) {
                    // scenario 2
                    maxValue = givenMax || 0;
                    minValue = givenMin || -100;
                } else {
                    // scenario 3
                    var maxSize = Math.max(maxValue, -minValue);
                    var tmpMaxValue = 100 * (maxValue / maxSize);
                    var tmpMinValue = -100 * ((-minValue) / maxSize);

                    tmpMinValue = Math.max(-100, math.roundp(tmpMinValue - 5, 10));
                    tmpMaxValue = Math.min(100, math.roundp(tmpMaxValue + 5, 10));

                    minValue = givenMin || tmpMinValue;
                    maxValue = givenMax || tmpMaxValue;
                }
            } else {
                if (maxValue >= 0 && minValue >= 0) {
                    // scenario 1
                    minValue = givenMin || makeMin(minValue, maxValue);
                    maxValue = givenMax || makeMax(minValue, maxValue);
                } else if (maxValue <= 0 && minValue <= 0) {
                    // scenario 2
                    maxValue = givenMax || -makeMin(-maxValue, -minValue);
                    minValue = givenMin || -makeMax(-maxValue, -minValue);
                } else {
                    // scenario 3
                    var ma = maxValue;
                    var mi = minValue;
                    maxValue = givenMax || makeMax(mi, ma);
                    minValue = givenMin || -makeMax(-ma, -mi);
                }
            }

            if (!Number.isFinite(minValue) || !Number.isFinite(maxValue)) {
                setMinMax();
                return;
            }

            if (minValue === maxValue) {
                maxValue = minValue + 0.5;
                minValue -= 0.5;
            }

            if (Math.abs(maxValue - minValue) > 1e-11) {
                minValue = math.roundp(minValue, 0.01);
                maxValue = math.roundp(maxValue, 0.01);
            }

            updateMinMax(minValue, maxValue, precision, 8);
        }

        // public methods -----------------------------------------------------

        this.refreshInfo = function () {
            refreshMinMax();

            if (!dataAxis) { return; }

            var attrs = this.getMergedAttributeSet(true);

            handleLineProps(chartModel, attrs.line, dataAxis, 'line');
            handleLineProps(chartModel, attrs.line, dataAxis, 'tick');

            if (attrs.axis.label) {
                handleCharacterProps(chartModel, attrs.character, dataAxis, 'label');
            } else {
                dataAxis.labelFontColor = 'transparent';
                dataAxis.labelFontSize = 1;
            }
            dataAxis.labelPlacement = 'outside';

            gridModel.refreshInfo();
            titleModel.refreshInfo();
        };

        this.setFormat = function (newFormat) {
            format = newFormat;
        };

        this.getGrid = function () {
            return gridModel;
        };

        this.getTitle = function () {
            return titleModel;
        };

        this.getAxisType = function () {
            return axisType;
        };

        this.getAxisPos = function () {
            return axPos;
        };

        this.getCrossAxis = function () {
            return crossAx;
        };

        // operation generators -----------------------------------------------

        /**
         * Generates the undo operations needed to restore this chart axis.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the undo operations.
         *
         * @param {Position} position
         *  The position of the parent chart object in the sheet.
         */
        this.generateRestoreOperations = function (generator, position) {

            // restore this axis
            if (this.hasExplicitAttributes()) {
                var properties = { axis: axisId, axPos, crossAx, attrs: this.getExplicitAttributeSet(false) };
                if (axisType === 'z') {
                    properties.zAxis = true;
                }
                generator.generateDrawingOperation(CHANGE_CHART_AXIS, position, properties, { undo: true });
            }

            // restore the axis title, and the grid line settings
            titleModel?.generateRestoreOperations(generator, position);
            gridModel?.generateRestoreOperations(generator, position);
        };

        /**
         * Generates the operations and undo operations to update or restore
         * the formula expressions of the source link of the axis title object.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operations cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @param {Position} position
         *  The position of the parent chart object in the sheet.
         */
        this.generateUpdateTaskOperations = function (sheetCache, updateTask, position, options) {
            titleModel?.generateUpdateTaskOperations(sheetCache, updateTask, position, options);
        };

        // initialization -----------------------------------------------------

        // destructor
        this.registerDestructor(function () {
            if (dataAxis) { delete dataAxis.labelFormatter; }
        });
    }
}
