/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';
import { handleCharacterProps } from '@/io.ox/office/drawinglayer/view/chartstyleutil';
import { CHANGE_CHART_TITLE } from '@/io.ox/office/spreadsheet/utils/operations';
import { XSourceLinkedModel } from '@/io.ox/office/spreadsheet/model/drawing/xsourcelinkedmodel';

// class TitleModel ===========================================================

/**
 * A title in a chart object. Can be used as main title of the chart, or as
 * title of a chart axis.
 *
 * @param {SheetChartModel} chartModel
 *  The parent chart model containing this title object.
 *
 * @param {String} axisId
 *  The identifier of this title object. Can be null for the main title
 *  of the chart, or the id for the respective axis title.
 */
export default class TitleModel extends XSourceLinkedModel.mixin(AttributedModel) {

    constructor(chartModel, axisId, data, textKey, dataKey) {

        // base constructor
        super(
            // parameters "sheetModel" and "linkAttrQNames" for `XSourceLinkedModel` mixin
            chartModel.sheetModel, "text.link",
            // all following parameters for `AttributedModel` base class
            chartModel.docModel, undefined, { families: ["text", "character"] }
        );

        var self = this;

        // public methods -----------------------------------------------------

        this.refreshInfo = function (chartTitle) {

            var display = this.resolveText('text.link') || chartTitle;
            var textLink = this.resolveText('text.link');
            var seriesTitle = chartTitle;

            if (textLink) {
                display = textLink;
            } else if (seriesTitle) {
                display = seriesTitle;
            }

            // If the ChartTitle is an automatic title and if the chart contains more than one dataseries, hide the auto title.
            if (!axisId && !textLink && seriesTitle && chartModel.getModelData().series.length > 1) {
                display = '';
            }

            var change = data[textKey] !== display;
            data[textKey] = display;

            if (display) {
                var attrs = self.getMergedAttributeSet(true);
                handleCharacterProps(chartModel, attrs.character, data, dataKey);
            }
            // Fix for Bug 54278 - Chart - title with source link - change title name
            if (change) {
                chartModel.trigger('change:drawing', 'series');
            }
            return change;
        };

        // operation generators -----------------------------------------------

        /**
         * Generates the undo operations needed to restore this title object.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the undo operations.
         *
         * @param {Position} position
         *  The position of the parent chart object in the sheet.
         */
        this.generateRestoreOperations = function (generator, position) {
            if (this.hasExplicitAttributes()) {
                var properties = { attrs: this.getExplicitAttributeSet() };
                if (axisId !== null) {
                    properties.axis = axisId;
                }
                generator.generateDrawingOperation(CHANGE_CHART_TITLE, position, properties, { undo: true });
            }
        };

        /**
         * Generates the operations and undo operations to update or restore
         * the formula expressions of the source link of this title object.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operations cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @param {Position} position
         *  The position of the parent chart object in the sheet.
         */
        this.generateUpdateTaskOperations = function (sheetCache, updateTask, position, options) {
            // mix-in class `XSourceLinkedModel` generates the necessary change operations
            var properties = {};
            if (axisId !== null) {
                properties.axis = axisId;
            }
            this.implGenerateUpdateTaskOperations(sheetCache, updateTask, CHANGE_CHART_TITLE, position, properties, options);
        };

        // initialization -----------------------------------------------------

        // Fix for Bug 54278 - Chart - title with source link - change title name
        this.on('refresh:formulas', function () {
            chartModel.refreshChartDebounced();
        });
    }
}
