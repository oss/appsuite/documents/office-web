/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { math } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { BaseObject } from '@/io.ox/office/tk/objects';
import { getColLabel, getRowLabel, getSeriesLabel } from '@/io.ox/office/spreadsheet/view/labels';

// class ChartFormatter ===================================================

export default class ChartFormatter extends BaseObject {

    constructor(docModel, chartModel) {

        // base constructor
        super();

        // private methods ----------------------------------------------------

        function isAnyDateTime(format, value) {
            if (format && format.isAnyDateTime() && Number.isFinite(value)) {
                return true;
            }
        }

        function newInfoHolder(type, label) {
            return {
                max: -Infinity,
                date: false,
                type,
                label,
                unclean: false,
                exp: 0
            };
        }

        function handleData(info, source, dataPoint, format, nullAsZero) {
            var realNr = source.value;
            var display = source.display;

            if (!format && source.format) {
                format = source.format;
                if (isAnyDateTime(format, realNr)) {
                    info.date = format;
                }
                // Store the first format for the axis values
                if (!info.format) {
                    info.format = format;
                }
            }

            if (!Number.isFinite(realNr) && info.type === 'y' && nullAsZero) {
                realNr = 0;
            }

            var label = null;
            var res = null;
            if (info.date) {
                res = realNr;
                info.max = Math.max(info.max, Math.abs(realNr));
                label = display;
            } else if (!Number.isFinite(realNr)) {
                info.unclean = true;
                res = nullAsZero ? 0 : undefined;
                label = display;
            } else if (format) {
                // Do not use info.format
                info.max = Math.max(info.max, Math.abs(realNr));
                res = realNr;
                label = docModel.numberFormatter.formatValue(format, realNr).text;
            } else {
                info.max = Math.max(info.max, Math.abs(realNr));
                res = realNr;
                label = docModel.numberFormatter.autoFormatNumber(realNr, { stdLen: 12 }).text;
            }

            dataPoint[info.label] = label;
            dataPoint[info.type] = res;
        }

        function calcMaxExp(info) {
            if (Number.isFinite(info.max) && !Number.isFinite(info.exp)) {
                var parts = math.splitNumber(info.max);
                if (Math.abs(parts.expn) < 6) {
                    info.exp = 0;
                } else {
                    info.exp = parts.expn;
                }
            } else {
                info.exp = 0;
            }
        }

        function handleMantissa(info, dataPoint) {
            if (info.exp) {
                dataPoint[info.type] /= Math.pow(10, info.exp);
            }
        }

        function handleLabelLength(info, dataPoint) {
            var label = dataPoint[info.label];
            if (label && label.length > 30) {
                dataPoint[info.label] = label.substring(0, 27) + '...';
            }
        }

        /**
         * Set the dataseries axis attributes for the canvas data object, if the
         * axes contains the axis for the DataSeries.
         *
         * @param {Array} axes
         * @param {String} axisType the type of the axis 'X' or 'Y'
         * @param {Object} dataSeries the DataSeries to the axis attributes
         * @returns {Boolean} If the Attributes are set for the DataSeries.
         */
        function setDataSeriesAxis(axes, axisType, dataSeries) {
            var isSet = false;
            axisType = axisType.toUpperCase();
            var axPos = axisType === 'X' ? 'b' : 'l';
            var axis;
            for (var i = 0; i < axes.length; i++) {
                axis = axes[i];
                if (axis.axisId === dataSeries['axis' + axisType + 'Id']) {
                    if (i > 0) {
                        dataSeries['axis' + axisType + 'Index'] = i;
                    }
                    dataSeries['axis' + axisType + 'Type'] = axis.axPos === axPos ? 'primary' : 'secondary';
                    break;
                }
            }

            return isSet;
        }

        // public methods -----------------------------------------------------

        this.format = function () {
            var data = chartModel.getModelData();

            var form = null;
            var xyChart = false;
            var bubbleChart = false;
            var nullAsZero = false;
            var attrs = chartModel.getMergedAttributeSet(true);
            var stacking = attrs.chart.stacking;
            data.linearX = false;

            if (data.series.length) {
                form = data.series[0].model.getExplicitAttributeSet(true).series.format;
                if (form) {
                    form = this.parseFormatCode(form);
                }
                bubbleChart = chartModel.isBubbleChart();
                xyChart = chartModel.isXYType();
                nullAsZero = bubbleChart || chartModel.isPieOrDonut() || /^(column|bar)/.test(chartModel.getChartType());
            }

            _.each(data.series, function (dataSeries, seriesIndex) {
                dataSeries.seriesIndex = seriesIndex;

                var before = data.series[seriesIndex - 1];

                if (before && before.axisXType === dataSeries.axisXType) {
                    dataSeries.xInfo = before.xInfo;
                } else {
                    dataSeries.xInfo = newInfoHolder('x', 'label');
                }
                if (before && before.axisYType === dataSeries.axisYType) {
                    dataSeries.yInfo = before.yInfo;
                } else {
                    dataSeries.yInfo = newInfoHolder('y', 'name');
                }

                var x = dataSeries.xInfo;
                var y = dataSeries.yInfo;

                if (dataSeries.nameHolder) {
                    dataSeries.name = dataSeries.nameHolder.display;
                } else if (docModel.getApp().isODF()) {
                    var fallback = true;
                    try {
                        var valueRanges = dataSeries.model.resolveRanges('series.bubbles');
                        if (!valueRanges || !valueRanges.length) {
                            valueRanges = dataSeries.model.resolveRanges('series.values');
                        }
                        if (valueRanges && valueRanges.length) {
                            var valueRange = valueRanges[0];
                            if (Math.abs(valueRange.a1.c - valueRange.a2.c) > 0) {
                                dataSeries.name = getRowLabel(valueRange.a1.r, { prefix: true });
                                fallback = false;
                            } else if (Math.abs(valueRange.a1.r - valueRange.a2.r) > 0) {
                                dataSeries.name = getColLabel(valueRange.a1.c, { prefix: true });
                                fallback = false;
                            } else if (dataSeries.dps.length) {
                                dataSeries.name = dataSeries.dps[0].name;
                                fallback = false;
                            }
                        }
                    } catch (e) {
                        globalLogger.warn('chart error while formatting', e);
                    }

                    if (fallback) {
                        dataSeries.name = getSeriesLabel(seriesIndex);
                    }
                } else {
                    dataSeries.name = getSeriesLabel(seriesIndex);
                }

                var dataPoints = dataSeries.dps;

                var onlyNumberNamedSource = false;

                if (xyChart) {
                    onlyNumberNamedSource = !(_.find(dataPoints, function (dataPoint) {
                        return !dataPoint.nameSource || dataPoint.nameSource.value === null || !Number.isFinite(dataPoint.nameSource.value);
                    }));
                }

                var n = 0;
                _.each(dataPoints, function (dataPoint) {
                    dataPoint.x = undefined;
                    dataPoint.y = undefined;
                    dataPoint.z = undefined;

                    if (dataPoint.sizeSource) {
                        dataPoint.z = Math.abs(dataPoint.sizeSource.value);

                        // fix for funny bug in canvasjs
                        // Don't know what this funny bug is or when it occur. But it overrides the markerBorderColor of the bubbles in the chartModel
                        // dataPoint.markerBorderColor = 'transparent';
                        // dataPoint.markerBorderThickness = 1;

                    } else {
                        dataPoint.z = bubbleChart ? 1 : undefined;
                    }

                    if (dataPoint.visible && (!bubbleChart || !Number.isNaN(dataPoint.z))) {
                        if (dataPoint.valueSource === null) {
                            // fix for charts without values e.g. bubblechart with one column
                            dataPoint[y.type] = n + 1;
                        } else {
                            handleData(y, dataPoint.valueSource, dataPoint, form, nullAsZero);
                        }

                        if (xyChart && dataPoint.nameSource && onlyNumberNamedSource) {
                            handleData(x, dataPoint.nameSource, dataPoint, null, nullAsZero);
                        } else {
                            dataPoint[x.type] = n + 1;
                            data.linearX = true;
                        }

                        //ugly part against double formatting!!!
                        if (dataPoint.nameSource) {
                            dataPoint[x.label] = dataPoint.nameSource.display;
                        } else {
                            dataPoint[x.label] = String(dataPoint[x.type]);
                        }

                        if (dataPoint.label) { dataPoint.legendLabel = dataPoint.label; }

                        handleLabelLength(x, dataPoint);
                        handleLabelLength(y, dataPoint);

                        dataPoint.legendText = dataPoint.legendLabel;

                        if (dataPoint.x === null) {
                            dataPoint.x = undefined;
                        }
                        if (!Number.isFinite(dataPoint.y)) {
                            dataPoint.y = undefined;
                        }
                        if (dataPoint.z === null) {
                            dataPoint.z = undefined;
                        }
                        n++;
                    }
                });

                if (xyChart && x.unclean) {
                    _.each(dataPoints, function (dataPoint, n) {
                        dataPoint.x = n + 1;
                    });
                }

                if (xyChart) {
                    calcMaxExp(x);
                }
                calcMaxExp(y);
                var xAxisType = chartModel.getAxisTypeForDrawing('x');
                var yAxisType = chartModel.getAxisTypeForDrawing('y');
                if (!setDataSeriesAxis(data.axisX, xAxisType, dataSeries)) {
                    setDataSeriesAxis(data.axisX2, xAxisType, dataSeries);
                }
                if (!setDataSeriesAxis(data.axisY, yAxisType, dataSeries)) {
                    setDataSeriesAxis(data.axisY2, yAxisType, dataSeries);
                }

                var xAxisModel = chartModel.getAxisModel(dataSeries.axisXId);
                if (xAxisModel) {
                    xAxisModel.setFormat(xAxisType === 'x' ? x : y);
                }
                var yAxisModel = chartModel.getAxisModel(dataSeries.axisYId);
                if (yAxisModel) {
                    yAxisModel.setFormat(yAxisType === 'y' ? y : x);
                }
            });

            var chartType = chartModel.getChartType();
            var mustReverseDataPoints = false;
            if (docModel.getApp().isODF()) {
                mustReverseDataPoints = /^(pie|donut)/.test(chartType);
            }

            _.each(data.series, function (dataSeries) {
                var x = dataSeries.xInfo;
                var y = dataSeries.yInfo;
                _.each(dataSeries.dps, function (dataPoint) {
                    if (!xyChart && (stacking === 'clustered' || stacking === 'standard')) {
                        var thresholdY = 0.005;
                        //workaround that too small values stay visible
                        var rel = dataPoint.y / y.max;
                        if (Math.abs(rel) < thresholdY) {
                            if (rel < 0) {
                                dataPoint.y = -thresholdY * y.max;
                            } else {
                                dataPoint.y = thresholdY * y.max;
                            }
                        }
                    }

                    handleMantissa(y, dataPoint);
                    handleMantissa(x, dataPoint);
                });

                dataSeries.dataPoints = dataSeries.dps.slice(0);
                if (dataSeries.type === 'bubble') {
                    _.each(dataSeries.dataPoints, function (dataPoint) {
                        if (Number.isNaN(dataPoint.z)) {
                            delete dataPoint.x;
                            delete dataPoint.y;
                            delete dataPoint.z;
                        }
                    });
                }
                if (mustReverseDataPoints) {
                    dataSeries.dataPoints.reverse();
                }
            });

            data.data = data.series.slice(0);
            var mustReverse = false;
            chartModel.getModelData().legend.reversed = false;

            if (stacking === 'clustered' || stacking === 'standard') {
                if (docModel.getApp().isOOXML()) {
                    mustReverse = /^(bar)/.test(chartType);
                } else {
                    chartModel.getModelData().legend.reversed = mustReverseDataPoints || /^(bar)/.test(chartModel.getChartType());
                    mustReverse = /^(bar|area)/.test(chartType);
                }
            }
            if (mustReverse) {
                data.data.reverse();
            }
        };

        this.update = function (seriesIndex, values, titles, names, bubbles) {
            var data = chartModel.getModelData();

            var dataSeries = data.series[seriesIndex];
            var isBubble = dataSeries.type.indexOf('bubble') >= 0;
            if (!dataSeries) {
                //workaround for Bug 46928
                globalLogger.warn('no dataSeries found for ' + seriesIndex + ' title: ' + titles + ' names: ' + names);
                return;
            }

            // build series title
            dataSeries.nameHolder = titles ? titles[0] : null;

            function isValueNotNull(source) {
                return source && source.value !== null;
            }

            function hasValues(sources) {
                var count = isValueNotNull(dataSeries.nameHolder) ? 1 : 0;
                sources.forEach(function (source) {
                    if (isValueNotNull(source)) {
                        count++;
                    }
                });
                return count >= 1;
            }

            dataSeries.dps = [];
            _.each(values, function (valueCell, n) {
                var dataPoint = {};
                var nameSource = names[n];
                var sizeSource = bubbles[n];
                dataSeries.dps.push(dataPoint);
                if (!isBubble || (data.containsSeriesValues && isValueNotNull(valueCell) && (isValueNotNull(sizeSource) || sizeSource === undefined)) || hasValues([valueCell, nameSource, sizeSource])) {

                    dataPoint.nameSource = nameSource;    //x
                    dataPoint.valueSource = valueCell;  //y
                    dataPoint.sizeSource = sizeSource;  //z
                    dataPoint.color = null;
                    dataPoint.markerColor = null;
                    data.containsSeriesValues = true;
                    dataPoint.visible = true;
                } else {
                    dataPoint.nameSource = null;    //x
                    dataPoint.valueSource = null;  //y
                    dataPoint.sizeSource = null;  //z
                    dataPoint.color = null;
                    dataPoint.markerColor = null;
                    dataPoint.visible = false;
                }
            });
        };

        this.parseFormatCode = function (formatCode) {
            return docModel.numberFormatter.getParsedFormat(formatCode);
        };
    }
}
