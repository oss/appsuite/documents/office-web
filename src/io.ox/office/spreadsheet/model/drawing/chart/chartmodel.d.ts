/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { ChartModel } from "@/io.ox/office/drawinglayer/model/chartmodel";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { XSheetDrawingModel } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// class SheetChartModel ======================================================

export interface SheetChartModel extends ChartModel<SpreadsheetModel, XSheetDrawingModel>, XSheetDrawingModel { }

export class SheetChartModel extends ChartModel<SpreadsheetModel, XSheetDrawingModel> implements XSheetDrawingModel {
    resolveRanges(): Range3DArray;
    applyInsertSeriesOperation(context: SheetOperationContext): void;
    applyDeleteSeriesOperation(context: SheetOperationContext): void;
    applyChangeSeriesOperation(context: SheetOperationContext): void;
    applyDeleteAxisOperation(context: SheetOperationContext): void;
    applyChangeAxisOperation(context: SheetOperationContext): void;
    applyChangeGridOperation(context: SheetOperationContext): void;
    applyChangeTitleOperation(context: SheetOperationContext): void;
    applyChangeLegendOperation(context: SheetOperationContext): void;
}
