/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { BACKGROUND_TRANSFORMATIONS, getColorOfPattern, getColorSet, getStyleSet, isAutoColor,
    isAutoShape, toColorSetIndex, toStyleSetIndex } from '@/io.ox/office/drawinglayer/view/chartstyleutil';

// constants ==================================================================

var STYLESET = getStyleSet();

var COLORSET = getColorSet();

var DUMMY_SHAPE = { type: 'solid', color: { ...Color.BLUE, width: 36 } };

// class ManualColorHandler ===================================================

export default function ManualColorHandler(chartModel, data) {

    // the document model
    var docModel = chartModel.docModel;

    // private methods ----------------------------------------------------

    /*
        * Apply tint/shade depending on the cycle index. The colors of leading
        * series are darkened (color shade), the colors of trailing series are
        * lightened (color tint). Shade/tint is applied in an exclusive range of
        * -70% to 70%.
        *
        * Example 1: 3 data series using single-color shading with accent color 1
        * (e.g. automatic chart style #3). Shade/tint is applied per series.
        * Shade/tint changes in steps of 140%/(<series_count+1) = 140%/4 = 35%,
        * starting at -70%:
        *  Step 1: -70% -> Not used.
        *  Step 2: -35% -> Series 1 has 35% shade of accent color 1.
        *  Step 3:   0% -> Series 2 has pure accent color 1.
        *  Step 4:  35% -> Series 3 has 35% tint of accent color 1.
        *  Step 5:  70% -> Not used.
        *
        * Example 2: 20 data series using accent color pattern (e.g. automatic
        * chart style #2). Each color cycle has a size of 6 series (accent colors
        * 1 to 6). Shade/tint is applied per color cycle.
        *  Cycle #1: Series 1...6 are based on accent colors 1 to 6.
        *  Cycle #2: Series 7...12 are based on accent colors 1 to 6.
        *  Cycle #3: Series 13...18 are based on accent colors 1 to 6.
        *  Cycle #4: Series 19...20 are based on accent colors 1 to 2.
        * Shade/tint changes in steps of 140%/(cycle_count+1) = 140%/5 = 28%,
        * starting at -70%:
        *  Step 1: -70% -> Not used.
        *  Step 2: -42% -> Cycle #1 has 42% shade of accent colors 1...6
        *  Step 3: -14% -> Cycle #2 has 14% shade of accent colors 1...6
        *  Step 4:  14% -> Cycle #3 has 14% tint of accent colors 1...6
        *  Step 5:  42% -> Cycle #4 has 42% tint of accent colors 1...6
        *  Step 6:  70% -> Not used.
        *
        * 843 sal_Int32 nPhClr = maColorPattern[ static_cast< size_t >( nSeriesIdx % maColorPattern.size() ) ];
        * 845 size_t nCycleIdx = static_cast< size_t >( nSeriesIdx / maColorPattern.size() );
        * 846 size_t nMaxCycleIdx = static_cast< size_t >( mrData.mnMaxSeriesIdx / maColorPattern.size() );
        * 847 double fShadeTint = static_cast< double >( nCycleIdx + 1 ) / (nMaxCycleIdx + 2) * 1.4 - 0.7;
        * 848 if( fShadeTint != 0.0 )
        * 849 {
        * 850     Color aColor;
        * 851     aColor.setSrgbClr( nPhClr );
        * 852     aColor.addChartTintTransformation( fShadeTint );
        * 853     nPhClr = aColor.getColor( mrData.mrFilter.getGraphicHelper() );
        * 854 }
        * 856 return nPhClr;
        */

    function getColor(index, type, count) {
        var attributes = chartModel.getMergedAttributeSet(true);

        var colors = attributes.chart.chartColors;

        var result = null;

        if (colors) {

            result = getColorOfPattern(colors.meth, 'group', index, colors.schemeClr, count, docModel);

        } else if (attributes.chart.chartStyleId) {
            var colorSet = COLORSET[toColorSetIndex(attributes.chart.chartStyleId)];

            var res = getColorOfPattern('cycle', colorSet.type, index, colorSet.colors, count, docModel);
            result = {
                color: res,
                type
            };
        } else {
            result = DUMMY_SHAPE;
        }
        return result;
    }

    function getStyleSet(dataSeries, color) {
        var attributes = chartModel.getMergedAttributeSet(true);

        var result = color;
        var styleSet = null;

        if (attributes.chart.chartStyleId) {
            styleSet = STYLESET[toStyleSetIndex(attributes.chart.chartStyleId)];
            dataSeries.bevelEnabled = styleSet.bevelEnabled;
        }

        if (dataSeries.seriesIndex === 0) {
            data.cssBackgroundColor = null;
            if (attributes.chart.chartStyleId && result) {

                styleSet = STYLESET[toStyleSetIndex(attributes.chart.chartStyleId)];

                var bgColor = styleSet.bg;
                if (!bgColor) {
                    bgColor = transformOpColor(result.color, ...BACKGROUND_TRANSFORMATIONS);
                }

                data.cssBackgroundColor = docModel.getCssColor(bgColor, 'fill');
            }
        }
    }

    function getVaryColor() {
        if (chartModel.isVaryColorEnabled()) {
            var attributes = chartModel.getMergedAttributeSet(true);
            return attributes.chart.varyColors;
        }
        return false;
    }

    function applyColor(target, shape, targetKey) {
        var cssColor;

        if (!shape || shape.type === 'none') {
            cssColor = 'transparent';
        } else {
            var color = shape.color;
            if (shape.gradient && shape.gradient.colorStops && (!color || isAutoColor(color))) {
                color = shape.gradient.colorStops[0].color;
            }
            cssColor = docModel.getCssColor(color, 'fill');
        }

        target[targetKey] = cssColor;
    }

    // public functions ---------------------------------------------------

    this.handleColor = function (sourceKey, dataSeries, targetKey, count) {

        var attrs = dataSeries.model.getMergedAttributeSet(true);
        var shape = attrs[sourceKey];
        if (!shape) {
            return false;
        }
        var index = dataSeries.seriesIndex;

        _.each(dataSeries.dps, function (dataPoint) {
            dataPoint[targetKey] = null;
        });
        dataSeries[targetKey] = null;

        if (isAutoShape(shape)) {
            if (getVaryColor()) {
                dataSeries.color = 'transparent';
                _(dataSeries.dps).each(function (dataPoint, pi) {
                    applyColor(dataPoint, getColor(pi, shape.type, dataSeries.dps.length), targetKey);
                });
            } else {
                shape = getColor(index, shape.type, count);
            }
        }

        if (!isAutoShape(shape)) {
            applyColor(dataSeries, shape, targetKey);
        }

        getStyleSet(dataSeries, shape);

        var chartAttrs = chartModel.getMergedAttributeSet(true);
        // Bug 53441 - Import chart: => some charts display a black bg color
        if (index === 0 && chartAttrs.fill) {
            var fill = chartAttrs.fill;
            if (isAutoShape(fill) && chartAttrs.chart.chartStyleId) {
                data.cssBackgroundColor = '#' + this.getBackgroundColor(chartAttrs.chart.chartStyleId).fallbackValue;
            } else {
                applyColor(data, fill, 'cssBackgroundColor');
            }
        }

        function getLinearDataPointAttrs(attrsArray, dataPoints) {
            if (dataPoints === 0) {
                return;
            }
            const array = new Array(dataPoints);
            let nextIndex = 0;
            for (const attrs of attrsArray) {
                if (_.isNumber(attrs.index)) {
                    nextIndex = attrs.index;
                }
                const repeated = attrs.repeated || 1;
                for (let i = 0; i < repeated; i++) {
                    if (nextIndex >= dataPoints) {
                        return array;
                    }
                    array[nextIndex++] = attrs;
                }
            }
            return array;
        }

        const dataPointsAttrs = getLinearDataPointAttrs(attrs.series.dataPoints, dataSeries.dps.length);
        if (dataPointsAttrs && dataPointsAttrs.length) {
            _.each(dataSeries.dps, function (dataPoint, pi) {
                if (pi < dataPointsAttrs.length) {
                    var dataPointAttrs = dataPointsAttrs[pi];
                    if (!_.isEmpty(dataPointAttrs)) {
                        if (dataPointAttrs[sourceKey] && dataPointAttrs[sourceKey].color && !dataPointAttrs[sourceKey].gradient && isAutoColor(dataPointAttrs[sourceKey].color)) {
                            applyColor(dataPoint, getColor(index, dataPointAttrs[sourceKey].type, count), targetKey);
                        } else {
                            applyColor(dataPoint, dataPointAttrs[sourceKey], targetKey);
                        }
                    }
                }
            });
        }

        return true;
    };

    this.getBackgroundColor = function (chartStyleId) {
        var styleSet = STYLESET[toStyleSetIndex(chartStyleId)];

        var bgColor;
        if (styleSet?.bg) {
            bgColor = { ...styleSet.bg };
        } else {
            var colorSet = COLORSET[toColorSetIndex(chartStyleId)];
            if (colorSet) {
                var color = getColorOfPattern('cycle', 'single', 1, colorSet.colors, 3, docModel);
                bgColor = transformOpColor(color, ...BACKGROUND_TRANSFORMATIONS);
            } else {
                bgColor = { ...Color.AUTO };
            }
        }
        if (!bgColor.fallbackValue) {
            bgColor.fallbackValue = docModel.parseAndResolveColor(bgColor, 'fill').hex;
        }
        return bgColor;
    };

    // initialization -----------------------------------------------------

    data.colorSet = null;
}
