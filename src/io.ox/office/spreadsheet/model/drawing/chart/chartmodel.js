/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import CanvasJS from '@canvasjs/charts';

import _ from '$/underscore';
import $ from '$/jquery';

import { LOCALE_DATA } from '@/io.ox/office/tk/locale';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { escapeHTML, getBooleanOption, getStringOption } from '@/io.ox/office/tk/utils';

import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';

import { ChartModel } from '@/io.ox/office/drawinglayer/model/chartmodel';
import { BACKGROUND_TRANSFORMATIONS, getColorOfPattern, getColorSet, getStyleSet,
    handleCharacterProps, isAutoColor, isAutoShape, toChartStyleId, toColorSetIndex,
    toStyleSetIndex } from '@/io.ox/office/drawinglayer/view/chartstyleutil';

import { DELETE_CHART_AXIS } from '@/io.ox/office/spreadsheet/utils/operations';
import { Address, RangeArray, Range3DArray } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { XSheetDrawingModel } from '@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel';
import DataSeriesModel from '@/io.ox/office/spreadsheet/model/drawing/chart/dataseriesmodel';
import TitleModel from '@/io.ox/office/spreadsheet/model/drawing/chart/titlemodel';
import AxisModel from '@/io.ox/office/spreadsheet/model/drawing/chart/axismodel';
import LegendModel from '@/io.ox/office/spreadsheet/model/drawing/chart/legendmodel';
import ChartFormatter from '@/io.ox/office/spreadsheet/model/drawing/chart/chartformatter';
import ManualColorHandler from '@/io.ox/office/spreadsheet/model/drawing/chart/manualcolorchart';

// constants ==================================================================

var SUPPORTED_COMBINED = /^(column|area|line|scatter)/;

var SUPPORTED = /^(column|bar|area|line|scatter|bubble|pie|donut)/;

var VALID = /^(column|bar|area|line|scatter|bubble|pie|donut|sunburst|ofPie)/;

var TYPES = {
    'column standard': 'column',
    'column clustered': 'column',
    'column stacked': 'stackedColumn',
    'column percentStacked': 'stackedColumn100',
    'bar standard': 'bar',
    'bar clustered': 'bar',
    'bar stacked': 'stackedBar',
    'bar percentStacked': 'stackedBar100',
    'area standard': 'area',
    'area stacked': 'stackedArea',
    'area percentStacked': 'stackedArea100',
    'line standard': 'line',
    'line clustered': 'line',
    'line stacked': 'line',
    'line percentStacked': 'spline',
    'line standard curved': 'spline',
    'line clustered curved': 'spline',
    'line stacked curved': 'spline',
    'line percentStacked curved': 'spline',
    'scatter standard': 'line',
    'scatter standard curved': 'spline',
    'bubble standard': 'bubble',
    'pie standard': 'pie',
    'donut standard': 'doughnut',

    'sunburst standard': 'doughnut',
    'ofPie standard': 'pie'
};

var MARKER_TYPES = {
    circle: 'circle',
    dot: 'circle',
    square: 'square',
    triangle: 'triangle',
    x: 'cross',
    none: 'none'
};

var MARKER_LIST = ['circle', 'square', 'triangle', 'cross'];

function parseChartType(type, stacking, curved) {
    type = type.replace('2d', '').replace('3d', '');

    if (stacking.length) {
        type = type + ' ' + stacking;
    }
    if (curved && /(line|scatter)/.test(type)) {
        type += ' curved';
    }
    return type;
}

function parseCJSType(type, stacking, curved) {
    var t = parseChartType(type, stacking, curved);
    var res = TYPES[t];

    if (!res) {
        globalLogger.warn('no correct charttype found', t);
        if (!type) {
            res = 'column';
        } else if (!type.indexOf('radar') || !type.indexOf('boxWhisker')) {
            res = 'line';
        } else {
            res = 'column';
        }
    }

    if (res === 'line' && curved === true) {
        res = 'spline';
    }
    return res;
}

function isLineChart(seriesType) {
    if (seriesType.indexOf('line') >= 0 || seriesType.indexOf('scatter') >= 0) {
        return true;
    }
    return false;
}

// class SheetChartModel ======================================================

/**
 * A class that implements a chart model to be inserted into a sheet of a
 * spreadsheet document.
 *
 * @param {SheetModel} sheetModel
 *  The model instance of the sheet that contains this chart object.
 *
 * @param {DrawingCollection} parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param {Object} [initAttrSet]
 *  Initial formatting attribute set for this chart model.
 */
export class SheetChartModel extends XSheetDrawingModel.mixin(ChartModel) {

    constructor(sheetModel, parentCollection, initAttrSet, cloneData) {

        // base constructor
        super(sheetModel, parentCollection, initAttrSet);

        var self = this;

        var docModel = sheetModel.docModel;

        var data = {
            animationEnabled: false,
            culture:  'en',
            bg: 'white',
            backgroundColor: 'transparent',
            axisX: [],
            axisY: [],
            axisX2: [],
            axisY2: [],
            axisZ: [],
            creditHref: '',
            creditText: '',
            title: { text: '' },
            legend: {},
            data: [],
            series: [],
            toolTip: {
                backgroundColor: 'black',
                borderColor: 'black',
                fontColor: 'white',
                cornerRadius: 3,
                borderThickness: 4,
                fontStyle: 'normal',
                contentFormatter(e) {
                    var content = ' ';

                    for (var i = 0; i < e.entries.length; i++) {
                        if (e.entries[i].dataPoint.label) {
                            content += e.entries[i].dataPoint.label;
                        } else {
                            content += e.entries[i].dataPoint.x;
                        }
                        content += ': ';
                        if (e.entries[i].dataPoint.name) {
                            content += e.entries[i].dataPoint.name;
                        } else {
                            content += e.entries[i].dataPoint.y;
                        }
                        if (e.entries[i].dataSeries.type.indexOf('bubble') !== -1) {
                            var zValue = e.entries[i].dataPoint.z;
                            if (Number.isFinite(zValue)) {
                                zValue = docModel.numberFormatter.autoFormatNumber(zValue, { stdLen: 12 }).text;
                            }
                            content += ' (';
                            content += zValue;
                            content += ')';
                        }
                    }

                    return escapeHTML(content);
                }
            }
        };

        var indexLabelAttrs = {
            fontSize: 10,
            color: Color.AUTO,
            fontName: 'Arial'
        };

        var colorHandler = null;
        var chartFormatter = null;

        // client API methods -------------------------------------------------

        /**
         * this is only called by the Drawingframe to initialize the REAL CanvasJS-chart
         */
        this.getModelData = function () {
            return data;
        };

        var axisModelMap = {};

        /**
         * If the AxisModel not exist create it and add it to the axisModelMap.
         *
         * @param {Number} axisId Id of the axis
         * @param {String} axPos Position of the axis 't', 'r', 'b' or 'l'
         * @param {Number} crossAx the axis Id of the cross axis
         * @param {boolean} zAxis true if is a zAxis
         * @param {Object} attributeSet the attribute set of the axis.
         * @returns {AxisModel} The new AxisModel or the existing AxisModel.
         */
        function addAxis(axisId, axPos, crossAx, zAxis, attributeSet) {
            var axisModel = axisModelMap[axisId];
            if (!axisModel) {
                var axisType = zAxis ? 'z' : null;

                if (axisType === null) {
                    if (axPos === 't') {
                        axisType = 'x2';
                    } else if (axPos === 'r') {
                        axisType = 'y2';
                    } else if (axPos === 'b') {
                        axisType = 'x';
                    } else if (axPos === 'l') {
                        axisType = 'y';
                    }
                }

                if (axisType !== null) {
                    var isSecondarzAxis = axisType.length > 1;
                    // if it is a bar chart the axis type will be swapped 'x' to 'y' and 'y' to 'x'
                    axisType = self.getAxisTypeForDrawing(axisType[0]);
                    if (isSecondarzAxis) {
                        axisType += '2';
                    }
                    axisModelMap[axisId] = axisModel = new AxisModel(self, axisType, axisId, axPos, crossAx);
                }
            }

            if (axisModel) {
                axisModel.setAttributes(attributeSet);
            }

            return axisModel;
        }

        var mainTitleModel = this.member(new TitleModel(this, null, data.title, 'text'));

        var legendModel = this.member(new LegendModel(this, {}, data.legend));

        // client API methods -------------------------------------------------

        /**
         * Returns a clone of this chart model for the specified sheet.
         */
        this.clientCloneModel = function (targetModel, targetCollection) {
            // pass private clone data as hidden argument to the constructor
            return new SheetChartModel(targetModel, targetCollection, this.getExplicitAttributeSet(true), this.getCloneData());
        };

        /**
         * Generates additional undo operations needed to restore the contents
         * of this chart object, after it has been restored with an initial
         * 'insertDrawing' operation.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operations cache to be filled with the undo operations.
         *
         * @param {Position} position
         *  The position of this chart object in the sheet.
         */
        this.clientGenerateRestoreOperations = function (sheetCache, position) {

            // restore all data series of the chart
            data.series.forEach(function (seriesData, index) {
                seriesData.model.generateRestoreOperations(sheetCache, position, index);
            });

            // restore all axes of the chart
            _.each(axisModelMap, function (axisModel, axisId) {
                // workaround for Bug 46966
                if (axisId === 'z') { return; }

                axisModel.generateRestoreOperations(sheetCache, position);
            });

            // restore the main title and legend
            mainTitleModel.generateRestoreOperations(sheetCache, position);
            legendModel.generateRestoreOperations(sheetCache, position);
        };

        /**
         * Generates the operations and undo operations to update or restore
         * the formula expressions of the source links in this chart object.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operations cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @param {Position} position
         *  The position of this chart object in the sheet.
         */
        this.clientProcessUpdateTask = function (sheetCache, updateTask, position, options) {

            // generate the change operations for the source links in all data series
            data.series.forEach(function (seriesData, index) {
                seriesData.model.generateUpdateTaskOperations(sheetCache, updateTask, position, index, options);
            });

            // update the source links of the axis titles
            _.forEach(axisModelMap, function (axisModel) {
                axisModel.generateUpdateTaskOperations(sheetCache, updateTask, position, options);
            });

            // update the source link of the main title
            mainTitleModel.generateUpdateTaskOperations(sheetCache, updateTask, position, options);
        };

        // private methods ----------------------------------------------------

        /**
         * Debounced version of the public method SheetChartModel.refresh()
         * with increased delay time.
         */
        var refreshChartDebounced = this.debounce(innerRefresh, { delay: 500 });

        /**
         * Marks all data series with the "dirty" flag that overlap with the
         * passed cell range addresses.
         *
         * @param {Range3DArray|Range3D} ranges3d
         *  The cell range addresses to be checked.
         *
         * @returns {Boolean}
         *  Whether at least one data series is dirty.
         */
        function checkForDirtyDataSeries(ranges3d) {
            var changed = false;
            data.series.forEach(function (seriesData) {
                if (seriesData.needUpdate || seriesData.model.rangesOverlap(ranges3d)) {
                    changed = seriesData.needUpdate = true;
                }
            });
            return changed;
        }

        /**
         * Updates the visibility of data points, after columns or rows have
         * been shown or hidden in a sheet.
         */
        function updateColRowVisibility(sheet, intervals, columns) {

            // create 3D cell ranges as expected by `XSourceLinkedModel#rangesOverlap`
            var ranges3d = Range3DArray.fromRanges(docModel.addressFactory.getFullRangeList(intervals, columns), sheet);

            // check if the column/row interval overlaps with the source ranges of the data series
            if (checkForDirtyDataSeries(ranges3d) || mainTitleModel.rangesOverlap(ranges3d)) {
                refreshChartDebounced();
            }
        }

        /**
         * Updates the data series, after cell formatting has been changed in a
         * sheet (especially needed for updating axes and data point labels
         * after changing number formats).
         */
        function updateChangedCellStyles(sheet, ranges) {

            // create 3D cell ranges as expected by `XSourceLinkedModel#rangesOverlap`
            var ranges3d = Range3DArray.fromRanges(ranges, sheet);

            // update the chart, if the style ranges overlap with the source ranges of any data series
            if (checkForDirtyDataSeries(ranges3d)) {
                refreshChartDebounced();
            }
        }

        function isXYType() {
            var chartType = self.getChartType();
            return chartType.indexOf('bubble') === 0 || chartType.indexOf('scatter') === 0;
        }

        function isBubbleChart() {
            var chartType = self.getChartType();
            return chartType.indexOf('bubble') === 0;
        }

        function hasOnlyLineColor(attrs) {
            return getStringOption(attrs.fill, 'type', 'none') === 'none' && getStringOption(attrs.line, 'type', 'none') !== 'none';
        }

        function getDataLabelCanvasJsPosition(position) {
            var canvasJsPos = null;
            if (position) {
                switch (position) {
                    case 'lba':
                    case 'lbb':
                    case 'lbl':
                    case 'lbr':
                    case 'lbo':
                        canvasJsPos = 'outside';
                        break;
                    case 'lbc':
                    case 'lbi':
                    case 'lbib':
                    case 'lbbf':
                        canvasJsPos = 'inside';
                        break;
                }
            }
            return canvasJsPos;
        }

        function updateFormatting(subType) {
            var chartAttrs = self.getMergedAttributeSet(true);
            var pieDonut = self.isPieOrDonut();
            var isArea = self.isArea();
            var chartTitle = null;
            data.series.forEach(function (seriesData, seriesIndex) {
                var attrs = seriesData.model.getMergedAttributeSet(true);
                // for ods the curved attribute is on the chart for ooxml the attribute is on the series
                seriesData.type = parseCJSType(attrs.series.type, chartAttrs.chart.stacking, chartAttrs.chart.curved || attrs.series.curved);
                if (data.series.length === 1) {
                    chartTitle = seriesData.model.resolveText('series.title');
                }
                if (seriesData.needUpdate) {
                    return;
                }
                var lineChart = isLineChart(attrs.series.type);
                if (subType === 'series') {
                    var onlyLineColor = hasOnlyLineColor(attrs);
                    // If it is a line 3d chart not set the marker color
                    if (lineChart && attrs.series.chart3d) {
                        if (onlyLineColor) {
                            if (docModel.getApp().isOOXML()) {
                                colorHandler.handleColor('line', seriesData, 'lineColor', data.series.length);
                            }
                            colorHandler.handleColor('line', seriesData, 'color', data.series.length);
                        } else {
                            if (docModel.getApp().isOOXML()) {
                                colorHandler.handleColor('fill', seriesData, 'lineColor', data.series.length);
                            } else {
                                colorHandler.handleColor('fill', seriesData, 'color', data.series.length);
                            }
                        }
                    } else {
                        colorHandler.handleColor(lineChart ? 'markerFill' : 'fill', seriesData, 'markerColor', data.series.length);

                        if (colorHandler.handleColor(isBubbleChart() ? 'line' : 'markerBorder', seriesData, 'markerBorderColor', data.series.length)) {
                            _.each(seriesData.dps, function (dataPoint) {
                                dataPoint.markerBorderThickness = 1;
                            });
                        }
                        if (lineChart) {
                            if (docModel.getApp().isOOXML()) {
                                colorHandler.handleColor('line', seriesData, 'lineColor', data.series.length);
                            }
                            colorHandler.handleColor('line', seriesData, 'color', data.series.length);
                        } else {
                            // Needed if only the line Color for e.g. column chart is set, canvasjs not support lineColor for chart
                            if (onlyLineColor && !isAutoColor(attrs.line.color)) {
                                colorHandler.handleColor('line', seriesData, 'color', data.series.length);
                            } else {
                                colorHandler.handleColor('fill', seriesData, 'color', data.series.length);
                            }
                        }
                    }
                }
                if (pieDonut) {
                    var startAngle;
                    if (docModel.getApp().isODF()) {
                        startAngle = 360 - chartAttrs.chart.rotation;
                    } else {
                        startAngle = (attrs.series.chart3d ? chartAttrs.chart.rotationY : chartAttrs.chart.rotation) - 90;
                    }
                    seriesData.startAngle = startAngle;
                }
                if (legendModel.getMergedAttributeSet(true).legend.pos === 'off') {
                    seriesData.showInLegend = false;
                } else {
                    seriesData.showInLegend = true;
                    if (pieDonut) {
                        //we only see the first series
                        //canvasjs has rendering problems with to big legend-sizes
                        if ((seriesIndex > 0) || (seriesData.dps.length > 50)) {
                            seriesData.showInLegend = false;
                        }
                    }
                }

                if (seriesData.showInLegend && (!seriesData.dps || !seriesData.dps.length)) {
                    seriesData.showInLegend = false;
                }

                handleCharacterProps(self, indexLabelAttrs, seriesData, 'indexLabel');
                var dataLabels = [];
                if (!_.isEmpty(attrs.series.dataLabel)) {
                    seriesData.indexLabel = '{name}';
                    seriesData.indexLabelLineColor = null;
                    /**
                     * CanvasJs only supports 'inside' or 'outside'
                     *
                     * Default positions for xlsx/ods.
                     *
                     * Bar: right (outside)
                     * Column: above (outside)
                     * Line: right/above (outside)
                     * Pie: bestFit (inside)
                     * Bubble: right/above (outside)
                     * Area (not editable): center (inside) /above (outside)
                     */
                    var defaultDataLabelPosition = getDataLabelCanvasJsPosition(attrs.series.dataLabel.dlp);
                    seriesData.indexLabelPlacement = defaultDataLabelPosition ? defaultDataLabelPosition : pieDonut || (isArea && docModel.getApp().isOOXML()) ? 'inside' : 'outside';

                    if (attrs.series.dataLabel.dls) {
                        dataLabels = attrs.series.dataLabel.dls;
                    }
                } else {
                    seriesData.indexLabel = ' ';
                    seriesData.indexLabelLineColor = 'transparent';
                    seriesData.indexLabelPlacement = null;
                }

                _.each(seriesData.dps, function (dataPoint, dataPointIndex) {
                    var dataLabel = dataLabels[dataPointIndex];
                    if (dataLabel) {
                        var dataLabelPosition = getDataLabelCanvasJsPosition(dataLabel.dlp);
                        if (dataLabelPosition) {
                            dataPoint.indexLabelPlacement = dataLabelPosition;
                        }
                    }

                    if (lineChart) {
                        if (data.series.length > 1) {
                            dataPoint.markerType = MARKER_LIST[seriesIndex % MARKER_LIST.length];
                        } else {
                            dataPoint.markerType = MARKER_LIST[dataPointIndex % MARKER_LIST.length];
                        }
                        dataPoint.markerSize = sheetModel.getEffectiveZoom() * 7;

                        seriesData.legendMarkerType = dataPoint.markerType;
                    } else {
                        dataPoint.markerType = null;
                        dataPoint.markerSize = 0;

                        seriesData.legendMarkerType = 'square';
                    }
                });
            });

            chartFormatter.format();

            legendModel.refreshInfo();

            mainTitleModel.refreshInfo(chartTitle);

            refreshAxis();

            updateMaxDataPointSize();
        }

        /**
         * Inserts a new data series model into this chart model.
         */
        function insertDataSeries(index, attrs) {

            // workarounds for Bug 41988 (black charts from mac-excel)
            if (attrs.fill && !attrs.fill.color && !attrs.fill.gradient) {
                attrs.fill.color = Color.AUTO;
            }

            if (!isLineChart(attrs.series.type) && attrs.line && attrs.line.width) {
                //TODO: needed?
                delete attrs.line.width;
            }

            var chartAttrs = self.getMergedAttributeSet(true);
            // for ods the curved attribute is on the chart for ooxml the attribute is on the series
            var cjsType = parseCJSType(attrs.series.type, chartAttrs.chart.stacking, chartAttrs.chart.curved || attrs.series.curved);

            var seriesData = {
                type: cjsType,
                fillOpacity: 1,
                marker: MARKER_TYPES[attrs.series.marker],
                model: new DataSeriesModel(sheetModel, attrs),
                seriesIndex: index,
                needUpdate: true,
                dps: [],
                name: '',
                markerColor: 'transparent',
                color: 'transparent',
                indexLabelFontColor: 'transparent'
            };

            data.series.splice(index, 0, seriesData);

            self.listenTo(seriesData.model, 'refresh:formulas', function () {
                seriesData.needUpdate = true;
                refreshChartDebounced();
            });

            seriesData.axisXId = attrs.series.axisXIndex;
            seriesData.axisYId = attrs.series.axisYIndex;

            if ('axisZIndex' in attrs.series) {
                seriesData.axisZId = attrs.series.axisZIndex;
            }
            self.trigger('change:drawing');

            return true;
        }

        function compareFormats(group, other) {

            if (group.format === other.format) {
                group.display = null;
                group.result = (group.result || group.value) + ' ' + (other.result || other.value);
                return;
            }

            if (group.format.isAnyDateTime() && other.format.isAnyDateTime()) {
                group.display = null;
                group.result = (group.result || group.value) + (other.result || other.value);

                var groupDate = group.format.isAnyDate();
                var otherDate = other.format.isAnyDate();
                var groupTime = group.format.isAnyTime();
                var otherTime = other.format.isAnyTime();

                //take org format
                if ((groupDate && otherDate) || (groupTime && otherTime)) { return; }

                // use combined date/time format
                if ((groupDate && otherTime) || (groupTime && otherDate)) {
                    group.format = chartFormatter.parseFormatCode(LOCALE_DATA.shortDate + ' ' + LOCALE_DATA.longTime);
                }
            }
        }

        function reduceBigRange(source, index, name) {

            var ranges = data.series[index].model.resolveRanges(name);
            var range = ranges ? ranges.first() : null;
            var result = null;

            if (range) {
                var cols = range.cols();
                var rows = range.rows();
                if (cols > rows) {
                    globalLogger.warn('chart with 2d ranges as "' + name + '" reduceBigRange() must be implemented!');
                    result = source;
                } else if (rows > cols) {
                    var newList = [];
                    var numberFormatter = docModel.numberFormatter;
                    for (var row = 0; row < rows; ++row) {
                        var group = source[row * cols];
                        // check if it is not a hidden cell (filter or hide row/column
                        if (group) {
                            for (var col = 1; col < cols; ++col) {
                                var other = source[row * cols + col];
                                compareFormats(group, other);
                            }
                            group.display = numberFormatter.formatValue(group.format, group.result || group.value).text;
                            newList.push(group);
                        }
                    }
                    result = newList;
                }
            }
            if (!result) {
                result = source;
            }

            return result;
        }

        function updateMaxDataPointSize() {
            if (self.getSeriesCount() === 0) { return; }
            var rect = self.getRectangle();
            if (!rect) { return; }

            delete data.dataPointMaxWidth;
            delete data.dataPointMinWidth;

            var attrs = self.getMergedAttributeSet(true);
            var stacking = attrs.chart.stacking;
            var chartType = self.getChartType();
            var dataPointMaxWidth = null;

            if (stacking !== 'percentStacked' && stacking !== 'stacked' && data.series[0].dps.length === 1) {
                var multi = data.series.length;
                if (chartType.indexOf('bar') === 0) {
                    dataPointMaxWidth = Math.floor(rect.height / multi);
                } else if (chartType.indexOf('column') === 0) {
                    dataPointMaxWidth = Math.floor((rect.width * 0.9) / multi);
                }
                if (dataPointMaxWidth) {
                    data.dataPointMaxWidth = dataPointMaxWidth * 1.5;
                    data.dataPointMinWidth = dataPointMaxWidth * 0.5;
                }
            }

            ///////////////////////////////////

            var legendPos = legendModel.getMergedAttributeSet(true).legend.pos;
            var maxWidth = rect.width;

            if (legendPos === 'left' || legendPos === 'right' || legendPos === 'topRight') {
                maxWidth = rect.width / 4;
            }

            data.legend.maxWidth = maxWidth;
            data.legend.itemWrap = true;
            data.legend.maxHeight = rect.height;
        }

        function seriesAttrsToCellArray(array) {
            var result = [];
            if (_.isArray(array)) {
                array.forEach(function (value) {
                    var res = { display: value };
                    if (Number.isFinite(value)) {
                        res.value = value;
                    }
                    result.push(res);
                });
            }
            return result;
        }

        /**
         * Refreshes the source values of the data series of this chart.
         */
        function innerRefresh() {

            // nothing to do during import of the document
            if (!self.isImportFinished()) { return; }

            // all ranges to be querried, as array of arrays of range addresses
            var sourceRanges = [];
            // maps series indexes and source link types to array elements in 'sourceRanges'
            var collectInfo = [];

            var constantData = false;
            data.containsSeriesValues = false;
            data.series.forEach(function (seriesData, seriesIndex) {
                if (!seriesData.needUpdate) { return; }

                var info = { series: seriesIndex, indexes: {} };

                var tokenCount = 0;

                seriesData.model.iterateTokenArrays(function (tokenArray, linkKey, attrName) {

                    var entryRanges = seriesData.model.resolveRanges(linkKey);
                    if (!entryRanges.empty()) {
                        tokenCount++;
                        info.indexes[attrName] = sourceRanges.length;
                        sourceRanges.push(entryRanges);
                    }
                });

                if (tokenCount) {
                    collectInfo.push(info);
                } else {
                    constantData = true;
                    var sAttrs = seriesData.model.getExplicitAttributeSet(true).series;
                    chartFormatter.update(seriesIndex, seriesAttrsToCellArray(sAttrs.values), seriesAttrsToCellArray(sAttrs.title), seriesAttrsToCellArray(sAttrs.names), seriesAttrsToCellArray(sAttrs.bubbles));
                }
                seriesData.needUpdate = false;
            });

            if (!sourceRanges.length) {
                if (constantData) {
                    // fix for Bug Bug 51282
                    self.trigger('change:drawing', 'series');
                } else {
                    updateFormatting();
                }
                return;
            }

            if (sourceRanges.length > 0) {

                // query contents of visible cells for the data series (be nice and use a time-sliced loop)
                var allContents = [];
                var promise = self.iterateArraySliced(sourceRanges, function (ranges) {
                    allContents.push(docModel.getRangeContents(ranges, { blanks: true, visible: self.showOnlyVisibleCellsData(), attributes: true, display: true, maxCount: 1000 }));
                });

                // do not call the handler function, if the chart has been deleted in the meantime
                self.onFulfilled(promise, function () {

                    collectInfo.forEach(function (info) {

                        // returns the cell contents for the specified source link type
                        function getCellContents(key) {
                            return _.isNumber(info.indexes[key]) ? allContents[info.indexes[key]] : [];
                        }

                        // series values must exist
                        var valuesSource = getCellContents('values');
                        var titleSource = getCellContents('title');
                        var namesSource = getCellContents('names');
                        var bubblesSource = getCellContents('bubbles');

                        if (namesSource.length && namesSource.length >= valuesSource.length * 2) {
                            // workaround for TOO BIG ranges (2d range)
                            namesSource = reduceBigRange(namesSource, info.series, 'series.names');
                        }

                        if (titleSource.length && titleSource.length >= valuesSource.length * 2) {
                            // workaround for TOO BIG ranges (2d range)
                            titleSource = reduceBigRange(titleSource, info.series, 'series.title');
                        }

                        // fix for charts without values e.g. bubblechart with one column
                        if (!valuesSource.length) {
                            for (var i = 0; i < bubblesSource.length; i++) {
                                valuesSource[i] = null;
                            }
                        }

                        chartFormatter.update(info.series, valuesSource, titleSource, namesSource, bubblesSource);

                    });
                    refreshAxis();
                    self.trigger('change:drawing', 'series');
                });
            } else {
                data.series.forEach(function (seriesData) {
                    seriesData.needUpdate = false;
                });
                self.trigger('change:drawing', 'series');
            }
        }

        function changeAllSeriesAttrs(attrs) {
            return sheetModel.createAndApplyOperations(function (generator) {
                var position = self.getPosition();
                data.series.forEach(function (seriesData, seriesIndex) {
                    seriesData.model.generateChangeOperations(generator, position, seriesIndex, attrs);
                });
            }, { storeSelection: true });
        }

        // protected methods --------------------------------------------------

        this.refreshChartDebounced = function () {
            refreshChartDebounced();
        };

        /**
         * Handler for the document operation "insertChartSeries".
         *
         * @param {OperationContext} context
         *  A wrapper representing the "insertChartSeries" operation.
         */
        this.applyInsertSeriesOperation = function (context) {
            var index = context.optInt('series', data.series.length);
            insertDataSeries(index, context.optDict('attrs'));
        };

        /**
         * Handler for the document operation "deleteChartSeries".
         *
         * @param {OperationContext} context
         *  A wrapper representing the "deleteChartSeries" operation.
         */
        this.applyDeleteSeriesOperation = function (context) {

            var index = context.getInt('series'),
                seriesData = data.series[index];
            context.ensure(seriesData, 'invalid series index');

            seriesData.model.destroy();

            //workaround for Bug 43958, changing chart type to Bubble chart, dataseries count is cut to half
            //so we make a copy of the array, that the array in CanvasJS has still the same length
            data.series = data.series.slice(0);

            data.series.splice(index, 1);
            this.trigger('change:drawing', 'series');
        };

        /**
         * Handler for the document operation "changeChartSeries".
         *
         * @param {OperationContext} context
         *  A wrapper representing the "changeChartSeries" operation.
         */
        this.applyChangeSeriesOperation = function (context) {

            var seriesData = data.series[context.getInt('series')];
            context.ensure(seriesData, 'invalid series index');

            seriesData.model.setAttributes(context.getDict('attrs'));
            seriesData.needUpdate = true;
            this.trigger('change:drawing', 'series');
        };

        /**
         * Handler for the document operation "deleteChartAxis" to remove an
         * axis from the chart object entirely.
         *
         * @param {OperationContext} context
         *  A wrapper representing the "deleteChartAxis" operation.
         */
        this.applyDeleteAxisOperation = function (context) {
            var axisId = context.getInt('axis');
            var axisModel = axisModelMap[axisId];
            context.ensure(axisModel, 'invalid axisId');

            var axes = data['axis' + axisModel.getAxisType().toUpperCase()];
            var axis;
            var axisIndex = null;
            for (var i = 0; i < axes.length; i++) {
                axis = axes[i];
                if (axis.axisId === axisId) {
                    axisIndex = i;
                    break;
                }
            }
            if (axisIndex !== null) {
                axes.splice(axisIndex, 1);
            }

            delete axisModelMap[axisId];
        };

        /**
         * Handler for the document operation `changeChartAxis`.
         *
         * @param {OperationContext} context
         *  A wrapper representing the `changeChartAxis` operation.
         */
        this.applyChangeAxisOperation = function (context) {

            var axisModel = addAxis(context.getInt('axis'), context.getStr('axPos'), context.getInt('crossAx'), context.optBool('zAxis'), context.getDict('attrs'));
            context.ensure(axisModel, 'invalid axis identifier');

            this.trigger('change:drawing');
        };

        /**
         * Remove all axis from the ChartModel.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         * @param {Array<Number>} position
         *  The position of the ChartModel object in the sheet.
         */
        this.removeAllAxis = function (generator, position) {
            _.each(axisModelMap, function (axisModel, axisId) {
                axisModel.generateRestoreOperations(generator, position);
                generator.generateDrawingOperation(DELETE_CHART_AXIS, position, { axis: Number(axisId) });
            });

        };

        /**
         * Handler for the document operation `changeChartGrid`.
         *
         * @param {OperationContext} context
         *  A wrapper representing the `changeChartGrid` operation.
         */
        this.applyChangeGridOperation = function (context) {

            var axisModel = axisModelMap[context.getInt('axis')];
            context.ensure(axisModel, 'invalid axis identifier');

            var gridModel = axisModel.getGrid();
            context.ensure(gridModel, 'missing grid line model');

            gridModel.setAttributes(context.getDict('attrs'));
            this.trigger('change:drawing');
        };

        /**
         * Handler for the document operation `changeChartTitle`.
         *
         * @param {OperationContext} context
         *  A wrapper representing the `changeChartTitle` operation.
         */
        this.applyChangeTitleOperation = function (context) {

            var titleModel;
            if (context.has('axis')) {
                var axisId = context.getInt('axis');
                titleModel = axisId in axisModelMap ? axisModelMap[axisId].getTitle() : null;
            } else {
                titleModel = mainTitleModel;
            }

            context.ensure(titleModel, 'invalid axis identifier');

            titleModel.setAttributes(context.getDict('attrs'));
            this.trigger('change:drawing');
        };

        /**
         * Handler for the document operation `changeChartLegend`.
         *
         * @param {OperationContext} context
         *  A wrapper representing the `changeChartLegend` operation.
         */
        this.applyChangeLegendOperation = function (context) {
            legendModel.setAttributes(context.getDict('attrs'));
            this.trigger('change:drawing', 'series');
        };

        // public methods -----------------------------------------------------

        /**
         * is called by the DrawingController for a selected chart
         */
        this.getChartType = function () {
            var type = 'bar';
            if (data.series.length) {
                var curved = true;
                data.series.forEach(function (seriesData) {
                    if (curved && !seriesData.model.getMergedAttributeSet(true).series.curved) {
                        curved = false;
                    }
                });
                var firstSeriesType = self.getSeriesModel(0).getMergedAttributeSet(true).series.type;
                var stacking = self.getMergedAttributeSet(true).chart.stacking;
                type = parseChartType(firstSeriesType, stacking, curved);
            }

            return type;
        };

        this.getChartTypeForGui = function () {
            var chartType = self.getChartType();
            chartType = chartType.replace('clustered', 'standard');
            if (self.isMarkerOnly()) {
                chartType = chartType.replace(' curved', '');
                chartType += ' marker';
            }
            return chartType;
        };

        this.containsCurvedSeries = function () {
            var curved = false;
            data.series.forEach(function (seriesData) {
                if (!curved && seriesData.model.getMergedAttributeSet(true).series.curved) {
                    curved = true;
                }
            });
            return curved;
        };

        /**
         * chart CONTENT can be visible
         * (normal and fallbacks)
         */
        this.isValidChartType = function () {

            if (!data.series.length) {
                // no fallback for Bug 53212
                return true;
            }

            var firstType = self.getSeriesModel(0).getMergedAttributeSet(true).series.type;
            firstType = firstType.replace('2d', '').replace('3d', '');

            if (!VALID.test(firstType)) {
                return false;
            }

            if (data.series.length > 1) {
                var unsupported = _.find(data.series, function (dataSeries) {
                    var type = dataSeries.model.getMergedAttributeSet(true).series.type.replace('2d', '').replace('3d', '');
                    return type !== firstType && !(SUPPORTED_COMBINED.test(firstType) && SUPPORTED_COMBINED.test(type));
                });
                if (unsupported) {
                    return false;
                }
            }

            return true;
        };

        this.isRestorable = function () {

            if (!data.series.length) {
                return false;
            }

            var firstType = self.getSeriesModel(0).getMergedAttributeSet(true).series.type;

            if (!SUPPORTED.test(firstType)) {
                return false;
            }
            if (data.series.length > 1) {
                var different = _.find(data.series, function (dataSeries) {
                    var type = dataSeries.model.getMergedAttributeSet(true).series.type;
                    return type !== firstType;
                });

                if (different) {
                    return false;
                }
            }

            return !self.getMergedAttributeSet(true).chart.eb;
        };

        /**
         * Refreshes the source values of the data series of this chart.
         */
        this.refresh = this.debounce(innerRefresh, { delay: 50, maxDelay: 250 });

        this.getSeriesModel = function (index) {
            return data.series[index].model;
        };

        this.getLegendModel = function () {
            return legendModel;
        };

        this.getAxisModel = function (axisId) {
            return axisModelMap[axisId];
        };

        this.getTitleModel = function (axisId) {
            return axisId === null || axisId === undefined ? mainTitleModel : this.getAxisModel(axisId).getTitle();
        };

        /**
         * Invokes the passed callback function for all data series.
         *
         * @param {Function} callback
         *  (1) {Number} seriesIndex the index of the data series
         *  (2) {Object} tokens the object with the token name(key) and tokenArray(value)
         */
        this.iterateTokenArrays = function (callback) {
            data.series.forEach(function (seriesData) {
                var tokens = {};
                seriesData.model.iterateTokenArrays(function (tokenArray, linkKey, attrName) {
                    tokens[attrName] = tokenArray;
                });
                callback(seriesData.seriesIndex, tokens);
            });
        };

        /**
         * Returns the addresses of the source cell ranges referred by the data
         * series of this chart object.
         *
         * @returns {Range3DArray}
         *  The range addresses (with sheet indexes) of all data series.
         */
        this.resolveRanges = function () {
            var sourceRanges = new Range3DArray();
            data.series.forEach(function (seriesData) {
                seriesData.model.iterateTokenArrays(function (_tokenArray, linkKey) {
                    sourceRanges.append(seriesData.model.resolveRanges(linkKey));
                });
            });
            return sourceRanges;
        };

        function refreshAxis() {
            if (!data.series.length) {
                return;
            }
            _.each(axisModelMap, function (axisModel, axisId) {
                if (axisId === 'z') { return; }
                axisModel.refreshInfo();
            });
        }

        function getStyleId() {
            return self.getMergedAttributeSet(true).chart.chartStyleId;
        }

        function sourceExistsComplete(name) {
            return (data.series.length > 0) && data.series.every(function (seriesData) {
                return !_.isEmpty(seriesData.model.getMergedAttributeSet(true).series[name]);
            });
        }

        function isSingleSeries() {
            return self.isPieOrDonut() || data.series.length === 1;
        }

        this.getBackgroundColor = function () {
            var fillAttrs = this.getMergedAttributeSet(true).fill;
            return isAutoShape(fillAttrs) ? colorHandler.getBackgroundColor(getStyleId()) : fillAttrs.color;
        };

        this.isXYType = isXYType;

        this.isBubbleChart = isBubbleChart;

        this.isAxesEnabled = function () {
            return !self.isPieOrDonut();
        };

        this.isPieOrDonut = function () {
            return /^(pie|donut|doughnut|sunburst)/.test(self.getChartType());
        };

        this.isAreaOrLine = function () {
            return /^(area|line|scatter)/.test(self.getChartType());
        };

        this.isArea = function () {
            return /^(area)/.test(self.getChartType());
        };

        this.isVaryColorEnabled = function () {
            return isSingleSeries();
        };

        this.isVaryColor = function () {

            if (!isSingleSeries()) {
                return false;
            }

            var attrs = self.getExplicitAttributeSet(true);
            if (attrs && attrs.chart && attrs.chart.varyColors) {
                return true;
            }

            var colorInfo = self.getSeriesColorInfo();
            if (colorInfo.length <= 1) { return false; }

            return !_.isEqual(colorInfo[0], colorInfo[1]);
        };

        this.showOnlyVisibleCellsData = function () {
            var attrs = self.getExplicitAttributeSet(true);
            return getBooleanOption(attrs.chart, 'sovcd', true);
        };

        /**
         * it generates a unique-color-ID out of the internal chartStyleId
         * for describing the color patterns
         * return null, if one or more series have a unique fill color
         *
         * @returns {String|null}
         */
        this.getColorSet = function () {
            return 'cs' + toColorSetIndex(getStyleId());
        };

        /**
         * it generates a unique-style-ID out of the internal chartStyleId
         * for describing the background and the highlight of the chart
         * it ignores unique fill colors of the series
         *
         * @returns {String}
         */
        this.getStyleSet = function () {
            return 'ss' + toStyleSetIndex(getStyleId());
        };

        /**
         * Generate change style operations.
         *
         * @param {type} generator
         * @param {Number} chartStyleId Id of the chart style
         * @param {type} setBackgroundColor TRUE if the background color from the chart style should be set.
         */
        function generateChangeStyleIdOperations(generator, chartStyleId, setBackgroundColor) {

            var attrSet = {};
            if (setBackgroundColor) {
                var styleSet = getStyleSet()[toStyleSetIndex(chartStyleId)];

                var bgColor = styleSet.bg;
                if (!bgColor) {
                    var color = getColorOfPattern('cycle', 'single', 1, getColorSet()[toColorSetIndex(chartStyleId)].colors, 3, docModel);
                    bgColor = transformOpColor(color, ...BACKGROUND_TRANSFORMATIONS);
                }
                if (!bgColor.fallbackValue) {
                    bgColor.fallbackValue = docModel.parseAndResolveColor(bgColor, 'fill').hex;
                }

                attrSet.fill = { type: 'solid', color: bgColor };
            }

            attrSet.chart = { chartStyleId };

            self.generateChangeOperations(generator, attrSet);
        }

        // will only called if self.isVaryColorEnabled() == true;
        this.changeVaryColors = function (state) {
            return sheetModel.createAndApplyOperations(function (generator) {

                var chartStyleId = getStyleId();

                generateChangeStyleIdOperations(generator, chartStyleId);

                var position = self.getPosition();
                var colors = getColorSet()[toColorSetIndex(chartStyleId)];
                var count = 1;

                self.generateChangeOperations(generator, { chart: { varyColors: state } });

                var seriesData = data.series[0];
                var dataPoints = [];
                count = seriesData.dps.length;

                var seriesAttrs = seriesData.model.getMergedAttributeSet(true);

                seriesData.dps.forEach(function (dataPoint, index) {
                    dataPoints.push(getColorAttributes(index, count, colors, seriesAttrs, state, 'solid', 'solid'));
                });

                seriesData.model.generateChangeOperations(generator, position, 0, { series: { dataPoints } });

            }, { storeSelection: true });
        };

        function getColorAttributes(index, count, colors, seriesAttrs, varyColors, fillType, lineType) {
            var color;

            if (varyColors) {
                color = getColorOfPattern('cycle', colors.type, index, colors.colors, count, docModel);
            } else {
                color = getColorOfPattern('cycle', colors.type, 0, colors.colors, 3, docModel);
            }

            if (self.isMarkerOnly()) {
                lineType = 'none';
            }

            var colorAttributes = { fill: { type: fillType, color }, line: { type: lineType, color } };

            if (!isBubbleChart()) { // Fix for Bug 63144 - Chart - insert a Bubble Chart. The problem is that Markers are not allowed in Excel
                var onlyLine = self.isLineOnly();
                if (seriesAttrs.markerFill) {
                    if (onlyLine) {
                        colorAttributes.markerFill = _.clone(colorAttributes.fill);
                        colorAttributes.markerFill.type = 'none';
                    } else {
                        colorAttributes.markerFill = colorAttributes.fill;
                    }
                }

                if (seriesAttrs.markerBorder) {
                    if (onlyLine) {
                        colorAttributes.markerBorder = _.clone(colorAttributes.line);
                        colorAttributes.markerBorder.type = 'none';
                    } else {
                        colorAttributes.markerBorder = colorAttributes.line;
                    }
                }
            }
            return colorAttributes;
        }

        /**
         * updates the interal chartStyleId by the unique ID
         * deletes all series colors if existing
         *
         * @param {String} colorSet
         */
        this.changeColorSet = function (colorSet) {
            return sheetModel.createAndApplyOperations(function (generator) {

                var chartStyleId = getStyleId();
                var csId = colorSet.replace('cs', '') | 0;
                var styleSet = toStyleSetIndex(chartStyleId);

                chartStyleId = toChartStyleId(csId, styleSet);

                generateChangeStyleIdOperations(generator, chartStyleId);

                var position = self.getPosition();
                var colors = getColorSet()[toColorSetIndex(chartStyleId)];
                var count = 1;

                var seriesData = data.series[0];
                var seriesAttrs = seriesData.model.getMergedAttributeSet(true);

                if (self.isVaryColor()) {
                    var dataPoints = [];
                    count = seriesData.dps.length;

                    seriesData.dps.forEach(function (dataPoint, index) {
                        dataPoints.push(getColorAttributes(index, count, colors, seriesAttrs, true, 'solid', 'solid'));
                    });

                    // change the color of the legend
                    seriesData.model.generateChangeOperations(generator, position, 0, getColorAttributes(0, count, colors, seriesAttrs, true, 'solid', 'solid'));
                    // change the color of the e.g. column
                    seriesData.model.generateChangeOperations(generator, position, 0, { series: { dataPoints } });
                } else {
                    count = data.series.length;
                    data.series.forEach(function (seriesData, seriesIndex) {
                        seriesData.model.generateChangeOperations(generator, position, seriesIndex, getColorAttributes(seriesIndex, count, colors, seriesAttrs, true, 'solid', 'solid'));
                    });
                }

            }, { storeSelection: true });
        };

        /**
         * updates the interal chartStyleId by the unique ID
         * does not touch the series colors
         *
         * @param {String} styleSet
         */
        this.changeStyleSet = function (styleSet) {
            return sheetModel.createAndApplyOperations(function (generator) {

                var chartStyleId = getStyleId();
                var ssId = styleSet.replace('ss', '') | 0;
                var colorSet = toColorSetIndex(chartStyleId);
                chartStyleId = toChartStyleId(colorSet, ssId);

                generateChangeStyleIdOperations(generator, chartStyleId, true);

            }, { storeSelection: true });
        };

        this.getSeriesCount = function () {
            return data.series.length;
        };

        this.getFirstPointsCount = function () {
            try {
                if (data.series.length) {
                    return data.series[0].dps.length;
                }
            } catch (e) {
                globalLogger.warn('error while calling chartmodel.getFirstPointsCount()', e);
            }
            return 0;
        };

        /**
         * must be called for having correct behavior mit "varyColors" in combination with more than one series
         * FIXME: still needed?
         */
        this.firstInit = function () {
            self.trigger('change:drawing');
        };

        /**
         * must be called for having correct behavior mit "new CanvasJS.Chart" is called, because of a crazy bug inside there
         */
        this.resetData = function () {
            data.axisX._oldOptions = null;
            data.axisY._oldOptions = null;
        };

        this.hasDataPoints = function () {
            for (var i = 0; i < data.series.length; i++) {
                if (!_.isEmpty(data.series[i].dps)) { return true; }
            }
            return false;
        };

        /**
         * normally the assigned axisType (x or y) will be returned
         * except chart is a bar-type, then x & y are interchanged
         *
         * @param {String} axisType
         *
         * @returns {String}
         */
        this.getAxisTypeForDrawing = function (axisType) {
            var chartType = self.getChartType();
            if (chartType.indexOf('bar') === 0 && axisType !== 'z') {
                axisType = axisType === 'x' ? 'y' : 'x';
            }
            return axisType;
        };

        /**
         *
         * @param {String} axisType the type of the axis 'x' (Bottom) or 'y' (Left=
         * @returns {Number} id of the axis.
         */
        this.getAxisIdForType = function (axisType) {
            var axis = this.getAxisForType(axisType);
            return axis ? axis.axisId : null;
        };

        /**
         *
         * @param {String} axisType the type of the axis 'x' (Bottom) or 'y' (Left=
         *
         * @returns {AxisModel | null} the axis for the
         */
        this.getAxisForType = function (axisType) {

            var axis;
            var axisResult = null;
            var i;
            if (axisType === 'x') {
                for (i = 0; i < data.axisX.length; i++) {
                    axis = data.axisX[i];
                    axisResult = axis;
                    if (axis.axPos === 'b') {
                        break;
                    }
                }
                if (axisResult === null) {
                    for (i = 0; i < data.axisX2.length; i++) {
                        axis = data.axisX2[i];
                        axisResult = axis;
                        if (axis.axPos === 'b') {
                            break;
                        }
                    }
                }
            } else if (axisType === 'y') {
                for (i = 0; i < data.axisY.length; i++) {
                    axis = data.axisY[i];
                    axisResult = axis;
                    if (axis.axPos === 'l') {
                        break;
                    }
                }
                if (axisResult === null) {
                    for (i = 0; i < data.axisY2.length; i++) {
                        axis = data.axisY2[i];
                        axisResult = axis;
                        if (axis.axPos === 'l') {
                            break;
                        }
                    }
                }
            }
            return axisResult;
        };

        /**
         * transfers model-data to CanvasJs-data
         */
        this.updateRenderInfo = function () {
            updateFormatting();
            self.refresh();
        };

        /**
         * iterates all sources and calculates min and max positions,
         * direction of the series-values and the sheetindex
         *
         * @returns {Object}
         *  A descriptor with several properties of the data source:
         *  - {String} [warn]
         *      If existing, a specific warning code for invalid source data:
         *      - 'nodata': Source ranges are not available at all.
         *      - 'sheets': Source ranges are not on the same sheet.
         *      - 'directions': Source ranges are not in the same direction.
         * - {Range} range
         *      The bounding range of all source ranges in the targeted sheet.
         * - {Number} sheet
         *      Sheet index of all source ranges.
         * - {Number} axis
         *      Identifier of the main axis of the source ranges.
         */
        this.getDataSourceInfo = function () {

            var ranges = new RangeArray();
            var mainAxis = null;
            var sheet = null;
            var warn = null;
            var lastRange = null;
            var axis, col, row;
            data.series.forEach(function (seriesData) {
                var isBubbleChart = seriesData.type.indexOf('bubble') !== -1;
                if (isBubbleChart) {
                    lastRange = null;
                }
                var valuesRange, bubblesRange;
                seriesData.model.iterateTokenArrays(function (tokenArray, linkKey, attrName) {
                    var rangeList = seriesData.model.resolveRanges(linkKey);
                    if (rangeList.empty()) { return; }
                    var range = rangeList.first();
                    if (sheet === null) {
                        sheet = range.sheet1;
                    }
                    if (!range.isSheet(sheet)) {
                        warn = 'sheets';
                    }
                    if (isBubbleChart) {
                        var isValues = attrName === 'values';
                        var isBubbles = attrName === 'bubbles';
                        if (isValues || isBubbles || attrName === 'names' || attrName === 'title') {
                            if (mainAxis === null) {
                                var cols = range.cols();
                                var rows = range.rows();
                                axis = null;

                                if (isValues) {
                                    valuesRange = range;
                                    if (cols > 1 || rows > 1) {
                                        if (range.a1.c === range.a2.c) {
                                            axis = 0;
                                        } else if (range.a1.r === range.a2.r) {
                                            axis = 1;
                                        }
                                    }
                                }

                                if (isBubbles) {
                                    bubblesRange = range;
                                }

                                if (axis === null && bubblesRange && valuesRange) {
                                    var valueBubbleRange = valuesRange.boundary(bubblesRange);
                                    if (valueBubbleRange.a1.c === bubblesRange.a1.c) {
                                        axis = 1;
                                    } else if (valueBubbleRange.a1.r === bubblesRange.a1.r) {
                                        axis = 0;
                                    }
                                }
                                if (axis !== null) {
                                    mainAxis = axis;
                                }
                            }
                        }
                    } else if (attrName === 'values') {
                        axis = null;
                        col = range.cols();
                        row = range.rows();
                        if (col > row) {
                            axis = 1;
                        } else if (row > 1) {
                            axis = 0;
                        } else if (lastRange) {
                            if (lastRange.a1.c !== range.a1.c) {
                                axis = 0;
                            } else {
                                axis = 1;
                            }
                        }
                        if (_.isNumber(axis)) {
                            if (mainAxis === null) {
                                mainAxis = axis;
                            } else if (mainAxis !== axis) {
                                warn = 'directions';
                            }
                        }
                        lastRange = range;
                    }
                    // convert the 3D range (with sheet indexes) to a 2D range (without sheet indexes)
                    ranges.push(range.toRange());
                });
            });

            if (warn) { return { warn }; }
            if (sheet === null || ranges.empty()) { return { warn: 'nodata' }; }
            return { range: ranges.boundary(), axis: mainAxis, sheet };
        };

        this.getSeriesColorInfo = function () {

            function addColorInfo(attrs, points) {
                if (!attrs) { return; }

                if (!attrs.fill || attrs.fill.type === 'none') {
                    if (!attrs.line) { /* TODO: special ODF charts, we ignore it... */ return; }
                    points.push(attrs.line.color);
                } else {
                    points.push(attrs.fill.color);
                }
            }

            var points = [];
            if (isSingleSeries()) {
                var dataPoints = data.series[0].model.getExplicitAttributeSet(true).series.dataPoints;
                if (dataPoints) {
                    dataPoints.forEach(function (pointAttrs) {
                        addColorInfo(pointAttrs, points);
                    });
                }
            }
            if (!points.length) {
                data.series.forEach(function (seriesData) {
                    addColorInfo(seriesData.model.getExplicitAttributeSet(true), points);
                });
            }
            return points;
        };

        /**
         * checks if all dataseries have a sourcelink 'title'
         * which is the first row in a normal chart
         */
        this.isTitleLabel = function () {
            return sourceExistsComplete('title');
        };

        /**
         * checks if all dataseries have a sourcelink 'names'
         * which is the first column in a normal chart
         */
        this.isNamesLabel = function () {
            return sourceExistsComplete('names');
        };

        /**
         *
         * @returns {Boolean}
         */
        this.isMarkerOnly = function () {
            var marker = false;
            _.find(data.series, function (dataSeries) {
                var att = dataSeries.model.getMergedAttributeSet(true);
                if (isLineChart(att.series.type) && att.line.type === 'none') {
                    marker = true;
                }
                return true;
            });
            return marker;
        };

        this.isLineOnly = function () {
            var line = false;
            _.find(data.series, function (dataSeries) {
                var att = dataSeries.model.getMergedAttributeSet(true);
                if (isLineChart(att.series.type) && att.markerFill.type === 'none') {
                    line = true;
                }
                return true;
            });
            return line;
        };

        this.getDataLabel = function () {

            // DOCS-4608: ignore additional data series in pie charts
            if (this.isPieOrDonut()) {
                const attrSet = data.series[0]?.model.getMergedAttributeSet(true);
                return attrSet?.series.dataLabel ?? null;
            }

            var dataLabel = null;
            data.series.forEach(function (dataSeries) {
                var att = dataSeries.model.getMergedAttributeSet(true);
                dataLabel = dataLabel || att.series.dataLabel;
            });
            return dataLabel;
        };

        /**
         * Creates an image replacement from this model with the given
         * extent and image mimeType (either image/png or image/jpeg).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  - {String} [options.mimeType]
         *   If specified, the given image mime type will
         *   be used for image creation. Allowed values ATM
         *   are image/png (default) and image/jpeg
         *  - {Object} [options.size]
         *   If specified, the chart replacement will be
         *   rendered with the given size, otherwise, its
         *   size is retrieved from the model's drawing
         *   attributes.
         *
         * @returns {jQuery|Null}
         *  If successful, a floating jquery img node containing the image data
         *  as src dataUrl and with the requested width and height set
         *  accordingly.
         */
        this.getReplacementNode = function (options) {

            var drawingRectPx = this.getRectangle();
            if (!drawingRectPx) { return null; }

            var jqImgNode = null,
                modelData = this.getModelData(),
                chartId = 'io-ox-documents-chart-frame' + _.uniqueId(),
                widthHeightAttr = '"width="' + drawingRectPx.width + 'px" height="' + drawingRectPx.height + 'px"',
                jqDrawingDiv = $('<div class="chartnode chartholder" id="' + chartId + '" ' + widthHeightAttr + '>').css({
                    position: 'absolute',
                    left: -drawingRectPx.width,
                    top: -drawingRectPx.height,
                    width: drawingRectPx.width,
                    height: drawingRectPx.height
                });

            $(':root').append(jqDrawingDiv);

            var chartRenderer = new CanvasJS.Chart(chartId, _.extend(modelData, { backgroundColor: modelData.cssBackgroundColor })),
                jqCanvas = $('#' + chartId + ' .canvasjs-chart-canvas'),
                canvas = jqCanvas.get(0);

            try {
                // render chart
                chartRenderer.render();

                var dataUrl = canvas.toDataURL(getStringOption(options, 'mimeType', 'image/png'));

                if (dataUrl) {
                    jqImgNode = $('<img>').attr({
                        width: drawingRectPx.width,
                        height: drawingRectPx.height,
                        src: dataUrl
                    });

                    jqCanvas = dataUrl = chartRenderer = null;
                }
            } catch (ex) {
                globalLogger.exception(ex, 'replacement chart image rendering error');
            }

            jqDrawingDiv.remove();
            jqDrawingDiv = null;

            return jqImgNode;
        };

        /**
         * @param {Boolean} state
         */
        this.setMarkerOnly = function (state) {
            var noneShape = { type: 'none' };
            var solidShape = { type: 'solid' };

            var attrs = null;
            if (state) {
                attrs = { line: noneShape, fill: solidShape };
            } else {
                attrs = { line: solidShape, fill: solidShape };
            }

            return changeAllSeriesAttrs(attrs);
        };

        this.setDataLabel = function (state) {
            var dataLabel = state ? { dlt: ['value'] } : {};
            var attrs = { series: { dataLabel } };
            return changeAllSeriesAttrs(attrs);
        };

        this.getCloneData = function () {

            // a data object passed as hidden parameter to the constructor of the clone
            var cloneData = { series: [], axes: {}, legend: legendModel.getExplicitAttributeSet(), title: mainTitleModel.getExplicitAttributeSet() };

            _.each(axisModelMap, function (axisModel, axisId) {
                var aClone = {
                    axis: axisModel.getExplicitAttributeSet()
                };
                aClone.axPos = axisModel.getAxisPos();
                aClone.crossAx = axisModel.getCrossAxis();

                if (axisModel.getAxisType() === 'z') {
                    aClone.zAxis = true;
                }

                if (axisModel.getGrid()) {
                    aClone.grid = axisModel.getGrid().getExplicitAttributeSet();
                }
                if (axisModel.getTitle()) {
                    aClone.title = axisModel.getTitle().getExplicitAttributeSet();
                }
                cloneData.axes[axisId] = aClone;
            });

            data.series.forEach(function (dataSeries) {
                var nsp = dataSeries.model.getExplicitAttributeSet();

                dataSeries.model.iterateTokenArrays(function (tokenArray, linkKey, attrName) {
                    nsp.series[attrName] = tokenArray.getFormula("op", Address.A1, Address.A1);
                });

                cloneData.series.push(nsp);
            });

            return cloneData;
        };

        // initialization -----------------------------------------------------

        colorHandler = new ManualColorHandler(this, data);
        chartFormatter = this.member(new ChartFormatter(docModel, this));

        this.on('change:attributes', function () {
            updateFormatting('series');
        });

        this.on('change:drawing', function (subType) {
            updateFormatting(subType);
        });

        // additional processing and event handling after the document has been imported
        this.waitForImportSuccess(function (alreadyImported) {

            // refresh chart formatting after import
            if (!alreadyImported && sheetModel.isActive()) {
                refreshChartDebounced();
            }

            // refresh the chart after the visibility of columns has changed
            this.listenTo(docModel, 'change:columns', function (event) {
                if (event.visibility) {
                    updateColRowVisibility(event.sheet, event.intervals, true);
                }
            });

            // refresh the chart after the visibility of rows has changed
            this.listenTo(docModel, 'change:rows', function (event) {
                if (event.visibility) {
                    updateColRowVisibility(event.sheet, event.intervals, false);
                }
            });

            // refresh the chart after number formats of cells have been changed (e.g. used for axis labels)
            this.listenTo(docModel, 'change:cells', function (event) {
                if (!event.styleCells.empty()) {
                    updateChangedCellStyles(event.sheet, RangeArray.mergeAddresses(event.styleCells));
                }
            });

        }, this);

        // clone private data passed as hidden argument to the constructor
        if (_.isObject(cloneData)) {

            cloneData.series.forEach(function (attrs, index) {
                insertDataSeries(index, attrs);
            });

            mainTitleModel.setAttributes(cloneData.title);
            legendModel.setAttributes(cloneData.legend);

            _.each(cloneData.axes, function (axisData, axisId) {
                var targetAxisModel = addAxis(window.parseInt(axisId, 10), axisData.axPos, axisData.crossAx, axisData.zAxis, axisData.axis);
                if (!_.isEmpty(axisData.grid)) {
                    targetAxisModel.getGrid().setAttributes(axisData.grid);
                }
                if (!_.isEmpty(axisData.title)) {
                    targetAxisModel.getTitle().setAttributes(axisData.title);
                }
            });
        }

        // destructor
        this.registerDestructor(function () {
            _.forEach(axisModelMap, axisModel => axisModel.destroy());

            data.series.forEach(seriesData => {
                seriesData.model.destroy();
                // must delete, otherwise cansjs has still reference on this
                delete seriesData.model;
            });
        });
    }
}
