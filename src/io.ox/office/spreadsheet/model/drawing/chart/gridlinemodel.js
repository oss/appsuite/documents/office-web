/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';
import { handleLineProps } from '@/io.ox/office/drawinglayer/view/chartstyleutil';
import { CHANGE_CHART_GRID } from '@/io.ox/office/spreadsheet/utils/operations';

// class GridlineModel ========================================================

/**
 * Representation of the grid lines attached to an axis in a chart drawing
 * object.
 */
export default class GridlineModel extends AttributedModel {

    constructor(chartModel, axisId, attrs, dataAxis) {

        // base constructor
        super(chartModel.docModel, attrs, { families: ['line'] });

        // public methods -----------------------------------------------------

        this.refreshInfo = function () {

            if (!dataAxis) { return; }

            var lineAttrs = this.getMergedAttributeSet(true).line;
            handleLineProps(chartModel, lineAttrs, dataAxis, 'grid');
        };

        // operation generators -----------------------------------------------

        /**
         * Generates the undo operations needed to restore the grid lines.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the undo operations.
         *
         * @param {Position} position
         *  The position of the parent chart object in the sheet.
         */
        this.generateRestoreOperations = function (generator, position) {
            if (this.hasExplicitAttributes()) {
                var properties = { axis: axisId, attrs: this.getExplicitAttributeSet(true) };
                generator.generateDrawingOperation(CHANGE_CHART_GRID, position, properties, { undo: true });
            }
        };
    }
}
