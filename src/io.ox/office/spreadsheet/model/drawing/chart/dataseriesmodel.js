/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is } from '@/io.ox/office/tk/algorithms';

import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';

import { CHANGE_CHART_SERIES, INSERT_CHART_SERIES } from '@/io.ox/office/spreadsheet/utils/operations';
import { XSourceLinkedModel } from '@/io.ox/office/spreadsheet/model/drawing/xsourcelinkedmodel';

// class DataSeriesModel ======================================================

/**
 * Representation of a single data series in a chart object.
 *
 * @param {SheetModel} sheetModel
 *  The sheet model that contains the chart object with this data series.
 */
export default class DataSeriesModel extends XSourceLinkedModel.mixin(AttributedModel) {

    constructor(sheetModel, initAttributes) {

        // base constructor
        super(
            // parameters "sheetModel" and "linkAttrQNames" for `XSourceLinkedModel` mixin
            sheetModel,
            "series.title series.names series.values series.bubbles",
            // all following parameters for `AttributedModel` base class
            sheetModel.docModel,
            initAttributes,
            { families: ["series", "fill", "line", "markerFill", "markerBorder", "markerColor"] }
        );
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the addresses of the source cell ranges referred by the
     * specified series source type, if existing.
     *
     * @param {string} linkKey
     *  The fully qualified link key (attribute family and attribute name,
     *  separated by a period, e.g. "series.title") of the source link to be
     *  resolved.
     *
     * @returns {Range3DArray|null}
     *  An array of cell range addresses (with sheet indexes), if the specified
     *  link is associated with a cell range in the document; or `null`, if the
     *  specified link does not exist or contains constant data.
     */
    resolveRanges(linkKey) {
        const ranges = super.resolveRanges(linkKey);
        // DOCS-4122: exclude subtotal row of table ranges
        ranges?.forEach(range => {
            // only single-sheet ranges with multiple rows can be shortened
            if (!range.singleSheet() || (range.rows() === 1)) { return; }
            // resolve table collection of source sheet (not the own sheet of this model!)
            const tableCollection = this.docModel.getSheetModel(range.sheet1)?.tableCollection;
            // resolve footer area of covered table range
            const footerRange = tableCollection?.getTableModelAt(range.a2)?.getFooterRange();
            // check if the range ends inside the footer area
            if (footerRange?.contains(range.footerRow())) {
                range.a2.r -= 1;
            }
        });
        return ranges;
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the undo operations needed to restore this data series.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the undo operations.
     *
     * @param {Position} position
     *  The position of the parent chart object in the sheet.
     *
     * @param {number} index
     *  The index of this data series in the collection of all data series.
     */
    generateRestoreOperations(generator, position, index) {
        var properties = { series: index, attrs: this.getExplicitAttributeSet() };
        generator.generateDrawingOperation(INSERT_CHART_SERIES, position, properties, { undo: true });
    }

    /**
     * Generates the operations and undo operations needed to change the
     * formatting attributes of this data series.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {Position} position
     *  The position of the parent chart object in the sheet.
     *
     * @param {number} index
     *  The index of this data series in the collection of all data series.
     *
     * @param {Object} attributeSet
     *  The incomplete attribute set with all formatting attributes to be
     *  changed.
     */
    generateChangeOperations(generator, position, index, attributeSet) {
        var undoAttrSet = this.getUndoAttributeSet(attributeSet);
        if (!is.empty(undoAttrSet)) {
            generator.generateDrawingOperation(CHANGE_CHART_SERIES, position, { series: index, attrs: undoAttrSet }, { undo: true });
            generator.generateDrawingOperation(CHANGE_CHART_SERIES, position, { series: index, attrs: attributeSet });
        }
    }

    /**
     * Generates the operations and undo operations to update or restore the
     * formula expressions of the source links of this data series.
     *
     * @param {SheetCache} sheetCache
     *  The sheet operations cache to be filled with the operations.
     *
     * @param {FormulaUpdateTask} updateTask
     *  The update task to be executed.
     *
     * @param {Position} position
     *  The position of the parent chart object in the sheet.
     *
     * @param {number} index
     *  The index of this data series in the collection of all data series.
     */
    generateUpdateTaskOperations(sheetCache, updateTask, position, index, options) {
        // mix-in class `XSourceLinkedModel` generates the necessary change operations
        this.implGenerateUpdateTaskOperations(sheetCache, updateTask, CHANGE_CHART_SERIES, position, { series: index }, options);
    }
}
