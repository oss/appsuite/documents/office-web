/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, fun, itr, ary, jpromise } from "@/io.ox/office/tk/algorithms";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { makeRejected, isOriginCell, MergeMode, Address, AddressSet, RangeArray, modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { XSheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingcollection";
import { NoteModelConfig, NoteModel } from "@/io.ox/office/spreadsheet/model/drawing/notemodel";

// private functions ==========================================================

/**
 * Converts the passed anchor address of a cell note to its drawing position as
 * used in document operations.
 *
 * @param {Address} address
 *  The anchor address of a cell note.
 *
 * @returns {Position}
 *  The drawing position of the cell note used in document operations (column
 *  and row index of the address, as array).
 */
function addressToPosition(address) {
    return [address.c, address.r];
}

/**
 * Converts the passed drawing position as used in document operations to the
 * anchor address of a cell note.
 *
 * @param {Position} position
 *  The drawing position of a cell note used in document operations.
 *
 * @returns {Address|null}
 *  The anchor address of the cell note, if the passed drawing position is
 *  valid; otherwise `null`.
 */
function positionToAddress(position) {
    return (position.length === 2) ? new Address(position[0], position[1]) : null;
}

/**
 * Returns the drawing object position of the passed note model (the serialized
 * anchor cell address) as used in document operations.
 *
 * @param {NoteModel} noteModel
 *  The note model whose position will be returned.
 *
 * @returns {Position}
 *  The drawing object position of the passed note model.
 */
function resolveNotePosition(noteModel) {
    // use the JSON representation of an address (array with two integer elements)
    return addressToPosition(noteModel.getAnchor());
}

/**
 * Sort comparator callback function for sorted arrays note models.
 */
function modelSorter(noteModel1, noteModel2) {
    return Address.compare(noteModel1.getAnchor(), noteModel2.getAnchor());
}

// class NoteCollection =======================================================

/**
 * Represents the collection of cell notes of a single sheet of a spreadsheet
 * document.
 *
 * @param {SheetModel} sheetModel
 *  The sheet model instance containing this collection.
 */
export class NoteCollection extends XSheetDrawingCollection.mixin(DrawingCollection) {

    // all note models, mapped by anchor cell address
    #addressSet = new AddressSet();

    // all note models, sorted by their anchor address (row by row)
    #noteModels /*: NoteModel[]*/ = [];

    // constructor ------------------------------------------------------------

    constructor(sheetModel) {

        // base constructor
        super(sheetModel, sheetModel.docModel);

        // update position of cell anchors when moving cells in the sheet
        this.listenTo(sheetModel.cellCollection, "move:cells", this.#moveCellsHandler);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns an iterator for the note models in this collection.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {RangeSource} [options.ranges]
     *    An cell range list, or a single cell range address to search for
     *    notes. If omitted, visits all notes in the sheet.
     *  - {boolean} [options.vertical=false]
     *    If set to `false` or omitted, the addresses will be visited from
     *    first to last row, and in each row from left to right. If set to
     *    `true`, the addresses will be visited from first to last column, and
     *    in each column from top to bottom.
     *  - {boolean} [options.reverse=false]
     *    If set to `true`, the order of the visited notes will be reversed
     *    (e.g. from bottom row to top row, and in each row from right to
     *    left).
     *  - {boolean} [options.visible]
     *    If set to a boolean value, the iterator will skip notes with a
     *    visibility state (formatting attribute) different from the option
     *    value, regardless of their position in hidden or visible columns or
     *    rows. If omitted, the visibility state of the notes will be ignored.
     *  - {boolean} [options.visibleCells=false]
     *    If set to `true`, the iterator will skip notes contained in hidden
     *    columns or hidden rows.
     *
     * @yields {NoteModel}
     *  The models of the cell notes.
     */
    /*override*/ *yieldModels(options) {

        // filter predicate for the "visible" option
        const visible = options?.visible;
        const visibleFn = is.boolean(visible) ? (noteModel => noteModel.isVisible() === visible) : fun.true;

        // filter predicate for the "visibleCells" option
        const { cellCollection } = this.sheetModel;
        const visibleCellsFn = options?.visibleCells ? (address => cellCollection.isVisibleCell(address)) : fun.true;

        // shortcut to storage
        const addressSet = this.#addressSet;

        // helper function to visit the note models in one cell range
        function *yieldRange(range) {
            for (const element of addressSet.ordered(range ? { ...options, range } : options)) {
                // filter according to passed options
                const noteModel = element.model;
                if (visibleFn(noteModel) && visibleCellsFn(element)) {
                    yield noteModel;
                }
            }
        }

        // visit the specified cell ranges, or the entire sheet
        if (options?.ranges) {
            for (const range of ary.values(RangeArray.cast(options.ranges), { reverse: options.reverse })) {
                yield* yieldRange(range);
            }
        } else {
            yield* yieldRange();
        }
    }

    /**
     * Returns whether this collection contains any cell notes.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible]
     *    If set to a boolean value, only notes with a visibility state
     *    (formatting attribute) equal to the option value will be considered,
     *    regardless of their position in hidden or visible columns/rows. If
     *    omitted, the visibility state of the notes will be ignored.
     *  - {boolean} [options.visibleCells=false]
     *    If set to `true`, only notes contained in visible columns and rows
     *    will be considered.
     *
     * @returns {boolean}
     *  Whether this collection contains any cell notes.
     */
    hasNotes(options) {
        return !!itr.shift(this.yieldModels(options));
    }

    /**
     * Returns a descriptor object with information about available cell notes
     * in this collection.
     *
     * @returns {object}
     *  A descriptor object with the following properties:
     *  - {boolean} hasNotes
     *    Whether this collection contains any cell notes, regardless of their
     *    current visibility.
     *  - {boolean} hasVisibleNotes
     *    Whether this collection contains any visible cell notes.
     *  - {boolean} hasHiddenNotes
     *    Whether this collection contains any hidden cell notes.
     *  - {boolean} hasNotesInVisibleCells
     *    Whether this collection contains any cell notes, regardless of their
     *    current visibility.
     *  - {boolean} hasVisibleNotesInVisibleCells
     *    Whether this collection contains any visible cell notes.
     *  - {boolean} hasHiddenNotesInVisibleCells
     *    Whether this collection contains any hidden cell notes.
     */
    getNotesInfo() {

        // all flags to be returned
        let hasNotes = false;
        let hasVisibleNotes = false;
        let hasHiddenNotes = false;
        let hasNotesInVisibleCells = false;
        let hasVisibleNotesInVisibleCells = false;
        let hasHiddenNotesInVisibleCells = false;

        // counter for `*InVisibleCells` flags set to `true` for early loop exit
        let visibleCellsFlagCount = 0;

        // cell collection for cell visibility
        const { cellCollection } = this.sheetModel;

        // visit all note models as long as any flag is still `false`
        // (i.e., the respective cell note has not been found so far)
        for (const noteModel of this.yieldModels()) {

            // update the flags independent of call visibility
            const isVisible = noteModel.isVisible();
            hasNotes = true;
            if (isVisible) {
                hasVisibleNotes = true;
            } else {
                hasHiddenNotes = true;
            }

            // collect flags for visible cells (unless all flags have been collected already)
            if ((visibleCellsFlagCount < 3) && cellCollection.isVisibleCell(noteModel.getAnchor())) {
                if (!hasNotesInVisibleCells) {
                    hasNotesInVisibleCells = true;
                    visibleCellsFlagCount += 1;
                }
                if (isVisible && !hasVisibleNotesInVisibleCells) {
                    hasVisibleNotesInVisibleCells = true;
                    visibleCellsFlagCount += 1;
                } else if (!isVisible && !hasHiddenNotesInVisibleCells) {
                    hasHiddenNotesInVisibleCells = true;
                    visibleCellsFlagCount += 1;
                }
            }

            // early exit the loop, if all flags have been set to true
            if (visibleCellsFlagCount === 3) { break; }
        }

        return {
            hasNotes,
            hasVisibleNotes,
            hasHiddenNotes,
            hasNotesInVisibleCells,
            hasVisibleNotesInVisibleCells,
            hasHiddenNotesInVisibleCells
        };
    }

    /**
     * Returns the note model anchored at the specified cell address.
     *
     * @param {Address} address
     *  The address of the note's anchor cell.
     *
     * @returns {Opt<NoteModel>}
     *  The model of the cell note anchored at the specified cell, if existing;
     *  otherwise `undefined`.
     */
    getByAddress(address) {
        return this.#addressSet.at(address)?.model;
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations, and the undo operations, to insert a new cell
     * note into this collection.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {NoteModelConfig} modelConfig
     *  The configuration of the new cell note.
     *
     * @param {Dict} attrSet
     *  The formatting attribute set to be applied at the note's drawing frame.
     */
    generateInsertNoteOperations(generator, modelConfig, attrSet) {
        const operProps = { attrs: attrSet, ...modelConfig.toJSON() };
        generator.generateAnchorOperation(Op.INSERT_NOTE, modelConfig.anchor, operProps);
        generator.generateAnchorOperation(Op.DELETE_NOTE, modelConfig.anchor, null, { undo: true });
    }

    /**
     * Generates the operations, and the undo operations, to delete all cell
     * notes (optionally located in specific ranges) from this collection.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeSource} [ranges]
     *  The cell ranges containing the cell notes to be deleted. If omitted,
     *  all existing cell notes will be deleted.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when the operations have been generated
     *  successfully, or that will be rejected on any error.
     */
    generateDeleteAllNotesOperations(generator, ranges) {
        // bug 64896: delete in reversed order (uses an array iterator internally)
        // TODO: can be simplified when removing the "applyImmediately" generator mode
        const iterator = this.yieldModels({ ranges, reverse: true });
        return this.asyncForEach(iterator, noteModel => {
            noteModel.generateDeleteOperations(generator);
        });
    }

    /**
     * Generates the operations, and the undo operations, to show or hide all
     * cell notes in this collection.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {boolean} visible
     *  Whether to show all hidden notes (`true`), or to hide all visible notes
     *  (`false`).
     *
     * @returns {JPromise}
     *  A promise that will be resolved when the operations have been generated
     *  successfully, or that will be rejected on any error.
     */
    generateToggleAllNotesOperations(generator, visible) {
        const attrSet = { drawing: { hidden: !visible } };
        return this.asyncForEach(this.#addressSet, element => {
            if (element.model.isVisible() !== visible) {
                element.model.generateChangeOperations(generator, attrSet);
            }
        });
    }

    /**
     * Generates the operations, and the undo operations, to update the cell
     * notes after merging the specified cell ranges. All cell notes covered by
     * a merged range will be deleted.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeSource} mergedRanges
     *  An array of cell range addresses, or a single cell range address that
     *  will be merged.
     *
     * @param {MergeMode} mergeMode
     *  The merge mode for the specified ranges.
     *
     * @returns {JPromise}
     *  A promise that fulfils when all operations have been generated.
     */
    generateMergeCellsOperations(generator, mergedRanges, mergeMode) {

        // nothing to do without notes, or when unmerging a cell range
        if (!this.#addressSet.size || (mergeMode === MergeMode.UNMERGE)) {
            return jpromise.resolve();
        }

        // collect notes to be deleted (do not alter the container while iterating)
        // TODO: can be simplified when removing the "applyImmediately" generator mode
        let promise = this.asyncReduce(RangeArray.cast(mergedRanges), [], (deletedModels, mergedRange) => {
            for (const element of this.#addressSet.ordered({ range: mergedRange })) {
                if (!isOriginCell(element, mergedRange, mergeMode)) {
                    deletedModels.push(element.model);
                }
            }
        });

        // generate all operations to delete the notes
        promise = promise.then(deletedModels => {
            return this.asyncForEach(deletedModels, noteModel => {
                noteModel.generateDeleteOperations(generator);
            });
        });

        return promise;
    }

    /**
     * Generates the operations, and the undo operations, to move multiple cell
     * notes to new anchor cells. The positions of the drawing frames will be
     * updated relative to the new anchor cells.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {AddressArray} fromAddresses
     *  An array of cell addresses specifying the current anchor position of
     *  the cell notes.
     *
     * @param {AddressArray} toAddresses
     *  An array of cell addresses specifying the new anchor position of the
     *  cell notes. This array MUST contain exactly the same number of cell
     *  addresses as parameter "fromAddresses".
     *
     * @returns {JPromise}
     *  A promise that fulfils when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{NoteCollection} generateMoveAnchorOperations")
    generateMoveAnchorOperations(generator, fromAddresses, toAddresses) {

        // the address properties for the "moveNotes" operation
        const fromStr = fromAddresses.toOpStr();
        const toStr = toAddresses.toOpStr();

        // generate the "moveNotes" undo operation before updating the drawing frames
        generator.generateSheetOperation(Op.MOVE_NOTES, { from: toStr, to: fromStr }, { undo: true });

        // update the position of all note drawing frames
        let promise = this.asyncForEach(fromAddresses, (fromAddress, index) => {

            const noteModel = this.getByAddress(fromAddress);
            if (noteModel) {
                noteModel.generateMoveAnchorOperations(generator, toAddresses[index]);
                return;
            }

            // no note available: a comment thread must exist which contains a fallback note for compatibility
            const commentModel = this.sheetModel.commentCollection.getByAddress(fromAddress);
            if (!commentModel) { return makeRejected("operation"); }
        });

        // generate the "moveNotes" operation after updating the drawing frames
        promise = promise.then(() => {
            generator.generateSheetOperation(Op.MOVE_NOTES, { from: fromStr, to: toStr });
        });

        return promise;
    }

    // operation handlers -----------------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all cell
     * notes from the passed collection into this collection.
     *
     * @param {SheetOperationContext} _context
     *  A wrapper representing the document operation "copySheet".
     *
     * @param {NoteCollection} fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is missing
     *  in the operation.
     */
    applyCopySheetOperation(_context, fromCollection) {

        // clone the contents of the source collection
        for (const noteModel of fromCollection.yieldModels()) {
            const cloneModel = noteModel.clone(this.sheetModel, this);
            this.implInsertModel(resolveNotePosition(cloneModel), cloneModel);
        }
    }

    /**
     * Callback handler for the document operation "insertNote". Creates and
     * stores a new note, and triggers an "insert:drawing" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "insertNote".
     */
    applyInsertNoteOperation(context) {

        // multiple notes must not be located in the same cell
        const anchor = context.getAddress("anchor");
        context.ensure(!this.getByAddress(anchor), "duplicate anchor address %s", anchor);

        // the formatting attributes of the note drawing frame
        const attrSet = context.optDict("attrs");

        // configuration of the note model
        const modelConfig = new NoteModelConfig(
            anchor,
            context.optStr("author"),
            context.optStr("date"),
            context.optStr("text", { empty: true })
        );

        // create the note model (via collection base class)
        const noteModel = this.insertModel(addressToPosition(anchor), DrawingType.NOTE, attrSet, modelConfig);
        context.ensure(noteModel, "cannot insert note into cell %s", anchor);
    }

    /**
     * Callback handler for the document operation "deleteNote". Deletes an
     * existing note, and triggers a "delete:drawing" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "deleteNote".
     */
    applyDeleteNoteOperation(context) {
        const anchor = context.getAddress("anchor");
        const result = this.deleteModel(addressToPosition(anchor));
        context.ensure(result, "cannot delete note from cell %s", anchor);
    }

    /**
     * Callback handler for the document operation "changeNote". Changes the
     * settings of an existing note, and triggers a "change:drawing" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "changeNote".
     */
    applyChangeNoteOperation(context) {

        // operation must refer to an existing cell note
        const anchor = context.getAddress("anchor");
        const noteModel = this.getByAddress(anchor);
        context.ensure(noteModel, "no note found in cell %s", anchor);

        // set the new formatting attributes
        if (context.has("attrs")) {
            noteModel.setAttributes(context.getDict("attrs"));
        }

        // set the new text contents of the note
        if (context.has("text") && noteModel.setText(context.getStr("text", { empty: true }))) {
            this.trigger("change:drawing", noteModel, "text");
        }
    }

    /**
     * Callback handler for the document operation "moveNotes". Moves the
     * anchors of multiple notes to new cell addresses, and triggers a
     * "move:notes" event.
     *
     * @param {SheetOperationContext} context
     *  A wrapper representing the document operation "moveNotes".
     */
    applyMoveNotesOperation(context) {

        // get the source and target anchor addresses
        const fromAnchors = context.getAddressList("from");
        const toAnchors = context.getAddressList("to");
        context.ensure(fromAnchors.length === toAnchors.length, "anchor from/to count mismatch");

        // collect all note models before inserting them to prevent address collisions
        const movedNotes = [];

        // move all notes to their destination anchors
        fromAnchors.forEach((fromAnchor, index) => {

            // look up the note model at the old cell anchor
            var noteModel = this.getByAddress(fromAnchor);
            if (!noteModel) {
                // no note available: a comment thread must exist which contains a fallback note for compatibility
                var commentModel = this.sheetModel.commentCollection.getByAddress(fromAnchor);
                context.ensure(commentModel, "no note found in cell %s", fromAnchor);
                return;
            }

            // get the target anchor position (nothing to do if note does not move)
            const toAnchor = toAnchors[index];
            if (fromAnchor.equals(toAnchor)) { return; }

            // remove note model from internal collections, collect all models
            this.clientUnregisterModel(noteModel);
            movedNotes.push({ model: noteModel, target: toAnchor });
        });

        // insert all note models at their new anchor positions
        const movedModels = movedNotes.map(entry => {
            const noteModel = entry.model;
            noteModel.setAnchor(entry.target);
            this.clientRegisterModel(noteModel);
            return noteModel;
        });

        // moved notes will be notified with a "move:notes" bulk event
        this.trigger("move:notes", movedModels);
    }

    // protected methods ------------------------------------------------------

    /**
     * Constructs a note model instance.
     *
     * @param {DrawingCollection} parentCollection
     *  The parent drawing collection that will contain the new note.
     *
     * @param {DrawingType} drawingType
     *  The type of the drawing model to be created. The value DrawingType.NOTE
     *  is expected by this implementation.
     *
     * @param {Dict} attrSet
     *  Initial formatting attributes for the note model.
     *
     * @param {NoteModelConfig} modelConfig
     *  Additional note configuration.
     *
     * @returns {Opt<NoteModel>}
     *  The new note model instance, if the passed type is valid.
     */
    /*protected*/ clientCreateModel(parentCollection, drawingType, attrSet, modelConfig) {
        return (drawingType === DrawingType.NOTE) ? new NoteModel(this.sheetModel, parentCollection, attrSet, modelConfig) : undefined;
    }

    /**
     * Returns the note model addressed by the passed drawing object position
     * (the serialized anchor cell address) as used in document operations.
     *
     * @param {Position} position
     *  The drawing object position to be resolved.
     *
     * @returns {Opt<NoteModel>}
     *  The note model if available, otherwise null.
     */
    /*protected*/ clientResolveModel(position) {
        const address = positionToAddress(position);
        return address ? this.getByAddress(address) : undefined;
    }

    /**
     * Returns the drawing object position of the passed note model (the
     * serialized anchor cell address) as used in document operations.
     *
     * @param {NoteModel} noteModel
     *  The note model whose position will be returned.
     *
     * @returns {Position}
     *  The drawing object position of the passed note model.
     */
    /*protected*/ clientResolvePosition(noteModel) {
        // use the JSON representation of an address (array with two integer elements)
        return resolveNotePosition(noteModel);
    }

    /**
     * Returns the Z order index of the specified note model.
     *
     * @param {NoteModel} noteModel
     *  The note model whose Z index will be returned.
     *
     * @returns {number}
     *  The Z order index of the passed note model.
     */
    /*protected*/ clientResolveZIndex(noteModel) {
        // note models cannot be reordered, their Z order is fixed to the array
        // order of their anchor addressses (row-by-row)
        const ai = ary.fastFindFirstIndex(this.#noteModels, elem => modelSorter(noteModel, elem) <= 0);
        if (this.#noteModels[ai] === noteModel) { return ai; }
        modelLogger.warn("$badge{NoteCollection} clientResolveZIndex: unknown note model");
        return -1;
    }

    /**
     * Registers the passed note model after it has been inserted into this
     * drawing collection.
     *
     * @param {NoteModel} noteModel
     *  The new note model to be registered.
     *
     * @returns {boolean}
     *  Whether the note model has been registered successfully.
     */
    /*protected*/ clientRegisterModel(noteModel) {

        // notes must have unique anchor addresses
        const element = noteModel.getAnchor().clone();
        if (this.#addressSet.has(element)) { return false; }

        // add model to the address, insert it into the address set
        element.model = noteModel;
        this.#addressSet.add(element);

        // insert the note model into the sorted array
        ary.insertSorted(this.#noteModels, noteModel, modelSorter);

        return true;
    }

    /**
     * Unregisters the passed note model after it has been removed from this
     * drawing collection.
     *
     * @param {NoteModel} noteModel
     *  The note model to be unregistered.
     *
     * @returns {boolean}
     *  Whether the note model has been unregistered successfully.
     */
    /*protected*/ clientUnregisterModel(noteModel) {

        // remove the note model from the sorted array
        ary.deleteSorted(this.#noteModels, noteModel, modelSorter);

        // remove the note model from the address set
        return this.#addressSet.delete(noteModel.getAnchor());
    }

    // private methods --------------------------------------------------------

    /**
     * Recalculates the position of all cell notes, after cells have been moved
     * (including inserted/deleted columns or rows) in the sheet.
     */
    #moveCellsHandler({ transformer }) {

        // no preparation work needed without note models
        if (!this.#addressSet.size) { return; }

        // collect notes to be moved or deleted (do not alter the container while iterating)
        const movedNotes = [];
        const deletedModels = [];

        // when inserting, visit the notes in reversed order to prevent overlapping anchor addresses
        for (const element of this.#addressSet.ordered({ reverse: transformer.insert })) {
            const newAnchor = transformer.transformAddress(element);
            if (!newAnchor) {
                deletedModels.push(element.model);
            } else if (newAnchor.differs(element)) {
                movedNotes.push({ model: element.model, target: newAnchor });
            }
        }

        // first, delete all models (do not move notes over notes to be deleted)
        deletedModels.forEach(noteModel => {
            this.deleteModel(resolveNotePosition(noteModel));
        });

        // move all note models to their new position
        const movedModels = movedNotes.map(entry => {
            const noteModel = entry.model;
            this.clientUnregisterModel(noteModel);
            noteModel.setAnchor(entry.target);
            this.clientRegisterModel(noteModel);
            return noteModel;
        });

        // moved anchor address will be notified with a "move:notes" event
        this.trigger("move:notes", movedModels);
    }
}
