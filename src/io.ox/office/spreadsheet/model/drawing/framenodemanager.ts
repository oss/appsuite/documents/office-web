/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map, jpromise } from "@/io.ox/office/tk/algorithms";
import { createDiv, createSpan, insertChildren, replaceChildren } from "@/io.ox/office/tk/dom";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { INTERNAL_MODEL_CLASS, createDrawingFrame, getContentNode, checkEmptyTextShape } from "@/io.ox/office/drawinglayer/view/drawingframe";
import { PARAGRAPH_NODE_SELECTOR } from "@/io.ox/office/textframework/utils/dom";
import type { SheetDrawingModelType } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { withTextFrame } from "@/io.ox/office/spreadsheet/model/drawing/text/textframeutils";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";

// types ======================================================================

/**
 * Options for creating drawing frames.
 */
export interface DrawingFrameOptions {

    /**
     * If set to `true`, the drawing frame will be marked with a special CSS
     * class that makes it an internal model container with reduced rendering
     * (improved performance and reduced usage of memory). Default value is
     * `false`.
     */
    internal?: boolean;
}

// class FrameNodeManager =====================================================

/**
 * Provides a DOM container node for drawing frames used as model instances for
 * text contents as expected by the text framework.
 *
 * @param docModel
 *  The model of the spreadsheet document.
 */
export class FrameNodeManager extends ModelObject<SpreadsheetModel> {

    // static functions -------------------------------------------------------

    /**
     * Returns the root container node for all top-level drawing frames.
     *
     * @returns
     *  The root container node for all top-level drawing frames.
     */
    static createRootNode(): HTMLDivElement {
        const rootNode = createDiv("p sheet");
        rootNode.appendChild(createSpan({ label: "" }));
        return rootNode;
    }

    // properties -------------------------------------------------------------

    // the root container node for all top-level drawing frames
    readonly rootNode = FrameNodeManager.createRootNode();

    // all existing drawing frames, mapped by drawing model
    readonly #drawingFrameMap = new Map<SheetDrawingModelType, JQuery>();

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super(docModel);
        // text framework expects empty span elements between top-level drawings
        this.#clearRootNode();
    }

    protected override destructor(): void {
        this.removeAllDrawingFrames();
        super.destructor();
    }

    // public methods -----------------------------------------------------

    /**
     * Returns the DOM drawing frame that represents the passed drawing model.
     *
     * @param drawingModel
     *  The drawing model instance to return the DOM drawing frame for.
     *
     * @returns
     *  The DOM drawing frame for the passed drawing model; or `undefined`, if
     *  no drawing frame could be found.
     */
    getDrawingFrame(drawingModel: SheetDrawingModelType): Opt<JQuery> {
        return this.#drawingFrameMap.get(drawingModel);
    }

    /**
     * Creates a new DOM drawing frame that represents the passed drawing
     * model, and inserts it into the DOM tree represented by this instance.
     *
     * @param drawingModel
     *  The model of a drawing object to create a DOM drawing frame for.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new DOM drawing frame for the passed drawing model; or `undefined`,
     *  if the drawing frame could not be created correctly.
     */
    createDrawingFrame(drawingModel: SheetDrawingModelType, options?: DrawingFrameOptions): Opt<JQuery> {

        // the container node for the new drawing frame
        const containerNode = this.#getContainerNode(drawingModel);
        if (!containerNode) { return undefined; }

        // the new drawing frame, as jQuery object
        const drawingFrame = createDrawingFrame(drawingModel);
        drawingFrame[0].dataset.modelUid = drawingModel.uid;

        // store the drawing frame in the local cache (faster than DOM lookups)
        this.#drawingFrameMap.set(drawingModel, drawingFrame);

        // insert the drawing frame at the correct position into the DOM collection
        this.#insertDrawingFrame(drawingModel, containerNode, drawingFrame);

        // additional updates for internal drawing objects with text frames
        if (options?.internal) {
            drawingFrame.addClass(INTERNAL_MODEL_CLASS);
            checkEmptyTextShape(this.docModel, drawingFrame);
        }

        // create the drawing frames of all existing embedded drawing objects (e.g. when cloning)
        drawingModel.forEachChildModel(childModel => this.createDrawingFrame(childModel, options));

        return drawingFrame;
    }

    /**
     * Moves the DOM drawing frame associated to the passed drawing model to a
     * new position in the DOM tree represented by this instance.
     *
     * @param drawingModel
     *  The model of a drawing object whose DOM drawing frame will be moved.
     *
     * @returns
     *  The DOM drawing frame for the passed drawing model that has been moved
     *  to its current position; or `undefined`, if the drawing frame could not
     *  be moved correctly.
     */
    moveDrawingFrame(drawingModel: SheetDrawingModelType): Opt<JQuery> {

        // get the drawing frame from the map
        const drawingFrame = this.#drawingFrameMap.get(drawingModel);
        if (!drawingFrame) { return undefined; }

        // the new container node for the drawing frame
        const containerNode = this.#getContainerNode(drawingModel);
        if (!containerNode) { return undefined; }

        // detach the drawing frame (text framework expects empty span elements
        // between top-level drawings)
        drawingFrame.prev("span").remove();
        drawingFrame.detach();

        // insert the drawing frame at the correct position into the DOM collection
        this.#insertDrawingFrame(drawingModel, containerNode, drawingFrame);
        return drawingFrame;
    }

    /**
     * Removes the DOM drawing frame associated to the passed drawing model
     * from the DOM tree represented by this instance.
     *
     * @param drawingModel
     *  The model of a drawing object whose DOM drawing frame will be removed.
     */
    removeDrawingFrame(drawingModel: SheetDrawingModelType): void {

        // remove the drawing frame from the DOM container
        map.visit(this.#drawingFrameMap, drawingModel, drawingFrame => {

            // text framework expects empty span elements between top-level drawings
            drawingFrame.prev("span").remove();

            // remove the drawing frame (this removes all embedded drawing frames too)
            this.docApp.destroyImageNodes(drawingFrame);
            drawingFrame.remove();
        });

        // remove the drawing frames of all embedded child objects from the map
        const removeDrawingFrameFromMap = (parentModel: SheetDrawingModelType): void => {
            // remove the drawing frame from the local cache
            this.#drawingFrameMap.delete(parentModel);
            // remove the drawing frames of all embedded drawing models
            parentModel.forEachChildModel(removeDrawingFrameFromMap);
        };
        removeDrawingFrameFromMap(drawingModel);
    }

    /**
     * Removes all DOM drawing frames contained in this instance.
     */
    removeAllDrawingFrames(): void {
        this.docApp.destroyImageNodes(this.rootNode);
        this.#drawingFrameMap.clear();
        this.#clearRootNode();
    }

    /**
     * Updates the CSS text formatting of the text contents in the drawing
     * frame associated to the passed drawing model.
     *
     * @param drawingModel
     *  The model of the drawing objects whose text formatting will be updated.
     */
    updateTextFormatting(drawingModel: SheetDrawingModelType): void {

        // nothing to do, if the drawing frame or text frame does not exist (yet)
        map.visit(this.#drawingFrameMap, drawingModel, drawingFrame => {
            withTextFrame(drawingFrame, textFrame => {

                // list of all paragraph child nodes of the drawing frame
                const allParaNodes = textFrame.children(PARAGRAPH_NODE_SELECTOR);
                // drawing object contains default text formatting for all paragraphs
                const updateOptions = { baseAttributes: drawingModel.getMergedAttributeSet(true) };

                // update paragraph/character formatting of the embedded paragraph nodes
                const { paragraphStyles } = this.docModel;
                allParaNodes.get().forEach(paraNode => paragraphStyles.updateElementFormatting(paraNode, updateOptions));

                // update the lists (TODO: handle the promise?)
                jpromise.floating(this.docModel.updateLists(allParaNodes, updateOptions));
            });
        });
    }

    // private methods ----------------------------------------------------

    /**
     * Clears all contents from the root layer node, and prepares it for
     * insertion of drawing frames.
     */
    #clearRootNode(): void {
        replaceChildren(this.rootNode, createSpan({ label: "" }));
    }

    /**
     * Returns the DOM container node that becomes the parent node for the
     * drawing frame associated to the passed drawing model.
     *
     * @param drawingModel
     *  The drawing model to find the DOM conatiner node for.
     *
     * @returns
     *  The DOM container node for the passed drawing model; or `undefined` on
     *  any error.
     */
    #getContainerNode(drawingModel: SheetDrawingModelType): Opt<HTMLElement> {

        // no parent model: the root layer node becomes the container node
        if (!drawingModel.parentModel) { return this.rootNode; }

        // the content node of the parent drawing frame is the container for embedded drawing frames
        const drawingFrame = this.#drawingFrameMap.get(drawingModel.parentModel);
        const contentNode = drawingFrame ? getContentNode(drawingFrame) : undefined;
        if (contentNode?.length === 1) { return contentNode[0]; }

        // failure: cannot resolve parent content node
        renderLogger.error("$badge{FrameNodeManager} getContainerNode: missing container node for parent model");
        return undefined;
    }

    /**
     * Inserts the passed DOM drawing frame into its container node.
     *
     * @param drawingModel
     *  The model of the drawing object.
     *
     * @param containerNode
     *  The container node that will receive the passed drawing frame.
     *
     * @param drawingFrame
     *  The drawing frame to be inserted into the passed container node.
     */
    #insertDrawingFrame(drawingModel: SheetDrawingModelType, containerNode: HTMLElement, drawingFrame: JQuery): void {

        // the insertion position in the node list
        let zIndex = drawingModel.getZIndex();

        // text framework expects empty span elements between top-level drawings
        if (containerNode === this.rootNode) {
            zIndex *= 2;
            insertChildren(containerNode, zIndex, createSpan({ label: "" }));
            zIndex += 1;
        }

        // insert the drawing frame at the correct position into the DOM collection
        insertChildren(containerNode, zIndex, drawingFrame[0]);
    }
}
