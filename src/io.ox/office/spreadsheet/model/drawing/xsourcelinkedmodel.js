/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { is, str, itr, map } from "@/io.ox/office/tk/algorithms";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

// private functions ==========================================================

function mixinSourceLinkedModel(ModelBaseClass) {

    return class extends ModelBaseClass {

        /** Token arrays for dynamic source ranges, mapped by internal link keys. */
        #tokenArrayMap = new Map/*<string, SheetTokenArray>*/();

        /** Maps internal link keys to fully qualified attribute names. */
        #qNameMap = new Map/*<string, [family,attr]>*/();

        // constructor --------------------------------------------------------

        /**
         * @param {SheetModel} sheetModel
         *  The sheet model that contains the model object.
         *
         * @param {string} linkAttrQNames
         *  The fully qualified names of all formatting attributes (with
         *  leading family name, separated by a period, e.g. "text.link")
         *  containing source link formulas, separated by white-space
         *  characters.
         *
         * @param {unknown[]} baseCtorArgs
         *  All following arguments will be passed to the constructor of the
         *  base class `ModelBaseClass`.
         */
        constructor(sheetModel, linkAttrQNames, ...baseCtorArgs) {

            // base constructor
            super(...baseCtorArgs);

            // the parent sheet model
            this.sheetModel = sheetModel;

            // split the passed fully-qualified attribute names into family name and attribute name
            for (const qName of str.splitTokens(linkAttrQNames)) {
                this.#qNameMap.set(qName, qName.split("."));
            }

            // additional processing and event handling after the document has been imported
            this.waitForImportSuccess(() => {

                // create the token arrays after construction (but wait after the document has been imported,
                // otherwise the sheets referred by the token arrays may be missing)
                this.#updateTokenArrays();

                // update the token arrays after any formatting attributes have changed
                this.on("change:attributes", (_newAttrSet, oldAttrSet) => {
                    this.#updateTokenArrays(oldAttrSet);
                });

                // update the sheet indexes after the sheet collection has been manipulated
                this.listenTo(this.docModel, "transform:sheets", xfVector => {
                    for (const tokenArray of this.#tokenArrayMap.values()) {
                        tokenArray.transformSheets(xfVector);
                    }
                });

                // register this model in the dependency manager for formula recalculation
                this.docModel.dependencyManager.registerLinkedModel(this);
            });
        }

        /*protected override*/ destructor() {
            this.docModel.dependencyManager.unregisterLinkedModel(this);
            super.destructor();
        }

        // public methods -----------------------------------------------------

        /**
         * Returns the reference addresses of the link formulas.
         *
         * @returns {Address}
         *  The reference address of the link formulas.
         */
        getRefAddress() {
            return Address.A1.clone();
        }

        /**
         * Returns the address of the target cell to be used to resolve the
         * dependencies of the link formulas.
         *
         * @returns {Address}
         *  The address of the dependency target cell.
         */
        getDependencyTarget() {
            return Address.A1.clone();
        }

        /**
         * Returns an iterator that visits all token arrays of this instance.
         * The token arrays will be visited in no specific order.
         *
         * @returns {KeyedIterator<SheetTokenArray>}
         *  The new token array iterator. The first element of the result value
         *  is the fully qualified attribute name (attribute family and
         *  attribute name, separated by a period, e.g. "text.link") of the
         *  source link formula, used as internal map key.
         */
        tokenArrays() {
            return this.#tokenArrayMap.entries();
        }

        /**
         * Returns the addresses of the source cell ranges referred by the
         * specified formatting attribute, if existing.
         *
         * @param {string} linkKey
         *  The fully qualified attribute name (attribute family and attribute
         *  name, separated by a period, e.g. "text.link") of the source link
         *  to be resolved.
         *
         * @returns {Range3DArray|null}
         *  An array of cell range addresses (with sheet indexes), if the
         *  specified link is associated with a cell range in the document; or
         *  null, if the specified link does not exist or contains constant
         *  data.
         */
        resolveRanges(linkKey) {
            const tokenArray = this.#tokenArrayMap.get(linkKey);
            return tokenArray ? this.#resolveRangeList(tokenArray) : null;
        }

        /**
         * Returns the current text representation of the source link referred
         * by the specified formatting attribute.
         *
         * @param {string} linkKey
         *  The fully qualified attribute name (attribute family and attribute
         *  name, separated by a period, e.g. "text.link") of the source link
         *  to be resolved.
         *
         * @returns {string}
         *  The current text representation of the source link referred by the
         *  specified formatting attribute.
         */
        resolveText(linkKey) {

            // resolve an existing link into the document
            const tokenArray = this.#tokenArrayMap.get(linkKey);
            if (tokenArray) {

                // invalid source links result in the #REF! error code
                var ranges = this.#resolveRangeList(tokenArray);
                if (ranges.empty()) {
                    return this.docModel.formulaGrammarUI.REF_ERROR;
                }

                // resolve cell display strings (also from hidden columns/rows, but skip blank cells)
                var contents = this.docModel.getRangeContents(ranges, { display: true, maxCount: 100 });

                // skip cells with empty display string
                contents = contents.filter(entry => entry.display.length > 0);

                // concatenate all cell display strings with a simple space
                return contents.map(entry => entry.display).join(" ");
            }

            // get the original attribute value for the specified source link
            const qName = this.#qNameMap.get(linkKey);
            const attrValue = qName && this.getMergedAttributeSet(true)[qName[0]]?.[qName[1]];

            // filter non-empty strings from an array, concatenate all strings with a simple space
            return is.array(attrValue) ? attrValue.filter(element => is.string(element) && element.length).join(" ") : "";
        }

        /**
         * Returns whether any of the source links contained in this object
         * overlaps with any of the passed cell ranges.
         *
         * @param {Range3DSource} ranges
         *  The addresses of the cell ranges, or a single cell range address,
         *  to be checked. The cell range addresses MUST be instances of the
         *  class Range3D with sheet indexes.
         *
         * @returns {boolean}
         *  Whether any of the passed ranges overlaps with the source links of
         *  this object.
         */
        rangesOverlap(ranges) {
            return itr.some(this.#tokenArrayMap.values(), tokenArray => this.#resolveRangeList(tokenArray).overlaps(ranges));
        }

        /**
         * Invokes the specified callback function for all existing token
         * arrays of this object.
         *
         * @param {Function} callback
         *  The callback function to be invoked for each existing token array.
         *  Receives the following parameters:
         *  (1) {TokenArray} tokenArray
         *      The token array representing the source link formula.
         *  (2) {String} linkKey
         *      The fully qualified attribute name (attribute family and
         *      attribute name, separated by a period, e.g. "text.link").
         *  (3) {String} attrName
         *      The name of the formatting attribute containing the formula
         *      expression of the source link.
         *  (4) {String} attrFamily
         *      The family name of the formatting attribute containing the
         *      formula expression of the source link.
         */
        iterateTokenArrays(callback) {
            for (const [linkKey, tokenArray] of this.#tokenArrayMap) {
                const qName = this.#qNameMap.get(linkKey);
                if (qName) { callback(tokenArray, linkKey, qName[1], qName[0]); }
            }
        }

        /**
         * Fires a "refresh:formulas" event to the listeners of this drawing
         * model, after the source data referred by the source link formulas
         * has been changed. This method will be called automatically by the
         * dependency manager of the spreadsheet document.
         */
        refreshFormulas() {
            this.trigger("refresh:formulas");
        }

        // operation generators -----------------------------------------------

        /**
         * Collects the transformed formula expressions of all source links of
         * this object.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @param {Dict} operAttrSet
         *  An (incomplete) attribute set that will be extended with the new
         *  formula expressions.
         *
         * @param {Dict} [undoAttrSet]
         *  If specified, the (incomplete) undo attribute set that will be
         *  extended with the current formula expression.
         */
        implTransformAttributes(updateTask, operAttrSet, undoAttrSet) {

            // the formula grammar used for formula expression transformations
            const grammar = this.docModel.formulaGrammarOP;

            // collect the changed formula expressions
            this.iterateTokenArrays((tokenArray, _linkKey, attrName, attrFamily) => {
                updateTask.transformOpAttribute(tokenArray, grammar, attrFamily, attrName, operAttrSet, undoAttrSet);
            });
        }

        /**
         * Generates the operations and undo operations to update or restore
         * the formula expressions of all source links of this object.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operations cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @param {String} opName
         *  The name of the document operation to be generated for the formula
         *  expressions.
         *
         * @param {Position} position
         *  The position of the parent drawing object in the sheet.
         *
         * @param {Object} [properties]
         *  Additional properties to be inserted into the document operations.
         */
        implGenerateUpdateTaskOperations(sheetCache, updateTask, opName, position, properties, options) {

            // collect the changed formula expressions
            const operAttrSet = {};
            const undoAttrSet = {};
            this.implTransformAttributes(updateTask, operAttrSet, undoAttrSet);

            // generate the operations, if any formula expression has changed
            if (!is.empty(operAttrSet)) {
                const operProps = { ...properties, attrs: operAttrSet };
                sheetCache.generateDrawingOperation(opName, position, operProps, options);
                const undoProps = { ...properties, attrs: undoAttrSet };
                sheetCache.generateDrawingOperation(opName, position, undoProps, { undo: true });
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Updates the token arrays, after the formatting attributes of this
         * instance have been changed.
         */
        #updateTokenArrays(oldAttrSet) {

            // the current merged attribute set
            const attrSet = this.getMergedAttributeSet(true);
            // whether any source link attribute has been changed
            let changedTokens = false;

            // process all source link attributes in the current merged attribute set
            for (const [linkKey, [attrFamily, attrName]] of this.#qNameMap) {

                // the attribute value (string is a formula expression, array represents constant data)
                const oldValue = oldAttrSet ? oldAttrSet[attrFamily][attrName] : null;
                const newValue = attrSet[attrFamily][attrName];
                if (_.isEqual(oldValue, newValue)) { continue; }

                // create, update, or delete the token array according to type of source data
                if (is.string(newValue) && newValue.length) {
                    // string: parse link formula
                    map.upsert(this.#tokenArrayMap, linkKey, () => new TokenArray(this.sheetModel, "link")).parseFormula("op", newValue, Address.A1);
                    // token array has been created or changed: notify listeners
                    changedTokens = true;
                } else {
                    // else: constant source data, or invalid attribute value: delete token array
                    if (this.#tokenArrayMap.delete(linkKey)) {
                        changedTokens = true;
                    }
                }
            }

            // notify changed source link attributes
            if (changedTokens) {
                this.trigger("change:sourcelinks");
            }
        }

        /**
         * Interprets the passed token array, and returns the resulting cell
         * range addresses referred by the formula expression.
         *
         * @param {TokenArray} tokenArray
         *  The token array to be resolved to the cell range addresses.
         *
         * @returns {Range3DArray}
         *  An array with cell range addresses (with sheet indexes). If the
         *  token array cannot be evaluated successfully, an empty array will
         *  be returned.
         */
        #resolveRangeList(tokenArray) {
            return tokenArray.resolveRangeList(Address.A1, Address.A1, { resolveNames: "interpret" });
        }
    };
}

// exports ====================================================================

export const XSourceLinkedModel = {

    /**
     * Creates and returns a subclass of the passed model class, with new
     * methods specific to object models referring dynamically to cell source
     * data in the spreadsheet document via formula expressions stored in
     * formatting attributes.
     *
     * @param {CtorType<ModelT extends AttributedModel>} ModelBaseClass
     *  The model base class to be extended.
     *
     * @returns {CtorType<ModelT & XSourceLinkedModel>}
     *  The extended model class with support for source links into the
     *  spreadsheet document.
     */
    mixin: mixinSourceLinkedModel
};
