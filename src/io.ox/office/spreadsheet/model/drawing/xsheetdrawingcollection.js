/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, itr } from "@/io.ox/office/tk/algorithms";
import { convertHmmToLength } from "@/io.ox/office/tk/dom";
import { getChildNodePositionInNode } from "@/io.ox/office/tk/utils";

import { setExplicitAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { updateTextFramePosition } from "@/io.ox/office/drawinglayer/view/drawingframe";

import { Interval, AnchorMode, CellAnchorType, CellAnchorPoint, CellAnchor, modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { FrameNodeManager } from "@/io.ox/office/spreadsheet/model/drawing/framenodemanager";
import { withTextFrame } from "@/io.ox/office/spreadsheet/model/drawing/text/textframeutils";

const { max, round } = Math;

// private functions ==========================================================

function mixinSheetDrawingCollection(DrawingCollectionBaseClass) {

    class SheetDrawingCollection extends DrawingCollectionBaseClass {

        /** The manager for the DOM drawing frames of the drawing models in this collection. */
        #frameNodeManager/*: FrameNodeManager*/;

        /** Cached settings for drawing model frames, mapped by drawing model. */
        #modelFrameCache = new Map/*<SheetDrawingModelType, RenderSettings>*/();

        /** The column collections (size and formatting of all columns). */
        #colCollection/*: ColRowCollection*/;
        /** The row collections (size and formatting of all rows). */
        #rowCollection/*: ColRowCollection*/;

        /** Special behavior in OOXML documents. */
        #ooxml = this.docApp.isOOXML();

        // constructor --------------------------------------------------------

        /**
         * @param {SheetModel} sheetModel
         *  The sheet model that contains this drawing collection.
         *
         * @param {unknown[]} baseCtorArgs
         *  All following arguments will be passed to the constructor of the
         *  base class `ModelBaseClass`.
         */
        constructor(sheetModel, ...baseCtorArgs) {

            // base constructor
            super(...baseCtorArgs);

            // the parent sheet model
            this.sheetModel = sheetModel;

            // private properties
            this.#frameNodeManager = this.member(new FrameNodeManager(this.docModel));
            this.#colCollection = sheetModel.colCollection;
            this.#rowCollection = sheetModel.rowCollection;

            // create the DOM drawing frame for a new drawing model
            this.on("insert:drawing", drawingModel => {
                this.#frameNodeManager.createDrawingFrame(drawingModel, { internal: true });
            });

            // remove the DOM drawing frame of a deleted drawing model
            this.on("delete:drawing", drawingModel => {
                this.#frameNodeManager.removeDrawingFrame(drawingModel);
                this.#modelFrameCache.delete(drawingModel);
            });

            // move the DOM drawing frame of a drawing model to its new position (Z order)
            this.on("move:drawing", drawingModel => {
                this.#frameNodeManager.moveDrawingFrame(drawingModel);
            });

            // remove cached model frame settings if formatting or text contents have changed
            this.on("change:drawing", (drawingModel, changeType) => {
                if ((changeType === "attributes") || (changeType === "text")) {
                    this.#modelFrameCache.delete(drawingModel);
                }
            });
        }

        // public methods -----------------------------------------------------

        /**
         * Returns the DOM container node for the DOM models of all drawing
         * objects in this collection.
         *
         * @returns {HTMLElement}
         *  The DOM container node for the DOM models of all drawing objects in
         *  this collection.
         */
        getContainerNode() {
            return this.#frameNodeManager.rootNode;
        }

        /**
         * Returns the DOM drawing frame that represents the drawing model at
         * the specified document position. The DOM drawing frame is used to
         * store the text contents of the drawing objects.
         *
         * @param {Position} position
         *  The document position of a drawing model to return the DOM drawing
         *  frame for.
         *
         * @returns {Opt<JQuery>}
         *  The DOM drawing frame for the specified position; or `undefined`,
         *  if no drawing frame could be found.
         */
        getDrawingFrame(position) {
            const drawingModel = this.getModel(position);
            return drawingModel && this.#frameNodeManager.getDrawingFrame(drawingModel);
        }

        /**
         * Returns the DOM drawing frame that represents the passed drawing
         * model. The DOM drawing frame is used to store the text contents of
         * the drawing objects.
         *
         * @param {DrawingModel} drawingModel
         *  The drawing model instance to return the DOM drawing frame for.
         *
         * @returns {Opt<JQuery>}
         *  The DOM drawing frame for the passed drawing model; or `undefined`,
         *  if no drawing frame could be found.
         */
        getDrawingFrameForModel(drawingModel) {
            return this.#frameNodeManager.getDrawingFrame(drawingModel);
        }

        /**
         * Refreshes the settings of the DOM drawing frame associated with the
         * passed drawing model, and returns the cached rendering settings for
         * the drawing model.
         *
         * @param {DrawingModel} drawingModel
         *  The drawing model whose DOM drawing frame will be refreshed.
         *
         * @returns {Opt<object>}
         *  A descriptor with rendering data for the passed drawing model; or
         *  `undefined`, if no data is available for the drawing model. The
         *  descriptor will contain the following properties:
         *  - {JQuery} drawingFrame
         *    The DOM drawing frame associated with the drawing model. This
         *    drawing frame serves as model container for the text contents of
         *    shape objects.
         *  - {Dict} explicitAttrSet
         *    A deep copy of the explicit attribute set of the drawing model.
         *  - {object} [textPosition]
         *    The position of the text frame inside the drawing frame at a
         *    fixed zoom level of 100%, as object with the properties `left`,
         *    `top`, `right`, `bottom`, `width`, and `height`, in pixels. If
         *    the drawing model does not contain a text frame, this property
         *    will be omitted.
         */
        refreshDrawingFrameForModel(drawingModel) {

            // directly return existing cached settings for the passed drawing model
            let renderSettings = this.#modelFrameCache.get(drawingModel);
            if (renderSettings) { return renderSettings; }

            // resolve the DOM drawing frame, do nothing if the frame does not exist yet
            const drawingFrame = drawingModel.getModelFrame();
            if (!drawingFrame) { return undefined; }

            // create a new entry in the drawing model cache
            this.#modelFrameCache.set(drawingModel, renderSettings = { drawingFrame });

            // copy explicit formatting attributes (default character formatting)
            renderSettings.explicitAttrSet = drawingModel.getExplicitAttributeSet();
            setExplicitAttributeSet(drawingFrame, renderSettings.explicitAttrSet);

            // set the pixel size of the drawing frame (always for 100% zoom factor to get the original padding!)
            var anchorRect = drawingModel.getRectangleHmm();
            if (this.#ooxml && drawingModel.hasSwappedDimensions()) { anchorRect.rotateSelf(); }
            var width = convertHmmToLength(anchorRect.width, "px", 1);
            var height = convertHmmToLength(anchorRect.height, "px", 1);
            drawingFrame.css({ width, height });

            // additional processing for the embedded text frame
            withTextFrame(drawingFrame, textFrame => {

                // update the text frame position, but not the entire formatting (especially, do not create canvas elements!)
                const mergedAttrSet = drawingModel.getMergedAttributeSet(true);
                updateTextFramePosition(this.docApp, drawingFrame, mergedAttrSet);

                // reset CSS transformations before measuring DOM positions
                var drawingTransform = drawingFrame.css("transform");
                var textTransform = textFrame.css("transform");
                drawingFrame.css("transform", "");
                textFrame.css("transform", "");

                // store the padding of the text frame to its enclosing drawing frame (to be used by renderers)
                renderSettings.textPosition = getChildNodePositionInNode(drawingFrame, textFrame);

                // restore the CSS transformations
                drawingFrame.css("transform", drawingTransform);
                textFrame.css("transform", textTransform);
            });

            return renderSettings;
        }

        /**
         * Updates the CSS text formatting of the text contents in the drawing
         * frame associated to the passed drawing model.
         *
         * @param {DrawingModel} drawingModel
         *  The model of the drawing objects whose text formatting will be
         *  updated.
         */
        updateTextFormatting(drawingModel) {
            this.#frameNodeManager.updateTextFormatting(drawingModel);
        }

        /**
         * Returns whether the passed ranges can be deleted, i.e. if the ranges
         * do not contain drawing objects that cannot be restored with undo
         * operations.
         *
         * @param {RangeArray} ranges
         *  The addresses of the cell ranges to be deleted.
         *
         * @returns {boolean}
         *  Whether the cell ranges can be deleted safely.
         */
        canRestoreDeletedRanges(ranges) {
            return itr.every(this.yieldModels(), drawingModel => {

                // absolutely positioned or sized drawings will not be deleted
                const anchorMode = drawingModel.getAnchorMode(true);
                if (anchorMode !== AnchorMode.TWO_CELL) { return true; }

                // Check that the drawing object can be restored deeply (all children of groups).
                // Return false if it will be deleted and cannot be restored.
                return drawingModel.isDeepRestorable() || !ranges.contains(drawingModel.getRange());
            });
        }

        /**
         * Returns the sheet rectangle in pixels covered by the passed drawing
         * object.
         *
         * @param {DrawingModel} drawingModel
         *  The drawing object to return the rectangle for.
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {boolean} [options.absolute=false]
         *    If set to `true`, this method will return the absolute sheet
         *    position also for drawing objects embedded into other drawing
         *    objects. By default, this method returns the location of the
         *    specified drawing object relative to its parent object.
         *
         * @returns {Rectangle|null}
         *  The location of the specified drawing object in pixels according to
         *  the current sheet zoom factor, either relative to its parent object
         *  (sheet area for top-level drawing objects), or always absolute in
         *  the sheet area (according to the passed options); or null, if the
         *  drawing object is located entirely in hidden columns or rows and
         *  thus has no meaningful location.
         */
        getRectangleForModel(drawingModel, options) {

            // the top-level drawing model containing the passed drawing model
            const { rootModel } = drawingModel;
            // the complete anchor descriptor of the top-level drawing
            const sheetAnchor = rootModel.getSheetAnchor();

            // return null for all drawing objects (top-level and embedded) in hidden columns/rows
            const addr1 = sheetAnchor.cellAnchor.point1.a;
            const addr2 = sheetAnchor.cellAnchor.point2.a;
            if (this.#colCollection.isIntervalHidden(new Interval(addr1.c, addr2.c)) || this.#rowCollection.isIntervalHidden(new Interval(addr1.r, addr2.r))) {
                return null;
            }

            // the resulting rectangle in pixels
            let anchorRect = null;
            // the direct parent of the passed drawing model (`undefined` for top-level drawings)
            const { parentModel } = drawingModel;
            // whether to convert to effective absolute coordinates
            const absolute = options?.absolute;

            if (parentModel) {

                // the exact location of the drawing model in 1/100 mm
                var childRectHmm = absolute ? drawingModel.getEffectiveRectangleHmm() : drawingModel.getRectangleHmm();
                // the exact location of the parant drawing model in 1/100 mm
                var parentRectHmm = absolute ? sheetAnchor.rectHmm.rotatedBoundingBox(rootModel.getRotationRad()) : parentModel.getRectangleHmm();
                // the exact location of the parant drawing model in pixels
                var parentRectPx = absolute ? sheetAnchor.rectPx.rotatedBoundingBox(rootModel.getRotationRad()) : this.getRectangleForModel(parentModel);

                // conversion factor for left position and width
                var widthRatio = (parentRectHmm.width > 0) ? (parentRectPx.width / parentRectHmm.width) : 0;
                // conversion factor for top position and height
                var heightRatio = (parentRectHmm.height > 0) ? (parentRectPx.height / parentRectHmm.height) : 0;

                // the location of the drawing object, relative to its parent object
                anchorRect = childRectHmm.clone().scaleSelf(widthRatio, heightRatio).roundSelf();
            } else {
                // position of top-level drawings is contained in the anchor descriptor
                anchorRect = absolute ? sheetAnchor.rectPx.rotatedBoundingBox(rootModel.getRotationRad()) : sheetAnchor.rectPx;
            }

            // keep minimum size of 5 pixels for shapes, or 1 pixel for connectors
            var minSize = drawingModel.getMinSize();
            if (anchorRect.width < minSize) {
                anchorRect.left = max(0, anchorRect.left - round((minSize - anchorRect.width) / 2));
                anchorRect.width = minSize;
            }
            if (anchorRect.height < minSize) {
                anchorRect.top = max(0, anchorRect.top - round((minSize - anchorRect.height) / 2));
                anchorRect.height = minSize;
            }

            return anchorRect;
        }

        // anchor attribute generators ----------------------------------------

        /**
         * Returns the anchor attributes needed to set the position and size of
         * a drawing object to the passed sheet rectangle.
         *
         * @param {AnchorMode} anchorMode
         *  The display anchor mode to be converted to an appropriate value for
         *  the "anchor" attribute.
         *
         * @param {Rectangle} anchorRect
         *  The position of a drawing object in the sheet, either in pixels
         *  according to the current sheet zoom factor, or in 1/100 mm (see
         *  option `pixel`).
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {boolean} [options.pixel=false]
         *    If set to `true`, the passed anchor rectangle is interpreted as
         *    pixels according to the current sheet zoom factor. Otherwise, the
         *    anchor rectangle is interpreted in 1/100 of millimeters.
         *
         * @returns {Dict}
         *  The anchor attributes for the passed anchor mode and rectangle.
         */
        getAnchorAttributesForRect(anchorMode, anchorRect, options) {
            return this.#getAnchorAttrsForRect(this.#getAnchorType(anchorMode), anchorRect, options);
        }

        /**
         * Returns the anchor attributes needed to set the position and size of
         * a drawing object to the passed cell address and absolute size.
         *
         * @param {AnchorMode} anchorMode
         *  The anchor mode to be converted to an appropriate value for the
         *  "anchor" attribute.
         *
         * @param {Address} address
         *  The address of the cell containing the top-left corner of the
         *  drawing object.
         *
         * @param {number} width
         *  The width of the drawing object, in pixels or 1/100 mm (see option
         *  `pixel`).
         *
         * @param {number} height
         *  The height of the drawing object, in pixels or 1/100 mm (see option
         *  `pixel`).
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {boolean} [options.pixel=false]
         *    If set to `true`, the passed size is interpreted as pixels
         *    according to the current sheet zoom factor. Otherwise, the size
         *    is interpreted in 1/100 of millimeters.
         *
         * @returns {Dict}
         *  The anchor attributes for the passed anchor mode and location.
         */
        getAnchorAttributesForCellAndSize(anchorMode, address, width, height, options) {
            const anchorRect = this.sheetModel.getCellRectangle(address);
            anchorRect.width = width;
            anchorRect.height = height;
            return this.#getAnchorAttrsForRect(this.#getAnchorType(anchorMode), anchorRect, options);
        }

        /**
         * Calculates the current effective cell anchor attributes of the
         * specified top-level drawing object.
         *
         * @param {DrawingModel} drawingModel
         *  The model of a drawing object.
         *
         * @param {AnchorMode} [anchorMode]
         *  If specified, this method will generate the anchor attributes
         *  needed to change the display anchor mode to the passed value. By
         *  default, the returned attributes will contain the current anchor
         *  type attribute of the passed drawing model.
         *
         * @returns {Opt<Dict>}
         *  The anchor attributes that need to be applied at the drawing object
         *  to update its location; or `undefined`, if the drawing object does
         *  not need to be updated.
         */
        generateAnchorAttributes(drawingModel, anchorMode) {

            // the complete anchor descriptor with current settings
            const sheetAnchor = drawingModel.getSheetAnchor({ displayMode: true });

            // change the anchor type if specified
            if (!is.nullish(anchorMode)) {
                sheetAnchor.cellAnchor.type = this.#getAnchorType(anchorMode);
            }

            // check if the anchor attributes do change at all
            const anchorAttrs = sheetAnchor.createAttributes();
            return drawingModel.getChangedAnchorAttributes(anchorAttrs);
        }

        /**
         * Calculates the effective cell anchor attributes needed to move the
         * specified top-level drawing object to an absolute sheet rectangle.
         *
         * @param {DrawingModel} drawingModel
         *  The model of a drawing object.
         *
         * @param {Rectangle} anchorRect
         *  The position of a drawing object in the sheet, either in pixels
         *  according to the current sheet zoom factor, or in 1/100 mm (see
         *  option `pixel`).
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {boolean} [options.pixel=false]
         *    If set to `true`, the passed anchor rectangle is interpreted as
         *    pixels according to the current sheet zoom factor. Otherwise, the
         *    anchor rectangle is interpreted in 1/100 of millimeters.
         *
         * @returns {Opt<Dict>}
         *  The anchor attributes that need to be applied at the drawing object
         *  to update its location; or `undefined`, if the drawing object does
         *  not need to be updated.
         */
        generateAnchorAttributesForRect(drawingModel, anchorRect, options) {

            // calculate the anchor attributes for the passed rectangle
            const anchorAttrs = this.#getAnchorAttrsForRect(drawingModel.getAnchorType(), anchorRect, options);

            // check if the anchor attributes do change at all
            return drawingModel.getChangedAnchorAttributes(anchorAttrs);
        }

        // operation generators -----------------------------------------------

        /**
         * Generates the operations and undo operations to update and restore
         * the inactive anchor attributes of the top-level drawing objects,
         * after the size or visibility of the columns or rows in the sheet has
         * been changed.
         *
         * @param {OperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @returns {JPromise<void>}
         *  A promise that will fulfil when all operations have been generated.
         */
        @modelLogger.profileMethod("$badge{SheetDrawingCollectionBase} generateRefreshAnchorOperations")
        generateRefreshAnchorOperations(generator) {
            return this.asyncForEach(this.yieldModels(), drawingModel => {
                drawingModel.generateRefreshAnchorOperations(generator);
            });
        }

        /**
         * Generates the operations and undo operations to update and restore
         * the formula expressions of the source links in all drawing objects
         * that refer to some cells in this document.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operation cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         *
         * @returns {JPromise<void>}
         *  A promise that will fulfil when all operations have been generated.
         */
        @modelLogger.profileMethod("$badge{SheetDrawingCollectionBase} generateUpdateTaskOperations")
        generateUpdateTaskOperations(sheetCache, updateTask) {
            // bug 58153: do not iterate deeply into the drawing objects, they will update their children by themselves
            return this.asyncForEach(this.yieldModels(), drawingModel => {
                drawingModel.generateUpdateTaskOperations(sheetCache, updateTask);
            });
        }

        // private methods ----------------------------------------------------

        /**
         * Returns the anchor type attribute value for the specified display
         * anchor mode.
         *
         * @param {AnchorMode} anchorMode
         *  The display anchor mode to be converted to the attribute value.
         *
         * @returns {CellAnchorType}
         *  The resulting value for the attribute "anchor".
         */
        #getAnchorType(anchorMode) {
            // prefer internal two-cell anchor for OOXML
            switch (anchorMode) {
                case AnchorMode.ABSOLUTE:
                    return this.#ooxml ? CellAnchorType.TWO_CELL_AS_ABSOLUTE : CellAnchorType.ABSOLUTE;
                case AnchorMode.ONE_CELL:
                    return this.#ooxml ? CellAnchorType.TWO_CELL_AS_ONE_CELL : CellAnchorType.ONE_CELL;
                case AnchorMode.TWO_CELL:
                    return CellAnchorType.TWO_CELL;
            }
        }

        /**
         * Returns the complete anchor attributes needed to set the position
         * and size of a drawing object to the passed absolute rectangle in the
         * sheet area.
         *
         * @param {CellAnchorType} anchorType
         *  The anchor type to be inserted into the anchor attributes.
         *
         * @param {Rectangle} anchorRect
         *  The position of a drawing object in the sheet, either in pixels
         *  according to the current sheet zoom factor, or in 1/100 mm (see
         *  option `pixel`).
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {boolean} [options.pixel=false]
         *    If set to `true`, the passed anchor rectangle is interpreted as
         *    pixels according to the current sheet zoom factor. Otherwise, the
         *    anchor rectangle is interpreted in 1/100 of millimeters.
         *
         * @returns {Dict}
         *  The anchor attributes for the location of a drawing object.
         */
        #getAnchorAttrsForRect(anchorType, anchorRect, options) {

            // the new anchor attributes
            const anchorAttrs = {};

            // absolute position
            const col1Data = this.#colCollection.getEntryByOffset(anchorRect.left, options);
            const row1Data = this.#rowCollection.getEntryByOffset(anchorRect.top, options);
            anchorAttrs.left = col1Data.offsetHmm + col1Data.relOffsetHmm;
            anchorAttrs.top = row1Data.offsetHmm + row1Data.relOffsetHmm;

            // total object size
            const col2Data = this.#colCollection.getEntryByOffset(anchorRect.right(), options);
            const row2Data = this.#rowCollection.getEntryByOffset(anchorRect.bottom(), options);
            anchorAttrs.width = col2Data.offsetHmm + col2Data.relOffsetHmm - anchorAttrs.left;
            anchorAttrs.height = row2Data.offsetHmm + row2Data.relOffsetHmm - anchorAttrs.top;

            // cell anchor
            const point1 = CellAnchorPoint.createFromColRow(col1Data, row1Data);
            const point2 = CellAnchorPoint.createFromColRow(col2Data, row2Data);
            anchorAttrs.anchor = new CellAnchor(anchorType, point1, point2).toOpStr();

            return anchorAttrs;
        }
    }

    return SheetDrawingCollection;
}

// exports ====================================================================

export const XSheetDrawingCollection = {

    /**
     * Creates and returns a subclass of the passed drawing collection class,
     * with mix-in methods specific to spreadsheet documents.
     *
     * @param {CtorType<CollectionT extends DrawingCollection>} DrawingCollectionBaseClass
     *  The drawing collection base class to be extended.
     *
     * @returns {CtorType<CollectionT & XSheetDrawingCollection>}
     *  The extended drawing collection class for spreadsheet documents.
     */
    mixin: mixinSheetDrawingCollection
};
