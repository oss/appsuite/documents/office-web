/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ExtAttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";
import { DrawingAttributes, DrawingAttributeSet, DrawingModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import { XSheetDrawingModel } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface SheetDrawingAttributes extends DrawingAttributes {

    /**
     * The type and positions of the drawing cell anchor.
     */
    anchor: string;
}

// constants ==================================================================

export const DRAWING_ATTRIBUTES: ExtAttributeConfigBag<SheetDrawingAttributes, DrawingAttributes>;

// class SheetDrawingModel ====================================================

export interface SheetDrawingModel extends DrawingModel<SpreadsheetModel, DrawingAttributeSet, XSheetDrawingModel>, XSheetDrawingModel { }

export class SheetDrawingModel extends DrawingModel<SpreadsheetModel, DrawingAttributeSet, XSheetDrawingModel> implements XSheetDrawingModel { }
