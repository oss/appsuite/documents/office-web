/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TEXTFRAMECONTENT_NODE_CLASS, getContentNode, getTextFrameNode } from "@/io.ox/office/drawinglayer/view/drawingframe";

// public functions ===========================================================

/**
 * Returns the DOM text frame node embedded in the passed DOM drawing frame.
 *
 * @param drawingFrame
 *  The DOM drawing frame with an optional embedded text frame. May be nullish,
 *  in this case the method will return `undefined`.
 *
 * @returns
 *  The DOM text frame embedded in the passed drawing frame; or `undefined`, if
 *  the drawing frame does not contain a text frame.
 */
export function getTextFrame(drawingFrame: Nullable<JQuery>): Opt<JQuery> {
    const textFrame = drawingFrame && getTextFrameNode(drawingFrame);
    return (textFrame?.length === 1) ? textFrame : undefined;
}

/**
 * Invokes the callback function with the DOM text frame node embedded in the
 * passed DOM drawing frame node.
 *
 * @param drawingFrame
 *  The DOM drawing frame with an optional embedded text frame. May be nullish,
 *  in this case the method will do nothing, and will return `undefined`.
 *
 * @param callback
 *  The callback function that will be invoked if the passed drawing frame
 *  contains a text frame. Receives the text frame node as first parameter.
 *
 * @returns
 *  The return value of the callback function if it has been invoked; otherwise
 *  `undefined`.
 */
export function withTextFrame<T>(drawingFrame: Nullable<JQuery>, callback: (textFrame: JQuery) => T): Opt<T> {
    const textFrame = getTextFrame(drawingFrame);
    return textFrame && callback(textFrame);
}

/**
 * Creates a deep copy of the DOM text frame node in the passed source DOM
 * drawing frame, and inserts this copy into the target DOM drawing frame. The
 * current DOM text frame node of the target drawing frame will be removed. If
 * the source drawing frame does not contain a text frame, this function will
 * do nothing.
 *
 * @param sourceDrawingFrame
 *  The source DOM drawing frame with an optional embedded text frame.
 *
 * @param targetDrawingFrame
 *  The target DOM drawing frame that will receive the copied text frame.
 *
 * @returns
 *  The copied DOM text frame that has been inserted into the target drawing
 *  frame; or `undefined`, if the source drawing frame does not contain a text
 *  frame.
 */
export function copyTextFrame(sourceDrawingFrame: JQuery, targetDrawingFrame: JQuery): Opt<JQuery> {

    // remove the current text frame from the target drawing frame
    getTextFrameNode(targetDrawingFrame).remove();

    // the content node of the target drawing frame
    const targetContentNode = getContentNode(targetDrawingFrame);
    targetContentNode.removeClass(TEXTFRAMECONTENT_NODE_CLASS);

    // copy the text frame from the source drawing frame to the target drawing frame
    return withTextFrame(sourceDrawingFrame, sourceTextFrame => {
        targetContentNode.addClass(TEXTFRAMECONTENT_NODE_CLASS);
        return sourceTextFrame.clone(true, true).appendTo(targetContentNode);
    });
}
