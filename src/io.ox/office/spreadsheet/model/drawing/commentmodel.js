/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is } from '@/io.ox/office/tk/algorithms';

import { DrawingModel } from '@/io.ox/office/drawinglayer/model/drawingmodel';
import { resolveUserId } from '@/io.ox/office/editframework/utils/operationutils';

import { CellAnchor } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { XSheetDrawingModel } from '@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel';

// class CommentSettings ======================================================

/**
 * All additional settings of a spreadsheet cell comment.
 */
export class CommentSettings {

    constructor(anchor, parent, author, authorId, authorProvider, date, text, mentions, done, drawing) {
        this.anchor = anchor.clone();
        this.parent = parent;
        this.author = author || '';
        this.authorId = authorId || '';
        this.authorProvider = authorProvider;
        this.date = date || '';
        this.text = text || '';
        this.mentions = mentions;
        this.done = done || null;
        this.drawing = drawing;
    }

    /**
     * Returns a JSON object representation of the object.
     *
     * @param {boolean} addIndex if true add the index and the parentIndex to the JSON
     *
     * @returns {Object} the settings in a JSONObject
     */
    toJSON(addIndex) {

        var json = { text: this.text, mentions: this.mentions, date: this.date, author: this.author, authorId: this.authorId, authorProvider: this.authorProvider };

        if (!this.parent && this.done) {
            json.done = this.done;
        }

        if (this.drawing) {
            json.drawing = this.drawing;
        }

        if (addIndex) {
            json.index = this.index;

            if (this.parent) {
                json.parent = this.parent;
            }
        }

        return json;
    }

    clone() {
        return new CommentSettings(this.anchor, this.parent, this.author, this.authorId, this.authorProvider, this.date, this.text, this.mentions, this.done, this.drawing);
    }
}

// class CommentModel =========================================================

/**
 * The model of a comment thread.
 *
 * This class is a subclass of `DrawingModel` to represent the fallback
 * note shape owned by every comment thread (to be shown in legacy
 * spreadsheet applications without comment support).
 */
export class CommentModel extends XSheetDrawingModel.mixin(DrawingModel) {

    constructor(sheetModel, parentCollection, initSettings) {

        var settings = initSettings.clone();

        super(sheetModel, parentCollection, 'note', settings.drawing ? { drawing: settings.drawing } : undefined);

        var longDate = Date.parse(settings.date);
        longDate = Number.isFinite(longDate) ? longDate : 0;

        // client API methods -------------------------------------------------

        this.clientCloneModel = function (targetModel, targetCollection) {
            return new CommentModel(targetModel, targetCollection, settings);
        };

        // public methods -----------------------------------------------------

        this.getAnchor = function () {
            return settings.anchor;
        };

        this.getCellAnchor = function () {
            var attrSet = this.getMergedAttributeSet(true);
            return CellAnchor.parseOpStr(attrSet.drawing.anchor);
        };

        this.setAnchor = function (anchor) {
            settings.anchor = anchor;
        };

        this.getAnchorString = function () {
            return settings.anchor.toString();
        };

        this.getAnchorRectangle = function () {
            return sheetModel.getAnchorRectangle(settings.anchor);
        };

        this.getText = function () {
            return settings.text;
        };

        this.setText = function (text) {
            settings.text = text;
        };

        this.getMentions = function () {
            return settings.mentions;
        };

        this.setMentions = function (mentions) {
            settings.mentions = mentions;
        };

        this.getDate = function () {
            return settings.date;
        };

        this.getDateAsNumber = function () {
            return longDate;
        };

        this.getAuthor = function () {
            return settings.author;
        };

        this.getAuthorId = function () {
            return settings.authorId;
        };

        /**
         * Calculating the app suite user id from the authorId.
         * Only used from the class `CommentsPane` to show the author picture and informations.
         *
         * @returns {number}
         */
        this.getInternalUserId = function () {
            var authorId = parseInt(settings.authorId, 10);
            if (is.number(authorId)) {
                return resolveUserId(authorId);
            }
            return -1;
        };

        this.getParent = function () {
            return settings.parent;
        };

        this.setParent = function (parent) {
            settings.parent = parent;
        };

        this.isDone = function () {
            return settings.done ? settings.done : false;
        };

        this.equals = function (commentModel) {
            return this === commentModel;
        };

        this.isReply = function () {
            return !!settings.parent;
        };

        this.getId = function () {
            return this.uid;
        };

        this.clone = function (targetModel, targetCollection) {
            return new CommentModel(targetModel, targetCollection, settings.clone());
        };

        this.getJSONSettings = function () {

            var json = {};
            if (settings.text) { json.text = settings.text; }
            if (settings.mentions) { json.mentions = settings.mentions; }
            if (settings.author) { json.author = settings.author; }
            if (settings.authorId) { json.authorId = settings.authorId; }
            if (settings.authorProvider) { json.authorProvider = settings.authorProvider; }
            if (settings.date) { json.date = settings.date; }

            if (!this.isReply() && settings.done) {
                json.done = settings.done;
            }

            var attrs = this.getMergedAttributeSet(true);
            if (attrs.drawing) {
                json.attrs = { drawing: attrs.drawing };
            }

            return json;
        };

        /**
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.forEachChildModel = function () {
        };

        /**
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.getType = function () {
            return 'note';
        };

        /**
         * Return the zIndex for the drawing frame.
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.getZIndex = function () {
            return 0;
        };
    }
}
