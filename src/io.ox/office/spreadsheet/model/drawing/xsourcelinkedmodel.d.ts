/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { AttrSetConstraint } from "@/io.ox/office/editframework/utils/attributeutils";
import { AttributedModelEventMap, AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { SheetTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Type mapping for the events emitted by `XSourceLinkedModel` instances.
 */
export interface XSourceLinkedModelEventMap<AttrSetT extends AttrSetConstraint<AttrSetT>> extends AttributedModelEventMap<AttrSetT> {
    "change:sourcelinks": [];
}

// mixin XSourceLinkedModel ===================================================

export interface XSourceLinkedModel<
    AttrSetT extends AttrSetConstraint<AttrSetT>,
    EvtMapT extends XSourceLinkedModelEventMap<AttrSetT>
> extends ModelObject<SpreadsheetModel, EvtMapT> {
    readonly sheetModel: SheetModel;
    getRefAddress(): Address;
    getDependencyTarget(): Address;
    tokenArrays(): KeyedIterator<SheetTokenArray>;
    refreshFormulas(): void;
}

export namespace XSourceLinkedModel {
    export function mixin<
        AttrSetT extends AttrSetConstraint<AttrSetT>,
        EvtMapT extends XSourceLinkedModelEventMap<AttrSetT>
    >(ModelBaseClass: CtorType<AttributedModel<SpreadsheetModel, any, any, EvtMapT>>): CtorType<AttributedModel<SpreadsheetModel, any, any, EvtMapT> & XSourceLinkedModel<AttrSetT, EvtMapT>>;
}
