/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Rectangle } from "@/io.ox/office/tk/dom";
import { ShapeAttributeSet, DrawingModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { XSheetDrawingModel } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface NoteAttributeSet extends ShapeAttributeSet { }

// class NoteModel ============================================================

export interface NoteModel extends DrawingModel<SpreadsheetModel, NoteAttributeSet, XSheetDrawingModel>, XSheetDrawingModel { }

export class NoteModel extends DrawingModel<SpreadsheetModel, NoteAttributeSet, XSheetDrawingModel> implements XSheetDrawingModel {
    getAnchor(): Address;
    getAnchorRectangle(): Rectangle;
    getText(): string;
}
