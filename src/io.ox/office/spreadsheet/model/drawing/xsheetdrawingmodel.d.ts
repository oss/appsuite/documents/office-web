/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { RectangleLike, Rectangle } from "@/io.ox/office/tk/dom";
import { Position } from "@/io.ox/office/editframework/utils/operations";
import { PtDrawingAttributeSet, DrawingModel, DrawingModelType, DrawingModelIterator } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { SheetDrawingAnchor } from "@/io.ox/office/spreadsheet/utils/cellanchor";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface DrawingRectangleOptions {
    absolute?: boolean;
}

export interface DisplayModeOptions {
    displayMode?: boolean;
}

export interface ModelFrameSettings {
    drawingFrame: JQuery;
    explicitAttrSet: PtDrawingAttributeSet;
    textPosition?: RectangleLike;
}

export type SheetDrawingModelType = DrawingModelType<SpreadsheetModel, XSheetDrawingModel>;

export type SheetDrawingModelIterator = DrawingModelIterator<SpreadsheetModel, XSheetDrawingModel>;

// mixin XSheetDrawingModel ===================================================

export interface XSheetDrawingModel {

    readonly sheetModel: SheetModel;

    getDocPosition(): Position;
    getRange(): Range | null;
    getSheetAnchor(options?: DisplayModeOptions): SheetDrawingAnchor;
    getRectangle(options?: DrawingRectangleOptions): Rectangle | null;
    getMinSize(): number;
    hasSwappedDimensions(): boolean;
    getModelFrame(): Opt<JQuery>;
    refreshModelFrame(): Opt<ModelFrameSettings>;
    getChangedAnchorAttributes(anchorAttrs: Dict): Opt<Dict>;
}

export namespace XSheetDrawingModel {
    export function mixin(DrawingModelBaseClass: CtorType<DrawingModel<SpreadsheetModel>>): CtorType<SheetDrawingModelType>;
}
