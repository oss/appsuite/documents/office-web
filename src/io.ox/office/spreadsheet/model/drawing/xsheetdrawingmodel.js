/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { Rectangle } from '@/io.ox/office/tk/dom';
import { hasSwappedDimensions } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { CHANGE_DRAWING, DELETE_DRAWING, INSERT_DRAWING } from '@/io.ox/office/spreadsheet/utils/operations';
import { AnchorMode, CellAnchorPoint, SheetDrawingAnchor } from '@/io.ox/office/spreadsheet/utils/cellanchor';
import { getTextFrame } from '@/io.ox/office/spreadsheet/model/drawing/text/textframeutils';

// constants ==================================================================

const ANCHOR_ATTR_NAMES = ['left', 'top', 'width', 'height', 'anchor'];

// private functions ==========================================================

function mixinSheetDrawingModel(DrawingModelBaseClass) {

    return class extends DrawingModelBaseClass {

        // the attribute pool for drawing formatting
        #attrPool;

        // the current cell anchor specification
        #cellAnchor = null;

        // constructor --------------------------------------------------------

        /**
         * @param {SheetModel} sheetModel
         *  The sheet model that contains the model object.
         *
         * @param {DrawingCollection} parentCollection
         *  The parent drawing collection that will contain this drawing
         *  object.
         *
         * @param {unknown[]} baseCtorArgs
         *  All following arguments will be passed to the constructor of the
         *  base class `DrawingModelBaseClass`.
         */
        constructor(sheetModel, parentCollection, ...baseCtorArgs) {

            // base constructor (pass all arguments but the sheet model)
            super(parentCollection, ...baseCtorArgs);

            // the parent sheet model
            this.sheetModel = sheetModel;

            // the attribute pool for drawing formatting
            this.#attrPool = this.docModel.defAttrPool;

            // additional anchor handling for top-level drawing objects
            if (!this.isEmbedded) {

                // initially parse the value of the "anchor" attribute (may be incomplete)
                this.#cellAnchor = this.docModel.addressFactory.parseCellAnchor(this.getMergedAttributeSet(true).drawing.anchor);

                // calculate the complete anchor attributes from the existing attribute values
                var sheetAnchor = this.getSheetAnchor();
                this.setAttributes({ drawing: sheetAnchor.createAttributes() }, { silent: true });
                this.#cellAnchor = sheetAnchor.cellAnchor;

                // update the cell anchor specification from changed "anchor" attribute
                this.on('change:attributes', (newAttrSet, oldAttrSet) => {
                    if (newAttrSet.drawing.anchor !== oldAttrSet.drawing.anchor) {
                        this.#cellAnchor = this.docModel.addressFactory.parseCellAnchor(newAttrSet.drawing.anchor);
                    }
                });
            }
        }

        // public methods -----------------------------------------------------

        /**
         * Returns the global document position of this drawing model, as used
         * in document operations, including the leading sheet index.
         *
         * @returns {Position}
         *  The global document position of this drawing model.
         */
        getDocPosition() {
            return [this.sheetModel.getIndex(), ...this.getPosition()];
        }

        /**
         * Returns the current cell anchor type as used in document operations.
         *
         * @returns {CellAnchorType}
         *  The current cell anchor type.
         */
        getAnchorType() {
            return this.#cellAnchor.type;
        }

        /**
         * Returns the effective anchor mode of this drawing object.
         *
         * @param {boolean} displayMode
         *  If set to `false`, returns the internal anchor mode used for
         *  determining the active anchor attributes. If set to `true`, returns
         *  the anchor mode used for runtime behavior (how the drawing object
         *  behaves when manipulating the columns or rows in the sheet).
         *
         * @returns {AnchorMode}
         *  The effective anchor mode of this drawing object.
         */
        getAnchorMode(displayMode) {
            return this.#cellAnchor.getAnchorMode(displayMode);
        }

        /**
         * Returns the current cell anchor specification.
         *
         * @returns {CellAnchor|null}
         *  A clone of the current cell anchor specification for top-level
         *  drawings; or `null` for embedded drawings.
         */
        getCellAnchor() {
            return this.#cellAnchor?.clone() ?? null;
        }

        /**
         * Returns the address of the cell range covered by this drawing model
         * object.
         *
         * @returns {Range|null}
         *  The address of the cell range covered by this drawing model. If one
         *  of the offsets of the end position is zero, the respective end
         *  column/row index of the range will be decreased by one. Returns
         *  `null` for embedded drawings.
         */
        getRange() {
            return this.#cellAnchor?.getRange() ?? null;
        }

        /**
         * Calculates the complete cell anchor attributes for the anchor
         * settings of this drawing model, according to the active anchor
         * settings determined by its anchor type.
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {boolean} [options.displayMode=false]
         *    If set to `false`, uses the internal anchor mode used for
         *    determining the active anchor attributes. If set to `true`, uses
         *    the anchor mode used for runtime behavior (how the drawing object
         *    behaves when manipulating the columns or rows in the sheet).
         *
         * @returns {SheetDrawingAnchor}
         *  The anchor descriptor for this drawing object.
         */
        getSheetAnchor(options) {

            // the cell anchor object to be used to resolve the absolute position
            var cellAnchor = this.getCellAnchor();
            // the actual anchor mode used to find inactive anchor attributes
            var anchorMode = cellAnchor.getAnchorMode(options?.displayMode);
            // the current drawing attributes
            var drawingAttrs = this.getMergedAttributeSet(true).drawing;
            // the resulting location of the drawing object, in 1/100 mm
            var rectHmm = new Rectangle(0, 0, 0, 0);
            // the resulting location of the drawing object, in pixels
            var rectPx = new Rectangle(0, 0, 0, 0);
            // the points of the cell anchor to be modified in-place
            var point1 = cellAnchor.point1;
            var point2 = cellAnchor.point2;
            // the column and row collections
            var colCollection = this.sheetModel.colCollection;
            var rowCollection = this.sheetModel.rowCollection;
            // collection entry of the column/row collection
            var colEntry = null, rowEntry = null;

            // get start position of the drawing object
            switch (anchorMode) {
                case AnchorMode.TWO_CELL:
                case AnchorMode.ONE_CELL:
                    // left position in 1/100 mm and pixels
                    rectHmm.left = colCollection.getEntryOffsetHmm(point1.a.c, point1.x);
                    rectPx.left = colCollection.getEntryOffset(point1.a.c, point1.x);
                    // top position in 1/100 mm and pixels
                    rectHmm.top = rowCollection.getEntryOffsetHmm(point1.a.r, point1.y);
                    rectPx.top = rowCollection.getEntryOffset(point1.a.r, point1.y);
                    break;

                case AnchorMode.ABSOLUTE:
                    // start position as cell anchor
                    colEntry = colCollection.getEntryByOffset(drawingAttrs.left);
                    rowEntry = rowCollection.getEntryByOffset(drawingAttrs.top);
                    point1 = cellAnchor.point1 = CellAnchorPoint.createFromColRow(colEntry, rowEntry);
                    // left position in 1/100 mm and pixels
                    rectHmm.left = colEntry.offsetHmm + colEntry.relOffsetHmm;
                    rectPx.left = colEntry.offset + colEntry.relOffset;
                    // top position in 1/100 mm and pixels
                    rectHmm.top = rowEntry.offsetHmm + rowEntry.relOffsetHmm;
                    rectPx.top = rowEntry.offset + rowEntry.relOffset;
                    break;
            }

            // get effective size of the drawing object
            switch (anchorMode) {
                case AnchorMode.TWO_CELL:
                    // width in 1/100 mm and pixels
                    rectHmm.width = colCollection.getEntryOffsetHmm(point2.a.c, point2.x) - rectHmm.left;
                    rectPx.width = colCollection.getEntryOffset(point2.a.c, point2.x) - rectPx.left;
                    // height in 1/100 mm and pixels
                    rectHmm.height = rowCollection.getEntryOffsetHmm(point2.a.r, point2.y) - rectHmm.top;
                    rectPx.height = rowCollection.getEntryOffset(point2.a.r, point2.y) - rectPx.top;
                    break;

                case AnchorMode.ONE_CELL:
                case AnchorMode.ABSOLUTE:
                    // end position as cell anchor
                    colEntry = colCollection.getEntryByOffset(rectHmm.left + drawingAttrs.width);
                    rowEntry = rowCollection.getEntryByOffset(rectHmm.top + drawingAttrs.height);
                    point2 = cellAnchor.point2 = CellAnchorPoint.createFromColRow(colEntry, rowEntry);
                    // width in 1/100 mm and pixels
                    rectHmm.width = colEntry.offsetHmm + colEntry.relOffsetHmm - rectHmm.left;
                    rectPx.width = colEntry.offset + colEntry.relOffset - rectPx.left;
                    // height in 1/100 mm and pixels
                    rectHmm.height = rowEntry.offsetHmm + rowEntry.relOffsetHmm - rectHmm.top;
                    rectPx.height = rowEntry.offset + rowEntry.relOffset - rectPx.top;
                    break;
            }

            // OOXML has special behavior to swap width/height values of the drawing object if certain
            // rotation/flip conditions are met. For proper display, those values should be reverted back.
            if (this.docApp.isOOXML() && hasSwappedDimensions(drawingAttrs)) {
                rectHmm.rotateSelf();
                rectPx.rotateSelf();
            }

            // the resulting cell anchor descriptor
            return new SheetDrawingAnchor(anchorMode, rectHmm, rectPx, cellAnchor);
        }

        /**
         * Returns the rectangle in pixels covered by this drawing object,
         * either relative to its parent area (the sheet area for top-level
         * drawing objects, or the rectangle of the parent drawing object); or
         * always absolute in the sheet area.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  - {Boolean} [options.absolute=false]
         *      If set to true, this method will return the absolute sheet
         *      position also for drawing objects embedded into other drawing
         *      objects. By default, this method returns the location of the
         *      specified drawing object relative to its parent object.
         *
         * @returns {Rectangle|null}
         *  The location of this drawing object in pixels according to the
         *  current sheet zoom factor, either relative to its parent object
         *  (sheet area for top-level drawing objects), or always absolute in
         *  the sheet area (according to the passed options); or `null`, if the
         *  drawing object is located entirely in hidden columns or rows and
         *  thus has no meaningful location.
         */
        getRectangle(options) {
            return this.rootCollection.getRectangleForModel(this, options);
        }

        /**
         * Returns the minimum pixel size this drawing object needs for proper
         * rendering. For area-like objects, the minimum size is 5 pixels. For
         * connector shapes, the minimum size is 1 pixel in order to be able to
         * show horizontal or vertical straight lines.
         *
         * @returns {number}
         *  The minimum pixel size this drawing object needs for proper
         *  rendering.
         */
        getMinSize() {
            return this.isTwoPointShape() ? 1 : 5;
        }

        /**
         * Returns whether the transformation attributes of this drawing model
         * will lead to swapped width and height of the rendered drawing frame.
         *
         * @returns {boolean}
         *  Whether the transformation attributes will lead to swapped width
         *  and height of the rendered drawing frame.
         */
        hasSwappedDimensions() {
            return hasSwappedDimensions(this.getMergedAttributeSet(true).drawing);
        }

        /**
         * Returns the DOM drawing frame that represents this drawing model.
         * The DOM drawing frame is used to store the text contents of this
         * drawing object.
         *
         * @returns {Opt<JQuery>}
         *  The DOM drawing frame for this drawing model; or `undefined`, if no
         *  DOM drawing frame could be found.
         */
        getModelFrame() {
            return this.rootCollection.getDrawingFrameForModel(this);
        }

        /**
         * Refreshes the settings of the DOM drawing frame associated with this
         * drawing model, and returns the cached rendering settings.
         *
         * @returns {Opt<object>}
         *  A descriptor with rendering data for this drawing model; or
         *  `undefined`, if no data is available for the drawing model. See
         *  `SheetDrawingCollection.refreshDrawingFrameForModel()` for details.
         */
        refreshModelFrame() {
            return this.rootCollection.refreshDrawingFrameForModel(this);
        }

        /**
         * Extracts the anchor attributes from this drawing object.
         *
         * @returns {Dict}
         *  The anchor attributes of this drawing object.
         */
        getAnchorAttributes() {
            var drawingAttrs = this.getMergedAttributeSet(true).drawing;
            return _.pick(drawingAttrs, ANCHOR_ATTR_NAMES);
        }

        /**
         * Returns the passed anchor attributes if they differ from the anchor
         * attributes of this drawing model.
         *
         * @param {Dict} anchorAttrs
         *  The cell anchor attributes to be checked.
         *
         * @returns {Opt<Dict>}
         *  The passed anchor attributes, if they differ from the anchor
         *  attributes of this drawing model, otherwise `undefined`.
         */
        getChangedAnchorAttributes(anchorAttrs) {
            var drawingAttrs = this.getMergedAttributeSet(true).drawing;
            return _.some(anchorAttrs, function (value, key) {
                return value !== drawingAttrs[key];
            }) ? anchorAttrs : undefined;
        }

        /**
         * Returns an attribute set that will restore the current state of the
         * explicit attributes, after they have been changed according to the
         * passed attribute set. Intended to be used to create undo operations.
         * This method overwrites the implementation of the base class
         * AttributedModel, and always adds a complete set of anchor attributes
         * as expected by the export filters.
         *
         * @param {Dict} attrSet
         *  An (incomplete) attribute set to be merged over the current
         *  explicit attributes of this instance.
         *
         * @returns {Dict}
         *  An attribute set that will restore the current explicit attributes
         *  after they have been changed according to the passed attributes.
         */
        getUndoAttributeSet(attrSet) {

            // create the undo attributes
            var undoAttrSet = super.getUndoAttributeSet(attrSet);

            // bug 57983: always add complete set of anchor attributes
            if (!_.isEmpty(undoAttrSet) && attrSet.drawing && attrSet.drawing.anchor) {
                _.extend(undoAttrSet.drawing, this.getAnchorAttributes());
            }

            return undoAttrSet;
        }

        // operation generators -----------------------------------------------

        /**
         * Generates the operations, and the undo operations, to change the
         * attributes of this drawing object.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {Dict} attrSet
         *  The incomplete attribute set with all formatting attributes to be
         *  changed.
         */
        generateChangeOperations(generator, attrSet, options) {

            // create the undo attributes; nothing to do if no attribute actually changes
            var undoAttrSet = this.getUndoAttributeSet(attrSet);
            if (_.isEmpty(undoAttrSet)) { return; }

            // the position of the drawing object to be inserted into the operation
            var position = this.getPosition();

            generator.generateDrawingOperation(CHANGE_DRAWING, position, { attrs: undoAttrSet }, { undo: true });
            generator.generateDrawingOperation(CHANGE_DRAWING, position, { attrs: attrSet }, options);
        }

        /**
         * Generates the undo operations needed to restore this drawing object.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the undo operations.
         */
        generateRestoreOperations(generator) {

            // bug 47948: do not restore unsupported drawing types
            if (!this.isDeepRestorable()) { return; }

            // the operation position of the drawing object in the root collection
            var position = this.getPosition();

            // create an "insertDrawing" operation to restore the drawing model with its attributes
            var undoProps = { type: this.drawingType };
            var attrSet = this.getExplicitAttributeSet(true);
            if (!_.isEmpty(attrSet)) { undoProps.attrs = attrSet; }
            generator.generateDrawingOperation(INSERT_DRAWING, position, undoProps, { undo: true });

            // additional undo operations for complex drawing contents
            this.clientGenerateRestoreOperations(generator, position);

            // generate the operations to restore the text contents
            var modelFrame = this.getModelFrame();
            if (getTextFrame(modelFrame)) {
                var textGenerator = this.docModel.createOperationGenerator();
                var docPosition = [this.sheetModel.getIndex()].concat(position);
                textGenerator.generateContentOperations(modelFrame, docPosition);
                // DOCS-2756: prevent generating redo operations while executing the undo operations
                var undoOps = textGenerator.getOperations();
                undoOps.forEach(function (op) { op.noUndo = true; });
                generator.appendOperations(undoOps, { undo: true });
            }

            // generate the operations to restore all embedded drawing objects
            this.forEachChildModel(childModel => {
                childModel.generateRestoreOperations(generator);
            });
        }

        /**
         * Generates the operations to delete, and the undo operations to
         * restore this drawing object.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         */
        generateDeleteOperations(generator, options) {
            this.generateRestoreOperations(generator);
            this.clientGenerateDeleteOperation(generator, options);
        }

        /**
         * Generates the operation and undo operation to change the Z order of
         * this drawing object.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {number} toIndex
         *  The new Z order index of this drawing object.
         */
        generateReorderOperations(generator, toIndex) {
            var fromPos = this.getPosition();
            if (_.last(fromPos) !== toIndex) {
                var toPos = fromPos.slice(0, -1);
                toPos.push(toIndex);
                generator.generateMoveDrawingOperation(fromPos, toPos);
                generator.generateMoveDrawingOperation(toPos, fromPos, { undo: true, prepend: true });
            }
        }

        /**
         * Generates the operations and undo operations to update and restore
         * the inactive anchor attributes of this drawing object, after the
         * size or visibility of the columns or rows in the sheet has been
         * changed.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         */
        generateRefreshAnchorOperations(generator) {

            // nothing to do for embedded drawing objects
            if (this.isEmbedded) { return; }

            // recalculate the inactive anchor attributes according to the anchor type
            var anchorAttrs = this.rootCollection.generateAnchorAttributes(this);

            // create a change operation, if some attributes will be changed in this drawing
            if (anchorAttrs) {
                this.generateChangeOperations(generator, { drawing: anchorAttrs });
            }
        }

        /**
         * Generates the operations and undo operations to change the anchor
         * mode of this drawing object.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {AnchorMode} anchorMode
         *  The new display anchor mode for the drawing object.
         */
        generateChangeAnchorModeOperations(generator, anchorMode) {

            // nothing to do for embedded drawing objects
            if (this.isEmbedded) { return; }

            // recalculate the inactive anchor attributes according to the anchor type
            var anchorAttrs = this.rootCollection.generateAnchorAttributes(this, anchorMode);

            // create a change operation, if some attributes will be changed in this drawing
            if (anchorAttrs) {
                this.generateChangeOperations(generator, { drawing: anchorAttrs });
            }
        }

        /**
         * Generates the operations and undo operations to update or restore
         * the formula expressions of the source links in this drawing object
         * that refer to some cells in this document.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operations cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The update task to be executed.
         */
        generateUpdateTaskOperations(sheetCache, updateTask) {

            // In order to retain the array indexes in drawing positions, the operations to modify or delete this
            // drawing object will be prepended to the sheet cache so that they will be processed in reversed order
            // effectively. The undo operations will be processed in regular (ascending) order, so that preceding
            // drawing objects will be restored before following drawing objects will be modified.

            // update the anchor of top-level drawing objects, if cells in the own sheet will be moved (may delete a complete object!)
            var anchorAttrs = sheetCache.drawingCache.generateMovedAnchorAttributes(this);

            // delete drawing objects whose anchor has been deleted
            if (anchorAttrs === 'delete') {
                return this.generateDeleteOperations(sheetCache, { prepend: true });
            }

            // derived classes may generate new operations for child objects, and additional formatting attributes
            var attrSet = this.clientProcessUpdateTask(sheetCache, updateTask, this.getPosition(), { prepend: true });
            if (attrSet === 'delete') {
                return this.generateDeleteOperations(sheetCache, { prepend: true });
            }

            // reduce the attribute to the attributes that will actually change
            if (attrSet) { attrSet = this.getReducedAttributeSet(attrSet); }

            // add the new anchor attributes to the attribute set (bug 57983: do not reduce these attributes!)
            if (anchorAttrs) {
                attrSet = this.#attrPool.extendAttrSet(attrSet || {}, { drawing: anchorAttrs });
            }

            // create a change operation, if some attributes will be changed in this drawing
            // (document operation must be prepended, see comment above)
            if (!_.isEmpty(attrSet)) {
                this.generateChangeOperations(sheetCache, attrSet, { prepend: true });
            }

            // generate the operations to update all embedded drawing objects
            this.forEachChildModel(childModel => {
                childModel.generateUpdateTaskOperations(sheetCache, updateTask);
            });
        }

        // client API methods -------------------------------------------------

        /**
         * Subclasses may overwrite this method to generate another operation
         * to delete this drawing object. This implementation will generate a
         * "deleteDrawing" operation with JSON document position.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operation cache to be filled with the operation.
         *
         * @param {GeneratorOptions} [options]
         *  Optional parameters to be passed to the generator.
         */
        /*protected*/ clientGenerateDeleteOperation(sheetCache, options) {
            sheetCache.generateDrawingOperation(DELETE_DRAWING, this.getPosition(), undefined, options);
        }

        /**
         * Subclasses may overwrite this method to implement generating more
         * undo operations to restore this drawing model.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operation cache to be filled with the operations.
         *
         * @param {Position} position
         *  The current document position of this drawing object.
         */
        /*protected*/ clientGenerateRestoreOperations(/*_sheetCache, _position*/) {
            // intentionally empty, intended to be overwritten by subclasses
        }

        /**
         * Subclasses may overwrite this method to implement generating more
         * operations to update or restore the formula expressions inside this
         * drawing model, or to extend the attribute set to be changed and
         * restored.
         *
         * @param {SheetCache} sheetCache
         *  The sheet operation cache to be filled with the operations.
         *
         * @param {FormulaUpdateTask} updateTask
         *  The formula update task to be processed.
         *
         * @param {Position} position
         *  The current document position of this drawing object.
         *
         * @returns {TransformAnchorResult}
         *  The method may return an attribute set with additional formatting
         *  attributes to be applied at this drawing object, or the string
         *  "delete" to indicate that the drawing object will be deleted from
         *  the sheet by the passed transformer.
         */
        /*protected*/ clientProcessUpdateTask(/*_sheetCache, _updateTask, _position*/) {
            // intentionally empty, intended to be overwritten by subclasses
            return undefined;
        }

        /**
         * Subclasses must implement cloning this instance.
         *
         * @param {SheetModel} targetSheetModel
         *  The model instance of the new cloned sheet that will own the target
         *  drawing collection.
         *
         * @param {DrawingCollection} targetCollection
         *  The drawing collection that will own the clone returned by this
         *  method. May be the root drawing collection of the target sheet, or
         *  an embedded drawing collection of a group object.
         */
        /*protected abstract*/ clientCloneModel(/*targetSheetModel, targetCollection*/) {
            throw new ReferenceError("XSheetDrawingModel.clientCloneModel: missing implementation");
        }

        // protected methods --------------------------------------------------

        /**
         * Creates and returns a cloned instance of this drawing object model
         * for the specified sheet.
         *
         * _Attention:_ Used by the class SheetDrawingCollection during clone
         * construction. DO NOT CALL from external code!
         *
         * @param {SheetModel} targetSheetModel
         *  The model instance of the new cloned sheet that will own the target
         *  drawing collection.
         *
         * @param {DrawingCollection} targetCollection
         *  The drawing collection that will own the clone returned by this
         *  method. May be the root drawing collection of the target sheet, or
         *  an embedded drawing collection of a group object.
         *
         * @returns {this}
         *  A clone of this drawing model, initialized for ownership by the
         *  passed sheet model.
         */
        /*protected*/ clone(targetSheetModel, targetCollection) {

            // the new clone of this drawing model
            const cloneModel = this.clientCloneModel(targetSheetModel, targetCollection);
            // the embedded child collection of the new drawing model
            const cloneCollection = cloneModel.childCollection;

            // clone the child drawing collection (e.g. group objects)
            this.forEachChildModel((childModel, childIndex) => {
                const childCloneModel = childModel.clone(targetSheetModel, cloneCollection);
                cloneCollection.implInsertModel([childIndex], childCloneModel);
            });

            return cloneModel;
        }
    };
}

// exports ====================================================================

export const XSheetDrawingModel = {

    /**
     * Creates and returns a subclass of the passed drawing model class, with
     * mix-in methods specific to spreadsheet documents.
     *
     * @param {CtorType<ModelT extends DrawingModel>} DrawingModelBaseClass
     *  The drawing model base class to be extended.
     *
     * @returns {CtorType<ModelT & XSheetDrawingModel>}
     *  The extended drawing model class for spreadsheet documents.
     */
    mixin: mixinSheetDrawingModel
};
