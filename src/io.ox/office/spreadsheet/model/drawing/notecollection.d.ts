/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { YieldIndexedDrawingModelsOptions } from "@/io.ox/office/drawinglayer/model/indexedcollection";
import { RangeSource } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { SheetDrawingModelType } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { NoteModel } from "@/io.ox/office/spreadsheet/model/drawing/notemodel";
import { SheetDrawingCollectionEventMap, SheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/drawingcollection";

// types ======================================================================

export interface NoteModelIteratorOptions extends YieldIndexedDrawingModelsOptions {
    ranges?: RangeSource;
    vertical?: boolean;
    visibleCells?: boolean;
}

/**
 * Type mapping for the events emitted by `NoteCollection` instances.
 */
export interface NoteCollectionEventMap extends SheetDrawingCollectionEventMap {

    /**
     * Will be emitted after note models have been reordered.
     *
     * @param noteModels
     *  The models of all moved note objects.
     */
    "move:notes": [noteModels: NoteModel[]];
}

// class NoteCollection =======================================================

export class NoteCollection extends SheetDrawingCollection<NoteCollectionEventMap> {

    clientCreateModel(parentCollection: NoteCollection, drawingType: DrawingType, attrSet?: Dict): Opt<SheetDrawingModelType>;

    yieldModels(options?: NoteModelIteratorOptions): IterableIterator<NoteModel>;

    applyCopySheetOperation(context: SheetOperationContext, fromCollection: NoteCollection): void;
    applyInsertNoteOperation(context: SheetOperationContext): void;
    applyDeleteNoteOperation(context: SheetOperationContext): void;
    applyChangeNoteOperation(context: SheetOperationContext): void;
    applyMoveNotesOperation(context: SheetOperationContext): void;

    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise;
    generateRefreshAnchorOperations(generator: SheetOperationGenerator): JPromise;
    generateMergeCellsOperations(generator: SheetOperationGenerator, rangeSource: RangeSource, mergeMode: MergeMode): JPromise;
}
