/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * A sort list, with lower-case string entries.
 */
export type SortList = readonly string[];

/**
 * Type mapping for the events emitted by `SortListCollection` instances.
 */
export interface SortListCollectionEventMap {

    /**
     * Will be emitted after a new sort list collection has been inserted.
     */
    "insert:sortlist": [entries: SortList];
}

// class SortListCollection ===================================================

/**
 * Collects all defined lists of the spreadsheet document.
 */
export class SortListCollection extends ModelObject<SpreadsheetModel, SortListCollectionEventMap> {

    // properties -------------------------------------------------------------

    // all sort lists, in insertion order
    readonly #lists: SortList[] = [];
    // fast lookup of sort lists by value
    readonly #map = new Map<string, SortList[]>();

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super(docModel);

        // add default lists for long and short month names
        this.insertList(LOCALE_DATA.longMonths);
        this.insertList(LOCALE_DATA.shortMonths);

        // add default lists for long and short weekday names
        this.insertList(LOCALE_DATA.longWeekdays);
        this.insertList(LOCALE_DATA.shortWeekdays);
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts a new sort list into this collection, and triggers an
     * "insert:sortlist" event.
     *
     * @param entries
     *  The string values of the sort list.
     *
     * @returns
     *  The resulting new sort list, with lower-case entries.
     */
    insertList(entries: SortList): SortList {
        entries = entries.map(entry => entry.toLowerCase());
        this.#lists.push(entries);
        for (const entry of entries) {
            map.upsert(this.#map, entry, () => []).push(entries);
        }
        this.trigger("insert:sortlist", entries);
        return entries;
    }

    /**
     * returns whether any sort list contains the passed string.
     *
     * @param value
     *  The string value to be used to look for a sort list.
     *
     * @returns
     *  Whether any sort list contains the passed string.
     */
    hasList(value: string): boolean {
        return this.#map.has(value.toLowerCase());
    }

    /**
     * Returns all sort lists containing the passed string.
     *
     * @param value
     *  The string value to be used to look for a sort list.
     *
     * @returns
     *  The sort lists containing the passed value, in insertion order.
     */
    findLists(value: string): Opt<readonly SortList[]> {
        return this.#map.get(value.toLowerCase());
    }

    /**
     * Creates an iterator that visits the sort lists in this collection.
     *
     * @returns
     *  The new value iterator.
     */
    sortLists(): IterableIterator<SortList> {
        return this.#lists.values();
    }

    /**
     * Creates an iterator that visits the sort lists and their array indexes
     * in this collection.
     *
     * @returns
     *  The new entry iterator.
     */
    sortListEntries(): IndexedIterator<SortList> {
        return this.#lists.entries();
    }
}
