/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type CompareFn, ary } from "@/io.ox/office/tk/algorithms";

import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { type AddressIndexFn, Address } from "@/io.ox/office/spreadsheet/utils/address";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";

// class CellModelVector ======================================================

/**
 * An instance of this class represents a sorted list of existing cell models
 * in a single column or row of the sheet.
 *
 * All methods of this class (insert, delete, iteration) use fast binary search
 * to find the cell models.
 */
export class CellModelVector implements Iterable<CellModel> {

    // properties -------------------------------------------------------------

    // the cell model storage
    readonly #cellModels: CellModel[];

    // returns the column/row index of a cell model address used as vector index
    readonly #vecIndexFn: AddressIndexFn;

    // returns the column/row index of a cell model address used for internal sorting
    readonly #elIndexFn: AddressIndexFn;

    // compares the column/row index of two cell models
    readonly #compareFn: CompareFn<Address>;

    // constructor ------------------------------------------------------------

    /**
     * @param cellModel
     *  The first cell model to be inserted into the cell vector.
     *
     * @param columns
     *  Whether this cell vector is a column vector (`true`; cells will be
     *  sorted by their row index), or a row vector (`false`; cells will be
     *  sorted by their column index).
     */
    constructor(cellModel: CellModel, columns: boolean) {
        this.#cellModels = [cellModel];
        this.#vecIndexFn = Address.indexFn(columns);
        this.#elIndexFn = Address.indexFn(!columns);
        this.#compareFn = columns ? Address.compareVert : Address.compare;
    }

    // public accessors -------------------------------------------------------

    /**
     * The main index of this vector (the column index of a column vector, or
     * the row index of a row vector).
     */
    get index(): number {
        return this.#vecIndexFn(this.#cellModels[0].a);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the column/row index of the passed cell model that would be used
     * as sorting index by this vector.
     *
     * @param cellModel
     *  An arbitrary cell model to return the column/row index for.
     *
     * @returns
     *  The row index of the cell model, if this instance is a column vector;
     *  otherwise the column index.
     */
    sortIndex(cellModel: CellModel): number {
        return this.#elIndexFn(cellModel.a);
    }

    /**
     * Inserts a new cell model into this vector.
     *
     * _Attention:_ This vector DOES NOT take ownership of the cell models; and
     * it DOES NOT check whether it already contains a cell model with the same
     * address as the passed cell model (it is up to the caller to ensure
     * uniqueness of cell model addresses).
     *
     * @param cellModel
     *  The cell model to be inserted into this vector.
     */
    insertModel(cellModel: CellModel): void {

        // binary search for insert position (first cell model with col/row index greater than new cell model)
        const fn = this.#compareFn.bind(null, cellModel.a);
        const ai = ary.fastFindFirstIndex(this.#cellModels, elem => fn(elem.a) < 0);

        // insert model at found position (may be the end of the array)
        ary.insertAt(this.#cellModels, ai, cellModel);
    }

    /**
     * Removes a cell model from this vector.
     *
     * @param cellModel
     *  The cell model to be removed from this vector.
     *
     * @returns
     *  Whether this cell vector became empty.
     */
    removeModel(cellModel: CellModel): boolean {

        // binary search for deletion position (first cell model with col/row index greater/equal to passed cell model)
        const fn = this.#compareFn.bind(null, cellModel.a);
        const ai = ary.fastFindFirstIndex(this.#cellModels, elem => fn(elem.a) <= 0);

        // remove model if it is exactly equal (otherwise, the cell model does not exist in the vector)
        if (cellModel === this.#cellModels[ai]) {
            ary.deleteAt(this.#cellModels, ai);
        } else {
            modelLogger.error(`$badge{CellVector} remove: cell model at ${cellModel.a} not found`);
        }

        // return whether the vector drained
        return !this.#cellModels.length;
    }

    /**
     * Creates an iterator for the cell models in this vector.
     *
     * @returns
     *  A new iterator for the cell models in this vector.
     */
    [Symbol.iterator](): IterableIterator<CellModel> {
        return this.#cellModels.values();
    }

    /**
     * Creates an iterator that visits all existing cell models in this vector
     * covered by the passed index interval.
     *
     * @param interval
     *  The index interval for the cell models to be visited in this vector.
     *
     * @param [reverse=false]
     *  If set to `true`, the cell models in the interval will be visited in
     *  reversed order.
     *
     * @yields
     *  The existing cell models in the specified interval.
     */
    *yieldInterval(interval: Interval, reverse?: boolean): IterableIterator<CellModel> {

        const { first, last } = interval;
        const indexer = this.#elIndexFn;

        // performance: completely separated loop for reverse mode to keep all if/else outside the loop body
        if (reverse) {
            // binary search for start position
            let ai = ary.fastFindLastIndex(this.#cellModels, elem => indexer(elem.a) <= last);
            // visit all cell models inside the interval
            for (let elem = this.#cellModels[ai]; elem && (first <= indexer(elem.a)); ai -= 1, elem = this.#cellModels[ai]) {
                yield elem;
            }
        } else {
            // binary search for start position
            let ai = ary.fastFindFirstIndex(this.#cellModels, elem => first <= indexer(elem.a));
            // visit all cell models inside the interval
            for (let elem = this.#cellModels[ai]; elem && (indexer(elem.a) <= last); ai += 1, elem = this.#cellModels[ai]) {
                yield elem;
            }
        }
    }
}
