/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str, map, pick } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/baseformatter";

import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// private functions ==========================================================

/**
 * Converts a cell display string for usage in discrete filters. Converts the
 * string to lower-case, and trims leading and trailing space characters.
 */
export function convertFilterString(display: string, keepCase?: boolean): string {
    // bug 36264: trim space characters (but no other whitespace)
    return (keepCase ? display : display.toLowerCase()).replace(/^ +| +$/g, "");
}

// class TableFilterMatcher ===================================================

/**
 * A filter matcher implements a matching algorithm for specific filter rules
 * in a table column.
 */
export abstract class TableFilterMatcher extends ModelObject<SpreadsheetModel> {

    // whether the matcher is active (intended to be initialized by subclasses)
    protected activeFlag = false;

    // cache for matching results for all processed cell contents
    readonly #cache = new Map<ScalarType, WeakMap<ParsedFormat, boolean>>();

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this filter matcher is active, i.e. whether it actually
     * needs to check cell contents for filtering.
     *
     * Used to improve performance. If a matcher returns `false`, the cells in
     * the table column will not be processed at all.
     */
    active(): boolean {
        return this.activeFlag;
    }

    /**
     * Returns whether the passed cell value and/or display string matches the
     * filter rule.
     *
     * @param value
     *  The scalar value of the cell to be matched.
     *
     * @param format
     *  The parsed number format of the cell.
     *
     * @param display
     *  The formatted display string of the cell.
     *
     * @returns
     *  Whether the passed cell contents match the filter rule (i.e., whether
     *  the table row will be visible).
     */
    test(value: ScalarType, format: ParsedFormat, display: string): boolean {
        // "display" has been calculated form "value" and "format", no need to use it as cache key here
        const valueCache = map.upsert(this.#cache, value, () => new WeakMap());
        return map.upsert(valueCache, format, () => this.implTest(value, format, display));
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses implement the actual matching algorithm. Called if no result
     * has been cached yet for the cell contents.
     */
    protected abstract implTest(value: ScalarType, format: ParsedFormat, display: string): boolean;
}

// class NoneFilterMatcher ====================================================

/**
 * A filter matcher representing no active filtering (filter attribute type
 * "none").
 */
export class NoneFilterMatcher extends TableFilterMatcher {

    // protected methods ------------------------------------------------------

    override implTest(): boolean {
        return true;
    }
}

// class DiscreteFilterMatcher ================================================

/**
 * A filter matcher for the entries of a discrete filter rule (filter attribute
 * type "discrete").
 */
export class DiscreteFilterMatcher extends TableFilterMatcher {

    // all matching display strings (lower-case)
    readonly #entrySet = new Set<string>();

    // all matching date groups
    readonly #dateSet = new Set<string>();

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The spreadsheet document model.
     *
     * @param entries
     *  The entries of a discrete filter rule.
     */
    constructor(docModel: SpreadsheetModel, entries: unknown[]) {
        super(docModel);

        // process all filter entries in the passed array
        for (const entry of entries) {
            this.#registerEntry(entry);
        }

        // filter will be active if at least one valid matching entry has been found
        this.activeFlag = !!this.#entrySet.size || !!this.#dateSet?.size;
    }

    // protected methods ------------------------------------------------------

    override implTest(value: ScalarType, format: ParsedFormat, display: string): boolean {

        // try to convert the number to a date (fails for invalid numbers, proceed with string matcher then)
        const date = this.docModel.getDateForDateGroupFilter(value, format);
        if (date) {

            // try to match year only
            let match = String(date.getUTCFullYear());
            if (this.#dateSet.has(match)) { return true; }

            // try to match year and month (one based month in matcher!)
            match = str.concatTokens(match, String(date.getUTCMonth() + 1));
            if (this.#dateSet.has(match)) { return true; }

            // try to match year, month, and day
            match = str.concatTokens(match, String(date.getUTCDate()));
            if (this.#dateSet.has(match)) { return true; }

            // try to match date and hours
            match = str.concatTokens(match, String(date.getUTCHours()));
            if (this.#dateSet.has(match)) { return true; }

            // try to match date, hours, and minutes
            match = str.concatTokens(match, String(date.getUTCMinutes()));
            if (this.#dateSet.has(match)) { return true; }

            // try to match date and time
            match = str.concatTokens(match, String(date.getUTCSeconds()));
            if (this.#dateSet.has(match)) { return true; }

            // proceed with display string matcher, if no matching date group has been found
        }

        // match literal display strings
        return this.#entrySet.has(convertFilterString(display));
    }

    // private methods --------------------------------------------------------

    #registerEntry(entry: unknown): void {

        // literal strings match against cell display string (ignoring caharcter case)
        if (is.string(entry)) {
            this.#entrySet.add(convertFilterString(entry));
            return;
        }

        // extended filter entries, e.g. date groups
        if (is.dict(entry)) {
            const type = pick.string(entry, "t");
            const value = pick.string(entry, "v");

            switch (type) {

                case "date":
                    if (this.#dateSet) {
                        if (!value) {
                            modelLogger.warn("$badge{DiscreteFilterMatcher} invalid value in date group filter entry");
                        } else {
                            this.#dateSet.add(value);
                        }
                    } else {
                        modelLogger.warn("$badge{DiscreteFilterMatcher} date group filter not supported");
                    }
                    break;

                default:
                    modelLogger.warn("$badge{DiscreteFilterMatcher} unknown type of extended filter entry");
            }
            return;
        }

        // other entry value types are not supported
        modelLogger.warn("$badge{DiscreteFilterMatcher} unknown type of filter entry");
    }
}
