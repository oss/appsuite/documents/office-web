/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ContextEventBase, SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * Base interface of event objects triggered by sheet models and their child
 * collections.
 */
export interface SheetEventBase extends ContextEventBase {

    /**
     * The sheet model containing the collection that emitted the event.
     */
    sheetModel: SheetModel;

    /**
     * The zero-based index of the sheet model containing the collection that
     * emitted the event.
     */
    sheet: number;
}

/**
 * Base interface of event objects triggered by collections that can be
 * children of a sheet model or of the document model.
 */
export interface OptSheetEventBase extends ContextEventBase {

    /**
     * The sheet model containing the collection that emitted the event; or
     * `undefined` if the emitting collection is part of the document model.
     */
    sheetModel: Opt<SheetModel>;

    /**
     * The zero-based index of the sheet model containing the collection that
     * emitted the event; or `null` if the emitting collection is part of the
     * document model.
     */
    sheet: OptSheet;
}

// functions ==================================================================

/**
 * Creates a `SheetEventBase` or `OptSheetEventBase` event object with
 * additional properties.
 *
 * @param sheetModel
 *  The sheet model to be inserted into the event object. If set to
 *  `undefined`, an `OptSheetEventBase` object without sheet model will be
 *  created.
 *
 * @param sheet
 *  The zero-based index of the sheet.
 *
 * @param eventData
 *  The additional properties for the event object.
 *
 * @param [context]
 *  The context wrapper of a document operation.
 *
 * @returns
 *  The event object with the passed properties.
 */
export function createSheetEvent<DataT extends object>(sheetModel: SheetModel, sheet: number, eventData: DataT, context?: SheetOperationContext): SheetEventBase & DataT;
export function createSheetEvent<DataT extends object>(sheetModel: Opt<SheetModel>, sheet: OptSheet, eventData: DataT, context?: SheetOperationContext): OptSheetEventBase & DataT;
// implementation
export function createSheetEvent<DataT extends object>(sheetModel: Opt<SheetModel>, sheet: OptSheet, eventData: DataT, context?: SheetOperationContext): OptSheetEventBase & DataT {
    return {
        ...eventData,
        sheetModel,
        sheet,
        external: !!context?.external,
        importing: !!context?.importing
    };
}
