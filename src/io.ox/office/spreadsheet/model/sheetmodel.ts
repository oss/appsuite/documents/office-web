/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, to, itr, ary, dict, pick, jpromise } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE, Rectangle, convertLengthToHmm, convertHmmToLength } from "@/io.ox/office/tk/dom";
import { onceMethod, debounceMethod } from "@/io.ox/office/tk/objects";

import { type OpColor, Color } from "@/io.ox/office/editframework/utils/color";
import { transformPositionInsert, transformPositionDelete, transformPositionMove } from "@/io.ox/office/editframework/utils/otutils";
import type { PtAttrSet, CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";
import { type AttributedModelEventMap, AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";

import { type Position, CHANGE_SHEET, isPosition } from "@/io.ox/office/spreadsheet/utils/operations";
import { everyInArraySource } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { type IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import type { Direction, SheetPixelOptions } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetType, SplitMode, isSupportedSheetType, isCellSheetType, isVerticalDir, getReverseDirection, modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { PaneSide, PanePos, getNextPanePos } from "@/io.ox/office/spreadsheet/utils/paneutils";

import { type SheetEventBase, createSheetEvent } from "@/io.ox/office/spreadsheet/model/sheetevents";
import { type ISheetProps, SheetPropertySet } from "@/io.ox/office/spreadsheet/model/sheetpropset";
import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { ColAttributes, RowAttributes } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import type { ChangeColRowEvent, MoveColRowEvent, EntryOffsetOptions } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import { VisibleEntryMethod, ColCollection, RowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import { type MoveCellsEvent, type CellCollectionEventMap, CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";
import { type MergeCollectionEventMap, MergeCollection } from "@/io.ox/office/spreadsheet/model/mergecollection";
import { type HlinkCollectionEventMap, HlinkCollection } from "@/io.ox/office/spreadsheet/model/hlinkcollection";
import { type NameCollectionEventMap, NameCollection } from "@/io.ox/office/spreadsheet/model/namecollection";
import { type TableCollectionEventMap, TableCollection } from "@/io.ox/office/spreadsheet/model/tablecollection";
import { type DVRuleCollectionEventMap, DVRuleCollection } from "@/io.ox/office/spreadsheet/model/dvrulecollection";
import { type CFRuleCollectionEventMap, CFRuleCollection } from "@/io.ox/office/spreadsheet/model/cfrulecollection";

import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { type SheetGeneratorConfig, SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetCacheConfig, SheetCacheFlushResult, SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { OperationBuilder } from "@/io.ox/office/spreadsheet/model/operation/builder";

import type { SheetAttributePoolMap } from "@/io.ox/office/spreadsheet/model/style/sheetattributepool";
import type { FontMetrics } from "@/io.ox/office/spreadsheet/model/style/fontmetrics";

import type { SheetDrawingModelType } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { SheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/drawingcollection";
import type { NoteModel } from "@/io.ox/office/spreadsheet/model/drawing/notemodel";
import { NoteCollection } from "@/io.ox/office/spreadsheet/model/drawing/notecollection";
import type { CommentModel } from "@/io.ox/office/spreadsheet/model/drawing/commentmodel";
import { CommentCollection } from "@/io.ox/office/spreadsheet/model/drawing/commentcollection";

import { TokenArray, type SheetTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import { type FormulaUpdateTask, MoveCellsUpdateTask, RenameSheetUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

import type { BuildOperationsOptions, SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Specifies how to shrink a cell range address to its visible part.
 * - If set to "both", columns and rows will be shrunken.
 * - If set to "columns", only the columns of the range will be shrunken.
 * - If set to "rows", only the rows of the range will be shrunken.
 */
export type ShrinkVisibleMethod = "both" | "columns" | "rows";

/**
 * Options for the method `SheetModel.getCellRectangle()`.
 */
export interface CellRectangleOptions extends SheetPixelOptions {

    /**
     * If set to `true`, and the cell is part of a merged range, the location
     * of the entire merged range will be returned. Default value is `false`.
     */
    expandMerged?: boolean;
}

/**
 * The attributes of the attribute family "sheet".
 */
export interface SheetAttributes {

    /**
     * Specifies whether the sheet is visible in the GUI.
     */
    visible: boolean;

    /**
     * Specifies whether the sheet is locked. Contents in locked sheets cannot
     * be changed (except unlocked cells).
     */
    locked: boolean;

    /**
     * Base column width for undefined columns, in number of digits of the
     * default document font. Used by OOXML only.
     */
    baseColWidth: number;

    /**
     * Whether the total columns of all column groups in the sheet are located
     * before the groups.
     */
    colTotalBefore: boolean;

    /**
     * Whether the total rows of all row groups in the sheet are located before
     * the groups.
     */
    rowTotalBefore: boolean;

    // *** view attributes ***

    /**
     * The current zoom factor. The value 1 represents the standard zoom of
     * 100%.
     */
    zoom: number;

    /**
     * The selected cell ranges, as space-separated cell range list.
     */
    selectedRanges: string;

    /**
     * The array index of the active cell range in the "selectedRanges"
     * attribute value (the index of the cell range containing the active
     * cell).
     */
    activeIndex: number;

    /**
     * The address of the active cell in the selected cell ranges. Must be
     * located inside the active cell range (as specified by the attributes
     * `selectedRanges` and `activeIndex`).
     */
    activeCell: string;

    /**
     * The positions of all selected drawing objects. Each element of this
     * array is a sheet-local drawing position (an array of integers without
     * leading sheet index).
     */
    selectedDrawings: Position[];

    /**
     * Whether the grid lines between the cells will be rendered.
     */
    showGrid: boolean;

    /**
     * The color of the grid lines between the cells.
     */
    gridColor: OpColor;

    /**
     * The split mode used when split view is active.
     */
    splitMode: SplitMode;

    /**
     * Width of the left grid panes, as 1/100 mm for dynamic split mode, or
     * number of columns in frozen split mode. The value zero hides the right
     * grid panes.
     */
    splitWidth: number;

    /**
     * Height of the upper grid panes, as 1/100 mm for dynamic split mode, or
     * number of rows in frozen split mode. The value zero hides the lower grid
     * panes.
     */
    splitHeight: number;

    /**
     * Identifier of the active (focused) grid pane. Must be one of the string
     * values of the `PanePos` enumeration. Must be a visible grid pane,
     * according to the current split settings.
     */
    activePane: PanePos;

    /**
     * Horizontal scroll position in the left grid panes, as zero-based column
     * index.
     */
    scrollLeft: number;

    /**
     * Horizontal scroll position in the right grid panes, as zero-based column
     * index.
     */
    scrollRight: number;

    /**
     * Vertical scroll position in the upper grid panes, as zero-based row
     * index.
     */
    scrollTop: number;

    /**
     * Vertical scroll position in the lower grid panes, as zero-based row
     * index.
     */
    scrollBottom: number;
}

/**
 * Type of a complete attribute set of a sheet mdoel.
 */
export interface SheetAttributeSet {
    sheet: SheetAttributes;
    column: ColAttributes;
    row: RowAttributes;
}

/**
 * Type of a partial attribute set of a sheet mdoel.
 */
export type PtSheetAttributeSet = PtAttrSet<SheetAttributeSet>;

/**
 * Extended configuration options for sheet attributes.
 */
export interface ExtSheetAttrConfig {

    /**
     * If set to `true`, marks "important" sheet view attributes that will
     * always be sent back to the server when closing the document (if they
     * have been changed), even if the document has not been changed.
     */
    sendAlways?: true;
}

/**
 * Shape of event objects that will be emitted by "refresh:ranges" events while
 * invoking the method `SheetModel::refreshRanges`.
 */
export interface RefreshRangesEvent extends SheetEventBase {

    /**
     * The addresses of all cell ranges to be refreshed.
     */
    ranges: RangeArray;
}

/**
 * Event object emitted for drawing objects in a sheet.
 */
export interface SheetDrawingEvent extends SheetEventBase {

    /**
     * The model of the drawing object that caused the event.
     */
    drawingModel: SheetDrawingModelType;
}

export interface ChangeSheetDrawingEvent extends SheetDrawingEvent {

    /**
     * The exact type of the change event. The possible values are dependent on
     * the type of the drawing object. The value "attributes" denotes changed
     * formatting attributes, the value "text" denotes changed text contents.
     */
    changeType: string;
}

export interface MoveSheetDrawingEvent extends SheetDrawingEvent {

    /**
     * The old sorting index of the drawing model before it has been moved to
     * its new position.
     */
    fromIndex: number;
}

/**
 * Event object emitted for cell notes in a sheet.
 */
export interface NoteEvent extends SheetEventBase {

    /**
     * The model of the cell note that caused the event.
     */
    noteModel: NoteModel;
}

export interface ChangeNoteEvent extends NoteEvent {

    /**
     * The exact type of the change event. The value "attributes" denotes
     * changed formatting attributes, the value "text" denotes changed text
     * contents.
     */
    changeType: string;
}

export interface MoveNotesEvent extends SheetEventBase {

    /**
     *  The models of all moved note objects.
     */
    noteModels: readonly NoteModel[];
}

export interface MoveCommentsEvent extends SheetEventBase {

    /**
     *  The models of all moved comments.
     */
    commentModels: readonly CommentModel[];
}

/**
 * Type mapping for the events emitted by `SheetModel` instances.
 */
export interface SheetModelEventMap extends
    AttributedModelEventMap<SheetAttributeSet>,
    Omit<CellCollectionEventMap, "change:usedarea">,
    Omit<MergeCollectionEventMap, "change:usedarea">,
    HlinkCollectionEventMap,
    NameCollectionEventMap,
    TableCollectionEventMap,
    DVRuleCollectionEventMap,
    CFRuleCollectionEventMap {

    /**
     * Will be emitted while invoking the method `SheetModel::refreshRanges`.
     * Intended to initiate updating the visual representation of the notified
     * cell ranges.
     *
     * @param event
     *  The event object with the addresses of all cell ranges to be refreshed.
     */
    "refresh:ranges": [event: RefreshRangesEvent];

    /**
     * Will be emitted after the formatting attributes or the autostyle of
     * columns have been changed.
     *
     * @param event
     *  An event object with the intervals of all changed columns.
     */
    "change:columns": [event: ChangeColRowEvent];

    /**
     * Will be emitted after columns have been moved in the sheet.
     *
     * @param event
     *  An event object with the intervals of all moved columns.
     */
    "move:columns": [event: MoveColRowEvent];

    /**
     * Will be emitted after the formatting attributes or the autostyle of rows
     * have been changed.
     *
     * @param event
     *  An event object with the intervals of all changed rows.
     */
    "change:rows": [event: ChangeColRowEvent];

    /**
     * Will be emitted after rows have been moved in the sheet.
     *
     * @param event
     *  An event object with the intervals of all moved rows.
     */
    "move:rows": [event: MoveColRowEvent];

    /**
     * Will be emitted after the effective combined bounding range of the sheet
     * has been changed, e.g. after inserting or removing cells or merged
     * ranges. Note that, for performance reasons, the event object does _not_
     * contain the union of all used ranges of all collections in the sheet.
     */
    "change:usedarea": [event: SheetEventBase];

    /**
     * Will be emitted after a new drawing model has been inserted into the
     * drawing collection.
     *
     * @param event
     *  Event object with the new drawing model.
     */
    "insert:drawing": [event: SheetDrawingEvent];

    /**
     * Will be emitted before a drawing model will be removed from the drawing
     * collection.
     *
     * @param event
     *  Event object with the drawing model to be deleted.
     */
    "delete:drawing": [event: SheetDrawingEvent];

    /**
     * Will be emitted after the contents or formatting attributes of a drawing
     * object have been changed.
     *
     * @param event
     *  Event object with the changed drawing model.
     */
    "change:drawing": [event: ChangeSheetDrawingEvent];

    /**
     * Will be emitted after a drawing model has been moved to a new index. May
     * bubble up from embedded child collections, e.g. from a group object.
     *
     * @param event
     *  Event object with the moved drawing model.
     */
    "move:drawing": [event: MoveSheetDrawingEvent];

    /**
     * Will be emitted after a new cell note has been inserted into the note
     * collection.
     *
     * @param event
     *  Event object with the new cell note model.
     */
    "insert:note": [event: NoteEvent];

    /**
     * Will be emitted before a cell note model will be removed from the note
     * collection.
     *
     * @param event
     *  Event object with the cell note model to be deleted.
     */
    "delete:note": [event: NoteEvent];

    /**
     * Will be emitted after the contents or formatting attributes of a cell
     * note have been changed.
     *
     * @param event
     *  Event object with the changed cell note model.
     */
    "change:note": [event: ChangeNoteEvent];

    /**
     * Will be emitted after cell notes have been moved.
     *
     * @param event
     *  Event object with the moved cell notes.
     */
    "move:notes": [event: MoveNotesEvent];

    /**
     * Will be emitted after cell comments have been moved.
     *
     * @param event
     *  Event object with the moved cell comments.
     */
    "move:comments": [event: MoveCommentsEvent];
}

// constants ==================================================================

export const SHEET_ATTRIBUTES: AttributeConfigBag<SheetAttributes, ExtSheetAttrConfig> = {
    visible:            { def: true },
    locked:             { def: false },
    baseColWidth:       { def: 8 },
    colTotalBefore:     { def: false },
    rowTotalBefore:     { def: false },
    zoom:               { def: 1,               sendAlways: true },
    selectedRanges:     { def: "A1" },
    activeIndex:        { def: 0 },
    activeCell:         { def: "A1" },
    selectedDrawings:   { def: [] },
    showGrid:           { def: true,            sendAlways: true },
    gridColor:          { def: Color.AUTO,      sendAlways: true },
    splitMode:          { def: SplitMode.SPLIT, sendAlways: true },
    splitWidth:         { def: 0,               sendAlways: true },
    splitHeight:        { def: 0,               sendAlways: true },
    activePane:         { def: PanePos.TL },
    scrollLeft:         { def: 0 },
    scrollRight:        { def: 0 },
    scrollTop:          { def: 0 },
    scrollBottom:       { def: 0 }
};

// private constants ----------------------------------------------------------

/**
 * Additional fixed zoom scale for rendering depending on platform type.
 */
const ZOOM_SCALE_ADJUST = TOUCH_DEVICE ? 1.25 : 1;

// private functions ==========================================================

/**
 * Returns the bounding range of the passed cell range addresses. Either of the
 * passed range addresses may be missing (`undefined`).
 *
 * @param range1
 *  The first cell range address.
 *
 * @param range2
 *  The second cell range address.
 *
 * @returns
 *  The bounding range of the passed cell range addresses; or `undefined`, if
 *  both ranges are missing.
 */
function getBoundRange(range1: Opt<Range>, range2: Opt<Range>): Opt<Range> {
    return (range1 && range2) ? range1.boundary(range2) : (range1 ?? range2);
}

// class SheetModel ===========================================================

/**
 * Represents a single sheet in the spreadsheet document.
 */
export class SheetModel extends AttributedModel<SpreadsheetModel, SheetAttributePoolMap, SheetAttributeSet, SheetModelEventMap> {

    /**
     * The type of this sheet.
     */
    readonly sheetType: SheetType;

    /**
     * Whether the sheet type is supported by the spreadsheet application.
     */
    readonly isSupported: boolean;

    /**
     * Whether this sheet is a worksheet.
     */
    readonly isWorksheet: boolean;

    /**
     * Whether this sheet is a chart sheet.
     */
    readonly isChartsheet: boolean;

    /**
     * Whether this sheet contains cells (either a worksheet, or a macrosheet).
     */
    readonly isCellType: boolean;

    /**
     * Dynamic property set for the sheet model.
     */
    readonly propSet: SheetPropertySet;

    /**
     * The address factory of the document.
     */
    readonly addressFactory: AddressFactory;

    /**
     * The font metrics of the document.
     */
    readonly fontMetrics: FontMetrics;

    /**
     * The column collection containing all column attributes and formatting.
     */
    readonly colCollection: ColCollection;

    /**
     * The row collection containing all row attributes and formatting.
     */
    readonly rowCollection: RowCollection;

    /**
     * The cell collection containing all cell values, formulas, and
     * formatting.
     */
    readonly cellCollection: CellCollection;

    /**
     * The collection containing the addresses of all merged ranges.
     */
    readonly mergeCollection: MergeCollection;

    /**
     * The hyperlink collection containing the URLs and addresses of all cells
     * with hyperlinks.
     */
    readonly hlinkCollection: HlinkCollection;

    /**
     * The collection with defined names (named formula expressions) scoped to
     * this sheet.
     */
    readonly nameCollection: NameCollection;

    /**
     * The table range collection.
     */
    readonly tableCollection: TableCollection;

    /**
     * The data validation rule collection.
     */
    readonly dvRuleCollection: DVRuleCollection;

    /**
     * The conditional formatting rule collection.
     */
    readonly cfRuleCollection: CFRuleCollection;

    /**
     * The collection of drawing models embedded in the sheet area.
     */
    readonly drawingCollection: SheetDrawingCollection;

    /**
     * The collection of cell notes.
     */
    readonly noteCollection: NoteCollection;

    /**
     * The collection of cell comment threads.
     */
    readonly commentCollection: CommentCollection;

    /** All ranges to be notified with a "refresh:ranges" event. */
    readonly #refreshRanges = new RangeArray();
    // the original zoom factor stored in the document
    #originalZoom = 0;
    // the effective zoom factor, according to `#originalZoom`, and platform type
    #effectiveZoom = 0;

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The spreadsheet document model containing this sheet.
     *
     * @param sheetType
     *  The type identifier of this sheet.
     *
     * @param [initAttrSet]
     *  An attribute set with initial formatting attributes for the sheet.
     */
    constructor(docModel: SpreadsheetModel, sheetType: SheetType, initAttrSet?: Dict) {

        // base constructor
        super(docModel, initAttrSet, { attrPool: docModel.sheetAttrPool, families: ["sheet", "column", "row"] });

        // public properties
        this.sheetType = sheetType;
        this.isSupported = isSupportedSheetType(sheetType);
        this.isWorksheet = sheetType === SheetType.WORKSHEET;
        this.isChartsheet = sheetType === SheetType.CHARTSHEET;
        this.isCellType = isCellSheetType(sheetType);
        this.propSet = this.member(new SheetPropertySet(this));
        this.addressFactory = docModel.addressFactory;
        this.fontMetrics = docModel.fontMetrics;

        // update internal settings according to the current effective sheet zoom factor
        this.listenToProp(this.propSet, "zoom", this.#updateZoom);
        this.#updateZoom();

        // create the collection instances
        this.colCollection = this.member(new ColCollection(this));
        this.rowCollection = this.member(new RowCollection(this));
        this.cellCollection = this.member(new CellCollection(this));
        this.mergeCollection = this.member(new MergeCollection(this));
        this.hlinkCollection = this.member(new HlinkCollection(this));
        this.nameCollection = this.member(new NameCollection(this));
        this.tableCollection = this.member(new TableCollection(this));
        this.dvRuleCollection = this.member(new DVRuleCollection(this));
        this.cfRuleCollection = this.member(new CFRuleCollection(this));
        this.drawingCollection = this.member(new SheetDrawingCollection(this));
        this.noteCollection = this.member(new NoteCollection(this));
        this.commentCollection = this.member(new CommentCollection(this));

        // update frozen split settings after inserting/deleting columns or rows
        this.colCollection.on("move:intervals", event => this.#updateFrozenSplit(event.intervals, event.insert, true));
        this.rowCollection.on("move:intervals", event => this.#updateFrozenSplit(event.intervals, event.insert, false));

        // update selection after cell/drawing operations
        this.listenTo(this.cellCollection, "move:cells", this.#moveCellsHandler);
        this.listenTo(this.mergeCollection, "merge:cells", this.#mergeCellsHandler);
        this.listenTo(this.drawingCollection, "insert:drawing", this.#insertDrawingHandler);
        this.listenTo(this.drawingCollection, "delete:drawing", this.#deleteDrawingHandler);
        this.listenTo(this.drawingCollection, "move:drawing", this.#moveDrawingHandler);

        // forward all column/row collection events
        this.listenTo(this.colCollection, "move:intervals",   event => this.trigger("move:columns", event));
        this.listenTo(this.colCollection, "change:intervals", event => this.trigger("change:columns", event));
        this.listenTo(this.rowCollection, "move:intervals",   event => this.trigger("move:rows", event));
        this.listenTo(this.rowCollection, "change:intervals", event => this.trigger("change:rows", event));

        // forward changed used area (without the used area from the single collection to prevent misusage)
        this.listenTo(this.cellCollection,  "change:usedarea", ({ range, ...event }) => this.trigger("change:usedarea", event));
        this.listenTo(this.mergeCollection, "change:usedarea", ({ range, ...event }) => this.trigger("change:usedarea", event));

        // forward all cell collection events
        this.listenTo(this.cellCollection,  "change:cells", event => this.trigger("change:cells", event));
        this.listenTo(this.cellCollection,  "move:cells",   event => this.trigger("move:cells", event));
        this.listenTo(this.mergeCollection, "merge:cells",  event => this.trigger("merge:cells", event));

        // forward events of the child collections to own listeners
        this.listenToAllEvents(this.hlinkCollection,   this.trigger);
        this.listenToAllEvents(this.nameCollection,    this.trigger);
        this.listenToAllEvents(this.cfRuleCollection,  this.trigger);
        this.listenToAllEvents(this.dvRuleCollection,  this.trigger);
        this.listenToAllEvents(this.tableCollection,   this.trigger);

        // convert drawing event arguments to `SheetEventBase` event objects
        this.listenTo(this.drawingCollection, "insert:drawing", drawingModel => this.trigger("insert:drawing", this.createSheetEvent({ drawingModel })));
        this.listenTo(this.drawingCollection, "delete:drawing", drawingModel => this.trigger("delete:drawing", this.createSheetEvent({ drawingModel })));
        this.listenTo(this.drawingCollection, "change:drawing", (drawingModel, changeType) => this.trigger("change:drawing", this.createSheetEvent({ drawingModel, changeType })));
        this.listenTo(this.drawingCollection, "move:drawing",   (drawingModel, fromIndex) => this.trigger("move:drawing", this.createSheetEvent({ drawingModel, fromIndex })));

        // convert cell note event arguments to `SheetEventBase` event objects
        this.listenTo(this.noteCollection, "insert:drawing", noteModel => this.trigger("insert:note", this.createSheetEvent({ noteModel: noteModel as NoteModel })));
        this.listenTo(this.noteCollection, "delete:drawing", noteModel => this.trigger("delete:note", this.createSheetEvent({ noteModel: noteModel as NoteModel })));
        this.listenTo(this.noteCollection, "change:drawing", (noteModel, changeType) => this.trigger("change:note", this.createSheetEvent({ noteModel: noteModel as NoteModel, changeType })));
        this.listenTo(this.noteCollection, "move:notes",     noteModels => this.trigger("move:notes", this.createSheetEvent({ noteModels })));

        // convert comment event arguments to `SheetEventBase` event objects
        this.listenTo(this.commentCollection, "move:comments", commentModels => this.trigger("move:comments", this.createSheetEvent({ commentModels })));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the current sheet index of this sheet model.
     *
     * @returns
     *  The current zero-based sheet index of this sheet model.
     */
    getIndex(): number {
        return this.docModel.sheetCollection.getSheetIndexOfModel(this);
    }

    /**
     * Returns whether this sheet is the active sheet of the document.
     *
     * @returns
     *  Whether this sheet is the active sheet of the document.
     */
    isActive(): boolean {
        return this.getIndex() === this.docModel.getActiveSheet();
    }

    /**
     * Returns the name of this sheet model.
     *
     * @returns
     *  The name of this sheet model.
     */
    getName(): string {
        return this.docModel.getSheetName(this.getIndex())!;
    }

    /**
     * Returns a clone of the current sheet selection.
     *
     * @returns
     *  A deep clone of the current sheet selection.
     */
    getSelection(): SheetSelection {
        return this.propSet.get("selection").clone();
    }

    /**
     * Changes the current sheet selection.
     *
     * @param selection
     *  The new sheet selection.
     */
    setSelection(selection: SheetSelection): void {
        this.propSet.set("selection", selection);
    }

    /**
     * Creates a new token array for cell formulas located in this sheet.
     *
     * @returns
     *  A new token array for cell formulas located in this sheet.
     */
    createCellTokenArray(): SheetTokenArray {
        return new TokenArray(this, "cell") as SheetTokenArray;
    }

    /**
     * Returns whether the passed update task represents a move operation in
     * this sheet, i.e. it is a `MoveCellsUpdateTask`, and its sheet index
     * equals the own sheet index.
     *
     * @param updateTask
     *  The update task to be checked.
     *
     * @returns
     *  Whether the passed update task represents a move operation in this
     *  sheet.
     */
    isOwnMoveTask(updateTask: FormulaUpdateTask): updateTask is MoveCellsUpdateTask {
        return (updateTask instanceof MoveCellsUpdateTask) && (this.getIndex() === updateTask.sheet);
    }

    /**
     * Returns whether the passed update task represents a rename operation in
     * this sheet, i.e. it is a `RenameSheetUpdateTask`, and its sheet index
     * equals the own sheet index.
     *
     * @param updateTask
     *  The update task to be checked.
     *
     * @returns
     *  Whether the passed update task represents a rename operation in this
     *  sheet.
     */
    isOwnRenameTask(updateTask: FormulaUpdateTask): updateTask is RenameSheetUpdateTask {
        return (updateTask instanceof RenameSheetUpdateTask) && (this.getIndex() === updateTask.sheet);
    }

    /**
     * Returns whether this sheet model contains a comment thread with unsaved
     * changes.
     *
     * @returns
     *  Whether this sheet contains a comment thread with unsaved changes.
     */
    hasUnsavedComment(): boolean {
        return this.commentCollection.hasUnsavedComment();
    }

    /**
     * Delete unsaved changes in the comment threads of this sheet.
     */
    deleteUnsavedComment(): void {
        this.commentCollection.deleteUnsavedComment();
    }

    /**
     * Creates a `SheetEventBase` event object with additional properties.
     *
     * @param eventData
     *  The additional properties for the event object.
     *
     * @param [context]
     *  The context wrapper of a document operation.
     *
     * @returns
     *  The `SheetEventBase` event object with the passed properties.
     */
    createSheetEvent<DataT extends object>(eventData: DataT, context?: SheetOperationContext): SheetEventBase & DataT {
        return createSheetEvent(this, this.getIndex(), eventData, context);
    }

    // dimensions -------------------------------------------------------------

    /**
     * Converts the passed length in pixels to a length in 1/100 of millimeters,
     * according to the current sheet zoom factor.
     *
     * @param length
     *  The length in pixels.
     *
     * @returns
     *  The converted length in 1/100 of millimeters.
     */
    convertPixelToHmm(length: number): number {
        return convertLengthToHmm(length / this.#effectiveZoom, "px");
    }

    /**
     * Converts the passed length in 1/100 of millimeters to a length in pixels,
     * according to the current sheet zoom factor.
     *
     * @param length
     *  The length in 1/100 of millimeters.
     *
     * @returns
     *  The converted length in pixels.
     */
    convertHmmToPixel(length: number): number {
        return convertHmmToLength(length * this.#effectiveZoom, "px", 1);
    }

    /**
     * Returns the effective zoom factor to be used for this sheet, according
     * to the value of the view attribute "zoom", and the type of the current
     * device. On touch devices, the effective zoom factor will be increased
     * for better usability.
     *
     * @returns
     *  The effective zoom factor for this sheet.
     */
    getEffectiveZoom(): number {
        return this.#effectiveZoom;
    }

    /**
     * Returns the maximum width of the digits in the font of the current
     * default cell style.
     *
     * @returns
     *  The maximum width of the digits in the font of the current default cell
     *  style, in pixels.
     */
    getDefaultDigitWidth(): number {
        return this.fontMetrics.getDefaultDigitWidth(this.#effectiveZoom);
    }

    /**
     * Returns the effective horizontal padding between cell grid lines and the
     * text contents of the cell, according to the current zoom factor of this
     * sheet.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the padding, as used in
     *  document operations.
     *
     * @returns
     *  The effective horizontal cell content padding, in pixels.
     */
    getTextPadding(charAttrs: CharacterAttributes): number {
        return this.fontMetrics.getTextPadding(charAttrs, this.#effectiveZoom);
    }

    /**
     * Returns the total size of all horizontal padding occupied in a cell that
     * cannot be used for the cell contents, according to the current zoom
     * factor of this sheet. This value includes the text padding (twice, for
     * left and right border), and additional space needed for the grid lines.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the padding, as used in
     *  document operations.
     *
     * @returns
     *  The total size of the horizontal cell content padding, in pixels.
     */
    getTotalCellPadding(charAttrs: CharacterAttributes): number {
        return this.fontMetrics.getTotalCellPadding(charAttrs, this.#effectiveZoom);
    }

    /**
     * Calculates the effective scaled row height in pixels to be used for the
     * passed character attributes and the current zoom factor of this sheet.
     *
     * @param charAttrs
     *  Character formatting attributes influencing the line height, as used in
     *  document operations.
     *
     * @returns
     *  The row height for the passed character attributes, in pixels.
     */
    getRowHeight(charAttrs: CharacterAttributes): number {
        return this.fontMetrics.getRowHeight(charAttrs, this.#effectiveZoom);
    }

    /**
     * Returns the location of this sheet, either in 1/100 of millimeters, or
     * in pixels according to the current sheet zoom factor.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The location of the entire sheet. The rectangle always starts at
     *  coordinates `(0,0)`. Its size is equal to the total size of all visible
     *  columns and rows.
     */
    getSheetRectangle(options?: SheetPixelOptions): Rectangle {
        return new Rectangle(0, 0,
            this.colCollection.getTotalSize(options),
            this.rowCollection.getTotalSize(options)
        );
    }

    /**
     * Returns the location of the passed cell range in the sheet, either in
     * 1/100 of millimeters, or in pixels according to the current sheet zoom
     * factor.
     *
     * @param range
     *  The cell range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The location of the cell range in the sheet.
     */
    getRangeRectangle(range: Range, options?: SheetPixelOptions): Rectangle {
        return Rectangle.fromIntervals(
            this.colCollection.getIntervalPosition(range.colInterval(), options),
            this.rowCollection.getIntervalPosition(range.rowInterval(), options)
        );
    }

    /**
     * Returns the location of the specified cell in the sheet, either in 1/100
     * of millimeters, or in pixels according to the current sheet zoom factor.
     *
     * @param address
     *  The address of the cell.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The location of the cell in the sheet.
     */
    getCellRectangle(address: Address, options?: CellRectangleOptions): Rectangle {

        // try to extend the cell address to a merged range
        const mergedRange = options?.expandMerged ? this.mergeCollection.getMergedRange(address) : undefined;
        if (mergedRange) { return this.getRangeRectangle(mergedRange, options); }

        // build the rectangle from the position of a single cell
        return Rectangle.fromIntervals(
            this.colCollection.getEntryPosition(address.c, options),
            this.rowCollection.getEntryPosition(address.r, options)
        );
    }

    /**
     * Convenience shortcut to receive the anchor rectangle of a cell note
     * (always for the merged range covering the specified anchor cell, always
     * in pixels).
     *
     * @param anchor
     *  The anchor address of the cell note.
     *
     * @returns
     *  The location of the anchor cell in the sheet, in pixels.
     */
    getAnchorRectangle(anchor: Address): Rectangle {
        return this.getCellRectangle(anchor, { pixel: true, expandMerged: true });
    }

    /**
     * Returns the address of the cell range covering the passed absolute sheet
     * rectangle.
     *
     * @param rectangle
     *  The absolute position and size of the sheet rectangle, in pixels, or in
     *  1/100 of millimeters, according to the option `pixel` (see below).
     *
     * @param [options]
     *  Optional parameters. Supports all options also supported by the method
     *  `ColRowCollection.getEntryByOffset()`, especially the option `pixel`
     *  that causes to interpret the rectangle in pixels.
     *
     * @returns
     *  The address of the cell range covering the passed rectangle.
     */
    getRangeFromRectangle(rectangle: Rectangle, options?: EntryOffsetOptions): Range {

        // the column and row entries covering the start and end corner of the rectangle
        const colEntry1 = this.colCollection.getEntryByOffset(rectangle.left, options);
        const rowEntry1 = this.rowCollection.getEntryByOffset(rectangle.top, options);
        const colEntry2 = this.colCollection.getEntryByOffset(rectangle.right() - 1, options);
        const rowEntry2 = this.rowCollection.getEntryByOffset(rectangle.bottom() - 1, options);

        return Range.fromIndexes(colEntry1.index, rowEntry1.index, colEntry2.index, rowEntry2.index);
    }

    /**
     * Returns the range address of the used area in the sheet (including
     * merged ranges).
     *
     * @returns
     *  The range address of the used area in the sheet; or `undefined`, if all
     *  cell-based collections are empty.
     */
    getUsedRange(): Opt<Range> {
        return getBoundRange(this.cellCollection.getUsedRange(), this.mergeCollection.getUsedRange());
    }

    /**
     * Returns whether the passed cell range address consists of a single cell,
     * or whether it exactly covers a single merged range.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  Whether the cell is the only (merged) cell in the range.
     */
    isSingleCellInRange(range: Range): boolean {
        return range.single() || !!this.mergeCollection.getBoundingMergedRange(range);
    }

    /**
     * If the passed cell range address represents a single cell, or exactly
     * covers a merged range, the surrounding content range will be returned.
     * Otherwise, the passed range will be returned.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  The address of the content range of a single cell, otherwise the passed
     *  cell range address.
     */
    getContentRangeForCell(range: Range): Range {
        return this.isSingleCellInRange(range) ? this.cellCollection.findContentRange(range) : range;
    }

    /**
     * Returns whether all cells in the passed cell ranges are hidden.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns
     *  Whether all cells in the passed cell ranges are hidden.
     */
    areRangesHidden(ranges: RangeSource): boolean {
        return everyInArraySource(ranges, range =>
            this.colCollection.isIntervalHidden(range.colInterval()) || this.rowCollection.isIntervalHidden(range.rowInterval())
        );
    }

    /**
     * Returns the visible parts of the passed cell range addresses.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns
     *  The visible parts of the passed cell ranges, in the same order. If a
     *  range is split into multiple parts (hidden columns or rows inside the
     *  range), the range parts are ordered row-by-row. If the entire range
     *  list is located in hidden columns or rows, the returned array will be
     *  empty.
     */
    getVisibleRanges(ranges: RangeSource): RangeArray {
        return RangeArray.map(ranges, range => {
            const colIntervals = this.colCollection.getVisibleIntervals(range.colInterval());
            const rowIntervals = this.rowCollection.getVisibleIntervals(range.rowInterval());
            return RangeArray.fromIntervals(colIntervals, rowIntervals);
        });
    }

    /**
     * Returns the address of a visible cell located as close as possible to
     * the passed cell address.
     *
     * @param address
     *  The address of the cell.
     *
     * @param [method]
     *  Specifies how to look for another cell, if the specified cell is
     *  hidden. For methods `NEXT_PREV` and `PREV_NEXT`, independently looks
     *  for visible columns and rows. It may happen that a preceding column,
     *  but a following row (or vice versa) will be found.
     *
     * @returns
     *  The address of a visible cell near the passed cell address; or `null`,
     *  if all columns or all rows are hidden.
     */
    getVisibleCell(address: Address, method = VisibleEntryMethod.EXACT): Address | null {
        const col = this.colCollection.findVisibleIndex(address.c, method) ?? -1;
        const row = this.rowCollection.findVisibleIndex(address.r, method) ?? -1;
        return ((col >= 0) && (row >= 0)) ? new Address(col, row) : null;
    }

    /**
     * Shrinks the passed range address to the visible area, if the leading or
     * trailing columns/rows of the range are hidden.
     *
     * @param range
     *  The address of the original cell range.
     *
     * @param [method="both"]
     *  Specifies how to shrink a cell range address to its visible part.
     *
     * @returns
     *  The address of the visible area of the passed cell range; or
     *  `undefined`, if the range is completely hidden. The first and last
     *  column, and the first and last row of the range are visible (unless a
     *  specific method has been passed to shrink columns or rows only), but
     *  some inner columns/rows in the resulting range may always be hidden.
     */
    shrinkRangeToVisible(range: Range, method?: ShrinkVisibleMethod): Opt<Range> {

        // shrunken visible column interval
        const colInterval = (method !== "rows") ? this.colCollection.shrinkIntervalToVisible(range.colInterval()) : range.colInterval();
        // shrunken visible row interval
        const rowInterval = !colInterval ? null : (method !== "columns") ? this.rowCollection.shrinkIntervalToVisible(range.rowInterval()) : range.rowInterval();

        return (colInterval && rowInterval) ? Range.fromIntervals(colInterval, rowInterval) : undefined;
    }

    /**
     * Returns the address of a range that covers the passed range, and that
     * has been expanded to all hidden columns and rows directly preceding and
     * following the passed range.
     *
     * @param range
     *  The address of a range to be expanded.
     *
     * @returns
     *  An expanded cell range address including all leading and trailing
     *  hidden columns and rows.
     */
    expandRangeToHidden(range: Range): Range {

        // expanded column/row intervals
        const colInterval = this.colCollection.expandIntervalToHidden(range.colInterval());
        const rowInterval = this.rowCollection.expandIntervalToHidden(range.rowInterval());

        return Range.fromIntervals(colInterval, rowInterval);
    }

    /**
     * Returns the addresses of the merged cell ranges covering the visible
     * parts of the passed ranges. First, the cell ranges will be expanded to
     * the leading and trailing hidden columns and rows. This may reduce the
     * number of ranges in the result, if there are only hidden columns or rows
     * between the passed ranges.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns
     *  The addresses of the merged cell ranges, shrunken to leading and
     *  trailing visible columns and rows (but may contain inner hidden columns
     *  or rows).
     */
    mergeAndShrinkRanges(ranges: RangeSource): RangeArray {

        // expand to hidden columns/rows (modify array in-place), merge the ranges
        // (result may be smaller due to expansion to hidden columns/rows)
        const arr = RangeArray.map(ranges, range => this.expandRangeToHidden(range)).merge();

        // reduce merged ranges to visible parts (filter out ranges completely hidden)
        return RangeArray.map(arr, range => this.shrinkRangeToVisible(range));
    }

    // split settings ---------------------------------------------------------

    /**
     * Returns whether the view split mode is enabled for the columns in this
     * sheet, regardless of the specific split mode (dynamic or frozen mode).
     *
     * @returns
     *  Whether the view split mode is enabled for the columns in this
     *  sheet.
     */
    hasColSplit(): boolean {
        return this.#getSplitSizeValue(true) > 0;
    }

    /**
     * Returns whether the view split mode is enabled for the rows in this
     * sheet, regardless of the specific split mode (dynamic or frozen mode).
     *
     * @returns
     *  Whether the view split mode is enabled for the rows in this sheet.
     */
    hasRowSplit(): boolean {
        return this.#getSplitSizeValue(false) > 0;
    }

    /**
     * Returns whether the view split mode is enabled for this sheet,
     * regardless of the specific split mode (dynamic or frozen mode).
     *
     * @returns
     *  Whether the view split mode is enabled for the sheet.
     */
    hasSplit(): boolean {
        return this.hasColSplit() || this.hasRowSplit();
    }

    /**
     * Returns the current view split mode of this sheet.
     *
     * @returns
     *  The current view split mode. The value `null` indicates that the sheet
     *  is not split at all.
     */
    getSplitMode(): SplitMode | null {
        return this.hasSplit() ? this.propSet.get("splitMode") : null;
    }

    /**
     * Returns whether this sheet is currently split, and dynamic split mode is
     * activated. Returns also `true`, if the split is currently frozen, but
     * will thaw to dynamic split mode (`FROZEN_SPLIT` mode).
     *
     * @returns
     *  Whether dynamic split mode is activated (one of the split modes `SPLIT`
     *  or `FROZEN_SPLIT`).
     */
    hasDynamicSplit(): boolean {
        const splitMode = this.getSplitMode();
        return (splitMode === SplitMode.SPLIT) || (splitMode === SplitMode.FROZEN_SPLIT);
    }

    /**
     * Returns whether this sheet is currently split, and the frozen split mode
     * is activated.
     *
     * @returns
     *  Whether frozen split mode is activated (one of the split modes `FROZEN`
     *  or `FROZEN_SPLIT`).
     */
    hasFrozenSplit(): boolean {
        const splitMode = this.getSplitMode();
        return (splitMode === SplitMode.FROZEN) || (splitMode === SplitMode.FROZEN_SPLIT);
    }

    /**
     * Returns the width of the left area in a sheet in split mode, in 1/100 of
     * millimeters. In frozen split mode, returns the total size of the frozen
     * columns.
     *
     * @returns
     *  The width of the left area if the sheet is in split mode, otherwise
     *  zero.
     */
    getSplitWidthHmm(): number {
        return this.#getSplitSize(true, false);
    }

    /**
     * Returns the height of the upper area in a sheet in split mode, in 1/100
     * of millimeters. In frozen split mode, returns the total size of the
     * frozen rows.
     *
     * @returns
     *  The height of the upper area if the sheet is in split mode, otherwise
     *  zero.
     */
    getSplitHeightHmm(): number {
        return this.#getSplitSize(false, false);
    }

    /**
     * Returns the width of the left area in a sheet in split mode, in pixels
     * according to the current sheet zoom factor. In frozen split mode,
     * returns the total size of the frozen columns.
     *
     * @returns
     *  The width of the left area if the sheet is in split mode, otherwise
     *  zero.
     */
    getSplitWidth(): number {
        return this.#getSplitSize(true, true);
    }

    /**
     * Returns the height of the upper area in a sheet in split mode, in pixels
     * according to the current sheet zoom factor. In frozen split mode,
     * returns the total size of the frozen rows.
     *
     * @returns
     *  The height of the upper area if the sheet is in split mode, otherwise
     *  zero.
     */
    getSplitHeight(): number {
        return this.#getSplitSize(false, true);
    }

    /**
     * Returns the column/row interval shown in the leading area of the sheet.
     * If the first or last visible columns/rows are visible less than half of
     * their size, they will not be included in the interval, with one
     * exception: If the resulting interval would be empty, it will contain the
     * first visible column/row instead, regardless how much of its size is
     * visible.
     *
     * @param columns
     *  Whether to return the column interval of the left sheet area (`true`),
     *  or the row interval of the top sheet area (`false`).
     *
     * @returns
     *  The column/row interval shown in the leading area of the sheet if it is
     *  in split mode, otherwise `null`.
     */
    getSplitInterval(columns: boolean): Interval | null {

        // current split size (number of columns/rows in frozen split; early exit without split)
        const splitSize = this.#getSplitSizeValue(columns);
        if (splitSize === 0) { return null; }

        // start scroll anchor in the leading sheet area
        const startAnchor = this.propSet.get(columns ? "anchorLeft" : "anchorTop");
        // the resulting column/row interval
        const interval = new Interval(Math.floor(startAnchor));

        // build column/row interval in frozen split mode directly
        if (this.hasFrozenSplit()) {
            interval.last = interval.first + splitSize - 1;
            return interval;
        }

        // the column/row collection
        const collection = columns ? this.colCollection : this.rowCollection;
        // end scroll anchor in the leading sheet area
        const offsetHmm = collection.convertScrollAnchorToHmm(startAnchor);
        const endAnchor = collection.getScrollAnchorByOffset(offsetHmm + splitSize);

        // build the resulting interval
        interval.last = Math.floor(endAnchor);

        // exclude last column/row, if visible less than half of its size
        if (!interval.single() && (endAnchor % 1 < 0.5)) {
            interval.last -= 1;
        }

        // exclude first column/row, if visible less than half of its size
        if (!interval.single() && (startAnchor % 1 > 0.5)) {
            interval.first += 1;
        }

        return interval;
    }

    /**
     * Returns the column interval shown in the left area of the sheet. If the
     * first or last visible columns are visible less than half of their width,
     * they will not be included in the interval, with one exception: If the
     * resulting interval would be empty, it will contain the first visible
     * column instead, regardless how much of its width is visible.
     *
     * @returns
     *  The column interval shown in the left area of the sheet if it is in
     *  split mode, otherwise `null`.
     */
    getSplitColInterval(): Interval | null {
        return this.getSplitInterval(true);
    }

    /**
     * Returns the row interval shown in the upper area of the sheet. If the
     * first or last visible rows are visible less than half of their height,
     * they will not be included in the interval, with one exception: If the
     * resulting interval would be empty, it will contain the first visible row
     * instead, regardless how much of its height is visible.
     *
     * @returns
     *  The row interval shown in the upper area of the sheet if it is in split
     *  mode, otherwise `null`.
     */
    getSplitRowInterval(): Interval | null {
        return this.getSplitInterval(false);
    }

    /**
     * Sets the split size for dynamic split mode, and updates the scroll
     * anchor properties, if the horizontal or vertical split will be enabled.
     *
     * @param splitWidth
     *  The new value for the sheet property "splitWidth", in 1/100 of
     *  millimeters. The value zero will disable the split between left and
     *  right sheet area.
     *
     * @param splitHeight
     *  The new value for the sheet property "splitHeight", in 1/100 of
     *  millimeters. The value zero will disable the split between upper and
     *  lower sheet area.
     *
     * @param [sheetProps]
     *  Additional sheet properties to be set at this sheet model.
     */
    setDynamicSplit(splitWidth: number, splitHeight: number, sheetProps?: Partial<ISheetProps>): void {

        // the old split size
        const oldSplitWidth = this.getSplitWidthHmm();
        const oldSplitHeight = this.getSplitHeightHmm();

        // the new sheet properties for split mode
        sheetProps = sheetProps ? { ...sheetProps } : dict.create();
        sheetProps.splitMode = SplitMode.SPLIT;
        sheetProps.splitWidth = splitWidth;
        sheetProps.splitHeight = splitHeight;

        // Update left/right scroll positions. Copy from left to right, if
        // split has been disabled, or copy from right to left, if split
        // has been enabled.
        if ((oldSplitWidth > 0) && (sheetProps.splitWidth === 0)) {
            sheetProps.anchorRight = this.propSet.get("anchorLeft");
        } else if ((oldSplitWidth === 0) && (sheetProps.splitWidth > 0)) {
            sheetProps.anchorLeft = this.propSet.get("anchorRight");
        }

        // Update top/bottom scroll positions. Copy from top to bottom, if
        // split has been disabled, or copy from bottom to top, if split
        // has been enabled.
        if ((oldSplitHeight > 0) && (sheetProps.splitHeight === 0)) {
            sheetProps.anchorBottom = this.propSet.get("anchorTop");
        } else if ((oldSplitHeight === 0) && (sheetProps.splitHeight > 0)) {
            sheetProps.anchorTop = this.propSet.get("anchorBottom");
        }

        // set the new sheet properties
        this.propSet.update(sheetProps);
    }

    /**
     * Sets the split size for frozen split mode, and updates the scroll anchor
     * properties.
     *
     * @param colInterval
     *  If set to an index interval, the new column interval to be shown in
     *  frozen split mode. The value `null` will disable the split between left
     *  and right sheet area.
     *
     * @param rowInterval
     *  If set to an index interval, the new row interval to be shown in frozen
     *  split mode. The value `null` will disable the split between top and
     *  bottom sheet area.
     *
     * @param [sheetProps]
     *  Additional sheet properties to be set at this sheet model.
     */
    setFrozenSplit(colInterval: Interval | null, rowInterval: Interval | null, sheetProps?: Partial<ISheetProps>): void {

        // the new view attributes for split mode
        sheetProps = sheetProps ? { ...sheetProps } : dict.create();
        sheetProps.splitMode = this.hasDynamicSplit() ? SplitMode.FROZEN_SPLIT : SplitMode.FROZEN;

        // set column interval if specified
        if (colInterval) {
            sheetProps.splitWidth = colInterval.size();
            sheetProps.anchorLeft = colInterval.first;
            sheetProps.anchorRight = colInterval.last + 1;
        } else {
            sheetProps.splitWidth = 0;
        }

        // set row interval if specified
        if (rowInterval) {
            sheetProps.splitHeight = rowInterval.size();
            sheetProps.anchorTop = rowInterval.first;
            sheetProps.anchorBottom = rowInterval.last + 1;
        } else {
            sheetProps.splitHeight = 0;
        }

        // set the sheet properties
        this.propSet.update(sheetProps);
    }

    /**
     * Resets all split properties, effectively disabling any split view that
     * is currently active.
     */
    clearSplit(): void {
        this.setDynamicSplit(0, 0);
    }

    // sheet operations -------------------------------------------------------

    /**
     * Returns whether this sheet is visible in the user interface.
     *
     * @returns
     *  Whether this sheet is visible.
     */
    isVisible(): boolean {
        return this.#getSheetAttrs().visible;
    }

    /**
     * Returns whether this sheet is locked. In locked sheets, it is not
     * possible to insert, delete, or modify columns and rows, to edit
     * protected cells, or to manipulate drawing objects.
     *
     * @returns
     *  Whether this sheet is locked.
     */
    isLocked(): boolean {
        return !this.isWorksheet || this.#getSheetAttrs().locked;
    }

    /**
     * Returns whether this sheet contains at least one table range (including
     * autofilter) with active filter rules.
     *
     * @returns
     *  Whether this sheet contains at least one table range with active filter
     *  rules.
     */
    isFiltered(): boolean {
        return this.tableCollection.hasFilteredTables();
    }

    /**
     * Returns the current raw zoom factor of this sheet (the original value of
     * the sheet property "zoom"). To get the effective zoom factor that will
     * be used to render the cell contents, see the method `getEffectiveZoom()`.
     *
     * @returns
     *  The current raw zoom factor, as floating-point number. The value `1`
     *  represents a zoom of 100%.
     */
    getZoom(): number {
        return this.#originalZoom;
    }

    /**
     * Changes the current raw zoom factor of this sheet (the value of the
     * sheet property "zoom").
     *
     * @param zoom
     *  The new raw zoom factor, as floating-point number. The value `1`
     *  represents a zoom of 100%.
     */
    setZoom(zoom: number): void {
        this.propSet.set("zoom", zoom);
    }

    /**
     * Returns whether the cell grid has to be displayed for this sheet.
     *
     * @returns
     *  Whether the cell grid has to be displayed for this sheet.
     */
    isGridVisible(): boolean {
        return this.isCellType && this.propSet.get("showGrid");
    }

    /**
     * Changes the visibility of the cell grid for this sheet.
     *
     * @param state
     *  Whether the cell grid has to be displayed for this sheet.
     */
    toggleGridVisibility(state: boolean): void {
        if (this.isCellType) { this.propSet.set("showGrid", state); }
    }

    /**
     * Returns the current grid color of this sheet.
     *
     * @returns
     *  The current grid color of this sheet.
     */
    getGridColor(): Color {
        return this.propSet.get("gridColor");
    }

    /**
     * On first invocation, initializes the dynamic properties for this sheet
     * from the imported sheet attributes.
     */
    @onceMethod
    initializePropSet(): void {

        // DOCS-1931: prefer sheet properties from (reload) launch options
        let sheetAttrs = this.#getSheetAttrs() as unknown as Dict;
        const launchAttrs = pick.array(this.docApp.launchConfig, "sheetAttrsArray", true)[this.getIndex()];
        if (is.dict(launchAttrs)) { sheetAttrs = { ...sheetAttrs, ...launchAttrs }; }

        // all selected ranges (convert plain JSON data to instance of `RangeArray`)
        const selectedRanges = this.addressFactory.parseRangeList(to.string(sheetAttrs.selectedRanges, ""));
        // the array index of the active range (bug 34549: filter may send invalid index)
        const activeIndex = to.number(sheetAttrs.activeIndex, 0);
        // address of the active cell (convert string to instance of `Address`)
        const activeCell = this.addressFactory.parseAddress(to.string(sheetAttrs.activeCell, ""));
        // the array of selected drawing objects
        const selectedDrawings = to.array(sheetAttrs.selectedDrawings, true).filter(isPosition);
        // selection, built from multiple sheet attributes, expanded to merged ranges
        let selection = selectedRanges ? new SheetSelection(this.#getExpandedRanges(selectedRanges), activeIndex, activeCell, selectedDrawings) : undefined;

        // show warnings for invalid selection settings (bug 34549)
        if (!selection) {
            modelLogger.warn(`$badge{SheetModel} initializePropSet: sheet=${this.getIndex()}, invalid selection`);
            selection = SheetSelection.createFromAddress(Address.A1);
            selection.ranges = this.#getExpandedRanges(selection.ranges);
        } else {
            if (selection.active !== activeIndex) {
                modelLogger.warn(`$badge{SheetModel} initializePropSet: sheet=${this.getIndex()}, invalid range index in selection`);
            }
            if (!selection.activeRange()!.containsAddress(selection.address)) {
                modelLogger.warn(`$badge{SheetModel} initializePropSet: sheet=${this.getIndex()}, invalid active cell in selection`);
                selection.address = selection.activeRange()!.a1.clone();
            }
        }

        // the sheet properties
        const sheetProps: Partial<ISheetProps> = {
            selection,
            zoom: to.number(sheetAttrs.zoom, 1),
            showGrid: to.boolean(sheetAttrs.showGrid, true),
            gridColor: Color.parseJSON(sheetAttrs.gridColor),
            splitMode: to.enum(SplitMode, sheetAttrs.splitMode, SplitMode.SPLIT),
            splitWidth: to.number(sheetAttrs.splitWidth, 0),
            splitHeight: to.number(sheetAttrs.splitHeight, 0),
            activePane: to.enum(PanePos, sheetAttrs.activePane, PanePos.BR),
            anchorLeft: to.number(sheetAttrs.scrollLeft, 0),
            anchorRight: to.number(sheetAttrs.scrollRight, 0),
            anchorTop: to.number(sheetAttrs.scrollTop, 0),
            anchorBottom: to.number(sheetAttrs.scrollBottom, 0)
        };

        // Sheet view attributes in operations use the top-left grid pane as default pane
        // which is always visible without active view split. Internally, the view uses the
        // bottom-right grid pane as the default pane instead. According to the split settings,
        // the attribute "activePane", and the scroll attributes have to be translated.

        // no horizontal split: map all left pane settings to right pane settings
        if (sheetProps.splitWidth === 0) {
            sheetProps.activePane = getNextPanePos(sheetProps.activePane!, PaneSide.R);
            sheetProps.anchorRight = sheetProps.anchorLeft;
            delete sheetProps.anchorLeft;
        }

        // no vertical split: map all top pane settings to bottom pane settings
        if (sheetProps.splitHeight === 0) {
            sheetProps.activePane = getNextPanePos(sheetProps.activePane!, PaneSide.B);
            sheetProps.anchorBottom = sheetProps.anchorTop;
            delete sheetProps.anchorTop;
        }

        // update the sheet properties
        this.propSet.update(sheetProps);
    }

    /**
     * Returns all changed sheet attributes used in "changeSheet" document
     * operations according to the current dynamic sheet properties.
     *
     * @returns
     *  All changed sheet attributes matching the current sheet properties.
     */
    getChangedViewAttributes(): Dict {

        // the current merged sheet attributes
        const oldSheetAttrs = this.#getSheetAttrs();
        // the current selection (original value, not cloned)
        const selection = this.propSet.get("selection");

        // converts the passed scroll anchor to a simple column/row index
        const getScrollIndex = (scrollAnchor: number, columns: boolean): number =>
            Math.min(Math.round(scrollAnchor), this.addressFactory.getMaxIndex(columns));

        // sheet attributes according to the view settings (some attributes need further adjustment)
        const newSheetAttrs: Partial<SheetAttributes> = {
            selectedRanges: selection.ranges.toOpStr(),
            activeIndex: selection.active,
            activeCell: selection.address.toOpStr(),
            selectedDrawings: selection.drawings,
            zoom: this.#originalZoom,
            showGrid: this.propSet.get("showGrid"),
            gridColor: this.propSet.get("gridColor").toJSON(),
            splitWidth: this.propSet.get("splitWidth"),
            splitHeight: this.propSet.get("splitHeight"),
            scrollLeft: getScrollIndex(this.propSet.get("anchorLeft"), true),
            scrollRight: getScrollIndex(this.propSet.get("anchorRight"), true),
            scrollTop: getScrollIndex(this.propSet.get("anchorTop"), false),
            scrollBottom: getScrollIndex(this.propSet.get("anchorBottom"), false)
        };

        // Sheet view attributes in operations use the top-left grid pane as default pane
        // which is always visible without active view split. Internally, the view uses the
        // bottom-right grid pane as the default pane instead. According to the split settings,
        // the attribute "activePane" and the scroll attributes have to be translated.

        // whether a horizontal (left/right) or a vertical (top/bottom) split is active
        const hSplit = newSheetAttrs.splitWidth! > 0;
        const vSplit = newSheetAttrs.splitHeight! > 0;

        // active pane needs multiple adjustments
        let activePane = this.propSet.get("activePane");

        // no horizontal split: map all right pane settings to left pane settings
        if (!hSplit) {
            activePane = getNextPanePos(activePane, PaneSide.L);
            newSheetAttrs.scrollLeft = newSheetAttrs.scrollRight;
            delete newSheetAttrs.scrollRight;
        }

        // no vertical split: map all bottom pane settings to top pane settings (ODF: to bottom)
        if (!vSplit) {
            if (this.docModel.docApp.isODF()) {
                activePane = getNextPanePos(activePane, PaneSide.B);
                newSheetAttrs.scrollBottom = newSheetAttrs.scrollTop;
                delete newSheetAttrs.scrollTop;
            } else {
                activePane = getNextPanePos(activePane, PaneSide.T);
                newSheetAttrs.scrollTop = newSheetAttrs.scrollBottom;
                delete newSheetAttrs.scrollBottom;
            }
        }

        // reset frozen split mode, if all splits are gone
        const splitMode = (hSplit || vSplit) ? this.propSet.get("splitMode") : SplitMode.SPLIT;

        // bug 46166: in frozen split mode, the scrollable pane is always active regardless of view settings
        if (splitMode !== SplitMode.SPLIT) {
            activePane = !hSplit ? PanePos.BL : !vSplit ? PanePos.TR : PanePos.BR;
        }

        // assign effective sheet attributes
        newSheetAttrs.splitMode = splitMode;
        newSheetAttrs.activePane = activePane;

        // reduce new sheet attribute to changed ones
        for (const [name, value] of dict.entries(newSheetAttrs)) {
            if (_.isEqual(oldSheetAttrs[name], value)) {
                delete newSheetAttrs[name];
            }
        }

        return newSheetAttrs;
    }

    /**
     * Emits a "refresh:ranges" event to the listeners of this sheet model,
     * intended to initiate updating the visual representation of the specified
     * ranges. Can be used by model instances that want to refresh the display
     * of specific sheet ranges without generating any document operations.
     *
     * @param rangesArgs
     *  The addresses of the cell ranges to be refreshed.
     */
    refreshRanges(...rangesArgs: Array<Iterable<Range>>): void {
        this.#refreshRanges.appendIterable(itr.flatten(rangesArgs));
        this.#execRefreshRanges();
    }

    /**
     * Returns whether the contents of the passed cell ranges can be restored
     * completely by undo operations after deleting them.
     *
     * @param ranges
     *  The addresses of the cell ranges to be deleted.
     *
     * @returns
     *  Whether the cell ranges can be deleted safely.
     */
    canRestoreDeletedRanges(ranges: RangeArray): boolean {
        // bug 55208: deleting tables cannot be undone
        // bug 56368: check for unsupported drawings
        return this.tableCollection.canRestoreDeletedRanges(ranges) && this.drawingCollection.canRestoreDeletedRanges(ranges);
    }

    // operation generators ---------------------------------------------------

    /**
     * Creates a new empty operations generator, that has been initialized for
     * this sheet.
     *
     * @param [options]
     *  Optional parameters that will be passed to the constructor of the
     *  operations generator.
     *
     * @returns
     *  A new empty operations generator for this sheet.
     */
    createOperationGenerator(options?: SheetGeneratorConfig): SheetOperationGenerator {
        const generator = new SheetOperationGenerator(this.docModel, options);
        generator.sheet = this.getIndex();
        return generator;
    }

    /**
     * Creates a new operations generator associated to this sheet, invokes
     * the callback function, applies all operations contained in the
     * generator, and sends them to the server.
     *
     * @param callback
     *  The callback function. See description of the method
     *  `createAndApplySheetOperations` for details. Will be called in the
     *  context of this sheet model.
     *
     * @param [options]
     *  Optional parameters. Supports all options supported by the method
     *  `SpreadsheetModel::createAndApplySheetOperations`.
     *
     * @returns
     *  A promise that will fulfil after the operations have been applied and
     *  sent successfully (with the result of the callback function); or that
     *  will reject, if the callback has returned a rejected promise (with the
     *  result of that promise), or if sending the operations has failed for
     *  internal reasons (with an object containing a property "cause" set to
     *  "operation").
     */
    createAndApplyOperations<T>(callback: (this: this, generator: SheetOperationGenerator) => MaybeAsync<T>, options?: BuildOperationsOptions): JPromise<T> {
        return this.docModel.createAndApplySheetOperations(generator => {
            generator.sheet = this.getIndex();
            return callback.call(this, generator);
        }, options);
    }

    /**
     * Creates a new operation builder for spreadsheet operations and undo
     * operations by calling `SpreadsheetModel::buildOperations`, invokes the
     * callback function with a sheet cache object associated to this sheet,
     * applies all operations collected in the builder, sends them to the
     * server, and creates an undo action with the undo operations that have
     * been collected in the builder.
     *
     * @param callback
     *  The callback function. Receives sheet operation cache for this sheet
     *  model. May return a promise to defer applying and sending the
     *  operations until the promise has been resolved.
     *
     * @param [options]
     *  Optional parameters. Supports all options supported by the method
     *  `SpreadsheetModel::buildOperations`, and by the constructor of the
     *  class `SheetCache`.
     *
     * @returns
     *  A promise that will fulfil after the operations have been applied and
     *  sent successfully; or reject, if the callback has thrown an exception,
     *  or returned a rejected promise, or if applying the operations has
     *  failed.
     */
    buildOperations(callback: (sheetcache: SheetCache) => MaybeAsync, options?: BuildOperationsOptions & SheetCacheConfig): JPromise<SheetCacheFlushResult> {

        // create a sheet cache for this sheet model, invoke the callback function
        const promise = this.docModel.buildOperations(builder => {
            const sheetCache = builder.createSheetCache(this, options);
            return callback.call(self, sheetCache);
        }, options);

        // fulfil the returned promise with the result of the sheet cache
        return jpromise.fastThen(promise, builderResult => builderResult.sheetResults.get(this)!);
    }

    /**
     * Generates the operations, and the undo operations, to change the
     * formatting attributes of this sheet.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param attrSet
     *  The sheet attribute set to be inserted into the operations.
     */
    generateChangeOperations(sheetCache: SheetOperationGenerator, attrSet: Dict): void {
        const undoAttrSet = this.getUndoAttributeSet(attrSet);
        sheetCache.generateSheetOperation(CHANGE_SHEET, { attrs: attrSet });
        sheetCache.generateSheetOperation(CHANGE_SHEET, { attrs: undoAttrSet }, { undo: true });
    }

    /**
     * Generates the undo operation to restore the complete contents of this
     * sheet after it has been deleted.
     *
     * @param _sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateRestoreOperations(_sheetCache: SheetCache): JPromise {
        // TODO
        return jpromise.resolve();
    }

    /**
     * Generates the operations and undo operations to update or restore the
     * formulas of all cells, defined names, data validation rules, formatting
     * rules, and drawing objects in this sheet.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{SheetModel} generateUpdateTaskOperations")
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise {
        return jpromise.fastChain(
            () => this.cellCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.mergeCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.hlinkCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.nameCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.tableCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.dvRuleCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.cfRuleCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.drawingCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.noteCollection.generateUpdateTaskOperations(sheetCache, updateTask),
            () => this.commentCollection.generateUpdateTaskOperations(sheetCache, updateTask)
        );
    }

    /**
     * Generates the operations and undo operations to update and restore the
     * inactive anchor attributes of the top-level drawing objects, after the
     * size or visibility of the columns or rows in the sheet has been changed.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{SheetModel} generateRefreshAnchorOperations")
    generateRefreshAnchorOperations(generator: SheetOperationGenerator): JPromise {
        return jpromise.fastChain(
            () => this.drawingCollection.generateRefreshAnchorOperations(generator),
            () => this.noteCollection.generateRefreshAnchorOperations(generator),
            () => this.commentCollection.generateRefreshAnchorOperations(generator)
        );
    }

    /**
     * Generates the operations, and the undo operations, to insert blank
     * columns or rows, or to delete the specified columns or rows, by moving
     * the remaining existing cells and other contents in this sheet to the
     * left, right, top, or bottom.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param intervals
     *  The target intervals to be inserted or deleted.
     *
     * @param direction
     *  Where to move the existing cells to.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated, or
     *  that will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - "cells:pushoff": Inserting new cells is not possible because this
     *    would push existing cells off the end of the sheet.
     *  - "formula:matrix:insert": Inserting new cells is not possible because
     *    this would split a matrix formula.
     *  - "formula:matrix:delete": Deleting cells is not possible because this
     *    would shorten a matrix formula.
     *  - "table:move": Deleting rows would invalidate a table range.
     */
    @modelLogger.profileMethod("$badge{SheetModel} generateMoveOperations")
    generateMoveOperations(builder: OperationBuilder, intervals: IntervalSource, direction: Direction): JPromise {

        const columns = !isVerticalDir(direction);
        modelLogger.trace(() => `intervals=${IntervalArray.cast(intervals).toOpStr(columns)} dir=${direction}`);

        // create the sheet cache, register the address transfomer (preparations for insertion/deletion)
        const transformer = AddressTransformer.fromIntervals(this.addressFactory, intervals, direction);
        const sheetCache = builder.createSheetCache(this, { transformer });

        // generate the move operations for the intervals (must be executed first)
        const collection = columns ? this.colCollection : this.rowCollection;
        collection.generateMoveOperations(sheetCache, transformer);

        // update the contents in all sheets in the document
        const updateTask = new MoveCellsUpdateTask(sheetCache.sheet, transformer);
        return this.docModel.generateUpdateTaskOperations(builder, updateTask);
    }

    /**
     * Generates the operations, and the undo operations, to insert blank cells
     * at the specified cell range, or to delete the specified cell range, by
     * moving the remaining existing cells and other contents to the left,
     * right, top, or bottom.
     *
     * @param builder
     *  The operation builder to be filled with the operations.
     *
     * @param range
     *  The address of the target cell range to be inserted or deleted.
     *
     * @param direction
     *  Where to move the existing cells to.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated, or
     *  that will reject with an object with property "cause" set to one of the
     *  following error codes:
     *  - "cells:pushoff": Inserting new cells is not possible because this
     *    would push existing cells off the end of the sheet.
     *  - "formula:matrix:insert": Inserting new cells is not possible because
     *    this would split a matrix formula.
     *  - "formula:matrix:delete": Deleting cells is not possible because this
     *    would shorten a matrix formula.
     *  - "table:move": Deleting rows would invalidate a table range.
     */
    @modelLogger.profileMethod("$badge{SheetModel} generateMoveCellsOperations")
    generateMoveCellsOperations(builder: OperationBuilder, range: Range, direction: Direction): JPromise {

        modelLogger.trace(() => `range=${range} dir=${direction}`);

        // create the sheet cache, register the address transfomer (preparations for insertion/deletion)
        const transformer = AddressTransformer.fromRange(this.addressFactory, range, direction);
        const sheetCache = builder.createSheetCache(this, { transformer, moveCells: true });

        // generate the "moveCells" operations for the cells (must be executed first)
        sheetCache.generateMoveCellsOperation(range, direction);
        sheetCache.generateMoveCellsOperation(range, getReverseDirection(direction), { undo: true });

        // update the contents in all sheets in the document
        const updateTask = new MoveCellsUpdateTask(sheetCache.sheet, transformer);
        return this.docModel.generateUpdateTaskOperations(builder, updateTask);
    }

    // internal public methods ------------------------------------------------

    /**
     * Postprocessing of the sheet model, after all import operations have been
     * applied successfully.
     *
     * Called from the application import process. MUST NOT be called from
     * external code.
     *
     * @internal
     *
     * @returns
     *  A promise that will fulfil when this sheet model has been postprocessed
     *  successfully; or that will reject on any error.
     */
    async postProcessImport(): Promise<void> {
        // calculate missing settings for rows
        await this.rowCollection.postProcessImport();
        // create the token arrays of all formula cells
        await this.cellCollection.postProcessImport();
    }

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * settings from the passed source sheet model into this sheet model.
     *
     * @param context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromSheetModel
     *  The existing sheet model whose contents will be cloned into this sheet
     *  model.
     *
     * @throws
     *  An `OperationError`, if applying the operation has failed, e.g. if a
     *  required property is missing in the operation.
     */
    applyCopySheetOperation(context: SheetOperationContext, fromSheetModel: SheetModel): void {

        // copy all formatting attributes of the sheet
        this.setAttributes(fromSheetModel.getExplicitAttributeSet(true));

        // clone the contents of all collections
        this.colCollection.applyCopySheetOperation(context, fromSheetModel.colCollection);
        this.rowCollection.applyCopySheetOperation(context, fromSheetModel.rowCollection);
        this.cellCollection.applyCopySheetOperation(context, fromSheetModel.cellCollection);
        this.mergeCollection.applyCopySheetOperation(context, fromSheetModel.mergeCollection);
        this.hlinkCollection.applyCopySheetOperation(context, fromSheetModel.hlinkCollection);
        this.nameCollection.applyCopySheetOperation(context, fromSheetModel.nameCollection);
        this.tableCollection.applyCopySheetOperation(context, fromSheetModel.tableCollection);
        this.dvRuleCollection.applyCopySheetOperation(context, fromSheetModel.dvRuleCollection);
        this.cfRuleCollection.applyCopySheetOperation(context, fromSheetModel.cfRuleCollection);
        this.drawingCollection.applyCopySheetOperation(context, fromSheetModel.drawingCollection);
        this.noteCollection.applyCopySheetOperation(context, fromSheetModel.noteCollection);
        this.commentCollection.applyCopySheetOperation(context, fromSheetModel.commentCollection);

        // copy the sheet properties after cloning all sheet contents (bug 50600: cloning
        // drawing objects would modify the drawing selection if already existing)
        this.propSet.update(fromSheetModel.propSet.all());
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the internal zoom properties according to the state of the own
     * property set.
     */
    #updateZoom(): void {
        const zoom = this.#originalZoom = this.propSet.get("zoom");
        this.#effectiveZoom = zoom * ZOOM_SCALE_ADJUST;
    }

    /**
     * Returns the own merged sheet attributes.
     */
    #getSheetAttrs(): Readonly<SheetAttributes> {
        return this.getMergedAttributeSet(true).sheet;
    }

    /**
     * Expands all ranges but entire column/row ranges to the merged ranges in
     * the sheet.
     */
    #getExpandedRanges(ranges: RangeSource): RangeArray {
        return RangeArray.map(ranges, range =>
            (this.addressFactory.isColRange(range) || this.addressFactory.isRowRange(range)) ? range : this.mergeCollection.expandRangeToMergedRanges(range)
        );
    }

    /**
     * Returns the raw value of the specified split size property.
     *
     * @param columns
     *  Whether to return the split width (`true`, value of the sheet property
     *  "splitWidth"), or the split height (`false`, value of the sheet
     *  property "splitHeight").
     *
     * @returns
     *  The raw value of the specified split size property.
     */
    #getSplitSizeValue(columns: boolean): number {
        return this.isCellType ? this.propSet.get(columns ? "splitWidth" : "splitHeight") : 0;
    }

    /**
     * Returns the width or height of the leading area in a sheet in split
     * mode, in 1/100 of millimeters, or in pixels according to the current
     * sheet zoom factor. In frozen split mode, returns the total size of the
     * frozen columns/rows.
     *
     * @param columns
     *  Whether to return the width of the left sheet area (`true`), or the
     *  height of the top sheet area (`false`).
     *
     * @param pixel
     *  Whether to return pixels (`true`), or 1/100 of millimeters (`false`).
     *
     * @returns
     *  The width or height of the leading area if the sheet is in split mode,
     *  otherwise zero.
     */
    #getSplitSize(columns: boolean, pixel: boolean): number {

        // return total size of frozen columns/rows
        if (this.hasFrozenSplit()) {
            const interval = this.getSplitInterval(columns);
            const collection = columns ? this.colCollection : this.rowCollection;
            return interval ? collection.getIntervalPosition(interval, { pixel }).size : 0;
        }

        // return size of dynamic split
        const splitSize = this.#getSplitSizeValue(columns);
        return pixel ? this.convertHmmToPixel(splitSize) : splitSize;
    }

    /**
     * Updates the frozen split properties.
     */
    #updateFrozenSplit(intervals: IntervalArray, insert: boolean, columns: boolean): void {

        // property names for frozen split settings
        const LEADING_ANCHOR_NAME = columns ? "anchorLeft" : "anchorTop";
        const TRAILING_ANCHOR_NAME = columns ? "anchorRight" : "anchorBottom";
        const SPLIT_SIZE_NAME = columns ? "splitWidth" : "splitHeight";

        // process the passed intervals as long as there is a frozen split (may vanish inbetween)
        for (const interval of intervals.values({ reverse: !insert })) {

            // the frozen column/row interval (exit loop if not frozen)
            const frozenInterval = this.hasFrozenSplit() ? this.getSplitInterval(columns) : null;
            if (!frozenInterval) { break; }

            // the new sheet properties
            const sheetProps = dict.createPartial<ISheetProps>();

            if (insert) {

                // the number of inserted columns/rows
                const insertSize = interval.size();

                // inserted before frozen interval: update scroll anchor
                if (interval.first < frozenInterval.first) {
                    sheetProps[LEADING_ANCHOR_NAME] = frozenInterval.first + insertSize;
                    sheetProps[TRAILING_ANCHOR_NAME] = frozenInterval.last + 1 + insertSize;

                // inserted inside frozen interval: update size of frozen interval
                } else if (interval.first <= frozenInterval.last) {
                    sheetProps[SPLIT_SIZE_NAME] = frozenInterval.size() + insertSize;
                    sheetProps[TRAILING_ANCHOR_NAME] = frozenInterval.last + 1 + insertSize;
                }

            } else {

                // deleted inside frozen interval: update size of frozen interval
                const intersectInterval = interval.intersect(frozenInterval);
                if (intersectInterval) {
                    sheetProps[SPLIT_SIZE_NAME] = frozenInterval.size() - intersectInterval.size();
                    // bug #55777: Prevent deleting last col/row
                    if (sheetProps[SPLIT_SIZE_NAME] === 0) { sheetProps[SPLIT_SIZE_NAME] = 1; }
                }

                // deleted before frozen interval: update leading scroll anchor
                if (interval.first < frozenInterval.first) {
                    sheetProps[LEADING_ANCHOR_NAME] = interval.first;
                }

                // deleted before or inside frozen interval: update trailing scroll anchor
                if (interval.first <= frozenInterval.last) {
                    sheetProps[TRAILING_ANCHOR_NAME] = interval.first;
                    // bug #55777: Prevent deleting last col/row
                    if (sheetProps[TRAILING_ANCHOR_NAME] === 0) { sheetProps[TRAILING_ANCHOR_NAME] = 1; }
                }
            }

            // bug 46166: validate view settings when removing a frozen split
            if (sheetProps[SPLIT_SIZE_NAME] === 0) {

                // refresh the active pane
                const oldActivePane = this.propSet.get("activePane");
                const newActivePane = getNextPanePos(oldActivePane, columns ? PaneSide.R : PaneSide.B);
                if (oldActivePane !== newActivePane) {
                    sheetProps.activePane = newActivePane;
                }

                // reset frozen mode if a single frozen split has been removed (no split at all anymore)
                if (!this.getSplitInterval(!columns)) {
                    sheetProps.splitMode = SplitMode.SPLIT;
                }
            }

            // commit the new sheet properties
            this.propSet.update(sheetProps);
        }
    }

    /**
     * Updates the selection after applying external move operations.
     */
    #moveCellsHandler(event: MoveCellsEvent): void {
        // only for external operations
        if (event.external && !event.importing) {
            let selection = this.getSelection();
            selection = event.transformer.transformSheetSelection(selection);
            this.setSelection(selection);
        }
    }

    /**
     * Updates the selection after inserting or deleting merged ranges.
     */
    #mergeCellsHandler(): void {
        const selection = this.getSelection();
        selection.ranges = this.#getExpandedRanges(selection.ranges);
        // DOCS-3628: move active address to top-left corner of merged range
        const activeRange = selection.activeRange();
        if (activeRange) { selection.address = activeRange.a1; }
        this.setSelection(selection);
    }

    /**
     * Updates the drawing selection after a drawing object has been inserted.
     */
    #insertDrawingHandler(drawingModel: SheetDrawingModelType): void {

        // the current sheet selection
        const selection = this.getSelection();
        if (selection.drawings.length === 0) { return; }

        // current position of the modified drawing object (without sheet index)
        const modelPos = drawingModel.getPosition();

        // transform drawing selection
        for (const xfPos of selection.drawings) {
            transformPositionInsert(xfPos, modelPos, 1);
        }

        // write back the selection
        this.setSelection(selection);
    }

    /**
     * Updates the drawing selection after a drawing object has been deleted.
     */
    #deleteDrawingHandler(drawingModel: SheetDrawingModelType): void {

        // the current sheet selection
        const selection = this.getSelection();
        if (selection.drawings.length === 0) { return; }

        // current position of the modified drawing object (without sheet index)
        const modelPos = drawingModel.getPosition();

        // transform drawing selection
        ary.deleteAllMatching(selection.drawings, xfPos => !transformPositionDelete(xfPos, modelPos, 1));

        // write back the selection
        this.setSelection(selection);
    }

    /**
     * Updates the drawing selection after a drawing object has been moved to
     * another index.
     */
    #moveDrawingHandler(drawingModel: SheetDrawingModelType, fromIndex: number): void {

        // the current sheet selection
        const selection = this.getSelection();
        if (selection.drawings.length === 0) { return; }

        // current position of the modified drawing object (without sheet index)
        const modelPos = drawingModel.getPosition();

        // transform drawing selection according to the event type
        // TS cannot convert type `[...number[], number]` to `[number, ...number[]]` directly
        const fromPos = [...modelPos.slice(0, -1), fromIndex] as unknown as Position;
        const toIdx = ary.at(modelPos, -1)!;
        for (const xfPos of selection.drawings) {
            transformPositionMove(xfPos, fromPos, 1, toIdx);
        }

        // write back the selection
        this.setSelection(selection);
    }

    /**
     * Emits a "refresh:ranges" event for the collected ranges (debounced into
     * a microtask).
     */
    @debounceMethod({ delay: "microtask" })
    #execRefreshRanges(): void {
        if (this.#refreshRanges.length) {
            // clear array before emitting the event (handlers may register new ranges)
            const ranges = this.#refreshRanges.merge();
            this.#refreshRanges.clear();
            this.trigger("refresh:ranges", this.createSheetEvent({ ranges }));
        }
    }
}
