/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import type { PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";

import { type SheetModel, SheetAttrsModel } from "@/io.ox/office/spreadsheet/model/sheetattrsmodel";
import { type TableFilterMatcher, convertFilterString, NoneFilterMatcher, DiscreteFilterMatcher } from "@/io.ox/office/spreadsheet/model/tablefiltermatcher";

// types ======================================================================

/**
 * An extended entry in a discrete filter list for a table column, as used in
 * JSON document operations.
 */
export interface ExtDiscreteFilterEntry {
    /** Entry type: One of "date" (TBD). */
    t: string;
    /** Entry value, depending on the "type" property. */
    v: string;
}

/**
 * An entry in a discrete filter list for a table column, as used in JSON
 * document operations. Either a plain string to be used to filter for cell
 * display strings, or an `ExtDiscreteFilterEntry` to filter for special cell
 * values or formatting.
 */
export type DiscreteFilterEntry = string | ExtDiscreteFilterEntry;

export interface TableFilterAttributes {

    /**
     * The type of the filter rule in this column. The following values are
     * supported:
     * - "none": No filter rule is active.
     * - "discrete": Discrete filter. The attribute "entries" contains all cell
     *   entries to be shown for the table column.
     */
    type: "none" | "discrete";

    /**
     * All entries (cell display strings) that will be shown for this table
     * column. Only used, if the filter type is "discrete".
     */
    entries: DiscreteFilterEntry[];

    /**
     * Whether the dropdown button of the filter column will be hidden.
     */
    hideButton: boolean;
}

export interface TableSortAttributes {

    /**
     * Specifies how to sort the values in the column.
     * - "none": The column will not be sorted.
     * - "value": The column will be sorted by the values in the cells.
     * - "font": The column will be sorted by the text color of the cells.
     * - "fill": The column will be sorted by the fill color of the cells.
     * - "icon": The column will be sorted by icons in the cells.
     *
     * The property "undo" contains the names of all attributes that need to be
     * inserted into the undo operation that will be generated when changing
     * this attribute.
     */
    type: "none" | "value" | "font" | "fill" | "icon";

    /**
     * If set to `true`, the values in the column will be sorted in descending
     * order, and the selected cell color or font color will be pushed to the
     * bottom of the table.
     */
    descending: boolean;

    /**
     * The internal index of the format structure containing the font color or
     * fill color. Only used for sort types "font" or "fill".
     */
    dxf: number;

    /**
     * The name of a sort list used for sorting the column entries.
     */
    list: string;

    /**
     * The name of the icon set used for sorting the column. Only used for sort
     * type "icon".
     */
    is: string;

    /**
     * The identifier of the icon in the icon set as specified by the attribute
     * "is". Only used for sort type "icon".
     */
    id: number;
}

/**
 * The type shape of attribute sets supported by table columns.
 */
export interface TableColumnAttributeSet {
    filter: TableFilterAttributes;
    sort: TableSortAttributes;
}

/**
 * Partial table column attribute set (all families and attributes are
 * optional).
 */
export type PtTableColumnAttributeSet = PtAttrSet<TableColumnAttributeSet>;

// constants ==================================================================

export const FILTER_ATTRIBUTES: AttributeConfigBag<TableFilterAttributes> = {
    // "undo" contains the names of all attributes that need to be inserted into the
    // undo operation that will be generated when changing this attribute
    type:       { def: "none", undo: ["entries"] },
    entries:    { def: [] },
    hideButton: { def: false }
};

export const SORT_ATTRIBUTES: AttributeConfigBag<TableSortAttributes> = {
    // "undo" contains the names of all attributes that need to be inserted into the
    // undo operation that will be generated when changing this attribute
    type:       { def: "none", undo: "*" },
    descending: { def: false },
    dxf:        { def: 0 },
    list:       { def: "" },
    is:         { def: "" },
    id:         { def: 0 }
};

// class TableColumnModel =====================================================

/**
 * Stores filter and sorting settings for a single column in a table range.
 */
export class TableColumnModel extends SheetAttrsModel<TableColumnAttributeSet> {

    // cached filter matcher (created on demand)
    #filterMatcher: Opt<TableFilterMatcher>;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this table column.
     *
     * @param [cloneModelOrAttrSet]
     *  A table column model to be cloned, or the initial table attribute set
     * (style sheet reference, and explicit table attributes).
     */
    constructor(sheetModel: SheetModel, attrSet?: PtTableColumnAttributeSet);
    constructor(sheetModel: SheetModel, cloneModel: TableColumnModel);
    // implementation
    constructor(sheetModel: SheetModel, cloneModelOrAttrSet?: TableColumnModel | PtTableColumnAttributeSet) {

        const clone = cloneModelOrAttrSet instanceof TableColumnModel;
        const attrSet = clone ? cloneModelOrAttrSet.getExplicitAttributeSet(true) : cloneModelOrAttrSet;
        super(sheetModel, ["filter", "sort"], attrSet, { autoClear: true });

        // clear cached filter matcher if any filter attribut changes
        this.on("change:attributes", () => {
            // performance: do not try to compare changed attributes to detect whether filter or sort has changed
            this.#filterMatcher?.destroy();
            this.#filterMatcher = undefined;
        });
    }

    protected override destructor(): void {
        this.#filterMatcher?.destroy();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the filter/sort dropdown button will be displayed for
     * this table column.
     *
     * @returns
     *  Whether the filter/sort dropdown button will be displayed for this
     *  table column.
     */
    isButtonVisible(): boolean {
        return !this.getMergedAttributeSet(true).filter.hideButton;
    }

    /**
     * Returns whether this column model contains active filter rules.
     *
     * @returns
     *  Whether this column model contains active filter rules.
     */
    isFiltered(): boolean {
        return this.getMergedAttributeSet(true).filter.type !== "none";
    }

    /**
     * Returns the discrete filter entries of this column model if available.
     *
     * @returns
     *  The discrete filter entries of this column model if available.
     */
    getDiscreteEntries(): Opt<DiscreteFilterEntry[]> {
        const { type, entries } = this.getMergedAttributeSet(true).filter;
        return (type === "discrete") ? entries.map(entry => is.string(entry) ? convertFilterString(entry) : entry) : undefined;
    }

    /**
     * Returns whether this column model contains active sort options.
     *
     * @returns
     *  Whether this column model contains active sort options.
     */
    isSorted(): boolean {
        return this.getMergedAttributeSet(true).sort.type !== "none";
    }

    /**
     * Returns whether this column model sorts its contents in descending
     * order.
     *
     * @returns
     *  Whether this column model sorts its contents in descending order.
     */
    isDescending(): boolean {
        return this.getMergedAttributeSet(true).sort.descending;
    }

    /**
     * Returns whether this column model contains any filter rules, or sort
     * options, that may require to refresh the table range containing this
     * column.
     *
     * @returns
     *  Whether this column model contains any filter/sorting settings.
     */
    isRefreshable(): boolean {
        return this.isFiltered() || this.isSorted();
    }

    /**
     * Returns the filter matcher according to the current filter attributes.
     *
     * @returns
     *  The filter matcher for this table column.
     */
    getFilterMatcher(): TableFilterMatcher {
        return (this.#filterMatcher ??= this.#createFilterMatcher());
    }

    // private methods --------------------------------------------------------

    #createFilterMatcher(): TableFilterMatcher {

        // the current filter attributes
        const filterAttrs = this.getMergedAttributeSet(true).filter;

        // create matchers for different filter types
        switch (filterAttrs.type) {

            case "none":
                break;

            case "discrete":
                if (is.array(filterAttrs.entries)) {
                    return new DiscreteFilterMatcher(this.docModel, filterAttrs.entries);
                }
                break;
        }

        // fallback to no filtering
        return new NoneFilterMatcher(this.docModel);
    }
}
