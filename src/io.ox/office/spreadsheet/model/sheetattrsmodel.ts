/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { AttrSetConstraint, PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { type AttributedModelConfig, type AttributedModelEventMap, AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";

import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SheetAttributePoolMap } from "@/io.ox/office/spreadsheet/model/style/sheetattributepool";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// re-exports =================================================================

export type { SheetModel };

// class SheetAttrsModel ======================================================

/**
 * Generic base class for models with formatting attributes contained by a
 * sheet in a spreadsheet document.
 */
export class SheetAttrsModel<
    AttrSetT extends AttrSetConstraint<AttrSetT>,
    EvtMapT extends AttributedModelEventMap<AttrSetT> = AttributedModelEventMap<AttrSetT>
> extends AttributedModel<SpreadsheetModel, SheetAttributePoolMap, AttrSetT, EvtMapT> {

    /**
     * The parent sheet model containing this model object.
     */
    readonly sheetModel: SheetModel;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The parent sheet model containing this model object.
     */
    protected constructor(sheetModel: SheetModel, families: Array<KeysOf<AttrSetT>>, attrSet?: PtAttrSet<AttrSetT>, config?: AttributedModelConfig<SpreadsheetModel, SheetAttributePoolMap, AttrSetT>) {
        super(sheetModel.docModel, attrSet, { ...config, attrPool: sheetModel.docModel.sheetAttrPool, families });
        this.sheetModel = sheetModel;
    }
}
