/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, str, ary, dict } from "@/io.ox/office/tk/algorithms";
import type { AttrValueDict } from "@/io.ox/office/tk/dom";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { CellAnchor } from "@/io.ox/office/spreadsheet/utils/cellanchor";

// types ======================================================================

/**
 * Cell range addresses grouped by their type.
 */
export interface RangeGroups {

    /**
     * The full column ranges.
     */
    colRanges?: RangeArray;

    /**
     * The full row ranges.
     */
    rowRanges?: RangeArray;

    /**
     * Remaining cell ranges that are neither column ranges nor row ranges.
     */
    cellRanges?: RangeArray;
}

// class AddressFactory =======================================================

/**
 * An instance of this class contains the maximum possible column index and row
 * index of cell addresses in a spreadsheet document, and provides methods to
 * deal with index intervals (columns and rows), cell addresses, and cell range
 * addresses.
 *
 * @param colCount
 *  The total number of columns contained in a sheet.
 *
 * @param rowCount
 *  The total number of rows contained in a sheet.
 */
export class AddressFactory {

    #maxCol: number;
    #maxRow: number;

    // constructor ------------------------------------------------------------

    constructor(colCount: number, rowCount: number) {
        this.#maxCol = colCount - 1;
        this.#maxRow = rowCount - 1;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns the total number of columns contained in a sheet.
     *
     * @returns
     *  The total number of columns contained in a sheet.
     */
    get colCount(): number {
        return this.#maxCol + 1;
    }

    /**
     * Returns the total number of rows contained in a sheet.
     *
     * @returns
     *  The total number of rows contained in a sheet.
     */
    get rowCount(): number {
        return this.#maxRow + 1;
    }

    /**
     * Returns the maximum possible column index in a sheet (the column index
     * of the cells in the rightmost column).
     *
     * @returns
     *  The maximum possible column index in a sheet.
     */
    get maxCol(): number {
        return this.#maxCol;
    }

    /**
     * Returns the maximum possible row index in a sheet (the row index of the
     * cells in the lowermost row).
     *
     * @returns
     *  The maximum possible row index in a sheet.
     */
    get maxRow(): number {
        return this.#maxRow;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the maximum column or row index in a sheet.
     *
     * @param columns
     *  Whether to return the maximum column index (`true`), or the maximum row
     *  index (`false`).
     *
     * @returns
     *  The maximum column or row index in a sheet.
     */
    getMaxIndex(columns: boolean): number {
        return columns ? this.#maxCol : this.#maxRow;
    }

    /**
     * Returns the number of columns or rows in a sheet.
     *
     * @param columns
     *  Whether to return the number of columns (`true`), or the number of rows
     *  (`false`).
     *
     * @returns
     *  The number of columns or rows in a sheet.
     */
    getSize(columns: boolean): number {
        return (columns ? this.#maxCol : this.#maxRow) + 1;
    }

    /**
     * Updates the maximum available column and row index used by this factory.
     *
     * @param colCount
     *  The total number of columns contained in a sheet.
     *
     * @param rowCount
     *  The total number of rows contained in a sheet.
     */
    updateSheetSize(colCount: number, rowCount: number): void {
        this.#maxCol = colCount - 1;
        this.#maxRow = rowCount - 1;
    }

    // index intervals --------------------------------------------------------

    /**
     * Parses a column/row interval from a string in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @param columns
     *  Set to `true` to parse a column interval (e.g. "B:C"); set to `false`
     *  to parse a row interval (e.g. "2:3").
     *
     * @returns
     *  The parsed column/row interval; or `undefined`, if the passed string
     *  cannot be parsed to a valid column/row interval.
     */
    parseInterval(parseStr: string, columns: boolean): Opt<Interval> {
        const interval = Interval.parseAs(parseStr, columns);
        return (interval && (interval.last <= this.getMaxIndex(columns))) ? interval : undefined;
    }

    /**
     * Parses a column/row interval list from a string of space-separated
     * column/row intervals in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @param columns
     *  Set to `true` to parse a list of column intervals (e.g. "B:C E"); set
     *  to `false` to parse a list of row intervals (e.g. "2:3 5").
     *
     * @returns
     *  The (non-empty) list of column/row intervals; or `undefined`, if the
     *  passed string cannot be parsed to a valid column/row interval list.
     */
    parseIntervalList(parseStr: string, columns: boolean): Opt<IntervalArray> {
        const intervals = IntervalArray.map(str.splitTokens(parseStr), token => this.parseInterval(token, columns));
        return intervals.empty() ? undefined : intervals;
    }

    /**
     * Returns whether the passed index interval contains valid indexes (inside
     * the sheet limits of this factory).
     *
     * @param interval
     *  The index interval to be checked.
     *
     * @param columns
     *  Set to `true` to check a column interval; set to `false` to check a row
     *  interval.
     *
     * @returns
     *  Whether the passed index interval contains valid indexes.
     */
    isValidInterval(interval: Interval, columns: boolean): boolean {
        return (interval.first >= 0) && (interval.first <= interval.last) && (interval.last <= this.getMaxIndex(columns));
    }

    /**
     * Returns the index interval covering all columns or rows starting at the
     * specified index up to the maximum index in a sheet.
     *
     * @param first
     *  The first index to be inserted into the index interval.
     *
     * @param columns
     *  Whether to return the column interval (`true`), or the row interval
     *  (`false`).
     *
     * @returns
     *  The column or row interval in a sheet, starting at the specified index,
     *  covering all columns/row up to the end of the sheet.
     */
    getEndInterval(first: number, columns: boolean): Interval {
        return new Interval(first, this.getMaxIndex(columns));
    }

    /**
     * Returns the index interval covering all columns or rows in a sheet.
     *
     * @param columns
     *  Whether to return the column interval (`true`), or the row interval
     *  (`false`).
     *
     * @returns
     *  The full column or row interval in a sheet. Each invocation of this
     *  method creates a new interval object that can be modified further.
     */
    getFullInterval(columns: boolean): Interval {
        return new Interval(0, this.getMaxIndex(columns));
    }

    // cell addresses ---------------------------------------------------------

    /**
     * Parses a cell address from a string in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The parsed cell address; or `undefined`, if the passed string cannot be
     *  parsed to a valid cell address.
     */
    parseAddress(parseStr: string): Opt<Address> {
        const address = Address.parse(parseStr);
        return (address && (address.c <= this.#maxCol) && (address.r <= this.#maxRow)) ? address : undefined;
    }

    /**
     * Parses a cell address list from a string of space-separated cell
     * addresses in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The (non-empty) list of cell addresses; or `undefined`, if the passed
     *  string cannot be parsed to a valid cell address list.
     */
    parseAddressList(parseStr: string): Opt<AddressArray> {
        const addresses = AddressArray.map(str.splitTokens(parseStr), token => this.parseAddress(token));
        return addresses.empty() ? undefined : addresses;
    }

    /**
     * Returns whether the passed cell address contains valid column and row
     * indexes (inside the sheet limits of this factory).
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether the passed cell address contains valid column and row indexes.
     */
    isValidAddress(address: Address): boolean {
        return (address.c >= 0) && (address.c <= this.#maxCol) && (address.r >= 0) && (address.r <= this.#maxRow);
    }

    /**
     * Returns a new cell address clamped into the sheet limits.
     *
     * @param address
     *  The cell address to be clamped.
     *
     * @returns
     *  The clamped cell address. Will always be a new address instance, never
     *  the address passed to this method.
     */
    getClampedAddress(address: Address): Address {
        return new Address(math.clamp(address.c, 0, this.#maxCol), math.clamp(address.r, 0, this.#maxRow));
    }

    // cell range addresses ---------------------------------------------------

    /**
     * Parses a cell range address from a string in A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The parsed cell range address; or `undefined`, if the passed string
     *  cannot be parsed to a valid cell range address.
     */
    parseRange(parseStr: string): Opt<Range> {
        const range = Range.parse(parseStr);
        return (range && (range.a2.c <= this.#maxCol) && (range.a2.r <= this.#maxRow)) ? range : undefined;
    }

    /**
     * Parses a cell range list from a string of space-separated addresses in
     * A1 notation.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The (non-empty) list of cell range addresses; or `undefined`, if the
     *  passed string cannot be parsed to a valid cell range list.
     */
    parseRangeList(parseStr: string): Opt<RangeArray> {
        const ranges = RangeArray.map(str.splitTokens(parseStr), token => this.parseRange(token));
        return ranges.empty() ? undefined : ranges;
    }

    /**
     * Returns whether the passed cell range address contains valid start and
     * end addresses (inside the sheet limits of this factory) that are in
     * correct order (start before end).
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  Whether the passed cell range address contains valid start and end
     *  addresses in correct order.
     */
    isValidRange(range: Range): boolean {
        return (range.a1.c >= 0) && (range.a1.c <= range.a2.c) && (range.a2.c <= this.#maxCol) &&
            (range.a1.r >= 0) && (range.a1.r <= range.a2.r) && (range.a2.r <= this.#maxRow);
    }

    /**
     * Returns whether the passed range address covers the entire sheet.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  Whether the passed range address covers the entire sheet.
     */
    isSheetRange(range: Range): boolean {
        return this.isColRange(range) && this.isRowRange(range);
    }

    /**
     * Returns the cell range address of the entire area in a sheet, from the
     * top-left cell to the bottom-right cell.
     *
     * @returns
     *  The cell range address of an entire sheet. Each invocation of this
     *  method creates a new cell range object that can be modified further.
     */
    getSheetRange(): Range {
        return Range.fromIndexes(0, 0, this.#maxCol, this.#maxRow);
    }

    /**
     * Returns whether the passed range address covers one or more entire
     * columns in the sheet.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  Whether the passed range address covers one or more entire columns.
     */
    isColRange(range: Range): boolean {
        return (range.a1.r === 0) && (range.a2.r === this.#maxRow);
    }

    /**
     * Returns the address of the cell range covering the specified column
     * interval.
     *
     * @param interval
     *  The column interval, or a single zero-based column index.
     *
     * @returns
     *  The address of the column range.
     */
    getColRange(interval: Interval | number): Range {
        return is.number(interval) ?
            Range.fromIndexes(interval, 0, interval, this.#maxRow) :
            Range.fromIndexes(interval.first, 0, interval.last, this.#maxRow);
    }

    /**
     * Returns the addresses of the cell ranges covering the specified column
     * intervals.
     *
     * @param intervals
     *  An array of column intervals, or a single column interval.
     *
     * @returns
     *  The addresses of the cell ranges specified by the column intervals.
     */
    getColRangeList(intervals: IntervalSource): RangeArray {
        return RangeArray.map(intervals, interval => this.getColRange(interval));
    }

    /**
     * Returns whether the passed range address covers one or more entire rows
     * in the sheet.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  Whether the passed range address covers one or more entire rows.
     */
    isRowRange(range: Range): boolean {
        return (range.a1.c === 0) && (range.a2.c === this.#maxCol);
    }

    /**
     * Returns the address of the cell range covering the specified row
     * interval.
     *
     * @param interval
     *  The row interval, or a single zero-based row index.
     *
     * @returns
     *  The address of the row range.
     */
    getRowRange(interval: Interval | number): Range {
        return is.number(interval) ?
            Range.fromIndexes(0, interval, this.#maxCol, interval) :
            Range.fromIndexes(0, interval.first, this.#maxCol, interval.last);
    }

    /**
     * Returns the addresses of the cell ranges covering the specified row
     * intervals.
     *
     * @param intervals
     *  An array of row intervals, or a single row interval.
     *
     * @returns
     *  The addresses of the cell ranges specified by the row intervals.
     */
    getRowRangeList(intervals: IntervalSource): RangeArray {
        return RangeArray.map(intervals, interval => this.getRowRange(interval));
    }

    /**
     * Returns whether the passed range address covers one or more entire
     * columns or rows in the sheet.
     *
     * @param range
     *  A cell range address.
     *
     * @param columns
     *  Whether to check for a full column range (`true`), or for a full row
     *  range (`false`).
     *
     * @returns
     *  Whether the passed range address covers one or more entire columns or
     *  rows.
     */
    isFullRange(range: Range, columns: boolean): boolean {
        return columns ? this.isColRange(range) : this.isRowRange(range);
    }

    /**
     * Returns the address of the cell range covering the specified column or
     * row interval.
     *
     * @param interval
     *  The column or row interval, or a single column or row index.
     *
     * @param columns
     *  Whether to create a full column range (`true`), or a full row range
     *  (`false`).
     *
     * @returns
     *  The address of the full column or row range.
     */
    getFullRange(interval: Interval | number, columns: boolean): Range {
        return columns ? this.getColRange(interval) : this.getRowRange(interval);
    }

    /**
     * Returns the addresses of the cell ranges covering the specified column
     * or row intervals.
     *
     * @param intervals
     *  An array of index intervals, or a single index interval.
     *
     * @param columns
     *  Whether to create column ranges (`true`), or row ranges (`false`).
     *
     * @returns
     *  The addresses of the cell ranges specified by the row intervals.
     */
    getFullRangeList(intervals: IntervalSource, columns: boolean): RangeArray {
        return columns ? this.getColRangeList(intervals) : this.getRowRangeList(intervals);
    }

    /**
     * Distributes the passed cell range addresses into full column ranges,
     * full row ranges, and all other cell ranges.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @returns
     *  The passed cell ranges, grouped by their type.
     */
    getRangeGroups(ranges: RangeSource): RangeGroups {
        const groups = ary.group(RangeArray.cast(ranges), range => this.isColRange(range) ? "colRanges" : this.isRowRange(range) ? "rowRanges" : "cellRanges");
        return dict.from(groups, ([key, array]) => [key, new RangeArray(array)]);
    }

    /**
     * Returns a new cell range address that has been moved by the specified
     * distance, but always keeping the range address inside the sheet limits.
     *
     * See method `getCroppedMovedRange()` for an alternative keeping the
     * specified move distance intact.
     *
     * @param range
     *  The address of the cell range to be moved.
     *
     * @param cols
     *  The number of columns the cell range will be moved. Negative values
     *  will move the cell range towards the leading border of the sheet,
     *  positive values will move the cell range towards the trailing border.
     *
     * @param rows
     *  The number of rows the cell range will be moved. Negative values will
     *  move the cell range towards the top border of the sheet, positive
     *  values will move the cell range towards the bottom border.
     *
     * @returns
     *  The address of the moved cell range. If the move distances passed to
     *  this method are too large, the range will only be moved to the sheet
     *  borders.
     */
    getBoundedMovedRange(range: Range, cols: number, rows: number): Range {

        // reduce move distance to keep the range address valid
        cols = math.clamp(cols, -range.a1.c, this.#maxCol - range.a2.c);
        rows = math.clamp(rows, -range.a1.r, this.#maxRow - range.a2.r);

        // return the new range
        return Range.fromIndexes(range.a1.c + cols, range.a1.r + rows, range.a2.c + cols, range.a2.r + rows);
    }

    /**
     * Returns a new cell range address that has been moved by the specified
     * distance. If the moved cell range leaves the sheet area, it will be
     * cropped at the sheet borders.
     *
     * See method `getBoundedMovedRange()` for an alternative keeping the
     * original size of the range intact.
     *
     * @param range
     *  The address of the cell range to be moved.
     *
     * @param cols
     *  The number of columns the cell range will be moved. Negative values
     *  will move the cell range towards the leading border of the sheet,
     *  positive values will move the cell range towards the trailing border.
     *
     * @param rows
     *  The number of rows the cell range will be moved. Negative values will
     *  move the cell range towards the top border of the sheet, positive
     *  values will move the cell range towards the bottom border.
     *
     * @returns
     *  The address of the moved cell range. If the specified move distances
     *  are too large, the cell range will be cropped to the sheet area. If the
     *  cell range has been moved completely outside the sheet, this method
     *  will return `undefined`.
     */
    getCroppedMovedRange(range: Range, cols: number, rows: number): Opt<Range> {

        // the address components of the moved range
        const col1 = Math.max(0, range.a1.c + cols);
        const row1 = Math.max(0, range.a1.r + rows);
        const col2 = Math.min(this.#maxCol, range.a2.c + cols);
        const row2 = Math.min(this.#maxRow, range.a2.r + rows);

        // return undefined, if the resulting range becomes invalid
        return ((col1 <= col2) && (row1 <= row2)) ? Range.fromIndexes(col1, row1, col2, row2) : undefined;
    }

    /**
     * Returns the values of all data attributes needed to annotate a DOM
     * element representing a cell range.
     *
     * @param range
     *  The cell range address.
     *
     * @returns
     *  The DOM data attributes for the cell range.
     */
    getRangeDataAttrs(range: Range): AttrValueDict {
        const opStr = range.toOpStr();
        const dataset: AttrValueDict = { range: opStr };
        if (this.isColRange(range)) { dataset.cols = opStr.replace(/\d/g, ""); }
        if (this.isRowRange(range)) { dataset.rows = opStr.replace(/[A-Z]/g, ""); }
        return dataset;
    }

    // drawing cell anchor ----------------------------------------------------

    /**
     * Parses a cell anchor string for a drawing object.
     *
     * @param parseStr
     *  The string to be parsed.
     *
     * @returns
     *  The parsed and validated cell anchor.
     */
    parseCellAnchor(parseStr: string): CellAnchor {
        const cellAnchor = CellAnchor.parseOpStr(parseStr);
        const { point1, point2 } = cellAnchor;
        const addr1 = point1.a = this.getClampedAddress(point1.a);
        const addr2 = point2.a = this.getClampedAddress(point2.a);
        addr2.c = Math.max(addr1.c, addr2.c);
        addr2.r = Math.max(addr1.r, addr2.r);
        if (addr1.c === addr2.c) { point2.x = Math.max(point1.x, point2.x); }
        if (addr1.r === addr2.r) { point2.y = Math.max(point1.y, point2.y); }
        return cellAnchor;
    }
}
