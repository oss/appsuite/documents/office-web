/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict } from "@/io.ox/office/tk/algorithms";

import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import type { DisplayText, TransformSheetVector } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { type SheetModel, SheetAttrsModel } from "@/io.ox/office/spreadsheet/model/sheetattrsmodel";
import { TransformMode, type AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import { type FormulaUpdateTask, type RenameSheetUpdateTask, MoveCellsUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

// types ======================================================================

export enum DVRuleErrorType {
    INFO = "info",
    WARNING = "warning",
    ERROR = "error"
}

export interface DVRuleAttributes {

    /**
     * The type of the data validation rule. Supports all types used in the
     * document operation "insertDVRule".
     */
    type: string;

    /**
     * The comparison operator for most validation types, used together with
     * the attributes "value1" and "value2".
     */
    compare: string;

    /**
     * The first value used together with the comparison operator. Must be a
     * formula expression as string, without the leading equality sign.
     */
    value1: string;

    /**
     * The second value used together with the comparison operators "between"
     * and "notBetween". Must be a formula expression as string, without the
     * leading equality sign.
     */
    value2: string;

    /**
     * Specifies whether to show an informational tooltip when a cell with this
     * data validation is selected.
     */
    showInfo: boolean;

    /**
     * The headline for the informational tooltip.
     */
    infoTitle: string;

    /**
     * The message text for the informational tooltip.
     */
    infoText: string;

    /**
     * Specifies whether to show an alert box when an invalid value has been
     * entered into a cell with this data validation rule.
     */
    showError: boolean;

    /**
     * The headline for the alert box.
     */
    errorTitle: string;

    /**
     * The message text for the alert box.
     */
    errorText: string;

    /**
     * The type of the alert box.
     */
    errorType: DVRuleErrorType;

    /**
     * Specifies whether to show a drop-down button at the selected cell, if
     * the data validation type is "list" or "source".
     */
    showDropDown: boolean;

    /**
     * Specifies whether to allow blank cells regardless of the actual
     * validation type and conditions.
     */
    ignoreEmpty: boolean;
}

export interface DVRuleAttributeSet {
    dvrule: DVRuleAttributes;
}

/**
 * Shared configuration for data validation rules in a collection.
 */
export interface DVRuleSharedConfig {

    /**
     * If `true`, integer indexes will be used to address validation rules in
     * operations (operation property `index`). Otherwise, the target range
     * addresses will be used to identify operations (allows partial deletion
     * and partial replacement on insertion).
     */
    indexedOps: boolean;

    /**
     * If `true`, the operations will contain a custom reference address
     * (operation property `ref`). Otherwise, the reference address will be
     * inferred automatically from the top-left cell of the bounding range of
     * the target ranges.
     */
    customRef: boolean;
}

/**
 * Result element for a list validation type (rule types "list" or "source").
 */
export interface DVListResult {

    /**
     * For rule type "source", the cell value, or formula result (`null` for
     * blank cells). Will be omitted for rule type "list" to indicate that the
     * "display" property needs to be parsed.
     */
    value?: ScalarType;

    /**
     * For rule type "source", the display string for the list entry (will be
     * the empty string for blank cells). The value `null` represents a cell
     * result that cannot be formatted to a valid display string with the
     * current number format of the cell. For rule type "list", the literal
     * list entry (always a string) that needs to be parsed dynamically (may be
     * a formatted number, a boolean, or even a formula expression that will
     * result in a formula cell).
     */
    display: DisplayText;
}

// constants ==================================================================

export const DVRULE_ATTRIBUTES: AttributeConfigBag<DVRuleAttributes> = {
    type:           { def: "all" },
    compare:        { def: "" },
    value1:         { def: "" },
    value2:         { def: "" },
    showInfo:       { def: true },
    infoTitle:      { def: "" },
    infoText:       { def: "" },
    showError:      { def: true },
    errorTitle:     { def: "" },
    errorText:      { def: "" },
    errorType:      { def: DVRuleErrorType.ERROR },
    showDropDown:   { def: true },
    ignoreEmpty:    { def: true }
};

// private constants ----------------------------------------------------------

// validation types visualized with a cell drop-down button
const DROPDOWN_TYPES = new Set(["source", "list"]);

// all properties that need to be written together in a change operation
const RULE_TYPE_PROPERTIES = new Map<string, Array<keyof DVRuleAttributes>>();
for (const type of ["integer", "number", "date", "time", "length"]) {
    RULE_TYPE_PROPERTIES.set(type, ["type", "compare", "value1", "value2"]);
}
for (const type of ["list", "source", "custom"]) {
    RULE_TYPE_PROPERTIES.set(type, ["type", "value1"]);
}

// private functions ==========================================================

/**
 * Transforms the specified target ranges.
 *
 * @param transformer
 *  An address transformer specifying how to transform the passed target
 *  ranges.
 *
 * @param targetRanges
 *  The addresses of the target ranges to be transformed.
 *
 * @param [reverse]
 *  If set to `true`, the opposite move operation will be used to transform the
 *  target ranges.
 *
 * @returns
 *  The transformed target ranges.
 */
function transformTargetRanges(transformer: AddressTransformer, targetRanges: RangeArray, reverse = false): RangeArray {
    // transform the passed ranges, expand the end of the range when inserting cells
    return transformer.transformRanges(targetRanges, { transformMode: TransformMode.EXPAND, reverse });
}

// class DVRuleModel ==========================================================

/**
 * Stores data validation settings for one or more cell ranges in a sheet.
 */
export class DVRuleModel extends SheetAttrsModel<DVRuleAttributeSet> {

    // shared collection/model configuration
    readonly #config: Readonly<DVRuleSharedConfig>;
    // the token arrays representing the validation attributes "value1" and "value2"
    readonly #tokenArrayMap = this.member(new Map<"value1" | "value2", TokenArray>());

    // the target cell ranges covered by the data validation
    #targetRanges!: RangeArray;
    // the reference address for formulas with relative references
    #refAddress!: Address;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this instance.
     *
     * @param targetRanges
     *  The addresses of all cell ranges covered by this validation rule.
     *
     * @param ruleAttrs
     *  The data validation attributes.
     *
     * @param refFormula
     *  The custom reference address, as formula expression.
     *
     * @param config
     *  The shared configuration of the parent collection.
     *
     * @param cloneModel
     *  An existing validation rule model model to be cloned.
     */
    constructor(sheetModel: SheetModel, targetRanges: RangeArray, ruleAttrs: Partial<DVRuleAttributes>, refFormula: string | null, config: Readonly<DVRuleSharedConfig>);
    constructor(sheetModel: SheetModel, cloneModel: DVRuleModel);
    // implementation
    constructor(sheetModel: SheetModel, cloneModelOrRanges: RangeArray | DVRuleModel, ruleAttrs?: Partial<DVRuleAttributes>, refFormula?: string | null, config?: Readonly<DVRuleSharedConfig>) {

        // whether this constructor needs to clone an existing rule model
        const clone = cloneModelOrRanges instanceof DVRuleModel;
        const cloneModel = clone ? cloneModelOrRanges : undefined;
        const targetRanges = clone ? undefined : cloneModelOrRanges;

        // base constructor
        if (cloneModel) { ruleAttrs = cloneModel.#getRuleAttrs(true); }
        super(sheetModel, ["dvrule"], { dvrule: ruleAttrs });

        // shared configuration of the parent collection
        this.#config = cloneModel ? cloneModel.#config : config!;

        // initialize the target ranges
        this.setTargetRanges(cloneModel ? cloneModel.#targetRanges : targetRanges!);

        // parse a custom reference address
        if (this.#config.customRef) {
            refFormula = clone ? cloneModelOrRanges.#generateRefFormula(cloneModelOrRanges.#refAddress, sheetModel.getName()) : refFormula;
            this.setRefFormula(refFormula || "");
        }

        // create the token arrays for the formula expressions in "value1" and "value2"
        this.#tokenArrayMap.set("value1", new TokenArray(sheetModel, "rule"));
        this.#tokenArrayMap.set("value2", new TokenArray(sheetModel, "rule"));

        // parse the source formulas after changing value attributes
        this.on("change:attributes", this.#changeAttributesHandler);
        this.#changeAttributesHandler();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the addresses of all target cell ranges of this validation rule.
     *
     * @param [transformer]
     *  An address transformer specifying how to transform the passed target
     *  ranges. If omitted, the original target ranges will be returned.
     *
     * @returns
     *  The addresses of all cell ranges.
     */
    getTargetRanges(transformer?: AddressTransformer): RangeArray {
        return transformer ? transformTargetRanges(transformer, this.#targetRanges) : this.#targetRanges.deepClone();
    }

    /**
     * Returns whether the passed cell address is contained by the target cell
     * ranges of this validation rule.
     *
     * @param address
     *  The address of the cell to be checked.
     *
     * @returns
     *  Whether the specified cell is contained in this validation rule.
     */
    containsCell(address: Address): boolean {
        return this.#targetRanges.containsAddress(address);
    }

    /**
     * Returns the reference addresses of this validation rule (the top-left
     * cell of the bounding range of all target cell ranges of this validation
     * rule).
     *
     * @returns
     *  The address of the reference cell of this validation rule.
     */
    getRefAddress(): Address {
        return this.#refAddress.clone();
    }

    /**
     * Returns whether to show a drop-down button next to a cell, according to
     * the validation type (`source` and `list` only).
     *
     * @returns
     *  Whether to show a drop-down button next to a cell.
     */
    showDropDownButton(): boolean {
        const ruleAttrs = this.#getRuleAttrs();
        return ruleAttrs.showDropDown && DROPDOWN_TYPES.has(ruleAttrs.type);
    }

    /**
     * Returns the list contents for validations of type "list" or "source"
     * according to the formula expression contained in the attribute "value1".
     *
     * @param targetAddress
     *  The target address used to resolve relative references in the formula
     *  expression.
     *
     * @returns
     *  The contents of the cells, if this rule is a data validation of type
     *  "list" or "source", and the formula expression contained in the
     *  attribute "value1" could be resolved successfully to a list; otherwise
     *  an empty array.
     */
    queryListContents(targetAddress: Address): DVListResult[] {

        // resolve source data by validation type
        switch (this.#getRuleAttrs().type) {

            case "list": {
                const stringList = this.#tokenArrayMap.get("value1")?.resolveStringList();
                // convert plain string array to objects with "display" property (no "value" to force parsing the text later)
                // DOCS-3080: strings need to be trimmed (for example, list may be given as "a; b; c; d")
                return stringList?.map(text => ({ display: text.trim() })) ?? [];
            }

            case "source": {
                // get the cell range addresses contained in the first token array
                const value = this.#tokenArrayMap.get("value1")?.interpretFormula("ref", this.#refAddress, targetAddress)?.value;
                // collect all cell values and display strings, restrict to 1000 entries
                return (value instanceof Range3DArray) ? this.docModel.getRangeContents(value, { compressed: true, blanks: true, display: true, maxCount: 1000 }) as DVListResult[] : [];
            }
        }

        // default (wrong validation type)
        return [];
    }

    /**
     * Changes the target cell ranges of this validation rule.
     *
     * @param newRanges
     *  The addresses of the new target cell ranges. The old target ranges of
     *  this validation rule will be replaced completely.
     *
     * @returns
     *  Whether the current target ranges have changed.
     */
    setTargetRanges(newRanges: RangeArray): boolean {

        // clone and merge the target ranges to prevent changes form external code
        newRanges = newRanges.merge();

        // nothing to do if the ranges will not change
        if (newRanges.empty() || this.#targetRanges?.deepEquals(newRanges)) {
            return false;
        }

        // set the new target ranges
        this.#targetRanges = newRanges;

        // update implicit reference address
        if (!this.#config.customRef && this.#targetRanges.length) {
            this.#refAddress = this.#targetRanges.refAddress()!;
        }

        return true;
    }

    /**
     * Changes the reference address of this validation rule.
     *
     * @param refFormula
     *  A formula expression that contains a single reference with a single
     *  cell address.
     */
    setRefFormula(refFormula: string): void {

        // parse the passed reference address expression
        const tokenArray = new TokenArray(this.sheetModel, "rule");
        tokenArray.parseFormula("op", refFormula, Address.A1);

        // try to convert to a single reference address (do not pass a reference
        // sheet index, sheet name is expected to exist in the formula expression)
        const ranges = tokenArray.resolveRangeList(Address.A1, Address.A1);

        // if the expression resolves to a single cell address, use it as reference address
        const isSingle = (ranges.length === 1) && ranges[0].single();
        this.#refAddress = isSingle ? ranges[0].a1 : Address.A1.clone();
    }

    /**
     * Changes all formula expressions stored in the validation attributes
     * "value1" and "value2" with sheet references, after the sheet collection
     * in the document has been changed.
     */
    transformSheets(xfVector: TransformSheetVector): void {
        for (const tokenArray of this.#tokenArrayMap.values()) {
            tokenArray.transformSheets(xfVector);
        }
    }

    /**
     * Tries to replace unresolved sheet names in the token arrays with
     * existing sheet indexes. Intended to be used after document import to
     * refresh all token arrays that refer to sheets that did not exist during
     * their creation.
     */
    refreshAfterImport(): void {
        for (const tokenArray of this.#tokenArrayMap.values()) {
            tokenArray.refreshAfterImport(this.#refAddress);
        }
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates the operations and undo operations to update and restore the
     * formula expressions of this data validation rule.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param index
     *  The original index of this validation rule to be inserted into the
     *  document operations.
     *
     * @param updateTask
     *  The update task to be executed.
     */
    generateUpdateTaskOperations(sheetCache: SheetCache, index: number, updateTask: FormulaUpdateTask): void {

        // DOCS-1941: In order to retain array indexes used as rule identifiers (OOXML), the operations to modify
        // or delete this rule will be prepended to the sheet cache so that they will be processed in reversed
        // order effectively. The undo operations will be processed in regular (ascending) order, so that preceding
        // rules will be restored before following rules will be modified.

        // the formula grammar used for formula expression transformations
        const grammar = this.docModel.formulaGrammarOP;

        // the properties for the document and undo operation
        const operProps = dict.create();
        let undoProps: Opt<Dict> = dict.create();

        // the new transformed target ranges for this rule
        let newTargetRanges = this.#targetRanges;
        // whether the target ranges need to be updated
        let updateRanges = false;
        // restored target ranges after undoing the transformation
        let undoTargetRanges: Opt<RangeArray>;
        // the new reference address for the formula expressions
        let newRefAddress: Opt<Address> = this.#refAddress;

        // update the target ranges, if the moved cells are located in the own sheet
        if (this.sheetModel.isOwnMoveTask(updateTask)) {

            // transform the current target ranges
            const transformer = updateTask.transformer;
            newTargetRanges = this.getTargetRanges(transformer);
            if (newTargetRanges.empty()) {
                const props = this.#setOperationTarget(dict.create(), index);
                sheetCache.generateSheetOperation(Op.DELETE_DVRULE, props, { prepend: true });
                this.#generateRestoreOperations(sheetCache, index);
                return;
            }

            // nothing more to do, if the target ranges do not change at all
            updateRanges = !this.#targetRanges.deepEquals(newTargetRanges);
            if (updateRanges) {

                // simulate what happens when undoing the change (restored ranges may differ from original)
                undoTargetRanges = transformTargetRanges(transformer, newTargetRanges, true);

                // if the reference address will be deleted, relocate the relative references in the formulas to the
                // reference address that remains valid during range transformation (top-left cell of restored ranges)
                newRefAddress = transformer.transformAddress(this.#refAddress);
                if (!newRefAddress) {
                    const undoRefAddress = undoTargetRanges.refAddress()!;
                    updateTask = new MoveCellsUpdateTask(updateTask.sheet, transformer, this.#refAddress, undoRefAddress);
                    newRefAddress = transformer.transformAddress(undoRefAddress);
                }
            }
        }

        // calculate the new formula expressions for the validation rule
        for (const [propName, tokenArray] of this.#tokenArrayMap) {
            updateTask.transformOpProperty(tokenArray, grammar, propName, operProps, undoProps);
        }

        // DOCS-2754: add all dependent properties that describe the rule settings
        if (!is.empty(operProps)) {
            const ruleAttrs = this.#getRuleAttrs();
            const propNames = RULE_TYPE_PROPERTIES.get(ruleAttrs.type);
            if (propNames) {
                for (const propName of propNames) {
                    if (!(propName in operProps)) {
                        operProps[propName] = undoProps[propName] = ruleAttrs[propName];
                    }
                }
            }
        }

        // ODF: put changed reference address into the operations
        const renameRefSheet = this.#config.customRef && this.sheetModel.isOwnRenameTask(updateTask);
        if (this.#config.customRef && newRefAddress && (renameRefSheet || this.#refAddress.differs(newRefAddress) || !is.empty(operProps))) {
            const newSheetName = renameRefSheet ? (updateTask as RenameSheetUpdateTask).sheetName : undefined;
            operProps.ref = this.#generateRefFormula(newRefAddress, newSheetName);
            undoProps.ref = this.#generateRefFormula(this.#refAddress);
        }

        // DOCS-1825: OOXML: always put target ranges (also unchanged) into the operation (needed for OT)
        if (this.#config.indexedOps && newTargetRanges && (updateRanges || !is.empty(operProps))) {
            operProps.ranges = newTargetRanges.toOpStr();
            undoProps.ranges = this.#targetRanges.toOpStr();
        }

        // ODF: generate operations to restore deleted ranges, or to delete auto-expanded ranges
        if (!this.#config.indexedOps && undoTargetRanges && !this.#targetRanges.deepEquals(undoTargetRanges)) {

            // delete ranges that have been added while re-inserting deleted cells (auto-expansion)
            const surplusRanges = undoTargetRanges.difference(this.#targetRanges);
            if (!surplusRanges.empty()) {
                const surplusProps = { ranges: surplusRanges.toOpStr() };
                sheetCache.generateSheetOperation(Op.DELETE_DVRULE, surplusProps, { undo: true });
            }

            // delete and recreate the rule, if the target ranges need to be expanded after undo
            if (!this.#targetRanges.difference(undoTargetRanges).empty()) {
                sheetCache.generateSheetOperation(Op.DELETE_DVRULE, { ranges: undoTargetRanges.toOpStr() }, { undo: true });
                this.#generateRestoreOperations(sheetCache, index);
                undoProps = undefined;
            }
        }

        // generate the operation (must be prepended, see comment above)
        if (!is.empty(operProps)) {
            this.#setOperationTarget(operProps, index);
            sheetCache.generateSheetOperation(Op.CHANGE_DVRULE, operProps, { prepend: true });
        }

        // generate the undo operation
        if (!is.empty(undoProps)) {
            this.#setOperationTarget(undoProps!, index);
            sheetCache.generateSheetOperation(Op.CHANGE_DVRULE, undoProps, { undo: true });
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the merged or explicit rule attributes.
     */
    #getRuleAttrs(explicit: true): Opt<Partial<DVRuleAttributes>>;
    #getRuleAttrs(): DVRuleAttributes;
    // implementation
    #getRuleAttrs(explicit?: boolean): Opt<Partial<DVRuleAttributes>> {
        return (explicit ? this.getExplicitAttributeSet(true) : this.getMergedAttributeSet(true)).dvrule;
    }

    /**
     * Updates the token arrays after the validation attributes of this
     * validation rule have been changed.
     */
    #changeAttributesHandler(newAttrSet?: DVRuleAttributeSet, oldAttrSet?: DVRuleAttributeSet): void {

        // extract the validation attributes from the passed attribute sets
        const newAttrs = newAttrSet?.dvrule ?? this.#getRuleAttrs();
        const oldAttrs = oldAttrSet?.dvrule ?? dict.create();

        // parse a changed formula expression in attribute "value1" or "value2"
        for (const [propName, tokenArray] of this.#tokenArrayMap) {
            if (newAttrs[propName] !== oldAttrs[propName]) {
                tokenArray.parseFormula("op", newAttrs[propName], this.#refAddress);
            }
        }
    }

    /**
     * Returns a formula expression with sheet name representing the passed
     * reference address.
     *
     * @param refAddress
     *  The reference address of a validation rule.
     *
     * @param [sheetName]
     *  A custom sheet name to be inserted into the reference. If omitted, the
     *  name of the own parent sheet will be used.
     *
     * @returns
     *  The passed reference cell, as formula expression with sheet name.
     */
    #generateRefFormula(refAddress: Address, sheetName?: string): string {
        const tokenArray = new TokenArray(this.sheetModel, "rule");
        tokenArray.appendAddress(refAddress, { sheet: sheetName || this.sheetModel.getIndex(), relSheet: true });
        return tokenArray.getFormulaOp();
    }

    /**
     * Generates the undo operation to restore this validation rule after it
     * has been deleted.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param index
     *  The index of this validation rule to be inserted into the document
     *  operations.
     */
    #generateRestoreOperations(generator: SheetOperationGenerator, index: number): void {

        // build the undo operation properties (always put the index into theoperation, used by the method
        // `applyInsertOperation()` to restore the original array order which is needed in generic unit tests)
        const undoProps: Dict = {
            index,
            ranges: this.#targetRanges.toOpStr(),
            // add explicit "dvrule" attributes as top-level properties
            ...this.#getRuleAttrs(true)
        };

        // create the formula expression for the reference address
        if (this.#config.customRef) {
            undoProps.ref = this.#generateRefFormula(this.#refAddress);
        }

        // generate the undo operation
        generator.generateSheetOperation(Op.INSERT_DVRULE, undoProps, { undo: true });
    }

    // add required properties to identify the rule
    // ODF: target ranges are used for identification (no "index" property)
    #setOperationTarget(operProps: Dict, index: number): Dict {
        if (this.#config.indexedOps) {
            operProps.index = index;
        } else {
            operProps.ranges = this.#targetRanges.toOpStr();
        }
        return operProps;
    }
}
