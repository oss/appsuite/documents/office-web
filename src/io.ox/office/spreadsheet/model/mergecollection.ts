/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ReverseOptions, ary, jpromise } from "@/io.ox/office/tk/algorithms";

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";

import type { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { MAX_MERGED_RANGES_COUNT, MergeMode, modelLogger, makeRejected, isVerticalDir, isLeadingDir, getAdjacentRange } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { FindMatchType, RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";
import { someInArraySource } from "@/io.ox/office/spreadsheet/utils/arraybase";

import { type AddressTransformer, TransformMode } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { type UsedRangeCollectionEventMap, UsedRangeCollection } from "@/io.ox/office/spreadsheet/model/usedrangecollection";
import type { MoveCellsEvent } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import type { GeneratorOptions, SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SheetCache } from "@/io.ox/office/spreadsheet/model/operation/sheetcache";
import type { FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import type { SheetEventBase } from "@/io.ox/office/spreadsheet/model/sheetevents";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * The type of an address in a merged range.
 */
export enum MergeMatchType {

    /**
     * An arbitrary cell inside a merged cell range.
     */
    ALL = "all",

    /**
     * The upper left reference cell.
     */
    REFERENCE = "reference",

    /**
     * A hidden (non-reference) cell.
     */
    HIDDEN = "hidden"
}

/**
 * Event object emitted by an instance of `MergeCollection`.
 */
export interface MergeCellsEvent extends SheetEventBase {

    /**
     * All merged cell ranges removed from the collection.
     */
    deleteRanges: RangeArray;

    /**
     * All merged cell ranges inserted into the collection.
     */
    insertRanges: RangeArray;
}

/**
 * Type mapping for the events emitted by `MergeCollection` instances.
 */
export interface MergeCollectionEventMap extends UsedRangeCollectionEventMap {

    /**
     * Will be emitted after merged ranges have been changed in this collection
     * (inserted and/or deleted).
     *
     * @param event
     *  An event object with the deleted and inserted merged ranges.
     */
    "merge:cells": [event: MergeCellsEvent];
}

// private types --------------------------------------------------------------

interface TransformRangeOptions extends ReverseOptions {

    /**
     * If set to `true`, merged ranges will not be capped at the sheet limit
     * when inserting, but deleted (used to test whether inserting columns/rows
     * would be possible without capping). Default value is `false`.
     */
    strict?: boolean;
}

// class MergeCollection ======================================================

/**
 * Collects information about all merged cell ranges of a single sheet in a
 * spreadsheet document.
 */
export class MergeCollection extends UsedRangeCollection<MergeCollectionEventMap> {

    // storage for all merged ranges
    #mergedRangeSet = new RangeSet();
    // all merged ranges, mapped by key of their start addresses
    readonly #mergedRangeMap = new Map<string, Range>();
    // set of start/end addresses of all merged ranges used to calculate the bounding range
    readonly #usedAddressSet = new AddressSet();

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The sheet model instance containing this collection.
     */
    constructor(sheetModel: SheetModel) {

        // base constructor
        super(sheetModel);

        // update merged ranges after moving cells, or inserting/deleting columns or rows
        this.listenTo(sheetModel.cellCollection, "move:cells", this.#moveCellsHandler);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this collection is empty.
     *
     * @returns
     *  Whether this collection is empty
     */
    isEmpty(): boolean {
        return !this.#mergedRangeSet.size;
    }

    /**
     * Returns, whether the cell at the specified address is the upper left
     * corner of a merged cell range.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  Whether the cell at the specified address is the upper left corner of a
     *  merged cell range.
     */
    isReferenceCell(address: Address): boolean {
        return !!this.getMergedRange(address, MergeMatchType.REFERENCE);
    }

    /**
     * Returns, whether the cell at the specified address is a hidden cell
     * inside a merged cell range.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  Whether the cell at the specified address is a hidden cell inside a
     *  merged cell range.
     */
    isHiddenCell(address: Address): boolean {
        return !!this.getMergedRange(address, MergeMatchType.HIDDEN);
    }

    /**
     * Returns, whether the cell at the specified address is part of a merged
     * cell range. It can be the reference cell or a hidden cell.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  Whether the cell is part of a merged cell range.
     */
    isMergedCell(address: Address): boolean {
        return !!this.getMergedRange(address, MergeMatchType.ALL);
    }

    /**
     * Returns whether the passed range is contained in this collection.
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  Whether the specified range is contained in this collection.
     */
    isMergedRange(range: Range): boolean {
        return this.#mergedRangeSet.has(range);
    }

    /**
     * Returns whether the single columns of the passed range are merged. The
     * merged columns must exactly fit into the range, and all columns in the
     * range must be merged.
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  Whether the specified range is entirely vertically merged.
     */
    isVerticallyMerged(range: Range): boolean {
        return this.#isColRowMerged(range, true);
    }

    /**
     * Returns whether the single rows of the passed range are merged. The
     * merged rows must exactly fit into the range, and all rows in the range
     * must be merged.
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  Whether the specified range is entirely horizontally merged.
     */
    isHorizontallyMerged(range: Range): boolean {
        return this.#isColRowMerged(range, false);
    }

    /**
     * Returns the range address of a merged cell range for a specified cell
     * address.
     *
     * @param address
     *  The address of a cell.
     *
     * @param type
     *  The type of the specified address.
     *
     * @returns
     *  The address of a merged range that covers the passed cell address; or
     *  `undefined`, if no merged range exists at the address.
     */
    getMergedRange(address: Address, type = MergeMatchType.ALL): Opt<Range> {

        // quickly find a merged range by its start address
        const mergedRange = this.#mergedRangeMap.get(address.key);

        // do not try to search in the set for search type REFERENCE (start address only)
        if (type === MergeMatchType.REFERENCE) { return mergedRange?.clone(); }

        // return a found merged range (but return null if search type is HIDDEN)
        if (mergedRange) { return (type === MergeMatchType.HIDDEN) ? undefined : mergedRange.clone(); }

        // find a merged range covering the passed address (it will not start at that address, otherwise
        // it would have been found above, therefore return it for search types HIDDEN and ALL)
        return this.#mergedRangeSet.findOneByAddress(address)?.clone();
    }

    /**
     * Returns whether any of the passed cell ranges overlaps with at least one
     * merged cell range in this collection.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param matchType
     *  Specifies which merged ranges from this collection will match.
     *
     * @returns
     *  Whether any of the passed cell ranges contains or overlaps with at
     *  least one merged cell range.
     */
    coversAnyMergedRange(ranges: RangeSource, matchType = FindMatchType.OVERLAP): boolean {
        return someInArraySource(ranges, range => !!this.#mergedRangeSet.findOne(range, matchType));
    }

    /**
     * Returns an iterator that yields the range addresses of all merged cell
     * ranges that overlap with one of the passed ranges.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param matchType
     *  Specifies which merged ranges from this collection will match.
     *
     * @yields
     *  The range addresses of all merged ranges that overlap with one of the
     *  passed ranges.
     */
    *yieldMergedRanges(ranges: RangeSource, matchType = FindMatchType.OVERLAP): IterableIterator<Range> {
        for (const range of RangeArray.cast(ranges)) {
            yield* this.#mergedRangeSet.yieldMatching(range, matchType);
        }
    }

    /**
     * Returns the range addresses of all merged cell ranges that overlap with
     * one of the passed ranges.
     *
     * @param ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param matchType
     *  Specifies which merged ranges from this collection will match.
     *
     * @returns
     *  The range addresses of all merged ranges that overlap with one of the
     *  passed ranges.
     */
    getMergedRanges(ranges: RangeSource, matchType = FindMatchType.OVERLAP): RangeArray {
        return RangeArray.from(this.yieldMergedRanges(ranges, matchType)) as RangeArray;
    }

    /**
     * Returns the merged range that completely contains or is equal to the
     * passed cell range. Note the difference to the method `getMergedRanges`
     * which returns all merged ranges that are contained IN the passed ranges.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  The address of a merged range that contains the passed range if
     *  existing, otherwise `undefined`.
     */
    getBoundingMergedRange(range: Range): Opt<Range> {
        return this.#mergedRangeSet.findOne(range, FindMatchType.COVER)?.clone();
    }

    /**
     * Expands the passed range address, so that it will include all merged
     * ranges it is currently covering partly.
     *
     * @param range
     *  The address of a cell range that will be expanded to the merged ranges
     *  contained in this collection.
     *
     * @returns
     *  The address of the expanded cell range.
     */
    expandRangeToMergedRanges(range: Range): Range {

        // shallow copy of the range set, to prevent processing them multiple times
        const pendingRangeSet = new RangeSet(this.#mergedRangeSet);

        // repeat as long as "pendingRangeSet" provides new merged ranges to expand with
        for (;;) {

            // find all merged ranges overlapping with the current range, return the
            // current range address if no more merged ranges can be found
            const mergedRanges = pendingRangeSet.deleteMatching(range);
            if (!mergedRanges.length) { return range.clone(); }

            // expand the range to all merged ranges
            range = range.boundary(mergedRanges.boundary()!);
        }
    }

    /**
     * Shrinks the passed range address, so that it will not include any merged
     * ranges it is currently covering partly.
     *
     * @param range
     *  The address of a cell range that will be shrunk until it does not
     *  partially contain any merged ranges.
     *
     * @param columns
     *  The direction in which the passed range will be shrunk. If set to
     *  `true`, the range will be shrunken at its left and right borders,
     *  otherwise at its top and bottom borders.
     *
     * @returns
     *  The address of the shrunk range; or `undefined`, if no valid range was
     *  left after shrinking (the range has been shrunk to zero columns/rows).
     */
    shrinkRangeFromMergedRanges(range: Range, columns: boolean): Opt<Range> {

        // the new shrunken range
        let newRange: Opt<Range>;

        // shrink the range at the left or top border as often as possible
        for (;;) {
            newRange = this.#shrinkRangeAtLeadingBorder(range, columns);
            if (!newRange) { return undefined; }
            if (range.equals(newRange)) { break; }
            range = newRange;
        }

        // shrink the range at the right or bottom border as often as possible
        for (;;) {
            newRange = this.#shrinkRangeAtTrailingBorder(range, columns);
            if (!newRange) { return undefined; }
            if (range.equals(newRange)) { break; }
            range = newRange;
        }

        return newRange;
    }

    // operation generators ---------------------------------------------------

    /**
     * Generates and applies "mergeCells" operations for all passed ranges.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param rangeSource
     *  An array of range addresses, or a single cell range address.
     *
     * @param mergeMode
     *  The merge mode to be inserted into the generated operations.
     *
     * @returns
     *  A promise that will fulfil when the operations have been generated
     *  successfully, or that will be rejected with an object with property
     *  "cause" set to one of the following error codes:
     *  - "merge:overlap": The passed cell ranges overlap each other.
     *  - "merge:overflow": Tried to merge too many ranges at once.
     */
    generateMergeCellsOperations(generator: SheetOperationGenerator, rangeSource: RangeSource, mergeMode: MergeMode): JPromise {

        // shortcuts to sheet model and collections
        const { sheetModel } = this;
        const { colCollection, rowCollection, cellCollection } = sheetModel;
        // whether to merge single columns in the passed ranges
        const vertical = mergeMode === MergeMode.VERTICAL;
        // whether to merge single rows in the passed ranges
        const horizontal = mergeMode === MergeMode.HORIZONTAL;
        // whether to unmerge the ranges
        const unmerge = mergeMode === MergeMode.UNMERGE;

        // convert ranges to unique array
        let ranges = RangeArray.cast(rangeSource).unify();

        // merging may have no effect for specific ranges
        if (!unmerge) {
            ranges = ranges.filter(range => horizontal ? !range.singleCol() : vertical ? !range.singleRow() : !range.single());
        }

        // nothing to do without ranges
        if (ranges.empty()) { return this.createResolvedPromise(); }

        // do not allow to merge overlapping ranges
        if (!unmerge && !ranges.distinct()) {
            return makeRejected("merge:overlap");
        }

        // count number of new merged ranges, shorten the new merged ranges to the used
        // area of the cell collection (bug 30662)
        if (!unmerge) {

            // the used area of the sheet (may be `undefined`)
            const usedRange = sheetModel.getUsedRange();
            // the maximum used column/row index
            const maxIndex = usedRange ? usedRange.a2.get(mergeMode !== MergeMode.HORIZONTAL) : 0;
            // the total number of created merged ranges
            let rangeCount = 0;

            // count the new merged ranges, shorten entire column/row ranges (bug 30662)
            const { addressFactory } = this.docModel;
            for (const range of ranges) {
                if (vertical) {
                    // reduce range to used area for entire rows
                    if (addressFactory.isRowRange(range)) { range.a2.c = maxIndex; }
                    rangeCount += range.cols();
                } else if (horizontal) {
                    // reduce range to used area for entire columns
                    if (addressFactory.isColRange(range)) { range.a2.r = maxIndex; }
                    rangeCount += range.rows();
                } else {
                    rangeCount += 1;
                }
            }

            // overflow check for range count
            if (rangeCount > MAX_MERGED_RANGES_COUNT) {
                return makeRejected("merge:overflow");
            }

            // divide the ranges into column ranges, row ranges, and regular cell ranges
            const rangeGroups = addressFactory.getRangeGroups(ranges);

            // check for oversized column ranges
            let checkPromise = rangeGroups.colRanges ? colCollection.checkMaxIntervalSize(rangeGroups.colRanges.colIntervals()) : null;
            if (checkPromise) { return checkPromise; }

            // check for oversized row ranges
            checkPromise = rangeGroups.rowRanges ? rowCollection.checkMaxIntervalSize(rangeGroups.rowRanges.rowIntervals()) : null;
            if (checkPromise) { return checkPromise; }

            // check for oversized cell ranges
            checkPromise = rangeGroups.cellRanges ? cellCollection.checkMaxRangeSize(rangeGroups.cellRanges) : null;
            if (checkPromise) { return checkPromise; }
        }

        // collect all current merged ranges covered by the passed ranges (will be unmerged regardless
        // of the merge type, and therefore need to be restored with undo operations)
        const restoreRanges = new RangeArray();
        let remainingRanges = Array.from(this.#mergedRangeSet);

        // first, unmerge all ranges that are partly covered by the new merged ranges
        const unmergeRanges = new RangeArray();
        for (const range of ranges) {

            // split the remaining merged ranges (not yet covered by a range passed to this method)
            // into two groups, depending whether they are covered by the range currently processed
            const [remainingRanges2, overlapRanges] = ary.split(remainingRanges, mergedRange => mergedRange.overlaps(range));

            // generate an "unmerge" operation (also, if merging causes to unmerge existing merged ranges)
            if (unmerge || overlapRanges.length) {
                unmergeRanges.push(range);
            }

            // all merged ranges covered by the current range need to be restored in undo (see below)
            restoreRanges.append(overlapRanges);

            // continue with only the merged ranges not covered by a range passed to this method
            remainingRanges = remainingRanges2;
        }

        // generate the operation to unmerge the ranges
        generator.generateMergeCellsOperation(unmergeRanges, MergeMode.UNMERGE);

        // delete covered notes and comments before merging the ranges (but undo must insert them after "unmerge"!)
        const generator2 = sheetModel.createOperationGenerator({ applyImmediately: true });
        return jpromise.fastChain(

            // delete covered notes and comments before merging the ranges (but undo must insert them after "unmerge"!)
            () => sheetModel.noteCollection.generateMergeCellsOperations(generator2, ranges, mergeMode),
            () => sheetModel.commentCollection.generateMergeCellsOperations(generator2, ranges, mergeMode),
            () => { generator.appendOperations(generator2); },

            // merge all ranges ("unmerge" operation already done above)
            unmerge ? null : (() => {
                generator.generateMergeCellsOperation(ranges, MergeMode.UNMERGE, { undo: true });
                generator.generateMergeCellsOperation(ranges, mergeMode);
            }),

            // generate the hyperlink operations
            () => sheetModel.hlinkCollection.generateMergeCellsOperations(generator, ranges, mergeMode),

            // generate the cell operations (e.g.: move values to top-left cells, expand formatting)
            () => cellCollection.generateMergeCellsOperations(generator, ranges, mergeMode),

            // Undo: restore deleted cell notes and comments after the merged ranges have been unmerged,
            // and add all old merged ranges to the undo generator that need to be restored
            () => {
                generator.appendOperations(generator2, { undo: true });
                this.#generateRangesOperations(generator, restoreRanges, { undo: true });
            }
        );
    }

    /**
     * Generates the undo operations to restore the merged ranges in this
     * collection that would not be restored automatically with the reverse
     * operation of the passed update task.
     *
     * @param sheetCache
     *  The sheet operation cache to be filled with the operations.
     *
     * @param updateTask
     *  The update task to be executed.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    generateUpdateTaskOperations(sheetCache: SheetCache, updateTask: FormulaUpdateTask): JPromise {

        // only process move operations in the own sheet
        if (!this.sheetModel.isOwnMoveTask(updateTask)) { return jpromise.resolve(); }

        // collect all merged ranges that cannot be restored by applying the reversed move operation
        const transformer = updateTask.transformer;
        const restoreRanges = new RangeArray();
        const promise = this.asyncForEach(this.#mergedRangeMap.values(), mergedRange => {

            // bug 64115: check that the move operation does not push merged ranges outside the sheet
            if (transformer.insert && !this.#transformMergedRange(transformer, mergedRange, { strict: true })) {
                OperationError.throwCause("cells:pushoff");
            }

            // transform the merged range back and forth to decide whether it can be restored implicitly
            const transformRange = this.#transformMergedRange(transformer, mergedRange);
            const restoredRange = transformRange ? this.#transformMergedRange(transformer, transformRange, { reverse: true }) : undefined;

            // collect all ranges that cannot be restored implicitly
            if (!restoredRange || restoredRange.differs(mergedRange)) {
                restoreRanges.push(mergedRange);
            }
        });

        // add all merged ranges to the undo generator that need to be restored
        promise.done(() => this.#generateRangesOperations(sheetCache, restoreRanges, { undo: true }));
        return promise;
    }

    /**
     * Generates the operations, and the undo operations, to repeatedly copy
     * the merged ranges from the source range into the specified direction.
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param range
     *  The address of the source cell range to be copied.
     *
     * @param direction
     *  The direction in which the specified cell range will be expanded.
     *
     * @param count
     *  The number of columns/rows to extend the cell range into the
     *  specified direction.
     *
     * @returns
     *  A promise that will fulfil when all operations have been generated.
     */
    @modelLogger.profileMethod("$badge{MergeCollection} generateAutoFillOperations")
    generateAutoFillOperations(generator: SheetOperationGenerator, range: Range, direction: Direction, count: number): JPromise {

        // the merged ranges to be copied
        const mergedRanges = this.getMergedRanges(range, FindMatchType.CONTAIN);
        // the target range to be filled with the merged ranges
        const targetRange = getAdjacentRange(range, direction, count);

        // first step for undo: delete the merged ranges generated by auto-fill
        generator.generateMergeCellsOperation(targetRange, MergeMode.UNMERGE, { undo: true });

        // delete all existing merged ranges from target range (and generate all undo operations to restore them)
        let promise = this.coversAnyMergedRange(targetRange) ?
            this.generateMergeCellsOperations(generator, targetRange, MergeMode.UNMERGE) :
            this.createResolvedPromise();

        // nothing more to do, if no merged ranges exist in the source range
        if (mergedRanges.empty()) { return promise; }

        // generate the operations to copy the merged ranges
        promise = promise.then(() => {

            // number of repetitions
            const columns = !isVerticalDir(direction);
            const size = range.size(columns);
            const cycles = Math.floor(count / size);
            if (cycles === 0) { return; }

            // create all merged ranges for the target range
            const sign = isLeadingDir(direction) ? -1 : 1;
            const newMergedRanges = new RangeArray();
            for (let cycle = 1; cycle <= cycles; cycle += 1) {
                const diff = sign * cycle * size;
                for (const mergedRange of mergedRanges) {
                    newMergedRanges.push(mergedRange.clone().moveBoth(diff, columns));
                }
            }

            // generate the operations (but no undo operations, has been done above already)
            this.#generateRangesOperations(generator, newMergedRanges);
        });

        return promise;
    }

    // operation implementations ----------------------------------------------

    /**
     * Callback handler for the document operation "copySheet". Clones all
     * merged ranges from the passed collection into this collection.
     *
     * @param context
     *  A wrapper representing the "copySheet" document operation.
     *
     * @param fromCollection
     *  The existing collection whose contents will be cloned into this new
     *  collection.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyCopySheetOperation(context: SheetOperationContext, fromCollection: MergeCollection): void {

        // clone the contents of the source collection
        for (const mergedRange of fromCollection.#mergedRangeSet) {
            this.#insertMergedRange(mergedRange.clone());
        }

        // update the used range, and emit the "change:usedarea" event
        this.updateUsedRange(context);
    }

    /**
     * Callback handler for the document operation "mergeCells". Merges the
     * cell range, or removes any merged ranges from the range covered by the
     * operation.
     *
     * @param context
     *  A wrapper representing the "mergeCells" document operation.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyMergeCellsOperation(context: SheetOperationContext): void {

        // the ranges covered by the operation
        const ranges = context.getRangeList("ranges");
        // the method how to merge/unmerge the operation range
        const mergeMode = context.optEnum("type", MergeMode, MergeMode.MERGE);

        // merged ranges must not overlap (but allow overlapping when unmerging)
        if (mergeMode !== MergeMode.UNMERGE) {
            context.ensure(ranges.distinct(), "overlapping merged ranges");
        }

        // all merged ranges deleted from the collection
        const deleteRanges = new RangeArray();
        // all merged ranges added to the collection
        const insertRanges = new RangeArray();

        // inserts the passed merged range into the containers, and inserts it into "insertedRanges"
        const registerMergedRange = (mergedRange: Range): void => {
            this.#insertMergedRange(mergedRange);
            insertRanges.push(mergedRange.clone());
        };

        // implicitly remove all merged ranges that overlap with the passed range,
        // regardless of passed type (needed to merge over merged ranges)
        for (const range of ranges) {
            for (const mergedRange of this.#mergedRangeSet.yieldMatching(range)) {
                this.#removeMergedRange(mergedRange);
                deleteRanges.push(mergedRange);
            }
        }

        // handle the different merge types
        switch (mergeMode) {

            case MergeMode.MERGE:
                ranges.forEach(range => {
                    if (range.cells() > 1) {
                        registerMergedRange(range);
                    }
                });
                break;

            case MergeMode.HORIZONTAL:
                ranges.forEach(range => {
                    if (range.cols() > 1) {
                        for (let row = range.a1.r; row <= range.a2.r; row += 1) {
                            registerMergedRange(Range.fromIndexes(range.a1.c, row, range.a2.c, row));
                        }
                    }
                });
                break;

            case MergeMode.VERTICAL:
                ranges.forEach(range => {
                    if (range.rows() > 1) {
                        for (let col = range.a1.c; col <= range.a2.c; col += 1) {
                            registerMergedRange(Range.fromIndexes(col, range.a1.r, col, range.a2.r));
                        }
                    }
                });
                break;

            case MergeMode.UNMERGE:
                // all merged ranges have been removed already, see above
                break;
        }

        // emit the "merge:cells" event with arrays of changed merged ranges
        if (deleteRanges.length || insertRanges.length) {
            this.trigger("merge:cells", this.createSheetEvent({ deleteRanges, insertRanges }, context));
        }

        // update the used range, and emit the "change:usedarea" event
        this.updateUsedRange(context);
    }

    // protected methods ------------------------------------------------------

    /**
     * Implementation of abstract base method. Calculates the used range of
     * this collection (the range containing all merged ranges).
     */
    protected override calculateUsedRange(): Opt<Range> {
        return this.#usedAddressSet.boundary();
    }

    // private methods --------------------------------------------------------

    #getUsedEnd(mergedRange: Range): Address {
        const { addressFactory } = this.docModel;
        const address = mergedRange.a2.clone();
        if (addressFactory.isColRange(mergedRange)) { address.r = 0; }
        if (addressFactory.isRowRange(mergedRange)) { address.c = 0; }
        return address;
    }

    /**
     * Inserts the passed merged range into the internal containers.
     */
    #insertMergedRange(mergedRange: Range): void {
        this.#mergedRangeSet.add(mergedRange);
        this.#mergedRangeMap.set(mergedRange.a1.key, mergedRange);
        this.#usedAddressSet.add(mergedRange.a1);
        this.#usedAddressSet.add(this.#getUsedEnd(mergedRange));
    }

    /**
     * Removes the passed merged range from the internal containers.
     */
    #removeMergedRange(mergedRange: Range): void {
        this.#mergedRangeSet.delete(mergedRange);
        this.#mergedRangeMap.delete(mergedRange.a1.key);
        this.#usedAddressSet.delete(mergedRange.a1);
        this.#usedAddressSet.delete(this.#getUsedEnd(mergedRange));
    }

    /**
     * Returns whether the single columns or rows of the passed range are
     * merged. The merged columns/rows must exactly fit into the range, and all
     * columns/rows in the range must be merged.
     *
     * @param range
     *  The address of a cell range.
     *
     * @returns
     *  Whether the columns/rows of the specified range are merged.
     */
    #isColRowMerged(range: Range, columns: boolean): boolean {

        // all merged ranges that are contained in the passed range
        const containedRanges = this.getMergedRanges(range, FindMatchType.CONTAIN);

        // the number of merged ranges contained by the passed range must be equal to the
        // column/row count of the range, otherwise there is no need to iterate at all
        if (containedRanges.length !== range.size(columns)) {
            return false;
        }

        // check size and position of the merged ranges (if all merged ranges start and
        // end at the boundaries of the passed range, and are single-sized in the other
        // direction, the entire range is filled by single-sized merged ranges)
        return containedRanges.every(mergedRange => {
            return (mergedRange.size(columns) === 1) &&
                (mergedRange.getStart(!columns) === range.getStart(!columns)) &&
                (mergedRange.getEnd(!columns) === range.getEnd(!columns));
        });
    }

    /**
     * Generates the "mergeCells" undo operations needed to restore the
     * specified merged cell ranges. The number of generated operations will be
     * minimized by grouping consecutive single column ranges and single row
     * ranges into merge operations with merge type "horizontal" or "vertical".
     *
     * @param generator
     *  The operations generator to be filled with the operations.
     *
     * @param ranges
     *  An array of addresses of merged cell ranges.
     *
     * @param [options]
     *  Optional parameters for the operation generator.
     */
    #generateRangesOperations(generator: SheetOperationGenerator, ranges: Range[], options?: GeneratorOptions): void {

        // all ranges to be generated with a vertical merge
        const vertOpRanges = new RangeArray();
        // all ranges to be generated with a horizontal merge
        const horzOpRanges = new RangeArray();
        // all ranges to be generated with a regular merge
        const mergeOpRanges = new RangeArray();

        // split the merged ranges into single-column, single-row, and two-dimensional ranges
        const rangeGroups = ary.group(ranges, range => range.singleCol() ? "col" : range.singleRow() ? "row" : "range");

        // collect and merge adjacent single-line ranges
        rangeGroups.forEach((groupRanges, groupType) => {

            // regular two-dimensional merged ranges
            if (groupType === "range") {
                mergeOpRanges.append(groupRanges);
                return;
            }

            // orientation of the single-line ranges
            const columns = groupType === "col";
            // the target range list for single ranges
            const singleOpRanges = columns ? vertOpRanges : horzOpRanges;

            // sort the ranges according to the direction
            const comparator = columns ? Address.compare : Address.compareVert;
            groupRanges.sort((range1, range2) => comparator(range1.a1, range2.a1));

            // due to the sorting, ranges that can be combined into a single range are side-by-side in the array
            for (let ai = 0, al = groupRanges.length, count: number; ai < al; ai += count) {

                // the first range for the new compound range
                const range1 = groupRanges[ai];
                const index = range1.getStart(columns);
                const start = range1.getStart(!columns);
                const end = range1.getEnd(!columns);

                // continue in the array as long as following ranges are exactly adjacent to `range1`
                for (count = 1; ai + count < al; count += 1) {
                    const range2 = groupRanges[ai + count];
                    if (index + count !== (range2.getStart(columns)) || (start !== range2.getStart(!columns)) || (end !== range2.getEnd(!columns))) { break; }
                }

                // create one compound range for all adjacent single-line ranges
                const compoundRange = columns ? range1.colRange(0, count) : range1.rowRange(0, count);
                ((count > 1) ? singleOpRanges : mergeOpRanges).push(compoundRange);
            }
        });

        // generate the operations for the different types of merged ranges
        generator.generateMergeCellsOperation(mergeOpRanges, MergeMode.MERGE, options);
        generator.generateMergeCellsOperation(vertOpRanges, MergeMode.VERTICAL, options);
        generator.generateMergeCellsOperation(horzOpRanges, MergeMode.HORIZONTAL, options);
    }

    /**
     * Transforms the specified merged range.
     *
     * @param transformer
     *  An address transformer that specifies how to transform the passed
     *  merged range.
     *
     * @param mergedRange
     *  The address of a merged range to be transformed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed merged range if available (will contain more than one
     *  cell); or `undefined` for a deleted merged range.
     */
    #transformMergedRange(transformer: AddressTransformer, mergedRange: Range, options?: TransformRangeOptions): Opt<Range> {

        // transform the passed range without expanding the end of the range
        const transformMode = options?.strict ? TransformMode.STRICT : TransformMode.RESIZE;
        const newMergedRange = transformer.transformRanges(mergedRange, { reverse: options?.reverse, transformMode }).first();

        // skip merged ranges that have been shrunken to a single cell
        return (newMergedRange && !newMergedRange.single()) ? newMergedRange : undefined;
    }

    /**
     * Recalculates the position of all merged ranges, after cells have been
     * moved (including inserted/deleted columns or rows) in the sheet.
     */
    #moveCellsHandler({ transformer }: MoveCellsEvent): void {

        // fill the containers during range transformation
        const oldRangeSet = this.#mergedRangeSet;
        this.#mergedRangeSet = new RangeSet();
        this.#mergedRangeMap.clear();
        this.#usedAddressSet.clear();

        // process all original merged ranges
        for (const mergedRange of oldRangeSet) {
            const newMergedRange = this.#transformMergedRange(transformer, mergedRange);
            if (newMergedRange) { this.#insertMergedRange(newMergedRange); }
        }

        // update the used range, and emit the "change:usedarea" event
        this.updateUsedRange();
    }

    /**
     * Shrinks the passed range by all merged ranges jutting out to the left or
     * top.
     */
    #shrinkRangeAtLeadingBorder(oldRange: Range, columns: boolean): Opt<Range> {

        // outer cells of the passed range
        const borderRange = columns ? oldRange.leadingCol() : oldRange.headerRow();
        // copy of the passed range that will be shrunken
        const newRange = oldRange.clone();

        // reduce the passed range by all found merged ranges that are jutting out
        for (const mergedRange of this.#mergedRangeSet.yieldMatching(borderRange)) {
            if (mergedRange.getStart(columns) < oldRange.getStart(columns)) {
                const index = Math.max(newRange.getStart(columns), mergedRange.getEnd(columns) + 1);
                if (index > newRange.getEnd(columns)) { return undefined; }
                newRange.setStart(index, columns);
            }
        }

        return newRange;
    }

    /**
     * Shrinks the passed range by all merged ranges jutting out to the right
     * or bottom.
     */
    #shrinkRangeAtTrailingBorder(oldRange: Range, columns: boolean): Opt<Range> {

        // outer cells of the passed range
        const borderRange = columns ? oldRange.trailingCol() : oldRange.footerRow();
        // copy of the passed range that will be shrunken
        const newRange = oldRange.clone();

        // reduce the passed range by all found merged ranges that are jutting out
        for (const mergedRange of this.#mergedRangeSet.yieldMatching(borderRange)) {
            if (oldRange.getEnd(columns) < mergedRange.getEnd(columns)) {
                const index = Math.min(newRange.getEnd(columns), mergedRange.getStart(columns) - 1);
                if (index < newRange.getStart(columns)) { return undefined; }
                newRange.setStart(index, columns);
            }
        }

        return newRange;
    }
}
