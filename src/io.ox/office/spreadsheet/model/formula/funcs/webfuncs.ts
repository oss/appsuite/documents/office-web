/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with web content.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// exports ====================================================================

export default declareFunctionSpecModule("web", factory => {

    factory.fixed("DDE", "val:any", 3, 4, ["val:str", "val:str", "val:str", "val:int"], {
        name: { ooxml: null },
        hidden: true
    });

    factory.unaryStr("ENCODEURL", {
        name: { ooxml: "_xlfn.ENCODEURL", odf: "COM.MICROSOFT.ENCODEURL" },
        hidden: true
    });

    factory.binaryStr("FILTERXML", {
        name: { ooxml: "_xlfn.FILTERXML", odf: "COM.MICROSOFT.FILTERXML" },
        hidden: true
    });

    factory.binary1("HYPERLINK", "val:any", ["val:str", "val:any"], {
        recalc: "once",
        resolve(url, value = url) {
            return this.createOperand(value, { url });
        }
    });

    factory.unbound("RTD", "val:any", 3, 1, ["val:str", "val:str", "val:str"], {
        name: { odf: null },
        hidden: true
    });

    factory.unaryStr("WEBSERVICE", {
        name: { ooxml: "_xlfn.WEBSERVICE", odf: "COM.MICROSOFT.WEBSERVICE" },
        hidden: true
    });
});
