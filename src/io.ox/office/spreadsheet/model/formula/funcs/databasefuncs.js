/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

/******************************************************************************
 *
 * This module implements all spreadsheet functions treating cell ranges like
 * minimal databases.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { is, fun } from "@/io.ox/office/tk/algorithms";

import { ErrorCode, isErrorCode, Range3D } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { VarianceMethod, div, product, minOf, maxOf, variance } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// class Database =============================================================

/**
 * Database created from the range of cells.
 * The first row of the range will be used for the database column labels.
 *
 * @param {Range3D} range to create the database.
 *
 * @param {String | Number} field indicate the column wich are used for the databasefuncs.
 *  It's the name or the index of the column.
 *
 * @param {FormulaContext} context the formulacontext.
 *
 * @param {Boolean} [emptyFieldIsInvalid=true] false if the field value can be null, otherwise
 *  the field must be a String or Number.
 */
function Database(range, field, context, emptyFieldIsInvalid = true) {

    // public methods --------------------------------------------------------

    /**
     * Return the database column index by the name of the label.
     *
     * @param {String} name to the column index.
     * @returns {Number} the column index or -1 if the column label with the given name not exists.
     */
    this.getColumnIndexByName = function (name) {
        let index = -1;

        if (name) {
            name = name.toLowerCase();
            const iterator = context.createScalarIterator(getRowRange(range, range.a1.r));
            for (const opState of iterator) {
                if (name === context.convertToString(opState.value).toLowerCase()) {
                    index = opState.offset;
                    break;
                }
            }
        }

        return index;
    };

    // initialization ---------------------------------------------------------

    if (range.rows() < 2) {
        throw ErrorCode.VALUE;
    }

    // Todo Check max row size
    if (emptyFieldIsInvalid && (field === null)) {
        throw ErrorCode.NAME;
    }

    this.columnIndex = -1;

    if (field !== null) {
        if (is.string(field)) {
            this.columnIndex = this.getColumnIndexByName(field);
        } else if (is.number(field) && (field >= 1) && (field <= range.cells())) {
            this.columnIndex = field - 1;
        }

        if (this.columnIndex < 0) {
            throw ErrorCode.VALUE;
        }
    }
}

// class Criteria =============================================================

/**
 * Criteria is a range wich specifie the criteria to filter the database values.
 * The minimum of rows are two rows, the first is the name to identifie the database
 * columns and the second is for the criterias.
 *
 * @param {Range3D} range the range to create the criteria.
 * @param {Database} db to check if the columns of the criteria exists.
 * @param {FormulaContext} context the formulacontext.
 */
function Criteria(range, db, context) {

    const filters = [];
    const criteriaRowSize = range.rows() - 1;

    function getFilterForRow(rowIndex) {
        let filter = filters.find(rowFilter => rowFilter.rowIndex === rowIndex);
        if (!filter) {
            filter = [];
            filter.rowIndex = rowIndex;
            filters.push(filter);
        }
        return filter;
    }

    /**
     * Return a range of criteria values from a column without the label.
     *
     * @param {Number} columnIndex the column index to values.
     * @returns {Range3D} the created range.
     */
    function getColumnValueRange(columnIndex) {
        return Range3D.fromIndexes3D(range.a1.c + columnIndex, range.a1.r + 1, range.a1.c + columnIndex, range.a2.r, range.sheet1, range.sheet2);
    }

    /**
     * Check if values match to the criteria filters.
     *
     * @param {Object[]} values the values to check.
     * @returns {Boolean} true if the filter matches, otherwise false.
     */
    this.matchValues = function (values) {
        return withoutCriteria || filters.some(rowFilters => {
            return !rowFilters || rowFilters.every(columnFilters => {
                const rowValue = values[columnFilters.dbColumnIndex];
                return columnFilters.every(filterMatcher => filterMatcher(rowValue));
            });
        });
    };

    // initialization -----------------------------------------------------

    if (range.rows() < 2) {
        throw ErrorCode.VALUE;
    }

    // iterate through all columns
    context.forEachScalar(getRowRange(range, range.a1.r), (colName, opState1) => {

        const dbColumnIndex = db.getColumnIndexByName(context.convertToString(colName));
        if (dbColumnIndex < 0) { throw ErrorCode.DIV0; }

        // Iterate through all rows. If a criteria is reached save it.
        context.forEachScalar(getColumnValueRange(opState1.col), (value, opState2) => {

            const rowFilters = getFilterForRow(opState2.row);
            const filterMatcher = context.createFilterMatcher(value);
            let columnFilters = rowFilters[dbColumnIndex];

            if (!is.array(columnFilters)) {
                columnFilters = [];
                columnFilters.dbColumnIndex = dbColumnIndex;
                rowFilters.push(columnFilters);
            }

            columnFilters.push(filterMatcher);

        });
    });

    if (context.OOXML) {
        while (criteriaRowSize > filters.length) {
            filters.push(null);
        }
    }

    const withoutCriteria = filters.length === 0;
}

// private functions ==========================================================

/**
 * Create a range which includes a singel row with all columns.
 *
 * @param {Range3D} range the range to extract the new range
 * @param {type} rowIndex the row index for create the range
 * @returns {Range3D} the created range.
 */
function getRowRange(range, rowIndex) {
    return Range3D.fromIndexes3D(range.a1.c, rowIndex, range.a2.c, rowIndex, range.sheet1, range.sheet2);
}

/**
 * Get the function result.
 *
 * @param {Range3D} dbRange
 *  to create the database.
 *
 * @param {String|Number} field
 *  indicate the column wich are used for the databasefuncs. It's the name
 *  or the index of the column.
 *
 * @param {Range3D} crRange
 *  the range to create the criteria.
 *
 * @param {FormulaContext} context
 *  the formulacontext.
 *
 * @param {Function} resultCallback
 *  a function wich get the result array as the first parameter.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.resultType="array"]
 *      If "array" add the number values to an result array, if "count"
 *      only count the matched rows.
 *  - {Boolean} [options.onlyNumbers=true]
 *      if true the value for the result must be a Number, otherwise all
 *      values will be added to the result
 *  - {Number} [options.stopOnMatchCount=0]
 *      if <=0 all values will be added to the result, otherwise stops on
 *      the x match of the value.
 *  - {Boolean} [options.emptyFieldIsNotValid=true]
 *      true if the field value can be null, otherwise the field must be a
 *      String or Number.
 *
 * @returns {Any}
 *  the return value of the resultCallback.
 */
function getResult(dbRange, field, crRange, context, resultCallback, options) {

    const resultIsArray = (options?.resultType ?? "array") === "array";
    let result = resultIsArray ? [] : 0;
    const onlyNumbers = options?.onlyNumbers ?? true;
    const stopOnMatchCount = options?.stopOnMatchCount ?? 0;
    let sum = 0;
    const emptyFieldIsNotValid = options?.emptyFieldIsNotValid ?? true;
    const db = new Database(dbRange, field, context, emptyFieldIsNotValid);
    const criteria = new Criteria(crRange, db, context);
    const endRowIndex = dbRange.a2.r;

    for (let rowIndex = dbRange.a1.r + 1; rowIndex <= endRowIndex; rowIndex++) {
        const rowValues = context.getScalarsAsArray(getRowRange(dbRange, rowIndex), { blankCells: true, acceptErrors: true });

        if (criteria.matchValues(rowValues)) {
            const cellValue = rowValues[db.columnIndex];
            if (resultIsArray && isErrorCode(cellValue)) { throw cellValue; }
            const isNum = is.number(cellValue);
            if ((!onlyNumbers && cellValue !== null) || isNum) {

                if (isNum) { sum += cellValue; }

                if (resultIsArray) {
                    result.push(cellValue);
                    if (stopOnMatchCount > 0 && result.length >= stopOnMatchCount) {
                        break;
                    }
                } else {
                    result++;
                    if (stopOnMatchCount > 0 && result >= stopOnMatchCount) {
                        break;
                    }
                }
            }
        }
    }

    return resultCallback(result, sum);
}

/**
 * Return the count of the cells which are matched to the criteria
 *
 * @param {Range3D} dbRange @see getResult()
 * @param {String | Number} field @see getResult()
 * @param {Range3D} criteriaRange @see getResult()
 * @param {FormulaContext} context @see getResult()
 * @param {Boolean} nonblankCells if true or if the field is null all field cells will count
 * @returns {Number} the count of cells.
 */
function getCountResult(dbRange, field, criteriaRange, context, nonblankCells) {
    return getResult(dbRange, field, criteriaRange, context, fun.identity, {
        resultType: "count",
        onlyNumbers: (field !== null) && !nonblankCells,
        emptyFieldIsNotValid: false
    });
}

// exports ====================================================================

export default declareFunctionSpecModule("database", factory => {

    factory.ternary("DAVERAGE", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, (numbers, sum) => div(sum, numbers.length));
    });

    factory.ternary("DCOUNT", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getCountResult(dbRange, field, crRange, this, false);
    });

    factory.ternary("DCOUNTA", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getCountResult(dbRange, field, crRange, this, true);
    });

    factory.ternary("DGET", "val:any", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, values => {
            if (values.length === 0) { throw ErrorCode.VALUE; }
            if (values.length > 1) { throw ErrorCode.NUM; }
            return values[0];
        }, { onlyNumbers: false });
    });

    factory.ternary("DMAX", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, numbers => numbers.length ? maxOf(numbers) : 0);
    });

    factory.ternary("DMIN", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, numbers => numbers.length ? minOf(numbers) : 0);
    });

    factory.ternary("DPRODUCT", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, numbers => numbers.length ? product(numbers) : 0);
    });

    factory.ternary("DSTDEV", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, (numbers, sum) => variance(numbers, sum, VarianceMethod.DEV, false));
    });

    factory.ternary("DSTDEVP", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, (numbers, sum) => variance(numbers, sum, VarianceMethod.DEV, true));
    });

    factory.ternary("DSUM", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, (_numbers, sum) => sum);
    });

    factory.ternary("DVAR", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, (numbers, sum) => variance(numbers, sum, VarianceMethod.VAR, false));
    });

    factory.ternary("DVARP", "val:num", ["ref:single", "val", "ref:single"], function (dbRange, field, crRange) {
        return getResult(dbRange, field, crRange, this, (numbers, sum) => variance(numbers, sum, VarianceMethod.VAR, true));
    });
});
