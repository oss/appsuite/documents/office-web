/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with matrixes.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 * In spreadsheet documents, matrixes can be given as literals in formulas, or
 * can be extracted from rectangular cell ranges. The formula engine provides
 * the function signature type "mat" (and its sub types such as "mat:num"), and
 * the class `Matrix` whose instances contain the elements of a matrix in a
 * two-dimensional array.
 *
 *****************************************************************************/

import { fun } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { mul } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import { ensureMatrixDim, NumberMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import type { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { NumberAggregatorOptions } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// constants ==================================================================

// aggregation options for the SUMPRODUCT function
const SUMPRODUCT_OPTIONS: NumberAggregatorOptions = {
    valMode: "exact",       // value operands: must be a number
    matMode: "skip",        // matrix operands: skip strings and booleans
    refMode: "skip",        // reference operands: skip strings and booleans
    emptyParam: true,       // empty parameter will lead to #VALUE! (due to EXACT mode)
    // multiRange: false,   // do not accept multi-range references
    // multiSheet: false,   // do not accept multi-sheet references
    checkSize: "exact"      // operands must have equal width and height
};

// aggregation options for other matrix sum functions (SUMX2MY2 etc.)
const SUM_XY_OPTIONS: NumberAggregatorOptions = {
    valMode: "exact",               // value operands: must be numbers
    matMode: "skip",                // matrix operands: skip strings and booleans
    refMode: "skip",                // reference operands: skip strings and booleans
    emptyParam: true,               // empty parameter will lead to #VALUE! (due to EXACT mode)
    // multiRange: false,           // do not accept multi-range references
    // multiSheet: false,           // do not accept multi-sheet references
    checkSize: "size",              // operands must have same element count, but ignore width/height (e.g. 2x2 and 1x4)
    sizeMismatchError: ErrorCode.NA // throw #N/A error code if the sizes do not match
};

// private functions ==========================================================

/**
 * Throws an error code, if the passed matrix is not a square matrix (i.e., its
 * width and height are different).
 *
 * @param matrix
 *  The matrix to be checked.
 *
 * @returns
 *  The size of a square matrix.
 *
 * @throws
 *  The error code `#VALUE!`, if the passed matrix is not a square matrix.
 */
function ensureSquareMatrix(matrix: NumberMatrix): number {
    const dim = matrix.cols();
    return (dim === matrix.rows()) ? dim : ErrorCode.VALUE.throw();
}

function aggregateSumProduct(result: number, nums: number[]): number {
    return result + nums.reduce(mul, 1);
}

function aggregateSumX2MY2(result: number, nums: number[]): number {
    return result + nums[0] * nums[0] - nums[1] * nums[1];
}

function aggregateSumX2PY2(result: number, nums: number[]): number {
    return result + nums[0] * nums[0] + nums[1] * nums[1];
}

function aggregateSumXMY2(result: number, nums: number[]): number {
    const diff = nums[0] - nums[1];
    return result + diff * diff;
}

// exports ====================================================================

export default declareFunctionSpecModule("matrix", factory => {

    const ANY_MAT_FORCE = factory.param("any", { mat: "force" });

    factory.unary("MDETERM", "val:num", "mat:num", function (matrix) {

        // only square matrixes have a determinant
        const dim = ensureSquareMatrix(matrix);

        // shortcut: use compact formulas for small matrixes
        const vals = matrix.raw();
        switch (dim) {
            case 1: return vals[0][0];
            case 2: return vals[0][0] * vals[1][1] - vals[1][0] * vals[0][1];
            case 3: return vals[0][0] * vals[1][1] * vals[2][2] + vals[0][1] * vals[1][2] * vals[2][0] + vals[0][2] * vals[1][0] * vals[2][1]
                - vals[2][0] * vals[1][1] * vals[0][2] - vals[2][1] * vals[1][2] * vals[0][0] - vals[2][2] * vals[1][0] * vals[0][1];
        }

        // clone the matrix for in-place LU decomposition
        const luMat = matrix.clone();
        const luVals = luMat.raw();

        // the sign of the determinant changes with every row-swap
        let sign = 1;

        // in-place LU decomposition: matrix L and U will be stored in the same matrix,
        // the diagonal elements of L (all one) will not be stored
        for (let i = 0; i < dim; i += 1) {

            // bug 48482: escape if algorithm runs too long
            this.checkEvalTime();

            // find the first row with non-zero element in column `i` (skip leading rows)
            let row: number;
            for (row = i; row < dim; row += 1) {
                if (luVals[row][i] !== 0) { break; }
            }

            // if column `i` contains zeros only, the matrix is not invertible (determinant is zero)
            if (row === dim) { return 0; }

            // swap rows to move the row with non-zero column element to position `i`
            if (i !== row) {
                luMat.swapRows(i, row);
                sign = -sign;
            }

            // the diagonal element
            const diag = luVals[i][i];

            // update the remaining (following) rows in the matrix
            for (row = i + 1; row < dim; row += 1) {
                const rowVec = luVals[row];
                // calculate the elements of the L matrix
                rowVec[i] /= diag;
                // calculate the elements of the U matrix
                const factor = -rowVec[i];
                for (let col = i + 1; col < dim; col += 1) {
                    rowVec[col] += factor * luVals[i][col];
                }
            }
        }

        // multiply the resulting diagonal elements of the U matrix
        return luVals.reduce((det, rowVec, row) => det * rowVec[row], sign);
    });

    factory.unary("MINVERSE", "mat:num", "mat:num", function (matrix) {

        // only square matrixes are invertible
        const dim = ensureSquareMatrix(matrix);

        // left: original matrix, will be transformed into identity matrix
        const lMat = matrix.clone();
        const lVals = lMat.raw();

        // right: identity matrix, will be transformed into inverse matrix
        const rMat = NumberMatrix.identity(dim);

        // process all rows in both matrixes
        for (let i = 0; i < dim; i += 1) {

            // bug 48482: escape if algorithm runs too long
            this.checkEvalTime();

            // find the first row with non-zero element in column `i` (skip leading rows)
            let row: number;
            for (row = i; row < dim; row += 1) {
                if (lVals[row][i] !== 0) { break; }
            }

            // if column `i` contains zeros only, the matrix is not invertible
            if (row === dim) { throw ErrorCode.NUM; }

            // swap rows to move the row with non-zero column element to position `i`
            if (i !== row) {
                lMat.swapRows(i, row);
                rMat.swapRows(i, row);
            }

            // scale the row to have the number 1 on the diagonal of the original matrix
            let factor = 1 / lVals[i][i];
            lMat.scaleRow(i, factor);
            rMat.scaleRow(i, factor);

            // subtract row `i` from all other rows (scaled accordingly) to convert
            // all elements of column `i` to zero (skip row `i` of course)
            for (row = 0; row < dim; row += 1) {
                if (row !== i) {
                    factor = -lVals[row][i];
                    lMat.addScaledRow(row, i, factor);
                    rMat.addScaledRow(row, i, factor);
                }
            }
        }

        // this is the inverse matrix now
        return rMat;
    });

    factory.binary("MMULT", "mat:num", ["mat:num", "mat:num"], (matrix1, matrix2) => {

        // width of first matrix must match height of second matrix
        if (matrix1.cols() !== matrix2.rows()) { throw ErrorCode.VALUE; }

        // calculate all elements of the result matrix
        const vals1 = matrix1.raw();
        const vals2 = matrix2.raw();
        return new NumberMatrix(matrix1.rows(), matrix2.cols(), (row, col) => {
            let elem = 0;
            for (let i = 0, l = matrix1.cols(); i < l; i += 1) {
                elem += vals1[row][i] * vals2[i][col];
            }
            return elem;
        });
    });

    factory.unary("MUNIT", "mat:num", "val:int", {
        name: { ooxml: "_xlfn.MUNIT" },
        resolve: size => {
            if (size < 1) { throw ErrorCode.VALUE; }
            ensureMatrixDim(size, size);
            return NumberMatrix.identity(size);
        }
    });

    // bug 47280: forced matrix context also in cell formulas
    factory.unbound("SUMPRODUCT", "val:num", 1, 1, [ANY_MAT_FORCE], {
        category: "math", // additionally to "matrix"
        resolve(...operands: Operand[]) {
            return this.aggregateNumbersParallel(operands, 0, aggregateSumProduct, fun.identity, SUMPRODUCT_OPTIONS);
        }
    });

    factory.binary("SUMX2MY2", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], {
        category: "math", // additionally to "matrix"
        resolve(...operands) {
            return this.aggregateNumbersParallel(operands, 0, aggregateSumX2MY2, fun.identity, SUM_XY_OPTIONS);
        }
    });

    factory.binary("SUMX2PY2", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], {
        category: "math", // additionally to "matrix"
        resolve(...operands) {
            return this.aggregateNumbersParallel(operands, 0, aggregateSumX2PY2, fun.identity, SUM_XY_OPTIONS);
        }
    });

    factory.binary("SUMXMY2", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], {
        category: "math", // additionally to "matrix"
        resolve(...operands) {
            return this.aggregateNumbersParallel(operands, 0, aggregateSumXMY2, fun.identity, SUM_XY_OPTIONS);
        }
    });

    factory.unary("TRANSPOSE", "mat:any", "mat:any", matrix => matrix.transpose());
});
