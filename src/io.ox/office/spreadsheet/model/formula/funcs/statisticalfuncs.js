/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all statistical spreadsheet functions.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { is, math, fun, ary } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { add, mul, div, factorial, devSq } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import { CompareResult, compareNumbers, NumberMatrix, ensureMatrixDim } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import * as StatUtils from "@/io.ox/office/spreadsheet/model/formula/utils/statutils";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// constants ==================================================================

// standard options for numeric parameter iterators
const SKIP_MAT_SKIP_REF = { // id: CSS5
    valMode: "convert", // value operands: convert strings and booleans to numbers
    matMode: "skip", // matrix operands: skip strings and booleans
    refMode: "skip", // reference operands: skip strings and booleans
    emptyParam: true, // empty parameters count as zero
    multiRange: true, // accept multi-range references
    multiSheet: true  // accept multi-sheet references
};

// standard options for numeric parameter iterators
const RCONVERT_ALL_SKIP_EMPTY = { // id: RRR0
    valMode: "rconvert", // value operands: convert strings to numbers (but not booleans)
    matMode: "rconvert", // matrix operands: convert strings to numbers (but not booleans)
    refMode: "rconvert" // reference operands: convert strings to numbers (but not booleans)
    // emptyParam: false, // skip all empty parameters
    // multiRange: false, // do not accept multi-range references
    // multiSheet: false  // do not accept multi-sheet references
};

// options for FREQUENCY function iterators
const FREQUENCY_OPTIONS = { // id: SSS0
    valMode: "skip", // value operands: skip strings and booleans
    matMode: "skip", // matrix operands: skip strings and booleans
    refMode: "skip" // reference operands: skip strings and booleans
    // emptyParam: false, // skip all empty parameters
    // multiRange: false, // do not accept multi-range references
    // multiSheet: false  // do not accept multi-sheet references
};

// private functions ==========================================================

/**
 * Returns the counts of all numbers in the passed operand, that are less
 * than, equal to, and greater than the specified number.
 *
 * @param {FormulaContext} context
 *  The formula context used to iterate the numbers in the passed operand.
 *
 * @param {number} number
 *  The number to be compared with the numbers contained in the operand.
 *
 * @param {OperandIteratorSource} operand
 *  A function operand of any type supported by the iterator methods of the
 *  formula context.
 *
 * @param {CommonNumberIteratorOptions} options
 *  Optional parameters passed to the number iterator used internally.
 *
 * @returns {Object}
 *  A result object with the following properties:
 *  - {Number} "lt": The count of all numbers contained in the operand that
 *      are less than the number passed to this method.
 *  - {Number} "eq": The count of all numbers contained in the operand that
 *      are equal to the number passed to this method.
 *  - {Number} "gt": The count of all numbers contained in the operand that
 *      are greater than the number passed to this method.
 */
function getCompareCounts(context, number, operand, options) {
    return context.aggregateNumbers([operand], { lt: 0, eq: 0, gt: 0 }, (result, n) => {
        switch (compareNumbers(n, number)) {
            case CompareResult.LESS:    result.lt += 1; break;
            case CompareResult.EQUAL:   result.eq += 1; break;
            case CompareResult.GREATER: result.gt += 1; break;
        }
        return result;
    }, fun.identity, options);
}

// Euclidean norm of row index R starting in column index C;
// matrix A has count N columns.
function tGetColumnEuclideanNorm(matA, nR, nC, nN) {
    let norm = 0;
    for (let col = nC; col < nN; col += 1) {
        const elem = matA.get(nR, col);
        norm += elem * elem;
    }
    return Math.sqrt(norm);
}

/* Calculates a QR decomposition with Householder reflection.
 * For each NxK matrix A exists a decomposition A=Q*R with an orthogonal
 * NxN matrix Q and a NxK matrix R.
 * Q=H1*H2*...*Hk with Householder matrices H. Such a householder matrix can
 * be build from a vector u by H=I-(2/u'u)*(u u'). This vectors u are returned
 * in the columns of matrix A, overwriting the old content.
 * The matrix R has a quadric upper part KxK with values in the upper right
 * triangle and zeros in all other elements. Here the diagonal elements of R
 * are stored in the vector R and the other upper right elements in the upper
 * right of the matrix A.
 * The function returns false, if calculation breaks. But because of round-off
 * errors singularity is often not detected.
 */
function calculateQRdecomposition(matA, pVecR, nK, nN) {

    for (let col = 0; col < nK; col += 1) {
        // calculate vector u of the householder transformation
        const fScale = getColumnMaximumNorm(matA, col, col, nN);
        if (fScale === 0) {
            // A is singular
            throw ErrorCode.VALUE;
        }
        for (let row = col; row < nN; row += 1) {
            matA.set(row, col, matA.get(row, col) / fScale, col, row);
        }
        const fEuclid = getColumnEuclideanNorm(matA, col, col, nN);
        const fFactor = 1 / fEuclid / (fEuclid + Math.abs(matA.get(col, col)));
        const fSignum = getSign(matA.get(col, col));
        matA.set(col, col, matA.get(col, col) + fSignum * fEuclid);
        pVecR[col] = -fSignum * fScale * fEuclid;

        // apply Householder transformation to A
        for (let c = col + 1; c < nK; c += 1) {
            const fSum = getColumnSumProduct(matA, col, matA, c, col, nN);
            for (let row = col; row < nN; row += 1) {
                matA.set(row, c, matA.get(row, c) - fSum * fFactor * matA.get(row, col));
            }
        }
    }
}

// same with transposed matrix A, N is count of columns, K count of rows
function tCalculateQRdecomposition(matA, pVecR, nK, nN) {

    for (let row = 0; row < nK; row += 1) {
        // calculate vector u of the householder transformation
        const fScale = tGetColumnMaximumNorm(matA, row, row, nN);
        if (fScale === 0) {
            // A is singular
            throw ErrorCode.VALUE;
        }
        for (let col = row; col < nN; col += 1) {
            matA.set(row, col, matA.get(row, col) / fScale);
        }
        const fEuclid = tGetColumnEuclideanNorm(matA, row, row, nN);
        const fFactor = 1 / fEuclid / (fEuclid + Math.abs(matA.get(row, row)));
        const fSignum = getSign(matA.get(row, row));
        matA.set(row, row, matA.get(row, row) + fSignum * fEuclid);
        pVecR[row] = -fSignum * fScale * fEuclid;

        // apply Householder transformation to A
        for (let r = row + 1; r < nK; r += 1) {
            const fSum = tGetColumnSumProduct(matA, row, matA, r, row, nN);
            for (let col = row; col < nN; col += 1) {
                matA.set(r, col, matA.get(r, col) - fSum * fFactor * matA.get(row, col));
            }
        }
    }
}

/* Applies a Householder transformation to a column vector Y with is given as
 * Nx1 Matrix. The Vektor u, from which the Householder transformation is build,
 * is the column part in matrix A, with column index C, starting with row
 * index C. A is the result of the QR decomposition as obtained from
 * lcl_CaluclateQRdecomposition.
 */
function applyHouseholderTransformation(matA, nC, matY, nN) {

    const fDenominator = getColumnSumProduct(matA, nC, matA, nC, nC, nN);
    const fNumerator = getColumnSumProduct(matA, nC, matY, 0, nC, nN);
    const fFactor = 2 * (fNumerator / fDenominator);
    for (let row = nC; row < nN; row += 1) {
        matY.setByIndex(row, matY.getByIndex(row) - fFactor * matA.get(row, nC));
    }
}

// Same with transposed matrices A and Y.
function tApplyHouseholderTransformation(matA, nR, matY, nN) {

    const fDenominator = tGetColumnSumProduct(matA, nR, matA, nR, nR, nN);
    const fNumerator = tGetColumnSumProduct(matA, nR, matY, 0, nR, nN);
    const fFactor = 2 * (fNumerator / fDenominator);
    for (let col = nR; col < nN; col += 1) {
        matY.setByIndex(col, matY.getByIndex(col) - fFactor * matA.get(nR, col));
    }
}

/* Solve for X in R*X=S using back substitution. The solution X overwrites S.
 * Uses R from the result of the QR decomposition of a NxK matrix A.
 * S is a column vector given as matrix, with at least elements on index
 * 0 to K-1; elements on index>=K are ignored. Vector R must not have zero
 * elements, no check is done.
 */
function solveWithUpperRightTriangle(matA, pVecR, matS, nK, transposed) {

    for (let rowp1 = nK; rowp1 > 0; rowp1 -= 1) {
        const row = rowp1 - 1;
        let fSum = matS.get(row, 0);
        for (let col = rowp1; col < nK; col += 1) {
            const val = transposed ? matA.get(col, row) : matA.get(row, col);
            fSum -= val * matS.getByIndex(col);
        }
        matS.setByIndex(row, fSum / pVecR[row]);
    }
}

/* Solve for X in R' * X= T using forward substitution. The solution X
 * overwrites T. Uses R from the result of the QR decomposition of a NxK
 * matrix A. T is a column vectors given as matrix, with at least elements on
 * index 0 to K-1; elements on index>=K are ignored. Vector R must not have
 * zero elements, no check is done.
 */
function solveWithLowerLeftTriangle(matA, pVecR, matT, nK, transposed) {

    for (let row = 0; row < nK; row += 1) {
        let fSum = matT.get(row, 0);
        for (let col = 0; col < row; col += 1) {
            const val = transposed ? matA.get(row, col) : matA.get(col, row);
            fSum -= val * matT.getByIndex(col);
        }
        matT.setByIndex(row, fSum / pVecR[row]);
    }
}

/* Calculates Z = R * B
 * R is given in matrix A and vector VecR as obtained from the QR
 * decomposition in lcl_CalculateQRdecomposition. B and Z are column vectors
 * given as matrix with at least index 0 to K-1; elements on index>=K are
 * not used.
 */
function applyUpperRightTriangle(matA, pVecR, matB, matZ, nK, transposed) {

    for (let row = 0; row < nK; row += 1) {
        let fSum = pVecR[row] * matB.get(row, 0);
        for (let col = row + 1; col < nK; col += 1) {
            const val = transposed ? matA.get(col, row) : matA.get(row, col);
            fSum += val * matB.getByIndex(col);
        }
        matZ.setByIndex(row, fSum);
    }
}

function getMeanOverAll(matrix) {
    return matrix.reduce(0, add) / matrix.size();
}

function getSumProduct(matA, matB) {
    let sum = 0;
    for (let i = 0, size = matA.size(); i < size; i += 1) {
        sum += matA.getByIndex(i) * matB.getByIndex(i);
    }
    return sum;
}

function getSSresid(matX, matY, slope) {
    let sum = 0;
    for (let i = 0, size = matY.size(); i < size; i += 1) {
        const temp = matX.getByIndex(i) - slope * matY.getByIndex(i);
        sum += temp * temp;
    }
    return sum;
}

// Calculates means of the columns of matrix X. X is a RxC matrix;
// ResMat is a 1xC matrix (=row).
function calculateColumnMeans(pX, resMat, nC, nR) {
    for (let i = 0; i < nC; i += 1) {
        let fSum = 0;
        for (let r = 0; r < nR; r += 1) {
            fSum += pX.get(r, i);
        }
        resMat.setByIndex(i, fSum / nR);
    }
}

// Calculates means of the rows of matrix X. X is a RxC matrix;
// ResMat is a Rx1 matrix (=column).
function calculateRowMeans(pX, resMat, nC, nR) {
    for (let i = 0; i < nR; i += 1) {
        let fSum = 0;
        for (let c = 0; c < nC; c += 1) {
            fSum += pX.get(i, c);
        }
        resMat.setByIndex(i, fSum / nC);
    }
}

function calculateColumnsDelta(mat, columnMeans, nC, nR) {
    for (let i = 0; i < nC; i += 1) {
        for (let r = 0; r < nR; r += 1) {
            mat.set(r, i, mat.get(r, i) - columnMeans.getByIndex(i));
        }
    }
}

function calculateRowsDelta(mat, pRowMeans, nC, nR) {
    for (let k = 0; k < nR; k += 1) {
        for (let c = 0; c < nC; c += 1) {
            mat.set(k, c, mat.get(k, c) - pRowMeans.getByIndex(k));
        }
    }
}

// <A(Ra);B(Rb)> starting in column index C;
// Ra and Rb are indices of rows, matrices A and B have count N columns.
function tGetColumnSumProduct(matA, nRa, matB, nRb, nC, nN) {
    let fResult = 0;
    for (let col = nC; col < nN; col += 1) {
        fResult += matA.get(nRa, col) * matB.get(nRb, col);
    }
    return fResult;
}

//Special version for use within QR decomposition.
//<A(Ca);B(Cb)> starting in row index R;
//Ca and Cb are indices of columns, matrices A and B have count N rows.
function getColumnSumProduct(matA, nCa, matB, nCb, nR, nN) {
    let fResult = 0;
    for (let row = nR; row < nN; row += 1) {
        fResult += matA.get(row, nCa) * matB.get(row, nCb);
    }
    return fResult;
}

//Special version for use within QR decomposition.
//Maximum norm of column index C starting in row index R;
//matrix A has count N rows.
function getColumnMaximumNorm(matA, nC, nR, nN) {
    let fNorm = 0;
    for (let row = nR; row < nN; row += 1) {
        fNorm = Math.max(fNorm, Math.abs(matA.get(row, nC)));
    }
    return fNorm;
}

//Special version for use within QR decomposition.
//Euclidean norm of column index C starting in row index R;
//matrix A has count N rows.
function getColumnEuclideanNorm(matA, nC, nR, nN) {
    let fNorm = 0;
    for (let row = nR; row < nN; row += 1) {
        fNorm += matA.get(row, nC) * matA.get(row, nC);
    }
    return Math.sqrt(fNorm);
}

// no mathematical signum, but used to switch between adding and subtracting
function getSign(number) {
    return (number >= 0) ? 1 : -1;
}

//Maximum norm of row index R starting in col index C;
//matrix A has count N columns.
function tGetColumnMaximumNorm(matA, nR, nC, nN) {
    let fNorm = 0;
    for (let col = nC; col < nN; col += 1) {
        fNorm = Math.max(fNorm, Math.abs(matA.get(nR, col)));
    }
    return fNorm;
}

// Fill default values in matrix X, transform Y to log(Y) in case LOGEST|GROWTH,
// determine sizes of matrices X and Y, determine kind of regression, clone
// Y in case LOGEST|GROWTH, if constant.
function checkMatrix(log, matX, matY) {
    let nCase = null;
    const nCY = matY.cols();
    const nRY = matY.rows();
    let M = 0;
    let N = 0;

    const nCountY = nCY * nRY;

    matY.forEach(value => {
        if (!is.number(value)) {
            throw ErrorCode.NA;
        }
    });

    if (log) {
        matY.forEach((value, row, col) => {
            if (value < 0) {
                throw ErrorCode.NA;
            }
            matY.set(row, col, Math.log(value));
        });
    }

    if (matX) {
        const nCX = matX.cols();
        const nRX = matX.rows();

        matX.forEach(value => {
            if (!is.number(value)) {
                throw ErrorCode.NA;
            }
        });

        if (nCX === nCY && nRX === nRY) {
            nCase = 1; // simple regression
            M = 1;
            N = nCountY;
        } else if (nCY !== 1 && nRY !== 1) {
            throw ErrorCode.NA;
        } else if (nCY === 1) {
            if (nRX !== nRY) { throw ErrorCode.NA; }
            nCase = 2; // Y is column
            N = nRY;
            M = nCX;
        } else if (nCX !== nCY) {
            throw ErrorCode.NA;
        } else {
            nCase = 3; // Y is row
            N = nCY;
            M = nRX;
        }
    } else {
        matX = new NumberMatrix(nRY, nCY, 0);

        let i = 1;
        matX.forEach((_value, row, col) => {
            matX.set(row, col, i);
            i += 1;
        });
        nCase = 1;
        N = nCountY;
        M = 1;
    }

    return { nCase, M, N, matX, matY };
}

function calculateRGPRKP(knownY, knownX, constant, stats, _bRKP) {

    let i = 0;
    let fIntercept = 0;
    let fSSreg = 0;
    let fDegreesFreedom = 0;
    let fSSresid = 0;
    let fFstatistic = 0;
    let fRMSE = 0;
    let fSigmaSlope = 0;
    let fSigmaIntercept = 0;
    let col = 0;
    let row = 0;
    let fR2 = 0;
    let fPart = 0;
    let aVecR = null;
    let matZ = null;
    let means = null;
    let slopes = null;
    let bIsSingular = false;

    let matY = knownY;
    let matX = knownX;

    const bConstant = constant ?? true;

    //CheckMatrix(_bRKP,nCase,nCX,nCY,nRX,nRY, K,N,matX,matY)
    const checked = checkMatrix(_bRKP, matX, matY);
    // 1 = simple; 2 = multiple with Y as column; 3 = multiple with Y as row
    const nCase = checked.nCase;

    const K = checked.M, N = checked.N; // K=number of variables X, N=number of data samples
    matX = checked.matX;
    matY = checked.matY;

    // Enough data samples?
    /* original code !!
        if ((bConstant && (N < K + 1)) || (!bConstant && (N < K)) || (N < 1) || (K < 1)) {
    */
    // new code to be excel conform
    if ((N < K) || (N < 1) || (K < 1)) {
        throw ErrorCode.NA;
    }

    const matWidth = stats ? 5 : 1;
    const resMat = new NumberMatrix(matWidth, K + 1, 0);

    // Fill unused cells in resMat; order (row,column)
    if (stats) {
        resMat.fill(2, 2, 4, K, ErrorCode.NA);
    }

    // Uses sum(x-MeanX)^2 and not [sum x^2]-N * MeanX^2 in case bConstant.
    // Clone constant matrices, so that Mat = Mat - Mean is possible.
    let fMeanY = 0;
    if (bConstant) {
        // DeltaY is possible here; DeltaX depends on nCase, so later
        fMeanY = getMeanOverAll(matY);
        matY.transform(value => value - fMeanY);
    }

    if (nCase === 1) {
        // calculate simple regression
        let fMeanX = 0;
        if (bConstant) {
            // Mat = Mat - Mean
            fMeanX = getMeanOverAll(matX);
            matX.transform(value => value - fMeanX);
        }
        const fSumXY = getSumProduct(matX, matY);
        const fSumX2 = getSumProduct(matX, matX);

        // new code to be excel conform
        let fSlope = fSumXY / fSumX2;
        if (Number.isNaN(fSlope)) { fSlope = 0; }

        fIntercept = 0;
        if (bConstant) {
            fIntercept = fMeanY - fSlope * fMeanX;
        }
        resMat.set(0, 1, _bRKP ? Math.exp(fIntercept) : fIntercept);
        resMat.set(0, 0, _bRKP ? Math.exp(fSlope) : fSlope);

        if (stats) {
            fSSreg = fSlope * fSlope * fSumX2;
            resMat.set(4, 0, fSSreg);

            fDegreesFreedom = (bConstant) ? N - 2 : N - 1;
            resMat.set(3, 1, fDegreesFreedom);

            fSSresid  = getSSresid(matX, matY, fSlope);
            resMat.set(4, 1, fSSresid);

            if (fDegreesFreedom === 0 || fSSresid === 0 || fSSreg === 0) {
                // exact fit; test SSreg too, because SSresid might be
                // unequal zero due to round of errors
                resMat.set(4, 1, 0); // SSresid
                resMat.set(3, 0, ErrorCode.NA); // F
                resMat.set(2, 1, 0, 1, 2); // RMSE
                resMat.set(1, 0, 0, 0, 1); // SigmaSlope
                if (bConstant) {
                    resMat.set(1, 1, 0); //SigmaIntercept
                } else {
                    resMat.set(1, 1, ErrorCode.NA);
                }
                resMat.set(2, 0, 1); // R^2
            } else {
                fFstatistic = (fSSreg / K) / (fSSresid / fDegreesFreedom);
                resMat.set(3, 0, fFstatistic);

                // standard error of estimate
                fRMSE = Math.sqrt(fSSresid / fDegreesFreedom);
                resMat.set(2, 1, fRMSE);

                fSigmaSlope = fRMSE / Math.sqrt(fSumX2);
                resMat.set(1, 0, fSigmaSlope);

                if (bConstant) {
                    fSigmaIntercept = fRMSE * Math.sqrt(fMeanX * fMeanX / fSumX2 + 1 / (N));
                    resMat.set(1, 1, fSigmaIntercept);
                } else {
                    resMat.set(1, 1, ErrorCode.NA);
                }

                fR2 = fSSreg / (fSSreg + fSSresid);
                resMat.set(2, 0, fR2);
            }
        }
        return resMat;
    } else { // calculate multiple regression;

        // Uses a QR decomposition X = QR. The solution B = (X'X)^(-1) * X' * Y
        // becomes B = R^(-1) * Q' * Y
        if (nCase === 2) { // Y is column

            aVecR = ary.fill(N, 0); // for QR decomposition
            // Enough memory for needed matrices?
            means = new NumberMatrix(1, K, 0); // mean of each column
            // for Q' * Y , inter alia
            if (stats) {
                matZ = matY.clone(); // Y is used in statistic, keep it
            } else {
                matZ = matY; // Y can be overwritten
            }
            slopes = new NumberMatrix(K, 1, 0); // from b1 to bK

            if (bConstant) {
                calculateColumnMeans(matX, means, K, N);
                calculateColumnsDelta(matX, means, K, N);
            }
            calculateQRdecomposition(matX, aVecR, K, N);

            // Later on we will divide by elements of aVecR, so make sure
            // that they aren't zero.
            bIsSingular = false;
            for (row = 0; row < K && !bIsSingular; row++) {
                bIsSingular = bIsSingular || aVecR[row] === 0;
            }
            if (bIsSingular) { throw ErrorCode.VALUE; }
            // Z = Q' Y;
            for (col = 0; col < K; col++) {
                applyHouseholderTransformation(matX, col, matZ, N);
            }
            // B = R^(-1) * Q' * Y <=> B = R^(-1) * Z <=> R * B = Z
            // result Z should have zeros for index>=K; if not, ignore values
            for (i = 0; i < K; i++) {
                slopes.setByIndex(i, matZ.getByIndex(i));
            }
            solveWithUpperRightTriangle(matX, aVecR, slopes, K, false);
            fIntercept = 0;
            if (bConstant) {
                fIntercept = fMeanY - getSumProduct(means, slopes);
            }

            // Fill first line in result matrix
            resMat.set(0, K, _bRKP ? Math.exp(fIntercept) : fIntercept);
            for (i = 0; i < K; i++) {
                resMat.set(0, K - 1 - i, _bRKP ? Math.exp(slopes.getByIndex(i)) : slopes.getByIndex(i));
            }

            if (stats) {
                fSSreg = 0;
                fSSresid  = 0;
                // re-use memory of Z;
                matZ.fill(0, 0, N - 1, 0, 0);
                // Z = R * Slopes
                applyUpperRightTriangle(matX, aVecR, slopes, matZ, K, false);
                // Z = Q * Z, that is Q * R * Slopes = X * Slopes
                for (let colp1 = K; colp1 > 0; colp1 -= 1) {
                    applyHouseholderTransformation(matX, colp1 - 1, matZ, N);
                }
                fSSreg = getSumProduct(matZ, matZ);
                // re-use Y for residuals, Y = Y-Z
                for (i = 0; i < N; i++) {
                    matY.setByIndex(i, matY.getByIndex(i) - matZ.getByIndex(i));
                }
                fSSresid = getSumProduct(matY, matY);
                resMat.set(4, 0, fSSreg);
                resMat.set(4, 1, fSSresid);

                fDegreesFreedom = ((bConstant) ? N - K - 1 : N - K);
                resMat.set(3, 1, fDegreesFreedom);

                if (fDegreesFreedom === 0 || fSSresid === 0 || fSSreg === 0) {
                    // exact fit; incl. observed values Y are identical
                    resMat.set(4, 1, 0); // SSresid
                    // F = (SSreg/K) / (SSresid/df) = #DIV/0!
                    resMat.set(3, 0, ErrorCode.NA); // F
                    // RMSE = Math.sqrt(SSresid / df) = Math.sqrt(0 / df) = 0
                    resMat.set(2, 1, 0); // RMSE
                    // SigmaSlope[i] = RMSE * Math.sqrt(matrix[i,i]) = 0 * Math.sqrt(...) = 0
                    for (i = 0; i < K; i++) {
                        resMat.set(1, K - 1 - i, 0);
                    }
                    // SigmaIntercept = RMSE * Math.sqrt(...) = 0
                    if (bConstant) {
                        resMat.set(1, K, 0); //SigmaIntercept
                    } else {
                        resMat.set(1, K, ErrorCode.NA);
                    }
                    //  R^2 = SSreg / (SSreg + SSresid) = 1
                    resMat.set(2, 0, 1); // R^2
                } else {
                    fFstatistic = (fSSreg /  K) / (fSSresid / fDegreesFreedom);
                    resMat.set(3, 0, fFstatistic);

                    // standard error of estimate = root mean SSE
                    fRMSE = Math.sqrt(fSSresid / fDegreesFreedom);
                    resMat.set(2, 1, fRMSE);

                    // standard error of slopes
                    // = RMSE * Math.sqrt(diagonal element of (R' R)^(-1) )
                    // standard error of intercept
                    // = RMSE * Math.sqrt( Xmean * (R' R)^(-1) * Xmean' + 1/N)
                    // (R' R)^(-1) = R^(-1) * (R')^(-1). Do not calculate it as
                    // a whole matrix, but iterate over unit vectors.
                    fSigmaIntercept = 0;

                    for (i = 0; i < K; i++) {
                        //re-use memory of MatZ
                        matZ.fill(0, 0, K - 1, 0, 0); // Z = unit vector e
                        matZ.setByIndex(i, 1);
                        //Solve R' * Z = e
                        solveWithLowerLeftTriangle(matX, aVecR, matZ, K, false);
                        // Solve R * Znew = Zold
                        solveWithUpperRightTriangle(matX, aVecR, matZ, K, false);
                        // now Z is column col in (R' R)^(-1)
                        fSigmaSlope = fRMSE * Math.sqrt(matZ.getByIndex(i));
                        resMat.set(1, K - 1 - i, fSigmaSlope);
                        // (R' R) ^(-1) is symmetric
                        if (bConstant) {
                            fPart = getSumProduct(means, matZ);
                            fSigmaIntercept += fPart * means.getByIndex(i);
                        }
                    }
                    if (bConstant) {
                        fSigmaIntercept = fRMSE * Math.sqrt(fSigmaIntercept + 1 / N);
                        resMat.set(1, K, fSigmaIntercept);
                    } else {
                        resMat.set(1, K, ErrorCode.NA);
                    }

                    fR2 = fSSreg / (fSSreg + fSSresid);
                    resMat.set(2, 0, fR2);
                }
            }
            return resMat;
        } else { // nCase === 3, Y is row, all matrices are transposed
            aVecR = ary.fill(N, 0); // for QR decomposition
            // Enough memory for needed matrices?
            means = new NumberMatrix(K, 1, 0); // mean of each row
            // for Q' * Y , inter alia
            if (stats) {
                matZ = matY.clone(); // Y is used in statistic, keep it
            } else {
                matZ = matY; // Y can be overwritten
            }
            slopes = new NumberMatrix(1, K, 0); // from b1 to bK
            if (bConstant) {
                calculateRowMeans(matX, means, N, K);
                calculateRowsDelta(matX, means, N, K);
            }

            tCalculateQRdecomposition(matX, aVecR, K, N);

            // Later on we will divide by elements of aVecR, so make sure
            // that they aren't zero.
            bIsSingular = false;
            for (row = 0; row < K && !bIsSingular; row++) {
                bIsSingular = bIsSingular || aVecR[row] === 0;
            }
            if (bIsSingular) { throw ErrorCode.VALUE; }
            // Z = Q' Y
            for (row = 0; row < K; row++) {
                tApplyHouseholderTransformation(matX, row, matZ, N);
            }
            // B = R^(-1) * Q' * Y <=> B = R^(-1) * Z <=> R * B = Z
            // result Z should have zeros for index>=K; if not, ignore values
            for (col = 0; col < K; col++) {
                slopes.set(0, col, matZ.get(0, col));
            }
            solveWithUpperRightTriangle(matX, aVecR, slopes, K, true);
            fIntercept = 0;
            if (bConstant) {
                fIntercept = fMeanY - getSumProduct(means, slopes);
            }
            // Fill first line in result matrix
            resMat.set(0, K, _bRKP ? Math.exp(fIntercept) : fIntercept);
            for (i = 0; i < K; i++) {
                resMat.set(0, K - 1 - i, _bRKP ? Math.exp(slopes.getByIndex(i)) : slopes.getByIndex(i));
            }

            if (stats) {
                fSSreg = 0;
                fSSresid  = 0;

                matZ.fill(0, 0, 0, N - 1, 0); // re-use memory of Z;

                // Z = R * Slopes
                applyUpperRightTriangle(matX, aVecR, slopes, matZ, K, true);
                // Z = Q * Z, that is Q * R * Slopes = X * Slopes
                for (let rowp1 = K; rowp1 > 0; rowp1 -= 1) {
                    tApplyHouseholderTransformation(matX, rowp1 - 1, matZ, N);
                }
                fSSreg = getSumProduct(matZ, matZ);
                // re-use Y for residuals, Y = Y-Z
                for (i = 0; i < N; i++) {
                    matY.setByIndex(i, matY.getByIndex(i) - matZ.getByIndex(i));
                }
                fSSresid = getSumProduct(matY, matY);
                resMat.set(4, 0, fSSreg);
                resMat.set(4, 1, fSSresid);

                fDegreesFreedom = ((bConstant) ? N - K - 1 : N - K);
                resMat.set(3, 1, fDegreesFreedom);

                if (fDegreesFreedom === 0 || fSSresid === 0 || fSSreg === 0) {
                    // exact fit; incl. case observed values Y are identical
                    resMat.set(4, 1, 0); // SSresid
                    // F = (SSreg/K) / (SSresid/df) = #DIV/0!
                    resMat.set(3, 0, ErrorCode.NA); // F
                    // RMSE = Math.sqrt(SSresid / df) = Math.sqrt(0 / df) = 0
                    resMat.set(2, 1, 0); // RMSE
                    // SigmaSlope[i] = RMSE * Math.sqrt(matrix[i,i]) = 0 * Math.sqrt(...) = 0
                    for (i = 0; i < K; i++) {
                        resMat.set(1, K - 1 - i, 0);
                    }

                    // SigmaIntercept = RMSE * Math.sqrt(...) = 0
                    if (bConstant) {
                        resMat.set(1, K, 0); //SigmaIntercept
                    } else {
                        resMat.set(1, K, ErrorCode.NA);
                    }
                    //  R^2 = SSreg / (SSreg + SSresid) = 1
                    resMat.set(2, 0, 1); // R^2
                } else {
                    fFstatistic = (fSSreg /  K) / (fSSresid / fDegreesFreedom);
                    resMat.set(3, 0, fFstatistic);

                    // standard error of estimate = root mean SSE
                    fRMSE = Math.sqrt(fSSresid / fDegreesFreedom);
                    resMat.set(2, 1, fRMSE);

                    // standard error of slopes
                    // = RMSE * Math.sqrt(diagonal element of (R' R)^(-1) )
                    // standard error of intercept
                    // = RMSE * Math.sqrt( Xmean * (R' R)^(-1) * Xmean' + 1/N)
                    // (R' R)^(-1) = R^(-1) * (R')^(-1). Do not calculate it as
                    // a whole matrix, but iterate over unit vectors.
                    // (R' R) ^(-1) is symmetric
                    fSigmaIntercept = 0;

                    for (row = 0; row < K; row++) {
                        //re-use memory of MatZ
                        matZ.fill(0, 0, 0, K - 1, 0); // Z = unit vector e
                        matZ.setByIndex(row, 1);
                        //Solve R' * Z = e
                        solveWithLowerLeftTriangle(matX, aVecR, matZ, K, true);
                        // Solve R * Znew = Zold
                        solveWithUpperRightTriangle(matX, aVecR, matZ, K, true);
                        // now Z is column col in (R' R)^(-1)
                        fSigmaSlope = fRMSE * Math.sqrt(matZ.getByIndex(row));
                        resMat.set(1, K - 1 - row, fSigmaSlope);
                        if (bConstant) {
                            fPart = getSumProduct(means, matZ);
                            fSigmaIntercept += fPart * means.getByIndex(row);
                        }
                    }
                    if (bConstant) {
                        fSigmaIntercept = fRMSE * Math.sqrt(fSigmaIntercept + 1 /  N);
                        resMat.set(1, K, fSigmaIntercept);
                    } else {
                        resMat.set(1, K, ErrorCode.NA);
                    }

                    fR2 = fSSreg / (fSSreg + fSSresid);
                    resMat.set(2, 0, fR2);
                }
            }
            return resMat;
        }
    }
}

// exports ====================================================================

export default declareFunctionSpecModule("statistical", factory => {

    const ANY_MAT_PASS = factory.param("any", { mat: "pass" });
    const ANY_MAT_FORCE = factory.param("any", { mat: "force" });

    factory.unbound("AVEDEV", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {

        // default result (no numbers found in parameters) is the #NUM! error code (not #DIV/0!)
        function finalize(numbers, sum) {
            if (numbers.length === 0) { throw ErrorCode.NUM; }
            const mean = sum / numbers.length;
            return numbers.reduce((result, number) => result + Math.abs(number - mean), 0) / numbers.length;
        }

        // empty parameters count as zero: AVEDEV(1,) = 0.5
        return this.aggregateNumbersAsArray(args, finalize, SKIP_MAT_SKIP_REF);
    });

    factory.unbound("AVERAGE", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupAverage(this, args);
    });

    factory.unbound("AVERAGEA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupAverage(this, args, { matMode: "zero", refMode: "zero" });
    });

    factory.ternary2("AVERAGEIF", "val:num", ["ref:val", "val:any", "ref:val"], function () {
        return this.aggregateFilteredCells(false, 0, add, div);
    });

    factory.unbound("AVERAGEIFS", "val:num", 3, 2, ["ref:val", "ref:val", "val:any"], function () {
        return this.aggregateFilteredCells(true, 0, add, div);
    });

    factory.fixed("BETA.DIST", "val:num", 4, 6, ["val:num", "val:num", "val:num", "val:bool", "val:num", "val:num"], {
        name: { ooxml: "_xlfn.BETA.DIST", odf: "COM.MICROSOFT.BETA.DIST" },
        resolve(x, alpha, beta, cumulative, a = 0, b = 1) {
            if ((alpha <= 0) || (beta <= 0)) { throw ErrorCode.NUM; }
            const scale = b - a;
            if (this.ODF) {
                if (cumulative) {
                    if (x < a) { return 0; }
                    if (x > b) { return 1; }
                } else if (x < a || x > b) {
                    return 0;
                }
                if (scale <= 0) { throw ErrorCode.NUM; }
            } else if (x < a || x > b || a === b) { throw ErrorCode.NUM; }

            x = (x - a) / scale; // convert to standard form
            return cumulative ?
                StatUtils.betaDist(x, alpha, beta) :
                StatUtils.betaDistPDF(x, alpha, beta) / scale;
        }
    });

    factory.fixed("BETA.INV", "val:num", 3, 5, ["val:num", "val:num", "val:num", "val:num", "val:num", "val:num"], {
        name: { ooxml: "_xlfn.BETA.INV", odf: "COM.MICROSOFT.BETA.INV" },
        resolve: StatUtils.betaInv
    });

    factory.fixed("BETADIST", "val:num", 3, { ooxml: 5, odf: 6 }, ["val:num", "val:num", "val:num", "val:num", "val:num", "val:bool"], {
        resolve(x, alpha, beta, a = 0, b = 1, cumulative = false) {
            if ((alpha <= 0) || (beta <= 0)) { throw ErrorCode.NUM; }
            const scale = b - a;
            if (this.ODF) {
                if (x < a) { return 0; }
                if (x > b) { return 1; }

                if (scale <= 0) { throw ErrorCode.NUM; }
            } else if (x < a || x > b || a === b) { throw ErrorCode.NUM; }

            x = (x - a) / scale; // convert to standard form
            return cumulative ?
                StatUtils.betaDist(x, alpha, beta) :
                StatUtils.betaDistPDF(x, alpha, beta) / scale;
        }
    });

    factory.fixed("BETAINV", "val:num", 3, 5, ["val:num", "val:num", "val:num", "val:num", "val:num", "val:num"], "BETA.INV");

    factory.fixed("BINOM.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.BINOM.DIST", odf: "COM.MICRSOFT.BINOM.DIST" },
        resolve: StatUtils.getBinomDist
    });

    factory.fixed("BINOM.DIST.RANGE", "val:num", 3, 4, ["val:num", "val:num", "val:num", "val:num"], {
        // AOO 4.4 writes "B" (https://bz.apache.org/ooo/show_bug.cgi?id=126519)
        name: { ooxml: "_xlfn.BINOM.DIST.RANGE", odf: ["BINOM.DIST.RANGE", "B"] },
        resolve: StatUtils.binomDistRange
    });

    factory.ternaryNum("BINOM.INV", {
        name: { ooxml: "_xlfn.BINOM.INV", odf: "COM.MICRSOFT.BINOM.INV" },
        resolve: StatUtils.binomInv
    });

    factory.fixed("BINOMDIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], "BINOM.DIST");

    factory.binaryNum("CHIDIST", {
        name: { odf: "LEGACY.CHIDIST" },
        resolve: "CHISQ.DIST.RT"
    });

    factory.binaryNum("CHIINV", {
        name: { odf: "LEGACY.CHIINV" },
        resolve: "CHISQ.INV.RT"
    });

    factory.ternary("CHISQ.DIST", "val:num", ["val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.CHISQ.DIST", odf: "COM.MICROSOFT.CHISQ.DIST" },
        resolve: StatUtils.chiSqDist
    });

    factory.binaryNum("CHISQ.DIST.RT", {
        name: { ooxml: "_xlfn.CHISQ.DIST.RT", odf: "COM.MICROSOFT.CHISQ.DIST.RT" },
        resolve: StatUtils.getChiDist
    });

    factory.binaryNum("CHISQ.INV", {
        name: { ooxml: "_xlfn.CHISQ.INV", odf: "COM.MICROSOFT.CHISQ.INV" },
        resolve: StatUtils.chiSqInv
    });

    factory.binaryNum("CHISQ.INV.RT", {
        name: { ooxml: "_xlfn.CHISQ.INV.RT", odf: "COM.MICROSOFT.CHISQ.INV.RT" },
        resolve: StatUtils.chiInv
    });

    factory.binaryNum("CHISQ.TEST", {
        name: { ooxml: "_xlfn.CHISQ.TEST", odf: "COM.MICROSOFT.CHISQ.TEST" },
        resolve: StatUtils.chiSqTest
    });

    factory.ternary2("CHISQDIST", "val:num", ["val:num", "val:num", "val:bool"], {
        name: { ooxml: null },
        resolve: StatUtils.chiSqDist
    });

    factory.binaryNum("CHISQINV", {
        name: { ooxml: null },
        resolve: "CHISQ.INV"
    });

    factory.binary("CHITEST", "val:num", ["mat:num", "mat:num"], {
        name: { odf: "LEGACY.CHITEST" },
        resolve: "CHISQ.TEST"
    });

    factory.ternaryNum("CONFIDENCE", "CONFIDENCE.NORM");

    factory.ternaryNum("CONFIDENCE.NORM", {
        name: { ooxml: "_xlfn.CONFIDENCE.NORM", odf: "COM.MICROSOFT.CONFIDENCE.NORM" },
        resolve: StatUtils.confidence
    });

    factory.ternaryNum("CONFIDENCE.T", {
        name: { ooxml: "_xlfn.CONFIDENCE.T", odf: "COM.MICROSOFT.CONFIDENCE.T" },
        resolve: StatUtils.confidenceT
    });

    factory.binary("CORREL", "val:num", [ANY_MAT_PASS, ANY_MAT_PASS], "PEARSON");

    factory.unbound("COUNT", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupCount(this, args);
    });

    factory.unbound("COUNTA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupCountA(this, args);
    });

    // single range only (throws #VALUE! for complex references)
    factory.unary("COUNTBLANK", "val:num", "ref:val", function (range) {
        // empty strings (formula results) are counted as blank in Excel
        return this.countBlankCells(range, { emptyStr: this.OOXML });
    });

    // single range only (throws #VALUE! for complex reference)
    factory.binary("COUNTIF", "val:num", ["ref:val", "val:any"], function () {
        return this.countFilteredCells();
    });

    // single ranges only (throws #VALUE! for complex references)
    factory.unbound("COUNTIFS", "val:num", 2, 2, ["ref:val", "val:any"], function () {
        return this.countFilteredCells();
    });

    factory.binary("COVAR", "val:num", [ANY_MAT_PASS, ANY_MAT_PASS], "COVARIANCE.P");

    factory.binary("COVARIANCE.P", "val:num", [ANY_MAT_PASS, ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.COVARIANCE.P", odf: "COM.MICROSOFT.COVARIANCE.P" },
        resolve(valuesY, valuesX) {
            const data = StatUtils.getForecastData(this, valuesY, valuesX);
            return data.sumDYX / data.count;
        }
    });

    factory.binary("COVARIANCE.S", "val:num", [ANY_MAT_PASS, ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.COVARIANCE.S", odf: "COM.MICROSOFT.COVARIANCE.S" },
        resolve(valuesY, valuesX) {
            const data = StatUtils.getForecastData(this, valuesY, valuesX);
            return div(data.sumDYX, data.count - 1);
        }
    });

    factory.ternaryNum("CRITBINOM", "BINOM.INV");

    factory.unbound("DEVSQ", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        // default result (no numbers found in parameters) is the #NUM! error code (not #DIV/0!)
        const finalize = (numbers, sum, count) => count ? devSq(numbers, sum) : ErrorCode.NUM.throw();
        // empty parameters count as zero: DEVSQ(1,) = 0.5
        return this.aggregateNumbersAsArray(args, finalize, SKIP_MAT_SKIP_REF);
    });

    factory.ternary("EXPON.DIST", "val:num", ["val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.EXPON.DIST", odf: "COM.MICROSOFT.EXPON.DIST" },
        resolve: StatUtils.exponDist
    });

    factory.ternary("EXPONDIST", "val:num", ["val:num", "val:num", "val:bool"], "EXPON.DIST");

    factory.fixed("F.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        // accept "COM.MICROSOFT.F.DIST" in ODF (http://opengrok.libreoffice.org/xref/core/sc/source/core/tool/compiler.cxx#2628)
        name: { ooxml: "_xlfn.F.DIST", odf: ["FDIST", "COM.MICROSOFT.F.DIST"] },
        resolve: StatUtils.fDistLeftTail
    });

    factory.ternaryNum("F.DIST.RT", {
        name: { ooxml: "_xlfn.F.DIST.RT", odf: "COM.MICROSOFT.F.DIST.RT" },
        resolve: StatUtils.fDistRightTail
    });

    factory.ternaryNum("F.INV", {
        // LO 5.0 writes "COM.MICROSOFT.F.INV" (https://bugs.documentfoundation.org/show_bug.cgi?id=94214)
        name: { ooxml: "_xlfn.F.INV", odf: ["FINV", "COM.MICROSOFT.F.INV"] },
        resolve: StatUtils.fInvLeftTail
    });

    factory.ternaryNum("F.INV.RT", {
        name: { ooxml: "_xlfn.F.INV.RT", odf: "COM.MICROSOFT.F.INV.RT" },
        resolve: StatUtils.fInv
    });

    factory.binary("F.TEST", "val:num", ["mat:num", "mat:num"], {
        name: { ooxml: "_xlfn.F.TEST", odf: "COM.MICROSOFT.F.TEST" },
        resolve: StatUtils.fTest
    });

    factory.ternaryNum("FDIST", {
        name: { odf: "LEGACY.FDIST" },
        resolve: "F.DIST.RT"
    });

    factory.ternaryNum("FINV", {
        name: { odf: "LEGACY.FINV" },
        resolve: "F.INV.RT"
    });

    factory.unaryNum("FISHER", number => {
        if (Math.abs(number) >= 1) { throw ErrorCode.NUM; }
        return Math.atanh(number);
    });

    factory.unaryNum("FISHERINV", "TANH");

    factory.ternary("FORECAST", "val:num", ["val:num", ANY_MAT_FORCE, ANY_MAT_FORCE], "FORECAST.LINEAR");

    factory.fixed("FORECAST.ETS", "val:num", 3, 6, ["val:date", "any"], {
        name: { ooxml: "_xlfn.FORECAST.ETS", odf: null },
        hidden: true
    });

    factory.fixed("FORECAST.ETS.CONFINT", "val:num", 3, 7, ["val:date", "any"], {
        name: { ooxml: "_xlfn.FORECAST.ETS.CONFINT", odf: null },
        hidden: true
    });

    factory.fixed("FORECAST.ETS.SEASONALITY", "val:num", 2, 4, ["any"], {
        name: { ooxml: "_xlfn.FORECAST.ETS.SEASONALITY", odf: null },
        hidden: true
    });

    factory.fixed("FORECAST.ETS.STAT", "val:num", 3, 6, ["any"], {
        name: { ooxml: "_xlfn.FORECAST.ETS.STAT", odf: null },
        hidden: true
    });

    factory.ternary("FORECAST.LINEAR", "val:num", ["val:num", ANY_MAT_FORCE, ANY_MAT_FORCE], {
        name: { ooxml: "_xlfn.FORECAST.LINEAR", odf: null },
        resolve(x, valuesY, valuesX) {
            return StatUtils.getForecastY(this, x, valuesY, valuesX);
        }
    });

    factory.binary("FREQUENCY", "mat:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], function (data, bins) {

        // all numbers of the "bins" parameter (early exit if the result becomes too large)
        bins = this.getNumbersAsArray(bins, FREQUENCY_OPTIONS);
        ensureMatrixDim(bins.length + 1, 1);

        // the bins sorted by value, with their original array index (_.sortBy keeps indexes stable)
        bins = ary.sortBy(bins.map((max, index) => ({ max, index })), el => el.max);

        // all numbers in the source data range (sorted for binary search per bin)
        const numbers = this.getNumbersAsArray(data, FREQUENCY_OPTIONS).sort(math.compare);
        // the result matrix (last element is the count of numbers greater than all bins)
        const matrix = new NumberMatrix(bins.length + 1, 1, 0);
        // element index in the "numbers" array from last iteration
        let lastIndex = 0;

        // count the numbers in the source data per bin
        for (const bin of bins) {

            // find array index of the first number greater than the bin limit
            const index = ary.fastFindFirstIndex(numbers, number => bin.max < number, lastIndex);

            // insert the result at the correct position in the matrix
            matrix.set(bin.index, 0, index - lastIndex);

            // start next iteration from current array index
            lastIndex = index;
        }

        // count of all numbers greater than the largest bin
        return matrix.set(bins.length, 0, numbers.length - lastIndex);
    });

    factory.binary("FTEST", "val:num", ["mat:num", "mat:num"], "F.TEST");

    factory.unaryNum("GAMMA", {
        name: { ooxml: "_xlfn.GAMMA" },
        resolve(x) {
            if ((x <= 0) && (x === Math.floor(x))) { throw ErrorCode.NUM; }
            return StatUtils.getGamma(x);
        }
    });

    factory.fixed("GAMMA.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.GAMMA.DIST", odf: "COM.MICROSOFT.GAMMA.DIST" },
        resolve: StatUtils.gammaDist
    });

    factory.ternaryNum("GAMMA.INV", {
        name: { ooxml: "_xlfn.GAMMA.INV", odf: "COM.MICROSOFT.GAMMA.INV" },
        resolve: StatUtils.gammaInv
    });

    factory.fixed("GAMMADIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], "GAMMA.DIST");

    factory.ternaryNum("GAMMAINV", "GAMMA.INV");

    factory.unaryNum("GAMMALN", "GAMMALN.PRECISE");

    factory.unaryNum("GAMMALN.PRECISE", {
        name: { ooxml: "_xlfn.GAMMALN.PRECISE", odf: "COM.MICROSOFT.GAMMALN.PRECISE" },
        resolve(x) {
            if (x <= 0) { throw ErrorCode.NUM; }
            return StatUtils.getLogGamma(x);
        }
    });

    factory.unaryNum("GAUSS", {
        name: { ooxml: "_xlfn.GAUSS" },
        resolve: StatUtils.gauss
    });

    factory.unbound("GEOMEAN", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {

        // default result (no numbers found in parameters) is the #NUM! error code
        // a single 0 in the parameters results in the #NUM! error code
        function finalize(result, count) {
            if ((result === 0) || (count === 0)) { throw ErrorCode.NUM; }
            return Math.pow(result, 1 / count);
        }

        // empty parameters count as zero: GEOMEAN(1,) = #NUM!
        return this.aggregateNumbers(args, 1, mul, finalize, SKIP_MAT_SKIP_REF);
    });

    factory.fixed("GROWTH", "mat:num", 1, 4, [ANY_MAT_FORCE, ANY_MAT_FORCE, ANY_MAT_FORCE, "val:bool"], {
        resolve(knownY, knownX, newX, constant) {
            return StatUtils.calculateTrendGrowth(this, knownY, knownX, newX, constant, true);
        }
    });

    factory.unbound("HARMEAN", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {

        // adds reciprocal of the passed number to the intermediate result
        function aggregate(result, number) {
            // a single 0 in the parameters results in the #NUM! error code
            if (number === 0) { throw ErrorCode.NUM; }
            return result + 1 / number;
        }

        // default result (no numbers found in parameters) is the #N/A error code
        function finalize(result, count) {
            if (count === 0) { throw ErrorCode.NA; }
            return count / result;
        }

        // empty parameters count as zero: GEOMEAN(1,) = #NUM!
        return this.aggregateNumbers(args, 0, aggregate, finalize, SKIP_MAT_SKIP_REF);
    });

    factory.fixed("HYPGEOM.DIST", "val:num", 5, 5, ["val:int", "val:int", "val:int", "val:int", "val:bool"], {
        name: { ooxml: "_xlfn.HYPGEOM.DIST", odf: "COM.MICROSOFT.HYPGEOM.DIST" },
        resolve(x, n, M, N, isCumulative) {

            if ((x < 0) || (n < x) || (M < x) || (N < n) || (N < M) || (x < n - N + M)) {
                throw ErrorCode.NUM;
            }

            if (isCumulative) {
                let val = 0;
                for (let i = 0; i <= x; i += 1) {
                    val += StatUtils.getHypGeomDist(i, n, M, N);
                }
                return val;
            }

            return StatUtils.getHypGeomDist(x, n, M, N);
        }
    });

    factory.fixed("HYPGEOMDIST", "val:num", 4, 4, ["val:int", "val:int", "val:int", "val:int"], StatUtils.getHypGeomDist);

    factory.binary("INTERCEPT", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], function (valuesY, valuesX) {
        return StatUtils.getForecastY(this, 0, valuesY, valuesX);
    });

    factory.unbound("KURT", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {

        function finalize(numbers, sum, count) {

            if (!count) { throw ErrorCode.DIV0; }

            const mean = sum / count;
            const vSum = numbers.reduce((vSum, num) => vSum + (num - mean) * (num - mean), 0);

            const stdDev = Math.sqrt(vSum / (count - 1));
            if (stdDev === 0) { throw ErrorCode.DIV0; }

            let dx = 0, xpower4 = 0;
            for (const number of numbers) {
                dx = (number - mean) / stdDev;
                xpower4 += Math.pow(dx, 4);
            }

            const duplicatedPart = (count - 2) * (count - 3);
            const leftPart = count * (count + 1) / (count - 1) / duplicatedPart;
            const rightPart = 3 * (count - 1) * (count - 1) / duplicatedPart;

            return xpower4 * leftPart - rightPart;
        }

        return this.aggregateNumbersAsArray(args, finalize, SKIP_MAT_SKIP_REF);
    });

    factory.binary("LARGE", "val:num", [ANY_MAT_FORCE, "val:int"], function (operand, k) {
        return StatUtils.groupLarge(this, operand, k);
    });

    factory.fixed("LINEST", "mat:any", 1, 4, ["mat", "mat", "val:bool", "val:bool"], {
        resolve(knownY, knownX, constant, stats) {
            return calculateRGPRKP(knownY, knownX, constant, stats, false);
        }
    });

    factory.fixed("LOGEST", "mat:any", 1, 4, ["mat", "mat", "val:bool", "val:bool"], {
        resolve(knownY, knownX, constant, stats) {
            return calculateRGPRKP(knownY, knownX, constant, stats, true);
        }
    });

    factory.ternaryNum("LOGINV", "LOGNORM.INV");

    factory.fixed("LOGNORM.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.LOGNORM.DIST", odf: "COM.MICROSOFT.LOGNORM.DIST" },
        resolve(x, mean = 0, sigma = 1, isCumulative = true) {

            if (sigma <= 0) {
                throw ErrorCode.NUM;
            }

            if (isCumulative) {
                return (x <= 0) ? 0 : (StatUtils.integralPhi((Math.log(x) - mean) / sigma));
            }

            // density
            if (x <= 0) {
                throw ErrorCode.NUM;
            }

            return (StatUtils.phi((Math.log(x) - mean) / sigma) / sigma / x);
        }
    });

    factory.ternaryNum("LOGNORM.INV", {
        name: { ooxml: "_xlfn.LOGNORM.INV", odf: "COM.MICROSOFT.LOGNORM.INV" },
        resolve(p, mean, sigma) {
            if (sigma <= 0 || p <= 0 || p >= 1) {
                throw ErrorCode.NUM;
            } else {
                return (Math.exp(mean + sigma * StatUtils.gaussInv(p)));
            }
        }
    });

    factory.ternaryNum("LOGNORMDIST", "LOGNORM.DIST");

    factory.unbound("MAX", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupMax(this, args);
    });

    factory.unbound("MAXA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupMax(this, args, { refMode: "zero" });
    });

    factory.unbound("MAXIFS", "val:num", 3, 2, ["ref:val", "ref:val", "val:any"], {
        name: { ooxml: "_xlfn.MAXIFS", odf: null },
        resolve() {

            // aggregate the maximum of all numbers
            const aggregate = (a, b) => Math.max(a, b);
            // default result (no numbers found in parameters) is zero (no #NUM! error code from infinity)
            const finalize = result => Number.isFinite(result) ? result : 0;

            return this.aggregateFilteredCells(true, -Infinity, aggregate, finalize);
        }
    });

    factory.unbound("MEDIAN", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupMedian(this, args);
    });

    factory.unbound("MIN", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupMin(this, args);
    });

    factory.unbound("MINA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupMin(this, args, { refMode: "zero" });
    });

    factory.unbound("MINIFS", "val:num", 3, 2, ["ref:val", "ref:val", "val:any"], {
        name: { ooxml: "_xlfn.MINIFS", odf: null },
        resolve() {

            // aggregate the minimum of all numbers
            const aggregate = (a, b) => Math.min(a, b);
            // default result (no numbers found in parameters) is zero (no #NUM! error code from infinity)
            const finalize = result => Number.isFinite(result) ? result : 0;

            return this.aggregateFilteredCells(true, Infinity, aggregate, finalize);
        }
    });

    factory.unbound("MODE", "val:num", 1, 1, [ANY_MAT_PASS], "MODE.SNGL");

    factory.unbound("MODE.SNGL", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.MODE.SNGL", odf: "COM.MICROSOFT.MODE.SNGL" },
        resolve(...args) { return StatUtils.groupModeSngl(this, args); }
    });

    factory.unbound("MODE.MULT", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.MODE.MULT", odf: "COM.MICROSOFT.MODE.MULT" },
        resolve(...args) { return StatUtils.groupModeMult(this, args); }
    });

    factory.fixed("NEGBINOM.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.NEGBINOM.DIST", odf: "COM.MICROSOFT.NEGBINOM.DIST" },
        resolve(x, r, p, cumulative) {
            if ((r < 0) || (x < 0) || (p < 0) || (p > 1)) { throw ErrorCode.NUM; }
            return cumulative ?
                (1 - StatUtils.betaDist(1 - p, x + 1, r)) :
                StatUtils.getNegBinomDist(x, r, p);
        }
    });

    factory.ternaryNum("NEGBINOMDIST", StatUtils.getNegBinomDist);

    factory.fixed("NORM.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.NORM.DIST", odf: "COM.MICROSOFT.NORM.DIST" },
        resolve(x, mean, stDev, cumulative) {
            if (stDev <= 0) { throw ErrorCode.NUM; }
            if (cumulative) {
                return StatUtils.integralPhi((x - mean) / stDev);
            } else {
                return StatUtils.phi((x - mean) / stDev) / stDev;
            }
        }
    });

    factory.ternaryNum("NORM.INV", {
        name: { ooxml: "_xlfn.NORM.INV", odf: "COM.MICROSOFT.NORM.INV" },
        resolve(x, mean, sigma) {
            if ((sigma <= 0) || (x <= 0) || (x >= 1)) { throw ErrorCode.NUM; }
            return StatUtils.gaussInv(x) * sigma + mean;
        }
    });

    factory.binary("NORM.S.DIST", "val:num", ["val:num", "val:bool"], {
        name: { ooxml: "_xlfn.NORM.S.DIST", odf: "COM.MICROSOFT.NORM.S.DIST" },
        resolve(x, cumulative) {
            if (cumulative) { return StatUtils.integralPhi(x); }
            return Math.exp(-(x * x) / 2) / Math.sqrt(2 * Math.PI);
        }
    });

    factory.unaryNum("NORM.S.INV", {
        name: { ooxml: "_xlfn.NORM.S.INV", odf: "COM.MICROSOFT.NORM.S.INV" },
        resolve(x) {
            if ((x <= 0) || (x >= 1)) { throw ErrorCode.NUM; }
            return StatUtils.gaussInv(x);
        }
    });

    factory.fixed("NORMDIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], "NORM.DIST");

    factory.ternaryNum("NORMINV", "NORM.INV");

    factory.unaryNum("NORMSDIST", StatUtils.integralPhi);

    factory.unaryNum("NORMSINV", "NORM.S.INV");

    factory.binary("PEARSON", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], function (valuesY, valuesX) {
        return StatUtils.getPearsonCoeff(this, valuesY, valuesX);
    });

    factory.binary("PERCENTILE", "val:num", [ANY_MAT_PASS, "val:num"], "PERCENTILE.INC");

    factory.binary("PERCENTILE.EXC", "val:num", [ANY_MAT_PASS, "val:num"], {
        name: { ooxml: "_xlfn.PERCENTILE.EXC", odf: "COM.MICROSOFT.PERCENTILE.EXC" },
        resolve(operand, p) {
            return StatUtils.groupPercentileExc(this, operand, p);
        }
    });

    factory.binary("PERCENTILE.INC", "val:num", [ANY_MAT_PASS, "val:num"], {
        name: { ooxml: "_xlfn.PERCENTILE.INC", odf: "COM.MICROSOFT.PERCENTILE.INC" },
        resolve(operand, p) {
            return StatUtils.groupPercentileInc(this, operand, p);
        }
    });

    factory.fixed("PERCENTRANK", "val:num", 2, { ooxml: 3, odf: 2 }, [ANY_MAT_PASS, "val:num", "val:int"], "PERCENTRANK.INC");

    factory.fixed("PERCENTRANK.EXC", "val:num", 2, 3, [ANY_MAT_PASS, "val:num", "val:int"], {
        name: { ooxml: "_xlfn.PERCENTRANK.EXC", odf: "COM.MICROSOFT.PERCENTRANK.EXC" },
        resolve(operand, p, significance) {
            return StatUtils.groupPercentRankExc(this, operand, p, significance);
        }
    });

    factory.fixed("PERCENTRANK.INC", "val:num", 2, 3, [ANY_MAT_PASS, "val:num", "val:int"], {
        name: { ooxml: "_xlfn.PERCENTRANK.INC", odf: "COM.MICROSOFT.PERCENTRANK.INC" },
        resolve(operand, p, significance) {
            return StatUtils.groupPercentRankInc(this, operand, p, significance);
        }
    });

    factory.binaryInt("PERMUT", (number, chosen) => {
        if ((chosen <= 0) || (number < chosen)) { throw ErrorCode.NUM; }
        return factorial(number) / factorial(number - chosen);
    });

    factory.binaryInt("PERMUTATIONA", {
        name: { ooxml: "_xlfn.PERMUTATIONA" },
        resolve(base, exp) {
            if ((base < 0) || (exp < 0)) { throw ErrorCode.NUM; }
            return Math.pow(base, exp);
        }
    });

    factory.unaryNum("PHI", {
        name: { ooxml: "_xlfn.PHI" },
        resolve: StatUtils.phi
    });

    factory.ternary("POISSON", "val:num", ["val:num", "val:int", "val:bool"], "POISSON.DIST");

    factory.ternary("POISSON.DIST", "val:num", ["val:num", "val:int", "val:bool"], {
        name: { ooxml: "_xlfn.POISSON.DIST", odf: "COM.MICROSOFT.POISSON.DIST" },
        resolve: StatUtils.poisson
    });

    factory.fixed("PROB", "val:num", 3, 4, ["mat:num", "mat:num", "val:num", "val:num"], {
        resolve(matW, matP, bound1, bound2 = bound1) {

            const numCol1 = matP.cols();
            const numCol2 = matW.cols();
            const numRows1 = matP.rows();
            const numRows2 = matW.rows();
            const lower = Math.min(bound1, bound2);
            const upper = Math.max(bound1, bound2);

            if (numCol1 !== numCol2 || numRows1 !== numRows2 || numCol1 === 0 || numRows1 === 0 || numCol2 === 0 || numRows2 === 0) {
                throw ErrorCode.VALUE;
            }

            let sum = 0;
            let res = 0;
            let bStop = false;

            for (let i = 0; i < numCol1 && !bStop; i++) {
                for (let j = 0; j < numRows1 && !bStop; j += 1) {
                    const p = matP.get(j, i);
                    const w = matW.get(j, i);
                    if (p < 0 || p > 1) {
                        bStop = true;
                    } else {
                        sum += p;
                        if (w >= lower && w <= upper) {
                            res += p;
                        }
                    }
                }
            }
            if (bStop || Math.abs(sum - 1) > 1E-7) {
                throw ErrorCode.VALUE;
            }
            return res;
        }
    });

    factory.binary("QUARTILE", "val:num", [ANY_MAT_PASS, "val:int"], "QUARTILE.INC");

    factory.binary("QUARTILE.EXC", "val:num", [ANY_MAT_PASS, "val:int"], {
        name: { ooxml: "_xlfn.QUARTILE.EXC", odf: "COM.MICROSOFT.QUARTILE.EXC" },
        resolve(operand, q) {
            return StatUtils.groupQuartileExc(this, operand, q);
        }
    });

    factory.binary("QUARTILE.INC", "val:num", [ANY_MAT_PASS, "val:int"], {
        name: { ooxml: "_xlfn.QUARTILE.INC", odf: "COM.MICROSOFT.QUARTILE.INC" },
        resolve(operand, q) {
            return StatUtils.groupQuartileInc(this, operand, q);
        }
    });

    factory.ternary2("RANK", "val:num", ["val:num", "ref", "val:bool"], "RANK.EQ");

    factory.ternary2("RANK.AVG", "val:num", ["val:num", "ref", "val:bool"], {
        name: { ooxml: "_xlfn.RANK.AVG", odf: "COM.MICROSOFT.RANK.AVG" },
        resolve(number, ref, ascending) {

            // the count of numbers in the source data less than, equal to, or greater than the passed number
            const counts = getCompareCounts(this, number, ref, SKIP_MAT_SKIP_REF);

            // throw #N/A error code, if the passed number does not exist in the source data
            if (counts.eq === 0) { throw ErrorCode.NA; }

            // the average rank of the number is the average of the individual ranks of each found number
            return (ascending ? counts.lt : counts.gt) + (counts.eq + 1) / 2;
        }
    });

    factory.ternary2("RANK.EQ", "val:num", ["val:num", "ref", "val:bool"], {
        name: { ooxml: "_xlfn.RANK.EQ", odf: "COM.MICROSOFT.RANK.EQ" },
        resolve(number, ref, ascending) {

            // the count of numbers in the source data less than, equal to, or greater than the passed number
            const counts = getCompareCounts(this, number, ref, SKIP_MAT_SKIP_REF);

            // throw #N/A error code, if the passed number does not exist in the source data
            if (counts.eq === 0) { throw ErrorCode.NA; }

            // the rank of the number is the count of predecessors (reverse mode: successors) for the number,
            // plus 1 (it counts itself once, regardless how often it exists in the source data)
            return (ascending ? counts.lt : counts.gt) + 1;
        }
    });

    factory.binary("RSQ", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], function (valuesY, valuesX) {
        const pearson = StatUtils.getPearsonCoeff(this, valuesY, valuesX);
        return pearson * pearson;
    });

    factory.unbound("SKEW", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        const finalize = (numbers, sum) => StatUtils.calculateSkews(numbers, sum, false);
        return this.aggregateNumbersAsArray(args, finalize, SKIP_MAT_SKIP_REF);
    });

    factory.unbound("SKEW.P", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.SKEW.P", odf: "SKEWP" },
        resolve(...args) {
            const finalize = (numbers, sum) => StatUtils.calculateSkews(numbers, sum, true);
            return this.aggregateNumbersAsArray(args, finalize, SKIP_MAT_SKIP_REF);
        }
    });

    factory.binary("SLOPE", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], function (valuesY, valuesX) {
        const data = StatUtils.getForecastData(this, valuesY, valuesX);
        return div(data.sumDYX, data.sumDX2);
    });

    factory.binary("SMALL", "val:num", [ANY_MAT_FORCE, "val:int"], function (operand, k) {
        return StatUtils.groupSmall(this, operand, k);
    });

    factory.ternaryNum("STANDARDIZE", (x, mean, sigma) => {
        if (sigma <= 0) { throw ErrorCode.NUM; }
        return (x - mean) / sigma;
    });

    factory.unbound("STDEV", "val:num", 1, 1, [ANY_MAT_PASS], "STDEV.S");

    factory.unbound("STDEV.P", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.STDEV.P", odf: "COM.MICROSOFT.STDEV.P" },
        resolve(...args) { return StatUtils.groupStdDevP(this, args); }
    });

    factory.unbound("STDEV.S", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.STDEV.S", odf: "COM.MICROSOFT.STDEV.S" },
        resolve(...args) { return StatUtils.groupStdDevS(this, args); }
    });

    factory.unbound("STDEVA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupStdDevS(this, args, { refMode: "zero" });
    });

    factory.unbound("STDEVP", "val:num", 1, 1, [ANY_MAT_PASS], "STDEV.P");

    factory.unbound("STDEVPA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupStdDevP(this, args, { refMode: "zero" });
    });

    factory.binary("STEYX", "val:num", [ANY_MAT_FORCE, ANY_MAT_FORCE], function (valuesY, valuesX) {
        const data = StatUtils.getForecastData(this, valuesY, valuesX);
        const diff = data.sumDY2 - div(data.sumDYX * data.sumDYX, data.sumDX2);
        return Math.sqrt(div(diff, Math.max(0, data.count - 2)));
    });

    factory.ternary("T.DIST", "val:num", ["val:num", "val:int", "val:bool"], {
        name: { ooxml: "_xlfn.T.DIST", odf: "COM.MICROSOFT.T.DIST" },
        resolve(t, df, cumulative) {
            if (df < 1) { throw ErrorCode.NUM; }
            return StatUtils.getTDist(t, df, cumulative ? 4 : 3);
        }
    });

    factory.binaryNumInt("T.DIST.2T", {
        name: { ooxml: "_xlfn.T.DIST.2T", odf: "COM.MICROSOFT.T.DIST.2T" },
        resolve(t, df) {
            if ((df < 1) || (t < 0)) { throw ErrorCode.NUM; }
            return StatUtils.getTDist(t, df, 2);
        }
    });

    factory.binaryNumInt("T.DIST.RT", {
        name: { ooxml: "_xlfn.T.DIST.RT", odf: "COM.MICROSOFT.T.DIST.RT" },
        resolve(t, df) {
            if (df < 1) { throw ErrorCode.NUM; }
            return StatUtils.getTDist(t, df, 1);
        }
    });

    factory.binaryNumInt("T.INV", {
        name: { ooxml: "_xlfn.T.INV", odf: "COM.MICROSOFT.T.INV" },
        resolve(p, df) {
            if ((df < 1) || (p <= 0) || (p > 1)) { throw ErrorCode.NUM; }
            return (p < 0.5) ? -StatUtils.getTInv(1 - p, df, 4) : StatUtils.getTInv(p, df, 4);
        }
    });

    factory.binaryNumInt("T.INV.2T", {
        name: { ooxml: "_xlfn.T.INV.2T", odf: "COM.MICROSOFT.T.INV.2T" },
        resolve(p, df) {
            if ((df < 1) || (p <= 0) || (p > 1)) { throw ErrorCode.NUM; }
            return (p < 0.5) ? -StatUtils.getTInv(1 - p, df, 2) : StatUtils.getTInv(p, df, 2);
        }
    });

    factory.fixed("T.TEST", "val:num", 4, 4, [ANY_MAT_PASS, ANY_MAT_PASS, "val:int", "val:int"], {
        name: { ooxml: "_xlfn.T.TEST", odf: "COM.MICROSOFT.T.TEST" },
        resolve(data1, data2, tails, type) {

            let count1;
            let count2;
            let S1 = 0;
            let S2 = 0;
            let sum1 = 0;
            let sum2 = 0;
            let sumSqr1 = 0;
            let sumSqr2 = 0;
            let t;
            let f;

            // standard options for numeric parameter iterators
            const TTEST_PARAM = { // id: ESS
                valMode: "exact", // value operands: must be numbers
                matMode: "skip", // matrix operands: skip strings and booleans
                refMode: "skip", // reference operands: skip strings and booleans
                emptyParam: true // empty parameters count as zero
            };

            function finalize(result) {
                const sum1 = result[0];
                const sum2 = result[1];
                const sumSqrD = result[2];
                const count = result[3];
                const diff = Math.abs(sum1 - sum2);

                t = Math.sqrt(count - 1) * diff / Math.sqrt(count * sumSqrD - diff * diff);
                f = count - 1;
            }

            function agregate(result, num) {
                result[0] += num;
                result[1] += num * num;
                return result;
            }

            function finalize2(result, count) {
                sum1 = result[0];
                sumSqr1 = result[1];
                count1  = count;
            }

            function finalize3(result, count) {
                sum2 = result[0];
                sumSqr2 = result[1];
                count2  = count;
            }

            if (tails !== 1 && tails !== 2) {
                throw ErrorCode.NUM;
            }
            if (type === 1) {
                this.aggregateNumbersParallel([data1, data2], [0, 0, 0, 0], (result, nums) => {
                    result[0] += nums[0];
                    result[1] += nums[1];
                    result[2] += (nums[0] - nums[1]) * (nums[0] - nums[1]);
                    result[3] += 1;

                    return result;
                }, finalize, TTEST_PARAM);
            } else {
                if (type !== 2 && type !== 3) {
                    throw ErrorCode.NUM;
                }

                this.aggregateNumbers([data1], [0, 0], agregate, finalize2, TTEST_PARAM);
                this.aggregateNumbers([data2], [0, 0], agregate, finalize3, TTEST_PARAM);

                if (type === 2) {
                    S1 = (sumSqr1 - sum1 * sum1 / count1) / (count1 - 1);
                    S2 = (sumSqr2 - sum2 * sum2 / count2) / (count2 - 1);
                    t = Math.abs(sum1 / count1 - sum2 / count2) /
                        Math.sqrt((count1 - 1) * S1 + (count2 - 1) * S2) *
                        Math.sqrt(count1 * count2 * (count1 + count2 - 2) / (count1 + count2));
                    f = count1 + count2 - 2;
                } else {
                    S1 = (sumSqr1 - sum1 * sum1 / count1) / (count1 - 1) / count1;
                    S2 = (sumSqr2 - sum2 * sum2 / count2) / (count2 - 1) / count2;
                    if (S1 + S2 === 0) {
                        throw ErrorCode.VALUE;
                    }
                    t = Math.abs(sum1 / count1 - sum2 / count2) / Math.sqrt(S1 + S2);
                    const c = S1 / (S1 + S2);
                    f = 1 / (c * c / (count1 - 1) + (1 - c) * (1 - c) / (count2 - 1));
                }
            }
            return StatUtils.getTDist(t, f, tails);
        }
    });

    factory.ternary("TDIST", "val:num", ["val:num", "val:int", "val:int"], {
        // AOO 4.4 writes "TDIST" (https://bz.apache.org/ooo/show_bug.cgi?id=126519)
        name: { odf: ["LEGACY.TDIST", "TDIST"] },
        resolve(t, df, tails) {
            if ((df < 1) || (t < 0)) { throw ErrorCode.NUM; }
            if ((tails < 1) || (tails > 2)) { throw ErrorCode.NUM; }
            return StatUtils.getTDist(t, df, tails);
        }
    });

    factory.binaryNumInt("TINV", "T.INV.2T");

    factory.fixed("TREND", "mat:num", 1, 4, [ANY_MAT_FORCE, ANY_MAT_FORCE, ANY_MAT_FORCE, "val:bool"], {
        resolve(knownY, knownX, newX, constant) {
            return StatUtils.calculateTrendGrowth(this, knownY, knownX, newX, constant, false);
        }
    });

    factory.binary("TRIMMEAN", "val:num", [ANY_MAT_PASS, "val:num"], function (set, alpha) {

        if (alpha < 0 || alpha >= 1) {
            throw ErrorCode.NUM;
        }

        const numbers = this.getNumbersAsArray(set, RCONVERT_ALL_SKIP_EMPTY).sort(math.compare);
        const count = numbers.length;
        if (count === 0) { throw ErrorCode.VALUE; }

        let numIndex = Math.floor(alpha * count);
        if (numIndex % 2 !== 0) {
            numIndex -= 1;
        }
        numIndex /= 2;

        if (numIndex > count) {
            globalLogger.error("statisticalfuncs.js TRIMMEAN: wrong index!");
        }

        let sum = 0;
        for (let i = numIndex; i < count - numIndex; i += 1) {
            sum += numbers[i];
        }

        return sum / (count - 2 * numIndex);
    });

    factory.fixed("TTEST", "val:num", 4, 4, [ANY_MAT_PASS, ANY_MAT_PASS, "val:int", "val:int"], "T.TEST");

    factory.unbound("VAR", "val:num", 1, 1, [ANY_MAT_PASS], "VAR.S");

    factory.unbound("VAR.P", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.VAR.P", odf: "COM.MICROSOFT.VAR.P" },
        resolve(...args) { return StatUtils.groupVarP(this, args); }
    });

    factory.unbound("VAR.S", "val:num", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.VAR.S", odf: "COM.MICROSOFT.VAR.S" },
        resolve(...args) { return StatUtils.groupVarS(this, args); }
    });

    factory.unbound("VARA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupVarS(this, args, { refMode: "zero" });
    });

    factory.unbound("VARP", "val:num", 1, 1, [ANY_MAT_PASS], "VAR.P");

    factory.unbound("VARPA", "val:num", 1, 1, [ANY_MAT_PASS], function (...args) {
        return StatUtils.groupVarP(this, args, { refMode: "zero" });
    });

    factory.fixed("WEIBULL", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], "WEIBULL.DIST");

    factory.fixed("WEIBULL.DIST", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:bool"], {
        name: { ooxml: "_xlfn.WEIBULL.DIST", odf: "COM.MICROSOFT.WEIBULL.DIST" },
        resolve(x, alpha, beta, cumulative) {
            if ((alpha <= 0) || (beta <= 0) || (x < 0)) { throw ErrorCode.NUM; }
            return cumulative ?
                (1 - Math.exp(-Math.pow(x / beta, alpha))) :
                (alpha / Math.pow(beta, alpha) * Math.pow(x, alpha - 1) * Math.exp(-Math.pow(x / beta, alpha)));
        }
    });

    factory.ternary2("Z.TEST", "val:num", [ANY_MAT_PASS, "val:num", "val:num"], {
        name: { ooxml: "_xlfn.Z.TEST", odf: "COM.MICROSOFT.Z.TEST" },
        resolve(mat, x, sigma) {

            if (is.number(sigma) && sigma <= 0) {
                throw ErrorCode.NUM;
            }

            const numbers = this.getNumbersAsArray(mat, RCONVERT_ALL_SKIP_EMPTY);
            const count = numbers.length;
            if (count <= 1) {
                throw ErrorCode.DIV0;
            }

            let sum = 0, sumSqr = 0;
            for (const number of numbers) {
                sum += number;
                sumSqr += number * number;
            }

            const mean = sum / count;

            if (is.number(sigma)) {
                return 0.5 - StatUtils.gauss((mean - x) * Math.sqrt(count) / sigma);
            }

            sigma = (sumSqr - sum * sum / count) / (count - 1);
            return 0.5 - StatUtils.gauss((mean - x) / Math.sqrt(sigma / count));
        }
    });

    factory.ternary2("ZTEST", "val:num", [ANY_MAT_PASS, "val:num", "val:num"], "Z.TEST");
});
