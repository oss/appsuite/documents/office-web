/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with complex
 * numbers.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 * In spreadsheet documents, complex numbers are represented by real numbers
 * (complex number without imaginary part), or by strings looking like complex
 * numbers, e.g. "2+3i" or "-4j". The formula engine provides the function
 * signature type "val:comp", and the class `Complex` whose instances represent
 * complex numbers converted from real numbers, or from string representations
 * of complex numbers.
 *
 *****************************************************************************/

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";

import { Complex } from "@/io.ox/office/spreadsheet/model/formula/utils/complex";
import type { ScalarIteratorOptions, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// constants ==================================================================

// options for iterating complex numbers in operands of `IMSUM` and `IMPRODUCT`
const AGGREGATE_OPTIONS: ScalarIteratorOptions = {
    emptyParam: false, // empty parameters are skipped: IMSUM("1+i",) = "1+i"
    multiRange: false, // do not accept multi-range references
    multiSheet: false  // do not accept multi-sheet references
};

// private functions ==========================================================

/**
 * Returns a resolver function for `IMSUM` and `IMPRODUCT` that reduces all
 * complex numbers of all function parameters to their sum or product.
 *
 * @param aggregate
 *  The aggregation callback function. Receives two complex numbers, and must
 *  return the resulting complex number.
 *
 * @returns
 *  The resulting function implementation for `IMSUM` and `IMPRODUCT`.
 */
function implementComplexAggregation(aggregate: (c1: Complex, c2: Complex) => Complex) {

    // create the resolver callback function, will be called with FormulaContext context
    return function complexAggregator(this: FormulaContext): Complex {

        // the resulting complex product
        let result: Opt<Complex>;
        // whether any parameter was non-empty
        let hasAny = false;

        this.iterateOperands(0, operand => {
            this.forEachScalar(operand, value => {
                const c = this.convertToComplex(value);
                result = result ? aggregate(result, c) : new Complex(c.real, c.imag, c.unit);
            }, AGGREGATE_OPTIONS);
            if (!operand.isEmpty()) { hasAny = true; }
        });

        // only empty arguments: return #N/A instead of zero
        if (!hasAny) { throw ErrorCode.NA; }
        // only references to empty cells: return 0
        return result ?? new Complex(0, 0);
    };
}

// exports ====================================================================

export default declareFunctionSpecModule("complex", factory => {

    const ANY_MAT_PASS = factory.param("any", { mat: "pass" });

    factory.ternary2("COMPLEX", "val:comp", ["val:num", "val:num", "val:str"], (real, imag, unit) => {
        // if specified, unit must be lower-case "i" or "j"
        if (unit && (unit !== "i") && (unit !== "j")) { throw ErrorCode.VALUE; }
        // create a complex number (will be converted to a string)
        return new Complex(real, imag, unit);
    });

    factory.unary("IMABS", "val:num", "val:comp", c => c.abs());

    factory.unary("IMAGINARY", "val:num", "val:comp", c => c.imag);

    factory.unary("IMARGUMENT", "val:num", "val:comp", c => c.arg());

    factory.unaryComp("IMCONJUGATE", c => c.conj());

    factory.unaryComp("IMCOS", c => c.cos());

    factory.unaryComp("IMCOSH", {
        name: { ooxml: "_xlfn.IMCOSH" },
        resolve: c => c.cosh()
    });

    factory.unaryComp("IMCOT", {
        name: { ooxml: "_xlfn.IMCOT" },
        resolve: c => c.cot()
    });

    factory.unaryComp("IMCSC", {
        name: { ooxml: "_xlfn.IMCSC" },
        resolve: c => c.csc()
    });

    factory.unaryComp("IMCSCH", {
        name: { ooxml: "_xlfn.IMCSCH" },
        resolve: c => c.csch()
    });

    factory.binaryComp("IMDIV", (c1, c2) => c1.div(c2));

    factory.unaryComp("IMEXP", c => c.exp());

    factory.unaryComp("IMLN", c => c.ln());

    factory.unaryComp("IMLOG10", c => c.log10());

    factory.unaryComp("IMLOG2", c => c.log2());

    factory.binary("IMPOWER", "val:comp", ["val:comp", "val:num"], (c, e) => c.pow(e));

    factory.unbound("IMPRODUCT", "val:comp", 1, 1, [ANY_MAT_PASS], implementComplexAggregation((c1, c2) => c1.mul(c2)));

    factory.unary("IMREAL", "val:num", "val:comp", c => c.real);

    factory.unaryComp("IMSEC", {
        name: { ooxml: "_xlfn.IMSEC" },
        resolve: c => c.sec()
    });

    factory.unaryComp("IMSECH", {
        name: { ooxml: "_xlfn.IMSECH" },
        resolve: c => c.sech()
    });

    factory.unaryComp("IMSIN", c => c.sin());

    factory.unaryComp("IMSINH", {
        name: { ooxml: "_xlfn.IMSINH" },
        resolve: c => c.sinh()
    });

    factory.unaryComp("IMSQRT", c => c.sqrt());

    factory.binaryComp("IMSUB", (c1, c2) => c1.sub(c2));

    factory.unbound("IMSUM", "val:comp", 1, 1, [ANY_MAT_PASS], implementComplexAggregation((c1, c2) => c1.add(c2)));

    factory.unaryComp("IMTAN", {
        name: { ooxml: "_xlfn.IMTAN" },
        resolve: c => c.tan()
    });
});
