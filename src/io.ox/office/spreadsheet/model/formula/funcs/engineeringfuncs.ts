/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions that fit more or less into
 * the function category "engineering".
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import Bessel, { type BesselFn } from "bessel";

import { is } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { erf, erfc } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// private functions ==========================================================

/**
 * Throws the #NUM! error code, if the passed number is not supported by the
 * bit manipulation functions (restricted to unsigned 48-bit integers).
 *
 * @param num
 *  The input number to be tested.
 *
 * @returns
 *  The passed number, if it is valid, for convenience.
 *
 * @throws
 *  The error code #NUM!, if the passed number is negative, or is greater than
 *  or equal to 2^48.
 */
function ensure48bit(num: bigint): bigint {
    return ((num >= 0n) && (num <= 0xFFFF_FFFF_FFFFn)) ? num : ErrorCode.NUM.throw();
}

/**
 * Shifts the passed number by the specified number of bits to the left.
 *
 * @param num
 *  The number to be shifted.
 *
 * @param bits
 *  The number of bits to shift. A negative value will shift the number to the
 *  right.
 *
 * @returns
 *  The shifted number.
 *
 * @throws
 *  The error code #NUM!, if the passed number or bit count is invalid.
 */
function shiftBitsLeft(num: bigint, bits: number): bigint {
    ensure48bit(num);
    // special case for bitshift functions: bit count will be truncated (negative values towards zero!)
    bits = Math.trunc(bits);
    if ((bits < -53) || (bits > 53)) { throw ErrorCode.NUM; }
    // weird Excel behavior: BITLSHIFT(1,48) is #NUM!; but BITLSHIFT(1,[49...53]) is zero
    if (bits === 48) { throw ErrorCode.NUM; }
    if ((bits < -48) || (bits > 48)) { return 0n; }
    return (bits > 0n) ? ensure48bit(num << BigInt(bits)) : (num >> BigInt(-bits));
}

// implementation of binary ERF() without error checking (ODF)
function erfBinODF(a: number, b?: number): number {
    // equal values always result in zero (do not calculate erf(a) explicitly)
    if (a === b) { return 0; }
    // no second parameter: calculate erf(a)
    const erf_a = erf(a);
    if (!is.number(b)) { return erf_a; }
    // absolute value of parameters are equal: do not calculate erf(b)
    return (a === -b) ? (-2 * erf_a) : (erf(b) - erf_a);
}

// implementation of binary ERF() with error checking (OOXML)
function erfBinOOX(a: number, b?: number): number {
    // legacy ERF() function: negative numbers result in #NUM! error
    if ((a < 0) || (is.number(b) && (b < 0))) { throw ErrorCode.NUM; }
    return erfBinODF(a, b);
}

// implementation of ERFC() with error checking (OOXML)
function erfcOOX(a: number): number {
    // legacy ERFC() function: negative numbers result in #NUM! error
    if (a < 0) { throw ErrorCode.NUM; }
    return erfc(a);
}

// wrapper for external bessel functions that throws #NUM! for invalid order
function wrapBessel(fn: BesselFn): BesselFn {
    return (num, order) => ((order < 0) || (order > 1e7)) ? ErrorCode.NUM.throw() : fn(num, order);
}

// exports ====================================================================

export default declareFunctionSpecModule("engineering", factory => {

    factory.binaryNumInt("BESSELI", wrapBessel(Bessel.besseli));

    factory.binaryNumInt("BESSELJ", wrapBessel(Bessel.besselj));

    factory.binaryNumInt("BESSELK", wrapBessel(Bessel.besselk));

    factory.binaryNumInt("BESSELY", wrapBessel(Bessel.bessely));

    factory.binaryBint("BITAND", {
        name: { ooxml: "_xlfn.BITAND" },
        resolve: (num1, num2) => ensure48bit(num1) & ensure48bit(num2)
    });

    factory.binaryBintNum("BITLSHIFT", {
        name: { ooxml: "_xlfn.BITLSHIFT" },
        resolve: shiftBitsLeft
    });

    factory.binaryBint("BITOR", {
        name: { ooxml: "_xlfn.BITOR" },
        resolve: (num1, num2) => ensure48bit(num1) | ensure48bit(num2)
    });

    factory.binaryBintNum("BITRSHIFT", {
        name: { ooxml: "_xlfn.BITRSHIFT" },
        resolve: (num, bits) => shiftBitsLeft(num, -bits)
    });

    factory.binaryBint("BITXOR", {
        name: { ooxml: "_xlfn.BITXOR" },
        resolve: (num1, num2) => ensure48bit(num1) ^ ensure48bit(num2)
    });

    factory.binary1Num("DELTA", (number, step) => {
        if (!is.number(step)) { step = 0; }
        return (number === step) ? 1 : 0;
    });

    factory.binary1Num("ERF", {
        resolve: { ooxml: erfBinOOX, odf: erfBinODF }
    });

    factory.unaryNum("ERF.PRECISE", {
        name: { ooxml: "_xlfn.ERF.PRECISE", odf: "COM.MICROSOFT.ERF.PRECISE" },
        resolve: erf
    });

    factory.unaryNum("ERFC", {
        resolve: { ooxml: erfcOOX, odf: erfc }
    });

    factory.unaryNum("ERFC.PRECISE", {
        name: { ooxml: "_xlfn.ERFC.PRECISE", odf: "COM.MICROSOFT.ERFC.PRECISE" },
        resolve: erfc
    });

    factory.binary1Num("GESTEP", (number, step) => {
        if (!is.number(step)) { step = 0; }
        return (number >= step) ? 1 : 0;
    });
});
