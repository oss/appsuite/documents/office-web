/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all mathematical spreadsheet functions.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { is, math, fun } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { add, div, mod, POW, atanxy, acoth, mround, dround, binomial, factorial, factorial2 } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import type { GroupAggregateAnyFunc, GroupAggregateNumFunc } from "@/io.ox/office/spreadsheet/model/formula/utils/statutils";
import * as statutils from "@/io.ox/office/spreadsheet/model/formula/utils/statutils";
import type { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { CommonNumberIteratorOptions, NumberAggregatorOptions, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

const {
    groupAverage, groupCount, groupCountA, groupMax, groupMin, groupSum, groupProduct,
    groupStdDevS, groupStdDevP, groupVarS, groupVarP,
    groupMedian, groupModeSngl, groupLarge, groupSmall,
    groupPercentileInc, groupQuartileInc, groupPercentileExc, groupQuartileExc
} = statutils;

// types ======================================================================

interface AggregateAnyFuncInfo {
    type: "any";
    func: GroupAggregateAnyFunc;
}

interface AggregateNumFuncInfo {
    type: "val:int" | "val:num";
    func: GroupAggregateNumFunc;
}

type AggregateFuncInfo = AggregateAnyFuncInfo | AggregateNumFuncInfo;

// constants ==================================================================

// mathematical constants
const PI_2 = Math.PI / 2;
const PI_180 = Math.PI / 180;

// standard options for numeric parameter iterators
const SKIP_MAT_SKIP_REF_SKIP_EMPTY: CommonNumberIteratorOptions = { // id: CSS4
    valMode: "convert",     // value operands: convert strings and booleans to numbers
    matMode: "skip",        // matrix operands: skip strings and booleans
    refMode: "skip",        // reference operands: skip strings and booleans
    // emptyParam: false,   // skip all empty parameters
    multiRange: true,       // accept multi-range references
    multiSheet: true        // accept multi-sheet references
};

// standard options for numeric parameter iterators
const RCONVERT_ALL_SKIP_EMPTY: CommonNumberIteratorOptions = { // id: RRR0
    valMode: "rconvert",    // value operands: convert strings to numbers (but not booleans)
    matMode: "rconvert",    // matrix operands: convert strings to numbers (but not booleans)
    refMode: "rconvert"     // reference operands: convert strings to numbers (but not booleans)
    // emptyParam: false,   // skip all empty parameters
    // multiRange: false,   // do not accept multi-range references
    // multiSheet: false    // do not accept multi-sheet references
};

// all function implementations used by the `SUBTOTAL` function, mapped by its `index` parameter
const SUBTOTAL_FUNCTIONS: Array<Opt<GroupAggregateAnyFunc>> = [
    undefined,
    groupAverage,
    groupCount,
    groupCountA,
    groupMax,
    groupMin,
    groupProduct,
    groupStdDevS,
    groupStdDevP,
    groupSum,
    groupVarS,
    groupVarP
];

// all function implementations used by the AGGREGATE function
const AGGREGATE_FUNCTIONS_INFO: AggregateFuncInfo[] = [
    { type: "any",     func: groupAverage },
    { type: "any",     func: groupCount },
    { type: "any",     func: groupCountA },
    { type: "any",     func: groupMax },
    { type: "any",     func: groupMin },
    { type: "any",     func: groupProduct },
    { type: "any",     func: groupStdDevS },
    { type: "any",     func: groupStdDevP },
    { type: "any",     func: groupSum },
    { type: "any",     func: groupVarS },
    { type: "any",     func: groupVarP },
    { type: "any",     func: groupMedian },
    { type: "any",     func: groupModeSngl },
    { type: "val:int", func: groupLarge },
    { type: "val:int", func: groupSmall },
    { type: "val:num", func: groupPercentileInc },
    { type: "val:int", func: groupQuartileInc },
    { type: "val:num", func: groupPercentileExc },
    { type: "val:int", func: groupQuartileExc }
];

// private functions ==========================================================

/**
 * Returns the greatest common divisor of the passed integer numbers.
 *
 * @param int1
 *  The first integer number.
 *
 * @param int2
 *  The second integer number.
 *
 * @returns
 *  The greatest common divisor of the passed numbers.
 */
function gcd(int1: number, int2: number): number {
    while (int2 > 0) {
        const tmp = int2;
        int2 = Math.round(int1 % int2);
        int1 = tmp;
    }
    return int1;
}

// exports ====================================================================

export default declareFunctionSpecModule("math", factory => {

    const ANY_MAT_PASS = factory.param("any", { mat: "pass" });

    factory.unaryNum("ABS", Math.abs);

    factory.unaryNum("ACOS", Math.acos);

    factory.unaryNum("ACOSH", Math.acosh);

    factory.unaryNum("ACOT", {
        name: { ooxml: "_xlfn.ACOT" },
        resolve: n => PI_2 - Math.atan(n)
    });

    factory.unaryNum("ACOTH", {
        name: { ooxml: "_xlfn.ACOTH" },
        resolve: acoth
    });

    factory.unbound("AGGREGATE", "val:num", 3, 1, ["val:int", "val:int", "any", "any", "ref"], {
        name: { ooxml: "_xlfn.AGGREGATE", odf: "COM.MICROSOFT.AGGREGATE" },
        recalc: "always", // function result depends on hidden state of rows
        resolve(func: number, flags: number) {

            // get the function implementation (one-based to zero-based index!)
            const funcInfo = AGGREGATE_FUNCTIONS_INFO[func - 1];
            if (!funcInfo) { throw ErrorCode.VALUE; }

            // initialize the options according to the passed flags:
            const options: CommonNumberIteratorOptions = {
                // bit 0: whether to visit visible rows only (hidden columns will always be visited!)
                skipHiddenRows: !!(flags & 1),
                // bit 1: whether to skip error codes in source data (by default, they will be thrown immediately)
                skipErrors: !!(flags & 2)
            };

            // "any" functions: invoke the function implementation with the remaining operands
            if (funcInfo.type === "any") {
                return funcInfo.func(this, this.getOperands(2), options);
            }

            // parameter count of functions with numeric fourth parameter (e.g. LARGE/SMALL) are restricted
            if (this.getOperandCount() !== 4) { throw ErrorCode.VALUE; }
            return funcInfo.func(this, this.getOperand(2), this.getOperand(3, funcInfo.type), options);
        }
    });

    factory.unaryNum("ASIN", Math.asin);

    factory.unaryNum("ASINH", Math.asinh);

    factory.unaryNum("ATAN", Math.atan);

    factory.binaryNum("ATAN2", atanxy);

    factory.unaryNum("ATANH", Math.atanh);

    // old `CEILING` function of Excel that rounds negative numbers away from zero
    factory.binaryNum("CEILING", {
        name: { odf: "COM.MICROSOFT.CEILING" },
        resolve: (n, prec) => {
            // throw #NUM! for negative multiplier on positive numbers (but not vice versa!)
            if ((n > 0) && (prec < 0)) { throw ErrorCode.NUM; }
            return mround(n, prec, "inflate");
        }
    });

    factory.ternary1Num("CEILING.MATH", {
        name: { ooxml: "_xlfn.CEILING.MATH", odf: "COM.MICROSOFT.CEILING.MATH" },
        resolve(n, prec, mode) {
            // missing multiplier defaults to 1
            prec = is.number(prec) ? prec : 1;
            // round down negative numbers (away from zero) if `mode` is non-zero
            return mround(n, prec, mode ? "inflate" : "ceil");
        }
    });

    // old `CEILING` function of ODF that behaves like `CEILING.MATH`
    factory.ternary1Num("CEILING.ODF", {
        name: { ooxml: null, odf: "CEILING" },
        resolve: "CEILING.MATH"
    });

    factory.binary1Num("CEILING.PRECISE", {
        name: { ooxml: "_xlfn.CEILING.PRECISE", odf: "COM.MICROSOFT.CEILING.PRECISE" },
        hidden: true, // replaced by CEILING.MATH in XL2013
        resolve: "CEILING.MATH"
    });

    factory.binaryInt("COMBIN", binomial);

    factory.binaryInt("COMBINA", {
        name: { ooxml: "_xlfn.COMBINA" },
        resolve: (n, k) => binomial(n + k - 1, k)
    });

    factory.unaryNum("COS", Math.cos);

    factory.unaryNum("COSH", Math.cosh);

    factory.unaryNum("COT", {
        name: { ooxml: "_xlfn.COT" },
        resolve: n => div(1, Math.tan(n))
    });

    factory.unaryNum("COTH", {
        name: { ooxml: "_xlfn.COTH" },
        resolve: n => div(1, Math.tanh(n))
    });

    factory.unaryNum("CSC", {
        name: { ooxml: "_xlfn.CSC" },
        resolve: n => div(1, Math.sin(n))
    });

    factory.unaryNum("CSCH", {
        name: { ooxml: "_xlfn.CSCH" },
        resolve: n => div(2 * Math.exp(-n), 1 - Math.exp(-2 * n))
    });

    factory.unaryNum("DEGREES", n => n / PI_180);

    // round to multiples of 2, always away from zero (e.g.: -0.5 to -2)
    factory.unaryNum("EVEN", n => mround(n, 2, "inflate"));

    factory.unaryNum("EXP", Math.exp);

    factory.unaryInt("FACT", factorial);

    factory.unaryInt("FACTDOUBLE", factorial2);

    // old `FLOOR` function of Excel that rounds negative numbers towards zero
    factory.binaryNum("FLOOR", {
        name: { odf: "COM.MICROSOFT.FLOOR" },
        resolve: (n, prec) => {
            // throw #NUM! for negative multiplier on positive numbers (but not vice versa!)
            if ((n > 0) && (prec < 0)) { throw ErrorCode.NUM; }
            return mround(n, prec, "trunc");
        }
    });

    factory.ternary1Num("FLOOR.MATH", {
        name: { ooxml: "_xlfn.FLOOR.MATH", odf: "COM.MICROSOFT.FLOOR.MATH" },
        resolve(n, prec, mode) {
            // missing multiplier defaults to 1
            prec = is.number(prec) ? prec : 1;
            // round up negative numbers (towards zero) if `mode` is non-zero
            return mround(n, prec, mode ? "trunc" : "floor");
        }
    });

    // old `FLOOR` function of ODF that behaves like `FLOOR.MATH`
    factory.ternary1Num("FLOOR.ODF", {
        name: { ooxml: null, odf: "FLOOR" },
        resolve: "FLOOR.MATH"
    });

    factory.binary1Num("FLOOR.PRECISE", {
        name: { ooxml: "_xlfn.FLOOR.PRECISE", odf: "COM.MICROSOFT.FLOOR.PRECISE" },
        hidden: true, // replaced by FLOOR.MATH in XL2013
        resolve: "FLOOR.MATH"
    });

    factory.unbound("GCD", "val:num", 1, 1, [ANY_MAT_PASS], fun.do(() => {

        // returns the GCD of the result and the new number
        function aggregate(interm: number, n: number): number {
            // round down to integers, fail on negative numbers
            n = Math.floor(n);
            return (n < 0) ? ErrorCode.NUM.throw() : gcd(interm, n);
        }

        // throw #VALUE! without any number (reference to empty cells)
        function finalize(interm: number, count: number): number {
            return (count === 0) ? ErrorCode.VALUE.throw() : interm;
        }

        return function (this: FormulaContext, ...operands: Operand[]): number {
            // start with 0, needed to force calling aggregate() for all function
            // parameters for integer conversion and check for negative numbers
            // Booleans lead to error: GCD(6,TRUE) = #VALUE!
            // empty parameters are ignored: GCD(6,) = 6
            return this.aggregateNumbers(operands, 0, aggregate, finalize, RCONVERT_ALL_SKIP_EMPTY);
        };
    }));

    // always rounds down, e.g. INT(-1.1) = -2 (rounding is already done by `unaryInt()`)
    factory.unaryInt("INT", fun.identity);

    factory.binary1Num("ISO.CEILING", {
        name: { odf: "COM.MICROSOFT.ISO.CEILING" },
        hidden: true, // replaced by CEILING.MATH in XL2013
        resolve: "CEILING.PRECISE"
    });

    factory.unbound("LCM", "val:num", 1, 1, [ANY_MAT_PASS], fun.do(() => {

        // updates the LCM in the passed result according to the passed number
        function aggregate(interm: number, n: number): number {
            // round down to integers, fail on negative numbers
            n = Math.floor(n);
            if (n < 0) { throw ErrorCode.NUM; }
            return ((interm === 0) || (n === 0)) ? 0 : (interm * n / gcd(n, interm));
        }

        // throw #VALUE! without any number (reference to empty cells)
        function finalize(interm: number, count: number): number {
            return (count === 0) ? ErrorCode.VALUE.throw() : interm;
        }

        // start with 1, needed to force calling aggregate() for all function
        // parameters for integer conversion and check for negative numbers
        // Booleans lead to error: LCM(6,TRUE) = #VALUE!
        // empty parameters are ignored: LCM(6,) = 6 while LCM(6,0) = 0
        return function (this: FormulaContext, ...operands: Operand[]): number {
            return this.aggregateNumbers(operands, 1, aggregate, finalize, RCONVERT_ALL_SKIP_EMPTY);
        };
    }));

    factory.unaryNum("LN", Math.log);

    factory.binary1Num("LOG", (n, base) => is.number(base) ? (Math.log(n) / Math.log(base)) : Math.log10(n));

    factory.unaryNum("LOG10", Math.log10);

    factory.binaryNum("MOD", mod);

    factory.binaryNum("MROUND", (n, prec) => {
        // throw #NUM! error, if the signs of the numbers do not match
        if (n * prec < 0) { throw ErrorCode.NUM; }
        // always round negative numbers with absolute logic (-0.5 to -1 instead of 0)
        return mround(n, prec, "iround");
    });

    factory.unbound("MULTINOMIAL", "val:num", 1, 1, [ANY_MAT_PASS], fun.do(() => {

        // round down all numbers to integers
        const MULTINOMIAL_OPTIONS: CommonNumberIteratorOptions = { ...RCONVERT_ALL_SKIP_EMPTY, floor: true };

        // use the same simple implementation as Excel: if the sum of all numbers exceeds
        // the value 170, getting factorial in the numerator will fail with a #NUM! error,
        // although the final result of the function would be small enough
        function finalize(numbers: number[], sum: number): number {
            if (numbers.length === 0) { throw ErrorCode.VALUE; }
            const denom = numbers.reduce((i, n) => i * factorial(n), 1);
            return factorial(sum) / denom;
        }

        // the implementation of the MULTINOMIAL function
        return function (this: FormulaContext, ...operands: Operand[]): number {
            return this.aggregateNumbersAsArray(operands, finalize, MULTINOMIAL_OPTIONS);
        };
    }));

    factory.unary("N", "val:num", "val", v => is.number(v) ? v : (v === true) ? 1 : 0);

    factory.unaryNum("NEG", {
        name: { ooxml: null },
        resolve: n => -n
    });

    // round away from zero to next odd integer (0 becomes 1)
    factory.unaryNum("ODD", n => (n < 0) ? (mround(n - 1, 2, "floor") + 1) : (mround(n + 1, 2, "ceil") - 1));

    factory.nullaryNum("PI", Math.PI);

    factory.binaryNum("POWER", { resolve: POW });

    factory.unbound("PRODUCT", "val:num", 1, 1, [ANY_MAT_PASS], function (...operands: Operand[]) {
        return groupProduct(this, operands);
    });

    factory.binaryNum("QUOTIENT", (n1, n2) => Math.trunc(div(n1, n2)));

    factory.unaryNum("RADIANS", n => n * PI_180);

    factory.nullaryNum("RAND", {
        recalc: "always",
        resolve: Math.random
    });

    factory.binaryNum("RANDBETWEEN", {
        recalc: "always",
        resolve: (min, max) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            if (max < min) { throw ErrorCode.NUM; }
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    });

    factory.binary1NumInt("ROUND", {
        minParams: { ooxml: 2, odf: 1 },
        resolve: (n, digits) => dround(n, digits || 0, "iround")
    });

    factory.binary1NumInt("ROUNDDOWN", {
        minParams: { ooxml: 2, odf: 1 },
        resolve: (n, digits) => dround(n, digits || 0, "trunc")
    });

    factory.binary1NumInt("ROUNDUP", {
        minParams: { ooxml: 2, odf: 1 },
        resolve: (n, digits) => dround(n, digits || 0, "inflate")
    });

    factory.unaryNum("SEC", {
        name: { ooxml: "_xlfn.SEC" },
        resolve: n => div(1, Math.cos(n))
    });

    factory.unaryNum("SECH", {
        name: { ooxml: "_xlfn.SECH" },
        resolve: n => div(2 * Math.exp(-n), 1 + Math.exp(-2 * n))
    });

    factory.fixed("SERIESSUM", "val:num", 4, 4, ["val:num", "val:num", "val:num", ANY_MAT_PASS], fun.do(() => {

        const AGGREGATE_OPTIONS: NumberAggregatorOptions = {
            valMode: "rconvert",    // value operands: convert strings to numbers (but not booleans)
            matMode: "exact",       // matrix operands: do not accept strings or booleans
            refMode: "exact"        // reference operands: do not accept strings or booleans
            // emptyParam: false,   // skip empty parameter
            // multiRange: false,   // do not accept multi-range references
            // multiSheet: false    // do not accept multi-sheet references
        };

        // the finalizer callback: throw #N/A error code, if no coefficients are available
        function finalize(interm: number, count: number): number {
            return (count === 0) ? ErrorCode.NA.throw() : interm;
        }

        // the implementation of the SERIESSUM function
        return function (this: FormulaContext, x: number, n: number, m: number, c: Operand): number {
            const aggregate = (interm: number, a: number, i: number): number => interm + a * x ** (n + i * m);
            return this.aggregateNumbers([c], 0, aggregate, finalize, AGGREGATE_OPTIONS);
        };
    }));

    factory.unaryNum("SIGN", math.sgn);

    factory.unaryNum("SIN", Math.sin);

    factory.unaryNum("SINH", Math.sinh);

    factory.unaryNum("SQRT", Math.sqrt);

    factory.unaryNum("SQRTPI", n => Math.sqrt(Math.PI * n));

    factory.unbound("SUBTOTAL", "val:num", 2, 1, ["val:int", ANY_MAT_PASS], {
        recalc: "always", // function result depends on hidden state of rows
        resolve(func: number, ...operands: Operand[]): number {

            // get the function implementation (one-based to zero-based index!)
            if ((func < 1) || (func > 199)) { throw ErrorCode.VALUE; }
            const groupFunc = SUBTOTAL_FUNCTIONS[func % 100];
            if (!groupFunc) { throw ErrorCode.VALUE; }

            // Function indexes above 100 are used to skip manually hidden rows.
            // Bug 49375: If a filter is active (auto-filter or table), ALL hidden rows will
            // ALWAYS be skipped (not only filtered rows, but also all manually hidden rows).
            return groupFunc(this, operands, {
                skipHiddenRows: func >= 100,
                skipFilteredRows: true
            });
        }
    });

    factory.unbound("SUM", "val:num", 1, 1, [ANY_MAT_PASS], function (...operands: Operand[]) {
        return groupSum(this, operands, { collectFormats: true });
    });

    factory.ternary2("SUMIF", "val:num", ["ref:val", "val:any", "ref:val"], function () {
        return this.aggregateFilteredCells(false, 0, add);
    });

    factory.unbound("SUMIFS", "val:num", 3, 2, ["ref:val", "ref:val", "val:any"], function () {
        return this.aggregateFilteredCells(true, 0, add);
    });

    factory.unbound("SUMSQ", "val:num", 1, 1, [ANY_MAT_PASS], function (...operands: Operand[]) {
        return this.aggregateNumbers(operands, 0, (i, n) => i + n * n, fun.identity, SKIP_MAT_SKIP_REF_SKIP_EMPTY);
    });

    factory.unaryNum("TAN", Math.tan);

    factory.unaryNum("TANH", Math.tanh);

    factory.binary1NumInt("TRUNC", (n, digits) => dround(n, digits || 0, "trunc"));
});
