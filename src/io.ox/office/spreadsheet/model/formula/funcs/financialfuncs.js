/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all financial spreadsheet functions.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { is, math, fun } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { InternalErrorCode } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import { div } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import { getYearFracData, getYearFrac, countDays, addDaysToDate, addMonthsToDate, addYearsToDate, makeUTCDate } from "@/io.ox/office/spreadsheet/model/formula/utils/dateutils";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

const { floor, ceil, pow, log } = Math;

// constants ==================================================================

// standard options for date parameter iterators
const RCONVERT_ALL_SKIP_EMPTY = { // id: RRR0
    valMode: "rconvert", // value operands: convert strings to numbers (but not booleans)
    matMode: "rconvert", // matrix operands: convert strings to numbers (but not booleans)
    refMode: "rconvert", // reference operands: convert strings to numbers (but not booleans)
    emptyParam: false, // skip empty parameter
    multiRange: false, // do not accept multi-range references
    multiSheet: false  // do not accept multi-sheet references
};

const RCONVERT_ALL_TO_DATE_SKIP_EMPTY = { // id: RRR0
    valMode: "rconvert", // value operands: convert strings to numbers (but not booleans)
    matMode: "rconvert", // matrix operands: convert strings to numbers (but not booleans)
    refMode: "rconvert", // reference operands: convert strings to numbers (but not booleans)
    emptyParam: false, // skip empty parameter
    multiRange: false, // do not accept multi-range references
    multiSheet: false, // do not accept multi-sheet references
    dates: true, // convert numbers to dates
    floor: true // remove time components from all dates
};

// options for the functions IRR, MIRR, etc.
const IRR_ITERATOR_OPTIONS = { // id: ESS5
    valMode: "exact", // value operands: do not accept strings or booleans
    matMode: "skip", // matrix operands: skip strings and booleans
    refMode: "skip", // reference operands: skip strings and booleans
    emptyParam: true, // do not skip empty parameter
    multiRange: true, // accept multi-range references
    multiSheet: true  // accept multi-sheet references
};

// options for the NPV function
const NPV_ITERATOR_OPTIONS = { // id: CSS5
    valMode: "convert", // value operands: convert strings and booleans to numbers
    matMode: "skip", // matrix operands: skip strings and booleans
    refMode: "skip", // reference operands: skip strings and booleans
    emptyParam: true, // do not skip empty parameter
    multiRange: true, // accept multi-range references
    multiSheet: true  // accept multi-sheet references
};

// private functions ==========================================================

function log1p(x) {
    return log(1 + x);
}

function modF(number) {
    return number - floor(number);
}

/**
 * @param {Date} startDate
 *  The start date, as UTC date value.
 *
 * @param {Date} endDate
 *  The end date, as UTC date value. If this date is located before the
 *  start date, the dates will be swapped internally (the result will never
 *  be negative).
 *
 * @param {Number} [dateMode=0]
 *  The date mode to be used to calculate the number of years. See method
 *  DateUtils.getYearFracData() for details.
 */
function getDaysDiff(startDate, endDate, dateMode) {
    return getYearFracData(startDate, endDate, dateMode).days;
}

/*
 * private global functions copied from openoffice interpr2.cxx
 * https://github.com/LibreOffice/core/blob/3a2037b11c28fdf9409a336f8086790bc6f8f86f/sc/source/core/tool/interpr2.cxx
 */

/* ScGetGDA */
function getGDA(fValue, fRest, fTimeLength, fPeriod, fFactor) {
    var fInterest, fOldValue, fNewValue;
    fInterest = fFactor / fTimeLength;
    if (fInterest >= 1) {
        fInterest = 1;
        if (fPeriod === 1) {
            fOldValue = fValue;
        } else {
            fOldValue = 0;
        }
    } else {
        fOldValue = fValue * pow(1 - fInterest, fPeriod - 1);
    }
    fNewValue = fValue * pow(1 - fInterest, fPeriod);

    return Math.max(0, fOldValue - Math.max(fNewValue, fRest));
}

/* ScGetCompoundInterest */
function getCompoundInterest(fInterest, fZr, fZzr, fBw, fZw, fF) {
    var fRmz = getRmz(fInterest, fZzr, fBw, fZw, fF);     // fuer kapz auch bei fZr === 1
    var fCompoundInterest;
    if (fZr === 1) {
        if (fF > 0) {
            fCompoundInterest = 0;
        } else {
            fCompoundInterest = -fBw;
        }
    } else {
        if (fF > 0) {
            fCompoundInterest = getZw(fInterest, fZr - 2, fRmz, fBw, 1) - fRmz;
        } else {
            fCompoundInterest = getZw(fInterest, fZr - 1, fRmz, fBw, 0);
        }
    }
    return fCompoundInterest * fInterest;
}

function interVDB(fValue, fRest, fTimeLength, fTimeLength1, fPeriod, fFactor) {
    var fVdb = 0;
    var fIntEnd = ceil(fPeriod);
    var nLoopEnd = fIntEnd;

    var fTerm, fLia;
    var fSalvageValue = fValue - fRest;
    var bNowLia = false;

    var fGda;
    var i;
    fLia = 0;
    for (i = 1; i <= nLoopEnd; i++) {
        if (!bNowLia) {
            fGda = getGDA(fValue, fRest, fTimeLength, i, fFactor);
            fLia = fSalvageValue / (fTimeLength1 - (i - 1));

            if (fLia > fGda) {
                fTerm = fLia;
                bNowLia = true;
            } else {
                fTerm = fGda;
                fSalvageValue -= fGda;
            }
        } else {
            fTerm = fLia;
        }

        if (i === nLoopEnd) {
            fTerm *= (fPeriod + 1 - fIntEnd);
        }

        fVdb += fTerm;
    }
    return fVdb;
}

function getDuration(settlement, maturity, coupon, fYield, frequency, mode) {
    if (coupon < 0 || fYield < 0 || settlement >= maturity) { throw ErrorCode.NUM; }
    ensureFrequency(frequency);
    mode = getModeFromParam(mode);

    var fYearfrac = getYearFrac(settlement, maturity, mode);
    var fNumOfCoups = getCoupnum(settlement, maturity, frequency, mode);
    var fDur = 0;
    var f100 = 100;
    coupon *= f100 / frequency;    // coupon is used as cash flow
    fYield /= frequency;
    fYield += 1;

    var nDiff = fYearfrac * frequency - fNumOfCoups;

    var t;

    for (t = 1; t < fNumOfCoups; t++) {
        fDur += (t + nDiff) * (coupon) / pow(fYield, t + nDiff);
    }

    fDur += (fNumOfCoups + nDiff) * (coupon + f100) / pow(fYield, fNumOfCoups + nDiff);

    var p = 0;
    for (t = 1; t < fNumOfCoups; t++) {
        p += coupon / pow(fYield, t + nDiff);
    }

    p += (coupon + f100) / pow(fYield, fNumOfCoups + nDiff);

    fDur /= p;
    fDur /= frequency;

    return fDur;
}

/* Calculates the resulting amount for the passed interest rate and the given XIRR parameters. */
function sca_XirrResult(rValues, rDates, fRate) {
    /*  V_0 ... V_n = input values.
        D_0 ... D_n = input dates.
        R           = input interest rate.
        r   := R+1
        E_i := (D_i-D_0) / 365
                    n    V_i                n    V_i
        f(R)  =  SUM   -------  =  V_0 + SUM   ------- .
                    i=0  r^E_i              i=1  r^E_i
    */
    var D_0 = rDates[0];
    var r = fRate + 1;
    var fResult = rValues[0];
    for (var i = 1, nCount = rValues.length; i < nCount; ++i) {
        fResult += rValues[i] / pow(r, countDays(D_0, rDates[i]) / 365);
    }
    return fResult;
}

/*
    * Calculates the first derivation of lcl_sca_XirrResult().
    * lcl_sca_XirrResult_Deriv1()
    */
function sca_XirrResult_Deriv1(rValues, rDates, fRate) {
    /*  V_0 ... V_n = input values.
        D_0 ... D_n = input dates.
        R           = input interest rate.
        r   := R+1
        E_i := (D_i-D_0) / 365
                                n    V_i
        f'(R)  =  [ V_0 + SUM   ------- ]'
                            i=1  r^E_i
                            n           V_i                 n    E_i V_i
                =  0 + SUM   -E_i ----------- r'  =  - SUM   ----------- .
                        i=1       r^(E_i+1)             i=1  r^(E_i+1)
    */
    var D_0 = rDates[0];
    var r = fRate + 1;
    var fResult = 0;
    for (var i = 1, nCount = rValues.length; i < nCount; ++i) {
        var E_i = countDays(D_0, rDates[i]) / 365;
        fResult -= E_i * rValues[i] / pow(r, E_i + 1);
    }
    return fResult;
}

/*
    * ScGetBw
    */
function getBw(fInterest, fZzr, fRmz, fZw, fF) {
    var fBw;
    if (fInterest === 0) {
        fBw = fZw + fRmz * fZzr;
    } else if (fF > 0) {
        fBw = (fZw * pow(1 + fInterest, -fZzr)) + (fRmz * (1 - pow(1 + fInterest, -fZzr + 1)) / fInterest) + fRmz;
    } else {
        fBw = (fZw * pow(1 + fInterest, -fZzr)) + (fRmz * (1 - pow(1 + fInterest, -fZzr)) / fInterest);
    }
    return -fBw;
}

/*
    * private global functions copied from openoffice analysishelper.cxx
    * https://github.com/LibreOffice/core/blob/db17d3c17c40d6b0e92392cf3c6e343d1d17b771/scaddins/source/analysis/analysishelper.cxx
    */

function getRmz(fZins, fZzr, fBw, fZw, nF) {
    var fRmz;
    if (fZins === 0) {
        fRmz = (fBw + fZw) / fZzr;
    } else {
        var fTerm = pow(1 + fZins, fZzr);
        if (nF > 0) {
            fRmz = (fZw * fZins / (fTerm - 1) + fBw * fZins / (1 - 1 / fTerm)) / (1 + fZins);
        } else {
            fRmz = fZw * fZins / (fTerm - 1) + fBw * fZins / (1 - 1 / fTerm);
        }
    }
    return -fRmz;
}

function getZw(fZins, fZzr, fRmz, fBw, nF) {
    var fZw;
    if (fZins === 0) {
        fZw = fBw + fRmz * fZzr;
    } else {
        var fTerm = pow(1 + fZins, fZzr);
        if (nF > 0) {
            fZw = fBw * fTerm + fRmz * (1 + fZins) * (fTerm - 1) / fZins;
        } else {
            fZw = fBw * fTerm + fRmz * (fTerm - 1) / fZins;
        }
    }
    return -fZw;
}

/**
 * COUPDAYBS: get day count: coupon date before settlement <-> settlement
 */
function getCoupdaybs(nSettle, nMat, nFreq, nBase) {
    if (nSettle >= nMat) {
        throw ErrorCode.NUM;
    }

    var date = getCouppcd(nSettle,  nMat, nFreq);
    return getDaysDiff(date, nSettle, nBase);
}

/**
 *  COUPDAYS: get day count: coupon date before settlement <-> coupon date after settlement
 */
function getCoupdays(nSettle, nMat, nFreq, nBase) {
    if (nSettle >= nMat) {
        throw ErrorCode.NUM;
    }
    if (nBase === 1) {
        var date = getCouppcd(nSettle, nMat, nFreq);
        var nextDate = addMonthsToDate(date, 12 / nFreq);
        return getDaysDiff(date, nextDate, nBase);
    }
    return getYearFracData(nSettle, nMat, nBase).daysPerYear / nFreq;
}

/**
 * COUPNUM: get count of coupon dates
 */
function getCoupnum(nSettle, nMat, nFreq/*,  nBase */) {
    //TODO: use nBase ?
    if (nSettle >= nMat) {
        throw ErrorCode.NUM;
    }

    var date = getCouppcd(nSettle, nMat, nFreq);
    var months = (nMat.getUTCFullYear() - date.getUTCFullYear()) * 12 + nMat.getUTCMonth() - date.getUTCMonth();
    return months * nFreq / 12;
}

/**
 * COUPNCD: find first coupon date after settlement (is never equal to settlement)
 */
function getCoupncd(rSettle, rMat, nFreq) {
    var date = makeUTCDate({ Y: rSettle.getUTCFullYear(), M: rMat.getUTCMonth(), D: rMat.getUTCDate() });
    if (date > rSettle) {
        date = addYearsToDate(date, -1);
    }
    //FIXME: better calculate instead of using loops!!!
    while (date <= rSettle) {
        date = addMonthsToDate(date, 12 / nFreq);
    }
    return date;
}

/**
 * COUPPCD: find last coupon date before settlement (can be equal to settlement)
 */
function getCouppcd(rSettle, rMat, nFreq) {
    var date = makeUTCDate({ Y: rSettle.getUTCFullYear(), M: rMat.getUTCMonth(), D: rMat.getUTCDate() });
    if (date < rSettle) {
        date = addYearsToDate(date, 1);
    }
    //FIXME: better calculate instead of using loops!!!
    while (date > rSettle) {
        date = addMonthsToDate(date, -12 / nFreq);
    }
    return date;
}

/**
 * COUPDAYSNC: get day count: settlement <-> coupon date after settlement
 * GetCoupdaysnc()
 */
function getCoupdaysnc(nSettle, nMat, nFreq, nBase) {
    if (nSettle >= nMat) {
        throw ErrorCode.NUM;
    }

    if ((nBase !== 0) && (nBase !== 4)) {
        var date = getCoupncd(nSettle,  nMat, nFreq);
        return getDaysDiff(nSettle, date, nBase);
    }
    return getCoupdays(nSettle, nMat, nFreq, nBase) - getCoupdaybs(nSettle, nMat, nFreq, nBase);
}

/**
 * getPrice_()
 */
function getPrice(nSettle, nMat, fRate, fYield, fRedemp, nFreq, nBase) {
    var fFreq = nFreq;

    var fE = getCoupdays(nSettle, nMat, nFreq, nBase);
    var fDSC_E = getCoupdaysnc(nSettle, nMat, nFreq, nBase) / fE;
    var fN = getCoupnum(nSettle, nMat, nFreq, nBase);
    var fA = getCoupdaybs(nSettle, nMat, nFreq, nBase);

    var fRet = fRedemp / (pow(1 + fYield / fFreq, fN - 1 + fDSC_E));
    fRet -= 100 * fRate / fFreq * fA / fE;

    var fT1 = 100 * fRate / fFreq;
    var fT2 = 1 + fYield / fFreq;

    for (var fK = 0; fK < fN; fK++) {
        fRet += fT1 / pow(fT2, fK + fDSC_E);
    }

    return fRet;
}

/**
 * getYield_()
 */
function getYield(nSettle, nMat, fCoup, fPrice, fRedemp, nFreq, nBase) {
    var fRate = fCoup;
    var fPriceN = 0;
    var fYield1 = 0;
    var fYield2 = 1;
    var fPrice1 = getPrice(nSettle, nMat, fRate, fYield1, fRedemp, nFreq, nBase);
    var fPrice2 = getPrice(nSettle, nMat, fRate, fYield2, fRedemp, nFreq, nBase);
    var fYieldN = (fYield2 - fYield1) * 0.5;

    for (var nIter = 0; nIter < 100 && fPriceN !== fPrice; nIter++) {
        fPriceN = getPrice(nSettle, nMat, fRate, fYieldN, fRedemp, nFreq, nBase);

        if (fPrice === fPrice1) {
            return fYield1;
        } else if (fPrice === fPrice2) {
            return fYield2;
        } else if (fPrice === fPriceN) {
            return fYieldN;
        } else if (fPrice < fPrice2) {
            fYield2 *= 2;
            fPrice2 = getPrice(nSettle, nMat, fRate, fYield2, fRedemp, nFreq, nBase);

            fYieldN = (fYield2 - fYield1) * 0.5;
        } else {
            if (fPrice < fPriceN) {
                fYield1 = fYieldN;
                fPrice1 = fPriceN;
            } else {
                fYield2 = fYieldN;
                fPrice2 = fPriceN;
            }

            fYieldN = fYield2 - (fYield2 - fYield1) * ((fPrice - fPrice2) / (fPrice1 - fPrice2));
        }
    }

    if (Math.abs(fPrice - fPriceN) > fPrice / 100) {
        throw ErrorCode.NUM; // result not precise enough
    }

    return fYieldN;
}

/*
    *  https://github.com/LibreOffice/core/blob/3a2037b11c28fdf9409a336f8086790bc6f8f86f/sc/source/core/tool/interpr2.cxx
    *  RateIteration
    */
function rateIteration(fNper, fPayment, fPv, fFv, fPayType, fGuess) {
    // See also #i15090#
    // Newton-Raphson method: x(i+1) = x(i) - f(x(i)) / f'(x(i))
    // This solution handles integer and non-integer values of Nper different.
    // If ODFF will constraint Nper to integer, the distinction of cases can be
    // removed; only the integer-part is needed then.
    var bValid = true, bFound = false;
    var fX, fXnew, fTerm, fTermDerivation;
    var fGeoSeries, fGeoSeriesDerivation;
    var nIterationsMax = 150;
    var nCount = 0;
    var fEpsilonSmall = 1E-14;
    var SCdEpsilon = 1E-7;
    // convert any fPayType situation to fPayType === zero situation
    fFv -= fPayment * fPayType;
    fPv += fPayment * fPayType;
    if (fNper === Math.round(fNper)) { // Nper is an integer value
        fX = fGuess;
        while (!bFound && nCount < nIterationsMax) {
            var fPowN, fPowNminus1;  // for (1+fX)^Nper and (1+fX)^(Nper-1)
            fPowNminus1 = pow(1 + fX, fNper - 1);
            fPowN = fPowNminus1 * (1 + fX);
            if (math.isZero(fX)) {
                fGeoSeries = fNper;
                fGeoSeriesDerivation = fNper * (fNper - 1) / 2;
            } else {
                fGeoSeries = (fPowN - 1) / fX;
                fGeoSeriesDerivation = fNper * fPowNminus1 / fX - fGeoSeries / fX;
            }
            fTerm = fFv + fPv * fPowN + fPayment * fGeoSeries;
            fTermDerivation = fPv * fNper * fPowNminus1 + fPayment * fGeoSeriesDerivation;

            if (Math.abs(fTerm) < fEpsilonSmall) {
                bFound = true;  // will catch root which is at an extreme
            } else {
                if (math.isZero(fTermDerivation)) {
                    fXnew = fX + 1.1 * SCdEpsilon;  // move away from zero slope
                } else {
                    fXnew = fX - fTerm / fTermDerivation;
                }
                nCount++;
                // more accuracy not possible in oscillating cases
                bFound = (Math.abs(fXnew - fX) < SCdEpsilon);
                fX = fXnew;
            }
        }
        // Gnumeric returns roots < -1, Excel gives an error in that cases,
        // ODFF says nothing about it. Enable the statement, if you want Excel's
        // behavior.
        bValid = (fX >= -1 + SCdEpsilon);
        //" + SCdEpsilon " as workaround for error in porting code from C++ to js
        // Update 2013-06-17: Gnumeric (v1.12.2) doesn't return roots <= -1
        // anymore.
        //bValid = (fX > -1);
    } else { // Nper is not an integer value.
        fX = (fGuess < -1) ? -1 : fGuess;   // start with a valid fX
        while (bValid && !bFound && nCount < nIterationsMax) {
            if (math.isZero(fX)) {
                fGeoSeries = fNper;
                fGeoSeriesDerivation = fNper * (fNper - 1) / 2;
            } else {
                fGeoSeries = (pow(1 + fX, fNper) - 1) / fX;
                fGeoSeriesDerivation = fNper * pow(1 + fX, fNper - 1) / fX - fGeoSeries / fX;
            }
            fTerm = fFv + fPv * pow(1 + fX, fNper) + fPayment * fGeoSeries;
            fTermDerivation = fPv * fNper * pow(1 + fX, fNper - 1) + fPayment * fGeoSeriesDerivation;
            if (Math.abs(fTerm) < fEpsilonSmall) {
                bFound = true;  // will catch root which is at an extreme
            } else {
                if (math.isZero(fTermDerivation)) {
                    fXnew = fX + 1.1 * SCdEpsilon;  // move away from zero slope
                } else {
                    fXnew = fX - fTerm / fTermDerivation;
                }
                nCount++;
                // more accuracy not possible in oscillating cases
                bFound = (Math.abs(fXnew - fX) < SCdEpsilon);
                fX = fXnew;
                bValid = (fX >= -1 + SCdEpsilon);  // otherwise pow(1+fX,fNper) will fail
                //" + SCdEpsilon " as workaround for error in porting code from C++ to js
            }
        }
    }
    fGuess = fX;    // return approximate root
    return {
        valid: bValid && bFound,
        guess: fGuess
    };
}

function ensureFrequency(frequency) {
    if (frequency !== 1 && frequency !== 2 && frequency !== 4) { throw ErrorCode.NUM; }
}

function ensurePeriods(period, periodCount) {
    if (periodCount <= 0 || period < 1 || period > periodCount) { throw ErrorCode.NUM; }
}

function getModeFromParam(mode) {
    if (mode < 0 || mode > 4) { throw ErrorCode.NUM; }
    if (!is.number(mode)) { mode = 0; }
    return mode;
}

function getPeriodTypeFromParam(periodType) {
    if (!is.number(periodType)) {
        periodType = 0;
    } else if (periodType !== 0 && periodType !== 1) {
        periodType = 1;
    }
    return periodType;
}

// exports ====================================================================

export default declareFunctionSpecModule("financial", factory => {

    factory.fixed("ACCRINT", "val:num", 6, { ooxml: 8, odf: 7 }, ["val:date", "val:date", "val:date", "val:num", "val:num", "val:num", "val:int", "val:bool"],
        function (issue, _firstInterest, settlement, rate, par, frequency, mode, calcMethod) {
            ensureFrequency(frequency);

            if (rate <= 0 || par <= 0 || issue >= settlement) { throw ErrorCode.NUM; }

            if (!is.boolean(calcMethod)) { calcMethod = true; }
            if (calcMethod === false) { throw InternalErrorCode.UNSUPPORTED; }

            return par * rate * getYearFrac(issue, settlement, mode);
        }
    );

    factory.fixed("ACCRINTM", "val:num", 4, 5, ["val:date", "val:date", "val:num", "val:num", "val:int"], function (issue, settlement, rate, par, mode) {
        if ((rate <= 0) || (par <= 0) || (issue >= settlement)) { throw ErrorCode.NUM; }
        return rate * par *  getYearFrac(issue, settlement, mode);
    });

    factory.fixed("AMORDEGRC", "val:num", 6, 7, ["val:num", "val:date", "val:date", "val:num", "val:num", "val:num", "val:int"],
        function (cost, purchasedDate, firstEndDate, salvage, period, rate, mode) {
            if (purchasedDate > firstEndDate || rate <= 0 || salvage < 0 || cost <= 0 || salvage > cost) { throw ErrorCode.NUM; }
            if (mode === 2) { throw ErrorCode.NUM; }

            var nPer =  period;
            var fUsePer = 1 / rate;
            var fAmorCoeff;

            if (fUsePer < 3) {
                fAmorCoeff = 1;
            } else if (fUsePer < 5) {
                fAmorCoeff = 1.5;
            } else if (fUsePer <= 6) {
                fAmorCoeff = 2;
            } else {
                fAmorCoeff = 2.5;
            }
            rate *= fAmorCoeff;
            var fNRate = Math.round(getYearFrac(purchasedDate, firstEndDate, mode) * rate * cost);
            cost -= fNRate;
            var fRest = cost - salvage;   // aboriginal cost - residual value - sum of all write-downs

            for (var n = 0; n < nPer; n++) {
                fNRate = Math.round(rate * cost);
                fRest -= fNRate;

                if (fRest < 0) {
                    switch (nPer - n) {
                        case 0:
                        case 1:
                            return Math.round(cost * 0.5);
                        default:
                            return 0;
                    }
                }

                cost -= fNRate;
            }

            return fNRate;
        }
    );

    factory.fixed("AMORLINC", "val:num", 6, 7, ["val:num", "val:date", "val:date", "val:num", "val:num", "val:num", "val:int"],
        function (cost, purchasedDate, firstEndDate, salvage, period, rate, mode) {
            if (purchasedDate > firstEndDate || rate <= 0 || salvage < 0 || cost <= 0 || salvage > cost) { throw ErrorCode.NUM; }
            if (mode === 2) { throw ErrorCode.NUM; }

            var nPer = period;
            var fOneRate = cost * rate;
            var fCostDelta = cost - salvage;
            var f0Rate = getYearFrac(purchasedDate, firstEndDate, mode) * rate * cost;
            var nNumOfFullPeriods =  (cost - salvage - f0Rate) / fOneRate;

            if (period === 0) { return f0Rate; }
            if (nPer <= nNumOfFullPeriods) { return fOneRate; }
            if (nPer === nNumOfFullPeriods + 1) { return fCostDelta - fOneRate * nNumOfFullPeriods - f0Rate; }
            return 0;
        }
    );

    factory.fixed("COUPDAYBS", "val:num", 3, 4, ["val:day", "val:day", "val:int", "val:int"], function (settlement, maturity, frequency, mode) {
        if (settlement >= maturity) { throw ErrorCode.NUM; }
        ensureFrequency(frequency);
        mode = getModeFromParam(mode);
        return getCoupdaybs.call(this, settlement, maturity, frequency, mode);
    });

    factory.fixed("COUPDAYS", "val:num", 3, 4, ["val:day", "val:day", "val:int", "val:int"], function (settlement, maturity, frequency, mode) {
        if (settlement >= maturity) { throw ErrorCode.NUM; }
        ensureFrequency(frequency);
        mode = getModeFromParam(mode);
        return getCoupdays.call(this, settlement, maturity, frequency, mode);
    });

    factory.fixed("COUPDAYSNC", "val:num", 3, 4, ["val:day", "val:day", "val:int", "val:int"], function (settlement, maturity, frequency, mode) {
        if (settlement >= maturity) { throw ErrorCode.NUM; }
        ensureFrequency(frequency);
        mode = getModeFromParam(mode);
        return getCoupdaysnc.call(this, settlement, maturity, frequency, mode);
    });

    factory.fixed("COUPNCD", "val:num", 3, 4, ["val:day", "val:day", "val:int", "val:int"], function (settlement, maturity, frequency/*, mode*/) {
        if (settlement >= maturity) { throw ErrorCode.NUM; }
        ensureFrequency(frequency);
        //mode = getModeFromParam(mode);
        return getCoupncd.call(this, settlement, maturity, frequency);
    });

    factory.fixed("COUPNUM", "val:num", 3, 4, ["val:day", "val:day", "val:int", "val:int"], function (settlement, maturity, frequency, mode) {
        if (settlement >= maturity) { throw ErrorCode.NUM; }
        ensureFrequency(frequency);
        mode = getModeFromParam(mode);
        return getCoupnum.call(this, settlement, maturity, frequency, mode);
    });

    factory.fixed("COUPPCD", "val:num", 3, 4, ["val:day", "val:day", "val:int", "val:int"], function (settlement, maturity, frequency/*, mode*/) {
        if (settlement >= maturity) { throw ErrorCode.NUM; }
        ensureFrequency(frequency);
        //mode = getModeFromParam(mode);
        return getCouppcd(settlement, maturity, frequency);
    });

    factory.fixed("CUMIPMT", "val:num", 6, 6, ["val:num", "val:int", "val:num", "val:int", "val:int", "val:int"],
        function (rate, periodCount, presValue, startPeriod, endPeriod, payType) {
            var fRmz, fZinsZ;

            if (startPeriod < 1 || endPeriod < startPeriod || rate <= 0 || endPeriod > periodCount  || periodCount <= 0 || presValue <= 0 || (payType !== 0 && payType !== 1)) {
                throw ErrorCode.NUM;
            }

            fRmz = getRmz(rate, periodCount, presValue, 0, payType);

            fZinsZ = 0;

            var  nStart =  startPeriod;
            var  nEnd =  endPeriod;

            if (nStart === 1) {
                if (payType <= 0) { fZinsZ = -presValue; }
                nStart++;
            }

            for (var i = nStart; i <= nEnd; i++) {
                if (payType > 0) {
                    fZinsZ += getZw(rate,  i - 2, fRmz, presValue, 1) - fRmz;
                } else {
                    fZinsZ += getZw(rate,  i - 1, fRmz, presValue, 0);
                }
            }
            return fZinsZ * rate;
        }
    );

    factory.fixed("CUMPRINC", "val:num", 6, 6, ["val:num", "val:int", "val:num", "val:int", "val:int", "val:int"],
        function (rate, periodCount, presValue, startPeriod, endPeriod, payType) {
            var fRmz, fKapZ;

            if (startPeriod < 1 || endPeriod < startPeriod || rate <= 0 || endPeriod > periodCount  || periodCount <= 0 || presValue <= 0 || (payType !== 0 && payType !== 1)) {
                throw ErrorCode.NUM;
            }

            fRmz = getRmz(rate, periodCount, presValue, 0, payType);

            fKapZ = 0;

            var nStart = startPeriod;
            var nEnd = endPeriod;

            if (nStart === 1) {
                if (payType <= 0) {
                    fKapZ = fRmz + presValue * rate;
                } else {
                    fKapZ = fRmz;
                }
                nStart++;
            }
            for (var i = nStart; i <= nEnd; i++) {
                if (payType > 0) {
                    fKapZ += fRmz - (getZw(rate, i - 2, fRmz, presValue, 1) - fRmz) * rate;
                } else {
                    fKapZ += fRmz - getZw(rate, i - 1, fRmz, presValue, 0) * rate;
                }
            }

            return fKapZ;
        }
    );

    factory.fixed("DB", "val:num", 4, 5, ["val:num", "val:num", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(cost, salvage, life, period, months) {
            if (cost < 0 || salvage < 0 || life < 0 || period < 0) { throw ErrorCode.NUM; }
            if (months < 1 || months > 12 || life > 1200 || period > (life + 1) || salvage > cost) { throw ErrorCode.NUM; }
            if (!is.number(months)) { months = 12; }

            var nOffRate = 1 - pow(salvage / cost, 1 / life);
            nOffRate = floor((nOffRate * 1000) + 0.5) / 1000;
            var nFirstOffRate = cost * nOffRate * months / 12;
            var nGda2 = 0;
            if (floor(period) === 1) {
                nGda2 = nFirstOffRate;
            } else {
                var nSumOffRate = nFirstOffRate;
                var nMin = life;
                if (nMin > period) { nMin = period; }
                var iMax = floor(nMin);
                for (var i = 2; i <= iMax; i++) {
                    nGda2 = (cost - nSumOffRate) * nOffRate;
                    nSumOffRate += nGda2;
                }
                if (period > life) {
                    nGda2 = ((cost - nSumOffRate) * nOffRate * (12 - months)) / 12;
                }
            }
            return nGda2;
        }
    });

    factory.fixed("DDB", "val:num", 4, 5, ["val:num", "val:num", "val:num", "val:num", "val:num"], {
        format: "currency",
        resolve(cost, salvage, life, period, factor) {
            if (cost < 0 || salvage < 0 || life < 0 || period < 0 || factor < 0) { throw ErrorCode.NUM; }
            if (!is.number(factor)) { factor = 2; }
            return getGDA(cost, salvage, life, period, factor);
        }
    });

    factory.fixed("DISC", "val:num", 4, 5, ["val:date", "val:date", "val:num", "val:num", "val:int"],
        function (settlement, maturity, price, discount, mode) {
            if (price <= 0 || discount <= 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            mode = getModeFromParam(mode);
            return (1 - price / discount) / getYearFrac(settlement, maturity, mode);
        }
    );

    factory.binaryNumInt("DOLLARDE", (value, fraction) => {
        if (fraction <= 0) { throw ErrorCode.NUM; }
        return modF(value) / fraction * pow(10, ceil(Math.log10(fraction))) + floor(value);
    });

    factory.binaryNumInt("DOLLARFR", (value, fraction) => {
        if (fraction <= 0) { throw ErrorCode.NUM; }
        return modF(value) * fraction * pow(10, -ceil(Math.log10(fraction))) + floor(value);
    });

    factory.fixed("DURATION", "val:num", 5, 6, ["val:date", "val:date", "val:num", "val:num", "val:int", "val:int"],
        function (settlement, maturity, coupon, fYield, frequency, mode) {
            return getDuration.call(this, settlement, maturity, coupon, fYield, frequency, mode);
        }
    );

    factory.binaryNumInt("EFFECT", {
        name: { odf: "EFFECTIVE" },
        resolve(rate, period) {
            if ((rate <= 0) || (period < 1)) { throw ErrorCode.NUM; }
            return (1 + rate / period) ** period - 1;
        }
    });

    factory.fixed("EUROCONVERT", "val:num", 3, 4, ["val:num", "val:str", "val:str", "val:bool"], {
        name: { ooxml: null }
    });

    factory.fixed("FV", "val:num", 3, 5, ["val:num", "val:num", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(rate, periodCount, pmt, currentValue, payType) {
            if (payType !== 0 && payType !== 1) { payType = 0; }
            if (!is.number(currentValue)) { currentValue = 0; }
            return getZw(rate, periodCount, pmt, currentValue, payType);
        }
    });

    factory.binary("FVSCHEDULE", "val:num", ["val:num", "any"], {
        resolve(principal, scheduleParam) {
            this.forEachNumber(scheduleParam, sched => { principal *= 1 + sched; }, {
                valMode: "rconvert", // value operands: convert strings to numbers (but not booleans)
                matMode: "exact", // matrix operands: convert strings to numbers (but not booleans)
                refMode: "exact", // reference operands: convert strings to numbers (but not booleans)
                emptyParam: false, // skip empty parameter
                multiRange: false, // do not accept multi-range references
                multiSheet: false  // do not accept multi-sheet references
            });
            return principal;
        }
    });

    factory.fixed("INTRATE", "val:num", 4, 5, ["val:date", "val:date", "val:num", "val:num", "val:int"],
        function (settlement, maturity, price, discount, mode) {
            if (price <= 0 || discount <= 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            mode = getModeFromParam(mode);
            return ((discount / price) - 1) / getYearFrac(settlement, maturity, mode);
        }
    );

    factory.fixed("IPMT", "val:num", 4, 6, ["val:num", "val:num", "val:num", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(rate, period, periodCount, currentValue, futureValue, periodType) {
            if (rate <= 0) { throw ErrorCode.NUM; }
            ensurePeriods(period, periodCount);
            if (!is.number(futureValue)) { futureValue = 0; }
            periodType = getPeriodTypeFromParam(periodType);
            return getCompoundInterest(rate, period, periodCount, currentValue, futureValue, periodType);
        }
    });

    factory.binary1("IRR", "val:num", ["any", "val:num"], {
        format: "percent",
        resolve(values, guess) {

            // collect the numbers from the operand (needed to process multiple times)
            var numbers = this.getNumbersAsArray(values, IRR_ITERATOR_OPTIONS);
            if (numbers.length < 2) { throw ErrorCode.NUM; }

            var EPSILON = 1E-7;
            var ITERATIONS = 20;

            // default guess is 10%
            if (this.isMissingOperand(1)) { guess = 0.1; }

            var x = (guess === -1) ? 0.1 : guess;
            var diff = 1;

            // callback functions cannot be defined in for-loops
            function calcNextX() {
                var numer = 0;
                var denom = 0;
                numbers.forEach(function (number, index) {
                    numer += number / pow(x + 1, index);
                    denom -= index * number / pow(x + 1, index + 1);
                });
                return x - numer / denom;
            }

            for (var i = 0; (diff > EPSILON) && (i < ITERATIONS); i += 1) {
                var xNew = calcNextX();
                diff = Math.abs(xNew - x);
                if (!Number.isFinite(diff)) { throw ErrorCode.NUM; }
                x = xNew;
            }

            if (diff >= EPSILON) { throw ErrorCode.NUM; }
            return ((guess === 0) && (Math.abs(x) < EPSILON)) ? 0 : x;
        }
    });

    factory.fixed("ISPMT", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:num"], function (rate, period, periodCount, currentValue) {
        if (rate <= 0) { throw ErrorCode.NUM; }
        ensurePeriods(period, periodCount);
        return currentValue * rate * (period / periodCount - 1);
    });

    factory.fixed("MDURATION", "val:num", 5, 6, ["val:date", "val:date", "val:num", "val:num", "val:int", "val:int"],
        function (settlement, maturity, coupon, fYield, frequency, mode) {
            const result = getDuration.call(this, settlement, maturity, coupon, fYield, frequency, mode);
            return result / (1 + (fYield / frequency));
        }
    );

    factory.ternary("MIRR", "val:num", ["any", "val:num", "val:num"], {
        format: "percent",
        resolve(values, financeRate, reInvestRate) {
            if (reInvestRate === -1) { throw ErrorCode.DIV0; }

            var fRate1_reinvest = reInvestRate + 1;
            var fRate1_invest = financeRate + 1;

            var fNPV_reinvest = 0;
            var fPow_reinvest = 1;
            var fNPV_invest = 0;
            var fPow_invest = 1;

            var count = this.forEachNumber(values, function (value) {
                if (value > 0) {          // reinvestments
                    fNPV_reinvest += value * fPow_reinvest;
                } else if (value < 0) {     // investments
                    fNPV_invest += value * fPow_invest;
                }
                fPow_reinvest /= fRate1_reinvest;
                fPow_invest /= fRate1_invest;
            }, IRR_ITERATOR_OPTIONS);

            var fResult = -div(fNPV_reinvest, fNPV_invest);
            fResult *= pow(fRate1_reinvest, count - 1);
            fResult = pow(fResult, div(1, count - 1));
            return fResult - 1;
        }
    });

    factory.binaryNumInt("NOMINAL", (rate, periods) => {
        if (periods < 1 || rate <= 0) { throw ErrorCode.NUM; }
        return ((rate + 1) ** (1 / periods) - 1) * periods;
    });

    factory.fixed("NPER", "val:num", 3, 5, ["val:num", "val:num", "val:num", "val:num", "val:int"],
        function (rate, payment, currentValue, futureValue, periodType) {
            if (rate <= 0) { throw ErrorCode.NUM; }
            if (!is.number(futureValue)) { futureValue = 0; }
            periodType = getPeriodTypeFromParam(periodType);

            if (rate === 0) {
                return -(currentValue + futureValue) / payment;
            }
            if (periodType > 0) {
                return log(-(rate * futureValue - payment * (1 + rate)) / (rate * currentValue + payment * (1 + rate))) / log1p(rate);
            }
            return log(-(rate * futureValue - payment) / (rate * currentValue + payment)) / log1p(rate);
        }
    );

    factory.unbound("NPV", "val:num", 2, 1, ["val:num", "any"], {
        format: "currency",
        resolve(rate) {
            if (rate === -1) { throw ErrorCode.DIV0; }
            const aggregate = (sum, value, index) => sum + value / pow(rate + 1, index + 1);
            return this.aggregateNumbers(this.getOperands(1), 0, aggregate, fun.identity, NPV_ITERATOR_OPTIONS);
        }
    });

    factory.fixed("ODDFPRICE", "val:num", 8, 9, ["val:day", "val:day", "val:day", "val:day", "val:num", "val:num", "val:num", "val:int", "val:int"], { }); // not implemented in calc

    factory.fixed("ODDFYIELD", "val:num", 8, 9, ["val:day", "val:day", "val:day", "val:day", "val:num", "val:num", "val:num", "val:int", "val:int"], { }); // not implemented in calc

    factory.fixed("ODDLPRICE", "val:num", 7, 8, ["val:day", "val:day", "val:day", "val:num", "val:num", "val:num", "val:int", "val:int"], { }); // not implemented in calc

    factory.fixed("ODDLYIELD", "val:num", 7, 8, ["val:day", "val:day", "val:day", "val:num", "val:num", "val:num", "val:int", "val:int"],
        function (settlement, maturity, lastInterest, rate, price, discount, frequency, mode) {
            if (rate < 0 || price <= 0 || discount <= 0 || settlement >= maturity || lastInterest >= settlement) { throw ErrorCode.NUM; }
            ensureFrequency(frequency);
            mode = getModeFromParam(mode);

            var fFreq = frequency;
            var fDCi = getYearFrac(lastInterest, maturity, mode) * fFreq;
            var fDSCi = getYearFrac(settlement, maturity, mode) * fFreq;
            var fAi = getYearFrac(lastInterest, settlement, mode) * fFreq;

            //TODO: this formula is copied from libreoffice,
            //the result from excel is much more exactly!!!

            var y = discount + fDCi * 100 * rate / fFreq;
            y /= price + fAi * 100 * rate / fFreq;
            y--;
            y *= fFreq / fDSCi;

            return y;
        }
    );

    factory.ternaryNum("PDURATION", {
        name: { ooxml: "_xlfn.PDURATION" },
        resolve(rate, currentValue, futureValue) {
            if (rate <= 0 || currentValue <= 0 || futureValue <= 0) { throw ErrorCode.NUM; }
            if (!is.number(futureValue)) { futureValue = 0; }

            return (log(futureValue) - log(currentValue)) / log1p(rate);
        }
    });

    factory.fixed("PMT", "val:num", 3, 5, ["val:num", "val:num", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(rate, periodCount, currentValue, futureValue, periodType) {
            if (rate <= 0 || periodCount <= 0) { throw ErrorCode.NUM; }
            if (!is.number(futureValue)) { futureValue = 0; }
            periodType = getPeriodTypeFromParam(periodType);
            return getRmz(rate, periodCount, currentValue, futureValue, periodType);
        }
    });

    factory.fixed("PPMT", "val:num", 4, 6, ["val:num", "val:num", "val:num", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(rate, period, periodCount, currentValue, futureValue, periodType) {
            if (rate <= 0) { throw ErrorCode.NUM; }
            ensurePeriods(period, periodCount);
            if (!is.number(futureValue)) { futureValue = 0; }
            periodType = getPeriodTypeFromParam(periodType);

            var rmz = getRmz(rate, periodCount, currentValue, futureValue, periodType);
            var interest = getCompoundInterest(rate, period, periodCount, currentValue, futureValue, periodType);
            return rmz - interest;
        }
    });

    factory.fixed("PRICE", "val:num", 6, 7, ["val:day", "val:day", "val:num", "val:num", "val:int", "val:int"], {
        format: "currency",
        resolve(settlement, maturity, rate, fYield, discount, frequency, mode) {
            if (discount <= 0 || rate < 0 || fYield  < 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            ensureFrequency(frequency);
            mode = getModeFromParam(mode);
            return getPrice.call(this, settlement, maturity, rate, fYield, discount, frequency, mode);
        }
    });

    factory.fixed("PRICEDISC", "val:num", 4, 5, ["val:day", "val:day", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(settlement, maturity, discount, redemption, mode) {
            if (discount <= 0 || redemption <= 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            mode = getModeFromParam(mode);
            return redemption * (1 - discount * getYearFrac(settlement, maturity, mode));
        }
    });

    factory.fixed("PRICEMAT", "val:num", 5, 6, ["val:day", "val:day", "val:day", "val:num", "val:int"], {
        format: "currency",
        resolve(settlement, maturity, issue, rate, fYield, mode) {
            if (rate < 0 || fYield  < 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            mode = getModeFromParam(mode);

            var fIssMat = getYearFrac(issue, maturity, mode);
            var fIssSet = getYearFrac(issue, settlement, mode);
            var fSetMat = getYearFrac(settlement, maturity, mode);

            var fRet = 1 + fIssMat * rate;
            fRet /= 1 + fSetMat * fYield;
            fRet -= fIssSet * rate;
            fRet *= 100;

            return fRet;
        }
    });

    factory.fixed("PV", "val:num", 3, 5, ["val:num", "val:num", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(rate, periodCount, pmt, futureValue, payType) {
            if (payType !== 0 && payType !== 1) { payType = 0; }
            if (!is.number(futureValue)) { futureValue = 0; }
            return getBw(rate, periodCount, pmt, futureValue, payType);
        }
    });

    factory.fixed("RATE", "val:num", 3, 6, ["val:num", "val:num", "val:num", "val:num", "val:int", "val:num"], {
        format: "percent",
        resolve(periodCount, pmt, currentValue, futureValue, payType, guess) {
            if (periodCount <= 0) { throw ErrorCode.NUM; }
            if (!is.number(payType)) { payType = 0; }
            if (payType !== 0 && payType !== 1) { payType = 1; }
            if (!is.number(futureValue)) { futureValue = 0; }

            var bDefaultGuess = true;
            var fGuess = 0.1;
            var fOrigGuess = 0.1;
            if (is.number(guess)) {
                fOrigGuess = fGuess = guess;
                bDefaultGuess = false;
            }

            var b = rateIteration(periodCount, pmt, currentValue, futureValue, payType, fGuess);
            fGuess = b.guess;
            if (!b.valid) {
                if (bDefaultGuess) {
                    var fX = fOrigGuess;
                    for (var nStep = 2; nStep <= 10 && !b.valid; ++nStep) {
                        fGuess = fX * nStep;
                        b = rateIteration(periodCount, pmt, currentValue, futureValue, payType, fGuess);
                        fGuess = b.guess;
                        if (!b.valid) {
                            fGuess = fX / nStep;
                            b = rateIteration(periodCount, pmt, currentValue, futureValue, payType, fGuess);
                            fGuess = b.guess;
                        }
                    }
                }
                if (!b.valid) {
                    throw ErrorCode.NUM;
                }
            }
            return fGuess;
        }
    });

    factory.fixed("RECEIVED", "val:num", 4, 5, ["val:date", "val:date", "val:num", "val:num", "val:int"], {
        format: "currency",
        resolve(settlement, maturity, price, discount, mode) {
            if (price <= 0 || discount <= 0 || settlement > maturity) { throw ErrorCode.NUM; }
            return price / (1 - (discount * getYearFrac(settlement, maturity, mode)));
        }
    });

    factory.ternaryNum("RRI", {
        // AOO 4.4 writes "ZGZ" (https://bz.apache.org/ooo/show_bug.cgi?id=126519)
        name: { ooxml: "_xlfn.RRI", odf: ["RRI", "ZGZ"] },
        resolve(periodCount, currentValue, futureValue) {
            if (periodCount <= 0 || currentValue <= 0 || futureValue < 0) { throw ErrorCode.NUM; }
            if (!is.number(futureValue)) { futureValue = 0; }
            return pow(futureValue / currentValue, 1 / periodCount) - 1;
        }
    });

    factory.ternaryNum("SLN", {
        format: "currency",
        resolve(cost, salvage, life) {
            if (salvage < 0 || life <= 0) { throw ErrorCode.NUM; }
            return (cost - salvage) / life;
        }
    });

    factory.fixed("SYD", "val:num", 4, 4, ["val:num", "val:num", "val:num", "val:num"], {
        format: "currency",
        resolve(cost, salvage, life, period) {
            if (salvage < 0 || life <= 0 || period <= 0) { throw ErrorCode.NUM; }
            return ((cost - salvage) * (life - period + 1)) / ((life * (life + 1)) / 2);
        }
    });

    factory.ternary("TBILLEQ", "val:num", ["val:date", "val:date", "val:num"], (settlement, maturity, discount) => {
        if (discount <= 0 || settlement > maturity) { throw ErrorCode.NUM; }
        maturity = addDaysToDate(maturity, 1);
        var nDiff = getDaysDiff(settlement, maturity, 0);
        if (nDiff > 360) { throw ErrorCode.NUM; }
        var fRet = (365 * discount) / (360 - (discount *  nDiff));
        return fRet;
    });

    factory.ternary("TBILLPRICE", "val:num", ["val:date", "val:date", "val:num"], {
        format: "currency",
        resolve(settlement, maturity, discount) {
            if (discount <= 0 || settlement > maturity) { throw ErrorCode.NUM; }
            maturity = addDaysToDate(maturity, 1);
            var fraction = getYearFrac(settlement, maturity, 0);
            if (modF(fraction) === 0) { throw ErrorCode.NUM; }
            return 100 * (1 - discount * fraction);
        }
    });

    factory.ternary("TBILLYIELD", "val:num", ["val:date", "val:date", "val:num"], (settlement, maturity, price) => {
        if (price <= 0 || settlement >= maturity) { throw ErrorCode.NUM; }
        const nDiff = getDaysDiff(settlement, maturity, 0) + 1;
        if (nDiff > 360) { throw ErrorCode.NUM; }
        return (100 / price - 1) / nDiff * 360;
    });

    factory.fixed("VDB", "val:num", 5, 7, ["val:num", "val:num", "val:num", "val:num", "val:num", "val:num", "val:bool"], {
        format: "currency",
        resolve(cost, salvage, life, startPeriod, endPeriod, factor, noSwitch) {
            if (cost < 0 || salvage < 0 || life < 0 || startPeriod < 0 || endPeriod < 0 || factor < 0) { throw ErrorCode.NUM; }
            if (!is.boolean(noSwitch)) { noSwitch = false; }
            if (!is.number(factor)) { factor = 2; }

            var fIntStart = floor(startPeriod);
            var fIntEnd = ceil(endPeriod);
            var nLoopStart = fIntStart;
            var nLoopEnd = fIntEnd;

            var fVdb = 0;
            if (noSwitch) {
                for (var i = nLoopStart + 1; i <= nLoopEnd; i++) {
                    var fTerm = getGDA(cost, salvage, life, i, factor);

                    //respect partial period in the Beginning/ End:
                    if (i === nLoopStart + 1) {
                        fTerm *= (Math.min(endPeriod, fIntStart + 1) - startPeriod);
                    } else if (i === nLoopEnd) {
                        fTerm *= (endPeriod + 1 - fIntEnd);
                    }
                    fVdb += fTerm;
                }
            } else {

                var fTimeLength1 = life;

                //@ The question of all questions: "Is this right"
                if (startPeriod !== floor(startPeriod)) {
                    if (factor > 1) {
                        if (startPeriod >= life / 2) {
                            var fPart = startPeriod - life / 2;
                            startPeriod = life / 2;
                            endPeriod -= fPart;
                            fTimeLength1 += 1;
                        }
                    }
                }

                cost -= interVDB(cost, salvage, life, fTimeLength1, startPeriod, factor);
                fVdb = interVDB(cost, salvage, life, life - startPeriod, endPeriod - startPeriod, factor);
            }
            return fVdb;
        }
    });

    factory.ternary2("XIRR", "val:num", ["any", "any", "val:num"], function (values, dates, guess) {
        var aValues = this.getNumbersAsArray(values, RCONVERT_ALL_SKIP_EMPTY);
        var aDates = this.getNumbersAsArray(dates, RCONVERT_ALL_TO_DATE_SKIP_EMPTY);

        if ((aValues.length < 2) || (aValues.length !== aDates.length)) {
            throw ErrorCode.NUM;
        }

        if (!is.number(guess)) { guess = 0.1; }

        // result interest rate, initialized with passed guessed rate, or 10%
        var fResultRate = guess;
        if (fResultRate <= -1) {
            throw ErrorCode.NUM;
        }

        // maximum epsilon for end of iteration
        var fMaxEps = 1e-10;
        // maximum number of iterations
        var nMaxIter = 50;

        // Newton's method - try to find a fResultRate, so that lcl_sca_XirrResult() returns 0.
        var nIter = 0;
        var fResultValue;
        var nIterScan = 0;
        var bContLoop = false;
        var bResultRateScanEnd = false;

        // First the inner while-loop will be executed using the default Value fResultRate
        // or the user guessed fResultRate if those do not deliver a solution for the
        // Newton's method then the range from -0.99 to +0.99 will be scanned with a
        // step size of 0.01 to find fResultRate's value which can deliver a solution
        do {
            if (nIterScan >= 1) {
                fResultRate = -0.99 + (nIterScan - 1) * 0.01;
            }
            do {
                fResultValue = sca_XirrResult(aValues, aDates, fResultRate);
                var fNewRate = fResultRate - fResultValue / sca_XirrResult_Deriv1(aValues, aDates, fResultRate);
                var fRateEps = Math.abs(fNewRate - fResultRate);
                fResultRate = fNewRate;
                bContLoop = (fRateEps > fMaxEps) && (Math.abs(fResultValue) > fMaxEps);
            } while (bContLoop && (++nIter < nMaxIter));

            nIter = 0;

            if (!Number.isFinite(fResultRate) || !Number.isFinite(fResultValue)) {
                bContLoop = true;
            }

            ++nIterScan;
            bResultRateScanEnd = (nIterScan >= 200);
        }
        while (bContLoop && !bResultRateScanEnd);

        if (bContLoop) {
            throw ErrorCode.NUM;
        }
        return fResultRate;
    });

    factory.ternary("XNPV", "val:num", ["val:num", "any", "any"], {
        format: "currency",
        resolve(rate, values, dates) {
            var aValList = this.getNumbersAsArray(values, RCONVERT_ALL_SKIP_EMPTY);
            var aDateList = this.getNumbersAsArray(dates, RCONVERT_ALL_TO_DATE_SKIP_EMPTY);

            var nNum = aValList.length;

            if (nNum !== aDateList.length || nNum < 2) { throw ErrorCode.NUM; }

            var fRet = 0;
            var fNull = aDateList[0];
            rate++;

            for (var i = 0; i < nNum; i++) {
                fRet += aValList[i] / (pow(rate, (countDays(fNull, aDateList[i])) / 365));
            }

            return fRet;
        }
    });

    factory.fixed("YIELD", "val:num", 6, 7, ["val:day", "val:day", "val:num", "val:num", "val:num", "val:int", "val:int"],
        function (settlement, maturity, rate, price, discount, frequency, mode) {
            if (rate < 0 || price <= 0 || discount <= 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            ensureFrequency(frequency);
            mode = getModeFromParam(mode);
            return getYield.call(this, settlement, maturity, rate, price, discount, frequency, mode);
        }
    );

    factory.fixed("YIELDDISC", "val:num", 4, 5, ["val:day", "val:day", "val:num", "val:num", "val:int"],
        function (settlement, maturity, price, discount, mode) {
            if (price <= 0 || discount <= 0 || settlement >= maturity) { throw ErrorCode.NUM; }
            mode = getModeFromParam(mode);
            return (discount / price - 1) / getYearFrac(settlement, maturity, mode);
        }
    );

    factory.fixed("YIELDMAT", "val:num", 5, 6, ["val:day", "val:day", "val:day", "val:num", "val:num", "val:int"],
        function (settlement, maturity, issue, rate, price, mode) {

            if ((rate < 0) || (price <= 0) || (settlement >= maturity)) { throw ErrorCode.NUM; }

            mode = getModeFromParam(mode);

            var fIssMat = getYearFrac(issue, maturity, mode);
            var fIssSet = getYearFrac(issue, settlement, mode);
            var fSetMat = getYearFrac(settlement, maturity, mode);

            var y = 1 + fIssMat * rate;
            y /= price / 100 + fIssSet * rate;
            y -= 1;
            y /= fSetMat;

            return y;
        }
    );
});
