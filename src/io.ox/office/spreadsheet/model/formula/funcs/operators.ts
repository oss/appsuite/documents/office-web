/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all unary and binary operators used in spreadsheet
 * formulas.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { type ScalarType, CompareResult, ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { MAX_REF_LIST_SIZE, InternalErrorCode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { add, sub, mul, div, POW } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import type { OperandRefType } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { ParamSigSpec, SimpleFunctionSpec } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// private functions ==========================================================

/**
 * Checks that the passed cell ranges refer to the same sheet. Intended to be
 * used for parameters of type "ref:sheet" (the cell ranges inside a range list
 * have been checked already).
 */
function checkEqualSheet(ranges1: Range3DArray, ranges2: Range3DArray): void {
    if (ranges1.first()!.sheet1 !== ranges2.first()!.sheet1) {
        throw ErrorCode.VALUE;
    }
}

// exports ====================================================================

export default declareFunctionSpecModule(undefined, factory => {

    const REF_DEP_PASS = factory.param("ref", { dep: "pass" });
    const REF_SHEET_DEP_SKIP = factory.param("ref:sheet", { dep: "skip" });

    function registerCompareOp(opKey: string, opSpec: SimpleFunctionSpec<boolean, [ScalarType, ScalarType]>): void {
        factory.binary(opKey, "val:bool", ["val", "val"], opSpec);
    }

    function registerRefOp(opKey: string, paramSig: ParamSigSpec<"ref" | "ref:sheet">, opSpec: SimpleFunctionSpec<OperandRefType, [Range3DArray, Range3DArray]>): void {
        factory.binary(opKey, "ref", [paramSig, paramSig], opSpec);
    }

    // unary arithmetic operators ---------------------------------------------

    // unary plus operator (passes all values, no conversion to number)
    factory.unaryScalar("uplus", {
        name: "+",
        resolve: v => v
    });

    // unary minus operator (always converts to numbers)
    factory.unaryNum("uminus", {
        name: "-",
        resolve: n => -n
    });

    // unary percent operator
    factory.unaryNum("upct", {
        name: "%",
        resolve: n => n / 100
    });

    // binary arithmetic operators --------------------------------------------

    // binary addition operator
    factory.binaryNum("add", {
        name: "+",
        format: "infer",
        resolve: add
    });

    // binary subtraction operator
    factory.binaryNum("sub", {
        name: "-",
        format: "infer",
        resolve: sub
    });

    // binary multiplication operator
    factory.binaryNum("mul", {
        name: "*",
        resolve: mul
    });

    // binary division operator
    factory.binaryNum("div", {
        name: "/",
        resolve: div
    });

    // binary power operator
    factory.binaryNum("pow", {
        name: "^",
        resolve: POW
    });

    // string operators -------------------------------------------------------

    // binary string concatenation operator
    factory.binaryStr("con", {
        name: "&",
        resolve(str1, str2) {
            // concatenate the URL parts of both operands (fall-back to the regular strings if either URL is missing)
            const url1 = this.getOperand(0).url;
            const url2 = this.getOperand(1).url;
            const url = ((url1 !== null) || (url2 !== null)) ? ((url1 ?? str1) + (url2 ?? str2)) : null;
            return this.createOperand(str1 + str2, { url });
        }
    });

    // comparison operators ---------------------------------------------------

    // binary less-than operator
    registerCompareOp("lt", {
        name: "<",
        resolve(value1, value2) {
            return this.compareScalars(value1, value2) === CompareResult.LESS;
        }
    });

    // binary less-than-or-equal operator
    registerCompareOp("le", {
        name: "<=",
        resolve(value1, value2) {
            return this.compareScalars(value1, value2) <= CompareResult.EQUAL;
        }
    });

    // binary greater-than operator
    registerCompareOp("gt", {
        name: ">",
        resolve(value1, value2) {
            return this.compareScalars(value1, value2) === CompareResult.GREATER;
        }
    });

    // binary greater-than-or-equal operator
    registerCompareOp("ge", {
        name: ">=",
        resolve(value1, value2) {
            return this.compareScalars(value1, value2) >= CompareResult.EQUAL;
        }
    });

    // binary equals operator
    registerCompareOp("eq", {
        name: "=",
        resolve(value1, value2) {
            return this.equalScalars(value1, value2);
        }
    });

    // binary equals-not operator
    registerCompareOp("ne", {
        name: "<>",
        resolve(value1, value2) {
            return !this.equalScalars(value1, value2);
        }
    });

    // reference operators ----------------------------------------------------

    // binary list reference operator
    registerRefOp("list", REF_DEP_PASS, {
        name: { ooxml: ",", odf: "~" },
        resolve: (ranges1, ranges2) => ranges1.concat(ranges2)
    });

    // binary intersection reference operator
    registerRefOp("isect", REF_SHEET_DEP_SKIP, {
        name: { ooxml: " ", odf: "!" },
        recalc: "always", // resulting reference may not cause circular dependencies although parameters would do
        resolve(ranges1, ranges2) {

            // ranges in both operands must refer to the same sheet
            checkEqualSheet(ranges1, ranges2);

            // calculate the intersection of each range pair from ranges1 and ranges2,
            // process all outer ranges separately to be able to check resulting list
            // size inside the loop, otherwise this loop may create a MAX^2 list
            const resultRanges = new Range3DArray();
            for (const range1 of ranges1) {
                const ranges = ranges2.intersect(range1);
                if (resultRanges.length + ranges.length > MAX_REF_LIST_SIZE) {
                    throw InternalErrorCode.UNSUPPORTED;
                }
                resultRanges.append(ranges);
            }
            return resultRanges;
        }
    });

    // binary range reference operator
    registerRefOp("range", REF_SHEET_DEP_SKIP, {
        name: ":",
        recalc: "always", // resulting reference may cover cells not covered by the parameters
        resolve(ranges1: Range3DArray, ranges2: Range3DArray) {

            // ranges in both operands must refer to the same sheet
            checkEqualSheet(ranges1, ranges2);

            // build the bounding range from all ranges
            return ranges1.boundary()!.boundary(ranges2.boundary()!);
        }
    });
});
