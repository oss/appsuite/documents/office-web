/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions providing some kind of
 * information about the contents of sheet cells.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { is, str } from "@/io.ox/office/tk/algorithms";

import { ErrorCode, isErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { CellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import type { FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

const { floor, abs } = Math;

// constants ==================================================================

/**
 * Format codes for the `CELL` function, mapped by internal number format ID.
 */
const CELL_FORMAT_CODES = ["G", "F0", ",0", "F2", ",2", "C0", "C0-", "C2", "C2-", "P0", "P2", "S2", "G", "D4", "D1", "D2", "D3", "D5", "D7", "D6", "D9", "D8"];

// private functions ==========================================================

/**
 * Returns the merged attribute set of the specified cell.
 */
function getAttributeSet(sheetModel: SheetModel, address: Address): CellAttributeSet {
    return sheetModel.cellCollection.getAttributeSet(address);
}

/**
 * Generates the result for `CELL("address")`.
 */
function formatAddress(context: FormulaContext, sheet: OptSheet, address: Address): string {
    const sheetRefs = (sheet === null) ? undefined : SheetRefs.single(sheet, true);
    const cellRef = CellRef.create(address, true, true);
    context.checkCellRef(cellRef);
    // TODO: add file name
    return context.formulaGrammar.formatReference(context.docModel, context.getTargetAddress(), cellRef, sheetRefs);
}

/**
 * Generates the result for `CELL("color")`.
 */
function getColorFormat(sheetModel: SheetModel, address: Address): number {
    const negSection = sheetModel.cellCollection.getParsedFormat(address).numberSections[1];
    return negSection?.color ? 1 : 0;
}

/**
 * Generates the result for `CELL("format")`.
 */
function getFormatCode(context: FormulaContext, sheetModel: SheetModel, address: Address): string {
    const parsedFormat = sheetModel.cellCollection.getParsedFormat(address);
    const formatId = context.numberFormatter.resolveFormatId(parsedFormat);
    return (is.number(formatId) && (formatId <= CELL_FORMAT_CODES.length)) ? CELL_FORMAT_CODES[formatId] : "G";
}

/**
 * Generates the result for `CELL("type")`.
 */
function getValueType(context: FormulaContext, sheetModel: SheetModel, address: Address): string {
    const value = context.getCellValue(sheetModel, address);
    return (value === null) ? "b" : (typeof value === "string") ? "l" : "v";
}

/**
 * Generates the result for `CELL("prefix")`.
 */
function getPrefix(context: FormulaContext, sheetModel: SheetModel, address: Address): string {
    if (getValueType(context, sheetModel, address) !== "l") { return ""; }
    switch (getAttributeSet(sheetModel, address).cell.alignHor) {
        case "right":
            return '"';
        case "center":
        case "centerAcross":
            return "^";
        case "fill":
            return "\\";
        default:
            return "'";
    }
}

/**
 * Generates the result for `CELL("filename")`.
 */
function getFileName(context: FormulaContext, sheetModel: SheetModel): string {
    const filePath = context.docModel.docApp.getFilePath();
    const fileName = context.docModel.docApp.getFullFileName() || "";
    // always unquoted, regardless of document name, and sheet name
    return `/${str.joinTail(filePath, "/")}[${fileName}]${sheetModel.getName()}`;
}

// exports ====================================================================

export default declareFunctionSpecModule("information", factory => {

    const REF_SINGLE_DEP_SKIP = factory.param("ref:single", { dep: "skip" });
    const ANY_DEP_SKIP = factory.param("any", { dep: "skip" });

    factory.binary1("CELL", "val:any", ["val:str", REF_SINGLE_DEP_SKIP], {
        recalc: "always",
        resolve(infoType, range) {

            // resolve translated or English info type (prefer translations over English names)
            const paramMap = this.formulaResource.cellParamMap;
            const paramKey = paramMap.getKey(infoType, true) || paramMap.getKey(infoType, false);

            // Get the address of the target cell. If the parameter is omitted, then:
            // - OOXML: active cell in active sheet of the document during last modification (TODO),
            // - ODF: the formula reference address (formula cell itself).
            const refSheet = this.getRefSheet();
            const sheet = range ? range.sheet1 : refSheet;
            const sheetModel = this.getSheetModel(sheet);
            const address = range ? range.a1 : this.getTargetAddress();

            switch (paramKey) {
                case "ADDRESS":
                    return formatAddress(this, (sheet === refSheet) ? null : sheet, address);
                case "COL":
                    return address.c + 1;
                case "COLOR":
                    return getColorFormat(sheetModel, address);
                case "CONTENTS":
                    // bug 47975: prevent circular error when CELL references itself without second parameter
                    return range ? this.getCellValue(sheetModel, address) : 0;
                case "FILENAME":
                    return getFileName(this, sheetModel);
                case "FORMAT":
                    return getFormatCode(this, sheetModel, address);
                case "PARENTHESES":
                    throw ErrorCode.NA; // TODO
                case "PREFIX":
                    return getPrefix(this, sheetModel, address);
                case "PROTECT":
                    return getAttributeSet(sheetModel, address).cell.unlocked ? 0 : 1;
                case "ROW":
                    return address.r + 1;
                case "TYPE":
                    // bug 47975: prevent circular error when CELL references itself without second parameter
                    return range ? getValueType(this, sheetModel, address) : "l";
                case "WIDTH":
                    throw ErrorCode.NA; // TODO
                case null:
                    throw ErrorCode.VALUE;
            }
            throw ErrorCode.VALUE;
        },
        // result depends on reference address (behaves like a relative reference), if second parameter is missing
        relColRef: count => count < 2,
        relRowRef: count => count < 2
    });

    factory.nullary("CURRENT", "any", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.CURRENT" },
        hidden: true
    });

    factory.unary("ERROR.TYPE", "val:num", "val:any", v => isErrorCode(v) ? v.num : ErrorCode.NA.throw());

    factory.unary("ERRORTYPE.ODF", "val:num", "val:any", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.ERRORTYPE" }
    });

    factory.unary("INFO", "val:str", "any", {
        recalc: "always"
    });

    factory.unary("ISBLANK", "val:bool", "val:any", is.null); // error codes result in FALSE

    factory.unary("ISERR", "val:bool", "val:any", v => isErrorCode(v) && (v !== ErrorCode.NA));

    factory.unary("ISERROR", "val:bool", "val:any", isErrorCode);

    factory.unary("ISEVEN", "val:bool", "val:num", n => (floor(abs(n)) % 2) === 0);

    factory.unary("ISFORMULA", "val:bool", REF_SINGLE_DEP_SKIP, {
        name: { ooxml: "_xlfn.ISFORMULA" },
        recalc: "always",
        resolve(range) {

            // always use the first cell in the range (TODO: matrix context)
            const address = range.a1;

            // accept reference to own cell (no circular reference error)
            if (this.isRefSheet(range.sheet1) && this.isTargetAddress(address)) {
                return true; // do not check the cell, a new formula has not been inserted yet
            }

            // getCellFormula() returns null for value cells
            const sheetModel = this.getSheetModel(range.sheet1);
            return this.getCellFormula(sheetModel, address) !== null;
        }
    });

    factory.unary("ISLOGICAL", "val:bool", "val:any", is.boolean); // error codes and empty cells result in FALSE

    factory.unary("ISNA", "val:bool", "val:any", v => v === ErrorCode.NA);

    factory.unary("ISNONTEXT", "val:bool", "val:any", v => !is.string(v)); // error codes and empty cells result in TRUE

    factory.unary("ISNUMBER", "val:bool", "val:any", is.number); // error codes and empty cells result in FALSE

    factory.unary("ISODD", "val:bool", "val:num", n => (floor(abs(n)) % 2) !== 0);

    factory.unary("ISREF", "val:bool", ANY_DEP_SKIP, op => op.type === "ref"); // error codes result in FALSE

    factory.unary("ISTEXT", "val:bool", "val:any", is.string); // error codes and empty cells result in FALSE

    factory.nullary("NA", "val:err", () => ErrorCode.NA);

    factory.ternary1("STYLE", "any", ["any", "any", "any"], {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.STYLE" },
        hidden: true
    });

    // resolve cell references to values, accept error codes
    factory.unary("TYPE", "val:num", "val:any", function (v) {
        // array literals always result in 64 regardless of their contents
        if (this.getOperand(0).type === "mat") { return 64; }
        // determine type of value parameters and cell contents
        if (is.number(v)) { return 1; }
        if (is.string(v)) { return 2; }
        if (is.boolean(v)) { return 4; }
        if (isErrorCode(v)) { return 16; }
        // default (reference to empty cell)
        return 1;
    });
});
