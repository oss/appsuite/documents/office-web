/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with numeric
 * conversion, e.g. number systems such as binary, octal, hexadecimal, or Roman
 * numbers; and conversion between different measurement units.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { type RomanDepth, is, str, fmt, fun, map } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// types ======================================================================

type UnitCategory = "mass" | "length" | "area" | "volume" | "time" | "speed" | "pressure" | "force" | "energy" | "power" | "magnetism" | "temperature" | "information";

interface UnitConverter {
    toBase(n: number): number;
    toUnit(n: number): number;
}

interface UnitOptions {
    si?: number;
    bin?: boolean;
}

// constants ==================================================================

// SI prefixes for metric measurement units, with the exponents of powers of 10
const SI_PREFIXES = new Map(Object.entries({ Y: 24, Z: 21, E: 18, P: 15, T: 12, G: 9, M: 6, k: 3, h: 2, da: 1, e: 1, d: -1, c: -2, m: -3, u: -6, n: -9, p: -12, f: -15, a: -18, z: -21, y: -24 }));

// prefixes for binary measurement units (bits/bytes), with the exponents of powers of 2
const BIN_PREFIXES = new Map(Object.entries({ Yi: 80, Zi: 70, Ei: 60, Pi: 50, Ti: 40, Gi: 30, Mi: 20, ki: 10 }));

// class ProportUnitConverter =================================================

class ProportUnitConverter implements UnitConverter {
    readonly #factor: number;
    constructor(factor: number) { this.#factor = factor; }
    toBase(n: number): number { return n * this.#factor; }
    toUnit(n: number): number { return n / this.#factor; }
}

// class LinearUnitConverter ==================================================

class LinearUnitConverter implements UnitConverter {
    readonly #factor: number;
    readonly #offset: number;
    constructor(factor: number, offset: number) { this.#factor = factor; this.#offset = offset; }
    toBase(n: number): number { return (n + this.#offset) * this.#factor; }
    toUnit(n: number): number { return n / this.#factor - this.#offset; }
}

// globals ====================================================================

// regular expression cache for `DECIMAL`
const decimalREs = new Map<number, RegExp>();

// private functions ==========================================================

/**
 * Expands the passed string (binary, octal, or hexadecimal number) with
 * leading zero characters, according to the passed length.
 *
 * @param value
 *  A number as string (binary, octal, or hexadecimal).
 *
 * @param length
 *  If specified, the target length of the passed number. If omitted, the
 *  passed number will be returned unmodified.
 *
 * @param maxLength
 *  The maximum allowed length. Larger values will result in throwing the error
 *  code `#NUM!`.
 *
 * @param [ensureMin=false]
 *  If set to `true`, and the passed number is longer than the value passed in
 *  the parameter `length`, the error code `#NUM!` will be thrown. By default,
 *  larger numbers will be returned as passed in this case.
 *
 * @returns
 *  The resulting number with leading zero characters.
 *
 * @throws
 *  The error code `#NUM!`, if the passed length is invalid.
 */
function expandWithZeros(value: string, length: Opt<number>, maxLength: number, ensureMin?: boolean): string {

    // always throw, if the passed number is larger than the specified maximum
    if (value.length > maxLength) { throw ErrorCode.NUM; }

    // check and expand the number with leading zeros
    if (is.number(length)) {
        // length parameter must be valid
        if ((ensureMin && (length < value.length)) || (length > maxLength)) {
            throw ErrorCode.NUM;
        }
        return value.padStart(length, "0");
    }

    return value;
}

/**
 * Converts the string representing of a binary number to a decimal number.
 *
 * @param value
 *  A binary number string to be converted to a number if it is not longer than
 *  10 characters.
 *
 * @returns
 *  The resulting decimal number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed value is not a valid binary number,
 *  or if it is too long (more than 10 digits).
 */
function convertBinToDec(value: string): number {

    // check input value, maximum length of the input string is 10 digits
    if (!/^[01]{0,10}$/.test(value)) { throw ErrorCode.NUM; }

    // the parsed result (empty string is interpreted as zero)
    const result = (value.length > 0) ? parseInt(value, 2) : 0;

    // negative number, if 10th digit is 1
    return ((value.length === 10) && value.startsWith("1"))  ? (result - 0x400) : result;
}

/**
 * Converts the string representing of an octal number to a decimal number.
 *
 * @param value
 *  An octal number string to be converted to a number if it is not longer than
 *  10 characters.
 *
 * @returns
 *  The resulting decimal number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed value is not a valid octal number, or
 *  if it is too long (more than 10 digits).
 */
function convertOctToDec(value: string): number {

    // check input value, maximum length of the input string is 10 digits
    if (!/^[0-7]{0,10}$/.test(value)) { throw ErrorCode.NUM; }

    // the parsed result (empty string is interpreted as zero)
    const result = (value.length > 0) ? parseInt(value, 8) : 0;

    // negative number, if 10th digit is greater than 3
    return ((value.length === 10) && /^[4-7]/.test(value)) ? (result - 0x40000000) : result;
}

/**
 * Converts the string representing of a hexadecimal number to a decimal
 * number.
 *
 * @param value
 *  A hexadecimal number string to be converted to a number if it is not longer
 *  than 10 characters.
 *
 * @returns
 *  The resulting decimal number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed value is not a valid hexadecimal
 *  number, or if it is too long (more than 10 digits).
 */
function convertHexToDec(value: string): number {

    // check input value, maximum length of the input string is 10 digits
    if (!/^[0-9a-f]{0,10}$/i.test(value)) { throw ErrorCode.NUM; }

    // the parsed result (empty string is interpreted as zero)
    const result = (value.length > 0) ? parseInt(value, 16) : 0;

    // negative number, if 10th digit is greater than 7
    return ((value.length === 10) && /^[89a-f]/i.test(value)) ? (result - 0x10000000000) : result;
}

/**
 * Converts the passed decimal number to its binary representation. Inserts
 * leading zero characters if specified.
 *
 * @param n
 *  The number to be converted to its binary representation.
 *
 * @param [length]
 *  If specified, the target length of the passed number. See function
 *  `expandWithZeros()` for details.
 *
 * @returns
 *  The resulting binary number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed number results in more than 10 binary
 *  digits, or if the passed length is invalid.
 */
function convertDecToBin(n: number, length?: number): string {

    // restrict passed number to signed 10-digit binary
    if ((-n > 0x200) || (n > 0x1FF)) { throw ErrorCode.NUM; }

    // convert negative numbers to positive resulting in a 10-digit binary number
    if (n < 0) { n += 0x400; }

    // convert to hexadecimal number, expand to specified length
    return expandWithZeros(n.toString(2), length, 10, true);
}

/**
 * Converts the passed decimal number to its octal representation. Inserts
 * leading zero characters if specified.
 *
 * @param n
 *  The number to be converted to its octal representation.
 *
 * @param [length]
 *  If specified, the target length of the passed number. See function
 *  `expandWithZeros()` for details.
 *
 * @returns
 *  The resulting octal number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed number results in more than 10 octal
 *  digits, or if the passed length is invalid.
 */
function convertDecToOct(n: number, length?: number): string {

    // restrict passed number to signed 10-digit octal
    if ((-n > 0x20000000) || (n > 0x1FFFFFFF)) { throw ErrorCode.NUM; }

    // convert negative numbers to positive resulting in a 10-digit octal number
    if (n < 0) { n += 0x40000000; }

    // convert to octal number, expand to specified length
    return expandWithZeros(n.toString(8), length, 10, true);
}

/**
 * Converts the passed decimal number to its hexadecimal representation.
 * Inserts leading zero characters if specified.
 *
 * @param n
 *  The number to be converted to its hexadecimal representation.
 *
 * @param [length]
 *  If specified, the target length of the passed number. See function
 *  `expandWithZeros()` for details.
 *
 * @returns
 *  The resulting hexadecimal number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed number results in more than 10
 *  hexadecimal digits, or if the passed length is invalid.
 */
function convertDecToHex(n: number, length?: number): string {

    // restrict passed number to signed 10-digit hexadecimal
    if ((-n > 0x8000000000) || (n > 0x7FFFFFFFFF)) { throw ErrorCode.NUM; }

    // convert negative numbers to positive resulting in a 10-digit hex number
    if (n < 0) { n += 0x10000000000; }

    // convert to hexadecimal number, expand to specified length
    return expandWithZeros(n.toString(16).toUpperCase(), length, 10, true);
}

const getUnitConverters = fun.once(() => {

    // all unit converters for `CONVERT`, mapped by unit names (can be conflicting)
    const converterRegistry = new Map<string, Map<string, UnitConverter>>();

    function registerConverter(category: UnitCategory, unit: string, converter: UnitConverter): void {
        const converterMap = map.upsert(converterRegistry, unit, () => new Map(null));
        if (converterMap.has(category)) {
            globalLogger.error(`duplicate measurement unit "${unit}" in category "${category}"`);
        }
        converterMap.set(category, converter);
    }

    function registerPropUnit(category: UnitCategory, units: string, factor: number, options?: UnitOptions): void {
        for (const unit of str.splitTokens(units)) {
            registerConverter(category, unit, new ProportUnitConverter(factor));
            if (options?.si) {
                const base = 10 ** options.si;
                for (const [prefix, exp] of SI_PREFIXES) {
                    registerConverter(category, prefix + unit, new ProportUnitConverter(factor * base ** exp));
                }
            }
            if (options?.bin) {
                for (const [prefix, exp] of BIN_PREFIXES) {
                    registerConverter(category, prefix + unit, new ProportUnitConverter(factor * 2 ** exp));
                }
            }
        }
    }

    function registerLinrUnit(category: UnitCategory, units: string, factor: number, offset: number): void {
        for (const unit of str.splitTokens(units)) {
            registerConverter(category, unit, new LinearUnitConverter(factor, offset));
        }
    }

    const U = 1.660538782e-24;  // 1 U in grams
    const LBM = 453.59237;      // 1 pound in grams

    registerPropUnit("mass", "g",                   1, { si: 1 });      // gram
    registerPropUnit("mass", "u",                   U, { si: 1 });      // U (atomic mass unit)
    registerPropUnit("mass", "lbm",                 LBM);               // pound (avoirdupois)
    registerPropUnit("mass", "ozm",                 LBM / 16);          // ounce (avoirdupois)
    registerPropUnit("mass", "grain",               LBM / 7000);        // grain (avoirdupois)
    registerPropUnit("mass", "cwt shweight",        LBM * 100);         // hundredweight (short avoirdupois)
    registerPropUnit("mass", "ton",                 LBM * 2000);        // ton (short avoirdupois)
    registerPropUnit("mass", "stone",               LBM * 14);          // stone (long avoirdupois)
    registerPropUnit("mass", "uk_cwt lcwt hweight", LBM * 112);         // imperial hundredweight (long avoirdupois)
    registerPropUnit("mass", "uk_ton LTON brton",   LBM * 2240);        // imperial ton (long avoirdupois)
    registerPropUnit("mass", "sg",                  14593.9029372064);  // slug

    const LY = 9.4607304725808e15;  // 1 light year in meters
    const IN = 0.0254;              // 1 inch in meters
    const FT = 0.3048;              // 1 foot (12 inches) in meters
    const YD = 0.9144;              // 1 yard (3 feet) in meters
    const MI = 1609.344;            // 1 mile (1760 yards) in meters
    const PICA = IN / 6;            // 1 pica (1/6 inch) in meters
    const PT = PICA / 12;           // 1 point (1/12 pica) in meters
    const NMI = 1_852;              // 1 nautical mile in meters
    const ANMI = 1_853.184;         // 1 admiralty nautical mile in meters
    const SIN = 1 / 39.37;          // 1 U.S. survey inch in meters
    const SFT = SIN * 12;           // 1 U.S. survey foot in meters
    const SYD = SFT * 3;            // 1 U.S. survey yard in meters
    const SMI = SYD * 1_760;        // 1 U.S. survey mile in meters

    registerPropUnit("length", "m",           1,                   { si: 1 });  // meter
    registerPropUnit("length", "ang",         1e-10,               { si: 1 });  // angstrom
    registerPropUnit("length", "ly",          9.4607304725808e15,  { si: 1 });  // light year
    registerPropUnit("length", "pc parsec",   3.08567758128155e16, { si: 1 });  // parsec
    registerPropUnit("length", "in",          IN);                              // inch
    registerPropUnit("length", "ft",          FT);                              // foot
    registerPropUnit("length", "yd",          YD);                              // yard
    registerPropUnit("length", "ell",         IN * 45);                         // ell
    registerPropUnit("length", "mi",          MI);                              // mile
    registerPropUnit("length", "pica",        PICA);                            // pica
    registerPropUnit("length", "Picapt Pica", PT);                              // point
    registerPropUnit("length", "Nmi",         NMI);                             // nautical mile
    registerPropUnit("length", "survey_mi",   SMI);                             // U.S. survey mile

    const IN2 = IN ** 2; // 1 square inch in square meters

    registerPropUnit("area", "m2 m^2",                        1,     { si: 2 });    // square meter
    registerPropUnit("area", "ang2 ang^2",                    1e-20, { si: 2 });    // square angstom
    registerPropUnit("area", "ar",                            100);                 // are
    registerPropUnit("area", "Morgen",                        2_500);               // morgen (25 ares)
    registerPropUnit("area", "ha",                            10_000);              // hectare (100 ares)
    registerPropUnit("area", "ly2 ly^2",                      LY ** 2);             // square light year (no SI prefixes supported)
    registerPropUnit("area", "in2 in^2",                      IN2);                 // square inch
    registerPropUnit("area", "ft2 ft^2",                      FT ** 2);             // square foot
    registerPropUnit("area", "yd2 yd^2",                      YD ** 2);             // square yard
    registerPropUnit("area", "mi2 mi^2",                      MI ** 2);             // square mile
    registerPropUnit("area", "Picapt2 Picapt^2 Pica2 Pica^2", PT ** 2);             // square point
    registerPropUnit("area", "Nmi2 Nmi^2",                    NMI ** 2);            // square nautical mile
    registerPropUnit("area", "uk_acre",                       YD ** 2 * 4_840);     // international acre (4840 square feet)
    registerPropUnit("area", "us_acre",                       SYD ** 2 * 4_840);    // U.S. survey acre (4840 square survey feet)

    const IN3 = IN ** 3;            // 1 cubic inch in cubic meters
    const FT3 = FT ** 3;            // 1 cubic foot in cubic meters
    const TSP = 4.92892159375e-6;   // 1 teaspoon in cubic meters
    const UKPT = 5.6826125e-4;      // 1 imperial pint in cubic meters

    registerPropUnit("volume", "m3 m^3",                        1,     { si: 3 });  // cubic meter
    registerPropUnit("volume", "ang3 ang^3",                    1e-30, { si: 3 });  // cubic Angstom
    registerPropUnit("volume", "l L lt",                        0.001, { si: 1 });  // liter (1 dm^3)
    registerPropUnit("volume", "ly3 ly^3",                      LY ** 3);           // cubic light year (no SI prefixes supported)
    registerPropUnit("volume", "in3 in^3",                      IN3);               // cubic inch
    registerPropUnit("volume", "ft3 ft^3",                      FT3);               // cubic foot
    registerPropUnit("volume", "yd3 yd^3",                      YD ** 3);           // cubic yard
    registerPropUnit("volume", "mi3 mi^3",                      MI ** 3);           // cubic mile
    registerPropUnit("volume", "Picapt3 Picapt^3 Pica3 Pica^3", PT ** 3);           // cubic point
    registerPropUnit("volume", "Nmi3 Nmi^3",                    NMI ** 3);          // cubic nautical mile
    registerPropUnit("volume", "bushel",                        IN3 * 2150.42);     // U.S. bushel
    registerPropUnit("volume", "MTON",                          FT3 * 40);          // measurement ton
    registerPropUnit("volume", "GRT regton",                    FT3 * 100);         // gross register tonnage
    registerPropUnit("volume", "tspm",                          5e-6);              // metric teaspoon (5 ml)
    registerPropUnit("volume", "tsp",                           TSP);               // teaspoon
    registerPropUnit("volume", "tbs",                           TSP * 3);           // tablespoon (3 teaspoons)
    registerPropUnit("volume", "oz",                            TSP * 6);           // fluid ounce (6 teaspoons)
    registerPropUnit("volume", "cup",                           TSP * 48);          // cup (8 oz)
    registerPropUnit("volume", "pt us_pt",                      TSP * 96);          // U.S. pint (16 oz)
    registerPropUnit("volume", "qt",                            TSP * 192);         // U.S. quart (2 pint)
    registerPropUnit("volume", "gal",                           TSP * 768);         // U.S. gallon (8 pint)
    registerPropUnit("volume", "barrel",                        TSP * 32_256);      // U.S. oil barrel (42 gallons)
    registerPropUnit("volume", "uk_pt",                         UKPT);              // imperial pint
    registerPropUnit("volume", "uk_qt",                         UKPT * 2);          // imperial quart
    registerPropUnit("volume", "uk_gal",                        UKPT * 8);          // imperial gallon

    const HR = 3_600; // 1 hour in seconds

    registerPropUnit("time", "s sec",  1);            // second
    registerPropUnit("time", "mn min", 60);           // minute
    registerPropUnit("time", "hr",     HR);           // hour
    registerPropUnit("time", "d day",  HR * 24);      // day
    registerPropUnit("time", "yr",     HR * 8_766);   // year (`CONVERT` uses exactly 365.25 days)

    registerPropUnit("speed", "m/s m/sec", 1,      { si: 1 });  // meters per second
    registerPropUnit("speed", "m/h m/hr",  1 / HR, { si: 1 });  // meters per hour
    registerPropUnit("speed", "mph",       MI / HR);            // miles per hour
    registerPropUnit("speed", "kn",        NMI / HR);           // knot (nautical miles per hour)
    registerPropUnit("speed", "admkn",     ANMI / HR);          // admiralty knot (admiralty nautical miles per hour)

    const KGF = 9.806_65;           // 1 kilogram-force (kilopond) in newton
    const LBF = LBM * KGF / 1000;   // 1 pound-force in newton (LBM is in grams)

    registerPropUnit("force", "N",      1,          { si: 1 }); // newton
    registerPropUnit("force", "dy dyn", 1e-5,       { si: 1 }); // dyne
    registerPropUnit("force", "pond",   KGF / 1000, { si: 1 }); // pond
    registerPropUnit("force", "lbf",    LBF);                   // pound-force

    const ATM = 101_325; // 1 athmosphere in Pascal

    registerPropUnit("pressure", "Pa p",   1,       { si: 1 }); // pascal
    registerPropUnit("pressure", "atm at", ATM,     { si: 1 }); // athmosphere
    registerPropUnit("pressure", "mmHg",   133.322, { si: 1 }); // millimeter of mercury (with SI prefixes, e.g. "kmmHg", but not "mHg")
    registerPropUnit("pressure", "Torr",   ATM / 760);          // torr (no SI prefixes supported)
    registerPropUnit("pressure", "psi",    LBF / IN2);          // psi (pound-force per square inch)

    const HPL = FT * LBF * 550; // 1 mechanical horsepower in watt

    registerPropUnit("power", "W w",  1, { si: 1 });    // watt
    registerPropUnit("power", "PS",   KGF * 75);        // metric horsepower
    registerPropUnit("power", "HP h", HPL);             // mechanical horsepower

    registerPropUnit("energy", "J",       1,               { si: 1 });  // joule
    registerPropUnit("energy", "e",       1e-7,            { si: 1 });  // erg
    registerPropUnit("energy", "c",       4.184,           { si: 1 });  // thermodynamic calorie
    registerPropUnit("energy", "cal",     4.1868,          { si: 1 });  // international steam table calorie
    registerPropUnit("energy", "eV ev",   1.602176487E-19, { si: 1 });  // electronvolt
    registerPropUnit("energy", "Wh wh",   HR,              { si: 1 });  // watt-hour
    registerPropUnit("energy", "HPh hh",  HPL * HR);                    // horsepower-hour
    registerPropUnit("energy", "flb",     FT * LBF);                    // foot-pound
    registerPropUnit("energy", "BTU btu", 1055.05585262);               // British termal unit

    registerPropUnit("magnetism", "T",  1,    { si: 1 }); // tesla
    registerPropUnit("magnetism", "ga", 1e-4, { si: 1 }); // gauss

    registerPropUnit("temperature", "K kel", 1, { si: 1 });     // kelvin
    registerLinrUnit("temperature", "C cel", 1, 273.15);        // degree celsius
    registerLinrUnit("temperature", "F fah", 5 / 9, 459.67);    // degree fahrenheit
    registerPropUnit("temperature", "Rank",  5 / 9);            // degree rankine
    registerLinrUnit("temperature", "Reau",  5 / 4, 218.52);    // degree reaumur

    registerPropUnit("information", "bit",  1, { si: 1, bin: true });  // bit
    registerPropUnit("information", "byte", 8, { si: 1, bin: true });  // byte

    return converterRegistry;
});

// exports ====================================================================

export default declareFunctionSpecModule("conversion", factory => {

    factory.unary("ARABIC", "val:num", "val:str", {
        name: { ooxml: "_xlfn.ARABIC" },
        resolve(roman) {

            // trim space characters; roman number must not be longer than 255 characters (including minus sign)
            roman = roman.replace(/^ +| +$/g, "");
            if (roman.length > 255) { throw ErrorCode.VALUE; }

            // roman number may contain a leading minus sign
            let sign = 1;
            if (roman.startsWith("-")) {
                sign = -1;
                roman = roman.substr(1);
            }

            // parse the roman number; throw #VALUE! error, if the text cannot be parsed
            const result = fmt.parseRoman(roman);
            return Number.isFinite(result) ? (sign * result) : ErrorCode.VALUE.throw();
        }
    });

    factory.ternary2("BASE", "val:str", ["val:int", "val:int", "val:int"], {
        name: { ooxml: "_xlfn.BASE" },
        resolve(n, radix, length) {
            if ((n < 0) || (n > 0x1FFFFFFFFFFFFF)) { throw ErrorCode.NUM; }
            if ((radix < 2) || (radix > 36)) { throw ErrorCode.NUM; }
            if (is.number(length) && ((length < 0) || (length > 255))) { throw ErrorCode.NUM; }
            return expandWithZeros(n.toString(radix).toUpperCase(), length, 255);
        }
    });

    factory.unary("BIN2DEC", "val:num", "val:str", convertBinToDec);

    factory.binary1("BIN2HEX", "val:str", ["val:str", "val:int"], (n, l) => convertDecToHex(convertBinToDec(n), l));

    factory.binary1("BIN2OCT", "val:str", ["val:str", "val:int"], (n, l) => convertDecToOct(convertBinToDec(n), l));

    factory.fixed("COLOR", "val:num", 3, 4, ["val:int", "val:int", "val:int", "val:int"], {
        name: { ooxml: null, odf: "ORG.LIBREOFFICE.COLOR" },
        resolve(r: number, g: number, b: number, a?: number) {
            a = a || 0;
            if ((r < 0) || (r > 255)) { throw ErrorCode.VALUE; }
            if ((g < 0) || (g > 255)) { throw ErrorCode.VALUE; }
            if ((b < 0) || (b > 255)) { throw ErrorCode.VALUE; }
            if ((a < 0) || (a > 255)) { throw ErrorCode.VALUE; }
            return a * 0x1000000 + r * 0x10000 + g * 0x100 + b;
        }
    });

    factory.ternary("CONVERT", "val:num", ["val:num", "val:str", "val:str"], (n, srcUnit, tgtUnit) => {

        // initialize the converter map once
        const converterRegistry = getUnitConverters();

        // resolve source and target converters
        const srcConvMap = converterRegistry.get(srcUnit);
        const tgtConvMap = converterRegistry.get(tgtUnit);
        if (!srcConvMap || !tgtConvMap) { throw ErrorCode.NA; }

        // resolve converters for the same category present in both maps
        let srcConv: Opt<UnitConverter>;
        let tgtConv: Opt<UnitConverter>;
        srcConvMap.forEach((conv, cat) => {
            if (tgtConvMap.has(cat)) {
                if (srcConv) { throw ErrorCode.NA; } // ambiguous in both maps
                srcConv = conv;
                tgtConv = tgtConvMap.get(cat);
            }
        });

        // convert the passed number from source unit to base unit, then from base unit to target unit
        if (!srcConv || !tgtConv) { throw ErrorCode.NA; }
        return tgtConv.toUnit(srcConv.toBase(n));
    });

    factory.ternary("CONVERT.ODF", "val:num", ["val:num", "val:str", "val:str"], {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.CONVERT" },
        hidden: true
    });

    factory.binary1("DEC2BIN", "val:str", ["val:int", "val:int"], convertDecToBin);

    factory.binary1("DEC2HEX", "val:str", ["val:int", "val:int"], convertDecToHex);

    factory.binary1("DEC2OCT", "val:str", ["val:int", "val:int"], convertDecToOct);

    factory.binary("DECIMAL", "val:num", ["val:str", "val:int"], {
        name: { ooxml: "_xlfn.DECIMAL" },
        resolve(value, radix) {

            // check radix
            if ((radix < 2) || (radix > 36)) { throw ErrorCode.NUM; }

            // get or crete the test pattern for the input value
            const testRE = map.upsert(decimalREs, radix, () => {
                // for radices 2-10: decimal digits only; otherwise all digits and additional letters from A to radix
                const charClass = (radix <= 10) ? `0-${radix - 1}` : `0-9A-${fmt.formatAlphabetic(radix - 10)}`;
                return new RegExp(`^[${charClass}]+$`, "i");
            });

            // test the input value (`parseInt()` would ignore trailing garbage)
            if (!testRE.test(value)) { throw ErrorCode.NUM; }

            // convert text to decimal number
            const decimal = parseInt(value, radix);
            return (Number.isFinite(decimal) && (decimal >= 0)) ? decimal : ErrorCode.NUM.throw();
        }
    });

    factory.binary1("HEX2BIN", "val:str", ["val:str", "val:int"], (n, l) => convertDecToBin(convertHexToDec(n), l));

    factory.unary("HEX2DEC", "val:num", "val:str", convertHexToDec);

    factory.binary1("HEX2OCT", "val:str", ["val:str", "val:int"], (n, l) => convertDecToOct(convertHexToDec(n), l));

    factory.binary1("OCT2BIN", "val:str", ["val:str", "val:int"], (n, l) => convertDecToBin(convertOctToDec(n), l));

    factory.unary("OCT2DEC", "val:num", "val:str", convertOctToDec);

    factory.binary1("OCT2HEX", "val:str", ["val:str", "val:int"], (n, l) => convertDecToHex(convertOctToDec(n), l));

    factory.binary1("ROMAN", "val:str", ["val:int", "val:int"], (n, depth) => {

        // default is depth 0 (classic mode)
        depth = depth || 0;

        // check the input parameters
        if ((depth < 0) || (depth >= 5) || (n < 0) || (n > 3999)) { throw ErrorCode.VALUE; }

        // use number formatter to format to a roman number
        return fmt.formatRoman(n, { depth: depth as RomanDepth });
    });
});
