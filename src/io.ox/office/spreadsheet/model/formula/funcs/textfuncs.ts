/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with text.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { is, str, re, unicode, fun, ary, map } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { dround } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import type { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { ScalarIteratorOptions, AggregateAggregatorFn, AggregateFinalizerFn, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

const { floor, round, abs } = Math;
const { fromCharCode } = String;

// constants ==================================================================

// ANSI character mapping for `CODE` (Unicode character to ANSI code where different)
const CODE_MAP = new Map<string, number>([
    ["\u20AC", 128], ["\u0160", 138], ["\u2013", 150],
    ["\u201A", 130], ["\u2039", 139], ["\u2014", 151],
    ["\u0192", 131], ["\u0152", 140], ["\u02DC", 152],
    ["\u201E", 132], ["\u017D", 142], ["\u2122", 153],
    ["\u2026", 133], ["\u2018", 145], ["\u0161", 154],
    ["\u2020", 134], ["\u2019", 146], ["\u203A", 155],
    ["\u2021", 135], ["\u201C", 147], ["\u0153", 156],
    ["\u02C6", 136], ["\u201D", 148], ["\u017E", 158],
    ["\u2030", 137], ["\u2022", 149], ["\u0178", 159]
]);

// ANSI character mapping for `CHAR` (ANSI code to Unicode character)
const CHAR_MAP = ary.generate(256, code => map.findKey(CODE_MAP, v => v === code) ?? fromCharCode(code));

// all characters to be deleted by `CLEAN`
// TODO: other Unicode characters
const CLEAN_RE = /[\x00-\x1f\x80-\x9f\xad]/g;

// all characters to be converted by `PROPER`
// TODO: the reg-exp does not include all characters that count as letters
const PROPER_RE = /[A-Za-z\xAA\xB5\xBA\xC0-\xD6\xD8-\xF6\xF8-\u02B8\u02BB-\u02C1\u02D0-\u02D1\u02E0-\u02E4\u02EE\u0370-\u0373\u0376-\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0523]+/g;

// text particles for `BAHTTEXT`
const BT_0 = "\u0E28\u0E39\u0E19\u0E22\u0E4C"; // used for number zero, not for digits
const BT_1_L = "\u0E2B\u0E19\u0E36\u0E48\u0E07"; // leading one (number one, or one hundred, one thousand, etc.)
const BT_1_T = "\u0E40\u0E2D\u0E47\u0E14"; // trailing one (in 11, 21, 31, 201, 2001, etc.)
// all digits but zero, used as prefixes for powers of 10 (with the "leading one" variant)
const BT_DIGITS = [null, BT_1_L, "\u0E2A\u0E2D\u0E07", "\u0E2A\u0E32\u0E21", "\u0E2A\u0E35\u0E48", "\u0E2B\u0E49\u0E32", "\u0E2B\u0E01", "\u0E40\u0E08\u0E47\u0E14", "\u0E41\u0E1B\u0E14", "\u0E40\u0E01\u0E49\u0E32"];
// special combinations
const BT_10 = "\u0E2A\u0E34\u0E1A"; // number 10, also used as suffix for twenty, thirty, etc.
const BT_20 = "\u0E22\u0E35\u0E48"; // prefix for twenty: two-ten (BT_20 + BT_10)
const BT_1E2 = "\u0E23\u0E49\u0E2D\u0E22"; // hundreds
const BT_1E3 = "\u0E1E\u0E31\u0E19"; // thousands
const BT_1E4 = "\u0E2B\u0E21\u0E37\u0E48\u0E19"; // ten thousands
const BT_1E5 = "\u0E41\u0E2A\u0E19"; // hundred thousands
const BT_1E6 = "\u0E25\u0E49\u0E32\u0E19"; // millions
const BT_MINUS = "\u0E25\u0E1A";
const BT_BAHT = "\u0E1A\u0E32\u0E17";
const BT_SATANG = "\u0E2A\u0E15\u0E32\u0E07\u0E04\u0E4C";
const BT_NO_SATANG = "\u0E16\u0E49\u0E27\u0E19"; // suffix for zero Satang
const BT_NO_SATANG_M = "\u0E16\u0E49"; // suffix for negative rounded zero Baht, zero Satang

// maximal format string for decimal places for functions like `FIXED` and `DOLLAR`
const MAX_PLACES_STR = ".".padEnd(128, "0");

// options for iterating strings in operands of `CONCAT` and `TEXTJOIN`
const AGGREGATE_OPTIONS: ScalarIteratorOptions = {
    multiRange: false, // do not accept multi-range references
    multiSheet: true   // accept multi-sheet references
};

// private functions ==========================================================

function getPlacesFormatStr(places: number): string {
    return (places > 0) ? MAX_PLACES_STR.slice(0, places + 1) : "";
}

function formatWithPlaces(context: FormulaContext, formatCode: string, n: number, places: number): string {
    const rounded = dround(n, places, "round");
    const result = context.numberFormatter.formatValue(formatCode, rounded).text;
    return (result && (result.length <= 255)) ? result : ErrorCode.VALUE.throw();
}

/**
 * Generates the text for a single decimal digit for `BAHTTEXT`.
 */
function formatBahtDigit(n: number, pw10: number, pw10Name: string): string {
    const digit = floor(n / pw10) % 10;
    return (digit > 0) ? (BT_DIGITS[digit]! + pw10Name) : "";
}

/**
 * Generates the text for entire Baht for a number less than one million.
 */
function formatBahtBlock(n: number, leading: boolean, million: boolean): string {

    let result = "";

    // 100k, 10k, 1k, and 100 are simple combinations of digit and decimal suffix
    result += formatBahtDigit(n, 1e5, BT_1E5);
    result += formatBahtDigit(n, 1e4, BT_1E4);
    result += formatBahtDigit(n, 1e3, BT_1E3);
    result += formatBahtDigit(n, 1e2, BT_1E2);

    // create the text for tens
    const tens = floor(n / 10) % 10;
    if (tens === 1) {
        result += BT_10; // no prefix before "ten" for numbers 10 to 19
    } else if (tens === 2) {
        result += BT_20 + BT_10; // use alternative word for digit 2 in "twenty"
    } else if (tens >= 3) {
        result += BT_DIGITS[tens]! + BT_10; // digits 3 to 9 are regular
    }

    // append the text for the last digit
    if (leading && (n === 1)) {
        result += BT_1_L; // "leading one" variant only if nothing precedes the number one
    } else {
        const ones = n % 10;
        if (ones === 1) {
            result += BT_1_T; // use the "trailing one" variant
        } else if (ones >= 2) {
            result += BT_DIGITS[ones]!; // BT_DIGITS contains strings for `ones >= 2`
        }
    }

    return million ? (result + BT_1E6) : result;
}

/**
 * Implementation of the `BAHTTEXT` function.
 */
function formatBaht(n: number): string {

    // split the input value into Baht and Satang
    const minus = n < 0;
    let satang = round(abs(n) * 100);
    let baht = floor(satang / 100);
    satang %= 100;

    // special case: (positive or negative) zero Baht, zero Satang (Excel returns different suffixes?!)
    if ((baht === 0) && (satang === 0)) {
        return minus ? (BT_MINUS + BT_0 + BT_BAHT + BT_NO_SATANG_M) : (BT_0 + BT_BAHT + BT_NO_SATANG);
    }

    // the function result
    let result = "";

    // build the text for Baht
    if (baht > 0) {
        let million = false;
        while (baht > 0) {
            result = formatBahtBlock(baht % 1e6, baht < 1e6, million) + result;
            baht = floor(baht / 1e6);
            million = true;
        }
        result += BT_BAHT;
    }

    // prepend the minus
    if (minus) { result = BT_MINUS + result; }

    // append the text for Satang
    return result + ((satang > 0) ? (formatBahtBlock(satang, true, false) + BT_SATANG) : BT_NO_SATANG);
}

// exports ====================================================================

export default declareFunctionSpecModule("text", factory => {

    const ANY_MAT_PASS = factory.param("any", { mat: "pass" });

    factory.unaryStr("ASC", {
        hidden: true
    });

    factory.unary("BAHTTEXT", "val:str", "val:num", {
        name: { odf: "COM.MICROSOFT.BAHTTEXT" },
        resolve: formatBaht
    });

    // uses ANSI (aka Windows-1252) encoding
    factory.unary("CHAR", "val:str", "val:int", code => ((code >= 1) && (code <= 255)) ? CHAR_MAP[code] : ErrorCode.VALUE.throw());

    // TODO: other Unicode characters
    factory.unaryStr("CLEAN", text => text.replace(CLEAN_RE, ""));

    // uses ANSI (aka Windows-1252) encoding
    factory.unary("CODE", "val:num", "val:str", text => {
        if (text.length === 0) { throw ErrorCode.VALUE; }
        const code = CODE_MAP.get(text[0]) ?? text.charCodeAt(0);
        return ((code >= 1) && (code <= 255)) ? code : 63;
    });

    factory.unbound("CONCAT", "val:str", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.CONCAT", odf: null },
        resolve(...operands: Operand[]) {
            // the aggregation function (throws early, if the string length exceeds the limit)
            const aggregate = (str1: string, str2: string): string => this.concatStrings(str1, str2);
            return this.aggregateStrings(operands, "", aggregate, fun.identity, AGGREGATE_OPTIONS);
        }
    });

    // each parameter is a single value (function does not concatenate cell ranges or matrixes!)
    factory.unbound("CONCATENATE", "val:str", 1, 1, ["val:str"],  function (...operands: string[]) {
        return this.concatStrings(...operands);
    });

    factory.unaryStr("DBCS", {
        name: { odf: "JIS" },
        hidden: true
    });

    factory.binary1("DOLLAR", "val:str", ["val:num", "val:int"], function (n, places) {

        // check decimal places parameter
        if (!is.number(places)) { places = 2; }
        if (places > 127) { throw ErrorCode.VALUE; }

        // format passed value and validate length
        const replaceStr = getPlacesFormatStr(places);
        const formatCode = this.numberFormatter.getCurrencyFormat().formatCode.replace(/\.00/g, replaceStr);
        return formatWithPlaces(this, formatCode, n, places);
    });

    // exact case-sensitive comparison
    factory.binary("EXACT", "val:bool", ["val:str", "val:str"], (str1, str2) => str1 === str2);

    factory.ternary2("FIND", "val:num", ["val:str", "val:str", "val:int"], function (searchText, text, index) {
        if (!is.number(index)) { index = 1; }
        this.checkStringIndex(index);
        index -= 1;
        // do not search, if index is too large (needed if searchText is empty)
        if (index > text.length - searchText.length) { throw ErrorCode.VALUE; }
        // FIND searches case-sensitive
        index = text.indexOf(searchText, index);
        if (index < 0) { throw ErrorCode.VALUE; }
        return index + 1;
    });

    factory.ternary2("FINDB", "val:num", ["val:str", "val:str", "val:int"], {
        hidden: true,
        resolve: "FIND"
    });

    factory.ternary1("FIXED", "val:str", ["val:num", "val:int", "val:bool"], function (n, places, noGroups) {

        // check decimal places parameter
        if (!is.number(places)) { places = 2; }
        if (places > 127) { throw ErrorCode.VALUE; }

        // format passed value and validate length
        const formatCode = `${noGroups ? "0" : "#,##0"}${getPlacesFormatStr(places)}`;
        return formatWithPlaces(this, formatCode, n, places);
    });

    // function returns number of Unicode characters, not UTF-16 code units! (DOCS-2647: Unicode support)
    factory.binary1("LEFT", "val:str", ["val:str", "val:int"], function (text, count) {
        if (!is.number(count)) { count = 1; }
        this.checkStringLength(count);
        return unicode.slice(text, 0, count);
    });

    factory.binary1("LEFTB", "val:str", ["val:str", "val:int"], {
        hidden: true,
        resolve: "LEFT"
    });

    // function returns number of UTF-16 code units, not of Unicode characters! (DOCS-2647: Unicode support)
    factory.unary("LEN", "val:num", "val:str", text => text.length);

    factory.unary("LENB", "val:num", "val:str", {
        hidden: true,
        resolve: "LEN"
    });

    factory.unaryStr("LOWER", text => text.toLowerCase());

    // function counts UTF-16 code units, not Unicode characters! (DOCS-2647: Unicode support)
    factory.ternary("MID", "val:str", ["val:str", "val:int", "val:int"], function (text, start, count) {
        this.checkStringIndex(start);
        this.checkStringLength(count);
        // DOCS-2647: replace spliced surrogate characters
        return text.substr(start - 1, count).replace(/^[\udc00-\udfff]|[\ud800-\udbff]$/, "\ufffd");
    });

    factory.ternary("MIDB", "val:str", ["val:str", "val:int", "val:int"], {
        hidden: true,
        resolve: "MID"
    });

    factory.ternary1("NUMBERVALUE", "val:num", ["val:str", "val:str", "val:str"], {
        name: { ooxml: "_xlfn.NUMBERVALUE" },
        resolve(text, decSep, groupSep) {

            if (!is.string(decSep)) { decSep = LOCALE_DATA.dec; }
            if (decSep.length === 0) { throw ErrorCode.VALUE; }

            if (!is.string(groupSep)) { groupSep = LOCALE_DATA.group; }
            if (groupSep.length === 0) { throw ErrorCode.VALUE; }

            decSep = decSep[0];
            groupSep = groupSep[0];
            if (decSep === groupSep) { throw ErrorCode.VALUE; }

            if (text.length === 0) { return 0; }

            let portions = text.split(decSep, 3);
            if (portions.length > 2) { throw ErrorCode.VALUE; }

            const groupRE = new RegExp(re.escape(groupSep), "g");
            if ((portions.length === 2) && groupRE.test(portions[1])) { throw ErrorCode.VALUE; }

            portions = portions.map(portion => portion.replace(groupRE, ""));

            let preparedText = portions.join(LOCALE_DATA.dec);
            const matches = /%+/.exec(preparedText);
            let percents: Opt<string>;

            if (matches) {
                percents = matches[0];
                preparedText = preparedText.replace(percents, "%");
            }

            let value = this.numberFormatter.parseScalarValue(preparedText).value;
            if (!is.number(value)) { throw ErrorCode.VALUE; }

            if (percents && (percents.length > 1)) {
                value /= 10 ** (2 * percents.length - 2);
            }

            return value;
        }
    });

    factory.unary("PHONETIC", "val:str", "ref:single", {
        name: { odf: null },
        hidden: true
    });

    factory.unaryStr("PROPER", text => text.replace(PROPER_RE, str.capitalizeFirst));

    factory.fixed("REPLACE", "val:str", 4, 4, ["val:str", "val:int", "val:int", "val:str"], {
        resolve(oldText: string, start: number, count: number, newText: string) {
            this.checkStringIndex(start);
            this.checkStringLength(count);
            start -= 1;
            return str.replaceSubStr(oldText, start, start + count, newText);
        }
    });

    factory.fixed("REPLACEB", "val:str", 4, 4, ["val:str", "val:int", "val:int", "val:str"], {
        hidden: true,
        resolve: "REPLACE"
    });

    factory.binary("REPT", "val:str", ["val:str", "val:int"], function (text, repeat) {
        return this.repeatString(text, repeat);
    });

    // function returns number of Unicode characters, not UTF-16 code units! (DOCS-2647: Unicode support)
    factory.binary1("RIGHT", "val:str", ["val:str", "val:int"], function (text, count) {
        if (!is.number(count)) { count = 1; }
        this.checkStringLength(count);
        return count ? unicode.slice(text, -count) : "";
    });

    factory.binary1("RIGHTB", "val:str", ["val:str", "val:int"], {
        hidden: true,
        resolve: "RIGHT"
    });

    factory.unaryStr("ROT13", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.ROT13" },
        resolve: text => text.replace(/[a-zA-Z]/g, a => {
            const code = a.charCodeAt(0);
            const up = ((code < 91) ? 78 : 110) > code;
            return fromCharCode(up ? (code + 13) : (code - 13));
        })
    });

    factory.ternary2("SEARCH", "val:num", ["val:str", "val:str", "val:int"], function (searchText, text, index) {

        // prepare and check the passed index
        if (!is.number(index)) { index = 1; }
        this.checkStringIndex(index);
        index -= 1;

        // return index, if `searchText` is empty or a "match all" selector
        if (/^\**$/.test(searchText)) {
            if (text.length <= index) { throw ErrorCode.VALUE; }
            return index + 1;
        }

        // shorten the passed text according to the start index
        text = text.slice(index);

        // create the regular expression
        const regExp = this.convertPatternToRegExp(searchText);
        // find first occurrence of the pattern (method `String.match()`
        // with non-global RE adds extra property `index` to the result array)
        const matches = regExp.exec(text) as Required<RegExpMatchArray> | null;
        if (!matches) { throw ErrorCode.VALUE; }

        // return the correct (one-based) index of the search text
        return matches.index + index + 1;
    });

    factory.ternary2("SEARCHB", "val:num", ["val:str", "val:str", "val:int"], {
        hidden: true,
        resolve: "SEARCH"
    });

    factory.fixed("SUBSTITUTE", "val:str", 3, 4, ["val:str", "val:str", "val:str", "val:int"], {
        resolve(text: string, replaceText: string, newText: string, index?: number) {

            // check passed index first (one-based index)
            if (is.number(index) && (index <= 0)) { throw ErrorCode.VALUE; }

            // do nothing, if the text to be replaced is empty
            if (replaceText.length === 0) { return text; }

            // the regular expression that will find the text(s) to be replaced (SUBSTITUTE works case-sensitive)
            const regExp = new RegExp(re.escape(replaceText), "g");

            // replace all occurrences
            if (!is.number(index)) {
                return text.replace(regExp, newText);
            }

            // replace the specified occurrence of the text
            const tokens = text.split(regExp);
            if (index < tokens.length) {
                tokens[index - 1] += newText + tokens[index];
                tokens.splice(index, 1);
                return tokens.join(replaceText);
            }

            // index too large: return original text
            return text;
        }
    });

    factory.unary("T", "val:str", "val", v => is.string(v) ? v : "");

    factory.binary("TEXT", "val:str", ["val", "val:str"], {
        recalc: "once", // result depends on GUI language
        resolve(value, format) {
            // try to convert strings to numbers, but use original data if that fails
            const num = is.string(value) ? this.numberFormatter.convertScalarToNumber(value) : null;
            const result = this.numberFormatter.formatValue(format, num ?? value, { uiGrammar: true }).text;
            return result ?? ErrorCode.VALUE.throw();
        }
    });

    factory.unbound("TEXTJOIN", "val:str", 3, 1, ["val:str", "val:bool", ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.TEXTJOIN", odf: null },
        resolve(delimiter: string, skipEmpty: boolean, ...operands: Operand[]) {

            // offset of the last visited string
            let lastOffset = 0;

            // concatenate the strings with the delimiter
            const aggregate: AggregateAggregatorFn<string, string> = (text1, text2, _index, offset) => {
                // silently skip empty strings
                if (text2.length === 0) { return text1; }
                // repeat delimiter for sequences of blank cells
                const delimCount = !skipEmpty ? (offset - lastOffset) : (lastOffset < offset) ? 1 : 0;
                const delim = (delimCount > 0) ? this.repeatString(delimiter, delimCount) : "";
                lastOffset = offset;
                return this.concatStrings(text1, delim, text2);
            };

            // finally, add trailing separators for blank cells
            const finalize: AggregateFinalizerFn<string, string> = (text, _count, size) =>
                skipEmpty ? text : (text + this.repeatString(delimiter, size - lastOffset - 1));

            return this.aggregateStrings(operands, "", aggregate, finalize, AGGREGATE_OPTIONS);
        }
    });

    // TRIM removes space characters (U+0020) only, no other white-space or control characters
    factory.unaryStr("TRIM", text => text.replace(/^ +| +$/g, "").replace(/ {2,}/g, " "));

    // function supports full range of Unicode code points
    factory.unary("UNICHAR", "val:str", "val:int", {
        name: { ooxml: "_xlfn.UNICHAR" },
        resolve: code => {
            if ((code < 1) || (code > 0x10FFFF)) { throw ErrorCode.VALUE; }
            // UTF-16 surrogates
            if ((code >= 0xD800) && (code <= 0xDFFF)) { throw ErrorCode.NA; }
            // 32 reserved characters in Arabic presentation block
            if ((code >= 0xFDD0) && (code <= 0xFDEF)) { throw ErrorCode.NA; }
            // `FFFE` and `FFFF` are reserved in *all* planes
            if ((code & 0xFFFE) === 0xFFFE) { throw ErrorCode.NA; }
            // convert code point to Unicode character
            return String.fromCodePoint(code);
        }
    });

    // function supports full range of Unicode code points
    factory.unary("UNICODE", "val:num", "val:str", {
        name: { ooxml: "_xlfn.UNICODE" },
        resolve: value => value ? value.codePointAt(0)! : ErrorCode.VALUE.throw()
    });

    factory.unaryStr("UPPER", text => text.toUpperCase());

    factory.binary1("USDOLLAR", "val:str", ["val:num", "val:int"], {
        name: { odf: null },
        hidden: true,
        resolve: "DOLLAR"
    });

    factory.unary("VALUE", "val:num", "val", function (value) {
        // text can be converted to number, but boolean values will result in #VALUE!
        if (is.number(value)) { return value; }
        if (is.string(value)) { return this.convertToNumber(value); }
        return (value === null) ? 0 : ErrorCode.VALUE.throw();
    });
});
