/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with boolean
 * values.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 *****************************************************************************/

import { fun } from "@/io.ox/office/tk/algorithms";

import { type ScalarType, ErrorCode, isErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import type { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { ConvertIteratorOptions, OperandIteratorSource, AggregateAggregatorFn, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// constants ==================================================================

// standard options for logical aggregation functions (AND, OR, XOR)
const AGGREGATE_OPTIONS: ConvertIteratorOptions = { // id: CSS5
    valMode: "convert", // value operands: convert strings and numbers to booleans
    matMode: "skip",    // matrix operands: skip all strings
    refMode: "skip",    // reference operands: skip all strings
    emptyParam: true,   // empty parameters count as FALSE
    multiRange: true,   // accept multi-range references
    multiSheet: true    // accept multi-sheet references
};

// private functions ==========================================================

/**
 * Resolve an operand value according to the condition for the `IF` function
 * (called repeatedly in matrix context).
 */
function getIfOperand(context: FormulaContext, cond: boolean): Operand;
function getIfOperand(context: FormulaContext, cond: boolean, scalar: true): ScalarType;
// implementation
function getIfOperand(context: FormulaContext, cond: boolean, scalar?: boolean): ScalarType | Operand {
    const index = cond ? 1 : 2;
    return (index < context.getOperandCount()) ? context.getOperand(index, scalar ? "val:any" : "any") : cond;
}

/**
 * Creates and returns a resolver function for spreadsheet functions that
 * reduces all boolean values (and other values convertible to boolean values)
 * of all function parameters to the result of the function.
 *
 * @returns
 *  The resulting callback function implementation to be assigned to the
 *  `resolve` property of a function resource. The spreadsheet function will
 *  pass all boolean values of all its operands to the aggregation callback
 *  function. If no boolean values have been found at all, the spreadsheet
 *  function will result in the error code `#VALUE!`, instead of the initial
 *  result.
 */
function implementLogicalAggregation(initial: boolean, aggregate: AggregateAggregatorFn<boolean, boolean>) {

    // no booleans available results in the error code #VALUE! (not the initial value)
    function finalize(result: boolean, count: number): boolean {
        return (count === 0) ? ErrorCode.VALUE.throw() : result;
    }

    // create and return the resolver function
    return function logicalAggregator(this: FormulaContext, ...sources: OperandIteratorSource[]): boolean {
        return this.aggregateBooleans(sources, initial, aggregate, finalize, AGGREGATE_OPTIONS);
    };
}

// exports ====================================================================

export default declareFunctionSpecModule("logical", factory => {

    const BOOL_MAT_PASS = factory.param("val:bool", { mat: "pass" });
    const ANY_MAT_PASS = factory.param("any", { mat: "pass" });
    const ANY_LAZY_PASS = factory.param("any:lazy", { dep: "pass" });
    const ANY_LAZY_PASS_FORWARD = factory.param("any:lazy", { dep: "pass", mat: "forward" });

    // empty parameters count as FALSE: AND(TRUE,) = FALSE
    factory.unbound("AND", "val:bool", 1, 1, [ANY_MAT_PASS], {
        resolve: implementLogicalAggregation(true, (bool1, bool2) => bool1 && bool2)
    });

    factory.nullaryBool("FALSE", false);

    factory.fixed("IF", "any", { ooxml: 2, odf: 1 }, 3, [BOOL_MAT_PASS, ANY_LAZY_PASS_FORWARD, ANY_LAZY_PASS_FORWARD], function (cond: boolean) {

        // Under some circumstances, combine the result as matrix element-by-element from
        // the second and third parameter, otherwise pass through the result unmodified.
        //
        // Example (simple cell formulas):
        // =ISREF(IF(TRUE,A1:B2)) results in TRUE (the reference A1:B2 directly)
        // =ISREF(IF({TRUE},A1:B2)) results in FALSE (a matrix built from A1:B2)
        // =IF({1|0},{2|3},{4|5}) results in {2|5} (result matrix built element-by-element)
        //
        // Example (matrix formula with reference selector):
        // {=IF(A1:B2,{2|3},{4|5})} combines the matrixes according to A1:B2
        //
        if (
            ((this.contextType !== "val") && (this.getOperand(0).type === "mat")) ||
            ((this.contextType === "mat") && (this.getOperand(0).type !== "val"))
        ) {

            // the dimension of the result matrix is the boundary of all parameters
            const matrixDim = this.getOperands(0).reduce((dim: Dimension | null, operand) =>
                Dimension.boundary(dim, operand.getDimension({ errorCode: ErrorCode.NA })), // #N/A thrown for complex references
            null)!;

            // build the matrix by evaluating all conditions separately
            return this.aggregateMatrix(matrixDim, () => getIfOperand(this, this.getOperand(0, "val:bool"), true));
        }

        // otherwise, return one of the unconverted operands
        return getIfOperand(this, cond);
    });

    // always returns scalar values (in difference to IF)
    factory.binaryScalar("IFERROR", (value1, value2) => isErrorCode(value1) ? value2 : value1);

    // always returns scalar values (in difference to IF)
    factory.binaryScalar("IFNA", {
        name: { ooxml: "_xlfn.IFNA" },
        resolve: (value1, value2) => (value1 === ErrorCode.NA) ? value2 : value1
    });

    factory.unbound("IFS", "any", 2, 2, ["val:bool", ANY_LAZY_PASS], {
        name: { ooxml: "_xlfn.IFS", odf: null },
        resolve() {
            for (let index = 0, length = this.getOperandCount(); index < length; index += 2) {
                if (this.getOperandValue(index)) { return this.getOperand(index + 1); }
            }
            throw ErrorCode.NA;
        }
    });

    factory.unaryBool("NOT", fun.not);

    // empty parameters count as FALSE (but do not skip to catch empty-only parameters)
    factory.unbound("OR", "val:bool", 1, 1, [ANY_MAT_PASS], {
        resolve: implementLogicalAggregation(false, (bool1, bool2) => bool1 || bool2)
    });

    factory.nullaryBool("TRUE", true);

    factory.unbound("XOR", "val:bool", 1, 1, [ANY_MAT_PASS], {
        name: { ooxml: "_xlfn.XOR" },
        // empty parameters count as FALSE (but do not skip to catch empty-only parameters)
        resolve: implementLogicalAggregation(false, (bool1, bool2) => bool1 !== bool2)
    });
});
