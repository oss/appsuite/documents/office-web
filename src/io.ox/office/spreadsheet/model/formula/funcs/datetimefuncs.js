/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with date and time.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 * In spreadsheet documents, dates and times are represented by floating-point
 * numbers (integral part is number of days, counted from a specific "null
 * date", fractional part is the time of a day). The client-side formula engine
 * provides the function signature type "val:date", and uses the JavaScript
 * class `Date` containing the UTC date and time received from spreadsheet
 * cells, or other functions.
 *
 *****************************************************************************/

import { is, ary } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import {
    ISOWeekDay, DateAddMethod, DateCountMethod, Days360Method,
    isLeapYear, expandYear, makeUTCDate, getLocalTodayAsUTC, getLocalNowAsUTC,
    getDaysInMonth, getDaysInYear, addDaysToDate, addMonthsToDate,
    getISOWeekDay, getFirstDayOfISOWeek,
    countDays, countWeeks, countMonths, countYears,
    getWeekNumber, getDays360, getYearFrac
} from "@/io.ox/office/spreadsheet/model/formula/utils/dateutils";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

const { abs, floor, ceil } = Math;

// constants ==================================================================

// standard options for date parameter iterators
const RCONVERT_ALL_SKIP_EMPTY = { // id: RRR0
    valMode: "rconvert", // value operands: convert strings to numbers (but not booleans)
    matMode: "rconvert", // matrix operands: convert strings to numbers (but not booleans)
    refMode: "rconvert", // reference operands: convert strings to numbers (but not booleans)
    emptyParam: false, // skip empty parameter
    multiRange: false, // do not accept multi-range references
    multiSheet: false, // do not accept multi-sheet references
    dates: true, // convert numbers to dates
    floor: true // remove time components from all dates
};

// maps date modes to first day of week for function "WEEKNUM"
const WEEKNUM_FIRST_WEEKDAY = {
    1: ISOWeekDay.SU,
    2: ISOWeekDay.MO,
    11: ISOWeekDay.MO,
    12: ISOWeekDay.TU,
    13: ISOWeekDay.WE,
    14: ISOWeekDay.TH,
    15: ISOWeekDay.FR,
    16: ISOWeekDay.SA,
    17: ISOWeekDay.SU,
    21: ISOWeekDay.MO
};

const WEEKEND_PATTERNS = [
    null,
    "0000011", // 1 = Sat,Sun
    "1000001", // 2 = Sun,Mon
    "1100000", // 3 = Mon,Tue
    "0110000", // 4 = Tue,Wed
    "0011000", // 5 = Wed,Thu
    "0001100", // 6 = Thu,Fri
    "0000110", // 7 = Fri,Sat
    null,
    null,
    null,
    "0000001", // 11 = Sun
    "1000000", // 12 = Mon
    "0100000", // 13 = Tue
    "0010000", // 14 = Wed
    "0001000", // 15 = Thu
    "0000100", // 16 = Fri
    "0000010"  // 17 = Sat
];

// private functions ==========================================================

/**
 * Converts the optional `countMode` parameter of ODF date count functions to a
 * `DateCountMethod` enum value.
 */
function getCountMethod(countMode) {
    return (countMode === 1) ? DateCountMethod.FIRST_DAY : DateCountMethod.WHOLE_COUNT;
}

function getWeekendFromParam(weekend) {

    // parameter omitted: Saturday/Sunday
    if (weekend === undefined) {
        return WEEKEND_PATTERNS[1];
    }

    // value null (empty parameter, or blank cell) used as zero; booleans used as numbers
    if ((weekend === null) || is.boolean(weekend)) {
        weekend = weekend ? 1 : 0;
    }

    // convert numbers to predefiend pattern
    if (is.number(weekend)) {
        weekend = WEEKEND_PATTERNS[floor(weekend)];
        if (weekend) { return weekend; }
        throw ErrorCode.NUM;
    }

    // strings must consist of 7 binary digits
    if (is.string(weekend) && /^[01]{7}$/.test(weekend)) {
        return weekend;
    }

    throw ErrorCode.VALUE;
}

function getWorkDaysPerWeek(weekendCode) {
    return (weekendCode.match(/0/g) || "").length;
}

function getWorkPieces(startDate, endDate, holidays) {
    //FIXME: parse to full days

    const pieces = [];
    holidays = holidays.filter(holiday => holiday >= startDate && holiday <= endDate);

    // DOCS-3363: MUST pass sorter callback to sort by date value (not by formatted date string)
    holidays.sort(function (d1, d2) { return d1.getTime() - d2.getTime(); });

    var lastWorkDay = startDate;
    for (var i = 0; i < holidays.length; i++) {
        var holiday = holidays[i];
        if (lastWorkDay && countDays(lastWorkDay, holiday) > 0) {
            pieces.push({ from: lastWorkDay, to: addDaysToDate(holiday, -1) });
            lastWorkDay = addDaysToDate(holiday, 1);
        } else {
            lastWorkDay = null;
        }
    }
    if (!lastWorkDay) {
        lastWorkDay = addDaysToDate(ary.at(holidays, -1), 1);
    }
    if (countDays(lastWorkDay, endDate) >= 0) {
        pieces.push({ from: lastWorkDay, to: endDate });
    }
    return pieces;
}

function getWorkDays(startDate, endDate, weekend, workDaysPerWeek) {
    var startWeekDay = getISOWeekDay(startDate);
    var endWeekDay = getISOWeekDay(endDate);

    var startMonday = getFirstDayOfISOWeek(startDate);
    var endMonday = getFirstDayOfISOWeek(endDate);

    var workDays = 0;
    var i = 0;

    if (countDays(startMonday, endMonday) === 0) {
        //same week and also one timeslot
        for (i = startWeekDay; i <= endWeekDay; i++) {
            if (weekend[i] === "0") {
                workDays++;
            }
        }
    } else {
        //first week slot
        for (i = startWeekDay; i <= 6; i++) {
            if (weekend[i] === "0") {
                workDays++;
            }
        }

        //last week slot
        for (i = 0; i <= endWeekDay; i++) {
            if (weekend[i] === "0") {
                workDays++;
            }
        }

        //all weeks between
        var endSunday = addDaysToDate(endMonday, -1);
        var weeksDiff = countWeeks(startMonday, endSunday, DateCountMethod.FIRST_DAY);
        workDays += weeksDiff * workDaysPerWeek;

    }
    return workDays;
}

function findNextWorkday(startDate, endDate, weekend, workDaysPerWeek, prefix, daysCounter) {
    if (prefix < 0) {
        var tmpDate = startDate;
        startDate = endDate;
        endDate = tmpDate;
    }

    var sliceWeeks = floor(countDays(startDate, endDate) / 7) * prefix;
    var restWeeks = floor(daysCounter.value / workDaysPerWeek);

    var weeks = abs(sliceWeeks) < abs(restWeeks) ? sliceWeeks : restWeeks;

    if (weeks < -1 || weeks > 1) {
        startDate = addDaysToDate(startDate, (weeks - prefix) * 7);
        daysCounter.value -= (weeks - prefix) * workDaysPerWeek;
    }

    var dayCount = countDays(startDate, endDate) * prefix;
    for (var i = 0; i <= dayCount; i++) {
        var currentDate = addDaysToDate(startDate, i * prefix);
        var currentWeekDay = getISOWeekDay(currentDate);
        if (weekend[currentWeekDay] === "0") {
            daysCounter.value -= prefix;
            if (daysCounter.value === 0) {
                return currentDate;
            }
        }
    }
}

function networkDaysIntl(context, startDate, endDate, weekendParam, holidaysParam) {
    var weekend = getWeekendFromParam(weekendParam);
    var workDaysPerWeek = getWorkDaysPerWeek(weekend);
    var holidays = context.getNumbersAsArray(holidaysParam, RCONVERT_ALL_SKIP_EMPTY);

    if (workDaysPerWeek === 0) {
        return 0;
    }
    var prefix = 1;

    // order start and end date
    if (endDate < startDate) {
        var tmpDate = startDate;
        startDate = endDate;
        endDate = tmpDate;
        prefix = -1;
    }

    var workDays = 0;

    if (holidays.length) {
        for (const workPiece of getWorkPieces(startDate, endDate, holidays)) {
            workDays += getWorkDays(workPiece.from, workPiece.to, weekend, workDaysPerWeek);
        }
    } else {
        workDays += getWorkDays(startDate, endDate, weekend, workDaysPerWeek);
    }
    return workDays * prefix;
}

function workDayIntl(context, startDate, days, weekendParam, holidaysParam) {

    var weekend = getWeekendFromParam(weekendParam);
    var workDaysPerWeek = getWorkDaysPerWeek(weekend);
    var holidays = context.getNumbersAsArray(holidaysParam, RCONVERT_ALL_SKIP_EMPTY);

    if (workDaysPerWeek === 0) {
        throw ErrorCode.VALUE;
    }
    if (days === 0) {
        return startDate;
    }
    var prefix = 1;
    var endDate = null;
    var daysCounter = {
        value: days
    };

    var holidayCount = holidays.length;
    var maxDays = ceil((abs(days) / workDaysPerWeek) * 7) + holidayCount;

    //the safe way
    maxDays *= 2;

    // order start and end date
    if (days < 0) {
        prefix = -1;
        endDate = startDate;
        startDate = addDaysToDate(startDate, maxDays * prefix);
        //first day is always ignored!!!
        endDate = addDaysToDate(endDate, prefix);
    } else {
        endDate = addDaysToDate(startDate, maxDays * prefix);
        //first day is always ignored!!!
        startDate = addDaysToDate(startDate, prefix);
    }

    var workPieces = null;
    if (holidays) {
        workPieces = getWorkPieces(startDate, endDate, holidays);
        if (prefix < 0) {
            workPieces.reverse();
        }
    } else {
        workPieces = [{ from: startDate, to: endDate }];
    }

    for (var i = 0; i < workPieces.length; i++) {
        var workPiece = workPieces[i];
        var result = findNextWorkday(workPiece.from, workPiece.to, weekend, workDaysPerWeek, prefix, daysCounter);
        if (result) {
            return result;
        }
    }
    throw ErrorCode.REF;
}

function parseDateTime(context, text) {
    var result = context.numberFormatter.parseScalarValue(text);
    var parsedFormat = context.numberFormatter.getParsedFormat(result.format);
    if (parsedFormat.isAnyDateTime()) {
        return result.value;
    }
    throw ErrorCode.VALUE;
}

// exports ====================================================================

export default declareFunctionSpecModule("datetime", factory => {

    factory.ternary("DATE", "val:date", ["val:int", "val:int", "val:int"], {
        format: "date",
        resolve(year, month, day) {
            // Excel adds 1900 to all years less than 1900; e.g. 1899 becomes 3799 (TODO: different behavior for ODF?)
            if (year < 1900) { year += 1900; }
            // create a UTC date object (Date object expects zero-based month)
            return makeUTCDate({ Y: year, M: month - 1, D: day });
        }
    });

    factory.ternary("DATEDIF", "val:num", ["val:day", "val:day", "val:str"], {
        hidden: true
    });

    factory.unary("DATEVALUE", "val:num", "val:str", function (dateText) {
        return floor(parseDateTime(this, dateText));
    });

    factory.unary("DAY", "val:num", "val:day", date => date.getUTCDate());

    factory.binary("DAYS", "val:num", ["val:day", "val:day"], {
        name: { ooxml: "_xlfn.DAYS" },
        // DAYS() expects end date before start date
        resolve(endDate, startDate) { return countDays(startDate, endDate); }
    });

    factory.ternary2("DAYS360", "val:num", ["val:day", "val:day", "val:bool"], (startDate, endDate, euroMode) => {
        // euroMode missing or FALSE: use 30/360 US mode
        const method = euroMode ? Days360Method.EUR : Days360Method.US_DAYS;
        return getDays360(startDate, endDate, method);
    });

    factory.unary("DAYSINMONTH", "val:num", "val:day", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.DAYSINMONTH" },
        resolve(date) { return getDaysInMonth(date.getUTCFullYear(), date.getUTCMonth()); }
    });

    factory.unary("DAYSINYEAR", "val:num", "val:day", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.DAYSINYEAR" },
        resolve(date) { return getDaysInYear(date.getUTCFullYear()); }
    });

    factory.unary("EASTERSUNDAY", "val:date", "val:int", {
        // AOO 4.4 writes "EASTERSUNDAY" (https://bz.apache.org/ooo/show_bug.cgi?id=126519)
        name: { ooxml: null, odf: ["ORG.OPENOFFICE.EASTERSUNDAY", "EASTERSUNDAY"] },
        format: "date",
        resolve(year) {

            // adjust two-digit years, check validity (TODO: correct error code?)
            year = expandYear(year);
            if ((year < 1583) || (year > 9999)) { throw ErrorCode.NUM; }

            // implementation of the Gauss algorithm, adapted from OOo source code (sc/source/core/tool/interpr2.cxx)
            const n = year % 19;
            const b = floor(year / 100);
            const c = year % 100;
            const d = floor(b / 4);
            const e = b % 4;
            const f = floor((b + 8) / 25);
            const g = floor((b - f + 1) / 3);
            const h = (19 * n + b - d - g + 15) % 30;
            const i = floor(c / 4);
            const k = c % 4;
            const l = (32 + 2 * e + 2 * i - h - k) % 7;
            const m = floor((n + 11 * h + 22 * l) / 451);
            const o = h + l - 7 * m + 114;

            return makeUTCDate({ Y: year, M: floor(o / 31) - 1, D: o % 31 + 1 });
        }
    });

    factory.binary("EDATE", "val:date", ["val:day", "val:int"], {
        format: "date",
        // set day in result to the day of the passed original date
        resolve(date, months) { return addMonthsToDate(date, months); }
    });

    factory.binary("EOMONTH", "val:date", ["val:day", "val:int"], {
        format: "date",
        // set day in result to the last day in the resulting month
        resolve(date, months) { return addMonthsToDate(date, months, DateAddMethod.LAST); }
    });

    factory.unary("HOUR", "val:num", "val:date", date => date.getUTCHours());

    factory.unary("ISLEAPYEAR", "val:bool", "val:day", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.ISLEAPYEAR" },
        resolve(date) { return isLeapYear(date.getUTCFullYear()); }
    });

    factory.unary("ISOWEEKNUM", "val:num", "val:day", {
        // called WEEKNUM in the UI of AOO/LO
        name: { ooxml: "_xlfn.ISOWEEKNUM" },
        resolve(date) { return getWeekNumber(date, ISOWeekDay.MO, true); }
    });

    factory.unary("MINUTE", "val:num", "val:date", date => date.getUTCMinutes());

    factory.unary("MONTH", "val:num", "val:day", date => date.getUTCMonth() + 1); // class `Date` returns zero-based month!

    factory.ternary("MONTHS", "val:num", ["val:day", "val:day", "val:int"], {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.MONTHS" },
        resolve(startDate, endDate, countMode) {
            return countMonths(startDate, endDate, getCountMethod(countMode));
        }
    });

    factory.ternary2("NETWORKDAYS", "val:num", ["val:day", "val:day", "any"], function (startDate, endDate, holidays) {
        return networkDaysIntl(this, startDate, endDate, 1, holidays);
    });

    factory.fixed("NETWORKDAYS.INTL", "val:num", 2, 4, ["val:day", "val:day", "val", "any"], {
        name: { odf: "COM.MICROSOFT.NETWORKDAYS.INTL" },
        resolve(startDate, endDate, weekend, holidays) {
            return networkDaysIntl(this, startDate, endDate, weekend, holidays);
        }
    });

    factory.nullary("NOW", "val:date", {
        format: "datetime",
        recalc: "always",
        resolve: getLocalNowAsUTC
    });

    factory.unary("SECOND", "val:num", "val:date", date => date.getUTCSeconds());

    factory.ternary("TIME", "val:date", ["val:int", "val:int", "val:int"], {
        format: "time",
        resolve(hour, minute, second) {

            // the time value, as fraction of a day
            const time = (hour / 24 + minute / 1440 + second / 86400) % 1;

            // must not result in a negative value
            if (time < 0) { throw ErrorCode.NUM; }

            // convert to a date object, using the document's current null date
            return this.convertToDate(time);
        }
    });

    factory.unary("TIMEVALUE", "val:num", "val:str", function (timeText) {
        return parseDateTime(this, timeText) % 1;
    });

    factory.nullary("TODAY", "val:date", {
        format: "date",
        recalc: "always",
        resolve: getLocalTodayAsUTC
    });

    factory.binary1("WEEKDAY", "val:num", ["val:day", "val:int"], (date, mode = 1) => {

        // weekday index, from 0 to 6, starting at Sunday
        const weekDay = date.getUTCDay();

        // missing parameter `undefined` defaults to 1 (but: empty parameter `null` defaults to 0!)
        switch (mode) {
            case 1:
                return weekDay + 1; // 1 to 7, starting at Sunday
            case 2:
                return (weekDay === 0) ? 7 : weekDay; // 1 to 7, starting at Monday
            case 3:
                return (weekDay + 6) % 7; // 0 to 6, starting at Monday
        }

        // other modes result in #NUM! error
        throw ErrorCode.NUM;
    });

    factory.binary1("WEEKNUM", "val:num", ["val:day", "val:int"], (date, mode) => {

        // first day in the week, according to passed date mode (blank and missing default to 1)
        const firstDayOfWeek = WEEKNUM_FIRST_WEEKDAY[is.number(mode) ? mode : 1];
        // bail out if passed date mode is invalid
        if (!is.number(firstDayOfWeek)) { throw ErrorCode.NUM; }

        return getWeekNumber(date, firstDayOfWeek, mode === 21);
    });

    // compatibility function for AOO
    factory.binary("WEEKNUM.AOO", "val:num", ["val:day", "val:int"], {
        name: { ooxml: null, odf: "ORG.LIBREOFFICE.WEEKNUM_OOO" },
        hidden: true,
        resolve(date, mode) {
            // date mode 1 for Sunday, every other value for Monday
            return getWeekNumber(date, (mode === 1) ? ISOWeekDay.SU : ISOWeekDay.MO, true);
        }
    });

    factory.ternary("WEEKS", "val:num", ["val:day", "val:day", "val:int"], {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.WEEKS" },
        resolve(startDate, endDate, countMode) {
            return countWeeks(startDate, endDate, getCountMethod(countMode));
        }
    });

    factory.unary("WEEKSINYEAR", "val:num", "val:day", {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.WEEKSINYEAR" },
        resolve(date) {
            // the 28th of December has always maximum week number in ISO8601 mode
            return getWeekNumber(makeUTCDate({ Y: date.getUTCFullYear(), M: 11, D: 28 }), ISOWeekDay.MO, true);
        }
    });

    factory.ternary2("WORKDAY", "val:date", ["val:day", "val:int", "any"], {
        format: "date",
        resolve(startDate, days, holidays) {
            return workDayIntl(this, startDate, days, undefined, holidays);
        }
    });

    factory.fixed("WORKDAY.INTL", "val:date", 2, 4, ["val:day", "val:int", "val", "any"], {
        name: { odf: "COM.MICROSOFT.WORKDAY.INTL" },
        resolve(startDate, days, weekend, holidays) {
            return workDayIntl(this, startDate, days, weekend, holidays);
        }
    });

    factory.unary("YEAR", "val:num", "val:day", date => date.getUTCFullYear());

    factory.ternary2("YEARFRAC", "val:num", ["val:day", "val:day", "val:int"], getYearFrac);

    factory.ternary("YEARS", "val:num", ["val:day", "val:day", "val:int"], {
        name: { ooxml: null, odf: "ORG.OPENOFFICE.YEARS" },
        resolve(startDate, endDate, countMode) {
            return countYears(startDate, endDate, getCountMethod(countMode));
        }
    });
});
