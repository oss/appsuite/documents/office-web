/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/******************************************************************************
 *
 * This module implements all spreadsheet functions dealing with cell range
 * references used as values (cell address arithmetics), and functions that
 * look up specific values in a range of cells.
 *
 * See the documentation file `README.md` in this directory for details.
 *
 * In spreadsheet documents, cell range references can be given literally, or
 * can be the result of functions or operators.
 *
 *****************************************************************************/

import { is, fun, itr } from "@/io.ox/office/tk/algorithms";

import type { ScalarTypeNE, ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { CompareResult, ErrorCode, isErrorCode, throwErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import type { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";

import { CellType } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { throwEngineError } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import { type ScalarMatrix, NumberMatrix, ensureMatrixDim } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import { encodeComplexSheetName } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { ScalarIteratorOptions, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import { declareFunctionSpecModule } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

const { floor } = Math;

// types ======================================================================

/** All supported matching modes for the lookup functions. */
enum MatchMode {

    /** Exact matches, wildcard support, unordered source data. */
    EXACT = "exact",
    /** Binary search, ascending order, nearest source value less or equal to lookup value. */
    LESS = "less",
    /** Binary search, descending order, nearest source value greater or equal to lookup value. */
    GREATER = "greater"
}

/** A result descriptor for looking up scalar values in an operator. */
interface NearestScalar {

    /** The scalar value found in the operator. */
    value: ScalarType;
    /** The linear offset of the scalar value in the operator. */
    offset: number;
}

/** Callback function for looking up scalar values with binary search. */
type NearestScalarResolver = (offset: number, minOffset: number, maxOffset: number) => Opt<NearestScalar>;

// constants ==================================================================

// iterator options for all lookup functions
const LOOKUP_OPTIONS: ScalarIteratorOptions = {
    emptyParam: true,
    blankCells: false,
    acceptErrors: true
};

// private functions ==========================================================

/**
 * Resolve an operand value according to the condition for the `CHOOSE`
 * function (called repeatedly in matrix context).
 */
function getChooseOperand(context: FormulaContext, index: number, count: number): Operand;
function getChooseOperand(context: FormulaContext, index: number, count: number, scalar: true): ScalarType;
// implementation
function getChooseOperand(context: FormulaContext, index: number, count: number, scalar?: boolean): ScalarType | Operand {
    if ((index < 1) || (index >= count)) { throw ErrorCode.VALUE; }
    return context.getOperand(index, scalar ? "val:any" : "any");
}

function createNearestScalarResolver(context: FormulaContext, lookupSource: Operand): NearestScalarResolver {

    // return scalar value, and an element of a matrix
    switch (lookupSource.type) {

        // scalar value: return directly
        case "val": {
            const value = lookupSource.getRawValue() as ScalarType;
            return () => ({ value, offset: 0 });
        }

        // matrix: return element value from first row or column
        case "mat": {
            const matrix = lookupSource.getMatrix();
            return offset => ({ value: matrix.getByIndex(offset), offset });
        }

        // reference: return nearest non-blank cell value from first row or column
        case "ref": {

            const range3d = context.convertToValueRange(lookupSource.getRanges());
            const sheetModel = context.getSheetModel(range3d.sheet1);
            const { cellCollection } = sheetModel;
            const range = range3d.toRange();

            return (offset, minOffset, maxOffset) => {

                // try to resolve address directly (quickly without creating an iterator)
                const address = range.addressAt(offset);
                const value = context.getCellValue(sheetModel, address);
                if (value !== null) { return { value, offset }; }

                // search for the first non-blank cell following the specified cell
                const nextAddress = cellCollection.findFirstCell(range, { startAddress: address, skipStart: true, type: CellType.VALUE });
                if (nextAddress) {
                    const nextOffset = range.indexAt(nextAddress);
                    if (nextOffset <= maxOffset) {
                        return { value: context.getCellValue(sheetModel, nextAddress), offset: nextOffset };
                    }
                }

                // search for the first non-blank cell preceding the specified cell
                const prevAddress = cellCollection.findFirstCell(range, { startAddress: address, skipStart: true, type: CellType.VALUE, reverse: true });
                if (prevAddress) {
                    const prevOffset = range.indexAt(prevAddress);
                    if (minOffset <= prevOffset) {
                        return { value: context.getCellValue(sheetModel, prevAddress), offset: prevOffset };
                    }
                }

                // no non-blank cell available in the range
                return undefined;
            };
        }

        default:
            throwEngineError("createNearestScalarResolver(): unknown operand type");
    }
}

/**
 * A search algorithms for all lookup functions (`MATCH`, `LOOKUP`, `HLOOKUP`,
 * and `VLOOKUP`).
 *
 * @param context
 *  The formula context needed to resolve cell values from references.
 *
 * @param lookupValue
 *  The value to be searched in the lookup source.
 *
 * @param lookupSource
 *  The lookup source to search the specified value in.
 *
 * @param lookupDim
 *  The resolved dimension of the passed lookup source.
 *
 * @param lookupRow
 *  Whether to search in the first row of the lookup source (`true`), or in the
 *  first column (`false`).
 *
 * @param matchMode
 *  The search algorithm to be used.
 *
 * @returns
 *  The result descriptor for the matching value in the lookup source.
 *
 * @throws
 *  The error code `#N/A`, if no value has been found in the lookup source.
 */
function lookupMatchingResult(
    context: FormulaContext,
    lookupValue: ScalarTypeNE,
    lookupSource: Operand,
    lookupDim: Dimension,
    lookupRow: boolean,
    matchMode: MatchMode
): NearestScalar {

    // returns existing result, throws #N/A otherwise
    function ensureResult(result: Opt<NearestScalar>): NearestScalar {
        return result ?? ErrorCode.NA.throw();
    }

    // shrink to lookup vector for correct offset handling
    const lookupWidth = lookupRow ? lookupDim.rows : lookupDim.cols;
    if (lookupWidth > 1) {
        switch (lookupSource.type) {
            case "mat": {
                const matrix = lookupSource.getMatrix();
                const vector = lookupRow ? matrix.toRowVector(0) : matrix.toColVector(0);
                lookupSource = context.createOperand(vector);
                break;
            }
            case "ref": {
                const range3d = lookupSource.getRanges()[0].clone();
                range3d.setEnd(range3d.getStart(!lookupRow), !lookupRow);
                lookupSource = context.createOperand(range3d);
                break;
            }
            default: break;
        }
    }

    // exact match, unordered source list, pattern matching support
    if (matchMode === MatchMode.EXACT) {
        const iterator = context.createScalarIterator(lookupSource, LOOKUP_OPTIONS);
        const exactMatcher = context.createScalarMatcher(lookupValue);
        return ensureResult(itr.find(iterator, opState => exactMatcher(opState.value)));
    }

    // a callback function that returns matrix elements, or the nearest non-blank cells
    const scalarResolver = createNearestScalarResolver(context, lookupSource);
    // a scalar matcher that compares the lookup value to a scalar value
    const scalarMatcher = (matchMode === MatchMode.LESS) ?
        (value: ScalarType) => context.compareScalars(value, lookupValue) <= CompareResult.EQUAL :
        (value: ScalarType) => context.compareScalars(value, lookupValue) >= CompareResult.EQUAL;

    // the binary search interval
    let minOffset = 0;
    let maxOffset = (lookupRow ? lookupDim.cols : lookupDim.rows) - 1;
    // last matching result object with scalar value and offset
    let prevResult: Opt<NearestScalar>;

    // narrow down the offset interval
    while (minOffset <= maxOffset) {

        // the current offset in the binary search interval
        const currOffset = floor((minOffset + maxOffset) / 2);
        // fetch the scalar value from the current offset
        const nextResult = scalarResolver(currOffset, minOffset, maxOffset);

        // blank cells only: return previous result
        if (!nextResult) { return ensureResult(prevResult); }

        // if the scalar value matches, search in trailing interval
        if (scalarMatcher(nextResult.value)) {
            prevResult = nextResult;
            minOffset = nextResult.offset + 1;
        } else {
            maxOffset = nextResult.offset - 1;
        }
    }

    return ensureResult(prevResult);
}

/**
 * Resolves a scalar value located at the specified offset from the origin of
 * the operand.
 */
function getScalarAt(context: FormulaContext, operand: Operand, horizontal: boolean, line: number, offset: number): ScalarType {

    // effective row/column index depending on direction
    const row = horizontal ? line : offset;
    const col = horizontal ? offset : line;

    // resolve scalar value according to operand type
    switch (operand.type) {

        // scalar: fail for invalid (non-zero) offsets
        case "val":
            if ((row !== 0) || (col !== 0)) { throw ErrorCode.NA; }
            return operand.getRawValue() as ScalarType;

        // matrix: fail for invalid (out of bounds) offsets
        case "mat": {
            const matrix = operand.getMatrix();
            if ((row >= matrix.rows()) || (col >= matrix.cols())) { throw ErrorCode.NA; }
            return matrix.raw()[row][col];
        }

        // reference: ignore size of operand, scalar may be located outside
        case "ref": {
            const range3d = operand.getRanges()[0];
            const sheetModel = context.getSheetModel(range3d.sheet1);
            const address = range3d.a1.clone();
            address.c += col;
            address.r += row;
            return context.getCellValue(sheetModel, address);
        }

        default:
            throwEngineError("getScalarAt(): unknown operand type");
    }
}

/**
 * Implementation helper for `HLOOKUP` and `VLOOKUP` functions.
 */
function resolveLookup(context: FormulaContext, lookupValue: ScalarTypeNE, lookupSource: Operand, lookupRow: boolean, resultIndex: number, sorted = true): ScalarType {

    // #VALUE! thrown for complex references
    const lookupDim = lookupSource.getDimension({ errorCode: ErrorCode.VALUE });
    const resultSize = lookupRow ? lookupDim.rows : lookupDim.cols;
    if ((resultIndex < 1) || (resultIndex > resultSize)) { throw ErrorCode.NA; }

    // search for the lookup value (default for match mode is TRUE for missing parameter)
    const matchMode = sorted ? MatchMode.LESS : MatchMode.EXACT;
    const result = lookupMatchingResult(context, lookupValue, lookupSource, lookupDim, lookupRow, matchMode);

    // return resulting value from respective position in lookup source
    return (resultIndex === 1) ? result.value : getScalarAt(context, lookupSource, lookupRow, resultIndex - 1, result.offset);
}

// exports ====================================================================

export default declareFunctionSpecModule("reference", factory => {

    const INT_MAT_PASS = factory.param("val:int", { mat: "pass" });
    const REF_DEP_SKIP = factory.param("ref", { dep: "skip" });
    const REF_SINGLE_DEP_SKIP = factory.param("ref:single", { dep: "skip" });
    const REF_MULTI_DEP_SKIP = factory.param("ref:multi", { dep: "skip" });
    const ANY_MAT_PASS = factory.param("any", { mat: "pass" });
    const ANY_DEP_SKIP = factory.param("any", { dep: "skip" });
    const ANY_DEP_SKIP_MAT_PASS = factory.param("any", { dep: "skip", mat: "pass" });
    const ANY_LAZY_DEP_PASS = factory.param("any:lazy", { dep: "pass" });
    const ANY_LAZY_PASS_FORWARD = factory.param("any:lazy", { dep: "pass", mat: "forward" });

    factory.fixed("ADDRESS", "val:str", 2, 5, ["val:int", "val:int", "val:int", "val:bool", "val:str"], {
        resolve(row: number, col: number, absMode?: number, a1Style?: boolean, sheetName?: string) {

            // the resulting address string
            let result = "";

            // default for missing or empty (!) absolute mode (always defaults to 1)
            absMode = this.isMissingOrEmptyOperand(2) ? 1 : absMode!;
            if ((absMode < 1) || (absMode > 4)) { throw ErrorCode.VALUE; }

            // default for missing or empty (!) A1 style (default value is TRUE)
            a1Style = this.isMissingOrEmptyOperand(3) || a1Style!;

            // initialize the cell reference structure
            const cellRef = new CellRef(0, 0, (absMode % 2) > 0, absMode <= 2);
            const refAddress = new Address(0, 0);

            // get the effective column index (convert column offset in R1C1 notation to real column index)
            if (a1Style || cellRef.absCol) {
                // absolute column index, or any column index in A1 notation (one-based)
                cellRef.col = col - 1;
            } else if (col < 0) {
                // negative column offset in R1C1 notation, e.g. R1C[-16383]
                // (adjust reference address to be able to create large negative offsets)
                const { maxCol } = this.addressFactory;
                cellRef.col = maxCol + col;
                refAddress.c = maxCol;
            } else {
                // positive column offset in R1C1 notation, e.g. R1C[16383]
                cellRef.col = col;
                refAddress.c = 0;
            }

            // get the effective row index (convert row offset in R1C1 notation to real row index)
            if (a1Style || cellRef.absRow) {
                // absolute row index, or any row index in A1 notation (one-based)
                cellRef.row = row - 1;
            } else if (row < 0) {
                // negative row offset in R1C1 notation, e.g. R[-1048575]C1
                // (adjust reference address to be able to create large negative offsets)
                const { maxRow } = this.addressFactory;
                cellRef.row = maxRow + row;
                refAddress.r = maxRow;
            } else {
                // positive row offset in R1C1 notation, e.g. R[1048575]C1
                cellRef.row = row;
                refAddress.r = 0;
            }

            // validate the effective cell reference structure
            this.checkCellRef(cellRef);

            // start with encoded sheet name (enclosed in apostrophes if needed)
            // (not for missing or empty parameter, but for explicit empty string)
            if (is.string(sheetName)) {

                // enclose complex names in apostrophes
                if ((sheetName.length > 0) && !this.formulaGrammar.isSimpleSheetName(this.docModel, sheetName, { range: true, external: true })) {
                    sheetName = encodeComplexSheetName(sheetName);
                }

                // use sheet name with an exclamation mark as separator
                result = `${sheetName}!`;
            }

            // append the range address in A1 or R1C1 style
            const grammar = this.docModel.getFormulaGrammarUI(!a1Style);
            return result + grammar.formatReference(this.docModel, refAddress, cellRef);
        }
    });

    factory.unary("AREAS", "val:num", REF_DEP_SKIP, ranges => ranges.length);

    factory.unbound("CHOOSE", "any", 2, 1, [INT_MAT_PASS, ANY_LAZY_PASS_FORWARD], function (index: number) {

        const opCount = this.getOperandCount();

        // Under some circumstances, combine the result as matrix element-by-element from
        // the following parameters, otherwise pass through the result unmodified.
        //
        // Example (simple cell formulas):
        // =ISREF(CHOOSE(1,A1:B2)) results in TRUE (the reference A1:B2 directly)
        // =ISREF(CHOOSE({1},A1:B2)) results in FALSE (a matrix built from A1:B2)
        // =CHOOSE({1|2},{3|4},{5|6}) results in {3|6} (result matrix built element-by-element)
        //
        // Example (matrix formula with reference selector):
        // {=CHOOSE(A1:B2,{2|3},{4|5})} combines the matrixes according to A1:B2
        //
        if (
            ((this.contextType !== "val") && (this.getOperand(0).type === "mat")) ||
            ((this.contextType === "mat") && (this.getOperand(0).type !== "val"))
        ) {

            // the dimension of the result matrix is the boundary of all parameters (#N/A thrown for complex references)
            const matrixDim = this.getOperands(0).reduce<Dimension | null>((dim, operand) => Dimension.boundary(dim, operand.getDimension({ errorCode: ErrorCode.NA })), null)!;

            // build the matrix by evaluating all indexes separately
            return this.aggregateMatrix(matrixDim, () => getChooseOperand(this, this.getOperand(0, "val:int"), opCount, true));
        }

        // otherwise, return one of the unconverted operands
        return getChooseOperand(this, index, opCount);
    });

    factory.unary0("COLUMN", "val:num", REF_SINGLE_DEP_SKIP, {
        resolve(range) {
            // create a matrix with all column indexes in matrix context with explicit range
            if (range && (this.contextType === "mat")) {
                ensureMatrixDim(1, range.cols());
                const offset = range.a1.c + 1;
                return new NumberMatrix(1, range.cols(), (_row, col) => offset + col);
            }
            // use target reference address, if parameter is missing
            const address = range ? this.getMatrixAddress(range) : this.getTargetAddress();
            // function returns one-based column index
            return address.c + 1;
        },
        // result depends on reference address (behaves like a relative reference), if parameter is missing
        relColRef: count => count === 0
    });

    factory.unary("COLUMNS", "val:num", ANY_DEP_SKIP, function (operand) {
        const value = operand.getRawValue();
        switch (operand.type) {
            case "val":
                return isErrorCode(value) ? value.throw() : 1;
            case "mat":
                return (value as ScalarMatrix).cols();
            case "ref":
                return this.convertToRange(value as Range3DArray).cols();
            default:
                throwEngineError("ReferenceFuncs.COLUMNS.resolve(): unknown operand type");
        }
    });

    factory.unary("FORMULATEXT", "val:str", REF_SINGLE_DEP_SKIP, {
        name: { ooxml: "_xlfn.FORMULATEXT", odf: "FORMULA" },
        recalc: "always", // changing a formula may not change the result, thus no recalculation would be triggered
        resolve(range) {

            // always use the first cell in the range (TODO: matrix context)
            const address = range.a1;

            // TODO: resolve self-reference while entering a cell or shared formula

            // getCellFormula() returns null for blank and simple value cells
            const sheetModel = this.getSheetModel(range.sheet1);
            const result = this.getCellFormula(sheetModel, address);
            if (!result) { throw ErrorCode.NA; }

            // special handling for matrix formulas
            const formula = `=${result.formula}`;
            const staticMatrix = result.matrixRange && !result.dynamicMatrix;
            return staticMatrix ? `{${formula}}` : formula;
        }
    });

    factory.unbound("GETPIVOTDATA", "val:any", 2, 2, ["val:str", "ref", "val:str", "val:str"], {
        hidden: true
    });

    factory.fixed("HLOOKUP", "any", 3, 4, ["val", ANY_MAT_PASS, "val:int", "val:bool"], {
        resolve(lookupValue: ScalarTypeNE, lookupSource: Operand, row: number, sorted?: boolean) {
            return resolveLookup(this, lookupValue, lookupSource, true, row, sorted);
        }
    });

    factory.fixed("INDEX", "any", 2, 4, [ANY_DEP_SKIP, INT_MAT_PASS, INT_MAT_PASS, INT_MAT_PASS], {
        recalc: "always",
        resolve(source: Operand, row: number, col?: number, area?: number) {

            // one-based "area" parameter can be set to 1 or omitted
            const iarea = is.number(area) ? (area - 1) : 0;
            if (iarea < 0) { throw ErrorCode.REF; }

            // matrix or range address value
            let matrix: Opt<ScalarMatrix>;
            let range: Opt<Range3D>;

            // resolve value and dimension of the operand
            const dim: Dimension = fun.do(() => {
                switch (source.type) {
                    case "val":
                        if (iarea > 0) { throw ErrorCode.REF; }
                        return new Dimension(1, 1);
                    case "mat":
                        if (iarea > 0) { throw ErrorCode.REF; }
                        matrix = source.getRawValue() as ScalarMatrix;
                        return matrix.dim();
                    case "ref":
                        // resolve single reference from reference list
                        range = (source.getRawValue() as Range3DArray)[iarea];
                        if (!range) { throw ErrorCode.REF; }
                        if (!range.singleSheet()) { throw ErrorCode.VALUE; }
                        return Dimension.createFromRange(range);
                    default:
                        throwEngineError("function INDEX: invalid operand type");
                }
            });

            // DOCS-3080: single parameter is used as vector index in either direction
            if (!is.number(col)) {
                if (dim.rows === 1) { col = row; row = 1; } else { col = 0; }
            }

            // early exit for invalid column/row index parameters (parameters are one-based,
            // but value 0 is allowed to convert 2-dimensional source to vector)
            if ((row < 0) || (row > dim.rows) || (col < 0) || (col > dim.cols)) { throw ErrorCode.REF; }

            // process matrix value (zero indexes result in submatrixes which may become 1x1)
            if (matrix) {
                if ((row === 0) && (col === 0)) { return matrix; }
                if (row === 0) { return matrix.toColVector(col - 1); }
                if (col === 0) { return matrix.toRowVector(row - 1); }
                return matrix.raw()[row - 1][col - 1];
            }

            // process cell range address
            if (range) {
                const sheetModel = this.getSheetModel(range.sheet1);
                if (row > 0) { range = range.rowRange(row - 1); }
                if (col > 0) { range = range.colRange(col - 1); }
                // bug 55707: convert single cells to scalars in matrix mode
                if (this.isMatrixMode() && range.single()) {
                    return this.getCellValue(sheetModel, range.a1);
                }
                return range;
            }

            // otherwise, return scalar value
            return source;
        }
    });

    factory.binary1("INDIRECT", "ref", ["val:str", "val:bool"], {
        recalc: "always",
        resolve(refText, a1Style) {

            // default for missing or empty (!) A1 style (default value is TRUE)
            a1Style = this.isMissingOrEmptyOperand(1) || a1Style!;

            const sheetModel = this.getRefSheetModel();
            const tokenArray = new TokenArray(sheetModel, "link");
            const refAddress = this.getTargetAddress();

            tokenArray.parseFormula(a1Style ? "ui:a1" : "ui:rc", refText, refAddress);

            const ranges = tokenArray.resolveRangeList(refAddress, refAddress, { resolveNames: "simple" });
            return (ranges.length === 1) ? ranges.first()! : ErrorCode.REF.throw();
        }
    });

    factory.ternary2("LOOKUP", "any", ["val", ANY_MAT_PASS, ANY_DEP_SKIP_MAT_PASS], {
        recalc: "always", // result source may be smaller than lookup source
        resolve(lookupValue, lookupSource, resultSource?) {

            // scalar error codes not accepted (but other scalar values are, in contrast to `MATCH`)
            if (lookupSource.type === "val") {
                throwErrorCode(lookupSource.getRawValue());
            }

            // dimension of the lookup range (throws #VALUE! for complex references)
            const lookupDim = lookupSource.getDimension({ errorCode: ErrorCode.VALUE });
            // whether to search in the first row (true), or first column (false)
            const lookupRow = lookupDim.rows < lookupDim.cols;

            // check that result range is a vector (throws #VALUE! for complex references)
            const resultDim = resultSource?.getDimension({ errorCode: ErrorCode.VALUE });
            if (resultDim && (resultDim.rows > 1) && (resultDim.cols > 1)) { throw ErrorCode.NA; }

            // search for the lookup value (after resolving dimension of result source to get correct errors)
            const result = lookupMatchingResult(this, lookupValue, lookupSource, lookupDim, lookupRow, MatchMode.LESS);

            // return value from last column/row of lookup source, if third parameter is missing
            if (!resultSource || !resultDim) {
                // return existing result value from lookup vector if source is one-dimensional
                const last = (lookupRow ? lookupDim.rows : lookupDim.cols) - 1;
                return (last === 0) ? result.value : getScalarAt(this, lookupSource, lookupRow, last, result.offset);
            }

            // whether to resolve the result value horizontally or vertically
            // - prefer direction of result vector if not a single value
            // - use lookup direction otherwise (e.g. single cell address)
            const resultRow = (resultDim.cols > 1) || ((resultDim.rows === 1) && lookupRow);

            // return value from respective position in result source
            return getScalarAt(this, resultSource, resultRow, 0, result.offset);
        }
    });

    factory.ternary2("MATCH", "val:num", ["val", ANY_MAT_PASS, "val:num"], function (lookupValue, lookupSource, matchType) {

        // scalar values not accepted (return existing error code, otherwise #N/A)
        if (lookupSource.type === "val") {
            throwErrorCode(lookupSource.getRawValue());
            throw ErrorCode.NA;
        }

        // check that search range is a vector (#VALUE! thrown for complex references)
        const lookupDim = lookupSource.getDimension({ errorCode: ErrorCode.VALUE });
        if ((lookupDim.rows > 1) && (lookupDim.cols > 1)) { throw ErrorCode.NA; }

        // default match mode is LESS (ascending order of lookup range); empty parameter is zero (EXACT)
        const matchMode = (!is.number(matchType) || (matchType > 0)) ? MatchMode.LESS : (matchType < 0) ? MatchMode.GREATER : MatchMode.EXACT;

        // the result object, with (zero-based) "offset" property
        const result = lookupMatchingResult(this, lookupValue, lookupSource, lookupDim, lookupDim.cols > 1, matchMode);
        return result.offset + 1;
    });

    factory.fixed("OFFSET", "ref", 3, 5, [REF_DEP_SKIP, "val:int", "val:int", "val:int", "val:int"], {
        recalc: "always",
        resolve(ranges: Range3DArray, rows: number, cols: number, height?: number, width?: number) {

            // reference must contain a single range address
            const range = this.convertToRange(ranges, { errorCode: ErrorCode.VALUE });

            // apply offset
            range.a1.c += cols;
            range.a1.r += rows;
            range.a2.c += cols;
            range.a2.r += rows;

            // modify size (but not if "height" or "width" parameters exist but are empty)
            if (!this.isMissingOrEmptyOperand(4)) { range.a2.c = range.a1.c + width! - 1; }
            if (!this.isMissingOrEmptyOperand(3)) { range.a2.r = range.a1.r + height! - 1; }

            // check that the range does not leave the sheet boundaries
            return this.addressFactory.isValidRange(range) ? range : ErrorCode.REF.throw();
        }
    });

    factory.unary0("ROW", "val:num", REF_SINGLE_DEP_SKIP, {
        resolve(range) {
            // create a matrix with all row indexes in matrix context with explicit range
            if (range && (this.contextType === "mat")) {
                ensureMatrixDim(range.rows(), 1);
                const offset = range.a1.r + 1;
                return new NumberMatrix(range.rows(), 1, row => offset + row);
            }
            // use target reference address, if parameter is missing
            const address = range ? this.getMatrixAddress(range) : this.getTargetAddress();
            // function returns one-based row index
            return address.r + 1;
        },
        // result depends on reference address (behaves like a relative reference), if parameter is missing
        relRowRef: count => count === 0
    });

    factory.unary("ROWS", "val:num", ANY_DEP_SKIP, function (operand) {
        const value = operand.getRawValue();
        switch (operand.type) {
            case "val":
                return isErrorCode(value) ? value.throw() : 1;
            case "mat":
                return (value as ScalarMatrix).rows();
            case "ref":
                return this.convertToRange(value as Range3DArray).rows();
            default:
                throwEngineError("function ROWS: unknown operand type");
        }
    });

    // parameter is a string (sheet name) or a reference
    factory.unary0("SHEET", "val:num", ANY_DEP_SKIP, {
        name: { ooxml: "_xlfn.SHEET" },
        recalc: "always", // function result depends on index of a sheet (refresh on moving sheet)
        resolve(operand) {

            // use reference sheet, if parameter is missing
            if (!operand) { return this.getRefSheet() + 1; }

            const value = operand.getRawValue();

            // use sheet index of a reference
            if (operand.type === "ref") {
                return this.convertToRange(value as Range3DArray).sheet1 + 1;
            }

            // convert values to strings (sheet name may look like a number or boolean)
            if (operand.type === "val") {
                const sheet = this.docModel.getSheetIndex(this.convertToString(value as ScalarType));
                return (sheet < 0) ? ErrorCode.NA.throw() : (sheet + 1);
            }

            // always fail for matrix operands, regardless of context
            throw ErrorCode.NA;
        }
    });

    factory.unary0("SHEETS", "val:num", REF_MULTI_DEP_SKIP, {
        name: { ooxml: "_xlfn.SHEETS" },
        recalc: "always", // function result depends on indexes of sheets (refresh on moving sheet)
        resolve(range) {
            // return total number of sheets in document, if parameter is missing
            return range ? range.sheets() : this.docModel.getSheetCount();
        }
    });

    // no binary repetition pattern, SWITCH supports a "default value" (single parameter after a pair of parameters)!
    factory.unbound("SWITCH", "any", 3, 1, ["val", ANY_LAZY_DEP_PASS, ANY_LAZY_DEP_PASS], {
        name: { ooxml: "_xlfn.SWITCH", odf: null },
        resolve(selector: ScalarTypeNE) {

            // total number of operands
            const length = this.getOperandCount();
            // number of parameter pairs following the leading selector parameter
            const pairs = floor((length - 1) / 2);

            // try to find a parameter pair matching the selector
            for (let index = 0; index < pairs; index += 1) {
                // immediately throw on error code
                const value = this.getOperand(index * 2 + 1, "val");
                // do not convert data types while comparing (number 1 as selector DOES NOT select string "1")
                if (this.equalScalars(selector, value)) {
                    return this.getOperand(index * 2 + 2);
                }
            }

            // return default value if provided, otherwise #N/A error code (thrown automatically)
            return this.getOperand(pairs * 2 + 1);
        }
    });

    factory.fixed("VLOOKUP", "any", 3, 4, ["val", ANY_MAT_PASS, "val:int", "val:bool"], {
        resolve(lookupValue: ScalarTypeNE, lookupSource: Operand, col: number, sorted?: boolean) {
            return resolveLookup(this, lookupValue, lookupSource, false, col, sorted);
        }
    });
});
