/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str } from "@/io.ox/office/tk/algorithms";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { mapKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { TableRef, TableTargetRef, IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import type { DocRef } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { TableRegionKey } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type { TokenTextOptions } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// types ======================================================================

/**
 * Configuration options for instances of the class `TableToken`.
 */
export interface TableTokenConfig {

    /**
     * Reference to another spreadsheet document.
     */
    docRef?: DocRef;

    /**
     * The resource key of a table region. If omitted, the default region
     * "DATA" will be referred by the token. Additionally, the combined regions
     * "HEADER,DATA" and "DATA,TOTALS" will be accepted.
     */
    regionSpec?: string;

    /**
     * The name of a single table column referred by this token, or the name of
     * the first column in a column range. If omitted, the token will refer to
     * all columns of the table.
     */
    col1Name?: string;

    /**
     * The name of the second column in a column range. If omitted, the token
     * will refer to a single column, or all columns of the table.
     */
    col2Name?: string;

    /**
     * Whether to add a space characters after the opening bracket when
     * generating the text representation of the token.
     */
    openWs?: boolean;

    /**
     * Whether to add a space characters before the closing bracket when
     * generating the text representation of the token.
     */
    closeWs?: boolean;

    /**
     * Whether to add a space characters after each separator character inside
     * a complex table reference when generating the text representation of the
     * token.
     */
    sepWs?: boolean;
}

// class TableToken ===========================================================

/**
 * A formula token that represents a structured table reference in a formula.
 *
 * @param docAccess
 *  The document access interface.
 *
 * @param tableName
 *  The name of the table referred by this token.
 *
 * @param [config]
 *  Configuration options.
 */
export class TableToken extends BaseToken {

    readonly docAccess: IDocumentAccess;
    readonly tableName: string;

    readonly #docRef: Opt<DocRef>;
    readonly #regionSpec: string;
    readonly #regionKeys: Opt<TableRegionKey[]>;
    readonly #col1Name: string;
    readonly #col2Name: string;
    readonly #openWs: boolean;
    readonly #closeWs: boolean;
    readonly #sepWs: boolean;

    // constructor ------------------------------------------------------------

    constructor(
        docAccess: IDocumentAccess,
        tableName: string,
        config?: TableTokenConfig
    ) {
        super("table");

        this.docAccess = docAccess;
        this.tableName = tableName;

        this.#docRef = config?.docRef;
        this.#regionSpec = config?.regionSpec ?? "";
        this.#regionKeys = this.#regionSpec ? this.#regionSpec.split(",") as TableRegionKey[] : undefined;
        this.#col1Name = config?.col1Name ?? "";
        this.#col2Name = this.#col1Name && (config?.col2Name ?? "");
        this.#openWs = !!config?.openWs;
        this.#closeWs = !!config?.closeWs;
        this.#sepWs = !!config?.sepWs;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this token refers to another spreadsheet document.
     *
     * @returns
     *  Returns whether this token refers to another external spreadsheet
     *  document (`true`), or into its own document (`false`).
     */
    isExtDoc(): boolean {
        return !!this.#docRef?.isExtDoc();
    }

    /**
     * Returns whether this token represents a `#ThisRow` table region.
     */
    isRowRegion(): boolean {
        return this.#regionSpec === "ROW";
    }

    /**
     * Resolves the table range referred by this token.
     *
     * @returns
     *  The reference to the table range, if existing; otherwise `undefined`.
     */
    resolveTable(): Opt<TableRef> {
        return this.isExtDoc() ? undefined : this.docAccess.resolveTable(this.tableName);
    }

    /**
     * Returns the effective position of the cell range referred by this token.
     *
     * @param targetAddress
     *  The target address needed to resolve the special table region "ROW".
     *
     * @returns
     *  The effective position of the cell range referred by this token (with
     *  sheet indexes), if available; otherwise `null`.
     */
    getRange3D(targetAddress: Address): Range3D | null {
        const targetRef = this.#resolveTargetRef(targetAddress);
        return targetRef ? Range3D.fromRange3D(targetRef.range, targetRef.sheet) : null;
    }

    /**
     * Generates the text representation of this token.
     */
    override getText(grammar: FormulaGrammar, options?: TokenTextOptions): string {
        return this.#formatText(grammar, options);
    }

    /**
     * Resolves the new text representation of this token, before a sheet will
     * be deleted in the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the deleted sheet.
     *
     * @returns
     *  The text representation of this sheet reference token, if it has been
     *  changed; otherwise `null`.
     */
    override resolveDeletedSheet(grammar: FormulaGrammar, sheet: number): string | null {
        if (this.isExtDoc()) { return null; }
        // return simple #REF! error, if the table is located in the deleted sheet
        const tableRef = this.resolveTable();
        return (!tableRef || (tableRef.sheet === sheet)) ? grammar.REF_ERROR : null;
    }

    /**
     * Resolves the new text representation of this token, before a sheet will
     * be renamed in the spreadsheet document, or after an existing sheet has
     * been copied.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _sheet
     *  The index of the renamed sheet.
     *
     * @param _sheetName
     *  The new name of the sheet.
     *
     * @param [tableNames]
     *  A map that specifies how to change the table names in table references.
     *  Used when copying an existing sheet where the cloned table ranges have
     *  been renamed, and the formulas in the copied sheet should refer to the
     *  new table ranges. The old table names are the map keys, and the new
     *  table names are the map values. If omitted, no table references will be
     *  changed (e.g. when renaming an existing sheet).
     *
     * @returns
     *  The text representation of this table reference token, if it has been
     *  changed; otherwise `null`.
     */
    override resolveRenamedSheet(grammar: FormulaGrammar, _sheet: number, _sheetName: string, tableNames?: Dict<string>): string | null {

        // nothing to do without a table name map (rename sheet)
        if (!tableNames || this.isExtDoc()) { return null; }

        // return simple #REF! error, if the table token is invalid
        const tableRef = this.resolveTable();
        if (!tableRef) { return grammar.REF_ERROR; }

        // get the new table name (do not use the own property `tableName` for
        // lookup as map key, it may have different character case)
        const tableName = tableNames[tableRef.name];

        // if the table name is contained in the map, generate the new text representation
        return tableName ? this.#formatText(grammar, { tableName }) : null;
    }

    /**
     * Resolves the new text representation of this token, before a table range
     * will be renamed in the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param oldName
     *  The old name of the table range.
     *
     * @param newName
     *  The new name of the table range.
     *
     * @returns
     *  The text representation of this table token, if it would be changed;
     *  otherwise `null`.
     */
    override resolveRenamedTable(grammar: FormulaGrammar, oldName: string, newName: string): string | null {
        if (this.isExtDoc()) { return null; }

        // nothing to do, if the old name of the renamed table range does not match
        if (!str.equalsICC(oldName, this.tableName)) { return null; }

        // return simple #REF! error, if the table token is invalid
        const tableRef = this.docAccess.resolveTable(oldName);
        if (!tableRef) { return grammar.REF_ERROR; }

        // generate the new text representation
        return this.#formatText(grammar, { tableName: newName });
    }

    /**
     * Resolves the new text representation of this token, before cell ranges
     * will be moved in a specific sheet of the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the sheet containing the moved cell ranges.
     *
     * @param transformer
     *  An address transformer created while generating the move document
     *  operations.
     *
     * @returns
     *  The string representation of the moved cell reference, if it has been
     *  changed; otherwise `null`.
     */
    override resolveMovedCells(grammar: FormulaGrammar, sheet: number, transformer: AddressTransformer): string | null {
        if (this.isExtDoc()) { return null; }

        // return simple #REF! error, if the table token is invalid
        const targetRef = this.#resolveTargetRef();
        if (!targetRef) { return grammar.REF_ERROR; }

        // table ranges located on another sheet will not be affected
        if (sheet !== targetRef.sheet) { return null; }

        // transform the resulting range; return #REF!, if the range will be deleted
        const origRange = targetRef.range;
        if (transformer.transformRanges(origRange).empty()) {
            return grammar.REF_ERROR;
        }

        // if the table token refers to columns, invalidate it if the start or the end
        // column will be deleted (even if some inner columns remain available)
        if (this.#col1Name) {
            const col1Ranges = transformer.transformRanges(origRange.leadingCol());
            const col2Ranges = transformer.transformRanges(origRange.trailingCol());
            if (col1Ranges.empty() || col2Ranges.empty()) { return grammar.REF_ERROR; }
        }

        return null;
    }

    // protected methods ------------------------------------------------------

    /**
     * Returns a text description of this token for debug logging.
     */
    protected debugText(): string {
        let result = this.#docRef ? ("[" + String(this.#docRef.index) + "]!") : "";
        result += this.tableName;
        if (this.#regionKeys) { result += this.#regionKeys.map(regionKey => "#" + regionKey).join(""); }
        if (this.#col1Name) { result += "[" + this.#col1Name + "]"; }
        if (this.#col2Name) { result += ":[" + this.#col2Name + "]"; }
        return result;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the position of the table region referred by this token.
     *
     * @param tableRef
     *  The resolved reference to the table range.
     *
     * @param [targetAddress]
     *  The target address needed to resolve the special table region "ROW". If
     *  omitted, the "ROW" region will be resolved to the entire table range
     *  (region type "ALL").
     *
     * @returns
     *  The position of the table region referred by this token if available.
     */
    #resolveRegionRange(tableRef: TableRef, targetAddress?: Address): Opt<Range> {

        // the region specifier
        const regionSpec = this.#regionSpec || "DATA";

        // directly resolve specific region ranges
        switch (regionSpec) {
            case "ALL":     return tableRef.range.clone();
            case "HEADERS": return tableRef.header ? tableRef.range.headerRow() : undefined;
            case "TOTALS":  return tableRef.footer ? tableRef.range.footerRow() : undefined;
            // resolve "ROW" range to entire table range without target address
            case "ROW":     if (!targetAddress) { return tableRef.range.clone(); } break;
        }

        // the resulting region range (shortened by header and/or footer row)
        const regionRange = tableRef.range.clone();

        // shorten the region range by header row
        if (tableRef.header) {
            switch (regionSpec) {
                case "DATA":
                case "ROW":
                case "DATA,TOTALS":
                    regionRange.a1.r += 1;
                    break;
            }
        }

        // shorten the region range by footer row
        if (tableRef.footer) {
            switch (regionSpec) {
                case "DATA":
                case "ROW":
                case "HEADERS,DATA":
                    regionRange.a2.r -= 1;
                    break;
            }
        }

        // check validity of resulting range
        if (regionRange.a1.r > regionRange.a2.r) { return undefined; }

        // restrict to row range according to target address
        if ((regionSpec === "ROW") && targetAddress) {
            if (!regionRange.containsRow(targetAddress.r)) { return undefined; }
            regionRange.setBoth(targetAddress.r, false);
        }

        return regionRange;
    }

    /**
     * Resolves the cell area in the table range referred by this token,
     * including its region specifier and column references.
     *
     * @param [targetAddress]
     *  The target address needed to resolve the special table region "ROW". If
     *  omitted, the "ROW" region will be resolved to the entire table range
     *  (region type "ALL").
     *
     * @returns
     *  The reference to the target area, if existing; otherwise `undefined`.
     */
    #resolveTargetRef(targetAddress?: Address): Opt<TableTargetRef> {

        // resolve the table range referred by this token
        const tableRef = this.resolveTable();
        if (!tableRef) { return undefined; }

        // resolve the region rows referred by this token
        let targetRange = this.#resolveRegionRange(tableRef, targetAddress);
        if (!targetRange) { return undefined; }

        // resolve column names (nothing to return, if no valid column range is available)
        if (this.#col1Name) {

            // resolve position of single column, or start column in a column range
            const tableCol1 = tableRef.columns.indexOf(mapKey(this.#col1Name));
            if (tableCol1 < 0) { return undefined; }

            // resolve position of end column in a column range
            if (this.#col2Name) {
                const tableCol2 = tableRef.columns.indexOf(mapKey(this.#col2Name));
                if (tableCol2 < 0) { return undefined; }
                const col1 = Math.min(tableCol1, tableCol2);
                const cols = Math.abs(tableCol1 - tableCol2) + 1;
                targetRange = targetRange.colRange(col1, cols);
            } else {
                targetRange = targetRange.colRange(tableCol1);
            }
        }

        return { sheet: tableRef.sheet, range: targetRange };
    }

    /**
     * Generates the text representation of this token.
     */
    #formatText(grammar: FormulaGrammar, options?: TokenTextOptions & { tableName?: string }): string {

        // the regexp helper for table references
        const patterns = grammar.nameRefPatterns;

        const generateWs = (flag: boolean): string => flag ? " " : "";
        const brackets = (text: string): string => "[" + text + "]";
        const encodeColumn = (colName: string): string => brackets(patterns.encodeTableColumn(colName));

        // the external document reference
        const docRef = this.#docRef ? (brackets(String(this.#docRef.index)) + "!") : "";
        // the table name to be inserted (own table name can be overridden by options)
        const tableName = options?.tableName ?? this.tableName;
        // whether the token contains leading or trailing whitespace characters
        const outerWs = this.#openWs || this.#closeWs;

        // add empty brackets after table name in OOXML operation mode
        if (!this.#regionKeys && !this.#col1Name) {
            const emptyBrackets = !grammar.OF && !grammar.UI;
            return docRef + tableName + (emptyBrackets ? brackets(generateWs(outerWs)) : "");
        }

        // the key of a single table region (null for combined regions)
        const regionKey = (this.#regionKeys?.length === 1) ? this.#regionKeys[0] : null;
        // the string components to be added into a complex table reference
        const components: string[] = [];

        // add the at-sign shortcut for '#This Row' in UI grammar (e.g. =Table[@Col1] instead of =Table[[#This Row],[Col1]])
        if (grammar.UI && this.isRowRegion()) {
            const colRefText = !this.#col1Name ? "" :
                this.#col2Name ? (encodeColumn(this.#col1Name) + ":" + encodeColumn(this.#col2Name)) :
                patterns.isSimpleTableColumn(this.#col1Name) ? this.#col1Name : encodeColumn(this.#col1Name);
            components.push("@" + colRefText);

        // add a single region specifier with single brackets (e.g. =Table[#All])
        } else if (regionKey && !outerWs && !this.#col1Name) {
            components.push(patterns.getTableRegionName(regionKey)!);

        // add a simple column name with single brackets (not for columns with leading/trailing whitespace)
        } else if (!this.#regionKeys && this.#col1Name && !this.#col2Name && !outerWs && !/^\s|\s$/.test(this.#col1Name)) {
            components.push(patterns.encodeTableColumn(this.#col1Name));

        // create a complex table reference with regions and column names
        } else {

            // add region names if existing
            this.#regionKeys?.forEach(key => components.push(brackets(patterns.getTableRegionName(key)!)));

            // add the table column names
            if (this.#col1Name) {
                const col1Name = encodeColumn(this.#col1Name);
                components.push(this.#col2Name ? (col1Name + ":" + encodeColumn(this.#col2Name)) : col1Name);
            }
        }

        // Omit the table name in UI grammar (unqualified table reference), if the formula is
        // located inside the table; but only for table references consisting of a single column
        // (e.g. `[Col1]`), or a "this-row" reference to a single column, e.g. `[@Col1]`.
        let addTableName = true;
        if (!this.#docRef && options?.unqualifiedTables && grammar.UI) {
            // check for single-column references; the only allowed region is `#ThisRow` (@-reference)
            if (this.#col1Name && !this.#col2Name && (!this.#regionKeys || this.isRowRegion())) {
                // check that the generated formula is located inside the table range
                const tableRef = this.resolveTable();
                if (tableRef && (tableRef.sheet === options.refSheet)) {
                    const address = options.targetAddress;
                    addTableName = !address || !tableRef.range.containsAddress(address);
                }
            }
        }

        // join all table reference components to be enclosed in brackets after table name
        const compList = components.join(grammar.SEP + generateWs(this.#sepWs));

        // generate the string representation with all components and whitespace
        return docRef + (addTableName ? tableName : "") + brackets(generateWs(this.#openWs) + compList + generateWs(this.#closeWs));
    }
}
