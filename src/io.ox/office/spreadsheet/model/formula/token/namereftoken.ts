/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import type { SheetRef, SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { TokenType } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { type ResolveSheetRefsOptions, SheetRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/sheetreftoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class NameRefToken =========================================================

/**
 * A formula token representing an entity inside a sheet with a custom label,
 * e.g. a defined name, or a call to a macro function.
 *
 * @param type
 *  The type of the token.
 *
 * @param docAccess
 *  The document access interface.
 *
 * @param value
 *  The label of the defined name.
 *
 * @param [sheetRefs]
 *  The sheet reference structure (e.g. the particle "Sheet1" in the formula
 *  `Sheet1!my_name`), or `undefined` for a local entity in the own sheet, or a
 *  document-global entity.
 */
export abstract class NameRefToken extends SheetRefToken {

    readonly value: string;

    // constructor ------------------------------------------------------------

    constructor(type: TokenType, docAccess: IDocumentAccess, value: string, sheetRefs?: SheetRefs) {
        super(type, docAccess, sheetRefs);
        this.value = value;
    }

    // protected getters ------------------------------------------------------

    protected get sheetRef(): Opt<SheetRef> {
        return this.sheetRefs.r1;
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses generate the text representation of this token, but with the
     * specified sheet reference and custom label.
     */
    protected abstract formatText(grammar: FormulaGrammar, label: string, sheetRefs?: SheetRefs): string;

    /**
     * Generates the text representation of this token.
     */
    protected override getTextWithRefs(grammar: FormulaGrammar, options?: ResolveSheetRefsOptions): string {
        return this.formatText(grammar, this.value, this.resolveSheetRefs(options));
    }

    /**
     * Returns a text description of this token for debug logging.
     */
    protected override debugText(): string {
        return this.debugSheetPrefix() + this.value;
    }
}
