/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type TokenType, BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";

// class FixedToken ===========================================================

/**
 * A formula token of an arbitrary type with a fixed text representation
 * independent from a formula grammar.
 *
 * @param type
 *  The token type identifier.
 *
 * @param value
 *  The fixed text representation of the token.
 */
export class FixedToken extends BaseToken {

    value: string;

    // constructor ------------------------------------------------------------

    constructor(type: TokenType, value: string) {
        super(type);
        this.value = value;
    }

    // public methods ---------------------------------------------------------

    /**
     * Replaces the text of this token.
     *
     * @param value
     *  The new text to be inserted into this token.
     *
     * @returns
     *  Whether the text has actually changed.
     */
    setValue(value: string): boolean {
        if (this.value === value) { return false; }
        this.value = value;
        return true;
    }

    /**
     * Appends more text to this token.
     *
     * @param value
     *  The new text to be appended to this token.
     *
     * @returns
     *  Whether the token has actually changed (the passed text was not empty).
     */
    appendValue(value: string): boolean {
        if (value.length === 0) { return false; }
        this.value += value;
        return true;
    }

    /**
     * Generates the text representation of this token.
     */
    override getText(): string {
        return this.value;
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates a text description of this token for debugging (!) purposes.
     */
    protected override debugText(): string {
        return this.value;
    }
}
