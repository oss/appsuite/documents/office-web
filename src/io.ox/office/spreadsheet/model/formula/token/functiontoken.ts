/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class FunctionToken ========================================================

/**
 * A formula token that represents a built-in function in a formula.
 *
 * @param value
 *  The unique resource key of the built-in sheet function.
 */
export class FunctionToken extends BaseToken {

    readonly value: string;

    // constructor ------------------------------------------------------------

    constructor(value: string) {
        super("func");
        this.value = value;
    }

    // public methods ---------------------------------------------------------

    /**
     * Generates the text representation of this token.
     */
    override getText(grammar: FormulaGrammar): string {
        return grammar.getFunctionName(this.value) || this.value.toUpperCase();
    }

    // protected methods ------------------------------------------------------

    /**
     * Returns a text description of this token for debug logging.
     */
    protected override debugText(): string {
        return this.value;
    }
}
