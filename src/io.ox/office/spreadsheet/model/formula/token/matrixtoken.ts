/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ScalarMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class MatrixToken ==========================================================

/**
 * A formula token that represents a constant matrix literal in a formula.
 *
 * @param value
 *  The matrix literal represented by the token.
 */
export class MatrixToken extends BaseToken {

    readonly value: ScalarMatrix;

    // constructor ------------------------------------------------------------

    constructor(value: ScalarMatrix) {
        super("mat");
        this.value = value;
    }

    // public methods ---------------------------------------------------------

    /**
     * Generates the text representation of this token.
     */
    override getText(grammar: FormulaGrammar): string {

        // matrix delimiter characters
        const { MAT_ROW, MAT_COL } = grammar;

        let text = "{";
        for (const { row, col, value } of this.value.entries()) {
            if (col > 0) {
                text += MAT_COL;
            } else if (row > 0) {
                text += MAT_ROW;
            }
            text += grammar.formatScalar(value);
        }
        return text + "}";
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates the text representation for debug logging.
     */
    protected override debugText(): string {
        return this.value.toString();
    }
}
