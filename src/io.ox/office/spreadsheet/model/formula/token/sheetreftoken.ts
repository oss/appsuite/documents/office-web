/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import type { OptSheet, TransformSheetVector } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { SheetRef, SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { TokenType, TokenTextOptions } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// types ======================================================================

/**
 * Optional parameters for resolving sheet references from a `SheetRefToken`
 * token.
 */
export interface ResolveSheetRefsOptions {

    /**
     * A custom sheet reference for the first sheet to be returned instead of
     * the token's own first sheet reference.
     */
    sheet1Ref?: SheetRef;

    /**
     * A custom sheet reference for the second sheet to be returned instead of
     * the token's own second sheet reference.
     */
    sheet2Ref?: SheetRef;
}

// class SheetRefToken ========================================================

/**
 * Base class for formula tokens containing one or two sheet references.
 *
 * @param type
 *  The type of the token.
 *
 * @param docAccess
 *  The document access interface.
 *
 * @param [sheetRefs]
 *  The sheet references.
 */
export abstract class SheetRefToken extends BaseToken {

    protected readonly docAccess: IDocumentAccess;
    protected readonly sheetRefs: SheetRefs;

    // constructor ------------------------------------------------------------

    protected constructor(type: TokenType, docAccess: IDocumentAccess, sheetRefs?: SheetRefs) {
        super(type);

        this.docAccess = docAccess;
        this.sheetRefs = sheetRefs ?? new SheetRefs();

        // swap sheet references, if not in order
        const { r1, r2 } = this.sheetRefs;
        if (r1?.valid() && r2?.valid() && (r2.sheet < r1.sheet)) {
            this.sheetRefs.r2 = r1;
            this.sheetRefs.r1 = r2;
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this token refers to another spreadsheet document.
     *
     * @returns
     *  Returns whether this token refers to another external spreadsheet
     *  document (`true`), or into its own document (`false`).
     */
    isExtDoc(): boolean {
        return this.sheetRefs.isExtDoc();
    }

    /**
     * Returns whether this token contains a sheet reference, regardless if it
     * is valid.
     *
     * @returns
     *  Whether this token contains a sheet reference.
     */
    hasSheetRef(): boolean {
        return !!this.sheetRefs.r1;
    }

    /**
     * Returns whether a sheet reference of this token is invalid (it exists,
     * AND it points to a non-existing sheet).
     *
     * @returns
     *  Whether a sheet reference of this token is invalid.
     */
    hasSheetError(): boolean {
        return this.sheetRefs.hasError();
    }

    /**
     * Tries to replace unresolved sheet names with existing sheet indexes in
     * the spreadsheet document. Intended to be used after document import to
     * refresh all token arrays that refer to sheets that did not exist during
     * their creation.
     */
    override refreshSheets(): void {
        this.sheetRefs.refresh(this.docAccess);
    }

    /**
     * Refreshes the sheet references after a sheet has been inserted, deleted,
     * or moved in the document.
     *
     * @param xfVector
     *  The transformation vector for the sheet indexes.
     *
     * @returns
     *  Whether the token has been changed.
     */
    override transformSheets(xfVector: TransformSheetVector): boolean {
        return this.sheetRefs.transform(xfVector);
    }

    /**
     * Generates the text representation of this token.
     */
    override getText(grammar: FormulaGrammar, options?: TokenTextOptions): string {
        return this.getTextWithRefs(grammar, options);
    }

    /**
     * Resolves the new text representation of this token, before a sheet will
     * be deleted in the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the deleted sheet.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve relative references.
     *
     * @returns
     *  The text representation of this sheet reference token, if it has been
     *  changed; otherwise `null`.
     */
    override resolveDeletedSheet(grammar: FormulaGrammar, sheet: number, refSheet: OptSheet): string | null {

        // nothing to do for external references
        if (this.isExtDoc()) { return null; }

        // resolve the own sheet range (nothing to do on reference errors)
        const sheetInterval = this.sheetRefs.r1 ? this.resolveSheetInterval(refSheet) : null;
        if (!sheetInterval) { return null; }

        // if this token refers to the deleted sheet, it becomes a #REF! error
        if (sheetInterval.single()) {
            return (sheet === sheetInterval.first) ? this.getTextWithRefs(grammar, { sheet1Ref: new SheetRef(-1, true) }) : null;
        }

        // if this token refers to multiple sheets, and the deleted sheet is located at the start, shorten the range
        if (sheet === sheetInterval.first) {
            return this.getTextWithRefs(grammar, {
                sheet1Ref: new SheetRef(sheetInterval.first + 1, this.sheetRefs.r1!.abs),
                sheet2Ref: new SheetRef(sheetInterval.last, this.sheetRefs.r2!.abs)
            });
        }

        // if this token refers to multiple sheets, and the deleted sheet is located at the end, shorten the range
        if (sheet === sheetInterval.last) {
            return this.getTextWithRefs(grammar, {
                sheet1Ref: new SheetRef(sheetInterval.first, this.sheetRefs.r1!.abs),
                sheet2Ref: new SheetRef(sheetInterval.last - 1, this.sheetRefs.r2!.abs)
            });
        }

        return null;
    }

    /**
     * Resolves the new text representation of this token, before a sheet will
     * be renamed in the spreadsheet document, or after an existing sheet has
     * been copied.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the renamed sheet.
     *
     * @param sheetName
     *  The new name of the sheet.
     *
     * @returns
     *  The text representation of this sheet reference token, if it has been
     *  changed; otherwise `null`.
     */
    override resolveRenamedSheet(grammar: FormulaGrammar, sheet: number, sheetName: string): string | null {

        // nothing to do for external references
        if (this.isExtDoc()) { return null; }

        // create the string representation only if this token refers to the renamed sheet
        const { r1, r2 } = this.sheetRefs;
        const sheet1Ref = (r1?.sheet === sheet) ? new SheetRef(sheetName, r1.abs) : r1;
        const sheet2Ref = (r2?.sheet === sheet) ? new SheetRef(sheetName, r2.abs) : r2;
        if ((r1 !== sheet1Ref) || (r2 !== sheet2Ref)) {
            return this.getTextWithRefs(grammar, { sheet1Ref, sheet2Ref });
        }

        return null;
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses implement this method to resolve the text representation of
     * this token with optional custom sheet references.
     */
    protected abstract getTextWithRefs(grammar: FormulaGrammar, options?: TokenTextOptions & ResolveSheetRefsOptions): string;

    /**
     * Returns the sheet references of this token, and allows to override the
     * own sheet references with arbitrary sheet reference structures, as
     * offered by various public methods of this class, and other subclasses.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The effective sheet references.
     */
    protected resolveSheetRefs(options?: ResolveSheetRefsOptions): SheetRefs {
        const r1 = options?.sheet1Ref ?? this.sheetRefs.r1;
        const r2 = options?.sheet2Ref ?? this.sheetRefs.r2;
        return new SheetRefs(r1, r2, this.sheetRefs.doc);
    }

    /**
     * Returns the sheet interval referred by this token.
     *
     * @param refSheet
     *  The index of the reference sheet used if this token does not contain
     *  its own sheet reference.
     *
     * @returns
     *  The interval of sheets referred by this token. If the token does not
     *  contain a valid sheet interval (according to the passed reference
     *  sheet), `null` will be returned.
     */
    protected resolveSheetInterval(refSheet: OptSheet): Interval | null {
        if (this.isExtDoc()) { return null; }
        const sheet1 = this.sheetRefs.r1?.sheet ?? refSheet;
        const sheet2 = this.sheetRefs.r2?.sheet ?? sheet1;
        return (is.number(sheet1) && (sheet1 >= 0) && is.number(sheet2) && (sheet2 >= 0)) ? Interval.create(sheet1, sheet2) : null;
    }

    /**
     * Returns a text description of the sheet names for debug logging.
     */
    protected debugSheetPrefix(): string {

        // the external document reference
        let sheetName = this.sheetRefs.doc ? `[${this.sheetRefs.doc.index}]` : "";

        // convert sheet names
        const { r1, r2 } = this.sheetRefs;
        if (r1) { sheetName += String(r1); }
        if (r1 && r2 && !r1.equals(r2)) { sheetName += `:${r2}`; }

        return sheetName ? `${sheetName}!` : "";
    }
}
