/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { type CellRefs, CellRef, equalCellRefs, reorderCellRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import type { SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { TokenTextOptions } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { type ResolveSheetRefsOptions, SheetRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/sheetreftoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class ReferenceToken =======================================================

/**
 * A formula token that represents a cell range reference in a formula.
 */
export class ReferenceToken extends SheetRefToken {

    readonly #cellRefs: CellRefs;

    /**
     * @param docAccess
     *  The document access interface.
     *
     * @param [cellRefs]
     *  The cell references.
     *
     * @param [sheetRefs]
     *  The sheet references.
     */
    constructor(docAccess: IDocumentAccess, cellRefs?: CellRefs | CellRef, sheetRefs?: SheetRefs) {
        super("ref", docAccess, sheetRefs);

        // expand single cell reference to `CellRefs` structure
        this.#cellRefs = (cellRefs instanceof CellRef) ? { r1: cellRefs } : { ...cellRefs };

        // swap column/row indexes, if cell references are not in order
        reorderCellRefs(this.#cellRefs);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the address of the cell range represented by this token,
     * including sheet indexes; or `null`, if the range address is invalid.
     *
     * @param refAddress
     *  The address of the original reference cell the reference token is
     *  related to. If this address is different to `targetAddress`, the
     *  resolved cell range will be relocated according to these two addresses.
     *
     * @param targetAddress
     *  The address of the target cell for relocation. If this address is
     *  different to `refAddress`, the resolved cell range will be relocated
     *  according to these two addresses.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve references without
     *  sheets. If set to `null`, and this token does not contain its own sheet
     *  reference, the cell range address cannot be resolved.
     *
     * @param [wrapReferences=false]
     *  If set to `true`, and if the relocated cell range is located outside
     *  the sheet, the range will be wrapped at the sheet borders.
     *
     * @returns
     *  The address of the cell range referenced by this reference token; or
     *  `null`, if this token does not refer to a valid range.
     */
    getRange3D(refAddress: Address, targetAddress: Address, refSheet: OptSheet, wrapReferences?: boolean): Range3D | null {

        // resolve sheet references, return null on error
        const sheetInterval = this.resolveSheetInterval(refSheet);
        if (!sheetInterval) { return null; }

        // return immediately, if cell1Ref cannot be relocated
        const { r1, r2 } = this.#resolveCellRefs(refAddress, targetAddress, wrapReferences);
        if (!r1) { return null; }

        // create a 3D range with ordered column/row indexes
        const address1 = r1.toAddress();
        const address2 = r2?.toAddress();
        return address2 ?
            Range3D.fromAddresses3D(address1, address2, sheetInterval.first, sheetInterval.last) :
            Range3D.fromAddress3D(address1, sheetInterval.first, sheetInterval.last);
    }

    /**
     * Returns the addresses of the cell ranges that will be built from the
     * cell range represented by this token, including sheet indexes, but
     * expanded relative to a reference address according to the specified
     * target ranges.
     *
     * @param refAddress
     *  The address of the original reference cell this token is related to.
     *  The resulting ranges returned by this method will be expanded according
     *  to the distance of the target ranges, and this reference address.
     *
     * @param targetRanges
     *  The target ranges to expand the relative components of this reference
     *  to.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve references without
     *  sheets. If set to `null`, and this token does not contain its own sheet
     *  reference, the cell ranges cannot be resolved.
     *
     * @param [wrapReferences=false]
     *  If set to `true`, and if the relocated cell ranges are located outside
     *  the sheet, they will be wrapped at the sheet borders.
     *
     * @returns
     *  The resulting expanded cell range list (may be multiple ranges per
     *  target range in some situations in wrapping mode).
     */
    getExpandedRanges(refAddress: Address, targetRanges: RangeSource, refSheet: OptSheet, wrapReferences?: boolean): Range3DArray | null {

        // nothing to do for reference errors
        const { r1, r2 } = this.#cellRefs;
        if (!r1) { return null; }

        // resolve sheet references, return null on error
        const sheetInterval = this.resolveSheetInterval(refSheet);
        if (!sheetInterval) { return null; }

        // whether this reference is a single relative column/row reference
        const singleRelCol = this.#isSingleRelative(true);
        const singleRelRow = this.#isSingleRelative(false);

        // returns the adjusted index of the passed cell reference, according to reference address, and target range
        const getMovedIndex = (cellRef: CellRef, targetRange: Range, columns: boolean, leading: boolean): number => {
            const index = cellRef.getIndex(columns);
            if (cellRef.isAbs(columns)) { return index; }
            const targetIndex = (leading ? targetRange.a1 : targetRange.a2).get(columns);
            return index + targetIndex - refAddress.get(columns);
        };

        // returns the expanded column or row intervals of this token, according to the reference address and target range
        const getIntervals = (targetRange: Range, columns: boolean): IntervalArray | null => {

            // the moved index of the resulting range for the start address of the target range
            let index1 = getMovedIndex(r1, targetRange, columns, true);
            if (r2) { index1 = Math.min(index1, getMovedIndex(r2, targetRange, columns, true)); }

            // the moved index of the resulting range for the end address of the target range
            let index2 = getMovedIndex(r1, targetRange, columns, false);
            if (r2) { index2 = Math.max(index2, getMovedIndex(r2, targetRange, columns, false)); }

            // the largest valid column/row index in a sheet
            const maxIndex = this.docAccess.addressFactory.getMaxIndex(columns);

            // wrapping mode: detect how to expand the resulting intervals
            if (wrapReferences) {

                // Normalize indexes outside the sheet area: If the start index is negative, move it so that the interval
                // will be located inside the sheet area, or will wrap at its end. If the start index is too large, move
                // it back so that the interval becomes part of the sheet area. Afterwards, the interval will either be
                // completely inside the sheet area, or will exceed it at its end.
                //
                if (index1 < 0) {
                    index1 += maxIndex + 1;
                    index2 += maxIndex + 1;
                } else if (index1 > maxIndex) {
                    index1 -= maxIndex + 1;
                    index2 -= maxIndex + 1;
                }

                // Create the resulting column/row intervals.
                //
                // Case 1: If the end index is inside the sheet, the interval was either not wrapped, or was wrapped
                // completely without covering the sheet boundaries. In this case, a single interval will be created.
                //
                // Case 2: The resulting interval covers the sheet boundaries, and this reference token represents a
                // single relative column or row (e.g. the rows in the reference C2:D2). This means that the target
                // range has caused to expand the single column/row to an interval which needs to be split into two
                // parts: One at the leading border of the sheet, and one at the trailing border. Example: The
                // reference C2:D2 with reference address A3, and target range A1:A3, will be expanded to the ranges
                // C2:D2 (for target address A3), C1:D1 (for target address A2), and C0:D0 (for target address A1).
                // The latter range will be wrapped to C1048576:D1048576. Therefore, the resulting row intervals
                // returned by this function will be 1:2 and 1048576:1048576.
                //
                // Case 3: The resulting interval covers the sheet boundaries, and this reference token is not a single
                // relative column or row. This means that wrapping causes to swap the start/end index of the resulting
                // range with the effect that the entire interval of the sheet area will be covered. Example: The
                // reference C2:D4 with reference address A3, and target range A1:A3, will be expanded to the ranges
                // C2:D4 (for target address A3), C1:D3 (for target address A2), and C0:D2 (for target address A1).
                // The latter range will be wrapped and swapped to C2:D1048576. Therefore, all rows in the sheet will
                // be covered by the resulting ranges, and the row interval returned by this function will be 1:1048576.
                //
                const intervals = new IntervalArray();
                if (index2 <= maxIndex) { // case 1
                    intervals.push(new Interval(index1, index2));
                } else if (columns ? singleRelCol : singleRelRow) { // case 2
                    intervals.push(new Interval(0, index2 - maxIndex - 1));
                    intervals.push(new Interval(index1, maxIndex));
                } else { // case 3
                    intervals.push(new Interval(0, maxIndex));
                }
                return intervals;
            }

            // non-wrapping mode: try to crop the indexes to the valid sheet area, return null otherwise
            index1 = Math.max(index1, 0);
            index2 = Math.min(index2, maxIndex);
            return (index1 <= index2) ? new IntervalArray(new Interval(index1, index2)) : null;
        };

        return Range3DArray.map(targetRanges, targetRange => {
            const colIntervals = getIntervals(targetRange, true);
            const rowIntervals = colIntervals ? getIntervals(targetRange, false) : null;
            return rowIntervals ? Range3DArray.fromIntervals(colIntervals, rowIntervals, sheetInterval.first, sheetInterval.last) : null;
        });
    }

    /**
     * Returns whether this reference token contains at least one relative
     * column reference, e.g. the "C" in `C$4:$D$5`.
     *
     * @returns
     *  Whether this reference token contains at least one relative column
     *  reference.
     */
    hasRelCol(): boolean {

        const { r1, r2 } = this.#cellRefs;

        // reference errors (missing first cell reference) cannot be relative
        if (!r1) { return false; }

        // immediately return if the first cell reference contains a relative column
        if (!r1.absCol) { return true; }

        // otherwise check the column reference of the second cell reference if existing
        return !!r2 && !r2.absCol;
    }

    /**
     * Returns whether this reference token contains at least one relative row
     * reference, e.g. the "4" in `$C4:$D$5`.
     *
     * @returns
     *  Whether this reference token contains at least one relative row
     *  reference.
     */
    hasRelRow(): boolean {

        const { r1, r2 } = this.#cellRefs;

        // reference errors (missing first cell reference) cannot be relative
        if (!r1) { return false; }

        // immediately return if the first cell reference contains a relative row
        if (!r1.absRow) { return true; }

        // otherwise check the row reference of the second cell reference if existing
        return !!r2 && !r2.absRow;
    }

    /**
     * Changes the current cell range of this token, keeps all absolute flags
     * and sheet references intact.
     *
     * @param range
     *  The new position of this reference token.
     *
     * @returns
     *  Whether the token has been changed.
     */
    setRange(range: Range): boolean {

        let { r1, r2 } = this.#cellRefs;

        // whether the token changes (either start or end address)
        let changed = false;

        // create first cell reference, if missing
        if (!r1) {
            r1 = this.#cellRefs.r1 = new CellRef(0, 0, true, true);
            changed = true;
        }

        // create second cell reference, if missing (and if the passed range is not a single cell)
        if (!r2 && !range.single()) {
            r2 = this.#cellRefs.r2 = r1.clone(); // copy the absolute flags of first reference
            changed = true;
        }

        // set the new address indexes for the first cell reference, update the changed flag
        if (r1.col !== range.a1.c) { r1.col = range.a1.c; changed = true; }
        if (r1.row !== range.a1.r) { r1.row = range.a1.r; changed = true; }

        // set the new address indexes for the second cell reference, update the changed flag
        if (r2) {
            if (r2.col !== range.a2.c) { r2.col = range.a2.c; changed = true; }
            if (r2.row !== range.a2.r) { r2.row = range.a2.r; changed = true; }
            // reset `r2` if address and absolute flags are equal to `r1`
            if (r1.equals(r2)) { delete this.#cellRefs.r2; changed = true; }
        }

        return changed;
    }

    /**
     * Relocates this reference token to a new cell position.
     *
     * @param refAddress
     *  The source address to relocate the reference token from.
     *
     * @param targetAddress
     *  The target address to relocate the reference token to.
     *
     * @param [wrapReferences=false]
     *  Whether to wrap the addresses at the sheet boundaries.
     *
     * @returns
     *  Whether the token has been changed.
     */
    override relocateRange(refAddress: Address, targetAddress: Address, wrapReferences?: boolean): boolean {

        const { r1, r2 } = this.#cellRefs;

        // do not touch tokens representing a reference error (missing first cell reference)
        if (!r1 || refAddress.equals(targetAddress)) { return false; }

        // the address factory with the sheet limits
        const { addressFactory } = this.docAccess;

        // relocate first cell reference, bail out immediately on error
        if (!r1.relocate(addressFactory, refAddress, targetAddress, wrapReferences)) {
            delete this.#cellRefs.r1;
            delete this.#cellRefs.r2;
            return true;
        }

        // relocate second cell reference if existing
        if (r2 && !r2.relocate(addressFactory, refAddress, targetAddress, wrapReferences)) {
            delete this.#cellRefs.r1;
            delete this.#cellRefs.r2;
            return true;
        }

        // swap column/row indexes, if cell references are not in order
        reorderCellRefs(this.#cellRefs);
        return true;
    }

    /**
     * Resolves the new text representation of this token with the relative
     * address parts being relocated.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param refAddress
     *  The source reference address for relocation.
     *
     * @param targetAddress
     *  The target reference address for relocation.
     *
     * @param refSheet
     *  The index of the reference sheet to be used if this token does not
     *  contain its own sheet reference.
     *
     * @param wrapReferences
     *  Whether to wrap the addresses at the sheet boundaries.
     *
     * @returns
     *  The string representation of the moved cell reference, if it has been
     *  changed; otherwise `null`.
     */
    override resolveRelocateRange(grammar: FormulaGrammar, refAddress: Address, targetAddress: Address, refSheet: OptSheet, wrapReferences: boolean): string | null {

        // nothing to do, if this token represents a reference error
        const { r1, r2 } = this.#cellRefs;
        if (!r1) { return null; }

        // the original and relocated range, with sheet indexes (nothing more to do for reference errors)
        const relocRange3d = this.getRange3D(refAddress, targetAddress, refSheet, wrapReferences);
        if (!relocRange3d) { return null; }

        // convert the range address, nothing to do, if the range does not change (compare only the columns/rows, not the sheets)
        const cellRefs: CellRefs = {
            r1: CellRef.create(relocRange3d.a1, r1.absCol, r1.absRow),
            r2: r2 && CellRef.create(relocRange3d.a2, r2.absCol, r2.absRow)
        };

        // nothing changes, if the transformed references equal the original references
        if (equalCellRefs(this.#cellRefs, cellRefs)) { return null; }

        // swap column/row indexes, if cell references are not in order
        reorderCellRefs(cellRefs);

        // generate the reference text
        return grammar.formatReference(this.docAccess, targetAddress, cellRefs, this.sheetRefs);
    }

    /**
     * Resolves the new text representation of this token, before cell ranges
     * will be moved in a specific sheet of the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the sheet containing the moved cell ranges.
     *
     * @param transformer
     *  An address transformer created while generating the move document
     *  operations.
     *
     * @param refAddress
     *  The source reference address for relocation.
     *
     * @param targetAddress
     *  The target reference address for relocation.
     *
     * @param refSheet
     *  The index of the reference sheet to be used if this token does not
     *  contain its own sheet reference.
     *
     * @param freezeRelRefs
     *  Whether to suppress moving relative references in the token.
     *
     * @returns
     *  The string representation of the moved cell reference, if it has been
     *  changed; otherwise `null`.
     */
    override resolveMovedCells(grammar: FormulaGrammar, sheet: number, transformer: AddressTransformer, refAddress: Address, targetAddress: Address, refSheet: OptSheet, freezeRelRefs: boolean): string | null {

        // nothing to do, if this token represents a reference error
        const { r1, r2 } = this.#cellRefs;
        if (!r1) { return null; }

        // the original and relocated range, with sheet indexes (nothing more to do for reference errors)
        const oldRange3d = this.getRange3D(refAddress, refAddress, refSheet);
        if (!oldRange3d) { return null; }
        const relocRange3d = refAddress.equals(targetAddress) ? oldRange3d : this.getRange3D(refAddress, targetAddress, refSheet);
        if (!relocRange3d) { return null; }

        // convert the range address, nothing to do, if the range does not change (compare only the columns/rows, not the sheets)
        const cellRefs: CellRefs = fun.do(() => {

            // special behavior if relative references will not be transformed
            const isFrozen1 = freezeRelRefs && !r1.isAbs(transformer.columns);
            const isFrozen2 = freezeRelRefs && !r2?.isAbs(transformer.columns);

            // do not transform range, if both reference components are frozen, or if it refers to another sheet
            if ((isFrozen1 && isFrozen2) || !relocRange3d.isSheet(sheet)) {
                return {
                    r1: CellRef.create(relocRange3d.a1, r1.absCol, r1.absRow),
                    r2: r2 && CellRef.create(relocRange3d.a2, r2.absCol, r2.absRow)
                };
            }

            // transform only the end address of the range, if the start address is frozen
            if (isFrozen1) {
                const address2 = r2 && transformer.transformAddress(relocRange3d.a2);
                return {
                    r1: CellRef.create(relocRange3d.a1, r1.absCol, r1.absRow),
                    r2: r2 && address2 && CellRef.create(address2, r2.absCol, r2.absRow)
                };
            }

            // transform only the start address of the range, if the end address is frozen
            if (isFrozen2) {
                const address1 = transformer.transformAddress(relocRange3d.a1);
                return {
                    r1: address1 && CellRef.create(address1, r1.absCol, r1.absRow),
                    r2: r2 && CellRef.create(relocRange3d.a2, r2.absCol, r2.absRow)
                };
            }

            // otherwise: transform the entire range
            const newRange = transformer.transformRanges(relocRange3d.toRange()).first();
            return {
                r1: newRange ? CellRef.create(newRange.a1, r1.absCol, r1.absRow) : undefined,
                r2: (newRange && r2) ? CellRef.create(newRange.a2, r2.absCol, r2.absRow) : undefined
            };
        });

        // nothing changes, if the transformed references equal the original references
        if (equalCellRefs(this.#cellRefs, cellRefs)) { return null; }

        // swap column/row indexes, if cell references are not in order
        reorderCellRefs(cellRefs);

        // generate the reference text
        return grammar.formatReference(this.docAccess, targetAddress, cellRefs, this.sheetRefs);
    }

    /**
     * Resolves the new text representation of this token, after cell ranges
     * have been cut/paste in a specific sheet of the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the sheet containing the source cell range.
     *
     * @param cutRange
     *  The address of the cell range that has been cut to clipboard.
     *
     * @param pasteRange
     *  The address of the cell range to paste to.
     *
     * @param refSheet
     *  The index of the reference sheet to be used if this token does not
     *  contain its own sheet reference.
     *
     * @returns
     *  The new string representation of the pasted cell reference; or `null`,
     *  if the string representation does not change.
     */
    override resolveCutPasteCells(grammar: FormulaGrammar, sheet: number, cutRange: Range, pasteRange: Range, refSheet: OptSheet): string | null {

        // nothing to do, if this token represents a reference error
        const { r1, r2 } = this.#cellRefs;
        if (!r1) { return null; }

        // nothing to do, if this token represents a reference error, or points
        // to another sheet or a sheet range, or sheet reference is invalid
        const oldRange3d = this.getRange3D(Address.A1, Address.A1, refSheet);
        if (!oldRange3d?.isSheet(sheet)) { return null; }

        const sameCol = cutRange.a1.c === pasteRange.a1.c;
        const sameRow = cutRange.a1.r === pasteRange.a1.r;
        const columns = sameCol ? true : sameRow ? false : null;
        const forward = cutRange.a1.get(!columns) < pasteRange.a1.get(!columns);
        const oldRange = oldRange3d.toRange();
        const overlaps = oldRange.overlaps(pasteRange);

        const addr1 = r1.toAddress();
        if (r2 ? cutRange.contains(new Range(addr1, r2.toAddress())) : cutRange.containsAddress(addr1)) {

            // relocate the ranges with temporary clones of the cell references
            this.#cellRefs.r1 = r1.clone();
            this.#cellRefs.r2 = r2?.clone();
            this.relocateRange(cutRange.a1, pasteRange.a1, false);

            // store the relocated cell references, and restore the original references
            const cellRefs = { ...this.#cellRefs };
            this.#cellRefs.r1 = r1;
            this.#cellRefs.r2 = r2;

            return grammar.formatReference(this.docAccess, pasteRange.a1, cellRefs, this.sheetRefs);
        }

        // one end (top, right, bottom or left) completly covered by sourceRange
        if (r2 && (r1.getIndex(!!columns) >= cutRange.a1.get(!!columns)) && (r2.getIndex(!!columns) <= cutRange.a2.get(!!columns))) {

            if (!sameCol && !sameRow) { return null; }

            const newRefRange = new RangeArray(oldRange).difference(cutRange).first(); // oldRange without sourceRange
            if (!newRefRange) { return null; }

            const cutRefRange = cutRange.intersect(oldRange);
            if (!cutRefRange) { return null; }

            // current indexes
            let col1 = r1.col, row1 = r1.row;
            let col2 = r2.col, row2 = r2.row;

            const startCutted = cutRange.contains(columns ? oldRange.headerRow() : oldRange.leadingCol());
            const endCutted   = cutRange.contains(columns ? oldRange.footerRow() : oldRange.trailingCol());
            const startPasted = pasteRange.contains(columns ? oldRange.headerRow() : oldRange.leadingCol());
            const endPasted   = pasteRange.contains(columns ? oldRange.footerRow() : oldRange.trailingCol());

            const xOffset = cutRefRange.a1.c - cutRange.a1.c;
            const yOffset = cutRefRange.a1.r - cutRange.a1.r;
            const xOver   = cutRange.cols() - xOffset - cutRefRange.cols();
            const yOver   = cutRange.rows() - yOffset - cutRefRange.rows();

            if (startCutted) {
                if (endPasted) {
                    col1 = newRefRange.a1.c;
                    row1 = newRefRange.a1.r;
                    col2 = pasteRange.a1.c + xOffset + cutRefRange.cols() - 1;
                    row2 = pasteRange.a1.r + yOffset + cutRefRange.rows() - 1;
                } else if (overlaps || !forward) {
                    col1 = (xOffset > 0 || !forward) ? (pasteRange.a1.c + xOffset) : newRefRange.a1.c;
                    row1 = (yOffset > 0 || !forward) ? (pasteRange.a1.r + yOffset) : newRefRange.a1.r;
                    col2 = Math.max(cutRefRange.cols(), newRefRange.a2.c);
                    row2 = Math.max(cutRefRange.rows(), newRefRange.a2.r);
                }
            } else if (endCutted) {
                if (startPasted) {
                    col1 = Math.min(newRefRange.a1.c, pasteRange.a1.c + xOffset);
                    row1 = Math.min(newRefRange.a1.r, pasteRange.a1.r + yOffset);
                    col2 = (xOver > 0 && forward) ? pasteRange.a1.c + xOffset + cutRefRange.cols() - 1 : newRefRange.a2.c;
                    row2 = (yOver > 0 && forward) ? pasteRange.a1.r + yOffset + cutRefRange.rows() - 1 : newRefRange.a2.r;
                } else if (overlaps || forward) {
                    col1 = Math.min(newRefRange.a1.c, pasteRange.a1.c + xOffset);
                    row1 = Math.min(newRefRange.a1.r, pasteRange.a1.r + yOffset);
                    col2 = (xOver > 0 || forward) ? pasteRange.a1.c + xOffset + cutRefRange.cols() - 1 : newRefRange.a2.c;
                    row2 = (yOver > 0 || forward) ? pasteRange.a1.r + yOffset + cutRefRange.rows() - 1 : newRefRange.a2.r;
                }
            }

            // create new range from calculated values
            const newRange = Range.fromIndexes(col1, row1, col2, row2);

            // nothing changes, if the transformed range equals the original range
            if (newRange.equals(oldRange)) { return null; }

            // create copies of the cell references that will be modified to generate the text representation
            const cellRefs: CellRefs = {
                r1: CellRef.create(newRange.a1, r1.absCol, r1.absRow),
                r2: CellRef.create(newRange.a2, r2.absCol, r2.absRow)
            };

            // swap column/row indexes, if cell references are not in order
            reorderCellRefs(cellRefs);

            // generate the reference text
            return grammar.formatReference(this.docAccess, pasteRange.a1, cellRefs, this.sheetRefs);
        }

        return null;
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates the text representation of this token.
     */
    protected getTextWithRefs(grammar: FormulaGrammar, options?: TokenTextOptions & ResolveSheetRefsOptions): string {
        const refAddress = options?.refAddress ?? Address.A1;
        const targetAddress = options?.targetAddress ?? Address.A1;
        const cellRefs = this.#resolveCellRefs(refAddress, targetAddress, options?.wrapReferences);
        const sheetRefs = this.resolveSheetRefs(options);
        return grammar.formatReference(this.docAccess, targetAddress, cellRefs, sheetRefs);
    }

    /**
     * Returns a text description of this token for debug logging.
     */
    protected debugText(): string {
        const { r1, r2 } = this.#cellRefs;
        let result = this.debugSheetPrefix();
        result += String(r1 ?? ErrorCode.REF);
        if (r1 && r2) { result += ":" + String(r2); }
        return result;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether this reference token contains relative single column or
     * row references only.
     *
     * @param columns
     *  Whether to check the column references (`true`), or the row references
     *  (`false`).
     *
     * @returns
     *  Whether this reference token contains relative single column or row
     *  references only.
     */
    #isSingleRelative(columns: boolean): boolean {
        const { r1, r2 } = this.#cellRefs;
        // absolute index in first cell reference: this reference cannot be relative
        if (!r1 || r1.isAbs(columns)) { return false; }
        // no second cell reference: this reference is single and relative
        if (!r2) { return true; }
        // second cell reference must be relative, and the indexes must be equal
        return !r2.isAbs(columns) && (r1.getIndex(columns) === r2.getIndex(columns));
    }

    /**
     * Returns copies of the own cell references that have been relocated
     * according to the passed settings.
     *
     * @returns
     *  The resulting cell references.
     */
    #resolveCellRefs(refAddress: Address, targetAddress: Address, wrapReferences?: boolean): CellRefs {

        // performance: return original cell references, if nothing to relocate
        if (refAddress.equals(targetAddress)) { return this.#cellRefs; }

        // the address factory with the sheet limits
        const { addressFactory } = this.docAccess;

        // the result object returned by this method: start with clones of the cell references
        const cellRefs = { r1: this.#cellRefs.r1?.clone(), r2: this.#cellRefs.r2?.clone() };

        // if relocating first reference is not successful, remove it from the result
        if (cellRefs.r1 && !cellRefs.r1.relocate(addressFactory, refAddress, targetAddress, wrapReferences)) {
            delete cellRefs.r1;
        }

        // if relocating second reference is not successful, remove both cell references from the result
        if (cellRefs.r1 && cellRefs.r2 && !cellRefs.r2.relocate(addressFactory, refAddress, targetAddress, wrapReferences)) {
            delete cellRefs.r1;
            delete cellRefs.r2;
        }

        // swap column/row indexes, if cell references are not in order
        reorderCellRefs(cellRefs);
        return cellRefs;
    }
}
