/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str } from "@/io.ox/office/tk/algorithms";

import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { DefNameRef, TableRef, IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import type { SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { NameRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/namereftoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class DefinedNameToken =====================================================

/**
 * A formula token that represents a defined name in a formula.
 *
 * @param docAccess
 *  The document access interface.
 *
 * @param label
 *  The label of the defined name.
 *
 * @param [sheetRefs]
 *  The sheet reference structure (e.g. the particle "Sheet1" in the formula
 *  `Sheet1!my_name`), or `undefined` for a local name in the own sheet, or a
 *  global name.
 */
export class DefinedNameToken extends NameRefToken {

    constructor(docAccess: IDocumentAccess, label: string, sheetRefs?: SheetRefs) {
        super("name", docAccess, label, sheetRefs);
    }

    // public methods ---------------------------------------------------------

    /**
     * Resolves the defined name referred by this token.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve sheet-local names
     *  without sheet reference. If set to `null`, and this token does not
     *  contain its own sheet reference, this method looks for globally defined
     *  names only.
     *
     * @returns
     *  The reference to the defined name, if existing; otherwise `undefined`.
     */
    resolveName(refSheet: OptSheet): Opt<DefNameRef> {
        return this.#resolveName(this.value, refSheet);
    }

    /**
     * Resolves a table range referred by this token.
     *
     * @returns
     *  The reference to the table range, if existing; otherwise `undefined`.
     */
    resolveTable(): Opt<TableRef> {

        // no support for references to external table ranges
        if (this.isExtDoc()) { return undefined; }

        // invalid sheet reference (e.g.: #REF!!Table1) cannot refer to a table
        const sheetRef = this.sheetRef;
        if (sheetRef && !sheetRef.valid()) { return undefined; }

        // search for a table range in entire document
        const tableRef = this.docAccess.resolveTable(this.value);
        if (!tableRef) { return undefined; }

        // sheet reference exists (e.g.: Sheet2!Table1): ensure that sheet index matches
        return (!sheetRef || (sheetRef.sheet === tableRef.sheet)) ? tableRef : undefined;
    }

    /**
     * Resolves the new text representation of this token, before the label of
     * a defined name will be changed in the spreadsheet document.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param sheet
     *  The index of the sheet containing the changed name; or `null`, if a
     *  globally defined name has been changed.
     *
     * @param oldLabel
     *  The old label of the defined name.
     *
     * @param newLabel
     *  The new label of the defined name.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve sheet-local names.
     *
     * @returns
     *  The text representation of this name token, if it would be changed;
     *  otherwise `null`.
     */
    override resolveRelabeledName(grammar: FormulaGrammar, sheet: OptSheet, oldLabel: string, newLabel: string, refSheet: OptSheet): string | null {

        // nothing to do, if the old label of the relabeled name does not match
        if (!str.equalsICC(oldLabel, this.value)) { return null; }

        // try to find the name in the document (nothing to do, if the name cannot
        // be found, or if an unqualified name reference does not refer to the changed name)
        const nameRef = this.#resolveName(oldLabel, refSheet);
        if (!nameRef || (nameRef.sheet !== sheet)) { return null; }

        // generate the text representation for the new label
        return grammar.formatName(this.docAccess, newLabel, this.sheetRefs);
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates the text representation of this token with the specified sheet
     * reference and custom label.
     */
    protected override formatText(grammar: FormulaGrammar, label: string, sheetRefs?: SheetRefs): string {
        return grammar.formatName(this.docAccess, label, sheetRefs);
    }

    // private methods --------------------------------------------------------

    /**
     * Resolves a sheet-local defined name.
     *
     * @param sheet
     *  The index of the sheet to search for the name. If this value is not a
     *  number (invalid sheet reference in formula, e.g. "#REF!!sheet"), this
     *  method will return `undefined`.
     *
     * @param label
     *  The label of the sheet-local defined name to be returned.
     *
     * @returns
     *  The reference to an existing sheet-local name; otherwise `undefined`.
     */
    #resolveSheetName(sheet: unknown, label: string): Opt<DefNameRef> {
        return (is.number(sheet) && (sheet >= 0)) ? this.docAccess.resolveName(sheet, label) : undefined;
    }

    /**
     * Resolves the specified defined name.
     *
     * @param label
     *  The label of the defined name to be returned.
     *
     * @param refSheet
     *  The index of the reference sheet used to resolve sheet-local names
     *  without sheet reference. If set to `null`, and this token does not
     *  contain its own sheet reference, this method looks for globally defined
     *  names only.
     *
     * @returns
     *  The reference to an existing defined name; otherwise `null`.
     */
    #resolveName(label: string, refSheet: OptSheet): Opt<DefNameRef> {

        // TODO: support for external names
        if (this.isExtDoc()) { return undefined; }

        // sheet reference exists (e.g.: "Sheet2!name"): resolve from specified sheet, do not try global names
        if (this.sheetRef) { return this.#resolveSheetName(this.sheetRef.sheet, label); }

        // document reference without sheet reference (e.g.: "[0]!name") refers to global names only
        if (this.sheetRefs.doc) { return this.docAccess.resolveName(null, label); }

        // no sheet reference (i.e. just "name"): first try names in reference sheet, then global names
        return this.#resolveSheetName(refSheet, label) ?? this.docAccess.resolveName(null, label);
    }
}
