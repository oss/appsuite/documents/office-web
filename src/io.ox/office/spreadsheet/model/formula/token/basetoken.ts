/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { OptSheet, TransformSheetVector } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { RefSheetOptions, WrapReferencesOptions, UnqualifiedTablesOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// types ======================================================================

/**
 * Type specifiers for all supported formula token types.
 *
 * - `op`: A unary or binary operator.
 * - `sep`: A range list operator, or a separator in a function parameter list.
 * - `open`: An opening parenthesis.
 * - `close`: A closing parenthesis.
 * - `lit`: A scalar literal.
 * - `mat`: A matrix literal.
 * - `func`: A built-in function.
 * - `ref`: A cell range reference.
 * - `name`: A defined name.
 * - `macro`: A macro function call.
 * - `table`: A structured table reference.
 * - `ws`: A whitespace token.
 * - `bad`: A "bad token" containing unparsable text.
 */
export type TokenType = "op" | "sep" | "open" | "close" | "lit" | "mat" | "func" | "ref" | "name" | "macro" | "table" | "ws" | "bad";

/**
 * Optional parameters for resolving the text representation of a formula token
 * with the method `BaseToken#getText`.
 */
export interface TokenTextOptions extends RefSheetOptions, WrapReferencesOptions, UnqualifiedTablesOptions {

    /**
     * The address of the original reference cell the reference token is
     * related to. If omitted, cell A1 will be used instead. If this address is
     * different to the address resulting from the option `targetAddress`, the
     * resolved cell range will be relocated according to these two addresses.
     */
    refAddress?: Address;

    /**
     * The address of the target cell for relocation. If omitted, cell A1 will
     * be used instead. If this address is different to the address resulting
     * from the option `refAddress`, the resolved cell range will be relocated
     * according to these two addresses.
     */
    targetAddress?: Address;
}

export interface ResolveTokenOptions {

    /**
     * Whether the token will be updated after the respective change already
     * happened in the document. Default value is `false`.
     */
    afterChange?: boolean;
}

// class BaseToken ============================================================

/**
 * Base class for all formula tokens used in the formula parser.
 *
 * @param type
 *  The token type identifier.
 */
export abstract class BaseToken {

    readonly type: TokenType;

    // constructor ------------------------------------------------------------

    protected constructor(type: TokenType) {
        this.type = type;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the type identifier of this token matches any of the
     * passed token types.
     *
     * @param types
     *  The token type identifiers to be checked.
     *
     * @returns
     *  Whether the type of this token matches any of the passed token types.
     */
    isType(...types: TokenType[]): boolean {
        return types.includes(this.type);
    }

    /**
     * Generates the text representation of this token, according to the passed
     * formula grammar configuration.
     *
     * @param grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param [options]
     *  Optional parameters for relocation of range addresses, or for other
     *  token-specific special behavior.
     *
     * @returns
     *  The text representation of this token.
     */
    abstract getText(grammar: FormulaGrammar, options?: TokenTextOptions): string;

    /**
     * Tries to replace unresolved sheet names with existing sheet indexes in
     * the spreadsheet document. Intended to be used after document import to
     * refresh all token arrays that refer to sheets that did not exist during
     * their creation.
     */
    refreshSheets(): void {
        // intended to be overwritten in subclasses
    }

    /**
     * Refreshes the sheet references after a sheet has been inserted, deleted,
     * or moved in the document.
     *
     * @param _xfVector
     *  The transformation vector for the sheet indexes.
     *
     * @returns
     *  Whether the token has been changed.
     */
    transformSheets(_xfVector: TransformSheetVector): boolean {
        // intended to be overwritten in subclasses
        return false;
    }

    /**
     * Relocates this reference token to a new cell position.
     *
     * @param _refAddress
     *  The source address to relocate the reference token from.
     *
     * @param _targetAddress
     *  The target address to relocate the reference token to.
     *
     * @param [_wrapReferences=false]
     *  Whether to wrap the addresses at the sheet boundaries.
     *
     * @returns
     *  Whether the token has been changed.
     */
    relocateRange(_refAddress: Address, _targetAddress: Address, _wrapReferences?: boolean): boolean {
        // intended to be overwritten in subclasses
        return false;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token, after a sheet has been deleted in the spreadsheet
     * document.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _sheet
     *  The index of the deleted sheet.
     *
     * @param _refSheet
     *  The index of the reference sheet used to resolve relative references.
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveDeletedSheet(_grammar: FormulaGrammar, _sheet: number, _refSheet: OptSheet): string | null {
        return null;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token, after a sheet has been renamed in the spreadsheet
     * document.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _sheet
     *  The index of the renamed sheet.
     *
     * @param _sheetName
     *  The new name of the sheet.
     *
     * @param [_tableNames]
     *  A map that specifies how to change the table names in table references.
     *  Used when cloning an existing sheet where the cloned table ranges have
     *  been renamed, and the formulas in the cloned sheet should refer to the
     *  new table ranges. The old table names are the map keys, and the new
     *  table names are the map values. If omitted, no table references will be
     *  changed (when renaming an existing sheet).
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveRenamedSheet(_grammar: FormulaGrammar, _sheet: number, _sheetName: string, _tableNames?: Dict<string>): string | null {
        return null;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token, before the label of a defined name will be changed in the
     * spreadsheet document.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _sheet
     *  The index of the sheet containing the changed name; or `null`, if a
     *  globally defined name has been changed.
     *
     * @param _oldLabel
     *  The old label of the defined name.
     *
     * @param _newLabel
     *  The new label of the defined name.
     *
     * @param _refSheet
     *  The index of the reference sheet used to resolve sheet-local names.
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveRelabeledName(_grammar: FormulaGrammar, _sheet: OptSheet, _oldLabel: string, _newLabel: string, _refSheet: OptSheet): string | null {
        return null;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token, before a table range will be renamed in the spreadsheet
     * document.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _oldName
     *  The old name of the table range.
     *
     * @param _newName
     *  The new name of the table range.
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveRenamedTable(_grammar: FormulaGrammar, _oldName: string, _newName: string): string | null {
        return null;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token with the relative address parts being relocated.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _refAddress
     *  The source reference address for relocation.
     *
     * @param _targetAddress
     *  The target reference address for relocation.
     *
     * @param _refSheet
     *  The index of the reference sheet used to resolve local cell references.
     *
     * @param _wrapReferences
     *  Whether to wrap the addresses at the sheet boundaries.
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveRelocateRange(_grammar: FormulaGrammar, _refAddress: Address, _targetAddress: Address, _refSheet: OptSheet, _wrapReferences: boolean): string | null {
        return null;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token, before cell ranges will be moved in a specific sheet of
     * the spreadsheet document.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _sheet
     *  The index of the sheet containing the moved cell ranges.
     *
     * @param _transformer
     *  An address transformer created while generating the move document
     *  operations.
     *
     * @param _refAddress
     *  The source reference address for relocation.
     *
     * @param _targetAddress
     *  The target reference address for relocation.
     *
     * @param _refSheet
     *  The index of the reference sheet used to resolve local cell references.
     *
     * @param _freezeRelRefs
     *  Whether to suppress moving relative references in the token.
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveMovedCells(_grammar: FormulaGrammar, _sheet: number, _transformer: AddressTransformer, _refAddress: Address, _targetAddress: Address, _refSheet: OptSheet, _freezeRelRefs: boolean): string | null {
        return null;
    }

    /**
     * Subclasses may overwrite this method to return a new text representation
     * of this token, after cell ranges have been cut and pasted in a specific
     * sheet of the spreadsheet document.
     *
     * @param _grammar
     *  The formula grammar containing specific settings for formula tokens.
     *
     * @param _sheet
     *  The index of the sheet containing the source cell range.
     *
     * @param _cutRange
     *  The address of the cell range that has been cut to clipboard.
     *
     * @param _pasteRange
     *  The address of the cell range to paste to.
     *
     * @param _refSheet
     *  The index of the reference sheet.
     *
     * @returns
     *  The new text representation of this token, if available.
     */
    resolveCutPasteCells(_grammar: FormulaGrammar, _sheet: number, _cutRange: Range, _pasteRange: Range, _refSheet: OptSheet): string | null {
        return null;
    }

    /**
     * Generates a text description of this token for debugging (!) purposes.
     *
     * @returns
     *  The complete debug text representation of this token.
     */
    toString(): string {
        const result = this.debugText();
        return `${this.type}${result ? `[${result}]` : ""}`;
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates a text description of the contents of this token for debugging
     * (!) purposes.
     *
     * @returns
     *  The debug text representation of the contents of this token.
     */
    protected abstract debugText(): string;
}
