/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import type { SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { NameRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/namereftoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class MacroToken ===========================================================

/**
 * A formula token that represents a macro function call in a formula.
 *
 * @param docAccess
 *  The document access interface.
 *
 * @param funcName
 *  The name of the macro function.
 *
 * @param [sheetRefs]
 *  The sheet reference structure (e.g. the particle "Module1" in the formula
 *  expression `Module1!myFunc()` of a macro call), or `undefined` for a
 *  globally available macro function.
 */
export class MacroToken extends NameRefToken {

    constructor(docAccess: IDocumentAccess, funcName: string, sheetRefs?: SheetRefs) {
        super("macro", docAccess, funcName, sheetRefs);
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates the text representation of this token with the specified sheet
     * reference and custom label.
     */
    protected override formatText(grammar: FormulaGrammar, label: string, sheetRefs?: SheetRefs): string {
        return grammar.formatMacro(this.docAccess, label, sheetRefs);
    }
}
