/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ScalarType, scalarToString } from "@/io.ox/office/spreadsheet/utils/scalar";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

// class ScalarToken ==========================================================

/**
 * A formula token that represents a scalar literal value (a number, a string,
 * a boolean value, or an error code) in a formula.
 *
 * @param value
 *  The value for this token. The special value `null` is reserved for empty
 *  parameter in a function call, e.g. in the formula `=SUM(1,,2)`.
 */
export class ScalarToken extends BaseToken {

    value: ScalarType;

    // constructor ------------------------------------------------------------

    constructor(value: ScalarType) {
        super("lit");
        this.value = value;
    }

    // public methods ---------------------------------------------------------

    /**
     * Changes the current value of this token.
     *
     * @param value
     *  The new value for this token.
     *
     * @returns
     *  Whether the value of the token has actually been changed.
     */
    setValue(value: ScalarType): boolean {
        if (this.value === value) { return false; }
        this.value = value;
        return true;
    }

    /**
     * Generates the text representation of this token.
     */
    override getText(grammar: FormulaGrammar): string {
        return grammar.formatScalar(this.value);
    }

    // protected methods ------------------------------------------------------

    /**
     * Generates the text representation for debug logging.
     */
    protected override debugText(): string {
        return scalarToString(this.value);
    }
}
