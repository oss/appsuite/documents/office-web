/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, map, set } from "@/io.ox/office/tk/algorithms";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { rangeKeyFromIndexes } from "@/io.ox/office/spreadsheet/utils/range";
import type { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { type OptSheet, type TransformSheetVector, mapKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { GrammarType, FormulaType, ContextType, FormulaParentModel, RefSheetOptions, WrapReferencesOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { InternalErrorCode, resolveParentModel } from "@/io.ox/office/spreadsheet/model/formula/utils/types";

import type { TokenTextOptions, BaseToken, ScalarToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import { ParenthesisToken, ReferenceToken, DefinedNameToken, TableToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";

import type { ParseToken } from "@/io.ox/office/spreadsheet/model/formula/parser/parsetoken";
import type { ParseFormulaOptions } from "@/io.ox/office/spreadsheet/model/formula/parser/formulaparser";
import type { CompilerNode, CompilerResult } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacompiler";
import type { InterpretTokensOptions, InterpreterResult } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulainterpreter";
import { type DependencyTarget, FormulaDependencies } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladependencies";
import { BaseTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray/basetokenarray";

import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Specifies how to resolve defined names and table ranges when collecting cell
 * range lists contained in a token array.
 *
 * - `none`: Defined names and table ranges will not be accepted and lead to an
 *   empty result array.
 *
 * - `simple`: Table references will be resolved, and defined names will be
 *   resolved recursively (the defined name may contain range references, other
 *   defined names, and table references).
 *
 * - `interpret`: Table references will be resolved, and defined names will be
 *   interpreted (they may contain arbitrary formula expressions with functions
 *   that resolve to a reference list).
 */
export type ResolveNamesMode = "none" | "simple" | "interpret";

/**
 * Optional parameters for the method `TokenArray.resolveRangeList()`.
 */
export interface ResolveRangeListOptions extends RefSheetOptions, WrapReferencesOptions {

    /**
     * Specifies how to resolve defined names and table ranges contained in the
     * token array. Default value is `NONE`.
     */
    resolveNames?: ResolveNamesMode;
}

/**
 * Optional parameters for the method `TokenArray.extractRanges()`.
 */
export interface ExtractRangesOptions extends RefSheetOptions, WrapReferencesOptions {

    /**
     * If set to `true`, the range addresses referred by all defined names and
     * table ranges contained in the token array till be returned too. Default
     * value is `false`.
     */
    resolveNames?: boolean;
}

/**
 * The addresses of a cell range contained in a token array with additional
 * information, as returned by the method `TokenArray.extractRanges()`.
 */
export interface ExtractRangeData {

    /**
     * The cell range address represented by a formula token, with sheet
     * indexes.
     */
    range: Range3D;

    /**
     * The internal array index of the token containing the range (may be equal
     * for different objects in the array, for example for a single name token
     * referring to multiple cell ranges).
     */
    index: number;

    /**
     * The type of the token containing the range. Will be either "ref" for a
     * regular reference token, or "name" for a defined name that evaluates to
     * one or more ranges in the specified sheet, or "table" for a table range.
     */
    type: "ref" | "name" | "table";
}

/**
 * The addresses of all cell ranges contained in a token array with additional
 * information, as returned by the method `TokenArray.extractRanges()`.
 *
 * The array object will contain the additional boolean property `circular`
 * that specifies whether trying to resolve a defined name resulted in a
 * circular reference error.
 */
export type ExtractRangesResult = ExtractRangeData[] & { circular: boolean };

/**
 * Optional parameters for the method `TokenArray#interpretFormula`.
 */
export interface InterpretFormulaOptions extends InterpretTokensOptions {

    /**
     * If set to `true`, the token array will use an internal cache for the
     * formula results. It will return the cached results upon next invocations
     * of the method `TokenArray#interpretFormula`. The results will be cached
     * separately by reference address and target address. Default value is
     * `false`.
     */
    cacheResult?: boolean;
}

/**
 * Helper interface for a token array contained in a specific sheet. Can be
 * used to pass around token arrays that surely contain a reference to a sheet
 * model.
 *
 * The generic class `TokenArray` may be global (without sheet model, i.e. the
 * property `sheetModel` is set to `undefined`). This happens e.g. with a
 * globally defined name.
 */
export interface SheetTokenArray extends TokenArray {
    sheetModel: SheetModel;
}

// class TokenArray ===========================================================

/**
 * Represents a formula expression that has been parsed and split into single
 * formula tokens (instances of the class `BaseToken`). Provides support for
 * parsing formula expressions and handling of the resulting formula token
 * arrays.
 *
 * @param parentModel
 *  The spreadsheet document model, or a specific sheet model containing this
 *  instance. The parent model determines the default reference sheet index
 *  used by various methods of this class.
 *
 * @param formulaType
 *  The type of the token array. May cause special behavior in specific
 *  situations.
 */
export class TokenArray extends BaseTokenArray<SpreadsheetModel> {

    /**
     * The model of the sheet owning this instance, or `undefined` for global
     * formula expressions (e.g. globally defined names).
     */
    readonly sheetModel: Opt<SheetModel>;

    /** Cached compiler token tree. */
    #compilerResult: Opt<CompilerResult>;
    /** Cached formula results, mapped by reference and target addresses. */
    #resultCache: Opt<Map<string, InterpreterResult>>;

    /**
     * Whether this token array is currently interpreted for different target
     * addresses, to prevent recursion (set of address keys, the same token
     * array may be used for different formula cells, e.g. in shared formulas).
     */
    #interpreting?: Set<string>;

    // constructor ------------------------------------------------------------

    constructor(parentModel: FormulaParentModel, formulaType: FormulaType) {

        const { docModel, sheetModel } = resolveParentModel(parentModel);
        super(docModel, formulaType);

        // the sheet model for sheet-local formula expressions
        this.sheetModel = sheetModel;

        // handle changes in the token array
        this.on("*", () => {
            // clear compiler tree (changed reference tokens may result in different tree)
            this.#compilerResult = undefined;
            // clear all cached formula results if a single token in this instance has been changed
            this.clearResultCache();
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the index of the sheet that contains this token array.
     *
     * @returns
     *  The index of the sheet that contains this token array; or `null`, if
     *  this is a global token array with the document model as parent.
     */
    override getRefSheet(): OptSheet {
        return this.sheetModel?.getIndex() ?? null;
    }

    /**
     * Returns the text representation of the formula represented by this token
     * array.
     *
     * @param grammarType
     *  The identifier of the formula grammar to be used to generate the
     *  formula expression.
     *
     * @param refAddress
     *  The address of the original reference cell the formula is related to.
     *  If this address is different to `targetAddress`, the generated formula
     *  expression will be modified, as if the token array has been relocated
     *  according to these two addresses (without actually modifying the token
     *  array).
     *
     * @param targetAddress
     *  The address of the target cell for relocation. If this address is
     *  different to `refAddress`, the generated formula expression will be
     *  modified, as if the token array has been relocated according to these
     *  two addresses (without actually modifying the token array).
     *
     * @param [options]
     *  Optional parameters. Can be used to override the own "wrap references"
     *  state that has been set according to the formula type passed to the
     *  constructor.
     *
     * @returns
     *  The string representation of the formula.
     */
    getFormula(grammarType: GrammarType, refAddress: Address, targetAddress: Address, options?: WrapReferencesOptions): string {

        // the formula grammar expected by the formula tokens
        const grammar = this.docModel.getFormulaGrammar(grammarType);
        // prepare the options to be passed to the method `BaseToken.getText()`
        const textOptions: TokenTextOptions = { ...this.extendWithOwnOptions(options), refAddress, targetAddress };

        // concatenate the string representations of all formula tokens
        return this.tokens.reduce((formula, token) => formula + token.getText(grammar, textOptions), "");
    }

    /**
     * Returns the formula expression in operation grammar without reference
     * relocation for this token array.
     *
     * @returns
     *  The string representation of the formula for document operations.
     */
    getFormulaOp(): string {
        return this.getFormula("op", Address.A1, Address.A1);
    }

    /**
     * Resolves the token array to a list of constant strings. The formula
     * represented by this token array must consist of one or more literal
     * string tokens, separated by list operators. Whitespace tokens will be
     * ignored.
     *
     * @returns
     *  The string literals contained in this token array; or `null`, if the
     *  formula cannot be resolved successfully to a list of strings.
     */
    resolveStringList(): string[] | null {

        // the resulting strings
        const result: string[] = [];
        // a string with a single character reflecting the token types in the token array
        let tokenTags = "";

        // process all tokens as long as they are considered valid, collect token types
        this.tokens.every(token => {
            switch (token.type) {
                case "lit": {
                    tokenTags += "L";
                    const { value } = token as ScalarToken;
                    if (!is.string(value)) { return false; }
                    result.push(value);
                    return true;
                }
                case "sep":
                    tokenTags += ",";
                    return true;
                case "ws":
                    return true;
                default:
                    tokenTags += "I"; // invalid token
                    return false;
            }
        });

        // on success, check the formula structure
        return (/^L(,L)*$/).test(tokenTags) ? result : null;
    }

    /**
     * Resolves the token array to a list of range addresses. The formula
     * represented by this token array must be completely resolvable to a range
     * list (single reference, a list of range references separated by the list
     * operator, optionally enclosed in parentheses, defined names that resolve
     * to a range list if specified with the option `resolveNames`, also
     * recursively, table references, or a mixed list of range references,
     * defined names, and table references).
     *
     * To extract the range addresses contained in an arbitrary formula, the
     * method `extractRanges()` can be used instead.
     *
     * @param refAddress
     *  The source reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param targetAddress
     *  The target reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The addresses of the resulting cell ranges with their sheet indexes. If
     *  the formula cannot be resolved successfully to a range list, this
     *  method returns an empty array.
     */
    resolveRangeList(refAddress: Address, targetAddress: Address, options?: ResolveRangeListOptions): Range3DArray {

        // the resulting range addresses
        const resultRanges = new Range3DArray();

        // prevent recursive processing of the token array, e.g. in defined name
        if (this.#isInterpreting(targetAddress)) { return resultRanges; }

        // default mode for defined names
        const resolveNames = options?.resolveNames;

        // early exit, if this token array does not contain any reference or name tokens
        const hasTokens = resolveNames ? this.hasAnyRefTokens() : this.hasToken("ref");
        if (!hasTokens) { return resultRanges; }

        // add more options (`refSheet` and `wrapReferences`)
        const allOptions = this.extendWithOwnOptions(options);
        const { refSheet, wrapReferences } = allOptions;

        // shortcut to the document model
        const { docModel } = this;

        // process all tokens as long as they are considered valid, build a signature
        // string with a single character for each token to check the formula syntax
        let tokenTags = "";
        const valid = this.tokens.every(token => {

            switch (token.type) {

                case "ref": {
                    // the cell range address, will be null for invalid ranges (#REF! errors)
                    const range = (token as ReferenceToken).getRange3D(refAddress, targetAddress, refSheet, wrapReferences);
                    // check if the token contains a valid range (formula must not contain multi-sheet references)
                    if (!range?.singleSheet()) { return false; }

                    // append the valid range to the result
                    resultRanges.push(range);
                    tokenTags += "R";
                    return true;
                }

                case "name": {
                    // the model of the defined name referred by the passed token
                    const nameModel = resolveNames ? docModel.resolveNameModel(token as DefinedNameToken, refSheet) : undefined;
                    if (!nameModel) { return false; }

                    // prevent recursive invocation of this token array via other defined names
                    const ranges = this.#lockInterpreting(targetAddress, () => {
                        switch (resolveNames) {
                            case "simple":
                                // defined names are always defined relative to cell A1
                                return nameModel.tokenArray.resolveRangeList(Address.A1, targetAddress, allOptions);
                            case "interpret": {
                                // defined names are always defined relative to cell A1
                                const { value } = nameModel.tokenArray.interpretFormula("ref", Address.A1, targetAddress, allOptions);
                                return (value instanceof Range3DArray) ? value : null;
                            }
                            default:
                                return null;
                        }
                    });
                    if (!ranges || ranges.empty()) { return false; }

                    // append the new ranges to the result
                    resultRanges.append(ranges);
                    tokenTags += "R";
                    return true;
                }

                case "table": {
                    // try to resolve the table reference to a valid cell range address
                    const tableRange = resolveNames ? (token as TableToken).getRange3D(targetAddress) : null;
                    if (!tableRange) { return false; }

                    // append the valid range to the result
                    resultRanges.push(tableRange);
                    tokenTags += "R";
                    return true;
                }

                case "open":
                    tokenTags += "<";
                    return true;

                case "close":
                    tokenTags += ">";
                    return true;

                case "sep":
                    tokenTags += ",";
                    return true;

                case "ws":
                    return true;

                default:
                    return false;
            }
        });

        // reset resulting ranges if formula structure is invalid
        return (valid && /^(R(,R)*|<R(,R)*>)$/.test(tokenTags)) ? resultRanges : resultRanges.clear();
    }

    /**
     * Returns the addresses of all cell ranges this token array refers to.
     *
     * @param refAddress
     *  The source reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param targetAddress
     *  The target reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The addresses of all cell ranges contained in this token array with
     *  additional information.
     */
    extractRanges(refAddress: Address, targetAddress: Address, options?: ExtractRangesOptions): ExtractRangesResult {

        // descriptors for the resulting ranges, with the additional 'circular' property
        const circular = this.#isInterpreting(targetAddress);
        const rangesResult: ExtractRangesResult = Object.assign([], { circular });

        // prevent recursive processing of the token array in defined names
        if (circular) { return rangesResult; }

        // whether to resolve defined name tokens
        const resolveNames = options?.resolveNames;

        // early exit, if this token array does not contain any reference or name tokens
        const hasTokens = resolveNames ? this.hasAnyRefTokens() : this.hasToken("ref");
        if (!hasTokens) { return rangesResult; }

        // add more options (`refSheet` and `wrapReferences`)
        const allOptions = this.extendWithOwnOptions(options);
        const { refSheet, wrapReferences } = allOptions;

        // shortcut to the document model
        const { docModel } = this;

        // process all reference tokens and name tokens in this token array
        this.tokens.forEach((token, index) => {
            switch (token.type) {

                case "ref": {
                    const range = (token as ReferenceToken).getRange3D(refAddress, targetAddress, refSheet, wrapReferences); // returns null for invalid ranges (#REF! errors)
                    if (range) { rangesResult.push({ range, index, type: "ref" }); }
                    break;
                }

                case "name": {
                    // the model of the defined name referred by the token
                    const nameModel = resolveNames ? docModel.resolveNameModel(token as DefinedNameToken, refSheet) : undefined;
                    if (!nameModel) { return; }

                    // prevent recursive invocation of this token array via other defined names
                    const nameRangesResult = this.#lockInterpreting(targetAddress, () =>
                        // defined names are always defined relative to cell A1
                        nameModel.tokenArray.extractRanges(Address.A1, targetAddress, allOptions)
                    );

                    // insert the range descriptors with adjusted properties
                    nameRangesResult.forEach(rangeInfo => rangesResult.push({ range: rangeInfo.range, index, type: "name" }));

                    // update the own circular reference flag
                    rangesResult.circular = rangesResult.circular || nameRangesResult.circular;
                    break;
                }

                case "table": {
                    const tableRange = resolveNames ? (token as TableToken).getRange3D(targetAddress) : null;
                    if (tableRange) { rangesResult.push({ range: tableRange, index, type: "table" }); }
                    break;
                }

                default: break;
            }
        });

        return rangesResult;
    }

    // token manipulation -----------------------------------------------------

    /**
     * Parses the specified formula expression, creates an array of formula
     * tokens held by this instance, and emits a "change:tokens" event.
     *
     * @param grammarType
     *  The identifier of the formula grammar to be used to parse the passed
     *  formula expression.
     *
     * @param formula
     *  The formula expression to be parsed, without the leading equality sign.
     *
     * @param refAddress
     *  The address of the original reference cell the formula expression is
     *  related to. Will be used for example to resolve relative cell
     *  references in R1C1 notation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  An array of descriptors for all parsed formula tokens.
     */
    parseFormula(grammarType: GrammarType, formula: string, refAddress: Address, options?: ParseFormulaOptions): Array<Readonly<ParseToken>> {

        // add more options specific to this token array
        options = this.extendWithOwnOptions(options);

        // parse the formula string, extract all formula tokens, invoke change handlers
        const parser = this.docModel.getFormulaParser(grammarType);
        const parseTokens = parser.parse(this.docModel, formula, refAddress, options);
        const tokens = parseTokens.map(parseToken => parseToken.token);

        // add missing tokens that can be auto-corrected
        if (options.autoCorrect) {
            const compilerResult = this.#compileTokens(tokens);
            if (!compilerResult.error) {
                for (let index = 0; index < compilerResult.missingClose; index += 1) {
                    tokens.push(new ParenthesisToken(false));
                }
            }
        }

        // set the resulting tokens to this instance
        this.setTokens(tokens);

        // return the complete token descriptors
        return parseTokens;
    }

    /**
     * Tries to replace unresolved sheet names with existing sheet indexes in
     * the spreadsheet document, and to reparse all formulas with bad tokens.
     * Intended to be used after document import to refresh all token arrays
     * that refer to sheets that did not exist during their creation.
     *
     * @param refAddress
     *  The address of the original reference cell the formula expression is
     *  related to. Will be used for example to resolve relative cell
     *  references in R1C1 notation.
     */
    refreshAfterImport(refAddress: Address): void {

        // reparse formula if it contains a bad token, or name tokens (may become tables)
        if (this.hasToken("bad") || this.hasToken("name")) {
            this.parseFormula("op", this.getFormulaOp(), refAddress);
        } else if (this.hasAnySheetTokens()) {
            // otherwise refresh all sheet tokens
            this.tokens.forEach(token => token.refreshSheets());
            // the compiler tree may contain an 'unexpected' error due to missing sheets while importing
            this.#compilerResult = undefined;
            this.clearResultCache();
        }
    }

    /**
     * Updates all tokens with sheet references, and invokes the registered
     * change handlers, after a sheet has been inserted into, deleted from, or
     * moved in the document.
     *
     * @returns
     *  Whether any token in this token array has been changed.
     */
    transformSheets(xfVector: TransformSheetVector): boolean {

        // relocate all sheet tokens, and collect changed state (no early exit!!!)
        const changed = this.tokens.reduce((flag, token) => token.transformSheets(xfVector) || flag, false);

        // invoke all change handlers
        if (changed) { this.emit("change:tokens"); }
        return changed;
    }

    /**
     * Relocates the cell ranges in this token array to a new reference address
     * by adjusting the relative column and row components in all reference
     * tokens, and invokes the registered change handlers.
     *
     * @param refAddress
     *  The address of the original reference cell the formula is related to.
     *
     * @param targetAddress
     *  The address of the new reference cell the formula will be related to.
     *
     * @returns
     *  Whether any token in this token array has been changed.
     */
    relocateRanges(refAddress: Address, targetAddress: Address): boolean {

        // nothing to do, if the addresses are equal
        if (refAddress.equals(targetAddress)) { return false; }

        // relocate all reference tokens, and collect changed state (no early exit!!!)
        const { wrapReferences } = this;
        const changed = this.tokens.reduce((flag, token) => token.relocateRange(refAddress, targetAddress, wrapReferences) || flag, false);

        // invoke all change handlers
        if (changed) { this.emit("change:tokens"); }
        return changed;
    }

    // formula calculation ----------------------------------------------------

    /**
     * Compiles the formula tokens contained in this token array to a token
     * tree structure. See class `FormulaCompiler` for details.
     *
     * @returns
     *  The result descriptor for the compiled token tree.
     */
    compileFormula(): CompilerResult {
        return (this.#compilerResult ??= this.#compileTokens(this.tokens));
    }

    /**
     * Returns the dependencies of the formula represented by this token array.
     *
     * @param refAddress
     *  The source reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param depTarget
     *  The position of the target cells this formula is associated with. Can
     *  be a single cell address (e.g. for cell formulas), or a single cell
     *  range address, or an array of cell range addresses (e.g. the target
     *  ranges of a formatting rule).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A descriptor object with the dependency settings of this formula.
     */
    getDependencies(refAddress: Address, depTarget: DependencyTarget, options?: RefSheetOptions): FormulaDependencies {

        // the result descriptor object
        const deps = new FormulaDependencies();

        // compile the infix tokens to a token tree (early exit, if this token array is invalid)
        const compilerResult = this.compileFormula();
        if (compilerResult.error) { return deps; }

        // a single target address for cell formulas (and others, e.g. formatting rules in range mode); otherwise null (!)
        const targetAddress = (depTarget instanceof Address) ? depTarget : null;

        // prevent recursive processing of the token array, e.g. in defined name
        if (this.#isInterpreting(targetAddress ?? refAddress)) {
            deps.circular = true;
            return deps;
        }

        // insert the resource keys of all missing functions
        set.assign(deps.missingFuncs, compilerResult.missingFuncs);
        // insert the recalculation mode of the root node into the result
        deps.recalc = compilerResult.root.recalc;

        // add more options (refSheet and wrapReferences)
        const allOptions = this.extendWithOwnOptions(options);
        const { refSheet, wrapReferences } = allOptions;

        // array of target ranges for other formulas, e.g. formatting rules
        const targetRanges = targetAddress ? null : RangeArray.cast(depTarget as RangeSource);

        // collect all referecnes, names, and tables (used recursively)
        const collectReferences = (node: CompilerNode): Range3DArray | null => {

            // the formula token represented by the compiler node
            const { token, operands, signature } = node;

            // reference token: return the resolved range as token references
            if (token instanceof ReferenceToken) {
                if (targetAddress) {
                    const range3d = token.getRange3D(refAddress, targetAddress, refSheet, wrapReferences);
                    // returns null for invalid references (#REF! errors)
                    return range3d ? new Range3DArray(range3d) : null;
                }
                return token.getExpandedRanges(refAddress, targetRanges, refSheet, wrapReferences);
            }

            // name token: resolve the source references of the defined name
            if (token instanceof DefinedNameToken) {

                // the model of the defined name referred by the token
                const nameModel = this.docModel.resolveNameModel(token, refSheet);

                // if the token cannot be resolved to a name, register the label used in the formula
                if (!nameModel) {
                    deps.missingNames.add(mapKey(token.value));
                    return null;
                }

                // prevent recursive invocation of this token array via other defined names
                const nameDeps = this.#lockInterpreting(targetAddress ?? refAddress, () =>
                    // defined names will always be evaluated relative to cell A1
                    nameModel.tokenArray.getDependencies(Address.A1, depTarget, allOptions)
                );

                // add all dependencies of the defined name to the own result
                deps.mergeNameDeps(nameModel, nameDeps);

                // return the root ranges of the defined name to caller (parent operator)
                return nameDeps.rootRanges;
            }

            // table token: resolve the target range inside the table
            if (token instanceof TableToken) {
                const tableModel = this.docModel.resolveTableModel(token);
                if (!tableModel) { return null; }
                deps.tableMap.set(tableModel.key, tableModel);
                const tableRange = token.getRange3D(targetAddress ?? refAddress);
                return tableRange ? new Range3DArray(tableRange) : null;
            }

            // nothing more to do for operands, or unsupported functions
            if (!operands || !signature) { return null; }

            // operators or supported functions: resolve referencees of the operands
            const rootRanges = new Range3DArray();
            signature.forEach((paramSig, index) => {
                const operandRanges = collectReferences(operands[index]);
                switch (paramSig.depSpec) {
                    case "resolve":
                        deps.depRanges.append(operandRanges);
                        break;
                    case "pass":
                        rootRanges.append(operandRanges);
                        break;
                    case "skip":
                        break;
                }
            });
            return rootRanges;
        };

        // traverse the compiler tree, collect references from the formula, and from referred defined names
        const rootRanges = collectReferences(compilerResult.root);

        // add the root ranges of the root node to the result
        deps.rootRanges.append(rootRanges);

        return deps;
    }

    /**
     * Calculates the result of the formula represented by this token array.
     *
     * @param contextType
     *  The formula context type influencing the type of the formula result.
     *
     * @param refAddress
     *  The source reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param targetAddress
     *  The target reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The formula result object returned from the formula interpreter.
     */
    interpretFormula(contextType: ContextType, refAddress: Address, targetAddress: Address, options?: InterpretFormulaOptions): InterpreterResult {

        // compile the infix tokens to a token tree
        const compileDesc = this.compileFormula();
        // the formula interpreter
        const { formulaInterpreter } = this.docModel;

        // check compiler result
        if (compileDesc.error) {
            return formulaInterpreter.createErrorResult(contextType, compileDesc.error);
        }

        // prevent recursive interpretation
        if (this.#isInterpreting(targetAddress)) {
            return formulaInterpreter.createWarnResult(contextType, InternalErrorCode.CIRCULAR);
        }

        // extend the passed options with more internal options
        const allOptions = this.extendWithOwnOptions(options);

        // interpret the formula with extended options
        return this.#lockInterpreting(targetAddress, () => {

            // invokes the formula interpreter to calculate the actual result of the formula
            function interpretTokens(): InterpreterResult {
                return formulaInterpreter.interpretTokens(contextType, compileDesc.root, refAddress, targetAddress, allOptions);
            }

            // do not cache results of volatile formulas
            const cacheActive = allOptions.cacheResult && (compileDesc.root.recalc !== "always");
            if (!cacheActive) { return interpretTokens(); }

            // Create the cache key. Depending on the existence of relative column or row references
            // in the token array, the cache can be optimized according to the reference and target
            // address. Without relative column (or row) references, the column A (or the row 1
            // respectively) will be used as cache key for all reference and target addresses.
            //
            // Example: The formula expression =SUM($C1:$E4) will always be evaluated for columns C:E
            // regardless of the reference and target addresses. Therefore, the formula result will
            // be cached with column A as cache key, in order to resolve the cached result for any
            // reference and target address.

            // defined names may always contain relative column/row references
            const hasNames = this.hasToken("name");

            // if the formula does not contain relative column references, store all results at column A
            const relColRef = hasNames || compileDesc.root.relColRef;
            const refCol = relColRef ? refAddress.c : 0;
            const targetCol = relColRef ? targetAddress.c : 0;

            // if the formula does not contain relative row references, store all results at row 1
            const relRowRef = hasNames || compileDesc.root.relRowRef;
            const refRow = relRowRef ? refAddress.r : 0;
            const targetRow = relRowRef ? targetAddress.r : 0;

            // create the cache key from reference and target address
            const cacheKey = rangeKeyFromIndexes(refCol, refRow, targetCol, targetRow);

            // get or create the result cache
            const resultCache = this.#resultCache ??= new Map<string, InterpreterResult>();
            return map.upsert(resultCache, cacheKey, interpretTokens);
        });
    }

    /**
     * Clears all cached formula results. Formula results will be cached when
     * passing the option `cacheResult` to the method `interpretFormula()` of
     * this class.
     */
    clearResultCache(): void {
        this.#resultCache = undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Compiles the passed formula tokens on-the-fly, without modifying the
     * state of this instance. See class FormulaCompiler for details.
     *
     * @param tokens
     *  The array of formula tokens to be compiled, in infix notation.
     *
     * @returns
     *  The result descriptor of the compilation process. See description of
     *  the method FormulaCompiler.compileTokens() for details.
     */
    #compileTokens(tokens: readonly BaseToken[]): CompilerResult {
        return this.docModel.formulaCompiler.compileTokens(tokens);
    }

    /**
     * Returns whether this token array is currently being interpreted for the
     * specified target address.
     *
     * @param targetAddress
     *  The address of the target cell.
     *
     * @returns
     *  Whether this token array is currently being interpreted for the
     *  specified target address.
     */
    #isInterpreting(targetAddress: Address): boolean {
        return !!this.#interpreting?.has(targetAddress.key);
    }

    /**
     * Sets the 'interpreting' state for the specified target address, invokes
     * the specified callback function, and removes the 'interpreting' state
     * afterwards.
     *
     * @param targetAddress
     *  The address of the target cell to be locked for interpreting state.
     *
     * @param callback
     *  The callback function to be invoked with locked interpreting state.
     *
     * @returns
     *  The return value of the callback function.
     */
    #lockInterpreting<T>(targetAddress: Address, callback: FuncType<T>): T {
        const lockSet = this.#interpreting ??= new Set();
        try {
            lockSet.add(targetAddress.key);
            return callback();
        } finally {
            lockSet.delete(targetAddress.key);
        }
    }
}
