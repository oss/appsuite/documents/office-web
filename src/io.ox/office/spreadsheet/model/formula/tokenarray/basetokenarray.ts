/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { EventHub } from "@/io.ox/office/tk/events";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { FormulaType, RefSheetOptions, WrapReferencesOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import { SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { TokenType, BaseToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import { SeparatorToken, SheetRefToken, ReferenceToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";

// types ======================================================================

/**
 * Type mapping for the events emitted by `BaseTokenArray` instances.
 */
export interface TokenArrayEventMap {

    /**
     * Will be emitted if a single token in a token array has been changed.
     *
     * @param token
     *  The changed formula token.
     *
     * @param index
     *  The array index of the formula token.
     */
    "change:token": [token: BaseToken, index: number];

    /**
     * Will be emitted if multiple tokens in a token array have been changed,
     * e.g. after parsing a formula expression, or clearing the token array.
     */
    "change:tokens": [];
}

/**
 * Optional parameters for appending a cell reference token to a token array.
 */
export interface AppendReferenceOptions {

    /**
     * If set to `true`, the column and row parts will be marked as absolute
     * (leading dollar signs). Default value is `false`.
     */
    abs?: boolean;

    /**
     * The index of the sheet the reference token will point to. Can also be a
     * custom sheet name. If omitted, no sheet reference will be inserted into
     * the reference token (sheet-local reference).
     */
    sheet?: number | string;

    /**
     * If set to `true`, the ODF sheet reference will be made relative (e.g.
     * `Sheet1.A1` instead of `$Sheet1.A1`). Default value is `false`.
     */
    relSheet?: boolean;
}

// private types --------------------------------------------------------------

interface ExtendedOptions {
    refSheet: OptSheet;
    wrapReferences: boolean;
    detectCircular: boolean;
    unqualifiedTables: boolean;
}

// constants ==================================================================

/**
 * Special key for token type sets specifying whether a token array contains
 * tokens with sheet references.
 */
const SHEET_REF_KEY = Symbol("anysheetref");

// private functions ==========================================================

function createSheetRefs(options?: AppendReferenceOptions): Opt<SheetRefs> {
    const sheet = options?.sheet;
    return (is.number(sheet) || is.string(sheet)) ? SheetRefs.single(sheet, !options?.relSheet) : undefined;
}

// class BaseTokenArray =======================================================

/**
 * Helper base class for formula token arrays with fundamental formula parsing
 * capabilities using an arbitrary `IDocumentAccess` implementation object
 * instead of a full document model instance. Intended to be used with other
 * document access objects needing to parse formulas independent from the
 * document model, for example the `OTManager`.
 *
 * @param docModel
 *  The document access interface.
 *
 * @param formulaType
 *  The type of the token array. May cause special behavior in specific
 *  situations.
 */
export abstract class BaseTokenArray<DocAccessT extends IDocumentAccess = IDocumentAccess> extends EventHub<TokenArrayEventMap> {

    readonly docModel: DocAccessT;
    readonly formulaType: FormulaType;

    /** The array of all formula tokens. */
    #tokens = new Array<BaseToken>();
    /** Flag set with token type identifiers for fast existence check. */
    readonly #typeSet = new Set<TokenType | symbol>();

    // constructor ------------------------------------------------------------

    protected constructor(docModel: DocAccessT, formulaType: FormulaType) {
        super();
        this.docModel = docModel;
        this.formulaType = formulaType;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns the raw array of formula tokens contained by this instance.
     */
    get tokens(): readonly BaseToken[] {
        return this.#tokens;
    }

    /**
     * Returns whether this token array represents a cell type formula (simple
     * cell formula, shared formula, or matrix formula).
     */
    get isCellFormula(): boolean {
        return this.formulaType === "cell";
    }

    /**
     * Returns whether to wrap relocated range addresses at the sheet borders.
     */
    get wrapReferences(): boolean {
        return this.formulaType === "name";
    }

    // abstract methods -------------------------------------------------------

    /**
     * Returns the index of the sheet that contains this token array.
     *
     * @returns
     *  The index of the sheet that contains this token array; or `null`, if
     *  this is a global token array with the document model as parent.
     */
    abstract getRefSheet(): OptSheet;

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this token array is empty.
     *
     * @returns
     *  Whether this token array is empty.
     */
    empty(): boolean {
        return !this.#tokens.length;
    }

    /**
     * Returns whether this token array contains at least one token of any of
     * the specified token types.
     *
     * @param types
     *  The token type identifiers to be checked.
     *
     * @returns
     *  Whether this token array contains at least one token of any of the
     *  specified token types.
     */
    hasToken(...types: TokenType[]): boolean {
        return types.some(type => this.#typeSet.has(type));
    }

    /**
     * Returns whether this token array contains any reference tokens, or any
     * tokens referring to defined names or table ranges.
     *
     * @returns
     *  Whether this token array contains any reference tokens, or any tokens
     *  referring to defined names or table ranges.
     */
    hasAnyRefTokens(): boolean {
        return this.hasToken("ref", "name", "table");
    }

    /**
     * Returns whether this token array contains any tokens with existing sheet
     * reference (references such as `Sheet1!A1`, or sheet-local defined names
     * such as `Sheet1!name`).
     *
     * @returns
     *  Whether this token array contains any any tokens with existing sheet
     *  reference.
     */
    hasAnySheetTokens(): boolean {
        return this.#typeSet.has(SHEET_REF_KEY);
    }

    /**
     * Inserts the passed formula tokens into this token array.
     *
     * @param tokens
     *  The new formula tokens to be inserted into this token array.
     *
     * @returns
     *  A reference to this instance.
     */
    setTokens(tokens: readonly BaseToken[]): this {

        // early exit, if an empty token array will be cleared
        if (this.empty() && (tokens.length === 0)) { return this; }

        // collect all existing token types in lists, and in a flag set
        this.#tokens = Array.from(tokens);
        this.#typeSet.clear();
        for (const token of this.#tokens) { this.#registerToken(token); }

        // notify change handlers
        this.emit("change:tokens");
        return this;
    }

    /**
     * Removes all formula tokens from this token array.
     *
     * @returns
     *  A reference to this instance.
     */
    clearTokens(): this {
        return this.setTokens([]);
    }

    /**
     * Invokes a callback function used to modify the specified token in this
     * token array. Afterwards, emits a "change:token" event with the changed
     * token.
     *
     * @param index
     *  The array index of the token to be modified.
     *
     * @param callback
     *  The callback function invoked for the specified token. Must return a
     *  boolean value specifying whether the token has been modified. If the
     *  callback function returns `true`, the registered change handlers will
     *  be invoked afterwards.
     *
     * @returns
     *  A reference to this instance.
     */
    modifyToken(index: number, callback: (token: BaseToken) => boolean): this {

        // the token to be modified
        const token = this.#tokens[index];

        // invoke the callback, notify changed token if callback returns true
        if (token && callback(token)) {
            this.emit("change:token", token, index);
        }

        return this;
    }

    /**
     * Appends the passed formula token to the token array, and emits a
     * "change:token" event with the new token.
     *
     * @param token
     *  The new formula token. This token array will take ownership.
     *
     * @returns
     *  A reference to this instance.
     */
    appendToken(token: BaseToken): this {
        this.#tokens.push(token);
        this.#registerToken(token);
        this.emit("change:token", token, this.#tokens.length - 1);
        return this;
    }

    /**
     * Appends a new reference token to this token array, that will be built
     * from the passed cell address, and invokes the registered change handlers
     * for the new token.
     *
     * @param address
     *  The address of the cell to be added as reference token to this token
     *  array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    appendAddress(address: Address, options?: AppendReferenceOptions): this {

        // the cell and sheet references
        const abs = !!options?.abs;
        const cellRef = CellRef.create(address, abs, abs);
        const sheetRefs = createSheetRefs(options);

        // create and append the reference token
        return this.appendToken(new ReferenceToken(this.docModel, cellRef, sheetRefs));
    }

    /**
     * Appends a new reference token to this token array, that will be built
     * from the passed cell range address, and invokes the registered change
     * handlers for the new token.
     *
     * @param range
     *  The cell range address to be added as reference token to this token
     *  array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    appendRange(range: Range, options?: AppendReferenceOptions): this {

        // whether the range spans entire columns or rows
        const colRange = this.docModel.addressFactory.isColRange(range);
        const rowRange = this.docModel.addressFactory.isRowRange(range);

        // the cell and sheet references
        const abs = options?.abs;
        const r1 = CellRef.create(range.a1, abs || rowRange, abs || (colRange && !rowRange));
        const r2 = range.single() ? undefined : CellRef.create(range.a2, abs || rowRange, abs || (colRange && !rowRange));
        const sheetRefs = createSheetRefs(options);

        // create and append the reference token
        return this.appendToken(new ReferenceToken(this.docModel, { r1, r2 }, sheetRefs));
    }

    /**
     * Appends new reference tokens to this token array, that will be built
     * from the passed cell range list, and invokes the registered change
     * handlers for the new tokens. If the passed range array contains more
     * than one range address, the reference tokens will be separated by list
     * operator tokens.
     *
     * @param ranges
     *  The cell range addresses to be added as reference tokens to this token
     *  array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    appendRangeList(ranges: Range[], options?: AppendReferenceOptions): this {

        for (const [index, range] of ranges.entries()) {
            if (index) { this.appendToken(new SeparatorToken()); }
            this.appendRange(range, options);
        }

        return this;
    }

    // protected methods ------------------------------------------------------

    /**
     * Extends the passed map of optional parameters received by various public
     * methods of this instance, according to the type of the token array. Adds
     * the boolean options `wrapReferences`, `detectCircular`, and
     * `unqualifiedTables`; and inserts the own reference sheet index as option
     * `refSheet` if available.
     *
     * @param [options]
     *  The optional parameters received by a public method.
     *
     * @returns
     *  The extended optional parameters.
     */
    protected extendWithOwnOptions<T extends RefSheetOptions & WrapReferencesOptions>(options?: T): T & ExtendedOptions {

        // the reference sheet (prefer own sheet index, otherwise use passed reference sheet)
        const refSheet = this.getRefSheet() ?? options?.refSheet ?? null;

        return {
            wrapReferences: this.wrapReferences,
            ...options,
            refSheet,                              // overwrite "refSheet" passed in options
            detectCircular: this.isCellFormula,    // detect circular references only in cell and matrix formulas
            unqualifiedTables: this.isCellFormula  // unqualified tables only in cell and matrix formulas
        } as unknown as T & ExtendedOptions;
    }

    // private methods --------------------------------------------------------

    /**
     * Adds a new token inserted into this token array in the various internal
     * data structures used for caching and to optimize performance of various
     * public methods.
     *
     * @param token
     *  The token to be registered in the internal data structures.
     */
    #registerToken(token: BaseToken): void {
        this.#typeSet.add(token.type);
        if (token instanceof SheetRefToken) {
            this.#typeSet.add(SHEET_REF_KEY);
        }
    }
}
