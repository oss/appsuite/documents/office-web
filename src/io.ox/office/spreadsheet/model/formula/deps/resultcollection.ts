/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";

import { getCellValue } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import { depsLogger } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { type FormulaDescriptor, CellFormulaDescriptor, ModelFormulaDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladescriptor";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * Represents a scalar cell formula result calculated by a dependency manager.
 */
export interface ResultDescriptor extends Keyable {

    /**
     * The cell address of the formula result.
     */
    address: Address;

    /**
     * The scalar formula result.
     */
    value: ScalarType;

    /**
     * The timestamp when the formula has been calculated.
     */
    time: number;
}

/**
 * The address, sheet index, and sheet UID of a cell address that is part of a
 * reference cycle.
 */
export interface ReferenceCycleEntry {

    /**
     * The cell address.
     */
    address: Address;

    /**
     * The UID of the sheet model.
     */
    sheetUid: string;

    /**
     * The index of the sheet.
     */
    sheet: number;
}

export type ReferenceCycle = ReferenceCycleEntry[];

// class ResultSet ============================================================

/**
 * A set of cached formula results, mapped by cell address key.
 */
export class ResultSet extends KeySet<ResultDescriptor> {

    // public methods ---------------------------------------------------------

    /**
     * Inserts a new formula result into this set.
     *
     * @param address
     *  The cell address of the formula result.
     *
     * @param value
     *  The scalar formula result.
     *
     * @param time
     *  The timestamp when the formula has been calculated.
     *
     * @returns
     *  The result descriptor inserted into this set.
     */
    insert(address: Address, value: ScalarType, time: number): ResultDescriptor {
        value = getCellValue(value);
        const resultDesc: ResultDescriptor = { address, key: address.key, value, time };
        this.add(resultDesc);
        return resultDesc;
    }
}

// class ResultCollection =====================================================

/**
 * Encapsulates all data that will be collected during a single update cycle of
 * a dependency manager.
 */
export class ResultCollection {

    /**
     * The descriptors of all dirty formulas collected by an update cycle of a
     * dependency manager. This set contains all formula descriptors contained
     * in the properties "cellFormulaSet" and "modelFormulasMap".
     */
    readonly formulaSet = new KeySet<FormulaDescriptor>();

    /**
     * The descriptors of all dirty cell formulas collected by an update cycle
     * of a dependency manager. All formulas in this map will be calculated
     * while the calculation of another formula looks up the cell contents of
     * that dirty formulas. After calculation of a formula has finished, the
     * formula result will be stored in the property "resultsMap", and will be
     * returned when looking up the cell value the next time.
     */
    readonly cellFormulaSet = new KeySet<CellFormulaDescriptor>();

    /**
     * The descriptors of all dirty formulas contained in other model objects
     * collected by an update cycle of a dependency manager, mapped as sub maps
     * by sheet model.
     */
    readonly modelFormulasMap = new Map<SheetModel, KeySet<ModelFormulaDescriptor>>();

    /**
     * The results of all dirty formulas (normal formulas, shared formulas, and
     * matrix formulas) that have been calculated, as sets of objects mapped by
     * sheet model.
     */
    readonly resultsMap = new Map<SheetModel, ResultSet>();

    /**
     * All reference cycles found during calculation of formulas.
     */
    readonly referenceCycles: ReferenceCycle[] = [];

    /**
     * Whether any of the formulas could not be calculated correctly, e.g. due
     * to unimplemented functions.
     */
    hasErrors = false;

    // public methods ---------------------------------------------------------

    /**
     * Inserts a new formula descriptor into the internal collections of this
     * instance.
     *
     * @param formula
     *  The descriptor for the new formula to be registered.
     */
    insertFormula(formula: FormulaDescriptor): void {
        this.formulaSet.add(formula);
        if (formula instanceof CellFormulaDescriptor) {
            this.cellFormulaSet.add(formula);
            this.resultsMap.get(formula.sheetModel)?.delete(formula.cellAddress);
        } else if (formula instanceof ModelFormulaDescriptor) {
            map.upsert(this.modelFormulasMap, formula.sheetModel, () => new KeySet()).add(formula);
        } else {
            depsLogger.error("$badge{ResultCollection} insertFormula: unknown formula type");
        }
    }

    /**
     * Removes the formula descriptor from the internal collections of this
     * instance.
     *
     * @param formula
     *  The descriptor for the formula to be unregistered.
     */
    deleteFormula(formula: FormulaDescriptor): void {
        this.formulaSet.delete(formula);
        if (formula instanceof CellFormulaDescriptor) {
            this.cellFormulaSet.delete(formula);
        } else if (formula instanceof ModelFormulaDescriptor) {
            this.modelFormulasMap.get(formula.sheetModel)?.delete(formula);
        } else {
            depsLogger.error("$badge{ResultCollection} insertFormula: unknown formula type");
        }
    }

    /**
     * Returns an existing result set, or creates a new result set on demand.
     *
     * @param sheetModel
     *  The sheet model containing the formula results.
     *
     * @returns
     *  The result set for the specified sheet.
     */
    createResultSet(sheetModel: SheetModel): ResultSet {
        return map.upsert(this.resultsMap, sheetModel, () => new ResultSet());
    }

    /**
     * Returns an existing result for the specified cell.
     *
     * @param sheetModel
     *  The sheet model containing the formula result.
     *
     * @param address
     *  The address of the cell.
     *
     * @returns
     *  An existing result descriptor, otherwise `undefined`.
     */
    getResult(sheetModel: SheetModel, address: Address): Opt<ResultDescriptor> {
        return this.resultsMap.get(sheetModel)?.at(address);
    }
}
