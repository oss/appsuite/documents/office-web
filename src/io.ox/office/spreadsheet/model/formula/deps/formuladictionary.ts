/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { set, map } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";

import { getRangeKey } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { type FormulaDescriptor, CellFormulaDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladescriptor";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// class FormulaDictionary ====================================================

/**
 * A container for all registered formula descriptors in a spreadsheet
 * document. The descriptors will be mapped by their source references, in
 * order to provide efficient methods for looking up formulas depending on
 * changed cells, or changed ancestor formulas in a dependency chain.
 */
export class FormulaDictionary implements Cloneable<FormulaDictionary> {

    // the document model
    readonly #docModel: SpreadsheetModel;
    // cell range sets, mapped by sheet model (needed to find formulas by address or overlapping range)
    readonly #rangeSets = new Map<SheetModel, RangeSet>();
    // sets of formula descriptors depending on a cell range, mapped by 3D range key
    readonly #rangeFmlaMap = new Map<string, KeySet<FormulaDescriptor>>();
    // sets of formula descriptors depending on a defined name, mapped by name key
    readonly #nameFmlaMap = new Map<string, KeySet<FormulaDescriptor>>();
    // sets of formula descriptors depending on a table range, mapped by table key
    readonly #tableFmlaMap = new Map<string, KeySet<FormulaDescriptor>>();

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        this.#docModel = docModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this dictionary is completely empty.
     *
     * @returns
     *  Whether this dictionary is completely empty.
     */
    empty(): boolean {
        return !this.#rangeSets.size && !this.#rangeFmlaMap.size && !this.#nameFmlaMap.size && !this.#tableFmlaMap.size;
    }

    /**
     * Clears this dictionary completely.
     */
    clear(): void {
        this.#rangeSets.clear();
        this.#rangeFmlaMap.clear();
        this.#nameFmlaMap.clear();
        this.#tableFmlaMap.clear();
    }

    /**
     * Creates a clone of this formula dictionary, referring to the same
     * formula descriptors as this dictionary.
     *
     * @returns
     *  A clone of this formula dictionary.
     */
    clone(): FormulaDictionary {

        // start with an empty dictionary
        const clone = new FormulaDictionary(this.#docModel);

        // clone the formula maps of the cell range sets
        for (const [sheetModel, rangeSet] of this.#rangeSets) {
            clone.#rangeSets.set(sheetModel, RangeSet.from(rangeSet, range => range.clone()));
        }

        // clone the formula descriptors mapped by range keys
        for (const [key, formulas] of this.#rangeFmlaMap) {
            clone.#rangeFmlaMap.set(key, new KeySet(formulas));
        }

        // clone the formula descriptors mapped by name keys
        for (const [key, formulas] of this.#nameFmlaMap) {
            clone.#nameFmlaMap.set(key, new KeySet(formulas));
        }

        // clone the formula descriptors mapped by table keys
        for (const [key, formulas] of this.#tableFmlaMap) {
            clone.#tableFmlaMap.set(key, new KeySet(formulas));
        }

        return clone;
    }

    /**
     * Inserts a new formula descriptor into this dictionary. The formula
     * descriptor will be mapped by all of its source references.
     *
     * @param formula
     *  The descriptor of a formula to be inserted into this dictionary.
     */
    insertFormula(formula: FormulaDescriptor): void {

        // insert the formula descriptor into the formula maps for all source ranges
        for (const [sheetModel, ranges] of formula.references.rangesMap) {
            for (const range of ranges) {
                map.upsert(this.#rangeSets, sheetModel, () => new RangeSet()).add(range);
                map.upsert(this.#rangeFmlaMap, getRangeKey(sheetModel, range), () => new KeySet()).add(formula);
            }
        }

        // insert the formula descriptor into the formula maps for all defined names
        for (const nameKey of formula.references.nameMap.keys()) {
            map.upsert(this.#nameFmlaMap, nameKey, () => new KeySet()).add(formula);
        }

        // insert the formula descriptor into the formula maps for all table ranges
        for (const tableKey of formula.references.tableMap.keys()) {
            map.upsert(this.#tableFmlaMap, tableKey, () => new KeySet()).add(formula);
        }
    }

    /**
     * Removes a formula descriptor from this dictionary.
     *
     * @param formula
     *  The descriptor of a formula to be removed form this dictionary.
     */
    removeFormula(formula: FormulaDescriptor): void {

        // remove the formula descriptor from the formula maps for all source ranges
        for (const [sheetModel, ranges] of formula.references.rangesMap) {
            for (const range of ranges) {
                const rangeKey = getRangeKey(sheetModel, range);
                map.visit(this.#rangeFmlaMap, rangeKey, formulas => {
                    formulas.delete(formula);
                    if (!formulas.size) {
                        this.#rangeFmlaMap.delete(rangeKey);
                        this.#rangeSets.get(sheetModel)?.delete(range);
                    }
                });
            }
        }

        // remove the formula descriptor from the formula maps for all defined names
        for (const nameKey of formula.references.nameMap.keys()) {
            map.visit(this.#nameFmlaMap, nameKey, formulas => {
                formulas.delete(formula);
                if (!formulas.size) { this.#nameFmlaMap.delete(nameKey); }
            });
        }

        // remove the formula descriptor from the formula maps for all table ranges
        for (const tableKey of formula.references.tableMap.keys()) {
            map.visit(this.#tableFmlaMap, tableKey, formulas => {
                formulas.delete(formula);
                if (!formulas.size) { this.#tableFmlaMap.delete(tableKey); }
            });
        }
    }

    /**
     * Returns the descriptors of all formulas that depend directly on the
     * specified cell address.
     *
     * @param sheetModel
     *  The sheet model containing the cell with the passed address.
     *
     * @param address
     *  The address of the cell to look up formula descriptors for.
     *
     * @returns
     *  All formula descriptors depending on the specified cell address.
     */
    findFormulasForAddress(sheetModel: SheetModel, address: Address): KeySet<FormulaDescriptor> {

        // the collected formula descriptors
        const formulas = new KeySet<FormulaDescriptor>();

        // collect formula descriptors depending on all cell ranges containing the address
        const rangeSet = this.#rangeSets.get(sheetModel);
        if (rangeSet) {
            for (const srcRange of rangeSet.yieldByAddress(address)) {
                this.#mergeRangeFormulas(formulas, sheetModel, srcRange);
            }
        }

        return formulas;
    }

    /**
     * Returns the descriptors of all formulas that depend directly on any cell
     * in the specified cell range address.
     *
     * @param sheetModel
     *  The sheet model containing the cell range with the passed address.
     *
     * @param range
     *  The address of the cell range to look up formula descriptors for.
     *
     * @returns
     *  All formula descriptors depending on the specified cell range address.
     */
    findFormulasForRange(sheetModel: SheetModel, range: Range): KeySet<FormulaDescriptor> {

        // the collected formula descriptors
        const formulas = new KeySet<FormulaDescriptor>();

        // collect formula descriptors depending on all cell ranges overlapping with the range
        const rangeSet = this.#rangeSets.get(sheetModel);
        if (rangeSet) {
            for (const srcRange of rangeSet.yieldMatching(range)) {
                this.#mergeRangeFormulas(formulas, sheetModel, srcRange);
            }
        }

        return formulas;
    }

    /**
     * Returns the descriptors of all formulas that depend directly on the
     * formula represented by the specified formula descriptor.
     *
     * @param formula
     *  The descriptor of a formula to look up formula descriptors for.
     *
     * @returns
     *  All formula descriptors depending on the specified formula; or
     *  `undefined`, if no other formula depends on the formula.
     */
    findFormulasForFormula(formula: FormulaDescriptor): Opt<KeySet<FormulaDescriptor>> {

        // formulas can depend on cell formulas only (including shared formulas and matrix formulas)
        if (!(formula instanceof CellFormulaDescriptor)) { return undefined; }

        // find all formulas depending on any cell covered by a matrix formula
        if (formula.matrixRange) {
            return this.findFormulasForRange(formula.sheetModel, formula.matrixRange);
        }

        // find all formulas depending on the cell represented by the passed formula descriptor
        return this.findFormulasForAddress(formula.sheetModel, formula.cellAddress);
    }

    /**
     * Returns the descriptors of all formulas that depend directly on the
     * defined name with the specified key.
     *
     * @param nameKey
     *  The key of the defined name to look up formula descriptors for.
     *
     * @returns
     *  All formula descriptors depending on the specified defined name; or
     *  `undefined`, if no other formula depends on the defined name.
     */
    findFormulasForName(nameKey: string): Opt<KeySet<FormulaDescriptor>> {
        return this.#nameFmlaMap.get(nameKey);
    }

    /**
     * Returns the descriptors of all formulas that depend directly on the
     * table range with the specified key.
     *
     * @param tableKey
     *  The key of the table range to look up formula descriptors for.
     *
     * @returns
     *  All formula descriptors depending on the specified table range; or
     *  `undefined`, if no other formula depends on the table range.
     */
    findFormulasForTable(tableKey: string): Opt<KeySet<FormulaDescriptor>> {
        return this.#tableFmlaMap.get(tableKey);
    }

    // private methods --------------------------------------------------------

    /**
     * Merges all formula descriptors registered for the passed cell range
     * address into the formula set.
     */
    #mergeRangeFormulas(formulas: KeySet<FormulaDescriptor>, sheetModel: SheetModel, range: Range): void {
        set.assign(formulas, this.#rangeFmlaMap.get(getRangeKey(sheetModel, range)));
    }
}
