/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { MAX_FORMULA_COUNT } from "@/io.ox/office/spreadsheet/utils/config";
import { type ScalarType, ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";

import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { ContextType } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { ScalarMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import type { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

import type { TokenModel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { depsLogger, getCellKey, getAddressLabel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import type { FormulaDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladescriptor";
import { SheetDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/sheetdescriptor";
import { FormulaDictionary } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladictionary";
import { PendingCollection } from "@/io.ox/office/spreadsheet/model/formula/deps/pendingcollection";
import type { ReferenceCycleEntry, ReferenceCycle, ResultDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/resultcollection";
import { ResultCollection } from "@/io.ox/office/spreadsheet/model/formula/deps/resultcollection";

// types ======================================================================

/**
 * A descriptor object with information about a cell value resolved from a
 * dependency manager.
 */
export interface ValueDescriptor {

    /**
     * The resolved scalar value of the specified cell.
     */
    value: ScalarType;

    /**
     * How the cell value has been resolved.
     *
     * - If set to "calculated", the value has just been calculated by
     *   interpreting the dirty cell formula.
     * - If set to "cached", the result has already been calculated by a
     *   preceding call of this method during the update cycle currently
     *   running.
     * - If set to "fixed", the value has been returned directly from the cell
     *   model (either because the cell is not a formula cell, or the formula
     *   was not dirty).
     */
    type: "calculated" | "cached" | "fixed";

    /**
     * The time in milliseconds it took to calculate the result of a dirty
     * formula cell (type "calculated"). Will be zero for all other value types
     * ("cached" and "fixed").
     */
    time: number;
}

/**
 * Type mapping for the events emitted by `DependencyStore` instances.
 */
export interface DependencyStoreEventMap {

    /**
     * Will be emitted if the number of registered formulas exceeds a specific
     * limit in the server configuration. After that, the dependency manager
     * will not process any formulas in the document anymore.
     */
    "recalc:overflow": [];
}

// class DependencyStore ======================================================

/**
 * The complete storage and state of a dependency manager instance. This object
 * will be shared with all asynchronous workers used by the dependency manager.
 */
export class DependencyStore extends ModelObject<SpreadsheetModel, DependencyStoreEventMap> {

    /**
     * Stores the descriptors of all registered formulas in the document.
     *
     * The formula descriptors will be mapped by their unique formula keys.
     */
    readonly formulaSet = new KeySet<FormulaDescriptor>();

    /**
     * Stores a sheet descriptor object per existing sheet in the document.
     *
     * The sheet descriptors will be mapped by their sheet model.
     */
    readonly sheetDescMap = new Map<SheetModel, SheetDescriptor>();

    /**
     * A dictionary for all formula descriptors of this dependency manager.
     *
     * This is the central storage of this dependency manager that will be used
     * to find formulas depending on specific dirty cells, or other changed
     * document contents, in a very efficient way.
     */
    readonly formulaDictionary: FormulaDictionary;

    /**
     * Collected data for all document change events, and other information
     * needed to decide which formulas need to be recalculated in the next
     * update cycle.
     */
    pendingCollection: PendingCollection;

    /**
     * All dirty formula descriptors collected by an update cycle of this
     * dependency manager, and the formula results already calculated.
     */
    resultCollection: ResultCollection;

    /**
     * Specifies whether the dependency manager did not finish its initial
     * update cycle after importing the document yet. Will be `false` after the
     * first update cycle has finished_successfully_.
     */
    initialUpdateCycle = true;

    /**
     * Specifies whether all formulas need to be recalculated when the document
     * will be opened the next time.
     */
    calcOnLoadRequested = false;

    // stack with the addresses of all formulas currently calculated recursively
    readonly #formulaAddressStack: ReferenceCycleEntry[] = [];

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super(docModel);
        this.formulaDictionary = new FormulaDictionary(docModel);
        this.pendingCollection = new PendingCollection();
        this.resultCollection = new ResultCollection();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether all formulas need to be recalculated for the initial
     * update cycle.
     *
     * @returns
     *  Whether all formulas need to be initially recalculated currently.
     */
    needsInitialRecalc(): boolean {
        return this.initialUpdateCycle && this.docModel.globalConfig.calcOnLoad;
    }

    /**
     * Inserts a new formula descriptor into all collections of this dependency
     * manager.
     *
     * @param formula
     *  The descriptor for the new formula to be registered.
     *
     * @returns
     *  Whether the formula descriptor has been inserted into the internal
     *  formula collections. If the return value is `false`, this method has
     *  triggered a "recalc:overflow" event, and has stopped the entire
     *  recalculation cycle.
     */
    insertFormula(formula: FormulaDescriptor): boolean {

        // check the current number of formulas
        if ((MAX_FORMULA_COUNT > 0) && (this.formulaSet.size >= MAX_FORMULA_COUNT)) {
            // request setting the "calcOnLoad" flag in the document (notify native
            // application to recalculate all formulas when opening the document)
            this.calcOnLoadRequested ||= !this.initialUpdateCycle;
            this.trigger("recalc:overflow");
            return false;
        }

        // register the formula descriptor in the global map
        this.formulaSet.add(formula);

        // insert the formula descriptor into the sheet descriptor maps
        this.sheetDescMap.get(formula.sheetModel)!.insertFormula(formula);

        // insert the formula descriptor into the dictionary
        this.formulaDictionary.insertFormula(formula);

        // formula has been registered successfully
        return true;
    }

    /**
     * Removes a formula descriptor from all collections of this instance.
     *
     * @param formula
     *  The descriptor of the formula to be removed.
     *
     * @returns
     *  Whether the formula descriptor has been removed from the internal
     *  formula collections.
     */
    removeFormula(formula: FormulaDescriptor): boolean {

        // remove the formula descriptor from the global map
        if (!this.formulaSet.delete(formula)) { return false; }

        // remove the formula descriptor from the sheet descriptor (check existence, sheet
        // descriptor has been removed already while deleting a sheet in the document)
        this.sheetDescMap.get(formula.sheetModel)?.removeFormula(formula);

        // remove the formula descriptor from the dictionary
        this.formulaDictionary.removeFormula(formula);

        // formulas has been unregistered successfully
        return true;
    }

    /**
     * Removes multiple formula descriptors from all collections of this
     * instance.
     *
     * @param formulas
     *  The descriptors of the formulas to be removed. If a nullish value has
     *  been passed, nothing happens.
     */
    removeFormulas(formulas: Nullable<Iterable<FormulaDescriptor>>): void {
        if (formulas) {
            for (const formula of formulas) {
                this.removeFormula(formula);
            }
        }
    }

    /**
     * Refreshes the dependency settings of a formula descriptor that is
     * registered for this dependency manager.
     *
     * @param formula
     *  The descriptor of the formula to be refreshed.
     */
    refreshFormula(formula: FormulaDescriptor): void {

        // find and remove an existing formula descriptor
        if (!this.removeFormula(formula)) { return; }

        // refresh the source dependencies, and reinsert the descriptor
        formula.refreshReferences();
        this.insertFormula(formula);
    }

    /**
     * Clears all formula storage collections of this instance.
     */
    clearAllFormulas(): void {
        this.formulaSet.clear();
        this.sheetDescMap.clear();
        this.formulaDictionary.clear();
    }

    /**
     * Registers a new or changed model object with embedded token arrays.
     *
     * @param tokenModel
     *  The new or changed model instance with embedded token arrays.
     *
     * @param [update]
     *  If set to `true`, this method will try to update an existing formula
     *  descriptor, otherwise it will insert a new formula descriptor.
     */
    @depsLogger.profileMethod("$badge{DepStore} registerTokenModel")
    registerTokenModel(tokenModel: TokenModel, update = false): void {

        // remove old settings when updating an existing formula descriptor
        if (update) { this.unregisterTokenModel(tokenModel); }

        // register the token model for the next update cycle
        this.pendingCollection.registerTokenModel(tokenModel);
    }

    /**
     * Unregisters a deleted model object with embedded token arrays.
     *
     * @param tokenModel
     *  The model instance with embedded token arrays to be deleted.
     */
    @depsLogger.profileMethod("$badge{DepStore} unregisterTokenModel")
    unregisterTokenModel(tokenModel: TokenModel): void {

        // remove the token model from the pending settings
        this.pendingCollection.unregisterTokenModel(tokenModel);

        // remove all formula descriptors registered for the token model
        this.removeFormulas(this.sheetDescMap.get(tokenModel.sheetModel)?.modelFormulasMap.get(tokenModel));
    }

    /**
     * Updates the settings for a new sheet in the spreadsheet document.
     *
     * @param sheetModel
     *  The model instance of the new sheet.
     */
    @depsLogger.profileMethod("$badge{DepStore} registerSheetModel")
    registerSheetModel(sheetModel: SheetModel): void {
        depsLogger.log(() => `sheet="${sheetModel.getName()}" key=${sheetModel.uid}`);

        // create the sheet descriptor object for the new sheet
        this.sheetDescMap.set(sheetModel, new SheetDescriptor(sheetModel));

        // initialize the pending settings of the sheet for the next update cycle
        this.pendingCollection.registerSheetModel(sheetModel);
    }

    /**
     * Updates the settings before a sheet in the spreadsheet document will be
     * deleted.
     *
     * @param sheetModel
     *  The model instance of the deleted sheet.
     */
    @depsLogger.profileMethod("$badge{DepStore} unregisterSheetModel")
    unregisterSheetModel(sheetModel: SheetModel): void {
        depsLogger.log(() => `sheet="${sheetModel.getName()}" key=${sheetModel.uid}`);

        // unregister the pending settings of the sheet
        this.pendingCollection.unregisterSheetModel(sheetModel);

        // remove the sheet descriptor object for the deleted sheet
        const sheetDesc = map.remove(this.sheetDescMap, sheetModel);

        // remove all formulas of the deleted sheet from the internal structures (sheet descriptor
        // may not exist yet, e.g. when inserting/deleting a sheet very quickly in unit tests)
        this.removeFormulas(sheetDesc?.formulaSet);
    }

    /**
     * Returns an up-to-date cell value for the specified cell in the document.
     * If the cell is a formula cell that this dependency manager has marked as
     * dirty, the formula will be recalculated, and all cells referred by the
     * formula that are dirty too will be recalculated recursively. Otherwise,
     * the current value of the cell as stored in the cell model will be
     * returned.
     *
     * @param sheetModel
     *  The model of the sheet containing the cell to be resolved.
     *
     * @param address
     *  The address of the cell whose value will be resolved.
     *
     * @returns
     *  A descriptor object with information about the resolved cell value.
     */
    getCellValue(sheetModel: SheetModel, address: Address): ValueDescriptor {

        // the result descriptor
        const valueDesc: ValueDescriptor = { value: null, type: "fixed", time: 0 };

        // first, try to find a result already calculated by a preceding call of this method
        let resultDesc = this.resultCollection.getResult(sheetModel, address);
        if (resultDesc) {
            valueDesc.value = resultDesc.value;
            valueDesc.type = "cached";
            return valueDesc;
        }

        // in "recalculate all" mode, resolve the token array directly from the cell collection
        if (this.pendingCollection.recalcAll) {

            const tokenDesc = sheetModel.cellCollection.getTokenArray(address, { fullMatrix: true });
            resultDesc = tokenDesc ? this.#calculateCellFormula(sheetModel, tokenDesc.tokenArray, tokenDesc.refAddress, address, tokenDesc.matrixRange) : undefined;

        } else {

            // try to find a dirty formula, and calculate its result
            const formula = this.resultCollection.cellFormulaSet.get(getCellKey(sheetModel, address));
            if (formula) {
                // calculate the result of the cell formula
                resultDesc = this.#calculateCellFormula(sheetModel, formula.tokenArray, formula.refAddress, address, formula.matrixRange);
                // remove the formula descriptor (MUST be done after calculation, needed to detect circular references!)
                // if the formula has been removed already due to a circular reference, keep that result in the map
                this.resultCollection.deleteFormula(formula);
            }
        }

        // fill and return the descriptor if a formula has been calculated
        if (resultDesc) {
            valueDesc.value = resultDesc.value;
            valueDesc.type = "calculated";
            valueDesc.time = resultDesc.time;
            return valueDesc;
        }

        // return the current cell value for all other formulas, and simple cells
        valueDesc.value = sheetModel.cellCollection.getValue(address);
        return valueDesc;
    }

    /**
     * Reinitializes internal members after an update cycle has been completed
     * successfully.
     *
     * @returns
     *  The reference cycles detected during recalculation.
     */
    finalizeUpdateCycle(): ReferenceCycle[] {
        const refCycles = this.resultCollection.referenceCycles;
        this.pendingCollection = new PendingCollection();
        this.resultCollection = new ResultCollection();
        this.initialUpdateCycle = false;
        return refCycles;
    }

    // private methods --------------------------------------------------------

    /**
     * Calculates the result of a cell formula.
     */
    #calculateCellFormula(sheetModel: SheetModel, tokenArray: TokenArray, refAddress: Address, targetAddress: Address, matrixRange: Range | null): Opt<ResultDescriptor> {

        // take the time it needs to calculate the formula (and all formulas it depends on)
        const startTime = Date.now();

        depsLogger.trace(() => `calculating formula in cell ${getAddressLabel(sheetModel, targetAddress)}`);

        // put the target address with sheet UID onto the stack (used to detect circular references)
        this.#formulaAddressStack.push({ address: targetAddress.clone(), sheetUid: sheetModel.uid, sheet: sheetModel.getIndex() });

        // first, calculate the formula result
        const contextType: ContextType = matrixRange ? "mat" : "val";
        const interpretAddress = matrixRange ? refAddress : targetAddress;
        const formulaResult = tokenArray.interpretFormula(contextType, refAddress, interpretAddress, {
            matrixRange,
            recalcDirty: true
        });

        // detect circular reference errors, and other warnings and errors
        const isValid = formulaResult.type === "valid";
        if ((formulaResult.type === "warn") && (formulaResult.code === "circular")) {
            depsLogger.info(() => `reference cycle found: ${this.#formulaAddressStack.map(entry => getAddressLabel(this.docModel.getSheetModel(entry.sheet)!, entry.address)).join()}`);
            this.resultCollection.referenceCycles.push(this.#formulaAddressStack.slice());
        } else if (!isValid) {
            this.resultCollection.hasErrors = true;
        }
        this.#formulaAddressStack.pop();

        // keep old result in the map, if the formula has been recalculated already due to a circular reference
        const resultSet = this.resultCollection.createResultSet(sheetModel);
        if (resultSet.at(targetAddress)) { return undefined; }

        // immediately update the URL of the HYPERLINK function
        const resultURL = (isValid && formulaResult.url) || null; // convert `undefined` to `null`
        sheetModel.cellCollection.setFormulaURL(targetAddress, resultURL);

        // add the timestamp to the result map, needed to detect timeouts during calculation
        const time = Date.now() - startTime;

        // distribute the elements of a matrix result into the result map
        if (matrixRange) {
            const matrix = formulaResult.value as ScalarMatrix;
            const { c, r } = matrixRange.a1;
            for (const address of matrixRange.addresses()) {
                const value = matrix.get(address.r - r, address.c - c, ErrorCode.NA);
                resultSet.insert(address, value, time);
            }
            // return the cell result of the correct matrix element
            return resultSet.at(targetAddress);
        }

        // insert the new result into the result map
        return resultSet.insert(targetAddress, formulaResult.value as ScalarType, time);
    }
}
