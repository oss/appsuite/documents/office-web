# Dependency Manager

The dependency manager is a singleton of a Spreadsheet application. It collects all formula expressions in the document, e.g. cell formulas, shared formulas, conditional formatting rules, and defined names, and listens to all changes in the document contents done by applying document operations. After such changes, the collection of existing formulas will be updated, and they will be examined whether they depend on any changed document contents. Each of these dirty formulas will cause to update further results depending on them. Cell formulas will be recalculated, and the new results will be stored in the cells. Dirty formatting rules will be evaluated, and the new conditional formatting will be rendered in the document view.

## Architecture

A rough overview of the architecture:

- Class `DependencyManager` is the "main entry point". Each spreadsheet document model contains one instance.
- An instance of `DependencyManager` owns an instance of `DependencyWorker` with the complete logic of the background update task.
- A `DependencyManager` listens to and collects all change events from the document model and re-triggers the worker.
- A `DependencyWorker` owns four different instances of `AsyncWorker` implementing parts of the background update task.
  - `MainWorker`: High-level update cycle (trigger events, start the following three sub workers in order).
  - `PendingSheetWorker`: Process the collected change events, update internal storage (`PendingCollection` and friends).
  - `RecalcDirtyWorker`: Recalculate all collected dirty formulas (also recursively, if more formulas depend on new results), store results in `ResultCollection`.
  - `SendResultsWorker`: Apply all new formula results locally in the document model, and generate and send JSON operations to server.
- Workers can be aborted at any time, and will be aborted when the document triggers more change events while the workers are running
- Processed and calculated data will remain in the internal caches, new document changes will be processed and will extend the collections until the background task is completed.

## Glossary

### Cell Key

A unique identifier for a cell in the spreadsheet document, as used by the dependency manager.

A cell key consists of the [UID](#uid) of the sheet containing the cell, followed by a single exclamation mark, followed by the internal key of the cell address, as returned by the function `addressKey()`. Using the internal address key is faster than calculating the human-readable cell name with letters as column index.

Example: The cell key `obj123!1,3` represents the cell `B4` contained in the sheet with the UID `obj123`.

### Defined Name

A defined name (or named formula expression, or named range) is a formula expression with a unique name, that can be used as placeholder for that formula expression in any other formula in the spreadsheet document.

Defined names can be contained in the document itself (globally defined names), which makes them available in the entire document; or they can be contained in a specific sheet of the document (sheet-locally defined names), which makes them available in that sheet only. Sheet-local names can be accessed from other sheets by qualifying the defined name with the sheet name.

Example: The globally defined name `my_name` may contain the [formula expression](#formula-expression) `=Sheet1!$A$1:$C$4`. The cell formula `=SUM(my_name)` will then be equivalent to `=SUM(Sheet1!$A$1:$C$4)`.

### Formatting Rule

A formatting rule is the smallest part of a conditional formatting. It covers specific cell ranges in a sheet, and contains conditions that specify how to format these cell ranges. These conditions may contain one or more [formula
expressions](#formula-expression).

### Formula Dictionary

The central data structure of the dependency manager that stores all formulas mapped by their dependencies. The dictionary will be used for example to find dirty formulas efficiently that need to be recalculated after cells in the
document have been changed.

### Formula Expression

The textual representation of a spreadsheet formula.

Example: The formula expression `=SUM(Sheet1!A1:C4)` will be used to calculate the sum of all numbers in the cell range `A1:C4` of the sheet `Sheet1`.

### Formula Key

The unique key of a [token array](#token-array) used in the dependency manager. The exact form of a formula key depends on the object containing the token array:

- For the cells in a sheet, the [cell key](#cell-key) will be used as formula key.
- For conditions of a conditional formatting rule, the [UID](#uid) of the rule model (class `CFRuleModel`) will be used, followed by an exclamation mark, followed by an internal identifier of the condition.

### Reference Collection

A data structure that collects all external dependencies of a single formula, e.g. its cell range references, defined names, and the cell range references of these defined names (recursively).

### Table Range

A specific cell range in a sheet occupied by a table structure. A table range defines its own formatting, filtering settings, and sorting for its columns, rows, and cells. Entire table ranges, or specific parts of table ranges, can be referred in formula expressions, e.g. in the formula `=SUM(Table1[#Data])`.

### Token Array

A data structure (instance of the class `TokenArray`) that represents a parsed [formula expression](#formula-expression). Token arrays are contained in various different objects in a spreadsheet document, e.g. formula cells, defined names, or conditions of conditional formatting rules.

### UID

The globally unique identifier of a model object. UIDs are provided by the property `DObject.uid`, and they are unique through all Documents applications.

Example: The UID of a sheet model (class `SheetModel`, derived from class `DObject`) may be `obj123`. No other model object in this application, or any other Documents application will have the same UID, regardless of its type.
