/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map, set } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

import type { TokenModel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { ChangeCellsEvent } from "@/io.ox/office/spreadsheet/model/cellcollection";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";

// class PendingSheetData =====================================================

/**
 * Container for addresses of dirty cells and ranges in a single sheet.
 */
export class PendingSheetData {

    /**
     * The model of the sheet this instance is related to.
     */
    readonly sheetModel: SheetModel;

    /**
     * The addresses of all cell ranges where cells have been moved and thus
     * whose formula cells need to be updated completely.
     */
    dirtyRanges = new RangeArray();

    /**
     * The addresses of all dirty value cells (cells with changed values) in
     * the sheet. The cell addresses will be collected while processing cell
     * change events received from the document, and will cause to update all
     * formulas known by the dependency manager that depend directly or
     * indirectly on these cells.
     */
    dirtyValueCells = new AddressArray();

    /**
     * The addresses of all dirty formula cells (cells with changed formula
     * expression) in the sheet. The cell addresses will be collected while
     * processing cell change events received from the document, and will cause
     * to refresh the reference dependencies.
     */
    dirtyFormulaCells = new AddressArray();

    /**
     * The addresses of all formula cells in the sheet with outdated result
     * values. The cell addresses will be collected, and will cause to
     * recalculate all formulas, in order to get the correct results e.g. after
     * generating new cell formulas without the results.
     */
    readonly recalcAddressSet = new KeySet<Address>();

    /**
     * A set with all model objects with embedded token arrays that have been
     * inserted into the sheet, or that have been changed. The models will be
     * collected while processing change events received from the document, and
     * will cause to update all their dependency settings.
     */
    readonly dirtyModelSet = new Set<TokenModel>();

    /**
     * Marks a new sheet inserted into the pending collection. The next update
     * cycle of the dependency manager will fetch all formulas from the new
     * sheet, instead of just processing the changed formulas.
     */
    initialUpdate = false;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel) {
        this.sheetModel = sheetModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts the passed model with embedded token arrays into this instance.
     *
     * @param tokenModel
     *  The new or changed model object with embedded token arrays.
     */
    registerTokenModel(tokenModel: TokenModel): void {
        this.dirtyModelSet.add(tokenModel);
    }

    /**
     * Removes the passed model with embedded token arrays from this instance.
     *
     * @param tokenModel
     *  The model object with embedded token arrays to be deleted.
     */
    unregisterTokenModel(tokenModel: TokenModel): void {
        this.dirtyModelSet.delete(tokenModel);
    }
}

// class PendingCollection ====================================================

/**
 * Encapsulates all data that will be collected by a dependency manager when
 * processing document change events. Will be used to find and update all dirty
 * formulas depending on the data collected in an instance of this class.
 */
export class PendingCollection {

    /**
     * Specifies whether the next update cycle has to recalculate all existing
     * formulas, instead of just the dirty formulas according to the collected
     * data in this instance.
     */
    recalcAll = false;

    /**
     * Specifies whether the next update cycle has to recalculate all volatile
     * formulas, also if they do not depend on any changed cell.
     */
    recalcVolatile = true;

    /**
     * The model keys of all dirty defined names. After inserting a new defined
     * name, or changing its formula expression, all depending formulas need to
     * be recalculated.
     */
    readonly changedNameKeys = new Set<string>();

    /**
     * The model instances of all defined names with changed label. After
     * inserting a new defined name, or changing its label, all formulas that
     * contain unresolved names need to be recalculated in order to ged rid of
     * `#NAME?` errors.
     */
    readonly changedLabelsSet = new Set<NameModel>();

    /**
     * The model keys of all dirty table ranges. After changing the target
     * range of a table range, all depending formulas need to be recalculated,
     * even if the contents of the table did not change (e.g. when using the
     * functions `COLUMNS` or `ROWS`).
     */
    readonly changedTableKeys = new Set<string>();

    /**
     * A map with all data of changed cell contents in a sheet.
     */
    readonly changedSheetMap = new Map<SheetModel, PendingSheetData>();

    // public methods ---------------------------------------------------------

    /**
     * Registers a defined name in this collection that has been inserted into
     * the spreadsheet document, or that has been changed (either the formula
     * expression, or the label).
     *
     * @param nameModel
     *  The model instance of the new or changed defined name.
     *
     * @param labelChanged
     *  Whether the label of the defined name has been changed. MUST be set to
     *  `true` for new defined names inserted into the document.
     *
     * @param formulaChanged
     *  Whether the formula expression of the defined name has been changed.
     *  MUST be set to `true` for new defined names inserted into the document.
     */
    registerNameModel(nameModel: NameModel, labelChanged: boolean, formulaChanged: boolean): void {
        if (labelChanged) { this.changedLabelsSet.add(nameModel); }
        if (formulaChanged) { this.changedNameKeys.add(nameModel.key); }
    }

    /**
     * Unregisters a defined name from this collection that will be deleted
     * from the spreadsheet document.
     *
     * @param nameModel
     *  The model instance of the defined name that is about to be deleted.
     */
    unregisterNameModel(nameModel: NameModel): void {
        this.changedNameKeys.add(nameModel.key); // add to changed set to replace labels with #NAME? error
        this.changedLabelsSet.delete(nameModel);
    }

    /**
     * Registers a table range with a changed target range in this collection.
     *
     * @param tableModel
     *  The model instance of the changed table range.
     */
    registerTableModel(tableModel: TableModel): void {
        if (!tableModel.isAutoFilter()) {
            this.changedTableKeys.add(tableModel.key);
        }
    }

    /**
     * Unregisters a table range from this collection that will be deleted from
     * the spreadsheet document.
     *
     * @param tableModel
     *  The model instance of the table range that is about to be deleted.
     */
    unregisterTableModel(tableModel: TableModel): void {
        if (!tableModel.isAutoFilter()) {
            this.changedTableKeys.delete(tableModel.key);
        }
    }

    /**
     * Returns an existing container for dirty addresses for the specified
     * sheet, or creates a new container on demand.
     *
     * @param sheetModel
     *  The model instance of a sheet.
     *
     * @returns
     *  The collection of dirty cell addresses for the specified sheet.
     */
    createPendingSheetData(sheetModel: SheetModel): PendingSheetData {
        return map.upsert(this.changedSheetMap, sheetModel, () => new PendingSheetData(sheetModel));
    }

    /**
     * Returns an existing container for dirty addresses for the specified
     * sheet, or creates a new container on demand.
     *
     * @param sheetModel
     *  The model instance of a sheet.
     */
    removePendingSheetData(sheetModel: SheetModel): void {
        this.changedSheetMap.delete(sheetModel);
    }

    /**
     * Returns an existing container for dirty addresses for the specified
     * sheet.
     *
     * @param sheetModel
     *  The model instance of a sheet.
     *
     * @returns
     *  The collection of dirty cell addresses for the specified sheet, if
     *  existing; otherwise `undefined`.
     */
    getPendingSheetData(sheetModel: SheetModel): Opt<PendingSheetData> {
        return this.changedSheetMap.get(sheetModel);
    }

    /**
     * Adds initial settings for a new sheet into this collection.
     *
     * @param sheetModel
     *  The model instance of the new sheet.
     */
    registerSheetModel(sheetModel: SheetModel): void {
        this.createPendingSheetData(sheetModel).initialUpdate = true;
    }

    /**
     * Removes all settings for a deleted sheet from this collection: The range
     * arrays of moved cells, all sheet-locally defined names, and the table
     * ranges.
     *
     * @param sheetModel
     *  The model instance of the deleted sheet.
     */
    unregisterSheetModel(sheetModel: SheetModel): void {

        // remove all dirty addresses for the deleted sheet
        this.removePendingSheetData(sheetModel);

        // remove all pending sheet-local names contained in the deleted sheet
        for (const nameModel of sheetModel.nameCollection.yieldNameModels()) {
            this.unregisterNameModel(nameModel);
        }

        // remove all pending table models contained in the deleted sheet
        for (const tableModel of sheetModel.tableCollection.yieldTableModels()) {
            this.unregisterTableModel(tableModel);
        }
    }

    /**
     * Appends the addresses of dirty cells in a sheet according to the passed
     * cell change descriptor.
     *
     * @param sheetModel
     *  The model instance of the changed sheet.
     *
     * @param event
     *  The descriptor with the addresses of all changed value and formula
     *  cells, as received from a "change:cells" document event.
     *
     * @returns
     *  Whether any cell addresses have been registered.
     */
    registerDirtyCells(sheetModel: SheetModel, event: ChangeCellsEvent): boolean {
        const sheetData = this.createPendingSheetData(sheetModel);
        sheetData.dirtyValueCells.append(event.valueCells);
        sheetData.dirtyFormulaCells.append(event.formulaCells);
        return !event.valueCells.empty() || !event.formulaCells.empty();
    }

    /**
     * Moves all cached addresses of dirty cells in a sheet according to the
     * passed address transformer.
     *
     * @param sheetModel
     *  The model instance of the changed sheet.
     *
     * @param transformer
     *  The address transformer with the positions of all moved cells, as
     *  received from a "move:cells" document event.
     */
    moveDirtyCells(sheetModel: SheetModel, transformer: AddressTransformer): void {

        const sheetData = this.createPendingSheetData(sheetModel);
        sheetData.dirtyRanges.push(transformer.dirtyRange);

        // transform addresses of known dirty value and formula cells
        sheetData.dirtyValueCells = transformer.transformAddresses(sheetData.dirtyValueCells);
        sheetData.dirtyFormulaCells = transformer.transformAddresses(sheetData.dirtyFormulaCells);

        // transform the addresses in the address set via temporary array
        const recalcAddresses = transformer.transformAddresses(sheetData.recalcAddressSet);
        sheetData.recalcAddressSet.clear();
        set.assign(sheetData.recalcAddressSet, recalcAddresses);
    }

    /**
     * Inserts the passed model object with embedded token arrays into this
     * collection.
     *
     * @param tokenModel
     *  The new or changed model object with embedded token arrays.
     */
    registerTokenModel(tokenModel: TokenModel): void {
        const sheetData = this.createPendingSheetData(tokenModel.sheetModel);
        sheetData.registerTokenModel(tokenModel);
    }

    /**
     * Removes the passed model object with embedded token arrays from this
     * collection.
     *
     * @param tokenModel
     *  The model object with embedded token arrays to be deleted.
     */
    unregisterTokenModel(tokenModel: TokenModel): void {
        const sheetData = this.changedSheetMap.get(tokenModel.sheetModel);
        sheetData?.unregisterTokenModel(tokenModel);
    }
}
