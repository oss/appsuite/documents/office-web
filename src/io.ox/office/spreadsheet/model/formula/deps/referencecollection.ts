/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";

import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { getRangeLabel, getNameLabel, getTableLabel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import type { FormulaDependencies } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladependencies";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// class ReferenceCollection ==================================================

/**
 * A data structure that stores information about the source references a
 * formula expression depends on.
 *
 * The collection contains entries for the following references:
 * - Range addresses of the cell references contained in the formula.
 * - The models of the [defined name]s contained in the formula.
 * - The models of the [table range]s contained in the formula.
 *
 * @param docModel
 *  The document model containing all formulas.
 *
 * @param dependencies
 *  The resolved dependencies of a token array.
 */
export class ReferenceCollection {

    /**
     * Maps sheets to range arrays.
     */
    readonly rangesMap = new Map<SheetModel, RangeArray>();

    /**
     * The models of all defined names.
     */
    readonly nameMap: FormulaDependencies["nameMap"];

    /**
     * The models of all table ranges.
     */
    readonly tableMap: FormulaDependencies["tableMap"];

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, dependencies: FormulaDependencies) {

        this.nameMap = dependencies.nameMap;
        this.tableMap = dependencies.tableMap;

        // separate all source ranges by sheets (also the root ranges of cell formulas, and other formulas), and store
        // them by sheet UID independently from sheet indexes, so this keeps working when inserting/deleting sheets etc.
        dependencies.depRanges.concat(dependencies.rootRanges).unify().forEach(range3d => {
            const range2d = range3d.toRange();
            for (const sheetModel of docModel.yieldSheetModels({ first: range3d.sheet1, last: range3d.sheet2, cells: true })) {
                map.upsert(this.rangesMap, sheetModel, () => new RangeArray()).push(range2d);
            }
        });

        // merge the range addresses to prevent duplicates and overlapping ranges
        this.rangesMap.forEach(ranges => ranges.assign(ranges.merge()));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this collection is empty.
     *
     * @returns
     *  Whether this collection is empty.
     */
    empty(): boolean {
        return !this.rangesMap.size && !this.nameMap.size && !this.tableMap.size;
    }

    /**
     * Returns whether the specified name model is contained in this reference
     * collection.
     *
     * @param nameModel
     *  The model instance of a defined name.
     *
     * @returns
     *  Whether the name model is contained in this reference collection.
     */
    containsName(nameModel: NameModel): boolean {
        return this.nameMap.has(nameModel.key);
    }

    /**
     * Returns whether the specified table model is contained in this reference
     * collection.
     *
     * @param tableModel
     *  The model instance of a table range.
     *
     * @returns
     *  Whether the table model is contained in this reference collection.
     */
    containsTable(tableModel: TableModel): boolean {
        return this.tableMap.has(tableModel.key);
    }

    /**
     * Creates the string representation of all contents of this collection for
     * debug logging.
     *
     * @returns
     *  The string representation of all contents of this collection.
     */
    toString(): string {
        const labels: string[] = [];
        this.rangesMap.forEach((ranges, sheetModel) => ranges.forEach(range => labels.push(getRangeLabel(sheetModel, range))));
        for (const nameModel of this.nameMap.values()) { labels.push(getNameLabel(nameModel)); }
        for (const tableModel of this.tableMap.values()) { labels.push(getTableLabel(tableModel)); }
        return labels.join();
    }
}
