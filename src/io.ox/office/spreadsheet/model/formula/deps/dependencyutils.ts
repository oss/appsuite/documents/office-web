/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LogLevel, Logger } from "@/io.ox/office/tk/utils/logger";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import { CFRuleModel } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import type { XSourceLinkedModel } from "@/io.ox/office/spreadsheet/model/drawing/xsourcelinkedmodel";
import type { FormulaDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladescriptor";

// types ======================================================================

/**
 * Types of models containing token arrays.
 */
export type TokenModel = CFRuleModel | XSourceLinkedModel<any, any>;

// class DependencyLogger =====================================================

class DependencyLogger extends Logger {

    constructor() {
        super("spreadsheet:log-deps", { tag: "DEP", tagColor: 0x00FF00 });
    }

    /**
     * Prints all formula descriptors in the passed map to the browser console,
     * if dependency logging is active.
     *
     * @param formulas
     *  All formula descriptors to be logged to the browser console.
     */
    logFormulas(formulas: Iterable<FormulaDescriptor>): void {
        this.withLogLevel(LogLevel.TRACE, () => {
            for (const desc of formulas) {
                this.trace(`location=${desc} key=${desc.key} references=${desc.references} circular=${desc.circular}`);
            }
        });
    }
}

/**
 * Debug logger for the dependency manager, bound to the debug configuration
 * flag "spreadsheet:log-deps".
 */
export const depsLogger = new DependencyLogger();

// public functions ===========================================================

/**
 * Returns the formula key for the specified sheet UID and cell address.
 *
 * @param sheetModel
 *  The model of a sheet in a spreadsheet document.
 *
 * @param address
 *  The cell address to create a cell key for.
 *
 * @returns
 *  The formula key for the specified cell address.
 */
export function getCellKey(sheetModel: SheetModel, address: Address): string {
    return `${sheetModel.uid}!${address.key}`;
}

/**
 * Returns the formula key for the specified sheet UID and cell range address.
 *
 * @param sheetModel
 *  The model of a sheet in a spreadsheet document.
 *
 * @param range
 *  The cell range address to create a range key for.
 *
 * @returns
 *  The formula key for the specified cell range address.
 */
export function getRangeKey(sheetModel: SheetModel, range: Range): string {
    return `${sheetModel.uid}!${range.key}`;
}

/**
 * Returns the formula key for the specified source link formula.
 *
 * @param linkedModel
 *  The model instance of a source link.
 *
 * @param linkKey
 *  The fully qualified attribute name of the source link formula.
 *
 * @returns
 *  The formula key for the specified source link formula.
 */
export function getLinkKey(linkedModel: XSourceLinkedModel<any, any>, linkKey: string): string {
    return `${linkedModel.uid}!${linkKey}`;
}

// debugging ------------------------------------------------------------------

/**
 * Returns a readable label for the passed cell address for debug logging.
 *
 * @param sheetModel
 *  The model of the sheet containing the cell.
 *
 * @param address
 *  The address of a cell.
 *
 * @returns
 *  A readable label for the passed cell address for debug logging.
 */
export function getAddressLabel(sheetModel: SheetModel, address: Address): string {
    return `${sheetModel.getName()}!${address}`;
}

/**
 * Returns a readable label for the passed cell addresses for debug logging.
 *
 * @param sheetModel
 *  The model of the sheet containing the cell.
 *
 * @param addresses
 *  The cell addresses.
 *
 * @returns
 *  A readable label for the passed cell addresses for debug logging.
 */
export function getAddressesLabel(sheetModel: SheetModel, addresses: Iterable<Address>): string {
    return getRangesLabel(sheetModel, RangeArray.mergeAddresses(addresses));
}

/**
 * Returns a readable label for the passed cell range address for debug
 * logging.
 *
 * @param sheetModel
 *  The model of the sheet containing the cell range.
 *
 * @param range
 *  The address of a cell range.
 *
 * @returns
 *  A readable label for the passed cell range address for debug logging.
 */
export function getRangeLabel(sheetModel: SheetModel, range: Range): string {
    return `${sheetModel.getName()}!${range.single() ? range.a1 : range}`;
}

/**
 * Returns a readable label for the passed cell addresses for debug logging.
 *
 * @param sheetModel
 *  The model of the sheet containing the cell.
 *
 * @param ranges
 *  The cell range addresses.
 *
 * @returns
 *  A readable label for the passed cell addresses for debug logging.
 */
export function getRangesLabel(sheetModel: SheetModel, ranges: Range[]): string {
    return `${sheetModel.getName()}!${ranges.join(",")}`;
}

/**
 * Returns a readable label for the passed defined name for debug logging.
 *
 * @param nameModel
 *  The model instance of a defined name.
 *
 * @returns
 *  A readable label for the passed defined name for debug logging.
 */
export function getNameLabel(nameModel: NameModel): string {
    return `{NAME}${nameModel.sheetModel?.getName() ?? "[0]"}!${nameModel.label}`;
}

/**
 * Returns a readable label for the passed table range for debug logging.
 *
 * @param tableModel
 *  The model instance of a table range.
 *
 * @returns
 *  A readable label for the passed table range for debug logging.
 */
export function getTableLabel(tableModel: TableModel): string {
    return `{TABLE}${tableModel.sheetModel.getName()}!${tableModel.name}`;
}

/**
 * Returns a readable label for the passed formatting rule for debug logging.
 *
 * @param ruleModel
 *  The model instance of a formatting rule.
 *
 * @returns
 *  A readable label for the passed formatting rule for debug logging.
 */
export function getRuleLabel(ruleModel: CFRuleModel): string {
    return `{RULE}${ruleModel.sheetModel.getName()}!${ruleModel.getTargetRanges()}`;
}

/**
 * Returns a readable label for the passed source link for debug logging.
 *
 * @param linkedModel
 *  The model instance of a source link.
 *
 * @returns
 *  A readable label for the passed source link for debug logging.
 */
export function getLinkLabel(linkedModel: XSourceLinkedModel<any, any>): string {
    return `{LINK}${linkedModel.sheetModel.getName()}!${linkedModel.uid}`;
}

/**
 * Returns a readable label for the passed rule model or source link for debug
 * logging.
 *
 * @param tokenModel
 *  The model instance of a formatting rule, or of a source link.
 *
 * @returns
 *  A readable label for the passed model for debug logging.
 */
export function getTokenLabel(tokenModel: TokenModel): string {
    return (tokenModel instanceof CFRuleModel) ? getRuleLabel(tokenModel) : getLinkLabel(tokenModel);
}
