/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map, set } from "@/io.ox/office/tk/algorithms";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";

import { type RecalcMode, getRecalcMode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";

// types ======================================================================

/**
 * The position of the target cells a formula is associated with. Can be a
 * single cell address (e.g. for cell formulas), a single cell range address,
 * or an array of cell range addresses (e.g. the target ranges of a formatting
 * rule or chart model).
 */
export type DependencyTarget = Address | Range | RangeArray;

// class FormulaDependencies ==================================================

/**
 * A descriptor object with the dependency settings of a token array.
 */
export class FormulaDependencies {

    /**
     * The addresses of all cell ranges the formula depends on, with sheet
     * indexes, according to the passed reference address and target address.
     */
    readonly depRanges = new Range3DArray();

    /**
     * The addresses of all cell ranges the root node of the compiler tree
     * depends on. If the token array is associated to a defined name, these
     * ranges will be forwarded to the name token of the parent formula
     * expression, in order to decide how to proceed with these ranges.
     */
    readonly rootRanges = new Range3DArray();

    /**
     * A set with the models of all defined names, mapped by their model keys.
     */
    readonly nameMap = new Map<string, NameModel>();

    /**
     * A map with the models of all table ranges, mapped by their model keys.
     */
    readonly tableMap = new Map<string, TableModel>();

    /**
     * A set with the model keys of all missing defined names occurring in the
     * formula expression, or in any existing defined name referred by the
     * formula.
     */
    readonly missingNames = new Set<string>();

    /**
     * A set with the resource keys of all unimplemented functions that occur
     * in the formula.
     */
    readonly missingFuncs = new Set<string>();

    /**
     * The recalculation mode: Either "always" or "once", if at least one
     * function in the formula contains the respective recalculation mode
     * (either directly, or indirectly from a defined name; "always" will be
     * preferred over "once"); otherwise `null`.
     */
    recalc: RecalcMode = "normal";

    /**
     * Whether trying to resolve the defined names referred by the token array
     * resulted in a circular reference error.
     */
    circular = false;

    // public methods ---------------------------------------------------------

    /**
     * Merges the dependency settings of a defined name with the own settings.
     *
     * @param nameModel
     *  The model of the defined name.
     *
     * @param nameDeps
     *  The dependency settings of the defined name to be merged.
     */
    mergeNameDeps(nameModel: NameModel, nameDeps: FormulaDependencies): void {
        this.depRanges.append(nameDeps.depRanges);
        this.nameMap.set(nameModel.key, nameModel);
        map.assign(this.nameMap, nameDeps.nameMap);
        map.assign(this.tableMap, nameDeps.tableMap);
        set.assign(this.missingNames, nameDeps.missingNames);
        set.assign(this.missingFuncs, nameDeps.missingFuncs);
        this.recalc = getRecalcMode(this.recalc, nameDeps.recalc);
        this.circular ||= nameDeps.circular;
    }
}
