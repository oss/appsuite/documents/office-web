/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { RecalcMode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SheetTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import { type TokenModel, getTokenLabel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import type { DependencyTarget } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladependencies";
import { ReferenceCollection } from "@/io.ox/office/spreadsheet/model/formula/deps/referencecollection";

// class FormulaDescriptor ====================================================

/**
 * A descriptor object for a token array in a spreadsheet document containing
 * all its dependency settings.
 */
export abstract class FormulaDescriptor implements Keyable {

    /**
     * The unique key of this formula descriptor.
     */
    readonly key: string;

    /**
     * The sheet model containing the formula expression.
     */
    readonly sheetModel: SheetModel;

    /**
     * The token array represented by this descriptor object.
     */
    readonly tokenArray: SheetTokenArray;

    /**
     * The reference address of the formula, i.e. the address of the cell the
     * relative references are associated to. For cell formulas, this is the
     * address of the cell containing the formula. For shared formulas, this is
     * the address of its anchor cell. For matrix formulas, for conditional
     * formatting rules, and for data validation rules, this is the top-left
     * cell of the bounding range of the parent object. For source links, this
     * will be the address A1.
     */
    readonly refAddress: Address;

    /**
     * A collection with source references the token array depends on directly.
     * If this collection is empty, the token array does not depend on any
     * other external object in the spreadsheet document.
     */
    references!: ReferenceCollection;

    /**
     * A set with the model keys of all missing defined names occurring in the
     * formula expression, or in any existing defined name referred by the
     * formula.
     */
    missingNames!: Set<string>;

    /**
     * A set with the resource keys of all unimplemented functions that occur
     * in the token array.
     */
    missingFuncs!: Set<string>;

    /**
     * The recalculation mode of the formula expression inferred from the
     * recalculation modes of the contained functions.
     */
    recalc: RecalcMode = "normal";

    /**
     * If set to `true`, a circular dependency has been detected for the
     * formula represented by this descriptor. In this case, resolving the
     * dependency chain for this formula will be stopped to prevent endless
     * loops.
     */
    circular = false;

    // constructor ------------------------------------------------------------

    protected constructor(formulaKey: string, tokenArray: SheetTokenArray, refAddress: Address) {
        this.key = formulaKey;
        this.sheetModel = tokenArray.sheetModel;
        this.tokenArray = tokenArray;
        this.refAddress = refAddress;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the formula represented by this descriptor does not have
     * any external dependencies, and therefore does not need to be handled by
     * the dependency manager.
     *
     * @returns
     *  Whether the formula represented by this descriptor does not have any
     *  external dependencies.
     */
    isFixed(): boolean {
        return !this.recalc && this.references.empty() && !this.missingNames.size && !this.missingFuncs.size;
    }

    /**
     * Recalculates the dependency setings for the formula represented by this
     * descriptor.
     */
    refreshReferences(): void {

        // get the target position for resolving the dependencies
        const depTarget = this.getDependencyTarget();

        // resolve the dependencies of the token array (cell ranges, and defined names)
        const dependencies = this.tokenArray.getDependencies(this.refAddress, depTarget);
        this.references = new ReferenceCollection(this.tokenArray.docModel, dependencies);

        // copy the other settings
        this.missingNames = dependencies.missingNames;
        this.missingFuncs = dependencies.missingFuncs;
        this.recalc = dependencies.recalc;
        this.circular = dependencies.circular;
    }

    /**
     * Returns the target location of the formula for resolving dependencies,
     * i.e. the cell address of cell formulas, or the target ranges of model
     * formulas (formatting rules, chart objects).
     */
    abstract getDependencyTarget(): DependencyTarget;

    /**
     * Returns a string representation for this formula descriptor for debug
     * logging.
     *
     * @returns
     *  A string representation for this formula descriptor for debug logging.
     */
    abstract toString(): string;
}

// class CellFormulaDescriptor ================================================

/**
 * A descriptor object for a cell token array in a spreadsheet document
 * containing all its dependency settings.
 */
export class CellFormulaDescriptor extends FormulaDescriptor {

    /**
     * The actual address of a cell formula, i.e. the address of the cell the
     * formula is associated with. Will differ to the reference address in the
     * property `refAddress`, if the cell is part of a shared formula, but is
     * not its anchor cell. Will be `null`, if this instance does not represent
     * a cell formula.
     */
    readonly cellAddress: Address;

    /**
     * The bounding range of a matrix formula; otherwise `null`. The start
     * address of this range MUST be equal to the properties `refAddress` and
     * `cellAddress` of this instance.
     */
    readonly matrixRange: Range | null;

    // constructor ------------------------------------------------------------

    constructor(formulaKey: string, tokenArray: SheetTokenArray, refAddress: Address, cellAddress: Address, matrixRange: Range | null) {
        super(formulaKey, tokenArray, refAddress);
        this.cellAddress = cellAddress;
        this.matrixRange = matrixRange;
        this.refreshReferences();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the target location of the formula for resolving dependencies
     * (the cell address).
     */
    override getDependencyTarget(): DependencyTarget {
        return this.cellAddress;
    }

    /**
     * Returns a string representation for this formula descriptor for debug
     * logging.
     *
     * @returns
     *  A string representation for this formula descriptor for debug logging.
     */
    override toString(): string {
        return this.sheetModel.getName() + "!" + this.cellAddress.toOpStr();
    }
}

// class FormulaDescriptor ====================================================

/**
 * A descriptor object for a token array of some arbitrary model instance in a
 * spreadsheet document containing all its dependency settings, e.g. a
 * conditional formatting rule, or a chart model.
 */
export class ModelFormulaDescriptor extends FormulaDescriptor {

    /**
     * For conditions of formatting rules, this will be the parent rule model
     * containing the formula. For formulas of a source link model, this will
     * be the parent link model containing the formula (e.g. the data series
     * model of a chart). Otherwise, this property will be `null`.
     */
    readonly tokenModel: TokenModel;

    /**
     * The internal key of the token array in "tokenModel".
     */
    readonly tokenKey: string;

    // constructor ------------------------------------------------------------

    constructor(formulaKey: string, tokenArray: SheetTokenArray, refAddress: Address, tokenModel: TokenModel, tokenKey: string) {
        super(formulaKey, tokenArray, refAddress);
        this.tokenModel = tokenModel;
        this.tokenKey = tokenKey;
        this.refreshReferences();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the target location of the formula for resolving dependencies
     * (the cell address).
     */
    override getDependencyTarget(): DependencyTarget {
        return this.tokenModel.getDependencyTarget();
    }

    /**
     * Returns a string representation for this formula descriptor for debug
     * logging.
     *
     * @returns
     *  A string representation for this formula descriptor for debug logging.
     */
    override toString(): string {
        return getTokenLabel(this.tokenModel) + "!" + this.tokenKey;
    }
}
