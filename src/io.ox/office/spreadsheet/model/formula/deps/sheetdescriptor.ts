/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";
import { type TokenModel, depsLogger } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { type FormulaDescriptor, CellFormulaDescriptor, ModelFormulaDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladescriptor";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// class SheetDescriptor ======================================================

/**
 * This descriptor object collects all dependency settings for a sheet in a
 * spreadsheet document.
 */
export class SheetDescriptor {

    // properties -------------------------------------------------------------

    /**
     * The model of the sheet represented by this descriptor object.
     */
    readonly sheetModel: SheetModel;

    /**
     * The formula descriptors for all known token arrays of the sheet.
     */
    readonly formulaSet = new KeySet<FormulaDescriptor>();

    /**
     * The cell addresses of all known cell formulas of the sheet.
     */
    readonly addressSet = new AddressSet();

    /**
     * The descriptors of all formulas attached to a token array embedded in a
     * model object (e.g. conditions of formatting rules, source link formulas
     * of chart data series). The elements of this map are maps of formula
     * descriptors. The maps are keyed by the UIDs of the parent token model.
     */
    readonly modelFormulasMap = new Map<TokenModel, KeySet<ModelFormulaDescriptor>>();

    /**
     * The formula descriptors for all known token arrays that contain
     * references to non-existing defined names. These formulas are considered
     * to be dirty, if a defined name has been inserted, or an existing defined
     * name has changed its label, which may result in validating a `#NAME?`
     * error. The property is a map with the name keys (the upper-case labels)
     * of missing names as key, and stores a map of formulas per missing name.
     */
    readonly missingNamesMap = new Map<string, KeySet<FormulaDescriptor>>();

    /**
     * The formula descriptors for all known token arrays with recalculation
     * mode "always". These formulas will always be recalculated whenever
     * something in the document has changed, regardless of their dependencies.
     */
    readonly recalcAlwaysMap = new KeySet<FormulaDescriptor>();

    /**
     * The formula descriptors for all known token arrays with recalculation
     * mode "once". These formulas will be recalculated once after the document
     * has been imported, regardless of their dependencies.
     */
    readonly recalcOnceMap = new KeySet<FormulaDescriptor>();

    /**
     * The resource keys of all unimplemented functions that occur in any
     * formula of the sheet.
     */
    readonly missingFuncs = new Set<string>();

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel) {
        this.sheetModel = sheetModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts the passed formula descriptor into the formula caches of this
     * sheet descriptor.
     *
     * @param formula
     *  The formula descriptor to be inserted into the formula caches.
     */
    insertFormula(formula: FormulaDescriptor): void {

        // insert the formula descriptor into the own maps
        this.formulaSet.add(formula);
        switch (formula.recalc) {
            case "always": this.recalcAlwaysMap.add(formula); break;
            case "once":   this.recalcOnceMap.add(formula);   break;
            case "normal": break;
        }

        // insert the formula into the type-specific containers
        if (formula instanceof CellFormulaDescriptor) {
            this.addressSet.add(formula.cellAddress);
        } else if (formula instanceof ModelFormulaDescriptor) {
            map.upsert(this.modelFormulasMap, formula.tokenModel, () => new KeySet()).add(formula);
        } else {
            depsLogger.error("$badge{SheetDescriptor} insertFormula: unknown formula type");
        }

        // map the formula descriptor by missing name labels
        for (const nameKey of formula.missingNames) {
            map.upsert(this.missingNamesMap, nameKey, () => new KeySet()).add(formula);
        }

        // register all unimplemented functions
        for (const funcKey of formula.missingFuncs) {
            this.missingFuncs.add(funcKey);
        }

        depsLogger.trace(() => `new dependencies for ${formula}: ${formula.references}`);
    }

    /**
     * Deletes the specified formula descriptor from the formula caches of this
     * sheet descriptor.
     *
     * @param formula
     *  The descriptor of the formula to be deleted.
     */
    removeFormula(formula: FormulaDescriptor): void {

        // remove the formula descriptor from the own maps
        this.formulaSet.delete(formula);
        this.recalcAlwaysMap.delete(formula);
        this.recalcOnceMap.delete(formula);

        // update map of missing name labels
        for (const nameKey of formula.missingNames) {
            map.visit(this.missingNamesMap, nameKey, formulas => {
                formulas.delete(formula);
                if (!formulas.size) { this.missingNamesMap.delete(nameKey); }
            });
        }

        // remove the formula from the type-specific containers
        if (formula instanceof CellFormulaDescriptor) {
            this.addressSet.delete(formula.cellAddress);
        } else if (formula instanceof ModelFormulaDescriptor) {
            map.visit(this.modelFormulasMap, formula.tokenModel, formulas => {
                formulas.delete(formula);
                if (!formulas.size) { this.modelFormulasMap.delete(formula.tokenModel); }
            });
        } else {
            depsLogger.error("$badge{SheetDescriptor} removeFormula: unknown formula type");
        }

        depsLogger.trace(() => `removed dependencies for ${formula}`);
    }
}
