/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { set, jpromise } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import type { AttrSetConstraint } from "@/io.ox/office/editframework/utils/attributeutils";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { XSourceLinkedModelEventMap, XSourceLinkedModel } from "@/io.ox/office/spreadsheet/model/drawing/xsourcelinkedmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

import { depsLogger, getAddressesLabel, getLinkLabel, getNameLabel, getTableLabel, getRuleLabel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { type ValueDescriptor, type DependencyStoreEventMap, DependencyStore } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencystore";
import { type DependencyWorkerOptions, type DependencyWorkerEventMap, DependencyWorker } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyworker";

// types ======================================================================

/**
 * Options for recalculating the formulas in a spreadsheet document.
 */
export interface RecalcFormulasOptions {

    /**
     * If set to `true`, all available formulas in the document will be
     * recalculated, regardless of their dirty state. Default value is `false`.
     */
    all?: boolean;

    /**
     * If set to `false`, volatile formulas will NOT be recalculated (unless
     * they depend on dirty cells). This option will be ignored, if the option
     * "all" has been set. *Default value is `true`!*
     */
    volatile?: boolean;

    /**
     * The maximum time allowed for the recalculation cycle, in milliseconds.
     * If the time elapses before all formulas have been calculated, the
     * recalculation cycle will be aborted, and the document flag "calcOnLoad"
     * will be set.
     */
    timeout?: number;
}

/**
 * Type mapping for the events emitted by `DependencyManager` instances.
 */
export interface DependencyManagerEventMap extends DependencyStoreEventMap, DependencyWorkerEventMap { }

// class DependencyManager ====================================================

/**
 * Collects all change events in the document, and recalculates all formula
 * expressions depending on these changes.
 */
export class DependencyManager extends ModelObject<SpreadsheetModel, DependencyManagerEventMap> implements Disconnectable {

    /**
     * The complete storage and state of a dependency manager instance. This
     * object will be shared with all asynchronous workers used by this
     * dependency manager.
     */
    readonly #depStore: DependencyStore;

    /**
     * The asynchronous worker that implements the complete formula update
     * cycle, after changes have been detected in the document model.
     */
    readonly #updateWorker: DependencyWorker;

    /**
     * Whether this dependency manager is connected to the document model and
     * processes its events.
     *
     * The dependency manager will be disconnected if the number of formulas in
     * the document exceeds a certain limit for performance reasons.
     */
    #connected = true;

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The spreadsheet document model containing this instance.
     */
    constructor(docModel: SpreadsheetModel) {
        super(docModel);

        // private properties
        this.#depStore = this.member(new DependencyStore(docModel));
        this.#updateWorker = this.member(new DependencyWorker(docModel, this.#depStore));

        // forward all recalc events emitted from update worker
        this.listenToAllEvents(this.#updateWorker, this.trigger);

        // If the maximum allowed number of formulas (according to server configuration) has
        // been exceeded, this dependency manager will be detached from all document model
        // events (this prevents processing any further edit actions from the document).
        this.#depStore.on("recalc:overflow", () => {
            // detach and deactivate dependency manager
            this.disconnect();
            // immediately abort a running update worker
            this.#updateWorker.abort();
            // notify external listeners
            this.trigger("recalc:overflow");
        });

        // connect to document events after successful document import
        this.waitForImportSuccess(this.#connect);
    }

    // public methods ---------------------------------------------------------

    /**
     * Switches this dependency manager to permenent disconnected state. A
     * disconnected dependency manager will not collect or process any formulas
     * inserted into the document anymore. Once the dependency manager is in
     * disconnected state, it cannot return to active state.
     */
    disconnect(): void {
        this.stopListening();
        this.#depStore.clearAllFormulas();
        this.#connected = false;
    }

    /**
     * Returns whether this dependency manager is in active state, i.e. it
     * listens to changes in the document, and recalculates all formulas on
     * demand in a background task.
     *
     * @see `DependencyManager#disconnect`
     *
     * @returns
     *  Whether this dependency manager is in active state.
     */
    isConnected(): boolean {
        return this.#connected;
    }

    /**
     * Returns whether this document needs to be recalculated when it will be
     * opened the next time. Usually, this happens if the document will be
     * closed before the dependency manager finishes to recalculate all dirty
     * formulas.
     *
     * @returns
     *  Whether this document needs to be recalculated when opened.
     */
    needsCalcOnLoad(): boolean {
        // - when the flag is explicitly requested by this dependency manager
        // - when the initial update cycle has not been finished, and the flag has been imported
        // - when an update cycle is currently running and not yet finished
        return this.#depStore.calcOnLoadRequested || this.#depStore.needsInitialRecalc() || this.#updateWorker.running;
    }

    /**
     * Registers a new source-linked model (e.g. the data series model of a
     * chart object) for automatic updates after the referred cells have been
     * changed.
     *
     * @param linkModel
     *  The new linked model.
     */
    registerLinkedModel<
        AttrSetT extends AttrSetConstraint<AttrSetT>,
        EvtMapT extends XSourceLinkedModelEventMap<AttrSetT>,
        ModelT extends XSourceLinkedModel<AttrSetT, EvtMapT>
    >(linkModel: ModelT): void {

        // nothing to do in disconnected state
        if (!this.#connected) { return; }

        depsLogger.log(() => `$badge{DepManager} inserted source link ${getLinkLabel(linkModel)}`);
        this.#depStore.registerTokenModel(linkModel);
        this.listenTo(linkModel, "change:sourcelinks", () => this.#depStore.registerTokenModel(linkModel, true));

        jpromise.floating(this.#start());
    }

    /**
     * Unregisters a source-linked model (e.g. the data series model of a chart
     * object) that will be deleted from the spreadsheet document.
     *
     * @param linkModel
     *  The linked model to be deleted.
     */
    unregisterLinkedModel<
        AttrSetT extends AttrSetConstraint<AttrSetT>,
        EvtMapT extends XSourceLinkedModelEventMap<AttrSetT>,
        ModelT extends XSourceLinkedModel<AttrSetT, EvtMapT>
    >(linkModel: ModelT): void {

        // nothing to do in disconnected state
        if (!this.#connected) { return; }

        depsLogger.log(() => `$badge{DepManager} deleted source link ${getLinkLabel(linkModel)}`);
        this.stopListeningTo(linkModel);
        this.#depStore.unregisterTokenModel(linkModel);

        jpromise.floating(this.#start());
    }

    /**
     * Recalculates all dirty formulas in the document, and refreshes all cells
     * with outdated formula results, and all other document contents that
     * depend on formula results (e.g. charts, or conditional formatting). The
     * background task that calculates the formula results will run with high
     * priority (in difference to background tasks started automatically due to
     * change events of the document processed by this dependency manager).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil when all dirty formulas have been
     *  recalculated successfully.
     */
    @depsLogger.profileMethod("$badge{DepManager} recalcFormulas")
    recalcFormulas(options?: RecalcFormulasOptions): Promise<void> {

        // nothing to do in disconnected state
        if (!this.#connected) { return Promise.resolve(); }

        // initialize the "recalcAll" flag in the internal data
        if (options?.all) {
            this.#depStore.pendingCollection.recalcAll = true;
        }

        // start the update cycle, or continue a running update cycle
        let promise = this.#start({
            continue: !this.#depStore.pendingCollection.recalcAll,
            priority: true,
            volatile: options?.volatile ?? true
        });

        // initialize a timer that aborts the returned promise after the specified delay
        if (options?.timeout) {

            // create a promise that will reject after the delay time
            const timeout = new Promise<void>((_resolve, reject) => {
                this.setTimeout(() => {
                    // set the "calcOnLoad" document flag if the timeout is reached
                    this.#depStore.calcOnLoadRequested = true;
                    reject(new Error("timeout"));
                }, options.timeout);
            });

            // combine update task and timeout promise into a single promise
            promise = Promise.race([promise, timeout]);
        }

        return promise;
    }

    /**
     * Returns an up-to-date cell value for the specified cell in the document.
     * If the cell is a formula cell that this dependency manager has marked as
     * dirty, the formula will be recalculated, and all cells referred by the
     * formula that are dirty too will be recalculated recursively. Otherwise,
     * the current value of the cell as stored in the cell model will be
     * returned.
     *
     * @param sheetModel
     *  The model of the sheet containing the cell to be resolved.
     *
     * @param address
     *  The address of the cell whose value will be resolved.
     *
     * @returns
     *  A descriptor object with information about the resolved cell value.
     */
    getCellValue(sheetModel: SheetModel, address: Address): ValueDescriptor {
        return this.#depStore.getCellValue(sheetModel, address);
    }

    /**
     * Returns the resource keys of all unimplemented functions that occur in
     * any formula known by this dependency manager.
     *
     * @returns
     *  The resource keys of all unimplemented functions that occur in any
     *  formula known by this dependency manager.
     */
    getMissingFunctionKeys(): string[] {

        // collect all function keys from all sheets
        const missingFuncs = new Set<string>();
        for (const sheetDesc of this.#depStore.sheetDescMap.values()) {
            set.assign(missingFuncs, sheetDesc.missingFuncs);
        }

        // return the function resource keys as array
        return Array.from(missingFuncs);
    }

    // private methods --------------------------------------------------------

    /**
     * Starts the background task that processes all collected and pending
     * document events, refreshes the dependency chains, recalculates all dirty
     * formulas, and updates the model contents (cells, conditional formatting
     * rules, etc.).
     *
     * @param [options]
     *  Optional parameters.
     */
    #start(options?: DependencyWorkerOptions): Promise<void> {
        // nothing to do in disconnected state; worker debounces the `start` call internally
        return this.#connected ? this.#updateWorker.start(options) : Promise.resolve();
    }

    /**
     * Connects this instance to the document model (registers all needed event
     * handlers).
     */
    #connect(): void {

        // initialize dependency manager for all existing sheets after import
        for (const sheetModel of this.docModel.yieldSheetModels()) {
            this.#depStore.registerSheetModel(sheetModel);
        }

        // recalculate all formulas with new formula resources/grammars
        // (TEXT or FORMULATEXT functions may return different results)
        this.listenTo(this.docModel, "change:locale", () => {
            depsLogger.log("$event{change:locale} changed locale");
            jpromise.floating(this.recalcFormulas({ all: true }));
        });

        // inserted sheet: register all existing formulas (e.g. in a copied sheet)
        this.listenTo(this.docModel, "insert:sheet", event => {
            depsLogger.log(() => `$event{insert:sheet} inserted sheet "${event.sheetName}"`);
            // register the new sheet model for lazy update
            this.#depStore.registerSheetModel(event.sheetModel);
            jpromise.floating(this.#start());
        });

        // deleted sheet: remove all registered formulas of the sheet
        this.listenTo(this.docModel, "delete:sheet:before", event => {
            depsLogger.log(() => `$event{delete:sheet:before} deleted sheet "${event.sheetName}"`);
            // delete the sheet model and related data from all collections
            this.#depStore.unregisterSheetModel(event.sheetModel);
            jpromise.floating(this.#start());
        });

        // recalculate all volatile formulas after sheet has been moved (e.g. SHEET function)
        this.listenTo(this.docModel, "move:sheets:before", () => {
            depsLogger.log("$event{move:sheets:before} moved sheets");
            // simply recalculate all volatile formulas, nothing more to do
            jpromise.floating(this.#start());
        });

        // recalculate all volatile formulas after sheet has been renamed (e.g. INDIRECT function)
        this.listenTo(this.docModel, "rename:sheet", event => {
            depsLogger.log(() => `$event{rename:sheet} changed sheet name to "${event.sheetModel.getName()}"`);
            // simply recalculate all volatile formulas, nothing more to do
            jpromise.floating(this.#start());
        });

        // new defined name: update formulas with #NAME! error referring to the new name
        this.listenTo(this.docModel, "insert:name", event => {
            depsLogger.log(() => `$event{insert:name} inserted ${getNameLabel(event.nameModel)}`);
            // register the model of the new defined name for lazy update
            this.#depStore.pendingCollection.registerNameModel(event.nameModel, true, true);
            jpromise.floating(this.#start());
        });

        // deleted defined name: unregister the name model, and update all affected formulas
        this.listenTo(this.docModel, "delete:name:before", event => {
            depsLogger.log(() => `$event{delete:name:before} deleted ${getNameLabel(event.nameModel)}`);
            // delete the name model from the collection of pending document actions
            this.#depStore.pendingCollection.unregisterNameModel(event.nameModel);
            jpromise.floating(this.#start());
        });

        // changed defined name: update all formulas referring to the name
        this.listenTo(this.docModel, "change:name", event => {
            depsLogger.log(() => `$event{change:name} changed ${getNameLabel(event.nameModel)}`);
            // register the model of the changed defined name for lazy update
            this.#depStore.pendingCollection.registerNameModel(event.nameModel, !!event.label, !!event.formula);
            jpromise.floating(this.#start());
        });

        // deleted table model: unregister the table model (do not process pending change events)
        this.listenTo(this.docModel, "delete:table:before", event => {
            depsLogger.log(() => `$event{delete:table:before} deleted ${getTableLabel(event.tableModel)}`);
            // delete the table model from the collection of pending document actions
            this.#depStore.pendingCollection.unregisterTableModel(event.tableModel);
            jpromise.floating(this.#start());
        });

        // changed table model: update formulas if the target range of the table has changed
        this.listenTo(this.docModel, "change:table", event => {
            if (event.range) {
                depsLogger.log(() => `$event{change:table} changed target range of ${getTableLabel(event.tableModel)}`);
                // register the model of the changed table range for lazy update
                this.#depStore.pendingCollection.registerTableModel(event.tableModel);
                jpromise.floating(this.#start());
            }
            if (event.name) {
                depsLogger.log(() => `$event{change:table} changed name of ${getTableLabel(event.tableModel)}`);
                // functions like FORMULATEXT() may change
                jpromise.floating(this.#start());
            }
        });

        // register changed cell values and cell formulas
        this.listenTo(this.docModel, "change:cells", event => {
            // ignore the change events that have been generated due to the own last update cycle
            if (event.results) { return; }
            depsLogger.log(() => event.valueCells.empty() ? undefined : `$event{change:cells} changed values in ${getAddressesLabel(event.sheetModel, event.valueCells)}`);
            depsLogger.log(() => event.formulaCells.empty() ? undefined : `$event{change:cells} changed formulas in ${getAddressesLabel(event.sheetModel, event.formulaCells)}`);
            // register the addresses of all dirty value and formula cells
            if (this.#depStore.pendingCollection.registerDirtyCells(event.sheetModel, event)) {
                jpromise.floating(this.#start());
            }
        });

        // moved cells, including inserted or deleted columns/rows
        this.listenTo(this.docModel, "move:cells", event => {
            depsLogger.log(() => `$event{move:cells} moved cells in ${event.sheetModel.getName()}!${event.transformer.dirtyRange}`);
            // move the addresses of all dirty value and formula cells
            this.#depStore.pendingCollection.moveDirtyCells(event.sheetModel, event.transformer);
            jpromise.floating(this.#start());
        });

        // recalculate all volatile formulas after rows have been shown or hidden (e.g. SUBTOTAL function)
        this.listenTo(this.docModel, "change:rows", event => {
            // restrict to changed visibility of the rows
            if (!event.visibility) { return; }
            depsLogger.log(() => `$event{change:rows} changed row visibility in ${event.sheetModel.getName()}!${event.intervals.join()}`);
            // simply recalculate all volatile formulas, nothing more to do
            jpromise.floating(this.#start());
        });

        // new formatting rule: register all formula expressions of the conditions
        this.listenTo(this.docModel, "insert:cfrule", event => {
            depsLogger.log(() => `$event{insert:cfrule} inserted ${getRuleLabel(event.ruleModel)}`);
            this.#depStore.registerTokenModel(event.ruleModel);
            jpromise.floating(this.#start());
        });

        // deleted formatting rule: unregister all formula expressions of the conditions
        this.listenTo(this.docModel, "delete:cfrule", event => {
            depsLogger.log(() => `$event{delete:cfrule} deleted ${getRuleLabel(event.ruleModel)}`);
            this.#depStore.unregisterTokenModel(event.ruleModel);
            jpromise.floating(this.#start());
        });

        // changed formatting rule (target range, or formula expressions of conditions)
        this.listenTo(this.docModel, "change:cfrule", event => {
            depsLogger.log(() => `$event{change:cfrule} changed ${getRuleLabel(event.ruleModel)}`);
            this.#depStore.registerTokenModel(event.ruleModel, true);
            jpromise.floating(this.#start());
        });

        // initial update cycle for the imported sheets
        jpromise.floating(this.#start());
    }
}
