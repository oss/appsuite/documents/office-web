/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, itr, jpromise } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";
import { EObject, debounceMethod } from "@/io.ox/office/tk/objects";
import { type AsyncWorkerConfig, PromiseExecutor, AsyncWorker, breakLoop } from "@/io.ox/office/tk/workers";
import { UNITTEST, getDebugFlag } from "@/io.ox/office/tk/config";
import { LogLevel } from "@/io.ox/office/tk/utils/logger";

import { isErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

import { CellType } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { OperationBuilder } from "@/io.ox/office/spreadsheet/model/operation/builder";
import type { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

import type { TokenModel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { depsLogger, getCellKey, getAddressesLabel, getRangesLabel } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencyutils";
import { type FormulaDescriptor, CellFormulaDescriptor, ModelFormulaDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladescriptor";
import type { SheetDescriptor } from "@/io.ox/office/spreadsheet/model/formula/deps/sheetdescriptor";
import type { PendingSheetData } from "@/io.ox/office/spreadsheet/model/formula/deps/pendingcollection";
import type { FormulaDictionary } from "@/io.ox/office/spreadsheet/model/formula/deps/formuladictionary";
import type { ReferenceCycle } from "@/io.ox/office/spreadsheet/model/formula/deps/resultcollection";
import type { DependencyStore } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencystore";

import type SpreadsheetApp from "@/io.ox/office/spreadsheet/app/application";

// types ======================================================================

export interface DependencyWorkerOptions {

    /**
     * If set to `true`, an update cycle currently running will be continued.
     * By default, a running update cycle will be aborted, and a fresh update
     * cycle will be started.
     */
    continue?: boolean;

    /**
     * If set to `true`, the update cycle will be started with high timer
     * priority. If used together with the option "continue", the continued
     * update cycle will be swiched from low to high priority.
     */
    priority?: boolean;

    /**
     * If set to `false`, volatile formulas will NOT be recalculated (unless
     * they depend on dirty cells). This option will affect new update cycles
     * only, but not a running continued update cycle. *Default value is
     * `true`!*
     */
    volatile?: boolean;
}

/**
 * Type mapping for the events emitted by `DependencyWorker` instances.
 */
export interface DependencyWorkerEventMap {

    /**
     * Will be emitted if a calculation cycle has been started. This event will
     * be followed by either one "recalc:end" event, or by one "recalc:cancel"
     * event.
     */
    "recalc:start": [];

    /**
     * Will be emitted if a calculation cycle has been finished successfully.
     *
     * @param refCycles
     *  All reference cycles found during calculation of dirty formulas.
     */
    "recalc:end": [refCycles: ReferenceCycle[]];

    /**
     * Will be emitted if a calculation cycle has been canceled.
     */
    "recalc:cancel": [];

    /**
     * Will be triggered after finishing a specific worker stage. MUST be used
     * for debugging and testing ONLY!
     *
     * @param stage
     *  The identifier of the stage that has been finished.
     *  - Stage "collect": Pending dirty formulas have been collected.
     *  - Stage "calculate": All dirty formulas have been recalculated.
     *  - Stage "send": New formula results have been sent to the server.
     */
    "recalc:stage": [stage: "collect" | "calculate" | "send"];
}

// constants ==================================================================

// debounce delay for starting the main worker
const DEBOUNCE_DELAY = UNITTEST ? 10 : 100;

// initial delay for the first update cycle after import
const IMPORT_DELAY = UNITTEST ? 0 : 900;

// timer settings for regular (low-priority) runs (60% workload)
const STANDARD_TIMER_CONFIG: AsyncWorkerConfig = { slice: 300, interval: 200 };

// timer settings for important (high-priority) runs (80% workload)
const PRIORITY_TIMER_CONFIG: AsyncWorkerConfig = { slice: 400, interval: 100 };

// timer settings for unit test runs (96% workload)
const UNITTEST_TIMER_CONFIG: AsyncWorkerConfig = { slice: 240, interval: 10 };

// timer settings for debugging (2% workload)
const SLOW_TIMER_CONFIG: AsyncWorkerConfig = { slice: 10, interval: 490 };

// class BaseWorker ===========================================================

class BaseWorker<MT = void> extends AsyncWorker<MT> {

    /** The spreadsheet application instance. */
    protected readonly docApp: SpreadsheetApp;
    /** The document model. */
    protected readonly docModel: SpreadsheetModel;
    /** The shared dependency data storage. */
    protected readonly depStore: DependencyStore;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, depStore: DependencyStore) {
        super();
        this.docApp = docModel.docApp;
        this.docModel = docModel;
        this.depStore = depStore;
    }
}

// class PendingSheetWorker ===================================================

/**
 * An asynchronous worker that processes the change events of a single sheet
 * that have been collected in a `PendingSheetData` instance.
 */
class PendingSheetWorker extends BaseWorker<PendingSheetData> {

    // used to prevent double registration from "dirtyRanges" and "dirtyCells"
    readonly #registeredCellSet = new Set<string>();

    // execution cycle metadata
    #pendingData!: PendingSheetData;
    #sheetModel!: SheetModel;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, depStore: DependencyStore) {
        super(docModel, depStore);

        // prepare internal members
        this.steps.addStep(runner => {

            // sheet model and cell collection
            const pendingData = this.#pendingData = runner.metadata;
            this.#sheetModel = pendingData.sheetModel;

            // the addresses of the dirty (moved) cell ranges
            pendingData.dirtyRanges = pendingData.dirtyRanges.merge();

            this.#registeredCellSet.clear();
        });

        // register all relevant formulas of a *new* sheet (e.g. a copied sheet, bug 51453)
        this.steps.addStep(() => {

            const pendingData = this.#pendingData;
            if (!pendingData.initialUpdate) { return; }
            pendingData.initialUpdate = false;

            depsLogger.log(() => `collecting all formulas of new sheet "${this.#sheetModel.getName()}"...`);

            // collect the formulas of the entire sheet range
            pendingData.dirtyRanges = pendingData.dirtyRanges.append(this.#sheetModel.cellCollection.getUsedRange()).merge();

            // collect the formatting rules in the new sheet (copied sheets may contain conditional formatting)
            for (const ruleModel of this.#sheetModel.cfRuleCollection.yieldRuleModels()) {
                pendingData.registerTokenModel(ruleModel);
            }
        });

        // remove formula descriptors of all dirty formula cells
        this.steps.addStep(depsLogger.profileMethod("removing dirty formula cells...", () => {
            this.#unregisterFormulaCells(this.#pendingData.dirtyFormulaCells);
        }, { when: () => this.#pendingData.dirtyFormulaCells.length }));

        // remove all formula descriptors inside the dirty ranges
        this.steps.addStep(depsLogger.profileMethod("removing formulas in dirty ranges...", () => {
            const sheetDesc = depStore.sheetDescMap.get(this.#sheetModel);
            for (const dirtyRange of this.#pendingData.dirtyRanges) {
                // collect all addresses before unregistering the formulas (`AddressSet#ordered` not stable against deletion during iteration)
                const dirtyAddresses = Array.from(sheetDesc!.addressSet.ordered({ range: dirtyRange }));
                this.#unregisterFormulaCells(dirtyAddresses);
            }
        }, { when: () => this.#pendingData.dirtyRanges.length }));

        // register the changed formula cells (before dirty ranges; always recalculate all changed formula cells)
        this.steps.addStep(depsLogger.profileMethod("registering dirty cells...", () => {
            const dirtyCells = this.#pendingData.dirtyFormulaCells;
            this.#registerFormulaCells(dirtyCells, true);
            dirtyCells.clear();
        }, { when: () => this.#pendingData.dirtyFormulaCells.length }));

        // collect the formula cells in the moved ranges at their current positions
        this.steps.addStep(depsLogger.profileMethod("collecting dirty formula cells...", () => {
            const dirtyRanges = this.#pendingData.dirtyRanges;
            const addressIt = itr.map(this.#sheetModel.cellCollection.cellEntries(dirtyRanges, { type: CellType.FORMULA }), entry => entry.address);
            this.#registerFormulaCells(addressIt, depStore.needsInitialRecalc());
            dirtyRanges.clear();
        }, { when: () => this.#pendingData.dirtyRanges.length }));

        // register the formulas of all formatting rules and source links
        this.steps.addStep(depsLogger.profileMethod("collecting dirty token models...", () => {
            for (const tokenModel of this.#pendingData.dirtyModelSet) {
                const modelUid = tokenModel.uid;
                const refAddress = tokenModel.getRefAddress();
                for (const [key, tokenArray] of tokenModel.tokenArrays()) {
                    const formulaKey = `${modelUid}!${key}`;
                    const formula = new ModelFormulaDescriptor(formulaKey, tokenArray, refAddress, tokenModel, key);
                    // DOCS-4924: immediately escape the loop, if the number of formulas exceeds the configuration limit
                    if (!formula.isFixed() && !depStore.insertFormula(formula)) { return; }
                }
            }
        }, { when: () => this.#pendingData.dirtyModelSet.size }));
    }

    // public methods ---------------------------------------------------------

    // overridden for debug logging
    @depsLogger.profileMethod("$badge{PendingSheetWorker} start (phase 1)")
    override start(pendingData: PendingSheetData): Promise<void> {
        return super.start(pendingData);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a formula key for the current sheet and specified cell address.
     */
    #getCellKey(address: Address): string {
        return getCellKey(this.#sheetModel, address);
    }

    /**
     * Returns whether the cell value type matches the root operator of the
     * formula (bug 49463).
     */
    #isMatchingCellResult(tokenArray: TokenArray, address: Address): boolean {

        // missing cell value (no further checks needed)
        const cellValue = this.#sheetModel.cellCollection.getValue(address);
        if (cellValue === null) { return false; }

        // formulas may always result in an error
        if (isErrorCode(cellValue)) { return true; }

        // get the expected scalar result type from the root operator
        const rootRes = tokenArray.compileFormula().root.resource;
        const valType = rootRes ? rootRes.valType : "any";

        // compare the expected result type with the cell value type
        switch (valType) {
            case "num":
            case "date":    return is.number(cellValue);
            case "str":
            case "comp":    return is.string(cellValue);
            case "bool":    return is.boolean(cellValue);
            case "err":     return isErrorCode(cellValue);
            default:        return true;
        }
    }

    /**
     * Inserts the specified cell formulas into the internal collections.
     */
    #registerFormulaCells(addresses: Iterable<Address>, recalcAll = false): void {

        const { cellCollection } = this.#sheetModel;

        for (const address of addresses) {

            // the cell key will be used as formula descriptor key (prevent double registration)
            const formulaKey = this.#getCellKey(address);
            if (this.#registeredCellSet.has(formulaKey)) { continue; }
            this.#registeredCellSet.add(formulaKey);

            // nothing more to do, if there is no token array available for the key, e.g. deleted cell formulas)
            const tokenDesc = cellCollection.getTokenArray(address);
            if (!tokenDesc) { continue; }

            // create a new descriptor for the token array
            const formula = new CellFormulaDescriptor(formulaKey, tokenDesc.tokenArray, tokenDesc.refAddress, address, tokenDesc.matrixRange);

            // recalculate formula cell, if it has been registered in preceding update cycle (bug 56767: even if it is fixed now)
            let recalcFormula = recalcAll || this.#pendingData.recalcAddressSet.has(address);

            // recalculate ALL formula cells, if there is a cell without a valid result type
            if (!recalcFormula && this.depStore.initialUpdateCycle && !tokenDesc.matrixRange && !this.#isMatchingCellResult(tokenDesc.tokenArray, address)) {
                depsLogger.warn(() => `recalculating all formulas caused by wrong result type in ${formula}`);
                recalcFormula = recalcAll = this.depStore.pendingCollection.recalcAll = true;
            }

            // ignore the formula if it does not contain any dependencies
            if (recalcFormula || !formula.isFixed()) {
                // immediately escape the loop, if the number of formulas exceeds the configuration limit
                if (!this.depStore.insertFormula(formula)) { return; }
            } else {
                depsLogger.trace(() => `skipped fixed formula ${formula}`);
            }

            // register the new formula for recalculation
            if (recalcFormula) {
                this.#pendingData.recalcAddressSet.add(address);
            }
        }
    }

    /**
     * Removes the specified cell formulas from the internal collections.
     */
    #unregisterFormulaCells(addresses: Iterable<Address>): void {
        for (const address of addresses) {
            const formula = this.depStore.formulaSet.get(this.#getCellKey(address));
            if (formula) { this.depStore.removeFormula(formula); }
        }
    }
}

// class RecalcDirtyWorker ====================================================

/**
 * An asynchronous worker that recalculates all collected dirty formulas.
 *
 * Collects the descriptors of all dirty formulas depending directly on changed
 * cells, or indirectly on other dirty formulas, in the `ResultCollection` of
 * the shared storage object, and recalculates all collected dirty formulas.
 */
class RecalcDirtyWorker extends BaseWorker {

    // copy of the formula dictionary
    #lookupDictionary!: FormulaDictionary;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, depStore: DependencyStore) {
        super(docModel, depStore);

        // Recalculate-all mode: Calculate and collect the results of all cell formulas in the document.
        this.steps.addStep(depsLogger.profileMethod("recalculate all formulas...", runner => {

            // recalculate all formulas only if respective flag in pending collection is set
            if (!depStore.pendingCollection.recalcAll) { return; }

            const sheetRange = this.docModel.addressFactory.getSheetRange();
            const promise = runner.iterate(this.docModel.yieldSheetModels(), sheetModel => {
                const cellIter = sheetModel.cellCollection.cellEntries(sheetRange, { type: CellType.FORMULA });
                for (const cellEntry of cellIter) {
                    this.depStore.getCellValue(sheetModel, cellEntry.address);
                }
            });

            // stop the worker (skip all following tests)
            return jpromise.fastThen(promise, breakLoop);
        }, { when: () => depStore.pendingCollection.recalcAll }));

        // Create a copy of the formula dictionary that will be reduced during lookup to improve performance.
        this.steps.addStep(depsLogger.profileMethod("clone dictionary...", () => {
            this.#lookupDictionary = depStore.formulaDictionary.clone();
        }));

        // Collect all formulas depending on defined names with changed formula expression.
        this.steps.addStep(depsLogger.profileMethod("collecting formulas for dirty names...", () => {
            for (const nameKey of depStore.pendingCollection.changedNameKeys) {
                this.#registerDirtyFormulas(this.#lookupDictionary.findFormulasForName(nameKey));
            }
        }, { when: () => depStore.pendingCollection.changedNameKeys.size }));

        // Collect all formulas depending on table ranges with changed target range.
        this.steps.addStep(depsLogger.profileMethod("collecting formulas for dirty table ranges...", () => {
            for (const tableKey of depStore.pendingCollection.changedTableKeys) {
                this.#registerDirtyFormulas(this.#lookupDictionary.findFormulasForTable(tableKey));
            }
        }, { when: () => depStore.pendingCollection.changedTableKeys.size }));

        // Refresh the dependencies of all found formulas for dirty names and tables. The dependencies will include the new
        // source references of the changed defined names, and the target ranges of the changed tables.
        this.steps.addStep(depsLogger.profileMethod("refreshing formulas for dirty names and tables...",
            runner => runner.iterate(depStore.resultCollection.formulaSet, formula => depStore.refreshFormula(formula))
        ));

        // Collect all formulas with a #NAME? error, after a defined name changed its label, and refresh
        // their dependencies too, in order to resolve #NAME? errors to valid name references.
        this.steps.addStep(depsLogger.profileMethod("refreshing formulas for dirty name labels...",
            runner => runner.iterate(depStore.sheetDescMap.values(), sheetDesc => this.#collectDirtyNameLabels(sheetDesc))
        ));

        // Collect all initially dirty formulas from all sheets in the document.
        this.steps.addStep(depsLogger.profileMethod("collecting initially dirty formulas...",
            runner => jpromise.fastThen(
                runner.iterate(depStore.sheetDescMap.values(), sheetDesc => this.#collectInitiallyDirty(sheetDesc)),
                () => depsLogger.logFormulas(depStore.resultCollection.formulaSet)
            )
        ));

        // Extend the map of dirty formulas with other formulas depending on them.
        this.steps.addStep(depsLogger.profileMethod("collecting dependent dirty formulas...",
            runner => runner.iterate(this.#collectDependentDirtyLoop(), fun.undef),
            { when: () => depStore.resultCollection.cellFormulaSet.size }
        ));

        // Calculate and collect all formula results, as long as dirty formulas exist.
        this.steps.addStep(depsLogger.profileMethod("recalculating all dirty formulas...",
            runner => runner.iterate(itr.yieldFirst(depStore.resultCollection.cellFormulaSet), formula => {
                // side-effect call: "getCellValue" will create a result object, and will remove the entry
                // from "cellFormulaSet" so that the iterator loop will not run forever
                this.depStore.getCellValue(formula.sheetModel, formula.cellAddress);
            }),
            { when: () => depStore.resultCollection.cellFormulaSet.size }
        ));
    }

    // public methods ---------------------------------------------------------

    // overridden for debug logging
    @depsLogger.profileMethod("$badge{RecalcDirtyWorker} start (phase 2)")
    override start(): Promise<void> {
        return super.start();
    }

    // private methods --------------------------------------------------------

    /**
     * Inserts a new formula descriptor into the dirty formula map, and removes
     * it from the lookup dictionary.
     */
    #registerDirtyFormula(formula: FormulaDescriptor): void {
        this.#lookupDictionary.removeFormula(formula);
        this.depStore.resultCollection.insertFormula(formula);
    }

    /**
     * Inserts a map of formula descriptors into the dirty formula map, and
     * removes them from the lookup dictionary.
     */
    #registerDirtyFormulas(formulas: Opt<KeySet<FormulaDescriptor>>): void {
        if (formulas) {
            for (const formula of formulas) {
                this.#registerDirtyFormula(formula);
            }
        }
    }

    /**
     * Collects all formulas with #NAME? errors from a sheet.
     */
    #collectDirtyNameLabels(sheetDesc: SheetDescriptor): void {

        const { pendingCollection, resultCollection } = this.depStore;

        // process all defined names whose label has changed
        for (const nameModel of pendingCollection.changedLabelsSet) {

            // visit formulas with #NAME? errors using the current label...
            const destFormulas = sheetDesc.missingNamesMap.get(nameModel.key);
            if (!destFormulas) { continue; }

            for (const formula of destFormulas) {

                // the formula may have been refreshed already (multiple unresolved names)
                if (resultCollection.formulaSet.has(formula)) { continue; }

                // refresh formula dependencies
                this.depStore.refreshFormula(formula);

                // register formula as dirty if it refers to an existing name now
                if (formula.references.containsName(nameModel)) {
                    this.#registerDirtyFormula(formula);
                }
            }
        }
    }

    /**
     * Collects all initially dirty formulas from a sheet.
     */
    #collectInitiallyDirty(sheetDesc: SheetDescriptor): void {

        const { pendingCollection } = this.depStore;
        const { sheetModel } = sheetDesc;

        // Add all formulas that are permanently dirty (recalculation mode "always"). These formulas will
        // always be recalculated, regardless whether they depend on any dirty cells.
        if (this.depStore.initialUpdateCycle || pendingCollection.recalcVolatile) {
            this.#registerDirtyFormulas(sheetDesc.recalcAlwaysMap);
        }

        // Add all formulas that are initially dirty (recalculation mode "once"). These formulas will be
        // recalculated once after importing the document.
        if (this.depStore.initialUpdateCycle) {
            this.#registerDirtyFormulas(sheetDesc.recalcOnceMap);
        }

        // collected cell addresses in the sheet from document change actions
        const pendingSheetData = pendingCollection.getPendingSheetData(sheetModel);

        // the addresses of all formula cells that will be recalculated
        const recalcAddressSet = new KeySet(pendingSheetData?.recalcAddressSet);

        // Collect the dirty value cells. Dirty value cells that are formulas by themselves need
        // to be recalculated (the correct formula result may have been destroyed).
        const dirtyValueCells = pendingSheetData ? pendingSheetData.dirtyValueCells.filter(address => {
            if (this.depStore.formulaSet.get(getCellKey(sheetModel, address))) {
                recalcAddressSet.add(address);
                return false;
            }
            return true;
        }) : [];

        // nothing more to do without changed formula nor value cells
        if (!recalcAddressSet.size && !dirtyValueCells.length) { return; }

        // join the value cells to ranges for faster lookup of dependent formulas
        const dirtyValueRanges = RangeArray.mergeAddresses(dirtyValueCells);

        depsLogger.trace(() => recalcAddressSet.size ? `outdated formula results in ${getAddressesLabel(sheetModel, recalcAddressSet)}` : undefined);
        depsLogger.trace(() => dirtyValueRanges.length ? `dirty cells in ${getRangesLabel(sheetModel, dirtyValueRanges)}` : undefined);

        // Collect all dirty cell formulas. Cells with changed formula expressions must be recalculated in
        // order to get a valid initial result.
        for (const address of recalcAddressSet) {
            const formula = sheetDesc.formulaSet.get(getCellKey(sheetModel, address));
            if (formula) { this.#registerDirtyFormula(formula); }
        }

        // collect all formulas depending directly on dirty value cells
        for (const range of dirtyValueRanges) {
            this.#registerDirtyFormulas(this.#lookupDictionary.findFormulasForRange(sheetModel, range));
        }
    }

    /**
     * Extends the map of dirty formulas with other formulas depending on them.
     * Generator wrapper running repeatedly as long as more dependent formulas
     * have been collected. Intended to be used in asynchronous loops.
     */
    *#collectDependentDirtyLoop(): IterableIterator<void> {

        // descriptors of all cell formulas to collect dependent formulas for
        let pendingFormulas = new KeySet<FormulaDescriptor>(this.depStore.resultCollection.cellFormulaSet);

        // repeat as long as new formulas can be found depending on the formulas in "pendingFormulas"
        for (let depth = 1; pendingFormulas.size; depth += 1) {

            const sourceFormulas = pendingFormulas;
            pendingFormulas = depsLogger.takeTime(`collecting dependent formulas for chain depth ${depth}`, () => {

                // all new formula descriptors not yet contained in the dirty formula map
                const newFormulas = new KeySet<FormulaDescriptor>();
                // result set with formulas already collected
                const collectedFormulaSet = this.depStore.resultCollection.formulaSet;

                // process all formulas whose dependencies have not been calculated yet
                for (const sourceFormula of sourceFormulas) {

                    // find all formulas that depend directly on the current formula (ignore formulas from token models)
                    const destFormulas = this.#lookupDictionary.findFormulasForFormula(sourceFormula);
                    if (!destFormulas) { continue; }

                    // process all found formulas depending on the current formula (skip formulas that have been collected already)
                    for (const destFormula of destFormulas) {
                        if (!collectedFormulaSet.has(destFormula)) {
                            this.#registerDirtyFormula(destFormula);
                            newFormulas.add(destFormula);
                        }
                    }
                }

                // initialize the next iteration cycle
                depsLogger.logFormulas(newFormulas);
                return newFormulas;
            });

            // yield on each loop step for usage in asynchronous loops
            yield;
        }
    }
}

// class SendResultsWorker ====================================================

/**
 * An asynchronous worker that sends all recalculated formula results to the
 * server, and applies them locally in the document.
 */
class SendResultsWorker extends BaseWorker {

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, depStore: DependencyStore) {
        super(docModel, depStore);

        // Wait for another operations generator process currently running (needed to prevent internal
        // application error due to nested operation generators), this may even abort the entire update
        // cycle, e.g. after deleting a sheet which makes the calculated formula results useless.
        this.steps.addStep(() => docModel.waitForActionsProcessed());

        // Build the "changeCell" operations for all results.
        this.steps.addStep(depsLogger.profileMethod("building operations...", () => {

            const { resultCollection } = this.depStore;

            // create "changeCells" operations for all formula result values
            const builder = new OperationBuilder(this.docModel);
            for (const [sheetModel, resultSet] of resultCollection.resultsMap) {
                const { cellCache } = builder.createSheetCache(sheetModel, { skipUndo: true, filter: true });
                for (const resultDesc of resultSet) {
                    cellCache.changeCell(resultDesc.address, { v: resultDesc.value });
                }
            }
            const { generator } = builder.flushOperations();
            builder.destroy();

            // send the operations to the server, but only if the document is in edit mode,
            // and the update cycle was caused by a local change
            if (this.docApp.isEditable() && this.docApp.isLocallyModified()) {
                depsLogger.takeTime("sending operations...", () => {
                    this.docModel.sendOperations(generator);
                });
            }

            // request the next application to recalculate all formulas, if calculation led to an unresolvable error
            this.depStore.calcOnLoadRequested = resultCollection.hasErrors;

            // always apply all new formula results locally
            try {
                depsLogger.takeTime("applying operations...", () => {
                    this.docModel.invokeOperationHandlers(generator);
                });
            } catch (err) {
                // should not happen ... disconnect or app error state?
                depsLogger.exception(err);
                this.depStore.calcOnLoadRequested = true;
            }
        }));

        // Refresh all model objects containing dirty formulas locally (without operations).
        this.steps.addStep(depsLogger.profileMethod("updating models with token arrays...", () => {

            // the collected dirty token models (prevent multiple refresh of same model)
            const tokenModelSet = new Set<TokenModel>();

            // collects the token models that contain the passed dirty formulas
            const registerTokenModels = (formulas: KeySet<ModelFormulaDescriptor>): void => {
                for (const formula of formulas) {
                    tokenModelSet.add(formula.tokenModel);
                    formula.tokenArray.clearResultCache();
                }
            };

            // in "recalculate all" mode, collect all models from the entire document
            if (this.depStore.pendingCollection.recalcAll) {
                for (const sheetDesc of this.depStore.sheetDescMap.values()) {
                    for (const formulas of sheetDesc.modelFormulasMap.values()) {
                        registerTokenModels(formulas);
                    }
                }
            } else {
                for (const formulas of this.depStore.resultCollection.modelFormulasMap.values()) {
                    registerTokenModels(formulas);
                }
            }

            // refresh the dirty models
            for (const tokenModel of tokenModelSet) {
                tokenModel.refreshFormulas();
            }
        }));
    }

    // public methods ---------------------------------------------------------

    // overridden for debug logging
    @depsLogger.profileMethod("$badge{SendResultsWorker} start (phase 3)")
    override start(): Promise<void> {
        return super.start();
    }
}

// class DependencyWorker =====================================================

export class DependencyWorker extends EObject<DependencyWorkerEventMap> implements Abortable {

    /** The shared dependency data storage. */
    readonly #depStore: DependencyStore;
    /** Main worker for the entire update cycle. */
    readonly #mainWorker: BaseWorker;
    /** Asynchronous worker for first sub-step (process `pendingCollection`). */
    readonly #pendingWorker: PendingSheetWorker;
    /** Asynchronous worker for second sub-step (recalculate collected formulas). */
    readonly #recalcWorker: RecalcDirtyWorker;
    /** Asynchronous worker for third sub-step (send and apply formula results). */
    readonly #resultsWorker: SendResultsWorker;

    /** The promise wrapper representing a complete update cycle. */
    #executor: Opt<PromiseExecutor<void>>;
    /** First update after import uses increased delay time. */
    #firstUpdate = true;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, depStore: DependencyStore) {
        super();

        // private properties
        this.#depStore = depStore;
        this.#mainWorker = this.member(new BaseWorker(docModel, depStore));
        this.#pendingWorker = this.member(new PendingSheetWorker(docModel, depStore));
        this.#recalcWorker = this.member(new RecalcDirtyWorker(docModel, depStore));
        this.#resultsWorker = this.member(new SendResultsWorker(docModel, depStore));

        // initial delay before starting to do anything (increased delay once after import)
        this.#mainWorker.steps.addDelay(() => {
            const delay = this.#firstUpdate ? IMPORT_DELAY : 0;
            this.#firstUpdate = false;
            return delay;
        });

        // trigger the start event (after the initial delay!)
        this.#mainWorker.steps.addStep(() => {
            this.trigger("recalc:start");
        });

        // wait for running document operation generators before starting update cycle
        this.#mainWorker.steps.addStep(depsLogger.profileMethod("waiting for document actions...",
            () => docModel.waitForActionsProcessed()
        ));

        // process all document change events that have been collected in the pending collection
        this.#mainWorker.steps.addIterator(
            () => depStore.pendingCollection.changedSheetMap.values(),
            this.#pendingWorker
        );
        this.#mainWorker.steps.addStep(() => void this.trigger("recalc:stage", "collect"));

        // recalculate all collected dirty formulas
        this.#mainWorker.steps.addStep(this.#recalcWorker);
        this.#mainWorker.steps.addStep(() => void this.trigger("recalc:stage", "calculate"));

        // send all new formula results to server, and apply locally
        this.#mainWorker.steps.addStep(this.#resultsWorker);
        this.#mainWorker.steps.addStep(() => void this.trigger("recalc:stage", "send"));

        // trigger the end event
        this.#mainWorker.steps.addStep(() => {
            const refCycles = depStore.finalizeUpdateCycle();
            this.trigger("recalc:end", refCycles);
            this.#executor?.resolve();
            this.#executor = undefined;
        });
    }

    // public getters ---------------------------------------------------------

    get running(): boolean {
        return this.#mainWorker.running;
    }

    // public methods ---------------------------------------------------------

    start(options?: DependencyWorkerOptions): Promise<void> {

        // optionally, continue with running worker
        if (options?.continue && this.#mainWorker.running && this.#executor) {
            return this.#executor.promise;
        }

        // immediately abort running worker
        this.abort();

        // update timer configuration for all workers
        const config: AsyncWorkerConfig =
            UNITTEST ? UNITTEST_TIMER_CONFIG :
            getDebugFlag("spreadsheet:slow-dependencies") ? SLOW_TIMER_CONFIG :
            options?.priority ? PRIORITY_TIMER_CONFIG : STANDARD_TIMER_CONFIG;
        this.#mainWorker.configure(config);
        this.#pendingWorker.configure(config);
        this.#recalcWorker.configure(config);
        this.#resultsWorker.configure(config);

        // whether to recalculate all volatile formulas that do NOT depend on dirty cells
        this.#depStore.pendingCollection.recalcVolatile = options?.volatile ?? true;

        // debounced start of the execution cycle
        this.#startDebounced();

        // create the promise that will be returned to external callers (remains in
        // pending state until a complete update cycle has been finished successfully)
        this.#executor ??= new PromiseExecutor();
        return this.#executor.promise;
    }

    abort(): void {
        if (this.#mainWorker.running) {
            depsLogger.log("$badge{DepWorker} aborting update cycle");
            this.#mainWorker.abort();
            this.trigger("recalc:cancel");
        }
    }

    // public methods ---------------------------------------------------------

    @debounceMethod({ delay: DEBOUNCE_DELAY })
    #startDebounced(): void {
        if (!this.#mainWorker.running) {
            jpromise.floating(depsLogger.takeTime(
                "$badge{DepWorker} dependency update cycle",
                () => this.#mainWorker.start(),
                LogLevel.INFO
            ));
        }
    }
}
