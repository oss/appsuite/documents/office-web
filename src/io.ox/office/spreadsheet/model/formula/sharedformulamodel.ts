/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";
import type { GrammarType } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { SheetTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * The result after refreshing a dirty shared formula model.
 *
 * - `unchanged`: The shared formula was not dirty and did not change.
 *
 * - `deleted`: The shared formula model does not have any member cells and
 *   needs to be deleted.
 *
 * - `refreshed`: The shared formula model has been refreshed. The bounding
 *   range may have changed, but neither the reference cell nor the formula
 *   expression have changed.
 *
 * - `changed`: The shared formula model has been refreshed. The bounding range
 *   may have changed, and either the address of the reference cell, or the
 *   formula expression have changed.
 */
export type SharedFormulaRefreshResult = "unchanged" | "deleted" | "refreshed" | "changed";

// class SharedFormulaModel ===================================================

/**
 * A structure that is used to collect data about a shared formula in a sheet.
 */
export class SharedFormulaModel extends DObject {

    // properties -------------------------------------------------------------

    /**
     * The index of the shared formula, as used for the property `si` in
     * "changeCells" document operations, and in cell changesets (interface
     * `CellChangeSet`).
     */
    readonly index: number;

    /**
     * The parsed formula expression of the shared formula. Will be `null` for
     * uninitialized model instances (no anchor cell with formula available).
     */
    readonly tokenArray: SheetTokenArray;

    /**
     * The addresses of all cells that are part of the shared formula.
     */
    readonly addressSet = new AddressSet();

    /**
     * The address of the anchor cell (the top-left-most cell that is part of
     * the shared formula. This address will be used as reference address to
     * parse the formula expression of the shared formula.
     */
    anchorAddress: Address;

    /**
     * The bounding range of the shared formulas (the smallest range containing
     * all member cells, i.e. `this.addressSet.boundary()`).
     */
    boundRange: Range;

    /**
     * Whether the `addressSet` of this instance has been changed. Used for
     * bulk operations for multiple changed cells. After modifying all cells,
     * the reference address and bounding range of dirty shared formulas will
     * be recalculated only once.
     */
    #dirtyAddresses = false;

    /**
     * Whether the `tokenArray` of this instance has been changed. Needed to
     * detect that all cells of a modified shared formula need to be notified
     * as "changed".
     */
    #dirtyFormula = false;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, si: number, address: Address);
    constructor(sheetModel: SheetModel, sourceModel: SharedFormulaModel);
    // implementation
    constructor(sheetModel: SheetModel, arg1: number | SharedFormulaModel, arg2?: Address) {
        super();

        const clone = arg1 instanceof SharedFormulaModel;
        this.index = clone ? arg1.index : arg1;
        this.tokenArray = this.member(sheetModel.createCellTokenArray());
        this.anchorAddress = (clone ? arg1.anchorAddress : arg2!).clone();

        if (clone) {
            const formula = arg1.getFormulaOp(this.anchorAddress);
            this.tokenArray.parseFormula("op", formula, this.anchorAddress);
            for (const addr of arg1.addressSet) {
                this.addressSet.add(addr.clone());
            }
            this.boundRange = arg1.boundRange.clone();
        } else {
            this.addressSet.add(this.anchorAddress);
            this.boundRange = new Range(this.anchorAddress.clone());
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the formula expression of this shared formula, relocated to the
     * specified target address.
     *
     * @param grammarType
     *  The identifier of the formula grammar for the formula expression.
     *
     * @param targetAddress
     *  The target address to generate the formula expression for.
     *
     * @returns
     *  The formula expression for the specified target address.
     */
    getFormula(grammarType: GrammarType, targetAddress: Address): string {
        return this.tokenArray.getFormula(grammarType, this.anchorAddress, targetAddress);
    }

    /**
     * Returns the formula expression of this shared formula in operation
     * grammar, relocated to the specified target address.
     *
     * @param targetAddress
     *  The target address to generate the formula expression for.
     *
     * @returns
     *  The formula expression for the specified target address.
     */
    getFormulaOp(targetAddress: Address): string {
        return this.tokenArray.getFormula("op", this.anchorAddress, targetAddress);
    }

    /**
     * Inserts the address and formula expression of a new member cell into
     * this shared formula.
     *
     * @param address
     *  The address of the cell to be inserted as member cell of this shared
     *  formula.
     *
     * @param formula
     *  The formula expression (in operation grammar) to be set as reference
     *  formula for this shared formula model.
     */
    registerCell(address: Address, formula: string | null): void {
        this.addressSet.add(address.clone());
        if (is.string(formula)) {
            this.tokenArray.parseFormula("op", formula, address);
            this.#dirtyFormula = true;
        }
        this.#dirtyAddresses = true;
    }

    /**
     * Removes the address of a member cell from this shared formula.
     *
     * @param address
     *  The address of the member cell to be removed.
     */
    unregisterCell(address: Address): void {
        this.addressSet.delete(address);
        this.#dirtyAddresses = true;
    }

    /**
     * Refreshes the properties `anchorAddress` and `boundRange`, if this
     * instance has been marked as "dirty".
     *
     * @returns
     *  Whether this shared formula model is now empty (no more member cells),
     *  and needs to be deleted form the parent containers.
     */
    refresh(): SharedFormulaRefreshResult {

        // shared formula does not have member cells anymore
        if (!this.addressSet.size) {
            return "deleted";
        }

        // `addressSet` has not been changed
        if (!this.#dirtyAddresses) {
            return "unchanged";
        }

        // update bounding range and anchor address
        const oldAnchorAddress = this.anchorAddress;
        this.anchorAddress = this.addressSet.first()!;
        this.boundRange = this.addressSet.boundary()!;
        this.#dirtyAddresses = false;


        // formula expression or anchor address has changed
        if (this.#dirtyFormula || this.anchorAddress.differs(oldAnchorAddress)) {
            this.#dirtyFormula = false;
            return "changed";
        }

        // neither formula expression nor anchor address have changed
        return "refreshed";
    }
}
