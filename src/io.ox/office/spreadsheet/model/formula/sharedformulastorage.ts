/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, map } from "@/io.ox/office/tk/algorithms";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { type SharedFormulaRefreshResult, SharedFormulaModel } from "@/io.ox/office/spreadsheet/model/formula/sharedformulamodel";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";

// class SharedFormulaStorage =================================================

/**
 * A container for shared formulas in a sheet.
 */
export class SharedFormulaStorage extends SheetChildModel {

    // the shared formulas, mapped by their shared index
    readonly #store = this.member(new Map<number, SharedFormulaModel>());
    // model indexes of all shared formulas, as sorted intervals
    readonly #indexes: Array<Pair<number>>;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, sourceMap?: SharedFormulaStorage) {
        super(sheetModel);

        if (sourceMap) {
            for (const [si, sharedModel] of sourceMap.#store) {
                this.#store.set(si, new SharedFormulaModel(sheetModel, sharedModel));
            }
            this.#indexes = sourceMap.#indexes.map(e => [...e]);
        } else {
            this.#indexes = [];
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the model of an existing shared formula.
     *
     * @param si
     *  The model index of the shared formula, as used in document operations.
     *
     * @returns
     *  The shared formula model with the specified model index if existing.
     */
    get(si: number | null): Opt<SharedFormulaModel> {
        return (si === null) ? undefined : this.#store.get(si);
    }

    /**
     * Returns an existing, or creates a new shared formula with the specified
     * shared index.
     *
     * @param si
     *  The model index of the shared formula, as used in document operations.
     *
     * @param address
     *  The address of a cell that is part of the shared formula.
     *
     * @returns
     *  The shared formula model with the specified model index.
     */
    upsert(si: number, address: Address): SharedFormulaModel {
        return map.upsert(this.#store, si, () => {
            this.#insertIndex(si);
            return new SharedFormulaModel(this.sheetModel, si, address);
        });
    }

    /**
     * Updates the reference addresses, bounding ranges, and token arrays of
     * all dirty shared formulas.
     *
     * @param [handlers]
     *  Callback functions mapped by the update result key which will be
     *  invoked for all shared formulas with the respective update result.
     */
    refresh(handlers?: PtRecord<SharedFormulaRefreshResult, (sharedModel: SharedFormulaModel) => void>): void {
        for (const [si, sharedModel] of this.#store) {
            const result = sharedModel.refresh();
            handlers?.[result]?.(sharedModel);
            if (result === "deleted") {
                this.#store.delete(si);
                this.#removeIndex(si);
            }
        }
    }

    /**
     * Returns an iterator that visits the models of all shared formulas.
     *
     * @returns
     *  An iterator that visits the models of all shared formulas.
     */
    values(): IterableIterator<SharedFormulaModel> {
        return this.#store.values();
    }

    /**
     * Returns an iterator that visits the sorted index intervals of all shared
     * formulas.
     *
     * @returns
     *  An iterator that visits the sorted index intervals.
     */
    intervals(): IterableIterator<Pair<number>> {
        return this.#indexes.values();
    }

    // private methods --------------------------------------------------------

    /**
     * Inserts a model index into the inetrval array.
     */
    #insertIndex(index: number): void {

        // array index of the interval that contains or precedes the passed index
        let ai = ary.fastFindLastIndex(this.#indexes, int => int[0] <= index);

        // nothing to do, if index is already contained in the interval
        let interval = this.#indexes[ai];
        if (interval && (index <= interval[1])) { return; }

        // extend the interval, if it ends exactly before the index; otherwise create a new interval
        if (interval && (interval[1] + 1 === index)) {
            interval[1] = index;
        } else {
            ai += 1;
            interval = ary.insertAt(this.#indexes, ai, [index, index]);
        }

        // try to merge with the next interval in the array
        const nextInterval = this.#indexes[ai + 1];
        if (nextInterval && (interval[1] + 1 === nextInterval[0])) {
            interval[1] = nextInterval[1];
            ary.deleteAt(this.#indexes, ai + 1);
        }
    }

    /**
     * Removes a model index from the interval array.
     */
    #removeIndex(index: number): void {

        // array index of the interval that contains or precedes the passed index
        const ai = ary.fastFindLastIndex(this.#indexes, int => int[0] <= index);

        // nothing to do, if index is not contained in the interval
        const interval = this.#indexes[ai];
        if (!interval || (interval[1] < index)) { return; }

        // delete a single-index interval; or shorten the interval, if the index is located at beginning or end; or split it
        if (interval[0] === interval[1]) {
            ary.deleteAt(this.#indexes, ai);
        } else if (interval[0] === index) {
            interval[0] += 1;
        } else if (interval[1] === index) {
            interval[1] -= 1;
        } else {
            ary.insertAt(this.#indexes, ai + 1, [index + 1, interval[1]]);
            interval[1] = index - 1;
        }
    }
}
