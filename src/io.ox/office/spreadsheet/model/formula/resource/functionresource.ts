/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { is, str, fun, ary, pick } from "@/io.ox/office/tk/algorithms";
import { LogLevel } from "@/io.ox/office/tk/utils/logger";

import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { type ContextType, type RecalcMode, MAX_PARAM_COUNT } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { FuncCategorySpec, FormatCategorySpec, ReturnTypeKey, ParamTypeKey, ParamDepSpec, ParamMatSpec, GenParamSigSpec, GenResolverFn, RelRefResolverFn } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import { Resource, type ResourceMap } from "@/io.ox/office/spreadsheet/model/formula/resource/resourcemap";

const { floor } = Math;

// types ======================================================================

/**
 * Translated help texts for a single parameter of a spreadsheet function.
 */
export interface ParameterHelp {

    /**
     * The translated name of the parameter.
     */
    readonly name: string;

    /**
     * The translated description text of the parameter.
     */
    readonly description: string;

    /**
     * Whether the parameter is required or optional.
     */
    readonly optional: boolean;
}

/**
 * Translated help texts for a spreadsheet function and its parameters.
 */
export interface FunctionHelp {

    /**
     * The translated upper-case name of the function.
     */
    readonly name: string;

    /**
     * A short description for the function itself.
     */
    readonly description: string;

    /**
     * An array with the names and descriptions of all parameters supported by
     * the function.
     */
    readonly params: readonly ParameterHelp[] | null;

    /**
     * The zero-based index of the first trailing parameter that can be used
     * repeatedly in the function; or `-1`, if the function does not support
     * parameter repetition.
     */
    readonly repeatStart: number;

    /**
     * The number of trailing repeated parameters; or `0`, if the function does
     * not support parameter repetition.
     */
    readonly cycleLength: number;

    /**
     * The maximum number of repetition cycles supported by the function,
     * according to the index of first repeated parameter, and the size of the
     * repetition cycles. Will be `0`, if the function does not support
     * parameter repetition.
     */
    readonly cycleCount: number;
}

/**
 * A resource map with function descriptors.
 */
export type FunctionResourceMap = ResourceMap<string, FunctionResource>;

/**
 * All type information of a single operator or function parameter.
 */
export interface ParamSignature {

    /**
     * The exact data type of the parameter.
     */
    readonly typeKey: ParamTypeKey;

    /**
     * Specifies how to resolve dependencies for the parameter.
     */
    readonly depSpec: ParamDepSpec;

    /**
     * The base data type of the parameter used as context type for evaluation.
     */
    readonly baseType: ContextType;

    /**
     * The context type for evaluating the parameter subexpression.
     */
    readonly contextType: ContextType;

    /**
     * Whether to forward outer matrix context to the parameter subexpression,
     * instead of using the own context type.
     */
    readonly preferMat: boolean;

    /**
     * Whether to forward outer matrix dimension in matrix context when
     * evaluating the parameter subexpression (`true`), instead of inferring
     * the matrix size from the parameter (`false`).
     */
    readonly forwardDim: boolean;
}

/**
 * The entire type signature of an operator or function.
 */
export type FunctionSignature = readonly ParamSignature[];


/**
 * Similar to the interface `FunctionSpec` but with properties resolved to one
 * specific file format.
 */
export interface ResolvedFuncSpec {
    category?: FuncCategorySpec | FuncCategorySpec[];
    name?: string | string[] | null;
    hidden?: true;
    minParams: number;
    maxParams?: number;
    repeatParams?: number;
    type: ReturnTypeKey;
    signature: GenParamSigSpec[];
    format?: FormatCategorySpec;
    recalc?: RecalcMode;
    resolve?: GenResolverFn | string;
    relColRef?: RelRefResolverFn;
    relRowRef?: RelRefResolverFn;
}

// private functions ==========================================================

/**
 * Returns a generic name for a function parameter, used as fall-back for
 * parameters without an existing name in the help resources.
 *
 * @param index
 *  The zero-based index of the function parameter.
 *
 * @returns
 *  A generic name for a function parameter, to be used in the GUI.
 */
function getGenericParamName(index: number): string {
    //#. Short generic name for an unknown argument in a function of a spreadsheet formula,
    //#. used to create a dummy function signature in tooltips.
    //#. %1$d is the numeric index added to the argument.
    //#. Example for a resulting tooltip using this text: NEWFUNCTION(argument1, argument2, [argument3, ...]).
    return gt.pgettext("funchelp", "argument%1$d", index + 1);
}

// class FunctionResource =====================================================

/**
 * Represents all information needed for a single operator or function.
 */
export class FunctionResource extends Resource<string> {

    // properties -------------------------------------------------------------

    // all categories this function is part of
    readonly categories: Set<string>;

    // whether the function should be hidden in the GUI
    readonly hidden: boolean;

    // recalculation mode of the function
    readonly recalc: RecalcMode;

    // function has relative column behavior (result changes according to reference column)
    readonly relColRef: Opt<RelRefResolverFn>;

    // function has relative row behavior (result changes according to reference row)
    readonly relRowRef: Opt<RelRefResolverFn>;

    // minimum number of parameters
    readonly minParams: number;

    // maximum number of parameters
    readonly maxParams: Opt<number>;

    // parameter repetition count
    readonly repeatParams: Opt<number>;

    // type category of the return value
    readonly retType: ContextType;

    // exact data type of scalar return values
    readonly valType: string | null;

    // exact data type of matrix return values
    readonly matType: string | null;

    // number format category for numbers returned from the function
    readonly format: FormatCategorySpec | null;

    // signature data for all function parameters
    readonly signature: FunctionSignature;

    // whether all parameters in the signature are scalars (val-type)
    readonly allValType: boolean;

    // resolver callback with the function implementation
    readonly resolve: Opt<GenResolverFn>;

    // help resources for the function
    readonly help: Opt<FunctionHelp>;

    // constructor ------------------------------------------------------------

    constructor(key: string, nativeNames: string[], localName: string, funcSpec: ResolvedFuncSpec) {

        super(key, nativeNames[0], localName);

        // store the alternative native names contained in the array
        this.altNativeNames.push(...nativeNames.slice(1));
        // convert category list to a set
        this.categories = new Set(ary.wrap(funcSpec.category));

        // add the format category, hidden flag, recalculation mode, and reference mode flags
        this.format = funcSpec.format || null;
        this.hidden = !!funcSpec.hidden;
        this.recalc = funcSpec.recalc || "normal";
        this.relColRef = funcSpec.relColRef;
        this.relRowRef = funcSpec.relRowRef;

        // copy parameter counts
        this.minParams = funcSpec.minParams;
        if (is.number(funcSpec.maxParams)) { this.maxParams = funcSpec.maxParams; }
        if (is.number(funcSpec.repeatParams)) { this.repeatParams = funcSpec.repeatParams; }

        // parse function return type
        const typeTokens = funcSpec.type.split(":");
        const retType = this.retType = typeTokens[0] as ContextType;
        this.valType = (retType === "val") ? (typeTokens[1] || "any") : null;
        this.matType = (retType === "mat") ? (typeTokens[1] || "any") : null;

        // convert signature string to an array of parameter descriptors
        this.signature = funcSpec.signature.map(paramSig => {

            // first token is the type key
            const typeKey = is.string(paramSig) ? paramSig : paramSig.type;
            // base type is the leading part of the type key (e.g. the "val" in "val:bool")
            const baseType = typeKey.split(":")[0] as ContextType;
            // source dependency behavior
            const depSpec: ParamDepSpec = (!is.string(paramSig) && paramSig.dep) || "resolve";
            // special behavior for matrix context
            const matSpec: ParamMatSpec = (!is.string(paramSig) && paramSig.mat) || "none";

            // whether the parameter is value type in a value operator
            const valType = (baseType === "val") && (retType === "val");
            // the effective context type for the operand (used to resolve the entire subtree)
            const contextType = (matSpec === "force") ? "mat" : baseType;
            // whether to prefer forwarding outer matrix context type
            const preferMat = valType || (matSpec === "pass") || (matSpec === "forward");
            // whether to forward outer matrix dimension in matrix context
            const forwardDim = valType || (matSpec === "forward");

            return { typeKey, depSpec, baseType, contextType, preferMat, forwardDim };
        });

        // whether all parameters are val-type
        this.allValType = this.signature.every(typeData => typeData.baseType === "val");
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates the help resources for this function.
     *
     * @param jsonData
     *  The raw help resource data for the function.
     *
     * @param [silent=false]
     *  Whether to silently skip any problems (no console warnings).
     */
    initHelp(jsonData: unknown, silent?: boolean): void {

        // writes a warning or error message for the current function
        const log = silent ? fun.undef : (msg: string, error?: true) => {
            const text = `$badge{FunctionResource} ${msg} function ${this.key}`;
            modelLogger.write(error ? LogLevel.ERROR : LogLevel.WARN, text);
        };

        // no double initialization
        if (this.help) { log("Help exists already for"); return; }

        // function data entry must be an object
        if (!is.dict(jsonData)) { log("Invalid map entry for", true); return; }

        if (!jsonData.d) {
            log("Missing description for", true);
        }

        // check parameter help availability of required parameters
        const params = pick.array(jsonData, "p", true).map(value => ({
            n: pick.string(value, "n", ""),
            d: pick.string(value, "d", "")
        }));
        const minParams = this.minParams;
        if (params.length < minParams) {
            log("Missing help for required parameters for");
            while (params.length < minParams) { params.push({ n: "", d: "" }); }
        }

        // check parameter help availability of fixed number of optional parameters
        const maxParams = this.maxParams;
        if (is.number(maxParams) && (params.length < maxParams)) {
            log("Missing help for optional parameters for");
            while (params.length < maxParams) { params.push({ n: "", d: "" }); }
        }

        // validate number of array elements of function parameter help
        if (is.number(maxParams)) {
            while (params.length < maxParams) { params.push({ n: "", d: "" }); }
            params.splice(maxParams);
        }

        // check parameter help availability of variable number of optional parameters
        const repeatParams = is.number(maxParams) ? null : (this.repeatParams || 1);
        if (repeatParams) {
            const optParams = params.length - minParams;
            // remove help texts of superfluous optional parameters
            if ((optParams !== 0) && (optParams !== repeatParams)) {
                log("Superfluous variable parameters for");
                params.splice(minParams + ((optParams > repeatParams) ? repeatParams : 0));
            }
        }

        // create a single parameter array with name, description, and additional information
        const paramHelp = params.map<ParameterHelp>((param, index) => ({
            name: is.string(param.n) ? str.trimAll(param.n) : getGenericParamName(index),
            description: is.string(param.d) ? str.trimAll(param.d) : "",
            optional: minParams <= index
        }));

        // properties for repeating parameters
        const repeatStart = repeatParams ? (paramHelp.length - repeatParams) : -1;
        const cycleLength = repeatParams || 0;
        const cycleCount = repeatParams ? floor((MAX_PARAM_COUNT - repeatStart) / repeatParams) : 0;

        // create the type-checked function help resource
        const funcHelp: FunctionHelp = {
            name: this.localName,
            description: str.trimAll(pick.string(jsonData, "d", "")),
            params: paramHelp,
            repeatStart,
            cycleLength,
            cycleCount
        };

        // overwrite the read-only property
        (this as Writable<this>).help = funcHelp;
    }
}
