/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import type { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";

import type { ErrorLiteral, ScalarTypeNE, ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import type { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import type { RecalcMode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { Complex } from "@/io.ox/office/spreadsheet/model/formula/utils/complex";
import type { ScalarMatrixNE, ScalarMatrix, NumberMatrix, StringMatrix, BooleanMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";

import type { OperandValType, OperandRefType, OperandAnyType, Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import type { FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";

// types ======================================================================

/**
 * Available function category identifiers.
 */
export type FuncCategorySpec = "complex" | "conversion" | "database" | "datetime" | "engineering" | "financial" | "information" | "logical" | "math" | "matrix" | "reference" | "statistical" | "text" | "web";

/**
 * A number format category for numeric operator or function results.
 */
export type FormatCategorySpec = "percent" | "fraction" | "currency" | "date" | "time" | "datetime" | "infer";

/**
 * Enumerates the data types of the return value of an operator or function for
 * all supported return type keys. Intended to be used on type-level only, not
 * for values at runtime.
 *
 * Example: The expression `ReturnType["val:date"]` converts the return type
 * key `"val:date"` to the actual function return type `Date`.
 */
export interface ReturnType {
    "val:num":  number | NumberMatrix;
    "val:bint": bigint;
    "val:date": Date;
    "val:str":  string;
    "val:bool": boolean;
    "val:err":  ErrorLiteral;
    "val:comp": Complex;
    "val:any":  OperandValType;
    "mat:num":  NumberMatrix;
    "mat:str":  StringMatrix;
    "mat:bool": BooleanMatrix;
    "mat:any":  ScalarMatrix;
    ref:        OperandRefType;
    any:        OperandAnyType;
}

/**
 * All string keys for the return type of an operator or function.
 */
export type ReturnTypeKey = KeysOf<ReturnType>;

/**
 * Enumerates the types of the parameters of an operator or function for all
 * supported parameter type keys. Intended to be used on type-level only, not
 * for values at runtime.
 *
 * Example: The expression `ParamType["val:date"]` converts the parameter type
 * key `"val:date"` to the actual parameter type `Date`.
 */
export interface ParamType {
    val:          ScalarTypeNE;
    "val:num":    number;
    "val:int":    number;
    "val:bint":   bigint;
    "val:date":   Date;
    "val:day":    Date;
    "val:str":    string;
    "val:bool":   boolean;
    "val:comp":   Complex;
    "val:any":    ScalarType;
    mat:          ScalarMatrixNE;
    "mat:num":    NumberMatrix;
    "mat:str":    StringMatrix;
    "mat:bool":   BooleanMatrix;
    "mat:any":    ScalarMatrix;
    ref:          Range3DArray;
    "ref:sheet":  Range3DArray;
    "ref:single": Range3D;
    "ref:multi":  Range3D;
    "ref:val":    Range3D;
    any:          Operand;
    "any:lazy":   Operand;
}

/**
 * All string keys for the data types of a parameter for an operator or
 * function.
 */
export type ParamTypeKey = KeysOf<ParamType>;

/**
 * Specifies how to calculate the dependencies for an operator or function
 * parameter.
 *
 * - "resolve" (default): A reference token as operand will be added to the set
 *   of source dependencies of the formula.
 *
 * - "skip": The parameter will not add its reference operand to the set of
 *   source dependencies of the formula. Usually, this option is used by
 *   operands and functions that calculate with the reference itself, without
 *   resolving the cell contents.
 *
 * - "pass": The parameter will pass its reference operand to its parent
 *   operator or function parameter for further handling of dependencies
 *   according to its configured behavior.
 */
export type ParamDepSpec = "resolve" | "skip" | "pass";

/**
 * Defines a specific behaviour for the operator or function parameter when it
 * is being evaluated in matrix context.
 *
 * - "none" (default): The parameter will be evaluated in matrix context, if it
 *   is matrix type, regardless of outer context type.
 *
 * - "force": The parameter will always be evaluated in matrix context,
 *   regardless of the outer context type. In difference to declaring `MAT`
 *   type for the parameter explicitly, simple references will not be resolved
 *   to a matrix immediately. This makes it possible to iterate over very large
 *   cell ranges without triggering the limitation of the matrix size.
 *
 * - "pass": Outer matrix context will be passed to the operand. Useful
 *   especially for parameters of type "any" to influence evaluation of
 *   references in matrix context.
 *
 * - "forward": Similar to "pass". Additionally, the outer matrix dimension
 *   will be forwarded to the operand instead of being inferred from the size
 *   of the operand.
 */
export type ParamMatSpec = "none" | "force" | "pass" | "forward";

/**
 * Optional configuration for a single function parameter, used in a signature
 * array.
 */
export interface ParamTypeOptions {
    dep?: ParamDepSpec;
    mat?: ParamMatSpec;
}

/**
 * Complete specification of a single function parameter with resolved
 * configuration options, used in a signature array.
 *
 * @template PTK
 *  The parameter type key (e.g. `ExtParamTypeSpec<"val:num">`).
 */
export interface ExtParamTypeSpec<PTK extends ParamTypeKey> extends Required<ParamTypeOptions> {
    type: PTK;
}

/**
 * Specification of a single function parameter used in a signature array,
 * either a simple string (type of the parameter), or as object with resolved
 * configuration options.
 *
 * @template PTK
 *  The parameter type key (e.g. `ParamSigSpec<"val:num">`).
 */
export type ParamSigSpec<PTK extends ParamTypeKey> = PTK | ExtParamTypeSpec<PTK>;

/**
 * Most generic type representing all possible parameter signatures.
 */
export type GenParamSigSpec = ParamSigSpec<ParamTypeKey>;

/**
 * Base constraint of generic type parameter for the return type of a resolver
 * callback function (type `ResolverFn<>`).
 *
 * `RTC` means "return type constraint".
 */
export type RTC = OperandAnyType;

/**
 * Base constraint of generic type parameter for a single parameter type of a
 * resolver callback function (type `ResolverFn<>`). The type `undefined` is
 * allowed in order to support optional function parameters.
 *
 * `PTC` means "parameter type constraint".
 */
export type PTC = Opt<OperandAnyType | Operand>;

/**
 * A resolver callback function with the actual implementation of a spreadsheet
 * operator or function. Resolver callback functions will always be invoked
 * with an instance of `FormulaContext` as calling context.
 *
 * @template RT
 *  The return type of the resolver callback function. Must be any data type
 *  supported by the formula engine (`OperandAnyType`). In addition, resolver
 *  functions may always return instances of `Operator` (containing a value
 *  with the appropriate operand type).
 *
 * @template PTT
 *  The type list of all parameters of the resolver callback function (a tuple
 *  of types). The type `undefined` is allowed in order to support optional
 *  function parameters.
 */
export type ResolverFn<RT extends RTC, PTT extends PTC[]> = (this: FormulaContext, ...args: PTT) => RT | Operand;

/**
 * Most generic type representing all possible resolver callback functions.
 */
export type GenResolverFn = ResolverFn<RTC, PTC[]>;

/**
 * Specification of a resolver for a spreadsheet operator or function. Can be a
 * function with the implementation, or the string key of another operator or
 * function whose resolver function shall be used.
 */
export type ResolverSpec<RTK extends ReturnTypeKey, PTT extends PTC[]> = ResolverFn<ReturnType[RTK], PTT> | string;

/**
 * A callback function used to specify whether the spreadsheet function behaves
 * similar to a cell reference with relative column/row index, i.e. it may
 * return different results for the same (constant) parameters, depending on
 * the position of the formula containing the function (reference address).
 *
 * @param params
 *  The number of parameters passed to the function.
 *
 * @returns
 *  Whether the function behaves like a relative reference.
 */
export type RelRefResolverFn = (params: number) => boolean;

/**
 * A record of values mapped by file format identifier.
 */
type FileFormatRecord<T> = Record<FileFormatType, T>;

/**
 * A single value (for all file formats), or a record mapped by file format
 * identifier that requires to specify properties for all supported file
 * formats.
 */
export type ReqFileFormatRecord<T> = T | FileFormatRecord<T>;

/**
 * A single value (for all file formats), or a record mapped by file format
 * identifier that allows to specify properties for some supported file
 * formats, and leave out others.
 */
export type OptFileFormatRecord<T> = T | Partial<FileFormatRecord<T>>;

/**
 * The full specification for an operator or function. See the documentation
 * file `README.md` for details.
 *
 * @template RTK
 *  The return type key (the value of the property `type`).
 *
 * @template PTT
 *  The type list of all parameters of the resolver callback function (a tuple
 *  of types).
 */
export interface FunctionSpec<RTK extends ReturnTypeKey, PTT extends PTC[]> {
    category?: FuncCategorySpec | FuncCategorySpec[];
    name?: OptFileFormatRecord<string | string[] | null>;
    hidden?: OptFileFormatRecord<true>;
    minParams: ReqFileFormatRecord<number>;
    maxParams?: ReqFileFormatRecord<number>;
    repeatParams?: ReqFileFormatRecord<number>;
    type: RTK;
    signature: GenParamSigSpec[];
    format?: OptFileFormatRecord<FormatCategorySpec>;
    recalc?: OptFileFormatRecord<RecalcMode>;
    resolve?: OptFileFormatRecord<ResolverSpec<RTK, PTT>>;
    relColRef?: OptFileFormatRecord<RelRefResolverFn>;
    relRowRef?: OptFileFormatRecord<RelRefResolverFn>;
}

/**
 * Most generic type representing all possible function specification objects.
 */
export type GenFunctionSpec = FunctionSpec<ReturnTypeKey, any>;

/**
 * Type of a simplified function specification object with a reduced property
 * set used by the class `FunctionSpecFactory`.
 *
 * The properties `minParams`, `maxParams`, `type`, and `signature` will be
 * generated automatically by the methods of that class. The expected resolver
 * callback function will be strongly typed according to the used class method.
 *
 * @template RT
 *  The data type of the return value of the resolver function.
 *
 * @template PTT
 *  The type list of all parameters of the resolver callback function.
 */
export type SimpleFunctionSpec<RT extends RTC, PTT extends PTC[]> =
 (Omit<GenFunctionSpec, "type" | "minParams" | "maxParams" | "repeatParams" | "signature" | "resolve"> & {
     minParams?: ReqFileFormatRecord<number>;
     resolve?: OptFileFormatRecord<ResolverFn<RT, PTT> | string>;
 }) | ResolverFn<RT, PTT> | string;

/**
 * Type of a callback function that receives a function specification factory,
 * and inserts new functions into it.
 */
export type FunctionSpecRegisterFn = (factory: FunctionSpecFactory) => void;

export interface FunctionSpecRegisterModule {
    category: Opt<FuncCategorySpec>;
    register: FunctionSpecRegisterFn;
}

// public functions ===========================================================

/**
 * Helper function for typesafe registration of function registration modules.
 *
 * @param category
 *  The function category to be attached to all functions registered in that
 *  module.
 *
 * @param register
 *  The registration callback function.
 *
 * @returns
 *  The result object expected to be default-exported from the module.
 */
export function declareFunctionSpecModule(category: Opt<FuncCategorySpec>, register: FunctionSpecRegisterFn): FunctionSpecRegisterModule {
    return { category, register };
}

// class FunctionSpecFactory ==================================================

export class FunctionSpecFactory implements KeyedIterable<GenFunctionSpec> {

    readonly #specMap = new Map<string, GenFunctionSpec>();

    readonly #category: Opt<FuncCategorySpec>;

    // constructor ------------------------------------------------------------

    constructor(module: FunctionSpecRegisterModule) {
        this.#category = module.category;
        module.register(this);
    }

    // public methods ---------------------------------------------------------

    /**
     * Specification of a single function parameter with additional options in
     * a signature array.
     */
    param<PTK extends ParamTypeKey>(type: PTK, options?: ParamTypeOptions): ExtParamTypeSpec<PTK> {
        return { type, dep: options?.dep || "resolve", mat: options?.mat || "none" };
    }

    /**
     * Returns an iterator for all registered entries.
     */
    [Symbol.iterator](): KeyedIterator<GenFunctionSpec> {
        return this.#specMap.entries();
    }

    // function registration --------------------------------------------------

    /**
     * Registers a function with unlimited number of parameter.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param minParams
     *  The minimum number of parameters expected by the function.
     *
     * @param repeatParams
     *  The size of the trailing parameter repetition pattern.
     *
     * @param signature
     *  The type signature of the function.
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    unbound<RTK extends ReturnTypeKey, PTT extends PTC[]>(
        resKey: string,
        typeKey: RTK,
        minParams: ReqFileFormatRecord<number>,
        repeatParams: ReqFileFormatRecord<number>,
        signature: GenParamSigSpec[],
        funcSpec: Omit<FunctionSpec<RTK, PTT>, "type" | "minParams" | "maxParams" | "repeatParams" | "signature"> | ResolverSpec<RTK, PTT>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.#register(resKey, { ...resolvedSpec, type: typeKey, minParams, repeatParams, signature });
    }

    /**
     * Registers a function with a fixed maximum number of parameters.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param minParams
     *  The minimum number of parameters expected by the function.
     *
     * @param maxParams
     *  The maximum number of parameters accepted by the function.
     *
     * @param signature
     *  The type signature of the function.
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    fixed<RTK extends ReturnTypeKey, PTT extends PTC[]>(
        resKey: string,
        typeKey: RTK,
        minParams: ReqFileFormatRecord<number>,
        maxParams: ReqFileFormatRecord<number>,
        signature: GenParamSigSpec[],
        funcSpec: Omit<FunctionSpec<RTK, PTT>, "type" | "minParams" | "maxParams" | "repeatParams" | "signature"> | ResolverSpec<RTK, PTT>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.#register(resKey, { ...resolvedSpec, type: typeKey, minParams, maxParams, signature });
    }

    // nullary functions ------------------------------------------------------

    /**
     * Registers a nullary (parameterless) function.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    nullary<RTK extends ReturnTypeKey>(
        resKey: string,
        typeKey: RTK,
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], []>
    ): void {
        this.fixed(resKey, typeKey, 0, 0, [], funcSpec);
    }

    nullaryNum(resKey: string, funcSpec: number | SimpleFunctionSpec<number, []>): void {
        this.nullary(resKey, "val:num", is.number(funcSpec) ? () => funcSpec : funcSpec);
    }

    nullaryBool(resKey: string, funcSpec: boolean | SimpleFunctionSpec<boolean, []>): void {
        this.nullary(resKey, "val:bool", is.boolean(funcSpec) ? () => funcSpec : funcSpec);
    }

    // unary functions --------------------------------------------------------

    /**
     * Registers a unary function with a mandarory parameter.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param paramSpec
     *  The type signature of the function parameter.
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    unary<RTK extends ReturnTypeKey, PTK extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        paramSpec: ParamSigSpec<PTK>,
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK]]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 1, 1, [paramSpec], resolvedSpec);
    }

    /**
     * Registers a unary function with an optional parameter.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param paramSpec
     *  The type signature of the function parameter.
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    unary0<RTK extends ReturnTypeKey, PTK extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        paramSpec: ParamSigSpec<PTK>,
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK]?]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 0, 1, [paramSpec], resolvedSpec);
    }

    unaryNum(resKey: string, funcSpec: SimpleFunctionSpec<number, [number]>): void {
        this.unary(resKey, "val:num", "val:num", funcSpec);
    }

    unaryInt(resKey: string, funcSpec: SimpleFunctionSpec<number, [number]>): void {
        this.unary(resKey, "val:num", "val:int", funcSpec);
    }

    unaryStr(resKey: string, funcSpec: SimpleFunctionSpec<string, [string]>): void {
        this.unary(resKey, "val:str", "val:str", funcSpec);
    }

    unaryBool(resKey: string, funcSpec: SimpleFunctionSpec<boolean, [boolean]>): void {
        this.unary(resKey, "val:bool", "val:bool", funcSpec);
    }

    unaryComp(resKey: string, funcSpec: SimpleFunctionSpec<Complex, [Complex]>): void {
        this.unary(resKey, "val:comp", "val:comp", funcSpec);
    }

    unaryScalar(resKey: string, funcSpec: SimpleFunctionSpec<ScalarType, [ScalarType]>): void {
        this.unary(resKey, "val:any", "val:any", funcSpec);
    }

    // binary functions -------------------------------------------------------

    /**
     * Registers a binary function. All parameters are mandatory.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param signature
     *  The type signature of the function (must be a pair of parameter types).
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    binary<RTK extends ReturnTypeKey, PTK1 extends ParamTypeKey, PTK2 extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        signature: [ParamSigSpec<PTK1>, ParamSigSpec<PTK2>],
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK1], ParamType[PTK2]]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 2, 2, signature, resolvedSpec);
    }

    /**
     * Registers a binary function. First parameter is mandatory, second is
     * optional.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param signature
     *  The type signature of the function (must be a pair of parameter types).
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    binary1<RTK extends ReturnTypeKey, PTK1 extends ParamTypeKey, PTK2 extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        signature: [ParamSigSpec<PTK1>, ParamSigSpec<PTK2>],
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK1], ParamType[PTK2]?]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 1, 2, signature, resolvedSpec);
    }

    binaryNum(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number]>): void {
        this.binary(resKey, "val:num", ["val:num", "val:num"], funcSpec);
    }

    binary1Num(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number?]>): void {
        this.binary1(resKey, "val:num", ["val:num", "val:num"], funcSpec);
    }

    binaryInt(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number]>): void {
        this.binary(resKey, "val:num", ["val:int", "val:int"], funcSpec);
    }

    binary1Int(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number?]>): void {
        this.binary1(resKey, "val:num", ["val:int", "val:int"], funcSpec);
    }

    binaryNumInt(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number]>): void {
        this.binary(resKey, "val:num", ["val:num", "val:int"], funcSpec);
    }

    binary1NumInt(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number?]>): void {
        this.binary1(resKey, "val:num", ["val:num", "val:int"], funcSpec);
    }

    binaryBint(resKey: string, funcSpec: SimpleFunctionSpec<bigint, [bigint, bigint]>): void {
        this.binary(resKey, "val:bint", ["val:bint", "val:bint"], funcSpec);
    }

    binaryBintNum(resKey: string, funcSpec: SimpleFunctionSpec<bigint, [bigint, number]>): void {
        this.binary(resKey, "val:bint", ["val:bint", "val:num"], funcSpec);
    }

    binaryStr(resKey: string, funcSpec: SimpleFunctionSpec<string, [string, string]>): void {
        this.binary(resKey, "val:str", ["val:str", "val:str"], funcSpec);
    }

    binaryComp(resKey: string, funcSpec: SimpleFunctionSpec<Complex, [Complex, Complex]>): void {
        this.binary(resKey, "val:comp", ["val:comp", "val:comp"], funcSpec);
    }

    binaryScalar(resKey: string, funcSpec: SimpleFunctionSpec<ScalarType, [ScalarType, ScalarType]>): void {
        this.binary(resKey, "val:any", ["val:any", "val:any"], funcSpec);
    }

    // ternary functions ------------------------------------------------------

    /**
     * Registers a ternary function. All parameters are mandatory.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param signature
     *  The type signature of the function (must be a triple of parameter
     *  types).
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    ternary<RTK extends ReturnTypeKey, PTK1 extends ParamTypeKey, PTK2 extends ParamTypeKey, PTK3 extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        signature: [ParamSigSpec<PTK1>, ParamSigSpec<PTK2>, ParamSigSpec<PTK3>],
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK1], ParamType[PTK2], ParamType[PTK3]]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 3, 3, signature, resolvedSpec);
    }

    /**
     * Registers a ternary function. First and second parameters are mandatory,
     * third is optional.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param signature
     *  The type signature of the function (must be a triple of parameter
     *  types).
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    ternary2<RTK extends ReturnTypeKey, PTK1 extends ParamTypeKey, PTK2 extends ParamTypeKey, PTK3 extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        signature: [ParamSigSpec<PTK1>, ParamSigSpec<PTK2>, ParamSigSpec<PTK3>],
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK1], ParamType[PTK2], ParamType[PTK3]?]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 2, 3, signature, resolvedSpec);
    }

    /**
     * Registers a ternary function. First parameter is mandatory, second and
     * third are optional.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param typeKey
     *  The type key of the function return value.
     *
     * @param signature
     *  The type signature of the function (must be a triple of parameter
     *  types).
     *
     * @param funcSpec
     *  The remaining configuration of the function.
     */
    ternary1<RTK extends ReturnTypeKey, PTK1 extends ParamTypeKey, PTK2 extends ParamTypeKey, PTK3 extends ParamTypeKey>(
        resKey: string,
        typeKey: RTK,
        signature: [ParamSigSpec<PTK1>, ParamSigSpec<PTK2>, ParamSigSpec<PTK3>],
        funcSpec: SimpleFunctionSpec<ReturnType[RTK], [ParamType[PTK1], ParamType[PTK2]?, ParamType[PTK3]?]>
    ): void {
        const resolvedSpec = is.dict(funcSpec) ? funcSpec : { resolve: funcSpec };
        this.fixed(resKey, typeKey, 1, 3, signature, resolvedSpec);
    }

    ternaryNum(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number, number]>): void {
        this.ternary(resKey, "val:num", ["val:num", "val:num", "val:num"], funcSpec);
    }

    ternary2Num(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number, number?]>): void {
        this.ternary2(resKey, "val:num", ["val:num", "val:num", "val:num"], funcSpec);
    }

    ternary1Num(resKey: string, funcSpec: SimpleFunctionSpec<number, [number, number?, number?]>): void {
        this.ternary1(resKey, "val:num", ["val:num", "val:num", "val:num"], funcSpec);
    }

    // private methods --------------------------------------------------------

    /**
     * Registers a function specification.
     *
     * @param resKey
     *  The resource key of the function or operator.
     *
     * @param funcSpec
     *  The configuration of the function.
     */
    #register(resKey: string, funcSpec: GenFunctionSpec): void {
        if (this.#specMap.has(resKey)) {
            const msg = `resource with key "${resKey}" exists already`;
            formulaLogger.error(`$badge{FunctionSpecFactory} ${msg}`);
            throw new Error(msg);
        }
        const category = !funcSpec.category ? this.#category : !this.#category ? funcSpec.category : [this.#category].concat(funcSpec.category);
        this.#specMap.set(resKey, category ? { ...funcSpec, category } : funcSpec);
    }
}
