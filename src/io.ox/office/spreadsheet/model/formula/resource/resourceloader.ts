/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { is, map, dict, pick } from "@/io.ox/office/tk/algorithms";
import { type LocaleData, LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";

// types ======================================================================

/**
 * Local help resource for an unlocalized function parameter.
 */
interface LocalParamHelp {
    /** The zero-based index of the parameter. */
    i: number;
    /** The name of the parameter. */
    n: string;
    /** Description text for the parameter. */
    d: string;
}

/**
 * Local help resource for a (partially) unlocalized function.
 */
interface LocalFuncHelp {
    /** Description text for the function. */
    description?: string;
    /** Help for function parameters. */
    params?: LocalParamHelp[];
}

// constants ==================================================================

// a map that contains missing help texts for functions
const LOCAL_FUNC_HELP: Dict<LocalFuncHelp> = {

    // HYPGEOM.DIST contains a new parameter (existing help is mapped by HYPGEOMDIST, and will be used by HYPGEOM.DIST too)
    HYPGEOMDIST: {
        params: [{
            i: 4,
            //#. The name of the fifth argument of the spreadsheet function HYPGEOM.DIST (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Two#HYPGEOM.DIST)
            n: gt.pgettext("funchelp-HYPGEOM.DIST", "cumulative"),
            //#. Description for the fifth argument of the spreadsheet function HYPGEOM.DIST (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Two#HYPGEOM.DIST)
            d: gt.pgettext("funchelp-HYPGEOM.DIST", "If set to TRUE, calculates the cumulative distribution function; if set to FALSE, calculates the probability density function.")
        }]
    },

    // NEGBINOM.DIST contains a new parameter (existing help is mapped by NEGBINOMDIST, and will be used by NEGBINOM.DIST too)
    NEGBINOMDIST: {
        params: [{
            i: 3,
            //#. The name of the fourth argument of the spreadsheet function NEGBINOM.DIST (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Four#NEGBINOM.DIST)
            n: gt.pgettext("funchelp-NEGBINOM.DIST", "cumulative"),
            //#. Description for the fourth argument of the spreadsheet function NEGBINOM.DIST (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Four#NEGBINOM.DIST)
            d: gt.pgettext("funchelp-NEGBINOM.DIST", "If set to TRUE, calculates the cumulative distribution function; if set to FALSE, calculates the probability density function.")
        }]
    },

    // NORMS.DIST contains a new parameter (existing help is mapped by NORMSDIST, and will be used by NORM.S.DIST too)
    NORMSDIST: {
        params: [{
            i: 1,
            //#. The name of the second argument of the spreadsheet function NORM.S.DIST (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Five#NORM.S.DIST)
            n: gt.pgettext("funchelp-NORM.S.DIST", "cumulative"),
            //#. Description for the second argument of the spreadsheet function NORM.S.DIST (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Five#NORM.S.DIST)
            d: gt.pgettext("funchelp-NORM.S.DIST", "If set to TRUE, calculates the cumulative distribution function; if set to FALSE, calculates the probability density function.")
        }]
    },

    // third parameter of NUMBERVALUE is not supported/documented in AOO
    NUMBERVALUE: {
        params: [{
            i: 2,
            //#. The name of the third argument of the spreadsheet function NUMBERVALUE (see https://help.libreoffice.org/Calc/NUMBERVALUE)
            n: gt.pgettext("funchelp-NUMBERVALUE", "group_separator"),
            //#. Description for the third argument of the spreadsheet function NUMBERVALUE (see https://help.libreoffice.org/Calc/NUMBERVALUE)
            d: gt.pgettext("funchelp-NUMBERVALUE", "Defines the character used to separate groups of numbers (e.g. thousands, millions).")
        }]
    },

    // PERCENTRANK.INC contains a new parameter (existing help is mapped by PERCENTRANK, and will be used by PERCENTRANK.INC too)
    PERCENTRANK: {
        params: [{
            i: 2,
            //#. The name of the third argument of the spreadsheet function PERCENTRANK.INC (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Four#PERCENTRANK.INC)
            n: gt.pgettext("funchelp-PERCENTRANK.INC", "significance"),
            //#. Description for the third argument of the spreadsheet function PERCENTRANK.INC (see https://help.libreoffice.org/Calc/Statistical_Functions_Part_Four#PERCENTRANK.INC)
            d: gt.pgettext("funchelp-PERCENTRANK.INC", "The number of significant digits to round the returned value to.")
        }]
    }
};

// maps function keys with help resources to function keys without help resources
const MISSING_HELP_FUNC_MAP: Dict<string[]> = {
    BETAINV: ["BETA.INV"],
    BINOMDIST: ["BINOM.DIST"],
    CRITBINOM: ["BINOM.INV"],
    "CEILING.ODF": ["CEILING", "CEILING.MATH"],
    CHISQDIST: ["CHISQ.DIST"],
    CHIDIST: ["CHISQ.DIST.RT"],
    CHISQINV: ["CHISQ.INV"],
    CHIINV: ["CHISQ.INV.RT"],
    CHITEST: ["CHISQ.TEST"],
    CONFIDENCE: ["CONFIDENCE.NORM"],
    COVAR: ["COVARIANCE.P"],
    ERF: ["ERF.PRECISE"],
    ERFC: ["ERFC.PRECISE"],
    EXPONDIST: ["EXPON.DIST"],
    FDIST: ["F.DIST.RT"],
    FINV: ["F.INV.RT"],
    FTEST: ["F.TEST"],
    "FLOOR.ODF": ["FLOOR", "FLOOR.MATH"],
    FORECAST: ["FORECAST.LINEAR"],
    GAMMADIST: ["GAMMA.DIST"],
    GAMMAINV: ["GAMMA.INV"],
    GAMMALN: ["GAMMALN.PRECISE"],
    HYPGEOMDIST: ["HYPGEOM.DIST"],
    LOGNORMDIST: ["LOGNORM.DIST"],
    LOGINV: ["LOGNORM.INV"],
    MODE: ["MODE.SNGL"],
    NEGBINOMDIST: ["NEGBINOM.DIST"],
    NORMDIST: ["NORM.DIST"],
    NORMINV: ["NORM.INV"],
    NORMSDIST: ["NORM.S.DIST"],
    NORMSINV: ["NORM.S.INV"],
    PERCENTILE: ["PERCENTILE.INC"],
    PERCENTRANK: ["PERCENTRANK.INC"],
    POISSON: ["POISSON.DIST"],
    QUARTILE: ["QUARTILE.INC"],
    RANK: ["RANK.EQ"],
    STDEVP: ["STDEV.P"],
    STDEV: ["STDEV.S"],
    TINV: ["T.INV.2T"],
    TTEST: ["T.TEST"],
    VARP: ["VAR.P"],
    VAR: ["VAR.S"],
    WEIBULL: ["WEIBULL.DIST"],
    ZTEST: ["Z.TEST"]
};

// globals ====================================================================

// cache for existing JSON formula resources
const singletonCache = new Map<string, Promise<Dict>>();

// public functions ===========================================================

/**
 * Loads the localized JSON resource data for the specified locale.
 *
 * @param localeData
 *  The locale data with the language identifier of the JSON resource data to
 *  be loaded.
 *
 * @returns
 *  A promise that will fulfil with the JSON resource data for the specified
 *  locale.
 */
export function loadFormulaResource(localeData: LocaleData): Promise<Dict> {
    return map.upsert(singletonCache, localeData.lc, async () => {

        // load the JSON resource file (filenames consist of language code only)
        modelLogger.info(`$badge{ResourceLoader} Loading formula resource for locale "${localeData.lc}"`);
        const jsonModule = await import(`./data/${localeData.language}.json`) as typeof import("*.json");
        const jsonDict = jsonModule.default as Dict<Dict>;

        // extract dictionary for locasle, fallback to language
        const jsonData = jsonDict[localeData.lc] || jsonDict[localeData.language];

        // add local translations for missing function help texts (only for UI locale)
        if (localeData.lc === LOCALE_DATA.lc) {

            // get or create the `help` section of the resource
            const funcHelpMap = pick.dict(jsonData, "help") ?? (jsonData.help = dict.create());

            // add missing help texts that have been defined locally in this file
            dict.forEach(LOCAL_FUNC_HELP, (localFuncHelp, funcKey) => {

                // get or create the function entry in the `help` section of the resource
                const funcHelp = pick.dict(funcHelpMap, funcKey) ?? (funcHelpMap[funcKey] = dict.create());

                // add missing function description
                if (!funcHelp.d && localFuncHelp.description) {
                    funcHelp.d = localFuncHelp.description;
                }

                // add local parameter help if missing in the loaded resource file
                if (localFuncHelp.params) {
                    const funcParams = pick.array(funcHelp, "p") ?? (funcHelp.p = []);
                    localFuncHelp.params.forEach(localParamHelp => {
                        const index = localParamHelp.i;
                        if (!funcParams[index]) { funcParams[index] = localParamHelp; }
                    });
                }
            });
        }

        // distribute existing help resources to functions without own help
        const funcHelpMap = pick.dict(jsonData, "help") ?? (jsonData.help = dict.create());

        // copy existing entries to missing keys
        dict.forEach(MISSING_HELP_FUNC_MAP, (missingKeys, existingKey) => {
            const funcHelp = pick.dict(funcHelpMap, existingKey);
            if (funcHelp) {
                missingKeys.forEach(missingKey => {
                    if (!is.dict(funcHelpMap[missingKey])) {
                        funcHelpMap[missingKey] = funcHelp;
                    }
                });
            }
        });

        return jsonData;
    });
}
