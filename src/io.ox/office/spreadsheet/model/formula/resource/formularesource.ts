/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary, dict, pick } from "@/io.ox/office/tk/algorithms";
import { type LocaleData, localeDataRegistry } from "@/io.ox/office/tk/locale";
import { DObject, memoizeMethod } from "@/io.ox/office/tk/objects";
import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";

import type { ErrorKey } from "@/io.ox/office/spreadsheet/utils/scalar";
import { modelLogger } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { Resource, ResourceMap } from "@/io.ox/office/spreadsheet/model/formula/resource/resourcemap";
import { loadFormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/resourceloader";
import { type FunctionSpecRegisterModule, FunctionSpecFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import type { FunctionHelp, ResolvedFuncSpec, FunctionResourceMap } from "@/io.ox/office/spreadsheet/model/formula/resource/functionresource";
import { FunctionResource } from "@/io.ox/office/spreadsheet/model/formula/resource/functionresource";

import OperatorsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/operators";
import ComplexFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/complexfuncs";
import ConversionFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/conversionfuncs";
import DatabaseFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/databasefuncs";
import DateTimeFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/datetimefuncs";
import EngineeringFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/engineeringfuncs";
import FinancialFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/financialfuncs";
import InformationFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/informationfuncs";
import LogicalFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/logicalfuncs";
import MathFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/mathfuncs";
import MatrixFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/matrixfuncs";
import ReferenceFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/referencefuncs";
import StatisticalFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/statisticalfuncs";
import TextFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/textfuncs";
import WebFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/webfuncs";

// types ======================================================================

/**
 * The type of a `FormulaResource` instance.
 *
 * - `std`: The standard formula resource for the file format and UI locale
 *   used by the resource factory.
 *
 * - `oox`: The formula resource for the OOXML formula syntax, regardless of
 *   the file format used by the resource factory.
 *
 * - `en`: The formula resource for English locale, regardless of the UI locale
 *   used by the resource factory.
 */
export type FormulaResourceType = "std" | "oox" | "en";

/**
 * Resource keys of boolean literals in formulas.
 */
export type BooleanKey = keyof typeof NATIVE_BOOLEAN_NAMES;

/**
 * Resource keys of table regions in formulas.
 */
export type TableRegionKey = keyof typeof NATIVE_REGION_NAMES;

/**
 * Resource keys for first parameter of the CELL function in formulas.
 */
export type CellParamKey = keyof typeof NATIVE_CELL_PARAM_NAMES;

// constants ==================================================================

// the native prefix characters for R1C1 reference notation
const NATIVE_RC_PREFIX_CHARS = "RC";

// maps the keys of boolean literals to their native names used in file format
const NATIVE_BOOLEAN_NAMES = {
    f: "FALSE",
    t: "TRUE"
};

// maps the keys of error codes to their native names used in file format
const NATIVE_ERROR_NAMES: Record<ErrorKey, string> = {
    NULL: "#NULL!",
    DIV0: "#DIV/0!",
    VALUE: "#VALUE!",
    REF: "#REF!",
    NAME: "#NAME?",
    NUM: "#NUM!",
    NA: "#N/A",
    DATA: "#GETTING_DATA"
};

// maps the keys of table regions to their native names used in file format
const NATIVE_REGION_NAMES = {
    ALL: "#All",
    HEADERS: "#Headers",
    DATA: "#Data",
    TOTALS: "#Totals",
    ROW: "#This Row"
};

// maps the keys of named parameters of CELL to their English names (supported in all locales)
const NATIVE_CELL_PARAM_NAMES = {
    ADDRESS: "address",
    COL: "col",
    COLOR: "color",
    CONTENTS: "contents",
    FILENAME: "filename",
    FORMAT: "format",
    PARENTHESES: "parentheses",
    PREFIX: "prefix",
    PROTECT: "protect",
    ROW: "row",
    TYPE: "type",
    WIDTH: "width"
};

// maps resource keys of operators to their local names
const LOCAL_OPERATOR_NAMES: Dict<string> = {
    uplus: "+",
    uminus: "-",
    upct: "%",
    add: "+",
    sub: "-",
    mul: "*",
    div: "/",
    pow: "^",
    con: "&",
    lt: "<",
    le: "<=",
    gt: ">",
    ge: ">=",
    eq: "=",
    ne: "<>",
    // "list" depends on locale
    isect: " ",
    range: ":"
};

// globals ====================================================================

// module exports for operator specifications, as array
const OPERATOR_MODULES: FunctionSpecRegisterModule[] = [OperatorsModule];

// module exports for function specifications, as array
const FUNCTION_MODULES: FunctionSpecRegisterModule[] = [
    ComplexFuncsModule,
    ConversionFuncsModule,
    DatabaseFuncsModule,
    DateTimeFuncsModule,
    EngineeringFuncsModule,
    FinancialFuncsModule,
    InformationFuncsModule,
    LogicalFuncsModule,
    MathFuncsModule,
    MatrixFuncsModule,
    ReferenceFuncsModule,
    StatisticalFuncsModule,
    TextFuncsModule,
    WebFuncsModule
];

// private functions ==========================================================

// resolves alternatives for different file format (if available)
function resolveDictForFileFormat<T extends object>(source: T, fileFormat: FileFormatType): T {
    return dict.mapDict(source as unknown as Dict, value => is.dict(value) ? value[fileFormat] : value) as T;
}

/**
 * Creates a formula resource map for specific native names from the passed
 * JSON data.
 *
 * @param nativeNames
 *  The native names mapped by resource keys to be used to create the resource
 *  map.
 *
 * @param localNames
 *  JSON data containing the localized names.
 *
 * @param dbgLabel
 *  An arbitrary label for the resources, used in debug warining messages only.
 *
 * @returns
 *  The resource map containing all resources specified in `nativeNames`.
 */
function createStringResourceMap<KeyT extends string>(
    nativeNames: Record<KeyT, string>,
    localNames: unknown,
    dbgLabel: string
): ResourceMap<KeyT> {

    // generator for function resources (converter from `Record` entries to `Resource`)
    function *yieldResources(): EntryIterator<KeyT, Resource<KeyT>> {
        for (const [key, nativeName] of dict.entries(nativeNames)) {
            let localName = pick.string(localNames, key);
            if (!localName) {
                modelLogger.error(`$badge{FormulaResource} Missing ${dbgLabel} translation: key=${key}`);
                localName = nativeName;
            }
            yield [key, new Resource(key, nativeName, localName)];
        }
    }

    return new ResourceMap(yieldResources());
}

/**
 * Creates a resource map containing the operator or function implementations
 * supported by the specified file format, resolved from an internal function
 * specification map.
 *
 * @param specModules
 *  The raw specifications of the operators or functions to be converted.
 *
 * @param fileFormat
 *  The identifier of the file format.
 *
 * @param localNames
 *  The localized names of the operators or functions, mapped by unique key.
 *
 * @returns
 *  The formula resource map containing descriptors for operators or functions.
 */
function createFunctionResourceMap(
    specModules: FunctionSpecRegisterModule[],
    fileFormat: FileFormatType,
    localNames: Dict
): FunctionResourceMap {

    // create a clone for deleting entries (needed to detect missing descriptors)
    const resolvedLocalNames = resolveDictForFileFormat(localNames, fileFormat);
    // collect intermediate resolvers (functions or strings) for second-step initialization below
    const resolvers = new Map<string, ResolvedFuncSpec["resolve"]>();

    // generator for function resources (converter from `FunctionSpecRegisterModule[]` elements to `FunctionResource`)
    function *yieldFunctionResources(): KeyedIterator<FunctionResource> {

        // process all specification modules
        for (const specModule of specModules) {

            // generate all function specifications by invoking the module callback function with a new factory object
            const factory = new FunctionSpecFactory(specModule);

            // process all generated function specifications
            for (const [key, funcSpec] of factory) {

                // resolve file format dependent properties of the implementation
                const resolvedSpec = resolveDictForFileFormat(funcSpec, fileFormat) as ResolvedFuncSpec;

                // get the native name(s) of the function or operator (default: resource key)
                const nativeNames = (resolvedSpec.name === null) ? [] : !resolvedSpec.name ? [key] : ary.wrap(resolvedSpec.name);

                // skip descriptors not supported in the specified file format
                if (nativeNames.length === 0) { continue; }

                // find the translated name
                const localName = pick.string(resolvedLocalNames, key);
                delete resolvedLocalNames[key];

                // skip descriptors without translation
                if (!localName) {
                    modelLogger.error(`$badge{FormulaResource} Missing translation: key=${key}`);
                    continue;
                }

                // collect all resolvers for deferred initialization below
                resolvers.set(key, resolvedSpec.resolve);

                // create the resource descriptor with preferred native name (first array element)
                yield [key, new FunctionResource(key, nativeNames, localName, resolvedSpec)];
            }
        }
    }

    // create the map with all function resource objects
    const resMap = new ResourceMap(yieldFunctionResources());

    // resolve string references to other resolver functions
    for (const [key, resource] of resMap) {
        let resolve = resolvers.get(key);
        while (is.string(resolve)) { resolve = resolvers.get(resolve); }
        if (resolve) { (resource as Writable<FunctionResource>).resolve = resolve; }
    }

    return resMap;
}

/**
 * Creates a unique key for the passed file format and locale data, that
 * contains everything needed to identify a specific `FormulaResource`
 * instance.
 *
 * @param fileFormat
 *  The identifier of the file format related to the formula resource data.
 *
 * @param localeData
 *  The locale data for the formula resources to be loaded. Uses the
 *  properties `lc`, `dec`, and `group` to create the unique key.
 */
function resourceKey(fileFormat: FileFormatType, localeData: Readonly<LocaleData>): string {
    return fileFormat + ":" + localeData.lc + ":" + localeData.dec + ":" + localeData.group;
}

// class FormulaResource ======================================================

/**
 * Contains and provides access to all native and localized resources used in
 * spreadsheet formulas, for example operator and function descriptors (syntax
 * descriptions, and actual implementations), function names, names of error
 * codes and boolean literals, and the help texts for functions and function
 * parameters. An instance of this class is not bound to a single application
 * instance but will be created once for all documents loaded from files with a
 * specific file format.
 *
 * @param fileFormat
 *  The identifier of the file format related to the formula resource data
 *  represented by this instance.
 *
 * @param locale
 *  The locale identifier of the formula resources to be loaded.
 */
export class FormulaResource extends DObject {

    // static functions -------------------------------------------------------

    /**
     * Returns an existing formula resource, or creates a new instance on
     * demand.
     *
     * Formula resources are expensive at creation, and thus will be shared by
     * all spreadsheet application instances.
     *
     * @param fileFormat
     *  The identifier of the file format related to the formula resource data.
     *
     * @param localeData
     *  The locale data for the formula resources to be loaded. Uses the
     *  properties `lc`, `dec`, and `group` to create the unique key.
     *
     * @returns
     *  A promise that will fulfil with the formula resource.
     */
    @memoizeMethod(resourceKey)
    @modelLogger.profileMethod("$badge{FormulaResource} create")
    static async create(fileFormat: FileFormatType, localeData: LocaleData): Promise<FormulaResource> {
        modelLogger.log(() => `$badge{FormulaResource} create: fileFormat="${fileFormat}", locale="${localeData.lc}"`);
        const jsonData = await loadFormulaResource(localeData);
        return new FormulaResource(fileFormat, localeData, jsonData);
    }

    // properties -------------------------------------------------------------

    /**
     * The file format of the formula syntax.
     */
    readonly fileFormat: FileFormatType;

    /**
     * The locale name.
     */
    readonly locale: string;

    /**
     * Resource map with the native and localized names of boolean literals.
     */
    readonly booleanMap: ResourceMap<BooleanKey>;

    /**
     * Resource map with the native and localized names of error code literals.
     */
    readonly errorMap: ResourceMap<ErrorKey>;

    /**
     * Resource map with the native and localized names of regions in table
     * ranges.
     */
    readonly regionMap: ResourceMap<TableRegionKey>;

    /**
     * Resource map with the native and localized names of parameters of the
     * function `CELL`.
     */
    readonly cellParamMap: ResourceMap<CellParamKey>;

    /**
     * Resoure map with the descriptors for all supported operators.
     */
    readonly operatorMap: FunctionResourceMap;

    /**
     * Resource map with the descriptors for all supported functions.
     */
    readonly functionMap: FunctionResourceMap;

    /** The localized decimal separator. */
    readonly #localDec: string;
    /** The localized group separator. */
    readonly #localGroup: string;
    /** The native function parameter separator. */
    readonly #nativeSep: string;
    /** The localized function parameter separator. */
    readonly #localSep: string;
    /** The localized R1C1 prefix characters. */
    readonly #localPrefixChars: string;

    // constructor ------------------------------------------------------------

    private constructor(fileFormat: FileFormatType, localeData: Readonly<LocaleData>, jsonData: Dict) {
        super({ _kind: "singleton" });
        modelLogger.info(`$badge{FormulaResource} Constructing instance for format "${fileFormat}" and locale "${localeData.lc}"`);

        // generic properties
        this.fileFormat = fileFormat;
        this.locale = localeData.lc;

        // formula delimiters
        this.#localDec = localeData.dec;
        this.#localGroup = localeData.group;
        this.#nativeSep = (fileFormat === FileFormatType.ODF) ? ";" : ",";
        this.#localSep = (this.#localDec === ",") ? ";" : ",";

        // create the string resource maps
        this.booleanMap = createStringResourceMap(NATIVE_BOOLEAN_NAMES, jsonData.booleans, "boolean");
        this.errorMap = createStringResourceMap(NATIVE_ERROR_NAMES, jsonData.errors, "error code");
        this.regionMap = createStringResourceMap(NATIVE_REGION_NAMES, jsonData.regions, "table region");
        this.cellParamMap = createStringResourceMap(NATIVE_CELL_PARAM_NAMES, jsonData.cellParams, "CELL parameter");

        // resolve operator and function descriptors, and the maps with native and translated names
        this.operatorMap = createFunctionResourceMap(OPERATOR_MODULES, fileFormat, { ...LOCAL_OPERATOR_NAMES, list: this.#localSep });
        this.functionMap = createFunctionResourceMap(FUNCTION_MODULES, fileFormat, pick.dict(jsonData, "functions", true));

        // the R1C1 prefix characters (ODF does not translate these characters in the UI)
        this.#localPrefixChars = (fileFormat === FileFormatType.ODF) ? NATIVE_RC_PREFIX_CHARS : pick.string(jsonData, "RC", "RC").toUpperCase();

        // process each entry in the function help map
        const rawHelpData = pick.dict(jsonData, "help", true);
        for (const [funcKey, rawHelpEntry] of dict.entries(rawHelpData)) {
            // the function descriptor resolved for the current file format (silently skip
            // function help for functions not supported by the current file format)
            this.functionMap.get(funcKey)?.initHelp(rawHelpEntry);
        }

        // add dummy help descriptors for all remaining functions without help resouces
        const missingHelpKeys: string[] = [];
        for (const [funcKey, funcRes] of this.functionMap) {
            if (!funcRes.help && !funcRes.hidden) {
                funcRes.initHelp({ d: "", p: [] }, true);
                missingHelpKeys.push(funcKey);
            }
        }

        // print all function names without help texts
        if (missingHelpKeys.length > 0) {
            modelLogger.warn(() => `$badge{FormulaResource} Missing help for functions: ${missingHelpKeys.sort().join(", ")}`);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the decimal separator for the current UI language.
     *
     * @param localized
     *  Whether to return the localized decimal separator according to the
     *  locale passed to the constructor (`true`), or the native decimal
     *  separator used in the file format (`false`).
     *
     * @returns
     *  The decimal separator character.
     */
    getDec(localized: boolean): string {
        return localized ? this.#localDec : ".";
    }

    /**
     * Returns the group separator for the current UI language.
     *
     * @param localized
     *  Whether to return the localized group separator according to the locale
     *  passed to the constructor (`true`), or the native group separator used
     *  in the file format (`false`).
     *
     * @returns
     *  The decimal separator character.
     */
    getGroup(localized: boolean): string {
        return localized ? this.#localGroup : ",";
    }

    /**
     * Returns the list separator character. The list separator is used to
     * separate parameters of functions, and in OOXML for the range list
     * operator.
     *
     * @param localized
     *  Whether to return the localized list separator character according to
     *  the locale passed to the constructor (`true`), or the native list
     *  separator character used in the file format (`false`).
     *
     * @returns
     *  The list separator character.
     */
    getSeparator(localized: boolean): string {
        return localized ? this.#localSep : this.#nativeSep;
    }

    /**
     * Returns both prefix characters used in R1C1 references (for column and
     * row).
     *
     * @param localized
     *  Whether to return the localized prefix characters according to the
     *  locale passed to the constructor (`true`), or the native prefix
     *  characters used in the file format (`false`).
     *
     * @returns
     *  The prefix characters used in R1C1 references (a string with two
     *  characters, first the row prefix, then the column prefix).
     */
    getRCPrefixChars(localized: boolean): string {
        return localized ? this.#localPrefixChars : NATIVE_RC_PREFIX_CHARS;
    }

    /**
     * Returns the help resources for the specified function.
     *
     * @param key
     *  The unique resource key of the function.
     *
     * @returns
     *  The help resources for the specified function; or `undefined` for
     *  invalid keys or functions without help resources.
     */
    getFunctionHelp(key: string): Opt<FunctionHelp> {
        const funcRes = this.functionMap.get(key);
        return funcRes ? funcRes.help : undefined;
    }
}

// class FormulaResourceFactory ===============================================

/**
 * A factory for formula resource instances.
 */
export class FormulaResourceFactory extends DObject {

    // static functions -------------------------------------------------------

    /**
     * Creates a factory for `FormulaResource` instances for the passed file
     * format and locale data.
     *
     * @param fileFormat
     *  The file format identifier.
     *
     * @param localeData
     *  The locale data to be used in translated formula grammars.
     *
     * @returns
     *  A promise that will fulfil with the formula resource factory.
     */
    @memoizeMethod(resourceKey)
    @modelLogger.profileMethod("$badge{FormulaResourceFactory} create")
    static async create(fileFormat: FileFormatType, localeData: Readonly<LocaleData>): Promise<FormulaResourceFactory> {
        modelLogger.log(() => `$badge{FormulaResourceFactory} create: fileFormat="${fileFormat}"`);

        // load all formula resources needed by the resource factory
        const stdPromise = FormulaResource.create(fileFormat, localeData);
        const ooxPromise = FormulaResource.create(FileFormatType.OOX, localeData);
        const enPromise = FormulaResource.create(fileFormat, localeDataRegistry.getLocaleData("en_US"));

        // wait for the formula resources, and create the factory
        const [stdResource, ooxResource, enResource] = await Promise.all([stdPromise, ooxPromise, enPromise]);
        return new FormulaResourceFactory({
            std: stdResource,
            oox: ooxResource,
            en:  enResource
        });
    }

    // properties -------------------------------------------------------------

    // cache for formula resources, mapped by resource type
    readonly #resources: Record<FormulaResourceType, FormulaResource>;

    // constructor ------------------------------------------------------------

    private constructor(resources: Record<FormulaResourceType, FormulaResource>) {
        super({ _kind: "singleton" });
        this.#resources = resources;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the formula resource instance used by the specified formula
     * grammar.
     *
     * @param type
     *  The type specifier of the formula resource.
     *
     * @returns
     *  The formula resource instance.
     */
    getResource(type: FormulaResourceType): FormulaResource {
        return this.#resources[type];
    }
}
