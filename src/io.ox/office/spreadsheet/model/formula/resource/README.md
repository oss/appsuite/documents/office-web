# Spreadsheet Formula Resources

This directory contains definitions for all localized text resources used in Spreadsheet formulas:

- Translated function names.
- Translated names for boolean literals, error codes, and table regions.
- Translated enumerations for function parameters, e.g. for the CELL function.
- Translated letters for R1C1 reference mode.
- Tooltip help texts for functions and function parameters.

The raw JSON data will be transformed by the internal Vite plugin [@open-xchange/vite-plugin-formula-resource](../../../../../../../vite/vite-plugin-formula-resource.ts) into data to be consumed by the Spreadsheet application.

## Add New Language

1. Add a JSON file in the directory `/data` for the new language. Follow the JSON syntax of existing files:

   - Root key in JSON data must be the language code without region (e.g. `"de"`).
   - The file may contain region-specific entries mapped by additional root keys (e.g. `"de_AT"`).
   - Each root entry must be an object with the following keys:
     - `RC`: Two upper-case letters used in RC references (column, row).
     - `booleans`: Translated names of Boolean literals, mapped by `t` and `f`.
     - `errors`: Translated names of error code literals, mapped by upper-case error key (e.g. `DIV0` for the error code `#DIV/0!`).
     - `regions`: Translated names of table regions, mapped by upper-case region key (e.g. `HEADERS` for the table region `#Headers`).
     - `functions`: Translated function names, mapped by upper-case function keys.
     - `cellParams`: Translated enumeration for the first `CELL` function parameter, mapped by upper-case key (e.g. `ADDRESS` for the `"address"` parameter).

2. Add a JSON file for help resources in the directory `/help` (optional). Usually, these files will be generated from another publicly available source.

3. Add a JSON file with a key mapping for the help resources in the directory `/mapping` (required, if help resources are available). This file maps the fixed internal function keys to the actual function names used in the help resources.

4. Commit and push everything. The Vite plugin will take care of all the rest.
