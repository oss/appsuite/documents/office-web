/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createListPattern } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";
import { formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";

// class Resource =============================================================

/**
 * Base class for single formula resource objects. Contains the native name,
 * and the localized name of an arbitrary formula resource object (an operator,
 * a function, etc.), and optionally other custom properties.
 */
export class Resource<KeyT> {

    /**
     * The unique key of the resource object.
     */
    readonly key: KeyT;

    /**
     * The native name of the resource object.
     */
    readonly nativeName: string;

    /**
     * The localized name of the resource object.
     */
    readonly localName: string;

    /**
     * Alternative native names that can be used for the resource object.
     */
    readonly altNativeNames: string[] = [];

    /**
     * Alternative localized names that can be used for the resource object.
     */
    readonly altLocalNames: string[] = [];

    // constructor ------------------------------------------------------------

    /**
     * @param key
     *  The unique key of the resource object.
     *
     * @param nativeName
     *  The native name of the resource object.
     *
     * @param localName
     *  The localized name of the resource object.
     */
    constructor(
        key: KeyT,
        nativeName: string,
        localName: string
    ) {
        this.key = key;
        this.nativeName = nativeName;
        this.localName = localName;
    }
}

// class ResourceMap ==========================================================

/**
 * A generator callback function that will be invoked for elements in a source
 * JSON map, and returns resource objects.
 *
 * @param data
 *  The map element (JSON data) to be converted to a resource object.
 *
 * @param key
 *  The resource key of the current map element.
 *
 * @returns
 *  A resource object; or nullish to skip the map element.
 */
export type ResourceGeneratorFunc<KeyT, ResT extends Resource<KeyT>, DataT> = (data: DataT, key: KeyT) => Nullable<ResT>;

/**
 * A map with formula resource objects of a specific type.
 */
export class ResourceMap<KeyT, ResT extends Resource<KeyT> = Resource<KeyT>> implements EntryIterable<KeyT, ResT> {

    readonly #map = new Map<KeyT, ResT>();

    /**
     * The keys of all resource objects as map values, mapped by their native
     * names.
     */
    readonly #nativeKeys = new Map<string, KeyT>();

    /**
     * The keys of all resource objects as map values, mapped by their
     * localized names.
     */
    readonly #localKeys = new Map<string, KeyT>();

    /**
     * The native names of all resource objects, as array.
     */
    readonly #nativeNames: string[] = [];

    /**
     * The localized names of all resource objects, as array.
     */
    readonly #localNames: string[] = [];

    /**
     * A regular expression pattern matching any of the supported native names.
     */
    readonly #nativePattern: string;

    /**
     * A regular expression patterns matching any of the supported localized
     * names.
     */
    readonly #localPattern: string;

    // constructor ------------------------------------------------------------

    constructor(source: EntryIterable<KeyT, ResT>) {

        // fill the containers from the data source
        for (const [key, resource] of source) {
            if (this.#map.has(key)) {
                const msg = `resource with key "${key}" exists already`;
                formulaLogger.error(`$badge{FunctionSpecFactory} ${msg}`);
                throw new Error(msg);
            }
            this.#map.set(key, resource);
            this.#nativeKeys.set(resource.nativeName.toUpperCase(), key);
            this.#localKeys.set(resource.localName.toUpperCase(), key);
            resource.altNativeNames.forEach(altName => this.#nativeKeys.set(altName.toUpperCase(), key));
            resource.altLocalNames.forEach(altName => this.#localKeys.set(altName.toUpperCase(), key));
            this.#nativeNames.push(resource.nativeName, ...resource.altNativeNames);
            this.#localNames.push(resource.localName, ...resource.altLocalNames);
        }

        // sort the native and localized names of all resource objects
        this.#nativeNames.sort();
        this.#localNames.sort();

        // RE patterns matching any of the supported names (sort by name length to create a greedy pattern)
        this.#nativePattern = createListPattern(this.#nativeNames);
        this.#localPattern = createListPattern(this.#localNames);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the resource object with the passed unique key.
     *
     * @param key
     *  The unique key of the resource object.
     *
     * @returns
     *  The resource object if existing.
     */
    get(key: KeyT): Opt<ResT> {
        return this.#map.get(key);
    }

    /**
     * Returns an iterator for all resource objects in this map.
     *
     * @returns
     *  An iterator for all resource objects in this map.
     */
    [Symbol.iterator](): EntryIterator<KeyT, ResT> {
        return this.#map.entries();
    }

    /**
     * Visits all resources in this map.
     *
     * @param fn
     *  The callback function invoked for each resource.
     */
    forEach(fn: (resource: ResT, key: KeyT) => void): void {
        this.#map.forEach(fn);
    }

    /**
     * Returns the native or localized name of the resource object with the
     * passed unique key.
     *
     * @param key
     *  The unique key of the resource object.
     *
     * @param localized
     *  Whether to return the localized name according to the UI language
     *  (`true`), or the native name as used in the file format (`false`).
     *
     * @returns
     *  The native or localized name of the resource object; or `null`, if the
     *  resource object does not exist.
     */
    getName(key: KeyT, localized: boolean): string | null {
        const resource = this.get(key);
        return !resource ? null : localized ? resource.localName : resource.nativeName;
    }

    /**
     * Converts the passed native or localized name of a resource object to its
     * unique key.
     *
     * @param name
     *  The native or localized name (case-insensitive).
     *
     * @param localized
     *  Whether the passed name is localized according to the UI language
     *  (`true`), or native as used in the file format (`false`).
     *
     * @returns
     *  The unique key of the resource object with the passed name; or `null`,
     *  if the resource object does not exist.
     */
    getKey(name: string, localized: boolean): KeyT | null {
        const keyMap = localized ? this.#localKeys : this.#nativeKeys;
        return keyMap.get(name.toUpperCase()) ?? null;
    }

    /**
     * Returns a sorted list with the native or localized names of all resource
     * objects in this collection.
     *
     * @param localized
     *  Whether to return the localized names according to the UI language
     *  (`true`), or the native names as used in the file format (`false`).
     *
     * @returns
     *  A sorted list with the native or localized names of all resource
     *  objects in this collection.
     */
    getAllNames(localized: boolean): readonly string[] {
        return localized ? this.#localNames : this.#nativeNames;
    }

    /**
     * Returns the pattern for a regular expression matching any of the native
     * or localized names of all resource objects in this collection.
     *
     * @param localized
     *  Whether to return a pattern for the localized names according to the UI
     *  language (`true`), or for the native names as used in the file format
     *  (`false`).
     *
     * @returns
     *  The pattern for a regular expression matching any of the native or
     *  localized names of all resource objects in this collection, in the form
     *  `"(name1|name2|name3|...)"`. The names in the pattern are sorted by
     *  length (longest names first) to result in a greedy pattern.
     */
    getAllPattern(localized: boolean): string {
        return localized ? this.#localPattern : this.#nativePattern;
    }
}
