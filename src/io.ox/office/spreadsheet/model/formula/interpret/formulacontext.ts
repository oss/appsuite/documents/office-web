/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, re, fun, ary, dict, pick } from "@/io.ox/office/tk/algorithms";

import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

import { getDebugFlag } from "@/io.ox/office/spreadsheet/utils/config";
import type { ErrorLiteral, ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { ErrorCode, CompareResult, isErrorCode, throwErrorCode, getScalarType, compareScalars } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { type Range3DSource, Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { type OptSheet, MAX_LENGTH_STANDARD_FORMULA } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { iterateParallelSynchronized } from "@/io.ox/office/spreadsheet/utils/iterator";

import type { ContextType, ConvertMode, CheckSizeMode, RefSheetOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { InternalErrorCode, MAX_EVAL_TIME, throwEngineError } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import { Complex } from "@/io.ox/office/spreadsheet/model/formula/utils/complex";
import type { MatrixGeneratorFn, ScalarMatrixNE, ScalarMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { getMatrixAddress, ensureMatrixDim, Matrix, NumberMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import type { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";

import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import type { ParamType, ParamTypeKey } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import type { OperandValType, OperandRefType, OperandAnyType, OperandRawType, OperandConfig } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import { Operand, OperandResolver } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import { type FilterAggregatorFunc, type FilterFinalizerFunc, FilterAggregator } from "@/io.ox/office/spreadsheet/model/formula/interpret/filteraggregator";

import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import type { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";
import { CellType } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

// types ======================================================================

/**
 * Optional parameters for the constructor of the class `FormulaContext`.
 */
export interface FormulaContextConfig extends RefSheetOptions {

    /**
     * If set to `true`, trying to dereference a cell reference covering the
     * target address leads to a circular dependeny error. Default value is
     * `false`.
     */
    detectCircular?: boolean;

    /**
     * If set to `true`, all dirty cell formulas resolved by cell reference
     * operands will be recalculated recursively using the dependency manager
     * of the document. Default value is `false`.
     */
    recalcDirty?: boolean;
}

/**
 * Optional parameters for converting scalar operand values to cell range
 * addresses.
 */
export interface ConvertToRangeOptions {

    /**
     * A specific error code literal that will be thrown if the cell range list
     * contains more than one cell range address. Default value is
     * `ErrorCode.REF`.
     */
    errorCode?: ErrorLiteral;

    /**
     * If set to `true`, the cell range address may refer to multiple sheets.
     * Default value is `false` (cell range must refer to a single sheet).
     */
    multiSheet?: boolean;
}

/**
 * The result descriptor of the method `FormulaContext#getCellFormula`.
 */
export interface FormulaDescriptor {

    /**
     * The localized formula expression of the specified cell.
     */
    formula: string;

    /**
     * Range of the matrix formula the cell is part of.
     */
    matrixRange: Range | null;

    /**
     * Whether the cell is part of a dynamic matrix formula.
     */
    dynamicMatrix: boolean;
}

/**
 * All data types accepted as source data for operand iterators.
 */
export type OperandIteratorSource = Operand | OperandRawType | Range3D;

/**
 * Common options for all methods of the class `FormulaContext` generating or
 * using operand iterators.
 */
export interface CommonIteratorOptions {

    /**
     * If set to `true`, the iterator will visit the empty function parameter
     * (represented by the value `null`). By default, empty parameters will be
     * skipped silently.
     */
    emptyParam?: boolean;

    /**
     * If set to `true`, a reference operand may contain multiple cell ranges,
     * e.g. by using the reference list operator. By default, a reference must
     * refer to a single range.
     */
    multiRange?: boolean;

    /**
     * If set to `true`, a reference operand may refer to multiple sheets. By
     * default, a reference must refer to a single sheet.
     */
    multiSheet?: boolean;

    /**
     * If set to `true`, the number format categories of cells referred by the
     * passed reference operand will be resolved and provided by the iterator.
     * Default value is `false`.
     */
    collectFormats?: boolean;

    /**
     * If set to `true`, cells in all hidden rows will be skipped. Cells in
     * visible rows and hidden columns will be visited. Default value is
     * `false`.
     */
    skipHiddenRows?: boolean;

    /**
     * If set to `true`, cells in all hidden rows will be skipped, if the sheet
     * contains at least one active filter range (either a table range, or an
     * auto-filter). Cells in visible rows and hidden columns will be visited.
     * Default value is `false`.
     */
    skipFilteredRows?: boolean;
}

/**
 * Options for generating or using operand iterators for scalar values of any
 * type.
 */
export interface ScalarIteratorOptions extends CommonIteratorOptions {

    /**
     * If set to `true`, the iterator will visit blank cells in a reference
     * operand (cells represented by the value `null`). By default, blank cells
     * will be skipped silently. Please use with caution, as this may become a
     * serious performance bottleneck for large cell ranges.
     */
    blankCells?: boolean;

    /**
     * If set to `true`, the iterator will visit error codes too. By default,
     * error codes will be thrown immediately.
     */
    acceptErrors?: boolean;
}

/**
 * Options for generating or using operand iterators for numbers.
 */
export interface ConvertIteratorOptions extends CommonIteratorOptions {

    /**
     * Conversion strategy for scalar operands.
     */
    valMode?: ConvertMode;

    /**
     * Conversion strategy for matrix operands.
     */
    matMode?: ConvertMode;

    /**
     * Conversion strategy for reference operands.
     */
    refMode?: ConvertMode;
}

/**
 * Options for generating or using operand iterators for numbers.
 */
export interface CommonNumberIteratorOptions extends ConvertIteratorOptions {

    /**
     * If set to `true`, error code literals passed as scalar value, contained
     * in matrixes, or contained in references, will be skipped silently. By
     * default, error code literals will be thrown immediately.
     */
    skipErrors?: boolean;

    /**
     * If set to `true`, the numbers will be rounded down to the next integer
     * (negative numbers will be rounded down too, i.e. away from zero!).
     * Default value is `false`.
     */
    floor?: boolean;
}

/**
 * Options for generating or using operand iterators for numbers or dates.
 */
export interface NumberIteratorOptions extends CommonNumberIteratorOptions {

    /**
     * If set to `true`, the numbers will be converted to date objects. If
     * conversion of a value to a date fails, the error code `#VALUE!` will be
     * thrown. If the option `floor` has been set to `true`, the time
     * components of the resulting dates will be removed (the time will be set
     * to midnight). Default value is `false`.
     */
    dates?: boolean;
}

/**
 * The type of the result value of an operand iterator visiting the scalar
 * values of the operand.
 */
export interface OperandState<T> {

    /**
     *  The scalar value of the operand currently visited.
     */
    value: T;

    /**
     * The sequence index of the scalar value AFTER filtering (a counter for
     * the scalar values that ignores all other skipped values in the operand).
     */
    index: number;

    /**
     * The relative position of the scalar value in the operand.
     * - For a scalar operand, this property will be zero.
     * - For matrixes, this will be the index of the matrix element (counted by
     *   rows).
     * - For cell references, this will be the index of the cell in all cells
     *   covered by the cell reference (counted across all cell ranges
     *   contained in the reference operand).
     */
    offset: number;

    /**
     * The relative column index of the scalar value in the operand.
     * - For a scalar operand, this property will be zero.
     * - For matrixes and cell references, this is the column index in the
     *   matrix or current cell range.
     */
    col: number;

    /**
     * The relative row index of the scalar value in the operand.
     * - For a scalar operand, this property will be zero.
     * - For matrixes and cell references, this is the row index in the matrix
     *   or current cell range.
     */
    row: number;

    /**
     * The parsed number format of the cell referred by a reference operand
     * (only if the option `collectFormats` has been set); or the value `null`
     * for any other operands.
     */
    format: ParsedFormat | null;
}

/**
 * An iterator visiting the operands of a function.
 */
export type OperandIterator<T> = IterableIterator<OperandState<T>>;

/**
 * A callback function invoked for each value provided by an operand iterator.
 * Calling context is the formula context instance.
 *
 * @param value
 *  The current scalar value.
 *
 * @param state
 *  The complete state information of the operand, including its value, offset,
 *  column/row indexes, and parsed number format.
 */
export type OperandIteratorFn<T> = (this: FormulaContext, value: T, state: OperandState<T>) => void;

/**
 * The type of the result value of an iterator visiting the scalar values of
 * multiple operands simultaneously.
 */
export type ParallelOperandState<T> = OperandState<T[]>;

/**
 * A parallel iterator visiting the scalar values of multiple operands
 * simultaneously.
 */
export type ParallelOperandIterator<T> = OperandIterator<T[]>;

/**
 * Options for creating a parallel operand iterator.
 */
export interface ParallelOperandIteratorOptions {

    /**
     * If set to `true`, the parallel operand iterator will stop only at the
     * offsets where _all_ source iterators provide a value. If one of the
     * iterators skips an offset, this offset will not be visited at all.
     *
     * By default, the parallel operand iterator will stop at all offsets where
     * _at least one_ source iterator provides a value.
     */
    complete?: boolean;
}

/**
 * All data types accepted as source data for operand aggregators.
 */
export type OperandAggregatorSource = readonly OperandIteratorSource[];

/**
 * A callback function to combine a scalar value of an operand with the current
 * intermediate result of an aggregation cycle.
 *
 * @param intermediate
 *  The current intermediate result. On first invocation, this is the value
 *  passed in the `initial` parameter of the running aggregation function.
 *
 * @param value
 *  The scalar value to be combined with the intermediate result.
 *
 * @param index
 *  The zero-based sequence index of the scalar value (a counter for the scalar
 *  values that ignores all other skipped values in the operands).
 *
 * @param offset
 *  The relative position of the scalar in all operands (including all skipped
 *  empty parameters, and blank cells).
 *
 * @returns
 *  The new intermediate result which will be passed to the next invocation of
 *  this callback function, or may throw an error code.
 */
export type AggregateAggregatorFn<I, S> = (this: FormulaContext, intermediate: I, value: S, index: number, offset: number) => I;

/**
 * A callback function invoked at the end of an operand aggregation cycle to
 * convert the intermediate result (returned by the last invocation of the
 * aggregation callback function) to the actual function result.
 *
 * @param intermediate
 *  The last intermediate result to be converted to the final result of the
 *  implemented function.
 *
 * @param count
 *  The total count of all visited numbers.
 *
 * @param size
 *  The total size of all operands, including all skipped values.
 *
 * @param format
 *  The parsed number format of the cell referred by reference operands (only
 *  if the option `collectFormats` has been set); or the value `null` for any
 *  other operands.
 *
 * @returns
 *  The final result of the aggregation, or an error code literal. The type of
 *  the final result does not matter, but must match the type key of the
 *  implemented function. Alternatively, the finalizer may throw an error code.
 */
export type AggregateFinalizerFn<T, I> = (this: FormulaContext, intermediate: I, count: number, size: number, format: ParsedFormat | null) => T | ErrorLiteral;

/**
 * A callback function invoked at the end of an operand aggregation cycle to
 * convert the intermediate result (array of numbers, as returned by the last
 * invocation of the aggregation callback function) to the actual function
 * result.
 *
 * @param intermediate
 *  The collected numbers from the operands, as plain JavaScript array.
 *
 * @param sum
 *  The sum of all collected numbers, for convenience.
 *
 * @param count
 *  The count of all collected numbers (the length of the array in the first
 *  parameter), for convenience.
 */
export type AggregateNumbersAsArrayFinalizerFn<T> = (this: FormulaContext, intermediate: number[], count: number, size: number) => T | ErrorLiteral;

/**
 * A callback function to aggregate the scalar values of multiple operands in
 * parallel.
 *
 * @param intermediate
 *  The current intermediate result. On first invocation, this is the value
 *  passed in the `initial` parameter of the running aggregation function.
 *
 * @param tuple
 *  The scalar values received from all operands to be combined with the
 *  intermediate result, as array.
 *
 * @param index
 *  The zero-based sequence index of the scalar values (a counter for the
 *  scalars that ignores all other skipped values in the operands).
 *
 * @returns
 *  The new intermediate result which will be passed to the next invocation of
 *  this callback function, or may throw an error code.
 */
export type AggregateParallelAggregatorFn<I, S> = (this: FormulaContext, intermediate: I, tuple: S[], index: number) => I;

/**
 * A callback function invoked at the end of a parallel operand aggregation
 * cycle to convert the intermediate result (returned by the last invocation of
 * the aggregation callback function) to the actual function result.
 *
 * @param intermediate
 *  The last intermediate result to be converted to the final result of the
 *  implemented function.
 *
 * @param count
 *  The count of all visited scalar tuples.
 *
 * @returns
 *  The final result of the aggregation, or an error code literal. The type of
 *  the final result does not matter, but must match the type key of the
 *  implemented function. Alternatively, the finalizer may throw an error code.
 */
export type AggregateParallelFinalizerFn<T, I> = (this: FormulaContext, intermediate: I, count: number) => T | ErrorLiteral;

/**
 * A callback function invoked at the end to convert the collected numbers to
 * the actual function result.
 *
 * @param tuples
 *  The collected number tuples, as two-dimensional JavaScript array. Each
 *  array element is a tuple with as many numbers as operands have been passed
 *  to this method. The first array element of the inner arrays corresponds to
 *  the first operand, and so on.
 *
 * @param sums
 *  The sums of all collected numbers, for convenience. The first array element
 *  is the sum of all numbers of the first operand, and so on.
 *
 * @param count
 *  The count of all collected numbers per operand (the length of the `tuples`
 *  array), for convenience.
 *
 * @returns
 *  The final result of the aggregation, or an error code literal. The type of
 *  the final result does not matter, but must match the type key of the
 *  implemented function. Alternatively, the finalizer may throw an error code.
 */
export type AggregateNumbersParallelAsArraysFinalizerFn<T> = (this: FormulaContext, tuples: number[][], sums: number[], count: number) => T | ErrorLiteral;

/**
 * Options for aggregating numbers in parallel.
 */
export interface NumberAggregatorOptions extends CommonNumberIteratorOptions {

    /**
     * Specifies how to check the size of the operands. Default value is
     * "exact".
     */
    checkSize?: CheckSizeMode;

    /**
     * The error code literal that will be thrown if the passed operands do not
     * have the same size. Default value is `ErrorCode.VALUE`.
     */
    sizeMismatchError?: ErrorLiteral;
}

/**
 * Optional parameters for the method `FormulaContext.countBlankCells()`.
 */
export interface CountBlankCellsOptions {

    /**
     * If set to `true`, empty strings (e.g. returned by a formula) will be
     * counted as blank cells too.
     */
    emptyStr?: boolean;
}

/**
 * A predicate function that takes a single scalar value, and returns whether
 * the passed scalar value matches a specific filter criterion wrapped
 * internally.
 *
 * @param value
 *  The scalar value to be matched against a filter criterion.
 *
 * @returns
 *  Whether the passed scalar value matches the filter criterion.
 */
export type ScalarFilterMatcher = (value: ScalarType) => boolean;

// private types --------------------------------------------------------------

interface ResolverStackEntry {
    resolver: OperandResolver;
    contextType: ContextType;
    matDim: Dimension | null;
    matRow: number;
    matCol: number;
}

type ConverterFn<T> = (value: ScalarType) => T;

type AggregateIteratorFn<T> = (this: FormulaContext, source: OperandIteratorSource, callback: OperandIteratorFn<T>, options?: ConvertIteratorOptions) => number;

// constants ==================================================================

// whether to disable timeout detection
const DEBUG_FORMULAS = getDebugFlag("spreadsheet:debug-formulas");

// private functions ==========================================================

/**
 * Throws the first error code found in the passed matrix. If no error code has
 * been found, the passed matrix will be returned (narrowed to the type
 * `ScalarMatrixNE`).
 *
 * @param matrix
 *  A matrix containing any scalar values used in formulas.
 *
 * @returns
 *  The passed matrix, if it does not contain an error code.
 *
 * @throws
 *  The first error code literal contained in the passed matrix.
 */
function ensureScalarMatrixNE(matrix: ScalarMatrix): ScalarMatrixNE {
    matrix.forEach(throwErrorCode);
    return matrix as ScalarMatrixNE;
}

/**
 * Checks that all elements in the passed matrix are passing the specified type
 * predicate. If any matrix element fails, the error code `#VALUE!` will be
 * thrown. If any matrix element is an error code, it will be thrown instead.
 * If all matrix elements are valid, the passed matrix will be returned
 * (narrowed to the type of the passed type predicate).
 *
 * @param matrix
 *  A matrix containing any scalar values used in formulas.
 *
 * @param predicate
 *  The predicate callback function invoked for every matrix element.
 *
 * @returns
 *  The passed matrix, if all its elements are valid according to the test.
 *
 * @throws
 *  The first error code contained in the passed matrix, or the error code
 *  `#VALUE!`, if a matrix element does not pass the truth test.
 */
function ensureTypedMatrix<T extends ScalarType>(matrix: ScalarMatrix, predicate: (value: ScalarType) => value is T): Matrix<T> {
    matrix.forEach(elem => {
        throwErrorCode(elem);
        if (!predicate(elem)) { throw ErrorCode.VALUE; }
    });
    return matrix as Matrix<T>;
}

/**
 * Checks that all elements in the passed matrix are numbers. If any matrix
 * element fails, the error code `#VALUE!` will be thrown. If any matrix
 * element is an error code, it will be thrown instead. If all matrix elements
 * are numbers, an instance of `NumberMatrix` will be returned.
 *
 * @param matrix
 *  A matrix containing any scalar values used in formulas.
 *
 * @returns
 *  The passed matrix, if all its elements are numbers.
 *
 * @throws
 *  The first error code contained in the passed matrix, or the error code
 *  `#VALUE!`, if a matrix element is not a number.
 */
function ensureNumberMatrix(matrix: ScalarMatrix): NumberMatrix {
    return (matrix instanceof NumberMatrix) ? matrix : new NumberMatrix(ensureTypedMatrix(matrix, is.number));
}

/**
 * Checks that all cell range addresses in the passed list refer to the same
 * single sheet.
 *
 * @param ranges
 *  The cell range list to be checked.
 *
 * @returns
 *  The cell range list passed to this function, if it is valid.
 *
 * @throws
 *  An instance of `ErrorLiteral` on any error:
 *  - The error code `#NULL!`, if the range array is empty.
 *  - The error code `#VALUE!`, if the ranges refer to different sheets.
 */
function checkSingleSheetRanges(ranges: Range3DArray): Range3DArray {
    if (ranges.empty()) { throw ErrorCode.NULL; }
    if (!ranges.singleSheet()) { throw ErrorCode.VALUE; }
    return ranges;
}

/**
 * Returns the data type key of the passed operand.
 */
function getOperandType(source: OperandIteratorSource): ContextType {
    if (source instanceof Operand) { return source.type; }
    if (source instanceof Matrix) { return "mat"; }
    if ((source instanceof Range3D) || (source instanceof Range3DArray)) { return "ref"; }
    return "val";
}

/**
 * Returns the size of the passed operand.
 */
function getOperandSize(source: OperandIteratorSource): number {
    const value = (source instanceof Operand) ? source.getRawValue() : source;
    if (value instanceof Matrix) { return value.size(); }
    if ((value instanceof Range3D) || (value instanceof Range3DArray)) { return value.cells(); }
    return 1; // scalar value
}

/**
 * Creates a scalar converter callback function for typed operand iterators.
 */
function createConverterFn<T extends ScalarType>(
    source: OperandIteratorSource,
    options: Opt<ConvertIteratorOptions>,
    generator: (convertMode: ConvertMode) => ConverterFn<T | null> | null
): ConverterFn<T | null> {

    // conversion strategy
    const convertMode = pick.string(options, getOperandType(source) + "Mode", "convert") as ConvertMode;

    // call the generator function with the conversion mode for the operand
    const converter = generator(convertMode);

    // throw internal error on invalid conversion mode
    return converter ?? throwEngineError(`createConverterFn(): unknown conversion mode: "${convertMode}"`);
}

/**
 * Creates an iterator that filters all `null` values provided by the passed
 * iterator, and adds the sequence index to the result.
 *
 * @yields
 *  The non-null operands with their sequence indexes.
 */
function *filterNullOperands<T>(iterator: OperandIterator<T>): OperandIterator<Exclude<T, null>> {
    let index = 0;
    for (const opState of iterator) {
        if (opState.value !== null) {
            opState.index = index;
            index += 1;
            yield opState as OperandState<Exclude<T, null>>;
        }
    }
}

/**
 * A static filter matcher callback function that matches non-blank cells (any
 * scalar value but `null`).
 *
 * @param value
 *  A scalar value to be matched by a data filter.
 *
 * @returns
 *  Whether the passed value is not `null`.
 */
function scalarMatcher(value: ScalarType): boolean {
    return value !== null;
}

/**
 * A static filter matcher callback function that matches blank cells only
 * (represented by the value `null`).
 *
 * @param value
 *  A scalar value to be matched by a data filter.
 *
 * @returns
 *  Whether the passed value is `null`.
 */
function blankMatcher(value: ScalarType): boolean {
    return value === null;
}

/**
 * A static filter matcher callback function that matches blank cells
 * (represented by the value null), and cells with empty strings (may be
 * returned by formula cells).
 *
 * @param value
 *  A scalar value to be matched by a data filter.
 *
 * @returns
 *  Whether the passed value is null or an empty string.
 */
function emptyMatcher(value: ScalarType): boolean {
    return (value === "") || (value === null);
}

/**
 * A static filter matcher callback function that does not match anything.
 *
 * @returns
 *  The boolean value `false`.
 */
function nothingMatcher(): false {
    return false;
}

// public functions ===========================================================

/**
 * A set with numeric number format categories.
 */
const NUMERIC_SET = dict.fill("number scientific currency percent fraction", true);

/**
 * A set with date/time format categories.
 */
const DATETIME_SET = dict.fill("date time datetime", true);

/**
 * Resolves the number format for adding two formatted numbers.
 *
 * @param parsedFormat1
 *  The first parsed number format, or `null` to indicate an invalid number
 *  format that cannot be combined with the other number format.
 *
 * @param parsedFormat2
 *  The second parsed number format, or `null` to indicate an invalid number
 *  format that cannot be combined with the other number format.
 *
 * @returns
 *  One of the parsed number formats passed to this function to be used for the
 *  sum of two formatted numbers; or `null`, if the format categories of the
 *  number formats cannot be combined.
 */
export function combineParsedFormats(parsedFormat1: ParsedFormat | null, parsedFormat2: ParsedFormat | null): ParsedFormat | null {

    // skip invalid categories (null values)
    if (!parsedFormat1 || !parsedFormat2) { return null; }

    // skip standard category
    if (parsedFormat1.isStandard()) { return parsedFormat2; }
    if (parsedFormat2.isStandard()) { return parsedFormat1; }

    // separate handling for numeric categories, and date/time categories
    const isNumeric1 = parsedFormat1.category in NUMERIC_SET;
    const isNumeric2 = parsedFormat2.category in NUMERIC_SET;
    const isDateTime1 = !isNumeric1 && (parsedFormat1.category in DATETIME_SET);
    const isDateTime2 = !isNumeric2 && (parsedFormat2.category in DATETIME_SET);

    // dates and times win over numeric categories
    if (isDateTime1 && isNumeric2) { return parsedFormat1; }
    if (isNumeric1 && isDateTime2) { return parsedFormat2; }

    // first of two numeric categories wins (TODO: more specific precedences?)
    if (isNumeric1 && isNumeric2) { return parsedFormat1; }

    // anything else cannot be combined (e.g. two dates/times)
    return null;
}

// class FormulaContext =======================================================

/**
 * An instance of this class serves as calling context object for the
 * implementations of operators and functions, so that the symbol `this` inside
 * these implementations provides various useful helper methods.
 */
export class FormulaContext {

    /**
     * The spreadsheet document model.
     */
    readonly docModel: SpreadsheetModel;

    /**
     * Whether the file format of the spreadsheet document is OOXML, and the
     * formula will be interpreted as in MS Excel.
     */
    readonly OOXML: boolean;

    /**
     * Whether the file format of the spreadsheet documenbt is ODF, and the
     * formula will be interpreted as in OpenOffice.
     */
    readonly ODF: boolean;

    /**
     * The cell address factory of the document.
     */
    readonly addressFactory: AddressFactory;

    /**
     * The number formatter of the document model.
     */
    readonly numberFormatter: NumberFormatter;

    /**
     * The formula resource containing localized configuration for formulas.
     */
    readonly formulaResource: FormulaResource;

    /**
     * The formula grammar configuration for the current UI language, the
     * current file format of the document, and the current reference style of
     * the document (A1 or R1C1).
     */
    readonly formulaGrammar: FormulaGrammar;

    /** The reference sheet of the formula. */
    readonly #refSheet: OptSheet;

    /** The source reference address (anchor of matrix formulas). */
    readonly #refAddress: Address;

    /** The target reference address (address of current target element in a matrix formula). */
    readonly #targetAddress: Address;

    /** Whether to check the target address for circular reference errors. */
    readonly #detectCircular: boolean;

    /** Whether to recalculate all dirty cell formulas resolved by cell references. */
    readonly #recalcDirty: boolean;

    /** The standard number format. */
    readonly #standardFormat: ParsedFormat;

    /** Maximum length of a string, maximum valid string index (one-based). */
    readonly #MAX_STRING_LEN: number;

    /** Maximum time stamp after which interpreting a formula will fail (prevent freezing browser). */
    #END_EVAL_TIME = Date.now() + MAX_EVAL_TIME;

    /** Operand resolvers of ancestor operators/functions. */
    readonly #resolverStack: ResolverStackEntry[] = [];

    /** Provides access to the array of operands of the current function/operator. */
    #operandResolver = new OperandResolver();

    /** Current context type for an operator. */
    #contextType: ContextType = "any";

    // dimension of the result matrix (in matrix context)
    #matDim: Dimension | null = null;

    /** Current row index while calculating the results of a matrix. */
    #matRow = 0;

    /** Current column index while calculating the results of a matrix. */
    #matCol = 0;

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The spreadsheet document model containing this instance.
     *
     * @param targetAddress
     *  The address of the target reference cell in the specified reference
     *  sheet the formula is related to.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(docModel: SpreadsheetModel, targetAddress: Address, config?: FormulaContextConfig) {

        this.docModel = docModel;
        this.OOXML = docModel.docApp.isOOXML();
        this.ODF = docModel.docApp.isODF();

        this.addressFactory = docModel.addressFactory;
        this.numberFormatter = docModel.numberFormatter;
        this.formulaResource = docModel.formulaResource;
        this.formulaGrammar = docModel.formulaGrammarUI;

        this.#refSheet = pick.number(config, "refSheet") ?? null;
        this.#refAddress = targetAddress.clone();
        this.#targetAddress = targetAddress.clone();

        this.#detectCircular = !!config?.detectCircular;
        this.#recalcDirty = !!config?.recalcDirty;

        this.#standardFormat = this.numberFormatter.standardFormat;
        this.#MAX_STRING_LEN = this.ODF ? 65535 : 32767;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the context type that will be used to evaluate the current
     * function or operator.
     *
     * @returns
     *  The current context type.
     */
    get contextType(): ContextType {
        return this.#contextType;
    }

    // data type conversion ---------------------------------------------------

    /**
     * Tries to convert the passed scalar value to a floating-point number.
     *
     * @param value
     *  A scalar value to be converted to a number.
     *  - Bigints and dates will be converted to numbers.
     *  - Strings that represent a valid number (according to the current GUI
     *    language) will be converted to the number.
     *  - The boolean value `false` will be converted to `0`.
     *  - The boolean value `true` will be converted to `1`.
     *  - The special value `null` representing an empty cell will be converted
     *    to `0`.
     *
     * @param [toInt=false]
     *  If set to `true`, the number will be rounded down to the next integer
     *  (negative numbers will be rounded down too, i.e. away from zero!).
     *
     * @returns
     *  The floating-point number, if available.
     *
     * @throws
     *  If the passed value is an error code literal, it will be thrown. If the
     *  value cannot be converted to a floating-point number, the error code
     *  `#VALUE!` will be thrown.
     */
    convertToNumber(value: OperandValType, toInt?: boolean): number {

        // immediately throw error codes
        throwErrorCode(value);

        // convert the scalar value to a number
        value = this.numberFormatter.convertScalarToNumber(value, toInt);

        // throw #VALUE! error code, if conversion has failed
        return value ?? ErrorCode.VALUE.throw();
    }

    /**
     * Tries to convert the passed scalar value to a `bigint`.
     *
     * @param value
     *  A scalar value to be converted to a `bigint`.
     *  - Integer numbers and dates will be converted to bigints (date serial
     *    number without time).
     *  - Strings that represent a valid integer (according to the current GUI
     *    language) will be converted to a bigint representing that number.
     *  - The boolean value `false` will be converted to `0n`.
     *  - The boolean value `true` will be converted to `1n`.
     *  - The special value `null` representing an empty cell will be converted
     *    to `0n` too.
     *
     * @returns
     *  The bigint value, if available.
     *
     * @throws
     *  If the passed value is an error code literal, it will be thrown. If the
     *  resulting number has a fractional part, the error code #NUM! will be
     *  thrown. If the value cannot be converted to a bigint, the error code
     *  `#VALUE!` will be thrown.
     */
    convertToBigInt(value: OperandValType): bigint {

        // immediately throw error codes
        throwErrorCode(value);

        // convert the scalar value to a number
        value = this.numberFormatter.convertScalarToNumber(value);

        // throw #VALUE! error code, if conversion has failed
        if (value === null) { throw ErrorCode.VALUE; }

        // try to convert number to bigint (throws for fractions and too large numbers)
        try {
            return BigInt(value);
        } catch {
            throw ErrorCode.NUM;
        }
    }

    /**
     * Tries to convert the passed scalar value to a UTC date.
     *
     * @param value
     *  A scalar value to be converted to a date.
     *  - Strings that represent a valid number (according to the current GUI
     *    language) will be converted to a date representing that number.
     *  - The boolean value `false` will be converted to the null date of the
     *    number formatter.
     *  - The boolean value `true` will be converted to the day following the
     *    null date.
     *  - The special value `null` representing an empty cell will be converted
     *    to the null date too.
     *
     * @param [toInt=false]
     *  If set to `true`, the time components of the resulting date will be
     *  removed (the time will be set to midnight).
     *
     * @returns
     *  The UTC date, if available.
     *
     * @throws
     *  If the passed value is an error code literal, it will be thrown. If the
     *  value cannot be converted to a date value, the error code `#VALUE!`
     *  will be thrown.
     */
    convertToDate(value: OperandValType, toInt?: boolean): Date {

        // immediately throw error codes
        throwErrorCode(value);

        // convert the scalar value to a date
        value = this.numberFormatter.convertScalarToDate(value, toInt);

        // throw #VALUE! error code, if conversion has failed
        return value ?? ErrorCode.VALUE.throw();
    }

    /**
     * Tries to convert the passed scalar value to a string.
     *
     * @param value
     *  A scalar value to be converted to a string.
     *  - Numbers and dates will be converted to decimal or scientific notation
     *    (according to the current GUI language).
     *  - Boolean values will be converted to their localized text
     *    representation (formulas will always be calculated using the current
     *    GUI language).
     *  - The special value `null` representing an empty cell will be converted
     *    to the empty string.
     *
     * @returns
     *  The string representation of the value, if available.
     *
     * @throws
     *  If the passed value is an error code literal, it will be thrown. All
     *  other values will be converted to strings.
     */
    convertToString(value: OperandValType): string {

        // immediately throw error codes
        throwErrorCode(value);

        // convert the scalar value to a string
        value = this.numberFormatter.convertScalarToString(value, MAX_LENGTH_STANDARD_FORMULA);

        // throw #VALUE! error code, if conversion has failed
        return value ?? ErrorCode.VALUE.throw();
    }

    /**
     * Tries to convert the passed scalar value to a boolean value.
     *
     * @param value
     *  A scalar value to be converted to a boolean.
     *  - Floating-point numbers will be converted to `true` if not zero,
     *    otherwise `false`.
     *  - The null date (without time) will be converted to `false`, all other
     *    dates will be converted to `true`.
     *  - Strings containing the exact translated name of the boolean values
     *    (case-insensitive) will be converted to the respective boolean value
     *    (formulas will always be calculated using the current GUI language).
     *  - The special value `null` representing an empty cell will be converted
     *    to `false`.
     *
     * @returns
     *  The boolean value, if available.
     *
     * @throws
     *  If the passed value is an error code literal, it will be thrown. If the
     *  value cannot be converted to a boolean, the error code `#VALUE!` will
     *  be thrown.
     */
    convertToBoolean(value: OperandValType): boolean {

        // immediately throw error codes
        throwErrorCode(value);

        // convert the scalar value to a boolean
        value = this.numberFormatter.convertScalarToBoolean(value);

        // throw #VALUE! error code, if conversion has failed
        return value ?? ErrorCode.VALUE.throw();
    }

    /**
     * Tries to convert the passed scalar value to a complex number.
     *
     * @param value
     *  A scalar value to be converted to a complex number.
     *  - Floating-point numbers will be converted to complex numbers with the
     *    imaginary coefficient set to zero.
     *  - Strings must represent a valid complex number (optional leading
     *    signed real coefficient, optional trailing signed imaginary
     *    coefficient, followed by the lower-case character `i` or `j`). The
     *    coefficients must containing the decimal separator of the current GUI
     *    language.
     *  - The special value `null` representing an empty cell will be converted
     *    to the complex number `0`.
     *
     * @returns
     *  The complex number, if available.
     *
     * @throws
     *  If the passed value is an error code literal, it will be thrown.
     *  Boolean values will cause to throw the error code `#VALUE!`. If the
     *  string cannot be converted to a complex number, the error code `#NUM!`
     *  will be thrown (not the error code `#VALUE!` as thrown by the other
     *  conversion methods).
     */
    convertToComplex(value: OperandValType): Complex {

        // immediately throw error codes
        throwErrorCode(value);

        // boolean values result in #VALUE! (but not invalid strings)
        if (is.boolean(value)) { throw ErrorCode.VALUE; }

        // treat date objects as plain unformatted numbers
        if (value instanceof Date) {
            value = this.convertToNumber(value);
        }

        // convert numbers and strings to complex numbers
        if (Number.isFinite(value)) {
            value = new Complex(value as number, 0);
        } else if (is.string(value)) {
            value = this.numberFormatter.convertStringToComplex(value);
        } else if (value === null) {
            // empty cells are treated as complex number 0 (in difference to empty strings)
            value = new Complex(0, 0);
        }

        // the resulting value must be a complex number (invalid strings will
        // result in #NUM! instead of #VALUE!)
        return (value instanceof Complex) ? value : ErrorCode.NUM.throw();
    }

    /**
     * Extracts a single cell range from the passed cell range list. The range
     * list must contain exactly one cell range address. Unless specified via
     * an option, the cell range must refer to a single sheet.
     *
     * @param ranges
     *  A cell range list, or a single cell range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The cell range address, if available.
     *
     * @throws
     *  An error code literal, if the cell range list cannot be converted to a
     *  cell range address:
     *  - The error code `#NULL!`, if the cell range list is empty.
     *  - The error code `#REF!` (or the error code passed with the option
     *    `errorCode`), if the range list consists of more than one element.
     *  - The error code `#VALUE!`, if the option `multiSheet` has not been
     *    set, and a single cell range address is available but refers to
     *    multiple sheets.
     */
    convertToRange(ranges: Range3DSource, options?: ConvertToRangeOptions): Range3D {

        // convert parameter to a range array
        const ranges2 = Range3DArray.cast(ranges);

        // must not be empty
        if (ranges2.empty()) { throw ErrorCode.NULL; }

        // must be a single range in a single sheet
        if (ranges2.length > 1) {
            throw options?.errorCode ?? ErrorCode.REF;
        }

        // must point to a single sheet, unless multiple sheets are allowed (always throw the #VALUE! error code)
        const range = ranges2.first()!;
        return (options?.multiSheet || range.singleSheet()) ? range : ErrorCode.VALUE.throw();
    }

    /**
     * Extracts a single cell range from the passed cell range list, intended
     * to be resolved to the cell contents in that range. The range list must
     * contain exactly one cell range address pointing to a single sheet, and
     * the reference cell of this formula context must not be part of that cell
     * range.
     *
     * @param ranges
     *  A cell range list, or a single cell range address.
     *
     * @returns
     *  The resulting cell range address, if available. The sheet indexes in
     *  that range will be equal.
     *
     * @throws
     *  An error code literal, if the cell range list cannot be converted to a
     *  cell range address:
     *  - The error code `#NULL!`, if the cell range list is empty.
     *  - The error code `#VALUE!`, if the range list contains more than one
     *    element, or if the only range refers to multiple sheets.
     *  - The internal error code `#CIRCULAR`, if a single range is available,
     *    but the reference cell is part of that range (a circular reference).
     */
    convertToValueRange(ranges: Range3DSource): Range3D {
        // resolve to a single range (throw #VALUE! on multiple ranges), detect circular references
        return this.#checkCircularReference(this.convertToRange(ranges, { errorCode: ErrorCode.VALUE }));
    }

    /**
     * Converts an operand according to the parameter type key used in the
     * operator or function signature.
     *
     * @param operand
     *  The operand to be converted.
     *
     * @param typeKey
     *  The parameter type key used to decide how to convert the operand value.
     *
     * @returns
     *  The resulting operand value, if available, according to the passed type
     *  key.
     *
     * @throws
     *  Any error code that occured while trying to convert the operand value.
     */
    convertOperandToValue(operand: Operand, typeKey: ParamTypeKey): OperandAnyType | Operand {
        switch (typeKey) {
            case "val":
                return throwErrorCode(this.#getScalar(operand));
            case "val:num":
                return this.convertToNumber(this.#getScalar(operand));
            case "val:int":
                return this.convertToNumber(this.#getScalar(operand), true);
            case "val:bint":
                return this.convertToBigInt(this.#getScalar(operand));
            case "val:date":
                return this.convertToDate(this.#getScalar(operand));
            case "val:day":
                return this.convertToDate(this.#getScalar(operand), true);
            case "val:str":
                return this.convertToString(this.#getScalar(operand));
            case "val:bool":
                return this.convertToBoolean(this.#getScalar(operand));
            case "val:comp":
                return this.convertToComplex(this.#getScalar(operand));
            case "val:any":
                return this.#getScalar(operand);
            case "mat":
                return ensureScalarMatrixNE(operand.getMatrix());
            case "mat:num":
                // no conversion from other data types in matrix parameters!
                return ensureNumberMatrix(operand.getMatrix());
            case "mat:str":
                // no conversion from other data types in matrix parameters!
                return ensureTypedMatrix(operand.getMatrix(), is.string);
            case "mat:bool":
                // no conversion from other data types in matrix parameters!
                return ensureTypedMatrix(operand.getMatrix(), is.boolean);
            case "mat:any":
                return operand.getMatrix();
            case "ref":
                return operand.getRanges();
            case "ref:sheet":
                return checkSingleSheetRanges(operand.getRanges());
            case "ref:single":
                return this.convertToRange(operand.getRanges());
            case "ref:multi":
                return this.convertToRange(operand.getRanges(), { multiSheet: true });
            case "ref:val":
                return this.convertToValueRange(operand.getRanges());
            case "any":
            case "any:lazy":
                return operand;
        }
        throwEngineError(`FormulaContext.convertOperandToValue(): unknown type key: "${typeKey}"`);
    }

    /**
     * Returns whether the passed scalar values are considered to be equal
     * according to the current file format. For ODF documents, the character
     * case of strings matters. Provided to help implementing the comparison
     * operators.
     *
     * @param value1
     *  The first value to be compared.
     *
     * @param value2
     *  The second value to be compared.
     *
     * @returns
     *  Whether the passed scalar values are considered to be equal.
     */
    equalScalars(value1: ScalarType, value2: ScalarType): boolean {
        return this.docModel.formulaInterpreter.equalScalars(value1, value2);
    }

    /**
     * Compares the passed scalar values according to the current file format.
     * For ODF documents, the character case of strings matters. Provided to
     * help implementing the comparison operators.
     *
     * @param value1
     *  The first value to be compared.
     *
     * @param value2
     *  The second value to be compared.
     *
     * @returns
     *  The result of the coparison.
     */
    compareScalars(value1: ScalarType, value2: ScalarType): CompareResult {
        return this.docModel.formulaInterpreter.compareScalars(value1, value2);
    }

    /**
     * Converts the passed scalar value to be stored in an operand.
     * - A date will be converted to a floating-point number, if possible.
     * - If the value is `INF` or `NaN`, the error code `#NUM!` will be
     *   returned instead.
     * - If the number is too close to zero (using a denormalized binary
     *   representation internally), zero will be returned.
     * - If a string is too long, the error code `#VALUE!` will be returned.
     * - Complex numbers will be converted to their string representation,
     *   unless one of the coefficients is `INF` or `NaN` which will result in
     *   the error code `#NUM!` too.
     *
     * @param value
     *  The value to be validated.
     *
     * @returns
     *  The validated value, ready to be inserted into a formula operand.
     */
    validateValue(value: number | Date): number | ErrorLiteral;
    validateValue(value: string | Complex): string | ErrorLiteral;
    validateValue(value: boolean): boolean;
    validateValue(value: null): null;
    validateValue(value: ErrorLiteral): ErrorLiteral;
    validateValue(value: OperandValType): ScalarType;
    // implementation
    validateValue(value: OperandValType): ScalarType {

        // convert date object to floating-point number
        if (value instanceof Date) {
            value = this.numberFormatter.convertDateToNumber(value);
            return is.number(value) ? value : ErrorCode.NUM;
        }

        // convert complex numbers to strings
        if (value instanceof Complex) {
            // convert INF or NaN to #NUM! error
            if (!Number.isFinite(value.real) || !Number.isFinite(value.imag)) { return ErrorCode.NUM; }
            // convert very small (denormalized) numbers to zero
            if (math.isZero(value.real)) { value.real = 0; }
            if (math.isZero(value.imag)) { value.imag = 0; }
            // convert to string
            value = this.numberFormatter.convertComplexToString(value, MAX_LENGTH_STANDARD_FORMULA);
        }

        // validate floating-point numbers
        if (is.number(value)) {
            // convert INF or NaN to #NUM! error
            if (!Number.isFinite(value)) { return ErrorCode.NUM; }
            // convert very small (denormalized) numbers to zero
            return math.isZero(value) ? 0 : value;
        }

        // convert bigints to numbers
        if (is.bigint(value)) {
            value = Number(value);
            // convert INF or NaN to #NUM! error
            return Number.isFinite(value) ? value : ErrorCode.NUM;
        }

        // validate strings
        if (is.string(value)) {
            // check maximum length of string (depends on file format)
            return (value.length <= this.#MAX_STRING_LEN) ? value : ErrorCode.VALUE;
        }

        // no conversion for other values
        return value;
    }

    /**
     * Checks the passed string length used in string functions. Throws the
     * error code `#VALUE!`, if the index is less than zero, or larger than the
     * maximum length of string results supported by the formula engine.
     *
     * @param length
     *  The string length to be checked.
     *
     * @throws
     *  The error code `#VALUE!`, if the passed string length is invalid.
     */
    checkStringLength(length: number): void {
        if ((length < 0) || (length > this.#MAX_STRING_LEN)) { throw ErrorCode.VALUE; }
    }

    /**
     * Checks the passed character index used in string functions. Throws the
     * error code `#VALUE!`, if the index is less than one, or larger than the
     * maximum length of string results supported by the formula engine.
     *
     * @param index
     *  The character index to be checked.
     *
     * @throws
     *  The error code `#VALUE!`, if the passed character index is invalid.
     */
    checkStringIndex(index: number): void {
        if ((index < 1) || (index > this.#MAX_STRING_LEN)) { throw ErrorCode.VALUE; }
    }

    /**
     * Concatenates the passed strings, and validates the length of the
     * resulting string.
     *
     * @param args
     *  The strings to be concatenated.
     *
     * @returns
     *  The concatenation of the passed strings.
     *
     * @throws
     *  The error code `#VALUE!`, if the length of the resulting string would
     *  be invalid.
     */
    concatStrings(...args: string[]): string {
        const result = args.reduce((str1, str2) => str1 + str2, "");
        this.checkStringLength(result.length);
        return result;
    }

    /**
     * Repeats the passed string, and validates the length of the resulting
     * string.
     *
     * @param text
     *  The string to be repeated.
     *
     * @param repeat
     *  The number of repetitions.
     *
     * @returns
     *  The resulting repeated string.
     *
     * @throws
     *  The error code `#VALUE!`, if the length of the resulting string would
     *  be invalid.
     */
    repeatString(text: string, repeat: number): string {
        this.checkStringLength(text.length * repeat);
        return text.length ? text.repeat(repeat) : "";
    }

    /**
     * Checks whether the passed cell reference structure is valid.
     *
     * @param cellRef
     *  The cell reference structure to be checked.
     *
     * @throws
     *  The error code `#VALUE!`, if the passed cell reference is not valid.
     */
    checkCellRef(cellRef: CellRef): void {
        if (!this.addressFactory.isValidAddress(cellRef.toAddress())) { throw ErrorCode.VALUE; }
    }

    /**
     * Checks whether the passed cell range addresses have equal dimensions
     * (width and height, ignoring the sheet indexes of 3D ranges).
     *
     * @param range1
     *  The first cell range to be checked.
     *
     * @param range2
     *  The second cell range to be checked.
     *
     * @throws
     *  The error code `#VALUE!`, if the dimensions (width and height) of the
     *  passed cell range addresses are different.
     */
    checkRangeDims(range1: Range, range2: Range): void {
        if (!Range.prototype.equalSize.call(range1, range2)) { throw ErrorCode.VALUE; }
    }

    /**
     * Checks the elapsed time since this formula context instance has been
     * created. If the duration exceeds the limit set in the constant
     * `MAX_EVAL_TIME`, the internal error code `#TIMEOUT` will be thrown.
     *
     * @throws
     *  The internal error code `#TIMEOUT`, if the duration exceeds the
     *  configured time limit.
     */
    checkEvalTime(): void {
        if (DEBUG_FORMULAS && (Date.now() > this.#END_EVAL_TIME)) { throw InternalErrorCode.TIMEOUT; }
    }

    // operand access ---------------------------------------------------------

    /**
     * Returns the number of existing operands passed to the operator or
     * function.
     *
     * @returns
     *  The number of existing operands.
     */
    getOperandCount(): number {
        return this.#operandResolver.size();
    }

    /**
     * Returns the specified operand passed to the operator or function.
     *
     * @param index
     *  The zero-based index of an operand.
     *
     * @param [typeKey="any"]
     *  The parameter type key used to decide how to convert the operand value.
     *  If omitted, or set to "any", the operand will be returned as-is (as
     *  instance of the class `Operand`).
     *
     * @returns
     *  The specified operand, if available, according to the passed type key.
     *
     * @throws
     *  The error code `#N/A`, if the operand does not exist; or any other
     *  error code occurred during conversion of the operand value.
     */
    getOperand(index: number): Operand;
    getOperand<PTK extends ParamTypeKey>(index: number, typeKey: PTK): ParamType[PTK];
    // implementation
    getOperand(index: number, typeKey?: ParamTypeKey): OperandAnyType | Operand {
        const operand = this.#operandResolver.resolve(index);
        if (!operand) { throw ErrorCode.NA; }
        return typeKey ? this.convertOperandToValue(operand, typeKey) : operand;
    }

    /**
     * Returns the raw value of the specified operand passed to the operator or
     * function.
     *
     * @param index
     *  The zero-based index of an operand.
     *
     * @returns
     *  The raw value of the specified operand, if available, including literal
     *  error codes.
     *
     * @throws
     *  The error code `#N/A`, if the operand does not exist.
     */
    getOperandValue(index: number): OperandRawType {
        return this.getOperand(index).getRawValue();
    }

    /**
     * Returns whether the specified operand is missing (the optional
     * parameters at the end of a function; e.g. the third parameter in the
     * function call `SUM(1,2)` is missing).
     *
     * @param index
     *  The zero-based index of an operand.
     *
     * @returns
     *  Whether the specified operand is missing. Returns `false` for existing
     *  but empty operands, e.g. in the formula `=SUM(1,,2)`.
     */
    isMissingOperand(index: number): boolean {
        return index >= this.#operandResolver.size();
    }

    /**
     * Returns whether the specified operand exists, but is empty (e.g. the
     * second operand in the function call `SUM(1,,2)` is empty).
     *
     * @param index
     *  The zero-based index of an operand.
     *
     * @returns
     *  Whether the specified operand exists but is empty. Returns `false` for
     *  missing operands (optional parameters at the end of the function).
     *  Returns `false` for references to empty cells too (the operand is a
     *  reference, and not empty).
     */
    isEmptyOperand(index: number): boolean {
        const operand = this.#operandResolver.resolve(index);
        return !!operand?.isEmpty();
    }

    /**
     * Returns whether the specified operand is missing (the optional
     * parameters at the end of a function; e.g. the third parameter in the
     * function call `SUM(1,2)` is missing), or empty (e.g. the second operand
     * in the function call `SUM(1,,2)` is empty).
     *
     * @param index
     *  The zero-based index of an operand.
     *
     * @returns
     *  Whether the specified operand is missing or empty.
     */
    isMissingOrEmptyOperand(index: number): boolean {
        const operand = this.#operandResolver.resolve(index);
        return operand?.isEmpty() ?? true;
    }

    /**
     * Returns the specified operands passed to the operator or function as an
     * array.
     *
     * @param first
     *  The zero-based index of the first operand to be returned.
     *
     * @param [length]
     *  The number of operands to be returned. Must be a positive integer. If
     *  this value would exceed the number of available operands, the error
     *  code `#VALUE!` will be thrown. If omitted, all remaining existing
     *  operands will be returned.
     *
     * @returns
     *  The specified operand and its successors, if existing. This array will
     *  never be empty.
     *
     * @throws
     *  The error code `#VALUE!`, if the specified operands do not exist.
     */
    getOperands(first: number, length?: number): readonly Operand[] {
        const resolver = this.#operandResolver;
        let count = resolver.size() - first;
        if (length) { count = Math.min(count, length); }
        if (count < 1) { throw ErrorCode.VALUE; }
        return ary.generate(count, index => resolver.resolve(first + index)!);
    }

    /**
     * Visits the existing operands of the processed operator or function.
     *
     * @param first
     *  Specifies the zero-based index of the first operand to be visited.
     *
     * @param callback
     *  The callback function that will be invoked for each operand.
     *
     * @throws
     *  The error code `#VALUE!`, if the specified operand does not exist.
     */
    iterateOperands(first: number, callback: (this: this, operand: Operand, index: number) => void): void {
        this.getOperands(first).forEach((operand, index) => callback.call(this, operand, first + index));
    }

    /**
     * Creates a new instance of the class `Operand` that will be bound to this
     * formula context.
     *
     * @param value
     *  The value to be stored as operand.
     *
     * @param [options]
     *  Optional parameters for the `Operand` constructor.
     *
     * @returns
     *  A new operand with the passed value and options.
     */
    createOperand(value: OperandAnyType | Operand, options?: OperandConfig): Operand {
        return new Operand(this, value, options);
    }

    // document access --------------------------------------------------------

    /**
     * Returns whether the passed sheet index is equal to the reference sheet
     * the interpreted formula is located in.
     *
     * @param sheet
     *  A sheet index to be tested against the reference sheet.
     *
     * @returns
     *  Whether the passed sheet index is equal to the reference sheet.
     */
    isRefSheet(sheet: number): boolean {
        const refSheet = this.#refSheet;
        return (refSheet !== null) && (refSheet >= 0) && (refSheet === sheet);
    }

    /**
     * Returns the index of the reference sheet the interpreted formula is
     * located in.
     *
     * @returns
     *  The zero-based index of the reference sheet.
     *
     * @throws
     *  The error code `#REF!`, if the formula is being interpreted without a
     *  reference sheet.
     */
    getRefSheet(): number {
        return (this.#refSheet === null) ? ErrorCode.REF.throw() : this.#refSheet;
    }

    /**
     * Returns the model of the reference sheet the interpreted formula is
     * located in.
     *
     * @returns
     *  The model instance of the reference sheet.
     *
     * @throws
     *  The error code `#REF!`, if the formula is being interpreted without a
     *  reference sheet.
     */
    getRefSheetModel(): SheetModel {
        return this.getSheetModel(this.getRefSheet());
    }

    /**
     * Returns the address of the source reference cell the interpreted formula
     * is located in. In a matrix formula, this is the address of its anchor
     * cell.
     *
     * @returns
     *  A clone of the address of the source reference cell.
     */
    getRefAddress(): Address {
        return this.#refAddress.clone();
    }

    /**
     * Returns whether the passed cell address is equal to the address of the
     * target reference cell the interpreted formula is located in.
     *
     * @param address
     *  A cell address to be tested against the target reference address.
     *
     * @returns
     *  Whether the passed cell address is equal to the address of the target
     *  reference cell.
     */
    isTargetAddress(address: Address): boolean {
        return this.#targetAddress.equals(address);
    }

    /**
     * Returns the address of the target reference cell the interpreted formula
     * is located in. May differ from the source reference address in matrix
     * formulas while calculating the single elements of a result matrix.
     *
     * @returns
     *  A clone of the address of the target reference cell.
     */
    getTargetAddress(): Address {
        return this.#targetAddress.clone();
    }

    /**
     * Returns the address of the cell in the passed cell range, according to
     * the current element position in matrix context.
     *
     * @param range
     *  The address of a cell range.
     *
     * @returns
     *  The address of the cell in the passed range, according to the current
     *  element position in matrix context.
     *
     * @throws
     *  The error code `#N/A`, if the current element position exceeds the size
     *  of the passed cell range address.
     */
    getMatrixAddress(range: Range): Address {
        return getMatrixAddress(range, this.#matRow, this.#matCol);
    }

    /**
     * Returns the model of the specified sheet.
     *
     * @returns
     *  The model instance of the specified sheet.
     *
     * @throws
     *  The error code `#REF!`, if the passed sheet index is invalid.
     */
    getSheetModel(sheet: number): SheetModel {
        return this.docModel.getSheetModel(sheet) ?? ErrorCode.REF.throw();
    }

    /**
     * Returns the current value (or formula result) of the specified cell.
     *
     * @param sheetModel
     *  The model of the sheet.
     *
     * @param address
     *  The address of the cell in the specified sheet.
     *
     * @returns
     *  The current value (or formula result) of the specified cell.
     */
    getCellValue(sheetModel: SheetModel, address: Address): ScalarType {

        // default case: resolve current cell value from cell collection
        if (!this.#recalcDirty) {
            return sheetModel.cellCollection.getValue(address);
        }

        // resolve cell value via dependency manager by recalculating dirty formulas recursively
        const valueDesc = this.docModel.dependencyManager.getCellValue(sheetModel, address);
        // adjust own evaluation time limit (do not count the evaluation time of the dependent formulas)
        this.#END_EVAL_TIME += valueDesc.time;
        // return the scalar value
        return valueDesc.value;
    }

    /**
     * Returns the current values (or formula results) of all cells in the
     * specified cell range as matrix.
     *
     * @param sheetModel
     *  The model of the sheet.
     *
     * @param range
     *  The address of the cell range in the specified sheet.
     *
     * @returns
     *  The matrix containing all values in the specified cell range.
     *
     * @throws
     *  - The error code `#REF!`, if the passed sheet index is invalid.
     *  - Internal `#UNSUPPORTED`, if the resulting matrix would be too large.
     */
    getCellMatrix(sheetModel: SheetModel, range: Range): ScalarMatrix {

        // dimension of the resulting matrix
        const matrixDim = Dimension.createFromRange(range);
        ensureMatrixDim(matrixDim);

        // build an empty matrix
        const matrix: ScalarMatrix = new Matrix(matrixDim.rows, matrixDim.cols, null);
        const row0 = range.a1.r;
        const col0 = range.a1.c;

        // fill existing cell values
        for (const { address } of sheetModel.cellCollection.cellEntries(range, { type: CellType.VALUE })) {
            matrix.set(address.r - row0, address.c - col0, this.getCellValue(sheetModel, address));
        }

        return matrix;
    }

    /**
     * Returns the localized formula expression of the specified cell.
     *
     * @param sheetModel
     *  The model of the sheet.
     *
     * @param address
     *  The address of the cell in the specified sheet.
     *
     * @returns
     *  A result object, if the cell contains a formula; otherwise `null`.
     *
     * @throws
     *  The error code `#REF!`, if the passed sheet index is invalid.
     */
    getCellFormula(sheetModel: SheetModel, address: Address): FormulaDescriptor | null {

        // get formula data from cell collection; return null if formula is not available
        const tokenDesc = sheetModel.cellCollection.getTokenArray(address, { fullMatrix: true, grammarType: "ui" });
        if (!tokenDesc) { return null; }

        // return the formula expression, add the matrix settings
        return {
            formula: tokenDesc.formula!,
            matrixRange: tokenDesc.matrixRange,
            dynamicMatrix: tokenDesc.dynamicMatrix
        };
    }

    // iterator helpers -------------------------------------------------------

    /**
     * Creates an iterator that visits the values provided by the passed value
     * iterators simultaneously.
     *
     * @param iterators
     *  An array of value iterators as created by the methods of this class.
     *
     * @returns
     *  The new iterator that visits all scalar values contained in the passed
     *  iterators. The iterator will stop at all offsets with *at least one*
     *  available non-blank value. The values covered by the other iterators
     *  may represent blank cells.
     */
    createParallelIterator<T>(iterators: Array<OperandIterator<T>>, options: ParallelOperandIteratorOptions & { complete: true }): ParallelOperandIterator<T>;
    createParallelIterator<T>(iterators: Array<OperandIterator<T>>, options?: ParallelOperandIteratorOptions): ParallelOperandIterator<T | null>;
    // implementation
    *createParallelIterator<T>(iterators: Array<OperandIterator<T>>, options?: ParallelOperandIteratorOptions): ParallelOperandIterator<T | null> {

        // create a parallel iterator for all source operand iterators
        const iterator = iterateParallelSynchronized(iterators, opState => opState.offset);

        // create the expected result object for the iterator
        let index = 0;
        for (const { values: opStates, offset, complete, first } of iterator) {

            // skip incomplete iterator steps if specified in passed options
            if (!complete && options?.complete) { continue; }

            // create the result value with an array of scalars, and the column/row indexes (copy from first valid source iterator)
            const value = opStates.map(opState => opState?.value ?? null);
            yield { value, offset, col: first.col, row: first.row, index, format: null };

            index += 1;
        }
    }

    // scalar iterators -------------------------------------------------------

    /**
     * Creates an iterator that visits all scalar values contained in the
     * passed operand, matrix, or reference.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new scalar iterator. The method `next()` will throw any error code
     *  contained in the passed operand (unless option `acceptErrors` is set).
     */
    createScalarIterator(source: OperandIteratorSource, options?: ScalarIteratorOptions): OperandIterator<ScalarType> {
        return this.#createOperandIterator(source, fun.identity, options);
    }

    /**
     * Visits all scalar values contained in the passed operand, by creating a
     * scalar iterator, and invoking the callback function for all values it
     * provides.
     *
     * @param source
     *  The operand to be visited.
     *
     * @param fn
     *  The callback function invoked for each scalar value contained in the
     *  operand. Will be called in the context of this instance.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The total count of the visited scalar values.
     *
     * @throws
     *  Any error code contained in the passed operand (unless option
     *  `acceptErrors` is set).
     */
    forEachScalar(source: OperandIteratorSource, fn: OperandIteratorFn<ScalarType>, options?: ScalarIteratorOptions): number {
        const iterator = this.createScalarIterator(source, options);
        return this.#forEachValue(iterator, fn);
    }

    /**
     * Returns all scalar values contained in the passed operand as a plain
     * array.
     *
     * @param source
     *  The operand whose contents will be returned as array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The scalar values contained in the passed operand.
     *
     * @throws
     *  Any error code contained in the passed operand (unless option
     *  `acceptErrors` is set).
     */
    getScalarsAsArray(source: OperandIteratorSource, options?: ScalarIteratorOptions): ScalarType[] {
        return this.#iteratorToArray(this.createScalarIterator(source, options));
    }

    // number iterators -------------------------------------------------------

    /**
     * Creates an iterator that visits all floating-point numbers contained in
     * the passed operand. Provides the value of a scalar operand, all elements
     * of a matrix operand, and the cell contents referred by a reference
     * operand.
     *
     * In difference to simple value parameters where strings and boolean
     * values contained in matrixes and cell ranges will always be converted to
     * numbers if possible, the iterator will implement different conversion
     * strategies as required by the respective functions.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new number iterator. The method `next()` will throw any error code
     *  contained in the passed operand, or that will be thrown while
     *  converting the operand values to numbers.
     */
    createNumberIterator(source: OperandIteratorSource, options: NumberIteratorOptions & { dates: true }): OperandIterator<Date>;
    createNumberIterator(source: OperandIteratorSource, options?: NumberIteratorOptions): OperandIterator<number>;
    // implementation
    createNumberIterator(source: OperandIteratorSource, options?: NumberIteratorOptions): OperandIterator<number | Date> {

        // whether to skip error codes silently
        const skipErrors = options?.skipErrors;
        // whether to round numbers down to integers
        const floorMode = options?.floor;
        // date mode (convert all numbers to date values)
        const dateMode = options?.dates;

        // pick the correct converter callback function
        const converter1 = createConverterFn<number>(source, options, convertMode => {
            switch (convertMode) {

                // convert value to number (may still fail for strings)
                case "convert":
                    return value => this.convertToNumber(value, floorMode);

                // convert value to number (may still fail for strings)
                case "skip":
                    return value => !is.number(value) ? null : floorMode ? Math.floor(value) : value;

                // convert value to number (may still fail for strings)
                case "exact":
                    return value => !is.number(value) ? ErrorCode.VALUE.throw() : floorMode ? Math.floor(value) : value;

                // convert value to number (may still fail for strings)
                case "rconvert":
                    return value => is.boolean(value) ? ErrorCode.VALUE.throw() : this.convertToNumber(value, floorMode);

                // replace all strings with zero
                case "zero":
                    return value => is.string(value) ? 0 : this.convertToNumber(value, floorMode);
            }

            return null;
        });

        // convert numbers to dates if specified
        const converter2: ConverterFn<number | Date | null> = dateMode ? (value => this.convertToDate(converter1(value))) : converter1;

        // silently skip error codes if specified
        const converter3: ConverterFn<number | Date | null> = skipErrors ? (value => isErrorCode(value) ? null : converter2(value)) : converter2;

        // create an iterator for all values in the operand, try to convert to numbers
        const iterator = this.#createOperandIterator(source, converter3, { ...options, blankCells: false, acceptErrors: skipErrors });

        // create the resulting iterator that filters all `null` values and adds the sequence index
        return filterNullOperands(iterator);
    }

    /**
     * Visits all floating-point numbers contained in the passed operand, by
     * creating a number iterator, and invoking the callback function for all
     * numbers it provides.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param fn
     *  The callback function invoked for each floating-point number contained
     *  in the operand. Will be called in the context of this instance.
     *
     * @param [options]
     *  Optional parameters passed to the number iterator used internally.
     *
     * @returns
     *  The total count of the visited numbers.
     *
     * @throws
     *  Any error code contained in the passed operand, or thrown while
     *  converting values to numbers.
     */
    forEachNumber(source: OperandIteratorSource, fn: OperandIteratorFn<Date>, options: NumberIteratorOptions & { dates: true }): number;
    forEachNumber(source: OperandIteratorSource, fn: OperandIteratorFn<number>, options?: NumberIteratorOptions): number;
    // implementation
    forEachNumber(operand: OperandIteratorSource, fn: OperandIteratorFn<any>, options?: NumberIteratorOptions): number {
        const iterator = this.createNumberIterator(operand, options);
        return this.#forEachValue(iterator, fn);
    }

    /**
     * Returns all floating-point numbers contained in the passed operand as a
     * plain array.
     *
     * @param source
     *  The operand whose contents will be returned as array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The floating-point numbers contained in the passed operand.
     *
     * @throws
     *  Any error code contained in the passed operand, or thrown while
     *  converting values to numbers.
     */
    getNumbersAsArray(source: OperandIteratorSource, options: NumberIteratorOptions & { dates: true }): Date[];
    getNumbersAsArray(operand: OperandIteratorSource, options?: NumberIteratorOptions): number[];
    // implementation
    getNumbersAsArray(source: OperandIteratorSource, options?: NumberIteratorOptions): number[] | Date[] {
        return this.#iteratorToArray(this.createNumberIterator(source, options));
    }

    /**
     * Reduces all numbers (and other values convertible to numbers) of the
     * passed operands.
     *
     * @param sources
     *  The operands containing numbers to be aggregated.
     *
     * @param initial
     *  The initial intermediate result.
     *
     * @param aggregate
     *  The aggregation callback function.
     *
     * @param finalize
     *  The finalizer callback function.
     *
     * @param [options]
     *  Optional parameters passed to the number iterator used internally. The
     *  behaviour of the option `collectFormats` changes as following: If set
     *  to `true`, the number formats of all visited numbers will be
     *  aggregated. If a resuting number format could be determined, the return
     *  value of this method will be an instance of `Operand` that the final
     *  parsed number format.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateNumbers<T, I>(
        sources: OperandAggregatorSource,
        initial: I,
        aggregate: AggregateAggregatorFn<I, number>,
        finalize: AggregateFinalizerFn<T, I>,
        options?: CommonNumberIteratorOptions
    ): T {
        return this.#aggregateValues(sources, initial, this.forEachNumber, aggregate, finalize, options);
    }

    /**
     * Reduces all numbers (and other values convertible to numbers) of the
     * passed operands. All numbers will be collected in a plain JavaScript
     * array first, and will be passed at once to the provided callback.
     *
     * @param sources
     *  The operands containing numbers to be aggregated.
     *
     * @param finalize
     *  The finalizer callback function.
     *
     * @param [options]
     *  Parameters passed to the number iterator used internally.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateNumbersAsArray<T>(
        sources: OperandAggregatorSource,
        finalize: AggregateNumbersAsArrayFinalizerFn<T>,
        options?: CommonNumberIteratorOptions
    ): T {

        // all numbers collected in an array
        const numbers: number[] = [];

        // stores the new number in the array, and updates the intermediate sum
        const aggregate: AggregateAggregatorFn<number, number> = (sum, n) => {
            numbers.push(n);
            return sum + n;
        };

        // invokes the passed finalizer callback with the collected numbers and sum
        const finalizeImpl: AggregateFinalizerFn<T, number> =
            (sum, count) => finalize.call(this, numbers, sum, count);

        // resolve the final result, throw error codes
        return this.aggregateNumbers(sources, 0, aggregate, finalizeImpl, options);
    }

    /**
     * Reduces all numbers (and other values convertible to numbers) of the
     * passed operands. The numbers of the passed operands will be visited
     * simultaneously, according to their relative position in the operand.
     *
     * @param sources
     *  The operands containing numbers to be aggregated. The operands array
     *  MUST NOT be empty. If the operand contains a complex reference (i.e.,
     *  multiple ranges, or a range referring to multiple sheets), the error
     *  code `#VALUE!` will be thrown.
     *
     * @param initial
     *  The initial intermediate result.
     *
     * @param aggregate
     *  The aggregation callback function.
     *
     * @param finalize
     *  The finalizer callback function.
     *
     * @param [options]
     *  Optional parameters passed to the number iterators used internally.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateNumbersParallel<T, I>(
        sources: OperandAggregatorSource,
        initial: I,
        aggregate: AggregateParallelAggregatorFn<I, number>,
        finalize: AggregateParallelFinalizerFn<T, I>,
        options?: NumberAggregatorOptions
    ): T {

        // convert parameter to plain array of `Operand` instances (needed to get the operand sizes)
        const operands = sources.map(source => (source instanceof Operand) ? source : new Operand(this, source));

        // the size checking mode
        const checkSize = options?.checkSize ?? "exact";

        // in parallel mode, all operands must have equal size
        if ((operands.length > 1) && (checkSize !== "none")) {
            const exact = checkSize === "exact";
            const dim0 = operands[0].getDimension({ errorCode: ErrorCode.VALUE }); // #VALUE! thrown for complex references
            const size = dim0.size();
            operands.forEach((operand, index) => {
                if (index === 0) { return; }
                const dim = operand.getDimension({ errorCode: ErrorCode.VALUE }); // #VALUE! thrown for complex references
                if (exact ? !dim0.equals(dim) : (size !== dim.size())) {
                    throw options?.sizeMismatchError ?? ErrorCode.VALUE;
                }
            });
        }

        // create the number iterators for every operand
        const iterators = operands.map(operand => this.createNumberIterator(operand, options));

        // the intermediate result
        let intermediate = initial;
        // the count of all visited number tuples in all operands
        let count = 0;
        // create the parallel iterator that visits all tuples consisting of numbers only
        const iterator = this.createParallelIterator(iterators, { complete: true });

        // visit all number tuples and invoke the aggregation callback function
        for (const { value } of iterator) {
            intermediate = aggregate.call(this, intermediate, value, count);
            count += 1;
        }

        // resolve the final result, throw error codes
        return throwErrorCode(finalize.call(this, intermediate, count));
    }

    /**
     * Reduces all numbers (and other values convertible to numbers) of the
     * passed operands. The numbers of the passed operands will be visited
     * simultaneously, according to their relative position in the operand, and
     * they will be collected in plain JavaScript arrays first, and will be
     * passed at once to the provided callback.
     *
     * @param sources
     *  The operands containing numbers to be aggregated. The operands array
     *  MUST NOT be empty. If the operand contains a complex reference (i.e.,
     *  multiple ranges, or a range referring to multiple sheets), the error
     *  code `#VALUE!` will be thrown.
     *
     * @param finalize
     *  A callback function invoked at the end to convert the collected numbers
     *  to the actual function result. Receives the following parameters:
     *  (1) {Array<Array<Numbers>>} tuples
     *      The collected number tuples, as two-dimensional JavaScript array.
     *      Each array element is a tuple with as many numbers as operands have
     *      been passed to this method. The first array element of the inner
     *      arrays corresponds to the first operand, and so on.
     *  (2) {Array<Number>} sums
     *      The sums of all collected numbers, for convenience. The first array
     *      element is the sum of all numbers of the first operand, and so on.
     *  (3) {Number} count
     *      The count of all collected numbers per operand (the length of the
     *      'numbers' array), for convenience.
     *  Must return the final result of the aggregation, or an error code
     *  object. The type of the final result does not matter, but must match
     *  the type key of the implemented function. Alternatively, the finalizer
     *  may throw an error code. Will be called in the context of this
     *  instance.
     *
     * @param [options]
     *  Optional parameters passed to the number iterators used internally.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateNumbersParallelAsArrays<T>(
        sources: OperandAggregatorSource,
        finalize: AggregateNumbersParallelAsArraysFinalizerFn<T>,
        options?: NumberAggregatorOptions
    ): T {

        // numbers of all operands, collected in an array of arrays
        const tuples: number[][] = [];
        // the initial value passed to the aggregator (the sums of all operands)
        const initial = ary.fill(sources.length, 0);

        // stores the new numbers in the arrays, and updates the sums
        const aggregate: AggregateParallelAggregatorFn<number[], number> = (sums, tuple) => {
            tuples.push(tuple);
            tuple.forEach((n, i) => (sums[i] += n));
            return sums;
        };

        // invokes the passed finalizer callback with the collected numbers
        const finalizeImpl: AggregateParallelFinalizerFn<T, number[]> =
            (sums, count) => finalize.call(this, tuples, sums, count);

        // resolve the final result, throw error codes
        return this.aggregateNumbersParallel(sources, initial, aggregate, finalizeImpl, options);
    }

    // string iterators -------------------------------------------------------

    /**
     * Creates an iterator that visits all strings contained in the passed
     * operand. Provides the value of a scalar operand, all elements of a
     * matrix operand, and the cell contents referred by a reference operand.
     *
     * In difference to simple value parameters where numbers and boolean
     * values contained in matrixes and cell ranges will always be converted to
     * strings if possible, this method implements different conversion
     * strategies as required by the respective functions.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new string iterator. The method `next()` will throw any error code
     *  contained in the passed operand, or that will be thrown while
     *  converting the operand values to strings.
     */
    createStringIterator(
        source: OperandIteratorSource,
        options?: ConvertIteratorOptions
    ): OperandIterator<string> {

        // pick the correct converter callback function
        const converter = createConverterFn<string>(source, options, convertMode => {
            switch (convertMode) {

                // convert value to string
                case "convert":
                    return value => this.convertToString(value);

                // skip numbers and boolean values
                case "skip":
                    return value => is.string(value) ? value : null;

                // throw #VALUE! error code on numbers, booleans, and empty function parameters
                case "exact":
                    return value => is.string(value) ? value : ErrorCode.VALUE.throw();

                default:
                    return null;
            }
        });

        // create an iterator for all values in the operand, try to convert to strings
        const iterator = this.#createOperandIterator(source, converter, { ...options, blankCells: false, acceptErrors: false });

        // create the resulting iterator that filters all null values and adds the sequence index
        return filterNullOperands(iterator);
    }

    /**
     * Visits all strings contained in the passed operand, by creating a string
     * iterator, and invoking the callback function for all strings it
     * provides.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param fn
     *  The callback function invoked for each string contained in the operand.
     *  Will be called in the context of this instance.
     *
     * @param [options]
     *  Parameters passed to the string iterator used internally.
     *
     * @returns
     *  The total count of the visited strings.
     *
     * @throws
     *  Any error code contained in the passed operand, or thrown while
     *  converting values to strings.
     */
    iterateStrings(
        source: OperandIteratorSource,
        fn: OperandIteratorFn<string>,
        options?: ConvertIteratorOptions
    ): number {
        const iterator = this.createStringIterator(source, options);
        return this.#forEachValue(iterator, fn);
    }

    /**
     * Reduces all strings (and other values convertible to strings) of the
     * passed operands.
     *
     * @param sources
     *  The operands containing strings to be aggregated.
     *
     * @param initial
     *  The initial intermediate result.
     *
     * @param aggregate
     *  The aggregation callback function.
     *
     * @param finalize
     *  The finalizer callback function.
     *
     * @param [options]
     *  Parameters passed to the string iterator used internally. The behaviour
     *  of the option `collectFormats` changes as following: If set to `true`,
     *  the number formats of all visited strings will be aggregated. If a
     *  resuting number format could be determined, the return value of this
     *  method will be an instance of `Operand` that contains the final parsed
     *  number format.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateStrings<T, I>(
        sources: OperandAggregatorSource,
        initial: I,
        aggregate: AggregateAggregatorFn<I, string>,
        finalize: AggregateFinalizerFn<T, I>,
        options?: ConvertIteratorOptions
    ): T {
        return this.#aggregateValues(sources, initial, this.iterateStrings, aggregate, finalize, options);
    }

    // boolean iterators ------------------------------------------------------

    /**
     * Creates an iterator that visits all boolean values contained in the
     * passed operand. Provides the value of a scalar operand, all elements of
     * a matrix operand, and the cell contents referred by a reference operand.
     *
     * In difference to simple value parameters where numbers and strings
     * contained in matrixes and cell ranges will always be converted to
     * boolean values if possible, this method implements different conversion
     * strategies as required by the respective functions.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new boolean iterator. The method `next()` will throw any error code
     *  contained in the passed operand, or that will be thrown while
     *  converting the operand values to booleans.
     */
    createBooleanIterator(
        source: OperandIteratorSource,
        options?: ConvertIteratorOptions
    ): OperandIterator<boolean> {

        // pick the correct converter callback function
        const converter = createConverterFn<boolean>(source, options, convertMode => {
            switch (convertMode) {

                // convert value to boolean (may still fail for strings)
                case "convert":
                    return value => this.convertToBoolean(value);

                // skip strings (but not numbers)
                case "skip":
                    return value => is.string(value) ? null : this.convertToBoolean(value);

                // throw #VALUE! error code on numbers, strings, and empty function parameters
                case "exact":
                    return value => is.boolean(value) ? value : ErrorCode.VALUE.throw();

                default:
                    return null;
            }
        });

        // create an iterator for all values in the operand, try to convert to booleans
        const iterator = this.#createOperandIterator(source, converter, { ...options, blankCells: false, acceptErrors: false });

        // create the resulting iterator that filters all null values and adds the sequence index
        return filterNullOperands(iterator);
    }

    /**
     * Visits all boolean values contained in the passed operand, by creating a
     * boolean iterator, and invoking the callback function for all boolean
     * values it provides.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param fn
     *  The callback function invoked for each boolean value contained in the
     *  operand. Will be called in the context of this instance.
     *
     * @param [options]
     *  Parameters passed to the boolean value iterator used internally.
     *
     * @returns
     *  The total count of the visited boolean values.
     *
     * @throws
     *  Any error code contained in the passed operand, or thrown while
     *  converting values to booleans.
     */
    iterateBooleans(
        source: OperandIteratorSource,
        fn: OperandIteratorFn<boolean>,
        options?: ConvertIteratorOptions
    ): number {
        const iterator = this.createBooleanIterator(source, options);
        return this.#forEachValue(iterator, fn);
    }

    /**
     * Reduces all boolean values (and other values convertible to boolean
     * values) of the passed operands.
     *
     * @param sources
     *  The operands containing the boolean values to be aggregated.
     *
     * @param initial
     *  The initial intermediate result.
     *
     * @param aggregate
     *  The aggregation callback function.
     *
     * @param finalize
     *  The finalizer callback function.
     *
     * @param [options]
     *  Parameters passed to the boolean value iterator used internally. The
     *  behaviour of the option `collectFormats` changes as following: If set
     *  to `true`, the number formats of all visited boolean values will be
     *  aggregated. If a resuting number format could be determined, the return
     *  value of this method will be an instance of `Operand` that contains the
     *  final parsed number format.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateBooleans<T, I>(
        sources: OperandAggregatorSource,
        initial: I,
        aggregate: AggregateAggregatorFn<I, boolean>,
        finalize: AggregateFinalizerFn<T, I>,
        options?: ConvertIteratorOptions
    ): T {
        return this.#aggregateValues(sources, initial, this.iterateBooleans, aggregate, finalize, options);
    }

    // matrix helpers ---------------------------------------------------------

    /**
     * Returns whether the current context type is matrix context, and the
     * dimension of the result matrix is known (the method `getMatrixDim` will
     * return a dimension).
     *
     * @returns
     *  Whether the current context type is matrix context, and the dimension
     *  of the result matrix is known.
     */
    isMatrixMode(): boolean {
        return (this.#contextType === "mat") && !!this.#matDim;
    }

    /**
     * Returns the precalculated dimension of the matrix result calculated for
     * the current operator/function in matrix context.
     *
     * @returns
     *  The precalculated dimension of the matrix result; or `null`, if no
     *  matrix result will be calculated.
     */
    getMatrixDim(): Dimension | null {
        return this.#matDim;
    }

    /**
     * Builds a matrix result with the result values of the specified element
     * aggregation callback function.
     *
     * @param matrixDim
     *  The dimension of the matrix result.
     *
     * @param aggregate
     *  The aggregation callback function. Will be invoked for every element of
     *  the result matrix. Must return the scalar value of the matrix element,
     *  or may throw an error code.
     *
     * @returns
     *  The result matrix built from the return values of the aggregation
     *  callback function.
     *
     * @throws
     *  An error code if calculating the result matrix has failed.
     */
    aggregateMatrix(matrixDim: Dimension, aggregate: MatrixGeneratorFn<ScalarType>): ScalarMatrix {

        // build the result matrix (in matrix context)
        try {
            this.pushOperandResolver(this.#operandResolver, "mat", matrixDim);
            return new Matrix(matrixDim.rows, matrixDim.cols, (matRow, matCol) => {
                // check maximum evaluation time for each matrix element to prevent freezing browser
                this.checkEvalTime();
                this.setMatrixPosition(matRow, matCol);
                try {
                    return aggregate.call(this, matRow, matCol);
                } catch (error) {
                    if (isErrorCode(error)) { return error; }
                    throw error; // rethrow internal errors
                }
            });
        } finally {
            this.popOperandResolver();
        }
    }

    // data filters -----------------------------------------------------------

    /**
     * Returns the number of blank cells in the passed cell ranges.
     *
     * @param ranges
     *  The addresses of the cell ranges, or a single cell range address, in
     *  which to count the blank cells.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The number of blank cells in the passed cell ranges.
     */
    countBlankCells(ranges: OperandRefType, options?: CountBlankCellsOptions): number {

        // the number of non-blank cells
        let scalarCount = 0;

        // the callback function according to the empty-string mode (created outside
        // the loop to prevent to check the "emptyStr" flag in each invocation)
        const counterFn: OperandIteratorFn<ScalarType> = options?.emptyStr ?
            value => (value !== "") && (scalarCount += 1) :
            () => (scalarCount += 1);

        // count all non-blank cells in the reference
        this.forEachScalar(ranges, counterFn, {
            acceptErrors: true, // count all error codes
            multiRange: true, // accept multi-range references
            multiSheet: true  // accept multi-sheet references
        });

        // return number of remaining blank cells
        return ranges.cells() - scalarCount;
    }

    /**
     * Converts the passed simple filter pattern to a case-insensitive regular
     * expression object.
     *
     * @param pattern
     *  The simple filter pattern. An asterisk character in the string matches
     *  any sequence of characters (also the empty sequence), and a question
     *  mark matches an arbitrary single character. If an asterisk or question
     *  mark is preceded by a tilde character, they lose their special meaning
     *  and will be matched literally.
     *
     * @param [complete=false]
     *  If set to `true`, the regular expression created by this method will
     *  only match complete strings. By default, any substring of a string will
     *  be matched.
     *
     * @returns
     *  A case-insensitive regular expression that represents the passed simple
     *  filter pattern.
     */
    convertPatternToRegExp(pattern: string, complete?: boolean): RegExp {

        // convert passed search text to regular expression pattern matching the text literally
        pattern = re.escape(pattern)
            // replace all occurrences of asterisk (without leading tilde) to '.*' RE pattern (e.g. 'a\*b' => 'a.*b')
            .replace(/(^|[^~])\\\*/g, "$1.*")
            // replace all occurrences of question mark (without leading tilde) to '.' RE pattern (e.g. 'a\?b' => 'a.b')
            .replace(/(^|[^~])\\\?/g, "$1.")
            // remove tilde characters before (already escaped) asterisks and question marks (e.g. 'a~\*b' => 'a\*b')
            .replace(/~(\\[*?])/g, "$1");

        // optionally, let the regular expression only match complete strings
        if (complete) { pattern = `^${pattern}$`; }

        // create the case-insensitive regular expression
        return new RegExp(pattern, "i");
    }

    /**
     * Creates a scalar matcher predicate function for the specified criterion.
     *
     * @param criterion
     *  A scalar value representing the criterion. Numbers, boolean values, and
     *  error codes will match the corresponding scalar values (strict type
     *  checking). The value `null` represents a reference to an empty cell,
     *  and will match the number zero only (but not the empty string, and not
     *  empty cells). Strings may specify filter patterns with asterisk
     *  characters or question marks.
     *
     * @returns
     *  A predicate function that matches the passed criterion.
     */
    createScalarMatcher(criterion: ScalarType): ScalarFilterMatcher {

        // create a string matcher with pattern matching support
        if (is.string(criterion) && /[*?]/.test(criterion)) {
            const regExp = this.convertPatternToRegExp(criterion, true);
            return value => is.string(value) && regExp.test(value);
        }

        // test scalars for equality; empty values (`null`) are treated like zeros
        return this.equalScalars.bind(this, criterion ?? 0);
    }

    /**
     * Creates a filter matcher predicate function for the specified filter
     * criterion.
     *
     * @param criterion
     *  A scalar value representing the filter criterion. Numbers, boolean
     *  values, and error codes will match the corresponding values, and will
     *  match strings that can be converted to these values. The value `null`
     *  represents a reference to an empty cell, and will match the number zero
     *  only (but not the empty string, and not empty cells). Strings may
     *  contain leading comparison operators, or may specify filter patterns
     *  with asterisk characters or question marks. Otherwise, string criteria
     *  will be converted to numbers, boolean values, or error codes, if
     *  possible, and will match the corresponding values.
     *
     * @returns
     *  A predicate function that matches the passed criterion.
     */
    createFilterMatcher(criterion: ScalarType): ScalarFilterMatcher {

        // the number formatter of the document
        const { numberFormatter } = this;
        // the filter operator (empty string for criterion without operator)
        let op: Opt<string>;
        // the filter operand to be compared with the values passed to the matcher
        let comp: ScalarType = null;
        // whether to test for equality (needs some special handling)
        let equality = false;

        // parse the filter operator and the filter operand from a string criterion
        if (is.string(criterion)) {
            const matches = /^(<=|>=|<>|<|>|=)?(.*)$/.exec(criterion)!;
            op = matches[1] || "";
            // leading apostrophe in operand counts as regular character
            comp = numberFormatter.parseScalarValue(matches[2], { keepApos: true }).value;
        } else {
            // all other data types cause filtering by equality
            op = "";
            comp = criterion;
        }

        // testing for equality converts strings in source data to other data types
        equality = (op === "") || (op === "=");

        // empty filter operand (empty parameter, or reference to empty cell) matches zero
        // numbers (and strings that can be parsed to zero number) in source data, but not
        // empty strings, FALSE, or empty cells
        if (comp === null) {
            return value => (is.string(value) ? numberFormatter.parseScalarValue(value).value : value) === 0;
        }

        // special handling for the empty string as filter operand
        if (comp === "") {
            switch (op) {
                // empty string without filter operator matches empty strings AND empty cells
                case "": return emptyMatcher;
                // single equality operator without operand matches blank cells, but not empty strings
                case "=": return blankMatcher;
                // single inequality operator without operand matches anything but blank cells
                case "<>": return scalarMatcher;
                // all other comparison operators without operand do not match anything
                default: return nothingMatcher;
            }
        }

        // equality or inequality with pattern matching
        if (is.string(comp) && /[*?]/.test(comp) && (equality || (op === "<>"))) {

            // the regular expression for the pattern (match complete strings only)
            const regExp = this.convertPatternToRegExp(comp, true);

            // create the pattern matcher predicate function (pattern filters work on strings only)
            return value => equality === (is.string(value) && regExp.test(value));
        }

        // strings are always matched case-insensitively
        if (is.string(comp)) { comp = comp.toUpperCase(); }

        // try to convert string values to numbers when testing equality with a number
        const convertToNumber = equality && is.number(comp);
        // the data type identifier of the filter operand
        const compType = getScalarType(comp);

        // create the comparison matcher predicate function (pattern filters work on strings only)
        return value => {

            // The equality operator converts strings in source data to numbers if possible. Strings will
            // not be converted to numbers if the comparison operand is not a number (too expensive); in
            // this case the comparison operand cannot be converted to a number (has been tried above),
            // thus a string value looking like a number will not be equal to the string comparison operand.
            // BUT: Strings will NOT be converted to booleans or error codes, and the inequality operator
            // does not convert anything at all.
            if (convertToNumber && is.string(value)) {
                const converted = numberFormatter.parseScalarValue(value).value;
                if (is.number(converted)) { value = converted; }
            }

            // compare the passed value with the filter operand (resulting NaN means incompatible types)
            const canCompare = (value !== null) && (compType === getScalarType(value));
            const result = canCompare ? compareScalars(value, comp) : CompareResult.NA;

            // return whether the value matches, according to the filter operand
            switch (op) {
                case "":
                case "=":  return result === CompareResult.EQUAL;
                case "<>": return result !== CompareResult.EQUAL; // incompatible types match the inequality operator
                case "<":  return result === CompareResult.LESS;
                case "<=": return result <= CompareResult.EQUAL;
                case ">":  return result === CompareResult.GREATER;
                case ">=": return result >= CompareResult.EQUAL;
                default:   throwEngineError("FormulaContext.comparisonMatcher(): invalid operator");
            }
        };
    }

    /**
     * Implementation helper for `COUNTIF` and `COUNTIFS`. Counts all entries
     * in the cell ranges that match the filter criteria.
     *
     * @returns
     *  The number of cells matched by the specified criteria.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    countFilteredCells(): number | NumberMatrix {

        // wrapper for the filter ranges, and matcher predicate functions
        const aggregator = new FilterAggregator(this, 0);

        // count all matching entries in the filter ranges
        return aggregator.countMatches();
    }

    /**
     * Reduces all numbers that match a specific filter criterion to the result
     * of a spreadsheet function.
     *
     * @param multi
     *  The type of the function to be implemented.
     *  - If set to `false`, the function uses a single filter criterion,
     *    passed as the leading parameters `filterRange` and `criterion` to the
     *    function, e.g. `SUMIF` or `AVERAGEIF`. The data range will be passed
     *    as optional third parameter to the function (if missing, the filter
     *    range will be used instead).
     *  - If set to `true`, the function takes a mandatory leading data range,
     *    followed by an arbitrary number of parameter pais representing the
     *    filtered ranges and filter criteria, e.g. `SUMIFS`, `AVERAGEIFS`,
     *    `MINIFS`, or `MAXIFS`.
     *
     * @param initial
     *  The initial intermediate result, passed to the first invocation of the
     *  aggregation callback function.
     *
     * @param aggregate
     *  The aggregation callback function.
     *
     * @param [finalize]
     *  The finalizer callback function.
     *
     * @returns
     *  The result of the spreadsheet function.
     *
     * @throws
     *  An error code if calculating the function result has failed.
     */
    aggregateFilteredCells(
        multi: boolean,
        initial: number,
        aggregate: FilterAggregatorFunc,
        finalize?: FilterFinalizerFunc
    ): number | NumberMatrix {

        // wrapper for the filter ranges, and matcher predicate functions
        const aggregator = new FilterAggregator(this, multi ? 1 : 0);
        // the first filter range (needed to compare range size with data range)
        const filterRange = aggregator.getFilterRange(0);

        // the data range
        const dataRange = fun.do(() => {

            // multi functions (SUMIFS etc.): data range is the mandatory first parameter
            if (multi) { return this.getOperand(0, "ref:val"); }

            // single mode (SUMIF etc.): ignore original size of the data range, set it to the size of the source range
            const range = this.isMissingOperand(2) ? null : this.getOperand(2, "ref:val");

            // no data range passed: aggregate filter range directly
            if (!range) { return filterRange.clone(); }

            // restrict data range to sheet limits
            range.a2.c = Math.min(range.a1.c + filterRange.cols() - 1, this.addressFactory.maxCol);
            range.a2.r = Math.min(range.a1.r + filterRange.rows() - 1, this.addressFactory.maxRow);

            // shorten the source range, if the data range has been shortened
            filterRange.a2.c = filterRange.a1.c + range.cols() - 1;
            filterRange.a2.r = filterRange.a1.r + range.rows() - 1;

            return range;
        });

        // the data range must have the same size as the filter ranges
        return aggregator.aggregateNumbers(dataRange, initial, aggregate, finalize);
    }

    // internal methods -------------------------------------------------------

    /**
     * Sets the specified element position inside a matrix formula.
     *
     * **ATTENTION**: This method is used by the formula interpreter and shall
     *  not be called from outside.
     *
     * @param matRow
     *  The row index in the matrix context.
     *
     * @param matCol
     *  The column index in the matrix context.
     */
    setMatrixPosition(matRow: number, matCol: number): void {
        this.#targetAddress.c += (matCol - this.#matCol);
        this.#targetAddress.r += (matRow - this.#matRow);
        this.#matRow = matRow;
        this.#matCol = matCol;
    }

    /**
     * Registers the operands of the next function or operator to be resolved.
     *
     * **ATTENTION**: This method will be invoked by the formula interpreter to
     *  be able to reuse this context instance for all functions and operators
     *  contained in a complete formula; and shall not be called from outside.
     *
     * @param resolver
     *  A resolver that provides access to the operands of the function or
     *  operator that will be processed next.
     *
     * @param contextType
     *  The context type used to evaluate an operator.
     */
    pushOperandResolver(resolver: OperandResolver, contextType: ContextType, matrixDim: Dimension | null): void {
        this.#resolverStack.push({ resolver: this.#operandResolver, contextType: this.#contextType, matDim: this.#matDim, matRow: this.#matRow, matCol: this.#matCol });
        this.#operandResolver = resolver;
        this.#contextType = contextType;
        this.#matDim = matrixDim;
        this.setMatrixPosition(0, 0);
    }

    /**
     * Removes the last operand resolver from the inetrnal stack that has been
     * registered with the method `pushOperandResolver()`.
     *
     * **ATTENTION**: This method is used by the formula interpreter and shall
     *  not be called from outside.
     */
    popOperandResolver(): void {
        const entry = this.#resolverStack.pop()!;
        this.#operandResolver = entry.resolver;
        this.#contextType = entry.contextType;
        this.#matDim = entry.matDim;
        this.setMatrixPosition(entry.matRow, entry.matCol);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the scalar value from the passed operand, according to the
     * current element position of a matrix formula.
     *
     * @param operand
     *  The operand to be converted to a scalar value.
     *
     * @returns
     *  The scalar value, if the operand can be resolved.
     *
     * @throws
     *  An internal error code carried by the operand.
     */
    #getScalar(operand: Operand): ScalarType {
        return (this.#contextType === "mat") ? operand.getScalar(this.#matRow, this.#matCol) : operand.getScalar(null, null);
    }

    /**
     * Throws the internal error code `#CIRCULAR`, if the passed cell range
     * address contains the reference cell of this formula context.
     *
     * @param range
     *  The cell range address to be checked.
     *
     * @returns
     *  The passed cell range address, if it does not contain the reference
     *  cell.
     *
     * @throws
     *  The internal error code `#CIRCULAR`, if the passed cell range address
     *  contains the reference cell of this formula context.
     */
    #checkCircularReference(range: Range3D): Range3D {
        const refSheet = this.#refSheet;
        const isCircular = this.#detectCircular && (refSheet !== null) && range.containsSheet(refSheet) && range.containsAddress(this.#refAddress);
        return isCircular ? InternalErrorCode.CIRCULAR.throw() : range;
    }

    /**
     * Creates an iterator that visits all scalar values contained in the
     * passed operand, matrix, or reference.
     *
     * @param source
     *  The operand whose contents will be visited.
     *
     * @param converter
     *  A callback function invoked for each scalar value contained in the
     *  operand. Receives the value as first parameter, and must return the
     *  converted scalar value. May throw a formula error code.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The scalar values contained in the passed operand.
     */
    *#createOperandIterator<T>(
        source: OperandIteratorSource,
        converter: ConverterFn<T>,
        options?: ScalarIteratorOptions
    ): OperandIterator<T> {

        // resolve passed options
        const { emptyParam, blankCells, acceptErrors, multiRange, multiSheet, collectFormats, skipHiddenRows, skipFilteredRows }: ScalarIteratorOptions = options ?? {};
        // counter for time check in large loops
        let evalCounter = 0;

        // creates a new result object for the operand iterator
        const createState = (value: ScalarType, offset: number, width: number, col: number, row: number, format: ParsedFormat | null): OperandState<T> => {
            // throw error codes, unless error codes will be accepted
            if (!acceptErrors) { throwErrorCode(value); }
            // check total evaluation time (throws if formula evaluation takes too long)
            evalCounter += 1;
            if (evalCounter % 50 === 0) { this.checkEvalTime(); }
            // convert the passed value (may throw an error code)
            const converted = converter(value);
            // fall-back to standard number format
            if (!format && collectFormats) { format = this.#standardFormat; }
            // adjust offset
            offset += width * row + col;
            // return the iterator result object
            return { value: converted, offset, col, row, index: offset, format };
        };

        // creates an iterator that visits a scalar value (skip empty parameters unless specified otherwise)
        const yieldScalar = function *(value: ScalarType): OperandIterator<T> {
            if (emptyParam || (value !== null)) {
                yield createState(value, 0, 1, 0, 0, null);
            }
        };

        // creates an iterator that visits all elements of a matrix
        const yieldMatrix = function *(matrix: ScalarMatrix): OperandIterator<T> {
            const width = matrix.cols();
            for (const entry of matrix.entries()) {
                yield createState(entry.value, 0, width, entry.col, entry.row, null);
            }
        };

        // creates an iterator that visits the cells in a range, or an array of ranges
        const yieldReference = function *(this: FormulaContext, ranges: Range3D[]): OperandIterator<T> {

            // check whether multiple ranges are supported
            if (!multiRange && (ranges.length > 1)) {
                throw ErrorCode.VALUE;
            }

            // type of cells to be visited by the cell range iterator
            const cellType = blankCells ? CellType.ALL : CellType.VALUE;

            let offset = 0;
            for (const range3d of ranges) {
                this.#checkCircularReference(range3d);

                // check whether sheet ranges are supported
                if (!multiSheet && !range3d.singleSheet()) {
                    throw ErrorCode.VALUE;
                }

                // convert the cell range to an instance of class `Range`
                const range2d = range3d.toRange();
                // number of cells per single sheet
                const cellCount = range2d.cells();
                // start position of the iteration range, and its width (needed for relative indexes)
                const col0 = range2d.a1.c, row0 = range2d.a1.r, width = range2d.cols();

                // visit all worksheets covered by the 3D cell range
                for (const sheetModel of this.docModel.yieldSheetModels({ first: range3d.sheet1, last: range3d.sheet2, cells: true })) {

                    // the cell collection of the visited sheet
                    const { cellCollection } = sheetModel;
                    // whether to skip cells in hidden rows
                    const visibleMode = (skipHiddenRows || (skipFilteredRows && sheetModel.isFiltered())) ? "rows" : false;

                    // iterate all content cells in the cell collection (skip empty cells, unless the option is set),
                    // convert cell entries to result objects (cell value, and relative column/row offsets)
                    for (const { address } of cellCollection.cellEntries(range2d, { type: cellType, visible: visibleMode })) {
                        const value = this.getCellValue(sheetModel, address);
                        const parsedFormat = collectFormats ? cellCollection.getParsedFormat(address) : null;
                        yield createState(value, offset, width, address.c - col0, address.r - row0, parsedFormat);
                    }

                    // update offset for next cell range
                    offset += cellCount;
                }
            }
        }.bind(this);

        // process a real operand (instance of class Operand)
        if (source instanceof Operand) {
            const value = source.getRawValue();
            switch (source.type) {
                case "val": yield* yieldScalar(value as ScalarType);      break;
                case "mat": yield* yieldMatrix(value as ScalarMatrix);    break;
                case "ref": yield* yieldReference(value as Range3DArray); break;
                case "any": throwEngineError("FormulaContext._createOperandIterator(): invalid operand type");
            }
            return;
        }

        // passed operand can also be a matrix, a range address, or an array of range addresses
        if (source instanceof Matrix) {
            yield* yieldMatrix(source);
        } else if (source instanceof Range3D) {
            yield* yieldReference([source]);
        } else if (source instanceof Range3DArray) {
            yield* yieldReference(source);
        } else if (source !== undefined) {
            yield* yieldScalar(source);
        }
    }

    /**
     * Invokes a callback function for an operand iterator.
     *
     * @returns
     *  The number of values visited by the iterator.
     */
    #forEachValue<T>(iterator: OperandIterator<T>, fn: OperandIteratorFn<T>): number {
        let count = 0;
        for (const opState of iterator) {
            fn.call(this, opState.value, opState);
            count += 1;
        }
        return count;
    }

    /**
     * Converts the passed iterator to an array.
     */
    #iteratorToArray<T>(iterator: OperandIterator<T>): T[] {
        return Array.from(iterator, opState => opState.value);
    }

    /**
     * Implementation helper for aggregators over multiple operands.
     */
    #aggregateValues<T, I, S>(
        sources: OperandAggregatorSource,
        initial: I,
        iterate: AggregateIteratorFn<S>,
        aggregate: AggregateAggregatorFn<I, S>,
        finalize: AggregateFinalizerFn<T, I>,
        options?: CommonIteratorOptions & { collectFormats?: boolean }
    ): T {

        // the intermediate result
        let intermediate = initial;
        // the total counter for the visited values across multiple operands
        let valueCount = 0;
        // the total counter for the offset across multiple operands
        let offsetCount = 0;
        // the intermediate parsed number format
        let resultFormat = options?.collectFormats ? this.#standardFormat : null;

        // process all passed operands (`operands` may be an instance of `IArguments`!)
        sources.forEach(source => {
            valueCount += iterate.call(this, source, (value, opState) => {
                const currIndex = valueCount + opState.index;
                const currOffset = offsetCount + opState.offset;
                intermediate = aggregate.call(this, intermediate, value, currIndex, currOffset);
                if (resultFormat) { resultFormat = combineParsedFormats(resultFormat, opState.format); }
            }, options);
            offsetCount += getOperandSize(source);
        });

        // resolve the final result, immediately throw error codes
        return throwErrorCode(finalize.call(this, intermediate, valueCount, offsetCount, resultFormat));
    }
}
