/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import { PresetCodeId } from "@/io.ox/office/editframework/model/formatter/presetformattable";

import type { ErrorLiteral, ScalarType, CompareScalarOptions, CompareResult } from "@/io.ox/office/spreadsheet/utils/scalar";
import { CompareNullMode, ErrorCode, isErrorCode, equalScalars, compareScalars, scalarToString } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { ContextType, RefSheetOptions, WrapReferencesOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { InternalErrorCode, isInternalError, throwEngineError } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";
import { FormulaErrorCode, FormulaError } from "@/io.ox/office/spreadsheet/model/formula/utils/formulaerror";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import { type ScalarMatrix, type ErrorMatrix, Matrix, isValidMatrixDim } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";

import { ScalarToken, MatrixToken, ReferenceToken, DefinedNameToken, TableToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import type { FormatCategorySpec } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import type { FunctionSignature, FunctionResource } from "@/io.ox/office/spreadsheet/model/formula/resource/functionresource";
import type { CompilerNode } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacompiler";
import type { OperandRawType, OperandAnyType, OperandConfig } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import { Operand, OperandResolver } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import { combineParsedFormats, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";
import type { InterpretFormulaOptions } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Options for interpreting a compiled formula.
 */
export interface InterpretTokensOptions extends RefSheetOptions, WrapReferencesOptions {

    /**
     * The entire bounding range of a matrix formula. Used only if the passed
     * context type is `MAT`. If not set, the matrix size will be inferred from
     * the operands.
     */
    matrixRange?: Range | null;

    /**
     * If set to `true`, trying to dereference a cell reference covering the
     * target address leads to a circular dependeny error. Default value is
     * `false`.
     */
    detectCircular?: boolean;

    /**
     * If set to `true`, all dirty cell formulas referred by the reference
     * tokens in the passed compiled token tree will be recalculated
     * recursively using the dependency manager of the document. Default value
     * is `false`.
     */
    recalcDirty?: boolean;
}

/**
 * Interpreter result, if a valid result has been calculated.
 */
export interface InterpreterValidResult {

    /**
     * Union discriminator.
     */
    type: "valid";

    /**
     * The result value of the formula.
     */
    value: OperandRawType;

    /**
     * The identifier of a number format as integer, or a format code as
     * string, associated with the result value. The property will be omitted
     * for generic formula results.
     */
    format?: ParsedFormat;

    /**
     * The URL of a hyperlink associated to the formula result, e.g. as result
     * of the HYPERLINK function.
     */
    url?: string;
}

/**
 * Interpreter result, if the formula structure is valid, but a formula result
 * cannot be calculated. The formula may be inserted into the document model
 * though.
 */
export interface InterpreterWarnResult {

    /**
     * Union discriminator.
     */
    type: "warn";

    /**
     * The warning key. Can be an error key as defined by the built-in error
     * codes (uuper-case keys of enum `ErrorCode`), or one of the following
     * special warning keys:
     *
     * - "circular": The formula results in a circular reference (the formula
     *   wants to interpret itself again during calculation, e.g. directly or
     *   indirectly via cell references, or with defined names referring each
     *   other).
     *
     * - "unsupported": An unsupported feature, e.g. a function that has not
     *   been implemented yet) was found in the formula.
     *
     * - "engine": A warning that an undetermined internal error has occurred
     *   while interpreting the formula tokens.
     */
    code: string;

    /**
     * The value to be used as formula result. May be a native error code, e.g.
     * #VALUE!, or any other scalar value (e.g. zero for circular formulas).
     *
     * If the context type of the formula was `MAT`, a 1x1 matrix will be
     * returned.
     */
    value: ScalarType | ScalarMatrix;
}

/**
 * Interpreter result, if the formula structure is invalid. The formula
 * expression MUST NOT be inserted into the document model.
 */
export interface InterpreterErrorResult {

    /**
     * Union discriminator.
     */
    type: "error";

    /**
     * The error type identifier.
     */
    code: FormulaErrorCode;

    /**
     * An appropriate error code.
     *
     * If the context type of the formula was `MAT`, a 1x1 matrix will be
     * returned.
     */
    value: ErrorLiteral | ErrorMatrix;
}

export type InterpreterResult = InterpreterValidResult | InterpreterWarnResult | InterpreterErrorResult;

// types --------------------------------------------------------------

/**
 * Intermediate formula result.
 */
interface IntermediateResult {

    /**
     * The result value of the interpreted formula, according to the context
     * type. May be any internal error code, regardless of the context type.
     */
    value: OperandRawType;

    /**
     * A parsed number format associated with the result value. The property
     * will be omitted for generic unformatted formula results.
     */
    format?: ParsedFormat;

    /**
     * The URL of a hyperlink associated to the formula result, as provided by
     * the operand passed to this method. The property will be omitted if no
     * URL is available.
     */
    url?: string;
}

// functions ==========================================================

/**
 * Checks that the passed type of an operand, operator, or function, and the
 * context type of the formula or subexpression match. Throws a `REFERENCE`
 * formula exception, if the context type is `REF`, and the passed type is
 * different from `REF` or `ANY`.
 *
 * Background: Microsoft Excel rejects such formulas immediately when loading a
 * document, and even announces a broken file format if such formulas are
 * written into the file.
 *
 * Examples of such invalid formulas are:
 * - `=A1:1` (combined number with range operator).
 * - `=OFFSET(1,1,1)` (passing a number to a reference-only parameter).
 *
 * It is possible to use operands and functions with return type `ANY` in
 * reference context, e.g.:
 * - `=A1:name` (return type of defined names is `ANY`).
 * - `=A1:IF(TRUE,A1,B1)` (return type of `IF` is `ANY`).
 *
 * If the defined name or function returns a non-reference value, the formula
 * simply results in the `#VALUE!` error.
 *
 * @param retType
 *  The return type of an operator or function, or the type of a single operand
 *  to be checked.
 *
 * @param contextType
 *  The context type that influences how to resolve values, matrixes, and
 *  references. If the context type is `REF`, only the return types `REF` and
 *  `ANY` will be accepted. All other return types will cause to throw an
 *  internal `REFERENCE` exception that marks the formula structure to be
 *  ill-formed (see description above).
 *
 * @throws
 *  The special `REFERENCE` exception (instance of `FormulaError`), if the
 *  passed return type is not supported in reference context.
 */
function checkContextType(retType: ContextType, contextType: ContextType): void {
    // context type REF only accepts REF and ANY return types
    if ((contextType === "ref") && (retType !== "ref") && (retType !== "any")) {
        throw new FormulaError("expected reference token", FormulaErrorCode.REFERENCE);
    }
}

// class InterpreterInstance ==================================================

/**
 * Resolves a compiled formula token array to the result of the formula. This
 * implementation has been moved outside the class `FormulaInterpreter` to be
 * able to allow reentrancy (resolving multiple formulas recursively, e.g. in
 * defined names).
 */
class InterpreterInstance extends ModelObject<SpreadsheetModel> {

    readonly #formulaContext: FormulaContext;
    readonly #rootNode: CompilerNode;
    readonly #refSheet: OptSheet;
    readonly #refAddress: Address;
    readonly #targetAddress: Address;

    readonly #wrapReferences: boolean;
    readonly #recalcDirty: boolean;

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The spreadsheet document model containing this instance.
     *
     * @param rootNode
     *  The root node of the compiled token tree, as returned by the method
     *  `FormulaCompiler::compileTokens`.
     *
     * @param refAddress
     *  The source reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param targetAddress
     *  The target reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(docModel: SpreadsheetModel, rootNode: CompilerNode, refAddress: Address, targetAddress: Address, config?: InterpretTokensOptions) {
        super(docModel);

        // properties
        this.#rootNode = rootNode;

        this.#refSheet = config?.refSheet ?? null;
        this.#refAddress = refAddress;
        this.#targetAddress = targetAddress;

        this.#wrapReferences = !!config?.wrapReferences;
        this.#recalcDirty = !!config?.recalcDirty;

        // the calling context for the operator resolver callback function
        this.#formulaContext = new FormulaContext(docModel, targetAddress, config);
    }

    // public methods ---------------------------------------------------------

    /**
     * Calculates the result of the formula represented by the token array
     * passed in the constructor.
     *
     * @param contextType
     *  The context type that influences how to resolve values, matrixes, and
     *  references.
     *
     * @param matrixDim
     *  The expected dimension of a matrix result. Used if the context type is
     *  `MAT`. If set to `null`, the matrix size will be inferred from the
     *  operands.
     *
     * @returns
     *  The result descriptor.
     *
     * @throws
     *  Any fatal internal interpreter error (instance of `FormulaError`).
     */
    getResult(contextType: ContextType, matrixDim: Dimension | null): IntermediateResult {

        // get the result operand from the root node of the token tree
        const operand = fun.do(() => {
            try {
                return this.#resolveNode(this.#rootNode, contextType, matrixDim);
            } catch (error) {
                // operators may throw formula error codes as dedicated result, but rethrow
                // interpreter errors (class FormulaError), and other internal JS exceptions
                if (isErrorCode(error)) { return this.#createOperand(error); }
                throw error;
            }
        });

        // ANY context: resolve to actual type of the resulting operand
        const opType = (contextType === "any") ? operand.type : contextType;

        // resolve to result value according to context type (internal errors will be thrown)
        const value = fun.do(() => {
            try {
                switch (opType) {
                    case "val":
                        return operand.getScalar(null, null);
                    case "mat":
                        return operand.getMatrix();
                    case "ref":
                        return operand.getRanges();
                    default:
                        throwEngineError(`InterpreterInstance.getResult(): unknown context type: "${opType}"`);
                }
            } catch (error) {
                // operand may throw formula error codes as dedicated result, but rethrow
                // interpreter errors (class FormulaError), and other internal JS exceptions
                if (isErrorCode(error)) { return error; }
                throw error;
            }
        });

        // create the result object (optional properties will be added below)
        const result: IntermediateResult = { value };

        // resolve the parsed number format
        const parsedFormat = operand.format;
        if (parsedFormat && !parsedFormat.isStandard()) {
            result.format = parsedFormat;
        }

        // resolve the URL of a hyperlink
        if (operand.url) {
            result.url = operand.url;
        }

        return result;
    }

    // private methods --------------------------------------------------------

    /**
     * Resolves the passed reference token to its result.
     *
     * @param refToken
     *  The formula token representing a cell range reference. If the token
     *  cannot be resolved to a cell range address, this method returns the
     *  #REF! error code.
     *
     * @returns
     *  The cell range addresses contained in the reference, or a #REF! error
     *  code.
     */
    #resolveReference(refToken: ReferenceToken): Range3DArray | ErrorLiteral {
        const range = refToken.getRange3D(this.#refAddress, this.#targetAddress, this.#refSheet, this.#wrapReferences);
        return range ? new Range3DArray(range) : ErrorCode.REF;
    }

    /**
     * Resolves the passed name token to its result.
     *
     * @param nameToken
     *  The formula token representing a defined name. If the name does not
     *  exist in the document, this method returns the #NAME? error code.
     *
     * @param contextType
     *  The context type that influences how to resolve values, matrixes, and
     *  references.
     *
     * @param matrixDim
     *  The expected dimension of a matrix result. If set to null, the size of
     *  the result matrix will be inferred from the operand.
     *
     * @returns
     *  The result value of the defined name (may be values, matrixes, or
     *  references).
     */
    #resolveDefinedName(nameToken: DefinedNameToken, contextType: ContextType, matrixDim: Dimension | null): OperandRawType {

        // resolve the model of a defined name from the token (unknown names result in the #NAME? error code)
        const nameModel = this.docModel.resolveNameModel(nameToken, this.#refSheet);
        if (!nameModel) { return ErrorCode.NAME; }

        // resolve relative references in the name according to context address
        const targetAddress = this.#formulaContext.getRefAddress();

        // resolve the formula in the defined name (defined names are always relative to cell A1)
        const options: InterpretFormulaOptions = { refSheet: this.#refSheet, recalcDirty: this.#recalcDirty };

        // reconstruct the matrix range
        if (matrixDim) {
            options.matrixRange = Range.fromAddressAndSize(targetAddress, matrixDim.cols, matrixDim.rows);
        }

        // bug 56629: matrix context without matrix range: resolve as any context
        if (!matrixDim && (contextType === "mat")) {
            contextType = "any";
        }

        // defined names are always defined relative to cell A1
        return nameModel.tokenArray.interpretFormula(contextType, Address.A1, targetAddress, options).value;
    }

    /**
     * Resolves the passed table token to its result.
     *
     * @param tableToken
     *  The formula token representing a structured table reference. If the
     *  token cannot be resolved to a range, this method returns the #REF!
     *  error code.
     *
     * @returns
     *  The resulting range referred by the passed table token, or the  #REF!
     *  error code.
     */
    #resolveTableRange(tableToken: TableToken): Range3DArray | ErrorLiteral {
        const range = tableToken.getRange3D(this.#targetAddress);
        return range ? new Range3DArray(range) : ErrorCode.REF;
    }

    /**
     * Creates a new instance of the class Operand. An operand is an object
     * that carries formula result values of any data type.
     *
     * @param value
     *  The value to be stored as operand.
     *
     * @param [options]
     *  Optional parameters for the operand.
     *
     * @returns
     *  A new operand with the passed value and options.
     */
    #createOperand(value: OperandAnyType | Operand, options?: OperandConfig): Operand {
        // do not create a new instance, if an operand has been passed without additional options
        return (!options && (value instanceof Operand)) ? value : this.#formulaContext.createOperand(value, options);
    }

    /**
     * Creates an operand object for the passed parser token.
     *
     * @param operandNode
     *  The compiler token representing an operand (scalar values, constant
     *  matrixes, cell references, defined names).
     *
     * @param contextType
     *  The context type that influences how to resolve values, matrixes, and
     *  references.
     *
     * @param matrixDim
     *  The expected dimension of a matrix result. If set to null, the size of
     *  the result matrix will be inferred from the operand.
     *
     * @returns
     *  An operand instance representing the passed token.
     *
     * @throws
     *  The internal error code `#UNSUPPORTED`, if the type of the passed token
     *  is not supported.
     */
    #resolveOperandNode(operandNode: CompilerNode, contextType: ContextType, matrixDim: Dimension | null): Operand {

        // the parser token representing the operand value
        const { token } = operandNode;

        // the resulting context type of the operand token
        let opType: ContextType;
        // the resulting value to be inserted into the operand
        let value: OperandAnyType;

        // resolve token values by type
        formulaLogger.log(() => `> resolving operand token ${token} with context type ${contextType}`);
        if (token instanceof ScalarToken) {
            value = token.value;
            opType = (value === ErrorCode.REF) ? "ref" : "val";
        } else if (token instanceof MatrixToken) {
            value = token.value;
            opType = "mat";
        } else if (token instanceof ReferenceToken) {
            value = this.#resolveReference(token);
            opType = "ref";
        } else if (token instanceof DefinedNameToken) {
            value = this.#resolveDefinedName(token, contextType, matrixDim);
            opType = "any";
        } else if (token instanceof TableToken) {
            value = this.#resolveTableRange(token);
            opType = "ref";
        } else {
            throwEngineError(`InterpreterInstance.resolveOperandNode(): unknown token type: "${token.type}"`);
        }

        // check context type (will throw "reference" exception)
        checkContextType(opType, contextType);
        return this.#createOperand(value);
    }

    /**
     * Converts a number format category to a parsed number format intended to
     * be returned with the result of an operator or function.
     */
    #getParsedFormat(resolver: OperandResolver, category: FormatCategorySpec): ParsedFormat | null {

        const { numberFormatter } = this.docModel;

        // infer the number format from all operands
        if (category === "infer") {
            let parsedFormat: ParsedFormat | null = numberFormatter.standardFormat;
            for (let index = 0; parsedFormat && (index < resolver.size()); index += 1) {
                const operand = resolver.get(index);
                const opFormat = operand?.format ?? numberFormatter.standardFormat;
                parsedFormat = combineParsedFormats(parsedFormat, opFormat);
            }
            return parsedFormat;
        }

        // convert format category to parsed number format
        switch (category) {
            case "percent":  return numberFormatter.getPercentFormat({ int: true }); // no decimal places
            case "fraction": return numberFormatter.getParsedFormat(PresetCodeId.FRACTION_LONG); // two-digit fractions
            case "currency": return numberFormatter.getCurrencyFormat({ red: true });
            case "date":     return numberFormatter.getSystemDateFormat();
            case "time":     return numberFormatter.getSystemTimeFormat();
            case "datetime": return numberFormatter.getSystemDateTimeFormat();
            default:         return null;
        }
    }

    #getOperand(resource: FunctionResource, signature: FunctionSignature, operands: Array<Operand | null>, matRow: number, matCol: number): Operand {

        // check maximum evaluation time for each operator to prevent freezing browser
        this.#formulaContext.checkEvalTime();

        // catch thrown error codes, return them as result value
        const result = fun.do(() => {
            try {

                // initialize the correct matrix element position, before resolving parameter values
                this.#formulaContext.setMatrixPosition(matRow, matCol);

                // resolve the operand values according to the signature (except lazy function parameters)
                const params = operands.map((operand, index) => {
                    return operand ? this.#formulaContext.convertOperandToValue(operand, signature[index].typeKey) : null;
                });

                // get the raw result of the operator (convert thrown error codes to scalar values)
                formulaLogger.logTokens("\xa0 resolving with parameters", params);
                return resource.resolve!.apply(this.#formulaContext, params);

            } catch (error) {
                // operators may throw formula error codes as dedicated result
                if (isErrorCode(error)) { return error; }
                // rethrow interpreter errors (class FormulaError), and other internal JS exceptions
                throw error;
            }
        });

        // ensure to return an Operand instances
        return this.#createOperand(result);
    }

    /**
     * Processes the passed operator or function call. Calculates the required
     * number of operands from the following compiler tokens, and calculates
     * the result of the operator or function.
     *
     * @param subNode
     *  The root node of a compiler token sub-tree representing an operator or
     *  function.
     *
     * @param srcOperands
     *  The operands attached to the operator.
     *
     * @param contextType
     *  The context type that influences how to resolve values, matrixes, and
     *  references.
     *
     * @param matrixDim
     *  The expected dimension of a matrix result. Used if the context type is
     *  `MAT`. If set to `null`, the size of the result matrix will be inferred
     *  from the operands of the passed token node.
     *
     * @returns
     *  An operand instance representing the result of the operator or function
     *  call.
     *
     * @throws
     *  The `MISSING` formula error, if there are not enough tokens available
     *  in the compiled token array.
     */
    #resolveOperatorNode(subNode: CompilerNode, srcOperands: readonly CompilerNode[], contextType: ContextType, matrixDim: Dimension | null): Operand {

        // immediately throw compiler errors without any further processing
        if (subNode.error) { throw subNode.error; }

        // the parser token representing the operator or function
        const { token } = subNode;
        formulaLogger.log(() => `> processing operator ${token} with ${srcOperands.length} parameters and context type ${contextType}`);

        // the operator/function resource; return #NAME? for unknown functions (external functions, macro calls)
        const { resource } = subNode;
        if (!resource) { return this.#createOperand(ErrorCode.NAME); }

        // check context type (will throw "reference" exception)
        checkContextType(resource.retType, contextType);

        // check missing implementation
        if (!is.function(resource.resolve)) {
            formulaLogger.warn(() => `$badge{InterpreterInstance} resolveOperatorNode: unsupported operator or function "${token}"`);
            return this.#createOperand(InternalErrorCode.UNSUPPORTED);
        }

        // whether the matrix range has been passed explicitly
        if (matrixDim && !isValidMatrixDim(matrixDim)) {
            return this.#createOperand(InternalErrorCode.UNSUPPORTED);
        }

        // create and fill the operand resolver
        const signature = subNode.signature!; // "resolve" and "signature" exist together
        const resolver = new OperandResolver();
        signature.forEach((typeData, index) => {

            // the operand node to be passed to the node resolver
            const opNode = srcOperands[index];
            // the effective context type for the operand subtree
            const opContextType = (typeData.preferMat && (contextType === "mat")) ? "mat" : typeData.contextType;
            // whether to forward outer matrix dimension to operand subtree
            const forwardDim = typeData.forwardDim && (opContextType === "mat");

            resolver.push(() => this.#resolveNode(opNode, opContextType, forwardDim ? matrixDim : null));
        });

        // resolve the unconverted operands (except lazy function parameters)
        const operands = signature.map((typeData, index) => (typeData.typeKey === "any:lazy") ? null : resolver.resolve(index));

        // Implicitly expand matrix range for value operators, if all operands are value type, and at least
        // one will receive a matrix. Example: In the formula =SUM({1;2|3;4}+1), the addition operator will
        // add the scalar number 1 to each matrix element, before handing the new matrix to SUM.
        if (!matrixDim && (resource.retType === "val")) {

            // in matrix context: any value-type parameter with a matrix or reference;
            // otherwise: all parameters must be value-type, any of them gets a matrix (no reference)
            const matType = contextType === "mat";
            if (matType || resource.allValType) {
                try {
                    operands.forEach((operand, index) => {
                        if (operand && (matType ? ((signature[index].baseType === "val") && (operand.type !== "val")) : (operand.type === "mat"))) {
                            const opDim = operand.getDimension({ errorCode: ErrorCode.VALUE }); // #VALUE! thrown for complex references
                            matrixDim = Dimension.boundary(matrixDim, opDim);
                        }
                    });
                } catch (error) {
                    // #VALUE! error code thrown for complex references
                    if (isErrorCode(error)) { return this.#createOperand(error); }
                    throw error;
                }

                // validate the resulting matrix size early
                if (matrixDim) {
                    if (!isValidMatrixDim(matrixDim)) {
                        return this.#createOperand(InternalErrorCode.UNSUPPORTED);
                    }
                    contextType = "mat";
                }
            }
        }

        // calculate the result of the operator
        const operand = formulaLogger.takeTime(resource.key, () => {
            try {

                // register the operator resolver with the current operands at the formula context, to make the
                // operands accessible to function implementations via public methods of the context
                this.#formulaContext.pushOperandResolver(resolver, contextType, matrixDim);

                // invoke the resolver callback function of the operator
                let operand = this.#getOperand(resource, signature, operands, 0, 0);
                // expected return type category
                const retType = resource.retType;

                // allow matrix-type operators to return scalars (especially error codes)
                if ((retType === "mat") && (operand.type === "val")) {
                    operand = this.#createOperand(new Matrix(operand.getRawValue() as ScalarType));
                }

                // exit immediately if the result does not match the specified type
                if ((retType !== "any") && (retType !== operand.type) &&
                    // allow error codes as return values for ref-type operators
                    !((retType === "ref") && (operand.type === "val") && isErrorCode(operand.getRawValue())) &&
                    // in matrix context, scalar-type functions/operands are allowed to return matrixes
                    !((retType === "val") && (contextType === "mat") && (operand.type === "mat"))
                ) {
                    throwEngineError(`InterpreterInstance.resolveOperatorNode(): ${resource.key}: operator result type does not match operator return type`);
                }

                // expand scalar operators to matrix results if required
                if (matrixDim && (operand.type === "val")) {
                    formulaLogger.info(() => `\xa0 expand scalar result to ${matrixDim} matrix`);
                    const matrix = new Matrix(matrixDim.rows, matrixDim.cols, (matRow, matCol) => {
                        if ((matRow === 0) && (matCol === 0)) { return operand.getRawValue() as ScalarType; }
                        const elementOp = this.#getOperand(resource, signature, operands, matRow, matCol);
                        if (elementOp.type === "val") { return elementOp.getRawValue() as ScalarType; }
                        throwEngineError("InterpreterInstance.resolveOperatorNode(): invalid matrix element type");
                    });
                    operand = this.#createOperand(matrix);
                }

                return operand;

            } finally {
                // remove the operand resolver from the internal stack of the formula context
                this.#formulaContext.popOperandResolver();
            }
        });

        // resolve the number format category of the resource to a parsed number format
        const parsedFormat = (resource.format && (contextType !== "mat")) ? this.#getParsedFormat(resolver, resource.format) : null;
        // create an operand with resulting number format if available
        return parsedFormat ? this.#createOperand(operand, { format: parsedFormat }) : operand;
    }

    /**
     * Returns the result of the specified compiler token. If it is an operand,
     * it will be returned. If it is an operator or function call, its result
     * will be calculated and returned. If the operands of the operator or
     * functions are also operators or functions, their results will be
     * calculated recursively.
     *
     * @param tokenNode
     *  A node from a compiled token tree to be processed.
     *
     * @param contextType
     *  The context type that influences how to resolve values, matrixes, and
     *  references.
     *
     * @param matrixDim
     *  The expected dimension of a matrix result. Used if the context type is
     *  `MAT`. If set to `null`, the size of the result matrix will be inferred
     *  from the operands of an operator or function contained in the passed
     *  token node.
     *
     * @returns
     *  The result of the specified compiler token.
     */
    #resolveNode(tokenNode: CompilerNode, contextType: ContextType, matrixDim: Dimension | null): Operand {
        const operand = tokenNode.operands ? this.#resolveOperatorNode(tokenNode, tokenNode.operands, contextType, matrixDim) : this.#resolveOperandNode(tokenNode, contextType, matrixDim);
        formulaLogger.log(() => `< result of ${tokenNode}: ${operand}`);
        return operand;
    }
}

// class FormulaInterpreter ===================================================

/**
 * Calculates the result for a compiled tree of compiler token nodes. See class
 * `FormulaCompiler` for details about compiled token trees.
 */
export class FormulaInterpreter extends ModelObject<SpreadsheetModel> {

    /** File-format dependent options for value comparison. */
    readonly #COMPARE_OPTIONS: CompareScalarOptions = {
        withCase: this.docApp.isODF(),
        nullMode: CompareNullMode.CONVERT,
    };

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed scalar values are considered to be equal
     * according to the current file format. For ODF documents, the character
     * case of strings matters.
     *
     * @param value1
     *  The first value to be compared.
     *
     * @param value2
     *  The second value to be compared.
     *
     * @returns
     *  Whether the passed scalar values are considered to be equal. See public
     *  function `equalScalars` for more details.
     */
    equalScalars(value1: ScalarType, value2: ScalarType): boolean {
        return equalScalars(value1, value2, this.#COMPARE_OPTIONS);
    }

    /**
     * Compares the passed scalar values according to the current file format.
     * For ODF documents, the character case of strings matters.
     *
     * @param value1
     *  The first value to be compared.
     *
     * @param value2
     *  The second value to be compared.
     *
     * @returns
     *  A negative value, if `value1` is less than `value2`; or a positive
     *  value, if `value1` is greater than `value2`; or zero, if both values
     *  are of the same type and are equal.
     */
    compareScalars(value1: ScalarType, value2: ScalarType): CompareResult {
        return compareScalars(value1, value2, this.#COMPARE_OPTIONS);
    }

    /**
     * Returns an interpreter result object that represents a warning based on
     * an error code.
     *
     * @param contextType
     *  The context type that influences how to create the result value. If the
     *  context type is `MAT`, a 1x1 matrix with an error code as element will
     *  be created, otherwise the error code will be set as value directly.
     *
     * @param errorCode
     *  The error code to be inserted into the result. Can be any regular or
     *  internal error code.
     *
     * @returns
     *  The interpreter result object of type "warn".
     */
    createWarnResult(contextType: ContextType, errorCode: ErrorLiteral): InterpreterWarnResult {
        const error = isInternalError(errorCode) ? errorCode.value : errorCode;
        const value = (contextType === "mat") ? new Matrix(error) : error;
        formulaLogger.info(() => `result: type=warn code=${errorCode.key} value=${scalarToString(value)}`);
        return { type: "warn", code: errorCode.key, value };
    }

    /**
     * Returns an interpreter result object that represents a fatal error.
     *
     * @param contextType
     *  The context type that influences how to create the result value. If the
     *  context type is `MAT`, a 1x1 matrix with the error code #N/A as element
     *  will be created, otherwise the error code #N/A will be set as value
     *  directly.
     *
     * @param errorKey
     *  The error code to be inserted into the result.
     *
     * @returns
     *  The interpreter result object of type "error". The property `value`
     *  will be set to the formula error code #N/A (or a 1x1 matrix with that
     *  error code).
     */
    createErrorResult(contextType: ContextType, errorKey: FormulaErrorCode): InterpreterErrorResult {
        const value = (contextType === "mat") ? new Matrix(ErrorCode.NA) : ErrorCode.NA;
        formulaLogger.info(() => `result: type=error code=${errorKey} value=${scalarToString(value)}`);
        return { type: "error", code: errorKey, value };
    }

    /**
     * Calculates the result of the formula represented by the passed compiled
     * token array.
     *
     * @param contextType
     *  The context type that influences how to resolve values, matrixes, and
     *  references.
     *
     * @param rootNode
     *  The root node of the compiled token tree, as returned by the method
     *  `compileTokens()`.
     *
     * @param refAddress
     *  The source reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param targetAddress
     *  The target reference address used to relocate reference tokens with
     *  relative column/row components.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The result descriptor of the formula interpreter.
     */
    @formulaLogger.profileMethod("$badge{FormulaInterpreter} interpretTokens")
    interpretTokens(contextType: ContextType, rootNode: CompilerNode, refAddress: Address, targetAddress: Address, options?: InterpretTokensOptions): InterpreterResult {
        formulaLogger.log(() => `contextType=${contextType}`);

        // the actual interpreter instance, needed for recursive interpretation of e.g. defined names
        const instance = new InterpreterInstance(this.docModel, rootNode, refAddress, targetAddress, options);

        try {

            // calculate the formula reult
            const matrixRange = (contextType === "mat") && options?.matrixRange;
            const matrixDim = matrixRange ? Dimension.createFromRange(matrixRange) : null;
            const instResult = instance.getResult(contextType, matrixDim);

            // create a special result object for internal error codes (in catch andler below)
            if (isInternalError(instResult.value)) {
                return this.createWarnResult(contextType, instResult.value);
            }

            // log result to console and return ("finally" block will clean up the interpreter instance)
            const result: InterpreterValidResult = { type: "valid", ...instResult };
            formulaLogger.info(() => `result: type=valid value=${scalarToString(result.value)} format=${result.format?.formatCode} url=${result.url}`);
            return result;

        } catch (error) {

            // create a special result object for internal error codes
            if (isErrorCode(error)) {
                return this.createWarnResult(contextType, error);
            }

            // internal interpreter errors (invalid formula structure)
            if (error instanceof FormulaError) {
                formulaLogger.info(`\xa0 error="${error.message}"`);
                return this.createErrorResult(contextType, error.code);
            }

            throw error; // re-throw internal JS exceptions

        } finally {
            instance.destroy();
        }
    }
}
