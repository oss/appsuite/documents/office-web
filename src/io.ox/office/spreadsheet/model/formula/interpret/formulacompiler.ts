/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, ary } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";

import { type RecalcMode, MAX_PARAM_COUNT, getRecalcMode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { LogLevel, formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";
import { FormulaErrorCode, FormulaError } from "@/io.ox/office/spreadsheet/model/formula/utils/formulaerror";
import type { FunctionSignature, FunctionResource } from "@/io.ox/office/spreadsheet/model/formula/resource/functionresource";
import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import { isWsToken, ScalarToken, OperatorToken, SheetRefToken, ReferenceToken, FunctionToken, TableToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * A result descriptor for the compiled token tree.
 */
export interface CompilerResult {

    /**
     * The root node of the compiled token tree. Will be a single node
     * containing the error code `#N/A` as operand, if the compilation process
     * has failed (see next property `error`).
     */
    root: CompilerNode;

    /**
     * The value `null`, if the token array has been compiled successfully.
     * Otherwise, one of the following error codes:
     * - `MISSING`: The end of the token array has been reached unexpectedly
     *   (e.g. incomplete formula).
     * - `UNEXPECTED`: A token with an invalid type has been found.
     */
    error: FormulaErrorCode | null;

    /**
     * The number of missing closing parentheses at the end of the formula
     * expression.
     */
    missingClose: number;

    /**
     * A set with the resource keys of all unimplemented functions that occur
     * in the passed array of formula tokens.
     */
    missingFuncs?: Set<string>;
}

// optional token parameter or return value
type OptToken = Opt<BaseToken>;

// categories of operators with equal precedence
type PrecedenceCategory = keyof typeof OPERATOR_PRECEDENCE_CATEGORIES;

// states for parsing a complete function call
const enum FuncState { START = 0, OPEN = 1, PARAM = 2, SEP = 3, CLOSE = 4, END = 5 }

// constants ==================================================================

// maps operator precedence categories to operator resource keys
const OPERATOR_PRECEDENCE_CATEGORIES = {
    cmp:   new Set(["eq", "ne", "lt", "le", "gt", "ge"]),
    con:   new Set(["con"]),
    upre:  new Set(["add", "sub"]),
    upost: new Set(["upct"]),
    add:   new Set(["add", "sub"]),
    mul:   new Set(["mul", "div"]),
    pow:   new Set(["pow"]),
    list:  new Set(["list"]),
    isect: new Set(["isect"]),
    range: new Set(["range"])
} satisfies Dict<Set<string>>;

// private functions ==========================================================

/**
 * Returns whether the passed formula token is an operator of the specified
 * precedence category.
 *
 * @param token
 *  The parser token to be checked. May be `null`, in this case this function
 *  will return `false`.
 *
 * @param category
 *  An operator precedence category.
 *
 * @returns
 *  Whether the passed value is an existing operator token of the specified
 *  precedence category.
 */
function isOperatorToken(token: OptToken, category: PrecedenceCategory): token is OperatorToken {
    return (token instanceof OperatorToken) && OPERATOR_PRECEDENCE_CATEGORIES[category].has(token.value);
}

/**
 * Throws a `FormulaError` instance with type `MISSING`.
 */
function throwMissing(msg: string): never {
    throw new FormulaError(msg, FormulaErrorCode.MISSING);
}

/**
 * Throws a `FormulaError` instance with type `UNEXPECTED`.
 */
function throwUnexpected(msg: string): never {
    throw new FormulaError(msg, FormulaErrorCode.UNEXPECTED);
}

// class CompilerNode =========================================================

/**
 * Represents an operand, an operator, or a function call in the compiled token
 * tree.
 */
export class CompilerNode {

    /**
     * A reference to the original parser token describing the operand, the
     * operator, or the function call.
     */
    readonly token: BaseToken;

    /**
     * The sub-trees of all operands associated to an operator; or `null` for
     * operands, empty array for operators without operands.
     */
    readonly operands: readonly CompilerNode[] | null;

    /**
     * The resource object with the implementation of an operator or function,
     * or `null` for operands, and for unknown operators or functions.
     */
    readonly resource: FunctionResource | null;

    /**
     * The recalculation mode of the function represented by this compiler
     * node, or of any function in the operand sub-trees.
     */
    readonly recalc: RecalcMode = "normal";

    /**
     * Whether the token represented by this compiler node, or any token in the
     * operand sub-trees contains or behaves like a relative column reference.
     */
    readonly relColRef: boolean = false;

    /**
     * Whether the token represented by this compiler node, or any token in the
     * operand sub-trees contains or behaves like a relative row reference.
     */
    readonly relRowRef: boolean = false;

    /**
     * The type signature of the operands (property `operands`) of an operator
     * or function; or `null` for operands, and for unknown operators or
     * unknown functions.
     */
    readonly signature: FunctionSignature | null = null;

    /**
     * An error object, if this node represents a formula operator, or a
     * built-in function (the property `resource` exists), but the number of
     * operands does not match the definition of the operator or function.
     */
    error: FormulaError | null = null;

    // constructor ------------------------------------------------------------

    constructor(token: BaseToken, operands?: CompilerNode[] | null, resource?: FunctionResource | null) {

        this.token = token;
        this.operands = operands ?? null;
        this.resource = resource ?? null;

        // reference tokens may contain relative/absolute columns and rows
        if (token instanceof ReferenceToken) {
            this.relColRef = token.hasRelCol();
            this.relRowRef = token.hasRelRow();
            return;
        }

        // table token may contain a `#ThisRow` reference, i.e. a relative row reference
        if (token instanceof TableToken) {
            this.relRowRef = token.isRowRegion();
            return;
        }

        // initialize recalculation mode and relative flags from descriptor
        if (resource) {
            this.recalc = resource.recalc || null;
            const operandCount = operands ? operands.length : 0;
            this.relColRef = resource.relColRef ? resource.relColRef(operandCount) : false;
            this.relRowRef = resource.relRowRef ? resource.relRowRef(operandCount) : false;
        }

        // update recalculation mode and relative flags from function operands
        if (operands) {
            this.recalc = operands.reduce((recalc, operand) => getRecalcMode(recalc, operand.recalc), this.recalc);
            this.relColRef = this.relColRef || operands.some(operand => operand.relColRef);
            this.relRowRef = this.relRowRef || operands.some(operand => operand.relRowRef);
        }

        // operands and operator descriptor must exist for further processing (built-in functions)
        if (!operands || !resource) { return; }

        // shortcuts to function resource properties
        const { minParams, signature } = resource;

        // get maximum number of parameters, and length of repeated sequences
        let maxParams: number;
        let repeatParams: number;
        if (is.number(resource.maxParams)) {
            maxParams = resource.maxParams;
            repeatParams = 1;
        } else {
            repeatParams = resource.repeatParams || 1;
            maxParams = minParams + math.floorp(MAX_PARAM_COUNT - minParams, repeatParams);
        }

        // check operand count
        const params = operands.length;
        if (!this.#ensure(minParams <= params, "not enough parameters", FormulaErrorCode.MISSING)) { return; }
        if (!this.#ensure(params <= maxParams, "too many parameters", FormulaErrorCode.UNEXPECTED)) { return; }

        // repeated parameter sequences must be complete
        if (!this.#ensure((params - minParams) % repeatParams === 0, "not enough parameters", FormulaErrorCode.MISSING)) { return; }

        // build the complete type signature
        if ((params === 0) || (signature.length > 0)) {
            this.signature = ary.generate(params, index => {

                // adjust index, if signature is shorter (repeat the last sequence)
                if (signature.length <= index) {
                    index = signature.length - repeatParams + ((index - signature.length) % repeatParams);
                }

                return signature[index];
            });
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the string representation of this node for debug logging.
     */
    toString(): string {
        return `${this.token}${this.operands ? `:${this.operands.length}` : ""}`;
    }

    // private methods --------------------------------------------------------

    /**
     * Initializes the property `error` of this node, if the passed condition
     * is falsy.
     *
     * @param condition
     *  The condition. If falsy, the property `error` of this instance will be
     *  set to a new instance of `FormulaError`.
     *
     * @param message
     *  The message text to be inserted into the formula error.
     *
     * @param code
     *  The internal error code to be inserted into the formula error.
     *
     * @returns
     *  Whether the passed condition was truthy.
     */
    #ensure(condition: unknown, message: string, code: FormulaErrorCode): boolean {
        if (!condition) { this.error = new FormulaError(message, code); }
        return !!condition;
    }
}

// class CompilerInstance =====================================================

/**
 * A class for one-way instances for parsing an array of formula tokens.
 */
class CompilerInstance {

    /**
     * The resources for all supported operators.
     */
    readonly #operatorMap: FormulaResource["operatorMap"];

    /**
     * The resources for all supported functions.
     */
    readonly #functionMap: FormulaResource["functionMap"];

    /**
     * The array of formula tokens, in infix notation.
     */
    readonly #tokens: readonly BaseToken[];

    /**
     * The array index of the next token to be compiled.
     */
    #index = 0;

    /**
     * Whether to stop subexpression at separator token (inside functions).
     */
    #stopAtSep = false;

    /**
     * The stack of operand root nodes waiting to be inserted into an operator.
     */
    readonly #operandStack: CompilerNode[] = [];

    /**
     * Number of missing closing parentheses at the end of the formula
     * expression.
     */
    #missingClose = 0;

    /**
     * Keys of all functions that do not contain an implementation callback.
     */
    readonly #missingFuncs = new Set<string>();

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel, tokens: readonly BaseToken[]) {
        // formula resource properties will be cached for the lifetime of this instance
        // only (this is a one-way instance for compiling a single formula expression)
        this.#operatorMap = docModel.formulaResource.operatorMap;
        this.#functionMap = docModel.formulaResource.functionMap;
        this.#tokens = tokens;
    }

    // public methods ---------------------------------------------------------

    /**
     * Compiles the tokens passed to the constructor from infix notation to a
     * tree structure. Removes all tokens that are not important for
     * interpreting the formula, e.g. white-space tokens, parentheses and
     * parameter separators of function calls, and all other regular
     * parentheses.
     *
     * @returns
     *  A result descriptor for the compiled token tree.
     */
    compileTokens(): CompilerResult {

        try {
            formulaLogger.logTokens("compiling", this.#tokens);

            // compile the entire token array as subexpression
            const resultToken = this.#compileExpression(this.#requireNextToken());

            // double-check the compiled tokens
            if (this.#operandStack.length === 0) {
                throwMissing("missing formula expression");
            }
            if (this.#operandStack.length > 1) {
                throwUnexpected("invalid formula expression");
            }

            // error, when tokens are left in the passed token array
            if (resultToken || (this.#index < this.#tokens.length)) {
                throwUnexpected("unexpected garbage at end of formula");
            }

            formulaLogger.withLogLevel(LogLevel.DEBUG, () => {
                const nodes: CompilerNode[] = [];
                (function pushNode(node: CompilerNode): void {
                    nodes.push(node);
                    if (node.operands) { node.operands.forEach(pushNode); }
                }(this.#operandStack[0]));
                formulaLogger.logTokens("compiled", nodes);
            });

        } catch (error) {

            if (error instanceof FormulaError) {
                formulaLogger.info(`$badge{FormulaCompiler} compileTokens: error="${error.message}", index=${this.#index - 1}`);
                return {
                    root: new CompilerNode(new ScalarToken(ErrorCode.NA)),
                    error: error.code,
                    missingClose: 0
                };
            }
            throw error;
        }

        return {
            root: this.#operandStack[0],
            error: null,
            missingClose: this.#missingClose,
            missingFuncs: this.#missingFuncs
        };
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the next parser token, and increases the array index.
     *
     * @returns
     *  The next available parser token; or `null`, if the end of the token
     *  array has been reached.
     */
    #getNextRawToken(): OptToken {
        const token = this.#tokens[this.#index] as OptToken;
        if (token) { this.#index += 1; }
        return token;
    }

    /**
     * Returns the next non-white-space parser token, and increases the array
     * index.
     *
     * @returns
     *  The next available parser token; or `null`, if the end of the token
     *  array has been reached.
     */
    #getNextToken(): OptToken {
        let token = this.#getNextRawToken();
        while (isWsToken(token)) {
            token = this.#getNextRawToken();
        }
        return token;
    }

    /**
     * Returns the next non-white-space parser token, and increases the array
     * index. Throws an exception, if no more parser tokens are available in
     * the array.
     *
     * @returns
     *  The next available parser token.
     *
     * @throws
     *  A `FormulaError` instance with type `MISSING`, if no more parser tokens
     *  are available.
     */
    #requireNextToken(): BaseToken {
        return this.#getNextToken() ?? throwMissing("unexpected end of formula");
    }

    /**
     * Pushes an operator node onto the operand stack, after pulling the
     * required number of operand nodes.
     */
    #pushOperator(token: BaseToken, params: number): void {

        // check that enough operands are available on the stack
        if (this.#operandStack.length < params) {
            throwMissing("invalid operand stack size");
        }

        // create a compiler node with the specified number of operands
        const operands = (params > 0) ? this.#operandStack.splice(-params) : [];
        const resource = (token instanceof OperatorToken) ? this.#operatorMap.get(token.value) : (token instanceof FunctionToken) ? this.#functionMap.get(token.value) : null;
        this.#operandStack.push(new CompilerNode(token, operands, resource));

        // collect the resource keys of all unimplemented functions
        if (resource && !resource.resolve && (token instanceof FunctionToken)) {
            this.#missingFuncs.add(resource.key);
        }
    }

    /**
     * Pushes an operand node onto the operand stack.
     */
    #pushOperand(token: BaseToken): void {
        this.#operandStack.push(new CompilerNode(token));
    }

    /**
     * Compiles a subexpression by invoking the passed function, and afterwards
     * inserts the passed token into the compiled token array.
     *
     * @param method
     *  A method of this instance invoked with the next available token (or
     *  `null`, if no more tokens are available).
     *
     * @param opToken
     *  The operator token to be inserted into the token array.
     *
     * @param params
     *  The number of parameters expected by the operator token.
     *
     * @returns
     *  The token returned by the passed callback function.
     */
    #compileTermAndPush(method: (this: this, token: OptToken) => OptToken, opToken: BaseToken, params: number): OptToken {
        const token = method.call(this, this.#getNextToken());
        this.#pushOperator(opToken, params);
        return token;
    }

    /**
     * Compiles a subexpression starting at the passed token, until the end of
     * the formula, or the specified terminator token.
     */
    #compileExpression(token: OptToken, stopAtSep?: boolean): OptToken {
        const oldStopAtSep = this.#stopAtSep;
        this.#stopAtSep = !!stopAtSep;
        token = this.#compileCompareTerm(token);
        this.#stopAtSep = oldStopAtSep;
        return token;
    }

    /**
     * Compiles a comparison operator subexpression.
     */
    #compileCompareTerm(token: OptToken): OptToken {
        token = this.#compileConcatTerm(token);
        while (isOperatorToken(token, "cmp")) {
            token = this.#compileTermAndPush(this.#compileConcatTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles a string concatenation operator subexpression.
     */
    #compileConcatTerm(token: OptToken): OptToken {
        token = this.#compileAddSubTerm(token);
        while (isOperatorToken(token, "con")) {
            token = this.#compileTermAndPush(this.#compileAddSubTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles an addition or subtraction operator subexpression.
     */
    #compileAddSubTerm(token: OptToken): OptToken {
        token = this.#compileMulDivTerm(token);
        while (isOperatorToken(token, "add")) {
            token = this.#compileTermAndPush(this.#compileMulDivTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles a multiplication or division operator subexpression.
     */
    #compileMulDivTerm(token: OptToken): OptToken {
        token = this.#compilePowTerm(token);
        while (isOperatorToken(token, "mul")) {
            token = this.#compileTermAndPush(this.#compilePowTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles a power operator subexpression.
     */
    #compilePowTerm(token: OptToken): OptToken {
        token = this.#compileUnaryTerm(token);
        while (isOperatorToken(token, "pow")) {
            token = this.#compileTermAndPush(this.#compileUnaryTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles a unary prefix/postfix operator subexpression.
     */
    #compileUnaryTerm(token: OptToken): OptToken {
        if (isOperatorToken(token, "upre")) {
            switch (token.value) {
                case "add": token = new OperatorToken("uplus"); break;
                case "sub": token = new OperatorToken("uminus"); break;
                default: break;
            }
            token = this.#compileTermAndPush(this.#compileUnaryTerm, token, 1);
        } else {
            token = this.#compileListTerm(token);
        }
        while (isOperatorToken(token, "upost")) {
            this.#pushOperator(token, 1);
            token = this.#getNextToken();
        }
        return token;
    }

    /**
     * Compiles a list operator subexpression.
     */
    #compileListTerm(token: OptToken): OptToken {
        token = this.#compileIntersectTerm(token);
        while (token && (isOperatorToken(token, "list") || (!this.#stopAtSep && (token.type === "sep")))) {
            if (token.type === "sep") { token = new OperatorToken("list"); }
            token = this.#compileTermAndPush(this.#compileIntersectTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles an intersection operator subexpression.
     */
    #compileIntersectTerm(token: OptToken): OptToken {
        token = this.#compileRangeTerm(token);
        while (isOperatorToken(token, "isect")) {
            token = this.#compileTermAndPush(this.#compileRangeTerm, token, 2);
        }
        return token;
    }

    /**
     * Compiles a range operator subexpression.
     */
    #compileRangeTerm(token: OptToken): OptToken {
        token = this.#compileOperand(token);
        while (isOperatorToken(token, "range")) {
            token = this.#compileTermAndPush(this.#compileOperand, token, 2);
        }
        return token;
    }

    /**
     * Compiles a single operand or function call in a subexpression.
     */
    #compileOperand(token: OptToken): OptToken {

        // an operand must exist
        if (!token) {
            throwMissing("unexpected end of formula");
        }

        // bug 33526: tokens must contain valid sheet references
        if ((token instanceof SheetRefToken) && token.hasSheetError()) {
            throwUnexpected("invalid sheet name");
        }

        // process all tokens by type
        switch (token.type) {

            // push scalar and matrix literals, referencs, defined names, and tables as operand
            case "lit":
            case "mat":
            case "ref":
            case "name":
            case "table":
                this.#pushOperand(token);
                return this.#getNextToken();

            // opening parenthesis: compile a subexpression up to the closing parenthesis
            case "open":
                return this.#compileParentheses();

            // function or macro call: compile parameters enclosed in parentheses
            case "func":
            case "macro":
                return this.#compileFunction(token);

            default:
                throwUnexpected("operand expected");
        }
    }

    /**
     * Compiles a complete subexpression in parentheses.
     */
    #compileParentheses(): OptToken {

        // start with the token following the opening parentheses
        const token = this.#compileExpression(this.#requireNextToken());

        // accept missing closing parenthesis at end of formula
        if (!token) {
            this.#missingClose += 1;
        } else if (token.type !== "close") {
            throwUnexpected("closing parenthesis expected");
        }
        return this.#getNextToken();
    }

    /**
     * Compiles a complete function call.
     */
    #compileFunction(funcToken: BaseToken): OptToken {

        // current token
        let token: OptToken;
        // current state of the function processor
        let state = FuncState.START;
        // number of parameters in the function
        let params = 0;

        while (state !== FuncState.END) {
            switch (state) {

                case FuncState.START:
                    // function start: next token must be an opening parenthesis
                    token = this.#requireNextToken();
                    if (token.type !== "open") { throwUnexpected("opening parenthesis expected"); }
                    state = FuncState.OPEN;
                    break;

                case FuncState.OPEN:
                    // opening parenthesis: a closing parenthesis may follow directly
                    token = this.#getNextToken();
                    // accept missing closing parenthesis after opening parenthesis at end of formula
                    if (!token) { this.#missingClose += 1; }
                    state = (!token || (token.type === "close")) ? FuncState.CLOSE : FuncState.PARAM;
                    break;

                case FuncState.PARAM:
                    // start of parameter subexpression
                    params += 1;
                    // accept missing closing parenthesis after separator at end of formula
                    if (!token) { this.#missingClose += 1; }
                    if (!token || token.isType("sep", "close")) {
                        // empty parameter, e.g. in SUM(1,,2)
                        this.#pushOperand(new ScalarToken(null));
                    } else {
                        // compile the parameter subexpression (stop at next separator token)
                        token = this.#compileExpression(token, true);
                        // accept missing closing parenthesis after parameter at end of formula
                        if (!token) {
                            this.#missingClose += 1;
                        } else if (!token.isType("sep", "close")) {
                            throwUnexpected("parameter separator expected");
                        }
                    }
                    state = (token && (token.type === "sep")) ? FuncState.SEP : FuncState.CLOSE;
                    break;

                case FuncState.SEP:
                    // parameter separator: a valid token must follow
                    token = this.#getNextToken();
                    state = FuncState.PARAM;
                    break;

                case FuncState.CLOSE:
                    // closing parenthesis: finalize the function call
                    this.#pushOperator(funcToken, params);
                    state = FuncState.END;
                    break;
            }
        }

        return this.#getNextToken();
    }
}

// class FormulaCompiler ======================================================

/**
 * Compiles arrays of formula tokens to a tree structure. This is needed as
 * preparation for interpreting the formula in order to get its current result.
 * All tokens that are not necessary for interpretation will be removed, e.g.
 * whitespace and parentheses.
 *
 * Example: The formula `=2+3*(4+5)` has been tokenized to the token array
 *
 *  `2, PLUS, 3, TIMES, OPEN, 4, PLUS, 5, CLOSE`.
 *
 * The compiler will convert this token array to the tree structure
 *
 *  `PLUS (2, TIMES (3, PLUS (4, 5)))`.
 */
export class FormulaCompiler extends ModelObject<SpreadsheetModel> {

    // public methods ---------------------------------------------------------

    /**
     * Compiles the passed tokens from infix notation to a tree structure.
     * Removes all tokens that are not important for interpreting the formula,
     * e.g. white-space tokens, parentheses and parameter separators of
     * function calls, and all other regular parentheses.
     *
     * @param tokens
     *  The array of formula tokens, in infix notation.
     *
     * @returns
     *  A result descriptor for the compiled token tree.
     */
    @formulaLogger.profileMethod("$badge{FormulaCompiler} compileTokens")
    compileTokens(tokens: readonly BaseToken[]): CompilerResult {

        // create a new compiler instance for each invocation (for reentrancy)
        const instance = new CompilerInstance(this.docModel, tokens);
        return instance.compileTokens();
    }
}
