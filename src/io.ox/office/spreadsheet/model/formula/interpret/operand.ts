/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

import { type ScalarType, ErrorCode, isErrorCode, scalarToString } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";

import { type ContextType, MAX_REF_LIST_SIZE, InternalErrorCode, isInternalError, throwEngineError } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import type { Complex } from "@/io.ox/office/spreadsheet/model/formula/utils/complex";
import { type ScalarMatrix, getMatrixAddress, ensureMatrixDim, Matrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import type { ConvertToRangeOptions, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";

// types ======================================================================

/**
 * All data types that can be used as scalar formula operands (e.g. operator or
 * function parameters, or return value).
 * - Scalar values (numbers, strings, booleans, `null`).
 * - An error code literal (instance of `ErrorLiteral`).
 * - A `bigint` (will be converted to a floating-point number immediately).
 * - A `Date` (will be converted to a floating-point number immediately).
 * - An instance of `Complex` (will be converted to a string immediately).
 */
export type OperandValType = ScalarType | bigint | Date | Complex;

/**
 * All data types that can be used as matrix formula operands (e.g. operator or
 * function parameters, or return value).
 */
export type OperandMatType = Matrix<OperandValType>;

/**
 * All data types that can be used as reference formula operands (e.g. operator
 * or function parameters, or return value).
 * - Single 3D cell range address (instance of `Range3D`).
 * - Array of 3D cell range addresses (instance of `Range3DArray`).
 */
export type OperandRefType = Range3D | Range3DArray;

/**
 * All data types that can be used as formula operands (e.g. operator or
 * function parameters, or return value).
 * - Scalar values (`OperandValType`).
 * - A scalar matrix (`OperandMatType`).
 * - Cell range addresses (`OperandRefType`).
 */
export type OperandAnyType = OperandValType | OperandMatType | OperandRefType;

/**
 * All data types that will be carried by and returned from formula operands.
 * - Scalar values (`ScalarType`) but not `Date` or `Complex`.
 * - A scalar matrix (instance of the class `Matrix<ScalarType>`).
 * - A cell range list (instance of the class `Range3DArray`) but not a single
 *   cell range (`Range3D`).
 */
export type OperandRawType = ScalarType | ScalarMatrix | Range3DArray;

/**
 * Configuration options for the class `Operand`.
 */
export interface OperandConfig {

    /**
     * A parsed number format associated with the value contained in the
     * operand. If omitted, the operand does not have a special number format
     * associated to it (the original number format will not change).
     */
    format?: ParsedFormat;

    /**
     * The URL of a hyperlink that will be part of the operand value. The URL
     * will be carried through the formula result, and can be used by the user
     * interface to provide a clickable link.
     */
    url?: string | null;
}

// private types --------------------------------------------------------------

/**
 * Storage array elements for class `OperandResolver`.
 */
interface OperandEntry {
    operand?: Operand;
    resolve(): Operand;
}

// class Operand ==============================================================

/**
 * Represents a single operand stored in the operand stack of a formula token
 * interpreter.
 *
 * @param context
 *  The context object providing a connection to the document model.
 *
 * @param value
 *  The value to be stored as operand. Can be any value of `OperandAnyType`, or
 *  another instance of this class for copy construction.
 *
 * @param [config]
 *  Configuration parameters for the new operand instance. These settings will
 *  override the settings copied from another `Operand` instance passed in as
 *  second argument.
 */
export class Operand {

    // properties -------------------------------------------------------------

    readonly type: ContextType;
    readonly format: ParsedFormat | null;
    readonly url: string | null;

    readonly #context: FormulaContext;
    readonly #value: OperandRawType = null;

    // constructor ------------------------------------------------------------

    constructor(context: FormulaContext, value: OperandAnyType | Operand, config?: OperandConfig) {

        this.#context = context;
        this.format = config?.format ?? null;
        this.url = config?.url ?? null;

        // copy construction (options for format and URL passed to constructor win over the operand's settings)
        if (value instanceof Operand) {
            this.type = value.type;
            this.#value = value.#value;
            if (!this.format) { this.format = value.format; }
            if (this.url === null) { this.url = value.url; }
            return;
        }

        // cell range passed: convert to single-element array (no further validation needed)
        if (value instanceof Range3D) {
            this.type = "ref";
            this.#value = new Range3DArray(value);
            return;
        }

        // cell range addresses passed: validate size
        if (value instanceof Range3DArray) {
            if (value.empty()) {
                value = ErrorCode.NULL;
                // continue below, operand becomes an error code (a scalar value)
            } else if (value.length > MAX_REF_LIST_SIZE) {
                value = InternalErrorCode.UNSUPPORTED;
                // continue below, operand becomes an error code (a scalar value)
            } else {
                this.type = "ref";
                this.#value = value;
                return;
            }
        }

        // matrix passed: validate matrix size, and all values in the matrix
        if (value instanceof Matrix) {
            ensureMatrixDim(value.dim());
            // narrow elements to `ScalarType` (no dates, no complex numbers)
            (value as Matrix<OperandValType>).transform(element => context.validateValue(element));
            this.type = "mat";
            this.#value = value as ScalarMatrix;
            return;
        }

        // otherwise: scalar value (as last check, preceding code may have changed the value)
        this.type = "val";
        this.#value = context.validateValue(value);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this operand represents the empty value (e.g. the second
     * operand in the formula `=SUM(1,,2)` is empty, and the value resolved
     * from a blank cell is empty).
     *
     * @returns
     *  Whether this operand represents the empty value.
     */
    isEmpty(): boolean {
        return this.#value === null;
    }

    /**
     * Returns the dimension (width and height) of this operand.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The dimension of this operand. The dimension of a scalar value is 1x1.
     *
     * @throws
     *  The error code `#REF!` (or another error code according to the passed
     *  options), if this is a reference operand that cannot be resolved to a
     *  single cell range address.
     */
    getDimension(options?: ConvertToRangeOptions): Dimension {

        // return dimensions of scalar and matrix
        switch (this.type) {

            case "val":
                return new Dimension(1, 1);

            case "mat":
                return (this.#value as ScalarMatrix).dim();

            case "ref": {
                // extract single cell range address (throws on error)
                const range = this.#context.convertToRange(this.#value as Range3DArray, options);
                return Dimension.createFromRange(range);
            }

            default:
                throwEngineError("Operand.getDimension(): unknown operand type");
        }
    }

    /**
     * Returns the unconverted value of this operand.
     *
     * @returns
     *  The unconverted value of this operand.
     *
     * @throws
     *  All internal error codes carried by this instance.
     */
    getRawValue(): OperandRawType {
        // immediately throw internal error codes
        return isInternalError(this.#value) ? this.#value.throw() : this.#value;
    }

    /**
     * Converts this operand to a scalar value.
     *
     * @param matRow
     *  The relative row index, used to resolve a scalar value in a matrix
     *  context. Specifies the row of a matrix, or the relative row index in a
     *  reference operand. If set to `null`, the scalar will be resolved in
     *  value context (top-left cell of a matrix, cell projection from a
     *  reference operand).
     *
     * @param matCol
     *  The relative column index, used to resolve a scalar value in a matrix
     *  context. Specifies the column of a matrix, or the relative column index
     *  in a reference operand. If set to `null`, the scalar will be resolved
     *  in value context (top-left cell of a matrix, cell projection from a
     *  reference operand).
     *
     * @returns
     *  The scalar value represented by this operand. Returns an existing
     *  scalar value, or the specified element of a matrix, or the contents of
     *  the specified cell from a cell reference (only if it does not cover the
     *  reference cell of the formula context passed to the constructor).
     *
     * @throws
     *  An error code if conversion to a scalar value fails.
     *  - All internal error codes carried by this instance.
     *  - The internal error code `#CIRCULAR`, if the operand results in a
     *    circular reference.
     *  - The error code `#VALUE!`, if the reference contains more than one
     *    cell range, or if the cell range cannot be resolved to a single value
     *    according to the reference cell.
     */
    getScalar(matRow: number | null, matCol: number | null): ScalarType {

        // immediately throw internal error codes
        if (isInternalError(this.#value)) { throw this.#value; }

        // return scalar value, and an element of a matrix
        switch (this.type) {

            case "val":
                return this.#value as ScalarType;

            case "mat": {
                const matrix = this.#value as ScalarMatrix;
                return matrix.get(matRow || 0, matCol || 0, ErrorCode.NA);
            }

            case "ref": {
                // extract single cell range address (throws on error)
                const range3d = this.#context.convertToValueRange(this.#value as Range3DArray);
                const sheetModel = this.#context.getSheetModel(range3d.sheet1);

                // pick value from a single cell
                if (range3d.single()) {
                    return this.#context.getCellValue(sheetModel, range3d.a1);
                }

                // in matrix context, return the cell relative to the position of the matrix element
                if ((matRow !== null) && (matCol !== null)) {
                    const address = getMatrixAddress(range3d, matRow, matCol);
                    return this.#context.getCellValue(sheetModel, address);
                }

                // the target reference cell of the formula context
                const refAddress = this.#context.getTargetAddress();

                // pick matching cell from a column range (row interval must cover the reference cell)
                if (range3d.singleCol()) {
                    if (!range3d.containsRow(refAddress.r)) { throw ErrorCode.VALUE; }
                    return this.#context.getCellValue(sheetModel, new Address(range3d.a1.c, refAddress.r));
                }

                // pick matching cell from a row range (column interval must cover the reference cell)
                if (range3d.singleRow()) {
                    if (!range3d.containsCol(refAddress.c)) { throw ErrorCode.VALUE; }
                    return this.#context.getCellValue(sheetModel, new Address(refAddress.c, range3d.a1.r));
                }

                // range is not left of, right of, above, or below the reference cell
                throw ErrorCode.VALUE;
            }

            default:
                throwEngineError("Operand.getScalar(): unknown operand type");
        }
    }

    /**
     * Converts this operand to a matrix.
     *
     * @returns
     *  The matrix represented by this operand. Returns an existing matrix, or
     *  a scalar value packed into a 1x1 matrix, or the contents of the
     *  referenced cells packed into a matrix. The reference must consist of a
     *  single cell range address that does not cover the reference cell, and
     *  the range must not exceed specific limits in order to prevent
     *  performance problems.
     *
     * @throws
     *  An error code if conversion to a matrix fails.
     *  - All internal error codes carried by this instance.
     *  - `#UNSUPPORTED`, if the resulting matrix would be too large.
     *  - `#CIRCULAR`, if the operand results in a circular reference.
     *  - The error code `#VALUE!`, if the operand contains a reference with
     *    more than one cell range address.
     */
    getMatrix(): ScalarMatrix {

        // immediately throw internal error codes
        if (isInternalError(this.#value)) { throw this.#value; }

        // pack scalar value into a 1x1 matrix, or return an existing matrix
        switch (this.type) {

            case "val":
                return new Matrix(this.#value as ScalarType);

            case "mat":
                return this.#value as ScalarMatrix;

            case "ref": {
                // extract single cell range address (throws on error), and return the matrix
                const range3d = this.#context.convertToValueRange(this.#value as Range3DArray);
                const sheetModel = this.#context.getSheetModel(range3d.sheet1);
                return this.#context.getCellMatrix(sheetModel, range3d.toRange());
            }

            default:
                throwEngineError("Operand.getMatrix(): unknown operand type");
        }
    }

    /**
     * Converts this operand to an unresolved cell range list.
     *
     * @returns
     *  The unresolved cell range list contained in this operand.
     *
     * @throws
     *  All literal error codes carried by this operand (regular error codes,
     *  and internal error codes); or the error code `#VALUE!`, if this operand
     *  is not reference type.
     */
    getRanges(): Range3DArray {

        // throw #VALUE! error for scalar values and matrixes (but throw original error codes)
        switch (this.type) {

            case "val":
                throw isErrorCode(this.#value) ? this.#value : ErrorCode.VALUE;

            case "mat":
                throw ErrorCode.VALUE;

            case "ref":
                // return the value (always an instance of Range3DArray)
                return this.#value as Range3DArray;

            default:
                throwEngineError("Operand.getRanges(): unknown operand type");
        }
    }

    /**
     * Returns the string representation of this operand for debug logging.
     */
    toString(): string {
        let text = (this.type === "val") ? scalarToString(this.#value) : String(this.#value);
        if (this.format) { text += ` [${this.format.category}]`; }
        return text;
    }
}

// class OperandResolver ======================================================

/**
 * A helper class used to store the compiler token nodes for the operands of an
 * operator or function call.
 *
 * Supports resolving the operands from the token nodes on demand, e.g. for
 * lazy operands. Will be passed to instances of class `FormulaContext` when
 * resolving the result of a specific operator or function, in order to be able
 * to reuse the same formula context instance for all operators and functions
 * of an entire formula.
 */
export class OperandResolver {

    // properties -------------------------------------------------------------

    readonly #entries: OperandEntry[] = [];

    // public methods ---------------------------------------------------------

    push(resolve: FuncType<Operand>): void {
        this.#entries.push({ resolve });
    }

    size(): number {
        return this.#entries.length;
    }

    get(index: number): Operand | null {
        return this.#entries[index]?.operand ?? null;
    }

    resolve(index: number): Operand | null {
        const data = this.#entries[index];
        return data ? (data.operand ??= data.resolve()) : null;
    }
}
