/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ScalarType, ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import { Matrix, NumberMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import type { AggregateAggregatorFn, AggregateFinalizerFn, ScalarFilterMatcher, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";

const { floor } = Math;

// types ======================================================================

/**
 * An aggregation callback function for filtering numbers.
 *
 * @param intermediate
 *  The current intermediate result. On first invocation, this is the number
 *  passed in the parameter `initial`.
 *
 * @param value
 *  The new number to be combined with the intermediate result.
 *
 * @returns
 *  The new intermediate result which will be passed to the next invocation of
 *  this callback function, or may throw an error code.
 */
export type FilterAggregatorFunc = (this: FormulaContext, intermediate: number, value: number) => number;

/**
 * A callback function invoked at the end to convert the intermediate
 * result (returned by the last invocation of the aggregation callback
 * function) to the actual function result.
 *
 * @param intermediate
 *  The last intermediate result to be converted to the result of the
 *  implemented function.
 *
 * @param count
 *  The count of all numbers the aggregation callback function has been
 *  invoked for.
 *
 * @returns
 *  The final numeric result of the spreadsheet function. Alternatively, may
 *  throw an error code.
 */
export type FilterFinalizerFunc = (this: FormulaContext, intermediate: number, count: number) => number;

// private types --------------------------------------------------------------

type FilterMatcherMatrix = Matrix<ScalarFilterMatcher>;

// class FilterAggregator =====================================================

/**
 * An instance of this class implements numeric aggregation of function
 * parameters based on some filter criteria.
 *
 * @param context
 *  The formula context providing the parameters of a function used for
 *  filtering and aggregation.
 *
 * @param filterIndex
 *  The zero-based index of the first function parameter contained in the
 *  passed formula context representing a filter criterion.
 */
export class FilterAggregator {

    /** Reference to the current formula context. */
    readonly #context: FormulaContext;

    /** Whether to return a matrix result. */
    readonly #matMode: boolean;

    /** Dimension of the matrixes used by this instance. */
    readonly #matDim: Dimension;

    /** The number of filter ranges. */
    readonly #rangeCount: number;

    /** The filter ranges. */
    readonly #filterRanges: Range3D[] = [];

    /** Filter matcher matrixes (one matrix per filter range). */
    readonly #filterMatchers: FilterMatcherMatrix[] = [];

    /** Fall-back matcher for missing elements in filter matcher matrixes (matches #N/A error code only). */
    readonly #fallbackMatcher: ScalarFilterMatcher;

    // constructor ------------------------------------------------------------

    constructor(context: FormulaContext, filterIndex: number) {

        this.#context = context;
        this.#matMode = context.isMatrixMode();
        this.#matDim = context.getMatrixDim() ?? new Dimension(1, 1);
        this.#rangeCount = floor((context.getOperandCount() - filterIndex) / 2);
        this.#fallbackMatcher = context.createFilterMatcher(ErrorCode.NA);

        for (let i = 0; i < this.#rangeCount; i += 1, filterIndex += 2) {

            // extract the filter range
            const filterRange = context.getOperand(filterIndex, "ref:val");
            this.#filterRanges.push(filterRange);

            // check that all filter ranges have equal size (throw a #VALUE! error if not)
            context.checkRangeDims(this.#filterRanges[0], filterRange);

            // extract the filter criterion (or a matrix with filter criteria in matrix context;
            // but always use a criteria matrix in value context for simpler implementation)
            const criteriaMatrix = this.#matDim ?
                context.getOperand(filterIndex + 1, "mat:any") :
                new Matrix(context.getOperand(filterIndex + 1, "val:any"));

            // create and store the matrix with filter matcher predicates
            const filterMatchers = criteriaMatrix.map(criterion => context.createFilterMatcher(criterion));
            this.#filterMatchers.push(filterMatchers);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the number of filter ranges extracted from the operands passed
     * to the constructor.
     *
     * @returns
     *  The number of filter ranges.
     */
    getRangeCount(): number {
        return this.#rangeCount;
    }

    /**
     * Returns the specified filter range extracted from the operands passed to
     * the constructor.
     *
     * @param index
     *  The array index of the filter range. MUST be a valid array index.
     *
     * @returns
     *  The specified filter range.
     */
    getFilterRange(index: number): Range3D {
        return this.#filterRanges[index] || ErrorCode.NA.throw();
    }

    /**
     * Returns the number of matching cells in the filter ranges according to
     * the filter criteria.
     *
     * @returns
     *  The number of matching cells in the filter ranges according to the
     *  filter criteria; or a matrix with the counts of all matching cells, if
     *  the formula context is in matrix formula mode.
     */
    countMatches(): number | NumberMatrix {

        // create a scalar iterator for each filter range
        const iterators = this.#filterRanges.map(filterRange =>
            this.#context.createScalarIterator(filterRange, {
                acceptErrors: true, // error codes can be matched by the filter criterion
                multiRange: false, // do not accept multi-range references
                multiSheet: false  // do not accept multi-sheet references
            })
        );

        // create a parallel iterator that visits all entries with at least one non-blank value
        const iterator = this.#context.createParallelIterator(iterators);

        // the number of iterator stops containing at least one non-blank cell
        let valueCount = 0;
        // the number of iterator stops matching the filter criteria
        const matchCounts = this.#createMatrix(0);

        // visit all entries in the filter ranges with at least one non-blank cell
        for (const opsState of iterator) {

            // count all iterator stops, needed to calculate the completely blank entries
            valueCount += 1;

            // count the iterator stops that match the filter criteria
            matchCounts.transform((matchCount, row, col) => matchCount + (this.#testValues(row, col, opsState.value) ? 1 : 0));
        }

        // Check whether completely blank entries will be counted too. This is the case if all
        // filter criteria will accept blank cells. For performance, the scalar iterators created
        // above will skip blank cells in its own filter range. Therefore, if the cells at a
        // specific offset are blank in all filter ranges, that offset will be skipped by the
        // parallel iterator. The number of completely blank entries that need to be counted is
        // the difference between number of cells in a filter range (all filter ranges have equal
        // size) and the total number of entries visited by the parallel iterator.
        const blankCount = this.getFilterRange(0).cells() - valueCount;
        matchCounts.transform((matchCount, row, col) => matchCount + (this.#testValue(row, col, null) ? blankCount : 0));

        // return the matched counts (convert 1x1 matrix back to scalar if not in matrix context)
        return this.#matMode ? matchCounts : matchCounts.get(0, 0);
    }

    /**
     * Aggregates the numbers in the passed data range according to the filter
     * ranges and filter criteria of this instance.
     *
     * @param dataRange
     *  The address of the cell range whose numbers will be aggregated.
     *
     * @param initial
     *  The initial intermediate result, passed to the first invocation of the
     *  aggregation callback function.
     *
     * @param aggregate
     *  The aggregation callback function.
     *
     * @param [finalize]
     *  The finalizer callback function.
     *
     * @returns
     *  The aggregated function result; or a matrix with aggregated function
     *  results, if the formula context is in matrix formula mode.
     */
    aggregateNumbers(
        dataRange: Range3D,
        initial: number,
        aggregate: FilterAggregatorFunc,
        finalize?: FilterFinalizerFunc
    ): number | NumberMatrix {

        // the data range must have the same size as the filter ranges
        this.#context.checkRangeDims(dataRange, this.getFilterRange(0));

        // shortcut to the formula context
        const context = this.#context;
        // the initial value for aggregation (number or matrix)
        const initialValues = this.#createMatrix(initial);
        // the number of iterator stops matching the filter criteria
        const matchCounts = this.#createMatrix(0);

        // resolve sheet models before aggregation (throws `#REF!` errors for invalid ranges)
        const sheetModels = this.#filterRanges.map(filterRange => context.getSheetModel(filterRange.sheet1));

        // aggregator that processes only the numbers that match the filter criteria
        const aggregateImpl: AggregateAggregatorFn<NumberMatrix, number> = (intermediate, value, _index, offset) => {

            // pluck the cell values from all filter ranges
            const filterValues = this.#filterRanges.map((filterRange, index) =>
                context.getCellValue(sheetModels[index], filterRange.addressAt(offset))
            );

            // call the aggregate() callback function for all matching result values in the matrix
            return intermediate.transform((elem, row, col) => {
                if (this.#testValues(row, col, filterValues)) {
                    matchCounts.add(row, col, 1);
                    return aggregate.call(context, elem, value);
                }
                return elem;
            });
        };

        // finalizer that passed the actual match count to the external finalizer
        const finalizeImpl: AggregateFinalizerFn<number | NumberMatrix, NumberMatrix> = intermediate => {

            // call the finalize() callback function for all matching result values in the matrix
            if (finalize) {
                intermediate.transform((elem, row, col) => {
                    const matchCount = matchCounts.get(row, col)!;
                    return finalize.call(context, elem, matchCount);
                });
            }

            // return the result value (convert 1x1 matrix back to scalar if not in matrix context)
            return this.#matMode ? intermediate : intermediate.get(0, 0);
        };

        // aggregate all numbers of the data range that match the criteria
        return context.aggregateNumbers([dataRange], initialValues, aggregateImpl, finalizeImpl, {
            refMode: "skip",    // skip all strings and boolean values
            multiRange: false,  // do not accept multi-range references
            multiSheet: false   // do not accept multi-sheet references
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a new matrix with the matrix result dimension, filled with the
     * specified number.
     *
     * @param value
     *  The number to be filled into all elements of matrix.
     *
     * @returns
     *  The new matrix instance.
     */
    #createMatrix(value: number): NumberMatrix {
        return new NumberMatrix(this.#matDim.rows, this.#matDim.cols, value);
    }

    /**
     * Returns whether the passed value matches the criterion at the specified
     * matrix position.
     *
     * @param matchers
     *  A matrix with filter matcher callback functions as elements.
     *
     * @param row
     *  The row index in the filter matcher matrix.
     *
     * @param col
     *  The column index in the filter matcher matrix.
     *
     * @param value
     *  The scalar value to be matched against the filter criterion stored at
     *  the specified matrix position.
     *
     * @returns
     *  Whether the passed value matches the filter criterion.
     */
    #matchValue(matchers: FilterMatcherMatrix, row: number, col: number, value: ScalarType): boolean {
        return matchers.get(row, col, this.#fallbackMatcher)(value);
    }

    /**
     * Returns whether the passed value matches all criteria at the specified
     * matrix position.
     *
     * @param row
     *  The row index in the criteria matrixes.
     *
     * @param col
     *  The column index in the criteria matrixes.
     *
     * @param value
     *  The scalar value to be matched against all filter criteria stored at
     *  the specified matrix position.
     *
     * @returns
     *  Whether the passed value matches all filter criteria.
     */
    #testValue(row: number, col: number, value: ScalarType): boolean {
        return this.#filterMatchers.every(matchers => this.#matchValue(matchers, row, col, value));
    }

    /**
     * Returns whether the passed values match the respective criteria at the
     * specified matrix position.
     *
     * @param row
     *  The row index in the criteria matrixes.
     *
     * @param col
     *  The column index in the criteria matrixes.
     *
     * @param values
     *  The scalar values to be matched against the respective filter criteria
     *  stored at the specified matrix position. MUST have the same number of
     *  elements as filter ranges exist.
     *
     * @returns
     *  Whether the passed values match all filter criteria.
     */
    #testValues(row: number, col: number, values: ScalarType[]): boolean {
        return this.#filterMatchers.every((matchers, index) => this.#matchValue(matchers, row, col, values[index]));
    }
}
