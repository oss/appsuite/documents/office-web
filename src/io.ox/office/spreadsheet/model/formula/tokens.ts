/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { FixedToken } from "@/io.ox/office/spreadsheet/model/formula/token/fixedtoken";
import { OperatorToken } from "@/io.ox/office/spreadsheet/model/formula/token/operatortoken";

// re-exports =================================================================

export * from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/fixedtoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/parenthesistoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/separatortoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/operatortoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/functiontoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/scalartoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/matrixtoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/sheetreftoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/referencetoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/namereftoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/definednametoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/macrotoken";
export * from "@/io.ox/office/spreadsheet/model/formula/token/tabletoken";

// public functions ===========================================================

/**
 * Returns whether the passed value is a specific operator token.
 *
 * @param token
 *  The token to be checked.
 *
 * @param op
 *  The expected operator.
 *
 * @returns
 *  Whether the passed value is an operator token with the passed operator.
 */
export function isOpToken(token: unknown, op: string): token is OperatorToken {
    return (token instanceof OperatorToken) && (token.value === op);
}

/**
 * Returns whether the passed value is a whitespace token.
 *
 * @param token
 *  The token to be checked.
 *
 * @returns
 *  Whether the passed value is a whitespace token.
 */
export function isWsToken(token: unknown): token is FixedToken {
    return (token instanceof FixedToken) && (token.type === "ws");
}
