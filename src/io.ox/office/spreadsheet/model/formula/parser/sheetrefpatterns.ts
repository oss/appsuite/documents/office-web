/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, re } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import type { RefSheetOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { SheetRef, SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";

// types ======================================================================

/**
 * Options for parsing references with sheets in formula expressions.
 */
export interface ParseSheetRefOptions extends RefSheetOptions {

    /**
     * If set to `true`, and the option `refSheet` has been set, all cell
     * references without a sheet reference, and all references to existing
     * sheet-local names will be extended with a sheet reference pointing to
     * the sheet contained in this option. Default value is `false`.
     *
     * Example: The formula expression `A1+global+local` extended to the first
     * sheet may become `Sheet1!A1+global+Sheet1!local`.
     */
    extendSheet?: boolean;
}

/**
 * Context for document and formula specific settings when using methods of
 * globally cached `SheetRefPatterns` instances.
 */
export interface SheetRefPatternsContext {
    docAccess: IDocumentAccess;
    refSheet: number;
    extendSheet: boolean;
}

// private types --------------------------------------------------------------

interface SheetNamePatternOptions {
    /** Whether to add a capturing group for external document reference. */
    extern?: boolean;
    /** Whether to add a capturing group for the #REF! error. */
    error?: boolean;
    /** Whether to generate an RE pattern for a range of sheets. */
    range?: boolean;
}

// constants ==================================================================

/**
 * RE capturing group pattern for the reference of an external document.
 *
 * Examples:
 * - `[1]` in the expression `[1]Sheet1!A1`.
 *
 * Capturing groups:
 * - The index of the external document, without brackets.
 */
const DOC_REF = re.group("\\[" + re.capture("\\d+") + "\\]");

// class SheetRefPatterns =====================================================

/**
 * Base class for configurations of different sheet reference syntaxes used in
 * formula expressions.
 */
export abstract class SheetRefPatterns {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `SheetRefPatterns`.
     */
    static create(formulaResource: FormulaResource, localized: boolean): SheetRefPatterns {
        switch (formulaResource.fileFormat) {
            case FileFormatType.OOX: return new SheetRefPatternsOOX(formulaResource, localized);
            case FileFormatType.ODF: return new SheetRefPatternsOF(formulaResource, localized);
        }
    }

    // properties -------------------------------------------------------------

    /**
     * RE pattern for the separator between sheet names and cell addresses,
     * defined names, or function names.
     */
    readonly SHEET_SEP: string;

    /**
     * RE capturing group pattern for the reference of an external document.
     *
     * Capturing groups depend on subclass. The number of capturing groups is
     * available in property `DOC_GROUPS`.
     */
    abstract readonly DOC_PREFIX: string;

    /**
     * RE capturing group pattern for a single sheet name with trailing sheet
     * separator, or a broken sheet reference (e.g. after deleting the
     * referenced sheet).
     *
     * Capturing groups depend on subclass. The number of capturing groups is
     * available in property `SHEET_GROUPS`.
     */
    abstract readonly SHEET_PREFIX: string;

    /**
     * RE capturing group pattern for a range of sheet names with trailing
     * sheet separator.
     *
     * Capturing groups depend on subclass. The number of capturing groups is
     * available in property `SHEETS_GROUPS`.
     */
    abstract readonly SHEETS_PREFIX: string;

    /**
     * Number of capturing groups in the `DOC_PREFIX` pattern.
     */
    abstract readonly DOC_GROUPS: number;

    /**
     * Number of capturing groups in the `SHEET_PREFIX` pattern.
     */
    abstract readonly SHEET_GROUPS: number;

    /**
     * Number of capturing groups in the `SHEETS_PREFIX` pattern.
     */
    abstract readonly SHEETS_GROUPS: number;

    /**
     * A regular expression that matches a simple sheet name, or a range of
     * simple sheet names, that do not need to be enclosed in apostrophes.
     * Supports an optional leading reference to an external file (also simple
     * name only), enclosed in brackets.
     *
     * Examples:
     * - Single sheet: `Sheet1`.
     * - Sheet range: `Sheet1:Sheet2`.
     * - Sheet with simple file name: `[file1.xlsx]Sheet1`.
     * - Sheet range with file path: `[path\to\file1.xlsx]Sheet1:Sheet2`.
     *
     * Capturing groups:
     * 1. The name of the external file (the part inside the brackets), may be
     *   `undefined`.
     * 2. The first sheet name.
     * 3. The second sheet name, may be `undefined`.
     */
    readonly RE_SIMPLE_EXT_SHEET_NAME: RegExp;

    /** Name of the #REF! error code. */
    readonly #REF_ERROR: string;
    /** Capturing group for a simple sheet name. */
    readonly #SIMPLE_NAME: string;
    /** Capturing group for a complex sheet name. */
    readonly #COMPLEX_NAME: string;

    // constructor ------------------------------------------------------------

    protected constructor(formulaResource: FormulaResource, openFormula: boolean, localized: boolean) {

        // character classes
        const FILE_SIMPLE_CHAR_CLASS    = re.class("\\w", ".", "\\\\", "\\xa1-\\u2027", "\\u202a-\\ud7ff", "\\ue000-\\uffff");
        const SHEET_LEADING_CHAR_CLASS  = re.class("a-z", "_", "\\xa1-\\u2027", "\\u202a-\\ud7ff", "\\ue000-\\uffff");
        const SHEET_SIMPLE_CHAR_CLASS   = re.class("\\w", !openFormula && ".", "\\xa1-\\u2027", "\\u202a-\\ud7ff", "\\ue000-\\uffff");
        const SHEET_COMPLEX_CHAR_CLASS  = re.class.neg("\\x00-\\x1f", "\\x80-\\x9f", "\\[", "\\]", "'", "*", "?", ":", "/", "\\\\");

        // capturing patterns
        this.#REF_ERROR = re.capture(re.escape(formulaResource.errorMap.getName("REF", localized)!));
        // bug 54388: ODF sheet names may start with digits, but a non-digit character MUST follow
        this.#SIMPLE_NAME = re.capture((openFormula ? "\\d?" : "") + SHEET_LEADING_CHAR_CLASS + SHEET_SIMPLE_CHAR_CLASS + "*");
        // DOCS-1642: Bug in Excel allows sheet names with trailing apostrophes.
        this.#COMPLEX_NAME = re.capture(SHEET_COMPLEX_CHAR_CLASS + "+" + re.group(re.group("''") + "+" + SHEET_COMPLEX_CHAR_CLASS + (openFormula ? "+" : "*")) + "*");

        // public RE patterns
        this.SHEET_SEP = (openFormula && !localized) ? "\\." : "!";

        // regular expressions
        const OPT_SIMPLE_FILE = re.group.opt("\\[" + re.capture(FILE_SIMPLE_CHAR_CLASS + "+") + "\\]");
        this.RE_SIMPLE_EXT_SHEET_NAME = new RegExp("^" + OPT_SIMPLE_FILE + this.#SIMPLE_NAME + re.group.opt(":" + this.#SIMPLE_NAME) + "$", "i");
    }

    // abstract methods -------------------------------------------------------

    /**
     * Returns a `SheetRefs` structure for the matches of a `DOC_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `DOC_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  A `SheetRefs` structure for the passed matches.
     */
    abstract parseDocRef(context: SheetRefPatternsContext, groups: string[], index: number): SheetRefs;

    /**
     * Returns a `SheetRefs` structure for the matches of a `SHEET_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `SHEET_PREFIX` pattern in the passed groups.
     *
     * @param error
     *  Whether to accept the #REF! error code in the matches following the
     *  sheet names.
     *
     * @returns
     *  The `SheetRefs` structure for the passed matches.
     */
    abstract parseSheetRef(context: SheetRefPatternsContext, groups: string[], index: number, error: boolean): SheetRefs;

    /**
     * Returns a `SheetRefs` structure for the matches of a `SHEETS_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `SHEETS_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  The `SheetRefs` structure for the passed matches.
     */
    abstract parseSheetRefs(context: SheetRefPatternsContext, groups: string[], index: number): SheetRefs;

    // protected methods ------------------------------------------------------

    /**
     * Returns an RE pattern for simple and complex sheet names.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The RE pattern needed to match a sheet name.
     */
    protected getSheetNamePattern(options?: SheetNamePatternOptions): string {
        const extDocPattern = options?.extern ? (DOC_REF + "?") : "";
        const simplePattern = extDocPattern + this.#SIMPLE_NAME + (options?.range ? (":" + this.#SIMPLE_NAME) : "");
        const complexPattern = "'" + extDocPattern + this.#COMPLEX_NAME + (options?.range ? (":" + this.#COMPLEX_NAME) : "") + "'";
        return re.group(simplePattern, complexPattern, options?.error && (extDocPattern + this.#REF_ERROR));
    }

    /**
     * Creates a `SheetRefs` structure for the passed simple or complex sheet
     * name.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param extDocName
     *  The index of an external document.
     *
     * @param simpleName
     *  A simple sheet name without any special characters that would cause to
     *  enclose the sheet name in apostrophes.
     *
     * @param complexName
     *  A complex sheet name with special characters that requires the sheet
     *  name to be enclosed in apostrophes.
     *
     * @param errorName
     *  The error code of a reference error, used a sheet has been deleted,
     *  e.g. in the formula `=#REF!A1`.
     *
     * @param abs
     *  Whether the sheet reference will be marked to be absolute.
     *
     * @returns
     *  The sheet reference structure. Its property "r2" will always be
     *  `undefined`.
     */
    protected createSheetRefs(context: SheetRefPatternsContext, extDocName: string, simpleName: string, complexName: string, errorName: string, abs: boolean): SheetRefs {

        // index of the external document (zero is the own document)
        const sheetRefs = extDocName ? this.parseDocRef(context, [extDocName], 0) : new SheetRefs();

        // create a single `SheetRef` object
        const r1 = sheetRefs.r1 =
            simpleName ? new SheetRef(simpleName, abs) :
            complexName ? new SheetRef(complexName.replace(/''/g, "'"), abs) :
            errorName ? new SheetRef(-1, abs) :
            undefined;

        // try to resolve sheet index in the own document
        if (!sheetRefs.isExtDoc() && r1 && is.string(r1.sheet)) {
            const sheetIndex = context.docAccess.getSheetIndex(r1.sheet);
            if (sheetIndex >= 0) { r1.sheet = sheetIndex; }
        }

        return sheetRefs;
    }
}

// class SheetRefPatternsOOX ==================================================

/**
 * Configuration settings and helper methods for sheet references in grammars
 * based on OOXML.
 */
class SheetRefPatternsOOX extends SheetRefPatterns {

    /**
     * RE capturing group pattern for the reference to an external document
     * with trailing sheet separator.
     *
     * Examples:
     * - `[1]!` in the expression `[1]!globalname`.
     *
     * Capturing groups:
     * 1. The document index (without brackets).
     */
    override readonly DOC_PREFIX: string;

    /**
     * RE capturing group pattern for a single sheet name with trailing sheet
     * separator, or a broken sheet reference after deleting the referenced
     * sheet, optionally with leading reference to an external document.
     *
     * Examples:
     * - `Sheet1` in the expression `Sheet1!A1`.
     * - `#REF!` in the expression `#REF!!A1`.
     * - `[1]Sheet2` in the expression `[1]Sheet2!!A1`.
     *
     * Capturing groups:
     * 1. The index of the external document (simple sheet name).
     * 2. The name of the sheet (simple sheet name).
     * 3. The index of the external document (complex sheet name).
     * 4. The name of the sheet (complex sheet name without apostrophes).
     * 5. The index of the external document (#REF! error code).
     * 6. The #REF! error code as sheet name.
     */
    override readonly SHEET_PREFIX: string;

    /**
     * RE capturing group pattern for a range of sheet names with trailing
     * sheet separator).
     *
     * Examples:
     * - `Sheet1:Sheet2!` in the expression `SUM(Sheet1:Sheet2!A1)`.
     * - `'[1]Sheet1:Sheet 2'` in the expression `SUM('[1]Sheet1:Sheet 2'!A1)`.
     *
     * Capturing groups:
     * 1. The index of the external document for simple sheet names.
     * 2. The name of the first sheet (simple sheet names).
     * 3. The name of the second sheet (simple sheet names).
     * 4. The index of the external document for complex sheet names.
     * 5. The name of the first sheet (complex sheet names).
     * 6. The name of the second sheet (complex sheet names).
     */
    override readonly SHEETS_PREFIX: string;

    /**
     * Number of capturing groups in the `DOC_PREFIX` pattern.
     */
    override readonly DOC_GROUPS = 1;

    /**
     * Number of capturing groups in the `SHEET_PREFIX` pattern.
     */
    override readonly SHEET_GROUPS = 6;

    /**
     * Number of capturing groups in the `SHEETS_PREFIX` pattern.
     */
    override readonly SHEETS_GROUPS = 6;

    // constructor ------------------------------------------------------------

    constructor(formulaResource: FormulaResource, localized: boolean) {
        super(formulaResource, false, localized);

        // public capturing groups
        this.DOC_PREFIX = DOC_REF + this.SHEET_SEP;
        this.SHEET_PREFIX = this.getSheetNamePattern({ extern: true, error: true }) + this.SHEET_SEP;
        this.SHEETS_PREFIX = this.getSheetNamePattern({ extern: true, range: true }) + this.SHEET_SEP;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a `SheetRefs` structure for the matches of a `DOC_PREFIX`
     * pattern.
     *
     * @param _context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `DOC_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  A `SheetRefs` structure for the passed matches.
     */
    override parseDocRef(_context: SheetRefPatternsContext, groups: string[], index: number): SheetRefs {
        return SheetRefs.global(parseInt(groups[index], 10));
    }

    /**
     * Returns a `SheetRefs` structure for the matches of a `SHEET_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `SHEET_PREFIX` pattern in the passed groups.
     *
     * @param error
     *  Whether to accept the #REF! error code in the matches following the
     *  sheet names.
     *
     * @returns
     *  The `SheetRefs` structure for the passed matches.
     */
    override parseSheetRef(context: SheetRefPatternsContext, groups: string[], index: number, error: boolean): SheetRefs {
        // `SHEET_PREFIX` groups: [simpleDoc, simpleName, complexDoc, complexName, errorDoc?, refError?]
        const extDocName = groups[index] || groups[index + 2] || (error ? groups[index + 4] : "");
        return this.createSheetRefs(context, extDocName, groups[index + 1], groups[index + 3], error ? groups[index + 5] : "", true);
    }

    /**
     * Returns a `SheetRefs` structure for the matches of a `SHEETS_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `SHEETS_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  The `SheetRefs` structures for the passed matches.
     */
    override parseSheetRefs(context: SheetRefPatternsContext, groups: string[], index: number): SheetRefs {
        // `SHEETS_PREFIX` groups: [simpleDoc, simpleName1, simpleName2, complexDoc, complexName1, complexName2]
        const extDocName = groups[index] || groups[index + 3];
        const sheetRefs = this.createSheetRefs(context, extDocName, groups[index + 1], groups[index + 4], "", true);
        sheetRefs.r2 = this.createSheetRefs(context, extDocName, groups[index + 2], groups[index + 5], "", true).r1;
        return sheetRefs;
    }
}

// class SheetRefPatternsOF ===================================================

/**
 * Configuration settings and helper methods for sheet references in grammars
 * based on OpenFormula.
 */
class SheetRefPatternsOF extends SheetRefPatterns {

    /**
     * RE capturing group pattern for the reference to an external document
     * (not supported for OpenDocument syntax).
     */
    override readonly DOC_PREFIX = "";

    /**
     * RE capturing group pattern for a single sheet name with optional
     * absolute marker and trailing sheet separator, or a broken sheet
     * reference after deleting the referenced sheet.
     *
     * Examples:
     * - `$Sheet1` in the expression `$Sheet1!A1`.
     * - `$#REF!` in the expression `$#REF!!A1`.
     *
     * Capturing groups:
     * 1. The absolute marker.
     * 2. The name of the sheet (simple sheet name).
     * 3. The name of the sheet (complex sheet name).
     * 4. The #REF! error code as sheet name.
     */
    override readonly SHEET_PREFIX: string;

    /**
     * RE capturing group pattern for a range of sheet names with optional
     * absolute markers and trailing sheet separator.
     *
     * Examples:
     * - `Sheet1:$Sheet2!` in the expression `SUM(Sheet1:$Sheet2!A1)`.
     *
     * Capturing groups:
     * 1. The absolute marker of the first sheet.
     * 2. The name of the first sheet (simple sheet name).
     * 3. The name of the first sheet (complex sheet name).
     * 4. The absolute marker of the second sheet.
     * 5. The name of the second sheet (simple sheet name).
     * 6. The name of the second sheet (complex sheet name).
     */
    override readonly SHEETS_PREFIX: string;

    /**
     * Number of capturing groups in the `DOC_PREFIX` pattern.
     */
    override readonly DOC_GROUPS = 0;

    /**
     * Number of capturing groups in the `SHEET_PREFIX` pattern.
     */
    override readonly SHEET_GROUPS = 4;

    /**
     * Number of capturing groups in the `SHEETS_PREFIX` pattern.
     */
    override readonly SHEETS_GROUPS = 6;

    // constructor ------------------------------------------------------------

    constructor(formulaResource: FormulaResource, localized: boolean) {
        super(formulaResource, true, localized);

        // additional RE patterns
        const ABS_REF = re.capture("\\$?");
        const SHEET_PATTERN = ABS_REF + this.getSheetNamePattern();

        // public capturing groups
        this.SHEET_PREFIX = ABS_REF + this.getSheetNamePattern({ error: true }) + this.SHEET_SEP;
        this.SHEETS_PREFIX = SHEET_PATTERN + ":" + SHEET_PATTERN + this.SHEET_SEP;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a `SheetRefs` structure for the matches of a `DOC_PREFIX`
     * pattern (not supported for OpenFormula).
     *
     * @param _context
     *  The parser context with the document access interface.
     *
     * @param _groups
     *  The capturing group matches of a regular expression.
     *
     * @param _index
     *  The start index of the `DOC_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  A `SheetRefs` structure for the passed matches.
     */
    override parseDocRef(_context: SheetRefPatternsContext, _groups: string[], _index: number): SheetRefs {
        return new SheetRefs();
    }

    /**
     * Returns a `SheetRefs` structure for the matches of a `SHEET_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `SHEET_PREFIX` pattern in the passed groups.
     *
     * @param error
     *  Whether to accept the #REF! error code in the matches following the
     *  sheet names.
     *
     * @returns
     *  The `SheetRefs` structure for the passed matches.
     */
    override parseSheetRef(context: SheetRefPatternsContext, groups: string[], index: number, error: boolean): SheetRefs {
        // `SHEET_PREFIX` groups: [absMarker, simpleName, complexName, refError] (no support for external documents)
        return this.createSheetRefs(context, "", groups[index + 1], groups[index + 2], error ? groups[index + 3] : "", !!groups[index]);
    }

    /**
     * Returns a `SheetRefs` structure for the matches of a `SHEETS_PREFIX`
     * pattern.
     *
     * @param context
     *  The parser context with the document access interface.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start index of the `SHEETS_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  The `SheetRefs` structure for the passed matches.
     */
    override parseSheetRefs(context: SheetRefPatternsContext, groups: string[], index: number): SheetRefs {
        // `SHEETS_PREFIX` groups: [absMarker1, simpleName1, complexName1, absMarker2, simpleName2, complexName2] (no support for external documents)
        const sheetRefs = this.parseSheetRef(context, groups, index, false); // no #REF! error allowed
        sheetRefs.r2 = this.parseSheetRef(context, groups, index + 3, false).r1;
        return sheetRefs;
    }
}
