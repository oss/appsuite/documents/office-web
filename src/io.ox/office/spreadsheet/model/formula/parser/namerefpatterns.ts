/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import type { TableRegionKey, FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { WHITESPACE } from "@/io.ox/office/spreadsheet/model/formula/parser/genericpatterns";

// constants ==================================================================

// character ranges (without brackets) for the leading character of a name or identifier
const NAME_LEADING_CHARS = "A-Z_a-z\\xA1\\xA4\\xA7-\\xA8\\xAA\\xAF-\\xBA\\xBC-\\u02B8\\u02BB-\\u02C1\\u02C7\\u02C9-\\u02CB\\u02CD\\u02D0-\\u02D1\\u02D8-\\u02DB\\u02DD\\u02E0-\\u02E4\\u02EE\\u0370-\\u0373\\u0376-\\u0377\\u037A-\\u037D\\u0386\\u0388-\\u038A\\u038C\\u038E-\\u03A1\\u03A3-\\u03F5\\u03F7-\\u0481\\u048A-\\u0523\\u0531-\\u0556\\u0559\\u0561-\\u0587\\u05D0-\\u05EA\\u05F0-\\u05F2\\u0621-\\u063F\\u0641-\\u064A\\u066E-\\u066F\\u0671-\\u06D3\\u06D5\\u06E5-\\u06E6\\u06EE-\\u06EF\\u06FA-\\u06FC\\u06FF\\u0710\\u0712-\\u072F\\u074D-\\u07A5\\u07B1\\u07CA-\\u07EA\\u07F4-\\u07F5\\u07FA\\u0904-\\u0939\\u093D\\u0950\\u0958-\\u0961\\u0971-\\u0972\\u097B-\\u097F\\u0985-\\u098C\\u098F-\\u0990\\u0993-\\u09A8\\u09AA-\\u09B0\\u09B2\\u09B6-\\u09B9\\u09BD\\u09CE\\u09DC-\\u09DD\\u09DF-\\u09E1\\u09F0-\\u09F1\\u0A05-\\u0A0A\\u0A0F-\\u0A10\\u0A13-\\u0A28\\u0A2A-\\u0A30\\u0A32-\\u0A33\\u0A35-\\u0A36\\u0A38-\\u0A39\\u0A59-\\u0A5C\\u0A5E\\u0A72-\\u0A74\\u0A85-\\u0A8D\\u0A8F-\\u0A91\\u0A93-\\u0AA8\\u0AAA-\\u0AB0\\u0AB2-\\u0AB3\\u0AB5-\\u0AB9\\u0ABD\\u0AD0\\u0AE0-\\u0AE1\\u0B05-\\u0B0C\\u0B0F-\\u0B10\\u0B13-\\u0B28\\u0B2A-\\u0B30\\u0B32-\\u0B33\\u0B35-\\u0B39\\u0B3D\\u0B5C-\\u0B5D\\u0B5F-\\u0B61\\u0B71\\u0B83\\u0B85-\\u0B8A\\u0B8E-\\u0B90\\u0B92-\\u0B95\\u0B99-\\u0B9A\\u0B9C\\u0B9E-\\u0B9F\\u0BA3-\\u0BA4\\u0BA8-\\u0BAA\\u0BAE-\\u0BB9\\u0BD0\\u0C05-\\u0C0C\\u0C0E-\\u0C10\\u0C12-\\u0C28\\u0C2A-\\u0C33\\u0C35-\\u0C39\\u0C3D\\u0C58-\\u0C59\\u0C60-\\u0C61\\u0C85-\\u0C8C\\u0C8E-\\u0C90\\u0C92-\\u0CA8\\u0CAA-\\u0CB3\\u0CB5-\\u0CB9\\u0CBD\\u0CDE\\u0CE0-\\u0CE1\\u0D05-\\u0D0C\\u0D0E-\\u0D10\\u0D12-\\u0D28\\u0D2A-\\u0D39\\u0D3D\\u0D60-\\u0D61\\u0D7A-\\u0D7F\\u0D85-\\u0D96\\u0D9A-\\u0DB1\\u0DB3-\\u0DBB\\u0DBD\\u0DC0-\\u0DC6\\u0E01-\\u0E3A\\u0E40-\\u0E4E\\u0E81-\\u0E82\\u0E84\\u0E87-\\u0E88\\u0E8A\\u0E8D\\u0E94-\\u0E97\\u0E99-\\u0E9F\\u0EA1-\\u0EA3\\u0EA5\\u0EA7\\u0EAA-\\u0EAB\\u0EAD-\\u0EB0\\u0EB2-\\u0EB3\\u0EBD\\u0EC0-\\u0EC4\\u0EC6\\u0EDC-\\u0EDD\\u0F00\\u0F40-\\u0F47\\u0F49-\\u0F6C\\u0F88-\\u0F8B\\u1000-\\u102A\\u103F\\u1050-\\u1055\\u105A-\\u105D\\u1061\\u1065-\\u1066\\u106E-\\u1070\\u1075-\\u1081\\u108E\\u10A0-\\u10C5\\u10D0-\\u10FA\\u10FC\\u1100-\\u1159\\u115F-\\u11A2\\u11A8-\\u11F9\\u1200-\\u1248\\u124A-\\u124D\\u1250-\\u1256\\u1258\\u125A-\\u125D\\u1260-\\u1288\\u128A-\\u128D\\u1290-\\u12B0\\u12B2-\\u12B5\\u12B8-\\u12BE\\u12C0\\u12C2-\\u12C5\\u12C8-\\u12D6\\u12D8-\\u1310\\u1312-\\u1315\\u1318-\\u135A\\u1380-\\u138F\\u13A0-\\u13F4\\u1401-\\u166C\\u166F-\\u1676\\u1681-\\u169A\\u16A0-\\u16EA\\u16EE-\\u16F0\\u1700-\\u170C\\u170E-\\u1711\\u1720-\\u1731\\u1740-\\u1751\\u1760-\\u176C\\u176E-\\u1770\\u1780-\\u17B3\\u17D7\\u17DC\\u1820-\\u1877\\u1880-\\u18A8\\u18AA\\u1900-\\u191C\\u1950-\\u196D\\u1970-\\u1974\\u1980-\\u19A9\\u19C1-\\u19C7\\u1A00-\\u1A16\\u1B05-\\u1B33\\u1B45-\\u1B4B\\u1B83-\\u1BA0\\u1BAE-\\u1BAF\\u1C00-\\u1C23\\u1C4D-\\u1C4F\\u1C5A-\\u1C7D\\u1D00-\\u1DBF\\u1E00-\\u1F15\\u1F18-\\u1F1D\\u1F20-\\u1F45\\u1F48-\\u1F4D\\u1F50-\\u1F57\\u1F59\\u1F5B\\u1F5D\\u1F5F-\\u1F7D\\u1F80-\\u1FB4\\u1FB6-\\u1FBC\\u1FBE\\u1FC2-\\u1FC4\\u1FC6-\\u1FCC\\u1FD0-\\u1FD3\\u1FD6-\\u1FDB\\u1FE0-\\u1FEC\\u1FF2-\\u1FF4\\u1FF6-\\u1FFC\\u2010\\u2013-\\u2016\\u2018\\u201C-\\u201D\\u2020-\\u2021\\u2025-\\u2027\\u2030\\u2032-\\u2033\\u2035\\u203B\\u2071\\u2074\\u207F\\u2081-\\u2084\\u2090-\\u2094\\u2102-\\u2103\\u2105\\u2107\\u2109-\\u2113\\u2115-\\u2116\\u2119-\\u211D\\u2121-\\u2122\\u2124\\u2126\\u2128\\u212A-\\u212D\\u212F-\\u2139\\u213C-\\u213F\\u2145-\\u2149\\u214E\\u2153-\\u2154\\u215B-\\u215E\\u2160-\\u2188\\u2190-\\u2199\\u21D2\\u21D4\\u2200\\u2202-\\u2203\\u2207-\\u2208\\u220B\\u220F\\u2211\\u2215\\u221A\\u221D-\\u2220\\u2223\\u2225\\u2227-\\u222C\\u222E\\u2234-\\u2237\\u223C-\\u223D\\u2248\\u224C\\u2252\\u2260-\\u2261\\u2264-\\u2267\\u226A-\\u226B\\u226E-\\u226F\\u2282-\\u2283\\u2286-\\u2287\\u2295\\u2299\\u22A5\\u22BF\\u2312\\u2460-\\u24B5\\u24D0-\\u24E9\\u2500-\\u254B\\u2550-\\u2574\\u2581-\\u258F\\u2592-\\u2595\\u25A0-\\u25A1\\u25A3-\\u25A9\\u25B2-\\u25B3\\u25B6-\\u25B7\\u25BC-\\u25BD\\u25C0-\\u25C1\\u25C6-\\u25C8\\u25CB\\u25CE-\\u25D1\\u25E2-\\u25E5\\u25EF\\u2605-\\u2606\\u2609\\u260E-\\u260F\\u261C\\u261E\\u2640\\u2642\\u2660-\\u2661\\u2663-\\u2665\\u2667-\\u266A\\u266C-\\u266D\\u266F\\u2C00-\\u2C2E\\u2C30-\\u2C5E\\u2C60-\\u2C6F\\u2C71-\\u2C7D\\u2C80-\\u2CE4\\u2D00-\\u2D25\\u2D30-\\u2D65\\u2D6F\\u2D80-\\u2D96\\u2DA0-\\u2DA6\\u2DA8-\\u2DAE\\u2DB0-\\u2DB6\\u2DB8-\\u2DBE\\u2DC0-\\u2DC6\\u2DC8-\\u2DCE\\u2DD0-\\u2DD6\\u2DD8-\\u2DDE\\u3000-\\u3003\\u3005-\\u3017\\u301D-\\u301F\\u3021-\\u3029\\u3031-\\u3035\\u3038-\\u303C\\u3041-\\u3096\\u309B-\\u309F\\u30A1-\\u30FF\\u3105-\\u312D\\u3131-\\u318E\\u31A0-\\u31B7\\u31F0-\\u321C\\u3220-\\u3229\\u3231-\\u3232\\u3239\\u3260-\\u327B\\u327F\\u32A3-\\u32A8\\u3303\\u330D\\u3314\\u3318\\u3322-\\u3323\\u3326-\\u3327\\u332B\\u3336\\u333B\\u3349-\\u334A\\u334D\\u3351\\u3357\\u337B-\\u337E\\u3380-\\u3384\\u3388-\\u33CA\\u33CD-\\u33D3\\u33D5-\\u33D6\\u33D8\\u33DB-\\u33DD\\u3400-\\u4DB5\\u4E00-\\u9FC3\\uA000-\\uA48C\\uA500-\\uA60C\\uA610-\\uA61F\\uA62A-\\uA62B\\uA640-\\uA65F\\uA662-\\uA66E\\uA680-\\uA697\\uA722-\\uA787\\uA78B-\\uA78C\\uA7FB-\\uA801\\uA803-\\uA805\\uA807-\\uA80A\\uA80C-\\uA822\\uA840-\\uA873\\uA882-\\uA8B3\\uA90A-\\uA925\\uA930-\\uA946\\uAA00-\\uAA28\\uAA40-\\uAA42\\uAA44-\\uAA4B\\uAC00-\\uD7A3\\uE000-\\uF848\\uF900-\\uFA2D\\uFA30-\\uFA6A\\uFA70-\\uFAD9\\uFB00-\\uFB06\\uFB13-\\uFB17\\uFB1D\\uFB1F-\\uFB28\\uFB2A-\\uFB36\\uFB38-\\uFB3C\\uFB3E\\uFB40-\\uFB41\\uFB43-\\uFB44\\uFB46-\\uFBB1\\uFBD3-\\uFD3D\\uFD50-\\uFD8F\\uFD92-\\uFDC7\\uFDF0-\\uFDFB\\uFE30-\\uFE31\\uFE33-\\uFE44\\uFE49-\\uFE52\\uFE54-\\uFE57\\uFE59-\\uFE66\\uFE68-\\uFE6B\\uFE70-\\uFE74\\uFE76-\\uFEFC\\uFF01-\\uFF5E\\uFF61-\\uFFBE\\uFFC2-\\uFFC7\\uFFCA-\\uFFCF\\uFFD2-\\uFFD7\\uFFDA-\\uFFDC\\uFFE0-\\uFFE6";

// character ranges for the inner character of a name or identifier
const NAME_INNER_CHARS = "\\d\\?A-Z_a-z\\xA1\\xA4\\xA7-\\xA8\\xAA\\xAF-\\xBA\\xBC-\\u034E\\u0350-\\u0377\\u037A-\\u037D\\u0384-\\u0386\\u0388-\\u038A\\u038C\\u038E-\\u03A1\\u03A3-\\u0523\\u0531-\\u0556\\u0559\\u0561-\\u0587\\u0591-\\u05BD\\u05BF\\u05C1-\\u05C2\\u05C4-\\u05C5\\u05C7\\u05D0-\\u05EA\\u05F0-\\u05F2\\u0600-\\u0603\\u0606-\\u0608\\u060B\\u060E-\\u061A\\u061F\\u0621-\\u063F\\u0641-\\u065E\\u0660-\\u0669\\u066E-\\u06D3\\u06D5-\\u06FF\\u070F-\\u074A\\u074D-\\u07B1\\u07C0-\\u07F6\\u07FA\\u0901-\\u0939\\u093C-\\u094D\\u0950-\\u0954\\u0958-\\u0963\\u0966-\\u096F\\u0971-\\u0972\\u097B-\\u097F\\u0981-\\u0983\\u0985-\\u098C\\u098F-\\u0990\\u0993-\\u09A8\\u09AA-\\u09B0\\u09B2\\u09B6-\\u09B9\\u09BC-\\u09C4\\u09C7-\\u09C8\\u09CB-\\u09CE\\u09D7\\u09DC-\\u09DD\\u09DF-\\u09E3\\u09E6-\\u09FA\\u0A01-\\u0A03\\u0A05-\\u0A0A\\u0A0F-\\u0A10\\u0A13-\\u0A28\\u0A2A-\\u0A30\\u0A32-\\u0A33\\u0A35-\\u0A36\\u0A38-\\u0A39\\u0A3C\\u0A3E-\\u0A42\\u0A47-\\u0A48\\u0A4B-\\u0A4D\\u0A51\\u0A59-\\u0A5C\\u0A5E\\u0A66-\\u0A75\\u0A81-\\u0A83\\u0A85-\\u0A8D\\u0A8F-\\u0A91\\u0A93-\\u0AA8\\u0AAA-\\u0AB0\\u0AB2-\\u0AB3\\u0AB5-\\u0AB9\\u0ABC-\\u0AC5\\u0AC7-\\u0AC9\\u0ACB-\\u0ACD\\u0AD0\\u0AE0-\\u0AE3\\u0AE6-\\u0AEF\\u0AF1\\u0B01-\\u0B03\\u0B05-\\u0B0C\\u0B0F-\\u0B10\\u0B13-\\u0B28\\u0B2A-\\u0B30\\u0B32-\\u0B33\\u0B35-\\u0B39\\u0B3C-\\u0B44\\u0B47-\\u0B48\\u0B4B-\\u0B4D\\u0B56-\\u0B57\\u0B5C-\\u0B5D\\u0B5F-\\u0B63\\u0B66-\\u0B71\\u0B82-\\u0B83\\u0B85-\\u0B8A\\u0B8E-\\u0B90\\u0B92-\\u0B95\\u0B99-\\u0B9A\\u0B9C\\u0B9E-\\u0B9F\\u0BA3-\\u0BA4\\u0BA8-\\u0BAA\\u0BAE-\\u0BB9\\u0BBE-\\u0BC2\\u0BC6-\\u0BC8\\u0BCA-\\u0BCD\\u0BD0\\u0BD7\\u0BE6-\\u0BFA\\u0C01-\\u0C03\\u0C05-\\u0C0C\\u0C0E-\\u0C10\\u0C12-\\u0C28\\u0C2A-\\u0C33\\u0C35-\\u0C39\\u0C3D-\\u0C44\\u0C46-\\u0C48\\u0C4A-\\u0C4D\\u0C55-\\u0C56\\u0C58-\\u0C59\\u0C60-\\u0C63\\u0C66-\\u0C6F\\u0C78-\\u0C7F\\u0C82-\\u0C83\\u0C85-\\u0C8C\\u0C8E-\\u0C90\\u0C92-\\u0CA8\\u0CAA-\\u0CB3\\u0CB5-\\u0CB9\\u0CBC-\\u0CC4\\u0CC6-\\u0CC8\\u0CCA-\\u0CCD\\u0CD5-\\u0CD6\\u0CDE\\u0CE0-\\u0CE3\\u0CE6-\\u0CEF\\u0CF1-\\u0CF2\\u0D02-\\u0D03\\u0D05-\\u0D0C\\u0D0E-\\u0D10\\u0D12-\\u0D28\\u0D2A-\\u0D39\\u0D3D-\\u0D44\\u0D46-\\u0D48\\u0D4A-\\u0D4D\\u0D57\\u0D60-\\u0D63\\u0D66-\\u0D75\\u0D79-\\u0D7F\\u0D82-\\u0D83\\u0D85-\\u0D96\\u0D9A-\\u0DB1\\u0DB3-\\u0DBB\\u0DBD\\u0DC0-\\u0DC6\\u0DCA\\u0DCF-\\u0DD4\\u0DD6\\u0DD8-\\u0DDF\\u0DF2-\\u0DF3\\u0E01-\\u0E3A\\u0E3F-\\u0E4E\\u0E50-\\u0E59\\u0E81-\\u0E82\\u0E84\\u0E87-\\u0E88\\u0E8A\\u0E8D\\u0E94-\\u0E97\\u0E99-\\u0E9F\\u0EA1-\\u0EA3\\u0EA5\\u0EA7\\u0EAA-\\u0EAB\\u0EAD-\\u0EB9\\u0EBB-\\u0EBD\\u0EC0-\\u0EC4\\u0EC6\\u0EC8-\\u0ECB\\u0ECD\\u0ED0-\\u0ED9\\u0EDC-\\u0EDD\\u0F00-\\u0F03\\u0F13-\\u0F39\\u0F3E-\\u0F47\\u0F49-\\u0F6C\\u0F71-\\u0F84\\u0F86-\\u0F8B\\u0F90-\\u0F97\\u0F99-\\u0FBC\\u0FBE-\\u0FCC\\u0FCE-\\u0FCF\\u1000-\\u1049\\u1050-\\u1099\\u109E-\\u10C5\\u10D0-\\u10FA\\u10FC\\u1100-\\u1159\\u115F-\\u11A2\\u11A8-\\u11F9\\u1200-\\u1248\\u124A-\\u124D\\u1250-\\u1256\\u1258\\u125A-\\u125D\\u1260-\\u1288\\u128A-\\u128D\\u1290-\\u12B0\\u12B2-\\u12B5\\u12B8-\\u12BE\\u12C0\\u12C2-\\u12C5\\u12C8-\\u12D6\\u12D8-\\u1310\\u1312-\\u1315\\u1318-\\u135A\\u135F-\\u1360\\u1369-\\u137C\\u1380-\\u1399\\u13A0-\\u13F4\\u1401-\\u166C\\u166F-\\u1676\\u1680-\\u169A\\u16A0-\\u16EA\\u16EE-\\u16F0\\u1700-\\u170C\\u170E-\\u1714\\u1720-\\u1734\\u1740-\\u1753\\u1760-\\u176C\\u176E-\\u1770\\u1772-\\u1773\\u1780-\\u17D3\\u17D7\\u17DB-\\u17DD\\u17E0-\\u17E9\\u17F0-\\u17F9\\u180E\\u1810-\\u1819\\u1820-\\u1877\\u1880-\\u18AA\\u1900-\\u191C\\u1920-\\u192B\\u1930-\\u193B\\u1940\\u1946-\\u196D\\u1970-\\u1974\\u1980-\\u19A9\\u19B0-\\u19C9\\u19D0-\\u19D9\\u19E0-\\u1A1B\\u1B00-\\u1B4B\\u1B50-\\u1B59\\u1B61-\\u1B7C\\u1B80-\\u1BAA\\u1BAE-\\u1BB9\\u1C00-\\u1C37\\u1C40-\\u1C49\\u1C4D-\\u1C7D\\u1D00-\\u1DE6\\u1DFE-\\u1F15\\u1F18-\\u1F1D\\u1F20-\\u1F45\\u1F48-\\u1F4D\\u1F50-\\u1F57\\u1F59\\u1F5B\\u1F5D\\u1F5F-\\u1F7D\\u1F80-\\u1FB4\\u1FB6-\\u1FC4\\u1FC6-\\u1FD3\\u1FD6-\\u1FDB\\u1FDD-\\u1FEF\\u1FF2-\\u1FF4\\u1FF6-\\u1FFE\\u2000-\\u200B\\u2010\\u2013-\\u2016\\u2018\\u201C-\\u201D\\u2020-\\u2021\\u2025-\\u2029\\u202F-\\u2030\\u2032-\\u2033\\u2035\\u203B\\u2044\\u2052\\u205F\\u2070-\\u2071\\u2074-\\u207C\\u207F-\\u208C\\u2090-\\u2094\\u20A0-\\u20B5\\u20D0-\\u20F0\\u2100-\\u214F\\u2153-\\u2188\\u2190-\\u2328\\u232B-\\u23E7\\u2400-\\u2426\\u2440-\\u244A\\u2460-\\u269D\\u26A0-\\u26BC\\u26C0-\\u26C3\\u2701-\\u2704\\u2706-\\u2709\\u270C-\\u2727\\u2729-\\u274B\\u274D\\u274F-\\u2752\\u2756\\u2758-\\u275E\\u2761-\\u2767\\u2776-\\u2794\\u2798-\\u27AF\\u27B1-\\u27BE\\u27C0-\\u27C4\\u27C7-\\u27CA\\u27CC\\u27D0-\\u27E5\\u27F0-\\u2982\\u2999-\\u29D7\\u29DC-\\u29FB\\u29FE-\\u2B4C\\u2B50-\\u2B54\\u2C00-\\u2C2E\\u2C30-\\u2C5E\\u2C60-\\u2C6F\\u2C71-\\u2C7D\\u2C80-\\u2CEA\\u2CFD\\u2D00-\\u2D25\\u2D30-\\u2D65\\u2D6F\\u2D80-\\u2D96\\u2DA0-\\u2DA6\\u2DA8-\\u2DAE\\u2DB0-\\u2DB6\\u2DB8-\\u2DBE\\u2DC0-\\u2DC6\\u2DC8-\\u2DCE\\u2DD0-\\u2DD6\\u2DD8-\\u2DDE\\u2DE0-\\u2DFF\\u2E2F\\u2E80-\\u2E99\\u2E9B-\\u2EF3\\u2F00-\\u2FD5\\u2FF0-\\u2FFB\\u3000-\\u3017\\u301D-\\u302F\\u3031-\\u303C\\u303E-\\u303F\\u3041-\\u3096\\u3099-\\u309F\\u30A1-\\u30FF\\u3105-\\u312D\\u3131-\\u318E\\u3192-\\u31B7\\u31C0-\\u31E3\\u31F0-\\u321E\\u3220-\\u3243\\u3250-\\u32FE\\u3300-\\u4DB5\\u4DC0-\\u9FC3\\uA000-\\uA48C\\uA490-\\uA4C6\\uA500-\\uA60C\\uA610-\\uA62B\\uA640-\\uA65F\\uA662-\\uA672\\uA67C-\\uA67D\\uA67F-\\uA697\\uA700-\\uA78C\\uA7FB-\\uA82B\\uA840-\\uA873\\uA880-\\uA8C4\\uA8D0-\\uA8D9\\uA900-\\uA92E\\uA930-\\uA953\\uAA00-\\uAA36\\uAA40-\\uAA4D\\uAA50-\\uAA59\\uAC00-\\uD7A3\\uD800-\\uFA2D\\uFA30-\\uFA6A\\uFA70-\\uFAD9\\uFB00-\\uFB06\\uFB13-\\uFB17\\uFB1D-\\uFB36\\uFB38-\\uFB3C\\uFB3E\\uFB40-\\uFB41\\uFB43-\\uFB44\\uFB46-\\uFBB1\\uFBD3-\\uFD3D\\uFD50-\\uFD8F\\uFD92-\\uFDC7\\uFDF0-\\uFDFD\\uFE20-\\uFE26\\uFE30-\\uFE31\\uFE33-\\uFE44\\uFE49-\\uFE52\\uFE54-\\uFE57\\uFE59-\\uFE66\\uFE68-\\uFE6B\\uFE70-\\uFE74\\uFE76-\\uFEFC\\uFF01-\\uFF5E\\uFF61-\\uFFBE\\uFFC2-\\uFFC7\\uFFCA-\\uFFCF\\uFFD2-\\uFFD7\\uFFDA-\\uFFDC\\uFFE0-\\uFFE6\\uFFE8-\\uFFEE";

// class NameRefPatterns ======================================================

/**
 * Configurations of different syntaxes for defined names, function names, and
 * structured table references used in formula expressions.
 */
export class NameRefPatterns {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `NameRefPatterns`.
     */
    static create(formulaResource: FormulaResource, localized: boolean): NameRefPatterns {
        // no subclasses needed (yet)
        return new NameRefPatterns(formulaResource, localized);
    }

    // properties -------------------------------------------------------------

    /**
     * RE character class for the leading character of a name or identifier.
     */
    readonly NAME_LEADING_CHAR_CLASS: string;

    /**
     * RE character class for other characters of a name or identifier.
     */
    readonly NAME_INNER_CHAR_CLASS: string;

    /**
     * RE character class for other characters of a function name.
     */
    readonly FUNC_INNER_CHAR_CLASS: string;

    /**
     * RE lookahead pattern to exclude an opening parenthesis and an
     * exclamation mark after another pattern.
     */
    readonly TERMINATOR: string;

    /**
     * RE capturing group pattern for a complete name or identifier.
     *
     * Capturing groups:
     * 1. The entire identifier.
     */
    readonly DEFINED_NAME: string;

    /**
     * RE capturing group pattern for a complete function name.
     *
     * Capturing groups:
     * 1. The entire function name.
     */
    readonly FUNC_NAME: string;

    /**
     * RE matching group pattern that matches the opening bracket of a
     * structured table reference.
     *
     * Capturing groups:
     * 1. The whitespace characters following the opening bracket.
     */
    readonly TABLE_OPEN: string;

    /**
     * RE matching group pattern that matches the closing bracket of a
     * structured table reference.
     *
     * Capturing groups:
     * 1. The whitespace characters preceding the closing bracket.
     * 2. The closing bracket, or an empty string if the bracket is missing at
     *    the end of the formula.
     */
    readonly TABLE_CLOSE: string;

    /**
     * RE matching group pattern that matches a separator with surrounding
     * whitespace inside a table reference.
     *
     * Capturing groups:
     * 1. The whitespace characters preceding the separator.
     * 2. The whitespace characters following the separator.
     */
    readonly TABLE_SEPARATOR: string;

    /**
     * RE matching group pattern that matches a single predefined table region
     * in structured table references (without brackets), followed by a closing
     * bracket.
     *
     * Examples:
     * - The string `#All` in `=Table[#All]`.
     *
     * Capturing groups:
     * 1. The name of the table region, with heading hash character.
     */
    readonly TABLE_SINGLE_REGION_NAME: string;

    /**
     * RE matching group pattern that matches a single column name (also
     * complex names) in structured table references (without brackets),
     * followed by a closing bracket.
     *
     * Examples:
     * - The string `Col'#2` in `=Table[Col'#2]`.
     *
     * Capturing groups:
     * 1. The column name.
     */
    readonly TABLE_SINGLE_COLUMN_NAME: string;

    /**
     * RE matching group pattern that matches a predefined table region in
     * structured table references, enclosed in brackets.
     *
     * Capturing groups:
     * 1. The name of the table region, without brackets, with leading hash
     *    character.
     */
    readonly TABLE_REGION_REF: string;

    /**
     * RE matching group pattern that matches a column reference in structured
     * table references, enclosed in brackets.
     *
     * Capturing groups:
     * 1. The column name, without brackets.
     */
    readonly TABLE_COLUMN_REF: string;

    /**
     * RE matching group pattern that matches a column range in structured
     * table references, each column name enclosed in brackets.
     *
     * Capturing groups:
     * 1. The first column name, without brackets.
     * 2. The second column name, without brackets.
     */
    readonly TABLE_COLUMN_RANGE_REF: string;

    /**
     * RE matching group pattern that matches a column reference in a
     * structured table reference. The column name may occur without brackets
     * (simple column names only), or inside brackets (any column name).
     *
     * Examples:
     * - Simple column name `Col1` in the table reference `Table1[@Col1]`.
     * - Complex column name `Col 2` (in brackets) in the table reference
     *   `Table1[@[Col 2]]`.
     *
     * Capturing groups:
     * 1. Leading whitespace characters before the column name.
     * 2. The simple column name found without brackets.
     * 3. The complex column name without the brackets.
     */
    readonly TABLE_MIXED_COLUMN_REF: string;

    /**
     * RE matching group pattern that matches a column range in a structured
     * table reference. Each column name may occur without brackets (simple
     * column names only), or inside brackets (any column name).
     *
     * Examples:
     * - The table reference `Table1[@Col1:[Col 2]]` contains a column range
     *   with the simple column name `Col1`, and the complex column name
     *   `Col 2` in brackets.
     *
     * Capturing groups:
     * 1. Leading whitespace characters before the column range.
     * 2. The simple name of the first column found without brackets.
     * 3. The complex name of the first column without the brackets.
     * 4. The simple name of the second column found without brackets.
     * 5. The complex name of the second column without the brackets.
     */
    readonly TABLE_MIXED_COLUMN_RANGE_REF: string;

    /** Whether this instance is used to handle formula expressions in the user interface. */
    readonly #localized: boolean;
    /** The resource map for table region keys. */
    readonly #regionMap: FormulaResource["regionMap"];
    /** Matches a simple table column name that may appear without brackets in a complex structured table reference. */
    readonly #RE_SIMPLE_TABLE_COLUMN_NAME: RegExp;
    /** Matches all characters in table column names that need to be escaped with an apostrophe. */
    readonly #RE_TABLE_COL_ESCAPE: RegExp;

    // constructor ------------------------------------------------------------

    protected constructor(formulaResource: FormulaResource, localized: boolean) {

        this.#localized = localized;
        const regionMap = this.#regionMap = formulaResource.regionMap;

        const openFormula = formulaResource.fileFormat === FileFormatType.ODF;

        // helper function enclosing the passed RE pattern into *literal* brackets
        const brackets = (pattern: string): string => "\\[" + pattern + "\\]";

        // RE pattern for the grammar's separator character (also used in table references)
        const SEP = re.escape(formulaResource.getSeparator(localized));

        // Character ranges (without brackets) for characters that need to be escaped in table column names.
        const COL_ESCAPE_CHARS = brackets("") + "'#" + (localized ? "@" : "");

        // Character ranges (without brackets) for characters of complex table column names. The comma
        // will be a complex table character, if it is also used as separator in the current grammar.
        const COL_COMPLEX_CHARS = "\\x00-\\x2B\\x2D-\\x2F\\x3A-\\x40\\x5B-\\x60\\x7B\\x7D\\x7E" + ((SEP === ",") ? SEP : "");

        // RE pattern for a simple column name in a structured table reference.
        // 1. The column name.
        const SIMPLE_COL_NAME = re.capture(re.class.neg(COL_COMPLEX_CHARS) + "+");

        // RE pattern for a complex column name in a structured table reference.
        // 1. The column name.
        const COMPLEX_COL_NAME = re.capture(re.group(re.class.neg(COL_ESCAPE_CHARS), "'.") + "+");

        // RE pattern for a simple column name without brackets, or any column name with brackets in a
        // structured table reference, e.g. in the formula `=Table1[@Col1:[Col 2]]`.
        // 1. The simple column name found without brackets.
        // 2. The complex column name found inside brackets, without the brackets.
        const MIXED_COL_NAME = re.group(SIMPLE_COL_NAME, brackets(COMPLEX_COL_NAME));

        // RE pattern for optional whitespace characters in a structured table reference
        // 1. The whitespace characters.
        const OPT_WS = re.capture(WHITESPACE + "*");

        // RE pattern for whitespace characters following a token in a structured table reference. Will only
        // be recognized if specific control characters that are valid inside a table reference will follow.
        // 1. The whitespace characters.
        const TRAILING_WS = re.group.opt(re.capture(WHITESPACE + "+") + re.ahead("\\[", "\\]", SEP, localized && "@", "$"));

        // character classes
        this.NAME_LEADING_CHAR_CLASS   = re.class(!openFormula && "\\\\", NAME_LEADING_CHARS);     // OOXML: allow leading backslash
        this.NAME_INNER_CHAR_CLASS     = re.class(!openFormula && ".\\\\", NAME_INNER_CHARS);      // OOXML: allow dots and backslash
        this.FUNC_INNER_CHAR_CLASS     = re.class(".", !openFormula && "\\\\", NAME_INNER_CHARS);  // OF: allow dots in difference to defined names

        // all valid inner characters of names must be excluded too, otherwise the previous groups would match less characters than they should
        this.TERMINATOR = re.ahead.neg("\\(", "!", this.NAME_INNER_CHAR_CLASS);

        // capturing patterns for names and functions
        this.DEFINED_NAME = re.capture(this.NAME_LEADING_CHAR_CLASS + this.NAME_INNER_CHAR_CLASS + "*");
        this.FUNC_NAME = re.capture(this.NAME_LEADING_CHAR_CLASS + this.FUNC_INNER_CHAR_CLASS + "*");

        // capturing patterns for table references
        this.TABLE_OPEN = "\\[" + TRAILING_WS;
        this.TABLE_CLOSE = OPT_WS + re.capture("\\]", "$");
        this.TABLE_SEPARATOR = OPT_WS + SEP + TRAILING_WS;
        this.TABLE_SINGLE_REGION_NAME = regionMap.getAllPattern(localized) + re.ahead("\\]");
        this.TABLE_SINGLE_COLUMN_NAME = COMPLEX_COL_NAME + re.ahead("\\]");
        this.TABLE_REGION_REF = brackets(regionMap.getAllPattern(localized));
        this.TABLE_COLUMN_REF = brackets(COMPLEX_COL_NAME);
        this.TABLE_COLUMN_RANGE_REF = brackets(COMPLEX_COL_NAME) + ":" + brackets(COMPLEX_COL_NAME);
        this.TABLE_MIXED_COLUMN_REF = OPT_WS + MIXED_COL_NAME;
        this.TABLE_MIXED_COLUMN_RANGE_REF = OPT_WS + MIXED_COL_NAME + ":" + MIXED_COL_NAME;

        // private regular expressions used in class methods
        this.#RE_SIMPLE_TABLE_COLUMN_NAME = new RegExp("^" + re.class.neg(COL_COMPLEX_CHARS) + "+$", "i");
        this.#RE_TABLE_COL_ESCAPE = new RegExp(re.class(COL_ESCAPE_CHARS), "g");
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed name of a table region to the unique resource key of
     * the table region.
     *
     * @param name
     *  The name of a table region (with leading hash character).
     *
     * @returns
     *  The resource key for the passed table region; or `null`, if the passed
     *  string is not the name of a supported table region.
     */
    getTableRegionKey(name: string): TableRegionKey | null {
        return this.#regionMap.getKey(name, this.#localized);
    }

    /**
     * Returns the name of a table region for the passed unique resource key.
     *
     * @param key
     *  The unique resource key of a table region.
     *
     * @returns
     *  The name of a table region for the passed resource key; or `null`, if
     *  the passed resource key cannot be resolved to the name of a table
     *  region.
     */
    getTableRegionName(key: TableRegionKey): string | null {
        return this.#regionMap.getName(key, this.#localized);
    }

    /**
     * Returns whether the passed table column name is considered to be simple,
     * i.e. it can be used without brackets in complex structured table
     * references in UI formula grammars.
     *
     * @param colName
     *  The table column name to be checked.
     *
     * @returns
     *  Whether the passed table column name is considered to be simple.
     */
    isSimpleTableColumn(colName: string): boolean {
        return this.#RE_SIMPLE_TABLE_COLUMN_NAME.test(colName);
    }

    /**
     * Converts the passed text representation of a table column name (used in
     * formula expressions) to the original column name.
     *
     * @param colName
     *  The encoded name of a table column.
     *
     * @returns
     *  The original table column name.
     */
    decodeTableColumn(colName: string): string {
        return colName.replace(/'(.)/g, "$1");
    }

    /**
     * Converts the passed table column name to the text representation of a
     * table column reference as used in structured table references.
     *
     * @param colName
     *  The name of a table column.
     *
     * @returns
     *  The string representation of the table column reference.
     */
    encodeTableColumn(colName: string): string {
        return colName.replace(this.#RE_TABLE_COL_ESCAPE, "'$&");
    }
}
