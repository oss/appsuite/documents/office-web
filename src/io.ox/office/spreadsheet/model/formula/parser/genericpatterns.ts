/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re } from "@/io.ox/office/tk/algorithms";

import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";

// constants ==================================================================

/**
 * RE character ranges for a whitespace or other non-printable character.
 */
export const WHITESPACE = re.class("\\s", "\\x00-\\x1f", "\\x80-\\x9f");

// class GenericPatterns ======================================================

/**
 * Configuration of different syntaxes for basic tokens in formula expressions.
 */
export class GenericPatterns {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `GenericPatterns`.
     */
    static create(formulaResource: FormulaResource, localized: boolean): GenericPatterns {
        // no subclasses needed (yet)
        return new GenericPatterns(formulaResource, localized);
    }

    // properties -------------------------------------------------------------

    /**
     * RE pattern that matches string literals (enclosed in double quotes,
     * embedded double quotes are represented by a sequence of two double quote
     * characters).
     *
     * Examples:
     * - `"abc"` for the three-character string `abc`.
     * - `"ab""cd"` for the five-character string `ab"cd`.
     *
     * Capturing groups:
     * 1. The string value with outer enclosing quote characters, and embedded
     *    duplicated quote characters.
     */
    readonly STRING_LITERAL: string;

    /**
     * RE pattern that matches boolean literals.
     */
    readonly BOOLEAN_LITERAL: string;

    /**
     * RE pattern that matches error code literals (only built-in supported
     * error codes, not other strings starting with a hash character).
     */
    readonly ERROR_LITERAL: string;

    /**
     * RE pattern for the #REF! error code.
     *
     * Capturing groups:
     * 1. The matched #REF! error code.
     */
    readonly REF_ERROR: string;

    /**
     * RE pattern that matches the list/parameter separator character.
     */
    readonly SEPARATOR: string;

    /**
     * RE pattern that matches all unary and binary operators.
     *
     * Capturing groups:
     * 1. The name of the matched operator.
     */
    readonly OPERATOR: string;

    // constructor ------------------------------------------------------------

    private constructor(formulaResource: FormulaResource, localized: boolean) {

        // scalar literals
        this.STRING_LITERAL     = re.capture(re.group('".*?"') + "+");
        this.BOOLEAN_LITERAL    = formulaResource.booleanMap.getAllPattern(localized);  // `getAllPattern` returns a capturing group
        this.ERROR_LITERAL      = formulaResource.errorMap.getAllPattern(localized);    // `getAllPattern` returns a capturing group
        this.REF_ERROR          = re.capture(re.escape(formulaResource.errorMap.getName("REF", localized)!));

        // separators and operators
        this.SEPARATOR  = re.escape(formulaResource.getSeparator(localized));
        this.OPERATOR   = formulaResource.operatorMap.getAllPattern(localized); // `getAllPattern` returns a capturing group
    }
}
