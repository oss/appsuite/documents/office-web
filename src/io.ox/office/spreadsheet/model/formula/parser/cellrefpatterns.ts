/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { parseCol, parseRow, Address } from "@/io.ox/office/spreadsheet/utils/address";
import { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";

// types ======================================================================

/**
 * Context for document and formula specific settings when using methods of
 * globally cached `CellRefPatterns` instances.
 */
export interface CellRefPatternsContext {
    refAddress: Address;
    maxCol: number;
    maxRow: number;
    wrapReferences: boolean;
}

// class CellRefPatterns ======================================================

/**
 * Base class for configurations of different address syntaxes used in formula
 * expressions.
 */
export abstract class CellRefPatterns {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `CellRefPatterns`.
     */
    static create(formulaResource: FormulaResource, localized: boolean, rcStyle: boolean): CellRefPatterns {
        const CellRefPatternsClass = rcStyle ? CellRefPatternsRC : CellRefPatternsA1;
        return new CellRefPatternsClass(formulaResource, localized);
    }

    // properties -------------------------------------------------------------

    /**
     * RE capturing group pattern for a single column index (absolute or relative).
     *
     * Capturing groups depend on subclass.
     */
    abstract readonly COL_REF: string;

    /**
     * RE capturing group pattern for a single row index (absolute or relative).
     *
     * Capturing groups depend on subclass.
     */
    abstract readonly ROW_REF: string;

    /**
     * RE capturing group pattern for a single cell address (absolute or relative).
     *
     * Capturing groups depend on subclass.
     */
    abstract readonly CELL_REF: string;

    /**
     * RE pattern for the #REF! error code, if supported as part of cell
     * address.
     */
    protected readonly refError: string | null;

    // constructor ------------------------------------------------------------

    protected constructor(formulaResource: FormulaResource, localized: boolean) {
        const openFormula = formulaResource.fileFormat === FileFormatType.ODF;
        this.refError = (openFormula && !localized) ? re.escape(formulaResource.errorMap.getName("REF", localized)!) : null;
    }

    // abstract methods -------------------------------------------------------

    /**
     * Extracts a column index from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match
     *  The matched column name of a regular expression.
     *
     * @returns
     *  The column for the passed match, if it is valid; otherwise -1.
     */
    abstract parseCol(context: CellRefPatternsContext, match: string): number;

    /**
     * Extracts a row index from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match
     *  The matched row name of a regular expression.
     *
     * @returns
     *  The row for the passed match, if it is valid; otherwise -1.
     */
    abstract parseRow(context: CellRefPatternsContext, match: string): number;

    /**
     * Extracts a cell address from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match1
     *  The first part of the address (A1: column name, RC: row index).
     *
     * @param match2
     *  The second part of the address (A1: row name, RC: column index).
     *
     * @returns
     *  A cell address for the passed matches, if the column and row indexes
     *  are valid; otherwise `null`.
     */
    abstract parseAddress(context: CellRefPatternsContext, match1: string, match2: string): Opt<Address>;

    /**
     * Returns a `CellRef` structure for the matches of a `COL_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `COL_REF` pattern in the passed groups.
     *
     * @param max
     *  Specifies which row index will be inserted into the cell reference. The
     *  value `false` will insert zero, the value `true` will insert the
     *  maximum row index.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the column index
     *  is valid; otherwise `undefined`.
     */
    abstract parseColRef(context: CellRefPatternsContext, groups: string[], index: number, max: boolean): Opt<CellRef>;

    /**
     * Returns a `CellRef` structure for the matches of a `ROW_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `ROW_REF` pattern in the passed groups.
     *
     * @param max
     *  Specifies which column index will be inserted into the cell reference.
     *  The value `false` will insert zero, the value `true` will insert the
     *  maximum column index.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the row index is
     *  valid; otherwise `undefined`.
     */
    abstract parseRowRef(context: CellRefPatternsContext, groups: string[], index: number, max: boolean): Opt<CellRef>;

    /**
     * Returns a `CellRef` structure for the matches of a `CELL_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `CELL_REF` pattern in the passed groups.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the column and
     *  row indexes are valid; otherwise `undefined`.
     */
    abstract parseCellRef(context: CellRefPatternsContext, groups: string[], index: number): Opt<CellRef>;
}

// class CellRefPatternsA1 ====================================================

/**
 * Configuration settings and helper methods for addresses in A1 notation.
 */
class CellRefPatternsA1 extends CellRefPatterns {

    /**
     * RE capturing group pattern for a single column index (absolute or
     * relative).
     *
     * Examples:
     * - `$D`, `ZZZ`.
     *
     * Capturing groups:
     * 1. The absolute marker.
     * 2. The column name.
     */
    override readonly COL_REF: string;

    /**
     * RE capturing group pattern for a single row index (absolute or
     * relative).
     *
     * Examples:
     * - `$3`, `999`.
     *
     * Capturing groups:
     * 1. The absolute marker.
     * 2. The row name.
     */
    override readonly ROW_REF: string;

    /**
     * RE capturing group pattern for a single cell address (absolute or
     * relative).
     *
     * Examples:
     * - `$D$3`, `D$999`, `$ZZZ3`, `ZZZ999`.
     *
     * Capturing groups:
     * 1. The absolute column marker.
     * 2. The column name.
     * 3. The absolute row marker.
     * 4. The row name.
     */
    override readonly CELL_REF: string;

    // constructor ------------------------------------------------------------

    constructor(formulaResource: FormulaResource, localized: boolean) {
        super(formulaResource, localized);

        const ABS_GROUP = re.capture("\\$?");

        this.COL_REF  = ABS_GROUP + re.capture("[a-z]+", this.refError);
        this.ROW_REF  = ABS_GROUP + re.capture("\\d+", this.refError);
        this.CELL_REF = this.COL_REF + this.ROW_REF;
    }

    // public methods ---------------------------------------------------------

    /**
     * Extracts a column index from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match
     *  The matched column name of a regular expression.
     *
     * @returns
     *  The column for the passed match, if it is valid; otherwise -1.
     */
    override parseCol(context: CellRefPatternsContext, match: string): number {
        const col = parseCol(match);
        return (col <= context.maxCol) ? col : -1;
    }

    /**
     * Extracts a row index from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match
     *  The matched row name of a regular expression.
     *
     * @returns
     *  The row for the passed match, if it is valid; otherwise -1.
     */
    override parseRow(context: CellRefPatternsContext, match: string): number {
        const row = parseRow(match);
        return (row <= context.maxRow) ? row : -1;
    }

    /**
     * Extracts a cell address from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param matchCol
     *  The matched column name.
     *
     * @param matchRow
     *  The matched row name.
     *
     * @returns
     *  A cell address for the passed matches, if the column and row indexes
     *  are valid; otherwise `undefined`.
     */
    override parseAddress(context: CellRefPatternsContext, matchCol: string, matchRow: string): Opt<Address> {
        const col = this.parseCol(context, matchCol);
        const row = this.parseRow(context, matchRow);
        return ((col >= 0) && (row >= 0)) ? new Address(col, row) : undefined;
    }

    /**
     * Returns a `CellRef` structure for the matches of a `COL_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `COL_REF` pattern in the passed groups.
     *
     * @param max
     *  Specifies which row index will be inserted into the cell reference. The
     *  value `false` will insert zero, the value `true` will insert the
     *  maximum row index.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the column index
     *  is valid; otherwise `undefined`.
     */
    override parseColRef(context: CellRefPatternsContext, groups: string[], index: number, max: boolean): Opt<CellRef> {
        const cellRef = new CellRef(0, max ? context.maxRow : 0, true, true);
        return cellRef.parseCol(groups[index + 1], groups[index], context.maxCol) ? cellRef : undefined;
    }

    /**
     * Returns a `CellRef` structure for the matches of a `ROW_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `ROW_REF` pattern in the passed groups.
     *
     * @param max
     *  Specifies which column index will be inserted into the cell reference.
     *  The value `false` will insert zero, the value `true` will insert the
     *  maximum column index.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the row index is
     *  valid; otherwise `undefined`.
     */
    override parseRowRef(context: CellRefPatternsContext, groups: string[], index: number, max: boolean): Opt<CellRef> {
        const cellRef = new CellRef(max ? context.maxCol : 0, 0, true, true);
        return cellRef.parseRow(groups[index + 1], groups[index], context.maxRow) ? cellRef : undefined;
    }

    /**
     * Returns a `CellRef` structure for the matches of a `CELL_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `CELL_REF` pattern in the passed groups.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the column and
     *  row indexes are valid; otherwise `undefined`.
     */
    override parseCellRef(context: CellRefPatternsContext, groups: string[], index: number): Opt<CellRef> {
        const cellRef = new CellRef(0, 0, true, true);
        // `CELL_REF` groups: [absColMarker, colName, absRowMarker, rowName]
        if (!cellRef.parseCol(groups[index + 1], groups[index], context.maxCol)) { return undefined; }
        if (!cellRef.parseRow(groups[index + 3], groups[index + 2], context.maxRow)) { return undefined; }
        return cellRef;
    }
}

// class CellRefPatternsRC ====================================================

/**
 * Configuration settings and helper methods for addresses in RC notation.
 */
class CellRefPatternsRC extends CellRefPatterns {

    /**
     * RE capturing group pattern for a single column index (absolute or
     * relative).
     *
     * Examples:
     * - `C4`, `C[-1]`, `C`.
     *
     * Capturing groups:
     * 1. The absolute column index.
     * 2. The relative column index without brackets.
     */
    override readonly COL_REF: string;

    /**
     * RE capturing group pattern for a single row index (absolute or
     * relative).
     *
     * Examples:
     * - `R3`, `R[-1]`, `R`.
     *
     * Capturing groups:
     * 1. The absolute row index.
     * 2. The relative row index without brackets.
     */
    override readonly ROW_REF: string;

    /**
     * RE capturing group pattern for a single cell address (absolute or
     * relative).
     *
     * Examples:
     * - `R3C4`, `R3C[-1]`, `R[1]C4`, `R[1]C[-1]`, `RC`.
     *
     * Capturing groups:
     * 1. The absolute row index.
     * 2. The relative row index without brackets.
     * 3. The absolute column index.
     * 4. The relative column index without brackets.
     */
    override readonly CELL_REF: string;

    // constructor ------------------------------------------------------------

    constructor(formulaResource: FormulaResource, localized: boolean) {
        super(formulaResource, localized);

        // RE pattern for an absolute index in RC notation, e.g. the numbers in R3C4.
        const ABS_INDEX = "\\d+";
        // RE capturing group for a relative index in RC notation, e.g. the numbers in R[-1]C[1].
        // Matches the empty brackets, as in R[]C[1], and the empty string, as in RC[1].
        const REL_INDEX = re.group.opt("\\[" + re.capture("-?\\d+") + "?\\]");
        // the prefix characters
        const [PREFIX_R, PREFIX_C] = formulaResource.getRCPrefixChars(localized);

        this.COL_REF  = PREFIX_C + re.group(re.capture(ABS_INDEX, this.refError), REL_INDEX);
        this.ROW_REF  = PREFIX_R + re.group(re.capture(ABS_INDEX, this.refError), REL_INDEX);
        this.CELL_REF = this.ROW_REF + this.COL_REF;
    }

    // public methods ---------------------------------------------------------

    /**
     * Extracts a column index from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match
     *  The matched column name of a regular expression.
     *
     * @returns
     *  The column for the passed match, if it is valid; otherwise -1.
     */
    override parseCol(context: CellRefPatternsContext, match: string): number {
        const col = parseInt(match, 10) - 1;
        return (col <= context.maxCol) ? col : -1;
    }

    /**
     * Extracts a row index from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param match
     *  The matched row name of a regular expression.
     *
     * @returns
     *  The row for the passed match, if it is valid; otherwise -1.
     */
    override parseRow(context: CellRefPatternsContext, match: string): number {
        const row = parseInt(match, 10) - 1;
        return (row <= context.maxRow) ? row : -1;
    }

    /**
     * Extracts a cell address from the passed matches of a regular expression.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param matchRow
     *  The matched row name.
     *
     * @param matchCol
     *  The matched column name.
     *
     * @returns
     *  A cell address for the passed matches, if the column and row indexes
     *  are valid; otherwise `null`.
     */
    override parseAddress(context: CellRefPatternsContext, matchRow: string, matchCol: string): Opt<Address> {
        const col = this.parseCol(context, matchCol);
        const row = this.parseRow(context, matchRow);
        return ((col >= 0) && (row >= 0)) ? new Address(col, row) : undefined;
    }

    /**
     * Returns a `CellRef` structure for the matches of a `COL_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `COL_REF` pattern in the passed groups.
     *
     * @param max
     *  Specifies which row index will be inserted into the cell reference. The
     *  value `false` will insert zero, the value `true` will insert the
     *  maximum row index.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the column index
     *  is valid; otherwise `undefined`.
     */
    override parseColRef(context: CellRefPatternsContext, groups: string[], index: number, max: boolean): Opt<CellRef> {
        const cellRef = new CellRef(0, max ? context.maxRow : 0, true, true);
        return cellRef.parseColRC(groups[index], groups[index + 1], context.maxCol, context.refAddress.c, context.wrapReferences) ? cellRef : undefined;
    }

    /**
     * Returns a `CellRef` structure for the matches of a `ROW_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `ROW_REF` pattern in the passed groups.
     *
     * @param max
     *  Specifies which column index will be inserted into the cell reference.
     *  The value `false` will insert zero, the value `true` will insert the
     *  maximum column index.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the row index is
     *  valid; otherwise `undefined`.
     */
    override parseRowRef(context: CellRefPatternsContext, groups: string[], index: number, max: boolean): Opt<CellRef> {
        const cellRef = new CellRef(max ? context.maxCol : 0, 0, true, true);
        return cellRef.parseRowRC(groups[index], groups[index + 1], context.maxRow, context.refAddress.r, context.wrapReferences) ? cellRef : undefined;
    }

    /**
     * Returns a `CellRef` structure for the matches of a `CELL_REF` pattern.
     *
     * @param context
     *  Document and formula specific settings.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the `CELL_REF` pattern in the passed groups.
     *
     * @returns
     *  A cell reference structure for the passed matches, if the column and
     *  row indexes are valid; otherwise `undefined`.
     */
    override parseCellRef(context: CellRefPatternsContext, groups: string[], index: number): Opt<CellRef> {
        const cellRef = new CellRef(0, 0, true, true);
        const { refAddress, maxCol, maxRow, wrapReferences: wrapRefs } = context;
        // `CELL_REF` groups: [absRow, relRow, absCol, relRol]
        if (!cellRef.parseRowRC(groups[index], groups[index + 1], maxRow, refAddress.r, wrapRefs)) { return undefined; }
        if (!cellRef.parseColRC(groups[index + 2], groups[index + 3], maxCol, refAddress.c, wrapRefs)) { return undefined; }
        return cellRef;
    }
}
