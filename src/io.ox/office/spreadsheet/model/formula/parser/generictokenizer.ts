/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re, ary } from "@/io.ox/office/tk/algorithms";

import { NumberParser } from "@/io.ox/office/editframework/model/formatter/parse/numberparser";

import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Matrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { type BaseToken, FixedToken, ParenthesisToken, SeparatorToken, OperatorToken, ScalarToken, MatrixToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import { WHITESPACE } from "@/io.ox/office/spreadsheet/model/formula/parser/genericpatterns";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { type TokenizerContext, Tokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/tokenizer";

// types ======================================================================

/**
 * Context object containing document-specific settings while the method
 * `GenericTokenizer#parse` of a globally cached formula parser is running.
 */
export interface GenericTokenizerContext extends TokenizerContext { }

// class GenericTokenizer =====================================================

/**
 * Helper tokenizer class implementing parsing basic formula tokens (literals,
 * operators, parentheses, whitespace, etc.).
 */
export class GenericTokenizer extends Tokenizer<GenericTokenizerContext, BaseToken> {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `GenericTokenizer`.
     *
     * @param formulaGrammar
     *  The formula grammar to be used.
     */
    static create(formulaGrammar: FormulaGrammar): GenericTokenizer {
        return new GenericTokenizer(formulaGrammar);
    }

    // properties -------------------------------------------------------------

    /**
     * The intermediate elements collected during parsing a matrix literal.
     * Also used as state indicator (`undefined` indicates top-level state,
     * existing array indicates matrix literal state).
     */
    #matrixValues: Opt<ScalarType[][]>;

    // constructor ------------------------------------------------------------

    /**
     * @param formulaGrammar
     *  The formula grammar to be used.
     */
    private constructor(formulaGrammar: FormulaGrammar) {
        super(formulaGrammar);

        // RE pattern for optional whitespace characters
        const OPT_WS = WHITESPACE + "*";

        // register initializer to reset local state
        this.addInitializer(() => {
            this.#matrixValues = undefined;
        });

        // opening/closing parentheses
        this.addPattern("\\(", () => new ParenthesisToken(true));
        this.addPattern("\\)", () => new ParenthesisToken(false));

        // whitespace (MUST be parsed before operators)
        this.addPattern(re.capture(WHITESPACE + "+"), ws => new FixedToken("ws", ws));

        // list/parameter separators (MUST be parsed before operators)
        this.addPattern(this.genericPatterns.SEPARATOR, () => new SeparatorToken());

        // unary/binary operators
        this.addPattern(this.genericPatterns.OPERATOR, op => {
            return new OperatorToken(formulaGrammar.getOperatorKey(op)!);
        });

        // string literals (also inside matrix literals)
        this.addPattern(this.genericPatterns.STRING_LITERAL, value => {
            return this.#processScalar(value.slice(1, -1).replace(/""/g, '"'));
        }, { states: ["start", "string", "matrix:open", "matrix:sep"] });

        // boolean literals (must be parsed before defined names) (also inside matrix literals)
        this.addPattern(this.genericPatterns.BOOLEAN_LITERAL + this.nameRefPatterns.TERMINATOR, value => {
            return this.#processScalar(formulaGrammar.getBooleanValue(value));
        }, { states: ["start", "boolean", "matrix:open", "matrix:sep"] });

        // error code literals (also inside matrix literals)
        this.addPattern(this.genericPatterns.ERROR_LITERAL + re.ahead.neg(this.nameRefPatterns.NAME_INNER_CHAR_CLASS), value => {
            return this.#processScalar(formulaGrammar.getErrorCode(value));
        }, { states: ["start", "error", "matrix:open", "matrix:sep"] });

        // fraction literals, e.g. `1 2/3` (before number literals!) (also signed inside matrix literals)
        this.addEntry(expr => {
            const parseResult = NumberParser.parseNumber(expr, { parseType: "fractional" });
            if (!parseResult || (parseResult.sign && !this.#matrixValues)) { return; }
            const tokenResult = this.#processScalar(parseResult.number);
            return tokenResult ? [parseResult.text, tokenResult] : undefined;
        }, { states: ["start", "fraction", "matrix:open", "matrix:sep"] });

        // floating-point literals (after fraction literals!) (ONLY inside matrix literals,
        // top-level numbers must be parsed after valid row ranges, e.g. `2:3` is a row range
        // instead of two numbers, but `2:(3)` is a number and a range operator)
        this.addEntry(expr => {
            const parseResult = NumberParser.parseNumber(expr, { decSep: formulaGrammar.DEC, parseType: "decimal" });
            if (!parseResult || (parseResult.sign && !this.#matrixValues)) { return; }
            const tokenResult = this.#processScalar(parseResult.number);
            return tokenResult ? [parseResult.text, tokenResult] : undefined;
        }, { states: ["number", "matrix:open", "matrix:sep"] });

        // matrix literals
        this.addPattern("\\{" + OPT_WS, () => {
            this.#matrixValues = [[]];
            return "matrix:open";
        }, { states: ["start", "matrix"] });

        // row separator in matrix literal
        this.addPattern(OPT_WS + re.escape(formulaGrammar.MAT_ROW) + OPT_WS, () => {
            this.#matrixValues!.push([]);
            return "matrix:sep";
        }, { states: "matrix:lit" });

        // column separator in matrix literal
        this.addPattern(OPT_WS + re.escape(formulaGrammar.MAT_COL) + OPT_WS, () => {
            return "matrix:sep";
        }, { states: "matrix:lit" });

        // optional closing brace in matrix literal
        this.addPattern(OPT_WS + re.capture("\\}", "$"), text => {
            if (!text && !this.context.autoCorrect) { throw new Error("missing closing brace in matrix"); }
            if (!this.#matrixValues!.every((row, idx, rows) => !idx || (row.length === rows[idx - 1].length))) {
                throw new Error("row size mismatch in matrix");
            }
            return new MatrixToken(new Matrix(this.#matrixValues!));
        }, { states: "matrix:lit" });
    }

    // private methods --------------------------------------------------------

    /**
     * Processes a scalar value according to parser state.
     * - Scalar mode: Returns a scalar token.
     * - Matrix mode: Pushes scalar value into matrix.
     */
    #processScalar(value: ScalarType): ScalarToken | string {
        if (this.#matrixValues) {
            ary.at(this.#matrixValues, -1)!.push(value);
            return "matrix:lit";
        }
        return new ScalarToken(value);
    }
}
