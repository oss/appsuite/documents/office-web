/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { TokenType, BaseToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import { FixedToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";

// class ParseToken ===========================================================

/**
 * A descriptor for a single formula token parsed from a formula expression
 * string, together with additional information such as the original display
 * text of the token.
 */
export class ParseToken<TokenT extends BaseToken = BaseToken> {

    /**
     * The formula token.
     */
    token: TokenT;

    /**
     * The array index of the formula token.
     */
    index = -1;

    /**
     * The display text of the token from the original formula expression.
     */
    text: string;

    /**
     * The start position of the text in the original formula expression.
     */
    start = 0;

    /**
     * The end position of the text in the original formula expression.
     */
    end = 0;

    // constructor ------------------------------------------------------------

    constructor(parseText: string, token: TokenT) {
        this.token = token;
        this.text = parseText;
    }

    // static functions -------------------------------------------------------

    /**
     * Creates a token descriptor containing a new fixed text token.
     *
     * @param tokenType
     *  The type of the fixed token to be inserted into the token descriptor.
     *
     * @param value
     *  The text representation of the fixed token in the parsed formula
     *  expression.
     *
     * @returns
     *  The token descriptor containing a new `FixedToken`.
     */
    static fixed(tokenType: TokenType, value: string): ParseToken<FixedToken> {
        return new ParseToken(value, new FixedToken(tokenType, value));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a text description of the token for debugging (!) purposes.
     */
    toString(): string {
        return this.token.toString();
    }
}
