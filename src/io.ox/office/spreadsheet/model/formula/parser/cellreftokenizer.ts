/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { type CellRefs, CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import type { SheetRefPatternsContext } from "@/io.ox/office/spreadsheet/model/formula/parser/sheetrefpatterns";
import type { CellRefPatternsContext } from "@/io.ox/office/spreadsheet/model/formula/parser/cellrefpatterns";
import { type TokenizerContext, Tokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/tokenizer";
import { ReferenceToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";

// types ======================================================================

/**
 * Context object containing document-specific settings while the method
 * `CellRefTokenizer#parse` of a globally cached formula parser is running.
 */
export interface CellRefTokenizerContext extends TokenizerContext, SheetRefPatternsContext, CellRefPatternsContext { }

// class CellRefTokenizer =====================================================

/**
 * Helper tokenizer class for different cell/range reference syntaxes.
 */
export abstract class CellRefTokenizer extends Tokenizer<CellRefTokenizerContext, ReferenceToken> {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `CellRefTokenizer`.
     *
     * @param formulaGrammar
     *  The formula grammar to be used. Contains the (native or localized) name
     *  of the #REF! error code used in broken references, and the localized
     *  prefixes for RC reference style.
     */
    static create(formulaGrammar: FormulaGrammar): CellRefTokenizer {
        const nativeOF = formulaGrammar.OF && !formulaGrammar.UI;
        return nativeOF ? new CellRefTokenizerOF(formulaGrammar) : new CellRefTokenizerUI(formulaGrammar);
    }

    // constructor ------------------------------------------------------------

    /**
     * @param formulaGrammar
     *  The formula grammar to be used. Contains the (native or localized) name
     *  of the #REF! error code used in broken references, and the localized
     *  prefixes for RC reference style.
     */
    protected constructor(formulaGrammar: FormulaGrammar) {
        super(formulaGrammar);
    }

    // protected methods ------------------------------------------------------

    /**
     * Creates a new cell reference token.
     *
     *
     * @param [cellRefs]
     *  The cell references.
     *
     * @param [sheetRefs]
     *  The sheet references.
     *
     * @returns
     *  The new `ReferenceToken`.
     */
    protected createReferenceToken(cellRefs?: CellRef | CellRefs, sheetRefs?: SheetRefs): ReferenceToken {

        const { docAccess, refSheet, extendSheet } = this.context;

        // bug 40293: add sheet reference to sheet-local cell references if specified
        if (!sheetRefs && extendSheet && (refSheet >= 0)) {
            sheetRefs = SheetRefs.single(refSheet, true);
        }

        return new ReferenceToken(docAccess, cellRefs, sheetRefs);
    }

    /**
     * Converts the matches of a regular expression to a `CellRefs` structure
     * for a cell range address.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the first `CELL_REF` pattern in the passed
     *  groups.
     *
     * @param [range=false]
     *  Whether to parse a second `CELL_REF` pattern for a column range.
     *
     * @returns
     *  A `CellRefs` structure, if the column and row indexes are all valid;
     *  otherwise `undefined`.
     */
    protected parseCells(groups: string[], index: number, range = false): Opt<CellRefs> {

        // parse the first cell reference, exit on error
        const r1 = this.cellRefPatterns.parseCellRef(this.context, groups, index);
        if (!r1) { return undefined; }

        // indicate single cell addresses by omitting property "r2"
        if (!range) { return { r1 }; }

        // parse the second cell reference, exit on error
        const r2 = this.cellRefPatterns.parseCellRef(this.context, groups, index + 4);
        return r2 && { r1, r2 };
    }

    /**
     * Converts the matches of a regular expression to a `CellRefs` structure
     * for a column range address.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the first `COL_REF` pattern in the passed
     *  groups.
     *
     * @param [range=false]
     *  Whether to parse a second `COL_REF` pattern for a column range.
     *
     * @returns
     *  A `CellRefs` structure, if the column indexes are all valid; otherwise
     *  `undefined`.
     */
    protected parseCols(groups: string[], index: number, range = false): Opt<CellRefs> {

        // parse the first column reference, exit on error
        const r1 = this.cellRefPatterns.parseColRef(this.context, groups, index, false);
        if (!r1) { return undefined; }

        // add end address to single column references
        if (!range) {
            return { r1, r2: new CellRef(r1.col, this.context.maxRow, r1.absCol, true) };
        }

        // parse the second column reference, exit on error
        const r2 = this.cellRefPatterns.parseColRef(this.context, groups, index + 2, true);
        return r2 && { r1, r2 };
    }

    /**
     * Converts the matches of a regular expression to a `CellRefs` structure
     * for a row range address.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param index
     *  The start array index of the first `ROW_REF` pattern in the passed
     *  groups.
     *
     * @param [range=false]
     *  Whether to parse a second `ROW_REF` pattern for a column range.
     *
     * @returns
     *  A `CellRefs` structure, if the row indexes are all valid; otherwise
     *  `undefined`.
     */
    protected parseRows(groups: string[], index: number, range = false): Opt<CellRefs> {

        // parse the first row reference, exit on error
        const r1 = this.cellRefPatterns.parseRowRef(this.context, groups, index, false);
        if (!r1) { return undefined; }

        // add end address to single row references
        if (!range) {
            return { r1, r2: new CellRef(this.context.maxCol, r1.row, true, r1.absRow) };
        }

        // parse the second row reference, exit on error
        const r2 = this.cellRefPatterns.parseRowRef(this.context, groups, index + 2, true);
        return r2 && { r1, r2 };
    }
}

// class CellRefTokenizerUI ===================================================

/**
 * Parser for cell references used in the GUI.
 */
class CellRefTokenizerUI extends CellRefTokenizer {

    /**
     * @param formulaGrammar
     *  The formula grammar to be used. Contains the (native or localized) name
     *  of the #REF! error code used in broken references, and the localized
     *  prefixes for RC reference style.
     */
    constructor(formulaGrammar: FormulaGrammar) {
        super(formulaGrammar);

        // helper RE patterns
        const { REF_ERROR } = this.genericPatterns;
        const { SHEET_PREFIX, SHEETS_PREFIX } = this.sheetRefPatterns;
        const { COL_REF, ROW_REF, CELL_REF } = this.cellRefPatterns;

        // cell range references (before single cell!), e.g. Sheet1!$A$1:$C$3
        this.#pushTokenizers(CELL_REF + ":" + CELL_REF, (groups, index) => this.parseCells(groups, index, true));
        // cell references, e.g. Sheet1!$A$1
        this.#pushTokenizers(CELL_REF, (groups, index) => this.parseCells(groups, index));

        // column range references (before single columns!), e.g. Sheet1!$A:$C
        this.#pushTokenizers(COL_REF + ":" + COL_REF, (groups, index) => this.parseCols(groups, index, true));
        // row range references (before single rows!), e.g. Sheet1!$1:$3
        this.#pushTokenizers(ROW_REF + ":" + ROW_REF, (groups, index) => this.parseRows(groups, index, true));

        // single column and row references (RC style only)
        if (this.formulaGrammar.RC) {
            // column references, e.g. Sheet1!C[1]
            this.#pushTokenizers(COL_REF, (groups, index) => this.parseCols(groups, index));
            // row references, e.g. Sheet1!R[1]
            this.#pushTokenizers(ROW_REF, (groups, index) => this.parseRows(groups, index));
        }

        // reference error with sheet, e.g. Sheet1!#REF!
        this.addPattern(SHEET_PREFIX + REF_ERROR, (...groups) => {
            return this.createReferenceToken(undefined, this.#parseSheet(groups));
        });

        // reference error in multiple sheets, e.g. Sheet1:Sheet3!#REF!
        this.addPattern(SHEETS_PREFIX + REF_ERROR, (...groups) => {
            return this.createReferenceToken(undefined, this.#parseSheets(groups));
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Pushes three tokenizers for the passed cell reference pattern:
     * - Local reference without sheet name (e.g. `$A$1`).
     * - Reference with single sheet name (e.g. `Sheet1!$A$1`).
     * - Reference with sheet range (e.g. `Sheet1:Sheet2!$A$1`).
     *
     * @param pattern
     *  The RE pattern for the cell address part in the reference.
     *
     * @param parseCellRefs
     *  The callback function to convert RE matches to a `CellRefs` structure.
     */
    #pushTokenizers(pattern: string, parseCellRefs: (groups: string[], index: number) => Opt<CellRefs>): void {

        // helper RE patterns
        const { SHEET_PREFIX, SHEETS_PREFIX } = this.sheetRefPatterns;
        const { TERMINATOR } = this.nameRefPatterns;

        // local reference, e.g. $A$1
        this.addPattern(pattern + TERMINATOR, (...groups) => {
            const cellRefs = parseCellRefs(groups, 0);
            return cellRefs && this.createReferenceToken(cellRefs);
        });

        // reference with sheet, e.g. Sheet1!$A$1
        this.addPattern(SHEET_PREFIX + pattern + TERMINATOR, (...groups) => {
            const cellRefs = parseCellRefs(groups, this.sheetRefPatterns.SHEET_GROUPS);
            return cellRefs && this.createReferenceToken(cellRefs, this.#parseSheet(groups));
        });

        // reference in multiple sheets, e.g. Sheet1:Sheet3!$A$1
        this.addPattern(SHEETS_PREFIX + pattern + TERMINATOR, (...groups) => {
            const cellRefs = parseCellRefs(groups, this.sheetRefPatterns.SHEETS_GROUPS);
            return cellRefs && this.createReferenceToken(cellRefs, this.#parseSheets(groups));
        });
    }

    /**
     * Checks that the passed `SheetRefs` structure is valid.
     *
     * @throws
     *  For unsupported references into external documents.
     */
    #validateSheets(sheetRefs: SheetRefs): SheetRefs {
        if (!(this.formulaGrammar.UI && sheetRefs.isExtDoc())) { return sheetRefs; }
        throw new Error("external document references not supported");
    }

    /**
     * Converts the matches of a regular expression to a `SheetRefs` structure
     * for a single sheet name.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @returns
     *  A `SheetRefs` structure for the passed matches.
     *
     * @throws
     *  For unsupported references into external documents.
     */
    #parseSheet(groups: string[]): SheetRefs {
        return this.#validateSheets(this.sheetRefPatterns.parseSheetRef(this.context, groups, 0, true));
    }

    /**
     * Converts the matches of a regular expression to a `SheetRefs` structure
     * for a a range of sheet names.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @returns
     *  A `SheetRefs` structure for the passed matches.
     *
     * @throws
     *  For unsupported references into external documents.
     */
    #parseSheets(groups: string[]): SheetRefs {

        if (this.formulaGrammar.OF) {
            const sheetRefs = this.sheetRefPatterns.parseSheetRef(this.context, groups, 0, false);
            sheetRefs.r2 = this.sheetRefPatterns.parseSheetRef(this.context, groups, 3, false).r1;
            return sheetRefs;
        }

        return this.#validateSheets(this.sheetRefPatterns.parseSheetRefs(this.context, groups, 0));
    }
}

// class CellRefTokenizerOF ===================================================

/**
 * Parser for native OpenFormula cell references.
 */
class CellRefTokenizerOF extends CellRefTokenizer {

    /**
     * @param formulaGrammar
     *  The formula grammar to be used. Contains the (native or localized) name
     *  of the #REF! error code used in broken references, and the localized
     *  prefixes for RC reference style.
     */
    constructor(formulaGrammar: FormulaGrammar) {
        super(formulaGrammar);

        // single cell address with sheet separator prefix
        const CELL_2D_REF = this.sheetRefPatterns.SHEET_SEP + this.cellRefPatterns.CELL_REF;
        // single sheet name (or #REF! error) and cell address with "." separator
        const CELL_3D_REF = this.sheetRefPatterns.SHEET_PREFIX + this.cellRefPatterns.CELL_REF;

        // encloses the passed RE pattern into literal brackets (OpenFormula reference syntax, e.g. `[.A1]`)
        const ref = (pattern: string): string => "\\[" + pattern + "\\]";

        // local cell reference, e.g. `[.$A$1]`
        this.addPattern(ref(CELL_2D_REF), (...groups) => {
            return this.createReferenceToken(this.parseCells(groups, 0));
        });

        // cell reference with sheet, e.g. `[$Sheet1.$A$1]`
        this.addPattern(ref(CELL_3D_REF), (...groups) => {
            return this.createReferenceToken(this.parseCells(groups, 4), this.#parseSheet(groups));
        });

        // local cell range reference, e.g. `[.$A$1:.$C$3]`
        this.addPattern(ref(CELL_2D_REF + ":" + CELL_2D_REF), (...groups) => {
            return this.createReferenceToken(this.parseCells(groups, 0, true));
        });

        // cell range reference with sheet, e.g. `[$Sheet1.$A$1:.$C$3]`
        this.addPattern(ref(CELL_3D_REF + ":" + CELL_2D_REF), (...groups) => {
            return this.createReferenceToken(this.parseCells(groups, 4, true), this.#parseSheet(groups));
        });

        // cell range reference in multiple sheets, e.g. `[$Sheet1.$A$1:$Sheet3.$C$3]`
        this.addPattern(ref(CELL_3D_REF + ":" + CELL_3D_REF), (...groups) => {
            const cellRefs: CellRefs = { r1: this.parseCells(groups, 4)?.r1, r2: this.parseCells(groups, 12)?.r1 };
            const sheetRefs = new SheetRefs(this.#parseSheet(groups, 0).r1, this.#parseSheet(groups, 8).r1);
            return this.createReferenceToken(cellRefs, sheetRefs);
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Converts the matches of a regular expression to a `SheetRefs` structure
     * for a single sheet name.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param [index=0]
     *  The start index of the `SHEET_PREFIX` pattern in the passed groups.
     */
    #parseSheet(groups: string[], index = 0): SheetRefs {
        return this.sheetRefPatterns.parseSheetRef(this.context, groups, index, true);
    }
}
