/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, itr, ary, map } from "@/io.ox/office/tk/algorithms";

import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import type { GenericPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/genericpatterns";
import type { SheetRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/sheetrefpatterns";
import type { CellRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/cellrefpatterns";
import type { NameRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/namerefpatterns";

// types ======================================================================

/**
 * An initializer callback function that will be invoked from the method
 * `Tokenizer#parse` before parsing a new formula subexpression.
 *
 * @param state
 *  The initial machine state passed to `Tokenizer#parse`.
 */
export type TokenizerInitializerFn = (state: string) => void;

/**
 * A callback function that tries to convert the beginning of a formula
 * expression to a parse token, or a state change of the tokenizer.
 *
 * @param expr
 *  The formula expression to be parsed.
 *
 * @returns
 *  A result tuple, or `undefined` if the callback function could not match
 *  anything. The tuple consists of two elements:
 *  1. The matched text.
 *  2. The resulting token, or a new machine state for the tokenizer.
 */
export type TokenizerEntryFn<TokenT extends object> = (expr: string) => Opt<[text: string, result: string | TokenT]>;

/**
 * A tokenizer callback function that processes the matched capturing groups of
 * a regular expression.
 *
 * @param groups
 *  The values of all capturing groups of the regular expression.
 *
 * @returns
 *  One of the following values:
 *  - The resulting token to be returned immediately.
 *  - A new machine state to continue with.
 *  - The value `undefined` to indicate to continue with the next tokenizer
 *    entry for the current machine state.
 *
 * @throws
 *  May throw an exception to indicate to abort parsing the formula due to an
 *  unrecoverable syntax error in the formula subexpression.
 */
export type TokenizerProcessFn<TokenT extends object> = (...groups: string[]) => Opt<TokenT | string>;

/**
 * Optional parameters for tokenizer entry registration.
 */
export interface TokenizerEntryOptions {

    /**
     * The machine states to be associated with a tokenizer entry. May be a
     * single state, or an array of states. Default value is the state "start"
     * (default value of the "state" parameter of method `Tokenizer#parse`).
     */
    states?: OrArray<string>;

    /**
     * Predicate function to conditionally exclude the tokenizer entry,
     * depending on some context state.
     */
    when?: () => boolean;
}

/**
 * Context object containing document-specific settings for an individual run
 * of the method `Tokenizer#parse`. Intended to be extended by subclasses of
 * `Tokenizer`.
 */
export interface TokenizerContext {

    /**
     * Whether to try to automatically correct simple syntax errors in formula
     * subexpressions, for example missing closing parentheses.
     */
    autoCorrect?: boolean;
}

/**
 * The result object of the method `Tokenizer#parse`.
 */
export interface TokenizerResult<TokenT extends object> {

    /**
     * The resulting token parsed from the beginning of the formula expression.
     */
    token: TokenT;

    /**
     * The length of the text representation of the token at the beginning of
     * the formula expression.
     */
    length: number;
}

// class Tokenizer ============================================================

/**
 * A generic tokenizer that implements parsing a subset of different available
 * formula tokens.
 */
export class Tokenizer<ContextT extends TokenizerContext, TokenT extends object> {

    /**
     * The formula grammar instance to be used by this tokenizer.
     */
    protected readonly formulaGrammar: FormulaGrammar;

    /**
     * RE pattern helper for basic formula tokens.
     */
    protected readonly genericPatterns: GenericPatterns;

    /**
     * RE pattern helper for sheet reference syntax.
     */
    protected readonly sheetRefPatterns: SheetRefPatterns;

    /**
     * RE pattern helper for address reference syntax.
     */
    protected readonly cellRefPatterns: CellRefPatterns;

    /**
     * RE pattern helper for name reference syntax.
     */
    protected readonly nameRefPatterns: NameRefPatterns;

    /**
     * Context metadata available while method `parse` is running.
     */
    protected context!: ContextT;

    /** All registered initializers. */
    readonly #initializers: TokenizerInitializerFn[] = [];
    /** All registered tokenizer entries, mapped by machine state. */
    readonly #registry = new Map<string, Array<TokenizerEntryFn<TokenT>>>();

    /** Current machine state of the tokenizer. */
    #state = "";

    // constructor ------------------------------------------------------------

    public constructor(formulaGrammar: FormulaGrammar) {
        this.formulaGrammar = formulaGrammar;
        this.genericPatterns = formulaGrammar.genericPatterns;
        this.sheetRefPatterns = formulaGrammar.sheetRefPatterns;
        this.cellRefPatterns = formulaGrammar.cellRefPatterns;
        this.nameRefPatterns = formulaGrammar.nameRefPatterns;
    }

    // public getters ---------------------------------------------------------

    /**
     * The current machine state of the tokenizer.
     */
    get state(): string {
        return this.#state;
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers an initializer callback function that will be invoked from the
     * method `parse` before parsing a new formula subexpression.
     *
     * @param fn
     *  The callback function to be invoked before parsing.
     */
    addInitializer(fn: (state: string) => void): void {
        this.#initializers.push(fn);
    }

    /**
     * Registers a new tokenizer entry, i.e. a callback function that tries to
     * parse the beginning of a formula expression.
     *
     * @param entry
     *  The matcher callback function.
     *
     * @param [options]
     *  Optional parameters.
     */
    addEntry(entry: TokenizerEntryFn<TokenT>, options?: TokenizerEntryOptions): void {
        const when = options?.when;
        const fn: typeof entry = when ? (expr => when.call(this) ? entry(expr) : undefined) : entry;
        for (const state of ary.wrap(options?.states ?? "start")) {
            map.upsert(this.#registry, state, () => []).push(fn);
        }
    }

    /**
     * Registers a new tokenizer based on a regular expression pattern string.
     *
     * @param pattern
     *  A pattern for a regular expression (without leading caret sign!). May
     *  be a nullish value or an empty string that will be skipped silently.
     *
     * @param process
     *  The processor callback function that will be invoked if the beginning
     *  of a formula expression was matched. Receives the matched text, and the
     *  capturing groups.
     *
     * @param [options]
     *  Optional parameters.
     */
    addPattern(pattern: Nullable<string>, process: TokenizerProcessFn<TokenT>, options?: TokenizerEntryOptions): void {
        if (pattern) {
            const re = new RegExp("^" + pattern, "i");
            this.addEntry(expr => {
                const matches = re.exec(expr);
                if (!matches) { return undefined; }
                const [text, ...groups] = matches;
                const result = process.apply(this, groups);
                return result ? [text, result] : undefined;
            }, options);
        }
    }

    /**
     * Matches the registered regular expressions against the beginning of the
     * passed formula expression, invokes the callback functions of matching
     * tokenizers, until the first returns a valid token object.
     *
     * @param expr
     *  The formula expression to be parsed.
     *
     * @param context
     *  Context metadata that will be available at protected property `context`
     *  in all methods called while this method is running.
     *
     * @param [state]
     *  The initial machine state to start parsing with.
     *
     * @returns
     *  The result object containing the resulting token, the text parsed from
     *  `expr`, and the remaining text to be parsed in `expr`.
     */
    parse(expr: string, context: ContextT, state = "start"): Opt<TokenizerResult<TokenT>> {

        // set the passed context
        this.context = context;
        // rescue original formula expression
        const origExpr = expr;

        // start with specified state
        this.#state = state;

        // invoke registered initializers
        for (const fn of this.#initializers) {
            fn.call(this, state);
        }

        // try to parse with different states
        for (;;) {

            // resolve the tokenizer entries for the passed state
            const entries = this.#registry.get(this.#state);
            if (!entries) { throw new Error("invalid machine state"); }

            // get the first non-nullish result (state or token)
            // eslint-disable-next-line @typescript-eslint/no-loop-func
            const result = itr.mapFirst(entries, fn => fn.call(this, expr));
            if (!result) { break; }

            // remove the matched token text from the formula expression
            expr = expr.slice(result[0].length);

            // continue to parse with a new machine state
            if (is.string(result[1])) {
                this.#state = result[1];
                continue;
            }

            // immediately return the resulting token
            return { token: result[1], length: origExpr.length - expr.length };
        }

        // no matching token found: restore old formula expression
        return undefined;
    }
}
