/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re, fun, debug } from "@/io.ox/office/tk/algorithms";
import { DObject, memoizeMethod } from "@/io.ox/office/tk/objects";

import { Address, stringifyCol, stringifyRow } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";
import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { SheetRef, SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { WHITESPACE } from "@/io.ox/office/spreadsheet/model/formula/parser/genericpatterns";
import type { CellRefPatternsContext } from "@/io.ox/office/spreadsheet/model/formula/parser/cellrefpatterns";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { type TokenizerContext, type TokenizerEntryOptions, Tokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/tokenizer";

// types ======================================================================

/**
 * Options for parsing strings to cell range lists.
 */
export interface ParseRangeListOptions {

    /**
     * If set to `true`, the parser will accept single commas, or single
     * semicolons as list separators. Nevertheless, the entire range list
     * expression must not use different separators. Default value is `false`.
     */
    extSep?: boolean;

    /**
     * If set to `true`, the parser will accept (and ignore!) arbitrary sheet
     * names in front of the cell ranges in the expression. By default, the
     * expression must not contain sheet names. Default value is `false`.
     */
    skipSheets?: boolean;
}

/**
 * Options for converting cell range lists to strings.
 */
export interface FormatRangeListOptions {

    /**
     * A sheet name that will be inserted for each cell range address according
     * to the formula grammar represented by this instance. By default, no
     * sheet name will be inserted into the string.
     */
    sheetName?: string;
}

// private types --------------------------------------------------------------

/**
 * Context object containing document-specific settings while the method
 * `RangeListParser#parse` of a globally cached parser instance is running.
 */
interface RangeListParserContext extends TokenizerContext, CellRefPatternsContext, Required<ParseRangeListOptions> {
    // access to document (address factory, sheet names)
    docAccess: IDocumentAccess;
    // cell ranges collected during parsing
    ranges: RangeArray;
    // current separator to be used in the entire range list (no mixed separators)
    lastSep: string;
}

// constants ==================================================================

const COL_REF   = re.capture("[a-z]+");
const ROW_REF   = re.capture("\\d+");
const COLS_REF  = COL_REF + ":" + COL_REF;
const ROWS_REF  = ROW_REF + ":" + ROW_REF;
const CELL_REF  = COL_REF + ROW_REF;
const RANGE_REF = CELL_REF + ":" + CELL_REF;

// class RangeListParser ======================================================

/**
 * An instance of this class parses range list expressions according to a
 * specific formula grammar. This is the base class of several class
 * specializations for the different grammars and reference syntaxes.
 *
 * @param grammar
 *  The formula grammar to be used to parse range list expressions.
 */
export class RangeListParser extends DObject {

    /**
     * Creates a new range list parser instance for the specified spreadsheet
     * document and formula grammar.
     *
     * @param formulaGrammar
     *  The formula grammar to be used to parse range list expressions.
     *
     * @returns
     *  A new range list parser for the specified document and formula grammar.
     */
    @memoizeMethod(fun.identity)
    static create(formulaGrammar: FormulaGrammar): RangeListParser {
        if (formulaGrammar.UI || formulaGrammar.RC) {
            throw new Error("cannot instantiate for UI grammars");
        }
        return new RangeListParser(formulaGrammar);
    }

    // properties -------------------------------------------------------------

    // formula grammar settings
    readonly #formulaGrammar: FormulaGrammar;
    // the tokenizer engine
    readonly #tokenizer: Tokenizer<TokenizerContext, RangeArray>;

    // current settings while method `parse` is running
    #context!: RangeListParserContext;

    // constructor ------------------------------------------------------------

    /**
     * @param formulaGrammar
     *  The formula grammar to be used to parse formula expressions.
     */
    private constructor(formulaGrammar: FormulaGrammar) {
        super({ _kind: "singleton" });

        // properties
        this.#formulaGrammar = formulaGrammar;
        this.#tokenizer = new Tokenizer(formulaGrammar);

        // bound methods as tokenizer callbacks
        const processSep = this.#processSep.bind(this);
        const acceptExtSep = (): boolean => this.#context.extSep;

        // accept sequences of various whitespace characters, use a SPACE as abstract separator
        this.#tokenizer.addPattern(WHITESPACE + "+", () => processSep(" "), { states: "range" });
        // accept other separators in extended mode only (only single characters, no sequences)
        this.#tokenizer.addPattern(re.capture(re.class(",;")), processSep, { states: "range", when: acceptExtSep });
        // terminate with the collected range array
        this.#tokenizer.addPattern("$", () => this.#context.ranges, { states: "range" });

        // bound methods as tokenizer callbacks
        const parseAddress = this.#parseAddress.bind(this);
        const parseRange = this.#parseRange.bind(this);
        const parseColRange = this.#parseColRange.bind(this);
        const parseRowRange = this.#parseRowRange.bind(this);
        const range2dOptions: TokenizerEntryOptions = { states: ["start", "sep"] };
        const range3dOptions: TokenizerEntryOptions = { ...range2dOptions, when: (): boolean => this.#context.skipSheets };

        // RE pattern for a single sheet name with sheet separator in 2D range lists to be ignored. No capturing groups.
        // Intentionally accepts most characters without having to quote the sheet name (fix for DOCS-4710, e.g. `Sheet-1.A1`).
        // Examples:
        // - `Sheet1!` in the expression `Sheet1!A1`.
        // - `'Sheet#2'!` in the expression `'Sheet#2'!A1`.
        const SHEET_PREFIX = re.group(re.class.neg(" .,;!'") + "+", re.group("'[^']*'") + "+") + formulaGrammar.sheetRefPatterns.SHEET_SEP;
        // RE pattern for a cell address with sheet without brackets, e.g. `Sheet1!A1`
        const CELL_3D_REF = SHEET_PREFIX + CELL_REF;

        // range address with sheet range (OF only, before 3D ranges!), e.g. `Sheet1.A1:Sheet2.C3`
        if (formulaGrammar.OF) {
            this.#tokenizer.addPattern(CELL_3D_REF + ":" + CELL_3D_REF, parseRange, range3dOptions);
        }

        // range address with sheet name (before cell addresses!), e.g. `Sheet1!A1:C3`
        this.#tokenizer.addPattern(SHEET_PREFIX + RANGE_REF, parseRange, range3dOptions);
        // cell address with sheet name, e.g. `Sheet1!A1`
        this.#tokenizer.addPattern(CELL_3D_REF, parseAddress, range3dOptions);

        // column/row intervals with sheet (OOX only)
        if (!formulaGrammar.OF) {
            // column interval with sheet name, e.g. `Sheet1!A:C`
            this.#tokenizer.addPattern(SHEET_PREFIX + COLS_REF, parseColRange, range3dOptions);
            // row interval with sheet name, e.g. `Sheet1!1:3`
            this.#tokenizer.addPattern(SHEET_PREFIX + ROWS_REF, parseRowRange, range3dOptions);
        }

        // local range address (before cell addresses!), e.g. `A1:C3`
        this.#tokenizer.addPattern(RANGE_REF, parseRange, range2dOptions);
        // local cell address, e.g. `A1`
        this.#tokenizer.addPattern(CELL_REF, parseAddress, range2dOptions);

        // local column/row intervals (OOX only)
        if (!formulaGrammar.OF) {
            // local column interval, e.g. `A:C`
            this.#tokenizer.addPattern(COLS_REF, parseColRange, range2dOptions);
            // local row interval, e.g. `1:3`
            this.#tokenizer.addPattern(ROWS_REF, parseRowRange, range2dOptions);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Parses the passed formula expression to a cell range list. The formula
     * is expected to contain range addresses separated by white-space
     * characters.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param formula
     *  The text to be parsed to a list of cell range addresses.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The cell range list parsed from the specified formula expression; or
     *  `undefined` on any parser error.
     */
    @formulaLogger.profileMethod("$badge{RangeListParser} parse")
    parse(docAccess: IDocumentAccess, formula: string, options?: ParseRangeListOptions): Opt<RangeArray> {
        formulaLogger.log(() => `formula="${formula}"`);

        // initialize context properties
        this.#context = {
            docAccess,
            ranges: new RangeArray(),
            lastSep: "",
            refAddress: Address.A1,
            maxCol: docAccess.addressFactory.maxCol,
            maxRow: docAccess.addressFactory.maxRow,
            wrapReferences: false,
            extSep: !!options?.extSep,
            skipSheets: !!options?.skipSheets
        };

        // parse the range list expression to a range array
        try {
            return this.#tokenizer.parse(formula, this.#context)?.token;
        } catch (error) {
            debug.logScriptError(error);
            return undefined;
        }
    }

    /**
     * Converts the passed cell range addresses to their text representation.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param ranges
     *  The cell ranges to be formatted to a string.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The text representation of the passed cell range addresses.
     */
    @formulaLogger.profileMethod("$badge{RangeListParser} format")
    format(docAccess: IDocumentAccess, ranges: Range[], options?: FormatRangeListOptions): string {

        // the sheet name to be inserted for each cell range address
        const sheetName = options?.sheetName;
        // pick the correct implementation method
        const formatRange = (this.#formulaGrammar.OF ? this.#formatRangeOF : this.#formatRangeOOX).bind(this);

        // convert the single range addresses in the passed array
        return RangeArray.cast(ranges).map(range => formatRange(docAccess, range, sheetName)).join(" ");
    }

    // private methods --------------------------------------------------------

    #processSep(sep: string): string {
        if (!this.#context.lastSep) {
            this.#context.lastSep = sep;
        } else if (this.#context.lastSep !== sep) {
            throw new Error("mixed list separators");
        }
        return "sep";
    }

    #processRange(range: Range | falsy): string {
        if (!range) { throw new Error("invalid cell range"); }
        this.#context.ranges.push(range);
        return "range";
    }

    /**
     * Extracts a cell address from the matches of a regular expression.
     *
     * @param groups
     *  The capturing groups of the regular expression.
     */
    #parseAddress(...groups: string[]): string {
        const { cellRefPatterns } = this.#formulaGrammar;
        const address = cellRefPatterns.parseAddress(this.#context, groups[0], groups[1]);
        return this.#processRange(address && new Range(address));
    }

    /**
     * Extracts a cell range address from the matches of a regular expression.
     *
     * @param groups
     *  The capturing groups of the regular expression.
     */
    #parseRange(...groups: string[]): string {
        const { cellRefPatterns } = this.#formulaGrammar;
        const address1 = cellRefPatterns.parseAddress(this.#context, groups[0], groups[1]);
        const address2 = cellRefPatterns.parseAddress(this.#context, groups[2], groups[3]);
        return this.#processRange(address1 && address2 && Range.fromAddresses(address1, address2));
    }

    /**
     * Extracts a cell range address for a column interval from the matches of
     * a regular expression.
     *
     * @param groups
     *  The capturing groups of the regular expression.
     */
    #parseColRange(...groups: string[]): string {
        const { cellRefPatterns } = this.#formulaGrammar;
        const col1 = (groups.length >= 1) ? cellRefPatterns.parseCol(this.#context, groups[0]) : -1;
        const col2 = (groups.length >= 2) ? cellRefPatterns.parseCol(this.#context, groups[1]) : col1;
        return this.#processRange((col1 >= 0) && (col2 >= 0) && Range.fromIndexes(col1, 0, col2, this.#context.maxRow));
    }

    /**
     * Extracts a cell range address for a row interval from the matches of a
     * regular expression.
     *
     * @param groups
     *  The capturing groups of the regular expression.
     */
    #parseRowRange(...groups: string[]): string {
        const { cellRefPatterns } = this.#formulaGrammar;
        const row1 = (groups.length >= 1) ? cellRefPatterns.parseRow(this.#context, groups[0]) : -1;
        const row2 = (groups.length >= 2) ? cellRefPatterns.parseRow(this.#context, groups[1]) : row1;
        return this.#processRange((row1 >= 0) && (row2 >= 0) && Range.fromIndexes(0, row1, this.#context.maxCol, row2));
    }

    /**
     * Returns the text representation of a single cell range address for OOX.
     */
    #formatRangeOOX(docAccess: IDocumentAccess, range: Range, sheetName?: string): string {

        // the text representation of the start and end address
        let a1Str: string;
        let a2Str: string | null = null;
        if (range.single()) {
            // single cell address (e.g. B3)
            a1Str = range.a1.toString();
        } else if (docAccess.addressFactory.isRowRange(range)) {
            // row interval (e.g. 3:5), preferred over column interval for entire sheet range
            a1Str = stringifyRow(range.a1.r);
            a2Str = stringifyRow(range.a2.r);
        } else if (docAccess.addressFactory.isColRange(range)) {
            // column interval (e.g. B:D)
            a1Str = stringifyCol(range.a1.c);
            a2Str = stringifyCol(range.a2.c);
        } else {
            // complete range address (e.g. B3:D5)
            a1Str = range.a1.toString();
            a2Str = range.a2.toString();
        }

        // the sheet reference structure
        const sheetRefs = sheetName ? SheetRefs.single(sheetName, false) : undefined;

        // generate the text representation with sheet names
        return this.#formulaGrammar.formatGenericRange(docAccess, a1Str, a2Str, sheetRefs);
    }

    /**
     * Returns the text representation of a single cell range address for OF.
     */
    #formatRangeOF(docAccess: IDocumentAccess, range: Range, sheetName?: string): string {

        // the text representation of the start address
        const a1Str = range.a1.toString();
        // the text representation of the end address
        const a2Str = range.single() ? null : range.a2.toString();

        // the sheet reference structure for the start and end address
        const sheet1Ref = sheetName ? new SheetRef(sheetName, false) : undefined;
        // the sheet reference structure for the end address
        const sheet2Ref = (sheetName && a2Str) ? sheet1Ref : undefined;

        // generate the text representation with sheet names
        const sheetRefs = new SheetRefs(sheet1Ref, sheet2Ref);
        return this.#formulaGrammar.formatGenericRange(docAccess, a1Str, a2Str, sheetRefs);
    }
}
