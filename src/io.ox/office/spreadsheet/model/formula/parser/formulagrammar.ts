/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, re } from "@/io.ox/office/tk/algorithms";
import { DObject, memoizeMethod } from "@/io.ox/office/tk/objects";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { autoFormatNumber } from "@/io.ox/office/editframework/model/formatter/autoformat";

import type { ScalarType, ErrorLiteral, ErrorKey } from "@/io.ox/office/spreadsheet/utils/scalar";
import { ScalarTypeId, ErrorCode, getScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { MAX_LENGTH_STANDARD_EDIT } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";
import { type SheetRef, type SheetRefs, encodeComplexSheetName } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { type CellRefs, CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { GenericPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/genericpatterns";
import { SheetRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/sheetrefpatterns";
import { CellRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/cellrefpatterns";
import { NameRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/namerefpatterns";

// types ======================================================================

/**
 * Configuration options for `FormulaGrammar` instances.
 */
export interface FormulaGrammarConfig {

    /**
     * Whether the formula grammar is used to handle formula expressions in the
     * user interface. Default value is `false`.
     */
    localized?: boolean;

    /**
     * Whether the formula grammar supports cell references in A1 notation
     * (`false`), or in RC notation (`true`). Default value is `false`.
     */
    rcStyle?: boolean;
}

/**
 * Optional parameters for the method `FormulaGrammar.isSimpleSheetName()`.
 */
export interface SimpleSheetNameOptions {

    /**
     * If set to `true`, the passed text may contain a leading simple file name
     * enclosed in brackets, e.g. `[doc.xlsx]Sheet1`, or even
     * `[path\to\doc.xlsx]Sheet1`.
     */
    external?: boolean;

    /**
     * If set to true, the passed text may contain a sheet range (two simple
     * sheet names separated by a colon), e.g. `Sheet1:Sheet2`.
     */
    range?: boolean;
}

// constants ==================================================================

// prefix for user-defined macro function in the OOXML file format
const UDF_PREFIX = "_xludf.";

// private functions ==========================================================

/**
 * Creates a unique key for the passed formula resource and formula grammar
 * configuration, that contains everything needed to identify a specific
 * `FormulaGrammar` instance.
 *
 * @param formulaResource
 *  The formula resource for a specific file format and locale.
 *
 * @param [config]
 *  Configuration options for the formula grammar.
 */
function grammarKey(formulaResource: FormulaResource, config?: FormulaGrammarConfig): string {
    return formulaResource.uid + ":" + (config?.localized ? "ui" : "op") + ":" + (config?.rcStyle ? "rc" : "a1");
}

/**
 * Returns a regular expression that matches strings in RC notation with
 * optional absolute row and column parts.
 *
 * @param rcPrefixChars
 *  The actual prefix characters for the row and column part. MUST be a string
 *  with exactly two characters (row and column).
 *
 * @returns
 *  A regular expression that matches absolute references in RC notation.
 *  Either part may be missing completely (references to entire columns or
 *  rows), or the prefix character may be present without index. Matching
 *  strings are for example `R2C3`, `R2C`, `RC3`, `RC`, `R2`, `C3`, `R`, `C`,
 *  or the empty string (!).
 */
function getSimpleRCRegExp(rcPrefixChars: string): RegExp {
    const rowPattern = re.group.opt(rcPrefixChars[0] + re.capture("\\d*"));
    const colPattern = re.group.opt(rcPrefixChars[1] + re.capture("\\d*"));
    return new RegExp("^" + rowPattern + colPattern + "$", "i");
}

// class FormulaGrammar =======================================================

/**
 * An instance of this class represents all settings of a formula grammar for a
 * specific file format and locale.
 */
export class FormulaGrammar extends DObject {

    // static functions -------------------------------------------------------

    /**
     * Returns an existing formula grammar instance; or creates a new instance
     * on demand, and stores it in the internal global cache of all formula
     * grammars.
     *
     * @param formulaResource
     *  The formula resource for a specific file format and locale.
     *
     * @param [config]
     *  Configuration options for the formula grammar.
     *
     * @returns
     *  The formula grammar instance.
     */
    @memoizeMethod(grammarKey)
    static create(formulaResource: FormulaResource, config?: FormulaGrammarConfig): FormulaGrammar {
        return new FormulaGrammar(formulaResource, config);
    }

    // properties -------------------------------------------------------------

    /**
     * Whether this grammar is for ODF documents.
     */
    readonly OF: boolean;

    /**
     * Whether the formula grammar is used to handle formula expressions in the
     * user interface.
     */
    readonly UI: boolean;

    /**
     * Whether this instance supports cell references in A1 notation (`false`),
     * or in RC notation (`true`).
     */
    readonly RC: boolean;

    /**
     * The decimal separator character.
     */
    readonly DEC: string;

    /**
     * The separator character in function parameter lists.
     */
    readonly SEP: string;

    /**
     * The separator character between rows in a matrix literal.
     */
    readonly MAT_ROW: string;

    /**
     * The separator character between row elements in a matrix literal.
     */
    readonly MAT_COL: string;

    /**
     * The string representation of the `#N/A` error code literal.
     */
    readonly NA_ERROR: string;

    /**
     * The string representation of the `#REF!` error code literal.
     */
    readonly REF_ERROR: string;

    /**
     * Whether the intersection operator is a space character.
     */
    readonly ISECT_SPACE: boolean;

    /**
     * The resource map for boolean literals.
     */
    readonly booleanMap: FormulaResource["booleanMap"];

    /**
     * The resource map for error code literals.
     */
    readonly errorMap: FormulaResource["errorMap"];

    /**
     * The resource map for formula operators.
     */
    readonly operatorMap: FormulaResource["operatorMap"];

    /**
     * The resource map for formula functions.
     */
    readonly functionMap: FormulaResource["functionMap"];

    /**
     * RexExp pattern helper for basic formula tokens.
     */
    readonly genericPatterns: GenericPatterns;

    /**
     * RexExp pattern helper for sheet reference syntax.
     */
    readonly sheetRefPatterns: SheetRefPatterns;

    /**
     * RexExp pattern helper for address reference syntax.
     */
    readonly cellRefPatterns: CellRefPatterns;

    /**
     * RexExp pattern helper for defined name and function name syntax.
     */
    readonly nameRefPatterns: NameRefPatterns;

    /** RC style prefix characters. */
    readonly #RC_PREFIX_CHARS: string | null;
    /** Matches localized simple RC references. */
    readonly #SIMPLE_RCREF_RE: RegExp;
    /** Matches English simple RC references. */
    readonly #SIMPLE_RCREF_EN_RE: Opt<RegExp>;
    /** Matches a valid label for a defined name. */
    readonly #VALID_NAME_RE: RegExp;

    // constructor ------------------------------------------------------------

    private constructor(formulaResource: FormulaResource, config?: FormulaGrammarConfig) {
        super({ _kind: "singleton" });

        // general properties
        this.OF = formulaResource.fileFormat === FileFormatType.ODF;
        this.UI = !!config?.localized;
        this.RC = !!config?.rcStyle;

        // separator characters
        this.DEC = formulaResource.getDec(this.UI);
        this.SEP = formulaResource.getSeparator(this.UI);
        this.MAT_ROW = (this.OF || this.UI) ? "|" : ";";
        this.MAT_COL = (this.OF || this.UI) ? ";" : ",";

        // the resource maps
        this.booleanMap = formulaResource.booleanMap;
        this.errorMap = formulaResource.errorMap;
        this.operatorMap = formulaResource.operatorMap;
        this.functionMap = formulaResource.functionMap;

        // special handling for space character used as operator
        this.ISECT_SPACE = this.operatorMap.getName("isect", this.UI) === " ";

        // specific error codes used in regular expressions, or when generating text representations
        this.NA_ERROR = this.errorMap.getName("NA", this.UI)!;
        this.REF_ERROR = this.errorMap.getName("REF", this.UI)!;

        // create the RE pattern helpers
        this.genericPatterns  = GenericPatterns.create(formulaResource, this.UI);
        this.sheetRefPatterns = SheetRefPatterns.create(formulaResource, this.UI);
        this.cellRefPatterns  = CellRefPatterns.create(formulaResource, this.UI, this.RC);
        this.nameRefPatterns  = NameRefPatterns.create(formulaResource, this.UI);

        // prefix characters for RC notation
        const rcPrefixCharsUI = formulaResource.getRCPrefixChars(this.UI);
        const rcPrefixCharsOP = formulaResource.getRCPrefixChars(false);

        // private regular expressions needed in class methods
        this.#RC_PREFIX_CHARS = this.RC ? rcPrefixCharsUI : null;
        this.#SIMPLE_RCREF_RE = getSimpleRCRegExp(rcPrefixCharsUI);
        this.#SIMPLE_RCREF_EN_RE = (rcPrefixCharsUI !== rcPrefixCharsOP) ? getSimpleRCRegExp(rcPrefixCharsOP) : undefined;
        this.#VALID_NAME_RE = new RegExp("^" + this.nameRefPatterns.DEFINED_NAME + "$", "i");
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed text is a reserved symbol according to this
     * formula grammar.
     *
     * Boolean literals, and strings that look like cell addresses are
     * considered to be reserved symbols. Cell addresses include the
     * representation of a relative cell reference in A1 notation, or a cell
     * reference in RC notation (either English, e.g. `R3C4`, or according to
     * this formula grammar, e.g. `Z3S4` in the German UI grammar).
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param text
     *  The text to be checked.
     *
     * @returns
     *  Whether the passed text is a reserved symbol according to this formula
     *  grammar.
     */
    isReservedSymbol(docAccess: IDocumentAccess, text: string): boolean {
        return this.#isBooleanLiteral(text) || this.#isReferenceSymbol(docAccess, text);
    }

    /**
     * Returns whether the passed sheet name is a simple sheet name, i.e. it
     * does not need to be enclosed in a pair of apostrophes in formula
     * expressions. Simple sheet names do not contain any reserved characters
     * (e.g. whitespace, or operators used in formulas), and do not look like
     * other reserved names (e.g. boolean literals, or cell references).
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param text
     *  The text to be checked.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  Whether the passed sheet name is a simple sheet name.
     */
    isSimpleSheetName(docAccess: IDocumentAccess, text: string, options?: SimpleSheetNameOptions): boolean {

        // the result of the regular expression test
        const matches = this.sheetRefPatterns.RE_SIMPLE_EXT_SHEET_NAME.exec(text);
        if (!matches) { return false; }

        // without "external" flag, external document name is not allowed
        if (matches[1] && !options?.external) { return false; }

        // without "range" flag, second sheet name is not allowed
        if (matches[3] && !options?.range) { return false; }

        // reserved symbols are not simple sheet names
        if (this.isReservedSymbol(docAccess, matches[2])) { return false; }
        if (matches[3] && this.isReservedSymbol(docAccess, matches[3])) { return false; }

        return true;
    }

    /**
     * Checks if the passed string can be used as label for a defined name.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param text
     *  The string to be checked.
     *
     * @returns
     *  The empty string, if the passed text is a valid label for a defined
     *  name; otherwise one of the following error codes:
     *  - `"name:empty"`: The passed label is empty.
     *  - `"name:invalid"`: The passed label contains invalid characters.
     *  - `"name:address"`: The passed label would be a valid, but conflicts
     *    with the representation of a relative cell reference in A1 notation,
     *    or a cell reference in RC notation (either English, e.g. `R3C4`, or
     *    according to current UI language, e.g. `Z3S4` in German).
     */
    validateNameLabel(docAccess: IDocumentAccess, text: string): string {

        // the passed string must not be empty
        if (text.length === 0) { return "name:empty"; }

        // check that the passed string does not contain invalid characters
        if (!this.#VALID_NAME_RE.test(text)) { return "name:invalid"; }

        // check that the text does not look like a boolean literal (TODO: own error code?)
        if (!this.OF && this.#isBooleanLiteral(text)) { return "name:invalid"; }

        // bug 38786: check that the text does not look like a cell address
        if (this.#isReferenceSymbol(docAccess, text)) { return "name:address"; }

        // the passed text is a valid label for a defined name
        return "";
    }

    /**
     * Converts the passed scalar value to its text representation in formula
     * expressions according to this formula grammar.
     *
     * @param value
     *  The value to be converted.
     *
     * @returns
     *  The string representation of the passed value.
     */
    formatScalar(value: ScalarType): string {

        switch (getScalarType(value)) {

            case ScalarTypeId.NUMBER: {
                const num  = value as number;
                return !Number.isFinite(num) ? this.getErrorName(ErrorCode.NUM)! : math.isZero(num) ? "0" :
                    autoFormatNumber(num, { dec: this.DEC, stdLen: MAX_LENGTH_STANDARD_EDIT }).text!;
            }

            case ScalarTypeId.STRING:
                // duplicate inner quotes, enclose in quotes
                return `"${(value as string).replace(/"/g, '""')}"`;

            case ScalarTypeId.BOOLEAN:
                return this.getBooleanName(value as boolean);

            case ScalarTypeId.ERROR:
                return this.getErrorName(value as ErrorLiteral) || this.NA_ERROR;

            case ScalarTypeId.NULL:
                return "";

            default:
                formulaLogger.warn("$badge{FormulaGrammar} formatScalar: unsupported scalar value type");
                return "";
        }
    }

    /**
     * A generic formatter for a cell reference that takes preformatted cell
     * address components, and adds the sheet names of the specified sheet
     * references according to the formula grammar of this instance.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param startStr
     *  The text representation of the start cell address (e.g. the text "A1"
     *  for the reference `Sheet1!A1:C3`).
     *
     * @param endStr
     *  The text representation of the end cell address (e.g. the text "C3" for
     *  the reference `Sheet1!A1:C3`). If set to `null` or to an empty string,
     *  a single cell address will be generated (e.g. `Sheet1!A1`).
     *
     * @param [sheetRefs]
     *  The sheet references to be added to the formatted reference text.
     *
     * @returns
     *  The string representation of the cell range reference.
     */
    formatGenericRange(docAccess: IDocumentAccess, startStr: string, endStr: string | null, sheetRefs?: SheetRefs): string {

        // native OpenFormula syntax: sheets of a sheet interval will be added individually
        // to the start and end addresses of the range, e.g.: `Sheet1.A1:Sheet2.B2`
        if (this.OF && !this.UI) {

            // the first sheet name with separator character (simple #REF! error for sheet reference errors)
            const r1 = sheetRefs?.r1;
            const sheet1Prefix = this.#generateOFSheetPrefix(docAccess, r1);
            if (sheet1Prefix === null) { return this.REF_ERROR; }

            // the second sheet name with separator character (simple #REF! error for sheet reference errors)
            const r2 = sheetRefs?.r2;
            const sheet2Prefix = r2 ? this.#generateOFSheetPrefix(docAccess, r2) : "";
            if (sheet2Prefix === null) { return this.REF_ERROR; }

            // the first sheet name with separator character, and the first cell address
            let result = sheet1Prefix + startStr;

            // add second cell address for sheet ranges, or for cell ranges (repeat
            // single cell address in a sheet range, e.g. Sheet1.A1:Sheet2.A1)
            if (sheet2Prefix || endStr) {
                result += `:${sheet2Prefix}${endStr || startStr}`;
            }

            return result;
        }

        // start with all sheet names before the cell range address, e.g. Sheet1:Sheet2!A1:C3
        // (sheet reference errors will result in a simple #REF! error without anything else)
        const sheetPrefix = this.#generateSheetRangePrefix(docAccess, sheetRefs);
        return (sheetPrefix === null) ? this.REF_ERROR : `${sheetPrefix}${startStr}${endStr ? `:${endStr}` : ""}`;
    }

    /**
     * Converts the passed reference structures to the text representation of a
     * complete cell range reference in formula expressions according to this
     * formula grammar.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param refAddress
     *  The reference address needed to create column/row offsets as used in RC
     *  notation, e.g. `R[1]C[-1]`.
     *
     * @param [cellRefs]
     *  The cell references to be added to the formatted reference text.
     *
     * @param [sheetRefs]
     *  The sheet references to be added to the formatted reference text.
     *
     * @returns
     *  The string representation of the cell range reference.
     */
    formatReference(docAccess: IDocumentAccess, refAddress: Address, cellRefs?: CellRefs | CellRef, sheetRefs?: SheetRefs): string {

        // shortcuts to properties
        const { REF_ERROR } = this;
        const rcPrefixChars = this.#RC_PREFIX_CHARS;
        const { addressFactory } = docAccess;

        // RC notation not supported in native formula grammars (UI only)
        if (!this.UI && rcPrefixChars) {
            formulaLogger.error("$badge{FormulaGrammar} formatReference: RC notation not supported in native grammar");
            return this.NA_ERROR;
        }

        // destructure the cell references
        const cr1 = (cellRefs instanceof CellRef) ? cellRefs : cellRefs?.r1;
        const cr2 = (cellRefs instanceof CellRef) ? undefined : cellRefs?.r2;

        // native OpenFormula syntax: references are enclosed in brackets, absolute markers for
        // sheets, sheet names are separated by periods, e.g.: `[$Sheet1.A1:$'Sheet 2'.B2]`
        if (this.OF && !this.UI) {

            // in OpenFormula, reference errors will be shown as simple #REF! error without sheet reference
            if (!cr1) { return "[" + REF_ERROR + "]"; }

            // the text representation of the start address
            let startStr = cr1.refText();
            // start address will always contain a leading period, also without sheet name
            if (!sheetRefs?.r1) { startStr = "." + startStr; }

            // the text representation of the end address
            let endStr = "";
            if (cr2) {
                endStr = cr2.refText();
                // end address will always contain a leading period, also without sheet name
                if (!sheetRefs?.r2) { endStr = "." + endStr; }
            }

            // enclose the entire reference into brackets
            return "[" + this.formatGenericRange(docAccess, startStr, endStr, sheetRefs) + "]";
        }

        // no valid range: generate a #REF! error with (optional) leading sheet name (ODF: simple #REF! error only)
        if (!cr1) {
            return this.OF ? REF_ERROR : this.formatGenericRange(docAccess, REF_ERROR, null, sheetRefs);
        }

        // the cell range address (with adjusted column/row indexes)
        const range = Range.fromAddresses(cr1.toAddress(), cr2?.toAddress());
        // the text representation of the start and end cell address
        let startStr: string, endStr = "";

        if (cr1.absCol && cr2?.absCol && addressFactory.isRowRange(range)) {
            // generate row interval (preferred over column interval for entire sheet range)
            startStr = rcPrefixChars ? cr1.rowTextRC(rcPrefixChars, refAddress) : cr1.rowText();
            if (!rcPrefixChars || !range.singleRow() || (cr1.absRow !== cr2.absRow)) {
                endStr = rcPrefixChars ? cr2.rowTextRC(rcPrefixChars, refAddress) : cr2.rowText();
            }
        } else if (cr1.absRow && cr2?.absRow && addressFactory.isColRange(range)) {
            // generate column interval
            startStr = rcPrefixChars ? cr1.colTextRC(rcPrefixChars, refAddress) : cr1.colText();
            if (!rcPrefixChars || !range.singleCol() || (cr1.absCol !== cr2.absCol)) {
                endStr = rcPrefixChars ? cr2.colTextRC(rcPrefixChars, refAddress) : cr2.colText();
            }
        } else {
            // generate range or cell address
            startStr = rcPrefixChars ? cr1.refTextRC(rcPrefixChars, refAddress) : cr1.refText();
            if (cr2) {
                endStr = rcPrefixChars ? cr2.refTextRC(rcPrefixChars, refAddress) : cr2.refText();
            }
        }

        return this.formatGenericRange(docAccess, startStr, endStr, sheetRefs);
    }

    /**
     * Converts the passed sheet reference and label of a defined name to the
     * text representation of a complete defined name reference in formula
     * expressions according to this formula grammar.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param label
     *  The label of the defined name.
     *
     * @param [sheetRefs]
     *  The sheet reference (e.g. the particle "Sheet1" in `Sheet1!name`). If
     *  omitted, no sheet name will be inserted into the returned string.
     *
     * @returns
     *  The string representation of the defined name.
     */
    formatName(docAccess: IDocumentAccess, label: string, sheetRefs?: SheetRefs): string {

        // no external names in ODF files
        if (this.OF && sheetRefs?.isExtDoc()) { return this.NA_ERROR; }

        // sheet reference error will be replaced with #REF! error
        const sheetPrefix = this.#generateSheetRangePrefix(docAccess, sheetRefs);
        return (sheetPrefix === null) ? this.REF_ERROR : (sheetPrefix + label);
    }

    /**
     * Converts the passed sheet reference and macro function name to the text
     * representation of a complete macro call in formula expressions according
     * to this formula grammar.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param label
     *  The name of a macro function.
     *
     * @param [sheetRefs]
     *  The sheet reference (e.g. the particle "Sheet1" in the formula
     *  `Sheet1!macro()`). If omitted, no sheet name will be inserted into the
     *  returned string.
     *
     * @returns
     *  The string representation of the macro call.
     */
    formatMacro(docAccess: IDocumentAccess, label: string, sheetRefs?: SheetRefs): string {

        // no external or sheet-local macros in ODF files
        if (this.OF && sheetRefs && (sheetRefs.r1 || sheetRefs.isExtDoc())) {
            return this.NA_ERROR;
        }

        // macro names equal to the internal name of a built-in function need to be prefixed
        // Example: the formula `=SUMME(1)+SUM(1)` in a German UI results in the expression `SUM(1)+_xludf.SUM(1)`
        const hasUdfPrefix = (UDF_PREFIX.length < label.length) && label.toLowerCase().startsWith(UDF_PREFIX);

        // add a missing prefix for operation grammar, or remove existing prefix for UI grammar
        if (this.UI && hasUdfPrefix) {
            label = label.slice(UDF_PREFIX.length);
        } else if (!this.UI && !hasUdfPrefix && this.functionMap.getKey(label, false)) {
            label = UDF_PREFIX + label;
        }

        // format the macro call like a defined name
        return this.formatName(docAccess, label, sheetRefs);
    }

    /**
     * Returns the name of the passed boolean value.
     *
     * @param value
     *  The boolean value to be converted to its string representation.
     *
     * @returns
     *  The name of the passed boolean value.
     */
    getBooleanName(value: boolean): string {
        return this.booleanMap.getName(value ? "t" : "f", this.UI)!;
    }

    /**
     * Converts the passed name of a boolean value to the boolean value.
     *
     * @param name
     *  The name of a boolean value (case-insensitive).
     *
     * @returns
     *  The boolean value for the passed name of a boolean value; or `null`, if
     *  the passed string is not the name of a boolean value.
     */
    getBooleanValue(name: string): boolean | null {
        const key = this.booleanMap.getKey(name, this.UI);
        return key ? (key === "t") : null;
    }

    /**
     * Returns the name of the passed error code.
     *
     * @param error
     *  The error code to be converted to its string representation.
     *
     * @returns
     *  The name of the passed error code; or `null`, if the passed error code
     *  cannot be resolved to a name.
     */
    getErrorName(error: ErrorLiteral): string | null {
        return this.errorMap.getName(error.key as ErrorKey, this.UI);
    }

    /**
     * Converts the passed error name to an error code literal.
     *
     * @param name
     *  The name of an error code (case-insensitive).
     *
     * @returns
     *  The error code instance for the passed error name; or `null`, if the
     *  passed string is not the name of a supported error code.
     */
    getErrorCode(name: string): ErrorLiteral | null {
        const key = this.errorMap.getKey(name, this.UI);
        return key ? ErrorCode[key] : null;
    }

    /**
     * Returns the name of an operator for the passed unique resource key.
     *
     * @param key
     *  The unique resource key of an operator.
     *
     * @returns
     *  The operator name for the passed resource key; or `null`, if the passed
     *  resource key cannot be resolved to the name of an operator.
     */
    getOperatorName(key: string): string | null {
        return this.operatorMap.getName(key, this.UI);
    }

    /**
     * Converts the passed operator name to the unique resource key of the
     * operator.
     *
     * @param name
     *  The name of an operator.
     *
     * @returns
     *  The resource key for the passed operator name; or `null`, if the passed
     *  string is not the name of a supported operator.
     */
    getOperatorKey(name: string): string | null {
        return this.operatorMap.getKey(name, this.UI);
    }

    /**
     * Returns the name of a function for the passed unique resource key.
     *
     * @param key
     *  The unique resource key of a function.
     *
     * @returns
     *  The function name for the passed resource key; or `null`, if the passed
     *  resource key cannot be resolved to the name of a function.
     */
    getFunctionName(key: string): string | null {
        return this.functionMap.getName(key, this.UI);
    }

    /**
     * Converts the passed function name to the unique resource key of the
     * function.
     *
     * @param name
     *  The name of a function (case-insensitive).
     *
     * @returns
     *  The resource key for the passed function name; or `null`, if the passed
     *  string is not the name of a supported function.
     */
    getFunctionKey(name: string): string | null {
        return this.functionMap.getKey(name, this.UI);
    }

    /**
     * Generates the formula expression for a generic subtotal formula.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param funcKey
     *  The resource key of a function.
     *
     * @param range
     *  The address of the cell range to be inserted into the formula. If set
     *  to `null`, a formula without cell range address will be created (e.g.
     *  `=SUM()`).
     *
     * @returns
     *  The formula expression of the subtotal formula.
     */
    generateAutoFormula(docAccess: IDocumentAccess, funcKey: string, range: Range | null, refAddress: Address): string {
        const funcName = this.getFunctionName(funcKey);
        if (!range) { return `${funcName}()`; }
        const r1 = new CellRef(range.a1.c, range.a1.r, false, false);
        const r2 = range.single() ? undefined : new CellRef(range.a2.c, range.a2.r, false, false);
        return `${funcName}(${this.formatReference(docAccess, refAddress, { r1, r2 })})`;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether the passed text is a boolean literal according to this
     * formula grammar.
     *
     * @param text
     *  The text to be checked.
     *
     * @returns
     *  Whether the passed text is a boolean literal.
     */
    #isBooleanLiteral(text: string): boolean {
        return this.booleanMap.getKey(text, this.UI) !== null;
    }

    /**
     * Returns whether the passed text is the representation of a relative cell
     * reference in A1 notation, or a cell reference in RC notation (either
     * English, e.g. `R3C4`, or according to this formula grammar, e.g. `Z3S4`
     * in the German UI grammar).
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param text
     *  The text to be checked.
     *
     * @returns
     *  Whether the passed text is the representation of a cell reference in A1
     *  or RC notation.
     */
    #isReferenceSymbol(docAccess: IDocumentAccess, text: string): boolean {

        // empty strings are no valid reference
        if (text.length === 0) { return false; }

        // cell address in A1 notation
        const address = docAccess.addressFactory.parseAddress(text);
        if (address) { return true; }

        // cell address in localized/native RC notation (regardless of the grammar)
        const matches = this.#SIMPLE_RCREF_RE.exec(text) ?? this.#SIMPLE_RCREF_EN_RE?.exec(text);
        if (!matches) { return false; }

        // extract column and row index (row before column in RC notation!)
        const row = matches[1] ? (parseInt(matches[1], 10) - 1) : 0;
        const col = matches[2] ? (parseInt(matches[2], 10) - 1) : 0;

        // column and row index must be valid in the document
        return docAccess.addressFactory.isValidAddress(new Address(col, row));
    }

    /**
     * Encodes the passed sheet name, if it is a complex name, and adds an
     * absolute marker if needed.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param sheetName
     *  The sheet name.
     *
     * @param abs
     *  Whether the sheet reference is absolute.
     *
     * @returns
     *  The encoded sheet name. Complex sheet names will be enclosed in
     *  apostrophes. If the absolute flag has been set, and the sheet name is
     *  not empty, a dollar sign will be added before the (encoded) sheet name.
     */
    #encodeAbsSheetName(docAccess: IDocumentAccess, sheetName: string, abs: boolean): string {

        // use the #REF! error code for invalid sheets, enclose complex sheet names in apostrophes
        if (sheetName && !this.isSimpleSheetName(docAccess, sheetName)) {
            sheetName = encodeComplexSheetName(sheetName);
        }

        // add the leading absolute marker in ODF mode
        return ((abs && sheetName) ? "$" : "") + sheetName;
    }

    /**
     * Returns the string representation of the passed sheet references for
     * regular token syntax, i.e. as range of sheet names, and with an
     * exclamation mark as sheet name separator.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param [sheetRefs]
     *  The sheet references.
     *
     * @returns
     *  The string representation for valid sheet references, with a trailing
     *  exclamation mark; the value `null`, if the sheet references are invalid
     *  (deleted sheet); or an empty string, if no sheet references have been
     *  passed (sheet-local reference).
     */
    #generateSheetRangePrefix(docAccess: IDocumentAccess, sheetRefs?: SheetRefs): string | null {

        // start with the external document reference
        let result = sheetRefs?.doc ? `[${sheetRefs.doc.index}]` : "";

        // empty string for missing sheet reference
        const r1 = sheetRefs?.r1;
        if (r1) {

            // convert first sheet name
            const sheet1Name = r1.resolve(docAccess, sheetRefs?.doc);
            if (sheet1Name === null) { return null; }

            // convert second sheet name
            const r2 = sheetRefs.r2;
            const sheet2Name = (r2 && !r1.equals(r2)) ? r2.resolve(docAccess, sheetRefs.doc) : "";
            if (sheet2Name === null) { return null; }

            // the resulting sheet names
            result += this.OF ? this.#encodeAbsSheetName(docAccess, sheet1Name, r1.abs) : sheet1Name;
            if (sheet2Name) {
                result += `:${this.OF ? this.#encodeAbsSheetName(docAccess, sheet2Name, r2!.abs) : sheet2Name}`;
            }

            // OOXML: enclose sheet range in apostrophes, if one of the sheet names is complex
            if (!this.OF && (!this.isSimpleSheetName(docAccess, sheet1Name) || (sheet2Name && !this.isSimpleSheetName(docAccess, sheet2Name)))) {
                result = encodeComplexSheetName(result);
            }
        }

        return result ? `${result}!` : "";
    }

    /**
     * Returns the string representation of the passed sheet reference for the
     * OpenFormula token syntax, i.e. with absolute sheet marker.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param sheetRef
     *  The sheet reference.
     *
     * @returns
     *  The string representation for the passed sheet reference; the value
     *  `null`, if the sheet reference is invalid (deleted sheet); or an empty
     *  string, if no sheet reference has been passed (sheet-local reference).
     */
    #generateOFSheetPrefix(docAccess: IDocumentAccess, sheetRef: Opt<SheetRef>): string | null {

        // empty string for missing sheet reference
        if (!sheetRef) { return ""; }

        // convert reference to sheet name, use the #REF! error code for invalid sheets,
        // enclose complex sheet names in apostrophes
        const sheetName = sheetRef.resolve(docAccess);
        return sheetName ? `${this.#encodeAbsSheetName(docAccess, sheetName, sheetRef.abs)}.` : null;
    }
}
