/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { insertAttribute } from "@/io.ox/office/editframework/utils/attributeutils";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import type { ResolveTokenOptions, BaseToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import type { BaseTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";

// types ======================================================================

/**
 * A callback function that will be invoked with the updated formula expression
 * of a formula update task.
 *
 * @param newExpr
 * The new formula expression to be inserted into the spreadsheet document.
 *
 * @param oldExpr
 *  The old (current) formula expression.
 */
export type TransformFormulaResultFn = (newExpr: string, oldExpr: string) => void;

/**
 * Optional parameters for transforming formula expressions.
 */
export interface TransformFormulaOptions {

    /**
     * If set to `true`, the transformed formula expression will be notified
     * even if it does not differ from the original formula expression. Useful
     * in GUI code where formulas will be transformed after the changes have
     * already been applied in the document model. Default value is `false`.
     */
    notifyAlways?: boolean;
}

// class FormulaUpdateTask ====================================================

/**
 * Abstract base class for a "formula update task", the representation of a
 * specific spreadsheet document change operation (e.g. a deleted sheet) that
 * needs refreshing all its formula expressions.
 */
export abstract class FormulaUpdateTask {

    readonly config?: ResolveTokenOptions;

    // constructor ------------------------------------------------------------

    protected constructor(config?: ResolveTokenOptions) {
        this.config = config;
    }

    // public methods ---------------------------------------------------------

    /**
     * Resolves the old and new formula expressions for the passed token array
     * and formula grammar.
     *
     * @param tokenArray
     *  The token array to be updated.
     *
     * @param grammar
     *  The formula grammar to be used to create the formula expressions.
     *
     * @param [resultFn]
     *  A callback function that will be invoked with the old and new formula
     *  expressions, if they differ.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new formula expression; or `undefined`, if it did not change.
     */
    transformFormula(tokenArray: BaseTokenArray, grammar: FormulaGrammar, resultFn?: TransformFormulaResultFn, options?: TransformFormulaOptions): Opt<string> {

        // check the token array (do not waste time to iterate the tokens if the checker returns false)
        if (tokenArray.empty() || !this.prepareTokens(tokenArray)) { return undefined; }

        // the reference sheet of the token array
        const refSheet = tokenArray.getRefSheet();

        // the current string representation of the formula
        let oldExpr = "";
        // the new string representation of the formula with moved cell references
        let newExpr = "";

        // collect the old and new text representation of the formula (no early exit!!!)
        tokenArray.tokens.forEach(token => {
            const oldToken = token.getText(grammar);
            const newToken = this.resolveToken(token, grammar, refSheet);
            oldExpr += oldToken;
            newExpr += newToken ?? oldToken;
        });

        // whether the formula expression has been transformed
        const changed = options?.notifyAlways || (oldExpr !== newExpr);

        // invoke the callback function if formula expressions are different
        if (changed && resultFn) { resultFn(newExpr, oldExpr); }

        // return the resulting string
        return changed ? newExpr : undefined;
    }

    /**
     * Resolves the formula expressions for the passed token array, and inserts
     * them to the passed operation property objects.
     *
     * @param tokenArray
     *  The token array to be updated.
     *
     * @param grammar
     *  The formula grammar to be used to create the formula expressions.
     *
     * @param propName
     *  The name of the property to be inserted into the passed operation
     *  property objects.
     *
     * @param operProps
     *  The operation properties that will receive the new formula expression.
     *
     * @param [undoProps]
     *  If specified, the undo operation properties that will receive the
     *  current formula expression.
     */
    transformOpProperty<T extends object>(tokenArray: BaseTokenArray, grammar: FormulaGrammar, propName: keyof T, operProps: T, undoProps?: T): void;
    transformOpProperty(tokenArray: BaseTokenArray, grammar: FormulaGrammar, propName: string, operProps: Dict, undoProps?: Dict): void;
    // implementation
    transformOpProperty(tokenArray: BaseTokenArray, grammar: FormulaGrammar, propName: string, operProps: Dict, undoProps?: Dict): void {
        this.transformFormula(tokenArray, grammar, (newExpr, oldExpr) => {
            operProps[propName] = newExpr;
            if (undoProps) { undoProps[propName] = oldExpr; }
        });
    }

    /**
     * Resolves the formula expressions of the passed token array, and inserts
     * them to the passed formatting attribute sets.
     *
     * @param tokenArray
     *  The token array to be updated.
     *
     * @param grammar
     *  The formula grammar to be used to create the formula expressions.
     *
     * @param attrFamily
     *  The family of the attribute to be changed.
     *
     * @param attrName
     *  The name of the attribute to be changed.
     *
     * @param operAttrSet
     *  An (incomplete) attribute set that will receive the new formula
     *  expression.
     *
     * @param [undoAttrSet]
     *  If specified, the (incomplete) undo attribute set that will receive the
     *  current formula expression.
     */
    transformOpAttribute(tokenArray: BaseTokenArray, grammar: FormulaGrammar, attrFamily: string, attrName: string, operAttrSet: Dict, undoAttrSet?: Dict): void {
        this.transformFormula(tokenArray, grammar, (newExpr, oldExpr) => {
            insertAttribute(operAttrSet, attrFamily, attrName, newExpr);
            if (undoAttrSet) { insertAttribute(undoAttrSet, attrFamily, attrName, oldExpr); }
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses implement a pre-check for the passed token array whether it
     * contains any tokens that would need to be updated for the respective
     * formula update task (performance optimization).
     *
     * @param tokenArray
     *  The token array to be updated by this formula update task.
     *
     * @returns
     *  Whether the token array contains any tokens that would need to be
     *  updated for this formula update task.
     */
    protected abstract prepareTokens(tokenArray: BaseTokenArray): boolean;

    /**
     * Subclasses implement to resolve the updated string representation of the
     * passed formula token.
     *
     * @param token
     *  The formula token to be updated.
     *
     * @param grammar
     *  The formula grammar to be used to create the formula expressions.
     *
     * @param refSheet
     *  The index of the reference sheet the passed token is related to.
     *
     * @returns
     *  The new string representation of the token, if it will change; or the
     *  value `null` to indicate that the token remains the same.
     */
    protected abstract resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null;
}

// class DeleteSheetUpdateTask ================================================

/**
 * Formula update task for a deleted sheet in the spreadsheet document. All
 * references in all formula expressions containing the deleted sheet become
 * invalid and will be replaced with `#REF!` errors.
 *
 * This update task must run before actually deleting the sheet (it must be
 * possible to recognize cell/name references using the sheet when parsing
 * formula expressions)!
 */
export class DeleteSheetUpdateTask extends FormulaUpdateTask {

    readonly sheet: number;

    // constructor ------------------------------------------------------------

    /**
     * @param sheet
     *  The index of the deleted sheet.
     */
    constructor(sheet: number) {
        super();
        this.sheet = sheet;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        return tokenArray.hasAnySheetTokens() || tokenArray.hasToken("table");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
        return token.resolveDeletedSheet(grammar, this.sheet, refSheet);
    }
}

// class RenameSheetUpdateTask ================================================

/**
 * Formula update task for a renamed or cloned sheet in the spreadsheet
 * document. All references in all formula expressions containing the renamed
 * sheet will be updated.
 *
 * This update task must run before actually renaming the sheet (it must be
 * possible to recognize cell/name references using the sheet when parsing
 * formula expressions)!
 */
export class RenameSheetUpdateTask extends FormulaUpdateTask {

    readonly sheet: number;
    readonly sheetName: string;
    readonly tableNames: Opt<Dict<string>>;

    // constructor ------------------------------------------------------------

    /**
     * @param sheet
     *  The index of the renamed or cloned sheet.
     *
     * @param sheetName
     *  The new name of the renamed or cloned sheet.
     *
     * @param [tableNames]
     *  A map that specifies how to change the table names in table references.
     *  Used when cloning an existing sheet where the cloned table ranges have
     *  been renamed, and the formulas in the cloned sheet should refer to the
     *  new table ranges. The old table names are the map keys, and the new
     *  table names are the map values. If omitted, no table references will be
     *  changed (when renaming an existing sheet).
     */
    constructor(sheet: number, sheetName: string, tableNames?: Dict<string>) {
        super();
        this.sheet = sheet;
        this.sheetName = sheetName;
        this.tableNames = tableNames;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        return tokenArray.hasAnySheetTokens() || tokenArray.hasToken("table");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar): string | null {
        return token.resolveRenamedSheet(grammar, this.sheet, this.sheetName, this.tableNames);
    }
}

// class RelabelNameUpdateTask ================================================

/**
 * Formula update task for changing the label of a defined name. All formula
 * expressions referring to the defined name will be updated to use the new
 * label.
 */
export class RelabelNameUpdateTask extends FormulaUpdateTask {

    readonly sheet: OptSheet;
    readonly oldLabel: string;
    readonly newLabel: string;

    // constructor ------------------------------------------------------------

    /**
     * @param sheet
     *  The index of the sheet containing the relabeled defined name; or `null`
     *  for a globally defined name.
     *
     * @param oldLabel
     *  The old label of the relabeled defined name.
     *
     * @param newLabel
     *  The new label of the relabeled defined name.
     */
    constructor(sheet: OptSheet, oldLabel: string, newLabel: string) {
        super();
        this.sheet = sheet;
        this.oldLabel = oldLabel;
        this.newLabel = newLabel;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        return tokenArray.hasToken("name");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
        return token.resolveRelabeledName(grammar, this.sheet, this.oldLabel, this.newLabel, refSheet);
    }
}

// class RenameTableUpdateTask ================================================

/**
 * Formula update task for changing the name of a table range. All formula
 * expressions referring to the table range will be updated to use the new
 * name.
 */
export class RenameTableUpdateTask extends FormulaUpdateTask {

    readonly oldName: string;
    readonly newName: string;

    // constructor ------------------------------------------------------------

    /**
     * @param oldName
     *  The old name of the renamed table range.
     *
     * @param newName
     *  The new name of the renamed table range.
     */
    constructor(oldName: string, newName: string) {
        super();
        this.oldName = oldName;
        this.newName = newName;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        return tokenArray.hasToken("table");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar): string | null {
        return token.resolveRenamedTable(grammar, this.oldName, this.newName);
    }
}

// class RelocateRangesUpdateTask =============================================

/**
 * Formula update task for relocating the relative references in a formula.
 */
export class RelocateRangesUpdateTask extends FormulaUpdateTask {

    readonly #refAddress: Address;
    readonly #targetAddress: Address;
    #wrapReferences = false;

    // constructor ------------------------------------------------------------

    /**
     * @param refAddress
     *  The source address for relocation.
     *
     * @param targetAddress
     *  The target address for relocation.
     */
    constructor(refAddress: Address, targetAddress: Address) {
        super();
        this.#refAddress = refAddress;
        this.#targetAddress = targetAddress;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        this.#wrapReferences = tokenArray.wrapReferences;
        return tokenArray.hasToken("ref");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
        return token.resolveRelocateRange(grammar, this.#refAddress, this.#targetAddress, refSheet, this.#wrapReferences);
    }
}

// class MoveCellsUpdateTask ==================================================

/**
 * Formula update task for moving a range of cells in a sheet. This includes
 * inserting or deleting some columns or rows. All affected references in all
 * formula expressions will be updated.
 */
export class MoveCellsUpdateTask extends FormulaUpdateTask {

    readonly sheet: number;
    readonly transformer: AddressTransformer;

    readonly #refAddress: Address;
    readonly #targetAddress: Address;
    #freezeRelRefs = false;

    // constructor ------------------------------------------------------------

    /**
     * @param sheet
     *  The index of the sheet containing the moved cells.
     *
     * @param transformer
     *  An address transformer specifying how to transform the cell references
     *  after cells have been moved in the sheet.
     */
    constructor(sheet: number, transformer: AddressTransformer);
    constructor(sheet: number, transformer: AddressTransformer, refAddress: Address, targetAddress: Address);
    // implementation
    constructor(sheet: number, transformer: AddressTransformer, refAddress?: Address, targetAddress?: Address) {
        super();
        this.sheet = sheet;
        this.transformer = transformer;
        this.#refAddress = refAddress ?? Address.A1;
        this.#targetAddress = targetAddress ?? Address.A1;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        this.#freezeRelRefs = tokenArray.formulaType === "name";
        return tokenArray.hasToken("ref") || tokenArray.hasToken("table");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
        return token.resolveMovedCells(grammar, this.sheet, this.transformer, this.#refAddress, this.#targetAddress, refSheet, this.#freezeRelRefs);
    }
}

// class CutPasteUpdateTask ===================================================

/**
 * Formula update task for pasting cells from clipboard that have been cut.
 * This includes recalculation of all dependent references.
 */
export class CutPasteUpdateTask extends FormulaUpdateTask {

    readonly sourceSheet: number;
    readonly sourceRange: Range;
    readonly targetRange: Range;

    // constructor ------------------------------------------------------------

    /**
     * @param sourceSheet
     *  The index of the sheet the cells have been cut from.
     *
     * @param sourceRange
     *  The address of the cell range that has been cut to clipboard.
     *
     * @param targetRange
     *  The address of the cell range to be pasted to.
     */
    constructor(sourceSheet: number, sourceRange: Range, targetRange: Range, config?: ResolveTokenOptions) {
        super(config);
        this.sourceSheet = sourceSheet;
        this.sourceRange = sourceRange;
        this.targetRange = targetRange;
    }

    // protected methods ------------------------------------------------------

    protected prepareTokens(tokenArray: BaseTokenArray): boolean {
        return tokenArray.hasToken("ref");
    }

    protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
        return token.resolveCutPasteCells(grammar, this.sourceSheet, this.sourceRange, this.targetRange, refSheet);
    }
}
