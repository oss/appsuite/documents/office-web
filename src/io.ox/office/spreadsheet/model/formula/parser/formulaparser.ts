/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, ary, debug } from "@/io.ox/office/tk/algorithms";
import { memoizeMethod } from "@/io.ox/office/tk/objects";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { WrapReferencesOptions, UnqualifiedTablesOptions } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";
import { formulaLogger } from "@/io.ox/office/spreadsheet/model/formula/utils/logger";
import { ParseToken } from "@/io.ox/office/spreadsheet/model/formula/parser/parsetoken";
import type { ParseSheetRefOptions } from "@/io.ox/office/spreadsheet/model/formula/parser/sheetrefpatterns";
import { type GenericTokenizerContext, GenericTokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/generictokenizer";
import { type CellRefTokenizerContext, CellRefTokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/cellreftokenizer";
import { type NameRefTokenizerContext, NameRefTokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/namereftokenizer";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { isOpToken, isWsToken, ScalarToken, OperatorToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";

// types ======================================================================

/**
 * Options for parsing formula expressions.
 */
export interface ParseFormulaOptions extends ParseSheetRefOptions, WrapReferencesOptions, UnqualifiedTablesOptions {

    /**
     * If set to `true`, an incomplete formula will be auto-corrected, e.g.
     * missing closing parentheses at the end will be added. Default value is
     * `false`.
     */
    autoCorrect?: boolean;
}

/**
 * Context object containing document-specific settings while the method
 * `FormulaParser#parse` of a globally cached formula parser is running.
 */
export interface FormulaParserContext extends GenericTokenizerContext, CellRefTokenizerContext, NameRefTokenizerContext { }

// class FormulaParser ========================================================

/**
 * An instance of this class parses formula expressions according to a specific
 * formula grammar. This is the base class of several class specializations for
 * the different grammars and reference syntaxes.
 */
export class FormulaParser {

    /**
     * Creates a new formula expression parser instance for the specified
     * spreadsheet document and formula grammar.
     *
     * @param formulaGrammar
     *  The formula grammar to be used to parse formula expressions.
     *
     * @returns
     *  A new formula expression parser for the specified document and formula
     *  grammar.
     */
    @memoizeMethod(fun.identity)
    static create(formulaGrammar: FormulaGrammar): FormulaParser {
        return new FormulaParser(formulaGrammar);
    }

    // properties -------------------------------------------------------------

    // formula grammar settings
    readonly #formulaGrammar: FormulaGrammar;

    // tokenizers for different kinds of tokens
    readonly #genericTokenizer: GenericTokenizer;
    readonly #cellRefTokenizer: CellRefTokenizer;
    readonly #nameRefTokenizer: NameRefTokenizer;

    // constructor ------------------------------------------------------------

    /**
     * @param formulaGrammar
     *  The formula grammar to be used to parse formula expressions.
     */
    private constructor(formulaGrammar: FormulaGrammar) {

        // formula grammar
        this.#formulaGrammar = formulaGrammar;

        // initialize token parsers
        this.#genericTokenizer = GenericTokenizer.create(formulaGrammar);
        this.#cellRefTokenizer = CellRefTokenizer.create(formulaGrammar);
        this.#nameRefTokenizer = NameRefTokenizer.create(formulaGrammar);
    }

    // public methods ---------------------------------------------------------

    /**
     * Parses and tokenizes the passed complete formula expression.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param formula
     *  The formula expression to be parsed and tokenized.
     *
     * @param refAddress
     *  The reference address used to resolve relative cell references.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  An array of token descriptors containing the parsed formula tokens, and
     *  the original text representations of the tokens.
     */
    @formulaLogger.profileMethod("$badge{FormulaParser} parse")
    parse(docAccess: IDocumentAccess, formula: string, refAddress: Address, options?: ParseFormulaOptions): Array<Readonly<ParseToken>> {
        formulaLogger.log(() => 'formula="' + formula + '"');

        // initialize context properties
        const context: FormulaParserContext = {
            docAccess,
            refSheet: options?.refSheet ?? -1,
            refAddress,
            maxCol: docAccess.addressFactory.maxCol,
            maxRow: docAccess.addressFactory.maxRow,
            wrapReferences: !!options?.wrapReferences,
            extendSheet: !!options?.extendSheet,
            unqualifiedTables: !!options?.unqualifiedTables,
            autoCorrect: !!options?.autoCorrect
        };

        // the descriptors of the resulting formula tokens
        const parseTokens: ParseToken[] = [];

        // extract all supported tokens from start of formula string
        while (formula) {

            // tokenizers will throw exceptions for recognized unfixable parse errors
            // (this stops the entire parse process for better performance)
            try {

                // try all tokenizers to extract a token
                const result =
                    // generic tokens shared by all grammars
                    this.#genericTokenizer.parse(formula, context) ??
                    // any type of cell reference without and with sheets
                    this.#cellRefTokenizer.parse(formula, context) ??
                    // number literal (must be parsed after valid row ranges, e.g. `2:3` is a row
                    // range instead of two numbers, but `2:(3)` is a number and a range operator)
                    this.#genericTokenizer.parse(formula, context, "number") ??
                    // defined names, table ranges, and function calls
                    this.#nameRefTokenizer.parse(formula, context);

                // push recognized token into the result array
                if (result) {
                    const parseText = formula.slice(0, result.length);
                    parseTokens.push(new ParseToken(parseText, result.token));
                    formula = formula.slice(result.length);
                    continue;
                }
            } catch (error) {
                debug.logScriptError(error);
            }

            // push a "bad" token, and immediately stop the tokenizer loop
            parseTokens.push(ParseToken.fixed("bad", formula));
            break;
        }
        formulaLogger.logTokens("tokenize", parseTokens);

        // whether the intersection operator is a space and needs post-processing
        const spaceAsIsect = this.#formulaGrammar.ISECT_SPACE;

        // post-process the token array (from last to first for in-place modifications)
        for (const [index, parseToken] of ary.entries(parseTokens, { reverse: true })) {

            // the current token
            const { token } = parseToken;
            // the token preceding and following the current token
            const prevParseToken: Opt<ParseToken> = parseTokens[index - 1];
            const nextParseToken: Opt<ParseToken> = parseTokens[index + 1];

            // replace combination of "op[sub] lit[number]" with a negative number, if the
            // minus is the leading token or preceded by an operator or opening parenthesis
            if (isOpToken(token, "sub") && (nextParseToken?.token instanceof ScalarToken)) {

                // the scalar value of the next token
                const nextValue = nextParseToken.token.value;
                if (is.number(nextValue) && (nextValue > 0)) {

                    // the next non-whitespace token preceding the current token
                    const prevNonWsParseToken = isWsToken(prevParseToken?.token) ? parseTokens[index - 2] : prevParseToken;
                    if (!prevNonWsParseToken || prevNonWsParseToken.token.isType("op", "sep", "open")) {
                        parseToken.token = new ScalarToken(-nextValue);
                        parseToken.text += nextParseToken.text;
                        ary.deleteAt(parseTokens, index + 1);
                    }
                }
            }

            // if the range intersection operator is a space character, insert an operator
            // token for a whitespace token containing a SPACE character and surrounded
            // by reference/name tokens or other subexpressions in parentheses
            if (spaceAsIsect && isWsToken(token)) {

                const spaceIndex = parseToken.text.indexOf(" ");
                if ((spaceIndex >= 0) && prevParseToken?.token.isType("ref", "name", "close") && nextParseToken?.token.isType("ref", "name", "open", "func")) {

                    // insert the remaining white-space following the first SPACE character
                    if (spaceIndex + 1 < parseToken.text.length) {
                        const trailingSpace = parseToken.text.slice(spaceIndex + 1);
                        ary.insertAt(parseTokens, index + 1, ParseToken.fixed("ws", trailingSpace));
                    }

                    // insert an intersection operator for the SPACE character after the current token
                    ary.insertAt(parseTokens, index + 1, new ParseToken(" ", new OperatorToken("isect")));

                    // shorten the current white-space token to the text preceding the first SPACE character, or remove it
                    if (spaceIndex > 0) {
                        token.setValue(parseToken.text.slice(0, spaceIndex));
                        parseToken.text = token.value;
                    } else {
                        ary.deleteAt(parseTokens, index);
                    }
                }
            }
        }

        formulaLogger.logTokens("postprocess", parseTokens);

        // add text positions to the parse tokens
        for (const [index, parseToken] of parseTokens.entries()) {
            parseToken.index = index;
            parseToken.start = (index === 0) ? 0 : parseTokens[index - 1].end;
            parseToken.end = parseToken.start + parseToken.text.length;
        }

        return parseTokens;
    }
}
