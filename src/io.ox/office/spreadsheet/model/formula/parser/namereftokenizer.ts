/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re, itr } from "@/io.ox/office/tk/algorithms";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { type DocRef, SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import type { TableRegionKey } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { WHITESPACE } from "@/io.ox/office/spreadsheet/model/formula/parser/genericpatterns";
import type { SheetRefPatternsContext } from "@/io.ox/office/spreadsheet/model/formula/parser/sheetrefpatterns";
import { type TokenizerContext, Tokenizer } from "@/io.ox/office/spreadsheet/model/formula/parser/tokenizer";
import { type TableTokenConfig, DefinedNameToken, TableToken, FunctionToken, MacroToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";

// types ======================================================================

/**
 * All token classes that refer to a formula token with a name.
 */
export type AnyNameToken = DefinedNameToken | TableToken | FunctionToken | MacroToken;

/**
 * Context object containing document-specific settings while the method
 * `NameRefTokenizer#parse` of a globally cached formula parser is running.
 */
export interface NameRefTokenizerContext extends TokenizerContext, SheetRefPatternsContext {
    refAddress: Address;
    unqualifiedTables: boolean;
}

// private types --------------------------------------------------------------

/**
 * Intermediate settings collected while parsing a structured table reference.
 */
interface TableParseData {
    tableName: string;
    tableConfig: TableTokenConfig;
    regionKeys: Set<TableRegionKey>;
}

// class NameRefTokenizer =====================================================

/**
 * Helper tokenizer class for different reference syntaxes for defined names,
 * structured table references, and function calls.
 */
export class NameRefTokenizer extends Tokenizer<NameRefTokenizerContext, AnyNameToken> {

    // static functions -------------------------------------------------------

    /**
     * Creates an instance of a matching internal implementation subclass of
     * `NameRefTokenizer`.
     *
     * @param formulaGrammar
     *  The formula grammar to be used. Contains the (native or localized) name
     *  of the #REF! error code used in broken sheet references.
     */
    static create(formulaGrammar: FormulaGrammar): NameRefTokenizer {
        // no subclasses needed (yet)
        return new NameRefTokenizer(formulaGrammar);
    }

    // properties -------------------------------------------------------------

    // intermediate configuration for a table token
    #tableData!: TableParseData;

    // constructor ------------------------------------------------------------

    /**
     * @param formulaGrammar
     *  The formula grammar to be used. Contains the (native or localized) name
     *  of the #REF! error code used in broken sheet references.
     */
    protected constructor(formulaGrammar: FormulaGrammar) {
        super(formulaGrammar);

        // helper RE patterns
        const { DOC_PREFIX, SHEET_PREFIX, DOC_GROUPS, SHEET_GROUPS } = this.sheetRefPatterns;
        const { TERMINATOR, DEFINED_NAME, FUNC_NAME, TABLE_OPEN } = this.nameRefPatterns;

        // RE pattern for defined name with terminator
        const NAME_REF = DEFINED_NAME + TERMINATOR;
        // RE pattern for table references with opening bracket for regions
        const TABLE_REF = formulaGrammar.OF ? undefined : (DEFINED_NAME + TABLE_OPEN);

        // RE pattern for function name with terminator (native OpenFormula allows whitespace before parenthesis)
        const FUNC_WS = (formulaGrammar.OF && !formulaGrammar.UI) ? (WHITESPACE + "*") : "";
        const FUNC_REF = FUNC_NAME + FUNC_WS + re.ahead("\\(");

        // external global references with document prefix
        if (DOC_PREFIX) {

            // macro call with document prefix, e.g. `[1]!my_macro()`
            this.addPattern(DOC_PREFIX + FUNC_REF, (...groups) => {
                return this.#createFunctionToken(groups[DOC_GROUPS], this.#parseDoc(groups));
            });

            // structured table reference with document prefix (before defined names!), e.g. `[1]!my_table[#DATA]`
            if (TABLE_REF) {
                this.addPattern(DOC_PREFIX + TABLE_REF, (...groups) => {
                    return this.#processTableOpen(groups[DOC_GROUPS], groups[DOC_GROUPS + 1], this.#parseDoc(groups).doc);
                });
            }

            // defined name with document prefix, e.g. `[1]!my_name`
            this.addPattern(DOC_PREFIX + NAME_REF, (...groups) => {
                return this.#createNameOrTableToken(groups[DOC_GROUPS], this.#parseDoc(groups));
            });
        }

        // macro call with sheet, e.g. `Module1!my_macro()`
        if (!formulaGrammar.OF) {
            this.addPattern(SHEET_PREFIX + FUNC_REF, (...groups) => {
                return this.#createFunctionToken(groups[SHEET_GROUPS], this.#parseSheet(groups));
            });
        }

        // table reference with sheet (UI only), e.g. `Sheet1!my_table[#DATA]`
        if (TABLE_REF && formulaGrammar.UI) {
            this.addPattern(SHEET_PREFIX + TABLE_REF, (...groups) => {
                return this.#createNameOrTableToken(groups[SHEET_GROUPS], this.#parseSheet(groups));
            });
        }

        // defined name with sheet, e.g. `Sheet1!my_name`
        this.addPattern(SHEET_PREFIX + NAME_REF, (...groups) => {
            return this.#createNameOrTableToken(groups[SHEET_GROUPS], this.#parseSheet(groups));
        });

        // local function call, or global macro call, e.g. `SUM()` or `my_macro()`
        this.addPattern(FUNC_REF, name => this.#createFunctionToken(name));

        // local structured table reference (before defined names!), e.g. `my_table[#DATA]`
        if (TABLE_REF) {
            this.addPattern(TABLE_REF, (tableName, openWs) => this.#processTableOpen(tableName, openWs));
        }

        // defined name without sheet reference
        this.addPattern(NAME_REF, name => this.#createNameOrTableToken(name));

        // unqualified table reference (no table name inside, OOX only, UI only), e.g. in `SUM([#DATA])`
        if (TABLE_REF && formulaGrammar.UI) {
            this.addPattern(TABLE_OPEN, openWs => {
                const { docAccess, refSheet, refAddress, unqualifiedTables } = this.context;
                const tableRef = (unqualifiedTables && (refSheet >= 0)) ? docAccess.resolveTableAt(refSheet, refAddress) : undefined;
                return tableRef && this.#processTableOpen(tableRef.name, openWs);
            });
        }

        // single table region without brackets, e.g. `#All` in `Table1[#All]`
        this.addPattern(this.nameRefPatterns.TABLE_SINGLE_REGION_NAME, regionName => {
            return this.#processTableRegion(regionName);
        }, { states: "table:open" });

        // single table column without brackets, e.g. `Col1` in `Table1[Col1]`
        this.addPattern(this.nameRefPatterns.TABLE_SINGLE_COLUMN_NAME, colName => {
            return this.#processTableCols(colName);
        }, { states: "table:open" });

        // at-sign as shortcut for `[#This Row]` in UI grammars
        if (formulaGrammar.UI) {
            this.addPattern("@" + WHITESPACE + "*", () => {
                this.#tableData.regionKeys.add("ROW");
                return "table:row";
            }, { states: "table:open" });
        }

        // table region in brackets, e.g. `[#All]` in `Table1[[#All],[Col1]]`
        this.addPattern(this.nameRefPatterns.TABLE_REGION_REF, regionName => {
            return this.#processTableRegion(regionName);
        }, { states: ["table:open", "table:sep"] });

        // table columns with brackets (native grammar only)
        if (!formulaGrammar.UI) {

            // table column range with brackets, e.g. `[Col1]:[Col2]` in `Table1[[#All],[Col1]:[Col2]]`
            this.addPattern(this.nameRefPatterns.TABLE_COLUMN_RANGE_REF, (col1Name, col2Name) => {
                return this.#processTableCols(col1Name, col2Name);
            }, { states: ["table:open", "table:sep"] });

            // table column name in brackets, e.g. `[Col1]` in `Table1[[#All],[Col1]]`
            this.addPattern(this.nameRefPatterns.TABLE_COLUMN_REF, colName => {
                return this.#processTableCols(colName);
            }, { states: ["table:open", "table:sep"] });
        }

        // table columns with or without brackets (UI grammar only)
        if (formulaGrammar.UI) {

            // table column range, e.g. `Col1:[Col 2]` in `Table1[[#All],Col1:[Col 2]]`
            this.addPattern(this.nameRefPatterns.TABLE_MIXED_COLUMN_RANGE_REF, (...groups) => {
                return this.#processTableMixedCols(groups);
            }, { states: ["table:open", "table:sep", "table:row"] });

            // table column name, e.g. `Col1` in `Table1[[#All],Col1]`
            this.addPattern(this.nameRefPatterns.TABLE_MIXED_COLUMN_REF, (...groups) => {
                return this.#processTableMixedCols(groups);
            }, { states: ["table:open", "table:sep", "table:row"] });
        }

        // separator in table references, e.g. the comma in `Table1[[#All],[Column1]]`
        this.addPattern(this.nameRefPatterns.TABLE_SEPARATOR, (_beforeWs, afterWs) => {
            this.#tableData.tableConfig.sepWs ||= !!afterWs;
            return "table:sep";
        }, { states: "table:ref" });

        // closing bracket of a structured table reference
        this.addPattern(this.nameRefPatterns.TABLE_CLOSE, (closeWs, closeBracket) => {
            return this.#processTableClose(closeWs, closeBracket);
        }, { states: ["table:open", "table:ref", "table:row"] });
    }

    // private methods --------------------------------------------------------

    /**
     * Checks that the passed `SheetRefs` structure is valid.
     *
     * @throws
     *  For unsupported references into external documents.
     */
    #validateSheets(sheetRefs: SheetRefs): SheetRefs {
        if (!(this.formulaGrammar.UI && sheetRefs.isExtDoc())) { return sheetRefs; }
        throw new Error("external document references not supported");
    }

    /**
     * Converts the matches of a `DOC_PREFIX` pattern to a `SheetRefs`
     * structure.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param [index=0]
     *  The start index of the `DOC_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  A `SheetRefs` structure for the passed matches.
     *
     * @throws
     *  For unsupported references into external documents.
     */
    #parseDoc(groups: string[], index = 0): SheetRefs {
        return this.#validateSheets(this.sheetRefPatterns.parseDocRef(this.context, groups, index));
    }

    /**
     * Converts the matches of a `SHEET_PREFIX` pattern to a `SheetRefs`
     * structure.
     *
     * @param groups
     *  The capturing group matches of a regular expression.
     *
     * @param [index=0]
     *  The start index of the `SHEET_PREFIX` pattern in the passed groups.
     *
     * @returns
     *  The `SheetRefs` structure for the passed matches. Its property "r2"
     *  will always be `undefined`.
     *
     * @throws
     *  For unsupported references into external documents.
     */
    #parseSheet(groups: string[], index = 0): SheetRefs {
        return this.#validateSheets(this.sheetRefPatterns.parseSheetRef(this.context, groups, index, true));
    }

    /**
     * Creates a new defined name token or a table token, if the label resolves
     * to a table range.
     *
     * @param label
     *  The label of the defined name or table range.
     *
     * @param [sheetRefs]
     *  A sheet reference structure for sheet-local named references.
     *
     * @returns
     *  A new `NameToken` or `TableToken`.
     */
    #createNameOrTableToken(label: string, sheetRefs?: SheetRefs): DefinedNameToken | TableToken {

        const { docAccess, refSheet, extendSheet } = this.context;

        // bug 40293: add sheet reference to existing sheet-local names if specified
        if (!sheetRefs && extendSheet && (refSheet >= 0) && docAccess.resolveName(refSheet, label)) {
            sheetRefs = SheetRefs.single(refSheet, true);
        }

        // create the defined name token
        const nameToken = new DefinedNameToken(docAccess, label, sheetRefs);

        // try to resolve an existing table range referred by the passed label
        const tableRef = nameToken.resolveTable();
        return tableRef ? new TableToken(this.context.docAccess, tableRef.name) : nameToken;
    }


    /**
     * Creates a new function/macro token.
     *
     * @param funcName
     *  The name of the function/macro.
     *
     * @param [sheetRefs]
     *  A sheet reference structure for macro calls with sheet/module name. Can
     *  be omitted for a built-in function, or a global macro call without
     *  sheet.
     *
     * @returns
     *  The token descriptor containing a new `FunctionToken` or `MacroToken`,
     *  and the passed text.
     */
    #createFunctionToken(funcName: string, sheetRefs?: SheetRefs): FunctionToken | MacroToken {

        // the resource key of a built-in sheet function (method `getFunctionKey` returns `null` for unknown functions)
        const funcKey = sheetRefs ? null : this.formulaGrammar.getFunctionKey(funcName);
        if (funcKey) { return new FunctionToken(funcKey); }

        // macro call (with or without sheet name prefix)
        return new MacroToken(this.context.docAccess, funcName, sheetRefs);
    }

    /**
     * Prepares parsing a structured table reference (the part enclosed in
     * brackets after the table name).
     *
     * @param tableName
     *  The name of the table range.
     *
     * @param openWs
     *  Whitespace following the opening bracket.
     *
     * @param [docRef]
     *  A `DocRef` structure with an external document index.
     */
    #processTableOpen(tableName: string, openWs: string, docRef?: DocRef): string {
        const tableConfig: TableTokenConfig = { docRef, openWs: !!openWs };
        this.#tableData = { tableName, tableConfig, regionKeys: new Set() };
        return "table:open";
    }

    /**
     * Adds a table region identifier to the internal table settings.
     */
    #processTableRegion(regionName: string): string {
        this.#tableData.regionKeys.add(this.nameRefPatterns.getTableRegionKey(regionName)!);
        return "table:ref";
    }

    /**
     * Adds a column name (or range) to the internal table settings.
     */
    #processTableCols(col1Name: string, col2Name = ""): string {
        const { tableConfig } = this.#tableData;
        if (tableConfig.col1Name) { throw new Error("multiple table column references"); }
        tableConfig.col1Name = this.nameRefPatterns.decodeTableColumn(col1Name);
        tableConfig.col2Name = this.nameRefPatterns.decodeTableColumn(col2Name);
        return "table:ref";
    }

    /**
     * Adds a column name (or range) with or without brackets to the internal
     * table settings.
     */
    #processTableMixedCols(groups: string[]): string {
        this.#tableData.tableConfig.openWs ||= (this.state === "table:open") && !!groups[0];
        return this.#processTableCols(groups[1] || groups[2], groups[3] || groups[4]);
    }

    /**
     * Creates a table token with the collected table reference settings.
     */
    #processTableClose(closeWs: string, closeBracket: string): TableToken {

        const { tableName, tableConfig, regionKeys } = this.#tableData;

        // compatibility: drop whitespace between at-sign and closing bracket
        tableConfig.closeWs = (this.state !== "table:row") && !!closeWs;

        // accept missing closing bracket in auto-correct mode only
        if (!closeBracket && !this.context.autoCorrect) { throw new Error("missing closing bracket in table reference"); }

        // build a valid region key
        switch (regionKeys.size) {
            case 0:
                break;
            case 1:
                tableConfig.regionSpec = itr.first(regionKeys)!;
                break;
            case 2: {
                if (regionKeys.has("HEADERS") && regionKeys.has("DATA")) {
                    tableConfig.regionSpec = "HEADERS,DATA";
                } else if (regionKeys.has("DATA") && regionKeys.has("TOTALS")) {
                    tableConfig.regionSpec = "DATA,TOTALS";
                } else {
                    throw new Error("invalid combination of table regions");
                }
                break;
            }
            default:
                throw new Error("invalid combination of table regions");
        }

        return new TableToken(this.context.docAccess, tableName, tableConfig);
    }
}
