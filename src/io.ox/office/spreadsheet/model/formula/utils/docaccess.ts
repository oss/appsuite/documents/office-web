/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";

// types ======================================================================

/**
 * Resolved reference to a specific defined name.
 */
export interface DefNameRef {

    /**
     * The index of the sheet containing the sheet-local defined name; or
     * `null` for a globally defined name.
     */
    sheet: OptSheet;

    /**
     * The case-exact label of the defined name.
     */
    label: string;

    /**
     * Implementations may or may not pass the reference to an existing model
     * instance of the name.
     */
    model?: NameModel;
}

/**
 * Resolved reference to a table range.
 */
export interface TableRef {

    /**
     * The case-exact name of the table range.
     */
    name: string;

    /**
     * The index of the sheet containing the table range.
     */
    sheet: number;

    /**
     * The address of the cell range covered by the table, including header and
     * footer rows.
     */
    range: Range;

    /**
     * Whether the table range contains a header row.
     */
    header: boolean;

    /**
     * Whether the table range contains a footer row.
     */
    footer: boolean;

    /**
     * The list of column names of the table range.
     */
    columns: readonly string[];

    /**
     * Implementations may or may not pass the reference to an existing table
     * model instance of the name.
     */
    model?: TableModel;
}

/**
 * Resolved target range for a specific table region and column range.
 */
export interface TableTargetRef {

    /**
     * The index of the sheet containing the table range.
     */
    sheet: number;

    /**
     * The target cell range.
     */
    range: Range;
}

/**
 * Minimal shape for an object acting like a spreadsheet document model used in
 * the spreadsheet formula engine. Must provide methods to convert between
 * sheet indexes and sheet names, and an address factory instance.
 *
 * There are other implementations of this interface, for example in the class
 * `OTManager` to manage executed sheet transformations needed for transforming
 * formula expressions.
 */
export interface IDocumentAccess {

    /**
     * The cell address factory containing the size of the sheets (number of
     * columns and rows) in a spreadsheet document.
     */
    readonly addressFactory: AddressFactory;

    /**
     * Returns the exact name of the sheet at the specified index.
     *
     * @param sheet
     *  The zero-based index of the sheet.
     *
     * @returns
     *  The name of the sheet at the passed index; or `null`, if the passed
     *  index is invalid.
     */
    getSheetName(sheet: number): string | null;

    /**
     * Returns the current index of the sheet with the specified name.
     *
     * @param sheetName
     *  The case-insensitive sheet name.
     *
     * @returns
     *  The current zero-based index of the sheet with the specified name; or
     *  `-1`, if a sheet with the passed name does not exist.
     */
    getSheetIndex(sheetName: string): number;

    /**
     * Resolves data for the specified defined name.
     *
     * @param sheet
     *  The index of the sheet to look for a sheet-local name; or `null` to
     *  look for a globally defined name.
     *
     * @param label
     *  The case-insensitive label of the defined name to look for.
     *
     * @returns
     *  The reference to the defined name, if existing; otherwise `undefined`.
     */
    resolveName(sheet: OptSheet, label: string): Opt<DefNameRef>;

    /**
     * Resolves data for the specified table range.
     *
     * @param tableName
     *  The case-insensitive name of the table range to look for.
     *
     * @returns
     *  The reference to the table range, if existing; otherwise `undefined`.
     */
    resolveTable(tableName: string): Opt<TableRef>;

    /**
     * Resolves data for a table range at the specified position.
     *
     * @param sheet
     *  The index of the sheet expected to contain the table range.
     *
     * @param address
     *  The address of a cell in the specified sheet.
     *
     * @returns
     *  The reference to the table range, if existing; otherwise `undefined`.
     */
    resolveTableAt(sheet: number, address: Address): Opt<TableRef>;
}
