/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, fun, map } from "@/io.ox/office/tk/algorithms";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";

// types ======================================================================

/**
 * Enumerates all supported rounding methods.
 *
 * - `floor`: Rounds down to the nearest smaller threshold value. Examples for
 *   integral rounding: `1.9 => 1`; `-1.1 => -2`.
 *
 * - `ceil`: Rounds up to the nearest larger threshold value. Examples for
 *   integral rounding: `1.1 => 2`; `-1.9 => -1`.
 *
 * - `round`: Rounds to the nearest threshold value. Rounds up if exactly in
 *   the middle between two threshold values. Examples for integral rounding:
 *   `1.5 => 2`; `-1.5 => -1`.
 *
 * - `trunc`: Rounds positive values down to the nearest smaller, and negative
 *   values up to the nearest larger threshold value (all towards zero).
 *   Examples for integral rounding: `1.9 => 1`; `-1.9 => -1`.
 *
 * - `inflate`: Rounds positive values up to the nearest larger, and negative
 *   values down to the nearest smaller threshold value (all away from zero).
 *   Examples for integral rounding: `1.1 => 2`; `-1.1 => -2`.
 *
 * - `iround`: Rounds to the nearest threshold value. Rounds away from zero if
 *   exactly in the middle between two threshold values. Examples for integral
 *   rounding: `1.5 => 2`; `-1.5 => -2`.
 */
export type RoundMode = "floor" | "ceil" | "round" | "trunc" | "inflate" | "iround";

/**
 * Discriminant for functions that can calculate the standard deviation and the
 * variance of a sample.
 */
export enum VarianceMethod {

    /**
     * Standard deviation.
     */
    DEV = "dev",

    /**
     * Variance.
     */
    VAR = "var"
}

/**
 * Result type of the function `slope`.
 */
export interface SlopeResult {

    /**
     * The sum of all input numbers.
     */
    sum: number;

    /**
     * The arithmetic mean of all input numbers.
     */
    mean: number;

    /**
     * The linear slope of all input numbers.
     */
    slope: number;
}

// constants ==================================================================

// mathematical constants
const SQRT_PI = Math.sqrt(Math.PI);

// maps rounding modes to rounding functions
const ROUND_FUNCS: Record<RoundMode, FuncType<number, [number]>> = { floor: Math.floor, ceil: Math.ceil, round: Math.round, trunc: Math.trunc, inflate: math.inflate, iround };

/**
 * Depth for the taylor expansion (exponent) and continued fraction expansion
 * (number of fractions) to calculate an approximation for `erf()` and
 * `erfc()`, see functions `erf_taylor()` and `erfc_cfrac()`.
 */
const ERF_DEPTH = 25;

/**
 * The threshold for `x` values to select between the two expansion algorithms
 * to calculate `erf()`.
 *
 * Using a depth of `25`, the minimum distance between both expansions is about
 * `6.6791e-13` at `x=1.9785`. Therefore, for `0<=x<=1.9785`, taylor expansion
 * will be used, and for `x>1.9785`, the continued fraction expansion will be
 * used.
 */
const ERF_THRESHOLD = 1.9785;

// private functions ==========================================================

/**
 * Rounds negative "halfs" away from zero ("inflate round"). Example: Rounds
 * the number -1.5 to -2 instead of -1 as `Math.round()` would do.
 */
function iround(n: number): number {
    return (n < 0) ? -Math.round(-n) : Math.round(n);
}

/**
 * Creates an array with all available factorials (for function `FACT`).
 */
function factorials(): number[] {
    const result: number[] = [];
    let fact = 1;
    while (Number.isFinite(fact)) {
        result.push(fact);
        fact *= result.length;
    }
    return result;
}

/**
 * Creates an array with all available double factorials (for function
 * `FACTDOUBLE`).
 */
function factorials2(): number[] {
    const result: number[] = [];
    let fact1 = 1;
    let fact2 = 1;
    while (Number.isFinite(fact1)) {
        result.push(fact1);
        fact1 *= (result.length + 1);
        if (Number.isFinite(fact2)) {
            result.push(fact2);
            fact2 *= (result.length + 1);
        }
    }
    return result;
}

/**
 * Returns the binomial coefficient of the passed input values without error
 * checking of input values.
 *
 * @param n
 *  The upper value of the binomial coefficient (size of the set to be chosen
 *  from).
 *
 * @param k
 *  The lower value of the binomial coefficient (number of choices from the
 *  set).
 *
 * @returns
 *  The binomial coefficient, if both values are non-negative, and `n` is
 *  greater than or equal to `k`.
 */
function binomial_core(n: number, k: number): number {

    // the resulting binomial coefficient
    let result = n - k + 1;

    // early exit when result leaves finite range
    for (let i = 2; (i <= k) && Number.isFinite(result); i += 1) {
        result /= i;
        result *= (n - k + i);
    }
    return result;
}

/**
 * Implementation of the taylor expansion for `erf()` with maximum depth `25`.
 * The maximum error to the "better" results of `erf()` as computed with taylor
 * expansion, depth `50`, is about `2.35e-13` at `x=1.9785` (the threshold
 * point between taylor expansion and continued fraction expansion).
 *
 * @param x
 *  The input parameter. MUST NOT be negative.
 *
 * @returns
 *  The approximated result of `erf(x)` for small positive `x`.
 */
function erf_taylor(x: number): number {
    let y = x, y2 = 0, i = 1, j = 3, fact = 1, sgn = -1;
    while ((i <= ERF_DEPTH) && (y !== y2)) {
        y2 = y;
        i += 1;
        y += sgn * x ** j / j / fact;
        j += 2;
        fact *= i;
        sgn = -sgn;
    }
    return 2 * y / SQRT_PI;
}

/**
 * Implementation of the continued fraction expansion for `erfc()` with depth
 * `25`. The maximum error to the "better" results of `erfc()` as computed with
 * continued fraction expansion, depth `50`, is about `4.3e-13` at `x=1.9785`
 * (the threshold point between taylor expansion and continued fraction
 * expansion).
 *
 * @param x
 *  The input parameter. MUST NOT be negative.
 *
 * @returns
 *  The approximated result of `erfc(x)` for large positive `x`.
 */
function erfc_cfrac(x: number): number {
    let y = 0, i = ERF_DEPTH, j = (i % 2 + 1);
    while (i >= 1) {
        y = i / (j * x + y);
        i -= 1;
        j = 3 - j;
    }
    return Math.exp(-x * x) / SQRT_PI / (x + y);
}

/**
 * Implementation of `erf()` for non-negative numbers. For all x equal to or
 * greater than 6, the result is assumed to be 1. The real result of `ERF` is
 * too close to 1 to be represented with the limited precision of JavaScript
 * floating-point numbers.
 *
 * @param x
 *  The input parameter. MUST NOT be negative.
 *
 * @returns
 *  The approximated result of `erf(x)`.
 */
function erf_abs(x: number): number {
    return (x >= 6) ? 1 : (x > ERF_THRESHOLD) ? (1 - erfc_cfrac(x)) : erf_taylor(x);
}

// public functions ===========================================================

/**
 * Returns the sum of the passed numbers (useful for callbacks).
 *
 * @param number1
 *  The first summand.
 *
 * @param number2
 *  The second summand.
 *
 * @returns
 *  The sum of the passed numbers.
 */
export function add(number1: number, number2: number): number {
    return number1 + number2;
}

/**
 * Returns the difference of the passed numbers (useful for callbacks).
 *
 * @param number1
 *  The minuend.
 *
 * @param number2
 *  The subtrahend.
 *
 * @returns
 *  The difference of the passed numbers.
 */
export function sub(number1: number, number2: number): number {
    return number1 - number2;
}

/**
 * Returns the product of the passed numbers (useful for callbacks).
 *
 * @param number1
 *  The first factor.
 *
 * @param number2
 *  The second factor.
 *
 * @returns
 *  The product of the passed numbers.
 */
export function mul(number1: number, number2: number): number {
    return number1 * number2;
}

/**
 * Returns the quotient of the passed numbers. If the divisor is zero, the
 * error code `#DIV/0!` will be thrown instead.
 *
 * @param number1
 *  The dividend.
 *
 * @param number2
 *  The divisor.
 *
 * @returns
 *  The quotient of the passed numbers.
 *
 * @throws
 *  The error code `#DIV/0!`, if the divisor is zero.
 */
export function div(number1: number, number2: number): number {
    return math.isZero(number2) ? ErrorCode.DIV0.throw() : (number1 / number2);
}

/**
 * Returns the floating-point modulo of the passed numbers. If the divisor is
 * zero, the error code `#DIV/0!` will be thrown instead. If the passed numbers
 * have different signs, the result will be adjusted according to the results
 * of other spreadsheet applications.
 *
 * @param number1
 *  The dividend.
 *
 * @param number2
 *  The divisor.
 *
 * @returns
 *  The modulo of the passed numbers.
 *
 * @throws
 *  The error code `#DIV/0!`, if the divisor is zero.
 */
export function mod(number1: number, number2: number): number {
    if (math.isZero(number2)) { throw ErrorCode.DIV0; }
    const result = number1 % number2;
    return (!math.isZero(result) && (number1 * number2 < 0)) ? (result + number2) : result;
}

/**
 * Returns the power of the passed numbers. If both numbers are zero, the error
 * code `#NUM!` will be thrown instead of returning `1`. If the base is zero,
 * and the exponent is negative, the error code `#DIV/0!` will be thrown
 * instead of returning infinite.
 *
 * @param base
 *  The base of the power to be calculated.
 *
 * @param expn
 *  The exponent of the power to be calculated.
 *
 * @returns
 *  The passed base raised to the exponent.
 *
 * @throws
 *  The error code `#NUM!`, if base and exponent are zero, or the error code
 *  `#DIV/0!`, if base is zero, and exponent is negative.
 */
export function powNaN(base: number, expn: number): number {
    // Math.pow() returns 1 for 0^0; Excel returns #NUM! error
    if (math.isZero(base)) {
        if (math.isZero(expn)) { throw ErrorCode.NUM; }
        if (expn < 0) { throw ErrorCode.DIV0; }
    }
    return base ** expn;
}

/**
 * Returns the power of the passed numbers. If both numbers are zero, `1` will
 * be returned. If the base is zero, and the exponent is negative, the error
 * code `#NUM!` will be thrown instead of returning infinity.
 *
 * @param base
 *  The base of the power to be calculated.
 *
 * @param expn
 *  The exponent of the power to be calculated.
 *
 * @returns
 *  The passed base raised to the exponent.
 *
 * @throws
 *  The error code `#NUM!`, if base is zero, and exponent is negative.
 */
export function pow1(base: number, expn: number): number {
    return (math.isZero(base) && (expn < 0)) ? ErrorCode.NUM.throw() : (base ** expn);
}

/**
 * An object that contains resolver functions for calculating the power of two
 * numbers, mapped by file format identifiers.
 */
export const POW = { ooxml: powNaN, odf: pow1 };

/**
 * Returns the two-parameter arc tangent of the passed coordinates. If both
 * numbers are zero, the error code `#DIV/0!` will be thrown instead.
 *
 * The order of the parameters `x` and `y` is switched compared to the native
 * method `Math.atan2`, according to usage in other spreadsheet applications.
 *
 * @param x
 *  The X coordinate.
 *
 * @param y
 *  The Y coordinate.
 *
 * @returns
 *  The arc tangent of the passed coordinates.
 *
 * @throws
 *  The error code `#DIV/0!`, if both numbers are zero.
 */
export function atanxy(x: number, y: number): number {
    // Spreadsheet applications use (x,y); but Math.atan2() expects (y,x)
    return (math.isZero(x) && math.isZero(y)) ? ErrorCode.DIV0.throw() : Math.atan2(y, x);
}

/**
 * Returns the argument resulting in the passed hyperbolic cotangent.
 */
export function acoth(x: number): number {
    return Math.log((x + 1) / (x - 1)) / 2;
}

/**
 * Rounds a number to the specified precision.
 *
 * @param n
 *  The number to be rounded.
 *
 * @param prec
 *  The multiplicator used for rounding. The passed number will be rounded to a
 *  multiple of (the absolute of) this value.
 *
 * @param mode
 *  The rounding method to be used.
 *
 * @returns
 *  the passed number, rounded to the nearest multiple of `m`.
 */
export function mround(n: number, prec: number, mode: RoundMode): number {

    // nothing complicated to do, if either number is zero
    if (n * prec === 0) { return 0; }

    // the rounder function to be used
    const roundFn = ROUND_FUNCS[mode];

    // apply rounder function to quotient
    prec = Math.abs(prec);
    return roundFn(n / prec) * prec;
}

/**
 * Rounds a number to the specified number of digits.
 *
 * @param n
 *  The number to be rounded.
 *
 * @param digits
 *  The number of digits to round to. Positive values round to digits after the
 *  decimal point, negative numbers round to integral powers of ten.
 *
 * @param mode
 *  The rounding method to be used.
 *
 * @returns
 *  the passed number, rounded to the specified number of digits.
 */
export function dround(n: number, digits: number, mode: RoundMode): number {

    // short-path for zero
    if (n === 0) { return 0; }

    // the rounder function to be used
    const roundFn = ROUND_FUNCS[mode];

    // short-path for integer rounding
    if (digits === 0) { return roundFn(n); }

    // split the passed number into mantissa and exponent
    const comps = math.splitNumber(n);

    // number of digits for rounding the normalized mantissa
    const mdigits = digits + comps.expn;

    // rounding mantissa to 14 decimal places (or more) does not change the passed number
    if (mdigits >= 14) { return n; }

    const exp10 = 10 ** digits;

    // rounding mantissa (which is always less than 10) to entire tens (or more)
    // results in 0 (towards zero) or a power of 10 (away from zero), special-case
    // implementation to prevent numeric overflow when truncating with large powers
    if (mdigits < 0) {
        const mant = roundFn(comps.mant * 10 ** Math.max(-307, mdigits));
        return (mant === 0) ? 0 : (mant / exp10);
    }

    // round the mantissa and construct the result
    return roundFn(n * exp10) / exp10;
}

/**
 * Returns the sum of the passed numbers.
 *
 * @param numbers
 *  The numbers to be added.
 *
 * @returns
 *  The sum of all passed numbers, or `0` if the passed array is empty.
 */
export function sum(numbers: number[]): number {
    return numbers.reduce(add, 0);
}

/**
 * Returns the product of the passed numbers.
 *
 * @param numbers
 *  The numbers to be multiplied.
 *
 * @returns
 *  The product of all passed numbers, or 1 if the passed array is empty.
 */
export function product(numbers: number[]): number {
    return numbers.reduce(mul, 1);
}

/**
 * Returns the minimum of the passed numbers.
 *
 * @param numbers
 *  The numbers to be processed.
 *
 * @returns
 *  The minimum of all passed numbers, or `Infinity` if the passed array is
 *  empty.
 */
export function minOf(numbers: number[]): number {
    return numbers.reduce((n1, n2) => Math.min(n1, n2), Infinity);
}

/**
 * Returns the maximum of the passed numbers.
 *
 * @param numbers
 *  The numbers to be processed.
 *
 * @returns
 *  The maximum of all passed numbers, or `-Infinity` if the passed array is
 *  empty.
 */
export function maxOf(numbers: number[]): number {
    return numbers.reduce((n1, n2) => Math.max(n1, n2), -Infinity);
}

/**
 * Returns the factorial of the passed number. Throws the error code `#NUM!`,
 * if the passed number is negative or too large.
 *
 * @param x
 *  The number to calculate the factorial for.
 *
 * @returns
 *  The factorial of the passed number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed number is negative, or too large.
 */
export const factorial = fun.do(() => {
    let cache: Opt<number[]>;
    return (x: number): number => {
        cache ??= factorials();
        return cache[Math.floor(x)] ?? ErrorCode.NUM.throw();
    };
});

/**
 * Returns the double factorial of the passed number. Throws the error code
 * `#NUM!`, if the passed number is negative or too large.
 *
 * @param x
 *  The number to calculate the double factorial for.
 *
 * @returns
 *  The double factorial of the passed number.
 *
 * @throws
 *  The error code `#NUM!`, if the passed number is negative, or too large.
 */
export const factorial2 = fun.do(() => {
    let cache: Opt<number[]>;
    return (x: number): number => {
        cache ??= factorials2();
        return cache[Math.floor(x)] ?? ErrorCode.NUM.throw();
    };
});

/**
 * Returns the binomial coefficient of the passed input values.
 *
 * @param n
 *  The upper value of the binomial coefficient (size of the set to be chosen
 *  from).
 *
 * @param k
 *  The lower value of the binomial coefficient (number of choices from the
 *  set).
 *
 * @returns
 *  The binomial coefficient, if both values are non-negative, and `n` is
 *  greater than or equal to `k`.
 *
 * @throws
 *  The error code `#NUM!`, if the passed values are invalid, or if the
 *  resulting binomial coefficient is too large.
 */
export const binomial = fun.do(() => {

    // cache for different combinations of input parameters
    const BINOM_CACHE = new Map<string, number>();
    // maximum input parameters for cache usage
    const CACHE_MAX_NUM = 50;

    // public implementation with error checking and caching
    return (n: number, k: number): number => {

        // check input values
        n = Math.floor(n);
        k = Math.floor(k);
        if ((n < 0) || (k < 0) || (n < k)) { throw ErrorCode.NUM; }

        // special cases: (n 0)=1 and (n n)=1
        if ((k === 0) || (k === n)) { return 1; }
        // special cases: (n 1)=n and (n n-1)=n
        if ((k === 1) || (k === n - 1)) { return n; }

        // binomial coefficient is symmetric: (n k) = (n n-k); use lower k for performance
        if (k * 2 > n) { k = n - k; }

        // use the cache, if both numbers are in a specific limit (k<=n, see above)
        if (n <= CACHE_MAX_NUM) {
            // result is always valid (finite) in the cachable limit
            return map.upsert(BINOM_CACHE, `${n},${k}`, () => binomial_core(n, k));
        }

        // do not cache larger numbers, check final result (may be infinite)
        const result = binomial_core(n, k);
        return Number.isFinite(result) ? result : ErrorCode.NUM.throw();
    };
});

/**
 * Returns the result of the Gauss error function.
 *
 * @param x
 *  The input value.
 *
 * @returns
 *  The result value of the Gauss error function.
 */
export function erf(x: number): number {
    // `erf` is an odd function: erf(-x)=-erf(x), with a shortcut for the function's zero: erf(0)=0
    return (x > 0) ? erf_abs(x) : (x < 0) ? -erf_abs(-x) : 0;
}

/**
 * Returns the result of the complementary Gauss error function.
 *
 * @param x
 *  The input value.
 *
 * @returns
 *  The result value of the complementary Gauss error function.
 */
export function erfc(x: number): number {
    // Prevent arithmetic underflow for large x: erf(6) returns exactly 1 due to precision limitations
    // of floating-point numbers; but erfc(6)=1-erf(6) is close to but greater than 0.
    return (x > ERF_THRESHOLD) ? erfc_cfrac(x) : (1 - erf(x));
}

/**
 * Returns the linear slope of a collection of Y values.
 *
 * @param numbers
 *  The Y values to be processed. The array indexes of the numbers are used as
 *  the X values associated to the array elements.
 *
 * @returns
 *  The sum, arithmetic mean, and linear slope of the passed numbers.
 *
 * @throws
 *  The error code `#DIV/0!`, if the passed array contains less than two
 *  elements.
 */
export function slope(numbers: number[]): SlopeResult {
    const sumY = sum(numbers);
    const meanX = (numbers.length - 1) / 2;
    const meanY = div(sumY, numbers.length);
    const sumDYX = numbers.reduce((s, y, x) => s + (x - meanX) * (y - meanY), 0);
    const sumDX2 = meanX * (meanX + 1) * (2 * meanX + 1) / 3;
    return { sum: sumY, mean: meanY, slope: div(sumDYX, sumDX2) };
}

/**
 * Returns the sum of the squares of the absolute distances of the passed
 * numbers to their arithmetic mean.
 *
 * @param numbers
 *  The numbers to be processed.
 *
 * @param total
 *  The sum of the passed numbers. In most situations, the sum of these numbers
 *  is already avaiable (e.g. during aggregation of function operands), and is
 *  therefore expected to be passed to this method for best performance.
 *
 * @returns
 *  The sum of the squares of the absolute distances of the passed numbers to
 *  their arithmetic mean.
 *
 * @throws
 *  The error code `#DIV/0!`, if the passed array is empty.
 */
export function devSq(numbers: number[], total: number): number {
    const mean = div(total, numbers.length);
    return numbers.reduce((s, n) => {
        const diff = n - mean;
        return s + diff * diff;
    }, 0);
}

/**
 * Returns the variance or the standard deviation of the passed numbers.
 *
 * @param numbers
 *  The numbers to calculate the variance or standard deviation for.
 *
 * @param total
 *  The sum of the passed numbers. In most situations, the sum of these numbers
 *  is already avaiable (e.g. during aggregation of function operands), and is
 *  therefore expected to be passed to this method for best performance.
 *
 * @param method
 *  The result type (either standard deviation, or variance).
 *
 * @param population
 *  If set to `true`, calculates the variance or standard deviation based on
 *  the entire population (the number of elements in the passed array). If set
 *  to `false`, calculates the result based on a sample (the number of elements
 *  reduced by one).
 *
 * @returns
 *  The variance of the passed numbers.
 *
 * @throws
 *  The error code `#DIV/0!`, if the passed array is empty; or if the passed
 *  array contains only one element, and the parameter `population` is set to
 *  false (variance based on sample).
 */
export function variance(numbers: number[], total: number, method: VarianceMethod, population: boolean): number {
    const devsq = devSq(numbers, total);
    const size = population ? numbers.length : (numbers.length - 1);
    const result = div(devsq, size);
    return (method === VarianceMethod.DEV) ? Math.sqrt(result) : result;
}
