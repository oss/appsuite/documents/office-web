/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * Enumerates different sub-types of formula errors.
 */
export enum FormulaErrorCode {

    /**
     * The end of the formula has been reached unexpectedly (e.g. incomplete
     * formula).
     */
    MISSING = "missing",

    /**
     * A token with an invalid type, or invalid garbage at the end of the
     * formula has been found.
     */
    UNEXPECTED = "unexpected",

    /**
     * An invalid token has been found where a reference token was expected.
     */
    REFERENCE = "reference"
}

// class FormulaError =========================================================

/**
 * A special exception to be thrown during parsing, compiling, or interpreting
 * spreadsheet formulas.
 */
export class FormulaError extends Error {

    override readonly name = "FormulaError";
    readonly code: FormulaErrorCode;

    constructor(message: string, code: FormulaErrorCode) {
        super(message);
        this.code = code;
    }
}
