/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { stringifyCol, stringifyRow, parseCol, parseRow, Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";

const { abs } = Math;

// types ======================================================================

/**
 * A structure with up to two cell references forming a cell range.
 * - no cell references: reference error (e.g. `=SUM(Sheet1!#REF!)`).
 * - one cell reference in `r1`: single cell (e.g. `=SUM(Sheet1!A1)`).
 * - two cell references: cell range (e.g. `=SUM(Sheet1!A1:C3)`).
 */
export interface CellRefs {
    r1?: CellRef;
    r2?: CellRef;
}

// private functions ==========================================================

/**
 * Returns a relocated column/row index.
 *
 * @param index
 *  The original column/row index.
 *
 * @param offset
 *  The distance to move the passed column/row index.
 *
 * @param maxIndex
 *  The maximum allowed column/row index in a cell address.
 *
 * @param [wrapReferences=false]
 *  Whether to wrap the column/row indexes at the sheet boundaries.
 *
 * @returns
 *  The resulting column/row index; or -1, if the passed column/row index
 *  cannot be relocated.
 */
function relocateIndex(index: number, offset: number, maxIndex: number, wrapReferences?: boolean): number {
    if (abs(offset) > maxIndex) { return -1; }
    index += offset;
    return wrapReferences ?
        ((index < 0) ? (index + maxIndex + 1) : (index > maxIndex) ? (index - maxIndex - 1) : index) :
        (((index < 0) || (index > maxIndex)) ? -1 : index);
}

// public functions ===========================================================

/**
 * Returns whether the passed cell reference ranges are equal.
 *
 * @param cellRefs1
 *  The first cell reference range to be compared.
 *
 * @param cellRefs2
 *  The second cell reference range to be compared.
 */
export function equalCellRefs(cellRefs1: CellRefs, cellRefs2: CellRefs): boolean {
    return CellRef.equal(cellRefs1.r1, cellRefs2.r1) && CellRef.equal(cellRefs1.r2, cellRefs2.r2);
}

/**
 * Swaps the components of the cell references in-place to be in ascending
 * order.
 */
export function reorderCellRefs(cellRefs: CellRefs): void {

    // nothing to do for single-cell references
    const { r1, r2 } = cellRefs;
    if (!r1 || !r2) { return; }

    // swap column indexes (and absolute flags!), if cell references are not in order
    if (r2.col < r1.col) {
        const absCol = r2.absCol;
        r2.absCol = r1.absCol;
        r1.absCol = absCol;
        const col = r2.col;
        r2.col = r1.col;
        r1.col = col;
    }

    // swap row indexes (and absolute flags!), if old cell references are not in order
    if (r2.row < r1.row) {
        const absRow = r2.absRow;
        r2.absRow = r1.absRow;
        r1.absRow = absRow;
        const row = r2.row;
        r2.row = r1.row;
        r1.row = row;
    }
}

// class CellRef ==============================================================

/**
 * A structure representing the address of a single cell in a reference token
 * (e.g. the particles `$A$1` or `$B$2` in `Sheet1!$A$1:$B$2`).
 */
export class CellRef implements Equality<CellRef>, Cloneable<CellRef> {

    // static functions -------------------------------------------------------

    /**
     * Creates a cell reference structure for the passed cell address.
     *
     * @param address
     *  The cell address.
     *
     * @param absCol
     *  Whether the column reference is absolute (as in $C2).
     *
     * @param absRow
     *  Whether the row reference is absolute (as in C$2).
     */
    static create(address: Address, absCol: boolean, absRow: boolean): CellRef {
        return new CellRef(address.c, address.r, absCol, absRow);
    }

    /**
     * Returns whether the passed optional cell reference structures are equal
     * (i.e. both defined and equal, or both undefined).
     *
     * @param r1
     *  The first optional cell reference.
     *
     * @param r2
     *  The second optional cell reference.
     */
    static equal(r1?: CellRef, r2?: CellRef): boolean {
        return (!r1 && !r2) || !!(r1 && r2 && r1.equals(r2));
    }

    // properties -------------------------------------------------------------

    col: number;
    row: number;
    absCol: boolean;
    absRow: boolean;

    // constructor ------------------------------------------------------------

    /**
     * @param col
     *  The zero-based column index.
     *
     * @param row
     *  The zero-based row index.
     *
     * @param absCol
     *  Whether the column reference is absolute (as in `$C2` or `RC3`).
     *
     * @param absRow
     *  Whether the row reference is absolute (as in `C$2` or `R3C`).
     */
    constructor(
        col: number,
        row: number,
        absCol: boolean,
        absRow: boolean
    ) {
        this.col = col;
        this.row = row;
        this.absCol = absCol;
        this.absRow = absRow;
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a deep clone of this cell reference structure.
     *
     * @returns
     *  A deep clone of this cell reference structure.
     */
    clone(): CellRef {
        return new CellRef(this.col, this.row, this.absCol, this.absRow);
    }

    /**
     * Returns whether this cell reference and the passed cell reference are
     * equal.
     *
     * @param cellRef
     *  The other cell reference to be compared with this cell reference.
     *
     * @returns
     *  Whether this cell reference and the passed cell reference are equal.
     */
    equals(cellRef: CellRef): boolean {
        return (this.col === cellRef.col) && (this.row === cellRef.row) && (this.absCol === cellRef.absCol) && (this.absRow === cellRef.absRow);
    }

    /**
     * Returns the cell address represented by this cell reference.
     *
     * @returns
     *  The cell address represented by this cell reference.
     */
    toAddress(): Address {
        return new Address(this.col, this.row);
    }

    /**
     * Returns whether the column or row index of this cell reference is marked
     * to be absolute.
     *
     * @param columns
     *  Whether to check the column index (`true`), or the row index (`false`).
     *
     * @returns
     *  Whether the column or row index of this cell reference is marked to be
     *  absolute.
     */
    isAbs(columns: boolean): boolean {
        return columns ? this.absCol : this.absRow;
    }

    /**
     * Returns the column or row index of this cell reference.
     *
     * @param columns
     *  Whether to return the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  The column or row index of this cell reference.
     */
    getIndex(columns: boolean): number {
        return columns ? this.col : this.row;
    }

    /**
     * Relocates the relative column/row components in this cell reference
     * in-place.
     *
     * @param addressFactory
     *  The cell address factory containing the sheet limits (number of columns
     *  and rows).
     *
     * @param refAddress
     *  The source reference address of the formula containing the token.
     *
     * @param targetAddress
     *  The target reference address for relocation.
     *
     * @param [wrapReferences=false]
     *  Whether to wrap the column/row indexes at the sheet boundaries.
     *
     * @returns
     *  Whether relocation was successful, and the address of this reference
     *  is still valid.
     */
    relocate(addressFactory: AddressFactory, refAddress: Address, targetAddress: Address, wrapReferences?: boolean): boolean {

        // relocate relative column
        if (!this.absCol) {
            this.col = relocateIndex(this.col, targetAddress.c - refAddress.c, addressFactory.maxCol, wrapReferences);
            if (this.col < 0) { return false; }
        }

        // relocate relative row
        if (!this.absRow) {
            this.row = relocateIndex(this.row, targetAddress.r - refAddress.r, addressFactory.maxRow, wrapReferences);
            if (this.row < 0) { return false; }
        }

        return true;
    }

    /**
     * Returns the complete name of the column part of this cell reference.
     *
     * @returns
     *  The complete name of the column part of this cell reference.
     */
    colText(): string {
        return (this.absCol ? "$" : "") + stringifyCol(this.col);
    }

    /**
     * Returns the complete name of the row part of this cell reference.
     *
     * @returns
     *  The complete name of the row part of this cell reference.
     */
    rowText(): string {
        return (this.absRow ? "$" : "") + stringifyRow(this.row);
    }

    /**
     * Returns the complete name of this cell reference.
     *
     * @returns
     *  The complete name of this cell reference.
     */
    refText(): string {
        return this.colText() + this.rowText();
    }

    /**
     * Returns the complete name of the column part of this cell reference, for
     * R1C1 notation.
     *
     * @param rcPrefixChars
     *  The prefix characters (two characters) used for R1C1 notation.
     *
     * @param refAddress
     *  The reference address needed to create column offsets as used in R1C1
     *  notation, e.g. `R1C[-1]`.
     *
     * @returns
     *  The complete name of the column part of this cell reference, for R1C1
     *  notation.
     */
    colTextRC(rcPrefixChars: string, refAddress: Address): string {
        return rcPrefixChars[1] + (this.absCol ? String(this.col + 1) : (this.col !== refAddress.c) ? `[${this.col - refAddress.c}]` : "");
    }

    /**
     * Returns the complete name of the row part of this cell reference, for
     * R1C1 notation.
     *
     * @param rcPrefixChars
     *  The prefix characters (two characters) used for R1C1 notation.
     *
     * @param refAddress
     *  The reference address needed to create row offsets as used in R1C1
     *  notation, e.g. `R[-1]C1`.
     *
     * @returns
     *  The complete name of the row part of this cell reference, for R1C1
     *  notation.
     */
    rowTextRC(rcPrefixChars: string, refAddress: Address): string {
        return rcPrefixChars[0] + (this.absRow ? String(this.row + 1) : (this.row !== refAddress.r) ? `[${this.row - refAddress.r}]` : "");
    }

    /**
     * Returns the complete name of this cell reference, for R1C1 notation.
     *
     * @param rcPrefixChars
     *  The prefix characters (two characters) used for R1C1 notation.
     *
     * @param refAddress
     *  The reference address needed to create column or row offsets as used in
     *  R1C1 notation, e.g. `R[-1]C[-1]`.
     *
     * @returns
     *  The complete name of this cell reference.
     */
    refTextRC(rcPrefixChars: string, refAddress: Address): string {
        return this.rowTextRC(rcPrefixChars, refAddress) + this.colTextRC(rcPrefixChars, refAddress);
    }

    /**
     * Parses a column index in A1 notation, and adds it to this cell reference
     * structure.
     *
     * @param colText
     *  The column name in A1 notation, e.g. `"A"` for the first column.
     *
     * @param absFlag
     *  The absolute marker. Each non-empty string will be recognized as
     *  absolute column index. An empty string or `null` will be recognized as
     *  relative column index.
     *
     * @param maxCol
     *  The largest valid column index that will be accepted by this method.
     *
     * @returns
     *  Whether the passed values represent a valid column index.
     */
    parseCol(colText: string, absFlag: string | null, maxCol: number): boolean {
        this.col = parseCol(colText);
        this.absCol = !!absFlag;
        return (this.col >= 0) && (this.col <= maxCol);
    }

    /**
     * Parses a row index in A1 notation, and adds it to this cell reference
     * structure.
     *
     * @param rowText
     *  The row name in A1 notation, e.g. `"1"` for the first row.
     *
     * @param absFlag
     *  The absolute marker. Each non-empty string will be recognized as
     *  absolute row index. An empty string or `null` will be recognized as
     *  relative row index.
     *
     * @param maxRow
     *  The largest valid row index that will be accepted by this method.
     *
     * @returns
     *  Whether the passed values represent a valid row index.
     */
    parseRow(rowText: string, absFlag: string | null, maxRow: number): boolean {
        this.row = parseRow(rowText);
        this.absRow = !!absFlag;
        return (this.row >= 0) && (this.row <= maxRow);
    }

    /**
     * Parses a column index in R1C1 notation, and adds it to this cell
     * reference structure.
     *
     * @param absText
     *  The absolute column name in R1C1 notation, e.g. `"1"` for the first
     *  column; or `null`, if the column index is relative (see parameter
     *  `relText`).
     *
     * @param relText
     *  The relative column name in R1C1 notation without brackets, e.g. `"-1"`
     *  for the preceding column. The empty string or `null` will be recognized
     *  as reference to the current column (as well as the string `"0"`, e.g.
     *  in `R1C[0]`, `R1C[]`, and `R1C`).
     *
     * @param maxCol
     *  The largest valid column index that will be accepted by this method.
     *
     * @param refCol
     *  The index of the reference column needed to resolve a column offset as
     *  used in R1C1 notation (e.g. in `R1C[-1]`) to the effective column
     *  index.
     *
     * @param [wrapReferences=false]
     *  Whether to wrap the column index at the sheet boundaries when resolving
     *  a column offset.
     *
     * @returns
     *  Whether the passed values represent a valid column index.
     */
    parseColRC(absText: string | null, relText: string | null, maxCol: number, refCol: number, wrapReferences?: boolean): boolean {
        this.col = absText ? (parseInt(absText, 10) - 1) : relText ? relocateIndex(refCol, parseInt(relText, 10), maxCol, wrapReferences) : refCol;
        this.absCol = !!absText;
        return (this.col >= 0) && (this.col <= maxCol);
    }

    /**
     * Parses a row index in R1C1 notation, and adds it to this cell reference
     * structure.
     *
     * @param absText
     *  The absolute row name in R1C1 notation, e.g. `"1"` for the first row;
     *  or `null`, if the row index is relative (see parameter `relText`).
     *
     * @param relText
     *  The relative row name in R1C1 notation without brackets, e.g. `"-1"`
     *  for the preceding row. The empty string or `null` will be recognized as
     *  reference to the current row (as well as the string `"0"`, e.g. in
     *  `R[0]C1`, `R[]C1`, and `RC1`).
     *
     * @param maxRow
     *  The largest valid row index that will be accepted by this method.
     *
     * @param refRow
     *  The index of the reference row needed to resolve a row offset as used
     *  in R1C1 notation (e.g. in R[-1]C1) to the effective row index.
     *
     * @param [wrapReferences=false]
     *  Whether to wrap the row index at the sheet boundaries when resolving a
     *  row offset.
     *
     * @returns
     *  Whether the passed values represent a valid row index.
     */
    parseRowRC(absText: string | null, relText: string | null, maxRow: number, refRow: number, wrapReferences?: boolean): boolean {
        this.row = absText ? (parseInt(absText, 10) - 1) : relText ? relocateIndex(refRow, parseInt(relText, 10), maxRow, wrapReferences) : refRow;
        this.absRow = !!absText;
        return (this.row >= 0) && (this.row <= maxRow);
    }

    /**
     * Returns a text description of this cell reference for debug logging.
     */
    toString(): string {
        return this.refText();
    }
}
