/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import type { TransformSheetVector } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { IDocumentAccess } from "@/io.ox/office/spreadsheet/model/formula/utils/docaccess";

// public functions ===========================================================

/**
 * Encloses the passed complex sheet name in apostrophes, and duplicates all
 * inner apostrophes contained in the sheet name.
 *
 * @param sheetName
 *  A complex sheet name.
 *
 * @returns
 *  The encoded complex sheet name.
 */
export function encodeComplexSheetName(sheetName: string): string {
    return `'${sheetName.replace(/'/g, "''")}'`;
}

// class DocRef ===============================================================

/**
 * A reference to an external document used in formula expressions.
 */
export class DocRef {

    /**
     * The index of the external document, e.g. the number in the brackets in
     * the formula `[1]!extname` or `[1]Sheet1!A1`. The value zero represents a
     * globally defined name with document reference in the own document, e.g.
     * the zero in `[0]!globalname`.
     */
    index: number;

    // constructor ------------------------------------------------------------

    constructor(index: number) {
        this.index = index;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this instance refers into another document.
     *
     * @returns
     *  Whether this instance refers into another external spreadsheet document
     *  (`true`), or to the own document (`false`).
     */
    isExtDoc(): boolean {
        return this.index !== 0; // index 0 for internal references
    }
}

// class SheetRef =============================================================

/**
 * A structure representing the sheet reference part in a range reference (e.g.
 * in `Sheet1!A1:B2`), or in a defined name (e.g. `Sheet1!name`).
 */
export class SheetRef implements Equality<SheetRef>, Cloneable<SheetRef> {

    /**
     * The zero-based sheet index, if a sheet with the passed name exists in
     * the document; or `-1` for a broken sheet reference; otherwise the sheet
     * name as string.
     */
    sheet: number | string;

    /**
     * Whether the sheet reference is marked to be absolute.
     */
    abs: boolean;

    // constructor ------------------------------------------------------------

    /**
     * @param sheet
     *  The zero-based sheet index, if a sheet with the passed name exists in
     *  the document; or `-1` for a broken sheet reference; otherwise the sheet
     *  name as string.
     *
     * @param abs
     *  Whether the sheet reference is marked to be absolute.
     */
    constructor(sheet: number | string, abs: boolean) {
        this.sheet = sheet;
        this.abs = abs;
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a clone of this sheet reference structure.
     *
     * @returns
     *  A clone of this sheet reference structure.
     */
    clone(): SheetRef {
        return new SheetRef(this.sheet, this.abs);
    }

    /**
     * Returns whether this sheet reference points to an existing sheet.
     *
     * @returns
     *  Whether this sheet reference points to an existing sheet.
     */
    valid(): boolean {
        return is.number(this.sheet) && (this.sheet >= 0);
    }

    /**
     * Returns whether this sheet reference and the passed sheet reference are
     * equal.
     *
     * @param sheetRef
     *  The other sheet reference to be compared with this sheet reference.
     *
     * @returns
     *  Whether this sheet reference and the passed sheet reference are equal.
     */
    equals(sheetRef: SheetRef): boolean {
        return (this.abs === sheetRef.abs) && (this.sheet === sheetRef.sheet);
    }

    /**
     * Returns the original name of the sheet referred by this sheet reference.
     *
     * @param docAccess
     *  The document access interface.
     *
     * @param [docRef]
     *  Reference to another spreadsheet document.
     *
     * @returns
     *  The original name of the sheet referred by this sheet reference; or
     *  `null`, if this sheet reference cannot be resolved to a sheet name.
     */
    resolve(docAccess: IDocumentAccess, docRef?: DocRef): string | null {
        return is.string(this.sheet) ? this.sheet : docRef?.isExtDoc() ? null : docAccess.getSheetName(this.sheet);
    }

    /**
     * Tries to replace an unresolved sheet name (property `sheet` is a string)
     * with an existing sheet index in the passed spreadsheet document.
     *
     * @param docAccess
     *  The document access interface.
     */
    refresh(docAccess: IDocumentAccess): void {
        const sheet = is.string(this.sheet) ? docAccess.getSheetIndex(this.sheet) : -1;
        if (sheet >= 0) { this.sheet = sheet; }
    }

    /**
     * Transforms the sheet index in this sheet reference, after a sheet has
     * been inserted, deleted, or moved in the document.
     *
     * @param xfVector
     *  The transformation vector for the sheet indexes.
     *
     * @returns
     *  Whether the sheet index in this sheet reference has been modified.
     */
    transform(xfVector: TransformSheetVector): boolean {
        // TODO: also for relative sheet references (ODF)?

        // invalid sheet name in reference: return false
        if (!this.valid()) { return false; }

        // from here on: `this.sheet` is a number
        const oldSheet = this.sheet as number;
        const newSheet = xfVector[oldSheet];

        // referenced sheet has been deleted or moved
        if (oldSheet !== newSheet) {
            this.sheet = is.number(newSheet) ? newSheet : -1;
            return true;
        }

        return false;
    }

    /**
     * Returns a text description of this sheet reference for debug logging.
     */
    toString(): string {
        return (this.sheet === -1) ? "#REF" : `${this.abs ? "$" : ""}${is.string(this.sheet) ? encodeComplexSheetName(this.sheet) : this.sheet}`;
    }
}

// class SheetRefs ============================================================

/**
 * A structure with up to two sheet references forming a sheet range, and an
 * optional reference to an external document.
 */
export class SheetRefs {

    /**
     * Creates a `SheetRefs` structure for a single sheet in the own document.
     */
    static single(sheet: number | string, abs: boolean): SheetRefs {
        return new SheetRefs(new SheetRef(sheet, abs));
    }

    /**
     * Creates a `SheetRefs` structure for a global reference, optionally to an
     * external document.
     */
    static global(index?: number): SheetRefs {
        const docRef = is.number(index) ? new DocRef(index) : undefined;
        return new SheetRefs(undefined, undefined, docRef);
    }

    // properties -------------------------------------------------------------

    /**
     * Reference to an external document (e.g. the [1]` in `[1]Sheet1!A1:B2`).
     * If set to `null`, the reference targets the own document.
     */
    doc: Opt<DocRef>;

    /**
     * First sheet in the reference (e.g. the `Sheet1` in `Sheet1!A1:B2` or
     * `[1]Sheet1:Sheet2!A1:B2`). Set to `null` for global references (e.g.
     * `globalname` or `[1]!extname`).
     */
    r1: Opt<SheetRef>;

    /**
     * Second sheet in the reference (e.g. `Sheet2` in `Sheet1:Sheet2!A1:B2`).
     * Set to `null` for single-sheet or global references.
     */
    r2: Opt<SheetRef>;

    // constructor ------------------------------------------------------------

    constructor(sheetRef1?: SheetRef, sheetRef2?: SheetRef, docRef?: DocRef) {
        this.doc = docRef;
        this.r1 = sheetRef1;
        this.r2 = sheetRef2;
    }

    // public properties ------------------------------------------------------

    /**
     * Returns whether this instance refers into another document.
     *
     * @returns
     *  Whether this instance refers to another spreadsheet document (`true`),
     *  or into the own document (`false`).
     */
    isExtDoc(): boolean {
        return !!this.doc?.isExtDoc();
    }

    /**
     * Returns whether a sheet reference of this instance is invalid (i.e., it
     * exists, AND it points to a non-existing sheet).
     *
     * @returns
     *  Whether a sheet reference of this instance is invalid.
     */
    hasError(): boolean {
        // sheet error: sheet reference must exist and must be invalid
        const { r1, r2 } = this;
        return (!!r1 && !r1.valid()) || (!!r2 && !r2.valid());
    }

    /**
     * Tries to replace unresolved sheet names with existing sheet indexes in
     * the spreadsheet document. Intended to be used after document import to
     * refresh all token arrays that refer to sheets that did not exist during
     * their creation.
     *
     * @param docAccess
     *  The document access interface.
     */
    refresh(docAccess: IDocumentAccess): void {
        if (!this.isExtDoc()) {
            this.r1?.refresh(docAccess);
            this.r2?.refresh(docAccess);
        }
    }

    /**
     * Refreshes the sheet references after a sheet has been inserted, deleted,
     * or moved in the document.
     *
     * @param xfVector
     *  The transformation vector for the sheet indexes.
     *
     * @returns
     *  Whether any sheet reference has been changed.
     */
    transform(xfVector: TransformSheetVector): boolean {
        if (this.isExtDoc()) { return false; }
        const changed1 = this.r1?.transform(xfVector);
        const changed2 = this.r2?.transform(xfVector);
        return !!(changed1 || changed2);
    }
}
