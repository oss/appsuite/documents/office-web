/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, math } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ErrorCode, isErrorCode } from '@/io.ox/office/spreadsheet/utils/scalar';
import { VarianceMethod, add, mul, div, binomial, erfc, variance } from '@/io.ox/office/spreadsheet/model/formula/utils/mathutils';
import { NumberMatrix, ensureMatrixDim } from '@/io.ox/office/spreadsheet/model/formula/formulautils';

const { floor, round, abs, pow, sqrt, exp, log, sin } = Math;

// constants ==================================================================

// minimum difference between iteration steps
const EPSILON = 1e-15;

// mathematical constants
const PI = Math.PI;
const PI2 = PI * 2;
const SQRT_1_2 = sqrt(0.5);
const SQRT_PI2_RECIP = 1 / sqrt(PI2);
const MAX_GAMMA_ARG = 171.624376956302;  // found experimental

// standard options for numeric parameter iterators
const SKIP_MAT_SKIP_REF_SKIP_EMPTY = { // id: CSS4
    valMode: 'convert', // value operands: convert strings and booleans to numbers
    matMode: 'skip', // matrix operands: skip strings and booleans
    refMode: 'skip', // reference operands: skip strings and booleans
    // emptyParam: false, // skip all empty parameters
    multiRange: true, // accept multi-range references
    multiSheet: true  // accept multi-sheet references
};

// standard options for numeric parameter iterators
const SKIP_MAT_SKIP_REF = { // id: CSS5
    valMode: 'convert', // value operands: convert strings and booleans to numbers
    matMode: 'skip', // matrix operands: skip strings and booleans
    refMode: 'skip', // reference operands: skip strings and booleans
    emptyParam: true, // empty parameters count as zero
    multiRange: true, // accept multi-range references
    multiSheet: true  // accept multi-sheet references
};

// standard options for the MODE functions
const MODE_OPTIONS = { // id: ESS3
    valMode: 'exact', // value operands: exact match for numbers (no strings, no booleans)
    matMode: 'skip', // matrix operands: skip strings and booleans
    refMode: 'skip', // reference operands: skip strings and booleans
    emptyParam: true, // empty parameters result in #VALUE! error
    multiRange: false, // do not accept multi-range references
    multiSheet: false  // do not accept multi-sheet references
};

// aggregation options for forecast/variance functions (FORECAST, COVARIANCE.P, STEYX, etc.)
const FORECAST_OPTIONS = {
    valMode: 'exact', // value operands: must be numbers
    matMode: 'skip', // matrix operands: skip strings and booleans
    refMode: 'skip', // reference operands: skip strings and booleans
    emptyParam: true, // empty parameter will lead to #VALUE! (due to 'exact' mode)
    multiRange: false, // do not accept multi-range references
    multiSheet: false, // do not accept multi-sheet references
    checkSize: 'size', // operands must have same element count, but ignore width/height (e.g. 2x2 and 1x4)
    sizeMismatchError: ErrorCode.NA // throw #N/A error code if the sizes do not match
};

// private functions ==========================================================

function expm1(value) {
    var e = exp(value);
    if (e === 1) {
        return value;
    }
    if (e - 1 === -1) {
        return -1;
    }
    return (e - 1) * value / log(e);
}

function log1p(value) {
    var p = 1 + value;

    if (p === 1) {
        return value;
    } else {
        return log(p) * value / (p - 1);
    }
}

/*
    *                x^a * (1-x)^b
    *    I_x(a,b) = ----------------  * result of ContFrac
    *                a * Beta(a,b)
    */
function getBetaHelperContFrac(x, a, b) {
    // like old version
    var a1 = 1.0,
        b1 = 1.0,
        a2,
        b2 = 1.0 - (a + b) / (a + 1.0) * x,
        fnorm, apl2m, d2m, d2m1,
        cfnew = 1.0,
        cf,
        rm = 1.0,
        bfinished = false,
        fMaxIter = 50000.0; // constant

    if (b2 === 0.0) {
        a2 = 0.0;
        fnorm = 1.0;
        cf = 1.0;
    } else {
        a2 = 1.0;
        fnorm = 1.0 / b2;
        cf = a2 * fnorm;
    }
    // loop security, normal cases converge in less than 100 iterations.
    // FIXME: You will get so much iteratons for fX near mean,
    // I do not know a better algorithm.

    do {
        apl2m = a + 2.0 * rm;
        d2m = rm * (b - rm) * x / ((apl2m - 1.0) * apl2m);
        d2m1 = -(a + rm) * (a + b + rm) * x / (apl2m * (apl2m + 1.0));
        a1 = (a2 + d2m * a1) * fnorm;
        b1 = (b2 + d2m * b1) * fnorm;
        a2 = a1 + d2m1 * a2 * fnorm;
        b2 = b1 + d2m1 * b2 * fnorm;

        if (b2 !== 0.0) {
            fnorm = 1.0 / b2;
            cfnew = a2 * fnorm;
            bfinished = (abs(cf - cfnew) < abs(cf) * EPSILON);
        }

        cf = cfnew;
        rm += 1.0;
    } while (rm < fMaxIter && !bfinished);

    return cf;
}

function getFDist(x, f1, f2) {
    var arg = f2 / (f2 + f1 * x),
        alpha = f2 / 2,
        beta = f1 / 2;

    return betaDist(arg, alpha, beta);
}

/**
 * The algorithm is based on lanczos13m53 in lanczos.hpp
 * in math library from http://www.boost.org
 * you must ensure z>0
 * Uses a variant of the Lanczos sum with a rational function.
 */
var getLanczosSum = (function () {
    var num = [
        23531376880.41075968857200767445163675473,
        42919803642.64909876895789904700198885093,
        35711959237.35566804944018545154716670596,
        17921034426.03720969991975575445893111267,
        6039542586.35202800506429164430729792107,
        1439720407.311721673663223072794912393972,
        248874557.8620541565114603864132294232163,
        31426415.58540019438061423162831820536287,
        2876370.628935372441225409051620849613599,
        186056.2653952234950402949897160456992822,
        8071.672002365816210638002902272250613822,
        210.8242777515793458725097339207133627117,
        2.506628274631000270164908177133837338626
    ];
    var denom = [
        0,
        39916800,
        120543840,
        150917976,
        105258076,
        45995730,
        13339535,
        2637558,
        357423,
        32670,
        1925,
        66,
        1
    ];

    return function (z) {
        // Horner scheme
        var sumNum,
            sumDenom,
            nI,
            zInv;

        if (z <= 1.0) {
            sumNum = num[12];
            sumDenom = denom[12];

            for (nI = 11; nI >= 0; --nI) {
                sumNum *= z;
                sumNum += num[nI];
                sumDenom *= z;
                sumDenom += denom[nI];
            }
        } else {
            // Cancel down with z^12; Horner scheme with reverse coefficients
            zInv = 1 / z;
            sumNum = num[0];
            sumDenom = denom[0];

            for (nI = 1; nI <= 12; ++nI) {
                sumNum *= zInv;
                sumNum += num[nI];
                sumDenom *= zInv;
                sumDenom += denom[nI];
            }
        }

        return sumNum / sumDenom;
    };
}());

/**
 * Helper function for getGamma.
 * The algorithm is based on tgamma in gamma.hpp
 * in math library from http://www.boost.org
 * You must ensure z>0; z>171.624376956302 will overflow.
 */
function getGammaHelper(z) {
    var gamma = getLanczosSum(z),
        g = 6.024680040776729583740234375,
        zgHelp = z + g - 0.5,
        fHalfpower = pow(zgHelp, z / 2 - 0.25); // avoid intermediate overflow

    gamma *= fHalfpower;
    gamma /= exp(zgHelp);
    gamma *= fHalfpower;

    if (z <= 20.0 && z === floor(z)) {
        gamma = round(gamma);
    }

    return gamma;
}

/**
 *  The algorithm is based on tgamma in gamma.hpp
 *  in math library from http://www.boost.org
 *  You must ensure z>0
 */
function getLogGammaHelper(z) {
    var g = 6.024680040776729583740234375,
        zgHelp = z + g - 0.5;

    return log(getLanczosSum(z)) + (z - 0.5) * log(zgHelp) - zgHelp;
}

// preconditions: 0.0 <= xs < xe <= n; xs,xe,n integral although double
// fFactor: q^n
function getBinomDistRange(n, xs, xe, fFactor, p, q) {
    var i,
        fSum,
        nXs = floor(xs); // skip summands index 0 to xs-1, start sum with index xs

    for (i = 1; i <= nXs && fFactor > 0; i++) {
        fFactor *= (n - i + 1) / i * p / q;
    }

    fSum = fFactor; // Summand xs
    var nXe = floor(xe);

    for (i = nXs + 1; i <= nXe && fFactor > 0; i++) {
        fFactor *= (n - i + 1) / i * p / q;
        fSum += fFactor;
    }

    return (fSum > 1) ? 1 : fSum;
}

// used in ScB and ScBinomDist
// preconditions: 0.0 <= x <= n, 0.0 < p < 1.0;  x,n integral although double
function getBinomDistPMF(x, n, p) {
    var q = (0.5 - p) + 0.5,
        fFactor = pow(q, n),
        count,
        i = 0;

    if (fFactor <= Number.MIN_VALUE) {
        fFactor = pow(p, n);

        if (fFactor <= Number.MIN_VALUE) {
            return betaDistPDF(p, x + 1.0, n - x + 1.0) / (n + 1.0);
        } else {
            count = floor(n - x);

            for (i = 0; i < count && fFactor > 0; i++) {
                fFactor *= (n - i) / (i + 1) * q / p;
            }

            return fFactor;
        }
    } else {
        count = floor(x);

        for (i = 0; i < count && fFactor > 0; i++) {
            fFactor *= (n - i) / (i + 1) * p / q;
        }

        return fFactor;
    }
}

function hasChangeOfSign(u, w) {
    return (u < 0 && w > 0) || (u > 0 && w < 0);
}

function betaDistFuncWrapper(x, p, alpha, beta) {
    return p - betaDist(x, alpha, beta);
}

function chiSqDistCDFWrapper(x, p, DF) {
    return p - getChiSqDistCDF(x, DF);
}

function chiDistWrapper(x, p, DF) {
    return p - getChiDist(x, DF);
}

function fDistWrapper(x, p, df1, df2) {
    return p - getFDist(x, df1, df2);
}

function gammaDistFuncWrapper(x, p, alpha, beta) {
    return p - getGammaDist(x, alpha, beta);
}

function tDistFuncWrapper(x, p, DF, type) {
    return p - getTDist(x, DF, type);
}

function iterateInverse(fAx, fBx, func) {
    var fYEps = 1e-307;
    if (fAx >= fBx) {
        globalLogger.error('$badge{StatUtils} iterateInverse: wrong interval');
        return;
    }
    //  find enclosing interval
    var fAy = func(fAx);
    var fBy = func(fBx);
    var fTemp;
    var nCount;

    for (nCount = 0; nCount < 1000 && !hasChangeOfSign(fAy, fBy); nCount++) {
        if (abs(fAy) <= abs(fBy)) {
            fTemp = fAx;
            fAx += 2.0 * (fAx - fBx);

            if (fAx < 0) {
                fAx = 0.0;
            }

            fBx = fTemp;
            fBy = fAy;
            fAy = func(fAx);
        } else {
            fTemp = fBx;
            fBx += 2.0 * (fBx - fAx);
            fAx = fTemp;
            fAy = fBy;
            fBy = func(fBx);
        }
    }

    if (fAy === 0) {
        return fAx;
    }

    if (fBy === 0) {
        return fBx;
    }

    if (!hasChangeOfSign(fAy, fBy)) {
        return null;
    }

    // inverse quadric interpolation with additional brackets
    // set three points
    var fPx = fAx;
    var fPy = fAy;
    var fQx = fBx;
    var fQy = fBy;
    var fRx = fAx;
    var fRy = fAy;
    var fSx = 0.5 * (fAx + fBx); // potential next point
    var bHasToInterpolate = true;
    nCount = 0;

    while (nCount < 500 && abs(fRy) > fYEps &&
        (fBx - fAx) > Math.max(abs(fAx), abs(fBx)) * EPSILON) {
        if (bHasToInterpolate) {
            if (fPy !== fQy && fQy !== fRy && fRy !== fPy) {
                fSx = fPx * fRy * fQy / (fRy - fPy) / (fQy - fPy) +
                    fRx * fQy * fPy / (fQy - fRy) / (fPy - fRy) +
                    fQx * fPy * fRy / (fPy - fQy) / (fRy - fQy);
                bHasToInterpolate = (fAx < fSx) && (fSx < fBx); // inside the brackets?
            } else {
                bHasToInterpolate = false;
            }
        }

        if (!bHasToInterpolate) {
            fSx = 0.5 * (fAx + fBx);
            // reset points
            fPx = fAx;
            fPy = fAy;
            fQx = fBx;
            fQy = fBy;
            bHasToInterpolate = true;
        }

        // shift points for next interpolation
        fPx = fQx;
        fQx = fRx;
        fRx = fSx;
        fPy = fQy;
        fQy = fRy;
        fRy = func(fSx);

        // update brackets
        if (hasChangeOfSign(fAy, fRy)) {
            fBx = fRx;
            fBy = fRy;
        } else {
            fAx = fRx;
            fAy = fRy;
        }

        // if last interration brought to small advance, then do bisection next
        // time, for safety
        bHasToInterpolate = bHasToInterpolate && (abs(fRy) * 2.0 <= abs(fQy));
        ++nCount;
    }

    return fRx;
}

// public functions ===========================================================

/**
 * Returns the value of the density function for a standard normal distribution.
 */
export function phi(number) {
    return SQRT_PI2_RECIP * exp(-number * number / 2);
}

export function integralPhi(x) {
    return erfc(-x * SQRT_1_2) / 2;
}

/**
 * Returns the value of taylor's polynomial multiplied by value x.
 */
export function taylor(polynomial, length, x) {
    var value = polynomial[length];

    for (var i = length - 1; i >= 0; i -= 1) {
        value = polynomial[i] + (value * x);
    }
    return value;
}


var t0 = [
    0.39894228040143268, -0.06649038006690545,  0.00997355701003582,
    -0.00118732821548045,  0.00011543468761616, -0.00000944465625950,
    0.00000066596935163, -0.00000004122667415,  0.00000000227352982,
    0.00000000011301172,  0.00000000000511243, -0.00000000000021218
];
var t2 = [
    0.47724986805182079,  0.05399096651318805, -0.05399096651318805,
    0.02699548325659403, -0.00449924720943234, -0.00224962360471617,
    0.00134977416282970, -0.00011783742691370, -0.00011515930357476,
    0.00003704737285544,  0.00000282690796889, -0.00000354513195524,
    0.00000037669563126,  0.00000019202407921, -0.00000005226908590,
    -0.00000000491799345,  0.00000000366377919, -0.00000000015981997,
    -0.00000000017381238,  0.00000000002624031,  0.00000000000560919,
    -0.00000000000172127, -0.00000000000008634,  0.00000000000007894
];
var t4 = [
    0.49996832875816688,  0.00013383022576489, -0.00026766045152977,
    0.00033457556441221, -0.00028996548915725,  0.00018178605666397,
    -0.00008252863922168,  0.00002551802519049, -0.00000391665839292,
    -0.00000074018205222,  0.00000064422023359, -0.00000017370155340,
    0.00000000909595465,  0.00000000944943118, -0.00000000329957075,
    0.00000000029492075,  0.00000000011874477, -0.00000000004420396,
    0.00000000000361422,  0.00000000000143638, -0.00000000000045848
];
var asympt = [-1.0, 1.0, -3.0, 15.0, -105.0];

/**
 * Calculates and returns the probability that a member of a standard normal population
 * will fall between the mean and x standard deviations from the mean.
 */
export function gauss(x) {

    var xAbs = abs(x);
    var xInt = floor(xAbs);
    var val = 0.0;

    if (xInt === 0) {
        val = taylor(t0, 11, (xAbs * xAbs)) * xAbs;
    } else if ((xInt >= 1) && (xInt <= 2)) {
        val = taylor(t2, 23, (xAbs - 2.0));
    } else if ((xInt >= 3) && (xInt <= 4)) {
        val = taylor(t4, 20, (xAbs - 4.0));
    } else {
        val = 0.5 + phi(xAbs) * taylor(asympt, 4, 1.0 / (xAbs * xAbs)) / xAbs;
    }

    return (x < 0) ? -val : val;
}

/**
 * Implementation of inverse Gaussian distribution function.
 */
export function gaussInv(x) {
    var q = x - 0.5,
        t,
        z;

    if (abs(q) <= 0.425) {
        t = 0.180625 - q * q;
        z = q * (((((((t * 2509.0809287301226727 + 33430.575583588128105) * t + 67265.770927008700853) * t + 45921.953931549871457) * t + 13731.693765509461125) * t + 1971.5909503065514427) * t + 133.14166789178437745) * t + 3.387132872796366608) /
            (((((((t * 5226.495278852854561 + 28729.085735721942674) * t + 39307.89580009271061) * t + 21213.794301586595867) * t + 5394.1960214247511077) * t + 687.1870074920579083) * t + 42.313330701600911252) * t + 1.0);
    } else {
        if (q > 0) {
            t = 1 - x;
        } else {
            t = x;
        }

        t = sqrt(-log(t));

        if (t <= 5.0) {
            t += -1.6;
            z = (((((((t * 7.7454501427834140764e-4 + 0.0227238449892691845833) * t + 0.24178072517745061177) * t + 1.27045825245236838258) * t + 3.64784832476320460504) * t + 5.7694972214606914055) * t + 4.6303378461565452959) * t + 1.42343711074968357734) /
                (((((((t * 1.05075007164441684324e-9 + 5.475938084995344946e-4) * t + 0.0151986665636164571966) * t + 0.14810397642748007459) * t + 0.68976733498510000455) * t + 1.6763848301838038494) * t + 2.05319162663775882187) * t + 1.0);
        } else {
            t += -5.0;
            z = (((((((t * 2.01033439929228813265e-7 + 2.71155556874348757815e-5) * t + 0.0012426609473880784386) * t + 0.026532189526576123093) * t + 0.29656057182850489123) * t + 1.7848265399172913358) * t + 5.4637849111641143699) * t + 6.6579046435011037772) /
                (((((((t * 2.04426310338993978564e-15 + 1.4215117583164458887e-7) * t + 1.8463183175100546818e-5) * t + 7.868691311456132591e-4) * t + 0.0148753612908506148525) * t + 0.13692988092273580531) * t + 0.59983220655588793769) * t + 1.0);
        }

        if (q < 0.0) {
            z = -z;
        }
    }

    return z;
}

/**
 * Implementation of Beta distribution function.
 */
export function betaDist(x, alpha, beta) {
    // special cases
    if (x <= 0.0) { // values are valid, see spec
        return 0.0;
    }

    if (x >= 1.0) { // values are valid, see spec
        return 1.0;
    }

    if (beta === 1.0) {
        return pow(x, alpha);
    }

    if (alpha === 1.0) {
        // 1.0 - pow(1.0-x,beta) is not accurate enough
        return -expm1(beta * log1p(-x));
    }

    //FIXME: need special algorithm for x near p for large a,b
    var result;
    // I use always continued fraction, power series are neither
    // faster nor more accurate.
    var y = (0.5 - x) + 0.5;
    var lnY = log1p(-x);
    var lnX = log(x);
    var tempX = x;
    var a = alpha;
    var b = beta;
    var reflect = x > alpha / (alpha + beta);

    if (reflect) {
        a = beta;
        b = alpha;
        tempX = y;
        y = x;
        lnX = lnY;
        lnY = log(x);
    }

    result = getBetaHelperContFrac(tempX, a, b) / a;
    var p = a / (a + b);
    var q = b / (a + b);
    var temp;

    if (a > 1.0 && b > 1.0 && p < 0.97 && q < 0.97) { //found experimental
        temp = betaDistPDF(tempX, a, b) * tempX * y;
    } else {
        temp = exp(a * lnX + b * lnY - logBeta(a, b));
    }

    result *= temp;

    if (reflect) {
        result = 0.5 - result + 0.5;
    }

    if (result > 1.0) { // ensure valid range
        result = 1.0;
    }

    if (result < 0.0) {
        result = 0.0;
    }

    return result;
}

/**
 * Beta distribution probability density function
 */
export function betaDistPDF(fX, fA, fB) {
    // special cases
    if (fA === 1.0) { // result b*(1-x)^(b-1)
        if (fB === 1.0) {
            return 1.0;
        }

        if (fB === 2.0) {
            return -2.0 * fX + 2.0;
        }

        if (fX === 1.0 && fB < 1.0) {
            throw ErrorCode.NUM;
        }

        if (fX <= 0.01) {
            return fB + fB * expm1((fB - 1.0) * log1p(-fX));
        } else {
            return fB * pow(0.5 - fX + 0.5, fB - 1.0);
        }
    }

    if (fB === 1.0) { // result a*x^(a-1)
        if (fA === 2.0) {
            return fA * fX;
        }

        if (fX === 0.0 && fA < 1.0) {
            throw ErrorCode.NUM;
        }

        return fA * pow(fX, fA - 1);
    }

    if (fX <= 0.0) {
        if (fA < 1.0 && fX === 0.0) {
            throw ErrorCode.NUM;
        } else {
            return 0.0;
        }
    }

    if (fX >= 1.0) {
        if (fB < 1.0 && fX === 1.0) {
            throw ErrorCode.NUM;
        } else {
            return 0.0;
        }
    }

    // normal cases; result x^(a-1)*(1-x)^(b-1)/Beta(a,b)
    var fLogDblMax = log(Number.MAX_VALUE);
    var fLogDblMin = log(Number.MIN_VALUE);
    var fLogY = (fX < 0.1) ? log1p(-fX) : log(0.5 - fX + 0.5);
    var fLogX = log(fX);
    var fAm1LogX = (fA - 1.0) * fLogX;
    var fBm1LogY = (fB - 1.0) * fLogY;
    var fLogBeta = logBeta(fA, fB);

    // check whether parts over- or underflow
    if (fAm1LogX < fLogDblMax  && fAm1LogX > fLogDblMin &&
        fBm1LogY < fLogDblMax  && fBm1LogY > fLogDblMin &&
        fLogBeta < fLogDblMax  && fLogBeta > fLogDblMin &&
        fAm1LogX + fBm1LogY < fLogDblMax && fAm1LogX + fBm1LogY > fLogDblMin) {
        return pow(fX, fA - 1.0) * pow(0.5 - fX + 0.5, fB - 1.0) / getBeta(fA, fB);
    } else {
        // need logarithm;
        // might overflow as a whole, but seldom, not worth to pre-detect it
        return exp(fAm1LogX + fBm1LogY - fLogBeta);
    }
}

export function getBeta(alpha, beta) {
    var a, b;

    if (alpha > beta) {
        a = alpha;
        b = beta;
    } else {
        a = beta;
        b = alpha;
    }

    if (a + b < MAX_GAMMA_ARG) { // simple case
        return getGamma(a) / getGamma(a + b) * getGamma(b);
    }

    // need logarithm
    // GetLogGamma is not accurate enough, back to Lanczos for all three
    // getGamma and arrange factors newly.
    var g = 6.024680040776729583740234375; //see getGamma
    var gm = g - 0.5;
    var lanczos = getLanczosSum(a);
    lanczos /= getLanczosSum(a + b);
    lanczos *= getLanczosSum(b);
    var ABgm = a + b + gm;
    lanczos *= sqrt((ABgm / (a + gm)) / (b + gm));
    var tempA = b / (a + gm); // (a+gm)/ABgm = 1 / ( 1 + b/(a+gm))
    var tempB = a / (b + gm);
    var result = exp(-a * log1p(tempA) - b * log1p(tempB) - gm);
    result *= lanczos;

    return result;
}

/**
 * Returns the inverse of the cumulative distribution function for a specified beta distribution.
 */
export function betaInv(p, alpha, beta, a, b) {
    var val;

    if (!is.number(a)) { a = 0; }
    if (!is.number(b)) { b = 1; }

    if (p < 0 || p >= 1 || a === b || alpha <= 0 || beta <= 0) {
        throw ErrorCode.NA;
    }

    if (p === 0) {
        return 0;
    } else {
        // 0..1 as range for iteration so it isn't extended beyond the valid range
        val = iterateInverse(0, 1, function (x) { return betaDistFuncWrapper(x, p, alpha, beta); });

        if (val === null) {
            throw ErrorCode.NUM;
        } else {
            return a + val * (b - a);    // scale to (A,B)
        }
    }
}

// Same as getBeta but with logarithm
export function logBeta(alpha, beta) {
    var a, b,
        g = 6.024680040776729583740234375, //see getGamma
        gm = g - 0.5,
        fLanczos,
        fLogLanczos,
        fABgm,
        fTempA, fTempB, fResult;

    if (alpha > beta) {
        a = alpha;
        b = beta;
    } else {
        a = beta;
        b = alpha;
    }

    fLanczos = getLanczosSum(a);
    fLanczos /= getLanczosSum(a + b);
    fLanczos *= getLanczosSum(b);
    fLogLanczos = log(fLanczos);
    fABgm = a + b + gm;
    fLogLanczos += 0.5 * (log(fABgm) - log(a + gm) - log(b + gm));
    fTempA = b / (a + gm); // (a+gm)/fABgm = 1 / ( 1 + b/(a+gm))
    fTempB = a / (b + gm);
    fResult = -a * log1p(fTempA) - b * log1p(fTempB) - gm;
    fResult += fLogLanczos;

    return fResult;
}

/**
 * Returns the individual term binomial distribution probability.
 *
 */
export function getBinomDist(x, n, p, isCum) {
    var q = (0.5 - p) + 0.5,
        factor,
        sum,
        count;

    x = floor(x); // truncated, see func spec
    n = floor(n); // trunc
    if (n < 0 || x < 0 || x > n || p < 0 || p > 1) { throw ErrorCode.NUM; }

    if (p === 0) { return (x === 0 || isCum) ? 1 : 0; }
    if (p === 1) { return (x === n) ? 1 : 0; }
    if (!isCum) { return getBinomDistPMF(x, n, p); }
    if (x === n) { return 1; }
    if (x === 0) { return factor; }

    factor = pow(q, n);

    if (factor <= Number.MIN_VALUE) {
        factor = pow(p, n);

        if (factor <= Number.MIN_VALUE) {
            return betaDist(q, n - x, x + 1);
        }
        if (factor > EPSILON) {
            sum = 1 - factor;
            count = floor(n - x - 1);

            for (var i = 0; i < count && factor > 0; i++) {
                factor *= (n - i) / (i + 1) * q / p;
                sum -= factor;
            }

            return (sum < 0) ? 0 : sum;
        }
        return getBinomDistRange(n, n - x, n, factor, q, p);
    }
    return getBinomDistRange(n, 0.0, x, factor, p, q);
}

/**
 * Returns the inverse of the cumulative distribution function for a specified beta distribution.
 */
export function binomInv(n, p, alpha) {

    n = floor(n);

    var i, fSum;

    if (n < 0.0 || alpha <= 0.0 || alpha >= 1.0 || p < 0.0 || p > 1.0) {
        throw ErrorCode.NA;
    }

    var q = 1.0 - p;
    var factor = pow(q, n);

    if (factor === 0) {

        factor = pow(p, n);
        if (factor === 0) { throw ErrorCode.VALUE; }

        fSum = 1.0 - factor;
        for (i = 0; i < n && fSum >= alpha; i++) {
            factor *= (n - i) / (i + 1) * q / p;
            fSum -= factor;
        }

        return (n - i);
    }

    fSum = factor;
    for (i = 0; i < n && fSum < alpha; i++) {
        factor *= (n - i) / (i + 1) * p / q;
        fSum += factor;
    }

    return i;
}

/**
 * Returns the probability of a trial result using a binomial distribution.
 */
export function binomDistRange(n, p, s, s2) {
    n = floor(n); // trunc to integer
    s = floor(s); // trunc to integer
    if (!is.number(s2)) { // mass function
        if (n < 0 || s < 0 || s > n || p < 0 || p > 1) { throw ErrorCode.NA; }
        if (p === 0) { return (s === 0) ? 1 : 0; }
        if (p === 1) { return (s === n) ? 1 : 0; }
        return getBinomDistPMF(s, n, p);
    }

    // nParamCount == 4
    s2 = floor(s2); // trunc to integer
    var q = (0.5 - p) + 0.5;
    var isValidX = (0 <= s && s <= s2 && s2 <= n);

    if (isValidX && 0 < p && p < 1) {

        if (s === s2) { // mass function
            return getBinomDistPMF(s, n, p);
        }

        var factor = q ** n;
        if (factor > Number.MIN_VALUE) {
            return getBinomDistRange(n, s, s2, factor, p, q);
        }

        factor = p ** n;
        if (factor > Number.MIN_VALUE) {
            // sum from j=s to s2 {(n choose j) * p^j * q^(n-j)}
            // = sum from i = n-s2 to n-s { (n choose i) * q^i * p^(n-i)}
            return getBinomDistRange(n, n - s2, n - s, factor, q, p);
        }

        return (betaDist(q, n - s2, s2 + 1) - betaDist(q, n - s + 1, s));
    }

    if (isValidX) { // not(0<p<1)
        if (p === 0) { return (s === 0) ? 1 : 0; }
        if (p === 1) { return (s2 === n) ? 1 : 0; }
    }

    throw ErrorCode.NA;
}

/**
 * Returns the cumulative beta probability density function.
 */
export function chiSqDist(x, DF, cumulative) {
    if (!is.boolean(cumulative)) {
        cumulative = true;
    }
    DF = floor(DF); // truncate degrees of freedom

    if (DF < 1) {
        throw ErrorCode.NA;
    }

    return cumulative ? getChiSqDistCDF(x, DF) : getChiSqDistPDF(x, DF);
}

/**
 * Returns the cumulative beta probability density function.
 */
export function chiSqInv(p, DF) {
    DF = floor(DF); // truncate degrees of freedom

    if (DF < 1 || p < 0 || p >= 1) {
        throw ErrorCode.NA;
    }

    var val = iterateInverse(DF * 0.5, DF, function (x) { return chiSqDistCDFWrapper(x, p, DF); });

    if (val === null) {
        throw ErrorCode.NUM;
    }

    return val;
}

/**
 * Returns the inverse of the one-tailed probability of the chi-squared distribution.
 * Replaces chiInv.
 */
export function chiInv(p, DF) {
    var val;
    DF  = floor(DF); // trunc Degrees of Freedom to integer

    if (DF < 1 || p <= 0 || p > 1) {
        throw ErrorCode.NA;
    }
    val = iterateInverse(DF * 0.5, DF, function (x) { return chiDistWrapper(x, p, DF); });

    if (val === null) {
        throw ErrorCode.NUM;
    }

    return val;
}

// for LEGACY.CHIDIST, returns right tail, fDF=degrees of freedom
/** You must ensure fDF>0.0 */
export function getChiDist(x, DF) {
    return (x <= 0) ? 1 : getUpRegIGamma(DF / 2, x / 2);
}

export function getChiSqDistCDF(x, DF) {
    return (x <= 0) ? 0 : getLowRegIGamma(DF / 2, x / 2);
}

export function getChiSqDistPDF(x, DF) {
    // you must ensure DF is positive integer
    var fValue,
        fCount;

    if (x <= 0) {
        return 0;    // see ODFF
    }

    if (DF * x > 1391000.0) {
        // intermediate invalid values, use log
        fValue = exp((0.5 * DF - 1) * log(x * 0.5) - 0.5 * x - log(2.0) - getLogGamma(0.5 * DF));
    } else { // DF is small in most cases, we can iterate
        if ((DF % 2.0) < 0.5) {
            // even
            fValue = 0.5;
            fCount = 2.0;
        } else {
            fValue = 1 / sqrt(x * PI2);
            fCount = 1.0;
        }

        while (fCount < DF) {
            fValue *= (x / fCount);
            fCount += 2.0;
        }

        if (x >= 1425.0) { // underflow in e^(-x/2)
            fValue = exp(log(fValue) - x / 2);
        } else {
            fValue *= exp(-x / 2);
        }
    }

    return fValue;
}

/**
 * Returns the test for independence.
 */
export function chiSqTest(mat1, mat2) {
    var numCol1 = mat1.cols(),
        numCol2 = mat2.cols(),
        numRows1 = mat1.rows(),
        numRows2 = mat2.rows(),
        valX,
        valE,
        chi = 0,
        DF;

    if (numRows1 !== numRows2 || numCol1 !== numCol2) {
        throw ErrorCode.NA;
    }

    for (var i = 0; i < numCol1; i++) {
        for (var j = 0; j < numRows1; j++) {
            valX = mat1.get(j, i);
            valE = mat2.get(j, i);
            chi += (valX - valE) * (valX - valE) / valE;
        }
    }

    if (numCol1 === 1 || numRows1 === 1) {
        DF = numCol1 * numRows1 - 1;

        if (DF === 0) {
            throw ErrorCode.VALUE;
        }
    } else {
        DF = (numCol1 - 1) * (numRows1 - 1);
    }

    return getChiDist(chi, DF);
}

/**
 * Returns the confidence interval for a population mean.
 */
export function confidence(alpha, sigma, n) {
    n = floor(n);

    if (sigma <= 0 || alpha <= 0 || alpha >= 1 || n < 1) {
        throw ErrorCode.NA;
    }
    return (gaussInv(1.0 - alpha / 2.0) * sigma / sqrt(n));
}

/**
 * Returns the
 */
export function confidenceT(alpha, sigma, n) {
    n = floor(n);

    if (sigma <= 0 || alpha <= 0 || alpha >= 1 || n < 1) {
        throw ErrorCode.NA;
    }
    return sigma * getTInv(alpha, n - 1, 2) / sqrt(n);
}

/**
 * Returns the exponential distribution.
 */
export function exponDist(x, lambda, isCumulative) {
    if (lambda <= 0) {
        throw ErrorCode.NA;
    }
    if (!isCumulative) { // density
        return (x >= 0)  ? (lambda * exp(-lambda * x)) : 0;
    }
    // distribution
    return (x > 0) ? (1 - exp(-lambda * x)) : 0;
}

/**
 * F probability distribution, left tail.
 */
export function fDistLeftTail(x, df1, df2, isCumulative) {
    if (!is.boolean(isCumulative)) {
        isCumulative = true;
    }
    // trunc degrees of freedom to int
    df1 = floor(df1);
    df2 = floor(df2);

    if (x < 0 || df1 < 1 || df2 < 1 || df1 >= 1.0E10 || df2 >= 1.0E10) {
        throw ErrorCode.NA;
    }
    if (isCumulative) {
        // left tail cumulative distribution
        return 1 - getFDist(x, df1, df2);
    } else {
        // probability density function
        return (pow(df1 / df2, df1 / 2) * pow(x, (df1 / 2) - 1) / (pow((1 + (x * df1 / df2)), (df1 + df2) / 2) * getBeta(df1 / 2, df2 / 2)));
    }
}

/**
 * F probability distribution, right tail.
 */
export function fDistRightTail(x, df1, df2) {
    // trunc degrees of freedom to int
    df1 = floor(df1);
    df2 = floor(df2);

    if (x < 0 || df1 < 1 || df2 < 1 || df1 >= 1.0E10 || df2 >= 1.0E10) {
        throw ErrorCode.NA;
    }

    return getFDist(x, df1, df2);
}

/**
 * Returns the inverse of the F probability distribution.
 */
export function fInv(p, f1, f2) {
    f1 = floor(f1);
    f2 = floor(f2);

    if (p <= 0 || f1 < 1.0 || f2 < 1.0 || f1 >= 1.0E10 || f2 >= 1.0E10 || p > 1.0) {
        throw ErrorCode.NA;
    }

    var val = iterateInverse(f1 * 0.5, f1, function (x) { return fDistWrapper(x, p, f1, f2); });

    if (val === null) {
        throw ErrorCode.NUM;
    }

    return val;
}

/**
 * Returns the inverse of the F probability distribution, left tailed.
 */
export function fInvLeftTail(p, f1, f2) {
    f1 = floor(f1);
    f2 = floor(f2);

    if (p <= 0 || f1 < 1.0 || f2 < 1.0 || f1 >= 1.0E10 || f2 >= 1.0E10 || p > 1.0) {
        throw ErrorCode.NA;
    }

    var val = iterateInverse(f1 * 0.5, f1, function (x) { return fDistWrapper(x, (1 - p), f1, f2); });

    if (val === null) {
        throw ErrorCode.NUM;
    }

    return val;
}

export function fTest(mat1, mat2) {
    var numCol1 = mat1.cols(),
        numCol2 = mat2.cols(),
        numRows1 = mat1.rows(),
        numRows2 = mat2.rows(),
        count1 = 0,
        count2 = 0,
        sum1 = 0,
        sumSqr1 = 0,
        sum2 = 0,
        sumSqr2 = 0,
        s1, s2,
        f, f1, f2,
        val,
        i, j;

    if (numRows1 !== numRows2 || numCol1 !== numCol2) {
        throw ErrorCode.NA;
    }

    for (i = 0; i < numCol1; i++) {
        for (j = 0; j < numRows1; j++) {
            val = mat1.get(j, i);
            sum1 += val;
            sumSqr1 += val * val;
            count1++;
        }
    }

    for (i = 0; i < numCol2; i++) {
        for (j = 0; j < numRows2; j++) {
            val = mat2.get(j, i);
            sum2 += val;
            sumSqr2 += val * val;
            count2++;
        }
    }

    if (count1 < 2 || count2 < 2) {
        throw ErrorCode.VALUE;
    }

    s1 = (sumSqr1 - sum1 * sum1 / count1) / (count1 - 1);
    s2 = (sumSqr2 - sum2 * sum2 / count2) / (count2 - 1);

    if (s1 === 0 || s2 === 0) {
        throw ErrorCode.VALUE;
    }

    if (s1 > s2) {
        f = s1 / s2;
        f1 = count1 - 1;
        f2 = count2 - 1;
    } else {
        f = s2 / s1;
        f1 = count2 - 1;
        f2 = count1 - 1;
    }

    return (2 * getFDist(f, f1, f2));
}

export function getLowRegIGamma(a, x) {
    var lnFactor = a * log(x) - x - getLogGamma(a);
    var factor = exp(lnFactor);    // Do we need more accuracy than exp(ln()) has?

    if (x > a + 1) { // includes X>1.0; 1-GetUpRegIGamma, continued fraction
        return 1 - factor * getGammaContFraction(a, x);
    } else {        // x<=1.0 || x<=fA+1.0, series
        return factor * getGammaSeries(a, x);
    }
}

export function getUpRegIGamma(a, x) {
    var lnFactor = a * log(x) - x - getLogGamma(a),
        factor = exp(lnFactor); //Do I need more accuracy than exp(ln()) has?;

    if (x > a + 1.0) { // includes x>1.0
        return factor * getGammaContFraction(a, x);
    } else { //x<=1 || x<=fA+1, 1-GetLowRegIGamma, series
        return 1.0 - factor * getGammaSeries(a, x);
    }
}

export function getLogGamma(z) {

    if (z >= MAX_GAMMA_ARG) {
        return getLogGammaHelper(z);
    }

    if (z >= 1) {
        return log(getGammaHelper(z));
    }

    if (z >= 0.5) {
        return log(getGammaHelper(z + 1) / z);
    }

    return getLogGammaHelper(z + 2) - log(z + 1) - log(z);
}

// The idea how this group of gamma functions is calculated, is
// based on the Cephes library
// online http://www.moshier.net/#Cephes [called 2008-02]
export function getGammaContFraction(fA, fX) {
    var fBigInv = EPSILON,
        fBig = 1 / fBigInv,
        fCount = 0,
        fNum = 0,  // dummy value
        fY = 1 - fA,
        fDenom = fX + 2 - fA,
        fPk = 0,   // dummy value
        fPkm1 = fX + 1,
        fPkm2 = 1,
        fQk = 1,   // dummy value
        fQkm1 = fDenom * fX,
        fQkm2 = fX,
        fApprox = fPkm1 / fQkm1,
        bFinished = false,
        fR = 0;    // dummy value

    do {
        fCount += 1;
        fY += 1;
        fNum = fY * fCount;
        fDenom += 2;
        fPk = fPkm1 * fDenom - fPkm2 * fNum;
        fQk = fQkm1 * fDenom - fQkm2 * fNum;

        if (fQk !== 0) {
            fR = fPk / fQk;
            bFinished = (abs((fApprox - fR) / fR) <= 0.5 * EPSILON);
            fApprox = fR;
        }

        fPkm2 = fPkm1;
        fPkm1 = fPk;
        fQkm2 = fQkm1;
        fQkm1 = fQk;

        if (abs(fPk) > fBig) {
            // reduce a fraction does not change the value
            fPkm2 *= fBigInv;
            fPkm1 *= fBigInv;
            fQkm2 *= fBigInv;
            fQkm1 *= fBigInv;
        }
    } while (!bFinished && fCount < 10000);

    // most iterations, if fX==fAlpha+1.0; approx sqrt(fAlpha) iterations then
    if (!bFinished) {
        throw ErrorCode.NUM;
    }

    return fApprox;
}

export function getGammaSeries(fA, fX) {
    var fDenomfactor = fA,
        fSummand = 1 / fA,
        fSum = fSummand,
        nCount = 1;

    do {
        fDenomfactor += 1;
        fSummand *= fX / fDenomfactor;
        fSum += fSummand;
        nCount += 1;
    } while (fSummand / fSum > 0.5 * EPSILON && nCount <= 10000);

    // large amount of iterations will be carried out for huge fAlpha, even
    // if fX <= fAlpha+1.0
    if (nCount > 10000) {
        throw ErrorCode.NUM;
    }

    return fSum;
}

export function getGamma(z) {
    var logPi = log(PI),
        logMaxVal = log(Number.MAX_VALUE),
        logDivisor,
        fLogTest;

    if (z > MAX_GAMMA_ARG) {
        throw ErrorCode.NUM;
    }

    if (z >= 1) {
        return getGammaHelper(z);
    }

    if (z >= 0.5) { // shift to x>=1 using Gamma(x)=Gamma(x+1)/x
        return getGammaHelper(z + 1) / z;
    }

    if (z >= -0.5) { // shift to x>=1, might overflow
        fLogTest = getLogGammaHelper(z + 2) - log(z + 1) - log(abs(z));

        if (fLogTest >= logMaxVal) {
            throw ErrorCode.NUM;
        }

        return getGammaHelper(z + 2) / (z + 1) / z;
    }

    // z<-0.5
    // Use Euler's reflection formula: gamma(x)= pi/ ( gamma(1-x)*sin(pi*x) )
    logDivisor = getLogGammaHelper(1 - z) + log(abs(sin(PI * z)));

    if (logDivisor - logPi >= logMaxVal) {   // underflow
        return 0.0;
    }

    if (logDivisor < 0) {
        if (logPi - logDivisor > logMaxVal) { // overflow
            throw ErrorCode.NUM;
        }
    }

    return exp(logPi - logDivisor) * ((sin(PI * z) < 0.0) ? -1.0 : 1.0);
}

export function getGammaDist(x, alpha, lambda) {
    return (x <= 0) ? 0 : getLowRegIGamma(alpha, x / lambda);
}

export function getGammaDistPDF(x, alpha, lambda) {

    // see ODFF
    if (x <= 0) { return 0; }

    // use exp(ln()) only for large arguments because of less accuracy
    var xr = x / lambda;
    if (xr > 1) {
        var logDblMax = log(Number.MAX_VALUE);

        if (log(xr) * (alpha - 1.0) < logDblMax && alpha < MAX_GAMMA_ARG) {
            return pow(xr, alpha - 1.0) * exp(-xr) / lambda / getGamma(alpha);
        }
        return exp((alpha - 1.0) * log(xr) - xr - log(lambda) - getLogGamma(alpha));
    }

    // xr near to zero
    if (alpha < MAX_GAMMA_ARG) {
        return pow(xr, alpha - 1.0) * exp(-xr) / lambda / getGamma(alpha);
    }
    return pow(xr, alpha - 1.0) * exp(-xr) / lambda / exp(getLogGamma(alpha));
}

/**
 * Returns the gamma distribution.
 */
export function gammaDist(x, alpha, beta, isCumulative) {

    // default is true
    isCumulative ??= true;

    if (alpha <= 0 || beta <= 0) {
        throw ErrorCode.NUM;
    } else {
        if (isCumulative) {                      // distribution
            return getGammaDist(x, alpha, beta);
        } else {                                // density
            return getGammaDistPDF(x, alpha, beta);
        }
    }
}

/**
 * Returns the inverse of gamma cumulative distribution.
 */
export function gammaInv(p, alpha, beta) {
    var val,
        start;

    if (alpha <= 0 || beta <= 0 || p < 0 || p >= 1) {
        throw ErrorCode.NUM;
    }

    if (p === 0) { return 0; }

    start = alpha * beta;
    val = iterateInverse(start * 0.5, start, function (x) { return gammaDistFuncWrapper(x, p, alpha, beta); });

    if (val === null) {
        throw ErrorCode.NUM;
    }

    return val;
}

/**
 * Helper function for negative binom distribution.
 */
export function getNegBinomDist(x, r, p) {
    var q = 1 - p,
        factor = pow(p, r);

    for (var i = 0; i < x; i++) {
        factor *= (i + r) / (i + 1) * q;
    }
    return factor;
}

/**
 * Helper function for poison distribution.
 */
export function poisson(x, lambda, isCumulative) {
    var poissonVar = 1,
        summand, sum, end;

    if (!is.boolean(isCumulative)) {
        isCumulative = true;
    }

    if (lambda < 0 || x < 0) {
        throw ErrorCode.NUM;
    }

    if (!isCumulative) {
        if (lambda === 0) {
            return 0;
        }

        if (lambda > 712) {   // underflow in exp(-lambda)
            // accuracy 11 Digits
            return (exp(x * log(lambda) - lambda - getLogGamma(x + 1)));
        }

        for (var f = 0; f < x; ++f) {
            poissonVar *= lambda / (f + 1);
        }
        return poissonVar * exp(-lambda);
    }

    if (lambda === 0) {
        return 1;
    }

    // underflow in exp(-lambda) (accuracy 12 digits)
    if (lambda > 712) {
        return getUpRegIGamma(x + 1, lambda);
    }

    // result is always undistinghable from 1
    if (x >= 936) {
        return 1;
    }

    summand = exp(-lambda);
    sum = summand;
    end = floor(x);
    for (var i = 1; i <= end; i++) {
        summand *= lambda / i;
        sum += summand;
    }
    return sum;
}

/**
 * Helper function for hypergeometric distribution.
 */
export function getHypGeomDist(x, n, M, N) {

    var leftUpper = binomial(M, x);
    var rightUpper = binomial(N - M, n - x);
    var bottom = binomial(N, n);

    return leftUpper * rightUpper / bottom;
}

/**
 * Helper function to get an exclusive percentile.
 */
export function getPercentileExclusive(numbers, p, sorted) {

    var size1 = numbers.length + 1;
    if (is.empty(numbers) || size1 === 1) { throw ErrorCode.VALUE; }
    if ((p <= 0) || (p >= 1)) { throw ErrorCode.NUM; }
    if (p * size1 < 1 || p * size1 > (size1 - 1)) { throw ErrorCode.NUM; }

    var index = floor(p * size1 - 1);
    var diff = p *  size1 - 1 - floor(p * size1 - 1);
    if (index > size1 - 1) {
        globalLogger.warn('$badge{StatUtils} getPercentileExclusive: wrong index');
    }

    if (!sorted) { numbers.sort(function (a, b) { return a - b; }); }
    var iter = numbers[index];
    if (diff === 0) { return iter; }

    if (index > size1) {
        globalLogger.warn('$badge{StatUtils} getPercentileExclusive: wrong index');
    }
    var val = iter;
    iter = numbers[index + 1];
    return val + diff * (iter - val);
}

/**
 * Helper function to get an inclusive percentile.
 */
export function getPercentileInclusive(numbers, p, sorted) {

    var size = numbers.length;
    if (size === 0) { throw ErrorCode.VALUE; }
    if ((p < 0) || (p > 1)) { throw ErrorCode.NUM; }

    if (size === 1) {
        return numbers[0];
    }

    var index = floor(p * (size - 1));
    var diff = p * (size - 1) - floor(p * (size - 1));
    if (index > size) {
        globalLogger.warn('$badge{StatUtils} getPercentileInclusive: wrong index');
    }

    if (!sorted) { numbers.sort(math.compare); }
    var iter = numbers[index];
    if (diff === 0) { return iter; }

    if (index > size - 1) {
        globalLogger.warn('$badge{StatUtils} getPercentileInclusive: wrong index');
    }
    var val = iter;
    iter = numbers[index + 1];
    return val + diff * (iter - val);
}

/**
 * Calculates inclusive or exclusive percent rank.
 */
export function percentRank(nums, val, significance, isInclusive) {

    var sortedNums = nums ? nums.sort(function (a, b) { return a - b; }) : [];
    var size = sortedNums.length;

    significance = is.number(significance) ? significance : 3;
    if (significance < 1) { throw ErrorCode.NUM; }

    if (size === 0) { throw ErrorCode.NUM; }
    if (val < sortedNums[0] || val > sortedNums[size - 1]) { throw ErrorCode.VALUE; }

    var res = (size === 1) ? 1 : getPercentRank(sortedNums, val, isInclusive);
    if (res === 0) { return 0; }

    var expon = floor(log(res));
    return round(res * pow(10, -expon + significance - 1)) / pow(10, -expon + significance - 1);
}

/**
 * Helper function for calculating inclusive and exclusive percent rank.
 */
function getPercentRank(numbers, val, isInclusive) {
    var count = numbers.length;
    var result;
    if (val === numbers[0]) {
        if (isInclusive) {
            result = 0;
        } else {
            result = 1 / (count + 1);
        }
    } else {
        var oldCount = 0;
        var oldVal = numbers[0];
        var i = 0;
        for (i = 1; i < count && numbers[i] < val; i++) {
            if (numbers[i] !== oldVal) {
                oldCount = i;
                oldVal = numbers[i];
            }
        }
        if (numbers[i] !== oldVal) {
            oldCount = i;
        }
        if (val === numbers[i]) {
            if (isInclusive) {
                if (count - 1 === 0) {
                    throw ErrorCode.DIV0;
                }
                result = oldCount / (count - 1);
            } else {
                result = (i + 1) / (count + 1);
            }
        } else {
            //  oldCount is the count of smaller entries
            //  val is between numbers[ oldCount - 1 ] and numbers[ oldCount ]
            //  use linear interpolation to find a position between the entries
            if (oldCount === 0) {
                globalLogger.warn('$badge{StatUtils} getPercentRank: old count is 0');
                result = 0;
            } else {
                var fract = (val - numbers[oldCount - 1]) / (numbers[oldCount] - numbers[oldCount - 1]);
                if (isInclusive) {
                    if (count - 1 === 0) {
                        throw ErrorCode.DIV0;
                    }
                    result = (oldCount - 1 + fract) / (count - 1);
                } else {
                    result = (oldCount + fract) / (count + 1);
                }
            }
        }
    }
    return result;
}

/**
 * Helper function to get median value from array of numbers.
 */
export function getMedian(numbers) {
    var count = numbers.length;
    if (count === 0) { throw ErrorCode.NUM; }
    numbers.sort((a, b) => a - b);
    // even array length: return arithmetic mean of both numbers in the middle of the array
    return (count % 2 === 0) ? ((numbers[count / 2 - 1] + numbers[count / 2]) / 2) : numbers[(count - 1) / 2];
}

/**
 * Helper function to get inclusive and exclusive quartile.
 */
export function getQuartile(numbers, quart, inclusive) {
    if (inclusive ? (quart < 0 || quart > 4) : (quart <= 0 || quart >= 4)) { throw ErrorCode.NUM; }
    if (quart === 2) { return getMedian(numbers); }
    var percentileFunc = inclusive ? getPercentileInclusive : getPercentileExclusive;
    return percentileFunc(numbers, quart / 4);
}

/**
 * Helper function for calculating Student's T distribution.
 *
 * @param {Number} T
 *  Value for which distribution is evaluated.
 * @param {Number} DF
 *  Degrees of freedom.
 * @param {String} type
 *  Type of distribution: 1: '1 tail', 2: '2 tail', 3: 'density', or 4: 'cumulative'.
 */
export function getTDist(T, DF, type) {

    // 1-tailed T-distribution
    if (type === 1) {
        return betaDist(DF / (DF + T * T), DF / 2, 0.5) / 2;
    }

    // 2-tailed T-distribution
    if (type === 2) {
        return betaDist(DF / (DF + T * T), DF / 2, 0.5);
    }

    // left-tailed T-distribution (probability density function)
    if (type === 3) {
        return pow(1 + (T * T / DF), -(DF + 1) / 2) / (sqrt(DF) * getBeta(0.5, DF / 2));
    }

    // left-tailed T-distribution (cumulative distribution function)
    if (type === 4) {
        var x = DF / (T * T + DF);
        var r = 0.5 * betaDist(x, 0.5 * DF, 0.5);
        return (T < 0 ? r : 1 - r);
    }

    // invalid type
    throw ErrorCode.NUM;
}

/**
 * Helper function for getting inverse T function.
 */
export function getTInv(alpha, size, type) {
    var val = iterateInverse(size * 0.5, size, function (x) { return tDistFuncWrapper(x, alpha, size, type); });
    if (val === null) {
        throw ErrorCode.NUM;
    }
    return val;
}

/**
 * Helper function for a couple of statistical functions working on sets of
 * X/Y number pairs.
 *
 * @param {FormulaContext} context
 *  The formula context used to collect the numbers of the passed operands.
 *
 * @param {Any} valuesY
 *  A formula operand representing the Y values of a data set.
 *
 * @param {Any} valuesX
 *  A formula operand representing the X values of a data set.
 *
 * @returns {Object}
 *  A result descriptor object with the following properties:
 *  - {Number} count
 *      The number of all X/Y value pairs in the passed source data (will
 *      be a positive integer; if there are no numbers in the operands
 *      passed to this method, the error code #DIV/0! will be thrown).
 *  - {Number} sumY
 *      The sum of all Y values.
 *  - {Number} sumX
 *      The sum of all X values.
 *  - {Number} meanY
 *      The arithmetic mean of all Y values.
 *  - {Number} meanX
 *      The arithmetic mean of all X values.
 *  - {Number} sumDYX
 *      The sum of (meanY-Y)*(meanX-X) for all X/Y value pairs.
 *  - {Number} sumDY2
 *      The sum of (meanY-Y)^2 for all X/Y value pairs.
 *  - {Number} sumDX2
 *      The sum of (meanX-X)^2 for all X/Y value pairs.
 *
 * @throws {ErrorCode}
 *  Any formula error code occured while collecting the result data.
 */
export function getForecastData(context, valuesY, valuesX) {

    function finalize(tuples, sums, count) {

        var meanY = div(sums[0], count);
        var meanX = div(sums[1], count);

        var sumDYX = 0, sumDY2 = 0, sumDX2 = 0;
        tuples.forEach(function (tuple) {
            var diffY = tuple[0] - meanY;
            var diffX = tuple[1] - meanX;
            sumDYX += diffY * diffX;
            sumDY2 += diffY * diffY;
            sumDX2 += diffX * diffX;
        });

        return {
            count,
            sumY: sums[0],
            sumX: sums[1],
            meanY,
            meanX,
            sumDYX,
            sumDY2,
            sumDX2
        };
    }

    return context.aggregateNumbersParallelAsArrays([valuesY, valuesX], finalize, FORECAST_OPTIONS);
}

/**
 * Returns the pearson correlation coefficient of the passed data set.
 *
 * @param {FormulaContext} context
 *  The formula context used to collect the numbers of the passed operands.
 *
 * @param {Any} valuesY
 *  A formula operand representing the Y values of a data set.
 *
 * @param {Any} valuesX
 *  A formula operand representing the X values of a data set.
 *
 * @returns {Number}
 *  The pearson correlation coefficient for the passed data set.
 *
 * @throws {ErrorCode}
 *  Any formula error code occured while collecting the result data.
 */
export function getPearsonCoeff(context, valuesY, valuesX) {
    var data = getForecastData(context, valuesY, valuesX);
    return div(data.sumDYX, sqrt(data.sumDY2 * data.sumDX2));
}

/**
 * Returns the expected Y value at the specified X value, according to the
 * linear slope of the passed data set.
 *
 * @param {FormulaContext} context
 *  The formula context used to collect the numbers of the passed operands.
 *
 * @param {Number} x
 *  The X coordinate of the point to caluculate the Y coordinate for.
 *
 * @param {Any} valuesY
 *  A formula operand representing the Y values of a data set.
 *
 * @param {Any} valuesX
 *  A formula operand representing the X values of a data set.
 *
 * @returns {Number}
 *  The predicted Y value for the passed X value and data set.
 *
 * @throws {ErrorCode}
 *  Any formula error code occured while collecting the result data.
 */
export function getForecastY(context, x, valuesY, valuesX) {
    var data = getForecastData(context, valuesY, valuesX);
    return data.meanY + div(data.sumDYX, data.sumDX2) * (x - data.meanX);
}

/**
 * Helper function to calculate Skew and SkewP functions.
 */
export function calculateSkews(numbers, sum, isSkewP) {

    var count = numbers.length;
    var mean = div(sum, count);

    var vSum = numbers.reduce(function (s, number) {
        return s + (number - mean) * (number - mean);
    }, 0);

    var stdDev = sqrt(vSum / (isSkewP ? count : count - 1));
    if (stdDev === 0) { throw ErrorCode.DIV0; }

    var xpower3 = numbers.reduce(function (x, number) {
        var dx = (number - mean) / stdDev;
        return x + dx * dx * dx;
    }, 0);

    if (isSkewP) {
        return xpower3 / count;
    }

    return (xpower3 * count) / ((count - 1) * (count - 2));
}

/*
    * http://stackoverflow.com/questions/14161990/how-to-implement-growth-function-in-javascript
    */
export function calculateTrendGrowth(context, knownY, knownX, newX, use_const, growth) {
    if (knownX) {
        var xDim = knownX.getDimension({ errorCode: ErrorCode.VALUE }); // #VALUE! thrown for complex references
        var yDim = knownY.getDimension({ errorCode: ErrorCode.VALUE }); // #VALUE! thrown for complex references
        if (!xDim.equals(yDim)) { throw ErrorCode.REF; }
    }

    var numberMode = {
        valMode: 'exact', // value operands: exact match for numbers (no strings, no booleans)
        matMode: 'exact', // matrix operands: exact match for numbers (no strings, no booleans)
        refMode: 'exact', // reference operands: exact match for numbers (no strings, no booleans)
        emptyParam: true, // empty parameters result in #VALUE! error
        multiRange: false, // do not accept multi-range references
        multiSheet: false  // do not accept multi-sheet references
    };
    var known_y = context.getNumbersAsArray(knownY, numberMode);
    var known_x = (knownX === undefined) ? null : context.getNumbersAsArray(knownX, numberMode);
    var new_x = (newX === undefined) ? null : context.getNumbersAsArray(newX, numberMode);
    var i = 0;

    // default values for optional parameters:
    if (!known_x) {
        known_x = known_y.map(function (y, i) { return i + 1; });
    }
    if (!new_x) {
        new_x = known_x.slice();
    }
    if (!is.boolean(use_const)) { use_const = true; }

    // calculate sums over the data:
    var n = known_y.length;
    var avg_x = 0; var avg_y = 0; var avg_xy = 0; var avg_xx = 0;
    for (i = 0; i < n; i++) {
        var x = known_x[i];
        var y = known_y[i];
        if (growth) {
            if (y <= 0.0) { throw ErrorCode.NUM; }
            y = log(y);
        }

        avg_x += x; avg_y += y; avg_xy += x * y; avg_xx += x * x;
    }
    avg_x /= n; avg_y /= n; avg_xy /= n; avg_xx /= n;

    // compute linear regression coefficients:
    var alpha, beta;
    if (use_const) {
        beta = (avg_xy - avg_x * avg_y) / (avg_xx - avg_x * avg_x);
        alpha = avg_y - beta * avg_x;
    } else {
        beta = avg_xy / avg_xx;
        alpha = 0;
    }
    // console.log("alpha = " + alpha + ", beta = " +  beta);

    // compute and return result array:
    return new NumberMatrix(new_x.length, 1, function (i) {
        var newV = alpha + beta * new_x[i];
        return growth ? exp(newV) : newV;
    });
}

// Helper functions for Math AGGREGATE & SUBTOTAL =============================

function extendOptions(fixedOptions, varOptions) {
    return varOptions ? { ...fixedOptions, ...varOptions } : fixedOptions;
}

export function groupAverage(context, operands, options) {
    // empty parameters count as zero: AVERAGEA(1,) = 0.5
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    // default result (no numbers found in parameters) is the #DIV/0! error code thrown by `div()`
    return context.aggregateNumbers(operands, 0, add, div, options);
}

export function groupCount(context, operands, options) {

    var count = 0;
    var numberFormatter = context.numberFormatter;

    // build the effective iterator options
    options = extendOptions({
        acceptErrors: true, // do not fail on error codes: =COUNT({1,#VALUE!}) = 1
        multiRange: true, // accept multi-range references
        multiSheet: true  // accept multi-sheet references
    }, options);

    _.each(operands, function (operand) {
        if (operand.type === "val") {
            // count booleans and strings that are convertible to numbers
            // (simple values only, not counted in matrixes and references)
            var value = numberFormatter.convertScalarToNumber(operand.getRawValue());
            if (value !== null) { count += 1; }
        } else {
            // matrixes and references: count real numbers only (no strings, no booleans)
            context.forEachScalar(operand, function (value) {
                if (typeof value === 'number') { count += 1; }
            }, options);
        }
    });

    return count;
}

export function groupCountA(context, operands, options) {

    var count = 0;
    var skipErrors = options && options.skipErrors;

    // increase count, unless value is an error code, and error codes will be ignored
    function countValue(value) {
        if (!skipErrors || !isErrorCode(value)) { count += 1; }
    }

    // build the effective iterator options
    options = extendOptions({
        acceptErrors: true, // visit all error codes (never throw, also while skipping them!)
        multiRange: true, // accept multi-range references
        multiSheet: true  // accept multi-sheet references
    }, options);

    _.each(operands, function (operand) {
        switch (operand.type) {
            case 'val':
                // all values are counted (also zeros, empty strings, FALSE,
                // error codes, empty parameters): COUNTA(1,,#VALUE!) = 3
                countValue(operand.getRawValue());
                break;
            case 'mat':
                // all matrix elements are counted (also zeros, empty strings, FALSE, error codes),
                // unless error codes will be ignored (the matrix elements must be visited in this case)
                if (!skipErrors) {
                    count += operand.getMatrix().size();
                } else {
                    context.forEachScalar(operand, countValue, options);
                }
                break;
            case 'ref':
                // all filled cells are counted (also error codes)
                context.forEachScalar(operand, countValue, options);
                break;
        }
    });

    return count;
}

export function groupMax(context, operands, options) {

    // the aggregation function
    function aggregate(a, b) { return Math.max(a, b); }

    // default result (no numbers found in parameters) is zero (no #NUM! error code from infinity)
    function finalize(result) { return Number.isFinite(result) ? result : 0; }

    // empty parameters count as zero: MAX(-1,) = 0
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    return context.aggregateNumbers(operands, -Infinity, aggregate, finalize, options);
}

export function groupMin(context, operands, options) {

    // the aggregation function
    function aggregate(a, b) { return Math.min(a, b); }

    // default result (no numbers found in parameters) is zero (no #NUM! error code from infinity)
    function finalize(result) { return Number.isFinite(result) ? result : 0; }

    // empty parameters count as zero: MIN(1,) = 0
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    return context.aggregateNumbers(operands, Infinity, aggregate, finalize, options);
}

export function groupSum(context, operands, options) {

    // create an `Operand` instance with the collected number format
    function finalize(result, _count, _size, format) {
        return format ? context.createOperand(result, { format }) : result;
    }

    // empty parameters are zero and can be ignored
    options = extendOptions(SKIP_MAT_SKIP_REF_SKIP_EMPTY, options);
    return context.aggregateNumbers(operands, 0, add, finalize, options);
}

export function groupProduct(context, operands, options) {

    // default result is 0, if no numbers have been found, e.g. =PRODUCT(,)
    function finalize(result, count) { return (count === 0) ? 0 : result; }

    // empty parameters are ignored: PRODUCT(1,) = 1
    options = extendOptions(SKIP_MAT_SKIP_REF_SKIP_EMPTY, options);
    return context.aggregateNumbers(operands, 1, mul, finalize, options);
}

function groupStdDev(context, operands, method, population, options) {

    // default result (no numbers found in parameters) is the #DIV/0! error code
    function finalize(numbers, sum) {
        return variance(numbers, sum, method, population);
    }

    // empty parameters count as 0, e.g.: STDEVP(1,) = 0.5
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    return context.aggregateNumbersAsArray(operands, finalize, options);
}

export function groupStdDevS(context, operands, options) {
    return groupStdDev(context, operands, VarianceMethod.DEV, false, options);
}

export function groupStdDevP(context, operands, options) {
    return groupStdDev(context, operands, VarianceMethod.DEV, true, options);
}

export function groupVarS(context, operands, options) {
    return groupStdDev(context, operands, VarianceMethod.VAR, false, options);
}

export function groupVarP(context, operands, options) {
    return groupStdDev(context, operands, VarianceMethod.VAR, true, options);
}

export function groupMedian(context, operands, options) {
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    return context.aggregateNumbersAsArray(operands, getMedian, options);
}

function groupMode(context, operands, multi, options) {

    // finds the most used number in all collected numbers
    function finalize(numbers) {

        // count the numbers, counts are mapped by number
        var counts = _.countBy(numbers);
        // find the maximum count
        var maxCount = _.reduce(counts, function (count1, count2) { return Math.max(count1, count2); }, 0);
        // no duplicate numbers: throw the #N/A error code
        if (maxCount < 2) { throw ErrorCode.NA; }

        // collect all duplicates in the array in the correct order
        var result = [];
        numbers.some(function (number) {
            var count = counts[number];
            if (count === maxCount) {
                result.push([number]);
                if (!multi) { return true; }
            }
            counts[number] = 0;
        });

        // no duplicate numbers: throw the #N/A error code
        if (result.length === 0) { throw ErrorCode.NA; }

        // single mode: return the first found number
        if (!multi) { return result[0][0]; }

        // multi mode: create a column vector as instance of Matrix
        ensureMatrixDim(result.length, 1);
        return new NumberMatrix(result);
    }

    options = extendOptions(MODE_OPTIONS, options);
    return context.aggregateNumbersAsArray(operands, finalize, options);
}

export function groupModeSngl(context, operands, options) {
    return groupMode(context, operands, false, options);
}

export function groupModeMult(context, operands, options) {
    return groupMode(context, operands, true, options);
}

function groupSmallLarge(context, operand, k, large, options) {

    // all numbers collected in an array
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    var numbers = context.getNumbersAsArray(operand, options);

    // check input value
    var len = numbers.length;
    if ((k < 1) || (k > len)) { throw ErrorCode.NUM; }

    // pick the k-largest or k-smallest number
    numbers.sort(math.compare);
    return numbers[large ? (len - k) : (k - 1)];
}

export function groupLarge(context, operand, k, options) {
    return groupSmallLarge(context, operand, k, true, options);
}

export function groupSmall(context, operand, k, options) {
    return groupSmallLarge(context, operand, k, false, options);
}

function groupPercentile(context, operand, p, inclusive, options) {

    // all numbers collected in an array
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    var numbers = context.getNumbersAsArray(operand, options);

    // calculate the resulting percentile
    var percentileFunc = inclusive ? getPercentileInclusive : getPercentileExclusive;
    return percentileFunc(numbers, p);
}

export function groupPercentileInc(context, operand, p, options) {
    return groupPercentile(context, operand, p, true, options);
}

export function groupPercentileExc(context, operand, p, options) {
    return groupPercentile(context, operand, p, false, options);
}

function groupPercentRank(context, operand, p, significance, inclusive, options) {

    // all numbers collected in an array
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    var numbers = context.getNumbersAsArray(operand, options);

    // calculate the resulting percent rank
    return percentRank(numbers, p, significance, inclusive);
}

export function groupPercentRankInc(context, operand, p, significance, options) {
    return groupPercentRank(context, operand, p, significance, true, options);
}

export function groupPercentRankExc(context, operand, p, significance, options) {
    return groupPercentRank(context, operand, p, significance, false, options);
}

function groupQuartile(context, operand, q, inclusive, options) {

    // all numbers collected in an array
    options = extendOptions(SKIP_MAT_SKIP_REF, options);
    var numbers = context.getNumbersAsArray(operand, options);

    // calculate the resulting quartile
    return getQuartile(numbers, q, inclusive);
}

export function groupQuartileInc(context, operand, q, options) {
    return groupQuartile(context, operand, q, true, options);
}

export function groupQuartileExc(context, operand, q, options) {
    return groupQuartile(context, operand, q, false, options);
}
