/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Logger } from "@/io.ox/office/tk/utils/logger";

// re-exports =================================================================

export { LogLevel } from "@/io.ox/office/tk/utils/logger";

// class FormulaLogger ========================================================

class FormulaLogger extends Logger {

    constructor() {
        super("spreadsheet:log-formulas", { tag: "FMLA", tagColor: 0x00FF00 });
    }

    /**
     * Writes all passed tokens to the browser console.
     *
     * @param message
     *  A message string written before the formula tokens.
     *
     * @param tokens
     *  An array of formula tokens to be written to the browser console. The
     *  array elements can be of any type convertible to a string.
     */
    logTokens(message: string, tokens: readonly unknown[]): void {
        this.log(() => `${message}: ${tokens.join(" ")}`);
    }
}

/**
 * Logger for messages related to the spreadsheet formula engine, bound to the
 * debug configuration flag `spreadsheet:log-formulas`.
 */
export const formulaLogger = new FormulaLogger();
