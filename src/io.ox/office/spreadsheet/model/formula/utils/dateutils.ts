/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    type DateComponents, MSEC_PER_DAY,
    isLeapYear, getDaysInFebruary, splitUTCDate,
    getFirstDayOfISOWeek, getFirstDayOfMonth, getFirstDayOfYear
} from "@/io.ox/office/tk/utils/dateutils";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";

const { floor, min } = Math;

// re-exports =================================================================

export * from "@/io.ox/office/tk/utils/dateutils";

// types ======================================================================

/**
 * Specifies how to calculate the number of weeks, months, or years between two
 * dates.
 */
export enum DateCountMethod {

    /**
     * Moves the start and end date to the first day of their week, month, or
     * year, and calculates the difference between these start days.
     *
     * Example: The number of months between Jan-30 and Mar-02 using this count
     * method is `2` (difference between Jan-01 and Mar-01 is 2 months).
     */
    FIRST_DAY = 0,

    /**
     * Calculates the number of complete weeks, months, or years (rounded down
     * to integer) from the exact difference of the start and end date.
     *
     * Example: The number of months between Jan-30 and Mar-02 using this count
     * method is `1` (difference is 1 whole month and a few days).
     */
    WHOLE_COUNT = 1
}

/**
 * The day count convention for the function `getDays360()`.
 */
export enum Days360Method {

    /**
     * The 30/360 European method.
     */
    EUR = 0,

    /**
     * The 30/360 US method as used by the spreadsheet function `DAYS360`.
     */
    US_DAYS = 1,

    /**
     * The 30/360 US method as used by the spreadsheet function `YEARFRAC`.
     */
    US_FRAC = 2
}

/**
 * Properties needed to calculate date differences as fractional years.
 */
export interface YearFracData {

    /**
     * The number of days between start date and end date.
     */
    days: number;

    /**
     * The number of days per year, according to the used date mode.
     */
    daysPerYear: number;
}

// private functions ==========================================================

/**
 * Swaps the passed dates in-place, if the second date is less than the first
 * date.
 *
 * @returns
 *  The number `-1`, if the dates were not sorted and have been swapped,
 *  otherwise the number `1`. Can be used to calculate the correctly signed
 *  difference between two dates, after using the sorted dates that always have
 *  a positive difference.
 */
function sortDates(utcDate1: Date, utcDate2: Date): -1 | 1 {
    if (utcDate2 < utcDate1) {
        const ts1 = utcDate1.getTime();
        utcDate1.setTime(utcDate2.getTime());
        utcDate2.setTime(ts1);
        return -1;
    }
    return 1;
}

/**
 * Returns the number of leap years contained in the passed year interval.
 *
 * @param year1
 *  The first year in the year interval (inclusive).
 *
 * @param year2
 *  The last year in the year interval (inclusive). MUST be equal to or greater
 *  than the value passed in firstYear.
 *
 * @returns
 *  The number of leap years contained in the passed year interval.
 */
function countLeapYears(year1: number, year2: number): number {

    // returns the number of years divisible by the specified value
    function getDivisibleCount(divisor: number): number {
        return floor(year2 / divisor) - floor((year1 - 1) / divisor);
    }

    // add the number of years divisible by 4, subtract the number of years
    // divisible by 100, add the number of years divisible by 400
    return getDivisibleCount(4) - getDivisibleCount(100) + getDivisibleCount(400);
}

/**
 * Returns whether the passed date is the last day of February.
 */
function isLastDayOfFeb(comps: DateComponents): boolean {
    return (comps.M === 1) && (comps.D === getDaysInFebruary(comps.Y));
}

/**
 * Returns whether first day/month is before or equal to second day/month
 * (assuming same year).
 */
function lessOrEqDate(comps1: DateComponents, comps2: DateComponents): boolean {
    return (comps1.M < comps2.M) || ((comps1.M === comps2.M) && (comps1.D <= comps2.D));
}

// public functions ===========================================================

/**
 * Returns the number of entire days between the passed start and end UTC date,
 * ignoring the time.
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value.
 *
 * @returns
 *  The number of days between the dates (negative, if the end date is less
 *  than the start date).
 */
export function countDays(utcDate1: Date, utcDate2: Date): number {
    // ignore time for each date separately (may otherwise distort the result by one day)
    return floor(utcDate2.getTime() / MSEC_PER_DAY) - floor(utcDate1.getTime() / MSEC_PER_DAY);
}

/**
 * Returns the number of weeks between the passed start and end UTC date,
 * ignoring the time.
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value.
 *
 * @param method
 *  The calculation method.
 *
 * @returns
 *  The number of weeks between the dates (negative, if the end date is less
 *  than the start date).
 */
export function countWeeks(utcDate1: Date, utcDate2: Date, method: DateCountMethod): number {

    // sort the dates, but remember the original order
    const sign = sortDates(utcDate1, utcDate2);

    // move dates to start of weeks if specified
    if (method === DateCountMethod.FIRST_DAY) {
        utcDate1 = getFirstDayOfISOWeek(utcDate1);
        utcDate2 = getFirstDayOfISOWeek(utcDate2);
    }

    // return the signed number of whole weeks between the effective dates
    return sign * floor(countDays(utcDate1, utcDate2) / 7);
}

/**
 * Returns the number of months between the passed start and end UTC date,
 * ignoring the time.
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value.
 *
 * @param method
 *  The calculation method.
 *
 * @returns
 *  The number of months between the dates (negative, if the end date is less
 *  than the start date).
 */
export function countMonths(utcDate1: Date, utcDate2: Date, method: DateCountMethod): number {

    // sort the dates, but remember the original order
    const sign = sortDates(utcDate1, utcDate2);

    // move dates to start of months if specified
    if (method === DateCountMethod.FIRST_DAY) {
        utcDate1 = getFirstDayOfMonth(utcDate1);
        utcDate2 = getFirstDayOfMonth(utcDate2);
    }

    // split the dates into their components
    const comps1 = splitUTCDate(utcDate1);
    const comps2 = splitUTCDate(utcDate2);

    // difference (number of months)
    let months = 12 * (comps2.Y - comps1.Y) + (comps2.M - comps1.M);

    // one month less, if end day is less than start day (but in another month)
    if ((months > 0) && (comps2.D < comps1.D)) { months -= 1; }

    // return the signed number of whole months between the effective dates
    return sign * months;
}
/**
 * Returns the number of years between the passed start and end UTC date,
 * ignoring the time.
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value.
 *
 * @param method
 *  The calculation method.
 *
 * @returns
 *  The number of years between the dates (negative, if the end date is less
 *  than the start date).
 */
export function countYears(utcDate1: Date, utcDate2: Date, method: DateCountMethod): number {

    // sort the dates, but remember the original order
    const sign = sortDates(utcDate1, utcDate2);

    // move dates to start of years if specified
    if (method === DateCountMethod.FIRST_DAY) {
        utcDate1 = getFirstDayOfYear(utcDate1);
        utcDate2 = getFirstDayOfYear(utcDate2);
    }

    // split the dates into their components
    const comps1 = splitUTCDate(utcDate1);
    const comps2 = splitUTCDate(utcDate2);

    // difference (number of years)
    let years = comps2.Y - comps1.Y;

    // one year less, if end day is less than start day (but in another year)
    if ((years > 0) && !lessOrEqDate(comps1, comps2)) { years -= 1; }

    // return the signed number of whole years between the effective dates
    return sign * years;
}

/**
 * Returns the number of days between the passed start and end UTC date for a
 * specific 30/360 day count convention (based on a year with 360 days, and all
 * months with 30 days each).
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value.
 *
 * @param method
 *  The day count convention.
 *
 * @returns
 *  The number of days between start date and end date. The result will be
 *  negative, if the start date is behind the end date.
 */
export function getDays360(utcDate1: Date, utcDate2: Date, method: Days360Method): number {

    // extract original year/month/day of the passed start date
    const comps1 = splitUTCDate(utcDate1);
    // extract original year/month/day of the passed end date
    const comps2 = splitUTCDate(utcDate2);
    // the adjusted day of the start and end date
    let day1 = 0, day2 = 0;

    switch (method) {

        // The 30/360 European method, as used by the functions DAYS360 and YEARFRAC
        case Days360Method.EUR:

            // if start date is day 31 of the month, set it to day 30
            day1 = min(comps1.D, 30);
            // if end date is day 31 of the month, set it to day 30
            day2 = min(comps2.D, 30);
            break;

        // The 30/360 US method, as used by the function DAYS360
        case Days360Method.US_DAYS:

            // if start date is the last day of its month, set it to day 30 (also for February)
            day1 = isLastDayOfFeb(comps1) ? 30 : min(comps1.D, 30);
            // if end date is day 31 of its month, and adjusted start day is 30,
            // set the end day to day 30 (28th/29th of February remains as is)
            day2 = (day1 === 30) ? min(comps2.D, 30) : comps2.D;
            break;

        // The 30/360 US method, as used by the function YEARFRAC (slightly different than DAYS360, of course)
        case Days360Method.US_FRAC: {

            // whether start date is the last day in February
            const lastFeb1 = isLastDayOfFeb(comps1);
            // whether end date is the last day in February
            const lastFeb2 = isLastDayOfFeb(comps2);

            // if start date is the last day of its month, set it to day 30 (also for February)
            day1 = lastFeb1 ? 30 : min(comps1.D, 30);
            // if (start date and end date are last day of February), OR if (original start day
            // is 30 or 31, and original end day is 31), set end date to day 30
            day2 = ((lastFeb1 && lastFeb2) || ((comps1.D >= 30) && (comps2.D === 31))) ? 30 : comps2.D;
            break;
        }
    }

    // the resulting number of days, based on a year with equally-sized months of 30 days each
    return (comps2.Y - comps1.Y) * 360 + (comps2.M - comps1.M) * 30 + day2 - day1;
}

/**
 * Calculates the difference between the passed dates, needed to calculate the
 * (fractional) number of years between two dates. Implements different methods
 * as used in various sheet functions.
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value. If this date is located before the start
 *  date, the dates will be swapped internally, so that the result will never
 *  be negative.
 *
 * @param [dateMode]
 *  The method to be used to calculate the difference between the passed dates.
 *  The following date modes are supported:
 *  - 0 (default): The "30/360 US" method.
 *  - 1: The "actual/actual" method (variable number of days per year).
 *  - 2: The "actual/360" method (exact number of days, 360 days per year).
 *  - 3: The "actual/365" method (exact number of days, 365 days per year).
 *  - 4: The "30/360 European" method.
 *
 * @returns
 *  The number of days between the passed dates, and the number of days per
 *  year to be used for the passed date mode.
 *
 * @throws
 *  The error code `#NUM!`, if the passed date mode is invalid.
 */
export function getYearFracData(utcDate1: Date, utcDate2: Date, dateMode = 0): YearFracData {

    // the resulting difference in days
    let days: number;
    // the number of days per year, according to the date mode
    let daysPerYear: number;

    // sort start and end date
    sortDates(utcDate1, utcDate2);

    // return fraction according to passed mode (default to mode 0 "30/360 US")
    switch (dateMode) {

        // method "30/360 US", special handling for last days in February
        case 0:
            days = getDays360(utcDate1, utcDate2, Days360Method.US_FRAC);
            daysPerYear = 360;
            break;

        // method "actual/actual"
        case 1: {
            days = countDays(utcDate1, utcDate2);
            daysPerYear = 365;

            // extract original year/month/day of the passed start date
            const comps1 = splitUTCDate(utcDate1);
            // extract original year/month/day of the passed end date
            const comps2 = splitUTCDate(utcDate2);

            // special behavior if end date is at most one year behind start date (regardless of actual day and month)
            if ((comps1.Y === comps2.Y) || ((comps1.Y + 1 === comps2.Y) && lessOrEqDate(comps2, comps1))) {

                // `YEARFRAC` uses 366 days per year, if (start date is located in a leap year, and is
                // before Mar-01), OR if (end date is located in a leap year, and is after Feb-28)
                if ((isLeapYear(comps1.Y) && (comps1.M <= 1)) || (isLeapYear(comps2.Y) && lessOrEqDate({ Y: 1, M: 1, D: 29 }, comps2))) {
                    daysPerYear += 1;
                }

            // otherwise: end date is more than one year after start date (including e.g. 2011-02-28 to 2012-02-29)
            } else {

                // include the number of leap years contained in the year interval as fractional part
                daysPerYear += countLeapYears(comps1.Y, comps2.Y) / (comps2.Y - comps1.Y + 1);
            }
            break;
        }

        // method "actual/360"
        case 2:
            days = countDays(utcDate1, utcDate2);
            daysPerYear = 360;
            break;

        // method "actual/365"
        case 3:
            days = countDays(utcDate1, utcDate2);
            daysPerYear = 365;
            break;

        // method "30/360 European"
        case 4:
            days = getDays360(utcDate1, utcDate2, Days360Method.EUR);
            daysPerYear = 360;
            break;

        // throw #NUM! error code for invalid date modes
        default:
            throw ErrorCode.NUM;
    }

    // return the result object with the number of days, and the days per year
    return { days, daysPerYear };
}

/**
 * Returns the number of years between start date and end date as fractional
 * number, according to the passed date mode.
 *
 * @param utcDate1
 *  The start date, as UTC date value.
 *
 * @param utcDate2
 *  The end date, as UTC date value. If this date is located before the start
 *  date, the dates will be swapped internally (the result will never be
 *  negative).
 *
 * @param [dateMode]
 *  The date mode to be used to calculate the number of years. See function
 *  `getYearFracData()` for details.
 *
 * @returns
 *  The number of years between start date and end date, as fractional number,
 *  according to the passed date mode.
 *
 * @throws
 *  The error code `#NUM!`, if the passed date mode is invalid.
 */
export function getYearFrac(utcDate1: Date, utcDate2: Date, dateMode = 0): number {
    const yearFracData = getYearFracData(utcDate1, utcDate2, dateMode);
    return yearFracData.days / yearFracData.daysPerYear;
}
