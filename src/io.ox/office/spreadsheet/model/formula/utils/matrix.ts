/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary } from "@/io.ox/office/tk/algorithms";

import type { ScalarTypeNE, ScalarType, ErrorLiteral } from "@/io.ox/office/spreadsheet/utils/scalar";
import { ErrorCode, scalarToString } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { InternalErrorCode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";

const { floor } = Math;

// types ======================================================================

/**
 * A generator callback function that will be invoked for creating the elements
 * of a new matrix.
 *
 * Note the parameter order (row index before column index), which is the usual
 * order for mathematical matrixes, but differs from the parameter order of
 * other methods taking column/row indexes of cell addresses (column index
 * before row index).
 *
 * @param row
 *  The zero-based row index of the matrix element.
 *
 * @param col
 *  The zero-based column index of the matrix element.
 *
 * @returns
 *  The new value for the respective matrix element.
 */
export type MatrixGeneratorFn<T> = (row: number, col: number) => T;

/**
 * A callback function for updating a single matrix element.
 *
 * @param value
 *  The current value of the matrix element.
 *
 * @returns
 *  The new value of the matrix element.
 */
export type MatixUpdateFn<T> = (value: T) => T;

/**
 * A callback function invoked for all elements in a matrix.
 *
 * @param value
 *  The value of the current matrix element.
 *
 * @param row
 *  The zero-based row index of the matrix element.
 *
 * @param col
 *  The zero-based column index of the matrix element.
 */
export type MatrixCallbackFn<T, R> = (value: T, row: number, col: number) => R;

/**
 * A callback function invoked for all elements in a matrix used to reduce the
 * matrix to a single value.
 *
 * @param reduced
 *  The return value of the previous invocation of the callback function (or
 *  the initial value on first invocation).
 *
 * @param value
 *  The value of the current matrix element.
 *
 * @param row
 *  The zero-based row index of the matrix element.
 *
 * @param col
 *  The zero-based column index of the matrix element.
 *
 * @returns
 *  The return value of the callback function will become the new result value
 *  that will be passed as parameter `reduced` with the next element.
 */
export type MatrixReduceFn<T, R> = (reduced: R, value: T, row: number, col: number) => R;

/**
 * The value type of an element iterator for a matrix.
 */
export interface MatrixEntry<T> {

    /**
     * The value of the matrix element.
     */
    value: T;

    /**
     * The zero-based row index of the matrix element.
     */
    row: number;

    /**
     * The zero-based column index of the matrix element.
     */
    col: number;
}

export type ScalarMatrixNE = Matrix<ScalarTypeNE>;
export type ScalarMatrix = Matrix<ScalarType>;
export type StringMatrix = Matrix<string>;
export type BooleanMatrix = Matrix<boolean>;
export type ErrorMatrix = Matrix<ErrorLiteral>;

// constants ==================================================================

/**
 * Maximum number of elements supported in matrixes.
 */
export const MAX_MATRIX_SIZE = 10_000;

// public functions ===========================================================

/**
 * Returns whether the passed matrix dimension is inside the supported limits.
 *
 * @param dim
 *  The dimension of a matrix.
 *
 * @param rows
 *  The number of rows in a matrix.
 *
 * @param cols
 *  The number of columns in a matrix.
 *
 * @returns
 *  Whether the passed matrix dimension is inside the limits supported by the
 *  formula interpreter.
 */
export function isValidMatrixDim(dim: Dimension): boolean;
export function isValidMatrixDim(rows: number, cols: number): boolean;
// implementation
export function isValidMatrixDim(arg1: Dimension | number, cols?: number): boolean {
    const size = is.number(arg1) ? (arg1 * cols!) : arg1.size();
    return (size > 0) && (size <= MAX_MATRIX_SIZE);
}

/**
 * Throws the internal error code `#UNSUPPORTED`, if the passed matrix
 * dimension is outside the limits supported by the formula interpreter.
 *
 * @param dim
 *  The dimension of a matrix.
 *
 * @param rows
 *  The number of rows in a matrix.
 *
 * @param cols
 *  The number of columns in a matrix.
 *
 * @throws
 *  The internal error code `#UNSUPPORTED`, if the passed matrix dimension is
 *  not inside the limits supported by the formula interpreter.
 */
export function ensureMatrixDim(dim: Dimension): void;
export function ensureMatrixDim(rows: number, cols: number): void;
// implementation
export function ensureMatrixDim(arg1: Dimension | number, cols?: number): void {
    if (!(isValidMatrixDim as AnyFunction)(arg1, cols)) {
        throw InternalErrorCode.UNSUPPORTED;
    }
}

/**
 * Returns the address of the cell in the passed cell range located at the
 * specified relative row and column indexes, taking auto-repetition as used in
 * matrix formulas into account.
 *
 * @param range
 *  The address of a cell range.
 *
 * @param row
 *  The relative row index of a matrix element. This value will be ignored, if
 *  the passed range consists of a single row (auto-repetition in matrix
 *  formulas).
 *
 * @param col
 *  The relative column index of a matrix element. This value will be ignored,
 *  if the passed range consists of a single row (auto-repetition in matrix
 * formulas).
 *
 * @returns
 *  The address of the cell in the passed range specified by the passed row and
 *  column indexes.
 *
 * @throws
 *  The error code `#N/A`, if the passed row index or column index exceeds the
 *  size of the passed cell range address.
 */
export function getMatrixAddress(range: Range, row: number, col: number): Address {
    if (range.singleCol()) { col = 0; }
    if (range.singleRow()) { row = 0; }
    if ((col >= range.cols()) || (row >= range.rows())) { throw ErrorCode.NA; }
    return new Address(range.a1.c + col, range.a1.r + row);
}

// class Matrix ===============================================================

/**
 * Representation of a matrix literal: a two-dimensional array of constant
 * scalar values.
 *
 * @template T
 *  The data type of the matrix elements. Intended to be scalar value types as
 *  used in spreadsheet formulas (`ScalarType` or subsets). However, users may
 *  choose to put other data types into a matrix.
 *
 * @param rows
 *  The number of rows contained in the new matrix. Must be a positive integer.
 *
 * @param cols
 *  The number of columns contained in the new matrix. Must be a positive
 *  integer.
 *
 * @param generator
 *  The generator callback function that will be invoked for every matrix
 *  element.
 *
 * @param [context]
 *  The calling context for the generator callback function.
 *
 * @param fill
 *  The value to be inserted into all matrix elements.
 *
 * @param init
 *  One of the following:
 *  - A source matrix. The new matrix will take ownership of the data arrays
 *    without copying.
 *  - A two-dimensional array of values which will become the contents of the
 *    new matrix instance. The array MUST contain at least one inner array. All
 *    inner arrays MUST contain at least one value, and all MUST contain the
 *    same number of values.
 *  - A scalar value to create a 1x1 matrix from.
 */
export class Matrix<T> implements CloneableThis {

    // properties -------------------------------------------------------------

    protected readonly _array: T[][];

    // constructor ------------------------------------------------------------

    constructor(rows: number, cols: number, generator: MatrixGeneratorFn<T>, context?: object);
    constructor(rows: number, cols: number, fill: T);
    constructor(init: Matrix<T> | T[][] | T);
    // implementation
    constructor(arg1: number | Matrix<T> | T[][] | T, cols?: number, valOrGen?: T | MatrixGeneratorFn<T>, context?: object) {
        if (is.number(cols)) {
            const rows = arg1 as number;
            if (is.function(valOrGen)) {
                this._array = ary.generate(rows, row => ary.generate(cols, col => valOrGen.call(context, row, col)));
            } else {
                this._array = ary.generate(rows, () => ary.fill(cols, valOrGen!));
            }
        } else {
            this._array = (arg1 instanceof Matrix) ? arg1._array : is.array(arg1) ? arg1 : [[arg1 as T]];
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a deep clone of this matrix.
     *
     * @returns
     *  A deep clone of this matrix.
     */
    clone(): this {
        return this._new(this._array.map(vector => vector.slice()));
    }

    /**
     * Returns the elements of this matrix as plain JavaScript array of arrays.
     *
     * @returns
     *  The elements of this matrix, as array of row vectors.
     */
    raw(): T[][] {
        return this._array;
    }

    /**
     * Returns the number of rows contained in this matrix.
     *
     * @returns
     *  The number of rows contained in this matrix.
     */
    rows(): number {
        return this._array.length;
    }

    /**
     * Returns the number of columns contained in this matrix.
     *
     * @returns
     *  The number of columns contained in this matrix.
     */
    cols(): number {
        return this._array[0].length;
    }

    /**
     * Returns the dimension of this matrix, i.e. the number of its rows and
     * columns in a single object.
     *
     * @returns
     *  The number of rows and columns contained in this matrix.
     */
    dim(): Dimension {
        return new Dimension(this.rows(), this.cols());
    }

    /**
     * Returns the number of elements contained in this matrix.
     *
     * @returns
     *  The number of elements contained in this matrix.
     */
    size(): number {
        return this.rows() * this.cols();
    }

    /**
     * Returns the value of the specified element in this matrix.
     *
     * @param row
     *  The zero-based row index of the element. If the matrix consists of a
     *  single row only, this index will be ignored, and the element from the
     *  existing row will be used instead (matrix auto-expansion as used in
     *  spreadsheet formulas).
     *
     * @param col
     *  The zero-based column index of the element. If the matrix consists of a
     *  single column only, this index will be ignored, and the element from
     *  the existing column will be used instead (matrix auto-expansion as used
     *  in spreadsheet formulas).
     *
     * @param [def]
     *  The default value to be returned if the passed indexes are located
     *  outside the matrix.
     *
     * @returns
     *  The value of the matrix element; or the specified default value, if the
     *  passed element indexes are located outside the matrix (except for
     *  auto-expansion, see above).
     */
    get(row: 0, col: 0, def?: unknown): T;
    get(row: number, col: number): Opt<T>;
    get<D>(row: number, col: number, def: D): T | D;
    // implementation
    get<D>(row: number, col: number, def?: D): Opt<T | D> {
        const rows = this.rows(), cols = this.cols();
        // auto-expansion for single row or column vector
        if (rows === 1) { row = 0; }
        if (cols === 1) { col = 0; }
        return ((row < rows) && (col < cols)) ? this._array[row][col] : def;
    }

    /**
     * Returns the value of the specified element in this matrix.
     *
     * @param index
     *  The zero-based row-oriented index of a matrix element. MUST be a valid
     *  index (not negative, less than the number of elements in the matrix).
     *
     * @returns
     *  The value of the matrix element.
     */
    getByIndex(index: number): T {
        const cols = this.cols();
        return this._array[floor(index / cols)][index % cols];
    }

    /**
     * Returns the specified row vector of this matrix.
     *
     * @param row
     *  The zero-based row index of the vector. MUST be a valid index (not
     *  negative, less than the number of rows in the matrix).
     *
     * @returns
     *  The specified row vector of this matrix.
     */
    toRowVector(row: number): this {
        return this._new([this._array[row]]);
    }

    /**
     * Returns the specified column vector of this matrix.
     *
     * @param col
     *  The zero-based column index of the vector. MUST be a valid index (not
     *  negative, less than the number of columns in the matrix).
     *
     * @returns
     *  The specified column vector of this matrix.
     */
    toColVector(col: number): this {
        const arr = this._array;
        return this._new(this.rows(), 1, row => arr[row][col]);
    }

    /**
     * Changes the value of the specified element in this matrix.
     *
     * @param row
     *  The zero-based row index of the element. MUST be a valid index (not
     *  negative, less than the number of rows in the matrix).
     *
     * @param col
     *  The zero-based column index of the element. MUST be a valid index (not
     *  negative, less than the number of columns in the matrix).
     *
     * @param value
     *  The new scalar value for the matrix element.
     *
     * @returns
     *  A reference to this instance.
     */
    set(row: number, col: number, value: T): this {
        this._array[row][col] = value;
        return this;
    }

    /**
     * Changes the value of the specified element in this matrix.
     *
     * @param index
     *  The zero-based row-oriented index of a matrix element. MUST be a valid
     *  index (not negative, less than the number of elements in the matrix).
     *
     * @param value
     *  The new value for the matrix element.
     *
     * @returns
     *  A reference to this instance.
     */
    setByIndex(index: number, value: T): this {
        const cols = this.cols();
        this._array[floor(index / cols)][index % cols] = value;
        return this;
    }

    /**
     * Updates the value of the specified element in this matrix to the return
     * value of the specified callback function.
     *
     * @param row
     *  The zero-based row index of the element. MUST be a valid index (not
     *  negative, less than the number of rows in the matrix).
     *
     * @param col
     *  The zero-based column index of the element. MUST be a valid index (not
     *  negative, less than the number of columns in the matrix).
     *
     * @param callback
     *  The callback function invoked with the element value.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this instance.
     */
    update(row: number, col: number, callback: MatixUpdateFn<T>, context?: object): this {
        this._array[row][col] = callback.call(context, this._array[row][col]);
        return this;
    }

    /**
     * Updates the value of the specified element in this matrix to the return
     * value of the specified callback function.
     *
     * @param index
     *  The zero-based row-oriented index of a matrix element. MUST be a valid
     *  index (not negative, less than the number of elements in the matrix).
     *
     * @param callback
     *  The callback function invoked with the element value.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this instance.
     */
    updateByIndex(index: number, callback: MatixUpdateFn<T>, context?: object): this {
        const cols = this.cols();
        return this.update(floor(index / cols), index % cols, callback, context);
    }

    /**
     * Changes the value of the specified rectangular subrange in this matrix.
     *
     * @param row1
     *  The zero-based row index of the first element. MUST be a valid index
     *  (not negative, less than the number of rows in the matrix).
     *
     * @param col1
     *  The zero-based column index of the first element. MUST be a valid index
     *  (not negative, less than the number of columns in the matrix).
     *
     * @param row2
     *  The zero-based row index of the second element. MUST be a valid index
     *  (not negative, less than the number of rows in the matrix, not less
     *  than `row1`).
     *
     * @param col2
     *  The zero-based column index of the second element. MUST be a valid
     *  index (not negative, less than the number of columns in the matrix, not
     *  less than `col1`).
     *
     * @param value
     *  The new value for the matrix elements.
     *
     * @returns
     *  A reference to this instance.
     */
    fill(row1: number, col1: number, row2: number, col2: number, value: T): this {
        const arr = this._array;
        for (let row = row1; row <= row2; row += 1) {
            for (let col = col1; col <= col2; col += 1) {
                arr[row][col] = value;
            }
        }
        return this;
    }

    /**
     * Creates an iterator that visits the values of all elements contained in
     * this matrix. The elements will be visited row-by-row.
     *
     * @yields
     *  The matrix values.
     */
    *values(): IterableIterator<T> {
        // visit the matrix rows (parent), and the elements in a row (child)
        const arr = this._array, rows = this.rows(), cols = this.cols();
        for (let row = 0; row < rows; row += 1) {
            for (let col = 0; col < cols; col += 1) {
                yield arr[row][col];
            }
        }
    }

    /**
     * Creates an iterator that visits all elements contained in this matrix.
     * The elements will be visited row-by-row. The iterator will provide the
     * value of the metrix elements, and their row and column indexes.
     *
     * @yields
     *  The matrix entries (row, column, value).
     */
    *entries(): IterableIterator<MatrixEntry<T>> {
        // visit the matrix rows (parent), and the elements in a row (child)
        const arr = this._array, rows = this.rows(), cols = this.cols();
        for (let row = 0; row < rows; row += 1) {
            for (let col = 0; col < cols; col += 1) {
                yield { row, col, value: arr[row][col] };
            }
        }
    }

    /**
     * Invokes the passed callback function for all elements contained in this
     * matrix.
     *
     * @param callback
     *  The callback function invoked for all elements in this matrix.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this instance.
     */
    forEach(callback: MatrixCallbackFn<T, void>, context?: object): this {
        // visit the matrix rows (parent), and the elements in a row (child)
        const arr = this._array, rows = this.rows(), cols = this.cols();
        for (let row = 0; row < rows; row += 1) {
            for (let col = 0; col < cols; col += 1) {
                callback.call(context, arr[row][col], row, col);
            }
        }
        return this;
    }

    /**
     * Creates a new matrix with the same size of this matrix, and the elements
     * set to the return values of the specified callback function.
     *
     * @param transform
     *  The callback function that will be invoked for each element in this
     *  matrix. The return values of the callback function will be inserted
     *  into the new matrix.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The new matrix with the return values of the callback function.
     */
    map<U>(transform: MatrixCallbackFn<T, U>, context?: object): Matrix<U> {
        const arr = this._array;
        return new Matrix(this.rows(), this.cols(), (row, col) =>
            transform.call(context, arr[row][col], row, col)
        );
    }

    /**
     * Invokes the passed callback function for all elements in this matrix,
     * and reduces the values to a single result value.
     *
     * @param initial
     *  The initial result value passed to the first invocation of the callback
     *  function.
     *
     * @param callback
     *  The callback function that will be invoked for each element in this
     *  matrix. The return value of the callback function will become the new
     *  result value.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The final result (the last return value of the callback function).
     */
    reduce<U>(initial: U, callback: MatrixReduceFn<T, U>, context?: object): U {
        this.forEach((value, row, col) => {
            initial = callback.call(context, initial, value, row, col);
        });
        return initial;
    }

    /**
     * Invokes the passed callback function for all elements contained in this
     * matrix, and sets the return values as new element values in-place.
     *
     * @param callback
     *  The callback function invoked for all elements in this matrix. Must
     *  return the new value for the element.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this instance.
     */
    transform(callback: MatrixCallbackFn<T, T>, context?: object): this {
        this.forEach((value, row, col) => this.set(row, col, callback.call(context, value, row, col)));
        return this;
    }

    /**
     * Swaps the specified rows in this matrix in-place.
     *
     * @param row1
     *  The index of the first row to be swapped with the other row. MUST be a
     *  valid row index.
     *
     * @param row2
     *  The index of the second row to be swapped with the other row. MUST be a
     *  valid row index.
     *
     * @returns
     *  A reference to this instance.
     */
    swapRows(row1: number, row2: number): this {
        const vector = this._array[row1];
        this._array[row1] = this._array[row2];
        this._array[row2] = vector;
        return this;
    }

    /**
     * Returns the transposed matrix of this matrix.
     *
     * @returns
     *  The transposed matrix of this matrix.
     */
    transpose(): this {
        const arr = this._array;
        return this._new(this.cols(), this.rows(), (row, col) => arr[col][row]);
    }

    /**
     * Returns the string representation of this matrix for debug logging.
     *
     * _Attention:_ DO NOT USE this method for GUI related code where correct
     * locale dependent representation of the matrix elements is required.
     *
     * @returns
     *  The string representation of this matrix for debug logging.
     */
    toString(): string {
        return `{${this._array.map(vector => vector.map(scalarToString).join(";")).join("|")}}`;
    }

    // protected methods ------------------------------------------------------

    /**
     * Hides the TypeScript workaround to create a correctly typed new instance
     * of a subclass.
     */
    protected _new(array: T[][]): this;
    protected _new(rows: number, cols: number, generator: MatrixGeneratorFn<T>, context?: object): this;
    // implementation
    protected _new<ArgsT extends unknown[]>(...args: ArgsT): this {
        return new (this.constructor as CtorType<this, ArgsT>)(...args);
    }
}

// class NumberMatrix =========================================================

/**
 * A matrix with numeric elements, with more specialized methods.
 */
export class NumberMatrix extends Matrix<number> {

    // static functions -------------------------------------------------------

    /**
     * Creates an identity matrix of the specified size.
     *
     * @param size
     *  The number of rows and columns of the identity matrix.
     *
     * @returns
     *  The new matrix instance.
     */
    static identity(size: number): NumberMatrix {
        return new NumberMatrix(size, size, (row, col) => (row === col) ? 1 : 0);
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds the passed number to the specified element in this matrix.
     *
     * @param row
     *  The zero-based row index of the element. MUST be a valid index (not
     *  negative, less than the number of rows in the matrix).
     *
     * @param col
     *  The zero-based column index of the element. MUST be a valid index (not
     *  negative, less than the number of columns in the matrix).
     *
     * @param value
     *  The number to be added to the matrix element.
     *
     * @returns
     *  A reference to this instance.
     */
    add(row: number, col: number, value: number): this {
        this._array[row][col] += value;
        return this;
    }

    /**
     * Adds the passed number to the specified element in this matrix.
     *
     * @param index
     *  The zero-based row-oriented index of a matrix element. MUST be a valid
     *  index (not negative, less than the number of elements in the matrix).
     *
     * @param value
     *  The number to be added to the matrix element.
     *
     * @returns
     *  A reference to this instance.
     */
    addByIndex(index: number, value: number): this {
        const cols = this.cols();
        this._array[floor(index / cols)][index % cols] += value;
        return this;
    }

    /**
     * Multiplies all elements of the specified row in-place with the passed
     * factor.
     *
     * @param row
     *  The index of the row to be changed. MUST be a valid row index. All
     *  elements of the specified row MUST be numbers.
     *
     * @param factor
     *  The factor to be used to multiply all row elements.
     *
     * @returns
     *  A reference to this instance.
     */
    scaleRow(row: number, factor: number): this {
        const arr = this._array;
        arr[row].forEach((_value, col, vector) => { vector[col] *= factor; });
        return this;
    }

    /**
     * Adds the scaled elements of a row in this matrix to the elements of
     * another row in-place.
     *
     * @param destRow
     *  The index of the target row to be changed. MUST be a valid row index.
     *  All elements of the specified row MUST be numbers.
     *
     * @param srcRow
     *  The index of the source row whose elements will be scaled and added to
     *  the elements in the target row. MUST be a valid row index. All elements
     *  of the specified row MUST be numbers.
     *
     * @param factor
     *  The factor to be used to multiply all elements of the target row.
     *
     * @returns
     *  A reference to this instance.
     */
    addScaledRow(destRow: number, srcRow: number, factor: number): this {
        const arr = this._array;
        const destVector = arr[destRow];
        arr[srcRow].forEach((value, col) => { destVector[col] += factor * value; });
        return this;
    }
}
