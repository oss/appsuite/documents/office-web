/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { type ScalarType, ErrorLiteral, ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { OptSheet } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

/**
 * Identifiers of supported formula grammars.
 *
 * - `op`: Fixed token representations as used in operations for the file
 *   format of the edited document.
 *
 * - `op:oox`: Fixed token representations as used in operations for the OOXML
 *   file format (independent of file format of the edited document).
 *
 * - `ui`: The localized token representations according to the current UI
 *   language of the application, and the current reference style (A1 or RC) of
 *   the edited document.
 *
 * - `ui:a1`: The localized token representations according to the current UI
 *   language of the application, in A1 reference style.
 *
 * - `ui:rc`: The localized token representations according to the current UI
 *   language of the application, in RC reference style.
 *
 * - `en:a1`: The token representations according to the American English UI
 *   language (locale "en_US"), in A1 reference style.
 *
 * - `en:rc`: The token representations according to the American English UI
 *   language (locale "en_US"), in RC reference style.
 */
export type GrammarType = "op" | "op:oox" | "ui" | "ui:a1" | "ui:rc" | "en:a1" | "en:rc";

/**
 * Different types of formulas that can occur in a spreadsheet document.
 *
 * - `cell`: A regular cell formula, a shared formula, or a matrix formula.
 *   This implies special handling for circular references when interpreting
 *   the formula.
 *
 * - `name`: The definition formula of a defined name. Reference tokens that
 *   will be located outside the sheet when relocating them will be wrapped at
 *   the sheet borders.
 *
 * - `rule`: The condition value of a data validation rule, or conditional
 *   formatting rule.
 *
 * - `link`: The source link in a model object (e.g. data series source in a
 *   chart object, or a linked text box).
 */
export type FormulaType = "cell" | "name" | "rule" | "link";

/**
 * Context types to be used to evaluate the result of a formula expression.
 *
 * - `val`: A single scalar value is expected, e.g. in simple cell formulas, or
 *   operators and functions working with simple values (the plus operator, the
 *   function `ABS`).
 *
 *   If the formula expression resolves to a matrix, its top-left element will
 *   be used as result. If the formula expression resolves to a cell reference,
 *   it will be resolved to a single result if possible.
 *
 * - `mat`: A matrix of scalar values is expected, e.g. in matrix formulas, or
 *   functions working on entire matrixes (the function `MMULT`).
 *
 *   If the formula expression resolves to a scalar value, it will be converted
 *   to a 1x1 matrix. If the formula expression resolves to a cell reference,
 *   it will be resolved to a matrix if possible.
 *
 * - `ref`: An unresolved cell reference is expected, e.g. reference operators
 *   (list, intersection, range), or functions calculating with range addresses
 *   instead of the cell contents (the function `OFFSET`).
 *
 *   If the formula expression resolves to a scalar value or matrix, an error
 *   will be generated.
 *
 * - `any`: Accepts all result types (scalars, matrixes, cell references), with
 *   minimum conversion, e.g. the result of defined names, functions passing
 *   one of their original parameters through (`IF`, `CHOOSE`), or functions
 *   that iterate over available values in scalar values, matrixes, and cell
 *   references (`SUM`, `PRODUCT`).
 */
export type ContextType = "val" | "mat" | "ref" | "any";

/**
 * Specifies how a function behaves when recalculating the formulas in the
 * spreadsheet document.
 *
 * - `always`: The function is "volatile", i.e. its result may differ for every
 *   evaluation, even if the parameter values did not change (e.g. functions
 *   `RAND` or `NOW`). A formula containing such a function will be
 *   recalculated in every document update cycle, regardless of the formula
 *   dependencies.
 *
 * - `once`: The function is considered to be initially dirty after the
 *   document has been imported (e.g. function `TEXT` depending on the UI
 *   language). A formula containing such a function will be recalculated once
 *   after importing the document.
 *
 * - `normal`: The function will never turn a formula to "dirty" state by
 *   itself.
 */
export type RecalcMode = "always" | "once" | "normal";

/**
 * Specifies the conversion strategy for scalar values for a specific target
 * data type.
 *
 * - `convert`: Convert as much as possible:
 *    - *To numbers*: Strings will be converted to numbers, if possible.
 *      Boolean values will be converted to numbers. Strings that represent a
 *      boolean value (e.g. "true") result in the error code `#VALUE!` though.
 *    - *To strings*: Numbers and boolean values will be converted to strings.
 *    - *To booleans*: Strings will be converted to boolean values, if
 *      possible. Numbers will be converted to boolean values. Strings that
 *      represent a number (e.g. "1") result in the error code `#VALUE!`
 *      though.
 *
 * - `skip`: Skip values of other types:
 *   - *To numbers*: Strings and all boolean values will be skipped silently,
 *     also strings that could be converted to numbers.
 *   - *To strings*: Numbers and boolean values will be skipped silently.
 *   - *To booleans*: All strings will be skipped silently, also strings that
 *     could be converted to boolean values. Numbers will be converted though.
 *
 * - `exact`: Accepts target data type only:
 *   - *To numbers*: Accepts numbers only. All strings, boolean values, and the
 *     empty function parameter result in the error code `#VALUE!`.
 *   - *To strings*: Accepts strings only. All numbers, boolean values, and the
 *     empty function parameter result in the error code `#VALUE!`.
 *   - *To booleans*: Accepts boolean values only. All numbers, strings, and
 *     the empty function parameter result in the error code `#VALUE!`.
 *
 * - `rconvert`: Restricted conversion for numbers. Strings will be converted
 *   to numbers, if possible. Boolean values and strings that represent a
 *   boolean value result in the error code `#VALUE!` though.
 *
 *   This conversion mode is not supported for strings and boolean values!
 *
 * - `zero`: Special conversion strategy for numbers. All strings will be
 *   treated as the number zero, also strings that could be converted to
 *   numbers. Boolean values will be converted to numbers though.
 *
 *   This conversion mode is not supported for strings and boolean values!
 */
export type ConvertMode = "convert" | "skip" | "exact" | "rconvert" | "zero";

/**
 * Specifies how to check the size of operands aggregated in parallel.
 *
 * - `exact`: The dimensions of all operands (width and height) must be equal.
 *
 * - `size`: The size of all operands (number of scalar values) must be equal,
 *   regardless of the exact width and height. Example: a 2x2 cell range and a
 *   1x4 matrix are compatible operands.
 *
 * - `none`: The size of the operands will not be checked.
 */
export type CheckSizeMode = "exact" | "size" | "none";

/**
 * The parent model instance of a formula expression or similar object.
 *
 * - A `SpreadsheetModel` document model for global formulas not associated to
 *   a specific sheet.
 *
 * - A `SheetModel` for sheet-local formulas.
 */
export type FormulaParentModel = SpreadsheetModel | SheetModel;

/**
 * Optional parameters for resolving cell references.
 */
export interface RefSheetOptions {

    /**
     * The index of the reference sheet used to resolve references without
     * sheets. If omitted, and the respective token does not contain its own
     * sheet reference, the target sheets cannot be resolved.
     */
    refSheet?: OptSheet;
}

/**
 * Optional parameters for resolving cell references.
 */
export interface WrapReferencesOptions {

    /**
     * If set to `true`, and if the relocated cell range is located outside the
     * sheet, the range will be wrapped at the sheet borders. Default value is
     * `false`.
     */
    wrapReferences?: boolean;
}

/**
 * Optional parameters for resolving cell references.
 */
export interface UnqualifiedTablesOptions {

    /**
     * If set to `true`, and a table reference (token `TableToken`) is located
     * inside the target table range (according to the options `refSheet` and
     * `targetAddress`), and the token refers to a single table column, the
     * table name will be omitted (unqualified table reference, e.g. `[Col1]`
     * instead of `Table[Col1]`). Will be ignored for operation grammars (used
     * for UI grammars only). Default value is `false`.
     */
    unqualifiedTables?: boolean;
}

// class InternalErrorLiteral =================================================

/**
 * An error code literal used for internal purpose in the formula engine (not
 * intended to be used as formula result or cell contents).
 */
export class InternalErrorLiteral extends ErrorLiteral {

    // properties -------------------------------------------------------------

    /**
     * The discriminant flag for internal error code literals.
     */
    readonly internal = true;

    /**
     * The scalar value the formula engine will use to resolve the internal
     * error code.
     */
    readonly value: ScalarType;

    // constructor ------------------------------------------------------------

    constructor(key: string, value: ScalarType) {
        super(key);
        this.value = value;
    }
}

// constants ==================================================================

/**
 * Maximum number of function parameters.
 */
export const MAX_PARAM_COUNT = 254;

/**
 * Maximum number of references in a reference list.
 */
export const MAX_REF_LIST_SIZE = 1024;

/**
 * Maximum time available for evaluating a single formula, in milliseconds.
 */
export const MAX_EVAL_TIME = 2000;

// enum InternalErrorCode =====================================================

/**
 * All built-in error codes with native names, as used in operations. Intended
 * to be used like an enumeration type.
 */
export const InternalErrorCode = {

    /**
     * Special error code literal to transport the "unsupported" state.
     */
    UNSUPPORTED: new InternalErrorLiteral("unsupported", ErrorCode.NA),

    /**
     * Special error code literal to transport the "circular reference" result.
     */
    CIRCULAR: new InternalErrorLiteral("circular", 0),

    /**
     * Special error code literal to transport the "timeout" state.
     */
    TIMEOUT: new InternalErrorLiteral("timeout", ErrorCode.NA),

    /**
     * Special error code literal for unrecoverable internal implementation
     * errors.
     */
    ENGINE: new InternalErrorLiteral("engine", ErrorCode.NA)

} satisfies Dict<InternalErrorLiteral>;

// public functions ===========================================================

/**
 * Returns whether the passed value is an internal error code.
 *
 * @param value
 *  Any result value used in formulas.
 *
 * @returns
 *  Whether the passed value is an internal error code.
 */
export function isInternalError(value: unknown): value is InternalErrorLiteral {
    return value instanceof InternalErrorLiteral;
}

/**
 * Prints the passed error message to the browser console, and throws the
 * internal error code `#ENGINE`.
 *
 * _Attention:_ MUST ONLY BE USED to indicate an error in the internal
 * implementation of the formula engine. NEVER throw this error code for wrong
 * user input.
 *
 * @param msg
 *  An error message to be printed to the debug console.
 *
 * @throws
 *  The internal error code `#ENGINE`.
 */
export function throwEngineError(msg: string): never {
    globalLogger.error(msg);
    throw InternalErrorCode.ENGINE;
}

/**
 * Resolves the effective recalculation mode for the passed recalculation
 * modes.
 *
 * @param recalc1
 *  The first recalculation mode.
 *
 * @param recalc2
 *  The second recalculation mode.
 *
 * @returns
 *  The effective recalculation mode for the passed recalculation modes.
 */
export function getRecalcMode(recalc1: RecalcMode, recalc2: RecalcMode): RecalcMode {
    if ((recalc1 === "always") || (recalc2 === "always")) { return "always"; }
    if ((recalc1 === "once") || (recalc2 === "once")) { return "once"; }
    return "normal";
}

/**
 * Converts a formula parent model (either document model or sheet model) to
 * the respective document model and (optional) sheet model.
 *
 * @param parentModel
 *  A formula parent model.
 *
 * @returns
 *  An object with properties `docModel` and `sheetModel` (the latter will be
 *  `undefined` if a document model has been passed).
 */
export function resolveParentModel(parentModel: FormulaParentModel): { docModel: SpreadsheetModel; sheetModel: Opt<SheetModel> } {
    const isSheetModel = "docModel" in parentModel;
    return {
        docModel: isSheetModel ? parentModel.docModel : parentModel,
        sheetModel: isSheetModel ? parentModel : undefined
    };
}
