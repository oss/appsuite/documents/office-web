/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math } from "@/io.ox/office/tk/algorithms";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { atanxy } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";

// private functions ==========================================================

/**
 * Throws the error code `#NUM!`, if the passed number is equal to, or too
 * close to zero.
 *
 * @param n
 *  The number to be checked.
 *
 * @returns
 *  The passed number, if it is not zero.
 *
 * @throws
 *  The error code `#NUM!`, if the passed number is zero.
 */
function checkNotZero(n: number): number {
    return math.isZero(n) ? ErrorCode.NUM.throw() : n;
}

// class Complex ==============================================================

/**
 * Representation of a complex number literal: a data structure with a
 * floating-point property for the real coefficient, and a floating-point
 * property for the imaginary coefficient.
 */
export class Complex {

    /** The real coefficient, i.e. the "a" in "a+bi". */
    real: number;

    /** The imaginary coefficient, i.e. the "b" in "a+bi". */
    imag: number;

    /** The imaginary unit, i.e. the "i" in "a+bi". */
    unit: Opt<string>;

    // constructor ------------------------------------------------------------

    /**
     * @param real
     *  The real coefficient of the complex number.
     *
     * @param imag
     *  The imaginary coefficient of the complex number.
     *
     * @param unit
     *  The imaginary unit (either "i" or "j"). If omitted, the imaginary unit
     *  to be used is undetermined yet (by default, "i" will be used in the
     *  end). Binary complex operations (e.g. addition) will use the first
     *  existing unit of their operands for their result.
     */
    constructor(real: number, imag: number, unit?: string) {
        this.real = real;
        this.imag = imag;
        this.unit = unit;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the sum of this and another complex number.
     *
     * @param c
     *  The complex summand.
     *
     * @returns
     *  The sum of the complex numbers.
     *
     * @throws
     *  The error code `#VALUE!`, if the imaginary units of the complex numbers
     *  are different.
     */
    add(c: Complex): Complex {
        const unit = this.#unit(c);
        return new Complex(this.real + c.real, this.imag + c.imag, unit);
    }

    /**
     * Returns the difference between this and another complex number.
     *
     * @param c
     *  The complex subtrahend.
     *
     * @returns
     *  The difference between the complex numbers.
     *
     * @throws
     *  The error code `#VALUE!`, if the imaginary units of the complex numbers
     *  are different.
     */
    sub(c: Complex): Complex {
        const unit = this.#unit(c);
        return new Complex(this.real - c.real, this.imag - c.imag, unit);
    }

    /**
     * Returns the product of this and another complex number.
     *
     * @param c
     *  The complex factor.
     *
     * @returns
     *  The product of the complex numbers.
     *
     * @throws
     *  The error code `#VALUE!`, if the imaginary units of the complex numbers
     *  are different.
     */
    mul(c: Complex): Complex {
        const unit = this.#unit(c);
        const real = this.real * c.real - this.imag * c.imag;
        const imag = this.real * c.imag + this.imag * c.real;
        return new Complex(real, imag, unit);
    }

    /**
     * Returns the quotient of this and another complex number.
     *
     * @param c
     *  The complex divisor.
     *
     * @returns
     *  The quotient of the complex numbers.
     *
     * @throws
     *  The error code `#VALUE!`, if the imaginary units of the complex numbers
     *  are different. The error code `#NUM!` after a division by zero.
     */
    div(c: Complex): Complex {
        const unit = this.#unit(c);
        // division of complex number by zero results in #NUM! instead of #DIV/0! as in regular formulas
        const quot = checkNotZero(c.real * c.real + c.imag * c.imag);
        const real = this.real * c.real + this.imag * c.imag;
        const imag = this.imag * c.real - this.real * c.imag;
        return new Complex(real / quot, imag / quot, unit);
    }

    /**
     * Returns whether this complex number is zero.
     *
     * @returns
     *  Whether this complex number is zero.
     */
    isZero(): boolean {
        return math.isZero(this.real) && math.isZero(this.imag);
    }

    /**
     * Returns the absolute value of this complex number.
     *
     * @returns
     *  The absolute value of this complex number.
     */
    abs(): number {
        return math.radius(this.real, this.imag);
    }

    /**
     * Returns the argument of this complex number, as radiant.
     *
     * @returns
     *  The argument of this complex number, as radiant.
     *
     * @throws
     *  The error code `#DIV/0!`, if this complex number is zero.
     */
    arg(): number {
        return atanxy(this.real, this.imag);
    }

    /**
     * Returns the conjugate of this complex number.
     *
     * @returns
     *  The conjugate of this complex number.
     */
    conj(): Complex {
        return new Complex(this.real, -this.imag, this.unit);
    }

    /**
     * Returns the exponential of this complex number.
     *
     * @returns
     *  The complex exponential.
     */
    exp(): Complex {
        const r = Math.exp(this.real);
        return new Complex(r * Math.cos(this.imag), r * Math.sin(this.imag), this.unit);
    }

    /**
     * Returns the logarithm of this complex number to the passed base.
     *
     * @param base
     *  The base of the logarithm.
     *
     * @returns
     *  The complex logarithm.
     *
     * @throws
     *  The error code `#DIV/0!`, if this complex number is zero.
     */
    log(base: number): Complex {
        const quot = Math.log(base);
        return new Complex(Math.log(this.abs()) / quot, this.arg() / quot, this.unit);
    }

    /**
     * Returns the natural logarithm of this complex number.
     *
     * @returns
     *  The natural complex logarithm.
     *
     * @throws
     *  The error code `#DIV/0!`, if this complex number is zero.
     */
    ln(): Complex {
        return new Complex(Math.log(this.abs()), this.arg(), this.unit);
    }

    /**
     * Returns the decimal logarithm of this complex number.
     *
     * @returns
     *  The decimal complex logarithm.
     *
     * @throws
     *  The error code `#DIV/0!`, if this complex number is zero.
     */
    log10(): Complex {
        return new Complex(Math.log10(this.abs()), this.arg() / Math.LN10, this.unit);
    }

    /**
     * Returns the binary logarithm of this complex number.
     *
     * @returns
     *  The binary complex logarithm.
     *
     * @throws
     *  The error code `#DIV/0!`, if this complex number is zero.
     */
    log2(): Complex {
        return new Complex(Math.log2(this.abs()), this.arg() / Math.LN2, this.unit);
    }

    /**
     * Returns the power of this complex number to a real exponent.
     *
     * @param exp
     *  The exponent.
     *
     * @returns
     *  The complex power.
     *
     * @throws
     *  The error code `#NUM!`, if both numbers are zero.
     */
    pow(exp: number): Complex {

        // `0^exp` is undefined for non-positive exponents, and `0` otherwise
        if (this.isZero()) {
            if (exp <= 0) { throw ErrorCode.NUM; }
            return new Complex(0, 0, this.unit);
        }

        // calculate complex power via polar representation
        const r = this.abs() ** exp, a = this.arg() * exp;
        return new Complex(r * Math.cos(a), r * Math.sin(a), this.unit);
    }

    /**
     * Returns the square root of this complex number.
     *
     * @returns
     *  The complex square root.
     */
    sqrt(): Complex {

        // square root of zero is zero
        if (this.isZero()) { return new Complex(0, 0, this.unit); }

        // calculate complex square root via polar representation
        const r = Math.sqrt(this.abs()), a = this.arg() / 2;
        return new Complex(r * Math.cos(a), r * Math.sin(a), this.unit);
    }

    /**
     * Returns the sine of this complex number.
     *
     * @returns
     *  The complex sine.
     */
    sin(): Complex {
        const real = Math.sin(this.real) * Math.cosh(this.imag);
        const imag = Math.cos(this.real) * Math.sinh(this.imag);
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the cosine of this complex number.
     *
     * @returns
     *  The complex cosine.
     */
    cos(): Complex {
        const real = Math.cos(this.real) * Math.cosh(this.imag);
        const imag = -Math.sin(this.real) * Math.sinh(this.imag);
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the secant of this complex number.
     *
     * @returns
     *  The complex secant.
     *
     * @throws
     *  The error code `#NUM!` after a division by zero.
     */
    sec(): Complex {
        // division by zero results in `#NUM!` instead of `#DIV/0!` as in regular formulas
        const quot = checkNotZero(Math.cos(2 * this.real) + Math.cosh(2 * this.imag));
        const real = 2 * Math.cos(this.real) * Math.cosh(this.imag) / quot;
        const imag = 2 * Math.sin(this.real) * Math.sinh(this.imag) / quot;
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the cosecant of this complex number.
     *
     * @returns
     *  The complex cosecant.
     *
     * @throws
     *  The error code `#NUM!` after a division by zero.
     */
    csc(): Complex {
        // division by zero results in `#NUM!` instead of `#DIV/0!` as in regular formulas
        const quot = checkNotZero(Math.cos(2 * this.real) - Math.cosh(2 * this.imag));
        const real = -2 * Math.sin(this.real) * Math.cosh(this.imag) / quot;
        const imag = 2 * Math.cos(this.real) * Math.sinh(this.imag) / quot;
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the tangent of this complex number.
     *
     * @returns
     *  The complex tangent.
     *
     * @throws
     *  The error code `#NUM!` after a division by zero.
     */
    tan(): Complex {
        // division by zero results in `#NUM!` instead of `#DIV/0!` as in regular formulas
        const quot = checkNotZero(Math.cos(2 * this.real) + Math.cosh(2 * this.imag));
        const real = Math.sin(2 * this.real) / quot;
        const imag = Math.sinh(2 * this.imag) / quot;
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the cotangent of this complex number.
     *
     * @returns
     *  The complex cotangent.
     *
     * @throws
     *  The error code `#NUM!` after a division by zero.
     */
    cot(): Complex {
        // division by zero results in `#NUM!` instead of `#DIV/0!` as in regular formulas
        const quot = checkNotZero(Math.cos(2 * this.real) - Math.cosh(2 * this.imag));
        const real = -Math.sin(2 * this.real) / quot;
        const imag = Math.sinh(2 * this.imag) / quot;
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the hyperbolic sine of this complex number.
     *
     * @returns
     *  The complex hyperbolic sine.
     */
    sinh(): Complex {
        const real = Math.sinh(this.real) * Math.cos(this.imag);
        const imag = Math.cosh(this.real) * Math.sin(this.imag);
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the hyperbolic cosine of this complex number.
     *
     * @returns
     *  The complex hyperbolic cosine.
     */
    cosh(): Complex {
        const real = Math.cosh(this.real) * Math.cos(this.imag);
        const imag = Math.sinh(this.real) * Math.sin(this.imag);
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the hyperbolic secant of this complex number.
     *
     * @returns
     *  The complex hyperbolic secant.
     *
     * @throws
     *  The error code `#NUM!` after a division by zero.
     */
    sech(): Complex {
        // division by zero results in `#NUM!` instead of `#DIV/0!` as in regular formulas
        const quot = checkNotZero(Math.cosh(2 * this.real) + Math.cos(2 * this.imag));
        const real = 2 * Math.cosh(this.real) * Math.cos(this.imag) / quot;
        const imag = -2 * Math.sinh(this.real) * Math.sin(this.imag) / quot;
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the hyperbolic cosecant of this complex number.
     *
     * @returns
     *  The complex hyperbolic cosecant.
     *
     * @throws
     *  The error code `#NUM!` after a division by zero.
     */
    csch(): Complex {
        // division by zero results in `#NUM!` instead of `#DIV/0!` as in regular formulas
        const quot = checkNotZero(Math.cosh(2 * this.real) - Math.cos(2 * this.imag));
        const real = 2 * Math.sinh(this.real) * Math.cos(this.imag) / quot;
        const imag = -2 * Math.cosh(this.real) * Math.sin(this.imag) / quot;
        return new Complex(real, imag, this.unit);
    }

    /**
     * Returns the string representation of this complex number for debug
     * logging.
     *
     * _Attention:_ DO NOT USE this method for GUI related code where correct
     * locale dependent representation of the complex number is required.
     *
     * @returns
     *  The string representation of this complex number for debug logging.
     */
    toString(): string {
        return `${this.real}${(this.imag >= 0) ? "+" : ""}${this.imag}${this.unit || "i"}`;
    }

    // private methods --------------------------------------------------------

    /**
     * Checks the imaginary unit of the passed complex number. Throws the error
     * code `#VALUE!`, if its imaginary unit differs from the own. Otherwise,
     * returns the resulting imaginary unit to be used (either may be missing).
     *
     * @param c
     *  The complex number to be checked.
     *
     * @returns
     *  The imaginary unit to be used for the resulting complex number.
     *
     * @throws
     *  The error code `#VALUE!`, if the imaginary unit of the passed complex
     *  number differs from the own imaginary unit.
     */
    #unit(c: Complex): Opt<string> {
        return (this.unit && c.unit && (this.unit !== c.unit)) ? ErrorCode.VALUE.throw() : (this.unit || c.unit);
    }
}
