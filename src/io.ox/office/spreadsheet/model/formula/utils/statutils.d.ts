/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CommonNumberIteratorOptions, OperandIteratorSource, OperandAggregatorSource, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";

// types ======================================================================

export type GroupAggregateAnyFunc = (context: FormulaContext, operands: OperandAggregatorSource, options?: CommonNumberIteratorOptions) => number;
export type GroupAggregateNumFunc = (context: FormulaContext, operand: OperandIteratorSource, k: number, options?: CommonNumberIteratorOptions) => number;

// functions ==================================================================

export function getPercentileInclusive(numbers: readonly number[], p: number, sorted: true): number;
export function getPercentileInclusive(numbers: number[], p: number, sorted?: boolean): number;

export const groupAverage: GroupAggregateAnyFunc;
export const groupCount: GroupAggregateAnyFunc;
export const groupCountA: GroupAggregateAnyFunc;
export const groupMax: GroupAggregateAnyFunc;
export const groupMin: GroupAggregateAnyFunc;
export const groupSum: GroupAggregateAnyFunc;
export const groupProduct: GroupAggregateAnyFunc;
export const groupStdDevS: GroupAggregateAnyFunc;
export const groupStdDevP: GroupAggregateAnyFunc;
export const groupVarS: GroupAggregateAnyFunc;
export const groupVarP: GroupAggregateAnyFunc;
export const groupMedian: GroupAggregateAnyFunc;
export const groupModeSngl: GroupAggregateAnyFunc;

export const groupLarge: GroupAggregateNumFunc;
export const groupSmall: GroupAggregateNumFunc;
export const groupPercentileInc: GroupAggregateNumFunc;
export const groupQuartileInc: GroupAggregateNumFunc;
export const groupPercentileExc: GroupAggregateNumFunc;
export const groupQuartileExc: GroupAggregateNumFunc;
