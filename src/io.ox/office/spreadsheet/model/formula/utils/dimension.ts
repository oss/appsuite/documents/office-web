/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Range } from "@/io.ox/office/spreadsheet/utils/range";

const { max } = Math;

// class Dimension ============================================================

/**
 * Representation the dimension of a formula operand, i.e. the number of rows
 * and columns of a matrix or cell range.
 */
export class Dimension implements Equality<Dimension>, Cloneable<Dimension> {

    // static functions -------------------------------------------------------

    /**
     * Returns the dimension of the passed cell range address.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  The dimension of the passed cell range address.
     */
    static createFromRange(range: Range): Dimension {
        return new Dimension(range.rows(), range.cols());
    }

    /**
     * Creates a new dimension instance representing the bounding size (the
     * maximum of the row numbers and column numbers of the passed dimensions).
     *
     * @param dim1
     *  The first dimension instance. If set to null, the second passed
     *  dimension will be returned.
     *
     * @param dim2
     *  The second dimension instance. If set to null, the first passed
     *  dimension will be returned.
     *
     * @returns
     *  The resulting boundary dimension. If both parameters are set to null,
     *  this method will return null.
     */
    static boundary(dim1: Dimension | null, dim2: Dimension | null): Dimension | null {
        if (!dim1) { return dim2; }
        if (!dim2) { return dim1; }
        return new Dimension(max(dim1.rows, dim2.rows), max(dim1.cols, dim2.cols));
    }

    // properties -------------------------------------------------------------

    readonly rows: number;
    readonly cols: number;

    // constructor ------------------------------------------------------------

    /**
     * @param rows
     *  The number of rows of the operand (positive integer).
     *
     * @param cols
     *  The number of columns of the operand (positive integer).
     */
    constructor(rows: number, cols: number) {
        this.rows = rows;
        this.cols = cols;
    }

    // public method ----------------------------------------------------------

    /**
     * Returns a clone of this instance.
     *
     * @returns
     *  A clone of this instance.
     */
    clone(): Dimension {
        return new Dimension(this.rows, this.cols);
    }

    /**
     * Returns whether the dimension represented by this instance and the
     * passed dimension are equal.
     *
     * @param dim
     *  The other dimension to compare with this instance.
     *
     * @returns
     *  Whether this dimension and the passed dimension are equal.
     */
    equals(dim: Dimension): boolean {
        return (this.rows === dim.rows) && (this.cols === dim.cols);
    }

    /**
     * Returns the number of elements contained in an operand of the current
     * dimension.
     *
     * @returns
     *  The number of elements represented by this instance.
     */
    size(): number {
        return this.rows * this.cols;
    }

    /**
     * Returns the string representation of this instance for debug logging.
     *
     * @returns
     *  The string representation of this instance for debug logging.
     */
    toString(): string {
        return `${this.rows}x${this.cols}`;
    }
}
