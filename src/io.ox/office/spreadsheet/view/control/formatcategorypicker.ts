/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { type RadioListConfig, RadioList } from "@/io.ox/office/tk/control/radiolist";
import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { FORMAT_CATEGORY_LIST } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// class FormatCategoryPicker =================================================

/**
 * A dropdown list control for choosing number format categories, that will
 * load predefined format codes in the format code picker control.
 */
export class FormatCategoryPicker extends RadioList<FormatCategory> {

    /**
     * @param _docView
     *  The document view containing this instance.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(_docView: SpreadsheetView, config?: RadioListConfig<FormatCategory>) {

        // base constructor
        super({
            icon: "bi:123",
            labelStyle: { fontSize: "1.15em", fontWeight: "bold" },
            tooltip: gt("Number format"),
            ...config,
            updateCaptionMode: "none"
        });

        // insert all supported list items
        for (const entry of FORMAT_CATEGORY_LIST) {
            if (entry.category !== FormatCategory.CUSTOM) {
                this.addOption(entry.category, { label: entry.label });
            }
        }
    }
}
