/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { type RadioGroupConfig, RadioGroup } from "@/io.ox/office/tk/control/radiogroup";
import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { FORMAT_CATEGORY_LIST } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// class FormatCategoryGroup ==================================================

/**
 * A button group with selected number format categories.
 */
export class FormatCategoryGroup extends RadioGroup<FormatCategory> {

    // constructor ------------------------------------------------------------

    /**
     * @param _docView
     *  The document view containing this instance.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(_docView: SpreadsheetView, config?: RadioGroupConfig<FormatCategory>) {

        super({
            toggleValue: FormatCategory.STANDARD,
            ...config
        });

        // set accessible name - WCAG 2.0 Success Criterion 4.1.2
        this.el.setAttribute("aria-label", gt("Number formats"));
        this.el.setAttribute("aria-labelledby", "dummy");

        // initialize the option buttons
        this.#createOptions();

        // update currency symbol according to changed locale
        this.listenToGlobal("change:locale", this.#createOptions);
    }

    // private methods --------------------------------------------------------

    /**
     * Creates an option button for this button group.
     *
     * @param category
     *  The internal number of the number format category, used as value for
     *  the option button.
     *
     * @param symbol
     *  The short label for the option button.
     */
    #addOption(category: FormatCategory, symbol: string): void {
        const { label } = FORMAT_CATEGORY_LIST.find(entry => entry.category === category)!;
        this.addOption(category, { icon: `txt:${symbol}`, tooltip: label, dropDownVersion: { label } });
    }

    /**
     * Creates all option buttons for this button group.
     */
    #createOptions(): void {
        this.clearOptions();
        this.#addOption(FormatCategory.CURRENCY, _.noI18n(LOCALE_DATA.currency.symbol));
        this.#addOption(FormatCategory.PERCENT,  _.noI18n("%"));
    }
}
