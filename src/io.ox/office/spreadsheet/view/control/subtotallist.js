/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";

// class SubtotalList =========================================================

/**
 * A dropdown list control for displaying different subtotal values.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 *
 * @param {object} [config]
 *  Configuration options. Will be passed to the base class `RadioList`.
 */
export class SubtotalList extends RadioList {

    constructor(docView, config) {

        // base constructor
        super({
            tooltip: gt("Type of the subtotal value"),
            ...config,
            repaintAlways: true // always repaint dropdown menu when opening
        });

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // update the visible label of this control
        this.listenTo(docView, "update:selection:data", () => this.refresh());

        // lazy repaint of the menu when selection changes
        this.menu.requestMenuRepaintOnEvent(docView, "update:selection:data");
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the visible label of this control.
     */
    /*protected override*/ implUpdate(type) {
        super.implUpdate(type);

        // get fresh subtotals
        const subtotals = this.docView.getSubtotalResult();

        // if selection does not contain numbers at all, show cell count
        if (subtotals.numbers === 0) { type = "cells"; }

        // update the caption of the menu button
        if (type in subtotals) {
            this.setLabel(this._getCaptionOptions(type, subtotals[type]).label);
        }
    }

    /**
     * Recreates all sheet entries in the dropdown menu.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // get fresh subtotals
        const subtotals = this.docView.getSubtotalResult();

        // creates an option button for the current subtotals
        const addOption = type => this.addOption(type, { ...this._getCaptionOptions(type, subtotals[type]) });

        // clear all old contents
        this.clearOptions();

        // Note: Option button values MUST be equal to the property names in class SubtotalResult!

        // always create the cell count entry
        this.addSection("cells");
        addOption("cells");

        // create numerical entries on demand
        if (subtotals.numbers > 0) {
            this.addSection("numbers");
            addOption("numbers");
            addOption("sum");
            addOption("min");
            addOption("max");
            addOption("average");
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns caption options of a list entry for the specified subtotal
     * type and value.
     */
    /*private*/ _getCaptionOptions(type, value) {

        // the parsed number format of the active cell
        const parsedFormat = this.docView.getActiveParsedFormat();
        // the resulting label text
        let label = "";

        // format finite numbers with the number format of the active cell
        if (Number.isFinite(value)) {

            // different formatting and special handling according to subtotal type
            if ((type === "cells") || (type === "numbers")) {
                // subtotals of type "cells" and "numbers" are integers
                label = String(value);
            } else if ((type === "sum") && parsedFormat.isAnyDateTime()) {
                // type "sum" with date/time: show localized #N/A error
                label = this.docModel.formulaGrammarUI.NA_ERROR;
            } else {
                // format number according to the active cell's number format
                label = this.docModel.numberFormatter.formatValue(parsedFormat, value).text || String(value);
            }
        }

        switch (type) {
            case "sum":
                return {
                    //#. Subtotal result in OX Spreadsheet's status bar: Sum of all selected numeric cells
                    label: gt("Sum: %1$s", label),
                    tooltip: gt("Sum of selected cells")
                };
            case "min":
                return {
                    //#. Subtotal result in OX Spreadsheet's status bar: Minimum of all selected numeric cells
                    label: gt("Min: %1$s", label),
                    tooltip: gt("Minimum value in selection")
                };
            case "max":
                return {
                    //#. Subtotal result in OX Spreadsheet's status bar: Maximum of all selected numeric cells
                    label: gt("Max: %1$s", label),
                    tooltip: gt("Maximum value in selection")
                };
            case "cells":
                return {
                    //#. Subtotal result in OX Spreadsheet's status bar: Count of all selected numeric cells AND text cells
                    label: gt("Count: %1$s", label),
                    tooltip: gt("Number of selected cells that contain data")
                };
            case "numbers":
                return {
                    //#. Subtotal result in OX Spreadsheet's status bar: Count of all selected numeric cells only (NOT text cells)
                    label: gt("Numerical count: %1$s", label),
                    tooltip: gt("Number of selected cells that contain numerical data")
                };
            case "average":
                return {
                    //#. Subtotal result in OX Spreadsheet's status bar: Average of all selected numeric cells
                    label: gt("Average: %1$s", label),
                    tooltip: gt("Average of selected cells")
                };
        }
    }
}
