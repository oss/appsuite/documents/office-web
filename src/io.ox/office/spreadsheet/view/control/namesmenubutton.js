/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button } from "@/io.ox/office/tk/control/button";
import { CompoundSplitButton } from "@/io.ox/office/baseframework/view/control/compoundsplitbutton";

// class NamesMenuButton ======================================================

/**
 * A compound button providing a shortcut to show the floating menu for all
 * defined names in the document, and a dropdown menu with additional options.
 */
export class NamesMenuButton extends CompoundSplitButton {

    constructor(docView) {

        // base constructor
        super(docView, {
            label: gt.pgettext("named ranges", "Named ranges"),
            tooltip: gt.pgettext("named ranges", "Show or hide the named ranges in this document"),
            toggle: true,
            value: "toggle"
        });

        // create all dropdown menu items
        this.addControl("name/insert/dialog", new Button({ icon: "svg:add", label: gt.pgettext("named ranges", "Add new range"), tooltip: gt.pgettext("named ranges", "Add a named range") }));
    }
}
