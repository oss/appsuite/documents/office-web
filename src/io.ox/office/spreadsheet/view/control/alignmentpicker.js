/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";
import { HOR_ALIGNMENT_LABELS, VERT_ALIGNMENT_LABELS } from "@/io.ox/office/spreadsheet/view/labels";

// constants ==================================================================

const HOR_ALIGNMENT_TOOLTIP = /*#. text alignment in cells */ gt("Horizontal alignment");
const VERT_ALIGNMENT_TOOLTIP = /*#. text alignment in cells */ gt("Vertical alignment");

// list entries for horizontal orientation
const HOR_ALIGNMENT_ENTRIES = [
    { value: "auto",    icon: "png:cell-h-align-auto" },
    { value: "left",    icon: "png:cell-h-align-left" },
    { value: "center",  icon: "png:cell-h-align-center" },
    { value: "right",   icon: "png:cell-h-align-right" },
    { value: "justify", icon: "png:cell-h-align-justify" }
];

// list entries for vertical orientation
const VERT_ALIGNMENT_ENTRIES = [
    { value: "top",     icon: "png:cell-v-align-top" },
    { value: "middle",  icon: "png:cell-v-align-middle" },
    { value: "bottom",  icon: "png:cell-v-align-bottom" },
    { value: "justify", icon: "png:cell-v-align-justify" }
];

// class AlignmentPicker ======================================================

/**
 * Base class for picker controls for horizontal or vertical cell alignment.
 */
class AlignmentPicker extends RadioList {

    constructor(entries, labels, tooltip) {

        // base constructor
        super({
            icon: entries[0].icon,
            tooltip,
            updateCaptionMode: "icon",
            dropDownVersion: { label: tooltip }
        });

        // initialization
        entries.forEach(({ icon, value }) => {
            const label = labels[value];
            this.addOption(value, { icon, label });
        });
    }
}

// class HAlignmentPicker =====================================================

/**
 * A picker control for horizontal cell alignment.
 */
export class HAlignmentPicker extends AlignmentPicker {
    constructor() {
        super(HOR_ALIGNMENT_ENTRIES, HOR_ALIGNMENT_LABELS, HOR_ALIGNMENT_TOOLTIP);
    }
}

// class VAlignmentPicker =====================================================

/**
 * A picker control for vertical cell alignment.
 */
export class VAlignmentPicker extends AlignmentPicker {
    constructor() {
        super(VERT_ALIGNMENT_ENTRIES, VERT_ALIGNMENT_LABELS, VERT_ALIGNMENT_TOOLTIP);
    }
}
