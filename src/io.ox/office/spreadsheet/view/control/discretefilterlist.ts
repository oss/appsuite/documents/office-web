/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { is, str, map, set, jpromise } from "@/io.ox/office/tk/algorithms";
import { splitUTCDateTime } from "@/io.ox/office/tk/utils/dateutils";
import { type CheckListEntry, CheckList } from "@/io.ox/office/tk/form";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import type { ScalarType, CompareScalarOptions } from "@/io.ox/office/spreadsheet/utils/scalar";
import { CompareNullMode, compareScalars } from "@/io.ox/office/spreadsheet/utils/scalar";
import { convertFilterString } from "@/io.ox/office/spreadsheet/model/tablefiltermatcher";
import type { DiscreteFilterEntry } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import type { CellContents, TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

interface DiscreteFilterListEntry extends CheckListEntry<DiscreteFilterEntry> {
    sortIndex: ScalarType;
}

// constants ==================================================================

//#. Represents a list entry in spreadsheet data filters that selects all blank (empty) cells in a column
const BLANK_CELLS_LABEL = gt.pgettext("filter", "Blank cells");

// sorting options for list entries
const SORT_OPTIONS: CompareScalarOptions = { numeric: true, nullMode: CompareNullMode.NULL_MAX };

// private functions ==========================================================

function valueSerializer(value: DiscreteFilterEntry): string {
    return is.string(value) ? (value || ":blank") : `${value.t}:${value.v}`;
}

// class DiscreteCheckListEntryBuilder ========================================

/**
 * Helper class to build the hierarchical configuration of a tree-style
 * `CheckList` control for a discrete filter with date group support.
 */
class DiscreteFilterListEntryBuilder {

    readonly docModel: SpreadsheetModel;

    readonly #displayCache = new Set<string>();
    readonly #dateGroupCache = new Map<string, DiscreteFilterListEntry[]>();
    readonly #listEntries: DiscreteFilterListEntry[] = [];
    readonly #dayEntries: DiscreteFilterListEntry[] = [];

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        this.docModel = docModel;
    }

    // public methods ---------------------------------------------------------

    insertEntry(cellEntry: CellContents): void {
        const date = this.docModel.getDateForDateGroupFilter(cellEntry.value, cellEntry.format);
        if (date) {
            this.#insertDate(date);
        } else {
            this.#insertDisplay(cellEntry.display);
        }
    }

    finalize(): DiscreteFilterListEntry[] {

        // returns a single child entry of the passed list entry, if its sort index is zero
        const resolveSingleChildWithZero = (entry: Opt<DiscreteFilterListEntry>): Opt<DiscreteFilterListEntry> => {
            const childEntry = (entry?.children?.length === 1) ? entry.children[0] : undefined;
            return (childEntry?.sortIndex === 0) ? childEntry : undefined;
        };

        // remove single midnight time entries from all day entries
        for (const dayEntry of this.#dayEntries) {
            const hourEntry = resolveSingleChildWithZero(dayEntry);
            const minuteEntry = resolveSingleChildWithZero(hourEntry);
            const secondEntry = resolveSingleChildWithZero(minuteEntry);
            if (secondEntry) { delete dayEntry.children; }
        }

        return this.#listEntries;
    }

    // private methods --------------------------------------------------------

    #insertDisplay(display: string): void {

        // the text label displayed in the list entry (preserve character case)
        // bug 36264: trim space characters but no other whitespace
        const label = convertFilterString(display, true);

        // insert first occurrence of the display string, ignoring character case
        const value = convertFilterString(display);
        if (set.toggle(this.#displayCache, value, true)) {

            // create the new list entry
            const listEntry: DiscreteFilterListEntry = {
                value,
                label: label ? _.noI18n(label) : BLANK_CELLS_LABEL,
                style: label ? null : { fontStyle: "italic" },
                sortIndex: label || null // empty string => "Blank cells" => last entry
            };

            // append to configuration list (list control will take care for sorting later)
            this.#listEntries.push(listEntry);
        }
    }

    #insertDate(date: Date): void {

        let listEntries = this.#listEntries;
        let value = "";

        const createListEntry = (token: number, label: string, extEntries?: DiscreteFilterListEntry[]): void => {
            value = str.concatTokens(value, String(token));
            listEntries = map.upsert(this.#dateGroupCache, value, () => {

                // the list of child entries
                const children: DiscreteFilterListEntry[] = [];

                // create the new list entry
                const listEntry: DiscreteFilterListEntry = {
                    value: { t: "date", v: value },
                    label: _.noI18n(label),
                    children,
                    sortIndex: token // sort by numbers, not by label (e.g. month names)
                };

                // append to configuration list (list control will take care for sorting later)
                listEntries.push(listEntry);
                extEntries?.push(listEntry);

                // insert the list of child entries into the cache
                return children;
            });
        };

        const { Y, M, D, h, m, s } = splitUTCDateTime(date);
        createListEntry(Y, String(Y).padStart(4, "0"));
        createListEntry(M + 1, LOCALE_DATA.longMonths[M]);
        createListEntry(D, String(D).padStart(2, "0"), this.#dayEntries);
        createListEntry(h, String(h).padStart(2, "0"));
        createListEntry(m, String(m).padStart(2, "0"));
        createListEntry(s, String(s).padStart(2, "0"));
    }
}

// class DiscreteFilterList ===================================================

/**
 * A check-list control that shows all entries of a discrete filter (all
 * different values in a table column). Supports a hierarchical tree view for
 * date group rules.
 */
export class DiscreteFilterList extends CheckList<DiscreteFilterEntry, DiscreteFilterListEntry> {

    constructor() {
        super([], [], {
            equality: _.isEqual,
            valueSerializer,
            sort: (e1, e2) => compareScalars(e1.sortIndex, e2.sortIndex, SORT_OPTIONS),
            checkAll: true
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Collects and inserts the cell entries of a table column into this list
     * control.
     *
     * @param tableModel
     *  The model of the table range in the active sheet to be filtered.
     *
     * @param tableCol
     *  The zero-based column index, relative to the table range.
     *
     * @returns
     *  A promise that will fulfil when the control has been initialized.
     */
    initialize(tableModel: TableModel, tableCol: number): JPromise<unknown> {

        this.clearEntries();

        // resolve the table column model
        const columnModel = tableModel.getColumnModel(tableCol);

        // unfiltered table column: show entries of all visible rows; filtered table column: show
        // entries for all data rows that are not filtered by other columns (but that may be hidden)
        const queryPromise = columnModel.isFiltered() ?
            tableModel.queryFilterResult({ skipColumns: [tableCol] }) :
            jpromise.resolve(tableModel.getVisibleDataRows());

        // fetch cell contents of the column from the document
        this.onFulfilled(queryPromise, rowIntervals => {

            // fetch all cell contents from the table column
            const cellArray = tableModel.getColumnContents(tableCol, rowIntervals);

            // create the configuration for the check list control
            const entryBuilder = new DiscreteFilterListEntryBuilder(tableModel.docModel);
            for (const cellEntry of cellArray) {
                entryBuilder.insertEntry(cellEntry);
            }

            // initialize list entries according to current filter rules
            const listEntries = entryBuilder.finalize();
            const value = columnModel.getDiscreteEntries() ?? listEntries.map(entry => entry.value);
            this.replaceEntries(listEntries, value);
        });

        return queryPromise;
    }
}
