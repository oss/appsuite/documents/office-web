/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button } from "@/io.ox/office/tk/control/button";
import { CompoundSplitButton } from "@/io.ox/office/baseframework/view/control/compoundsplitbutton";
import { SORT_ASC_BUTTON_OPTIONS, SORT_DESC_BUTTON_OPTIONS, SORT_CUSTOM_BUTTON_OPTIONS } from "@/io.ox/office/spreadsheet/view/labels";

// class SortMenuButton =======================================================

/**
 * Sorting control in toolbar.
 *
 * @param {SpreadsheetView} docView
 *  The document view containing this instance.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `CompoundSplitButton`, except "value" and "updateCaptionMode".
 */
export class SortMenuButton extends CompoundSplitButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            icon: SORT_ASC_BUTTON_OPTIONS.icon,
            //#. spreadsheet sorting: button tooltip for sorting
            label: gt.pgettext("sort", "Sorting"),
            ...config,
            value: "toggle"
        });

        // create all drop-down menu items
        this.addControl("table/sort", new Button({ value: "ascending",  ...SORT_ASC_BUTTON_OPTIONS }));
        this.addControl("table/sort", new Button({ value: "descending", ...SORT_DESC_BUTTON_OPTIONS }));
        this.addSection("dialog");
        this.addControl("table/sort/dialog", new Button(SORT_CUSTOM_BUTTON_OPTIONS));
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(sortOrder) {
        super.implUpdate(sortOrder);
        switch (sortOrder) {
            case "ascending":  this.setIcon(SORT_ASC_BUTTON_OPTIONS.icon);  break;
            case "descending": this.setIcon(SORT_DESC_BUTTON_OPTIONS.icon); break;
        }
    }
}
