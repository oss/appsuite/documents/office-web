/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { Color } from "@/io.ox/office/editframework/utils/color";
import { ColorPicker } from "@/io.ox/office/editframework/view/control/colorpicker";
import { BORDER_COLOR_LABEL } from "@/io.ox/office/spreadsheet/view/labels";

// class CellBorderColorPicker ================================================

/**
 * A selector control for the cell border color.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class CellBorderColorPicker extends ColorPicker {

    constructor(docView) {

        // base constructor
        super(docView, {
            splitValue: Color.AUTO,
            icon: "png:cell-line-color",
            tooltip: BORDER_COLOR_LABEL,
            title: BORDER_COLOR_LABEL,
            dropDownVersion: { label: BORDER_COLOR_LABEL },
            autoColorType: "line"
        });

        // refresh the "automatic" border color (depends on sheet grid color)
        this.listenTo(docView, "change:sheetprops", this._updateAutoColor);

        // update auto color once when import is done
        this.waitForImportSuccess(this._updateAutoColor);
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the color used for the "automatic color" list entry of this
     * color picker control (which is the grid color of the active sheet).
     */
    /*private*/ _updateAutoColor() {
        this.setAutoColor(this.docView.sheetModel.getGridColor());
    }
}
