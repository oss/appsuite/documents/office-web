/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { is, str, map } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA, localeDataRegistry } from "@/io.ox/office/tk/locale";

import { type RadioListConfig, AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import { yieldFixedCurrencyData } from "@/io.ox/office/editframework/model/formatter/currencycodes";
import { PresetCodeId, PresetFormatTable } from "@/io.ox/office/editframework/model/formatter/presetformattable";
import type { FormatCodeSpec, DecimalFormatOptions, PercentFormatOptions, ScientificFormatOptions, CurrencyCodeOptions, AccountingCodeOptions } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { FormatCategory, SYSDATE_LOCALE_TAG, SYSTIME_LOCALE_TAG } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { MORE_LABEL, CUSTOM_CURRENCY_LABEL } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// globals ====================================================================

const presetTableMap = new Map<string, PresetFormatTable>();

// private functions ==========================================================

// bug 35169: accept escaped and unescaped space as matching format code, trim trailing text token
function normalizeFormatCode(formatCode: string): string {
    return formatCode ? formatCode.toLowerCase().replace(/\\ |;@$/g, " ") : "";
}

function equalFormatCodes(formatCode1: string, formatCode2: string): boolean {
    formatCode1 = normalizeFormatCode(formatCode1);
    formatCode2 = normalizeFormatCode(formatCode2);
    return (formatCode1 === formatCode2) ||
        (str.startsWithICC(formatCode1, SYSDATE_LOCALE_TAG) && str.startsWithICC(formatCode2, SYSDATE_LOCALE_TAG)) ||
        (str.startsWithICC(formatCode1, SYSTIME_LOCALE_TAG) && str.startsWithICC(formatCode2, SYSTIME_LOCALE_TAG));
}

// class FormatCodePicker =====================================================

/**
 * A dropdown list control for selection of predefined number formats depending
 * to currently selected category in the FormatCategoryPicker.
 */
export class FormatCodePicker extends AppRadioList<SpreadsheetView, string> {

    /**
     * @param docView
     *  The document view containing this instance.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(docView: SpreadsheetView, config?: RadioListConfig<string>) {

        // base constructor
        super(docView, {
            label: MORE_LABEL,
            //#. Button tooltip: special number format codes for different categories of number formats
            tooltip: gt.pgettext("number-format", "More number formats"),
            ...config,
            updateCaptionMode: "none",
            matcher: (formatCode, optionCode) => this.#optionMatcher(formatCode, optionCode),
            filter: (_value, section) => this.#sectionFilter(section)
        });

        // CSS marker class for additional styling
        this.menu.el.classList.add("format-code-picker");

        // initialize the dropdown list
        this.#createOptions();

        // redraw the number format dropdown list after changing the UI locale
        this.listenTo(this.docModel.numberFormatter, "change:presets", this.#createOptions);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether the passed (arbitrary) format code matches with the
     * format code of a list option.
     */
    #optionMatcher(formatCode: string, optionCode: string): boolean {
        return !!formatCode && equalFormatCodes(formatCode, optionCode);
    }

    /**
     * Returns whether a format code belongs to a specific format category.
     */
    #isCategoryCode(formatCode: Opt<string>, category: FormatCategory): boolean {
        return is.string(formatCode) && (this.docModel.numberFormatter.getParsedFormat(formatCode).category === category);
    }

    /**
     * Returns whether the specified section is visible.
     */
    #sectionFilter(section: string): boolean {
        const category = section.split(":")[0] as FormatCategory;
        return this.#isCategoryCode(this.getValue(), category);
    }

    /**
     * Creates an option button in the dropdown menu.
     */
    #addOption(category: FormatCategory, formatSpec: FormatCodeSpec, previewValue: number | string, sectionIdx?: number): void {

        // the formatted preview value
        const { numberFormatter } = this.docModel;
        const parsedFormat = numberFormatter.getParsedFormat(formatSpec);
        const formatResult = numberFormatter.formatValue(parsedFormat, previewValue);

        // create an option button for the primary format code
        this.addOption(parsedFormat.formatCode, {
            section: `${category}:${sectionIdx || 0}`,
            classes: formatResult.color ? "negative-red" : null,
            label: _.noI18n(formatResult.text!)
        });
    }

    /**
     * Creates an entry in the dropdown menu for a decimal number format.
     */
    #addNumberOption(options: DecimalFormatOptions, sectionIdx?: number): void {
        const parsedFormat = this.docModel.numberFormatter.getDecimalFormat(options);
        this.#addOption(FormatCategory.NUMBER, parsedFormat, -1234.5678, sectionIdx);
    }

    /**
     * Creates entries in the dropdown menu for decimal number formats.
     */
    #addNumberOptions(options: DecimalFormatOptions, sectionIdx?: number): void {
        this.#addNumberOption({ int: true,  red: false, ...options }, sectionIdx);
        this.#addNumberOption({ int: false, red: false, ...options }, sectionIdx);
        this.#addNumberOption({ int: true,  red: true,  ...options }, sectionIdx);
        this.#addNumberOption({ int: false, red: true,  ...options }, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a currency format.
     */
    #addCurrencyOption(options: CurrencyCodeOptions | string, sectionIdx?: number): void {
        const { numberFormatter } = this.docModel;
        const parsedFormat = is.string(options) ? numberFormatter.getParsedFormat(options) : numberFormatter.getCurrencyFormat(options);
        this.#addOption(FormatCategory.CURRENCY, parsedFormat, -1234.5678, sectionIdx);
    }

    /**
     * Creates all entries in the dropdown menu for currency formats.
     */
    #addCurrencyOptions(presetTable: PresetFormatTable, sectionIdx?: number): void {
        // this.#addCurrencyOption(presetTable.getCurrencyCode({ int: true,  red: false }), sectionIdx);
        this.#addCurrencyOption(presetTable.getCurrencyCode({ int: false, red: false }), sectionIdx);
        // this.#addCurrencyOption(presetTable.getCurrencyCode({ int: true,  red: true  }), sectionIdx);
        this.#addCurrencyOption(presetTable.getCurrencyCode({ int: false, red: true  }), sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for an accounting format.
     */
    #addAccountingOption(options: AccountingCodeOptions | string, sectionIdx?: number): void {
        const { numberFormatter } = this.docModel;
        const parsedFormat = is.string(options) ? numberFormatter.getParsedFormat(options) : numberFormatter.getAccountingFormat(options);
        this.#addOption(FormatCategory.ACCOUNTING, parsedFormat, -1234.5678, sectionIdx);
    }

    /**
     * Creates all entries in the dropdown menu for accounting formats.
     */
    #addAccountingOptions(presetTable: PresetFormatTable, sectionIdx?: number): void {
        // this.#addAccountingOption(presetTable.getAccountingCode({ int: true,  blind: false }), sectionIdx);
        this.#addAccountingOption(presetTable.getAccountingCode({ int: false, blind: false }), sectionIdx);
        // this.#addAccountingOption(presetTable.getAccountingCode({ int: true,  blind: true  }), sectionIdx);
        this.#addAccountingOption(presetTable.getAccountingCode({ int: false, blind: true  }), sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a percentage format.
     */
    #addPercentOption(options: PercentFormatOptions, sectionIdx?: number): void {
        const parsedFormat = this.docModel.numberFormatter.getPercentFormat(options);
        this.#addOption(FormatCategory.PERCENT, parsedFormat, -0.1234, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a scientific format.
     */
    #addScientificOption(options: ScientificFormatOptions, sectionIdx?: number): void {
        const parsedFormat = this.docModel.numberFormatter.getScientificFormat(options);
        this.#addOption(FormatCategory.SCIENTIFIC, parsedFormat, -1234, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a fraction format.
     */
    #addFractionOption(formatSpec: FormatCodeSpec, previewValue: number, sectionIdx?: number): void {
        this.#addOption(FormatCategory.FRACTION, formatSpec, previewValue, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a date format.
     */
    #addDateOption(formatSpec: FormatCodeSpec, sectionIdx?: number): void {
        this.#addOption(FormatCategory.DATE, formatSpec, 44057, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a time format.
     */
    #addTimeOption(formatSpec: FormatCodeSpec, sectionIdx?: number): void {
        this.#addOption(FormatCategory.TIME, formatSpec, 1 + 48642 / 86400, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for a date/time format.
     */
    #addDateTimeOption(formatSpec: FormatCodeSpec, sectionIdx?: number): void {
        this.#addOption(FormatCategory.DATETIME, formatSpec, 44057 + 48642 / 86400, sectionIdx);
    }

    /**
     * Creates an entry in the dropdown menu for the "Custom Currency" dialog.
     */
    #addCustomCurrencyOption(category: FormatCategory): void {
        this.addOption(":custom", {
            section: category + ":999",
            label: CUSTOM_CURRENCY_LABEL,
            customHandler: () => {
                this.onFulfilled(import("@/io.ox/office/spreadsheet/view/dialog/customcurrencydialog"), ({ CustomCurrencyDialog }) => {
                    const dialog = new CustomCurrencyDialog(this.docView);
                    this.onSettled(dialog.show(), result => {
                        if (result.fulfilled) {
                            this.triggerCommit(result.value);
                        } else {
                            this.triggerCancel();
                        }
                    }, { async: true });
                });
            },
        });
    }

    /**
     * Creates all option buttons in the dropdown menu.
     */
    #createOptions(): void {

        const { numberFormatter } = this.docModel;

        // preset tables needed to generate builtin currency/accounting formats
        const currencyPresetTables = Array.from(yieldFixedCurrencyData(), ([locale]) => {
            return (locale === LOCALE_DATA.intl) ? numberFormatter.presetTable :
                map.upsert(presetTableMap, locale, () => new PresetFormatTable(localeDataRegistry.getLocaleData(locale), { _kind: "singleton" }));
        });

        this.clearOptions();

        // category NUMBER
        this.#addNumberOptions({ group: false }, 1);
        this.#addNumberOptions({ group: true  }, 2);

        // category CURRENCY
        for (const [index, presetTable] of currencyPresetTables.entries()) {
            this.#addCurrencyOptions(presetTable, index + 1);
        }

        // category ACCOUNTING
        for (const [index, presetTable] of currencyPresetTables.entries()) {
            this.#addAccountingOptions(presetTable, index + 1);
        }

        // category PERCENT
        this.#addPercentOption({ int: true,  red: false });
        this.#addPercentOption({ int: true,  red: true  });
        this.#addPercentOption({ int: false, red: false });
        this.#addPercentOption({ int: false, red: true  });

        // category SCIENTIFIC:
        this.#addScientificOption({ short: false, red: false });
        this.#addScientificOption({ short: true,  red: false });
        this.#addScientificOption({ short: false, red: true  });
        this.#addScientificOption({ short: true,  red: true  });

        // category FRACTION
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: false, denomSize:    1, shortFrac: false }), Math.E, 1);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: false, denomSize:    2, shortFrac: false }), Math.E, 1);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: false, denomSize:    3, shortFrac: false }), Math.E, 1);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize:   10, shortFrac: false }), 2.1,    2);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize:  100, shortFrac: false }), 2.01,   2);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize: 1000, shortFrac: false }), 2.001,  2);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize:    2, shortFrac: false }), 2.5,    3);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize:    4, shortFrac: false }), 2.25,   3);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize:    8, shortFrac: false }), 2.125,  3);
        this.#addFractionOption(numberFormatter.getFractionFormat({ fixedDenom: true,  denomSize:   16, shortFrac: false }), 2.0625, 3);

        // category DATE
        this.#addDateOption(numberFormatter.getSystemDateFormat({ longDate: false }), 1);
        this.#addDateOption(numberFormatter.getSystemDateFormat({ longDate: true }),  1);
        if ((LOCALE_DATA.language === "ja") && this.docApp.isOOXML()) {
            this.#addDateOption(numberFormatter.getParsedFormat(PresetCodeId.GENGO_DATE_SHORT), 1);
            this.#addDateOption(numberFormatter.getParsedFormat(PresetCodeId.GENGO_DATE_LONG),  1);
        }
        if (LOCALE_DATA.cjk) {
            this.#addDateOption(numberFormatter.getParsedFormat(PresetCodeId.CJK_DATE), 1);
        }
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: false, longMonth: false, hideYear: false, shortYear: false }), 2);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: false, longMonth: false, hideYear: false, shortYear: true  }), 2);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: false, longMonth: true,  hideYear: false, shortYear: false }), 2);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: false, longMonth: true,  hideYear: false, shortYear: true  }), 2);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: true,  longMonth: false, hideYear: false, shortYear: false }), 3);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: true,  longMonth: false, hideYear: false, shortYear: true  }), 3);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: true,  longMonth: true,  hideYear: false, shortYear: false }), 3);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: true,  longMonth: true,  hideYear: false, shortYear: true  }), 3);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: false, longMonth: false, hideYear: true,  shortYear: false }), 4);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: false, longMonth: true,  hideYear: true,  shortYear: false }), 4);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: true,  longMonth: false, hideYear: true,  shortYear: false }), 4);
        this.#addDateOption(numberFormatter.getFixedDateFormat({ hideDay: true,  longMonth: true,  hideYear: true,  shortYear: false }), 4);

        // category TIME
        this.#addTimeOption(numberFormatter.getSystemTimeFormat({ shortTime: false }), 1);
        this.#addTimeOption(numberFormatter.getSystemTimeFormat({ shortTime: true  }), 1);
        this.#addTimeOption(numberFormatter.getTimeFormat({ hours24: !LOCALE_DATA.hours24, seconds: true  }), 1);
        this.#addTimeOption(numberFormatter.getTimeFormat({ hours24: !LOCALE_DATA.hours24, seconds: false }), 1);
        if (LOCALE_DATA.cjk) {
            this.#addTimeOption(numberFormatter.getParsedFormat(PresetCodeId.CJK_TIME_LONG),  1);
            this.#addTimeOption(numberFormatter.getParsedFormat(PresetCodeId.CJK_TIME_SHORT), 1);
        }
        this.#addTimeOption(PresetCodeId.TIME_TENTH,   2);
        this.#addTimeOption(PresetCodeId.TIME_ELAPSED, 2);

        // category DATETIME
        this.#addDateTimeOption(numberFormatter.getSystemDateTimeFormat({ longDate: false, shortTime: false }), 1);
        this.#addDateTimeOption(numberFormatter.getSystemDateTimeFormat({ longDate: false, shortTime: true  }), 1);
        this.#addDateTimeOption(numberFormatter.getSystemDateTimeFormat({ longDate: true,  shortTime: false }), 2);
        this.#addDateTimeOption(numberFormatter.getSystemDateTimeFormat({ longDate: true,  shortTime: true  }), 2);
        this.#addDateTimeOption(numberFormatter.getFixedDateTimeFormat({ hideDay: false, longMonth: false, hideYear: false, shortYear: false, shortTime: false }), 3);
        this.#addDateTimeOption(numberFormatter.getFixedDateTimeFormat({ hideDay: false, longMonth: false, hideYear: false, shortYear: false, shortTime: true  }), 3);
        this.#addDateTimeOption(numberFormatter.getFixedDateTimeFormat({ hideDay: false, longMonth: false, hideYear: true,  shortYear: false, shortTime: false }), 4);
        this.#addDateTimeOption(numberFormatter.getFixedDateTimeFormat({ hideDay: false, longMonth: false, hideYear: true,  shortYear: false, shortTime: true  }), 4);

        // category TEXT
        this.#addOption(FormatCategory.TEXT, PresetCodeId.TEXT, "@");

        // additional entries to open the "Custom Format" dialog
        this.#addCustomCurrencyOption(FormatCategory.CURRENCY);
        this.#addCustomCurrencyOption(FormatCategory.ACCOUNTING);
    }
}
