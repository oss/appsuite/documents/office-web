/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { type CompareFn, math, str } from "@/io.ox/office/tk/algorithms";
import { createSpan } from "@/io.ox/office/tk/dom";
import { type DropdownListEntry, DropdownList } from "@/io.ox/office/tk/form";
import type { CollatorWrapper, CurrencyData } from "@/io.ox/office/tk/locale";
import { LOCALE_DATA, localeDataRegistry } from "@/io.ox/office/tk/locale";

import { type CurrencyCodeConfig, yieldFixedCurrencyData } from "@/io.ox/office/editframework/model/formatter/currencycodes";

import "@/io.ox/office/spreadsheet/view/control/currencysymbolpicker.less";

// types ======================================================================

/**
 * Configuration options for `CurrencySymbolPicker` instances.
 */
export interface CurrencySymbolPickerConfig {

    /**
     * A custom key to be stored as `data-key` attribute at the root element of
     * the form control.
     */
    key?: string;

    /**
     * The locale code of the currency to be set to selected state. If omitted
     * or set to an empty string, the passed currency symbol will be used.
     */
    locale?: string;

    /**
     * A literal currency symbol to be selected instead of a currency with
     * locale code.
     */
    symbol?: string;

    /**
     * Whether to select the blind version of the literal currency symbol.
     */
    blind?: boolean;
}

export type CurrencySymbolEntryValue = `${"locale" | "symbol" | "blind"}:${string}`;

/**
 * Configuration options for the list entries of a `CurrencySymbolPicker` form
 * control.
 */
export interface CurrencySymbolEntry extends DropdownListEntry<CurrencySymbolEntryValue> {
    label: string;
    currData: Readonly<CurrencyData>;
    currName: string;
    regionName: string;
    options?: CurrencyCodeConfig;
}

// functions ==================================================================

function *yieldFixedEntries(locale: string, currData: Readonly<CurrencyData>): IterableIterator<CurrencySymbolEntry> {

    // the translated name of the currency
    const currName = LOCALE_DATA.getCurrencyName(currData.iso) ?? "";

    // entry for literal currency symbol
    yield {
        value: `symbol:${currData.iso}`,
        section: "symbol",
        //#. Name of a currency, and its ISO code. Example: "Euro (EUR)"
        //#. %1$s currency name, e.g. "Euro"
        //#. %2$s ISO code of the currency, e.g. "EUR"
        label: gt.pgettext("number-format", "%1$s (%2$s)", currName, currData.iso),
        currData,
        currName,
        regionName: "",
        options: { locale },
    };

    // entry for blind currency symbol
    yield {
        value: `blind:${currData.iso}`,
        section: "symbol",
        //#. Name of a currency, and its ISO code, with additional notice "Hidden symbol",
        //#. as used in number formats to leave space for a symbol without displaying it.
        //#. Example: "Euro (EUR, hidden symbol)"
        //#. %1$s currency name, e.g. "Euro"
        //#. %2$s ISO code of the currency, e.g. "EUR"
        label: gt.pgettext("number-format", "%1$s (%2$s, hidden symbol)", currName, currData.iso),
        //#. Button tooltip: Select a currency number format with invisible currency symbol.
        tooltip: gt.pgettext("number-format", "Leave empty space for currency symbol"),
        currData,
        currName,
        regionName: "",
        options: { locale, symbolModifier: "blind" },
    };
}

// build all list entries with current UI language
function *yieldListEntries(): IterableIterator<CurrencySymbolEntry> {

    // all fixed currency entries on top of the list
    for (const [locale, currData] of yieldFixedCurrencyData()) {
        yield* yieldFixedEntries(locale, currData);
    }

    // all available currency symbols with locale identifier
    const localeEntries = Array.from(localeDataRegistry.yieldCurrencyData(), ([locale, currData]): CurrencySymbolEntry => {
        let currName = LOCALE_DATA.getCurrencyName(currData.iso) ?? "";
        const regionName = LOCALE_DATA.getRegionName(locale) ?? "";
        if (regionName && currName?.endsWith(regionName)) { currName = currName.slice(0, -regionName.length).trim(); }
        //#. Name of a currency, its ISO code, and the region where it is used. Example: "Euro (EUR, Italy)"
        //#. %1$s currency name, e.g. "Euro"
        //#. %2$s ISO code of the currency, e.g. "EUR"
        //#. %3$s region name, e.g. "Italy"
        const label = gt.pgettext("number-format", "%1$s (%2$s, %3$s)", currName, currData.iso, regionName);
        return { value: `locale:${locale}`, section: "locale", label, currData, currName, regionName, options: { locale, symbolModifier: "locale" } };
    });

    // sort entries by currency symbol, then by currency name and region name
    const sortFn: CompareFn<string> = (str1, str2) => LOCALE_DATA.getCollator().compare(str1, str2);
    localeEntries.sort((entry1, entry2) => math.comparePairs(entry1.currData.symbol, entry1.label, entry2.currData.symbol, entry2.label, sortFn));

    yield* localeEntries;
}

/**
 * Returns the value of the list entry to be selected in a new
 * `CurrencySymbolPicker` instance according to a parsed number format.
 */
function resolveEntryValue(config?: CurrencySymbolPickerConfig): CurrencySymbolEntryValue {

    // prefer a list entry with locale identifier
    if (config?.locale) {
        return `locale:${config.locale}`;
    }

    // try to find a builtin currency by symbol or ISO code
    if (config?.symbol) {
        for (const [, currData] of yieldFixedCurrencyData()) {
            if ((config.symbol === currData.symbol) || (config.symbol === currData.iso)) {
                return `${config.blind ? "blind" : "symbol"}:${currData.iso}`;
            }
        }
    }

    // fallback to UI currency
    return `symbol:${LOCALE_DATA.currency.iso}`;
}

// class CurrencySymbolPicker =================================================

/**
 * A dropdown list control with all available currency symbols.
 */
export class CurrencySymbolPicker extends DropdownList<string, CurrencySymbolEntry> {

    // collator used for quick-search
    readonly #searchCollator: CollatorWrapper;
    // <span> element in the anchor button containing the currency symbol
    readonly #symbolSpan: HTMLSpanElement;

    // constructor ------------------------------------------------------------

    constructor(config?: CurrencySymbolPickerConfig) {

        super(yieldListEntries(), resolveEntryValue(config), {
            classes: "currency-symbol-picker",
            key: config?.key,
            renderButton: (elem, entry) => this.#renderButton(elem, entry),
            quickSearch: (query, entry) => this.#quickSearch(query, entry),
        });

        this.#searchCollator = LOCALE_DATA.getCollator({ ignoreAccents: true });
        this.#symbolSpan = createSpan("currency-symbol");

        this.anchorEl.prepend(this.#symbolSpan);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the currency data of the selected list entry.
     */
    getCurrencyData(): Opt<CurrencyData> {
        return this.getSelectedEntry()?.currData;
    }

    // protected methods ------------------------------------------------------

    protected override implRefresh(): Opt<CurrencySymbolEntry> {
        const entry = super.implRefresh();
        if (entry) { this.#updateSymbolSpan(this.#symbolSpan, entry); }
        return entry;
    }

    // private methods --------------------------------------------------------

    #updateSymbolSpan(span: HTMLSpanElement, entry: CurrencySymbolEntry): void {
        const blind = entry.options?.symbolModifier === "blind";
        span.textContent = str.noI18n(entry.currData.symbol);
        span.style.opacity = blind ? "0.5" : "1";
    }

    #renderButton(elem: HTMLElement, entry: CurrencySymbolEntry): void {
        const symbolSpan = createSpan("currency-symbol");
        this.#updateSymbolSpan(symbolSpan, entry);
        elem.prepend(symbolSpan);
    }

    #quickSearch(query: string, entry: CurrencySymbolEntry): boolean {
        const labels = [entry.currData.symbol, entry.currData.iso, entry.currName, entry.regionName];
        return str.splitTokens(query).every(token => labels.some(text => this.#searchCollator.indexOf(text, token) >= 0));
    }
}
