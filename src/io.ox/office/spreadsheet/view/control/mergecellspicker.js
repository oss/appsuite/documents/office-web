/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { set } from "@/io.ox/office/tk/algorithms";
import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import { MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";

// constants ==================================================================

// the label or tooltip for this control
const TOOLTIP_LABEL = gt("Merge or unmerge cells");

// icon names for merge states
const MERGE_ICON = "png:merged-cells-on";
const MERGE_HOR_ICON = "png:merged-cells-horizontal";
const MERGE_VERT_ICON = "png:merged-cells-vertical";
const UNMERGE_ICON = "png:merged-cells-off";

// class MergeCellsPicker =====================================================

/**
 * The selector for merged ranges, as dropdown list with split button.
 *
 * @param {SpreadsheetView} docView
 *  The document view containing this instance.
 */
export class MergeCellsPicker extends AppRadioList {

    constructor(docView) {

        // base constructor
        super(docView, {
            icon: MERGE_ICON,
            tooltip: TOOLTIP_LABEL,
            dropDownVersion: { label: TOOLTIP_LABEL },
            highlight: Boolean,
            splitValue: "toggle",
            updateCaptionMode: "none",
            filter: value => this._visible.has(value)
        });

        // visibility flags of option buttons
        this._visible = new Set/*<MergeMode>*/();

        // initialization -----------------------------------------------------

        // create all drop-down menu items
        this.addOption(MergeMode.MERGE,      { icon: MERGE_ICON,      label: gt("Merge cells") });
        this.addOption(MergeMode.HORIZONTAL, { icon: MERGE_HOR_ICON,  label: gt("Merge cells horizontally") });
        this.addOption(MergeMode.VERTICAL,   { icon: MERGE_VERT_ICON, label: gt("Merge cells vertically") });
        this.addOption(MergeMode.UNMERGE,    { icon: UNMERGE_ICON,    label: gt("Unmerge cells") });

        // update state flags used for visibility of list items (to keep the "visible" callback functions simple and fast)
        this.waitForImportSuccess(this._updateVisibilityFlags);
        this.listenTo(docView, "change:selection merge:cells", this._updateVisibilityFlags);
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the icon according to the merged state of the selection.
     */
    /*protected override*/ implUpdate(state) {
        super.implUpdate(state);
        this.setIcon(this._visible.has(MergeMode.UNMERGE) ? UNMERGE_ICON : MERGE_ICON);
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the visibility flags of the list items after the selection
     * in the active sheet has been changed, or after the merge collection
     * of the active sheet has changed.
     */
    /*private*/ _updateVisibilityFlags() {

        // the collection of merged ranges in the active sheet
        const { mergeCollection } = this.docView;
        // the selected cell ranges
        const ranges = this.docView.selectionEngine.getSelectedRanges();

        // show the "merge" list item, if at least one range consists of more than a cell or a merged range
        set.toggle(this._visible, MergeMode.MERGE, ranges.some(range => {
            const mergedRange = range.single() ? undefined : mergeCollection.getMergedRange(range.a1);
            return !mergedRange?.contains(range);
        }));

        // show the "horizontal" list item, if at least one range does not consist entirely of horizontally merged cells
        set.toggle(this._visible, MergeMode.HORIZONTAL, ranges.some(range => {
            return (range.a1.c < range.a2.c) && (range.a1.r < range.a2.r) && !mergeCollection.isHorizontallyMerged(range);
        }));

        // show the "vertical" list item, if at least one range does not consist entirely of vertically merged cells
        set.toggle(this._visible, MergeMode.VERTICAL, ranges.some(range => {
            return (range.a1.c < range.a2.c) && (range.a1.r < range.a2.r) && !mergeCollection.isVerticallyMerged(range);
        }));

        // show the "unmerge" list item, if at least one range overlaps with a merged range
        set.toggle(this._visible, MergeMode.UNMERGE, mergeCollection.coversAnyMergedRange(ranges));
    }
}
