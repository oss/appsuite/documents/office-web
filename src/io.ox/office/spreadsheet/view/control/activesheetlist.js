/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { fun } from "@/io.ox/office/tk/algorithms";
import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";

import "@/io.ox/office/spreadsheet/view/control/activesheetgroup.less";

// class ActiveSheetList ======================================================

/**
 * The selector for the current active sheet, as radio dropdown list.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 *
 * @param {object} [config]
 *  Configuration options:
 *  - {boolean} [config.showNames=false]
 *    If set to `true`, the current sheet name will be shown as control label.
 *    By default, a menu icon without label will be shown.
 */
export class ActiveSheetList extends AppRadioList {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            classes: "active-sheet-group tab-button-style",
            icon: "bi:list",
            tooltip: gt("Select sheet"),
            caret: config?.showNames,
            highlight: fun.true, // always in selected state (also without initialized menu)
            updateCaptionMode: "none",
            showNames: config?.showNames
        });

        // register handler early for fast preview during import
        this.menu.requestMenuRepaintOnEvent(this.docApp, "docs:preview:activesheet");
        // rebuild the list of available sheets after it has been changed
        this.menu.requestMenuRepaintOnEvent(this.docModel, ["transform:sheets", "rename:sheet", "change:sheetattrs"]);

        // TODO: generic way to request menu repaint, and caption update depending on menu entry
        this.listenTo(this.docApp, "docs:preview:activesheet", () => this.refresh());
        this.listenTo(this.docModel, "transform:sheets rename:sheet change:sheetattrs", () => this.refresh());
    }

    // public methods ---------------------------------------------------------

    setWidth(width) {
        this.$el.css("width", width);
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(index) {
        super.implUpdate(index);
        if (this.config.showNames) {
            const sheetModel = this.docModel.getSheetModel(index);
            if (sheetModel) { // may be `null` in early GUI initialization
                this.setIcon(sheetModel.isLocked() ? "bi:lock-fill" : null, { classes: "small-icon" });
                this.setLabel(_.noI18n(sheetModel.getName()), { style: { whiteSpace: "pre" } });
            }
        }
    }

    /**
     * Recreates all sheet entries in the dropdown menu.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // remove all old items from the dropdown menu
        this.clearOptions();

        // create menu items for all visible sheets
        for (const sheetModel of this.docModel.yieldSheetModels({ supported: true, visible: true })) {

            const sheetIndex = sheetModel.getIndex();
            const sheetLabel = _.noI18n(sheetModel.getName());

            this.addOption(sheetIndex, {
                icon: sheetModel.isLocked() ? "bi:lock-fill" : null,
                iconClasses: "small-icon",
                label: sheetLabel,
                labelStyle: { whiteSpace: "pre" },
                tooltip: sheetLabel
            });
        }
    }
}
