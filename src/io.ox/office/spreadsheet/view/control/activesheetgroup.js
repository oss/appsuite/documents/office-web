/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { math, ary, jpromise } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE, createDiv, createSpan, createIcon } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { getNodePositionInPage } from "@/io.ox/office/tk/utils";
import {
    OPTION_BUTTON_SELECTOR, createButtonNode, enableNodes,
    getButtonValue, getCaptionText, setToolTip, showNodes
} from "@/io.ox/office/tk/forms";
import { debounceMethod } from "@/io.ox/office/tk/objects";

import { AppRadioGroup } from "@/io.ox/office/baseframework/view/basecontrols";
import { SheetContextMenu } from "@/io.ox/office/spreadsheet/view/popup/sheetcontextmenu";

import "@/io.ox/office/spreadsheet/view/control/activesheetgroup.less";

// class ActiveSheetGroup =====================================================

/**
 * The selector for the current active sheet, as radio group (all sheet tabs
 * are visible at the same time).
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class ActiveSheetGroup extends AppRadioGroup {

    constructor(docView) {

        // base constructor
        super(docView, { classes: "active-sheet-group tab-button-style", role: "tablist" });

        // the scroll button nodes
        this._$prevButton = createButtonNode({ classes: "scroll-button", icon: "bi:chevron-left" });
        this._$nextButton = createButtonNode({ classes: "scroll-button", icon: "bi:chevron-right" });

        // index of the first and last sheet tab button currently visible
        this._firstIndex = 0;
        this._lastIndex = 0;
        this._oldIndex = -1;
        this._scrollable = false;

        // set tool tips for scroll buttons
        setToolTip(this._$prevButton, gt("Scroll to the previous sheet"));
        setToolTip(this._$nextButton, gt("Scroll to the next sheet"));

        // register handler early for fast preview during import
        this.listenTo(docView.docApp, "docs:preview:activesheet", this._fillList);

        // initialization after import
        this.waitForImportSuccess(() => {
            // build the list of available sheets once after import
            this._fillList();
            // rebuild the list of available sheets after it has been changed
            this.listenTo(this.docModel, "transform:sheets rename:sheet change:sheetattrs ", this._fillList);
            this.listenTo(this.docModel, "insert:threadedcomment delete:threadedcomment:end new:threadedcomment:thread", this._fillList);
        });

        // create the context menu
        this.member(new SheetContextMenu(docView, this.$el));

        // add scroll buttons to the group node
        this.$el.append(this._$prevButton, this._$nextButton);

        // double click shows rename dialog (TODO: in-place editing)
        this.listenTo(this.$el, "dblclick", this._doubleClickHandler, { delegated: OPTION_BUTTON_SELECTOR });

        // initialize tracking observer for scroll buttons
        const scrollObserver = this.member(new TrackingObserver({
            start:   record => this._scrollTo(record.id),
            repeat:  "start",
            finally: record => this.triggerCancel({ sourceEvent: record.inputEvent }) // focus back to the application
        }));

        // start to observe the scroll buttons (suppress event propagation to not interfere with sheet move tracking)
        const TRACKING_CONFIG = { stopPropagation: true, repeatDelay: 700, repeatInterval: 100 };
        scrollObserver.observe(this._$prevButton[0], { ...TRACKING_CONFIG, id: "prev" });
        scrollObserver.observe(this._$nextButton[0], { ...TRACKING_CONFIG, id: "next" });

        // drag sheet buttons to a new position (not on touch devices)
        if (!TOUCH_DEVICE) {

            // initialize tracking observer for sheet buttons
            const dndObserver = this.member(new TrackingObserver(this._createDndTrackingCallbacks()));

            // start observing the sheet buttons
            dndObserver.observe(this.el, {
                cursor: "ew-resize",
                scrollDirection: "horizontal",
                scrollInterval: 500,
                scrollAccelBand: [-30, 30] // start scrolling before reaching the container borders
            });

            // cancel drag&drop when switching to readonly mode
            this.listenTo(this.docApp, "docs:editmode:leave", () => dndObserver.cancel());
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets this control to automatic width, shows all sheet tab buttons, and
     * hides the scroll buttons.
     */
    setFullWidth() {
        this.$el.css("width", "");
        this._showAllSheetButtons();
    }

    /**
     * Sets this control to a fixed width. Performs further initialization
     * according to the available space: Shows/hides the scroll buttons, and
     * draws the available sheet tabs according to the last cached scroll
     * position.
     *
     * @param {number} width
     *  The new outer width of this control, in pixels.
     */
    setWidth(width) {
        this.$el.css("width", width);
        this._renderSheetButtons();
    }

    // protected methods ------------------------------------------------------

    /**
     * Called every time the value of the control (the index of the active
     * sheet) has been changed.
     */
    /*protected override*/ implUpdate(newIndex) {
        super.implUpdate(newIndex);

        // scroll backwards, if the active sheet is located before the first visible;
        // or scroll forwards, if the active sheet is located after the last visible
        if ((this._oldIndex !== newIndex) && ((newIndex < this._firstIndex) || (newIndex > this._lastIndex))) {
            this._firstIndex = newIndex;
            this._renderSheetButtons();
        }

        this._oldIndex = newIndex;
    }

    // private methods --------------------------------------------------------

    /**
     * Shows all sheet buttons and removes explicit width, and hides the
     * scroll buttons.
     */
    /*private*/ _showAllSheetButtons() {
        const $buttons = this.getOptions();
        showNodes($buttons, true);
        $buttons.css({ width: "" });
        showNodes(this._$prevButton, false);
        showNodes(this._$nextButton, false);
        this._$nextButton.css({ marginLeft: "" });
        this._scrollable = false;
    }

    /**
     * Shows the sheet buttons starting at the start sheet index currently
     * cached.
     */
    /*private*/ _renderSheetButtons() {

        // the inner width of the group (space available for buttons)
        const groupWidth = this.$el.width();

        // all sheet tab buttons; do nothing if there are no sheets available (yet)
        const $sheetButtons = this.getOptions();
        if ($sheetButtons.length === 0) { return; }

        // initialize all sheet buttons, hide scroll buttons
        this._showAllSheetButtons();

        // get total width of all buttons (bug 35869: round down, browsers may report slightly oversized width due to text nodes)
        const $firstSheetButton = $sheetButtons.first();
        const $lastSheetButton = $sheetButtons.last();
        const leadingOffset = $firstSheetButton.offset().left;
        const buttonsWidth = Math.floor($lastSheetButton.offset().left + $lastSheetButton.outerWidth() - leadingOffset);
        if (buttonsWidth <= groupWidth) { return; }

        // collect position information of all buttons (performance: once for all buttons, before manipulating DOM again)
        const buttonPositions = $sheetButtons.get().map(buttonNode => {
            const $buttonNode = $(buttonNode);
            const buttonOffset = $buttonNode.offset().left - leadingOffset;
            return {
                offset: buttonOffset,
                widthFromStart: buttonOffset + $buttonNode.outerWidth(), // width from right border to end of button
                widthToEnd: buttonsWidth - buttonOffset // width from start of button to right border
            };
        });

        // total width of all buttons, including scroll buttons
        let totalWidth = 0;
        // remaining width available in the group node for sheet buttons
        let remainingWidth = 0;
        // maximum index of button that can be shown at the left border (without gap to right border)
        let maxIndex = 0;

        // do not show scroll buttons, if there is only one visible sheet
        if ($sheetButtons.length > 1) {

            // show scroll buttons
            showNodes(this._$prevButton, true);
            showNodes(this._$nextButton, true);
            this._scrollable = true;

            // get total width of all buttons, including scroll buttons, and inner remaining width
            totalWidth = this._$nextButton.offset().left + this._$nextButton.width() - this._$prevButton.offset().left;
            remainingWidth = groupWidth - (totalWidth - buttonsWidth);

            // hide leading sheet tabs
            maxIndex = ary.fastFindFirstIndex(buttonPositions, position => position.widthToEnd <= remainingWidth);
            this._firstIndex = math.clamp(this._firstIndex, 0, maxIndex);
            showNodes($sheetButtons.slice(0, this._firstIndex), false);

            // adjust button positions relative to first visible button
            const relOffset = buttonPositions[this._firstIndex].offset;
            buttonPositions.forEach(position => {
                position.offset -= relOffset;
                position.widthFromStart -= relOffset;
            });

            // hide trailing sheet tabs
            maxIndex = ary.fastFindFirstIndex(buttonPositions, position => position.widthFromStart > remainingWidth);
            this._lastIndex = Math.max(this._firstIndex, maxIndex - 1);
            showNodes($sheetButtons.slice(this._lastIndex + 1), false);

            // enable/disable scroll buttons
            enableNodes(this._$prevButton, this._firstIndex > 0);
            enableNodes(this._$nextButton, this._lastIndex + 1 < $sheetButtons.length);

        } else {
            // single sheet: initialize indexes
            this._firstIndex = this._lastIndex = 0;
            totalWidth = buttonsWidth;
            remainingWidth = groupWidth;
        }

        // shrink single visible sheet button, if still oversized
        let $sheetButton = $sheetButtons.eq(this._firstIndex);
        if ((this._firstIndex === this._lastIndex) && (remainingWidth < $sheetButton.width())) {
            $sheetButton.css({ width: remainingWidth });
            return;
        }

        // the remaining width between last visible button and right scroll button
        remainingWidth -= buttonPositions[this._lastIndex].widthFromStart;

        // add the first hidden sheet button after the last visible, if there is some space left
        if ((this._lastIndex + 1 < buttonPositions.length) && (remainingWidth >= 45)) {
            $sheetButton = $sheetButtons.eq(this._lastIndex + 1);
            showNodes($sheetButton, true);
            $sheetButton.css({ width: remainingWidth });
            remainingWidth = 0;
        }

        // fill the gap between last visible button and right scroll button to keep it rightmost
        this._$nextButton.css({ marginLeft: remainingWidth });
    }

    /**
     * Handles double click events on sheet tab buttons and initiates
     * renaming the sheet.
     */
    /*private*/ _doubleClickHandler() {
        jpromise.floating(this.docView.executeControllerItem("sheet/rename/dialog"));
    }

    /**
     * Scrolls to the left/right to make at least one preceding/following sheet
     * tab button visible.
     *
     * @param {"prev"|"next"} dir
     *  Specifies the scrolling direction.
     */
    /*private*/ _scrollTo(dir) {

        // do not try to scroll, if all buttons are visible
        if (!this._scrollable) { return; }

        // index of the old last visible sheet
        const oldLastIndex = this._lastIndex;

        // move to previous/next sheets until the index of the last sheet has changed
        switch (dir) {
            case "prev": {
                while ((this._firstIndex > 0) && (oldLastIndex === this._lastIndex)) {
                    this._firstIndex -= 1;
                    this._renderSheetButtons();
                }
                break;
            }
            case "next": {
                const $sheetButtons = this.getOptions();
                while ((this._lastIndex >= 0) && (this._lastIndex + 1 < $sheetButtons.length) && (oldLastIndex === this._lastIndex)) {
                    this._firstIndex += 1;
                    this._renderSheetButtons();
                }
                break;
            }
        }
    }

    /**
     * Creates the tracking callbacks for drag&drop tracking of a sheet tab
     * button.
     */
    /*private*/ _createDndTrackingCallbacks() {

        // tracker node shown while dragging a sheet button
        const trackerEl = createDiv({ id: "io-ox-office-spreadsheet-sheet-dnd-tracker", classes: "io-ox-office-main" });
        trackerEl.append(createDiv("caret-icon"));

        // helper node inside the tracker node containing the sheet name
        const sheetNameEl = trackerEl.appendChild(createSpan("sheet-name"));

        // the sheet tab button currently dragged
        let $sheetButton = $();
        // index of the sheet tab button is currently dragged, and the target button index
        let startIndex = -1;
        let targetIndex = -1;

        // returns whether the dragged sheet tab button will currently drop to a new position
        // (dragging directly behind the start button will not move the sheet)
        const isValidIndex = () => (targetIndex < startIndex) || (startIndex + 1 < targetIndex);

        const prepareTracking = event => {
            if (!this.docApp.isEditable()) { return false; }
            $sheetButton = $(event.target).closest(OPTION_BUTTON_SELECTOR);
            return $sheetButton.length > 0;
        };

        const startTracking = () => {

            // remember button index as long as tracking lasts
            startIndex = targetIndex = this.getOptions().index($sheetButton);

            // prepare the tracking node shown at the current insertion position
            sheetNameEl.textContent = getCaptionText($sheetButton);
            trackerEl.classList.add("hidden");
            document.body.append(trackerEl);
            $sheetButton.addClass("tracking-active");
        };

        const moveTracking = record => {

            // all sheet tab buttons
            const $sheetButtons = this.getOptions();
            // all visible sheet tab buttons
            const $visibleButtons = this.getOptions({ visible: true });
            // absolute position and size of all visible sheet buttons
            const buttonRects = $visibleButtons.get().map(getNodePositionInPage);
            // search rectangle of hovered sheet tab button in array of visible button rectangles
            const rectIndex = ary.fastFindLastIndex(buttonRects, rect => (rect.left + rect.width / 2) <= record.point.x) + 1;

            // calculate new insertion index (add number of leading hidden buttons)
            targetIndex = $sheetButtons.index($visibleButtons[0]) + rectIndex;

            // hide tracking icon on invalid position (next to start button)
            if (!isValidIndex()) {
                trackerEl.classList.add("hidden");
                return;
            }

            // whether to show the tracking position after the last visible button
            const trailing = rectIndex >= buttonRects.length;
            // position of the sheet tab button following the tracking position
            const buttonRect = trailing ? _.last(buttonRects) : buttonRects[rectIndex];
            // exact insertion position (center of the tracker node), in pixels
            const offsetX = buttonRect.left + (trailing ? buttonRect.width : 0);

            // update position of the tracking icon
            trackerEl.classList.remove("hidden");
            trackerEl.style.left = `${Math.round(offsetX - trackerEl.offsetWidth / 2)}px`;
            trackerEl.style.top = `${this.$el.offset().top - trackerEl.offsetHeight + 2}px`;
        };

        const scrollTracking = record => {

            // scroll buttons into the specified direction if possible
            if (record.scroll.x < 0) {
                this._scrollTo("prev");
            } else if (record.scroll.x > 0) {
                this._scrollTo("next");
            }

            // update position of the tracker node
            moveTracking(record);
        };

        const endTracking = record => {

            // leave text edit mode before manipulating the sheet order
            const promise = this.docView.leaveTextEditMode().then(() => {

                // source sheet index for document operation
                const fromSheet = getButtonValue($sheetButton);

                // move sheet only if its position will really change
                if (isValidIndex()) {

                    // adjust drop position, if target position follows source position
                    if (startIndex < targetIndex) { targetIndex -= 1; }

                    // the sheet tab button nodes
                    const $sheetButtons = this.getOptions();
                    // target sheet index for document operation
                    const toSheet = getButtonValue($sheetButtons[targetIndex]);

                    // change sheet order in the document
                    if (fromSheet !== toSheet) {
                        this.docView.executeControllerItem("document/movesheet", { from: fromSheet, to: toSheet });
                    }

                } else if (record.moved) {
                    // activate the sheet that has been tracked but will not be moved
                    // bug 50653: only when tracking point has been moved at all
                    this.triggerCommit(fromSheet);
                }
            });

            // remove tracking icon, focus back to document
            promise.always(cancelTracking);
        };

        const cancelTracking = () => {
            trackerEl.remove();
            $sheetButton?.removeClass("tracking-active");
            $sheetButton = null;
            this.docView.grabFocus();
        };

        return {
            prepare: prepareTracking,
            start:   startTracking,
            move:    moveTracking,
            scroll:  scrollTracking,
            end:     endTracking,
            cancel:  cancelTracking
        };
    }

    /**
     * Recreates all sheet tabs in this radio group.
     */
    @debounceMethod({ delay: "microtask" })
    /*private*/ _fillList() {

        // remove all old option buttons
        this.clearOptions();

        // create buttons for all visible sheets
        for (const sheetModel of this.docModel.yieldSheetModels({ supported: true, visible: true })) {

            const sheetIndex = sheetModel.getIndex();
            const sheetLabel = _.noI18n(sheetModel.getName());

            // create the sheet tab button
            const $button = this.addOption(sheetIndex, {
                icon: sheetModel.isLocked() ? "bi:lock-fill" : null,
                iconClasses: "small-icon",
                label: sheetLabel,
                tooltip: sheetLabel,
                // class "contextmenu" needed to bypass CM blocker in core/boot/fixes.js
                classes: "contextmenu",
                role: "tab"
            });

            // initialize the comment counter badge
            const threadCount = sheetModel.commentCollection.getThreadCount();
            if (threadCount > 0) {

                const $countBadge = $(`<div class="comment-thread-count" data-thread-count="${threadCount}">`);
                $button.prepend($countBadge.append(createIcon("svg:comment", 23)));

                $countBadge.on("click", event => {
                    if (sheetModel.isActive()) {
                        this.docView.commentsPane.toggle();
                        event.stopPropagation();
                    }
                });

                $countBadge.on("contextmenu", false);
            }
        }

        // move trailing scroll button to the end
        this.$el.append(this._$nextButton);

        // explicitly notify listeners, needed for reliable layout
        this.trigger("change:sheets");
    }
}
