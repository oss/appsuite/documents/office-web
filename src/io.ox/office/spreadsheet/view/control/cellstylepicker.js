/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { math } from "@/io.ox/office/tk/algorithms";
import { createDiv } from "@/io.ox/office/tk/dom";
import { getCaptionNode } from "@/io.ox/office/tk/forms";

import { getCssTextDecoration } from "@/io.ox/office/editframework/utils/attributeutils";
import { StyleSheetPicker } from "@/io.ox/office/editframework/view/control/stylesheetpicker";
import { getTextOrientation } from "@/io.ox/office/spreadsheet/utils/sheetutils";

// class CellStylePicker ======================================================

/**
 * A dropdown menu for cell style sheets.
 *
 * @param {SpreadsheetView} docView
 *  The document view containing this instance.
 */
export class CellStylePicker extends StyleSheetPicker {

    constructor(docView) {

        // base constructor
        super(docView, "cell", {
            width: "12em",
            icon: "png:table-style",
            tooltip: /*#. tool tip: predefined styles for spreadsheet cells */ gt("Cell style"),
            gridColumns: 6,
            resourceKey: "cellstylenames"
        });

        // create the sections for the stylesheet buttons
        this.addSection("markup");
        this.addSection("headings");
        this.addSection("themes");
        this.addSection("hidden");

        // add visible formatting of the cell styles to the option buttons
        this.listenTo(this.menu, "list:option:add", this._addOptionHandler);
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the passed option button in the dropdown menu.
     */
    /*private*/ _addOptionHandler($button, styleId) {

        // the merged attributes of the passed cell style sheet
        const attrSet = this.styleCollection.getStyleAttributeSet(styleId);
        // the character attributes
        const charAttrs = attrSet.character;
        // the cell attributes
        const cellAttrs = attrSet.cell;
        // the caption element to be formatted
        const $caption = getCaptionNode($button);
        // text orientation settings according to localized style name
        const textOrientation = getTextOrientation(_.noI18n.fix($caption.text()), cellAttrs.alignHor);

        // an additional background fill node as overlay (to prevent different line height in caption)
        const fillNode = createDiv("page-effect abs");
        const fillColor = this.docModel.getCssColor(cellAttrs.fillColor, "fill");
        if (fillColor !== "transparent") { fillNode.style.background = fillColor; }

        // an additional border node as overlay (to prevent different line height in caption)
        // TODO: use canvas for complex border styles
        const borderNode = createDiv("border-effect abs");
        borderNode.style.borderTop = this.docModel.getCssBorder(cellAttrs.borderTop, null, { preview: true });
        borderNode.style.borderBottom = this.docModel.getCssBorder(cellAttrs.borderBottom, null, { preview: true });
        borderNode.style.borderLeft = this.docModel.getCssBorder(cellAttrs.borderLeft, null, { preview: true });
        borderNode.style.borderRight = this.docModel.getCssBorder(cellAttrs.borderRight, null, { preview: true });

        $button.prepend(fillNode, borderNode);

        // add relevant character and cell formatting
        $caption.css({
            fontFamily: this.docModel.getCssFontFamily(charAttrs.fontName),
            fontSize: math.clamp(math.roundp(10 + (charAttrs.fontSize - 10) / 1.5, 0.1), 6, 22) + "pt",
            fontWeight: charAttrs.bold ? "bold" : "normal",
            fontStyle: charAttrs.italic ? "italic" : "normal",
            textDecoration: getCssTextDecoration(charAttrs),
            color: this.docModel.getCssTextColor(charAttrs.color, [cellAttrs.fillColor]),
            textAlign: textOrientation.cssTextAlign
        });
    }
}
