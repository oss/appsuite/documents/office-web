/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { map } from "@/io.ox/office/tk/algorithms";

import { detachChildren, createElement, createDiv, createSpan } from "@/io.ox/office/tk/dom";
import type { SelectListEntry, DropdownListEntry } from "@/io.ox/office/tk/form";
import { SelectList, DropdownList } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import type { FuncCategorySpec } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import type { FunctionHelp, FunctionResource } from "@/io.ox/office/spreadsheet/model/formula/resource/functionresource";
import { type SpreadsheetDialogConfig, getCategoryLabel } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

import "@/io.ox/office/spreadsheet/view/dialog/functiondialog.less";

// types ======================================================================

// type of category keys with special category "all"
type CategoryKey = FuncCategorySpec | typeof CATEGORY_ALL;

// configuration entries for the category dropdown list (make "label" required)
type CategoryEntry = DropdownListEntry<CategoryKey> & { label: string };

// configuration entries for the function <select> list
interface FunctionEntry extends SelectListEntry<string> {
    resource: FunctionResource;
}

// constants ==================================================================

// special drop-down list entry value for "show all functions"
const CATEGORY_ALL = "__all__";

//#. "Insert Function" dialog: label for a drop-down menu for categories
const PICK_CATEGORY_LABEL = gt.pgettext("function-dialog", "Pick a category");

//#. "Insert Function" dialog: list entry to show functions from all categories
const ALL_FUNCTIONS_LABEL = gt.pgettext("function-dialog", "All");

//#. "Insert Function" dialog: label for a list box containing function names
const PICK_FUNCTION_LABEL = gt.pgettext("function-dialog", "Pick a function");

//#. "Insert Function" dialog: subtitle for the short syntax description of a function
const SYNTAX_LABEL = gt.pgettext("function-dialog", "Syntax");

//#. "Insert Function" dialog: subtitle for the parameter list of a function
const PARAMETERS_LABEL = gt.pgettext("function-dialog", "Parameters");

//#. "Insert Function" dialog: name of an optional parameter in the description of a function
//#. %1$s is the name of the parameter to be marked as optional
const OPTIONAL_PARAM_NAME = gt.pgettext("function-dialog", "%1$s (optional)");

// minimum width of wide mode
const MIN_WIDE_WIDTH = 600;

// maximum number of entries in the selection list (wide screens)
const MAX_SELECTLIST_WIDE_SIZE = 15;

// maximum number of entries in the selection list (narrow screens)
const MAX_SELECTLIST_NARROW_SIZE = 8;

// class FunctionDialog =======================================================

/**
 * A modal dialog showing an overview of all function categories, all functions
 * and their appropriate description, function syntax and parameters. The user
 * can select a function from one of the categories.
 *
 * @param docView
 *  The spreadsheet view instance containing this dialog.
 *
 * @param [funcName]
 *  An optional translated name of a function to be selected initially in the
 *  dialog.
 */
export class FunctionDialog extends BaseDialog<string, SpreadsheetDialogConfig> {

    // document access
    readonly docModel: SpreadsheetModel;
    readonly docView: SpreadsheetView;

    // localized formula resource data
    readonly #formulaResource: FormulaResource;

    // the descriptors of all supported functions
    readonly #functionMap: FormulaResource["functionMap"];

    // the container for category and function pickers
    readonly #funcBox: HTMLElement;

    // the list control with the function names
    readonly #funcList;

    // the root node for the function description article
    readonly #descBox: HTMLElement;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView, funcName?: string) {

        // base constructor
        super({
            id: "io-ox-office-spreadsheet-function-dialog",
            //#. "Insert Function" dialog title
            title: gt.pgettext("function-dialog", "Insert function"),
            width: 750,
            //#. "Insert Function" dialog: label for OK button
            okLabel: gt.pgettext("function-dialog", "Insert"),
            movable: true,
            smallMobileFooter: true,
            focus: ".function-picker select",
            // close automatically, if active sheet or cell will be deleted or hidden by another client
            autoCloseScope: "activeCell"
        });

        // initialize properties
        this.docModel = docView.docModel;
        this.docView = docView;
        this.#formulaResource = this.docModel.formulaResource;
        this.#functionMap = this.#formulaResource.functionMap;

        // collect all information for functions to be shown
        const categFuncMapping = new Map<FuncCategorySpec, Set<string>>();
        const functionEntries: FunctionEntry[] = [];
        this.#functionMap.forEach(resource => {
            if (!resource.hidden) {
                const funcKey = resource.key;
                resource.categories.forEach(categKey => map.upsert(categFuncMapping, categKey, () => new Set()).add(funcKey));
                functionEntries.push({ value: funcKey, label: resource.localName, resource });
            }
        });

        // create the entries for category dropdown list; sort categories by translated labels (TODO: define a fixed
        // category order?), and add the special category "all" on top of the list
        const categoryEntries: CategoryEntry[] = Array.from(categFuncMapping.keys(), categKey => {
            return { value: categKey, section: "category", label: getCategoryLabel(categKey) };
        });
        LOCALE_DATA.getCollator().sortByProp(categoryEntries, "label");
        categoryEntries.unshift({ value: CATEGORY_ALL, section: "all", label: ALL_FUNCTIONS_LABEL });

        // sort function entries for the <select> list
        LOCALE_DATA.getCollator().sortByProp(functionEntries, "label");

        // flex container for the category and function pickers
        this.#funcBox = this.bodyNode.appendChild(createDiv("functions-box"));

        // create the category dropdown control with label
        const categoryList = new DropdownList<CategoryKey>(categoryEntries, CATEGORY_ALL, { type: "link", classes: "category-picker", label: _.noI18n(PICK_CATEGORY_LABEL + ":") });
        this.renderControl(this.#funcBox, categoryList);

        // create the function selection list and its label
        const funcRes = funcName && this.#functionMap.get(funcName);
        const funcKey = (funcRes && !funcRes.hidden) ? funcRes.key : functionEntries[0].value;
        this.#funcList = new SelectList(functionEntries, funcKey, { classes: "function-picker noI18n", size: MAX_SELECTLIST_WIDE_SIZE });
        this.renderControl(this.#funcBox, this.#funcList, { label: PICK_FUNCTION_LABEL });

        // container node for function description text
        this.#descBox = this.bodyNode.appendChild(createDiv("panel description-box"));

        // activate the selected function category (filter visible entries in the function list)
        categoryList.onChange(categKey => {
            const funcDict = (!categKey || (categKey === CATEGORY_ALL)) ? undefined : categFuncMapping.get(categKey);
            this.#funcList.filterEntries("category", funcDict ? (entry => funcDict.has(entry.value)) : undefined);
            this.#funcList.selectEntryAt(0);
        }, { invokeNow: true });

        // always focus the function select list when leaving the category dropdown menu (also with ESC)
        this.listenTo(categoryList, "menu:hide", this.grabFocus);

        // update function description for selected function
        this.#funcList.onChange(() => this.#updateFunctionDescription(), { invokeNow: true });

        // update layout of container elements
        this.on("show", () => {
            this.listenTo(window, "resize", this.#windowResizeHandler);
            this.#windowResizeHandler();
        });

        // bug 53227: Safari does NOT scroll automatically to the selected option
        if (_.browser.Safari) {
            this.on("show", () => this.#funcList.scrollToSelected());
        }

        // return the dialog result
        this.setOkHandler(() => this.#funcList.value);
    }

    // private methods --------------------------------------------------------

    /**
     * Sets the height of the description box to the height of the function
     * selection list.
     */
    #updateDescriptionHeight(): void {
        this.#descBox.style.height = `${this.#funcBox.offsetHeight}px`;
    }

    /**
     * Updates the layout of the dialog elements according to the size of the
     * browser window and dialog root elements.
     */
    #windowResizeHandler(): void {

        // narrow mode if the width of the dialog is small
        const narrowMode = this.bodyNode.clientWidth < MIN_WIDE_WIDTH;
        this.bodyNode.classList.toggle("narrow", narrowMode);

        // expand the function box
        this.#funcList.size = narrowMode ? MAX_SELECTLIST_NARROW_SIZE : MAX_SELECTLIST_WIDE_SIZE;
        this.#updateDescriptionHeight();

        // shrink function and description boxes if space is not sufficient
        const overflowHeight = this.#funcBox.scrollHeight - this.#funcBox.clientHeight;
        if (overflowHeight > 0) {
            this.#funcList.fitSizeInto(this.#funcList.selectEl.offsetHeight - overflowHeight);
            this.#updateDescriptionHeight();
        }
    }

    /**
     * Returns the fully formatted signature string for the passed function
     * help descriptor.
     *
     * @param functionHelp
     *  The function help descriptor.
     *
     * @returns
     *  The formatted signature for the specified function.
     */
    #getFunctionSignature(functionHelp: FunctionHelp): string {

        const SEP = this.#formulaResource.getSeparator(true) + " ";
        const cycleCount = functionHelp.cycleCount;
        const cycleLength = functionHelp.cycleLength;
        const paramCount = functionHelp.params?.length ?? 0;

        // start with the function name
        let signature = `${functionHelp.name}(`;

        // process all function parameters
        for (let i = 0; i < paramCount; i += 1) {

            let paramHelp = functionHelp.params![i];

            // add a single explicit parameter
            if ((cycleCount === 0) || (functionHelp.repeatStart < 0) || (i < functionHelp.repeatStart)) {
                const paramName = `${(i > 0) ? SEP : ""}${paramHelp.name}`;
                signature += paramHelp.optional ? `[${paramName}]` : paramName;
                continue;
            }

            // collect all repeating parameters
            const cycleParams: string[] = [];
            for (const last = i + cycleLength; i < last; i += 1) {
                paramHelp = functionHelp.params![i];
                cycleParams.push(paramHelp.name);
                signature += `${(i > 0) ? SEP : ""}${paramHelp.name}1`;
            }

            // add repeating parameters in brackets with ellipsis
            signature += "[...";
            for (let n = 0; n < cycleLength; n += 1) {
                signature += `${SEP}${cycleParams[n]}${cycleCount}`;
            }
            signature += "]";

            // exit loop early (all repeating parameters already added above)
            break;
        }

        signature += ")";
        return signature;
    }

    /**
     * Updates the description, syntax, and parameters for the selected
     * function.
     */
    #updateFunctionDescription(): void {

        // clean the function description node
        detachChildren(this.#descBox);

        // fetch the function help data for the specified function
        const functionHelp = this.#formulaResource.getFunctionHelp(this.#funcList.value);
        if (!functionHelp) { return; }

        // create the general function description (name, description, syntax)
        this.#descBox.append(
            createElement("h1", { label: _.noI18n(functionHelp.name) }),
            createElement("p", { label: _.noI18n(functionHelp.description) }),
            createElement("h2", { label: SYNTAX_LABEL }),
            createElement("p", { label: _.noI18n(this.#getFunctionSignature(functionHelp)) })
        );

        // nothing more to do for parameterless functions
        if (!functionHelp.params?.length) { return; }

        // create detailed parameter description
        this.#descBox.appendChild(createElement("h2", { label: PARAMETERS_LABEL }));
        const paramListNode = this.#descBox.appendChild(createElement("ul", "desc-params"));
        for (const param of functionHelp.params) {
            const itemNode = paramListNode.appendChild(createElement("li"));
            const paramName = param.optional ? _.noI18n.format(OPTIONAL_PARAM_NAME, param.name) : _.noI18n(param.name);
            itemNode.append(
                createSpan({ classes: "param-name", label: paramName }),
                createSpan({ label: _.noI18n(`\u2014 ${param.description}`) })
            );
        }
    }
}
