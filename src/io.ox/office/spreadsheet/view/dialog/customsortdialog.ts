/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { ary } from "@/io.ox/office/tk/algorithms";
import { detachChildren, setElementLabel, createText, createElement, createDiv, createIcon, createFieldSet, createDeleteButton } from "@/io.ox/office/tk/dom";
import { type DropdownListEntry, CheckBox, DropdownList } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import type { SortRule } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import { type SpreadsheetDialogConfig, SORT_ASC_BUTTON_OPTIONS, SORT_DESC_BUTTON_OPTIONS, getColRowLabel } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

import "@/io.ox/office/spreadsheet/view/dialog/customsortdialog.less";

// types ======================================================================

export interface CustomSortDialogResult {
    sortVert: boolean;
    hasHeader: boolean;
    sortRules: readonly SortRule[];

}

// array element type for all data needed for a sorting rule table row
interface SortRuleEntry {
    sortByNode: HTMLDivElement;
    sortByList: DropdownList<number>;
    sortOrderNode: HTMLDivElement;
    sortOrderList: DropdownList<boolean>;
}

// constants ==================================================================

// action key for the "Add sort criteria" button
const ADD_ACTION = "add";

// error messages
const ERR_MISSING_LABEL = gt.pgettext("sort", "All sort criteria must have a column or row specified.");
const ERR_MULTI_LABEL = gt.pgettext("sort", "Some columns or rows are being sorted more than once.");

//#. header label for sorting options (has headers, vertical or horizontal)
const SORT_OPTIONS_LABEL = gt.pgettext("sort", "Options");
//#. sorting: selected range has headers which shouldn't be sorted
const HAS_HEADERS_LABEL = gt.pgettext("sort", "Selection has headers");
//#. add new sorting rule to list
const ADD_LABEL = gt.pgettext("sort", "Add sort criteria");
//#. label for sorting direction selector ("top to bottom" or "left to right")
const DIRECTION_LABEL = gt.pgettext("sort", "Direction");
//#. sorting: sort range by (for example "column B" or "row 4")
const SORT_BY_LABEL = gt.pgettext("sort", "Sort by");
//#. sorting: the sort order (for example "ascending")
const SORT_ORDER_LABEL = gt.pgettext("sort", "Order");
//#. sorting: the sorting order is not yet defined
const UNDEF_LABEL = gt.pgettext("sort", "undefined");

// options for the "sorting direction" dropdown menu
const SORT_DIRECTION_ENTRIES: Array<DropdownListEntry<boolean>> = [
    { value: true,  label: /*#. sorting direction for spreadsheet data */ gt.pgettext("sort", "Top to bottom") },
    { value: false, label: /*#. sorting direction for spreadsheet data */ gt.pgettext("sort", "Left to right") }
];

// options for the "sorting order" dropdown menu
const SORT_ORDER_ENTRIES: Array<DropdownListEntry<boolean>> = [
    { ...SORT_ASC_BUTTON_OPTIONS,  value: false, icon: undefined },
    { ...SORT_DESC_BUTTON_OPTIONS, value: true,  icon: undefined }
];

// list entry for "sort by column" dropdown menu
const UNDEF_ENTRY: DropdownListEntry<number> = { value: -1, label: UNDEF_LABEL, style: { fontStyle: "italic" } };

// class CustomSortDialog =====================================================

/**
 * A modal dialog with options for sorting a cell range.
 *
 * @param docView
 *  The spreadsheet view containing this dialog instance.
 *
 * @param sortRange
 *  The address of the complete cell range to be sorted, including the
 *  header cells.
 *
 * @param tableModel
 *  The model of a table range to be sorted. If set to `undefined`, a simple
 *  cell range will be sorted.
 *
 * @param hasHeader
 *  Whether the passed cell range contains header cells that will not be sorted
 *  with the remaining cells. Will be used as initial state for a checkbox
 *  control.
 */
export class CustomSortDialog extends BaseDialog<CustomSortDialogResult, SpreadsheetDialogConfig> {

    // properties -------------------------------------------------------------

    readonly #docView: SpreadsheetView;
    readonly #sortRange: Range;
    readonly #tableModel: Opt<TableModel>;
    readonly #headersCbox: Opt<CheckBox>;
    readonly #directionList: Opt<DropdownList<boolean>>;
    readonly #rulesPanelNode: HTMLDivElement;
    readonly #rulesListNode: HTMLDivElement;
    #headerEntries: Array<DropdownListEntry<number>> = [];
    readonly #ruleEntries = new Array<SortRuleEntry>();
    readonly #addRuleButton: HTMLButtonElement;

    #hasHeader: boolean;
    #isVertical = true;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView, sortRange: Range, tableModel: Opt<TableModel>, hasHeader: boolean) {

        // base constructor
        super({
            id: "io-ox-office-spreadsheet-custom-sort-dialog",
            title: /*#. title for sort dialog box */ gt.pgettext("sort", "Custom sort"),
            okLabel: gt("Sort"),
            movable: true,
            smallMobileFooter: true,
            // close automatically, if active sheet will be deleted or hidden by another client
            autoCloseScope: "activeSheet"
        });

        // initialize properties
        this.#docView = docView;
        this.#sortRange = sortRange;
        this.#tableModel = tableModel;
        this.#hasHeader = hasHeader;

        // dialog elements for regular cell ranges only (not for table ranges)
        if (!tableModel) {

            // the container for all optional form controls
            const optionsNode = this.bodyNode.appendChild(createFieldSet({ classes: "option-fields", legend: SORT_OPTIONS_LABEL, indent: true }));

            // create and initialize the "selection has headers" check box
            this.#headersCbox = new CheckBox(hasHeader, { classes: "has-headers", label: HAS_HEADERS_LABEL });
            this.#headersCbox.onChange(state => { this.#hasHeader = state; this.#initializeSortRules(this.#getSortRules()); });
            this.renderControl(optionsNode, this.#headersCbox, {
                // disable the checkbox if only one row is available (in the specified direction)
                validator: () => this.#sortRange.size(!this.#isVertical) > 1
            });

            // create and initialize the "sorting direction" dropdown menu
            this.#directionList = new DropdownList(SORT_DIRECTION_ENTRIES, this.#isVertical, { type: "link", classes: "sort-direction", label: _.noI18n(DIRECTION_LABEL + ":") });
            this.#directionList.onChange(state => { this.#isVertical = !!state; this.#initializeSortRules(); });
            this.renderControl(optionsNode, this.#directionList);

            // trailing separator line (only if there are options available)
            this.bodyNode.appendChild(createElement("hr"));
        }

        // headline above the scrollable rules list
        const sortRulesHeadNode = this.bodyNode.appendChild(createDiv("sort-rule sort-rule-headline"));
        sortRulesHeadNode.append(
            createDiv({ classes: "sort-by", label: SORT_BY_LABEL }),
            createDiv({ classes: "sort-order", label: SORT_ORDER_LABEL }),
            createDeleteButton(() => undefined, { disabled: true })
        );

        // create the initial sorting rules (from table model, or default rule)
        this.#rulesPanelNode = this.bodyNode.appendChild(createDiv("panel sort-rule-panel"));
        this.#rulesListNode = this.#rulesPanelNode.appendChild(createDiv("list-group sort-rule-list"));
        this.#initializeSortRules();

        // prepare the "Add sort rule" button
        this.#addRuleButton = this.createActionButton(ADD_ACTION, ADD_LABEL, {
            alignLeft: true,
            handler: () => this.#appendSortRule({ index: -1, descending: false }),
            validator: this.#canAddSortRule,
            keepOpen: true
        });

        // action handler for the OK button
        this.setOkHandler(() => ({
            sortVert: this.#isVertical,
            hasHeader: this.#hasHeader,
            sortRules: this.#getSortRules()
        }), { keepOpen: "fail" });

        // update the state of the OK button (enabled, if there is no warning visible)
        this.addOkValidator(this.#updateListControls);

        // update layout of container elements
        this.on("show", () => {
            this.listenTo(window, "resize", this.#windowResizeHandler);
            this.#windowResizeHandler();
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the current array of sorting rules.
     */
    #getSortRules(): SortRule[] {
        return this.#ruleEntries.map(entry => ({
            index: entry.sortByList.value,
            descending: entry.sortOrderList.value
        }));
    }

    /**
     * Returns whether a new sorting rule can be inserted into the list.
     */
    #canAddSortRule(): boolean {
        return this.#ruleEntries.length < this.#headerEntries.length;
    }

    /**
     * Appends a new sorting rule to the end of the list.
     */
    #appendSortRule(sortRule: SortRule): void {

        // create the DOM container element for the sorting rule
        const ruleNode = this.#rulesListNode.appendChild(createDiv("list-group-item sort-rule"));

        // create the dropdown menu with all available columns/rows
        const sortByNode = ruleNode.appendChild(createDiv("sort-by"));
        const sortByList = new DropdownList(this.#headerEntries, sortRule.index, { type: "link", icon: "bi:exclamation-triangle-fill", undef: UNDEF_ENTRY });
        this.renderControl(sortByNode, sortByList);

        // create the dropdown menu for the sorting order
        const sortOrderNode = ruleNode.appendChild(createDiv("sort-order"));
        const sortOrderList = new DropdownList(SORT_ORDER_ENTRIES, sortRule.descending, { type: "link" });
        this.renderControl(sortOrderNode, sortOrderList);

        // push the settings into the internal array
        const entry: SortRuleEntry = { sortByNode, sortByList, sortOrderNode, sortOrderList };
        this.#ruleEntries.push(entry);

        // create and initialize the "delete rule" button
        ruleNode.append(createDeleteButton(() => {
            ary.deleteFirst(this.#ruleEntries, entry);
            ruleNode.remove();
            this.validate();
        }));

        // scroll down to the new rule
        this.#rulesPanelNode.scrollTop = this.#rulesPanelNode.scrollHeight;
    }

    /**
     * Updates the state and visibility of various buttons in the rule list.
     *
     * @returns
     *  Whether the rule list is warning-free.
     */
    #updateListControls(): boolean {

        // collect multiple rules with the same column/row index
        const ruleGroups = _.countBy(this.#ruleEntries, entry => entry.sortByList.value);

        // insert a warning icon to the passed dropdown menu
        let hasWarning = false;
        const setWarning = (node: HTMLDivElement, tooltip: string | null): void => {
            hasWarning ||= !!tooltip;
            setElementLabel(node, { tooltip });
            node.classList.toggle("warning-icon", !!tooltip);
        };

        // show warnings for invalid rule settings
        for (const entry of this.#ruleEntries) {
            const index = entry.sortByList.value;
            const node = entry.sortByNode;
            if (index < 0) {
                setWarning(node, ERR_MISSING_LABEL);
            } else if (ruleGroups[index] > 1) {
                setWarning(node, ERR_MULTI_LABEL);
            } else {
                setWarning(node, null);
            }
        }

        // allow to commit zero sort rules to a table/autofilter, but not to custom cell range
        return !hasWarning && !!(this.#tableModel ?? this.#ruleEntries.length);
    }

    /**
     * Clears all existing sorting rules, and adds the passed sorting rules if
     * specified; otherwise a default sorting rule according to the active cell
     * in the document view, or the existing sorting rules of the table model.
     */
    #initializeSortRules(sortRules?: SortRule[]): void {

        // initialize header range data
        const vertical = this.#isVertical;
        const headerRange = this.#tableModel ? this.#tableModel.getRange().headerRow() : this.#sortRange.lineRange(!vertical, 0);
        const { cellCollection } = this.#docView;

        // collect the dropdown entry settings for all header labels
        this.#headerEntries = Array.from(headerRange.addresses(), address => {

            // the current column/row index inside the header range
            const index = address.get(vertical);
            // use display strings of the header cells if specified
            const display = this.#hasHeader ? cellCollection.getDisplayString(address) : null;
            // empty or missing display string (e.g. format error): fall-back to column/row label
            const label = display || getColRowLabel(index, vertical, { prefix: true });
            // show fall-back column/row label in header mode in italic style
            const italic = this.#hasHeader && !display;

            // create the entry for the dropdown menu
            return { value: index, label, style: { fontStyle: italic ? "italic" : "none" } };
        });

        // delete all sorting rules, and DOM sorting rule elements
        detachChildren(this.#rulesListNode);
        this.#ruleEntries.length = 0;

        // use sorting rules of the table model if available
        if (!sortRules && this.#tableModel) {
            sortRules = this.#tableModel.getSortRules();
        }

        // add existing sorting rules, or a default sorting rule
        if (sortRules && (sortRules.length > 0)) {
            sortRules.forEach(sortRule => this.#appendSortRule(sortRule));
        } else {
            const index = this.#docView.selectionEngine.getActiveCell().get(vertical);
            this.#appendSortRule({ index, descending: false });
        }

        // update other dialog controls
        this.validate();
    }

    /**
     * Updates the layout of the dialog elements according to the size of the
     * browser window and dialog root elements.
     */
    #windowResizeHandler(): void {

        // set label to "Add rule" button to be able to measure oversizing
        const button = this.#addRuleButton;
        setElementLabel(button, ADD_LABEL);

        // replace label with "Plus" icon on narrow screens
        const footerRow = this.$footerNode.children(".row")[0];
        if (footerRow ? (footerRow.clientWidth < footerRow.scrollWidth) : (button.offsetTop < this.getOkButton().offsetTop)) {
            detachChildren(button);
            // add NBSP characters to fix the button height (too small if containing an icon only)
            button.append(createText("\xa0"), createIcon("bi:plus-lg"), createText("\xa0"));
            setElementLabel(button, { tooltip: ADD_LABEL });
        }
    }
}
