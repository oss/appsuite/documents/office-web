/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { is } from "@/io.ox/office/tk/algorithms";
import { createDiv, createSpan, createFieldSet, detachChildren } from "@/io.ox/office/tk/dom";
import { CheckBox, SpinField } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { generateCurrencyCode } from "@/io.ox/office/editframework/model/formatter/currencycodes";

import type { SpreadsheetDialogConfig } from "@/io.ox/office/spreadsheet/view/labels";
import { CurrencySymbolPicker } from "@/io.ox/office/spreadsheet/view/control/currencysymbolpicker";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

import "@/io.ox/office/spreadsheet/view/dialog/customcurrencydialog.less";

// class CustomCurrencyDialog =================================================

/**
 * A modal dialog with options for setting up a custom number format code.
 */
export class CustomCurrencyDialog extends BaseDialog<string, SpreadsheetDialogConfig> {

    readonly #docView: SpreadsheetView;

    readonly #currencyPicker: CurrencySymbolPicker;
    readonly #fracDigitsSpin: SpinField;
    // readonly #intDigitsSpin: SpinField;
    readonly #groupSepCheckBox: CheckBox;
    readonly #accountingCheckBox: CheckBox;
    readonly #isoSymbolCheckBox: CheckBox;
    readonly #negativeRedCheckBox: CheckBox;

    readonly #previewBox: HTMLElement;
    readonly #previewCells: Array<{ node: HTMLElement; value: number }> = [];

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The spreadsheet view containing this dialog instance.
     */
    constructor(docView: SpreadsheetView) {

        // base constructor
        super({
            id: "io-ox-office-custom-currency-dialog",
            //#. title for number format dialog box
            title: gt.pgettext("number-format", "Custom currency format"),
            movable: true,
            autoCloseScope: "activeCell",
            smallMobileFooter: true,
        });

        // private properties
        this.#docView = docView;

        // determine initial dialog settings from first section of number format
        const { numberSections } = docView.getActiveParsedFormat();
        const locale = numberSections[0]?.locale?.localeId?.intl;
        const currencyInfo = numberSections[0]?.currency;
        const numberInfo = numberSections[0]?.numbers;

        // the container element for the lists and spin fields
        const mainFieldSet = this.bodyNode.appendChild(createFieldSet("main-fields"));

        // "Currency symbol" dropdown list
        this.#currencyPicker = new CurrencySymbolPicker({ locale, ...currencyInfo, key: "currency-symbol" });
        this.renderControl(mainFieldSet, this.#currencyPicker, { label: gt.pgettext("number-format", "Currency symbol") });
        this.#currencyPicker.onChange(() => this.#refresh());

        // "Decimal digits" spin field
        const fracDigits = numberInfo?.minFracDigits ?? LOCALE_DATA.currency.precision;
        this.#fracDigitsSpin = new SpinField(fracDigits, { key: "frac-digits", min: 0, max: 10 });
        this.renderControl(mainFieldSet, this.#fracDigitsSpin, { label: gt.pgettext("number-format", "Decimal digits") });
        this.#fracDigitsSpin.onChange(() => this.#refresh());

        // "Leading zeros" spin field
        // const intDigits = numberInfo?.minIntDigits ?? 1;
        // this.#intDigitsSpin = new SpinField(intDigits, { key: "int-digits", min: 0, max: 10 });
        // this.renderControl(mainFieldSet, this.#intDigitsSpin, { label: gt.pgettext("number-format", "Leading zeros") });
        // this.#intDigitsSpin.onChange(() => this.#refresh());

        // the container for all checkboxes
        const optionsFieldSet = this.bodyNode.appendChild(createFieldSet({ classes: "option-fields", legend: gt("Options"), indent: true }));

        // "Group separator" check box
        const groupSep = numberInfo?.groupInt ?? true;
        this.#groupSepCheckBox = new CheckBox(groupSep, { key: "group-separator", label: gt.pgettext("number-format", "Use thousands separator") });
        this.renderControl(optionsFieldSet, this.#groupSepCheckBox);
        this.#groupSepCheckBox.onChange(() => this.#refresh());

        // "Use accounting format" check box
        const accounting = !!numberSections[0]?.fillToken;
        this.#accountingCheckBox = new CheckBox(accounting, { key: "accounting", label: gt.pgettext("number-format", "Use accounting format") });
        this.renderControl(optionsFieldSet, this.#accountingCheckBox);
        this.#accountingCheckBox.onChange(() => this.#refresh());

        // "Use ISO currency" check box
        const symbol = numberSections[0]?.currency?.symbol;
        const isoSymbol = !!symbol && (this.#currencyPicker.getCurrencyData()?.iso === symbol);
        this.#isoSymbolCheckBox = new CheckBox(isoSymbol, { key: "iso-symbol", label: gt.pgettext("number-format", "Use ISO currency code") });
        this.renderControl(optionsFieldSet, this.#isoSymbolCheckBox);
        this.#isoSymbolCheckBox.onChange(() => this.#refresh());

        // "Red text color" check box
        const negativeRed = numberSections[1]?.color?.key === "red";
        this.#negativeRedCheckBox = new CheckBox(negativeRed, { key: "negative-red", label: gt.pgettext("number-format", "Red text color for negative numbers") });
        this.renderControl(optionsFieldSet, this.#negativeRedCheckBox);
        this.#negativeRedCheckBox.onChange(() => this.#refresh());

        // pick a positive value for the preview box
        const cellValue = docView.getActiveCellValue();
        const cellNumber = is.number(cellValue) ? Math.abs(cellValue) : 0;
        const previewValue = (cellNumber >= 1) ? cellNumber : 1234.5678;

        // the box with the preview values
        const previewFieldSet = this.bodyNode.appendChild(createFieldSet({ legend: gt("Preview") }));
        const previewLabels = previewFieldSet.appendChild(createDiv("preview-labels"));
        this.#previewBox = previewFieldSet.appendChild(createDiv("preview-box"));
        const labels = [
            //#. Number format dialog: Preview field label for positive numbers
            [gt.pgettext("number-format", "Positive"), "positive", previewValue],
            //#. Number format dialog: Preview field label for negative numbers
            [gt.pgettext("number-format", "Negative"), "negative", -previewValue],
            //#. Number format dialog: Preview field label for zero
            [gt.pgettext("number-format", "Zero"), "zero", 0]
        ] as const;
        for (const [label, type, value] of labels) {
            previewLabels.append(createSpan({ label }));
            const node = this.#previewBox.appendChild(createDiv({ classes: "preview-cell", dataset: { type } }));
            this.#previewCells.push({ node, value });
        }

        // action handler for the OK button
        this.setOkHandler(this.#getFormatCode);

        // initialize dialog contents
        this.#refresh();
    }

    // private methods --------------------------------------------------------

    /**
     * Generates and returns the format code for the current dialog settings.
     */
    #getFormatCode(): string {
        const entry = this.#currencyPicker.getSelectedEntry();
        return generateCurrencyCode({
            ...entry?.options,
            currencyData: entry?.currData,
            patternType: this.#accountingCheckBox.value ? "accounting" : "currency",
            isoCurrency: this.#isoSymbolCheckBox.value,
            negativeRed: this.#negativeRedCheckBox.value,
            // intDigits: this.#intDigitsSpin.value,
            fracDigits: this.#fracDigitsSpin.value,
            groupSep: this.#groupSepCheckBox.value,
        });
    }

    /**
     * Refreshes the preview cells according to the current dialog settings.
     */
    #refresh(): void {

        const { numberFormatter } = this.#docView.docModel;
        const formatCode = this.#getFormatCode();

        for (const { node, value } of this.#previewCells) {
            const result = numberFormatter.formatValue(formatCode, value);
            detachChildren(node);
            for (const portion of result.portions) {
                const portionSpan = node.appendChild(createSpan({ label: portion.text }));
                if (portion.type === "blind") {
                    portionSpan.style.opacity = "0";
                } else if (portion.type === "fill") {
                    portionSpan.style.flexGrow = "1";
                }
            }
            node.classList.toggle("negative-red", result.color?.key === "red");
        }
    }
}
