/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { TextField } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import type { SpreadsheetDialogConfig } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

export interface SheetNameDialogConfig extends SpreadsheetDialogConfig {

    /**
     * The initial sheet name to be shown in the dialog.
     */
    sheetName: string;

    /**
     * A callback function invoked when the OK button of the dialog has been
     * clicked. If the function returns a promise that will be rejected, or
     * throws an exception, the dialog will be kept open.
     *
     * @param sheetName
     *  The current sheet name entered in the dialog.
     */
    actionHandler(sheetName: string): MaybeAsync;
}

// class SheetNameDialog ======================================================

/**
 * Shows a dialog that allows to enter a sheet name, and will invoke an
 * arbitrary callback function for the sheet name. The dialog will be kept open
 * until a valid sheet name has been entered.
 *
 * @param docView
 *  The spreadsheet view instance that has created this dialog.
 *
 * @param config
 *  Configuration options for the dialog.
 */
export class SheetNameDialog extends BaseDialog<void, SheetNameDialogConfig> {

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView, config: SheetNameDialogConfig) {

        // base constructor
        super({
            ...config,
            // close automatically, if active sheet will be deleted or hidden by another client
            autoCloseScope: "activeSheet"
        });

        // create the text input field
        const nameField = new TextField(config.sheetName, {
            classes: "sheet-name-input",
            placeholder: gt("Enter sheet name"),
            maxLength: docView.docModel.getMaxSheetNameLength()
        });

        // render the text control, and take ownership
        this.renderControl(this.bodyNode, nameField);

        // disable OK button while textfield is empty
        this.addOkValidator(() => !!nameField.rawText);

        // register the action handler for the OK button
        this.setOkHandler(() => config.actionHandler.call(this, nameField.value), { keepOpen: "fail" });
    }
}
