/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { str } from "@/io.ox/office/tk/algorithms";
import { TextField } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import { type OptSheet, MAX_NAME_LENGTH } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

export interface DefinedNameDialogConfig {

    /**
     * The model of an existing defined name. If passed, the dialog will offer
     * to change the defined name (the sheet scope cannot be changed though).
     * If omitted, the dialog will offer to create a new defined name (sheet
     * scope can be selected).
     */
    nameModel?: NameModel;

    /**
     * The initial label for the defined name to be shown when the dialog is
     * opened. If omitted, the label of the defined name passed in the option
     * `nameModel` will be used if available, otherwise an empty string.
     */
    label?: string;

    /**
     * The initial formula expression to be shown when the dialog is opened. If
     * omitted, the formula expression of the defined name passed in the option
     * `nameModel` will be used if available, otherwise an empty string.
     */
    formula?: string;
}

/**
 * A callback function invoked when the OK button of the "Defined Name" dialog
 * has been clicked. May throw an exception or return a rejected promise to
 * keep the dialog open in case of an error.
 *
 * @param label
 *  The current label entered in the dialog; or `null`, if the label has not
 *  been changed.
 *
 * @param formula
 *  The current formula expression entered in the dialog; or `null`, if the
 *  formula has not been changed.
 *
 * @param sheet
 *  The sheet index for sheet-local names, or `null` for global names.
 */
export type DefinedNameActionHandlerFn = (label: string | null, formula: string | null, sheet: OptSheet) => MaybeAsync;

// class DefinedNameDialog ====================================================

/**
 * Shows a dialog that allows to create or modify a defined name, and will
 * invoke an arbitrary callback function for the defined name. The dialog will
 * be kept open until the settings for the defined name are valid.
 *
 * @param docView
 *  The spreadsheet view instance that has created this dialog.
 *
 * @param actionHandler
 *  The callback function invoked when the OK button of the dialog has been
 *  clicked. Receives the current label and formula entered in the dialog, and
 *  MUST return a boolean value or a promise that states whether the operation
 *  using that label and formula expression has been finished successfully. In
 *  case of an error, the dialog will be kept open with its current settings.
 *
 * @param [config]
 *  Optional parameters.
 */
export class DefinedNameDialog extends BaseDialog<void> {

    constructor(docView: SpreadsheetView, actionHandler: DefinedNameActionHandlerFn, config?: DefinedNameDialogConfig) {

        // the document model
        const { docModel } = docView;
        // whether to change an existing defined name
        const nameModel = config?.nameModel;

        // base constructor
        super({
            title: nameModel ? gt("Change name") : gt("Insert name"),
            okLabel: nameModel ? gt("Change") : gt("Insert")
        });

        // helper function that returns the formula expression of an existing defined name
        const getNameFormula = (): string => nameModel?.getFormula("ui", docView.selectionEngine.getActiveCell()) ?? "";

        // create the text input field for the defined name's label
        const labelField = new TextField(config?.label ?? nameModel?.label ?? "", {
            classes: "form-group name-label-input",
            placeholder: gt("Enter name"),
            maxLength: MAX_NAME_LENGTH,
            autoTrim: true
        });
        this.renderControl(this.bodyNode, labelField, { label: gt("Name") });

        // create the text input field for the formula expression
        const formulaField = new TextField(config?.formula ?? getNameFormula(), {
            classes: "name-formula-input",
            placeholder: gt("Enter formula"),
            autoTrim: true
        });
        this.renderControl(this.bodyNode, formulaField, { label: gt("Formula") });

        // close automatically, if the sheet containing the processed defined name will be deleted by another client
        if (nameModel?.sheetModel) {
            this.listenTo(docModel, "delete:sheet:before", event => {
                if (nameModel.sheetModel === event.sheetModel) { this.close(); }
            });
        }

        // close automatically, if the processed defined name will be deleted by another client
        if (nameModel) {
            this.listenTo(docModel, "delete:name:before", event => {
                if (nameModel === event.nameModel) { this.close(); }
            });
        }

        // update unchanged label of the processed defined name when changed by another client
        if (nameModel) {
            this.listenTo(docModel, "change:name", event => {
                if ((nameModel === event.nameModel) && event.label && (labelField.rawText === event.label[0])) {
                    labelField.value = event.label[1];
                }
            });
        }

        // disable OK button while any textfield is empty
        this.addOkValidator(() => !!labelField.rawText && !!formulaField.rawText);

        // register the action handler for the OK button
        this.setOkHandler(() => {

            let newLabel: string | null = labelField.value;
            let newFormula: string | null = str.trimAll(formulaField.value);

            // create a new defined name
            if (!nameModel) {
                // TODO: support for sheet-local names: get sheet index from drop-down list
                return actionHandler.call(this, newLabel, newFormula, null);
            }

            // change existing defined name
            if (newLabel === nameModel.label) { newLabel = null; }
            if (newFormula === getNameFormula()) { newFormula = null; }
            if (newLabel || newFormula) {
                return actionHandler.call(this, newLabel, newFormula, null);
            }
        }, { keepOpen: "fail" });
    }
}
