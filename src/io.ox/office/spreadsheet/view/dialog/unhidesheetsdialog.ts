/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { itr } from "@/io.ox/office/tk/algorithms";
import { createFieldSet } from "@/io.ox/office/tk/dom";
import { CheckList } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// class UnhideSheetsDialog ===================================================

/**
 * A dialog that allows to make some or all hidden sheets visible in the
 * spreadsheet document.
 *
 * @param docView
 *  The spreadsheet view instance that has created this dialog.
 */
export class UnhideSheetsDialog extends BaseDialog<readonly number[]> {

    // properties -------------------------------------------------------------

    // the document model
    readonly #docModel: SpreadsheetModel;

    // the checklist control for all hidden sheets
    readonly #checkList: CheckList<number>;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView) {

        // base constructor
        super({
            id: "io-ox-office-spreadsheet-unhide-sheets-dialog",
            width: 400,
            title: gt("Unhide sheets"),
            movable: true
        });

        // initialize properties
        this.#docModel = docView.docModel;

        // the container for the checklist, with indentation
        const container = this.bodyNode.appendChild(createFieldSet({ indent: true }));

        // create and render the checklist control for all hidden sheets
        this.#checkList = new CheckList<number>([], [], { checkAll: true });
        this.renderControl(container, this.#checkList);

        // initialize the check list
        this.#fillCheckList();

        // refill checklist after any changes in the sheet collection
        this.listenTo(this.#docModel, ["transform:sheets", "rename:sheet", "change:sheetattrs"], this.#fillCheckList);

        // update enabled state of the OK button
        this.addOkValidator(() => !this.#checkList.noneChecked);

        // return the dialog result (the indexes of all sheets to be shown)
        this.setOkHandler(() => this.#checkList.value);
    }

    // private methods --------------------------------------------------------

    /**
     * Creates the list of check boxes.
     */
    #fillCheckList(): void {

        // create a checklist entry for each existing hidden sheet
        const sheetModels = this.#docModel.yieldSheetModels({ supported: true, visible: false });
        const entryIter = itr.map(sheetModels, sheetModel => ({
            value: sheetModel.getIndex(),
            label: _.noI18n(sheetModel.getName())
        }));

        // recreate the checkboxes in the checklist control
        this.#checkList.replaceEntries(entryIter, []);

        // close the dialog if there are no more hidden sheets available (concurrent editing)
        if (this.#checkList.size === 0) {
            this.setTimeout(this.close, 0);
        }
    }
}
