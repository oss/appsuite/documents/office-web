/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { ary } from "@/io.ox/office/tk/algorithms";
import { type CreateButtonOptions, enableElement, setElementLabel, createDiv, createButton, detachChildren, setFocus } from "@/io.ox/office/tk/dom";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

import "@/io.ox/office/spreadsheet/view/dialog/movesheetsdialog.less";

// types ======================================================================

type MoveDir = "first" | "prev" | "next" | "last";

// constants ==================================================================

// CSS class for unselected sheet badge buttons
const UNSELECTED_CLASS = "btn-default";

// CSS class for unselected sheet badge buttons
const SELECTED_CLASS = "btn-primary";

// maps keyboard keys to target directions for moving selection or badge node
const KEY_TO_DIRECTION: Dict<MoveDir> = {
    Home:      "first",
    ArrowUp:   "prev",
    ArrowDown: "next",
    End:       "last"
};

// maps target keys to forward/backward flag for moving badge node
const DIRECTION_TO_FORWARD: Record<MoveDir, boolean> = {
    first: false,
    prev:  false,
    next:  true,
    last:  true
};

// class MoveSheetsDialog =====================================================

/**
 * A modal dialog with a list of all sheets in the document that can be
 * reordered.
 *
 * @param docView
 *  The spreadsheet view instance containing this dialog.
 */
export class MoveSheetsDialog extends BaseDialog<readonly number[]> {

    // document access
    readonly docModel: SpreadsheetModel;
    readonly docView: SpreadsheetView;

    // the (scrollable) container for the badge list
    readonly #badgeCont = createDiv("badge-container");
    // the list of badge buttons
    readonly #badgeList = createDiv("badge-list");
    // the container for the move buttons
    readonly #buttonCont = createDiv("button-container");

    // the active (selected) badge button
    #activeBadge!: HTMLButtonElement;
    // the resulting sheet permutation vector
    #sortVector!: number[];

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView) {

        // base constructor
        super({
            id: "io-ox-office-spreadsheet-move-sheets-dialog",
            title: gt("Reorder sheets"),
            width: 420,
            focus: `button[data-sheet].${SELECTED_CLASS}`
        });

        // document accress
        this.docModel = docView.docModel;
        this.docView = docView;

        // insert the controls into the dialog body
        this.$bodyNode.append(this.#badgeCont, this.#buttonCont);
        this.#badgeCont.appendChild(this.#badgeList);
        this.#badgeList.setAttribute("role", "tablist");

        // the push buttons used to move the active badge
        this.#createMoveButton("first", { icon: "bi:chevron-double-up",   tooltip: gt("Move to top") });
        this.#createMoveButton("prev",  { icon: "bi:chevron-up",          tooltip: gt("Move up") });
        this.#createMoveButton("next",  { icon: "bi:chevron-down",        tooltip: gt("Move down") });
        this.#createMoveButton("last",  { icon: "bi:chevron-double-down", tooltip: gt("Move to bottom") });

        // keyboard shortcut handling
        this.$bodyNode.on("keydown", event => {
            // determine the direction to move to
            const direction = KEY_TO_DIRECTION[event.key];
            if (!direction) { return; }
            // select another badge, or move the active badge
            if (event.ctrlKey || event.metaKey) {
                this.#moveBadge(direction);
            } else {
                this.#moveSelection(direction);
            }
            // do not process the key event further
            return false;
        });

        // initialize sheet badge list
        this.on("show", () => {
            this.#initBadgeList();
            this.listenTo(window, "resize", this.#updateVerticalJustify);
        });

        // reset and repaint badge list after any modifications in the sheet collection
        this.listenTo(this.docModel, "transform:sheets", this.#initBadgeList);

        // reset and repaint badge list when hiding or showing sheets
        this.listenTo(this.docModel, "change:sheetattrs", event => {
            if (event.newAttrSet.sheet.visible !== event.oldAttrSet.sheet.visible) {
                this.#initBadgeList();
            }
        });

        // refresh badge label when renaming a sheet
        this.listenTo(this.docModel, "rename:sheet", event => {
            const sheetBadge = this.#findBadgeNode(event.sheet);
            if (sheetBadge) { this.#renderSheetBadge(sheetBadge, event.sheetModel); }
        });

        // return the permutation vector as dialog result
        this.setOkHandler(() => this.#sortVector);
    }

    // private methods --------------------------------------------------------

    #findBadgeNode(sheet: number): HTMLButtonElement | null {
        return this.#badgeList.querySelector<HTMLButtonElement>(`:scope > .btn[data-sheet="${sheet}"]`);
    }

    /**
     * Enables or disables the move buttons for the active (selected) sheet
     * badge, and scrolls the sheet badge list to that badge.
     */
    #updateControls(): void {
        for (const buttonNode of this.#buttonCont.childNodes as NodeListOf<HTMLButtonElement>) {
            const forward = DIRECTION_TO_FORWARD[buttonNode.dataset.action as MoveDir];
            const siblingBadge = forward ? this.#activeBadge.nextSibling : this.#activeBadge.previousSibling;
            enableElement(buttonNode, !!siblingBadge);
        }
        this.#activeBadge.scrollIntoView({ block: "nearest" });
    }

    /**
     * Update the sheet name in a sheet badge node.
     */
    #renderSheetBadge(sheetBadge: HTMLButtonElement, sheetModel: SheetModel): void {
        const sheetName = _.noI18n(sheetModel.getName());
        setElementLabel(sheetBadge, { label: sheetName, tooltip: sheetName });
    }

    /**
     * Changes the selection state of the passed sheet badge element.
     */
    #selectBadge(sheetBadge: HTMLButtonElement, selected: boolean): void {
        sheetBadge.classList.toggle(SELECTED_CLASS, selected);
        sheetBadge.classList.toggle(UNSELECTED_CLASS, !selected);
    }

    /**
     * Activates the specified sheet badge.
     */
    #activateBadge(sheetBadge: HTMLButtonElement): void {

        this.#selectBadge(this.#activeBadge, false);
        this.#activeBadge = sheetBadge;
        this.#selectBadge(this.#activeBadge, true);

        setFocus(this.#activeBadge);
        this.#updateControls();
    }

    #updateVerticalJustify(): void {
        // "justify-content:center" does not play well with scrollable nodes (at least in Chrome,
        // the upper part of the list cannot be scrolled into the view anymore but remains clippped)
        const scrollable = this.#badgeCont.clientHeight < this.#badgeCont.scrollHeight;
        this.#badgeCont.style.justifyContent = scrollable ? "start" : "center";
    }

    /**
     * Creates the badge nodes for all visible sheets in the document.
     */
    #initBadgeList(): void {

        // clear all old sheet badges
        detachChildren(this.#badgeList);

        // create badges for all visible and supported sheets
        for (const sheetModel of this.docModel.yieldSheetModels({ supported: true, visible: true })) {
            const sheetBadge = createButton({
                dataset: { sheet: sheetModel.getIndex() },
                click: () => this.#activateBadge(sheetBadge)
            });
            this.#renderSheetBadge(sheetBadge, sheetModel);
            this.#badgeList.appendChild(sheetBadge);
        }

        // select the badge of the active sheet
        this.#activeBadge = this.#findBadgeNode(this.docView.activeSheet) ?? (this.#badgeList.firstChild as HTMLButtonElement);
        this.#selectBadge(this.#activeBadge, true);

        // sort vector must contain all sheet indexes (also hidden and unsupported sheets)
        this.#sortVector = ary.sequence(this.docModel.getSheetCount());

        // update state of move buttons, and badge scroll position
        this.#updateControls();

        // update vertical justification of the badge list
        this.#updateVerticalJustify();
    }

    /**
     * Resolves the badge targeted by the passed specifier.
     *
     * @param direction
     *  The direction to move to from the current active sheet badge.
     *
     * @returns
     *  The new badge targeted by the specifier; or the value `null`, if there
     *  is no badge node available.
     */
    #resolveBadge(direction: MoveDir): HTMLButtonElement | null {
        switch (direction) {
            case "first": return this.#badgeList.firstChild as HTMLButtonElement;
            case "prev":  return this.#activeBadge.previousSibling as HTMLButtonElement | null;
            case "next":  return this.#activeBadge.nextSibling as HTMLButtonElement | null;
            case "last":  return this.#badgeList.lastChild as HTMLButtonElement;
        }
    }

    /**
     * Moves the sheet badge selection into the specified direction.
     */
    #moveSelection(direction: MoveDir): void {
        const targetBadge = this.#resolveBadge(direction);
        if (targetBadge) { this.#activateBadge(targetBadge); }
    }

    /**
     * Moves the active sheet badge into the specified direction.
     */
    #moveBadge(direction: MoveDir): void {

        const targetBadge = this.#resolveBadge(direction);
        if (targetBadge) {
            const fromSheet = parseInt(this.#activeBadge.dataset.sheet!, 10);
            const toSheet = parseInt(targetBadge.dataset.sheet!, 10);
            const forward = DIRECTION_TO_FORWARD[direction];
            if (forward) {
                targetBadge.after(this.#activeBadge);
            } else {
                targetBadge.before(this.#activeBadge);
            }
            ary.deleteFirst(this.#sortVector, fromSheet);
            this.#sortVector.splice(this.#sortVector.indexOf(toSheet) + (forward ? 1 : 0), 0, fromSheet);
        }

        setFocus(this.#activeBadge);
        this.#updateControls();
    }

    /**
     * Creates a button control that moves the selected sheet badge.
     */
    #createMoveButton(direction: MoveDir, options?: CreateButtonOptions): void {
        this.#buttonCont.appendChild(createButton({
            ...options,
            action: direction,
            click: () => this.#moveBadge(direction)
        }));
    }
}
