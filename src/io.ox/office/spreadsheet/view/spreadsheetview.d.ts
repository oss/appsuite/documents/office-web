/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Point } from "@/io.ox/office/tk/algorithms";
import { Rectangle } from "@/io.ox/office/tk/dom";

import { ItemExecuteOptions } from "@/io.ox/office/baseframework/controller/controlleritem";
import { PtParagraphAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { MixedBorder } from "@/io.ox/office/editframework/utils/mixedborder";
import { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import { CommentsPane } from "@/io.ox/office/editframework/view/pane/commentspane";
import TextBaseView, { TextBaseViewEventMap } from "@/io.ox/office/textframework/view/view";

import { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import { BorderName } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { PaneSide, PanePos } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { Position } from "@/io.ox/office/spreadsheet/utils/operations";

import { CellRectangleOptions, SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { SheetPropertySet } from "@/io.ox/office/spreadsheet/model/sheetpropset";
import { ColCollection, RowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";
import { MergeCollection } from "@/io.ox/office/spreadsheet/model/mergecollection";
import { HlinkCollection } from "@/io.ox/office/spreadsheet/model/hlinkcollection";
import { DVRuleCollection } from "@/io.ox/office/spreadsheet/model/dvrulecollection";
import { CFRuleCollection } from "@/io.ox/office/spreadsheet/model/cfrulecollection";
import { TableCollection } from "@/io.ox/office/spreadsheet/model/tablecollection";
import { SheetCollectionEventMap } from "@/io.ox/office/spreadsheet/model/sheetcollection";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import { SheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/drawingcollection";
import { NoteCollection } from "@/io.ox/office/spreadsheet/model/drawing/notecollection";
import { CommentCollection } from "@/io.ox/office/spreadsheet/model/drawing/commentcollection";

import { YellMessageCode } from "@/io.ox/office/spreadsheet/view/labels";
import { ViewPropertySet } from "@/io.ox/office/spreadsheet/view/viewpropset";
import { CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";
import { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { CornerPane } from "@/io.ox/office/spreadsheet/view/pane/cornerpane";
import { HeaderPane } from "@/io.ox/office/spreadsheet/view/pane/headerpane";
import { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import { RangeHighlighter } from "@/io.ox/office/spreadsheet/view/edit/rangehighlighter";
import { SelectionEngine } from "@/io.ox/office/spreadsheet/view/selection/selectionengine";
import { TextEditorBaseEventMap, TextEditorBase } from "@/io.ox/office/spreadsheet/view/edit/texteditorbase";

import SpreadsheetApp from "@/io.ox/office/spreadsheet/app/application";

// types ======================================================================

export type TextEditMode = "cell" | "drawing";

export interface TextEditModeOptions {
    editMode?: TextEditMode;
    force?: boolean;
}

/**
 * Optional parameters for the method `SpreadsheetView::scrollToCell`.
 */
export interface ScrollToCellOptions {

    /**
     * If set to `true`, focussing the active grid pane in frozen split mode
     * will be skipped. Default value is `false`.
     */
    preserveFocus?: boolean;
}

/**
 * Information about the current selection of a remote user in the active
 * sheet.
 */
export interface RemoteSelectionData {

    /**
     * The display name of the remote user.
     */
    userName: string;

    /**
     * The index of a color in the color scheme associated with the remote
     * user.
     */
    colorIndex: number;
}

/**
 * Information about the current cell selection of a remote user in the active
 * sheet.
 */
export interface RemoteCellSelectionData extends RemoteSelectionData {

    /**
     * The addresses of the cell ranges selected by the remote user.
     */
    ranges: RangeArray;
}

/**
 * Information about the current drawing selection of a remote user in the
 * active sheet.
 */
export interface RemoteDrawingSelectionData extends RemoteSelectionData {

    /**
     * The positions of all drawing objects selected by the remote user.
     */
    drawings: Position[];
}

/**
 * Type mapping for the events emitted by `SpreadsheetView` instances.
 */
export interface SpreadsheetViewEventMap extends
    TextBaseViewEventMap,
    SheetCollectionEventMap,
    TextEditorBaseEventMap {

    /**
     * Will be emitted before another sheet will be activated.
     *
     * @param sheet
     *  The zero-based index of the old active sheet in the document.
     */
    "before:activesheet": [sheet: number];

    /**
     * Will be emitted after another sheet has been activated.
     *
     * @param sheet
     *  The zero-based index of the new active sheet in the document.
     *
     * @param sheetModel
     *  The sheet model instance of the new active sheet.
     */
    "change:activesheet": [sheet: number, sheetModel: SheetModel];

    /**
     * Will be emitted after the selection data (active cell settings, mixed
     * borders, subtotal results) have been updated.
     */
    "update:selection:data": [];

    /**
     * Will be emitted directly before the event "change:sheetprops" caused by
     * a changed selection in the active sheet.
     *
     * @param newSel
     *  The new sheet selection of the active sheet.
     *
     * @param oldSel
     *  The old sheet selection of the active sheet.
     *
     * @param sheetChanged
     *  Whether the event was caused by activating another sheet.
     */
    "change:selection:prepare": [newSel: SheetSelection, oldSel: SheetSelection, sheetChanged: boolean];

    /**
     * Will be emitted directly after the event "change:sheetprops" caused by
     * a changed selection in the active sheet, for convenience.
     *
     * @param newSel
     *  The new sheet selection of the active sheet.
     *
     * @param oldSel
     *  The old sheet selection of the active sheet.
     *
     * @param sheetChanged
     *  Whether the event was caused by activating another sheet.
     */
    "change:selection": [newSel: SheetSelection, oldSel: SheetSelection, sheetChanged: boolean];

    /**
     * Will be emitted after the scroll position of any of the header and grid
     * panes has been changed.
     *
     * @param paneSide
     *  The pane side identifier of the scrolled header pane.
     *
     * @param scrollPos
     *  The new scroll position of the header pane.
     */
    "change:scrollpos": [paneSide: PaneSide, scrollPos: number];

    /**
     * Will be emitted after a new status label has been displayed.
     */
    "change:statuslabel": [text: string];

    /**
     * Will be emitted after creating or updating the dirty cell cache
     * entries in the rendering cache of the active sheet asynchronously.
     */
    "rendercache:ready": [ranges: RangeArray];
}

// class SpreadsheetView ======================================================

export class SpreadsheetView extends TextBaseView<SpreadsheetViewEventMap> {

    declare readonly docApp: SpreadsheetApp;
    declare readonly docModel: SpreadsheetModel;
    declare readonly commentsPane: CommentsPane<SpreadsheetView>;

    public constructor(docApp: SpreadsheetApp, docModel: SpreadsheetModel);

    readonly propSet: ViewPropertySet;
    readonly rangeHighlighter: RangeHighlighter;
    readonly selectionEngine: SelectionEngine;

    readonly activeSheet: number;
    readonly sheetModel: SheetModel;
    readonly sheetPropSet: SheetPropertySet;
    readonly colCollection: ColCollection;
    readonly rowCollection: RowCollection;
    readonly cellCollection: CellCollection;
    readonly mergeCollection: MergeCollection;
    readonly hlinkCollection: HlinkCollection;
    readonly dvRuleCollection: DVRuleCollection;
    readonly cfRuleCollection: CFRuleCollection;
    readonly tableCollection: TableCollection;
    readonly drawingCollection: SheetDrawingCollection;
    readonly noteCollection: NoteCollection;
    readonly commentCollection: CommentCollection;
    readonly renderCache: RenderCache;

    readonly cornerPane: CornerPane;

    override grabFocus(options?: ItemExecuteOptions & { targetAddress?: Address }): void;

    getGridPane(panePos: PanePos): GridPane;
    getVisibleGridPane(panePos: PanePos): GridPane;
    getActiveGridPane(): GridPane;
    getTargetGridPane(address: Address): GridPane;
    getGridPaneForPagePoint(point: Point | MouseEvent): Opt<GridPane>;
    getGridPanes(): readonly GridPane[];

    getHeaderPane(paneSide: PaneSide): HeaderPane;
    getColHeaderPane(panePos: PanePos): HeaderPane;
    getRowHeaderPane(panePos: PanePos): HeaderPane;
    getVisibleHeaderPane(columns: boolean): HeaderPane;
    getActiveHeaderPane(columns: boolean): HeaderPane;
    getHeaderPanes(): readonly HeaderPane[];
    getHeaderWidth(): number;
    getHeaderHeight(): number;
    getHeaderFontSize(): number;

    yellMessage(msgCode: YellMessageCode, success?: boolean): boolean;
    yellOnFailure<FT>(promise: JPromise<FT>): JPromise<FT>;

    requireEditMode(): boolean;

    getStatusLabel(): string | null;
    setStatusLabel(text: string | null): void;

    registerCanvasRenderer(canvasRenderer: CanvasBaseRenderer): void;

    getCommentCollection(): CommentCollection | null;
    convertPixelToHmm(length: number): number;
    convertHmmToPixel(length: number): number;
    getSheetRectangle(): Rectangle;
    getRangeRectangle(range: Range): Rectangle;
    getCellRectangle(address: Address, options?: CellRectangleOptions): Rectangle;

    scrollToCell(address: Address, options?: ScrollToCellOptions): void;
    scrollToDrawingFrame(position: Position): void;

    isTextEditMode(editMode?: TextEditMode): boolean;
    getActiveTextEditor(editMode?: TextEditMode): Opt<TextEditorBase>;
    enterTextEditMode(editMode: TextEditMode, options?: unknown): JPromise;
    leaveTextEditMode(options?: TextEditModeOptions): JPromise;
    cancelTextEditMode(options?: TextEditModeOptions): void;
    getTextEditFocusNode(options?: TextEditModeOptions): Opt<HTMLElement>;
    getTextAttributeSet(): PtParagraphAttributeSet;
    setTextAttributeSet(attrSet: PtParagraphAttributeSet): JPromise;

    remoteSelections(type: "cell"): IterableIterator<RemoteCellSelectionData>;
    remoteSelections(type: "drawing"): IterableIterator<RemoteDrawingSelectionData>;

    // ViewFuncMixin
    isSheetLocked(): boolean;
    ensureUnlockedActiveCell(options: object & { sync: true }): YellMessageCode | null;
    ensureUnlockedActiveCell(options?: object): JPromise;
    ensureUnlockedDrawings(options: object & { sync: true }): YellMessageCode | null;
    ensureUnlockedDrawings(options?: object): JPromise;
    getActiveCellValue(): ScalarType;
    getActiveParsedFormat(): ParsedFormat;
    getBorderAttributes(): Record<BorderName, MixedBorder>;
}
