/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { is, json, jpromise } from '@/io.ox/office/tk/algorithms';
import { Rectangle, convertHmmToLength } from '@/io.ox/office/tk/dom';
import { getOption } from '@/io.ox/office/tk/utils';

import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getColorOfPattern, getColorSet, isAutoColor } from '@/io.ox/office/drawinglayer/view/chartstyleutil';

import { AnchorMode, Address, Range, Range3D, Range3DArray } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import * as Op from '@/io.ox/office/spreadsheet/utils/operations';
import { TokenArray } from '@/io.ox/office/spreadsheet/model/formula/tokenarray';

// constants ==================================================================

// default width of new chart objects
const CHART_DEFAULT_WIDTH = convertHmmToLength(15000, 'px', 1);

// default height of new chart objects
const CHART_DEFAULT_HEIGHT = convertHmmToLength(10000, 'px', 1);

const DEFAULT_LINECOLOR = transformOpColor(Color.TEXT1, { lumMod: 15000 }, { lumOff: 85000 });

const AXIS_LABEL_ENABLED =  { axis: { label: true },  line: getNoneShapeAttributes() };
const AXIS_LABEL_LINE_ENABLED =  { axis: { label: true },  line: getStandardShapeAttributes() };
const AXIS_DISABLED = { axis: { label: false }, line: getNoneShapeAttributes() };
const GRID_ENABLED =  { line: getStandardShapeAttributes() };
const GRID_DISABLED = { line: getNoneShapeAttributes() };

// private functions ==========================================================

function getStandardShapeAttributes() {
    return { type: 'solid', color: json.deepClone(DEFAULT_LINECOLOR), width: 50 };
}

function getNoneShapeAttributes() {
    return { type: 'none', color: null, width: null };
}

function getHeadCharAttributes() {
    return { fontSize: 16, bold: false };
}

function getDefaultAttributes(docModel) {
    return json.deepClone({
        normal: {
            xAxis: AXIS_LABEL_LINE_ENABLED,
            xGrid: GRID_DISABLED,
            yAxis: AXIS_LABEL_ENABLED,
            yGrid: GRID_ENABLED
        },
        xy: {
            xAxis: AXIS_LABEL_LINE_ENABLED,
            xGrid: docModel.docApp.isOOXML() ? GRID_ENABLED : GRID_DISABLED,
            yAxis: AXIS_LABEL_LINE_ENABLED,
            yGrid: GRID_ENABLED
        },
        pie: {
            xAxis: AXIS_DISABLED,
            xGrid: GRID_DISABLED,
            yAxis: AXIS_DISABLED,
            yGrid: GRID_DISABLED
        }
    });
}

function getFormula(docModel, sheet, chart, from, to) {
    var range = new Range(from, to);

    if (chart) {
        // workaround for Bug 52802

        var formula = null;

        _.times(chart.getSeriesCount(), function (i) {
            var seriesData = chart.getSeriesModel(i);
            seriesData.iterateTokenArrays(function (tokenArray, linkKey) {
                if (formula) { return; }

                var oldRangeList = seriesData.resolveRanges(linkKey);
                if (oldRangeList.empty()) { return; }
                var oldRange = oldRangeList.first();

                if (oldRange.a1.equals(range.a1) && oldRange.a2.equals(range.a2)) {
                    formula = tokenArray.getFormulaOp();
                }
            });
        });

        if (formula) {
            return formula;
        }
    }

    var tokenArray = new TokenArray(docModel, "link");
    tokenArray.appendRange(range, { sheet, abs: true });
    return tokenArray.getFormulaOp();
}

function addDataSeries(docModel, generator, chartProperties, options) {

    var sourceSheet = options.sourceSheet;
    var position = options.position;
    var series = options.series;
    var namesFrom = options.namesFrom;
    var namesTo = options.namesTo;
    var title = options.title;
    var valueFrom = options.valueFrom;
    var valueTo = options.valueTo;
    var bubbleFrom = options.bubbleFrom;
    var bubbleTo = options.bubbleTo;
    var bubbleArray = options.bubbleArray;
    var lineAttrs = options.lineAttrs;
    var seriesData = options.seriesData;
    var colors = options.colors;
    var sourceChart = options.sourceChart;

    var insert = {};
    if (valueFrom) {
        insert.values = getFormula(docModel, sourceSheet, sourceChart, valueFrom, valueTo);
    }
    insert.type = seriesData.type;
    insert.dataLabel = seriesData.dataLabel;
    if (seriesData.curved) {
        insert.curved = seriesData.curved;
    }
    if (title) {
        insert.title = getFormula(docModel, sourceSheet, sourceChart, title);
    }
    if (namesFrom) {
        insert.names = getFormula(docModel, sourceSheet, sourceChart, namesFrom, namesTo);
    }
    if (bubbleFrom) {
        insert.bubbles = getFormula(docModel, sourceSheet, sourceChart, bubbleFrom, bubbleTo);
    } else if (bubbleArray) {
        insert.bubbles = bubbleArray;
    }

    var type = seriesData.type.replace('2d', '').replace('3d', '');
    if (type !== 'pie' && type !== 'donut') {
        if (type === 'bar') {
            insert.axisXIndex = 1;
            insert.axisYIndex = 0;
        } else {
            insert.axisXIndex = 0;
            insert.axisYIndex = 1;
        }
    }

    var properties = {
        series,
        attrs: { series: insert, chart: chartProperties, fill: { type: 'solid' } }
    };

    // TODO: _.clone( should not be needed, but it is, why?!
    if (lineAttrs) {
        properties.attrs.line = json.deepClone(lineAttrs);
    } else if (seriesData.markersOnly) {
        properties.attrs.line = getNoneShapeAttributes();
        properties.attrs.markerFill = getStandardShapeAttributes();
    } else {
        properties.attrs.line = getStandardShapeAttributes();
    }

    if (colors) {
        if (colors.length > 1) {
            var dataPoints = [];
            colors.forEach(function (color) {
                var dataPoint = {
                    fill: { type: 'solid', color },
                    line: { type: seriesData.markersOnly ? 'none' : 'solid', color }
                };
                if (seriesData.markersOnly) {
                    dataPoint.markerFill = dataPoint.fill;
                }
                dataPoints.push(dataPoint);
            });
            properties.attrs.series.dataPoints = dataPoints;
        }
        properties.attrs.line.color = colors[0];
        properties.attrs.fill.color = colors[0];
        if (seriesData.markersOnly) {
            properties.attrs.markerFill = colors[0];
        }
    }
    generator.generateDrawingOperation(Op.INSERT_CHART_SERIES, position, properties);
}

function generateSeriesOperations(options) {

    var docModel = options.docModel;
    var sourceRange = options.sourceRange;
    var sourceSheet = options.sourceSheet;
    var generator = options.generator;
    var position = options.position;
    var cells = options.cells;
    var axis = options.axis;
    var chart = options.chart;
    var seriesData = options.seriesData;
    var lineAttrs = options.lineAttrs;
    var sourceChart = options.sourceChart;

    // moved from createChart (old comment for this change:"odf support by Stefan 15 Feb 2017")
    // moved it to this position to use it for updateSeries type too
    if (!chart.chartStyleId) {
        chart.chartStyleId = 2;
    }

    var useAxis;
    // check if axis is set
    if (axis !== 0 && axis !== 1) {
        if (cells[0] === cells[1]) {
            useAxis = (sourceRange.cols() > sourceRange.rows()) ? 1 : 0;
        } else {
            useAxis = cells[0] ? 0 : 1;
        }
    } else {
        useAxis = axis;
    }

    var dataSeries;
    if (seriesData.type.indexOf('bubble') !== 0) {
        dataSeries = generateStandardSeriesOperations(docModel, sourceRange, sourceSheet, position, cells, useAxis, seriesData, lineAttrs, sourceChart, seriesData.type, options.switchRowColumn);
    } else if (docModel.getApp().isODF()) {
        dataSeries = generateBubbleODSSeriesOperations(docModel, sourceRange, sourceSheet, position, useAxis, seriesData, lineAttrs, sourceChart, options.switchRowColumn);
    } else {
        dataSeries = generateBubbleSeriesOperations(docModel, sourceRange, sourceSheet, position, useAxis, seriesData, lineAttrs, sourceChart, options.switchRowColumn);
    }

    if (chart.varyColors !== false && dataSeries.length > 1) {
        chart.varyColors = true;
    } else {
        chart.varyColors = false;
    }

    generator.generateDrawingOperation(Op.CHANGE_DRAWING, position, { attrs: { chart } });

    dataSeries.forEach(function (dataSerie) {
        addDataSeries(docModel, generator, chart, dataSerie);
    });

    return dataSeries.length;
}

function prepareSeriesColors(docModel, sourceColors, seriesCount, dataPointsCount, colorSetIndex, seriesType, transparent) {

    var count = 0;
    var pieDonut = /^(pie|donut)/.test(seriesType);
    var single = seriesCount === 1 || pieDonut;

    var varyColors = true;
    if (single) {
        count = !pieDonut ? 1 : dataPointsCount;
        if (!pieDonut || (count > 1 && sourceColors.length > 1 && !isAutoColor(sourceColors[0]) && _.isEqual(sourceColors[0], sourceColors[1]))) {
            varyColors = false;
        }
    } else {
        count = seriesCount;
    }

    var colorSet = null;
    var newColors = null;
    if (Number.isFinite(colorSetIndex)) {
        colorSet = getColorSet()[colorSetIndex];
        newColors = [];
    } else {
        colorSet = getColorSet()[1];
        newColors = sourceColors.slice(0);
    }

    for (var index = newColors.length; index < count; index++) {
        var color = getColorOfPattern('cycle', colorSet.type, varyColors ? index : 0, colorSet.colors, varyColors ? count : 3, docModel);
        if (transparent) { color = transformOpColor(color, { alpha: 75000 }); }
        newColors.push(color);
    }

    var result = [];
    if (single) {
        for (var i = 0; i < seriesCount; i++) {
            result.push(newColors);
        }
    } else {
        newColors.forEach(function (color) { result.push([color]); });
    }
    return result;
}

/**
 * generate all series from start to end.
 * Depending if first row/column contains a String,
 * it is defined as Names (datapoint) and Titles (dataseries)
 */
function generateStandardSeriesOperations(docModel, sourceRange, sourceSheet, position, cells, activeAxis, seriesData, lineAttrs, sourceChart, chartType, switchRowColumnByUser) {

    const { a1, a2 } = sourceRange;

    if (!switchRowColumnByUser && !docModel.getApp().isODF()) {
        var colCount = sourceRange.cols();
        var rowCount = sourceRange.rows();

        var titlePosition = getTitlePosition(docModel, a1, sourceSheet, colCount, rowCount);

        if (titlePosition === LEFT_TITLE || titlePosition === BOTH_TITLE) {
            colCount -= 1;
        }

        if (titlePosition === BOTH_TITLE) {
            rowCount -= 1;
        }

        if ((titlePosition === TOP_TITLE && rowCount - 1 > colCount) || (titlePosition !== TOP_TITLE && rowCount === colCount)) {
            activeAxis = 1 - activeAxis;
        }
    }

    const columns = !activeAxis;
    var max = a2.get(columns) - a1.get(columns);

    var namesFrom, namesTo;

    var noKeys = false;
    if (chartType.indexOf('scatter') === 0) {
        noKeys = max === 0 && !cells[activeAxis];
    } else {
        noKeys = !cells[activeAxis];
    }

    if (noKeys) {
        namesFrom = null;
        namesTo = null;
        a1.move(-1, columns);
        max += 1;
    } else {
        if (!cells[1 - activeAxis]) {
            namesFrom = a1;
        } else {
            namesFrom = Address.fromDir(columns, a1.get(columns), a1.get(!columns) + 1);
        }
        namesTo = Address.fromDir(columns, a1.get(columns), a2.get(!columns));
    }

    var seriesColors = prepareSeriesColors(docModel, seriesData.colors, max, (a2.get(!columns) - a1.get(!columns)) + 1, seriesData.colorSet, seriesData.type);
    var dataSeries = [];
    for (var i = 0; i < max; i++) {
        var activeIndex = a1.get(columns) + 1 + i;

        var title;
        var valueFrom;
        if (!cells[1 - activeAxis]) {
            title = null;
            valueFrom = Address.fromDir(columns, activeIndex, a1.get(!columns));
        } else {
            title = Address.fromDir(columns, activeIndex, a1.get(!columns));
            valueFrom = Address.fromDir(columns, activeIndex, a1.get(!columns) + 1);
        }
        var valueTo = Address.fromDir(columns, activeIndex, a2.get(!columns));

        dataSeries.push({
            sourceSheet,
            position,
            series: i,
            namesFrom,
            namesTo,
            title,
            valueFrom,
            valueTo,
            seriesData,
            lineAttrs,
            colors: seriesColors[i],
            sourceChart
        });
    }
    return dataSeries;
}

var NONE_TITLE = 0;
var TOP_TITLE  = 1;
var LEFT_TITLE = 2;
var BOTH_TITLE = 3;

function isStringCell(value) {
    return value !== '' && is.string(value);
}

function isRangeContentValueAString(index, rangeContents) {
    return rangeContents.length !== 0 && isStringCell(rangeContents[index].value);
}

function getTitlePosition(docModel, startRange, sourceSheet, colCount, rowCount) {
    var titlePos = NONE_TITLE;
    var res = docModel.getRangeContents(Range3D.fromAddress3D(startRange, sourceSheet), { blanks: true, attributes: true, display: true });
    var firstCellIsString = isRangeContentValueAString(0, res);

    var top = false;
    var left = false;
    if (colCount > 1) {
        res = docModel.getRangeContents(Range3D.fromAddress3D(new Address(startRange.c + 1, startRange.r), sourceSheet), { blanks: true, attributes: true, display: true });
        if (isRangeContentValueAString(0, res)) {
            top = true;
        } else if (rowCount === 1 && firstCellIsString) {
            left = true;
        }
    }
    if (rowCount > 1) {
        res = docModel.getRangeContents(Range3D.fromAddress3D(new Address(startRange.c, startRange.r + 1), sourceSheet), { blanks: true, attributes: true, display: true });
        if (isRangeContentValueAString(0, res)) {
            left = true;
        } else if (colCount === 1 && firstCellIsString) {
            top = true;
        }
    }

    if (top && left) {
        titlePos = BOTH_TITLE;
    } else if (top) {
        titlePos = TOP_TITLE;
    } else if (left) {
        titlePos = LEFT_TITLE;
    }

    return titlePos;
}

function getActiveAxis(colCount, rowCount, titlePos) {
    var activeAxis = 0;
    if (titlePos === TOP_TITLE) {
        if (colCount === 1 || colCount === rowCount - 1) {
            activeAxis = 1;
        } else if (rowCount <= 2) {
            activeAxis = 0;
        } else if (isEven(colCount) && colCount >= rowCount) {
            activeAxis = 1;
        }  else if (isOdd(colCount) && isEven(rowCount) && rowCount < colCount) {
            activeAxis = 1;
        }
    } else if (titlePos === LEFT_TITLE) {
        if (colCount === 1) {
            activeAxis = 1;
        } else if (rowCount === 1) {
            activeAxis = 0;
        } else if (rowCount === 2 || colCount === 2) {
            activeAxis = 1;
        } else if (rowCount - 1 === colCount) {
            activeAxis = 0;
        } else if (isEven(colCount)) {
            if (colCount > rowCount) {
                activeAxis = 1;
            }
        } else if (colCount >= rowCount || isOdd(rowCount)) {
            activeAxis = 1;
        }
    } else if (titlePos === BOTH_TITLE) {
        if (colCount === 2 || colCount === rowCount) {
            activeAxis = 1;
        } else if (rowCount === 2) {
            activeAxis = 0;
        } else if (rowCount === 3) {
            activeAxis = 1;
        } else if (isEven(colCount)) {
            if ((isOdd(rowCount) && rowCount >= colCount) || (colCount > rowCount)) {
                activeAxis = 1;
            }
        } else if (isOdd(rowCount) && colCount > rowCount) {
            activeAxis = 1;
        }
    } else {
        var evenColumnCount = isEven(colCount);
        var evenRowCount = isEven(rowCount);

        if (colCount === 1) {
            activeAxis = 1;
        } else if (rowCount === 1) {
            activeAxis = 0;
        } else if (colCount === rowCount) {
            activeAxis = 1;
        } else if (evenColumnCount && evenRowCount) {
            if (colCount > rowCount) {
                activeAxis = 1;
            }
        } else if ((evenColumnCount && rowCount > 1) || (!evenColumnCount && !evenRowCount && rowCount < colCount)) {
            activeAxis = 1;
        }
    }
    return activeAxis;
}

function isEven(number) {
    return !(number & 1);
}

function isOdd(number) {
    return number & 1;
}

function getActiveAxisForBubbleODS(titlePos, rowCount) {
    var activeAxis = 0;

    if (titlePos === TOP_TITLE || titlePos === BOTH_TITLE) {
        if (rowCount === 2) {
            activeAxis = 1;
        }
    } else if (rowCount === 1) {
        activeAxis = 1;
    }

    return activeAxis;
}

function generateBubbleODSSeriesOperations(docModel, sourceRange, sourceSheet, position, activeAxis, seriesData, lineAttrs, chartModel, switchRowColumnByUser) {
    var size = 0;

    const { a1, a2 } = sourceRange;

    var colCount = sourceRange.cols();
    var rowCount = sourceRange.rows();

    var titlePos = getTitlePosition(docModel, a1, sourceSheet, colCount, rowCount);

    if (!switchRowColumnByUser) {
        // activeAxis = 0 = Left To Right = Columns
        // activeAxis = 1 = Top to Bottom = Rows
        activeAxis = getActiveAxisForBubbleODS(titlePos, rowCount);
    }

    var activeStartIndex = null;
    var otherStartIndex = 0;
    var setName = null;
    var namesFrom, namesTo, seriesName;
    var setSeriesName = false;

    if (titlePos === TOP_TITLE) {
        if (switchRowColumnByUser) {
            if (rowCount === 2) {

            } else {
                setName = false;
                // datalabel title
            }
        } else {
            if (rowCount === 2) {
                activeStartIndex = 1;
                setName = false;
            } else {
                otherStartIndex = 1;
                setSeriesName = true;
            }
        }
    } else if (titlePos === LEFT_TITLE) {
        if (rowCount === 1) {
            activeStartIndex = 0;
            setSeriesName = true;
            otherStartIndex = 1;
        } else {
            activeStartIndex = 1;
            setName = false;
        }
    } else if (titlePos === BOTH_TITLE) {
        if (rowCount === 2) {
            activeStartIndex = 1;
            setSeriesName = true;
            otherStartIndex = 1;
            setName = false;
        } else {
            otherStartIndex = 1;
            setSeriesName = true;
            setName = false;
            activeStartIndex = 1;
        }
    }

    const columns = !activeAxis;
    var activeAxisCount = a2.get(columns) - a1.get(columns) + 1;
    var seriesColors = prepareSeriesColors(docModel, seriesData.colors, activeAxisCount / 2, (a2.get(!columns) - a1.get(!columns)) + 1, seriesData.colorSet, seriesData.type, true);

    if (activeStartIndex === null) {
        if (columns) {
            activeStartIndex = colCount === 1 || isEven(colCount) ? 0 : 1;
        } else {
            activeStartIndex = rowCount === 1 || isEven(rowCount) ? 0 : 1;
        }

    }

    var i = activeStartIndex;
    setName = setName === null ? i === 1 : setName;
    if (setName) {
        var activeNameIndex = a1.get(columns);
        var otherNameIndex = a1.get(columns) + otherStartIndex;

        namesFrom = Address.fromDir(columns, activeNameIndex, otherNameIndex);
        namesTo = Address.fromDir(columns, activeNameIndex, a2.get(!columns));
    }

    var dataSeries = [];

    for (; i < activeAxisCount; i += 2) {

        var activeIndex = a1.get(columns) + i;
        var otherIndex = a1.get(!columns) + otherStartIndex;

        var valuesFrom = null, valuesTo = null, bubbleFrom = null, bubbleTo = null;

        if (i + 2 <= activeAxisCount) {
            valuesFrom = Address.fromDir(columns, activeIndex, otherIndex);
            valuesTo = Address.fromDir(columns, activeIndex, a2.get(!columns));
            activeIndex++;
        }

        if (setSeriesName) {
            seriesName = Address.fromDir(columns, activeIndex, a1.get(!columns));
        }

        bubbleFrom = Address.fromDir(columns, activeIndex, otherIndex);
        bubbleTo = Address.fromDir(columns, activeIndex, a2.get(!columns));

        dataSeries.push({
            sourceSheet,
            position,
            series: size,
            namesFrom,
            namesTo,
            title: seriesName,
            valueFrom: valuesFrom,
            valueTo: valuesTo,
            bubbleFrom,
            bubbleTo,
            seriesData,
            lineAttrs,
            colors: seriesColors[size],
            sourceChart: chartModel,
            activeAxis // only needed for automated tests
        });
        size++;
    }

    return dataSeries;
}
function generateBubbleSeriesOperations(docModel, sourceRange, sourceSheet, position, activeAxis, seriesData, lineAttrs, chartModel, switchRowColumnByUser) {
    var size = 0;

    const { a1, a2 } = sourceRange;

    var colCount = sourceRange.cols();
    var rowCount = sourceRange.rows();

    var titlePos = getTitlePosition(docModel, a1, sourceSheet, colCount, rowCount);

    if (!switchRowColumnByUser) {
        // activeAxis = 0 = Left To Right = Columns
        // activeAxis = 1 = Top to Bottom = Rows
        activeAxis = getActiveAxis(colCount, rowCount, titlePos);
    }

    var activeStartIndex = null;
    var otherStartIndex = 0;
    var setXVal = false;
    var setSeriesName = false;
    const columns = !activeAxis;

    if (titlePos === TOP_TITLE) {
        if (columns) {

            if (rowCount === 1) {
                activeStartIndex = isEven(colCount) ? 1 : 0;
                setXVal = activeStartIndex && colCount > 2;
            } else {
                if (isEven(rowCount) || isEven(colCount) || colCount === 1) {
                    otherStartIndex = 1;
                }
                if (rowCount === 2) {
                    activeStartIndex = colCount > 1 && isOdd(colCount) ? 1 : 0;
                } else {
                    activeStartIndex = colCount !== 1 ? 1 : 0;
                }
                setXVal = activeStartIndex || (colCount === 1 && rowCount === 2);
            }
            setSeriesName = rowCount > 1 && colCount > 3 && (isEven(colCount) || isEven(rowCount));
        } else {
            activeStartIndex = (isEven(rowCount) && isOdd(colCount)) || (rowCount === 2 && isEven(colCount)) ? 1 : 0;
            setXVal = (rowCount > 2 && activeStartIndex) || (rowCount === 1 && colCount === 2) || (rowCount >= 2 && activeStartIndex && colCount > 1);
            otherStartIndex = rowCount === 1 || (colCount > 1 && isOdd(colCount) && !activeStartIndex) ? 1 : 0;
            setSeriesName = (colCount > 1 && isOdd(colCount)) && !activeStartIndex;
        }

    } else if (titlePos === LEFT_TITLE) {
        if (columns) {
            if (isEven(colCount)) {
                activeStartIndex = isEven(rowCount) && colCount > 2 ? 0 : 1;
                setXVal = (rowCount > 1 || colCount > 2) && activeStartIndex;
            } else {
                activeStartIndex = 0;
                otherStartIndex = colCount === 1 ? 1 : 0;
                setXVal = otherStartIndex === 1 && rowCount === 2;
            }
        } else {
            if (colCount <= 2) {
                activeStartIndex = (colCount === 1 && isEven(rowCount)) || (colCount > 1 && isOdd(rowCount) && rowCount > 1) ? 1 : 0;
                otherStartIndex = colCount === 2 ? 1 : 0;
                setXVal = (colCount === 1 && rowCount > 2 && activeStartIndex) || (colCount > 1 && isOdd(rowCount));
            } else {
                activeStartIndex = rowCount > 1 ? 1 : 0;
                otherStartIndex = 1;
                setXVal = activeStartIndex === 1;
            }
            setSeriesName = rowCount > 3 && colCount > 1;
        }
    } else if (titlePos === BOTH_TITLE) {
        otherStartIndex = 1;
        if (columns) {
            activeStartIndex = (rowCount === 2 && isOdd(colCount)) || (isOdd(rowCount) && (colCount > 2 && isEven(colCount))) ? 0 : 1;
            setSeriesName = colCount > 3 || rowCount === 2;
        } else {
            activeStartIndex = (colCount > 3 && ((isEven(colCount) && isEven(rowCount)) || isOdd(rowCount) || rowCount === 2)) || (colCount === 2 && isEven(rowCount)) || (colCount === 3 && (isOdd(rowCount) || rowCount === 2)) ? 1 : 0;
            setSeriesName = (colCount === 2 && rowCount > 2) || (colCount > 2 && rowCount !== 3);
        }
        setXVal = activeStartIndex === 1;
    }
    if (activeStartIndex === null) {
        if (columns) {
            activeStartIndex = colCount === 1 || isEven(colCount) ? 0 : 1;
        } else {
            activeStartIndex = rowCount === 1 || isEven(rowCount) ? 0 : 1;
        }

        setXVal = activeStartIndex === 1;
    }

    var activeAxisCount = a2.get(columns) - a1.get(columns) + 1;
    var xValFrom, xValTo;

    if (setXVal) {
        if (activeStartIndex) {
            xValFrom = Address.fromDir(columns, a1.get(columns), a1.get(!columns) + otherStartIndex);
            xValTo = Address.fromDir(columns, xValFrom.get(columns), a2.get(!columns));
        } else {
            xValFrom = Address.fromDir(columns, a1.get(columns), a1.get(!columns));
            xValTo = xValFrom; //Address.fromDir(columns, xValFrom.get(columns), end.get(!columns) - otherStartIndex);
        }
    }

    var seriesColors = prepareSeriesColors(docModel, seriesData.colors, activeAxisCount / 2, (a2.get(!columns) - a1.get(!columns)) + 1, seriesData.colorSet, seriesData.type, true);
    var i = activeStartIndex;
    var dataSeries = [];
    for (; i < activeAxisCount; i += 2) {
        var activeIndex = a1.get(columns) + i;
        var otherIndex = a1.get(!columns) + otherStartIndex;
        // Series name
        var seriesName = null;
        if (setSeriesName) {
            // Top-Left TB
            seriesName = Address.fromDir(columns, activeIndex, otherIndex - otherStartIndex);
        } else if (size === 0 && i + 2 >= activeAxisCount) {
            // Test if the cell is not used for the x values
            if (otherStartIndex && !(setXVal && activeIndex === xValFrom.get(columns) && a1.get(!columns) === otherIndex - 1)) {
                seriesName = Address.fromDir(columns, activeIndex, otherIndex - 1);
            } else if (activeStartIndex && !setXVal) {
                seriesName = Address.fromDir(columns, a1.get(columns), otherIndex);
            }
        }

        var bubbleArray = null;

        var yValFrom = Address.fromDir(columns, activeIndex, otherIndex);
        var yValTo = Address.fromDir(columns, activeIndex, a2.get(!columns));
        // Bubble Size
        var bubbleFrom = null, bubbleTo = null;
        if (i + 1 < activeAxisCount) {
            bubbleFrom = Address.fromDir(columns, yValFrom.get(columns) + 1, yValFrom.get(!columns));
            bubbleTo = Address.fromDir(columns, yValTo.get(columns) + 1, yValTo.get(!columns));
        } else if (!docModel.getApp().isODF()) {
            bubbleArray = [];
            var bubbleCount = yValTo.get(!columns) - yValFrom.get(!columns) + 1;
            for (var j = 0; j < bubbleCount; j++) {
                bubbleArray.push(1);
            }
        }

        dataSeries.push({
            sourceSheet,
            position,
            series: size,
            namesFrom: xValFrom,
            namesTo: xValTo,
            title: seriesName,
            valueFrom: yValFrom,
            valueTo: yValTo,
            bubbleFrom,
            bubbleTo,
            bubbleArray,
            seriesData,
            lineAttrs,
            colors: seriesColors[size],
            sourceChart: chartModel,
            activeAxis // only needed for automated tests
        });
        size++;
    }

    return dataSeries;
}

/**
 *
 * @param {type} docModel
 * @param {type} docView
 * @param {type} sourceRange
 * @param {type} sourceSheet
 * @param {Number} axis only set if it is a bubble chart
 * @param {type} forceTitle
 * @param {type} forceNames
 * @returns {Promise}
 */
function getContentForRange(docModel, docView, sourceRange, sourceSheet, axis, forceTitle, forceNames) {

    var sheetModel = docModel.getSheetModel(sourceSheet);

    var a1 = sourceRange.a1;
    var colSize = sourceRange.cols();
    var rowSize = sourceRange.rows();

    if (colSize > 50 || rowSize > 50) {
        docView.yell({ type: 'warning', message: gt('It is not possible to create a chart out of more than 50 input cells.') });
        return jpromise.reject();
    }

    const rightIndex = sheetModel.colCollection.findNextVisibleIndex(a1.c + 1);
    const bottIndex = sheetModel.rowCollection.findNextVisibleIndex(a1.r + 1);

    var rightAddress = new Address(rightIndex ?? a1.c, a1.r);
    var bottAddress = new Address(a1.c, bottIndex ?? a1.r);

    var ranges = new Range3DArray(
        Range3D.fromAddress3D(bottAddress, sourceSheet),
        Range3D.fromAddress3D(rightAddress, sourceSheet),
        Range3D.fromAddress3D(a1, sourceSheet)
    );

    // first array element of the result is an array with the contents of the three cells
    var res = docModel.getRangeContents(ranges, { blanks: true, attributes: true, display: true });
    var bottomCell = res[0];
    var rightCell = res[1];
    var startCell = res[2];

    function isLabelCell(cell) {
        if (!startCell) { return false; }
        if (!startCell.display) { return true; } // empty strings (blank cells) or null values (invalid number format)
        return cell.format.isAnyDateTime() || !is.number(cell.value);
    }

    var bottom = false;
    var right = false;

    if (colSize === 1 || rowSize === 1) {
        // axis is not set
        if (axis !== 0 && axis !== 1) {
            if (colSize === 1) {
                axis = 0;
            } else {
                axis = 1;
            }
        }
        var firstString = !is.number(startCell.value);
        if (axis === 1) {
            if (firstString) {
                bottom = true;
            }
        } else {
            if (firstString) {
                right = true;
            }
        }
    } else {
        bottom = isLabelCell(bottomCell);
        right = isLabelCell(rightCell);
    }

    if (is.boolean(forceTitle)) {
        if (axis === 0) {
            right = forceTitle;
        } else if (axis === 1) {
            bottom = forceTitle;
        }
    }
    if (is.boolean(forceNames)) {
        if (axis === 0) {
            bottom = forceNames;
        } else if (axis === 1) {
            right = forceNames;
        }
    }
    if (colSize === 1) {
        bottom = false;
    }
    if (rowSize === 1) {
        right = false;
    }

    return docModel.createResolvedPromise({ cells: [bottom, right], axis });
}

function prepareFallbackValueAttrs(docModel, attrs) {
    if (!attrs) {
        return;
    }
    prepareFallbackValueShape(docModel, attrs.line);
    prepareFallbackValueShape(docModel, attrs.fill);

    return attrs;
}

function prepareFallbackValueShape(docModel, shape) {
    if (!shape) {
        return;
    }
    if (shape.color && shape.color.type === 'scheme') {
        shape.color.fallbackValue = docModel.parseAndResolveColor(shape.color, 'fill').hex;
    }
    return shape;
}

// public functions ===========================================================

/**
 * Creates and inserts a new chart into the active sheet.
 *
 * @returns {jQuery.Promise}
 *  A promise that will be resolved after the chart has been created and
 *  inserted successfully, or that will be rejected on any error.
 */
export function createChart(app, chartData) {

    var docModel = app.docModel;
    var docView = app.docView;
    var gridPane = docView.getActiveGridPane();

    const { selectionEngine } = docView;

    var range = selectionEngine.getActiveRange();

    var forceNames;
    var axis;

    var chartAttrs = {
        stacking: chartData.stacking,
        curved: chartData.curved,
        sovcd: chartData.sovcd // see only visible cell data
    };

    var seriesData = {
        type: chartData.series.type,
        markersOnly: chartData.series.markersOnly,
        colors: [],
        curved: chartData.curved
    };

    if (seriesData.type.indexOf('bubble') !== -1 && Math.min(range.cols(), range.rows()) > 2) {
        forceNames = true;
        // TODO:YRI Is this needed, calculated in generateBubbleSeriesOperations(...) too
        if (range.cols() > range.rows()) {
            axis = 1;
        } else {
            axis = 0;
        }
    }

    var promise = getContentForRange(docModel, docView, range, docView.activeSheet, axis, undefined, forceNames);

    promise = promise.then(function (data) {

        var visibleRect = gridPane.getVisibleRectangle();

        var chartRect = new Rectangle(
            Math.max(0, Math.round(visibleRect.left + (visibleRect.width - CHART_DEFAULT_WIDTH) / 2)),
            Math.max(0, Math.round(visibleRect.top + (visibleRect.height - CHART_DEFAULT_HEIGHT) / 2)),
            CHART_DEFAULT_WIDTH,
            CHART_DEFAULT_HEIGHT
        );

        var anchorAttrs = docView.drawingCollection.getAnchorAttributesForRect(AnchorMode.TWO_CELL, chartRect, { pixel: true });
        var drawingDesc = { type: 'chart', attrs: { drawing: anchorAttrs } };
        drawingDesc.attrs.fill = { type: 'solid', color: Color.LIGHT1 };
        drawingDesc.attrs.line = { type: 'none' };

        return app.getController().execInsertDrawings([drawingDesc], function (generator, sheet, position) {

            var seriesCount = generateSeriesOperations({ docModel, sourceRange: range, sourceSheet: sheet, generator, position, cells: data.cells, axis: data.axis, chart: chartAttrs, seriesData, switchRowColumn: false });

            var defaults = getDefaultAttributes(docModel).normal;
            var createAxis = true;
            var type = seriesData.type.replace('2d', '').replace('3d', '');
            var x = 0;
            var y = 1;
            var allwaysShowLegend = false;
            switch (type) {
                case 'bubble':
                case 'scatter':
                    defaults = getDefaultAttributes(docModel).xy;
                    break;
                case 'pie':
                case 'donut':
                    createAxis = false;
                    allwaysShowLegend = true;
                    break;
                case 'bar':
                    x = 1;
                    y = 0;
                    break;
            }

            const legendPos = (allwaysShowLegend || seriesCount > 1) ? 'bottom' : 'off';
            generator.generateDrawingOperation(Op.CHANGE_CHART_LEGEND, position, { attrs: { legend: { pos:  legendPos } } });

            if (createAxis) {
                var xAxisDefault = defaults.xAxis;
                var yAxisDefault = defaults.yAxis;
                var xGridDefault = defaults.xGrid;
                var yGridDefault = defaults.yGrid;

                if (type === 'bar') {
                    xAxisDefault = defaults.yAxis;
                    yAxisDefault = defaults.xAxis;
                    xGridDefault = defaults.yGrid;
                    yGridDefault = defaults.xGrid;
                }
                generator.generateDrawingOperation(Op.CHANGE_CHART_AXIS, position, { axis: x, axPos: 'b', crossAx: y, attrs: prepareFallbackValueAttrs(docModel, xAxisDefault) });
                generator.generateDrawingOperation(Op.CHANGE_CHART_AXIS, position, { axis: y, axPos: 'l', crossAx: x, attrs: prepareFallbackValueAttrs(docModel, yAxisDefault) });
                generator.generateDrawingOperation(Op.CHANGE_CHART_GRID, position, { axis: x, attrs: prepareFallbackValueAttrs(docModel, xGridDefault) });
                generator.generateDrawingOperation(Op.CHANGE_CHART_GRID, position, { axis: y, attrs: prepareFallbackValueAttrs(docModel, yGridDefault) });
            }
        });
    });

    return docView.yellOnFailure(promise);
}

/**
 *
 * @param {Object} options
 *  - {Range} [options.range]
 *      whole start to end area of the new series
 *
 *  - {Number} [options.sheet]
 *
 *  - {ChartModel} [options.chartModel]
 *      is used for the position and id in the document and as fallback for chart-type-checks
 *
 *  - {String} [options.axis]
 *      series direction
 *
 *  - {Object} [options.chartData]
 *      is used for changing other attributes of the chart and for the type info (bubble or not)
 *
 *  - {Boolean} [options.forceTitle]
 *      is set, it overwrites the internal logic for title data
 *
 *  - {Boolean} [options.forceNames]
 *      is set, it overwrites the internal logic for names data
 *
 * @returns {jQuery.Promise}
 *  A promise that will be resolved after the chart has been changed
 *  successfully, or that will be rejected on any error.
 */
export function updateSeries(options) {

    var app         = getOption(options, 'app');
    var sourceRange = getOption(options, 'sourceRange');
    var sourceSheet = getOption(options, 'sourceSheet');
    var chartModel  = getOption(options, 'chartModel');
    var axis        = getOption(options, 'axis');
    var chartData   = getOption(options, 'chartData');
    var forceTitle  = getOption(options, 'forceTitle');
    var forceNames  = getOption(options, 'forceNames');
    var switchRowColumn  = getOption(options, 'switchRowColumn', false);
    // var typeChanged = getOption(options, 'typeChanged');

    var chartAttrs = null;
    var seriesData = null;
    var mergedAttributes = chartModel.getMergedAttributeSet(true);

    if (chartData) {
        chartAttrs = {
            stacking: chartData.stacking,
            curved: chartData.curved
        };
        seriesData = chartData.series;
        seriesData.curved = chartData.curved;
    } else {
        //CURVED is a workaround, filter has not all the information it needs!
        chartAttrs = {
            stacking: mergedAttributes.chart.stacking,
            curved: mergedAttributes.chart.curved
        };
        seriesData = {
            type: chartModel.getChartType().split(' ', 1)[0],
            markersOnly: chartModel.isMarkerOnly(),
            curved: mergedAttributes.chart.curved
        };
    }

    seriesData.colors = [];
    seriesData.dataLabel = chartModel.getDataLabel();

    chartAttrs.chartStyleId = mergedAttributes.chart.chartStyleId;
    chartAttrs.varyColors = chartModel.isVaryColor();
    chartAttrs.sovcd = chartModel.showOnlyVisibleCellsData();

    if (chartAttrs.chartStyleId) {
        seriesData.colorSet = chartModel.getColorSet().replace('cs', '') | 0;
    } else {
        seriesData.colorSet = null;
    }

    var clonedData = chartModel.getCloneData();
    var undoAttrs = chartModel.getUndoAttributeSet({ chart: chartAttrs });

    var docModel = app.getModel();
    var docView = app.getView();

    var promise = getContentForRange(docModel, docView, sourceRange, sourceSheet, axis, forceTitle, forceNames);

    promise = promise.then(function (data) {

        //only overwritten if source range height is 1 oder width is 1
        if (data.axis === 0 || data.axis === 1) {
            axis = data.axis;
        }
        var position = chartModel.getPosition();
        var lineAttrs = null;

        return chartModel.sheetModel.createAndApplyOperations(function (generator) {

            var oldCount = chartModel.getSeriesCount();
            var seriesCount = generateSeriesOperations({ docModel, sourceRange, sourceSheet, generator, position, cells: data.cells, axis, chart: chartAttrs, seriesData, lineAttrs, sourceChart: chartModel, switchRowColumn });

            _.times(oldCount, function () {
                generator.generateDrawingOperation(Op.DELETE_CHART_SERIES, position, { series: seriesCount });
            });

            // UNDO ---
            generator.generateDrawingOperation(Op.CHANGE_DRAWING, position, { attrs: undoAttrs }, { undo: true });

            var defaults = getDefaultAttributes(docModel).normal;
            var createAxis = true;
            var type = seriesData.type.replace('2d', '').replace('3d', '');
            var x = 0;
            var y = 1;
            switch (type) {
                case 'bubble':
                case 'scatter':
                    defaults = getDefaultAttributes(docModel).xy;
                    break;
                case 'pie':
                case 'donut':
                    createAxis = false;
                    break;
                case 'bar':
                    x = 1;
                    y = 0;
                    break;
            }

            var xAxisIdOld = chartModel.getAxisIdForType('x');
            var yAxisIdOld = chartModel.getAxisIdForType('y');
            if (createAxis) {
                generator.generateDrawingOperation(Op.DELETE_CHART_AXIS, position, { axis: x }, { undo: true });
                generator.generateDrawingOperation(Op.DELETE_CHART_AXIS, position, { axis: y }, { undo: true });
            }

            _.each(clonedData.series, function (obj, index) {
                generator.generateDrawingOperation(Op.INSERT_CHART_SERIES, position, { series: index, attrs: obj }, { undo: true });
            });

            chartModel.removeAllAxis(generator, position);

            if (!is.empty(clonedData.title)) {
                generator.generateDrawingOperation(Op.CHANGE_CHART_TITLE, position, { attrs: clonedData.title });
            }
            if (!is.empty(clonedData.legend)) {
                generator.generateDrawingOperation(Op.CHANGE_CHART_LEGEND, position, { attrs: clonedData.legend });
            }

            if (createAxis) {
                var xTitle;
                var yTitle;
                if (xAxisIdOld !== null) {
                    var xAxis = clonedData.axes[xAxisIdOld];
                    xTitle = xAxis.title;
                    defaults.xAxis = xAxis.axis;
                    defaults.xGrid = xAxis.grid;
                }
                if (yAxisIdOld !== null) {
                    var yAxis = clonedData.axes[yAxisIdOld];
                    yTitle = yAxis.title;
                    defaults.yAxis = yAxis.axis;
                    defaults.yGrid = yAxis.grid;
                }

                generator.generateDrawingOperation(Op.CHANGE_CHART_AXIS, position, { axis: x, axPos: 'b', crossAx: y, attrs: prepareFallbackValueAttrs(docModel, defaults.xAxis) });
                generator.generateDrawingOperation(Op.CHANGE_CHART_AXIS, position, { axis: y, axPos: 'l', crossAx: x, attrs: prepareFallbackValueAttrs(docModel, defaults.yAxis) });
                if (!is.empty(xTitle)) {
                    generator.generateDrawingOperation(Op.CHANGE_CHART_TITLE, position, { axis: x, attrs: xTitle });
                }
                if (!is.empty(yTitle)) {
                    generator.generateDrawingOperation(Op.CHANGE_CHART_TITLE, position, { axis: y, attrs: yTitle });
                }
                var xGridAttrs = prepareFallbackValueAttrs(docModel, defaults.xGrid);
                var yGridAttrs = prepareFallbackValueAttrs(docModel, defaults.yGrid);

                if (!is.empty(xGridAttrs)) {
                    generator.generateDrawingOperation(Op.CHANGE_CHART_GRID, position, { axis: x, attrs: xGridAttrs });
                }
                if (!is.empty(yGridAttrs)) {
                    generator.generateDrawingOperation(Op.CHANGE_CHART_GRID, position, { axis: y, attrs: yGridAttrs });
                }
            }

            _.times(seriesCount, function () {
                generator.generateDrawingOperation(Op.DELETE_CHART_SERIES, position, { series: oldCount }, { undo: true });
            });

        }, { storeSelection: true });
    });

    return promise;
}

export function generateOperationsFromModelData(generator, position, chartModelData) {

    var axes = chartModelData.axes;
    var series = chartModelData.series;
    var title = chartModelData.title;
    var legend = chartModelData.legend;

    _.each(series, function (serie, index) {
        var properties = {
            series: index,
            attrs: serie
        };
        generator.generateDrawingOperation(Op.INSERT_CHART_SERIES, position, properties);
    });

    _.each(axes, function (axisData, axisId) {
        axisId = window.parseInt(axisId, 10);
        if (!is.empty(axisData.axis)) {
            var properties = { axis: axisId, axPos: axisData.axPos, crossAx: axisData.crossAx, attrs: axisData.axis };
            if (axisData.zAxis) {
                properties.zAxis = axisData.zAxis;
            }
            generator.generateDrawingOperation(Op.CHANGE_CHART_AXIS, position, properties);
        }
        if (!is.empty(axisData.grid)) {
            generator.generateDrawingOperation(Op.CHANGE_CHART_GRID, position, { axis: axisId, attrs: axisData.grid });
        }
        if (!is.empty(axisData.title)) {
            generator.generateDrawingOperation(Op.CHANGE_CHART_TITLE, position, { axis: axisId, attrs: axisData.title });
        }
    });

    if (!is.empty(title)) {
        generator.generateDrawingOperation(Op.CHANGE_CHART_TITLE, position, { attrs: title });
    }

    if (!is.empty(legend)) {
        generator.generateDrawingOperation(Op.CHANGE_CHART_LEGEND, position, { attrs: legend });
    }
}

export function getStandardShape(docModel) {
    return prepareFallbackValueShape(docModel, getStandardShapeAttributes());
}

export function getHeadChar(/*docModel*/) {
    return getHeadCharAttributes();
}

export function getNoneShape() {
    return getNoneShapeAttributes();
}

/**
 * Only needed for JUnit test.
 */
export function getDataForBubbleSeriesOperations(docModel, sourceRange, sourceSheet, activeAxis, switchRowColumnByUser) {

    if (activeAxis === undefined) {
        activeAxis = null;
    }
    if (switchRowColumnByUser === undefined) {
        switchRowColumnByUser = false;
    }

    return generateBubbleSeriesOperations(docModel, sourceRange, sourceSheet, null, activeAxis, { colors: [], colorSet: [], type: 'bubble' }, null, null, switchRowColumnByUser);
}
