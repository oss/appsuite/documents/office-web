/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import { setFocus } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { Logger } from "@/io.ox/office/tk/utils/logger";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";
import type { PtParagraphAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";

import type { YellMessageCode } from "@/io.ox/office/spreadsheet/view/labels";
import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * Optional parameters passed to the method `TextEditorBase#implEnterEdit`.
 * Intended to be extended by subclasses.
 */
export interface TextEditEnterOptions { }

/**
 * Optional parameters passed to the method `TextEditorBase#implLeaveEdit`.
 * Intended to be extended by subclasses.
 */
export interface TextEditLeaveOptions { }

/**
 * Type mapping for the events emitted by `TextEditorBase` instances.
 */
export interface TextEditorBaseEventMap {

    /**
     * Will be emitted after entering the text edit mode.
     *
     * @param editMode
     *  The identifier of the edit mode implemented by the emitter.
     */
    "textedit:enter": [editMode: string];

    /**
     * Will be emitted after changing text while text edit mode is active
     * (editing the text contents of a cell or a drawing)
     *
     * @param editMode
     *  The identifier of the edit mode implemented by the emitter.
     */
    "textedit:change": [editMode: string];

    /**
     * Will be emitted after leaving the text edit mode.
     *
     * @param editMode
     *  The identifier of the edit mode implemented by the emitter.
     */
    "textedit:leave": [editMode: string];
}

// module logger ==============================================================

export const editLogger = new Logger("spreadsheet:log-editor", { tag: "EDIT", tagColor: 0x00FF00 });

// class TextEditorBase =======================================================

/**
 * Base class for different implementations of in-place text edit modes in a
 * spreadsheet document.
 */
export abstract class TextEditorBase extends ViewObject<SpreadsheetView, TextEditorBaseEventMap> {

    /**
     * The identifier of the edit mode implemented by this instance.
     */
    readonly editMode: string;

    /**
     * The active (focused) grid pane instance.
     */
    protected editGridPane: Opt<GridPane>;

    /**
     * Specifies whether text edit mode is currently active.
     */
    #editActive = false;

    /**
     * Specifies whether leaving edit mode has failed once with an alert.
     */
    #hasLeaveAlert = false;

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view that has created this instance.
     *
     * @param editMode
     *  The identifier of the edit mode implemented by the subclass.
     */
    protected constructor(docView: SpreadsheetView, editMode: string) {

        super(docView);
        this.editMode = editMode;

        // cancel edit mode when the application loses edit rights
        this.listenTo(this.docApp, "docs:editmode:leave", this.cancelEditMode);
    }

    protected override destructor(): void {
        this.cancelEditMode();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this text editor is currently active (text edit mode has
     * been started).
     *
     * @returns
     *  Whether the text edit mode is currently active.
     */
    isActive(): boolean {
        return this.#editActive;
    }

    /**
     * Starts text edit mode in the active grid pane.
     *
     * @param [options]
     *  Optional parameters forwarded to the internal implementation method
     *  `implEnterEdit`. The supported options depend on the actual subclass
     *  implementation.
     *
     * @returns
     *  The value `true`, if text edit mode has been started, or is already
     *  running. If text edit mode could not be activated due to any error,
     *  `false` will be returned.
     */
    @editLogger.profileMethod("$badge{TextEditorBase} enterEditMode")
    enterEditMode(options?: TextEditEnterOptions): boolean {

        // check whether this editor is already active
        if (this.#editActive) { return true; }

        // check global document edit mode (yells by itself)
        if (!this.docView.requireEditMode()) { return false; }

        // initialize class members
        this.#editActive = true;
        this.#hasLeaveAlert = false;
        this.editGridPane = this.docView.getActiveGridPane();

        // for safety, always cancel tracking mode when starting edit mode
        TrackingObserver.cancelActive();

        // invoke the subclass implementation
        const response = this.implEnterEdit(options);
        editLogger.log(() => `response=${response}`);

        // notify listeners, if edit mode has been started successfully
        if (response === true) {
            this.grabFocus();
            this.triggerEvent("enter");
            return true;
        }

        // cleanup after callback handler has rejected edit mode
        this.#editActive = false;
        this.editGridPane = undefined;

        // show warning alert if necessary
        if (response) { this.docView.yellMessage(response); }
        return false;
    }

    /**
     * Leaves the current text edit mode, and commits the pending changes if
     * necessary.
     *
     * @param [options]
     *  Optional parameters forwarded to the internal implementation method
     *  `implLeaveEdit`. The supported options depend on the actual subclass
     *  implementation.
     *
     * @returns
     *  The value `true`, if the current text edit mode has been left
     *  successfully. The value `false`, if the implementation has decided to
     *  keep the current text edit mode active.
     */
    @editLogger.profileMethod("$badge{TextEditorBase} leaveEditMode")
    leaveEditMode(options?: TextEditLeaveOptions): boolean {

        // nothing to do, if text edit mode is not active
        if (!this.#editActive) { return true; }

        // ignore edit mode, if the document does not have edit rights anymore
        if (!this.docView.isEditable()) {
            this.cancelEditMode();
            return true;
        }

        // invoke the subclass implementation
        const response = this.implLeaveEdit(options);
        editLogger.log(() => `response=${response}`);

        // deinitialize all edit mode settings, if edit mode has been left successfully
        if (response === true) {
            this.cancelEditMode();
            return true;
        }

        // immediately cancel active tracking sequence if edit mode cannot be left
        TrackingObserver.cancelActive();
        // grab back focus (deferred execution needed in Chrome)
        this.setTimeout(() => this.grabFocus(), 0);

        // show warning alert if necessary
        if (response) {
            this.#hasLeaveAlert = this.docView.yellMessage(response);
        }

        return false;
    }

    /**
     * Cancels the current text edit mode immediately, without committing any
     * pending changes.
     */
    @editLogger.profileMethod("$badge{TextEditorBase} cancelEditMode")
    cancelEditMode(): void {

        // nothing to do, if edit mode is not active (anymore)
        if (!this.#editActive) { return; }
        this.#editActive = false;

        // custom cleanup implemented in subclass
        this.implCancelEdit();

        // clean up grid pane
        this.editGridPane = undefined;

        // remove custom status labels
        this.docView.setStatusLabel(null);

        // notify listeners
        this.triggerEvent("leave");

        // bug 47936: hide previously shown warning alert
        if (this.#hasLeaveAlert) {
            this.docView.hideYell();
        }

        // focus back to document view
        this.docView.grabFocus();
    }

    /**
     * Returns the merged formatting attributes used in the current text edit
     * mode.
     *
     * @returns
     *  The attribute set with formatting attributes used in the current text
     *  edit mode; or `undefined`, if the text edit mode is currently inactive.
     *  Depending on the actual edit mode, the attribute set may contain
     *  different attribute families.
     */
    getAttributeSet(): Opt<Dict> {
        return this.#editActive ? this.implGetAttrs() : undefined;
    }

    /**
     * Modifies formatting attributes while the text edit mode is active.
     *
     * @param [attrSet]
     *  An incomplete attribute set with the new attributes to be applied to
     *  the current selection in the current text edit mode. If the parameter
     *  has been omitted, this method will do nothing.
     *
     * @returns
     *  A promise that will fulfil when the formatting attributes have been set
     *  successfully.
     */
    setAttributeSet(attrSet?: Dict): JPromise {

        // no edit mode active
        if (!this.#editActive) { return jpromise.reject(); }

        // nothing to do without attributes
        if (!is.dict(attrSet) || is.empty(attrSet)) { return jpromise.resolve(); }

        // invoke the format handler, convert the result to a promise
        return jpromise.invoke(() => this.implSetAttrs(attrSet));
    }

    /**
     * Returns the DOM node intended to contain the browser focus while text
     * edit mode is active.
     *
     * @returns
     *  The DOM node intended to contain the browser focus while text edit mode
     *  is active; or `undefined`, if this text editor is not active.
     */
    getFocusNode(): Opt<HTMLElement> {
        return this.#editActive ? this.implGetFocusNode() : undefined;
    }

    /**
     * Sets the browser focus to the text contents currently edited.
     */
    grabFocus(): void {
        const focusElem = this.getFocusNode();
        if (focusElem) { setFocus(focusElem); }
    }

    /**
     * Returns whether this text editor contains changes that need to be saved
     * when leaving text edit mode.
     *
     * @returns
     *  Whether this text editor contains unsaved changes.
     */
    hasUnsavedChanges(): boolean {
        return this.#editActive && this.implIsUnsaved();
    }

    // protected methods ------------------------------------------------------

    /**
     * Triggers the specified text edit mode event.
     *
     * @param state
     *  The state notified by the events triggered by this method, e.g. "enter"
     *  when entering the text edit mode, or "leave" when leaving the text edit
     *  mode.
     */
    protected triggerEvent(state: "enter" | "leave" | "change"): void {
        this.trigger(`textedit:${state}`, this.editMode);
    }

    // subclass API -----------------------------------------------------------

    /**
     * Will be invoked from the public method `enterEditMode`.
     *
     * @param [options]
     *  Optional parameters passed to the public method `enterEditMode`.
     *
     * @returns
     *  The value `true`, if text edit mode has been started successfully; or
     *  the value `false` or a string (an error code to be alerted), if text
     *  edit mode has not been started.
     */
    protected abstract implEnterEdit(config?: TextEditEnterOptions): boolean | YellMessageCode;

    /**
     * Will be invoked from the public method `leaveEditMode()`.
     *
     * @param [options]
     *  Optional parameters passed to the public method `leaveEditMode`.
     *
     * @returns
     *  The value `true`, if the edit mode has been left successfully; or the
     *  value `false` or a string (an error code to be alerted), if text edit
     *  mode remains active.
     */
    protected abstract implLeaveEdit(config?: TextEditLeaveOptions): boolean | YellMessageCode;

    /**
     * Will be invoked from the public method `cancelEditMode`.
     */
    protected abstract implCancelEdit(): void;

    /**
     * Will be called from the public method `getAttributeSet`.
     *
     * @returns
     *  The attribute set with formatting attributes used in the current text
     *  edit mode.
     */
    protected abstract implGetAttrs(): PtParagraphAttributeSet;

    /**
     * Will be invoked from the public method `setAttributeSet`.
     *
     * @param attrSet
     *  An incomplete attribute set with the new attributes to be applied.
     */
    protected abstract implSetAttrs(attrSet: PtParagraphAttributeSet): MaybeAsync;

    /**
     * Will be invoked from the public method `getFocusNode`.
     *
     * @returns
     *  The DOM node intended to contain the browser focus.
     */
    protected abstract implGetFocusNode(): HTMLElement;

    /**
     * Will be called from the public method `hasUnsavedChanges`.
     *
     * @returns
     *  Whether there are unsaved changes during text edit mode.
     */
    protected abstract implIsUnsaved(): boolean;
}
