/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is, math, str, ary, map, dict, jpromise } from "@/io.ox/office/tk/algorithms";
import {
    TOUCH_DEVICE, SMALL_DEVICE, IOS_SAFARI_DEVICE, SCROLLBAR_WIDTH,
    createElement, createSpan, setFocus, isFocused, focusLogger, createTextArea,
    getInputSelection, isCursorSelection, setInputSelection, replaceInputSelection,
    KeyCode, hasKeyCode, matchKeyCode, isEscapeKey, getSchemeColorIndex
} from "@/io.ox/office/tk/dom";
import { CHROME_ON_ANDROID, getIntegerOption, getStringOption } from "@/io.ox/office/tk/utils";
import { BUTTON_SELECTOR, FOCUSED_CLASS, getButtonValue } from "@/io.ox/office/tk/forms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { ToolTip } from "@/io.ox/office/tk/popup/tooltip";
import { ListMenu } from "@/io.ox/office/tk/popup/listmenu";

import { Color } from "@/io.ox/office/editframework/utils/color";
import { cloneAttributeSet, insertAttribute, getCssTextDecoration } from "@/io.ox/office/editframework/utils/attributeutils";

import { isStickyPopupsMode } from "@/io.ox/office/spreadsheet/utils/config";
import { hasWrappingAttributes } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { Address, Range, Direction, FindMatchType, splitDisplayString, splitFormulaParts } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import { getCellMoveDirection } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import { SignatureToolTip } from "@/io.ox/office/spreadsheet/view/popup/signaturetooltip";
import { editLogger, TextEditorBase } from "@/io.ox/office/spreadsheet/view/edit/texteditorbase";
import { CellUndoManager } from "@/io.ox/office/spreadsheet/view/edit/cellundomanager";

// types ======================================================================

/**
 * Specifies how to leave the cell edit mode.
 */
/*enum*/ const CommitMode = {

    /**
     * The current value will be committed to the active cell of the selection.
     */
    CELL: "cell",

    /**
     * The current value will be filled to all cells of the selection.
     */
    FILL: "fill",

    /**
     * The current value will be inserted as matrix formula into the selected
     * cell range.
     */
    MATRIX: "matrix"
};

// constants ==================================================================

// DOCS-735: touch devices: move cell text input node to top of application window
const EDIT_ON_TOP = TOUCH_DEVICE;

// key codes of all keys used to navigate in text area, or in the sheet (quick edit mode)
const NAVIGATION_KEYCODES = new Set([
    KeyCode.LEFT_ARROW,
    KeyCode.RIGHT_ARROW,
    KeyCode.UP_ARROW,
    KeyCode.DOWN_ARROW,
    KeyCode.PAGE_UP,
    KeyCode.PAGE_DOWN,
    KeyCode.HOME,
    KeyCode.END
]);

// private functions ==========================================================

/**
 * Serializes the list entries of the autocompletion popup menu for test
 * automation.
 *
 * @param {object} entry
 *  The list entry to be serialized.
 *
 * @returns {string}
 *  The value for the "data-value" element attribute.
 */
function serializeListEntry(entry) {
    return `${entry.type}:${entry.key}`;
}

// class CellTextEditor =======================================================

/**
 * Implementation of the text edit mode in cells.
 *
 * @param {SpreadsheetView} docView
 *  The document view that has created this instance.
 */
export class CellTextEditor extends TextEditorBase {

    constructor(docView) {

        // base constructor
        super(docView, "cell");

        // scope symbol for unbinding event listeners
        this._scope = Symbol("scope");

        // the underlay node containing highlighting for formulas
        this._cellUnderlayNode = $('<div class="underlay">');
        // the text area control
        this._cellInputNode = $(createTextArea());
        // the container element for the text underlay node and the text area control
        this._cellInputContainer = $('<div class="textarea-container noI18n" data-focus-role="textarea">').append(this._cellUnderlayNode, this._cellInputNode);

        // whether the focus will be in the formula tool pane
        this._formulaBarMode = false;
        // the underlay node containing highlighting for formulas in the formula tool pane
        this._paneUnderlayNode = null;
        // the text area control in the formula tool pane
        this._paneInputNode = null;

        // the effective DOM node for text input, also used as anchor node for popup nodes
        this._textInputNode = this._cellInputNode;
        // the effective DOM node for text output (mirrored from text input node)
        this._textOutputNode = this._cellInputNode;
        // the current text selection (needed to restore in focus/blur handling)
        this._textSelection = null;
        // minimum size of the text area, according to initial text (always hide rendered text in canvas)
        this._initialMinWidth = -1;
        this._initialMinHeight = -1;

        // tooltip attached to the text area for function/parameter help
        this._signatureToolTip = this.member(new SignatureToolTip(docView, () => this._textInputNode));
        // dropdown list attached to the text area (for function names)
        this._textAreaListMenu = this.member(new ListMenu({ anchor: () => this._textInputNode, autoFocus: false, focusableNodes: this._cellInputNode, valueSerializer: serializeListEntry }));
        // tooltip attached to the text area dropdown list (for the selected function)
        this._textAreaListToolTip = this.member(new ToolTip({ anchor: this._textAreaListMenu.el, anchorBorder: ["right", "left"], anchorAlign: "center", anchorAlignBox: () => this._getTextAreaListButtonNode() }));
        // unfiltered function entries for the autocompletion list menu
        this._functionEntries = [];

        // the cell attribute set of the edited cell
        this._cellAttrSet = null;
        // the effective font settings for the edited cell
        this._fontDesc = null;
        // the effective line height of the edited cell, in pixels
        this._lineHeight = null;
        // the token array representing the current formula in the text area
        this._tokenArray = null;
        // the token descriptors received form the formula parser for the current formula in the text area
        this._parseTokens = [];

        // local undo/redo manager while cell edit mode is active
        this._undoManager = null;
        // additional attributes applied locally at the text area
        this._editAttrSet = null;

        // initial text at the beginning of the edit mode
        this._originalText = null;
        // the text at the time the text area has been updated the last time
        this._prevEditText = null;
        // collected entries for type-ahead suggestions
        this._typeAheadEntries = null;

        // quick edit mode (true), or text cursor mode (false)
        this._quickEditMode = null;
        // the formula expression, if the input string starts with specific characters
        this._formulaExpr = null;
        // prefix for formula expression (equality sign; or empty string if edit text starts with plus/minus)
        this._formulaPrefix = null;
        // unique identifier of the current range highlighting mode for formulas
        this._highlightUid = null;
        // whether to show autocompletion list for a function or defined name
        this._funcAutoComplete = false;
        // token descriptor for function/name autocompletion
        this._funcAutoParseToken = null;
        // whether to keep autocompletion list open although focus is not in text area
        this._stickyAutoComplete = false;
        // how many closing parenthesis entered manually will be ignored
        this._funcAutoCloseData = [];
        // the text position of the current range address while in range selection mode
        this._rangeSelection = null;
        // the detected state of the mobile soft keyboard
        this._keyboardOpen = false;

        // whether updating the position is pending (used in method `updateTextAraePosition`)
        this._updatePositionPending = false;
        // whether updating the size is pending (used in method `updateTextAraePosition`)
        this._updateSizePending = false;
        // whether to suppress the autocompletion popup menu (used in method `updateTokenArray`)
        this._suppressAutoComplete = false;
        // whether to ignore focus event in the text area (no update of popup nodes, used in method `textAreaFocusHandler`)
        this._ignoreFocusEvent = false;

        // initialize sorted array with localized names of all visible functions
        this._initFunctionEntries();
        this.listenTo(this.docModel, "change:locale", this._initFunctionEntries);

        // additional initialization after import
        docView.waitForImport(() => {
            this._paneInputNode = docView.formulaPane.$textInput;
            this._paneUnderlayNode = docView.formulaPane.$textUnderlay;
            // bug 53420: user cannot choose function from the list in formula pane
            this._textAreaListMenu.registerFocusableNodes(this._paneInputNode);
        });

        // handle click events in the autocompletion popup menu
        this._textAreaListMenu.$el.on("click", BUTTON_SELECTOR, event => {
            this._applyAutoCompleteText(getButtonValue(event.currentTarget));
            return false; // popup menu has been closed, browser must not process the click
        });

        // hide function description tooltip when closing the autocompletion menu
        this.listenTo(this._textAreaListMenu, "popup:hide", () => {
            this._textAreaListToolTip.hide();
        });
    }

    /*protected override*/ destructor() {
        this._cellInputContainer.remove();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the current text is a formula expression.
     *
     * @returns {boolean}
     *  Whether the current text is a formula expression.
     */
    hasFormulaExpression() {
        return this._formulaExpr !== null;
    }

    /**
     * Inserts the text into the current text selection.
     *
     * @param {string} text
     *  The text to be inserted into the current selection.
     *
     * @param {number} [pos]
     *  The new cursor position to be set (relative in the inserted text). If
     *  negative, the text cursor will be placed from the end of the text. If
     *  omitted, the cursor will be set to the end of the inserted text.
     */
    insertText(text, pos) {
        replaceInputSelection(this._textInputNode[0], text, pos);
        this._textAreaInputHandler();
    }

    // protected methods ------------------------------------------------------

    /**
     * Callback handler for starting the cell edit mode.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {string} [options.text]
     *    The initial text to insert into the edit area. If omitted, the
     *    current value of the active cell will be inserted.
     *  - {number} [options.pos]
     *    The initial position of the text cursor. If omitted, the text cursor
     *    will be placed after the entire text. If negative, the text cursor
     *    will be placed from the end of the text.
     *  - {boolean} [options.quick=false]
     *    If set to `true`, the quick edit mode will be initiated. All cursor
     *    navigation keys will not move the text cursor in the text area
     *    control but will behave as in the normal sheet selection mode.
     *  - {boolean} [options.autoComplete=false]
     *    If set to `true`, the text passed with the option `text` will be used
     *    to show an autocompletion suggestion. By default, text edit mode will
     *    start without autocompletion.
     *  - {boolean} [options.stickyList=false]
     *    If set to `true`, the autocompletion drop-down list will be kept open
     *    when the text edit control is not focused.
     */
    /*protected override*/ implEnterEdit(options) {

        // the active cell must not be locked
        const errorCode = this.docView.ensureUnlockedActiveCell({ sync: true });
        if (errorCode) { return errorCode; }

        // the active grid pane used for editing
        const gridPane = this.editGridPane;
        // the scope symbol to be attached to all event handlers
        const scope = this._scope;

        // the location of the edited cell
        const { selectionEngine, sheetModel } = this.docView;
        const editAddress = selectionEngine.getActiveCell();
        this._editAttrSet = dict.create();

        // register the undo manager at application controller to redirect the GUI commands
        this._undoManager = new CellUndoManager(sheetModel, editAddress);
        this.docApp.docController.registerUndoManager(this._undoManager);

        // initialze the correct DOM node for popup menus and DOM events (force within the formula pane on small devices)
        this._formulaBarMode = !EDIT_ON_TOP && !!options?.formulaBarMode;
        this._textInputNode = this._formulaBarMode ? this._paneInputNode : this._cellInputNode;
        this._textOutputNode = this._formulaBarMode ? this._cellInputNode : this._paneInputNode;

        // initialize CSS formatting of the text area (this initializes most class members)
        this._updateTextAreaStyle();

        // scroll to the active cell (before checking edit mode); on touch devices,
        // scroll the cell to the top border of the grid pane (due to virtual keyboard)
        gridPane.scrollToCell(editAddress, { forceTop: EDIT_ON_TOP });

        // Bug 34366: Touch devices scroll the window out of visible area, so that neither the
        // toolpanes are visible, nor the editable cell. Root cause is the soft-keyboard.
        // However, scroll back to 0,0 and everything is fine. (Delay of 1000 necessary for iOS)
        if (TOUCH_DEVICE) {
            this.setTimeout(() => window.scrollTo(0, 0), 1000);
            if (CHROME_ON_ANDROID) {
                this.listenTo(window, "resize", this._androidKeyboardHandler, { scope });
            } else if (IOS_SAFARI_DEVICE) {
                this.listenTo(document, "selectionchange", this._iosKeyboardHandler, { scope });
            }
        }

        // initial state of quick edit mode
        this._setQuickEditMode(!!options?.quick);

        // process events of the text area element
        for (const node of [this._textInputNode, this._textOutputNode]) {
            this.listenTo(node, "keydown", this._textAreaKeyDownHandler, { scope });
            this.listenTo(node, "keypress", this._textAreaKeyPressHandler, { scope });
            this.listenTo(node, "input", this._textAreaInputHandler, { scope });
            this.listenTo(node, "mousedown mouseup touchstart touchend touchcancel", this._textAreaMouseTouchHandler, { scope });
            this.listenTo(node, "focus blur", this._textAreaFocusHandler, { scope });
        }

        // type-ahead suggestions will be collected on first access
        this._typeAheadEntries = null;

        // the token array stores the parsed formula for range highlighting
        this._parseTokens.length = 0;
        this._tokenArray = sheetModel.createCellTokenArray();
        this.listenTo(this._tokenArray, "change:token", this._changeTokenHandler, { scope });

        // highlight the ranges in the active sheet contained in the formula expression
        this._startRangeHighlighting();

        // update edit cell address and formula expressions for concurrent editing mode
        this.listenTo(this.docView, "update:formulas", this._updateFormulasHandler, { scope });

        // insert the text area control into the DOM of the active grid pane, and activate it
        const container = this._cellInputContainer.show();

        // DOCS-735: touch devices: move cell text input node to top of application window
        if (EDIT_ON_TOP) {
            const editHeight = this.docView.topPane.$el.outerHeight() + this.docView.mainPane.$el.outerHeight() + this.docView.formulaPane.$el.outerHeight() - 1;
            container.addClass("edit-on-top").css({ height: editHeight });
            this.docApp.getWindowNode().append(container);
        } else {
            gridPane.$el.append(container);
        }

        // DOCS-2331: enable F6 navigation into the text area
        this._cellInputNode.addClass("f6-target");

        // update position of the text area when scrolling the grid pane
        this.listenTo(gridPane, "change:scrollpos", () => {
            this._updateTextAreaPosition({ immediate: true, skipSize: true });
        }, { scope });

        // update zoom settings during edit mode
        this.listenToProp(sheetModel.propSet, "zoom", () => {
            this._updateTextAreaStyle();
            this._updateTextAreaPosition({ immediate: true });
        }, { scope });

        // concurrent editing: cancel cell edit mode if the edited cell becomes locked somehow
        // - another user protects the sheet, and the edited cell is locked
        // - another user inserts a table range, and the edited cell is in its header row
        this.listenToAllEvents(sheetModel, () => {
            if (this.docView.ensureUnlockedActiveCell({ sync: true })) {
                this.cancelEditMode();
            }
        }, { scope });

        // concurrent editing: update text area size/position after external
        // column/row/cell changes, or exit edit mode if the edited cell has been hidden
        this.listenTo(sheetModel, "change:columns change:rows change:cells", () => {
            if (sheetModel.cellCollection.isVisibleCell(this._undoManager.editAddress)) {
                this._updateTextAreaStyle();
                this._updateTextAreaPosition();
            } else {
                this.cancelEditMode();
            }
        }, { scope });

        // concurrent editing: update text area size/position after external merged
        // cell changes, or exit edit mode if the edited cell has been covered
        this.listenTo(sheetModel, "merge:cells", () => {
            if (sheetModel.mergeCollection.isHiddenCell(this._undoManager.editAddress)) {
                this.cancelEditMode();
            } else {
                this._updateTextAreaStyle();
                this._updateTextAreaPosition();
            }
        }, { scope });

        // get the formatted edit string
        this._originalText = this.docView.cellCollection.getEditString(editAddress);
        // the initial text for the text area
        const initialText = getStringOption(options, "text", null);
        // set efefctive text contents to be shown in the text area
        const editText = (initialText === null) ? this._originalText : initialText;

        // the initial cursor position for the text area (negative: position from end of text)
        let cursorPos = getIntegerOption(options, "pos", editText.length, -editText.length, editText.length);
        if (cursorPos < 0) { cursorPos = Math.max(0, editText.length + cursorPos); }

        // set initial contents, position, size, and selection of the text area
        this._initialMinWidth = this._initialMinHeight = -1;
        this._setTextAreaContents(editText, { start: cursorPos, end: cursorPos });

        // selection handler that changes cell ranges in formulas in cell edit mode
        selectionEngine.registerCellSelectionProvider({
            get: this._resolveCellSelection.bind(this),
            set: this._updateCellSelection.bind(this)
        });

        // apply the undo actions when undoing/redoing during cell edit mode
        this.listenTo(this._undoManager, "action:undo action:redo", this._restoreUndoAction, { scope });

        // if an explicit initial text has been passed, create an initial undo action with the original text
        if (initialText !== null) {
            this._undoManager.saveInitialAction(this._originalText);
        }

        // invoke input handler to handle the initial text (e.g. popups, additional undo action for custom text)
        this._prevEditText = (options?.autoComplete && (editText.length > 0)) ? "" : null;
        this._textAreaInputHandler();

        // special mode to keep autocompletion list menu open without focus in text area
        this._stickyAutoComplete = !!options?.stickyList;
        this._textAreaListMenu.config.autoClose = !this._stickyAutoComplete;

        // synchronize scroll position of text area and underlay node
        this.listenTo(this._cellInputNode, "scroll", () => {
            this._cellUnderlayNode.scrollTop(this._cellInputNode.scrollTop());
        }, { scope });

        // touch devices: trigger scroll event on textarea to force the correct position of the underlay node
        if (TOUCH_DEVICE) {
            this.setTimeout(() => this._cellInputNode.trigger("scroll"), 200);
        }

        // notify successfully started edit mode
        editLogger.log(() => [`starting in cell ${editAddress} with text`, editText]);
        return true;
    }

    /**
     * Callback handler for leaving the cell edit mode.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {CommitMode} [options.commitMode=CELL]
     *    Specifies how to leave the cell edit mode.
     *  - {boolean} [options.force=false]
     *    If set to `true`, this editor does not remain in cell edit mode if
     *    the formula expression is invalid. Instead, the edit text will be
     *    ignored, and nothing will be changed in the edit cell. The promise
     *    returned by this method will always be fulfilled, and never rejected.
     */
    /*protected override*/ implLeaveEdit(options) {

        // default commit mode is "cell" (change the active cell only)
        let commitMode = options?.commitMode ?? CommitMode.CELL;
        const forceMode = !!options?.force;
        editLogger.log(() => `commit-mode=${commitMode} force-mode=${forceMode}`);

        // the cell collection to be modified
        const { cellCollection } = this._undoManager.sheetModel;
        // local copy of the edit address for asynchronous operation handling
        const commitAddress = this._undoManager.editAddress.clone();
        // local copy of the selected ranges for asynchronous operation handling
        const commitRanges = this.docView.selectionEngine.getSelectedRanges();
        // bounding range of an existing matrix formula covering the active cell
        let matrixRange = cellCollection.getMatrixRange(commitAddress);
        // whether to update an existing matrix (prevent auto-expansion of 1x1 matrixes)
        const updateMatrix = matrixRange !== null;
        // fill the active range of the selection with a new matrix formula (ignore multi-selection)
        if (!matrixRange) { matrixRange = this.docView.selectionEngine.getActiveRange(); }

        // current selection state for undo/redo, before manipulating the selection
        const selectionState = this.docModel.getSelectionState();

        // the new cell text
        const editText = this._textInputNode.val();
        // parse the entered text: convert to other data types, or interpret the formula
        const parseResult = cellCollection.parseCellValue(editText, (commitMode === CommitMode.MATRIX) ? "mat" : "val", commitAddress, matrixRange);
        // whether to create a formula cell
        const createFormula = parseResult.f && parseResult.result;
        // whether the formula expression is invalid (do not create document operations)
        const syntaxError = createFormula && (parseResult.result.type === "error");

        // keep edit mode active, if an error has been found in the formula structure
        // (unless leaving the edit mode has been forced by caller)
        if (syntaxError && !forceMode) {
            this._setQuickEditMode(false);
            return "formula:invalid";
        }

        // if matrix edit mode has been left without a formula expression, behave like normal fill mode
        if (!createFormula && (commitMode === CommitMode.MATRIX)) { commitMode = CommitMode.FILL; }

        // special checks to keep the edit mode active, for different commit modes
        switch (forceMode ? null : commitMode) {

            // fail, if single-cell edit wants to change a matrix formula
            case CommitMode.CELL:
                if (updateMatrix && !matrixRange.single()) {
                    this._setQuickEditMode(false);
                    return "formula:matrix:change";
                }
                break;

            // fail, if fill mode wants to change parts of a matrix formula
            case CommitMode.FILL:
                if (cellCollection.coversAnyMatrixRange(this.docView.selectionEngine.getSelectedRanges(), FindMatchType.PARTIAL)) {
                    this._setQuickEditMode(false);
                    return "formula:matrix:change";
                }
                break;

            // fail, if another matrix formula overlapping with the active cell would be changed
            case CommitMode.MATRIX:
                if (!updateMatrix && cellCollection.coversAnyMatrixRange(matrixRange, FindMatchType.PARTIAL)) {
                    this._setQuickEditMode(false);
                    return "formula:matrix:change";
                }
                break;
        }

        // nothing more to do with formula syntax error or when editing a matrix, if leaving edit mode is forced
        if (forceMode && (syntaxError || updateMatrix)) {
            return true;
        }

        // add auto-wrapping attribute, if the text contains an explicit line break (except for formulas)
        if (!parseResult.f && (editText.indexOf("\n") >= 0)) {
            insertAttribute(this._editAttrSet, "cell", "wrapText", true);
        }

        // create the cell change set from the parse result
        const changeSet = _.pick(parseResult, "v", "f", "format");

        // performance: do not pass empty attributes object (prevent useless object processing)
        if (!is.empty(this._editAttrSet)) {
            changeSet.a = this._editAttrSet;
        }

        // send current value to document model after returning to selection mode
        let promise = null;
        switch (commitMode) {
            case CommitMode.CELL:
                // single cell mode: do not send operations if neither text nor formatting changes
                // (especially: do not replace shared formula cell with simple formula cell)
                // (but allow to change a single-cell matrix formula to a simple cell formula)
                if ((this._originalText !== editText) || changeSet.a || updateMatrix) {
                    promise = this._invokeLeaveAction(() => {
                        return this.docView.changeCell(changeSet, { address: commitAddress, selection: selectionState });
                    });
                }
                break;
            case CommitMode.FILL:
                promise = this._invokeLeaveAction(() => {
                    return this.docView.fillCells(changeSet, { ranges: commitRanges, selection: selectionState });
                });
                break;
            case CommitMode.MATRIX:
                promise = this._invokeLeaveAction(() => {
                    // if matrix edit mode has been left without a formula expression, behave like normal fill mode
                    return (parseResult.f && parseResult.result) ?
                        this.docView.setMatrixFormula(matrixRange, parseResult.f, parseResult.result.value, { selection: selectionState }) :
                        this.docView.fillCells(changeSet, { ranges: commitRanges, selection: selectionState });
                });
                break;
        }

        // create the hyperlink in a separate step, in order to get an own undo action
        if (parseResult.url && promise) {
            promise = jpromise.fastThen(promise, () => {
                const urlRanges = (commitMode === CommitMode.CELL) ? new Range(commitAddress) : commitRanges;
                return this.docView.insertHyperlink(parseResult.url, { createStyles: true, ranges: urlRanges, selection: selectionState });
            });
        }

        // nothing to do (cell not changed): hide previously shown alert box (bug 47936)
        if (!promise) { this.docView.hideYell(); }

        return true;
    }

    /**
     * Cleanup when canceling the cell edit mode.
     */
    /*protected override*/ implCancelEdit() {

        // stop listening to all events
        this.stopListeningToScope(this._scope);

        // the application controller
        const controller = this.docApp.docController;
        if (controller.destroyed) { return; }

        // unregister and destroy the undo manager
        controller.restoreUndoManager();
        this._undoManager.destroy();
        this._undoManager = null;

        // deinitialize range highlighting
        this.docView.rangeHighlighter.endHighlighting(this._highlightUid);
        this.docView.selectionEngine.unregisterCellSelectionProvider();
        this._setActiveSelectionProp(null);
        this._tokenArray.destroy();
        this._tokenArray = this._highlightUid = null;

        // back to cell selection mode
        this._quickEditMode = this._formulaExpr = this._typeAheadEntries = null;
        this._hideTextAreaPopups();

        // hide the highlighting mark-up in the underlay nodes
        this._cellUnderlayNode.empty();
        this._paneUnderlayNode.empty();

        // hide the text area control (bug 40321: DO NOT detach from DOM, Egde may freeze)
        this._cellInputContainer.hide();

        // DOCS-2331: disable F6 navigation into the text area
        this._cellInputNode.removeClass("f6-target");

        // deinitialze the DOM anchor node for popup menus and DOM events
        this._textInputNode = this._textOutputNode = null;
        this._keyboardOpen = false;
    }

    /**
     * Returns the merged formatting attributes of the edited cell, merged with
     * the pending formatting attributes applied during cell edit mode.
     *
     * @returns {PtParagraphAttributeSet}
     *  The merged attribute set of the edited cell.
     */
    /*protected override*/ implGetAttrs() {
        // merge the cell attributes with the current edit attributes
        return this.docModel.sheetAttrPool.extendAttrSet(this._cellAttrSet, this._editAttrSet, { clone: true });
    }

    /**
     * Applies the passed formatting attributes to the text area control.
     *
     * @param {PtParagraphAttributeSet} attrSet
     *  The attributes to be applied to the text area control.
     */
    /*protected override*/ implSetAttrs(attrSet) {

        // store attributes, but do not generate any document operations
        this.docModel.sheetAttrPool.extendAttrSet(this._editAttrSet, attrSet);

        // update the formatting of the text area
        this._updateTextAreaStyle();
        this._updateTextAreaPosition({ immediate: true });

        // create a new undo action
        this._undoManager.saveFormatAction(this._editAttrSet);
        this.triggerEvent("change");
    }

    /**
     * Returns the text input control intended to contain the browser focus.
     */
    /*protected override*/ implGetFocusNode() {
        return this._textInputNode[0];
    }

    /**
     * Returns whether this cell text editor contains unsaved changes (i.e.
     * whether the local undo stack is not empty).
     *
     * @returns {boolean}
     *  Whether this cell text editor contains unsaved changes.
     */
    /*protected override*/ implIsUnsaved() {
        return this._undoManager.getUndoCount() > 0;
    }

    // private methods --------------------------------------------------------

    /**
     * Invokes the callback handler that applies the text change when leaving
     * the text edit mdoe.
     */
    /*private*/ _invokeLeaveAction(callback) {
        // bug 47129: wait for a running document action
        // bug 67493: try to remain synchronous if possible
        return jpromise.fastThen(this.docModel.waitForActionsProcessed(), callback);
    }

    /**
     * Returns the active selection of the active sheet.
     */
    /*private*/ _getActiveSelectionProp() {
        return this.docView.propSet.get("activeSelection");
    }

    /**
     * Changes the active selection of the active sheet.
     */
    /*private*/ _setActiveSelectionProp(selection) {
        return this.docView.propSet.set("activeSelection", selection);
    }

    /**
     * Applies a transformation to the passed color, and returns the CSS
     * color value.
     */
    /*private*/ _getModifiedCssColor(jsonColor, colorType) {

        // the passed color, as instance of the class Color
        const color = Color.parseJSON(jsonColor);
        // the resolved color, including the effective luma
        const colorDesc = this.docModel.resolveColor(color, colorType);

        if ((colorType === "text") && (colorDesc.y > 0.25)) {
            color.transform("shade", 25000 / colorDesc.y);
        } else if ((colorType === "fill") && (colorDesc.y < 0.75)) {
            color.transform("tint", 25000 / (1 - colorDesc.y));
        }
        return this.docModel.resolveColor(color, colorType).css;
    }

    /**
     * Updates the CSS formatting of the text area control used for cell edit
     * mode according to the current formatting of the active cell.
     */
    /*private*/ _updateTextAreaStyle() {

        // current location of the edited cell
        const { sheetModel, editAddress } = this._undoManager;
        const { cellCollection } = sheetModel;

        // get cell attributes, and overwrite them with current edit attributes
        this._cellAttrSet = this.docModel.sheetAttrPool.extendAttrSet(cellCollection.getAttributeSet(editAddress), this._editAttrSet, { clone: true });

        // receive font settings, text line height, and text orientation
        const charAttrs = { ...this._cellAttrSet.character };
        const cellAttrs = this._cellAttrSet.cell;
        const fontDesc = this._fontDesc = this.docModel.getRenderFont(charAttrs, sheetModel.getEffectiveZoom());
        charAttrs.fontSize = fontDesc.size = math.clamp(fontDesc.size, 6, 100);
        const lineHeight = this._lineHeight = this.docModel.fontMetrics.getRowHeight(charAttrs, 1);
        const orientation = cellCollection.getTextOrientation(editAddress);

        // initialize alignment and text direction (also for on-top editing)
        const textAlign = (cellCollection.isFormulaCell(editAddress) && (cellAttrs.alignHor === "auto")) ? "left" : orientation.cssTextAlign;
        this._cellInputContainer.children().css({ textAlign }).attr("dir", orientation.domTextDir);

        // styles only for real in-place editing
        if (!EDIT_ON_TOP) {

            // set CSS attributes according to formatting of active cell
            this._cellInputContainer.children().css({
                // adapt cell padding to have stable poition of original cell text and edit text
                padding: `0 ${this.docModel.fontMetrics.getTextPadding(charAttrs, 1)}px`,
                lineHeight: lineHeight + "px",
                fontFamily: fontDesc.family,
                fontSize: fontDesc.size + "pt",
                fontWeight: fontDesc.bold ? "bold" : "normal",
                fontStyle: fontDesc.italic ? "italic" : "normal",
                textDecoration: getCssTextDecoration(charAttrs)
            });

            // set cell text color and fill color to the appropriate nodes
            this._cellInputNode.css("color", this._getModifiedCssColor(charAttrs.color, "text"));
            this._cellUnderlayNode.css("background-color", this._getModifiedCssColor(cellAttrs.fillColor, "fill"));
        }
    }

    /**
     * Updates the position and size of the text area control for the cell edit
     * mode, according to the current length of the text in the text area
     * control, and the scroll position of the grid pane.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.immediate=false]
     *    If set to `true`, the position of the text area will be updated
     *    immediately. By default, updating the text area will be done
     *    debounced.
     *  - {boolean} [options.skipSize=false]
     *    If set to `true`, the size of the text area will not be changed, only
     *    its position. Used in situations where the content of the text area
     *    does not change (e.g. while scrolling).
     */
    /*private*/ _updateTextAreaPosition(options) {

        // fixed position/size for on-top editing mode (mobile devices)
        if (EDIT_ON_TOP) { return; }

        // set the pending update flags for the deferred callback
        this._updatePositionPending = true;
        this._updateSizePending ||= !options?.skipSize;

        // invoke deferred callback immediately if specified, or if the text has caused a new line break
        const textArea = this._cellInputNode[0];
        if (options?.immediate || (textArea.clientHeight < textArea.scrollHeight)) {
            this._execUpdateTextAreaPosition();
        } else {
            this._execUpdateTextAreaPositionDebounced();
        }
    }

    /**
     * Implementation for `updateTextAreaPosition` (called either directly or
     * debounced).
     */
    /*private*/ _execUpdateTextAreaPosition() {

        // do nothing if cell edit mode is not active anymore
        if (!this.isActive() || (!this._updatePositionPending && !this._updateSizePending)) { return; }

        // current location of the edited cell
        const { sheetModel, editAddress } = this._undoManager;
        const { cellCollection } = sheetModel;
        // the cell input control
        const $textArea = this._cellInputNode;

        // the position of the edited cell
        let cellRect = null;
        // the visible area of the grid pane
        let visibleRect = null;
        // whether the text in the cell wraps automatically
        const cellValue = cellCollection.getValue(editAddress);
        const isWrapped = ((cellValue === null) || is.string(cellValue)) && hasWrappingAttributes(this._cellAttrSet) && !this.hasFormulaExpression();
        // the effective horizontal CSS text alignment
        const textAlign = $textArea.css("text-align");
        // round up width to multiples of the line height
        const blockWidth = Math.max(20, this._lineHeight);
        // the new width and height of the text area
        let targetWidth = 0, targetHeight = 0;
        // the current contents of the textarea
        const editValue = $textArea.val();

        // returns required width needed to show all text lines in a single line
        const getMinimumWidth = () => {

            // the resulting minimum width
            let minWidth = cellRect.width;

            // unwrapped mode: expand width to entire text lines
            if (!isWrapped) {
                // required width for all text lines
                const requiredWidth = splitDisplayString(editValue).reduce((memo, textLine) => Math.max(memo, this._fontDesc.getTextWidth(textLine)), 0);
                // round up to block width, enlarge to cell width
                minWidth = Math.max(minWidth, math.ceilp(requiredWidth, blockWidth) + blockWidth + SCROLLBAR_WIDTH);
            }

            // initialize or use the initial minimal width
            if (this._initialMinWidth < 0) {
                this._initialMinWidth = minWidth;
            } else {
                minWidth = Math.max(minWidth, this._initialMinWidth);
            }

            return minWidth;
        };

        // returns maximum width according to horizontal alignment and width of visible area
        const getMaximumWidth = () => {

            // stick to cell width in auto-wrapping mode
            if (isWrapped) { return visibleRect.width; }

            // the distance from left visible border to right cell border (for right alignment)
            const leftWidth = math.clamp(cellRect.right() - visibleRect.left, 0, visibleRect.width);
            // the distance from left cell border to right visible border (for left alignment)
            const rightWidth = math.clamp(visibleRect.right() - cellRect.left, 0, visibleRect.width);
            // effective available width
            let availWidth = 0;

            // get available width according to text alignment
            switch (textAlign) {
                case "right":
                    availWidth = leftWidth;
                    break;
                case "center":
                    availWidth = Math.min(leftWidth, rightWidth) * 2 - cellRect.width;
                    break;
                default:
                    availWidth = rightWidth;
            }

            // enlarge to at least half of the width of the visible area
            return Math.max(availWidth, Math.round(visibleRect.width / 2));
        };

        // get horizontal and vertical cell position and size
        cellRect = this.docView.getCellRectangle(editAddress, { expandMerged: true });
        // reduce width by one pixel to exclude the grid line
        cellRect.width = Math.max(blockWidth, cellRect.width - 1);
        // reduce height by one pixel to exclude the grid line
        cellRect.height -= 1;

        // get position and size of the visible area in the grid pane
        visibleRect = this.editGridPane.getVisibleRectangle();

        // on small Android devices, soft keyboard can overlay complete grid pane,
        // then the automatic height calculation has zero height,
        // after closing the soft keyboard it still keeps this height and that looks really wrong
        if (CHROME_ON_ANDROID) {
            visibleRect.height = Math.max(visibleRect.height, this._lineHeight);
        }

        // calculate and set the new size of the text area
        if (this._updateSizePending) {

            // find best width
            const minWidth = getMinimumWidth();
            const maxWidth = getMaximumWidth();

            // calculate width and height of the text area
            targetWidth = Math.min(minWidth, maxWidth);
            targetHeight = this._fontDesc.getCustomFontMetrics(node => {

                // the text whose height will be determined
                let text = editValue;

                // use NBSP instead of empty text; bug 35143: add NBSP to empty last line
                if (/(^|\n)$/.test(text)) { text += "\xa0"; }

                // prepare the helper node for multi-line text
                node.style.width = `${targetWidth}px`;
                node.style.padding = `0 ${sheetModel.getTextPadding(this._cellAttrSet.character)}px`;
                node.style.lineHeight = `${this._lineHeight}px`;
                node.style.whiteSpace = "pre-wrap";
                node.style.wordWrap = "break-word";

                // add a text node
                node.appendChild(document.createTextNode(text));

                return Math.min(Math.max(node.offsetHeight, cellRect.height), visibleRect.height);
            });

            // initialize or use the initial minimal height
            if (this._initialMinHeight < 0) {
                this._initialMinHeight = targetHeight;
            } else {
                targetHeight = Math.max(targetHeight, this._initialMinHeight);
            }

            // update size of DOM element, prevent unwanted scrollbar when text contents grow in narrow textarea
            $textArea.css({ width: targetWidth, height: targetHeight });

        } else {
            targetWidth = $textArea.outerWidth();
            targetHeight = $textArea.outerHeight();
        }

        // calculate and set the new position of the text area
        if (this._updatePositionPending) {

            // left and top position of the text area
            let leftOffset = cellRect.left;
            let topOffset = cellRect.top;

            // calculate horizontal position according to alignment
            switch (textAlign) {
                case "right":
                    leftOffset += cellRect.width - targetWidth;
                    break;
                case "center":
                    leftOffset += Math.round((cellRect.width - targetWidth) / 2);
                    break;
            }

            // move text area into visible area, set effective position
            leftOffset = math.clamp(leftOffset, visibleRect.left, visibleRect.right() - targetWidth);
            topOffset = math.clamp(topOffset, visibleRect.top, visibleRect.bottom() - targetHeight);
            this._cellInputContainer.css({ left: leftOffset - visibleRect.left, top: topOffset - visibleRect.top });
        }

        this._updatePositionPending = this._updateSizePending = false;
    }

    /**
     * Debounced version of method `_execUpdateTextAreaPosition`.
     */
    @debounceMethod({ delay: 250, maxDelay: 500 })
    /*private*/ _execUpdateTextAreaPositionDebounced() {
        this._execUpdateTextAreaPosition();
    }

    /**
     * Returns the selected option button in the dropdown menu element for the
     * text area.
     *
     * @returns {JQuery}
     *  The selected option button in the text area dropdown menu.
     */
    /*private*/ _getTextAreaListButtonNode() {
        return this._textAreaListMenu.getSelectedOptions().first();
    }

    /**
     * Updates and shows or hides the tooltip attached to the text area,
     * containing the name, parameter list, and description of the current
     * function.
     */
    /*private*/ _updateFunctionDescriptionToolTip() {

        // the selected button element in the autocompletion menu
        const $button = this._getTextAreaListButtonNode();
        // the name and type of the selected option button
        const entry = getButtonValue($button);
        // the resulting tooltip text
        let toolTipLabel = null;

        // create the tooltip text according to the type of the list entry
        switch (entry?.type) {

            case "name": {
                // the model of the selected defined name (TODO: sheet-local names)
                const nameModel = this.docModel.nameCollection.getNameModel(entry.key);
                if (nameModel) {
                    toolTipLabel = nameModel.getFormula("ui", this._undoManager.editAddress);
                    if (toolTipLabel) { toolTipLabel = `=${toolTipLabel}`; }
                }
                break;
            }

            case "table": {
                // the model of the selected table range
                const tableModel = this.docModel.getTableModel(entry.key);
                if (tableModel) {
                    const tokenArray = new TokenArray(tableModel.sheetModel, "cell");
                    tokenArray.appendRange(tableModel.getRange(), { sheet: tableModel.sheetModel.getIndex(), relSheet: true });
                    toolTipLabel = tokenArray.getFormula("ui", Address.A1, Address.A1);
                    tokenArray.destroy();
                }
                break;
            }

            case "func": {
                // the help descriptor for the selected function (nothing for defined names or tables)
                const functionHelp = this.docModel.formulaResource.getFunctionHelp(entry.key);
                if (functionHelp) { toolTipLabel = functionHelp.description; }
                break;
            }
        }

        // initialize the tooltip
        const toolTip = this._textAreaListToolTip;
        if (toolTipLabel) {
            toolTip.setText(_.noI18n(toolTipLabel));
            toolTip.show();
        } else {
            toolTip.hide();
        }
    }

    /**
     * Fills the member `functionEntries` (used for the autocompletion list
     * menu) with the localized names of all visible functions.
     */
    /*private*/ _initFunctionEntries() {
        this._functionEntries = ary.from(this.docModel.formulaResource.functionMap, ([key, resource]) => {
            return resource.hidden ? undefined : { key, name: resource.localName };
        });
        LOCALE_DATA.getCollator({ numeric: true }).sortByProp(this._functionEntries, "name");
    }

    /**
     * Creates entries in the text area popup list menu.
     *
     * @param {object[]} entries
     *  The unfiltered entries to be inserted into the list menu, as objects
     *  with the properties "key" (inserted as property "key" into the object
     *  values of the list entries) and "name" (inserted as property "name"
     *  into the object values of the list entries, used as visible labels of
     *  the list entries).
     *
     * @param {string} filter
     *  The filter string. Only entries with a "name" property starting with
     *  this string (ignoring character case) will be inserted into the list
     *  menu.
     *
     * @param {string} type
     *  The type of the list entries. Will be used as name for a section in the
     *  list menu, and will be inserted as property "type" into the object
     *  values of the list entries.
     *
     * @param {string} title
     *  The UI label for the section title for the specified entry type.
     */
    /*private*/ _createListMenuEntries(entries, filter, type, title) {

        // create a section for the list entries
        const listMenu = this._textAreaListMenu;
        listMenu.addSection(type, title);

        // filter for entries starting with the passed filter string
        filter = filter.toUpperCase();
        entries = entries.filter(entry => entry.name.substr(0, filter.length).toUpperCase() === filter);

        // create the list entries
        for (const { key, name } of entries) {
            listMenu.addOption({ key, name, type }, { label: _.noI18n(name) });
        }
    }

    /**
     * Returns information about the token containing or preceding the passed
     * text cursor position in the current formula string.
     *
     * @param {number} pos
     *  The text cursor position in the current formula string.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.ignoreWs=false]
     *    If set to `true`, the method will ignore the white-space token
     *    currently containing the text cursor, and will return the token
     *    preceding the white-space token.
     *  - {boolean} [options.preferOperand=false]
     *    Specifies how to resolve the situation that the text cursor position
     *    is located between two tokens. If set to true, and one of the tokens
     *    is an operand token (constant value tokens, array literal tokens,
     *    reference tokens, name tokens, or function name tokens), this token
     *    will be returned. Otherwise, always returns the token preceding the
     *    text position.
     *
     * @returns {Opt<ParseToken>}
     *  A descriptor for the token whose text representation contains the
     *  specified text cursor position. If the text cursor position is located
     *  outside of the formula text, or at its beginning (without the option
     *  "preferOperand"), this method will return the value `undefined`.
     */
    /*private*/ _findParseToken(pos, options) {

        // shortcut to parser tokens
        const parseTokens = this._parseTokens;

        // returns whether the specified token is an operand token
        const isOperand = ai => parseTokens[ai]?.token.isType("value", "array", "ref", "name", "func");

        // cursor is located at the beginning of the formula, and operands are preferred:
        // return the first token (following the text cursor position)
        if (options?.preferOperand && (pos === 0) && isOperand(0)) {
            return parseTokens[0];
        }

        // invalid text position passed
        if (pos <= 0) { return undefined; }

        // find the token containing or preceding the passed text position
        const index = ary.fastFindFirstIndex(parseTokens, parseToken => pos <= parseToken.end);
        const currParseToken = parseTokens[index];
        if (!currParseToken) { return undefined; }

        // check if the cursor is located between two tokens, and the trailing is the preferred operand
        if (options?.preferOperand && (pos === currParseToken.end) && !isOperand(index) && isOperand(index + 1)) {
            return parseTokens[index + 1];
        }

        // skip white-space token if specified
        if (options?.ignoreWs && (currParseToken.token.type === "ws")) {
            return (index > 0) ? parseTokens[index - 1] : undefined;
        }

        // return the descriptor of the found token
        return currParseToken;
    }

    /**
     * Returns information about the innermost function containing the passed
     * text cursor position in the current formula string.
     *
     * @param {number} pos
     *  The text cursor position in the current formula string.
     *
     * @returns {Opt<object>}
     *  A descriptor for the innermost function containing the specified text
     *  cursor position, in the following properties:
     *  - {string} funcKey
     *    The unique resource key of the function.
     *  - {number} paramIndex
     *    The zero-based index of the function parameter covering the specified
     *    text cursor position.
     *  - {number} paramCount
     *    The total number of parameters found in the function parameter list.
     *    If the formula is incomplete or contains errors, this number may be
     *    wrong, but it will always be greater than the value returned in the
     *    property "paramIndex".
     *
     *  If the text cursor position is outside of any function, this method
     *  will return `null`.
     */
    /*private*/ _findFunctionDescriptor(pos) {

        // information about the token at the passed position
        const parseToken = this._findParseToken(pos, { preferOperand: true });
        if (!parseToken) { return undefined; }

        // shortcut to parser tokens
        const parseTokens = this._parseTokens;
        // current token index, modified in seeking functions
        let index = 0;
        // the parameter index and count
        let param = 0, count = 0;

        // seek to the preceding opening parenthesis, skip embedded pairs of
        // parentheses, return number of skipped separator characters
        const seekToOpeningParenthesis = () => {
            let seps = 0;
            while (index >= 0) {
                switch (parseTokens[index].token.type) {
                    case "open":
                        return seps;
                    case "close":
                        index -= 1;
                        seekToOpeningParenthesis();
                        break;
                    case "sep":
                        seps += 1;
                        break;
                }
                index -= 1;
            }
            return 0;
        };

        // seek to the following closing parenthesis, skip embedded pairs of
        // parentheses, return number of skipped separator characters
        const seekToClosingParenthesis = () => {
            let seps = 0;
            while (index < parseTokens.length) {
                switch (parseTokens[index].token.type) {
                    case "open":
                        index += 1;
                        seekToClosingParenthesis();
                        break;
                    case "close":
                        return seps;
                    case "sep":
                        seps += 1;
                        break;
                }
                index += 1;
            }
            return seps;
        };

        // try as long as a function has been found, or the beginning of the formula is reached
        index = parseToken.index;
        while (index >= 0) {

            // seek to the preceding opening parenthesis
            param = seekToOpeningParenthesis();

            // check if a function name precedes the parenthesis
            const funcParseToken = parseTokens[index - 1];
            if (funcParseToken && (funcParseToken.token.type === "func")) {
                index += 1;
                count = seekToClosingParenthesis() + 1;
                return { funcKey: funcParseToken.token.value, paramIndex: param, paramCount: count };
            }

            // continue seeking to preceding opening parenthesis
            index -= 1;
        }

        // cursor is not located inside a function, but it may point to a top-level
        // function name, in that case return this function with invalid parameter index
        if ((parseToken.token.type === "func") && (parseTokens[parseToken.index + 1].token.type === "open")) {
            index = parseToken.index + 2;
            count = seekToClosingParenthesis() + 1;
            return { funcKey: parseToken.token.value, paramIndex: -1, paramCount: count };
        }

        return undefined;
    }

    /**
     * Hides the function signature tooltip and the function autocompletion
     * popup menu.
     */
    /*private*/ _hideTextAreaPopups() {
        if (!isStickyPopupsMode()) {
            this._signatureToolTip.hide();
            this._textAreaListMenu.hide();
        }
    }

    /**
     * Shows the function signature tooltip or the function autocompletion
     * popup menu if possible.
     *
     * @param {boolean} [suppressAutoComplete=false]
     *  If set to true, the popup menu for autocompletion of function names
     *  will be suppressed regardless of the current formula text and cursor
     *  position.
     */
    /*private*/ _updateTextAreaPopups(suppressAutoComplete) {

        // show popups only in formula mode, do not show them while dragging
        // a highlighted range, or when selecting new ranges for the formula
        if (!this.hasFormulaExpression() || this.docView.rangeHighlighter.hasTrackedRange() || this._getActiveSelectionProp()) {
            this._hideTextAreaPopups();
            return;
        }

        // the cursor position in the text area (exclude the equality sign)
        const textPos = this._textInputNode[0].selectionStart - this._formulaPrefix.length;
        // the text to be used for function autocompletion
        let autoText = null;

        // prefer function autocompletion over function tooltip
        this._funcAutoParseToken = null;

        // auto completion of names: cursor must be located exactly at the end of a function or name token
        const parseToken = (this._funcAutoComplete && !suppressAutoComplete) ? this._findParseToken(textPos) : undefined;
        if (parseToken && (parseToken.end === textPos)) {

            // get the text entered before the text cursor (may be a name token, a table token, or a function token)
            // TODO: process sheet-local names too
            if (((parseToken.token.type === "name") && !parseToken.token.hasSheetRef()) || parseToken.token.isType("table", "func")) {
                // Special case: do not show a popup menu when a literal number precedes the name.
                // Otherwise, e.g. entering scientific numbers will show a popup for functions starting with "E".
                if ((parseToken.index === 0) || (this._parseTokens[parseToken.index - 1].token.type !== "lit")) {
                    autoText = parseToken.text;
                }
            }

            // store token info (needed to replace the entered text with the full function name)
            this._funcAutoParseToken = parseToken;
        }

        // fill popup menu for autocompletion
        if (is.string(autoText)) {

            // insert all matching defined names, and all matching function names
            const listMenu = this._textAreaListMenu;
            listMenu.clearOptions();

            // insert defined names (the method `createListMenuEntries` expects an array of objects with "key" and "name" properties)
            // TODO: include sheet-local names
            let nameEntries = Array.from(this.docModel.nameCollection.yieldNameModels({ skipHidden: true })).map(nameModel => nameModel.label);
            nameEntries = LOCALE_DATA.getCollator().sort(nameEntries).map(label => ({ key: label, name: label }));
            this._createListMenuEntries(nameEntries, autoText, "name", gt.pgettext("named ranges", "Named ranges"));

            // insert table ranges (the method `createListMenuEntries` expects an array of objects with "key" and "name" properties)
            let tableEntries = Array.from(this.docModel.yieldTableModels(), tableModel => tableModel.name);
            tableEntries = LOCALE_DATA.getCollator().sort(tableEntries).map(name => ({ key: name, name }));
            this._createListMenuEntries(tableEntries, autoText, "table", /*#. special cell ranges in a spreadsheet for filtering etc. */ gt("Table ranges"));

            // insert the matching function names
            this._createListMenuEntries(this._functionEntries, autoText, "func", /*#. functions in spreadsheet formulas */ gt("Functions"));

            // show the menu, if it contains at least one entry
            if (listMenu.getOptions().length > 0) {
                this._signatureToolTip.hide();
                listMenu.show();
                listMenu.selectOptionAt(0, { scroll: true });
                this._updateFunctionDescriptionToolTip();
                return;
            }
        }

        // try to show a tooltip for the current function in a formula
        this._hideTextAreaPopups();
        const functionInfo = this._findFunctionDescriptor(textPos);
        if (functionInfo) {
            this._signatureToolTip.updateSignature(functionInfo.funcKey, functionInfo.paramIndex);
        }
    }

    /**
     * Sets the text selection in the active text input element.
     *
     * @param {number} start
     *  The start position of the selection.
     *
     * @param {number} [end]
     *  The end position of the selection. If omitted, a cursor selection at
     *  `start` will be set.
     */
    /*private*/ _setTextAreaSelection(start, end) {
        const textSelection = this._textSelection = { start, end: end ?? start };
        editLogger.trace(() => `set selection [${textSelection.start},${textSelection.end}]`);
        setInputSelection(this._textInputNode[0], textSelection);
    }

    /**
     * Changes the contents and selection of the text area, and updates its
     * position and size.
     *
     * @param {string} text
     *  The new contents of the text area.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {number} [options.start]
     *    The new start position of the selection. If omitted, the text cursor
     *    will be moved to the end of the new text.
     *  - {number} [options.end=options.start]
     *    The new end position of the selection. If omitted, and a start
     *    position has been specified with the option "start", a simple text
     *    cursor will be shown at the start position.
     *  - {boolean} [options.suppressAutoComplete=false]
     *    If set to `true`, the popup menu for autocompletion of function names
     *    will be suppressed.
     */
    /*private*/ _setTextAreaContents(text, options) {

        editLogger.trace(() => `set text "${text}"`);
        this._cellInputNode.val(text);
        this._paneInputNode.val(text);
        this._prevEditText = text;

        const start = getIntegerOption(options, "start", text.length, 0, text.length);
        const end = getIntegerOption(options, "end", start, start, text.length);
        this._setTextAreaSelection(start, end);

        this._updateTextAreaPosition({ immediate: true });
        this._updateTokenArray(options);
    }

    /**
     * Updates the function tooltip, and other settings according to the text
     * selection in the text area control.
     */
    /*private*/ _updateTextAreaAfterSelection() {

        // do not open the autocompletion menu even if the new cursor position is valid
        this._funcAutoComplete = false;

        // do not ignore closing parentheses entered manually anymore (function autocomplete)
        this._funcAutoCloseData.length = 0;

        // defer execution to let the browser process pending key events
        this.setTimeout(() => {
            // fail-safe check that cell edit mode is still active
            if (this.isActive()) { this._updateTextAreaPopups(); }
        }, 0);
    }

    /**
     * Updates the highlighting of cell references in the current formula while
     * cell edit mode is active.
     */
    /*private*/ _updateTextAreaHighlighting() {

        // descriptor objects for all resolved cell ranges in the formula
        const { editAddress } = this._undoManager;
        let rangeDescs = this._tokenArray.extractRanges(editAddress, editAddress, { resolveNames: true });

        // show the cell ranges located in the active sheet only
        rangeDescs = rangeDescs.filter(rangeInfo => rangeInfo.range.containsSheet(this.docView.activeSheet));

        // nothing to do without any tokens to be highlighted
        if (rangeDescs.length === 0) {
            this._cellUnderlayNode.empty();
            this._paneUnderlayNode.empty();
            return;
        }

        // collect the scheme color index and the size (token count) of the range descriptors (map by token index)
        const rangeDataMap = new Map/*<number, object>*/();
        let styleIndex = 0;
        rangeDescs.forEach(rangeDesc => {
            // skip multiple ranges based on the same name token
            map.upsert(rangeDataMap, rangeDesc.index, () => {
                const color = getSchemeColorIndex(styleIndex);
                styleIndex += 1;
                return { color, size: rangeDesc.size };
            });
        });

        // start with the equality sign (unless the edit text does not contain it, e.g. in "+A1+A2")
        const fragment = document.createDocumentFragment();
        if (this._formulaPrefix) {
            fragment.append(createSpan({ label: this._formulaPrefix }));
        }

        // process the token descriptor array
        this._parseTokens.forEach((parseToken, index) => {

            // special handling for whitespace (line breaks via <br> elements)
            if (parseToken.token.type === "ws") {
                parseToken.text.split(/\n/).forEach((spaces, index) => {
                    if (index) { fragment.append(createElement("br")); }
                    fragment.append(createSpan({ label: spaces }));
                });
                return;
            }

            // create a span element with highlight color, if a corresponding range exists
            const rangeData = rangeDataMap.get(index);
            const dataset = rangeData ? { style: rangeData.color } : undefined;
            fragment.append(createSpan({ dataset, label: parseToken.text }));
        });

        this._cellUnderlayNode[0].replaceChildren(fragment.cloneNode(true));
        this._paneUnderlayNode[0].replaceChildren(fragment);
    }

    /**
     * Saves the current state of the cell edit mode on top of the undo stack.
     * If the current undo action is not on top of the stack, all following
     * actions will be deleted first.
     */
    /*private*/ _saveUndoAction() {
        this._undoManager.saveInputAction(this._textInputNode[0]);
        this.triggerEvent("change");
    }

    /**
     * Uses the current action in the undo stack to restore the state of the
     * cell edit mode.
     */
    /*private*/ _restoreUndoAction(undoAction) {
        this._editAttrSet = cloneAttributeSet(undoAction.attrSet);
        this._updateTextAreaStyle();
        this._setTextAreaContents(undoAction.text, undoAction.selection);
        this.triggerEvent("change");
    }

    /**
     * Updates the formula token array according to the current contents of the
     * text area control used for cell edit mode. The token array will invoke
     * the changle callback handler that will cause to redraw the highlighted
     * ranges in all grid panes.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.immediate=false]
     *    If set to `true`, the token array and all other dependent settings
     *    will be updated immediately. By default, updating the token array
     *    will be debounced.
     *  - {boolean} [options.suppressAutoComplete=false]
     *    If set to `true`, the popup menu for autocompletion of function names
     *    will be suppressed regardless of the current formula text and cursor
     *    position.
     */
    /*private*/ _updateTokenArray(options) {

        // the current input string in the text area
        const value = this._textInputNode.val();

        // update the formula expression immediately
        const formulaParts = this._undoManager.textOnlyMode ? undefined : splitFormulaParts(value);
        this._formulaExpr = formulaParts ? formulaParts.expr : null;
        this._formulaPrefix = formulaParts ? formulaParts.prefix : "";

        // update the "suppress autocompletion" flag
        this._suppressAutoComplete ||= !!options?.suppressAutoComplete;

        // invoke deferred callback immediately if specified
        if (options?.immediate) {
            this._execUpdateTokenArray();
        } else {
            this._execUpdateTokenArrayDebounced();
        }
    }

    /**
     * Implementation for `updateTokenArray` (called either directly or
     * debounced).
     */
    /*private*/ _execUpdateTokenArray() {

        // the cell edit mode may have been finished already
        if (!this.isActive()) { return; }

        // parse the formula string
        if (this._formulaExpr) {
            this._parseTokens = this._tokenArray.parseFormula("ui", this._formulaExpr, this._undoManager.editAddress);
        } else {
            this._tokenArray.clearTokens();
            this._parseTokens.length = 0;
        }

        // insert highlighting mark-up for the underlay node
        this._updateTextAreaHighlighting();

        // update popup nodes and other settings dependent on the text selection
        this._updateTextAreaPopups(this._suppressAutoComplete);
        this._suppressAutoComplete = false;
    }

    /**
     * Debounced version of method `_execUpdateTokenArray`.
     */
    @debounceMethod({ delay: 100, maxDelay: 300 })
    /*private*/ _execUpdateTokenArrayDebounced() {
        this._execUpdateTokenArray();
    }

    /**
     * Callback handler for the token array used while cell edit mode is
     * active. Updates the edited formula expression while the token array will
     * be manipulated externally, e.g. when dragging reference ranges in the
     * sheet area.
     */
    /*private*/ _changeTokenHandler(token, index) {

        // shortcut to parser tokens
        const parseTokens = this._parseTokens;
        // override the text representation of the changed token
        parseTokens[index].text = token.getText(this.docModel.formulaGrammarUI);

        // update the text positions of the following tokens
        for (let ai = index, l = parseTokens.length; ai < l; ai += 1) {
            const parseToken = parseTokens[ai];
            parseToken.start = (ai === 0) ? 0 : parseTokens[ai - 1].end;
            parseToken.end = parseToken.start + parseToken.text.length;
        }

        // calculate the new formula string
        const formulaString = parseTokens.reduce((formula, parseToken) => formula + parseToken.text, "");

        // set the new formula, add an undo action
        this._setTextAreaContents(this._textInputNode.val()[0] + formulaString, { start: parseTokens[index].end + 1 });
        this._saveUndoAction();

        // immediately update the highlighted cell references
        this._updateTextAreaHighlighting();
        this._setActiveSelectionProp(null);
    }

    /**
     * Starts the range highlighter for the current edit address.
     */
    /*private*/ _startRangeHighlighting() {
        if (!this._undoManager.textOnlyMode) {
            this._highlightUid = this.docView.rangeHighlighter.startHighlighting(this._tokenArray, {
                refAddress: this._undoManager.editAddress,
                targetAddress: this._undoManager.editAddress,
                priority: true,
                draggable: true,
                resolveNames: true
            });
        }
    }

    /**
     * Updates the edit cell address for concurrent editing mode (cell may even
     * be deleted!), and updates the edited formula expression and all formula
     * expressions in the undo manager, when any operation has been applied in
     * the document that causes formula updates.
     */
    /*private*/ _updateFormulasHandler(updateTask) {

        // current location of the edited cell
        const { sheetModel } = this._undoManager;
        let { editAddress } = this._undoManager;

        // update edit address if cells in the edited sheet have been moved
        if (sheetModel.isOwnMoveTask(updateTask)) {
            editAddress = updateTask.transformer.transformAddress(editAddress);
            if (!editAddress) { return this.cancelEditMode(); }
        }

        // update the edit address in the undo manager
        this._undoManager.editAddress.assign(editAddress);

        // update formula expressions according to update task
        this._undoManager.transformFormulas(this._tokenArray, updateTask, newExpr => {
            this._setTextAreaContents(this._formulaPrefix + newExpr, { suppressAutoComplete: true });
        });

        // restart highlighting with new reference address
        this._startRangeHighlighting();
    }

    /**
     * Replaces the token before the text cursor with the selected entry from
     * the autocomplete menu.
     */
    /*private*/ _applyAutoCompleteText(entry) {

        // always hide the popup menu
        this._textAreaListMenu.hide();

        // fail-safety check
        const parseToken = this._funcAutoParseToken;
        if (!parseToken || !entry) { return; }

        // get new text and new cursor position
        const prefixLen = this._formulaPrefix.length;
        let newText = entry.name;
        let newStart = parseToken.start + newText.length + prefixLen;

        // insert parentheses after function name (unless the existing token is
        // a function token which means it already has an opening parenthesis)
        if (entry.type === "func") {
            if (parseToken.token.type === "name") {
                newText += "()";
                // ignore the next closing parenthesis entered manually
                this._funcAutoCloseData.push(true);
            }
            // move cursor behind the opening parenthesis
            newStart += 1;
        }

        // insert the new text into the formula string, prevent showing the
        // autocompletion popup list immediately again (important for defined names)
        newText = str.replaceSubStr(this._textInputNode.val(), parseToken.start + prefixLen, parseToken.end + prefixLen, newText);
        this._setTextAreaContents(newText, { start: newStart, suppressAutoComplete: true });
        this._saveUndoAction();
    }

    /**
     * Changes the quick edit mode while cell edit mode is active, and shows a
     * status label at the bottom of the application pane.
     *
     * @param {boolean} quickEditMode
     *  The new value of the quick edit mode.
     */
    /*private*/ _setQuickEditMode(quickEditMode) {

        // change the quick edit state
        if (this._quickEditMode === quickEditMode) { return; }
        this._quickEditMode = quickEditMode;
        editLogger.log(() => `${quickEditMode ? "insert" : "edit"} mode activated`);

        // update the status label
        this.docView.setStatusLabel(quickEditMode ?
            //#. Special edit mode in spreadsheet document: quickly type text into cell, use cursor keys to move to next cell
            gt("Insert mode") :
            //#. Standard edit mode in spreadsheet document: edit existing cell text, cursor keys move text cursor inside cell
            gt("Edit mode")
        );
    }

    /**
     * Handles "keydown" events in the text input control, while cell edit mode
     * is active.
     */
    /*private*/ _textAreaKeyDownHandler(event) {

        const listMenu = this._textAreaListMenu;
        // the option button currently selected in the autocompletion popup menu
        const $button = listMenu.getSelectedOptions().first();
        // the value of the option button
        const entry = getButtonValue($button);

        // short-cuts in visible popup menu for autocompletion
        if (listMenu.isVisible()) {

            // close popup menu on ESCAPE key
            if (isEscapeKey(event)) {
                listMenu.hide();
                return false;
            }

            // insert selected option button into the formula
            if (matchKeyCode(event, "TAB") || matchKeyCode(event, "ENTER")) {

                // Bug 31999: For performance reasons, the token array and the autocompletion popup
                // menu will be updated debounced. To prevent that ENTER/TAB keys insert a function name
                // unintentionally, the popup menu must be updated before applying an option button.
                this._updateTokenArray({ immediate: true });

                // list menu may have been hidden by the `updateTextAreaPopups` call,
                // do not apply any option in this case, but leave cell edit mode
                if (listMenu.isVisible()) {
                    this._applyAutoCompleteText(entry);
                    return false;
                }
            }

            // otherwise, try to move the selected option button
            if (listMenu.selectOptionForKey(event, $button)) {
                this._updateFunctionDescriptionToolTip();
                return false;
            }
        }

        // quit cell edit mode on ESCAPE key regardless of any modifier keys
        if (isEscapeKey(event)) {
            // signature-Tooltip is visible
            if (this._signatureToolTip.isVisible()) {
                this._signatureToolTip.hide();
            } else {
                this.cancelEditMode();
            }
            return false;
        }

        // toggle quick edit mode
        if (matchKeyCode(event, "F2")) {
            this._setQuickEditMode(!this._quickEditMode);
            // forget last cell range selected in the sheet
            this._setActiveSelectionProp(null);
            return false;
        }

        // ENTER without modifier keys (but ignoring SHIFT): set value, move cell down/up
        // TAB key without modifier keys (but ignoring SHIFT): set value, move cell to up/down
        const moveDir = getCellMoveDirection(event);
        if (moveDir) {
            // try to commit the current edit text (may fail, e.g. formula syntax error)
            if (this.leaveEditMode()) {
                // move the active cell into the specified direction
                editLogger.log(`moving active cell, direction=${moveDir}`);
                this.docView.selectionEngine.moveActiveCell(moveDir);
                // keep edit mode active on small devices
                if (SMALL_DEVICE) {
                    this._keyboardOpen = true;
                    if (this._formulaBarMode) {
                        this._paneInputNode.focus();
                    } else {
                        this.enterEditMode();
                    }
                }
            }
            return false;
        }

        // ENTER with CTRL or META key: fill value to all cells (no SHIFT), or insert matrix formula (SHIFT)
        if (matchKeyCode(event, "ENTER", { shift: null, ctrlOrMeta: true })) {
            this.leaveEditMode({ commitMode: event.shiftKey ? CommitMode.MATRIX : CommitMode.FILL });
            return false;
        }

        // ENTER with ALT key without any other modifier keys: insert line break
        if (matchKeyCode(event, "ENTER", { alt: true })) {
            this.insertText("\n");
            return false;
        }

        // ENTER with any other modifier keys: discard the entire event
        if (hasKeyCode(event, "ENTER")) {
            return false;
        }

        // any cursor navigation key
        if (NAVIGATION_KEYCODES.has(event.keyCode)) {
            if (this._quickEditMode) {
                // quick edit mode: do not let the text area process the key events
                event.preventDefault();
            } else {
                // standard edit mode: do not process the key events (no change of cell selection)
                event.stopPropagation();
                // update popup nodes and other settings dependent on the new text selection
                this._updateTextAreaAfterSelection();
                // forget last cell range selected in the sheet
                this._setActiveSelectionProp(null);
            }
            // let key event bubble up to move the cell cursor, or text cursor in the text area
        }

        // all other events will be processed by the text field
    }

    /**
     * Handles "keypress" events in the text input control, while cell edit
     * mode is active.
     */
    /*private*/ _textAreaKeyPressHandler(event) {

        // the entered Unicode character
        switch (String.fromCharCode(event.charCode)) {

            // Register an opening parenthesis entered manually. The related closing parenthesis
            // will be inserted even if it occurs among other closing parentheses inserted via
            // function autocompletion (see below).
            case "(":
                this._funcAutoCloseData.push(false);
                break;

            // When a function name has been inserted from the autocompletion popup menu, skip
            // a closing parenthesis because it has been inserted already by the autocompletion).
            case ")":
                if (this._funcAutoCloseData.pop() === true) {
                    // the text cursor needs to be moved one character forward
                    this._setTextAreaSelection(getInputSelection(this._textInputNode[0]).start + 1);
                    this._updateTextAreaPopups();
                    return false;
                }
                break;
        }
    }

    /**
     * Handles any changes in the text area.
     */
    /*private*/ _textAreaInputHandler() {

        // the current value of the text area
        const currEditText = this._textInputNode.val();
        // the text selection in the text area control
        const textSelection = getInputSelection(this._textInputNode[0]);
        // whether the selection in the text area is a simple cursor
        const isCursor = textSelection.start === textSelection.end;

        // do nothing if the edit text did not change at all
        if (this._prevEditText === currEditText) { return; }

        // try to show drop-down list with function names and defined names
        this._funcAutoComplete = isCursor;

        // update text area settings (position, formula range highlighting)
        this._updateTextAreaPosition();
        this._updateTokenArray();
        this._saveUndoAction();

        // deferred processing after the event has been processed by the browser
        this.setTimeout(() => {

            // do nothing if edit mode has been left before this callback will be triggered
            if (!this.isActive()) { return; }

            // synchronize text areas in grid pane and formula pane
            this._textOutputNode.val(this._textInputNode.val());

            // Mobile devices may add auto-correction text which modifies the selection after a text change event has
            // been processed. Recalculate the `funcAutoComplete` flag after the browser has set the final selection.
            this._funcAutoComplete = isCursorSelection(this._textInputNode[0]);
        }, 0);

        // forget last active cell range selected in the sheet (formulas with cell references)
        this._setActiveSelectionProp(null);

        // show type-ahead suggestion in the text area (not in formula edit mode)
        if (is.string(this._prevEditText) && (this._prevEditText.length < currEditText.length) && isCursor && (textSelection.end === currEditText.length) && !this.hasFormulaExpression()) {

            // first cycle: collect the contents of all text cells and formula cells resulting in text above and below the edited cell
            if (!this._typeAheadEntries) {
                this._typeAheadEntries = [];

                // current location of the edited cell
                const { sheetModel, editAddress } = this._undoManager;
                const { cellCollection } = sheetModel;

                // collect the values of all text cells, and the string results of formula cells
                const boundRange = Range.fromIndexes(editAddress.c, Math.max(0, editAddress.r - 256), editAddress.c, Math.min(this.docModel.addressFactory.maxRow, editAddress.r + 256));
                for (const { address, value } of cellCollection.linearCellEntries(editAddress, [Direction.UP, Direction.DOWN], { type: "value", boundRange, skipStart: true })) {
                    if (is.string(value) && (value.length > 0)) {
                        this._typeAheadEntries.push({ text: value, dist: Math.abs(editAddress.r - address.r) });
                    }
                }

                // sort the entries in the array by distance to the edited cell
                this._typeAheadEntries.sort((entry1, entry2) => entry1.dist - entry2.dist);

                // reduce the entries to a simple string array, that is still sorted by distance
                this._typeAheadEntries = this._typeAheadEntries.map(entry => entry.text);
            }

            // find the first matching type-ahead entry (the array is sorted by distance to edited cell, thus it will find the nearest matching entry)
            let suggestion = this._typeAheadEntries.find(entry => str.startsWithICC(entry, currEditText));

            // insert the remaining text of a matching entry, keep character case of the typed text
            if (suggestion) {
                suggestion = currEditText + suggestion.substr(currEditText.length);
                this._setTextAreaContents(suggestion, { start: currEditText.length, end: suggestion.length });
                this._saveUndoAction();
            }
        }

        // store current text for next iteration
        this._prevEditText = currEditText;
    }

    /**
     * Handles mousedown or touchstart events in the text area.
     */
    /*private*/ _textAreaMouseTouchHandler() {
        // leave quick-edit mode (cursor keys will move the text cursor)
        this._setQuickEditMode(false);
        // update popup nodes and other settings dependent on the new text selection
        this._updateTextAreaAfterSelection();
        // forget last cell range selected in the sheet
        this._setActiveSelectionProp(null);
    }

    /**
     * Hides the popup nodes if the focus leaves the text area control, unless
     * the autocompletion menu is being clicked.
     */
    /*private*/ _textAreaFocusHandler(event) {
        switch (event.type) {

            // text area focused: rebuild all active popup nodes
            case "focus": {
                focusLogger.trace(() => `$badge{CellTextEditor} ${this._formulaBarMode ? "formula bar" : "cell text field"} received browser focus`);
                this._cellInputContainer.addClass(FOCUSED_CLASS);
                if (!this._ignoreFocusEvent) {
                    this._updateTextAreaPopups();
                }

                // switch input/output text area variables
                if (!EDIT_ON_TOP && (event.target !== this._textInputNode[0])) {
                    focusLogger.trace("$badge{CellTextEditor} active input source changed");
                    const tmpNode = this._textInputNode;
                    this._textInputNode = this._textOutputNode;
                    this._textOutputNode = tmpNode;
                }

                break;
            }

            // text area lost focus: hide popup nodes (unless the autocomplete menu
            // has been clicked, in this case, move focus back to text area immediately)
            case "blur": {
                focusLogger.trace(() => `$badge{CellTextEditor} ${this._formulaBarMode ? "formula bar" : "cell text field"} lost browser focus`);

                // switch between grid pane and formula pane
                if (event.relatedTarget === this._textOutputNode[0]) {
                    focusLogger.trace("$badge{CellTextEditor} changing active input source");
                    this.enterEditMode({ formulaBarMode: !this._formulaBarMode, text: this._textInputNode.val(), restart: true });
                    break;
                }

                // rescue text selection in the text area control (needed to be restored after losing focus)
                this._textSelection = getInputSelection(this._textInputNode[0]);

                this.setTimeout(() => {
                    // weird failing E2E tests [C8314] and [C114382]: browser moves focus to <body> element
                    if (this.isActive() && (this._textAreaListMenu.hasFocus() || isFocused(document.body))) {
                        focusLogger.trace("$badge{CellTextEditor} restoring lost focus in text input source");
                        this._ignoreFocusEvent = true;
                        setFocus(this._textInputNode);
                        editLogger.trace(() => `restore selection [${this._textSelection.start},${this._textSelection.end}]`);
                        setInputSelection(this._textInputNode[0], this._textSelection);

                        // IE sends focus event deferred
                        this.setTimeout(() => { this._ignoreFocusEvent = false; }, 0);
                    } else {
                        this._cellInputContainer.removeClass(FOCUSED_CLASS);
                        if (!this._stickyAutoComplete) { this._hideTextAreaPopups(); }
                    }
                }, 10);
                break;
            }
        }
    }

    /**
     * Returns whether the current text selection in the text area control is
     * valid to start selecting new ranges for the current formula. The text
     * area must contain a formula string (starting with an equality sign), and
     * the start position of the text selection must be located behind the
     * leading equality sign, an operator, a list separator, or an opening
     * parenthesis).
     */
    /*private*/ _isValidRangeSelectionPosition() {

        // the cursor position in the token array (exclude leading equality sign)
        const cursorPos = this._textInputNode[0].selectionStart;
        const textPos = cursorPos - this._formulaPrefix.length;

        // invalid if not in cell edit mode with a formula
        if (!this.isActive() || !this.hasFormulaExpression()) { return false; }

        // existing view attribute, or cursor at the beginning of the formula is always valid
        if ((textPos === 0) || this._getActiveSelectionProp()) { return true; }

        // check if cursor is located after a space character that may be interpreted as intersection operator
        if (this.docModel.formulaGrammarUI.ISECT_SPACE && (this._textInputNode.val()[cursorPos - 1] === " ")) { return true; }

        // preceding token may be an operator, separator, or opening parenthesis (bug 48475: ignore white-space before cursor)
        const parseToken = this._findParseToken(textPos, { ignoreWs: true });
        return !!parseToken && (parseToken.end <= textPos) && parseToken.token.isType("op", "sep", "open");
    }

    /**
     * When in cell edit mode with a formula, returns the active selection
     * which will be used for keyboard navigation and mouse/touch tracking.
     */
    /*private*/ _resolveCellSelection() {

        // always return existing "activeSelection" view attribute
        const activeSelection = this._getActiveSelectionProp();
        if (activeSelection) { return activeSelection; }

        // check the text selection in the text area control
        const isValidPos = this._isValidRangeSelectionPosition();
        return isValidPos ? new SheetSelection() : null;
    }

    /**
     * When in cell edit mode with a formula, selects a single range and
     * inserts the range address into the formula string.
     */
    /*private*/ _updateCellSelection(selection) {

        // do nothing if ranges cannot be selected with the current text selection
        if (!this._isValidRangeSelectionPosition()) { return false; }

        // store current text selection, if no range are activated yet
        // (selected text will be replaced with initial range addresses)
        if (!this._getActiveSelectionProp()) {
            this._rangeSelection = getInputSelection(this._textInputNode[0]);
        }

        // build the new formula term to be inserted into the text
        const { sheetModel, editAddress } = this._undoManager;
        const tmpTokenArray = sheetModel.createCellTokenArray();
        tmpTokenArray.appendRangeList(selection.ranges);
        const references = tmpTokenArray.getFormula("ui", editAddress, editAddress);
        const formulaText = str.replaceSubStr(this._textInputNode.val(), this._rangeSelection.start, this._rangeSelection.end, references);
        tmpTokenArray.destroy();

        // insert the new formula text, set cursor behind the range (no real selection)
        this._rangeSelection.end = this._rangeSelection.start + references.length;
        this._setTextAreaContents(formulaText, { start: this._rangeSelection.end });
        this._saveUndoAction();

        // update the view attribute
        this._setActiveSelectionProp(selection);

        // do not perform standard sheet selection
        return true;
    }

    /*private*/ _androidKeyboardHandler() {
        this._keyboardOpen = !this._keyboardOpen;
        if (!this._keyboardOpen) {
            this.leaveEditMode({ force: true });
        }
    }

    /*private*/ _iosKeyboardHandler() {
        this._keyboardOpen = !this._keyboardOpen;
        if (!this._keyboardOpen && !window.getSelection().anchorNode) {
            this.leaveEditMode({ force: true });
        }
    }
}
