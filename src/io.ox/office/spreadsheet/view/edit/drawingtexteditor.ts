/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { math } from "@/io.ox/office/tk/algorithms";
import { type AnyKeyEvent, MAX_NODE_SIZE, isSelectAllKeyEvent, isEscapeKey } from "@/io.ox/office/tk/dom";

import { DOM_TEXT_EDIT_SUPPORT } from "@/io.ox/office/baseframework/app/extensionregistry";
import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { PtParagraphAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";

import { getContentNode } from "@/io.ox/office/drawinglayer/view/drawingframe";
import { getFirstTextPositionInTextFrame, getLastTextPositionInTextFrame } from "@/io.ox/office/textframework/utils/position";
import type SelectionEngine from "@/io.ox/office/textframework/selection/selection";

import type { SheetDrawingModel } from "@/io.ox/office/spreadsheet/model/drawing/drawingmodel";
import { getTextFrame, withTextFrame } from "@/io.ox/office/spreadsheet/model/drawing/text/textframeutils";
import type { YellMessageCode } from "@/io.ox/office/spreadsheet/view/labels";
import { type TextEditEnterOptions, TextEditorBase } from "@/io.ox/office/spreadsheet/view/edit/texteditorbase";

import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

export interface DrawingTextEditEnterOptions extends TextEditEnterOptions {

    /**
     * The cursor position inside the text frame to set the text cursor to. If
     * omitted, the cursor will be placed at the end of the entire text.
     */
    position?: Position;

    /**
     * The text to be appended to the existing text contents.
     */
    text?: string;
}

// class DrawingTextEditor ====================================================

/**
 * Implementation of the text edit mode in drawing objects.
 */
export class DrawingTextEditor extends TextEditorBase {

    // the root container for DOM models of all drawing objects in all sheets
    readonly #pageNode: HTMLElement;
    // the selection engine of the text framework
    readonly #selection: SelectionEngine;
    // scope symbol for unbinding event listeners
    readonly #scope = Symbol("scope");

    // the model of the drawing frame whose text is edited in drawing edit mode
    #editDrawingModel: Opt<SheetDrawingModel>;
    // the DOM drawing frame currently edited in drawing edit mode (directly manipulated by the text framework)
    _editDrawingFrame: Opt<JQuery>;
    // the DOM text frame currently edited in drawing edit mode (directly manipulated by the text framework)
    #editTextFrame: Opt<JQuery>;

    // the DOM drawing frame that has been rendered by the active grid pane (used as source for position and size)
    #renderDrawingFrame: Opt<JQuery>;

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view that has created this instance.
     */
    constructor(docView: SpreadsheetView) {

        // base constructor
        super(docView, "drawing");

        this.#pageNode = this.docModel.getNode()[0];
        this.#selection = this.docModel.getSelection();

        // DOCS-2331: initially remove page node from F6 navigation
        this.#pageNode.classList.remove("f6-target");
    }

    // protected methods ------------------------------------------------------

    /**
     * Callback handler for starting the drawing edit mode.
     *
     * @param [options]
     *  Optional parameters.
     */
    protected override implEnterEdit(options?: DrawingTextEditEnterOptions): boolean | YellMessageCode {

        // bug 63977: reject editing in unsupported browsers
        if (!DOM_TEXT_EDIT_SUPPORT) { return "drawing:text:editing"; }

        // reject editing in locked sheets
        const errorCode = this.docView.ensureUnlockedDrawings({ sync: true });
        if (errorCode) { return errorCode; }

        // single drawing selection required for text edit mode
        const drawingSelection = this.docView.selectionEngine.getSelectedDrawings();
        if (drawingSelection.length !== 1) { return false; }

        // the model of the selected drawing object to be edited
        const { sheetModel, drawingCollection } = this.docView;
        const editDrawingModel = this.#editDrawingModel = drawingCollection.getModel(drawingSelection[0]);
        if (!editDrawingModel) { return false; }

        // the text frame of the selected drawing object (early exit for drawing objects without text)
        const editDrawingFrame = this._editDrawingFrame = drawingCollection.getDrawingFrameForModel(editDrawingModel);
        const editTextFrame = this.#editTextFrame = editDrawingFrame ? getTextFrame(editDrawingFrame) : undefined;
        if (!editDrawingFrame || !editTextFrame) { return false; }

        // bug 53502: create drawing structure to enable text selection inside text frame
        // bug 53478: use the content node of the drawing frame
        if (_.browser.Firefox) {
            getContentNode(editDrawingFrame).removeAttr("contenteditable");
        }

        // the active grid pane used for editing
        const gridPane = this.editGridPane;
        if (!gridPane) { return false; }

        // the drawing frame rendered by the active grid pane (used as source for position and size of text frame)
        const renderDrawingFrame = this.#renderDrawingFrame = gridPane.drawingRenderer.getDrawingFrameForModel(editDrawingModel);
        if (!renderDrawingFrame) { return false; }

        // position after the last character in the text contents
        let cursorPos = options?.position ? [...editDrawingModel.getDocPosition(), ...options.position] as Position : this.#getLastCursorPosition();
        if (!cursorPos) { return false; }

        // insert the entire DOM tree with all drawing objects into the active grid pane
        gridPane.staticRootNode.append(this.#pageNode);

        // DOCS-2331: enable F6 navigation into the edited text shape
        this.#pageNode.classList.add("f6-target");

        // the scope symbol to be attached to all event handlers
        const scope = this.#scope;

        // update scroll position of the edit text frame (keep in sync with drawing frame)
        this.listenTo(gridPane, "change:scrollpos", this.#updateDrawingFramePosition, { scope });

        // update CSS attributes of the edit text frame, after the drawing frame has been (re-)rendered
        this.listenTo(gridPane, "render:drawingframe", (_drawingFrame, drawingModel) => {
            if (drawingModel === this.#editDrawingModel) { this.#updateDrawingFramePosition(); }
        }, { scope });

        // update zoom settings during edit mode
        this.listenTo(this.docView, "change:sheetprops", event => {
            if ("zoom" in event.newProps) { this.#updateDrawingFramePosition(); }
        }, { scope });

        // escape from edit mode when collection of drawing objects changes (most simple solution for now)
        this.listenTo(this.docView, ["insert:drawing", "delete:drawing", "move:drawing"], this.cancelEditMode, { scope });

        // DOCS-2031: concurrent editing: cancel edit mode if the edited drawing becomes locked somehow
        this.listenToAllEvents(sheetModel, () => {
            if (this.docView.ensureUnlockedDrawings({ sync: true })) {
                this.cancelEditMode();
            }
        }, { scope });

        // initialize the source and target text frames
        editDrawingFrame.addClass("edit-active");
        renderDrawingFrame.addClass("edit-active");
        this.#updateDrawingFramePosition();

        // restore the internal browser text selection after focusing
        this.listenTo(editTextFrame, "focusin", () => {
            this.#selection.restoreBrowserSelection();
        }, { scope });

        // process keyboard events of the text feame
        this.listenTo(editTextFrame, "keydown", this.#textFrameKeyDownHandler, { scope });

        // update paragraph/character formatting of the text frame
        drawingCollection.updateTextFormatting(editDrawingModel);

        // the text to be appended to the existing text contents
        if (options?.text) {
            this.docModel.insertText(options.text, cursorPos);
            if (!options?.position) {
                cursorPos = this.#getLastCursorPosition()!;
            }
        }

        // set initial text selection
        this.#selection.setTextSelection(cursorPos);

        // bug 53635: cursor visibility in Firefox
        if (_.browser.Firefox) { this.#selection.restoreBrowserSelection(); }

        // notify successfully started edit mode
        return true;
    }

    /**
     * Callback handler for leaving the drawing edit mode.
     */
    protected override implLeaveEdit(): boolean {
        // nothing to do, leaving edit mode cannot be vetoed
        return true;
    }

    /**
     * Cleanup when canceling the drawing edit mode.
     */
    protected override implCancelEdit(): void {

        const editTextFrame = this.#editTextFrame!;
        const editDrawingFrame = this._editDrawingFrame!;
        const renderDrawingFrame = this.#renderDrawingFrame!;

        this.stopListeningToScope(this.#scope);

        editDrawingFrame.removeClass("edit-active");
        editDrawingFrame.css("transform", ""); // reset previously set transform to fetch original width/height/positions
        editTextFrame.css("transform", "");

        renderDrawingFrame.removeClass("edit-active");

        // reset all properties related to a specific drawing frame
        this.#editDrawingModel = this._editDrawingFrame = this.#editTextFrame = this.#renderDrawingFrame = undefined;

        // DOCS-2331: disable F6 navigation into the edited text shape
        this.#pageNode.classList.remove("f6-target");
    }

    /**
     * Returns the merged character and paragraph attributes of the current
     * text selection, as provided by the text framework.
     *
     * @returns
     *  The merged attribute set of the text selection.
     */
    protected override implGetAttrs(): PtParagraphAttributeSet {
        return this.docView.getTextAttributeSet();
    }

    /**
     * Applies the passed attributes to the current text selection.
     *
     * @param attrSet
     *  The attributes to be applied to the text selection.
     */
    protected override implSetAttrs(attrSet: PtParagraphAttributeSet): JPromise {
        return this.docView.setTextAttributeSet(attrSet);
    }

    /**
     * Returns the text frame intended to contain the browser focus.
     */
    protected override implGetFocusNode(): HTMLElement {
        return this.#editTextFrame![0];
    }

    /**
     * Returns whether this drawing text editor contains unsaved changes.
     *
     * @returns
     *  Whether this drawing text editor contains unsaved changes.
     */
    protected override implIsUnsaved(): boolean {
        // changes in text frames are applied immediately
        return false;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the complete document position before the first character in the
     * edited drawing frame.
     *
     * @returns
     *  The complete document position before the first character in the edited
     *  drawing frame.
     */
    #getFirstCursorPosition(): Position | null {
        return getFirstTextPositionInTextFrame(this.#pageNode, this._editDrawingFrame!);
    }

    /**
     * Returns the complete document position behind the last character in the
     * edited drawing frame.
     *
     * @returns
     *  The complete document position behind the last character in the edited
     *  drawing frame.
     */
    #getLastCursorPosition(): Position | null {
        return getLastTextPositionInTextFrame(this.#pageNode, this._editDrawingFrame!);
    }

    /**
     * Updates the position and size of the drawing frame that is currently
     * edited, and its text frame.
     */
    #updateDrawingFramePosition(): void {

        // grid pane visible rectangle, used to fetch grid scroll positions
        const visibleGridRect = this.editGridPane!.getVisibleRectangle();
        // drawing rectangle values (visible drawing object returns valid rectangle)
        const drawingRectangle = this.#editDrawingModel!.getRectangle()!;
        // CSS inline styles of the source text frame
        const drawingFrameStyle = this.#renderDrawingFrame![0].style;
        // the current zoom factor of the active sheet
        const zoom = this.docView.getZoomFactor();

        // update position and size of the edited drawing frame
        this._editDrawingFrame!.css({
            left: math.clamp(drawingRectangle.left - visibleGridRect.left, -MAX_NODE_SIZE, MAX_NODE_SIZE),
            top: math.clamp(drawingRectangle.top - visibleGridRect.top, -MAX_NODE_SIZE, MAX_NODE_SIZE),
            width: drawingFrameStyle.width,
            height: drawingFrameStyle.height,
            transform: drawingFrameStyle.transform,
            zIndex: drawingFrameStyle.transform.length ? "1" : ""
        });

        // dynamically resolve the text frame (no caching, it may be recreated when updating the drawing frame)
        withTextFrame(this.#renderDrawingFrame, renderTextFrame => {

            // CSS inline styles of the source text frame
            const textFrameStyle = renderTextFrame[0].style;

            // update position and size of the edited text frame
            this.#editTextFrame!.css({
                left: textFrameStyle.left,
                top: textFrameStyle.top,
                width: textFrameStyle.width,
                height: textFrameStyle.height,
                transform: textFrameStyle.transform,
                fontSize: `${Math.round(16 * zoom)}px`
            });
        });
    }

    /**
     * Event handler for "keydown" events received from the current text frame.
     */
    #textFrameKeyDownHandler(event: AnyKeyEvent): void | false {

        // quit drawing edit mode on ESCAPE key regardless of any modifier keys
        if (isEscapeKey(event)) {
            this.cancelEditMode();
            return false;
        }

        // Ctrl+A selects the entire text in the drawing
        if (isSelectAllKeyEvent(event)) {
            const firstPos = this.#getFirstCursorPosition();
            const lastPos = this.#getLastCursorPosition();
            if (firstPos && lastPos) {
                this.#selection.setTextSelection(firstPos, lastPos);
            }
            return false;
        }
    }
}
