/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary } from "@/io.ox/office/tk/algorithms";
import { EObject } from "@/io.ox/office/tk/objects";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { ReferenceToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import type { ExtractRangesOptions, ExtractRangeData, TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

// types ======================================================================

export interface RangeHighlightOptions extends ExtractRangesOptions {

    /**
     * The source reference address used to resolve cell references with
     * relative column/row components. Default value is `Address.A1`.
     */
    refAddress?: Address;

    /**
     * The target reference address used to resolve cell references with
     * relative column/row components. Default value is `Address.A1`.
     */
    targetAddress?: Address;

    /**
     * If set to `true`, the range highlighting mode will be started as
     * high-priority mode, meaning that no other range highlighting mode can be
     * started as long as it is active. If another range highlighting mode with
     * high priority is currently running when trying to start a new
     * highlighting mode, nothing will happen. Default value is `false`.
     */
    priority?: boolean;

    /**
     * If set to `true`, the cell ranges representing reference tokens will be
     * marked as draggable. Draggable ranges will be rendered with additional
     * GUI handle elements that can be used to move or resize the respective
     * range. Default value is `false`.
     */
    draggable?: boolean;
}

/**
 * Type mapping for the events emitted by `RangeHighlighter` instances.
 */
export interface RangeHighlighterEventMap {

    /**
     * Will be emitted when the highlighted ranges have changed.
     */
    "change:ranges": [];
}

// class HighlightRangeDescriptor =============================================

/**
 * A descriptor object for a highlighted cell range in the document view.
 */
export class HighlightRangeDescriptor {

    // properties -------------------------------------------------------------

    /**
     * The cell range address of the highlighted range, with sheet indexes.
     */
    readonly range: Range3D;

    /**
     * Internal array index of the token array containing this highlighted
     * range.
     */
    readonly arrayIndex: number;

    /**
     * Internal index of the formula token containing this highlighted range,
     * in the token array specified by the property `arrayIndex`. Different
     * highlighted ranges may origin from the same formula token, e.g. from a
     * defined name in the formula containing multiple range addresses.
     */
    readonly tokenIndex: number;

    /**
     * A specific identifier of the originating formula token, built from the
     * token array index (property `arrayIndex`), and the token index (property
     * `tokenIndex`).
     */
    readonly tokenKey: string;

    /**
     * Whether this highlighted range is intended to be movable and resizable
     * with mouse or touch gestures. Needed to render the highlighted range
     * correctly.
     */
    readonly draggable: boolean;

    /**
     * Whether this highlighted range is currently being dragged around in the
     * GUI. The range will be rendered in a special way to indicate its special
     * state.
     */
    tracking = false;

    // constructor ------------------------------------------------------------

    constructor(rangeInfo: ExtractRangeData, arrayIndex: number, draggable?: boolean) {
        this.range = rangeInfo.range;
        this.arrayIndex = arrayIndex;
        this.tokenIndex = rangeInfo.index;
        this.tokenKey = `${arrayIndex},${rangeInfo.index}`;
        this.draggable = !!draggable && (rangeInfo.type === "ref");
    }
}

// class RangeHighlighter =====================================================

/**
 * Provides methods for showing and manipulating highlighted cell ranges in the
 * active sheet based on specific formula token arrays.
 *
 * Highlighted cell ranges will be rendered beside the regular cell selection.
 * They are used for example in formula edit mode to visualize the ranges and
 * defined names used in the formula, or to show the source ranges of a
 * selected chart object.
 *
 * Emits a "change:ranges" event when the highlighted ranges have changed.
 */
export class RangeHighlighter extends EObject<RangeHighlighterEventMap> {

    readonly #docModel: SpreadsheetModel;

    /**
     * The unique identifier of the current highlighting mode.
     */
    #highlightUid = "";

    /**
     * The token arrays currently used for highlighted ranges.
     */
    #tokenArrays = new Array<TokenArray>();

    /**
     * Additional highlighting options.
     */
    #options: Opt<RangeHighlightOptions>;

    /**
     * Range descriptors representing all highlighted ranges.
     */
    readonly #rangeDescs = new Array<HighlightRangeDescriptor>();

    /**
     * Array index of the range currently tracked.
     */
    #trackingIndex = -1;

    // constructor ------------------------------------------------------------

    constructor(docModel: SpreadsheetModel) {
        super();
        this.#docModel = docModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the range highlighting mode is currently active.
     *
     * @returns
     *  Whether the range highlighting mode is currently active.
     */
    isActive(): boolean {
        return this.#highlightUid.length > 0;
    }

    /**
     * Registers token arrays used to render highlighted cell ranges in the
     * active sheet.
     *
     * @param tokenArrays
     *  An array of token arrays, or a single token array instance, to be used
     *  for range highlighting.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A unique identifier for the highlighting mode started with this method
     *  call. This identifier needs to be passed to the method
     *  `endHighlighting()` in order to prevent canceling a highlighting mode
     *  started from another part of the code. If a range highlighting mode
     *  with high priority is currently active (see option `priority`), `null`
     *  will be returned instead.
     */
    startHighlighting(tokenArrays: TokenArray | TokenArray[], options?: RangeHighlightOptions): string | null {

        // do not break active highlighting mode with high priority
        if (this.isActive() && this.#options?.priority && !options?.priority) { return null; }

        // store tracking options for usage in event handlers
        this.#options = options;

        // unregister event handlers at old token arrays
        this.stopListening();

        // create a unique identifier for the new highlighting mode
        this.#highlightUid = EObject.makeUid();

        // store the token arrays
        this.#tokenArrays = ary.wrap(tokenArrays);
        this.#registerHandlers();

        // notify initial ranges to change handlers (e.g. renderer)
        this.#changeHandler();
        return this.#highlightUid;
    }

    /**
     * Leaves the specified range highlighting mode.
     *
     * @param uid
     *  The unique identifier of the range highlighting mode returned by the
     *  method `startHighlighting()`. If another highlighting mode with another
     *  unique identifier has been started in the meantime, this method will do
     *  nothing.
     *
     * @returns
     *  Whether the specified highlighting mode was still active, and has been
     *  left successfully.
     */
    endHighlighting(uid: string | null): boolean {

        // do nothing if the passed identifier does not match
        if (!uid || (uid !== this.#highlightUid)) { return false; }

        // unregister event handlers at old token arrays
        this.stopListening();

        // reset the identifier and token arrays
        this.#highlightUid = "";
        this.#tokenArrays.length = 0;

        // notify change handlers (e.g. renderer)
        this.#changeHandler();
        return true;
    }

    /**
     * Returns the cell range address of the specified highlighted range.
     *
     * @param rangeIndex
     *  The array index of the highlighted range.
     *
     * @returns
     *  The cell range address of the specified highlighted range if it exists;
     *  otherwise `null`.
     */
    getRange(rangeIndex: number): Range3D | null {
        const rangeDesc = this.#rangeDescs[rangeIndex];
        return rangeDesc ? rangeDesc.range : null;
    }

    /**
     * Invokes the passed callback function for all highlighted ranges.
     *
     * @param callbackFn
     *  The callback function that will be invoked for every highlighted range.
     */
    forEachRange(callbackFn: (rangeDesc: HighlightRangeDescriptor, rangeIndex: number) => void): void {
        this.#rangeDescs.forEach(callbackFn);
    }

    /**
     * Marks the specified highlighted range as the tracked range, used when
     * moving or resizing a highlighted range in the GUI.
     *
     * @param rangeIndex
     *  The array index of the highlighted range to be tracked, or `-1` to
     *  disable the tracking mode.
     */
    trackRange(rangeIndex: number): void {

        // nothing to do, if the passed index does not differ from current index
        if (this.#trackingIndex === rangeIndex) { return; }

        // reset tracking flag in old highlighted range
        const oldRangeDesc = this.#rangeDescs[this.#trackingIndex];
        if (oldRangeDesc) { oldRangeDesc.tracking = false; }
        this.#trackingIndex = rangeIndex;

        // set tracking flag in new highlighted range
        const newRangeDesc = this.#rangeDescs[this.#trackingIndex];
        if (newRangeDesc) { newRangeDesc.tracking = true; }

        // notify change handlers
        this.trigger("change:ranges");
    }

    /**
     * Returns whether a highlighted range is currently marked as tracking
     * range (see method `trackRange()` for more details).
     *
     * @returns
     *  Whether a highlighted range is currently marked as tracking range.
     */
    hasTrackedRange(): boolean {
        return !!this.#rangeDescs[this.#trackingIndex];
    }

    /**
     * Modifies the cell range address of a single reference token inside a
     * single token array currently registered for range highlighting.
     *
     * @param rangeIndex
     *  The array index of a highlighted range.
     *
     * @param range
     *  The new cell range address to be inserted into the reference token. The
     *  absolute/relative state of the reference components will not be
     *  changed.
     */
    changeRange(rangeIndex: number, range: Range): void {

        // the range descriptor containing all information to find the formula token to modify
        const rangeDesc = this.#rangeDescs[rangeIndex];
        // the token array instance
        const tokenArray = rangeDesc ? this.#tokenArrays[rangeDesc.arrayIndex] : undefined;

        // modify the reference token, if the indexes are valid
        if (rangeDesc && tokenArray) {
            tokenArray.modifyToken(rangeDesc.tokenIndex, token => (token instanceof ReferenceToken) && token.setRange(range));
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Handles change events from any of the token arrays, and notifies the own
     * event handlers.
     */
    #changeHandler(): void {

        // the reference addresses for relocation
        const refAddress = this.#options?.refAddress ?? Address.A1;
        const targetAddress = this.#options?.targetAddress ?? Address.A1;
        // whether dragging the cell ranges is enabled
        const draggable = this.#options?.draggable;

        // process all registered token arrays
        this.#rangeDescs.length = 0;
        this.#tokenArrays.forEach((tokenArray, arrayIndex) => {

            // array of range info objects from the current token array
            const rangesResult = tokenArray.extractRanges(refAddress, targetAddress, this.#options);

            // append highlight descriptor objects to the resulting array
            rangesResult.forEach(rangeInfo => {
                this.#rangeDescs.push(new HighlightRangeDescriptor(rangeInfo, arrayIndex, draggable));
            });
        });

        // restore the tracking flag
        const rangeDesc = this.#rangeDescs[this.#trackingIndex];
        if (rangeDesc) { rangeDesc.tracking = true; }

        // notify change handlers
        this.trigger("change:ranges");
    }

    /**
     * Registers the change listener at all current token arrays.
     */
    #registerHandlers(): void {
        for (const tokenArray of this.#tokenArrays) {
            this.listenToAllEvents(tokenArray, this.#changeHandler);
        }
        this.listenTo(this.#docModel, [
            "insert:name", "delete:name", "change:name",
            "insert:table", "delete:table", "change:table"
        ], this.#changeHandler);
    }
}
