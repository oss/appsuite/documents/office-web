/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { to, ary, dict } from "@/io.ox/office/tk/algorithms";
import { type InputSelection, getInputSelection } from "@/io.ox/office/tk/dom";
import { DObject, EObject } from "@/io.ox/office/tk/objects";

import { cloneAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { IUndoManagerEventMap, IUndoManager } from "@/io.ox/office/editframework/model/iundomanager";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { splitFormulaParts } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import type { SheetTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import type { TransformFormulaResultFn, FormulaUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";

// types ======================================================================

export interface UndoActionData {
    text: string;
    selection: InputSelection;
    attrSet: Dict;
}

/**
 * Type mapping for the events emitted by `CellUndoManager` instances.
 */
export interface CellUndoManagerEventMap extends IUndoManagerEventMap {

    /**
     * Will be emitted after an undo action has been applied.
     *
     * @param undoAction
     *  The applied undo action.
     */
    "action:undo": [undoAction: UndoAction];

    /**
     * Will be emitted after a redo action has been applied.
     *
     * @param redoAction
     *  The applied redo action.
     */
    "action:redo": [redoAction: UndoAction];

}

// class UndoAction ===========================================================

/**
 * A single undo action (text contents, selection, text formatting attributes,
 * and other settings) used in cell edit mdoe.
 *
 * @param text
 *  The current text contents.
 *
 * @param selection
 *  The current text selection.
 *
 * @param attrSet
 *  Attribute set with all text formatting attributes.
 *
 * @param sealed
 *  Whether this undo action is fixed, i.e. cannot be updated with other text
 *  contents.
 */
class UndoAction extends DObject implements UndoActionData {

    text: string;
    selection: InputSelection;
    formulaPrefix = "";

    readonly attrSet: Dict;
    readonly sealed: boolean;
    readonly tokenArray: SheetTokenArray;

    // constructor ------------------------------------------------------------

    constructor(
        sheetModel: SheetModel,
        text: string,
        selection: InputSelection,
        attrSet: Dict,
        sealed: boolean,
        refAddress: Address
    ) {
        super();
        this.text = text;
        this.selection = selection;
        this.attrSet = attrSet;
        this.sealed = sealed;
        this.tokenArray = this.member(sheetModel.createCellTokenArray());
        this.#parseFormula(refAddress);
    }

    // public methods ---------------------------------------------------------

    /**
     * Overwrites the text contents and selection in this instance.
     */
    updateText(text: string, selection: InputSelection, refAddress: Address): void {
        this.text = text;
        this.selection = selection;
        this.#parseFormula(refAddress);
    }

    // private methods --------------------------------------------------------

    /**
     * Transforms the formula expressions in all undo actions according to the
     * passed formula update task.
     */
    #parseFormula(refAddress: Address): void {
        const parts = splitFormulaParts(this.text);
        if (parts) {
            this.tokenArray.parseFormula("ui", parts.expr, refAddress);
            this.formulaPrefix = parts.prefix;
        } else {
            this.tokenArray.clearTokens();
            this.formulaPrefix = "";
        }
    }
}

// class CellUndoManager ======================================================

/**
 * A special undo manager that will be used to track the changes made in a cell
 * while the cell edit mode is active (without generating any document
 * operations).
 *
 * Additionally, the instance keeps track of the current location of the edited
 * cell (in the properties `sheetModel` and `editAddress`). This address may
 * change in concurrent editing mode when another user manipulates the sheet
 * while a cell is being edited.
 */
export class CellUndoManager extends EObject<CellUndoManagerEventMap> implements IUndoManager<CellUndoManagerEventMap> {

    /**
     * The model of the sheet containing the edited cell.
     */
    readonly sheetModel: SheetModel;

    /**
     * The address of the edited cell.
     */
    readonly editAddress: Address;

    /**
     * Specifies whether plain text editing without number recognition and
     * formula parsing is currently active.
     */
    readonly textOnlyMode: boolean;

    // array of all known undo actions
    readonly #actionStack = this.member<UndoAction[]>([]);

    // index of the current undo action in the stack
    #stackIndex = -1;

    // constructor ------------------------------------------------------------

    /**
     * @param sheetModel
     *  The model of the sheet containing the edited cell.
     *
     * @param editAddress
     *  The address of the edited cell.
     */
    constructor(sheetModel: SheetModel, editAddress: Address) {
        super();
        this.sheetModel = sheetModel;
        this.editAddress = editAddress;
        this.textOnlyMode = sheetModel.cellCollection.isTextFormat(editAddress);
    }

    // public methods ---------------------------------------------------------

    /**
     * Saves the passed text as initial undo action (e.g. the existing contents
     * of a cell).
     *
     * @param text
     *  The initial text to be stored in the stack.
     */
    saveInitialAction(text: string): void {
        this.#saveAction({ text, selection: { start: 0, end: text.length }, attrSet: dict.create() });
    }

    /**
     * Saves the current undo action of the passed text input control on top of
     * the undo stack. If the current undo action is not on top of the stack,
     * all following undo actions will be deleted first.
     *
     * @param input
     *  The text input control.
     */
    saveInputAction(input: HTMLTextAreaElement): void {
        this.#saveAction({ text: input.value, selection: getInputSelection(input) });
    }

    /**
     * Saves the passed formatting attribute set on top of the undo stack. If
     * the current undo action is not on top of the stack, all following undo
     * actions will be deleted first.
     *
     * @param attrSet
     *  The attribute set with changed text formatting attributes currently
     *  used for editing the cell text.
     */
    saveFormatAction(attrSet: Dict): void {
        this.#saveAction({ attrSet });
    }

    /**
     * Transforms the passed formula expression according to the formula update
     * task with UI formula grammar.
     */
    transformFormulas(tokenArray: SheetTokenArray, updateTask: FormulaUpdateTask, resultFn: TransformFormulaResultFn): void {

        // nothing to do in text-only mode
        if (this.textOnlyMode) { return; }

        // the formula grammar used in the UI
        const grammar = tokenArray.docModel.formulaGrammarUI;

        // transform all formulas in the undo stack
        this.#actionStack.forEach(action => {
            updateTask.transformFormula(action.tokenArray, grammar, newExpr => {
                action.updateText(action.formulaPrefix + newExpr, action.selection, this.editAddress);
            }, { notifyAlways: true });
        });

        // transform the passed token array
        updateTask.transformFormula(tokenArray, grammar, resultFn, { notifyAlways: true });
    }

    // IUndoManager implementation --------------------------------------------

    /**
     * Returns the number of undo actions available on the undo stack.
     *
     * @returns
     *  The number of undo actions available on the undo stack.
     */
    getUndoCount(): number {
        return this.#stackIndex;
    }

    /**
     * Restores the top-most undo action of the undo stack.
     */
    undo(): void {
        this.#emitNextAction(true);
    }

    /**
     * Returns the number of undo actions available on the redo stack.
     *
     * @returns
     *  The number of undo actions available on the redo stack.
     */
    getRedoCount(): number {
        return this.#actionStack.length - this.#stackIndex - 1;
    }

    /**
     * Restores the top-most undo action of the redo stack.
     */
    redo(): void {
        this.#emitNextAction(false);
    }

    // private methods --------------------------------------------------------

    /**
     * Saves the specified undo action on top of the undo stack.
     *
     * @param changeSet
     *  The change set with all properties for the new undo action.
     */
    #saveAction(changeSet: Partial<UndoActionData>): void {

        // the current undo action in the stack
        const undoAction = this.#actionStack[this.#stackIndex];
        // the attribute set passed to this method
        const attrSet = changeSet.attrSet;
        // changed attributes always create a new sealed undo action
        const newSealed = !undoAction || (!!attrSet && !_.isEqual(attrSet, undoAction.attrSet));

        // resolve the settings to be pushed into the new undo action
        const newText = changeSet.text ?? undoAction?.text ?? "";
        const newSelection = changeSet.selection ?? undoAction?.selection ?? { start: 0, end: 0 };
        const newAttrSet = attrSet ? cloneAttributeSet(attrSet) : to.dict(undoAction?.attrSet, true);

        // try to update the current (top-most, not fixed) undo action with current text,
        // otherwise delete all remaining (redo) actions, and push the new undo action
        if (undoAction && !undoAction.sealed && !newSealed && (this.#stackIndex + 1 === this.#actionStack.length)) {
            undoAction.updateText(newText, newSelection, this.editAddress);
        } else {
            this.#stackIndex += 1;
            ary.destroy(this.#actionStack.splice(this.#stackIndex));
            this.#actionStack.push(new UndoAction(this.sheetModel, newText, newSelection, newAttrSet, newSealed, this.editAddress));
        }

        // notify listeners
        this.trigger("change:stack");
    }

    /**
     * Emits an "action:undo" or "action:redo" event for the next undo or redo
     * action, and moves the stack pointer into the correct direction.
     *
     * @param undo
     *  Whether to pop the next undo action (`true`), or redo action (`false`)
     *  from the stack.
     */
    #emitNextAction(undo: boolean): void {
        const nextIndex = this.#stackIndex + (undo ? -1 : 1);
        if ((0 <= nextIndex) && (nextIndex <= this.#actionStack.length)) {
            this.#stackIndex = nextIndex;
            this.trigger(undo ? "action:undo" : "action:redo", this.#actionStack[nextIndex]);
            this.trigger("change:stack"); // for controller updates
        }
    }
}
