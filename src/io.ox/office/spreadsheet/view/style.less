/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";

/* ========================================================================= */
/* spreadsheet application                                                   */
/* ========================================================================= */

/* Necessary for the Viewer with plugged Spreadsheet, when the document
    currently viewed is opened for editing.
    The Drive side bar would be visible after launching Spreadsheet. */
.window-container[data-app-name="io.ox/office/spreadsheet"] {
    z-index: 2;
}

/* restrict all definitions to the OX Spreadsheet application */
.io-ox-office-main.io-ox-office-spreadsheet-main {

    /* z-index of the text input control rendered above the cells */
    --spreadsheet-input-z-index: 70;
    /* z-index of the overlay nodes used while tracking column/row size, and split tracking */
    --spreadsheet-overlay-z-index: 100;

    /* ===================================================================== */
    /* header panes and grid panes                                           */
    /* ===================================================================== */

    .pane-root {
        overflow: hidden;

        // background color outside the sheet area for grid and header panes (right of last column, below last row)
        --pane-root-background: #ddd;
        :root.dark & {
            --pane-root-background: #222;
        }

        /* column/row resize tracking ====================================== */

        .resize-tracking {

            > * {
                display: none;
                box-sizing: border-box;
                border: 2px fade(@doc-selection-color, 50%) none;
            }

            &[data-orientation="columns"] {
                > * { top: 0; bottom: 0; }
                > .leading { left: -10px; border-right-style: solid; }
                > .trailing { right: -10px; border-left-style: solid; }
            }

            &[data-orientation="rows"] {
                > * { left: 0; right: 0; }
                > .leading { top: -10px; border-bottom-style: solid; }
                > .trailing { bottom: -10px; border-top-style: solid; }
            }

            &.collapsed > * {
                border-color: fade(@doc-selection-color-collapsed, 50%);
            }
        }

        &.tracking-active .resize-tracking > * {
            display: block;
            z-index: var(--spreadsheet-overlay-z-index);
            background: fade(black, 3%); // always black regardless of theme
        }

        /* pane splits and tracking ======================================== */

        > .split-line {
            position: absolute;
            background: var(--strong-overlay-color);

            &.columns {
                top: 0;
                bottom: 0;
            }

            &.rows {
                left: 0;
                right: 0;
            }
        }

        > .split-tracking {
            box-sizing: content-box;
            z-index: var(--spreadsheet-overlay-z-index);
            touch-action: none;
            .focus-no-outline();

            &.columns { cursor: ew-resize; }
            &.rows { cursor: ns-resize; }

            // center cross must be located before tracking lines in DOM to be able to add hover
            // effects, but must be lifted on top (via z-index) to get hover events
            &.cross {
                z-index: calc(var(--spreadsheet-overlay-z-index) + 1);
                cursor: all-scroll;
            }

            &::after {
                content: "";
                display: block;
                width: 100%;
                height: 100%;
            }

            // hover effects only when tracking is inactive
            &:not(.tracking-active) {

                // hover effect for a split line
                &:not(.cross):hover::after {
                    background: fade(black, 10%);
                }

                // hover effect for both split lines when hovering the cross point
                &.cross:hover ~ .split-tracking:not(.tracking-active)::after {
                    background: fade(black, 10%);
                }
            }

            // document selection color when tracking the split lines
            &.tracking-active {
                &::after {
                    background: fade(@doc-selection-color, 50%);
                }
                &.collapsed::after {
                    background: fade(@doc-selection-color-collapsed, 50%);
                }
            }
        }

        /* special settings for text editing in the cell grid */
        .textarea-container {
            position: absolute;
            display: inline-block;
            background: white;

            &, > * { z-index: var(--spreadsheet-input-z-index); }

            > textarea {
                border-radius: 1px;
                box-shadow: 0 0 0 1px @doc-selection-color-inactive;
            }
        }
    } /* end of .pane-root */

    /* cell edit mode ====================================================== */

    .textarea-container {
        position: relative;
        padding: 0;
        background: var(--text-field-background);

        > * {
            overflow: hidden auto;
            margin: 0;
            padding: 0;
            background-color: transparent;
            // by default, Chrome renders text in underlay <div> with kerning, but text in a <textarea> without
            text-rendering: optimizelegibility;
        }

        > .underlay {
            .absolute-with-distance(0);
            color: transparent;
            white-space: pre-wrap;
            word-wrap: break-word;

            > span {
                border-radius: 1px;

                .generate-scheme-color-rules({
                    &[data-style="@{scheme-index}"] {
                        background-color: fade(@scheme-color, 10%);
                        box-shadow: 0 0 0 1px fade(@scheme-color, 50%);
                    }
                });
            }
        }

        > textarea {
            position: relative;
            display: inline-block;
            border: none;
            border-radius: 0;
            .focus-active-shadow-shadow();
        }

        // Text area on top of window on small devices because softkeyboard is often
        // too big and height is undetectable on iOS.
        &.edit-on-top {
            position: absolute;
            left: 1px;
            right: 1px;
            top: 1px;
            z-index: 10;

            > * {
                width: 100%;
                height: 100%;
                padding: 3px 7px;
                font-size: 1.25rem;
                line-height: normal;
            }
        }
    }
}
