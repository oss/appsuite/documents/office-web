/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { SMALL_DEVICE } from "@/io.ox/office/tk/dom";
import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import { FontColorToolBar } from "@/io.ox/office/textframework/view/toolbars";
import { COMBINED_TOOL_PANES } from "@/io.ox/office/spreadsheet/utils/config";

import {
    ALIGNMENT_HEADER_LABEL, NUMBERFORMAT_HEADER_LABEL, CELL_BORDER_LABEL, DECIMAL_DECREASE, DECIMAL_INCREASE,
    FORMAT_PAINTER_LABEL, HYPERLINK_BUTTON_OPTIONS, INSERT_CHART_BUTTON_OPTIONS,
    INSERT_COMMENT_OPTIONS, SHOW_COMMENTSPANE_CHECKBOX_OPTIONS
} from "@/io.ox/office/spreadsheet/view/labels";

import {
    Button, CompoundButton,
    CellFillColorPicker, CellStylePicker, HAlignmentPicker, VAlignmentPicker, MergeCellsPicker,
    FormatCategoryGroup, FormatCategoryPicker, FormatCodePicker,
    CellBorderFlagsPicker, CellBorderStylePicker, BorderWidthPicker, CellBorderColorPicker,
    ColRowOpIconButton, ColRowSizeField, InsertAutoSumButton, InsertFunctionButton,
    ImagePicker, ShapeTypePicker, ChartTypePicker, SheetTableStylePicker,
    SortMenuButton, RefreshButton
} from "@/io.ox/office/spreadsheet/view/controls";

// re-exports =================================================================

export * from "@/io.ox/office/textframework/view/toolbars";

// constants ==================================================================

const PREV_NOTE_OPTIONS = {
    icon: "png:comment-back",
    label: /*#. go to preceding note */ gt.pgettext("note-actions", "Previous"),
    tooltip: /*#. go to preceding note */ gt.pgettext("note-actions", "Go to previous note")
};

const PREV_COMMENT_OPTIONS = {
    icon: "png:comment-back",
    label: /*#. go to preceding comment */ gt.pgettext("comment-actions", "Previous"),
    tooltip: /*#. go to preceding comment */ gt.pgettext("comment-actions", "Go to previous comment")
};

const NEXT_NOTE_OPTIONS = {
    icon: "png:comment-next",
    label: /*#. go to next note */ gt.pgettext("note-actions", "Next"),
    tooltip: /*#. go to next note */ gt.pgettext("note-actions", "Go to next note")
};

const NEXT_COMMENT_OPTIONS = {
    icon: "png:comment-next",
    label: /*#. go to next comment */ gt.pgettext("comment-actions", "Next"),
    tooltip: /*#. go to next comment */ gt.pgettext("comment-actions", "Go to next comment")
};

const TOGGLE_NOTE_OPTIONS = {
    toggle: true,
    icon: "png:comment-show",
    label: /*#. show/hide the selected note in spreadsheet */ gt.pgettext("note-actions", "Show/Hide"),
    tooltip: /*#. show/hide the selected note in spreadsheet */ gt.pgettext("note-actions", "Show/Hide selected note")
};

const TOGGLE_COMMENT_OPTIONS = {
    toggle: true,
    icon: "png:comment-show",
    label: /*#. show/hide the selected comment in spreadsheet */ gt.pgettext("comment-actions", "Show/Hide"),
    tooltip: /*#. show/hide the selected comment in spreadsheet */ gt.pgettext("comment-actions", "Show/Hide selected comment")
};

const DELETE_NOTE_OPTIONS = {
    icon: "png:comment-remove",
    label: /*#. delete the selected note in spreadsheet */ gt.pgettext("note-actions", "Delete"),
    tooltip: /*#. delete the selected note in spreadsheet */ gt.pgettext("note-actions", "Delete note")
};

const DELETE_COMMENT_OPTIONS = {
    icon: "png:comment-remove",
    label: /*#. delete the selected comment in spreadsheet */ gt.pgettext("comment-actions", "Delete"),
    tooltip: /*#. delete the selected comment in spreadsheet */ gt.pgettext("comment-actions", "Delete comment")
};

const SHOW_ALL_NOTES_OPTIONS = {
    icon: "png:comment-show",
    label: /*#. show all notes in spreadsheet */ gt.pgettext("note-actions", "Show all"),
    tooltip: /*#. show all notes in spreadsheet */ gt.pgettext("note-actions", "Show all notes")
};

const SHOW_ALL_COMMENTS_OPTIONS = {
    icon: "png:comment-show",
    label: /*#. show all comments in spreadsheet */ gt.pgettext("comment-actions", "Show all"),
    tooltip: /*#. show all comments in spreadsheet */ gt.pgettext("comment-actions", "Show all comments")
};

const HIDE_ALL_NOTES_OPTIONS = {
    icon: "png:comment-show",
    label: /*#. hide all notes in spreadsheet */ gt.pgettext("note-actions", "Hide all"),
    tooltip: /*#. hide all notes in spreadsheet */ gt.pgettext("note-actions", "Hide all notes")
};

const HIDE_ALL_COMMENTS_OPTIONS = {
    icon: "png:comment-show",
    label: /*#. hide all comments in spreadsheet */ gt.pgettext("comment-actions", "Hide all"),
    tooltip: /*#. hide all comments in spreadsheet */ gt.pgettext("comment-actions", "Hide all comments")
};

const DELETE_ALL_NOTES_OPTIONS = {
    icon: "png:comment-remove",
    label: /*#. delete all notes in spreadsheet */ gt.pgettext("note-actions", "Delete all"),
    tooltip: /*#. delete all notes in spreadsheet */ gt.pgettext("note-actions", "Delete all notes")
};

const DELETE_ALL_COMMENTS_OPTIONS = {
    icon: "png:comment-remove",
    label: /*#. delete all comments in spreadsheet */ gt.pgettext("comment-actions", "Delete all"),
    tooltip: /*#. delete all comments in spreadsheet */ gt.pgettext("comment-actions", "Delete all comments")
};

// class CellColorToolBar =====================================================

/**
 * Extends the class `FontColorToolBar` with a color picker control for the
 * cell background.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class CellColorToolBar extends FontColorToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("cell/fillcolor", new CellFillColorPicker(docView), { visibleKey: "view/selection/cell" });
    }
}

// class CellStyleToolBar =====================================================

/**
 * A tool bar with a picker control for the cell style sheet.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class CellStyleToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, { visibleKey: "view/selection/cell" });

        // create the controls of this tool bar
        this.addControl("cell/stylesheet", new CellStylePicker(docView));
    }
}

// class CellAlignmentToolBar =================================================

//#. checkbox: automatic word wrapping (multiple text lines) in spreadsheet cells
const AUTO_WORD_WRAP_LABEL = gt("Automatic word wrap");

/**
 * A tool bar with controls to change text text alignment of the cells.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class CellAlignmentToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            visibleKey: "view/selection/cell",
            shrinkToMenu: {
                icon: "png:cell-h-align-auto",
                tooltip: ALIGNMENT_HEADER_LABEL,
                itemKey: "view/cell/alignment/menu"
            }
        });

        // create the controls of this tool bar
        this.addControl("cell/alignhor",  new HAlignmentPicker());
        this.addControl("cell/alignvert", new VAlignmentPicker());
        this.addControl("cell/linebreak", new Button({ icon: "png:insert-linebreak", tooltip: AUTO_WORD_WRAP_LABEL, toggle: true, dropDownVersion: { label: AUTO_WORD_WRAP_LABEL } }));
        this.addControl("cell/merge",     new MergeCellsPicker(docView));
    }
}

// class NumberFormatToolBar ==================================================

/**
 * A tool bar with controls used to change the cell number format.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class NumberFormatToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            visibleKey: "view/selection/cell",
            shrinkToMenu: {
                icon: "png:percent",
                tooltip: NUMBERFORMAT_HEADER_LABEL,
                itemKey: "view/cell/numberformat/menu"
            }
        });

        // create the controls of this tool bar
        this.addSection("category");
        this.addControl("cell/numberformat/category", new FormatCategoryGroup(docView));
        this.addControl("cell/numberformat/category", new FormatCategoryPicker(docView));
        this.addControl("cell/numberformat/code",     new FormatCodePicker(docView));

        this.addSection("actions");
        this.addControl("cell/numberformat/decimaldecrease", new Button({ icon: "png:dec-places-decrease", tooltip: DECIMAL_DECREASE, dropDownVersion: { label: DECIMAL_DECREASE } }));
        this.addControl("cell/numberformat/decimalincrease", new Button({ icon: "png:dec-places-increase", tooltip: DECIMAL_INCREASE, dropDownVersion: { label: DECIMAL_INCREASE } }));
    }
}

// class CellBorderToolBar ====================================================

/**
 * A tool bar with controls used to change the cell border style.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class CellBorderToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            visibleKey: "view/selection/cell",
            shrinkToMenu: {
                icon: "png:cell-style",
                tooltip: CELL_BORDER_LABEL,
                itemKey: "view/cell/border/menu",
                // DOCS-2548: exclude window blocker from "auto-close" for `CellBorderFlagsPicker`
                focusableNodes: ".io-ox-office-main.window-blocker"
            }
        });

        // controller key for the cell border style (depends on file format)
        const borderStyleKey = this.docApp.isOOXML() ? "cell/border/style/preset" : "cell/border/style";

        // create the controls of this tool bar
        this.addControl("cell/border/flags", new CellBorderFlagsPicker(docView));
        this.addControl(borderStyleKey,      new CellBorderStylePicker(docView));
        this.addControl("cell/border/width", new BorderWidthPicker(docView), { visibleKey: "document/odf" });
        this.addControl("cell/border/color", new CellBorderColorPicker(docView));
    }
}

// class CellFormatPainterToolBar =============================================

/**
 * A tool bar with a controls to copy the formatting of the selected cell.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class CellFormatPainterToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, { visibleKey: "view/selection/cell" });

        // create the controls of this tool bar
        this.addControl("cell/painter", new Button({ icon: "png:format-painter", tooltip: FORMAT_PAINTER_LABEL, toggle: true, dropDownVersion: { label: FORMAT_PAINTER_LABEL } }));
    }
}

// class ColRowToolBar ========================================================

/**
 * A tool bar with controls to modify columns or rows in the sheet.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 *
 * @param {Boolean} columns
 *  Whether to create controls for columns (true), or for rows (false).
 */
export class ColRowToolBar extends ToolBar {

    constructor(docView, columns) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl(columns ? "column/insert" : "row/insert", new ColRowOpIconButton(true, columns));
        this.addControl(columns ? "column/delete" : "row/delete", new ColRowOpIconButton(false, columns));
        if (!COMBINED_TOOL_PANES) {
            const sizeKey = columns ? "column/width/active" : "row/height/active";
            this.addControl(sizeKey, new ColRowSizeField(docView, columns));
        }
    }
}

// class InsertContentToolBar =================================================

/**
 * A tool bar with controls to insert something into the selected cells.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class InsertContentToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, { visibleKey: "view/selection/cell" });

        // create the controls of this tool bar
        this.addSection("formulas");
        this.addControl("cell/autoformula", new InsertAutoSumButton());
        this.addControl("function/insert/dialog", new InsertFunctionButton());
        this.addSection("hyperlinks");
        this.addControl("hyperlink/edit/dialog", new Button(HYPERLINK_BUTTON_OPTIONS));
    }
}

// class InsertDrawingToolBar =================================================

/**
 * A tool bar with controls to insert drawing objects into the sheet.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class InsertDrawingToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("image/insert/dialog", new ImagePicker());
        this.addControl("shape/insert", new ShapeTypePicker(docView));
        this.addControl("chart/insert", new ChartTypePicker(docView, INSERT_CHART_BUTTON_OPTIONS));
    }
}

// class InsertCommentToolBar =================================================

/**
 * A tool bar with controls to insert comments into the sheet.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class InsertCommentToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("threadedcomment/insert", new Button(INSERT_COMMENT_OPTIONS), { visibleKey: "document/ooxml" });
    }
}

// class TableToolBar =========================================================

/**
 * A tool bar with controls to manipulate table ranges.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class TableToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        this.addSection("style");
        this.addControl("table/stylesheet", new SheetTableStylePicker(docView), { visibleKey: "document/ooxml" });

        this.addSection("sort");
        this.addControl("table/sort", new SortMenuButton(docView));

        this.addSection("filter");
        //this.addControl("table/filter", new FilterButton());
        this.addControl("table/refresh", new RefreshButton());
    }
}

// class NotesToolBar =========================================================

/**
 * A tool bar with controls to manipulate cell notes.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class NotesToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // in OOXML, cell notes are called "Notes", in ODS, cell notes are called "Comments" (no support for threaded comments yet)
        const oox = docView.docApp.isOOXML();

        // for OOXML, create a dropdown compound menu that will be shown next to controls for (threaded) comments
        const target = oox ? this.addControl("view/notes/menu", new CompoundButton(docView, { label: gt("Notes"), skipStateAttr: true })) : this;

        // navigation controls (Prev/Next)
        target.addSection("navigation");
        target.addControl("note/goto/prev", new Button(oox ? PREV_NOTE_OPTIONS : PREV_COMMENT_OPTIONS));
        target.addControl("note/goto/next", new Button(oox ? NEXT_NOTE_OPTIONS : NEXT_COMMENT_OPTIONS));

        // form controls to change the active (selected) note
        target.addSection("activenote");
        target.addControl("note/visible", new Button(oox ? TOGGLE_NOTE_OPTIONS : TOGGLE_COMMENT_OPTIONS));
        target.addControl("note/delete",  new Button(oox ? DELETE_NOTE_OPTIONS : DELETE_COMMENT_OPTIONS));

        // form controls to change all notes in the sheet
        target.addSection("allnotes");
        target.addControl("note/show/all",   new Button(oox ? SHOW_ALL_NOTES_OPTIONS : SHOW_ALL_COMMENTS_OPTIONS));
        target.addControl("note/hide/all",   new Button(oox ? HIDE_ALL_NOTES_OPTIONS : HIDE_ALL_COMMENTS_OPTIONS));
        target.addControl("note/delete/all", new Button(oox ? DELETE_ALL_NOTES_OPTIONS : DELETE_ALL_COMMENTS_OPTIONS));
    }
}

// class CommentsToolBar ======================================================

/**
 * A tool bar with controls to manipulate cell comments.
 *
 * @param {SpreadsheetView} docView
 *  The document view instance containing this tool bar.
 */
export class CommentsToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        this.addSection("insert");
        this.addControl("threadedcomment/insert", new Button({ icon: "png:comment-add", label: gt.pgettext("comment-actions", "Insert comment") }));

        this.addSection("navigation");
        this.addControl("comment/goto/prev", new Button(PREV_COMMENT_OPTIONS));
        this.addControl("comment/goto/next", new Button(NEXT_COMMENT_OPTIONS));

        this.addSection("delete");
        this.addControl("threadedcomment/delete/thread", new Button(DELETE_COMMENT_OPTIONS));
        this.addControl("threadedcomment/delete/all",    new Button(DELETE_ALL_COMMENTS_OPTIONS));

        if (!SMALL_DEVICE) {
            this.addSection("view");
            this.addControl("view/commentspane/show", new Button({ icon: "png:comment-show", toggle: true, ...SHOW_COMMENTSPANE_CHECKBOX_OPTIONS }));
        }
    }
}
