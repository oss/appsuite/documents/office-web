/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is, str, fun, itr, json } from '@/io.ox/office/tk/algorithms';
import { getClipboardData, parseAndSanitizeHTML } from '@/io.ox/office/tk/utils';
import { TOUCH_DEVICE, parseIntAttribute, convertLengthToHmm, convertCssLength, convertCssLengthToHmm, escapeHTML, cleanXML, clearBrowserSelection, Rectangle } from '@/io.ox/office/tk/dom';

import { hasFileImageExtension, hasUrlImageExtension, insertDataUrl, insertFile, insertURL, isAbsoluteUrl, isBase64 } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { insertAttribute, getCssTextDecoration } from '@/io.ox/office/editframework/utils/attributeutils';
import { checkForHyperlink } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { OP_STD_TOKEN } from '@/io.ox/office/editframework/model/formatter/baseformatter';

import { MAX_FILL_CELL_COUNT, modelLogger, MergeMode, AnchorMode, Address, AddressArray, IntervalArray, Range, RangeArray, makeRejected } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { NoteModelConfig } from '@/io.ox/office/spreadsheet/model/drawing/notemodel';
import { getDrawingTypeLabel } from '@/io.ox/office/spreadsheet/view/labels';
import { generateOperationsFromModelData } from '@/io.ox/office/spreadsheet/view/chartcreator';

// constants ==================================================================

// the header text for HTML output
const HTML_HEADER = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">\n<head><meta charset="utf-8"></head>\n';

// the localized names of the standard number format
const STANDARD_NUMBER_FORMATS = [
    'Standard',
    'General',
    'Estandarra',
    'Skoueriek',
    'Standaard',
    'Yleinen',
    'Bendras',
    '\u041E\u043F\u0448\u0442\u043E',
    '\u0639\u0627\u062F\u06CC',
    'Geral',
    'Padr\xe3o',
    'Estandar'
].map(stdToken => stdToken.toLowerCase());

// private functions ==========================================================

/**
 * Returns all descendants of the passed root node matching the passed CSS
 * selector. Additionally, the root node itself will be included in the
 * result set, if it matches the selector.
 *
 * @param {HTMLElement|jQuery|String} rootNode
 *  The root node to be searched for matching elements, or HTML mark-up
 *  text to be searched on-the-fly.
 *
 * @returns {jQuery}
 *  A jQuery collection containing all matching elements.
 */
function collectMatchingNodes(rootNode, selector) {
    rootNode = $(rootNode);
    return rootNode.filter(selector).add(rootNode.find(selector));
}

/**
 * Converts a number format code to be used with the mso-number-format style of Excel.
 *
 * @param {String} format
 *  The generic number format code.
 *
 * @returns {String}
 *  The number format code prepared for Excel.
 */
function convertNumberFormatToExcel(format) {

    // TODO: the token 'standard' may be part of the format code, e.g.: Standard;-Standard;0
    if (format.toLowerCase() === 'standard') {
        return OP_STD_TOKEN;
    }

    // use UTF-8 representation
    return str.cleanNPC(format)
        // replace the ampersand with the text &amp; (must be done first!)
        .replace(/&/g, '&amp;')
        // replace space or backslash space with the text \\0020;
        .replace(/(\\\s|\s)/g, '\\0020')
        // replace backslash double quote with the text \\005c&#34;
        .replace(/\\"/g, '\\005c&#34;')
        // replace backslash percent with the text \\005c&#37;
        .replace(/\\%/g, '\\005c&#37;')
        // replace backslash exclamation mark with the text \\005c&#33;
        .replace(/\\!/g, '\\005c&#33;')
        // replace double backslash the text \\005c;&#92;
        .replace(/\\\\/g, '\\005c\\005c')
        // replace the left angle bracket with the text &lt;
        .replace(/</g, '&lt;')
        // replace the right angle bracket with the text &gt;
        .replace(/>/g, '&gt;')
        // replace the double quote character with the text &#34;
        .replace(/"/g, '&#34;')
        // replace the apostrophe with the text &#39; (&apos; is not an HTML entity!)
        .replace(/'/g, '&#39;');
}

/**
 * Converts a number format code to be used with the sdnum attribute of Calc.
 *
 * @param {String} format
 *  The generic number format code.
 *
 * @returns {String}
 *  The number format code prepared for Calc.
 */
function convertNumberFormatToCalc(format) {
    // TODO: the token 'standard' may be part of the format code, e.g.: Standard;-Standard;0
    return (format.toLowerCase() === 'standard') ? OP_STD_TOKEN : escapeHTML(format);
}

/**
 * Returns true for the standard number format.
 *
 * @param {String} format
 *  The number format code as String.
 *
 * @returns {Boolean}
 *  Returns true for the standard number format.
 */
function isStandardNumberFormat(format) {
    format = format.toLowerCase();
    return STANDARD_NUMBER_FORMATS.some(function (stdToken) {
        return format.indexOf(stdToken) > -1;
    });
}

/**
 * Creates a number format cell style object from a Calc number format
 * string.
 *
 * @param {String|Null} formatCode
 *  The number format code as String.
 *
 * @returns {Object|Null}
 *  Result object, with the properties 'format' and 'lcid'.
 */
function parseCalcNumberFormat(formatCode) {

    if (!is.string(formatCode)) { return null; }

    if (isStandardNumberFormat(formatCode)) {
        return { format: 0, lcid: -1 };
    }

    var matches = /(?:(\d{4,5}|0);){0,1}(?:(\d{4,5}|0);){0,1}(.*)/.exec(formatCode);
    var lcid = -1;

    if (matches && matches[3]) {
        matches[2] = parseInt(matches[2], 10);
        if (matches[2] > 0) {
            lcid = matches[2];
        } else {
            matches[1] = parseInt(matches[1], 10);
            lcid = (matches[1] > 0) ? matches[1] : 1033;
        }

        return { format: matches[3], lcid };
    }

    return null;
}

/**
 * Returns the direct child element that matches the given jQuery selector.
 * Is limited to work inside HTML tables, e.g. find a 'strong' as child of a 'td' element.
 *
 * @param {jQuery} element
 *  The jQuery root element to start looking for the child.
 *
 * @returns {jQuery}
 *  The matching child element.
 */
function findMatchingChild(element, selector) {
    return element.find(selector).filter(function () {
        var parent = $(this).parents('span,a,th,td,tr,tfoot,thead,tbody,colgroup,table').first();
        return parent[0] === element[0];
    });
}

/**
 * Returns true if the given jQuery selector matches a direct child element.
 * Is limited to work inside HTML tables, e.g. find a 'strong' as child of a 'td' element.
 *
 * @param {jQuery} element
 *  The jQuery root element to start looking for the child.
 *
 * @returns {Boolean}
 *  Returns true if a child of the given 'element' matches the given 'selector'.
 */
function hasMatchingChild(element, selector) {
    return findMatchingChild(element, selector).length > 0;
}

/**
 * Converts a CSS font size with measurement unit into a HTML font size.
 *
 * @param {String} cssSize
 *  The CSS font size value with its measurement unit to be converted, as String.
 *
 * @returns {Number}
 *  The converted CSS size.
 */
function convertToFontSize(cssSize) {
    var size = convertCssLength(cssSize, 'pt');
    if (size > 30) { return 7; }    // 7 -> 36pt
    if (size > 21) { return 6; }    // 6 -> 24pt
    if (size > 16) { return 5; }    // 5 -> 18pt
    if (size > 13) { return 4; }    // 4 -> 14pt
    if (size > 11) { return 3; }    // 3 -> 12pt
    if (size > 9) { return 2; }     // 2 -> 10pt
    if (size > 0) { return 1; }     // 1 -> 8pt
    return 0;
}

/**
 * Converts a HTML font size String (used as size attribute inside a font element)
 * into a size in point.
 *
 * @param {String} size
 *  The font size to be converted, as String.
 *
 * @returns {Number}
 *  The converted size, in point.
 */
function convertFontSizeToPoint(size) {
    switch (size) {
        case '1':
            return 8;
        case '2':
            return 10;
        case '3':
            return 12;
        case '4':
            return 14;
        case '5':
            return 18;
        case '6':
            return 24;
        case '7':
            return 36;
        default:
            return 0;
    }
}

/**
 * Returns the attribute map for the 'character' attribute family of the
 * HTML element.
 *
 * @param {jQuery} element
 *  The HTML element.
 *
 * @returns {Object}
 *  The character attribute map.
 */
function getCharAttributes(element) {

    // attribute map for the 'character' attribute family
    var charAttrs = {};
    // the <font> element, if HTML elements are used instead of CSS
    var fontElement = findMatchingChild(element, 'font');

    var fontName = fontElement.attr('face') || element.css('font-family');
    if (is.string(fontName) && fontName.length > 0) {
        fontName = (fontName.split(',')[0]).replace(/["']/g, '');
        charAttrs.fontName = fontName;
    }

    var fontSize = convertFontSizeToPoint(fontElement.attr('size')) || convertCssLength(element.css('font-size'), 'pt');
    if (fontSize > 0) {
        charAttrs.fontSize = fontSize;
    }

    if (hasMatchingChild(element, 'em,i')) {
        charAttrs.italic = true;
    } else {
        var fontStyle = element.css('font-style');
        if (is.string(fontStyle) && fontStyle.length > 0) {
            charAttrs.italic = fontStyle === 'italic';
        }
    }

    if (hasMatchingChild(element, 'strong,b')) {
        charAttrs.bold = true;
    } else {
        var fontWeight = element.css('font-weight');
        if (is.string(fontWeight) && fontWeight.length > 0) {
            charAttrs.bold = /^(bold|bolder)$/.test(fontWeight) || (parseInt(fontWeight, 10) >= 600);
        }
    }

    var textDecoration = element.css('text-decoration');
    if (hasMatchingChild(element, 'u')) {
        charAttrs.underline = true;
    } else if (is.string(textDecoration) && textDecoration.length > 0) {
        charAttrs.underline = textDecoration.indexOf('underline') >= 0;
    }
    if (hasMatchingChild(element, 's')) {
        charAttrs.strike = 'single';
    } else if (is.string(textDecoration) && textDecoration.length > 0) {
        charAttrs.strike = (textDecoration.indexOf('line-through') >= 0) ? 'single' : 'none';
    }

    var color = Color.parseCSS(fontElement.attr('color') || element.css('color'));
    if (color) {
        charAttrs.color = color.toJSON();
    }

    return charAttrs;
}

/**
 * Returns the attribute map for the 'cell' attribute family of the HTML
 * element.
 *
 * @param {jQuery} element
 *  The HTML element.
 *
 * @returns {Object}
 *  The cell attribute map.
 */
function getCellAttributes(element) {

    // attribute map for the 'cell' attribute family
    var cellAttrs = {};

    // map CSS border style to operation border line style
    function mapCssBorderStyle(style) {
        switch (style) {
            case 'solid':
            case 'groove':
            case 'ridge':
            case 'inset':
            case 'outset':
                return 'single';
            case 'double':
                return 'double';
            case 'dotted':
                return 'dotted';
            case 'dashed':
                return 'dashed';
            default:
                return 'none';
        }
    }

    // check value and filter prefix (webkit-, moz-, ms-)
    var alignHor = element.attr('align') || element.css('text-align');
    alignHor = is.string(alignHor) ? /(?:^|-)(left|center|right|justify)$/i.exec(alignHor) : null;
    if (alignHor) { cellAttrs.alignHor = alignHor[1].toLowerCase(); }

    // check value and filter prefix (webkit-, moz-, ms-)
    var alignVert = element.attr('valign') || element.css('vertical-align');
    alignVert = is.string(alignVert) ? /(?:^|-)(top|middle|bottom|justify)$/i.exec(alignVert) : null;
    if (alignVert) { cellAttrs.alignVert = alignVert[1].toLowerCase(); }

    var wrapText = element.css('white-space');
    if (is.string(wrapText) && wrapText.length > 0) {
        cellAttrs.wrapText = wrapText !== 'nowrap';
    }

    var fillColor = Color.parseCSS(element.attr('bgcolor') || element.css('background-color'), true);
    if (fillColor) {
        cellAttrs.fillColor = fillColor.toJSON();
    }

    ['Top', 'Bottom', 'Left', 'Right'].forEach(function (attrPart) {

        var // the element style attribute name
            oxAttrName = 'border' + attrPart,
            // the CSS attribute name
            cssAttrName = 'border-' + attrPart.toLowerCase(),
            // the border style
            borderStyle = mapCssBorderStyle(element.css(cssAttrName + '-style')),
            // the border width
            borderWidth,
            // the border color
            borderColor;

        if (borderStyle !== 'none') {

            if (_.browser.Firefox && element[0]) {
                // On FF element.css(cssAttrName + '-width') is always 0px, bug #35125
                borderWidth = element[0].style[oxAttrName + 'Width'] || element.css(cssAttrName + '-width');
            } else {
                borderWidth = element.css(cssAttrName + '-width');
            }
            borderColor = Color.parseCSS(element.css(cssAttrName + '-color'));

            cellAttrs[oxAttrName] = {
                style: borderStyle,
                width: convertCssLengthToHmm(borderWidth),
                color: borderColor ? borderColor.toJSON() : Color.AUTO
            };
        }

    });

    return cellAttrs;
}

/**
 * Retrieves the GUID of the source application from the passed HTML data.
 *
 * @param {HTMLElement|jQuery|String} rootNode
 *  The root node to be searched for the application identifier, or HTML
 *  mark-up text to be searched on-the-fly.
 *
 * @returns {String|Null}
 *  The GUID of the source application from the HTML data; or null, if no
 *  identifier has been found.
 */
function getApplicationGuid(rootNode) {
    return collectMatchingNodes(rootNode, '#ox-clipboard-data').attr('data-app-guid') || null;
}

/**
 * Returns true if the HTML contains exactly one element of the given type.
 *
 * @param {String|jQuery} html
 *  The HTML data to check.
 *
 * @param {String} query
 *  The element to search the hml node for,
 *  but only one arbitrary level deep
 *
 * @returns {jQuery|Null}
 *  Returns the matched jQuery object if such an object is found.
 *  or null otherwise
 */
function getContainedHtmlElements(html, query) {
    var jqResult = html.filter(query);
    if (!jqResult.length) {
        jqResult = html.find(query);
    }
    return (jqResult.length > 0) ? jqResult : null;
}

/**
 * Parses the plain text from the clipboard data and creates the cell contents.
 * Rows should be separated by line endings ('\r\n', '\r' or '\n')
 * and cells should be separated by tab or semicolon.
 *
 * @param {String} text
 *  The text input to parse.
 *
 * @returns {Object}
 *  A result object with the following properties:
 *  - {AddressChangeSet[]} changeSets
 *    The change sets for the cells, with cell addresses relative to the
 *    sheet origin cell A1.
 */
function parseTextData(text) {

    if (!is.string(text)) { return { changeSets: [] }; }

    // remove \r,\n, \t and ; to remove empty rows at the end of the text
    text = text.replace(/[;\s]+$/, '');

    // parse text lines according to the line ending type
    var lines = (text.indexOf('\r\n') > -1) ? text.split(/\r\n/) : text.split(/[\r\n]/);
    // look for the cell separator
    var sep = (text.indexOf('\t') > -1) ? '\t' : ';';
    // the change sets with the cell contents
    var changeSets = [];

    lines.forEach(function (line, row) {
        line.split(sep).forEach(function (text, col) {
            changeSets.push({ address: new Address(col, row), v: str.cleanNPC(text) });
        });
    });

    return { changeSets };
}

/**
 * Returns the container node with DOM data nodes for drawing objects that
 * have been copied into the clipboard by an OX editor application.
 */
function getDrawingContainerNode(clipboardNodes) {
    var containerNode = collectMatchingNodes(clipboardNodes, '#ox-clipboard-data[data-ox-drawings="true"]');
    return (containerNode.length > 0) ? containerNode.first() : null;
}

/**
 * Returns the position and size stored in the passed data node. This
 * method is tolerant against missing data attributes, and falls back to
 * reasonable default values.
 *
 * @param {jQuery} dataNode
 *  The DOM node containing a rectangle specification.
 *
 * @returns {Rectangle}
 *  The rectangle representing the location of the object.
 */
function getRectangleFromNode(dataNode, offsetX, offsetY) {
    return new Rectangle(
        offsetX + parseIntAttribute(dataNode[0], 'data-left', 0),
        offsetY + parseIntAttribute(dataNode[0], 'data-top', 0),
        parseIntAttribute(dataNode[0], 'data-width', 0) || 256,
        parseIntAttribute(dataNode[0], 'data-height', 0) || 192
    );
}

/**
 * Returns the explicit attributes stored in the passed data node..
 *
 * @param {jQuery} dataNode
 *  The DOM node containing a rectangle specification.
 *
 * @returns {Object|Null}
 *  The explicit attributes of the object.
 */
function getAttributesFromNode(dataNode) {

    // get the stringified JSON data from the 'data-attrs' element attribute
    var jsonStr = dataNode.attr('data-attrs');
    if (!jsonStr) { return null; }

    try {
        var attrs = JSON.parse(jsonStr);
        return is.dict(attrs) ? attrs : null;
    } catch {
        modelLogger.warn('$badge{ClipboardMixin} getAttributesFromNode: invalid attributes');
        return null;
    }
}

/**
 * Extracts the JSON data from the passed DOM node representing some
 * document contents in the clipboard.
 *
 * @param {jQuery} dataNode
 *  The DOM node containing JSON data.
 *
 * @returns {Any}
 *  The reconstructed JSON data; or null on error.
 */
function getJSONDataFromNode(dataNode) {

    // get the stringified JSON data from the 'data-json' element attribute
    var jsonStr = dataNode.attr('data-json');
    if (!jsonStr) { return null; }

    // parsing JSON may throw
    try {
        return JSON.parse(jsonStr);
    } catch {
        modelLogger.warn('$badge{ClipboardMixin} getJSONDataFromNode: invalid JSON data');
        return null;
    }
}

/**
 * Tries to add the 'imageData' or 'imageUrl' attribute to the
 * passed image attribute set, if possible.
 */
function addImageSourceToAttributes(imgNode, attributes) {

    // ignore all images without valid source URL
    var imageUrl = imgNode.attr('src');
    if (!imageUrl) { return false; }

    // add sub-object for image attributes
    if (!attributes.image) { attributes.image = {}; }

    // special handling for data URL, ignore all images without valid file extension
    if (isBase64(imageUrl)) {
        attributes.image.imageData = imageUrl;
        return true;
    }

    // URL must contain valid file extension
    if (hasFileImageExtension(imageUrl)) {
        attributes.image.imageUrl = imageUrl;
        return true;
    }

    return false;
}

// mix-in class ClipboardMixin ================================================

/**
 * Internal mix-in class for the class `GridPane` that implements handling
 * of the system clipboard.
 */
export default function ClipboardMixin() {

    // self reference (the `GridPane` instance)
    var gridPane = this;

    // the spreadsheet application, model, and view
    var docView = this.docView;
    const { selectionEngine } = docView;
    var docApp = docView.docApp;
    var docModel = docView.docModel;

    // document model instances
    var numberFormatter = docModel.numberFormatter;
    var addressFactory = docModel.addressFactory;
    var attrPool = docModel.sheetAttrPool;
    var autoStyles = docModel.autoStyles;

    // the clipboard node that carries the browser focus for copy/paste events
    var clipboardNode = docView.createClipboardNode().addClass(docApp.isViewerMode() ? 'viewer-mode' : '').text('\xa0').prependTo(this.$el);

    // public methods -----------------------------------------------------

    /**
     * Returns the clipboard node of this grid pane.
     *
     * @returns {JQuery<HTMLDivElement>}
     *  The clipboard node of this grid pane, as jQuery object.
     */
    this.getClipboardNode = function () {
        return clipboardNode;
    };

    /**
     * Invokes the "copy to clipboard" event handler manually for the
     * current selection. Copies the selected contents into the clipboard
     * node of this grid pane.
     */
    this.copy = function () {
        copyHandler({ external: true, type: 'copy' });
    };

    /**
     * Invokes the "cut to clipboard" event handler manually for the
     * current selection. Moves the selected contents into the clipboard
     * node of this grid pane.
     */
    this.cut = function () {
        cutHandler({ external: true, type: 'cut' });
    };

    // private methods ----------------------------------------------------

    /**
     * Clears the clipboard node and inserts the passed contents.
     *
     * @param {String|HTMLElement|jQuery} contents
     *  The contents to be inserted into the clipboard node. Can be any
     *  data that will be accepted by jQuery's append() method.
     *
     * @returns {jQuery}
     *  The clipboard node.
     */
    function insertClipboardContents(contents) {
        docApp.destroyImageNodes(clipboardNode);
        return clipboardNode.empty().append(contents);
    }

    /**
     * Returns the character styles as CSS style String.
     *
     * @param {Object|Null} attributeSet
     *  The formatting attributes of a cell, or null for undefined cells.
     *
     * @returns {String}
     *  The CSS style String.
     */
    function getCssCharacterStyles(attributeSet) {

        if (!attributeSet) { return ''; }

        // attribute map for the 'character' attribute family
        var charAttrs = attributeSet.character;
        // the character style attribute data
        var characterStyle = '';

        characterStyle += 'font-family:' + docModel.getCssFontFamily(charAttrs.fontName) + ';';
        characterStyle += 'font-size:' + charAttrs.fontSize + 'pt;';

        characterStyle += 'font-weight:' + (charAttrs.bold ? 'bold' : 'normal') + ';';
        characterStyle += 'font-style:' + (charAttrs.italic ? 'italic' : 'normal') + ';';

        characterStyle += 'text-decoration:' + getCssTextDecoration(charAttrs) + ';';

        characterStyle += 'color:' + docModel.getCssTextColor(charAttrs.color, [attributeSet.cell.fillColor]) + ';';
        return characterStyle;
    }

    /**
     * Returns the cell styles as CSS style String.
     *
     * @param {Object|Null} attributeSet
     *  The formatting attributes of a cell, or null for undefined cells.
     *
     * @returns {String}
     *  The CSS style String.
     */
    function getCssCellStyles(attributeSet) {

        if (!attributeSet) { return ''; }

        // attribute map for the 'cell' attribute family
        var cellAttrs = attributeSet.cell;
        // the cell style attribute data
        var cellStyle = '';

        if (/^(left|center|right|justify)$/.test(cellAttrs.alignHor)) { cellStyle += 'text-align:' + cellAttrs.alignHor + ';'; }
        if (/^(top|middle|bottom|justify)$/.test(cellAttrs.alignVert)) { cellStyle += 'vertical-align:' + cellAttrs.alignVert + ';'; }

        cellStyle += 'white-space:' + (cellAttrs.wrapText ? 'normal' : 'nowrap') + ';';
        cellStyle += 'background-color:' + docModel.getCssColor(cellAttrs.fillColor, 'fill') + ';';

        ['Top', 'Bottom', 'Left', 'Right'].forEach(function (attrPart) {

            var // the cell style attribute name
                oxAttrName = 'border' + attrPart,
                // the CSS attribute name
                cssAttrName = 'border-' + attrPart.toLowerCase() + ':',
                // the border cell style attribute value
                border = cellAttrs[oxAttrName],
                // the effective CSS attributes
                cssAttrs = docModel.getCssBorderAttributes(border);

            cellStyle += cssAttrName;
            cellStyle += ((cssAttrs && cssAttrs.width > 0) ? cssAttrs.style + ' ' + cssAttrs.width + 'px ' + cssAttrs.color : 'none');
            cellStyle += ';';
        });

        return cellStyle;
    }

    function getCssMergedCellBorders(range, currentStyles, cellCollection, autoStyles) {
        var cellStyle = '';

        ['Top', 'Right', 'Bottom', 'Left'].forEach(function (side) {
            // cleanup current styles from old border-attributes
            currentStyles = currentStyles.split('border-' + side.toLowerCase() + ':none;').join('');

            var adr     = (side === 'Top' || side === 'Left') ? range.a1 : range.a2,
                styleId = cellCollection.getStyleId(adr);

            if (autoStyles.isDefaultStyleId(styleId)) {
                return currentStyles;
            }

            var attributeSet    = autoStyles.getMergedAttributeSet(styleId),
                cellAttrs       = attributeSet.cell;

            var oxAttrName = 'border' + side,
                // the CSS attribute name
                cssAttrName = 'border-' + side.toLowerCase() + ':',
                // the border cell style attribute value
                border = cellAttrs[oxAttrName],
                // the effective CSS attributes
                cssAttrs = docModel.getCssBorderAttributes(border);

            cellStyle += cssAttrName;
            cellStyle += (cssAttrs && cssAttrs.width > 0) ? cssAttrs.style + ' ' + cssAttrs.width + 'px ' + cssAttrs.color : 'none';
            cellStyle += ';';
        });

        return currentStyles + cellStyle;
    }

    /**
     * Returns the character styles as a String of the HTML elements tree.
     *
     * @param {Object|Null} display
     *  The cell display string.
     *
     * @param {Object|Null} attributeSet
     *  The formatting attributes of a cell, or null for undefined cells.
     *
     * @param {String} [url]
     *  The URL of the hyperlink attached to the cell.
     *
     * @returns {String}
     *  The HTML elements String.
     */
    function getHtmlCharacterStyles(display, attributeSet, url) {

        // the character style HTML elements
        var characterStyle = '';
        // attribute map for the 'character' attribute family
        var charAttrs = attributeSet ? attributeSet.character : null;

        if (url && (url = checkForHyperlink(url))) {
            characterStyle += '<a href="' + url + '">';
        }

        if (attributeSet) {

            // create opening tags
            if (charAttrs.bold) { characterStyle += '<b>'; }
            if (charAttrs.italic) { characterStyle += '<i>'; }
            if (charAttrs.underline) { characterStyle += '<u>'; }
            if (charAttrs.strike !== 'none') { characterStyle += '<s>'; }

            characterStyle += '<font ';
            // To make it work in Calc we need to use just the font name instead of the entire CSS font family
            characterStyle += 'face="' + escapeHTML(charAttrs.fontName) + '" ';
            // We need to use rgb colors, like #RRGGBB
            characterStyle += 'color="' + docModel.getCssTextColor(charAttrs.color, [attributeSet.cell.fillColor]) + '" ';
            // Add the font-size CSS also here to support precise font sizes in Excel
            characterStyle += 'size="' + convertToFontSize(charAttrs.fontSize) + '" style="font-size:' + charAttrs.fontSize + 'pt;"';
            characterStyle += '>';
        }

        // add cell value (nothing to do for empty strings or null
        if (display) { characterStyle += escapeHTML(display); }

        if (attributeSet) {
            // create closing tags
            characterStyle += '</font>';

            if (charAttrs.strike !== 'none') { characterStyle += '</s>'; }
            if (charAttrs.underline) { characterStyle += '</u>'; }
            if (charAttrs.italic) { characterStyle += '</i>'; }
            if (charAttrs.bold) { characterStyle += '</b>'; }
        }

        if (url) { characterStyle += '</a>'; }

        return characterStyle;
    }

    /**
     * Returns the cell styles as HTML attributes String.
     *
     * @param {Object|Null} attributeSet
     *  The formatting attributes of a cell, or null for undefined cells.
     *
     * @returns {String}
     *  The HTML attributes String.
     */
    function getHtmlCellStyles(attributeSet) {

        if (!attributeSet) { return ''; }

        // attribute map for the 'cell' attribute family
        var cellAttrs = attributeSet.cell;
        // the cell style attribute data
        var cellStyle = '';

        if (/^(left|center|right|justify)$/.test(cellAttrs.alignHor)) { cellStyle += ' align="' + cellAttrs.alignHor + '"'; }
        if (/^(top|middle|bottom)$/.test(cellAttrs.alignVert)) { cellStyle += ' valign="' + cellAttrs.alignVert + '"'; }
        if (!Color.parseJSON(cellAttrs.fillColor).isAuto()) { cellStyle += ' bgcolor="' + docModel.getCssColor(cellAttrs.fillColor, 'fill') + '"'; }

        return cellStyle;
    }

    /**
     * Creates a DOM node for a drawing object, and adds data attributes
     * describing the position of the drawing object relative to the bounding
     * rectangle containing all drawing objects copied to the clipboard.
     */
    function createDataNodeFor(drawingModel, rectangle, boundRect, modelData) {

        // the data node to be returned
        var dataNode = $('<div data-type="' + drawingModel.drawingType + '">');

        // adjust rectangle relative to bounding rectangle, and the explicit attributes
        dataNode.attr({
            'data-left': rectangle.left - boundRect.left,
            'data-top': rectangle.top - boundRect.top,
            'data-width': rectangle.width,
            'data-height': rectangle.height,
            'data-attrs': JSON.stringify(drawingModel.getExplicitAttributeSet(true))
        });

        // add passed custom JSON data
        if (modelData) { dataNode.attr('data-json', JSON.stringify(modelData)); }

        return dataNode;
    }

    /**
     * Returns the BASE64 encoded image data URL, whose data URL is for the
     * given (image) drawing attributes.
     *
     * @param {Object} mergedAttr
     *  The merged image drawing attributes, containing the image width, height
     *  and source data or URL.
     *
     * @param {DrawingModel} imageModel
     *  The drawing model that represents an image
     *
     * @returns {String}
     *  The image data URL, containing the image data.
     */
    function getImageDataUrl(mergedAttr, imageModel) {

        var imageAttr = mergedAttr.image,
            imageUrl = null;

        if (imageAttr.imageData) {
            imageUrl = imageAttr.imageData;
        } else if (imageAttr.imageUrl) {

            if (/^(https?:\/\/|s?ftps?:)/.test(imageAttr.imageUrl)) {
                imageUrl = imageAttr.imageUrl;
            } else {
                var drawingNode = docView.getActiveGridPane().drawingRenderer.getDrawingFrameForModel(imageModel);
                var jqImageNode = $(drawingNode).find('img');
                var canvas = document.createElement('canvas');

                if (jqImageNode.length > 0) {
                    canvas.width = jqImageNode[0].naturalWidth || jqImageNode.width() || Math.floor(mergedAttr.drawing.width * 96 / 2540.0);
                    canvas.height = jqImageNode[0].naturalHeight || jqImageNode.height() || Math.floor(mergedAttr.drawing.height * 96 / 2540.0);

                    // retrieve image data as image/jpeg for performance reasons
                    try {
                        var canvasCtx = canvas.getContext('2d');
                        canvasCtx.drawImage(jqImageNode[0], 0, 0);
                        imageUrl = canvas.toDataURL('image/png');
                    } catch {
                        modelLogger.warn('$badge{ClipboardMixin} cannot convert image to data URL');
                    }
                }
            }
        }

        return imageUrl || 'data:,';
    }

    /**
     * Creates a HTML node, containing the data, necessary to
     * reconstruct an image object later in time (e.g. at paste)
     */
    function createDataNodeForImage(imageModel, rectangle, boundRect) {

        // create the DOM node representing the image object
        var dataNode = createDataNodeFor(imageModel, rectangle, boundRect),
            imageNode = $('<img>'),
            mergedAttr = imageModel.getMergedAttributeSet(true);

        // add image attributes
        imageNode.attr({
            width: rectangle.width,
            height: rectangle.height,
            src: getImageDataUrl(mergedAttr, imageModel)
        });

        // add image border style, if set
        var lineAttr = mergedAttr.line;
        if (lineAttr.type === 'solid') {
            imageNode.css({
                borderStyle: lineAttr.style,
                borderWidth: (lineAttr.width / 1000) + 'cm',
                borderColor: docModel.getCssColor(lineAttr.color, 'line')
            });
        }

        return dataNode.append(imageNode);
    }

    /**
     *
     */
    function createDataNodeForChart(chartModel, rectangle, boundRect) {

        if (!chartModel.isRestorable()) { return null; }

        // the JSON data to be transported in the DOM node
        var chartData = {
            modelData: chartModel.getCloneData()
        };

        // create the DOM node representing the chart object
        var dataNode = createDataNodeFor(chartModel, rectangle, boundRect, chartData);

        // try to create a replacement image that will be used when pasting into other documents
        dataNode.append(chartModel.getReplacementNode({ mimeType: 'image/png' }));

        return dataNode;
    }

    function extractElementAttributes(attrData, element) {

        attrPool.extendAttrSet(attrData.attrs, {
            character: getCharAttributes(element),
            cell: getCellAttributes(element)
        });

        // add format code information
        return Object.assign(attrData, parseCalcNumberFormat(element.attr('sdnum')));
    }

    /**
     * Creates a HTML text from the selected drawings.
     *
     * @param {DrawingModel[]} drawingModels
     *  The array of selected drawing models
     *
     * @returns {string|null}
     *  The HTML mark-up of a DOM container node with data nodes for the passed
     *  drawing objects. If none of the drawing objects could be converted to
     *  DOM nodes, null will be returned instead.
     */
    function convertDrawingModels(drawingModels) {

        // the positions of all drawing objects
        var rectangles = drawingModels.map(function (drawingModel) { return drawingModel.getRectangle(); });
        // the bounding rectangle of all drawings for the top-left position
        var boundRect = rectangles.reduce(function (boundRect, rect) { return boundRect ? boundRect.boundary(rect) : rect; }, null);
        // the resulting container up for all drawing obejcts
        var containerNode = $('<div>');

        // process all drawing models
        drawingModels.forEach(function (drawingModel, index) {

            // the location of the drawing object (already calculated above)
            var rectangle = rectangles[index];
            // the resulting data node
            var dataNode = null;

            // generate the DOM representation of the drawing model
            switch (drawingModel.drawingType) {
                case 'image':
                    dataNode = createDataNodeForImage(drawingModel, rectangle, boundRect);
                    break;
                case 'chart':
                    dataNode = createDataNodeForChart(drawingModel, rectangle, boundRect);
                    break;
            }

            // insert the data node into the container
            if (dataNode) { containerNode.append(dataNode); }
        });

        // nothing valid to copy
        if (containerNode.children().length === 0) { return ''; }

        // initialize the container node
        containerNode.attr({
            id: 'ox-clipboard-data',
            'data-app-guid': docApp.getGlobalUid(),
            'data-ox-drawings': true
        });

        return HTML_HEADER + '<body>' + containerNode[0].outerHTML + '</body>\n</html>\n';
    }

    /**
     * Creates a HTML table from the active range of the current selection.
     *
     * @param {Range} copyRange
     *  The range address.
     *
     * @param {Range} origRange
     *
     * @param {object} [options]
     *
     * @returns {String}
     *  The resulting HTML mark-up of the `<table>` element.
     */
    function getHtmlMarkupFromRange(copyRange, origRange, options) {

        function createCellData(address) {

            // cell model
            var cellModel = cellCollection.getCellModel(address);
            // auto-style id
            var styleId = cellCollection.getStyleId(address);
            // the merged attribute set of the cell
            var attributeSet = autoStyles.isDefaultStyleId(styleId) ? null : autoStyles.getMergedAttributeSet(styleId);
            // model of the cell note
            var noteModel = noteCollection.getByAddress(address);
            // model of the cell comment
            var commentModels = commentCollection.getByAddress(address);
            // JSON data for the cell
            var cellData = {};

            // cell value
            if (cellModel && cellModel.isError()) {
                cellData.error = formulaGrammar.getErrorName(cellModel.v);
            } else {
                cellData.value = cellModel ? cellModel.v : null;
            }

            // handle formula
            var formula = cellModel ? cellModel.getFormula("op:oox") : null;
            if (formula) {
                cellData.formula = formula;
                if (cellModel.mr) {
                    cellData.mr = cellModel.mr.toString();
                }
            }

            //FIXME: no styleId, there is a bug in calc!!!
            cellData.attributes = attributeSet ? _.pick(attributeSet, 'apply', 'cell', 'character'/*, 'style'*/) : {};
            cellData.styleId = styleId;
            cellData.explicit = autoStyles.getExplicitAttributeSet(styleId);

            // add data for the cell note
            if (noteModel) {
                var anchorRect = noteModel.getAnchorRectangle();
                var frameRect = noteModel.getRectangle().translateSelf(-anchorRect.right(), -anchorRect.top);
                cellData.comment = {
                    settings: noteModel.config.toJSON(),
                    attrs: noteModel.getExplicitAttributeSet(true),
                    rect: frameRect.toJSON()
                };
            }

            if (commentModels) {
                cellData.threadedComments = commentModels.map(function (commentModel) {
                    return commentModel.getJSONSettings();
                });
            }

            return cellData;
        }

        const formulaGrammar = docModel.getFormulaGrammar("op:oox");
        const { sheetModel, cellCollection, colCollection, rowCollection, noteCollection, commentCollection } = docView;
        var // the merged ranges covering the passed range
            mergedRanges = docView.mergeCollection.getMergedRanges(copyRange),
            // the formula parser
            tokenArray = sheetModel.createCellTokenArray(),
            // the top-left cell address
            startAddr = copyRange.a1;

        // DOCS-4185: skip hidden columns/rows in sheets with active filter
        const iterOptions = sheetModel.isFiltered() ? { visible: true } : undefined;
        const colIntervals = IntervalArray.from(colCollection.intervals(copyRange.colInterval(), iterOptions));
        const rowIntervals = IntervalArray.from(rowCollection.intervals(copyRange.rowInterval(), iterOptions));
        if (colIntervals.empty() || rowIntervals.empty()) { return ""; }

        const refAddress = new Address(colIntervals[0].first, rowIntervals[0].first);
        const sourceCols = origRange.cols() - copyRange.cols() + colIntervals.size();
        const sourceRows = origRange.rows() - copyRange.rows() + rowIntervals.size();
        const sourceRange = Range.fromAddressAndSize(refAddress, sourceCols, sourceRows);

        let tableMarkup = HTML_HEADER + '<body>\n<table id="ox-clipboard-data"';
        tableMarkup += ` data-ox-clipboard-type="${options?.cut ? 'cut' : 'copy'}"`;
        tableMarkup += ` data-app-guid="${docApp.getGlobalUid()}"`;
        tableMarkup += ` data-src-sheet="${sheetModel.uid}"`;
        tableMarkup += ` data-src-range="${sourceRange.toOpStr()}"`;

        modelLogger.trace(() => `orig=${origRange} copy=${copyRange} ref=${refAddress} cols=${sourceCols} rows=${sourceRows}`);

        // serialize the conditional formatting rules covering the passed range
        const pasteItems = sheetModel.cfRuleCollection.serializePasteItems(copyRange);
        if (pasteItems.length) {
            tableMarkup += ` data-ox-cond-formats="${escapeHTML(json.safeStringify(pasteItems))}"`;
        }

        tableMarkup += '><tbody>';

        for (const row of rowIntervals.indexes()) {

            // add a new row to the table markup
            tableMarkup += '\n <tr>';

            for (const col of colIntervals.indexes()) {

                const address = new Address(col, row);

                // skip cell if it is covered by a merged range
                const mergedRange = mergedRanges.findByAddress(address);
                const isReferenceCell = mergedRange?.startsAt(address);
                if (mergedRange && !isReferenceCell) { continue; }

                // the complete cell model (value, formula, style), may be null
                var cellModel = cellCollection.getCellModel(address);
                // the auto-style identifier
                var styleId = cellCollection.getStyleId(address);
                // the merged attribute set of the cell
                var attributeSet = autoStyles.isDefaultStyleId(styleId) ? null : autoStyles.getMergedAttributeSet(styleId);
                // the merged attribute set of existing cells with custom formatting
                var parsedFormat = autoStyles.isDefaultStyleId(styleId) ? null : autoStyles.getParsedFormat(styleId);

                var cssStyles = getCssCellStyles(attributeSet);

                tableMarkup += '<td';

                if (isReferenceCell) {
                    // add cell with colspan and rowspan
                    var colspan = mergedRange.cols();
                    if (colspan > 1) { tableMarkup += ' colspan="' + colspan + '"'; }
                    var rowspan = mergedRange.rows();
                    if (rowspan > 1) { tableMarkup += ' rowspan="' + rowspan + '"'; }
                    cssStyles = getCssMergedCellBorders(mergedRange, cssStyles, cellCollection, autoStyles);
                }

                // add cell and character style attributes
                let style = escapeHTML(getCssCharacterStyles(attributeSet) + cssStyles);

                // handle Excel number format
                const excelNumberFormat = parsedFormat ? convertNumberFormatToExcel(parsedFormat.formatCode) : null;
                if (excelNumberFormat) {
                    style += `mso-number-format:'${excelNumberFormat}';`;
                }

                if (style.length > 0) {
                    tableMarkup += ' style="' + style + '"';
                }

                // set the cell value type
                if (cellModel) {
                    if (cellModel.isNumber()) {
                        tableMarkup += ' x:num="' + cellModel.v + '"';
                        tableMarkup += ' sdval="' + cellModel.v + '"';
                    } else if (cellModel.isBoolean()) {
                        tableMarkup += ' x:bool="' + cellModel.v + '"';
                    } else if (cellModel.isError()) {
                        tableMarkup += ' x:err="' + escapeHTML(formulaGrammar.getErrorName(cellModel.v) || '') + '"';
                    } else if (cellModel.isText()) {
                        tableMarkup += ' x:str="' + escapeHTML(cellModel.v || '') + '"';
                    }
                }

                // handle Calc number format
                const calcNumberFormat = parsedFormat ? convertNumberFormatToCalc(parsedFormat.formatCode) : null;
                if (calcNumberFormat) {
                    tableMarkup += ` sdnum="1033;1033;${calcNumberFormat}"`;
                }

                var cellData = createCellData(address);

                if (cellData.formula) {

                    tokenArray.parseFormula("op:oox", cellData.formula, startAddr);
                    var enFormula = tokenArray.getFormula("en:a1");

                    modelLogger.log('$badge{ClipboardMixin} external clipboard - formula - range:', copyRange, ', original: ', cellData.formula, ', relocated: ', enFormula);

                    tableMarkup += ' x:fmla="' + escapeHTML(enFormula) + '"';
                    tableMarkup += ' formula="' + escapeHTML(enFormula) + '"';
                }

                if (isReferenceCell) {
                    cellData.mergedRangeCells = {};
                    cellData.mergedRange = false;

                    for (const cellEntry2 of cellCollection.cellEntries(mergedRange, { type: 'defined' })) {
                        const address2 = cellEntry2.address;
                        const col = address2.c - startAddr.c;
                        const row = address2.r - startAddr.r;
                        cellData.mergedRangeCells['col' + col + '_row' + row] = createCellData(address2);
                        cellData.mergedRange = true;
                    }
                }

                tableMarkup += ' data-ox-cell-data="' + escapeHTML(JSON.stringify(cellData)) + '"';

                // add HTML attributes
                tableMarkup += getHtmlCellStyles(attributeSet);

                tableMarkup += '>';

                // add HTML elements and cell content (also URLs returned from HYPERLINK function in cell formulas)
                var url = cellCollection.getEffectiveURL(address);
                tableMarkup += getHtmlCharacterStyles(cellModel ? cellModel.d : '', attributeSet, url);

                tableMarkup += '</td>';
            }

            tableMarkup += '</tr>';
        }

        tableMarkup += '</tr>\n</tbody></table>\n</body>\n</html>\n';

        return tableMarkup;
    }

    /**
     * Creates a plain text representation from the active range of the current
     * selection.
     *
     * @param {Range} range
     *  The range address.
     *
     * @returns {String}
     *  The resulting plain text string.
     */
    function getPlainTextFromRange(range) {

        // the resulting TAB-separated display strings
        var resultText = '';
        // the cell collection of the active sheet
        const { cellCollection } = docView;

        for (const { address } of cellCollection.cellEntries(range, { visible: true })) {
            if (address.c > range.a1.c) {
                resultText += '\t';
            } else if (address.r > range.a1.r) {
                resultText += '\r\n';
            }
            resultText += cellCollection.getDisplayString(address) || '';
        }

        return resultText;
    }

    /**
     * Handles 'copy' events of the clipboard node.
     * Creates a HTML table of the active range of the current selection
     * and generates the client clipboard id. Adds both to the system clipboard.
     * And invokes the Calcengine copy operation.
     */
    var copyHandler = modelLogger.profileMethod('$badge{ClipboardMixin} copyHandler', function (copyEvent) {

        // model of the active sheet
        const { sheetModel } = docView;
        // current selection in the active sheet
        var selection = selectionEngine.getSelection();
        // the clipboard event data
        var clipboardData = getClipboardData(copyEvent);
        // get drawing selection, if available, first
        var selectedDrawingModels = selectionEngine.getSelectedDrawingModels();
        // the HTML mark-up promise, whose resolved value is to be to copied
        var htmlMarkup = null;
        // the plain text Promise, whose resolved value is to be to copied
        var plainText = null;

        if (selectedDrawingModels.length > 0) {

            // generate HTML representation of drawing(s), no plain-text can be generated
            modelLogger.log('$badge{ClipboardMixin} copyHandler: generating HTML mark-up for ' + selectedDrawingModels.length + ' drawing objects');
            htmlMarkup = convertDrawingModels(selectedDrawingModels);

        } else {

            // bugs 40391, 48170, 62082: special handling for entire columns/rows
            const origRange = selection.activeRange().clone();
            const copyRange = origRange.clone();
            const usedRange = sheetModel.getUsedRange();

            // shorten column ranges to the used row interval
            if (addressFactory.isColRange(origRange)) {
                copyRange.a2.r = usedRange?.a2.r ?? 0;
            }

            // shorten row ranges to the used column interval
            if (addressFactory.isRowRange(origRange)) {
                copyRange.a2.c = usedRange?.a2.c ?? 0;
            }

            // restrict the number of cells to copy at the same time
            if (copyRange.cells() > MAX_FILL_CELL_COUNT) {
                docView.yellMessage('copy:overflow');
                if (!copyEvent.external) { copyEvent.preventDefault(); }
                return false;
            }

            // generate HTML table representation
            modelLogger.log('$badge{ClipboardMixin} copyHandler: generating HTML mark-up for selected cell range');
            htmlMarkup = getHtmlMarkupFromRange(copyRange, origRange, { cut: copyEvent.type === 'cut' });

            // generate plain text representation
            modelLogger.log('$badge{ClipboardMixin} copyHandler: generating plain-text representation of selected cell range');
            plainText = getPlainTextFromRange(copyRange);
        }

        // set clipboard debug pane content
        docModel.trigger('debug:clipboard', htmlMarkup);

        // if browser supports the clipboard API, add the generated data to the event
        if (clipboardData) {

            // clear clipboard and add the generated plain-text contents and and/or the HTML mark-up
            modelLogger.log('$badge{ClipboardMixin} copyHandler: add data to system clipboard');
            clipboardData.clearData();
            if (plainText) { clipboardData.setData('text/plain', plainText); }
            if (htmlMarkup) { clipboardData.setData('text/html', htmlMarkup); }

            // prevent default copy handling for desktop browsers, but not for touch devices
            if (!TOUCH_DEVICE) { copyEvent.preventDefault(); }

        } else if (htmlMarkup) {

            // add Excel XML name spaces for custom tag support
            $('html').attr({
                xmlns: 'http://www.w3.org/TR/REC-html40',
                'xmlns:o': 'urn:schemas-microsoft-com:office:office',
                'xmlns:x': 'urn:schemas-microsoft-com:office:excel'
            });

            // insert contents into the internal clipboard node, and focus it
            modelLogger.log('$badge{ClipboardMixin} copyHandler: add copied data to internal clipboard DOM node');
            insertClipboardContents(htmlMarkup).focus();

            // document.queryCommandSupported('copy') should return true if the command "is supported by the browser".
            // And if document.execCommand('copy') will succeed if called now. Checking to ensure the command was called from a user-initiated thread and other requirements are met.
            if (copyEvent.external && document.queryCommandSupported('copy')) {
                document.execCommand('copy');
            }

            // remove the XML namespaces after the browser has copied the data from the clipboard node
            window.setTimeout(function () { $('html').removeAttr('xmlns xmlns:o xmlns:x'); }, 0);
        }
    });

    /**
     * Handles 'cut' events of the clipboard node.
     * Invokes the copyHandler, then deletes the values and clears
     * the attributes of the selected cells.
     */
    function cutHandler(cutEvent) {

        // the clipboard event data
        var clipboardData = getClipboardData(cutEvent);

        // check for cell protection
        var errorCode = docView.ensureUnlockedSelection({ lockTables: 'header', lockMatrixes: 'partial', sync: true });
        if (errorCode) {
            docView.yellMessage(errorCode);
            if (!cutEvent.external) { cutEvent.preventDefault(); }
            return false;
        }

        // invoke clipboard copy handler first
        if (copyHandler(cutEvent) === false) { return false; }

        // implements deleting the selection after copy
        function deleteSelection() {
            docView.executeControllerItem(selectionEngine.hasDrawingSelection() ? 'drawing/delete' : 'cell/clear/all');
        }

        if (clipboardData) {
            // prevent default cut handling for desktop browsers, but not for touch devices
            if (!TOUCH_DEVICE) { cutEvent.preventDefault(); }
            deleteSelection();
        } else {
            // delete values and clear attributes of the selected cells (bug 39533: via controller for busy screen etc.)
            gridPane.setTimeout(deleteSelection, 0);
        }
    }

    /**
     * Inserts all drawing objects described in the passed drawing data
     * array with blocked user interface.
     *
     * @param {Array<Object>} drawingDescs
     *  The type and formatting attributes of the drawing objects to be
     *  inserted. See method DrawingMixin.execInsertDrawings() for more
     *  details.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after all drawing objects have
     *  been inserted into the document, or that will be rejected on
     *  any error.
     */
    function insertDrawings(drawingDescs) {

        // nothing to do without any valid drawing objects
        if (drawingDescs.length === 0) { return gridPane.createResolvedPromise(); }

        // index to be inserted into the names of the pasted objects
        var nameIndex = docView.drawingCollection.getModelCount({ deep: true });

        // post-processing for all drawings
        var promises = drawingDescs.map(function (drawingDesc) {

            // generate a name with effective type that may have changed e.g. from chart to image
            var typeLabel = _.noI18n.fix(getDrawingTypeLabel(drawingDesc.type));
            nameIndex += 1;
            drawingDesc.attrs.drawing.name = typeLabel + ' ' + nameIndex;

            // image attributes (needed to post-process the image URL)
            var imageAttrs = drawingDesc.attrs.image;
            if (!imageAttrs) { return; }

            // resolve BASE64 data or external URL to media URL
            var promise = null;
            if (isBase64(imageAttrs.imageData)) {
                promise = insertDataUrl(docApp, imageAttrs.imageData);
            } else if (isAbsoluteUrl(imageAttrs.imageUrl)) {
                promise = insertURL(docApp, imageAttrs.imageUrl);
            }

            // replace URL in the image attributes, remove "imageData" attribute
            return promise && promise.done(function (result) {
                imageAttrs.imageUrl = result.url;
                delete imageAttrs.imageData;
            });
        });

        // insert the new drawing objects into the document
        var promise = $.when(...promises).then(function () {
            return docApp.docController.execInsertDrawings(drawingDescs, function (generator, _sheet, position, drawingDesc) {
                if (drawingDesc.type === 'chart') {
                    generateOperationsFromModelData(generator, position, drawingDesc.modelData);
                }
            });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * tries to paste all drawing objects contained in the passed
     * drawing container node.
     */
    var pasteDrawingClipboard = modelLogger.profileMethod('$badge{ClipboardMixin} pasteDrawingClipboard', function (containerNode, otherAppGuid) {

        // model of the active sheet
        const { sheetModel } = docView;
        // pixel position of the active cell
        var activeCellRect = sheetModel.getCellRectangle(selectionEngine.getActiveCell(), { pixel: true });
        // the drawing attributes to be passed to insertDrawings()
        var drawingDescs = [];
        // the attribute pool for drawing attributes
        var attrPool = docModel.defAttrPool;

        // generate the formatting attributes to be passed to insertDrawings()
        containerNode.children().each(function () {

            // current drawing data node, as jQuery object
            var dataNode = $(this);
            // the drawing type stored in the data node
            var drawingType = dataNode.attr('data-type');
            // the location of the drawing object (relative to the bounding rectangle)
            var rectangle = getRectangleFromNode(dataNode, activeCellRect.left, activeCellRect.top);
            // the explicit formatting attributes of the object
            var attributes = getAttributesFromNode(dataNode);
            // additional JSON data from the DOM node
            var data = getJSONDataFromNode(dataNode);
            // child image node (for image objects, or as fall-back replacement image)
            var imgNode = dataNode.find('>img');
            // whether the data node has been processed successfully (ready to be pasted)
            var success = false;

            // paste charts only if they have been copied from this document
            if (drawingType === 'chart') {
                success = is.dict(data) && is.dict(data.modelData) && (docApp.getGlobalUid() === otherAppGuid);
                if (!success) { delete attributes.chart; }
            }

            // process image objects, or try to get a replacement image node as last fall-back
            if (!success && (imgNode.length > 0)) {
                drawingType = 'image';
                insertAttribute(attributes, 'drawing', 'aspectLocked', true);
                success = addImageSourceToAttributes(imgNode, attributes);
            }

            // TODO: other drawing types
            if (!success) { return; }

            // merge the formatting attributes for the target rectangle
            var anchorMode = (drawingType === 'image') ? AnchorMode.ONE_CELL : AnchorMode.TWO_CELL;
            var anchorAttrs = sheetModel.drawingCollection.getAnchorAttributesForRect(anchorMode, rectangle, { pixel: true });
            attributes = attrPool.extendAttrSet(attributes, { drawing: anchorAttrs });

            // collect the attributes for all valid objects for asynchronous insertion
            drawingDescs.push({ type: drawingType, attrs: attributes, ...data });
        });

        // lock the GUI, and insert the new drawing objects into the document
        return insertDrawings(drawingDescs);
    });

    var pasteImageClipboard = modelLogger.profileMethod('$badge{ClipboardMixin} pasteImageClipboard', function (htmlData, jqImgElements) {

        // model of the active sheet
        const { drawingCollection } = docView;
        // address of the active cell
        var activeCell = selectionEngine.getActiveCell();
        // the drawing attributes to be passed to insertDrawings()
        var drawingDescs = [];
        var promise = gridPane.createResolvedPromise(null);

        // generate the formatting attributes to be passed to insertDrawings()
        jqImgElements.get().forEach(function (srcImgNode) {
            promise = promise.then(function () {
                return docApp.createImageNode(srcImgNode.src).then(function ($imgNode) {

                    var imgNode = $imgNode[0];

                    // create the anchor attributes for the image object (zoom-independent size in 1/100 mm)
                    var width = convertLengthToHmm(imgNode.width || imgNode.naturalWidth || 250, 'px');
                    var height = convertLengthToHmm(imgNode.height || imgNode.naturalHeight || 250, 'px');
                    var anchorAttrs = drawingCollection.getAnchorAttributesForCellAndSize(AnchorMode.ONE_CELL, activeCell, width, height);

                    // add generic drawing attributes
                    var attributeSet = { drawing: anchorAttrs };
                    attributeSet.drawing.aspectLocked = true;

                    // add image source link
                    if (addImageSourceToAttributes($imgNode, attributeSet)) {
                        drawingDescs.push({ type: 'image', attrs: attributeSet });
                    }
                });
            });
        });

        // insert the new images into the document
        return promise.then(function () {
            return insertDrawings(drawingDescs);
        });
    });

    /**
     * Parses the HTML from the clipboard data for a table and
     * creates the cell contents and merged cell collection.
     *
     * @param {String|jQuery} html
     *  The HTML input to parse.
     *
     * @returns {Object}
     *  A result object with the following properties:
     *  - {AddressChangeSet[]} changeSets
     *    The change sets for the cells, with cell addresses relative to
     *    the sheet origin cell A1.
     *  - {RangeArray} mergedRanges
     *    An array of merged cell ranges.
     *  - {Dict<RangeArray>} urlMap
     *    A map with URLs as map keys, and range arrays as values.
     */
    function parseHTMLData(html) {

        // convert HTML mark-up string to DOM tree
        html = $(html);

        var formulaGrammar = docModel.getFormulaGrammar("op:oox"),
            // the clel change sets with addresses
            changeSets = [],
            // JSON data of all cell notes
            notes = [],
            // JSON data of all cell comments
            comments = [],
            // an array of ranges of merged cells
            mergedRanges = new RangeArray(),
            // the number of columns
            colCount,
            // stores all found merged ranges
            foundMergedRanges = {};

        // Bug 35126: Excel sends CSS rules containing fractional border widths, e.g.
        // 'border-top: .5pt solid ...' which will be translated to 0px borders in Chrome.
        // Only solution is to manually parse and modify the <style> element contained in
        // the pasted HTML data.
        html.find('>style').each(function () {

            // the first and only child node of the <style> element must be a text node
            if ((this.childNodes.length !== 1) || (this.firstChild.nodeType !== 3)) { return; }

            // the text contents of the <style> node
            var cssText = this.firstChild.nodeValue;

            // text must be a (multi-line) HTML comment
            if (!/^\s*<!--(.|[\r\n])+-->\s*$/.test(cssText)) { return; }

            // replace fractional line width in all border specifications with 1px
            cssText = cssText.replace(/([\r\n]\s*\{?(?:border(?:-top|-bottom|-left|-right)?|mso-diagonal-(?:down|up)):\s*)\.\d+pt /g, '$11px ');

            // replace non-standard line style 'hairline' with solid lines
            cssText = cssText.replace(/([\r\n]\s*\{?(?:border(?:-top|-bottom|-left|-right)?|mso-diagonal-(?:down|up)):.*?) hairline /g, '$1 solid ');

            // write the modified CSS rules back to the <style> element
            this.firstChild.nodeValue = cssText;
        });

        //TODO: use of Clipboard.getContainedHtmlElements(html, query)
        var table = html.find('table');
        if (table.length === 0) {
            table = html.filter('table');
        }

        // the default character and cell attributes for the table
        var tableAttrData = { attrs: attrPool.getDefaultValueSet(['character', 'cell']), format: null, lcid: null };
        delete tableAttrData.attrs.cell.formatId;
        delete tableAttrData.attrs.cell.formatCode;

        // merge in the character and cell attributes of the HTML table
        tableAttrData = extractElementAttributes(tableAttrData, table);

        // copy from same document
        const appGuid = table.attr('data-app-guid');
        const sameDoc = appGuid === docApp.getGlobalUid();
        const sameSheet = sameDoc && (docView.sheetModel.uid === table.attr('data-src-sheet'));

        // copy from any OX Spreadsheet document
        var intern = !!appGuid;

        // whether it is a cut event
        var cut = table.attr('data-ox-clipboard-type') === 'cut';

        const sourceRange = Range.parse(table.attr('data-src-range') || '');

        // make sure that table header, body and footer are in the correct order
        var rowNodes = _.union(table.find('thead tr').get(), table.find('tbody tr').get(), table.find('tfoot tr').get());

        colCount = _.max(rowNodes.map(function (rowNode) {
            return $(rowNode).children('th,td').get().reduce(function (memo, cell) {
                return memo + (parseInt(cell.colspan, 10) || 1);
            }, 0);
        }));

        rowNodes.forEach(function (rowNode, row) {

            var rowAttrData = null,
                cellId = 0,
                cellValue,
                linkChild,
                lastAttributes;

            rowNode = $(rowNode);

            var cellNodes = rowNode.children('td,th');

            for (var col = 0; col < colCount; col++) {

                var address = new Address(col, row);
                var changeSet = { address, v: null, f: null, mr: null };
                var cellAttrData = null;

                // nothing to do, if the cell is covered by a merged range (not the reference cell)
                var isMergedCell = mergedRanges.containsAddress(address);
                if (!isMergedCell || foundMergedRanges['col' + col + '_row' + row]) {

                    // the cell is not merged or it is the reference cell
                    var cellNode = $(cellNodes.get(cellId));
                    var colspan = cellNode.attr('colspan');
                    var rowspan = cellNode.attr('rowspan');

                    if (colspan || rowspan) {
                        // the cell is merged and it's a reference cell
                        colspan = (colspan && colspan.length > 0) ? parseInt(colspan, 10) : 1;
                        rowspan = (rowspan && rowspan.length > 0) ? parseInt(rowspan, 10) : 1;

                        mergedRanges.push(Range.fromIndexes(col, row, col + colspan - 1, row + rowspan - 1));
                    }

                    if (!intern) {
                        // look for Calc cell value
                        cellValue = cellNode.attr('sdval');
                        if (is.string(cellValue) && cellValue.length > 0) {
                            cellValue = parseFloat(cellValue);
                        }
                        if (!Number.isFinite(cellValue)) {
                            cellValue = cellNode.text();
                            // if the cell value contains '\n' we need to make sure that we keep only the line feeds
                            // the user wants and not those Excel added by surprise.
                            if ((cellValue.indexOf('\n') > -1) || (cellNode.find('br').length > 0)) {
                                // remove line feeds from HTML and leave only the <br> elements
                                cellNode.html(cellNode.html().replace(/<br\s*\/?>([^\n]*)\n +/gi, '<br>$1').replace(/\n /g, ''));
                                // make '\n' out of the <br> elements
                                cellNode.find('br').replaceWith('\n');
                                // finally the cell value contains only the user line feeds
                                cellValue = cellNode.text();
                            }
                        }

                        changeSet.v = is.string(cellValue) ? cleanXML(cellValue) : cellValue;
                    }

                    // insert hyperlinks
                    var url = findMatchingChild(cellNode, 'a').attr('href');
                    if (is.string(url) && url.length > 0) {
                        if ((url = checkForHyperlink(url))) {
                            changeSet.url = url;
                        }
                    }

                    var oxCellData = isMergedCell ? foundMergedRanges['col' + col + '_row' + row] : cellNode.attr('data-ox-cell-data');
                    if (oxCellData) {
                        oxCellData = isMergedCell ? oxCellData : JSON.parse(oxCellData);

                        if (oxCellData.formula) {
                            changeSet.f = oxCellData.formula;
                            if (oxCellData.mr) {
                                changeSet.mr = Range.parse(oxCellData.mr);
                            }
                        }

                        changeSet.v = oxCellData.error ? formulaGrammar.getErrorCode(oxCellData.error) : oxCellData.value;

                        // styles
                        if (sameDoc) {
                            changeSet.s = oxCellData.styleId;
                            changeSet.a = null;

                        } else if (intern) {
                            changeSet.a = oxCellData.explicit;
                            changeSet.s = '';

                        } else {
                            cellAttrData = json.deepClone(tableAttrData);
                            changeSet.a = cellAttrData.attrs ? cellAttrData.attrs : {};
                            attrPool.extendAttrSet(changeSet.a, oxCellData.attributes);
                            changeSet.s = ''; //set to empty string to overwrite old styleId with <Default>-StyleId
                        }

                        // if this is the referece cell of a merged range, get all defined cells of the merged range
                        //  (collected in "copyHandler")
                        if (oxCellData.mergedRange) {
                            Object.assign(foundMergedRanges, oxCellData.mergedRangeCells);
                            // if the colCount is less than the colspan, use the colspan to iterate (to get all covered cells)
                            //  otherwise use the normal colCount
                            colCount = (colCount > colspan) ? colCount : colspan;
                        }

                        // create cell notes
                        if (is.dict(oxCellData.comment) && is.dict(oxCellData.comment.attrs)) {
                            var settings = NoteModelConfig.parseJSON(address, oxCellData.comment.settings);
                            var frameRect = Rectangle.parseJSON(oxCellData.comment.rect);
                            if (frameRect.area() > 0) {
                                notes.push({ settings, attrs: oxCellData.comment.attrs, rect: frameRect });
                            }
                        }

                        // // create cell threaded comments
                        if (docApp.isOOXML() && is.array(oxCellData.threadedComments)) {
                            comments.push({ anchor: address, comments: oxCellData.threadedComments });
                        }
                    } else {

                        if (!rowAttrData) {
                            var rowParent = rowNode.parents('thead, tbody, tfoot, table').first();
                            rowAttrData = extractElementAttributes(json.deepClone(tableAttrData), rowParent);
                            rowAttrData = extractElementAttributes(rowAttrData, rowNode);
                        }

                        // merge in the character and cell attributes of the HTML row and cell
                        cellAttrData = extractElementAttributes(json.deepClone(rowAttrData), cellNode);
                    }

                    // merge in the hyperlink's character attributes that are attached to it's child span
                    linkChild = findMatchingChild(cellNode, 'a').children('span').first();
                    if (cellAttrData && (linkChild.length > 0)) {
                        attrPool.extendAttrSet(cellAttrData.attrs, { character: getCharAttributes(linkChild) });
                    }

                    if (!isMergedCell) { cellId++; }
                }

                // add the cell information to the cell contents object
                if (cellAttrData) {
                    if (cellAttrData.attrs) {
                        changeSet.a = cellAttrData.attrs;
                        lastAttributes = cellAttrData.attrs; // save "last" cell-attributes for following merged cells
                    }
                    if ('format' in cellAttrData) { changeSet.format = cellAttrData.format; }
                    if ('lcid' in cellAttrData) { changeSet.lcid = cellAttrData.lcid; }
                }

                // set styles for merged cells
                if (isMergedCell && lastAttributes) { changeSet.a = lastAttributes; }

                changeSets.push(changeSet);
            }
        });

        // parse conditional formatting data from JSON
        const cfItemsJson = intern && json.safeParse(table.attr('data-ox-cond-formats'));
        const cfPasteItems = cfItemsJson ? docView.cfRuleCollection.parsePasteItems(cfItemsJson) : [];

        return {
            changeSets,
            comments: notes,
            threadedComments: comments,
            mergedRanges,
            cfPasteItems,
            intern,
            sameDoc,
            sameSheet,
            cut,
            sourceRange
        };
    }

    /**
     * Generates the content objects needed to fill selected ranges with
     * clipboard contents repeatedly.
     *
     * @returns {ClipboardPasteItem[]}
     *  All calculated single ranges with contents object,
     *  merged ranges and some more for each pastable ranges. Furthermore it
     *  contains the conditional formats, if at least one exists.
     */
    function createPasteItems(sheetModel, parserResult, selection) {

        var sourceRange = parserResult.sourceRange;
        var refAddress = sourceRange?.a1 ?? selection.address;
        var targetRanges = selection.ranges;
        var pasteItems = [];

        // creates change sets with relocated addresses and formulas
        function createPasteItem(refAddress, targetAddress) {

            var intern = parserResult.intern;
            var cut = parserResult.cut;
            var tokenArray  = docView.sheetModel.createCellTokenArray();

            var changeSets = parserResult.changeSets.map(function (changeSet) {

                var colDiff = targetAddress.c - refAddress.c;
                var rowDiff = targetAddress.r - refAddress.r;
                var address = new Address(targetAddress.c + changeSet.address.c, targetAddress.r + changeSet.address.r);

                if (intern) {
                    changeSet = { ...changeSet, address };

                    if (changeSet.f) {
                        tokenArray.parseFormula("op:oox", changeSet.f, refAddress);
                        changeSet.f = cut ? tokenArray.getFormulaOp() : tokenArray.getFormula("op", refAddress, targetAddress);
                        if (changeSet.mr) {
                            changeSet.mr = changeSet.mr.clone().move(colDiff, rowDiff);
                        }
                    }

                    if (changeSet.format === null) { delete changeSet.format; }
                    return changeSet;
                }

                // if the value is a string, try to parse a format
                var parsed = is.string(changeSet.v) ? numberFormatter.parseScalarValue(changeSet.v, { blank: true }) : { value: changeSet.v };

                changeSet = {
                    address,
                    v: parsed.value,
                    f: null,
                    a: changeSet.a,
                    s: null
                };

                if (parsed.format) {
                    changeSet.format = parsed.format;
                }
                return changeSet;
            });

            // calculate new result directly, before pasting (prevents flickering).
            if ((changeSets.length === 1) && changeSets[0].f && !changeSets[0].mr) {
                var address = changeSets[0].address;
                tokenArray.parseFormula("op", changeSets[0].f, address);
                var result = tokenArray.interpretFormula('val', address, address);
                if (result.type === 'valid') { changeSets[0].v = result.value; }
            }

            return { address: targetAddress, changeSets };
        }

        // we have no sourceRange, so the copy comes from extern
        //   or
        // sourceRange does not fit in the targetRange
        if (!sourceRange || !targetRanges.every(targetRange => sourceRange.sizeFitsInto(targetRange))) {
            pasteItems.push(createPasteItem(refAddress, targetRanges[0].a1));
            return pasteItems;
        }

        // DOCS-4185: skip hidden columns/rows if source range is single-sized in respective direction
        // (individual decision for columns/rows as in MSXL, everything else would be too complicated)
        const sourceCols = sourceRange?.cols() ?? 1;
        const sourceRows = sourceRange?.rows() ?? 1;
        const { colCollection, rowCollection } = sheetModel;
        const skipHidden = sheetModel.isFiltered();
        const skipHiddenCols = skipHidden && (sourceCols === 1);
        const skipHiddenRows = skipHidden && (sourceRows === 1);

        // fill each target range with the source data
        for (const targetRange of targetRanges) {
            const rowIndexes = skipHiddenRows ? rowCollection.indexes(targetRange.rowInterval(), { visible: true }) : itr.sequence(targetRange.a1.r, targetRange.a2.r + 1, sourceRows);
            for (const row of rowIndexes) {
                const colIndexes = skipHiddenCols ? colCollection.indexes(targetRange.colInterval(), { visible: true }) : itr.sequence(targetRange.a1.c, targetRange.a2.c + 1, sourceCols);
                for (const col of colIndexes) {
                    pasteItems.push(createPasteItem(refAddress, new Address(col, row)));
                }
            }
        }

        return pasteItems;
    }

    /**
     * Handles "paste" events triggered by the clipboard node.
     */
    var pasteHandler = modelLogger.profileMethod('$badge{ClipboardMixin} pasteHandler', function (pasteEvent) {

        // model of the active sheet
        const { sheetModel, cellCollection, mergeCollection, cfRuleCollection, drawingCollection, noteCollection, commentCollection } = docView;
        // current selection in the active sheet
        var selection = selectionEngine.getSelection();
        // the clipboard event data
        var clipboardData = pasteEvent && pasteEvent.originalEvent && pasteEvent.originalEvent.clipboardData;
        // the cleaned up HTML data
        var htmlData;
        // the HTML data attached to the event
        var htmlRawData;
        // the plain text data attached to the event
        var textData;

        function handleParserResult(parserResult) {

            if (!parserResult.changeSets) {
                modelLogger.warn('$badge{ClipboardMixin} pasteHandler: no content');
                return gridPane.createResolvedPromise();
            }

            var promise = null;
            // cell contents for all target paste ranges
            var pasteItems = null;
            // number of all cells
            var cellCount = 0;
            // all ranges that will be overwritten
            var pasteRanges = new RangeArray();
            // the top-left addresses of all original paste ranges
            const pasteRefs = new AddressArray();
            // all cell notes to be inserted
            var notes = [];
            // all cell comments to be inserted
            var comments = [];

            var sourceRange = parserResult.sourceRange;

            // reject paste-promise when the clipboard-range at least does not fit in one of the target ranges
            if ((selection.ranges.length > 1) && sourceRange && !selection.ranges.every(sourceRange.sizeFitsInto, sourceRange)) {
                promise = makeRejected('paste:ranges:unfit');

            } else {

                // the parsed result
                pasteItems = createPasteItems(sheetModel, parserResult, selection);

                // iterate over all paste ranges (early exit on first error)
                var errorCode = null;
                pasteItems.every(function (pasteItem) {

                    var moveCols = pasteItem.address.c;
                    var moveRows = pasteItem.address.r;

                    // the target paste range for the current item
                    var pasteRange = null;
                    pasteItem.changeSets.forEach(function (changeSet) {
                        pasteRange = pasteRange ? pasteRange.expand(changeSet.address) : new Range(changeSet.address);
                    });
                    pasteItem.range = pasteRange;

                    pasteRefs.push(pasteItem.address);

                    // check and store the paste range
                    if (pasteRange) {

                        // early exit if the total number of cells exceeds the configured limit
                        cellCount += pasteRange.cells();
                        if (cellCount > MAX_FILL_CELL_COUNT) {
                            errorCode = 'paste:overflow';
                            return false;
                        }

                        // check that the target range is inside the sheet
                        if (!addressFactory.isValidAddress(pasteRange.a2)) {
                            errorCode = 'paste:outside';
                            return false;
                        }

                        // check for locked cells in the target range (early exit the loop if an error occurs)
                        errorCode = docView.ensureUnlockedRanges(pasteRange, { lockTables: 'header', lockMatrixes: 'partial', errorCode: 'paste:locked', sync: true });
                        if (errorCode) { return false; }

                        pasteRanges.push(pasteRange);
                    }

                    // set merged ranges for all paste ranges
                    if (parserResult.mergedRanges) {
                        pasteItem.mergedRanges = RangeArray.map(parserResult.mergedRanges, function (range) {
                            return addressFactory.getCroppedMovedRange(range, moveCols, moveRows);
                        });
                    }

                    // clone and move the cell notes
                    if (parserResult.comments) {
                        parserResult.comments.forEach(function (note) {
                            var settings = note.settings.clone();
                            settings.anchor.c += moveCols;
                            settings.anchor.r += moveRows;
                            if (addressFactory.isValidAddress(settings.anchor)) {
                                var anchorRect = sheetModel.getAnchorRectangle(settings.anchor);
                                var frameRect = note.rect.clone().translateSelf(anchorRect.right(), anchorRect.top);
                                var anchorAttrs = drawingCollection.getAnchorAttributesForRect(AnchorMode.ABSOLUTE, frameRect, { pixel: true });
                                var attrSet = { ...note.attrs };
                                attrSet.drawing = { ...attrSet.drawing, ...anchorAttrs };
                                notes.push({ settings, attrs: attrSet });
                            }
                        });
                    }

                    if (parserResult.threadedComments) {
                        parserResult.threadedComments.forEach(function (thread) {
                            var anchor = thread.anchor.clone();
                            anchor.c += moveCols;
                            anchor.r += moveRows;
                            if (addressFactory.isValidAddress(anchor)) {
                                thread.comments.forEach(function (settings) {
                                    comments.push({ anchor, settings });
                                });
                            }
                        });
                    }

                    return true;
                });

                promise = errorCode ? makeRejected(errorCode) : gridPane.createResolvedPromise(null);
            }

            // paste the contents contained in the passed result object
            promise = promise.then(function () {
                return sheetModel.createAndApplyOperations(function (generator) {

                    pasteRanges = pasteRanges.merge();
                    const activeSheet = docModel.getActiveSheet();

                    // collect all cell ranges to be preprocessed in some way
                    const unmergeRanges = new RangeArray();
                    const mergeRanges = new RangeArray();
                    let changeSets = [];
                    for (const pasteItem of pasteItems) {
                        const pasteRange = pasteItem.range;
                        // collect all ranges to be unmerged first
                        if (pasteRange && !pasteRange.single() && mergeCollection.coversAnyMergedRange(pasteRange)) {
                            unmergeRanges.push(pasteRange);
                        }
                        // collect all ranges to be merged
                        if (pasteItem.mergedRanges) {
                            mergeRanges.append(pasteItem.mergedRanges);
                        }
                        // collect all cell changesets
                        changeSets = changeSets.concat(pasteItem.changeSets);
                    }

                    var promise2 = gridPane.createResolvedPromise();

                    // unmerge cells if necessary
                    if (unmergeRanges.length) {
                        promise2 = promise2.then(function () {
                            return mergeCollection.generateMergeCellsOperations(generator, unmergeRanges, MergeMode.UNMERGE);
                        });
                    }

                    // merge cells
                    if (mergeRanges.length) {
                        promise2 = promise2.then(function () {
                            return mergeCollection.generateMergeCellsOperations(generator, mergeRanges, MergeMode.MERGE);
                        });
                    }

                    // set cell contents and hyperlinks
                    if (changeSets.length) {
                        promise2 = promise2.then(function () {
                            return cellCollection.generateCellsOperations(generator, changeSets);
                        });
                    }

                    // recalculate references, when paste on
                    // - the same document
                    // - the same sheets (different sheets are dull)
                    // - with contents from "cut"-event
                    promise2 = promise2.then(() => gridPane.iterateArraySliced(pasteItems, pasteItem => {
                        if (pasteItem.range && parserResult.cut && parserResult.sameSheet && sourceRange) {
                            return cellCollection.generateCutPasteOperations(generator, activeSheet, sourceRange, pasteItem.range);
                        }
                    }));

                    // recalculate the optimal row height
                    promise2 = promise2.then(function () {
                        return docView.generateUpdateRowHeightOperations(generator, pasteRanges);
                    });

                    // conditional formatting
                    if (parserResult.intern) {
                        promise2 = promise2.then(function () {
                            return cfRuleCollection.generatePasteOperations(generator, pasteRanges, pasteRefs, parserResult.cfPasteItems, parserResult.sameDoc, parserResult.sameSheet);
                        });
                    }

                    // temporary undo generator to be able to restore contents after deletion
                    var generator2 = sheetModel.createOperationGenerator({ applyImmediately: true });

                    // delete all existing cell notes in the target ranges
                    promise2 = promise2.then(function () {
                        return noteCollection.generateDeleteAllNotesOperations(generator2, pasteRanges);
                    });

                    // delete all existing cell comments in the target ranges delete must be executed before insert notes or comments
                    promise2 = promise2.then(function () {
                        return commentCollection.generateDeleteAllCommentsOperations(generator2, pasteRanges);
                    });

                    // append note and comment operations to parent generator
                    promise2 = promise2.then(function () {
                        generator.appendOperations(generator2);
                    });

                    // generate all cell notes
                    if (notes.length > 0) {
                        promise2 = promise2.then(function () {
                            notes.forEach(function (note) {
                                noteCollection.generateInsertNoteOperations(generator, note.settings, note.attrs, note.rect);
                            });
                        });
                    }

                    if (comments.length > 0) {
                        promise2 = promise2.then(function () {
                            comments.forEach(function (comment) {
                                commentCollection.generateInsertCommentOperation(generator, comment.anchor, comment.settings.text, comment.settings.author, comment.settings.authorId, comment.settings.authorProvider, comment.settings.date, comment.settings.done, true, comment.settings.mentions);
                            });
                        });
                    }

                    // Undo: restore old comments after deleting the pasted comments
                    promise2 = promise2.then(function () {
                        generator.appendOperations(generator2, { undo: true });
                    });

                    return promise2;

                }, { applyImmediately: true, storeSelection: true });
            });

            promise = promise.then(function () {
                var lastItem = _.last(pasteItems);
                if (lastItem && selectionEngine.isSingleCellSelection() && sourceRange) {
                    var newRange = Range.fromAddressAndSize(lastItem.address, sourceRange.cols(), sourceRange.rows());
                    selectionEngine.selectRange(newRange.intersect(addressFactory.getSheetRange()), { active: selectionEngine.getActiveCell() });
                }
            });

            // show alert messages on error
            return docView.yellOnFailure(promise);
        }

        function pasteHtmlClipboard(html) {
            modelLogger.log('$badge{ClipboardMixin} pasteHandler: pasting HTML clipboard contents');
            return handleParserResult(parseHTMLData(html));
        }

        function pasteTextClipboard(text) {
            // set clipboard debug pane content
            docModel.trigger('debug:clipboard', text);
            // parse text
            modelLogger.log('$badge{ClipboardMixin} pasteHandler: pasting plain-text clipboard contents');
            return handleParserResult(parseTextData(text));
        }

        // check if we have edit rights before pasting
        if (!docView.requireEditMode()) {
            pasteEvent.preventDefault();
            return false;
        }

        htmlRawData = clipboardData && clipboardData.getData('text/html');
        htmlData = is.string(htmlRawData) ? parseAndSanitizeHTML(htmlRawData) : $();

        var drawingContainerNode = getDrawingContainerNode(htmlData);
        var jqTableElements = getContainedHtmlElements(htmlData, 'table');
        var jqImgElements = !jqTableElements ? getContainedHtmlElements(htmlData, 'img') : null;

        if (drawingContainerNode || jqTableElements || jqImgElements) {
            // check if clipboard event data contains HTML

            // prevent default paste handling for desktop browsers, but not for touch devices
            if (!TOUCH_DEVICE) { pasteEvent.preventDefault(); }

            // set clipboard debug pane content
            docModel.trigger('debug:clipboard', htmlRawData);
            // render the HTML to apply CSS styles provided by Excel or Calc
            insertClipboardContents(htmlData);

            // enable busy blocker screen
            docView.enterBusy();

            // first, try to paste drawing objects that have been copied by ourselves
            var promise = null;
            if (drawingContainerNode) {
                promise = pasteDrawingClipboard(drawingContainerNode, getApplicationGuid(htmlData));
            } else if (jqImgElements) {
                promise = pasteImageClipboard(htmlData, jqImgElements);
            } else if (selectionEngine.hasSingleDrawingSelection()) {
                // if exactly one drawing is selected paste into this drawing
                promise = docModel.parseHTMLClipboardAndCreateOperations(clipboardNode);
            } else if (selectionEngine.hasDrawingSelection()) {
                // if more than one drawing is selected do nothing
                promise = $.when();
            } else {
                promise = pasteHtmlClipboard(clipboardNode);
            }

            // leave busy blocker screen
            gridPane.onSettled(promise, () => {
                docView.leaveBusy();
                docView.grabFocus();
            });

            // clear clipboard
            clipboardNode.text('\xa0');
            return false;
        }

        // focus and select the clipboard node
        clipboardNode.focus();

        // check if clipboard event data contains plain text
        // to be used in case the clipboard node contains no HTML table to parse
        textData = clipboardData?.getData('text/plain');

        // read and parse pasted data
        gridPane.setTimeout(function () {
            docModel.trigger('debug:clipboard', clipboardNode);

            drawingContainerNode = getDrawingContainerNode(clipboardNode);
            jqTableElements = getContainedHtmlElements(clipboardNode, 'table');
            jqImgElements = !jqTableElements ? getContainedHtmlElements(clipboardNode, 'img') : null;

            // enable busy blocker screen
            docView.enterBusy();

            // first, try to paste drawing objects that have been copied by ourselves
            var promise = null;
            if (drawingContainerNode) {
                promise = pasteDrawingClipboard(drawingContainerNode, getApplicationGuid(clipboardNode));
            } else if (jqImgElements) {
                promise = pasteImageClipboard(clipboardNode, jqImgElements);
            } else if (selectionEngine.hasSingleDrawingSelection()) {
                // if exactly one drawing is selected paste into this drawing
                promise = docModel.parseHTMLClipboardAndCreateOperations(clipboardNode);
            } else if (selectionEngine.hasDrawingSelection()) {
                // if more than one drawing is selected do nothing
                promise = $.when();
            } else if (jqTableElements) {
                promise = pasteHtmlClipboard(clipboardNode);
            } else if (textData) {
                promise = pasteTextClipboard(textData);
            } else {
                promise = gridPane.createResolvedPromise();
            }

            // leave busy blocker screen
            gridPane.onSettled(promise, () => {
                docView.leaveBusy();
                docView.grabFocus();
            });

            // clear clipboard
            clipboardNode.text('\xa0');
        }, 0);
    });

    /**
     * Handles "drop" events triggered by the root node of the grid pane.
     */
    function dropHandler(dropEvent) {

        // saving dataTransfer values from event for asynchronous handling (58601)
        // -> the FileList object from the event
        var eventFiles = dropEvent.originalEvent.dataTransfer.files;
        // the url data from the original event
        var urlData = dropEvent.originalEvent.dataTransfer.getData('Url');

        // nothing to do in locked sheets
        // TODO: the error message needs to be adjusted when dropping cell data will be supported
        // accessing the drop event data must be synchronous, otherwise the data is lost
        var promise = docView.ensureUnlockedDrawings({ errorCode: 'drawing:insert:locked' }).then(function () {

            // promises for the image descriptors (URL and name)
            var imagePromises = [];
            // whether any of the dropped data is not an image
            var unsupported = false;

            if (eventFiles.length > 0) {
                // data originates from local file system
                _.each(eventFiles, function (eventFile) {
                    if (hasFileImageExtension(eventFile.name)) {
                        imagePromises.push(insertFile(docApp, eventFile));
                    } else {
                        unsupported = true;
                    }
                });
            } else if (urlData.length > 0) {
                // data originates from another browser window
                if (hasUrlImageExtension(urlData)) {
                    imagePromises.push(insertURL(docApp, urlData));
                } else {
                    unsupported = true;
                }
            }

            // show a warning if nothing of the dropped data may be an image;
            // nothing more to do without pending images
            if (imagePromises.length === 0) {
                return unsupported ? makeRejected('drop:unsupported') : null;
            }

            // fail with empty resolved promise to be able to use $.when() which would
            // otherwise abort immediately if ANY of the promises fails
            imagePromises = imagePromises.map(imagePromise => imagePromise.catch(fun.undef));

            // immediately go into busy mode, wait for all image descriptors, insert the images afterwards
            docView.enterBusy();
            var promise = $.when(...imagePromises).then(function (...imageDescs) {
                // $.when() passes the results as function arguments, filter out invalid images
                return docApp.docController.execInsertImages(imageDescs.filter(Boolean));
            });

            gridPane.onSettled(promise, () => docView.leaveBusy());
            return promise;
        });

        // show warning alert if necessary (this is an event handler, do not return the promise)
        docView.yellOnFailure(promise);
    }

    // initialization -----------------------------------------------------

    // DOCS-2331: insert clipboard node into F6 navigation chain
    clipboardNode.addClass('f6-target').attr('tabindex', 0);

    // on touch devices, disable the content-editable mode of the clipboard node,
    // to prevent showing the virtual keyboard all the time
    if (TOUCH_DEVICE) { clipboardNode.removeAttr('contenteditable'); }

    // intercept DOM focus requests at the clipboard node (select clipboard contents)
    clipboardNode[0].focus = _.wrap(clipboardNode[0].focus, function (nativeFocusMethod) {

        // bug 29948: prevent grabbing to clipboard node while in text edit mode
        if (docView.isTextEditMode()) {
            modelLogger.warn('$badge{ClipboardMixin} focusHandler: clipboard will not be focused in text edit mode');
            return;
        }

        // first, move the browser focus to the clipboard node
        nativeFocusMethod.call(this);

        // On touch devices, selecting text nodes in a content-editable node will always show
        // the virtual keyboard. TODO: need to find a way to provide clipboard support.
        if (TOUCH_DEVICE) { return; }

        // safely clear the old browser selection
        clearBrowserSelection();

        // set the browser selection
        try {
            var docRange = window.document.createRange();
            docRange.setStart(clipboardNode[0], 0);
            docRange.setEnd(clipboardNode[0], clipboardNode[0].childNodes.length);
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(docRange);
        } catch (err) {
            modelLogger.exception(err, '$badge{ClipboardMixin} focusHandler: failed to select clipboard node');
        }
    });

    // handle system clipboard events
    clipboardNode.on('copy', copyHandler);
    clipboardNode.on('cut', cutHandler);
    clipboardNode.on('paste', pasteHandler);

    // handle system drag&drop events
    this.$el.on('drop', dropHandler);

    // hide clipboard node during text edit mode to make it unavailable for code looking up focusable nodes
    this.listenTo(docView, 'textedit:enter', function () { clipboardNode.hide(); });
    this.listenTo(docView, 'textedit:leave', function () { clipboardNode.show(); });

    // destructor
    this.registerDestructor(function () {
        docApp.destroyImageNodes(clipboardNode);
    });
}
