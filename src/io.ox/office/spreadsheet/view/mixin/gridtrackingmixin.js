/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is, math, coord } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE, containsNode, parseIntAttribute, convertHmmToLength } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { hasSwappedDimensions, getCssTransform } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import * as DrawingFrame from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getOxoPositionFromPixelPosition } from '@/io.ox/office/textframework/utils/position';

import { Address } from '@/io.ox/office/spreadsheet/utils/address';
import { Range } from '@/io.ox/office/spreadsheet/utils/range';
import { MAX_FILL_CELL_COUNT, MAX_AUTOFILL_COL_ROW_COUNT, Direction } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { SelectionMode, getSelectionMode } from '@/io.ox/office/spreadsheet/utils/paneutils';
import { getTextFrame } from '@/io.ox/office/spreadsheet/model/drawing/text/textframeutils';

// for side effects (activates `$.enableTouch()`)
import '$/io.ox/core/tk/nodetouch';

// constants ==================================================================

// mathematical constants
const PI_180 = Math.PI / 180;

// private functions ==========================================================

/**
 * Returns whether the passed tracking notification record will start text edit
 * mode for a text frame drawing.
 *
 * @param {TrackingRecord} record
 *  The tracking notification record to be evaluated.
 *
 * @param {JQuery} textFrame
 *  The text frame element of a drawing object.
 *
 * @returns {boolean}
 *  Whether to start text edit mode in the passed text frame.
 */
function isTextEdit(record, textFrame) {
    // bug 52956: start text edit mode only when a descendant node (paragraph, span) has been clicked (otherwise: move shape)
    // DOCS-1564: on touch devices, start text edit mode after a double-tap
    return TOUCH_DEVICE ? record.dblClick : containsNode(textFrame, record.target);
}

// mix-in class GridTrackingMixin =============================================

/**
 * Internal mix-in class for the class `GridPane` that implements all kinds of
 * mouse and touch tracking, for example selection, or manipulation of drawing
 * objects.
 */
export default function GridTrackingMixin() {

    // self reference (the `GridPane` instance)
    var gridPane = this;

    // the spreadsheet model and view
    var docView = this.docView;
    const { selectionEngine } = docView;
    var docModel = docView.docModel;

    // the root node of all rendering layers
    var layerRootNode = $(this.layerRootNode);

    // special behavior in OOXML documents
    var ooxml = docModel.docApp.isOOXML();

    // number of clicks on same cell (needed for double-click detection)
    var sameAddressClicks = 0;

    // private methods --------------------------------------------------------

    /**
     * Returns the address of the cell covering the page coordinates in the
     * passed tracking event.
     *
     * @param {TrackingRecord} record
     *  The tracking notification record.
     *
     * @param {Object} [options]
     *  Optional parameters that will be forwarded to the method
     *  ColRowCollection.getEntryByOffset().
     *
     * @returns {Address}
     *  The address of the cell containing the tracking position.
     */
    function getCellAddress(record, options) {
        return gridPane.getCellDataForPagePoint(record.point, options).address;
    }

    /**
     * Returns the address of the anchor cell for a tracking cell range,
     * according to the passed tracking position.
     *
     * @param {TrackingRecord} record
     *  The "start" tracking notification record.
     *
     * @param {Range} origRange
     *  The address of the cell range to be tracked.
     *
     * @param {OuterBorderKey|BorderCornerKey} trackerPos
     *  The position identifier of the tracker node.
     *
     * @param {Boolean} resize
     *  Whether resize tracking (true), or border tracking (false) is
     *  active. For border tracking, the tracker position passed in the
     *  parameter 'trackerPos' must be 't', 'b', 'l', or 'r'.
     *
     * @returns {Address}
     *  The address of the anchor cell in the passed range.
     */
    function getAnchorCell(record, origRange, trackerPos, resize) {
        switch (trackerPos) {
            case 'l':
                return resize ? origRange.a2.clone() : new Address(origRange.a1.c, getCellAddress(record).r);
            case 'r':
                return resize ? origRange.a1.clone() : new Address(origRange.a2.c, getCellAddress(record).r);
            case 't':
                return resize ? origRange.a2.clone() : new Address(getCellAddress(record).c, origRange.a1.r);
            case 'b':
                return resize ? origRange.a1.clone() : new Address(getCellAddress(record).c, origRange.a2.r);
            case 'tl':
                return origRange.a2.clone();
            case 'tr':
                return new Address(origRange.a1.c, origRange.a2.r);
            case 'bl':
                return new Address(origRange.a2.c, origRange.a1.r);
            case 'br':
                return origRange.a1.clone();
        }
    }

    /**
     * Recalculates the address of the tracked range according to the
     * passed tracking position.
     *
     * @param {TrackingRecord} record
     *  The tracking notification record.
     *
     * @param {Range} origRange
     *  The address of the original cell range currently tracked.
     *
     * @param {Address} anchorCell
     *  The anchor cell inside the passed original range, as has been
     *  returned by the method GridTrackingMixin.getAnchorCell().
     *
     * @param {String} trackerPos
     *  The position identifier of the tracker node.
     *
     * @param {Boolean} resize
     *  Whether resize tracking (true), or border tracking (false) is
     *  active. For border tracking, the tracker position passed in the
     *  parameter 'trackerPos' must be 't', 'b', 'l', or 'r'.
     *
     * @returns {Range}
     *  The address of the cell range currently tracked.
     */
    function recalcTrackedRange(record, origRange, anchorCell, trackerPos, resize) {

        // current column/row entries
        var trackingData = gridPane.getCellDataForPagePoint(record.point, { outerHidden: true });
        // the cell address currently tracked
        var address = trackingData.address;
        // the resulting range
        var range = null;

        // adjusts column/row index according to the offset inside the covered cell
        function adjustTrackedBorder(columns) {

            // the maximum column/row index
            var maxIndex = docModel.addressFactory.getMaxIndex(columns);
            // the column/row descriptor
            var entryData = columns ? trackingData.colDesc : trackingData.rowDesc;
            // whether the leading border is tracked
            var leadingBorder = resize ? (address.get(columns) < anchorCell.get(columns)) : (trackerPos === (columns ? 'l' : 't'));
            // whether the trailing border is tracked
            var trailingBorder = resize ? (address.get(columns) > anchorCell.get(columns)) : (trackerPos === (columns ? 'r' : 'b'));
            // the offset ratio in the tracked column/row
            var ratio = (entryData.size > 0) ? (entryData.relOffset / entryData.size) : Number.NaN;

            if (leadingBorder && (address.get(columns) < maxIndex) && (ratio > 0.5)) {
                // jump to next column/row if tracking leading border, and covering second half of the tracked column/row ...
                address.move(1, columns);
            } else if (trailingBorder && (address.get(columns) > 0) && (ratio < 0.5)) {
                // ... or to previous column/row if tracking trailing border, and covering first half of the tracked column/row
                address.move(-1, columns);
            }
        }

        // adjust column/row index according to the offset inside the tracked cell
        adjustTrackedBorder(false);
        adjustTrackedBorder(true);

        // build and return the new range according to the current position of the tracked cell
        if (resize) {
            // resize mode: build range between anchor cell and tracked cell
            range = Range.fromAddresses(anchorCell, address);
            if ((trackerPos === 't') || (trackerPos === 'b')) {
                // resize vertically: restore row indexes to original range
                range.a1.c = origRange.a1.c;
                range.a2.c = origRange.a2.c;
            } else if ((trackerPos === 'l') || (trackerPos === 'r')) {
                // resize horizontally: restore row indexes to original range
                range.a1.r = origRange.a1.r;
                range.a2.r = origRange.a2.r;
            }
        } else {
            // border mode: move entire range according to distance between tracked cell and anchor cell
            range = docModel.addressFactory.getBoundedMovedRange(origRange, address.c - anchorCell.c, address.r - anchorCell.r);
        }

        return range;
    }

    // cell selection (mouse) -------------------------------------------------

    /**
     * Creates the set of callbacks to handle all tracking notifications for
     * cell selection originating from mouse events.
     */
    function createSelectMouseCallbacks() {

        // current address (prevent updates while tracking over the same cell)
        let currentAddress = null;
        // address of last triggered "select:end" event
        let lastEndAddress = null;

        function prepareTracking() {
            return gridPane.getLastTrackingId() !== "select-touch";
        }

        function startTracking(record) {
            currentAddress = getCellAddress(record);
            gridPane.trigger('select:start', currentAddress, getSelectionMode(record.modifiers));
            gridPane.scrollToCell(currentAddress);
        }

        function moveTracking(record) {
            // update the current address according to the passed tracking event
            const address = getCellAddress(record, { outerHidden: true });
            if (address.differs(currentAddress)) {
                currentAddress = address;
                gridPane.trigger("select:move", currentAddress);
            }
        }

        function scrollTracking(record) {
            gridPane.scrollRelative(record.scroll.x, record.scroll.y);
            moveTracking(record);
        }

        function finalizeTracking(record) {
            // count number of clicks on the same cell (double-click detection)
            if (lastEndAddress?.equals(currentAddress)) {
                sameAddressClicks += 1;
            } else {
                sameAddressClicks = 1;
            }
            lastEndAddress = currentAddress;
            gridPane.trigger('select:end', currentAddress, record.type === "end");
            gridPane.scrollToCell(currentAddress);
            // double click detected: start cell edit mode
            if (record.dblClick) {
                // ignore notification if different addresses have been clicked
                if ((sameAddressClicks >= 2) && (gridPane.getLastTrackingId() === 'select-mouse')) {
                    gridPane.trigger('select:dblclick');
                }
                sameAddressClicks = 0;
            }
        }

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking,
            finally: finalizeTracking
        };
    }

    // cell selection (touch) -------------------------------------------------

    /**
     * Handles all tracking notifications for cell selection originating from
     * touch events.
     */
    function createSelectTouchCallbacks() {

        // cell address where tracking has started
        let startAddress = null;

        function prepareTracking(event) {
            return event.type === "touchstart"; // TODO: allow to specify via registration option
        }

        function startTracking(record) {
            startAddress = getCellAddress(record);
        }

        function endTracking(record) {
            if (record.moved) { return; }

            // current selection
            var selection = selectionEngine.getSelection();
            // array index of the range that contains a tapped cell
            var rangeIndex = 0;

            if (record.duration <= 750) {
                // short-tap: select a single cell, or start cell edit mode
                if (!selectionEngine.hasDrawingSelection() && selection.isActive(startAddress)) {
                    docView.enterTextEditMode('cell');

                } else if (!selectionEngine.hasDrawingSelection() && selection.containsAddress(startAddress)) {
                    rangeIndex = _.findIndex(selection.ranges, function (range) { return range.containsAddress(startAddress); });
                    gridPane.trigger('select:active', rangeIndex, startAddress);

                } else {
                    gridPane.trigger('select:start', startAddress, SelectionMode.SELECT);
                    gridPane.trigger('select:end', startAddress, true);
                    gridPane.scrollToCell(startAddress);
                }
            } else {
                if (!selection.isActive(startAddress) && selection.containsAddress(startAddress)) {
                    rangeIndex = _.findIndex(selection.ranges, function (range) { return range.containsAddress(startAddress); });
                    gridPane.trigger('select:active', rangeIndex, startAddress);

                } else if (!selection.containsAddress(startAddress)) {
                    gridPane.trigger('select:start', startAddress, SelectionMode.SELECT);
                    gridPane.trigger('select:end', startAddress, true);
                    gridPane.scrollToCell(startAddress);
                }

                $('html').trigger('contextmenu');
            }
        }

        return {
            prepare: prepareTracking,
            start: startTracking,
            end: endTracking
        };
    }

    // resize selection range (touch) -----------------------------------------

    /**
     * Creates the set of callbacks to handle tracking notifications to resize
     * selected ranges on touch devices.
     */
    function createSelectTouchResizeCallbacks() {

        // the position identifier of the tracked node
        var trackerPos = null;
        // the array index of the tracked range
        var rangeIndex = 0;
        // the original address of the tracked range
        var origRange = null;
        // the anchor cell in the original range
        var anchorCell = null;
        // the current address of the tracked range
        var currRange = null;

        function prepareTracking(event) {
            return $(event.target).closest(".selection-layer .select [data-pos]").length > 0;
        }

        function startTracking(record) {

            // the handle node currently tracked
            var trackerNode = $(record.target);
            // the root node of the range that will be tracked
            var rangeNode = trackerNode.closest('.range');

            // extract range address
            rangeIndex = parseIntAttribute(rangeNode[0], 'data-index', -1);
            origRange = selectionEngine.getSelectedRanges()[rangeIndex];

            // initialize additional data needed while tracking
            trackerPos = trackerNode.attr('data-pos');
            currRange = origRange.clone();
            anchorCell = getAnchorCell(record, origRange, trackerPos, true);
        }

        function moveTracking(record) {

            // the new range address
            var newRange = recalcTrackedRange(record, origRange, anchorCell, trackerPos, true);

            // update the tracked range in the respective token array, this will
            // trigger change events which will cause refreshing all grid panes
            if (currRange.differs(newRange)) {
                currRange = newRange;
                gridPane.trigger('select:range', rangeIndex, currRange);
            }
        }

        function scrollTracking(record) {
            gridPane.scrollRelative(record.scroll.x, record.scroll.y);
            moveTracking(record);
        }

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking
        };
    }

    // move/resize highlighted ranges -----------------------------------------

    /**
     * Creates the set of callbacks to handle all tracking notifications for
     * highlighted cell ranges.
     */
    function createHighlightCallbacks() {

        // the position identifier of the tracked node (border or corner handle)
        var trackerPos = null;
        // whether resize tracking is active
        var resize = false;
        // the array index of the tracked highlighted range
        var rangeIndex = null;
        // the original range address
        var origRange = null;
        // the anchor cell in the original range
        var anchorCell = null;
        // the current address of the tracked range
        var currRange = null;

        function prepareTracking(event) {
            return $(event.target).closest(".highlight-layer [data-pos]").length > 0;
        }

        function startTracking(record) {

            // the border/handle node currently tracked
            var trackerNode = $(record.target);
            // the root node of the highlighted range that will be tracked
            var rangeNode = trackerNode.closest('.range');

            // the array index and range address of the tracked range
            rangeIndex = parseIntAttribute(rangeNode[0], 'data-index', -1);
            origRange = docView.rangeHighlighter.getRange(rangeIndex);
            if (!origRange) {
                globalLogger.warn('$badge{GridTrackingMixin} initializeTracking: cannot find tracking range');
                record.cancel();
                return;
            }

            // initialize additional data needed while tracking
            trackerPos = trackerNode.attr('data-pos');
            resize = trackerNode.parent().hasClass('resizers');
            origRange = origRange.toRange(); // convert to simple range without sheet indexes
            currRange = origRange.clone();
            anchorCell = getAnchorCell(record, origRange, trackerPos, resize);

            // render the tracked range with special glow effect
            docView.rangeHighlighter.trackRange(rangeIndex);
        }

        function moveTracking(record) {

            // the new range address
            var newRange = recalcTrackedRange(record, origRange, anchorCell, trackerPos, resize);

            // update the tracked range in the respective token array, this will
            // trigger change events which will cause refreshing all grid panes
            if (currRange.differs(newRange)) {
                currRange = newRange;
                docView.rangeHighlighter.changeRange(rangeIndex, currRange);
            }
        }

        function scrollTracking(record) {
            gridPane.scrollRelative(record.scroll.x, record.scroll.y);
            moveTracking(record);
        }

        function cancelTracking() {
            if (origRange) {
                docView.rangeHighlighter.changeRange(rangeIndex, origRange);
            }
        }

        // finalizes the highlighted range
        function finalizeTracking() {
            docView.rangeHighlighter.trackRange(-1);
        }

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking,
            cancel: cancelTracking,
            finally: finalizeTracking
        };
    }

    // auto-fill --------------------------------------------------------------

    /**
     * Create the set of callbacks to handle all tracking notifications for the
     * auto-fill handle.
     */
    function createAutoFillCallbacks() {

        // shortcut to the model of the active sheet
        var sheetModel = null;
        // the sheet range without leading/trailing hidden columns/rows
        var visibleRange = null;
        // the initial selection range
        var origRange = null;
        // the sheet rectangle covered by the initial selection range
        var origRectangle = null;
        // start position of tracking (bottom-right corner of selection range)
        var startPoint = null;
        // whether to restrict tracking direction (column/row ranges)
        var fixedColumns = null;
        // resulting maximum number of columns that can be filled at the leading/trailing border
        var maxLeadingCols = 0;
        var maxTrailingCols = 0;
        // resulting maximum number of rows that can be filled at the leading/trailing border
        var maxLeadingRows = 0;
        var maxTrailingRows = 0;
        // whether the original range contains at least one merged range
        var hasMergedRange = false;

        function prepareTracking(event) {
            return $(event.target).closest(".selection-layer .autofill [data-pos]").length > 0;
        }

        function startTracking(record) {

            // cancel custom selection when starting auto-fill
            docView.selectionEngine.cancelCustomSelectionMode();

            // the model of the active sheet
            sheetModel = docView.sheetModel;

            // do not expand auto-fill range into leading/trailing hidden columns/rows
            visibleRange = sheetModel.shrinkRangeToVisible(docModel.addressFactory.getSheetRange());
            if (!visibleRange) { record.cancel(); return; }

            // store address and position of the original selection range
            origRange = selectionEngine.getSelectedRanges().first();
            origRectangle = docView.getRangeRectangle(origRange);
            startPoint = coord.point(origRectangle.right(), origRectangle.bottom());

            // special behavior for column/row ranges
            fixedColumns = docModel.addressFactory.isColRange(origRange) ? true : docModel.addressFactory.isRowRange(origRange) ? false : null;

            // restrict number of columns/rows according to selection mode
            var maxCols = 0, maxRows = 0;
            if (fixedColumns === true) {
                maxCols = MAX_AUTOFILL_COL_ROW_COUNT;
            } else if (fixedColumns === false) {
                maxRows = MAX_AUTOFILL_COL_ROW_COUNT;
            } else {
                maxCols = Math.floor(MAX_FILL_CELL_COUNT / origRange.rows());
                maxRows = Math.floor(MAX_FILL_CELL_COUNT / origRange.cols());
            }

            // calculate maximum number of columns/rows that can be filled at once
            maxLeadingCols = Math.min(maxCols, Math.max(0, origRange.a1.c - visibleRange.a1.c));
            maxTrailingCols = Math.min(maxCols, Math.max(0, visibleRange.a2.c - origRange.a2.c));
            maxLeadingRows = Math.min(maxRows, Math.max(0, origRange.a1.r - visibleRange.a1.r));
            maxTrailingRows = Math.min(maxRows, Math.max(0, visibleRange.a2.r - origRange.a2.r));

            // check whether the original range contains at least one merged range
            hasMergedRange = sheetModel.mergeCollection.coversAnyMergedRange(origRange);

            // render selected range with modified auto-fill style
            docView.propSet.set('autoFillData', { direction: Direction.DOWN, count: 0 });
        }

        function moveTracking(record) {

            // current tracking position
            var currPoint = gridPane.getSheetPointForPagePoint(record.point);
            // whether to delete parts of the selected range
            var deleteMode = origRectangle.containsPixel(currPoint);
            // the adjusted tracking position to determine the direction
            var x = (deleteMode || (currPoint.x >= startPoint.x)) ? currPoint.x : Math.min(currPoint.x + origRectangle.width, startPoint.x);
            var y = (deleteMode || (currPoint.y >= startPoint.y)) ? currPoint.y : Math.min(currPoint.y + origRectangle.height, startPoint.y);
            // whether to expand horizontally or vertically
            var columns = (fixedColumns === null) ? (Math.abs(y - startPoint.y) < Math.abs(x - startPoint.x)) : fixedColumns;
            // whether to expand the leading or trailing border
            var leading = columns ? (x < startPoint.x) : (y < startPoint.y);
            // the tracker position to calculate the target rectangle
            var trackerPos = columns ? (leading ? 'l' : 'r') : (leading ? 't' : 'b');
            // simulate tracking at a specific border of the selected range
            var range = recalcTrackedRange(record, origRange, leading ? origRange.a1 : origRange.a2, trackerPos, false);

            // calculate the auto-fill data from target range
            let direction, count;
            if (deleteMode) {
                // delete mode: always delete from trailing border
                direction = columns ? Direction.RIGHT : Direction.DOWN;
                count = range.getStart(columns) - origRange.getEnd(columns) - 1;
            } else if (leading) {
                // expand leading range border
                direction = columns ? Direction.LEFT : Direction.UP;
                count = Math.min(origRange.getStart(columns) - range.getStart(columns), columns ? maxLeadingCols : maxLeadingRows);
            } else {
                // expand trailing range border
                direction = columns ? Direction.RIGHT : Direction.DOWN;
                count = Math.min(range.getEnd(columns) - origRange.getEnd(columns), columns ? maxTrailingCols : maxTrailingRows);
            }

            // if the source range covers a merged range, expand only in multiples of the source range size
            if (!deleteMode && hasMergedRange) {
                var origSize = origRange.size(columns);
                var maxCount = leading ? origRange.getStart(columns) : (docModel.addressFactory.getMaxIndex(columns) - origRange.getEnd(columns));
                count = Math.min(math.ceilp(count, origSize), math.floorp(maxCount, origSize));
            }

            docView.propSet.set('autoFillData', { direction, count });

            // set correct mouse pointer according to direction
            const defaultCursor = (fixedColumns !== null) || (count === 0);
            record.cursor(defaultCursor ? null : columns ? 'ew-resize' : 'ns-resize');
        }

        function scrollTracking(record) {
            gridPane.scrollRelative(record.scroll.x, record.scroll.y);
            moveTracking(record);
        }

        function endTracking(record) {
            // apply auto-fill operation
            const itemValue = { ...docView.propSet.get('autoFillData') };
            itemValue.options = { copy: record.modifiers.altKey };
            // bug 39533: via controller for busy screen etc.
            docView.executeControllerItem('cell/autofill', itemValue);
        }

        function finalizeTracking() {
            // reset the view attribute
            docView.propSet.set('autoFillData', null);
            sheetModel = null;
        }

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking,
            end: endTracking,
            finally: finalizeTracking
        };
    }

    // select/move/resize drawings --------------------------------------------

    /**
     * Creates the set of callbacks to hndle all tracking notifications for
     * drawing frames.
     */
    function createDrawingCallbacks() {

        // the drawing frame of the clicked drawing object
        let activeFrame = null;
        // information for all selected drawing objects, mapped by model
        var drawingDescMap = new Map/*<SheetDrawingModelType, DrawingDescriptor>*/();
        // descriptor for the active (tracked) drawing object
        var activeDesc = null;
        // tracking start position (absolute position in the sheet, in pixels)
        var startPoint = null;
        // whether the mouse or touch point has really been moved (with threshold distance)
        var mouseMoved = false;
        // whether the drawing object will be moved (false for resize/rotate/adjust mode)
        var moveMode = false;
        // position of the tracked resizer handle (null for move/rotate/adjust mode)
        var resizerPos = null;
        // whether the rotate handle will be tracked (false for move/resize/adjust mode)
        var rotateMode = false;
        // whether horizontal/vertical tracking will be processed at all
        var activeX = false, activeY = false;
        // whether resizing in reversed direction (from trailing border) horizontally/vertically
        var reverseX = false, reverseY = false;
        // current move distance in horizontal/vertical direction
        var moveX = 0, moveY = 0;
        // temorary normalized move distance in horizontal/vertical direction used while moving
        var tempMoveX = 0, tempMoveY = 0;
        // minimum and maximum move distances in horizontal direction
        var minMoveX = 0, maxMoveX = 0;
        // minimum and maximum move distances in vertical direction
        var minMoveY = 0, maxMoveY = 0;
        // current resize factor in horizontal/vertical direction
        var scaleX = 1, scaleY = 1;
        // angle of the shape before rotation transformation
        var startAngle = 0;
        // difference between start angle, and angle after rotation applied
        var diffAngle = 0;
        // variables used for adjusting the shape via adj points
        var adjHandleNode, adjPos;
        var cxnObject, ahList, avList, gdList, snapRect;
        var adjMoveX = false, adjMoveY = false, isPolar = false;
        var xDiffpx, yDiffpx, minMaxRdiff;
        var minXValue, maxXValue, minYValue, maxYValue;
        var minXpos, maxXpos, minYpos, maxYpos, minAng, maxAng;
        var minR, maxR, minRPolar, maxRPolar, minRposPx, maxRposPx;
        var absMinMaxSumX, absMinMaxSumY, absMinMaxSumR;
        var actXpos, actYpos, currentPolar, currentAng, currentRadius;

        // returns the minimum scaling factor for resizing a specific drawing frame, according to location and sheet size
        function getMinScale(frameOffset, frameSize, sheetSize, reverse) {
            return reverse ? ((frameOffset - sheetSize) / frameSize + 1) : (-frameOffset / frameSize);
        }

        // returns the maximum scaling factor for resizing a specific drawing frame, according to location and sheet size
        function getMaxScale(frameOffset, frameSize, sheetSize, reverse) {
            return reverse ? (frameOffset / frameSize + 1) : ((sheetSize - frameOffset) / frameSize);
        }

        // returns the current location of the passed drawing object, according to the current tracking state
        function getDrawingRectangle(drawingDesc, record) {

            // new offset position and size of the drawing frame
            var rect = drawingDesc.rectangle.clone();
            // if function is triggered on tracking end event
            var isEndEvent = record.type === 'end';
            // flipping and rotation of the drawing model
            var rotation = drawingDesc.model.getRotationDeg();
            var flipH = drawingDesc.model.isFlippedH();
            var flipV = drawingDesc.model.isFlippedV();

            // adjust offset and size for move mode
            if (moveMode) {
                rect.left += isEndEvent ? moveX : tempMoveX;
                rect.top += isEndEvent ? moveY : tempMoveY;
            }

            // adjust offset and size for resize mode
            if (resizerPos) {

                // effective absolute scaling factors
                var maxAbsScaleX = activeX ? Math.abs((scaleX < 0) ? -drawingDesc.minScaleX : drawingDesc.maxScaleX) : 1;
                var maxAbsScaleY = activeY ? Math.abs((scaleY < 0) ? -drawingDesc.minScaleY : drawingDesc.maxScaleY) : 1;
                var absScaleX = activeX ? Math.min(maxAbsScaleX, Math.abs(scaleX)) : 1;
                var absScaleY = activeY ? Math.min(maxAbsScaleY, Math.abs(scaleY)) : 1;
                if (activeX && activeY && (record.modifiers.shiftKey || drawingDesc.model.isAspectLocked())) {
                    absScaleX = absScaleY = Math.min(maxAbsScaleX, maxAbsScaleY, Math.max(absScaleX, absScaleY));
                }

                // scale the drawing size
                var minSize = drawingDesc.model.getMinSize();
                rect.width = Math.max(minSize, Math.round(rect.width * absScaleX));
                rect.height = Math.max(minSize, Math.round(rect.height * absScaleY));

                // adjust offset (leading/trailing resizers)
                if (reverseX) { rect.left += drawingDesc.rectangle.width; }
                if (reverseX !== (scaleX < 0)) { rect.left -= rect.width; }
                if (reverseY) { rect.top += drawingDesc.rectangle.height; }
                if (reverseY !== (scaleY < 0)) { rect.top -= rect.height; }

                // resizing of rotated shape needs position correction
                if (isEndEvent && ((rotation !== 0) || flipH || flipV)) {
                    DrawingFrame.toggleTracking(drawingDesc.frameNode, true); // temporary enable visibility of tracking node to measure rect positions
                    var diff = DrawingFrame.getPositionDiffAfterResize(drawingDesc.frameNode, drawingDesc.trackerNode, rotation, flipH, flipV);
                    DrawingFrame.toggleTracking(drawingDesc.frameNode, false); // and disable back

                    rect.left = drawingDesc.rectangle.left - diff.leftDiff;
                    rect.top = drawingDesc.rectangle.top - diff.topDiff;
                }
            }

            return rect;
        }

        function prepareTracking(event) {
            activeFrame = gridPane.getDrawingFrameForEvent(event);
            return !!activeFrame;
        }

        // initializes the drawing according to the passed tracking notification
        function startTracking(record) {

            // blocking external operations
            docModel.setInternalOperationBlocker(true);

            // position of the tracked resizer handle (null for move/rotate mode, i.e. click into the drawing)
            resizerPos = DrawingFrame.getResizerHandleType(record.target);
            // whether rotation handle is grabbed
            rotateMode = DrawingFrame.isRotateHandle(record.target);
            // whether adjustment handle is grabbed
            adjHandleNode = DrawingFrame.isAdjustmentHandle(record.target) ? $(record.target) : null;
            // whether the drawing will be moved
            moveMode = !resizerPos && !rotateMode && !adjHandleNode;

            // the model of the clicked drawing object
            var activeModel = gridPane.drawingRenderer.getDrawingModelForFrame(activeFrame);
            // whether the clicked drawing frame is already selected
            var selected = DrawingFrame.isSelected(activeFrame);
            // whether to toggle the selection of the clicked drawing frame
            var toggle = moveMode && (record.modifiers.shiftKey || record.modifiers.ctrlKey || record.modifiers.metaKey);

            // do not modify the selection, if clicked on a selected drawing without modifier key
            if (!selected || toggle) {
                var drawingPos = activeModel.getPosition();
                selectionEngine.selectDrawing(drawingPos, { toggle });
                docView.scrollToDrawingFrame(drawingPos);
            }

            // whether exactly a single drawing objects are selected
            var singleSelection = selectionEngine.hasSingleDrawingSelection();
            // the text frame node of text shapes
            var textFrame = getTextFrame(activeFrame);

            // clicked into the text area of a drawing: start text edit mode
            if (moveMode && !toggle && singleSelection && textFrame && isTextEdit(record, textFrame)) {
                record.cancel();
                gridPane.grabFocus();
                // DOCS-3149: try to resolve text cursor position from click position
                const position = getOxoPositionFromPixelPosition(textFrame[0], record.point.x, record.point.y)?.start;
                docView.enterTextEditMode('drawing', { restart: true, position });
                return;
            }

            // no tracking after deselecting a selected drawing frame
            if (selected && toggle) {
                record.cancel();
                return;
            }

            // check edit mode and sheet protection (after updating the drawing selection!)
            if (!docView.isEditable() || docView.isSheetLocked()) {
                record.cancel();
                return;
            }

            // initialize information for all selected drawing objects
            const sheetRect = docView.getSheetRectangle();
            drawingDescMap.clear();
            let boundRect = null;
            diffAngle = tempMoveX = tempMoveY = 0;

            // collect information for all visible drawing objects to be tracked
            selectionEngine.getSelectedDrawings().forEach(function (position) {

                // the model of the drawing object
                var drawingModel = docView.drawingCollection.getModel(position);
                // the rectangle (in pixels); or null for hidden drawing objects
                var rectangle = drawingModel.getRectangle();
                // the DOM drawing frame for the model
                var drawingFrame = gridPane.drawingRenderer.getDrawingFrame(position);

                // nothing to do for hidden drawing objects, or for missing DOM frames
                if (!rectangle || !drawingFrame) { return; }

                // create and store the descriptor for the drawing object
                drawingDescMap.set(drawingModel, {
                    model: drawingModel,
                    position,
                    rectangle,
                    frameNode: drawingFrame,
                    rotatable: drawingModel.isDeepRotatable()
                });

                // take rotation of the drawing object into account, update the resulting bounding rectangle
                rectangle = rectangle.rotatedBoundingBox(drawingModel.getRotationRad());
                boundRect = boundRect ? boundRect.boundary(rectangle) : rectangle;

                // remove adjustment handle points if more than one drawing is selected
                if (!singleSelection) { DrawingFrame.clearAdjPoints(drawingFrame); }
            });

            // get the descriptor for the drawing object that will actually be tracked
            activeDesc = drawingDescMap.get(activeModel);

            // nothing to do without a valid selection
            if (!drawingDescMap.size || !activeDesc) {
                record.cancel();
                return;
            }

            // initialize further tracking data
            startPoint = gridPane.getSheetPointForPagePoint(record.point);
            mouseMoved = false;
            moveX = moveY = 0;
            scaleX = scaleY = 1;
            startAngle = activeDesc.model.getRotationDeg();

            // decide whether horizontal/vertical tracking is active (always in move/rotate mode)
            activeX = !resizerPos || /[lr]/.test(resizerPos);
            activeY = !resizerPos || /[tb]/.test(resizerPos);

            // decide whether to resize in reversed direction (from trailing border)
            reverseX = resizerPos && /l/.test(resizerPos);
            reverseY = resizerPos && /t/.test(resizerPos);

            // restrict movement of the drawing objects to the sheet area
            if (moveMode) {
                minMoveX = -boundRect.left;
                maxMoveX = sheetRect.width - boundRect.right();
                minMoveY = -boundRect.top;
                maxMoveY = sheetRect.height - boundRect.bottom();
            }

            // restrict resizing the drawing objects individually
            if (resizerPos) {
                drawingDescMap.forEach((drawingDesc, drawingModel) => {
                    var rect = drawingDesc.rectangle;
                    var skipRestricting = (drawingModel.getRotationDeg() !== 0) || drawingModel.isFlippedH() || drawingModel.isFlippedV();
                    var minSkipValue = -50000; // TODO: arbitrary big values for min and max, smarter algorithm is required for rotated/flipped shapes
                    var maxSkipValue = 50000; // see bug #52636 for more
                    drawingDesc.minScaleX = skipRestricting ? minSkipValue : getMinScale(rect.left, rect.width, sheetRect.width, reverseX);
                    drawingDesc.maxScaleX = skipRestricting ? maxSkipValue : getMaxScale(rect.left, rect.width, sheetRect.width, reverseX);
                    drawingDesc.minScaleY = skipRestricting ? minSkipValue : getMinScale(rect.top, rect.height, sheetRect.height, reverseY);
                    drawingDesc.maxScaleY = skipRestricting ? maxSkipValue : getMaxScale(rect.top, rect.height, sheetRect.height, reverseY);
                });
            }
            if (rotateMode) {
                drawingDescMap.forEach(drawingDesc => {
                    drawingDesc.frameNode.addClass(DrawingFrame.ROTATING_STATE_CLASSNAME);
                });
            }
            if (adjHandleNode) {
                drawingDescMap.forEach(drawingDesc => {
                    snapRect = drawingDesc.rectangle.clone();
                    snapRect.left = 0;
                    snapRect.top = 0;
                    cxnObject = adjHandleNode.data('cxnObj');
                    avList = cxnObject.avList;
                    gdList = cxnObject.gdList;
                    ahList = cxnObject.ah;
                    isPolar = ahList.type === 'polar';
                    adjMoveX = is.number(ahList.minX);
                    adjMoveY = is.number(ahList.minY);
                    adjPos = DrawingFrame.getPointPixelPosition(ahList, snapRect, avList, gdList);

                    if (isPolar) {
                        currentPolar = snapRect.pointToPolar(adjPos);
                        if (is.number(ahList.minAng) && is.number(ahList.maxAng)) {
                            minAng = DrawingFrame.getGeometryValue(ahList.minAng, snapRect, avList, gdList) / 60000;
                            maxAng = DrawingFrame.getGeometryValue(ahList.maxAng, snapRect, avList, gdList) / 60000;
                            currentAng = Math.round(currentPolar.a * 180 / Math.PI);
                            if (currentAng < 0) { currentAng += 360; }
                            currentAng = math.clamp(currentAng, minAng, maxAng);
                        }
                        if (is.number(ahList.minR) && is.number(ahList.maxR)) {
                            currentRadius = currentPolar.r;
                            minR = DrawingFrame.getGeometryValue(ahList.minR, snapRect, avList, gdList);
                            minRposPx = DrawingFrame.getPxPosFromGeo(minR, snapRect, avList, ahList, gdList, ahList.gdRefR);
                            minRPolar = snapRect.pointToPolar(minRposPx).r;

                            maxR = DrawingFrame.getGeometryValue(ahList.maxR, snapRect, avList, gdList);
                            maxRposPx = DrawingFrame.getPxPosFromGeo(maxR, snapRect, avList, ahList, gdList, ahList.gdRefR);
                            maxRPolar = snapRect.pointToPolar(maxRposPx).r;

                            absMinMaxSumR = Math.abs(maxR - minR);
                            minMaxRdiff = Math.abs(maxRPolar - minRPolar);
                        }
                    }

                    if (adjMoveX) {
                        minXValue = DrawingFrame.getGeometryValue(ahList.minX, snapRect, avList, gdList);
                        minXpos = DrawingFrame.getPxPosFromGeo(minXValue, snapRect, avList, ahList, gdList, ahList.gdRefX);

                        maxXValue = DrawingFrame.getGeometryValue(ahList.maxX, snapRect, avList, gdList);
                        maxXpos = DrawingFrame.getPxPosFromGeo(maxXValue, snapRect, avList, ahList, gdList, ahList.gdRefX);

                        xDiffpx = Math.abs(maxXpos.x - minXpos.x);
                        absMinMaxSumX = Math.abs(maxXValue - minXValue);
                    }
                    if (adjMoveY) {
                        minYValue = DrawingFrame.getGeometryValue(ahList.minY, snapRect, avList, gdList);
                        minYpos = DrawingFrame.getPxPosFromGeo(minYValue, snapRect, avList, ahList, gdList, ahList.gdRefY);

                        maxYValue = DrawingFrame.getGeometryValue(ahList.maxY, snapRect, avList, gdList);
                        maxYpos = DrawingFrame.getPxPosFromGeo(maxYValue, snapRect, avList, ahList, gdList, ahList.gdRefY);

                        yDiffpx = Math.abs(maxYpos.y - minYpos.y);
                        absMinMaxSumY = Math.abs(maxYValue - minYValue);
                    }
                });
            }
        }

        // updates the position of the drawing frames according to the passed tracking event
        function moveTracking(record) {

            // current tracking position (absolute sheet position, in pixels)
            var currPoint = gridPane.getSheetPointForPagePoint(record.point);
            var delta = coord.pointsDiff(startPoint, currPoint);
            // shift distance in both directions
            var shift = coord.point(activeX ? delta.x : 0, activeY ? delta.y : 0);

            if (resizerPos) {
                var signX = activeDesc.model.isFlippedH() ? -1 : 1;
                var signY = activeDesc.model.isFlippedV() ? -1 : 1;
                var normalizedCoords = DrawingFrame.getNormalizedResizeDeltas(delta.x, delta.y, activeX, activeY, (reverseX ? -1 : 1), (reverseY ? -1 : 1), startAngle);
                shift.x = normalizedCoords.x * signX;
                shift.y = normalizedCoords.y * signY;
                scaleX = shift.x / activeDesc.rectangle.width + 1;
                scaleY = shift.y / activeDesc.rectangle.height + 1;
            }

            // start tracking after a threshold of 3 pixels
            if (!mouseMoved && (math.radius(shift.x, shift.y) < 3)) { return; }

            // show tracking frames if mouse has moved the first time (even if the drawings will
            // not really move due to restrictions of the bounding rectangle in the sheet area)
            if (!mouseMoved) {
                drawingDescMap.forEach(drawingDesc => {
                    // skip non-rotatable shapes on rotation
                    if (rotateMode && !drawingDesc.rotatable) { return; }
                    DrawingFrame.toggleTracking(drawingDesc.frameNode, true);
                    drawingDesc.trackerNode = DrawingFrame.getTrackerNode(drawingDesc.frameNode);
                });
                mouseMoved = true;
            }

            // restrict to allowed move distances
            if (moveMode) {
                if (record.modifiers.shiftKey) { // restrict move to x or y axis only
                    if (Math.abs(shift.x) > Math.abs(shift.y)) {
                        shift.y = 0;
                    } else {
                        shift.x = 0;
                    }
                }
                moveX = math.clamp(shift.x, minMoveX, maxMoveX);
                moveY = math.clamp(shift.y, minMoveY, maxMoveY);
            }

            // update the rotate angle according to current tracking shift
            if (rotateMode) {
                var offsetX = currPoint.x - activeDesc.rectangle.centerX();
                var offsetY = currPoint.y - activeDesc.rectangle.centerY();
                var newAngle = Math.round(Math.atan2(offsetY, offsetX) / PI_180) + 90;
                // make 15 deg step with shift key
                if (record.modifiers.shiftKey) { newAngle = math.roundp(newAngle, 15); }
                if (activeDesc.model.isFlippedV()) { newAngle += 180; }
                diffAngle = newAngle - startAngle;
            }

            if (adjHandleNode) {
                var allowX = isPolar || adjMoveX;
                var allowY = isPolar || adjMoveY;
                var modAttrs = { avList: { ...avList } };
                var isFlipX = activeDesc.model.isFlippedH() ? -1 : 1;
                var isFlipY = activeDesc.model.isFlippedV() ? -1 : 1;

                var normDeltas = DrawingFrame.getNormalizedResizeDeltas(delta.x, delta.y, allowX, allowY, isFlipX, isFlipY, startAngle);
                var scaledValue = null;

                actXpos = normDeltas.x + adjPos.x;
                actYpos = normDeltas.y + adjPos.y;
                if (adjMoveX) {
                    actXpos = math.clamp(actXpos, Math.min(maxXpos.x, minXpos.x), Math.max(maxXpos.x, minXpos.x));
                    if (Number.isFinite(actXpos)) {
                        scaledValue = DrawingFrame.scaleAdjustmentValue(actXpos, minXpos.x, maxXpos.x, minXValue, xDiffpx, absMinMaxSumX);
                        if (Number.isFinite(scaledValue)) { modAttrs.avList[ahList.gdRefX] = scaledValue; }
                    }
                }
                if (adjMoveY) {
                    actYpos = math.clamp(actYpos, Math.min(maxYpos.y, minYpos.y), Math.max(maxYpos.y, minYpos.y));
                    if (Number.isFinite(actYpos)) {
                        scaledValue = DrawingFrame.scaleAdjustmentValue(actYpos, minYpos.y, maxYpos.y, minYValue, yDiffpx, absMinMaxSumY);
                        if (Number.isFinite(scaledValue)) { modAttrs.avList[ahList.gdRefY] = scaledValue; }
                    }

                }
                if (isPolar) {
                    currentPolar = snapRect.pointToPolar({ x: actXpos, y: actYpos });
                    if (is.number(minAng) && is.number(maxAng)) {
                        currentAng = Math.round(currentPolar.a * 180 / Math.PI);
                        if (currentAng < 0) { currentAng += 360; }
                        currentAng = math.clamp(currentAng, minAng, maxAng);
                    }
                    if (is.number(minR) && is.number(maxR)) {
                        currentRadius = currentPolar.r;
                        currentRadius = math.clamp(currentRadius, Math.min(minRPolar, maxRPolar), Math.max(minRPolar, maxRPolar));
                    }
                    if (is.number(ahList.gdRefAng)) {
                        modAttrs.avList[ahList.gdRefAng] = currentAng * 60000;
                    }
                    if (is.number(ahList.gdRefR)) {
                        scaledValue = DrawingFrame.scaleAdjustmentValue(currentRadius, minRPolar, maxRPolar, minR, minMaxRdiff, absMinMaxSumR, true);
                        if (Number.isFinite(scaledValue)) { modAttrs.avList[ahList.gdRefR] = scaledValue; }
                    }
                }

                adjHandleNode.css({ left: actXpos - 5, top: actYpos - 5 });
            }

            // move the tracker nodes relative to the drawing frames
            drawingDescMap.forEach((drawingDesc, drawingModel) => {

                var flipH = drawingModel.isFlippedH();
                var flipV = drawingModel.isFlippedV();

                if (moveMode) {
                    var normalizedCoords = DrawingFrame.getNormalizedMoveCoordinates(moveX, moveY, drawingModel.getRotationDeg(), flipH, flipV);
                    tempMoveX = normalizedCoords.x;
                    tempMoveY = normalizedCoords.y;
                }

                if (rotateMode) {
                    if (!drawingDesc.rotatable) { return; } // skip non-rotatable shapes on rotation
                    var trackerAngle = math.modulo(diffAngle + drawingModel.getRotationDeg(), 360);
                    var transform = getCssTransform(trackerAngle, flipH, flipV);
                    drawingDesc.frameNode.css({ transform });
                    DrawingFrame.addRotationHint(drawingDesc.frameNode.find(DrawingFrame.ROTATE_ANGLE_HINT_SELECTOR), trackerAngle, flipH, flipV);
                }

                var rectangle = getDrawingRectangle(drawingDesc, record);
                drawingDesc.trackerNode.css({
                    left: rectangle.left - drawingDesc.rectangle.left,
                    top: rectangle.top - drawingDesc.rectangle.top,
                    width: rectangle.width,
                    height: rectangle.height
                });
            });
        }

        function scrollTracking(record) {
            gridPane.scrollRelative(record.scroll.x, record.scroll.y);
            moveTracking(record);
        }

        // finalizes the drawing tracking sequence
        function finalizeTracking(record) {

            // whether to apply the changes (tracking sequence not canceled)
            const apply = record.type === "end";

            // deinitialize all tracking nodes
            drawingDescMap.forEach((drawingDesc, drawingModel) => {
                DrawingFrame.toggleTracking(drawingDesc.frameNode, false);
                if (rotateMode) {
                    DrawingFrame.removeRotateAngleHint(drawingDesc.frameNode);
                    drawingDesc.frameNode.removeClass(DrawingFrame.ROTATING_STATE_CLASSNAME);
                    if (mouseMoved && !apply) { DrawingFrame.updateCssTransform(drawingDesc.frameNode, drawingModel.getRotationDeg()); }
                }
            });

            // only generate document operations if the drawing was really moved
            if (apply && mouseMoved) {

                if (rotateMode) {
                    docView.executeControllerItem('drawing/transform/rotate', diffAngle);
                    drawingDescMap.forEach((drawingDesc, drawingModel) => {
                        DrawingFrame.updateResizersMousePointers(drawingDesc.frameNode, drawingModel.getRotationDeg());
                    });
                    return;
                }

                var toggleFlipH = scaleX < 0;
                var toggleFlipV = scaleY < 0;
                docView.executeControllerItem('drawing/change/explicit', Array.from(drawingDescMap, ([drawingModel, drawingDesc]) => {

                    var newAttrs = {
                        rotation: drawingModel.getRotationDeg(),
                        flipH: toggleFlipH !== drawingModel.isFlippedH(),
                        flipV: toggleFlipV !== drawingModel.isFlippedV()
                    };

                    // adjust the anchor position according to the rotation angle in OOXML
                    var rectangle = getDrawingRectangle(drawingDesc, record);
                    if (ooxml && drawingDesc.rotatable && hasSwappedDimensions(newAttrs)) {
                        rectangle.rotateSelf();
                    }

                    var drawingAttrs = docView.drawingCollection.generateAnchorAttributesForRect(drawingModel, rectangle, { pixel: true }) || {};
                    var attrSet = { drawing: drawingAttrs };

                    // toggle horizontal flipping attribute
                    if (toggleFlipH && drawingDesc.rotatable) {
                        drawingAttrs.flipH = newAttrs.flipH;
                    }

                    // toggle vertical flipping attribute (ODF: needs to be simulated as horizontal flip and 180deg rotation)
                    if (toggleFlipV && drawingDesc.rotatable) {
                        if (ooxml) {
                            drawingAttrs.flipV = newAttrs.flipV;
                        } else {
                            drawingAttrs.flipH = !newAttrs.flipH;
                            drawingAttrs.rotation = math.modulo(newAttrs.rotation + 180, 360);
                        }
                    }

                    // adjustment handles
                    if (adjHandleNode) {
                        var geoAttrs = attrSet.geometry = { avList: { ...avList } };
                        if (adjMoveX && Number.isFinite(actXpos)) {
                            geoAttrs.avList[ahList.gdRefX] = DrawingFrame.scaleAdjustmentValue(actXpos, minXpos.x, maxXpos.x, minXValue, xDiffpx, absMinMaxSumX);
                        }
                        if (adjMoveY && Number.isFinite(actYpos)) {
                            geoAttrs.avList[ahList.gdRefY] = DrawingFrame.scaleAdjustmentValue(actYpos, minYpos.y, maxYpos.y, minYValue, yDiffpx, absMinMaxSumY);
                        }
                        if (isPolar) {
                            if (is.number(ahList.gdRefAng) && Number.isFinite(currentAng)) {
                                geoAttrs.avList[ahList.gdRefAng] = currentAng * 60000;
                            }
                            if (is.number(ahList.gdRefR)) {
                                geoAttrs.avList[ahList.gdRefR] = DrawingFrame.scaleAdjustmentValue(currentRadius, minRPolar, maxRPolar, minR, minMaxRdiff, absMinMaxSumR, true);
                            }
                        }
                    }

                    // TODO: move this code into the drawing renderer as reaction on a document operation (remote editor support!)
                    if (resizerPos && selectionEngine.hasSingleDrawingSelection()) {
                        // bug 58180: reposition adjustment points after resize - they have wrong positions, and must be repainted
                        selectionEngine.getSelectedDrawings().forEach(function (position) {
                            var drawingFrame = gridPane.drawingRenderer.getDrawingFrame(position);
                            var zoomFactor = docView.getZoomFactor();
                            var width = convertHmmToLength(drawingAttrs.width, 'px', 1);
                            var height = convertHmmToLength(drawingAttrs.height, 'px', 1);
                            DrawingFrame.repositionAdjPoints(drawingFrame, zoomFactor, { width, height });
                        });
                    }

                    return { position: drawingDesc.position, attrs: attrSet };
                }));
            } else {
                // no longer blocking external operations, if no operation needs to be applied
                docModel.setInternalOperationBlocker(false);
            }

            // open context menu after a long-tap without move events
            if (TOUCH_DEVICE && apply && !mouseMoved && (record.duration > 750)) {
                activeDesc.frameNode.trigger('contextmenu');
            }

            // DOCS-1564: start edit mode after double taps on touch devices
            if (TOUCH_DEVICE && record.dblClick) {
                startTracking(record);
            }
        }

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking,
            finally: finalizeTracking
        };
    }

    // --------------------------------------------------------------------

    /**
     * Handles pinch events triggered by the touch framework.
     */
    var pinchHandler = (function () {

        var startDistance;
        var transformScale;
        var originX;
        var originY;
        var originOffsetX;
        var originOffsetY;
        var translateX;
        var translateY;
        var visibleRectangle;
        var scrollSizeNode = layerRootNode.parent();
        var lastPhase;
        var pinchActive = false;

        function calcOffset(translate, refSize, refMin) {
            var newRefSize = refSize / transformScale;
            var newRefMin = (refSize - newRefSize) / 2;
            return refMin + newRefMin - (translate * transformScale);
        }

        function handlePinchEvent(phase, event, distance, midPoint) {
            // workaround for IOS
            if (phase === 'cancel' && lastPhase === 'move') { phase = 'end'; }

            lastPhase = phase;

            switch (phase) {

                case 'start': {
                    event.preventDefault();
                    pinchActive = true;
                    startDistance = distance;
                    originX = midPoint.x;
                    originY = midPoint.y;

                    const offset = scrollSizeNode.offset();
                    originOffsetX = originX - offset.left;
                    originOffsetY = originY - offset.top;

                    // default values, if no move event is triggered before end event
                    translateX = translateY = 0;
                    transformScale = 1;

                    visibleRectangle = docView.getActiveGridPane().getVisibleRectangle();
                    break;
                }

                case 'move':
                    event.preventDefault();
                    transformScale = math.clamp(distance / startDistance, 0.5, 2.0);
                    transformScale = docView.zoomState.clampScale(transformScale);
                    translateX = midPoint.x - originX;
                    translateY = midPoint.y - originY;
                    break;

                case 'end': {
                    event.preventDefault();
                    if (!pinchActive) { return; }

                    const offsetX = calcOffset(translateX, visibleRectangle.width, visibleRectangle.left);
                    const offsetY = calcOffset(translateY, visibleRectangle.height, visibleRectangle.top);

                    const zoom = math.roundp(docView.zoomState.factor * transformScale, 0.05); // round to steps of 5%
                    const anchorRight = docView.colCollection.getScrollAnchorByOffset(offsetX, { pixel: true });
                    const anchorBottom = docView.rowCollection.getScrollAnchorByOffset(offsetY, { pixel: true });
                    docView.sheetPropSet.update({ zoom, anchorRight, anchorBottom });

                    pinchActive = false;
                    break;
                }

                case 'cancel':
                    pinchActive = false;
                    break;
            }
        }

        function updateCssTransformation() {
            switch (lastPhase) {
                case 'start':
                case 'move':
                    scrollSizeNode.css({
                        transform: `translate3d(${translateX}px,${translateY}px,0) scale(${transformScale})`,
                        transformOrigin: `${originOffsetX}px ${originOffsetY}px`
                    });
                    break;

                case 'end':
                case 'cancel':
                    scrollSizeNode.css({ transform: '', transformOrigin: '' });
                    break;
            }
        }

        return gridPane.debounce(handlePinchEvent, updateCssTransformation, { delay: "animationframe" });
    }());

    // initialization ---------------------------------------------------------

    // auto-fill: resize active selection range for auto-fill and auto-delete
    this.registerTrackingCallbacks("autofill", createAutoFillCallbacks());
    // highlight layer: move and resize highlighted cell ranges
    this.registerTrackingCallbacks("highlight", createHighlightCallbacks(), { keepTextEdit: true });
    // drawing layer: select, move, and resize drawing objects
    this.registerTrackingCallbacks("drawing", createDrawingCallbacks(), { readOnly: true });
    // change current selection range with resizer handles on touch devices
    this.registerTrackingCallbacks("select-touch-resize", createSelectTouchResizeCallbacks(), { readOnly: true, keepTextEdit: true });
    // touch devices: scroll, or set new selection
    this.registerTrackingCallbacks("select-touch", createSelectTouchCallbacks(), { readOnly: true, keepTextEdit: true });
    // set new selection, or extend current selection with mouse and modifier keys
    this.registerTrackingCallbacks("select-mouse", createSelectMouseCallbacks(), { readOnly: true, keepTextEdit: true });

    // do not treat clicks on same cell as double click if a controller item will
    // be executed inbetween, e.g. keyboard shortcuts (found by E2E test C272369)
    this.listenTo(docView, "controller:execute", () => {
        sameAddressClicks = 0;
    });

    // suppress native browser events
    layerRootNode.on('dblclick dragenter dragexit dragover dragleave', false);

    layerRootNode.enableTouch({ pinchHandler });

    // destructor
    this.registerDestructor(function () {
        layerRootNode.disableTouch();
    });
}
