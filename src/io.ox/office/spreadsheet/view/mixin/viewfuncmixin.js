/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, fun, itr, ary, map, dict, json, jpromise } from '@/io.ox/office/tk/algorithms';
import { BREAK, addProperty, convertLengthToHmm, getIntegerOption, getStringOption } from '@/io.ox/office/tk/utils';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { NO_BORDER, isVisibleBorder } from '@/io.ox/office/editframework/utils/border';
import { mixBorders, isVisibleBorder as isVisibleMixedBorder, getBorderFlags, getBorderAttributes, isFullyAmbiguousBorder } from '@/io.ox/office/editframework/utils/mixedborder';
import { checkForHyperlink } from '@/io.ox/office/editframework/utils/hyperlinkutils';

import { hasWrappingAttributes } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { Direction, MergeMode, Interval, ErrorCode, Address, Range, IntervalArray, RangeArray, FindMatchType, makeRejected, isVerticalDir, isLeadingDir,
    getOuterBorderKey, getBorderName, splitDisplayString, getAdjacentRange } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { SubtotalResult } from '@/io.ox/office/spreadsheet/utils/subtotalresult';
import { Dimension, getCellValue, isValidMatrixDim, Matrix } from '@/io.ox/office/spreadsheet/model/formula/formulautils';
import { DELETE_COLUMNS_TITLE, DELETE_COLUMNS_QUERY, DELETE_ROWS_TITLE, DELETE_ROWS_QUERY } from '@/io.ox/office/spreadsheet/view/labels';
import { HyperlinkDialog } from '@/io.ox/office/spreadsheet/view/dialogs';
import { FormatCodeParser } from "@/io.ox/office/editframework/model/formatter/codeparser";

// constants ==================================================================

// tolerance for optimal column width, in 1/100 mm
const SET_COLUMN_WIDTH_TOLERANCE = 50;

// tolerance for optimal row height (set explicitly), in 1/100 mm
const SET_ROW_HEIGHT_TOLERANCE = 15;

// tolerance for optimal row height (updated implicitly), in 1/100 mm
const UPDATE_ROW_HEIGHT_TOLERANCE = 50;

// default value for a visible border
const DEFAULT_SINGLE_BORDER = { style: 'single', width: convertLengthToHmm(1, 'px'), color: Color.AUTO };

// mix-in class ViewFuncMixin =================================================

/**
 * Mix-in class for the class `SpreadsheetView` that provides implementations
 * of complex document functionality and status requests, called from the
 * application controller.
 */
export function ViewFuncMixin() {

    // self reference (spreadsheet view instance)
    var self = this;

    // the spreadsheet model, and other model objects
    var docModel = this.docModel;
    var fontMetrics = docModel.fontMetrics;
    var numberFormatter = docModel.numberFormatter;
    var autoStyles = docModel.autoStyles;

    // the active sheet model and index
    var activeSheetModel = null;

    // collections of the active sheet
    var colCollection = null;
    var rowCollection = null;
    var mergeCollection = null;
    var cellCollection = null;
    var tableCollection = null;

    // the contents and formatting of the active cell
    var activeAddress = Address.A1;
    var activeCellValue = null;
    var activeTokenDesc = null;
    var activeAttributeSet = null;
    var activeParsedFormat = null;

    // the mixed borders of the current selection
    var selectedMixedBorders = {};

    // a deferred object used to wait for the mixed borders of the current selection
    var mixedBordersDef = this.createDeferred();

    // the subtotal data of the current selection
    var selectedSubtotals = new SubtotalResult();

    // private methods ----------------------------------------------------

    /**
     * Updates the cached data of the active cell (contents, formatting),
     * and the entire selection (border settings, subtotals). To improve
     * performance, updating settings of the entire selection (which is a
     * potentially expensive operation) will be done in a sliced background
     * loop.
     *
     * @param {Boolean} forceUpdate
     *  If set to true, current background timers for mixed border settings
     *  and subtotal results will always be aborted. By default, the
     *  background timers will not be aborted if the selected ranges have
     *  not been changed (e.g. while traversing the active cell inside the
     *  selected ranges).
     */
    var updateSelectionSettings = (function () {

        // initial delay before the background loops will be started
        var INITIAL_DELAY = 300;
        // options for the background timers to collect borders and subtotals in the cell collection
        var BACKGROUND_TIMER_OPTIONS = { slice: 200, interval: 50 };

        // timers for the background loops to collect borders and subtotal results
        var initialDelayTimer = null;
        var mixedBorderTimer = null;
        var subtotalResultTimer = null;
        // the selected cell ranges currently used to collect borders and subtotals
        var selectedRanges = new RangeArray();
        // the new mixed borders, mapped by border keys
        var newMixedBorders = null;

        function getMixedBorder(mixedBorder, border) {
            return mixedBorder ? mixBorders(mixedBorder, border) : { ...border };
        }

        function updateMixedBorder(key, border) {
            var mixedBorder = newMixedBorders[key];
            return (newMixedBorders[key] = getMixedBorder(mixedBorder, border));
        }

        // creates an iterator that provides cell ranges with existing border attributes
        function *createBorderAttrIterator(borderRange, visitHor, visitVert, visitDiag) {
            for (const { range, style } of cellCollection.styleRanges(borderRange)) {
                const borderMap = autoStyles.getBorderMap(style);
                const hasHor  = visitHor  && (borderMap.t || borderMap.b);
                const hasVert = visitVert && (borderMap.l || borderMap.r);
                const hasDiag = visitDiag && (borderMap.d || borderMap.u);
                if (hasHor || hasVert || hasDiag) {
                    range.borderMap = borderMap;
                    yield range;
                }
            }
        }

        function processOuterBorder(range, keyH, keyV) {

            var vertical = !keyH;
            var key = vertical ? keyV : keyH;
            var borderLength = 0;

            var iterator = createBorderAttrIterator(range, !vertical, vertical, false);
            var promise = self.iterateSliced(iterator, function (styleRange) {
                updateMixedBorder(key, styleRange.borderMap[key] || NO_BORDER);
                borderLength += styleRange.size(!vertical);
            }, BACKGROUND_TIMER_OPTIONS);

            return promise.then(function () {
                if (borderLength < range.size(!vertical)) {
                    updateMixedBorder(key, NO_BORDER);
                }
            });
        }

        // Returns the parts of the passed band interval with visible borders, and the resulting mixed border.
        // The resulting interval array contains intervals with additional property 'mixedBorder' that will be
        // null if the entire interval does not contain a visible border (i.e. if the result array is empty).
        function getBorderIntervals(bandInterval, vertical, leading) {

            var key = getOuterBorderKey(vertical, leading);
            var mixedBorder = null;

            var borderIntervals = IntervalArray.map(bandInterval.ranges, function (attrRange) {
                var border = attrRange.borderMap[key];
                if (isVisibleBorder(border)) {
                    mixedBorder = getMixedBorder(mixedBorder, border);
                    var interval = attrRange.interval(!vertical);
                    interval.border = border;
                    return interval;
                }
            });

            // variable 'mixedBorder' still null: no visible borders in the band
            borderIntervals.mixedBorder = mixedBorder;
            return borderIntervals;
        }

        function processBorderRanges(range, borderRanges, key, vertical) {

            var bandIntervals = vertical ? borderRanges.getColBands({ ranges: true }) : borderRanges.getRowBands({ ranges: true });
            var bandLength = range.size(!vertical);
            var first = range.getStart(vertical);
            var last = range.getEnd(vertical);

            // calculates the mixed borders of the passed adjoining intervals
            function processBorderIntervals(intervals1, intervals2) {

                // Add the precalculated visible mixed borders stored in the interval arrays.
                var mixedBorder = null;
                if (intervals1.mixedBorder) { mixedBorder = updateMixedBorder(key, intervals1.mixedBorder); }
                if (intervals2 && intervals2.mixedBorder) { mixedBorder = updateMixedBorder(key, intervals2.mixedBorder); }

                // Mix in an additional invisible border, if the visible border intervals do not cover the entire
                // band length (but do not calculate the size of all border intervals, if the mixed border already
                // has 'mixed' state, i.e. it already contains visible and invisible border states).
                if (!mixedBorder || !mixedBorder.mixed) {
                    // merge the passed interval arrays to determine the intervals covered by either of the arrays
                    // (these intervals contain a visible border, and the remaining intervals do not have a border)
                    var borderLength = intervals2 ? intervals1.concat(intervals2).merge().size() : intervals1.size();
                    if (borderLength < bandLength) { updateMixedBorder(key, NO_BORDER); }
                }
            }

            // no band intervals available: range does not contain any visible border lines
            if (!bandIntervals.length) {
                updateMixedBorder(key, NO_BORDER);
                return self.createResolvedPromise();
            }

            // add empty border, if there is a gap with inner borders before the first, after the last, or between band intervals
            if ((first + 1 < bandIntervals[0].first) || (ary.at(bandIntervals, -1).last + 1 < last) || bandIntervals.some((bandInterval, index) => {
                return (index > 0) && (bandIntervals[index - 1].last + 2 < bandInterval.first);
            })) {
                updateMixedBorder(key, NO_BORDER);
            }

            return self.iterateArraySliced(bandIntervals, (bandInterval, index) => {

                // whether to process the borders for the leading boundary of the current band (ignore leading boundary of leading range border)
                var useLeading = first < bandInterval.first;
                // whether to process the inner borders in the current band (the band must contain at least two columns/rows)
                var useInner = bandInterval.size() > 1;
                // whether to process the borders for the trailing boundary of the current band (ignore trailing boundary of trailing range border)
                var useTrailing = bandInterval.last < last;

                // the intervals with visible border attributes at the leading side of the band
                bandInterval.leading = (useLeading || useInner) ? getBorderIntervals(bandInterval, vertical, true) : null;
                // the intervals with visible border attributes at the trailing side of the band
                bandInterval.trailing = (useInner || useTrailing) ? getBorderIntervals(bandInterval, vertical, false) : null;

                // the preceding band interval, and whether it adjoins to the current band
                var prevBandInterval = bandIntervals[index - 1];
                var adjoinsPrevBand = prevBandInterval && (prevBandInterval.last + 1 === bandInterval.first);

                // the next band interval, and whether it adjoins to the current band
                var nextBandInterval = bandIntervals[index + 1];
                var adjoinsNextBand = nextBandInterval && (bandInterval.last + 1 === nextBandInterval.first);

                // combine the trailing borders of the preceding adjoined band with the leading borders of the current band
                if (adjoinsPrevBand) { processBorderIntervals(prevBandInterval.trailing, bandInterval.leading); }

                // process the leading borders of the current band, if the previous band does not adjoin
                if (useLeading && !adjoinsPrevBand) { processBorderIntervals(bandInterval.leading); }

                // combine the inner borders in the current band
                if (useInner) { processBorderIntervals(bandInterval.leading, bandInterval.trailing); }

                // combine the trailing borders of the current band, if the next band does not adjoin
                // (adjoining bands will be handled in the next iteration of this loop)
                if (useTrailing && !adjoinsNextBand) { processBorderIntervals(bandInterval.trailing); }

            }, BACKGROUND_TIMER_OPTIONS);
        }

        // process each range in the passed array (exit the loop early, if mixed border becomes ambiguous)
        function getMixedBorders(ranges, keyH, keyV) {
            return self.iterateArraySliced(ranges, function (range) {

                // early exit, if the collected mixed borders become completely ambiguous
                var skipH = !keyH || isFullyAmbiguousBorder(newMixedBorders[keyH]);
                var skipV = !keyV || isFullyAmbiguousBorder(newMixedBorders[keyV]);
                if (skipH && skipV) { return BREAK; }

                // property '_outerBorder' used for ranges located at the sheet boundary (do not collect inner borders)
                if (range._outerBorder) { return processOuterBorder(range, keyH, keyV); }

                // otherwise, collect the inner border attributes (skip ranges without inner borders)
                var processH = !skipH && !range.singleRow();
                var processV = !skipV && !range.singleCol();
                if (!processH && !processV) { return; }

                // use a cell style iterator to collect all cell ranges with visible borders into an array
                const borderRanges = new RangeArray();
                const iterator = createBorderAttrIterator(range, processH, processV, false);
                const promise2 = self.iterateSliced(iterator, borderRange => borderRanges.push(borderRange), BACKGROUND_TIMER_OPTIONS);

                // process the collected ranges with visible border attributes
                return promise2.then(() => jpromise.fastChain(
                    processH ? () => processBorderRanges(range, borderRanges, keyH, false) : undefined,
                    processV ? () => processBorderRanges(range, borderRanges, keyV, true) : undefined
                ));
            }, BACKGROUND_TIMER_OPTIONS);
        }

        // collects the mixed border states in the current selection in a background loop
        function collectMixedBorders() {

            const { maxCol, maxRow } = docModel.addressFactory;

            // Build the iterator ranges for the outer borders of the selected ranges. Use the border column/row inside the range,
            // and the adjacent column/row outside the range, to include border attributes coming from outside cells next to the
            // selected ranges. Range borders located directly at the sheet boundary will not be included here (no cells from
            // outside available). These ranges will be handled below.
            // Example: For the selection range B2:E5, the cells B1:E2 will be used to find the top border settings.
            var borderRangesT = RangeArray.map(selectedRanges, range => (range.a1.r === 0)      ? null : range.rowRange(-1, 2));
            var borderRangesB = RangeArray.map(selectedRanges, range => (range.a2.r === maxRow) ? null : range.rowRange(range.rows() - 1, 2));
            var borderRangesL = RangeArray.map(selectedRanges, range => (range.a1.c === 0)      ? null : range.colRange(-1, 2));
            var borderRangesR = RangeArray.map(selectedRanges, range => (range.a2.c === maxCol) ? null : range.colRange(range.cols() - 1, 2));

            // Build the iterator ranges for the outer borders of the selected ranges that are located at the sheet boundary.
            // Example: For the selection range B1:E5, the inner cells B1:E1 will be used to find the top border settings.
            borderRangesT.append(addProperty(RangeArray.map(selectedRanges, range => (range.a1.r === 0)      ? range.headerRow()   : null), '_outerBorder', true));
            borderRangesB.append(addProperty(RangeArray.map(selectedRanges, range => (range.a2.r === maxRow) ? range.footerRow()   : null), '_outerBorder', true));
            borderRangesL.append(addProperty(RangeArray.map(selectedRanges, range => (range.a1.c === 0)      ? range.leadingCol()  : null), '_outerBorder', true));
            borderRangesR.append(addProperty(RangeArray.map(selectedRanges, range => (range.a2.c === maxCol) ? range.trailingCol() : null), '_outerBorder', true));

            // collect all mixed borders of all outer and inner border positions
            newMixedBorders = {};
            mixedBorderTimer = jpromise.fastChain(
                () => getMixedBorders(selectedRanges, 'h', 'v'),
                () => getMixedBorders(borderRangesT, 't', null),
                () => getMixedBorders(borderRangesB, 'b', null),
                () => getMixedBorders(borderRangesL, null, 'l'),
                () => getMixedBorders(borderRangesR, null, 'r')
            );

            // resolve the deferred object, provide the new mixed borders as value
            return mixedBorderTimer.done(function () {

                // add missing entries (if the ranges do not contain any visible border)
                selectedMixedBorders = {};
                'tblrhv'.split('').forEach(function (key) {
                    var borderName = getBorderName(key);
                    selectedMixedBorders[borderName] = newMixedBorders[key] || { ...NO_BORDER };
                });

                mixedBordersDef.resolve(selectedMixedBorders);
            });
        }

        // collects the subtotal results in the current selection in a background loop
        function collectSubtotalResults() {

            // collect the subtotal results of the selected ranges
            var iterator = cellCollection.generateSubtotals(selectedRanges);
            selectedSubtotals = new SubtotalResult();
            subtotalResultTimer = self.iterateSliced(iterator, result => selectedSubtotals.add(result), BACKGROUND_TIMER_OPTIONS);

            // copy the resulting subtotals, and notify all listeners
            return subtotalResultTimer.done(function () {
                self.trigger('update:selection:data');
            });
        }

        // the actual implementation of the method updateSelectionSettings()
        return function (forceUpdate) {

            // update settings of the active cell synchronously for fast GUI feedback
            activeAddress = self.selectionEngine.getActiveCell();
            activeCellValue = cellCollection.getValue(activeAddress);
            activeTokenDesc = cellCollection.getTokenArray(activeAddress, { fullMatrix: true, grammarType: "ui" });
            activeAttributeSet = cellCollection.getAttributeSet(activeAddress);
            activeParsedFormat = cellCollection.getParsedFormat(activeAddress);

            // abort running timers, and restart the background loops
            var newSelectedRanges = self.selectionEngine.getSelectedRanges();
            if (forceUpdate || !selectedRanges.deepEquals(newSelectedRanges)) {

                // a new pending deferred object that can be used to wait for the border settings
                if (mixedBordersDef.state() !== 'pending') {
                    mixedBordersDef = self.createDeferred();
                }

                // abort running timers
                if (initialDelayTimer) { initialDelayTimer.abort(); initialDelayTimer = null; }
                if (mixedBorderTimer) { mixedBorderTimer.abort(); mixedBorderTimer = null; }
                if (subtotalResultTimer) { subtotalResultTimer.abort(); subtotalResultTimer = null; }

                // cache the current selection ranges
                selectedRanges = newSelectedRanges;

                // restart the background loops after a delay
                initialDelayTimer = self.executeDelayed(fun.undef, INITIAL_DELAY);
                initialDelayTimer.then(collectMixedBorders).then(collectSubtotalResults);
            }
        };
    }());

    /**
     * Returns the column/row intervals of the selected ranges, optionally
     * depending on a specific target column/row.
     *
     * @param {Boolean} columns
     *  Whether to return column intervals (true) or row intervals (false).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {number} [options.target]
     *      If specified, the zero-based index of a specific target column
     *      or row. If the current selection contains entire column/row
     *      ranges covering this column/row, the respective intervals of
     *      these ranges will be returned instead; otherwise a single
     *      interval containing the target column/row will be returned.
     *  - {boolean} [options.visible=false]
     *      If set to `true`, the returned intervals will be reduced to the
     *      column or row intervals covered by the visible panes.
     *  - {boolean} [options.strict=false]
     *      If set to `true`, this method returns an empty array for mixed
     *      selections (entire columns/rows together with regular cell
     *      ranges).
     *  - {boolean} [options.distinct=false]
     *      If set to `true`, the resulting intervals will not be merged
     *      (but still sorted). Useful for inserting columns/rows where the
     *      intervals are allowed to be adjoining or overlapping.
     *
     * @returns {IntervalArray}
     *  An array of column/row intervals for the current selection, already
     *  merged and sorted.
     */
    function getSelectedIntervals(columns, options) {

        // the selected ranges
        var ranges = self.selectionEngine.getSelectedRanges();
        // the target column/row
        var target = getIntegerOption(options, 'target');
        // the resulting intervals
        var intervals = null;

        // use entire column/row ranges in selection, if target index has been passed
        if (is.number(target)) {
            // convert only the full column/row ranges in selection to intervals
            intervals = IntervalArray.map(ranges, function (range) {
                return docModel.addressFactory.isFullRange(range, columns) ? range.interval(columns) : null;
            });
            // if the target column/row is not contained in the intervals, make an interval for it
            if (!intervals.containsIndex(target)) {
                intervals = new IntervalArray(new Interval(target));
            }
        } else if (options && options.strict) {
            // strict mode: do not allow entire column/row ranges mixed with other ranges
            var rangeGroups = docModel.addressFactory.getRangeGroups(ranges);
            if (dict.singleKey(rangeGroups)) {
                ranges = (columns ? rangeGroups.colRanges : rangeGroups.rowRanges) || rangeGroups.cellRanges;
                if (ranges) { intervals = ranges.intervals(columns); }
            }
        } else {
            // otherwise, use the column/row intervals of the entire selection
            intervals = ranges.intervals(columns);
        }

        // return empty array, if selection could not be resolved
        if (!intervals) { return new IntervalArray(); }

        // reduce to the intervals visible in the header panes
        if (options?.visible) {
            intervals = self.getVisibleIntervals(intervals, columns);
        }

        // merge and sort the resulting intervals (distinct mode: only sort the intervals)
        return options?.distinct ? intervals.sort().unify() : intervals.merge();
    }

    /**
     * Returns the column/row intervals from the current selection, if it
     * contains hidden columns/rows. If the selected columns/rows are
     * completely visible, the selection consists of a single column/row
     * interval, and that interval is located next to a hidden column/row
     * interval (at either side, preferring leading), that hidden interval
     * will be returned instead.
     *
     * @returns {IntervalArray}
     *  The column/row intervals calculated from the current selection,
     *  containing hidden columns/rows. May be an empty array.
     */
    function getSelectedHiddenIntervals(columns) {

        // the entire selected intervals
        var intervals = getSelectedIntervals(columns);

        // safety check, should not happen
        if (intervals.empty()) { return intervals; }

        // the column/row collection
        var collection = columns ? colCollection : rowCollection;
        // the mixed column/row attributes
        var attributes = collection.getMixedAttributes(intervals);

        // if attribute 'visible' is false or null, the intervals contain hidden entries
        if (attributes.visible !== true) { return intervals; }

        // the resulting intervals
        var resultIntervals = new IntervalArray();

        // no special behavior for multiple visible intervals
        if (intervals.length > 1) { return resultIntervals; }

        // try to find preceding hidden interval
        var lastIndex = intervals.first().first - 1;
        if ((lastIndex >= 0) && !collection.isVisibleIndex(lastIndex)) {
            const prevIndex = collection.findPrevVisibleIndex(lastIndex);
            resultIntervals.push(new Interval(is.number(prevIndex) ? (prevIndex + 1) : 0, lastIndex));
            return resultIntervals;
        }

        // try to find following hidden interval
        var firstIndex = intervals[0].last + 1;
        if ((firstIndex <= collection.maxIndex) && !collection.isVisibleIndex(firstIndex)) {
            const nextIndex = collection.findNextVisibleIndex(firstIndex);
            resultIntervals.push(new Interval(firstIndex, is.number(nextIndex) ? (nextIndex - 1) : collection.maxIndex));
            return resultIntervals;
        }

        // no hidden intervals found
        return resultIntervals;
    }

    /**
     * Generates the operations to set the optimal column width in the
     * specified column intervals, based on the content of cells.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalArray|Interval} colIntervals
     *  An array of column intervals, or a single column interval.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    function generateOptimalColumnWidthOperations(generator, colIntervals) {

        // resulting intervals with the new column widths
        var resultIntervals = new IntervalArray();
        // default width for empty columns
        var defWidth = docModel.sheetAttrPool.getDefaultValue('column', 'width');
        // maximum width for columns
        var maxWidth = fontMetrics.getMaxColWidthHmm();
        // the column iterator
        var colIt = colCollection.indexEntries(colIntervals, { visible: true });

        // process all visible columns
        var promise = self.iterateSliced(colIt, colDesc => {

            // the new column width
            var colWidth = 0;

            const colRange = docModel.addressFactory.getColRange(colDesc.index);
            for (const renderModel of self.renderCache.yieldCellModels(colRange)) {
                const contentWidth = renderModel.getOptimalContentWidth();
                const mergedRange = renderModel.mergedData?.range;
                if (contentWidth && (!mergedRange || mergedRange.singleCol())) {
                    colWidth = Math.max(colWidth, contentWidth);
                }
            }

            // convert to 1/100 mm; or use default column width, if no cells are available
            colWidth = (colWidth === 0) ? defWidth : activeSheetModel.convertPixelToHmm(colWidth);

            // bug 40737: restrict to maximum column width allowed in the UI
            // TODO: use real maximum according to file format: 255 digits in OOXML, 1000mm in ODF
            colWidth = Math.min(colWidth, maxWidth);

            // do not generate operations, if the custom width was not modified before, and the new width is inside the tolerance
            if (!colDesc.merged.customWidth && (Math.abs(colWidth - colDesc.merged.width) < SET_COLUMN_WIDTH_TOLERANCE)) {
                return;
            }

            // insert the optimal column width into the cache
            var lastInterval = resultIntervals.last();
            if (lastInterval && (lastInterval.last + 1 === colDesc.index) && (lastInterval.fillData.attrs.width === colWidth)) {
                lastInterval.last += 1;
            } else {
                lastInterval = new Interval(colDesc.index);
                lastInterval.fillData = { attrs: { width: colWidth, customWidth: false } };
                resultIntervals.push(lastInterval);
            }
        });

        // create the operations for all columns
        return promise.then(function () {
            return colCollection.generateIntervalOperations(generator, resultIntervals);
        });
    }

    /**
     * Generates row operations to set the optimal row height in the
     * specified row intervals, based on the content of cells.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {IntervalSource} rowIntervals
     *  An array of row intervals, or a single row interval.
     *
     * @param {RangeSource} [changedRanges]
     *  If specified, an operation has caused updating the automatic row
     *  heights implicitly. Rows with user-defined height (row attribute
     *  "customHeight" set to true) will not be changed in this case. By
     *  default, the optimal height of all rows in the passed intervals
     *  will be calculated, and all manual row heights will be reset to
     *  automatic mode.
     *
     * @returns {JPromise<IntervalArray>}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    function generateOptimalRowHeightOperations(generator, rowIntervals, changedRanges) {

        // resulting intervals with the new row heights
        var resultIntervals = new IntervalArray();
        // maximum height for rows
        var maxHeightHmm = fontMetrics.getMaxRowHeightHmm();
        // tolerance for automatic row height
        var heightTolerance = changedRanges ? UPDATE_ROW_HEIGHT_TOLERANCE : SET_ROW_HEIGHT_TOLERANCE;
        // current zoom factor
        var zoom = activeSheetModel.getZoom();
        // the row iterator
        var rowIt = rowCollection.indexEntries(rowIntervals, { visible: true });

        // do not change custom row height in update mode
        if (changedRanges) {
            rowIt = itr.filter(rowIt, rowDesc => !rowDesc.merged.customHeight);
        }

        // process all visible rows
        var promise = self.iterateSliced(rowIt, rowDesc => {

            // calculate resulting optimal height of the current row
            var rowHeightHmm = fontMetrics.getRowHeightHmm(rowDesc.attributes.character);

            // TODO: iterate all columns in the row
            const rowRange = docModel.addressFactory.getRowRange(rowDesc.index);
            for (const renderModel of self.renderCache.yieldCellModels(rowRange)) {

                // the address of the current cell
                const { address } = renderModel;

                // recalculate cell height if current cell has changed
                if (changedRanges?.containsAddress(address)) {

                    const { cellModel } = renderModel;
                    if (!cellModel || !cellModel.d) { continue; }

                    var attrSet = cellCollection.getAttributeSet(address);
                    var currentRowHeight = fontMetrics.getRowHeightHmm(attrSet.character);
                    var lines = 0;

                    if (hasWrappingAttributes(attrSet)) {
                        var fontDesc = docModel.getRenderFont(attrSet.character, zoom);
                        var cellWidth = activeSheetModel.getCellRectangle(address, { pixel: true }).width;
                        var totalPadding = activeSheetModel.getTotalCellPadding(attrSet.character);
                        var availableWidth = Math.max(2, cellWidth - totalPadding);
                        for (const paragraph of splitDisplayString(cellModel.d)) {
                            lines += fontDesc.getTextLines(paragraph, availableWidth).length;
                        }
                    } else {
                        lines = 1;
                    }

                    rowHeightHmm = Math.max(rowHeightHmm, lines * currentRowHeight);
                    continue;
                }

                // use effective cell content height from rendering cache
                const contentHeight = renderModel.getOptimalContentHeight();
                const mergedRange = renderModel.mergedData?.range;
                if (contentHeight && (!mergedRange || mergedRange.singleRow())) {
                    rowHeightHmm = Math.max(rowHeightHmm, activeSheetModel.convertPixelToHmm(contentHeight));
                }
            }

            // bug 40737: restrict to maximum row height allowed in the UI
            rowHeightHmm = Math.min(rowHeightHmm, maxHeightHmm);

            // do not generate operations, if the row height was optimal before, and the new height is inside the tolerance
            if (!rowDesc.merged.customHeight) {
                var oldHeightHmm = fontMetrics.convertRowHeightFromUnit(rowDesc.merged.height);
                if (Math.abs(rowHeightHmm - oldHeightHmm) < heightTolerance) {
                    return;
                }
            }

            // insert the optimal row height into the cache
            var lastInterval = resultIntervals.last();
            if (lastInterval && (lastInterval.last + 1 === rowDesc.index) && (lastInterval.fillData.attrs.height === rowHeightHmm)) {
                lastInterval.last += 1;
            } else {
                lastInterval = new Interval(rowDesc.index);
                lastInterval.fillData = { attrs: { height: rowHeightHmm, customHeight: false } };
                resultIntervals.push(lastInterval);
            }
        });

        // create the operations for all rows
        return jpromise.fastThen(promise, function () {
            return rowCollection.generateIntervalOperations(generator, resultIntervals);
        });
    }

    /**
     * Inserts new columns or rows into the active sheet, according to the
     * current selection.
     *
     * @param {Boolean} columns
     *  Whether to insert columns (true), or rows (false).
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    function insertIntervals(columns) {

        // check that the sheet is unlocked
        var promise = self.ensureUnlockedSheet();

        // resolve the selected intervals
        promise = promise.then(function () {
            var intervals = getSelectedIntervals(columns, { strict: true, distinct: true });
            return intervals.empty() ? makeRejected(columns ? 'cols:selection' : 'rows:selection') : intervals;
        });

        // generate and apply the operations
        promise = promise.then(function (intervals) {
            return docModel.buildOperations(function (builder) {
                var direction = columns ? Direction.RIGHT : Direction.DOWN;
                return activeSheetModel.generateMoveOperations(builder, intervals, direction);
            }, { storeSelection: true });
        });

        // show warning messages if needed
        return self.yellOnFailure(promise);
    }

    /**
     * Deletes existing columns or rows from the active sheet, according to
     * the current selection.
     *
     * @param {Boolean} columns
     *  Whether to delete columns (true), or rows (false).
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    function deleteIntervals(columns) {

        // the intervals to be deleted
        var intervals = null;
        // the undo mode to be passed to the generator code
        var undoMode = null;

        // check that the sheet is unlocked
        var promise = self.ensureUnlockedSheet();

        // resolve the selected intervals
        promise = promise.then(function () {
            intervals = getSelectedIntervals(columns, { strict: true });
            if (intervals.empty()) { return makeRejected(columns ? 'cols:selection' : 'rows:selection'); }
        });

        // ask to proceed if the column/row ranges have contents that cannot be restored
        promise = promise.then(function () {

            // nothing to do if everything in the deleted intervals can be restored
            if (activeSheetModel.canRestoreDeletedRanges(docModel.addressFactory.getFullRangeList(intervals, columns))) { return; }

            // clear existing undo actions
            undoMode = 'clear';

            // show the query dialog
            var title = columns ? DELETE_COLUMNS_TITLE : DELETE_ROWS_TITLE;
            var message = columns ? DELETE_COLUMNS_QUERY : DELETE_ROWS_QUERY;
            return self.showQueryDialog(title, message, {
                // close automatically, if active sheet will be deleted or hidden by another client
                autoCloseScope: 'activeSheet'
            });
        });

        // generate and apply the operations
        promise = promise.then(function () {
            return docModel.buildOperations(function (builder) {
                var direction = columns ? Direction.LEFT : Direction.UP;
                return activeSheetModel.generateMoveOperations(builder, intervals, direction);
            }, { storeSelection: true, undoMode });
        });

        // show warning messages if needed
        return self.yellOnFailure(promise);
    }

    /**
     * Toggles the visibility of the columns/rows in the current selection.
     *
     * @param {boolean} columns
     *  Whether to show hidden columns (`true`), or rows (`false`).
     *
     * @param {boolean} visible
     *  Whether to show (`true`) or hide (`false`) the selected columns/rows.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the operations have been generated.
     */
    function toggleIntervals(columns, visible) {

        // check that the sheet is unlocked
        var promise = self.ensureUnlockedSheet();

        // generate and apply the operations
        promise = promise.then(() => {
            return activeSheetModel.createAndApplyOperations(generator => {
                var collection = columns ? colCollection : rowCollection;
                var intervals = visible ? getSelectedHiddenIntervals(columns) : getSelectedIntervals(columns);
                return collection.generateFillOperations(generator, intervals, { attrs: { visible } });
            }, { applyImmediately: true, storeSelection: true });
        });

        // show warning messages if needed
        return self.yellOnFailure(promise);
    }

    /**
     * Performs additional checks whether the contents of the passed cell
     * ranges can be edited, regardless of their 'unlocked' attribute, and
     * the lock state of the active sheet. Used as helper method by various
     * public methods of this class that all support the same options.
     *
     * @param {RangeArray|Range} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {String} [options.lockTables='none']
     *      Specifies how to check table ranges covering the passed cell
     *      range addresses. See description of the method
     *      ViewFuncMixin.ensureUnlockedRanges() for more details.
     *  - {String} [options.lockMatrixes='none']
     *      Specifies how to check matrix formulas covering the passed cell
     *      ranges. See method ViewFuncMixin.ensureUnlockedRanges() for
     *      more details.
     *
     * @returns {String|Null}
     *  The value null, if the contents of the passed cell ranges can be
     *  edited; otherwise one of the following error codes:
     *  - 'table:change': If the option 'lockTables' has been set to
     *      'full', and the passed cell ranges overlap with a table range
     *      in the active sheet.
     *  - 'table:header:change': If the option 'lockTables' has been set to
     *      'header', and the passed cell ranges overlap with the header
     *      cells of a table range in the active sheet.
     *  - 'formula:matrix:change': If the option 'lockMatrixes' has been
     *      set, and the passed cell ranges overlap with a matrix formula
     *      in the active sheet.
     */
    function ensureEditableContents(ranges, options) {

        // check the table ranges according to the passed mode
        switch (options && options.lockTables) {

            case 'full':
                // skip auto-filter (always editable)
                if (itr.some(tableCollection.yieldTableModels(), tableModel => ranges.overlaps(tableModel.getRange()))) {
                    return 'table:change';
                }
                break;

            case 'header':
                // skip auto-filter (always editable)
                if (itr.some(tableCollection.yieldTableModels(), tableModel => {
                    var headerRange = tableModel.getHeaderRange();
                    return headerRange && ranges.overlaps(headerRange);
                })) {
                    return 'table:header:change';
                }
                break;
        }

        // DOCS-196: check the matrix formulas in the active sheet
        switch (options && options.lockMatrixes) {

            case 'full':
                if (cellCollection.coversAnyMatrixRange(ranges)) {
                    return 'formula:matrix:change';
                }
                break;

            case 'partial':
                if (cellCollection.coversAnyMatrixRange(ranges, FindMatchType.PARTIAL)) {
                    return 'formula:matrix:change';
                }
                break;
        }

        // the passed ranges are considered to be editable
        return null;
    }

    /**
     * A little helper function for various public methods that return a
     * promise after checking various conditions in the active sheet.
     *
     * @param {Null|String} errorCode
     *  The value null to indicate a successful check; or an error code as
     *  string, intended to be passed to `SpreadsheetView::yellMessage` or
     *  `SpreadsheetView::yellOnFailure`.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Boolean} [options.sync=false]
     *      If set to true, this method will return the passed error code
     *      directly instead of a promise.
     *
     * @returns {Promise|Null|String}
     *  A resolved promise, if the passed error code is null, or a promise
     *  that will be rejected with an object with 'cause' property set to
     *  the passed error code. If the option 'sync' has been set to true,
     *  the error code will be returned directly instead of a promise.
     */
    function createEnsureResult(errorCode, options) {

        // if 'sync' option is set, return the result directly
        if (options && options.sync) { return errorCode; }

        // create the resulting (resolved or rejected) promise
        return errorCode ? makeRejected(errorCode) : self.createResolvedPromise(null);
    }

    /**
     * Generates and applies the operations, and undo operations, to set
     * the contents and formatting of a cell.
     *
     * @param {Address} address
     *  The address of the cell to be modified.
     *
     * @param {CellChangeSet} changeSet
     *  The new properties to be applied at the cell.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully, or that will reject on any error.
     */
    function implChangeCell(address, changeSet, options) {

        // generate and apply the operations
        return activeSheetModel.createAndApplyOperations(function (generator) {

            // create the `changeCells` operations
            var promise = cellCollection.generateCellOperations(generator, address, changeSet);

            // set optimal row height afterwards (for changed rows only)
            return jpromise.fastThen(promise, function (changedRanges) {
                return self.generateUpdateRowHeightOperations(generator, changedRanges);
            });
        }, { applyImmediately: true, storeSelection: true, undoSelectionState: options && options.selection });
    }

    /**
     * Generates and applies the operations, and undo operations, to set
     * the contents and formatting of multiple independent cells.
     *
     * @param {AddressChangeSet[]} changeSets
     *  The new properties be applied at the cells, and their addresses.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully, or that will reject on any error.
     */
    function implChangeCells(changeSets, options) {

        // generate and apply the operations
        return activeSheetModel.createAndApplyOperations(function (generator) {

            // create the `changeCells` operations
            var promise = cellCollection.generateCellsOperations(generator, changeSets);

            // set optimal row height afterwards (for changed rows only)
            promise = promise.then(function (changedRanges) {
                return self.generateUpdateRowHeightOperations(generator, changedRanges);
            });

            // select the range passed with the options
            if (options?.selectRange) {
                promise.done(function () { self.selectionEngine.selectRange(options.selectRange); });
            }

            return promise;

        }, { applyImmediately: true, storeSelection: true, undoSelectionState: options && options.selection });
    }

    /**
     * Generates and applies the operations, and undo operations, to set
     * the contents of a cell to the passed edit text (tries to convert to
     * an appropriate data type, or to calculate a formula string).
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully, or that will reject on any error.
     */
    function implParseCellText(address, parseText, options) {

        // parse the passed text to a cell contents object
        var parseResult = cellCollection.parseCellValue(parseText, 'val', address);

        // if specified, ignore a recognized URL for a cell hyperlink
        if (options && options.skipHyperlink) {
            delete parseResult.url;
        }

        // create and apply the cell operation, update optimal row height
        var changeSet = _.pick(parseResult, 'v', 'f', 'format', 'url');
        return implChangeCell(address, changeSet, options);
    }

    /**
     * Generates and applies the operations, and undo operations to fill
     * all cell ranges with the same value and formatting, and to update
     * the optimal row height of the affected cells.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the cell has been changed, or
     *  that will be rejected on any error.
     */
    function implFillRanges(ranges, changeSet, options) {

        // generate and apply the operations
        return activeSheetModel.createAndApplyOperations(function (generator) {

            // try to fill the cell ranges
            var promise = cellCollection.generateFillOperations(generator, ranges, changeSet, options);

            // delete all cell notes and comments if specified
            if (options && options.deleteComments) {
                promise = promise.then(function (changedRanges) {
                    return activeSheetModel.noteCollection.generateDeleteAllNotesOperations(generator, ranges).then(() => changedRanges);
                });

                promise = promise.then(function (changedRanges) {
                    return activeSheetModel.commentCollection.generateDeleteAllCommentsOperations(generator, ranges).then(() => changedRanges);
                });
            }

            // set optimal row height afterwards (for changed rows only)
            promise = promise.then(function (changedRanges) {
                return self.generateUpdateRowHeightOperations(generator, changedRanges);
            });

            return promise;
        }, { applyImmediately: true, storeSelection: true, undoSelectionState: options && options.selection });
    }

    /**
     * Initializes all sheet-dependent class members according to the
     * current active sheet.
     */
    function changeActiveSheetHandler(_sheet, sheetModel) {

        // store model instance and index of the new active sheet
        activeSheetModel = sheetModel;

        // get collections of the new active sheet
        colCollection = sheetModel.colCollection;
        rowCollection = sheetModel.rowCollection;
        mergeCollection = sheetModel.mergeCollection;
        cellCollection = sheetModel.cellCollection;
        tableCollection = sheetModel.tableCollection;

        // forced update of selection settings (do not try to use cached data from old sheet)
        updateSelectionSettings(true);
    }

    // public methods -----------------------------------------------------

    /**
     * Returns the subtotal results in the current selection.
     *
     * @returns {SubtotalResult}
     *  The subtotal results in the current selection.
     */
    this.getSubtotalResult = function () {
        return selectedSubtotals;
    };

    /**
     * Returns whether the active sheet is locked. In locked sheets it is
     * not possible to insert, delete, or modify columns and rows, to edit
     * protected cells, or to manipulate drawing objects.
     *
     * @returns {Boolean}
     *  Whether the active sheet is locked.
     */
    this.isSheetLocked = function () {
        return activeSheetModel.isLocked();
    };

    /**
     * Returns a resolved or rejected promise according to the locked state
     * of the sheet.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {String} [options.errorCode='sheet:locked']
     *      The message code specifying the warning message shown when the
     *      sheet is locked. See method `SpreadsheetView::yellMessage` for
     *      details.
     *  - {Boolean} [options.sync=false]
     *      If set to true, this method will be executed synchronously, and
     *      the return value will be null or an error code instead of a
     *      promise.
     *
     * @returns {jQuery.Promise|Null|String}
     *  A promise that will be resolved, if the active sheet is not locked;
     *  or that will be rejected with an object with 'cause' property set
     *  to one of the following error codes:
     *  - 'readonly': The entire document is in read-only state.
     *  - passed error code or 'sheet:locked': The active sheet is locked.
     *  If the option 'sync' has been set to true, the return value will be
     *  null if the sheet is not locked, otherwise one of the error codes
     *  mentioned above as string.
     */
    this.ensureUnlockedSheet = function (options) {

        // prerequisite is global document edit mode
        var errorCode = this.isEditable() ? null : 'readonly';

        // return the specified error code, if the sheet is locked
        if (!errorCode && this.isSheetLocked()) {
            errorCode = getStringOption(options, 'errorCode', 'sheet:locked');
        }

        // if 'sync' option is set, return the result directly
        return createEnsureResult(errorCode, options);
    };

    /**
     * Returns a resolved or rejected promise according to the locked state
     * of the cells in the passed cell ranges. A cell is in locked state,
     * if its 'unlocked' attribute is not set, AND if the active sheet is
     * locked; or if the cell is part of another locked sheet structure,
     * according to the passed options.
     *
     * @param {RangeArray|Range} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {String} [options.errorCode='cells:locked']
     *      The message code specifying the warning message shown when the
     *      ranges are locked. See method `SpreadsheetView::yellMessage` for
     *      details.
     *  - {String} [options.lockTables='none']
     *      Specifies how to check a table range covering the cell ranges
     *      (the auto-filter, however, is never considered to be locked):
     *      - 'none' (default): Table ranges are not locked per se.
     *      - 'full': The entire table range is locked.
     *      - 'header': Only the header cells of the table are locked.
     *  - {String} [options.lockMatrixes='none']
     *      Specifies how to check the matrix formulas covering the passed
     *      cell ranges:
     *      - 'none' (default): Matrix formulas are not locked per se.
     *      - 'full': All matrix formulas are locked.
     *      - 'partial': Only matrix formulas covered partially are locked.
     *  - {Boolean} [options.sync=false]
     *      If set to true, this method will be executed synchronously, and
     *      the return value will be null or an error code instead of a
     *      promise.
     *
     * @returns {jQuery.Promise|Null|String}
     *  A resolved promise, if the active sheet is not locked, or if the
     *  cell ranges are not locked; otherwise (i.e. active sheet, AND the
     *  cell ranges are locked) a rejected promise with a specific error
     *  code. If the option 'sync' has been set to true, the return value
     *  will be null if the cell ranges are editable, otherwise an error
     *  code as string.
     */
    this.ensureUnlockedRanges = function (ranges, options) {

        // prerequisite is global document edit mode
        var errorCode = this.isEditable() ? null : 'readonly';

        // try to find a locked cell in a locked sheet
        if (!errorCode && this.isSheetLocked()) {

            // shortcut: use cached settings of the active cell, otherwise search in cell collection
            ranges = RangeArray.cast(ranges);
            var isActiveCell = (ranges.length === 1) && ranges.first().single() && ranges.first().startsAt(activeAddress);
            var isLocked = isActiveCell ? !activeAttributeSet.cell.unlocked : cellCollection.findCellWithAttributes(ranges, { cell: { unlocked: false } });

            // extract the actual error code from the passed options
            if (isLocked) { errorCode = (options && options.errorCode) || 'cells:locked'; }
        }

        // bug 39869: do not allow to change table ranges (according to passed options)
        if (!errorCode) { errorCode = ensureEditableContents(ranges, options); }

        // if 'sync' option is set, return the result directly
        return createEnsureResult(errorCode, options);
    };

    /**
     * Returns a resolved or rejected promise according to the locked state
     * of the specified cell in the active sheet. The cell is in locked
     * state, if its 'unlocked' attribute is not set, AND if the active
     * sheet is locked; or if the cell is part of another locked sheet
     * structure, according to the passed options.
     *
     * @param {Address} address
     *  The address of cell to be checked.
     *
     * @param {Object} [options]
     *  Optional parameters. See public method `ensureUnlockedRanges()` for
     *  details.
     *
     * @returns {jQuery.Promise|Null|String}
     *  A resolved promise, if the active sheet is not locked, or if the
     *  active cell in a locked sheet is not locked; otherwise (i.e. active
     *  sheet, AND active cell are locked) a rejected promise with a
     *  specific error code. If the option 'sync' has been set to true, the
     *  return value will be null if the active cell is editable, otherwise
     *  an error code as string.
     */
    this.ensureUnlockedCell = function (address, options) {
        return this.ensureUnlockedRanges(new Range(address), options);
    };

    /**
     * Returns a resolved or rejected promise according to the locked state
     * of the cell ranges in the current selection. A cell is in locked
     * state, if its 'unlocked' attribute is not set, AND if the active
     * sheet is locked; or if it is part of another locked sheet structure,
     * according to the passed options.
     *
     * @param {Object} [options]
     *  Optional parameters. See public method `ensureUnlockedRanges()` for
     *  details.
     *
     * @returns {jQuery.Promise|Null|String}
     *  A resolved promise, if the active sheet is not locked, or if the
     *  selected cell ranges are not locked; otherwise (i.e. active sheet,
     *  AND selected ranges are locked) a rejected promise with a specific
     *  error code. If the option 'sync' has been set to true, the return
     *  value will be null if the selected ranges are editable, otherwise
     *  an error code as string.
     */
    this.ensureUnlockedSelection = function (options) {
        return this.ensureUnlockedRanges(this.selectionEngine.getSelectedRanges(), options);
    };

    /**
     * Returns a resolved or rejected promise according to the locked state
     * of the active cell in the current selection. The active cell is in
     * locked state, if its 'unlocked' attribute is not set, AND if the
     * active sheet is locked; or if the cell is part of another locked
     * sheet structure, according to the passed options.
     *
     * @param {Object} [options]
     *  Optional parameters. See public method `ensureUnlockedRanges()` for
     *  details. The default value of the option 'lockTables' will be
     *  changed to 'header' though!
     *
     * @returns {jQuery.Promise|Null|String}
     *  A resolved promise, if the active sheet is not locked, or if the
     *  active cell in a locked sheet is not locked; otherwise (i.e. active
     *  sheet, AND active cell are locked) a rejected promise with a
     *  specific error code. If the option 'sync' has been set to true, the
     *  return value will be null if the active cell is editable, otherwise
     *  an error code as string.
     */
    this.ensureUnlockedActiveCell = function (options) {
        options = { lockTables: 'header', ...options };
        return this.ensureUnlockedRanges(new Range(activeAddress), options);
    };

    /**
     * Returns a resolved or rejected promise according to the editable
     * state of drawing objects in the active sheet.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {String} [options.errorCode='drawing:change:locked']
     *      The message code specifying the warning message shown when the
     *      drawings in the active sheet are locked. See method
     *      `SpreadsheetView::yellMessage` for details.
     *  - {Boolean} [options.sync=false]
     *      If set to true, this method will be executed synchronously, and
     *      the return value will be null or an error code instead of a
     *      promise.
     *
     * @returns {jQuery.Promise|Null|String}
     *  A promise that will be resolved, if the drawing objects in the
     *  active sheet are not locked; or that will be rejected with an
     *  object with 'cause' property set to one of the following error
     *  codes:
     *  - 'readonly': The entire document is in read-only state.
     *  - (the passed error code): The active sheet is locked.
     *  If the option 'sync' has been set to true, the return value will be
     *  null if the drawing objects in the sheet are not locked, otherwise
     *  one of the error codes mentioned above as string.
     */
    this.ensureUnlockedDrawings = function (options) {
        return this.ensureUnlockedSheet({ errorCode: 'drawing:change:locked', ...options });
    };

    /**
     * Returns a resolved or rejected promise according to the editable
     * state of cell notes in the active sheet.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {string} [options.errorCode="note:change:locked"]
     *    The message code specifying the warning message shown when the
     *    notes in the active sheet are locked. See method
     *    `SpreadsheetView::yellMessage` for details.
     *  - {boolean} [options.sync=false]
     *    If set to `true`, this method will be executed synchronously, and
     *    the return value will be `null` or an error code instead of a
     *    promise.
     *
     * @returns {JPromise|null|string}
     *  A promise that will fulfil, if the cell notes in the active sheet
     *  are not locked; or that will reject with an object with "cause"
     *  property set to one of the following error codes:
     *  - "readonly": The entire document is in read-only state.
     *  - (the passed error code): The active sheet is locked.
     *  If the option "sync" has been set to `true`, the return value will
     *  be `null` if the cell notes in the sheet are not locked, otherwise
     *  one of the error codes mentioned above as string.
     */
    this.ensureUnlockedNotes = function (options) {
        return this.ensureUnlockedSheet({ errorCode: 'note:change:locked', ...options });
    };

    // column operations --------------------------------------------------

    /**
     * Returns the mixed formatting attributes of all columns in the active
     * sheet covered by the current selection.
     *
     * @returns {Object}
     *  The mixed attributes of all selected columns, as simple object. All
     *  attributes that could not be resolved unambiguously will be set to
     *  the value null.
     */
    this.getColumnAttributes = function () {
        var intervals = getSelectedIntervals(true);
        return colCollection.getMixedAttributes(intervals);
    };

    /**
     * Returns the settings of the column the active cell is currently
     * located in.
     *
     * @returns {ColRowDescriptor}
     *  The column descriptor of the active cell.
     */
    this.getActiveColumnDescriptor = function () {
        return colCollection.getEntry(activeAddress.c);
    };

    /**
     * Returns whether the current selection contains hidden columns, or is
     * located next to hidden columns, that can be made visible.
     *
     * @returns {Boolean}
     *  Whether the current selection contains hidden columns.
     */
    this.canShowColumns = function () {
        return getSelectedHiddenIntervals(true).length > 0;
    };

    /**
     * Inserts new columns into the active sheet, according to the current
     * selection.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.insertColumns = function () {
        return insertIntervals(true);
    };

    /**
     * Deletes existing columns from the active sheet, according to the
     * current selection.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.deleteColumns = function () {
        return deleteIntervals(true);
    };

    /**
     * Toggles the visibility of the columns in the current selection.
     *
     * @param {boolean} visible
     *  Whether to show (`true`) or hide (`false`) the selected columns.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully, or that will be rejected on any error.
     */
    this.toggleColumns = function (visible) {
        return toggleIntervals(true, visible);
    };

    /**
     * Changes the width of columns in the active sheet, according to the
     * current selection.
     *
     * @param {Number} width
     *  The column width in 1/100 mm. If this value is less than 1, the
     *  columns will be hidden, and their original width will not be
     *  changed.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Number} [options.target]
     *      The target column to be changed. If specified, the column width
     *      will be set to that column only. If the current selection
     *      contains any ranges covering this column completely, the column
     *      width will be set to all columns contained in these ranges.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setColumnWidth = function (width, options) {

        var promise = this.ensureUnlockedSheet().then(function () {
            return activeSheetModel.createAndApplyOperations(function (generator) {

                // the target column to be modified
                var targetCol = getIntegerOption(options, 'target');
                // the column intervals to be modified
                var intervals = getSelectedIntervals(true, { target: targetCol });
                // hide columns if passed column width is zero; show hidden columns when width is changed
                var attributes = (width === 0) ? { visible: false } : { visible: true, width, customWidth: true };

                return colCollection.generateFillOperations(generator, intervals, { attrs: attributes }, { skipFiltered: true });
            }, { applyImmediately: true, storeSelection: true });
        });

        return this.yellOnFailure(promise);
    };

    /**
     * Sets optimal column width based on the content of cells.
     *
     * @param {Number} [targetCol]
     *  The target column to be changed. If the current selection contains
     *  any ranges covering this column completely, the optimal column
     *  width will be set to all columns contained in these ranges.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setOptimalColumnWidth = function (targetCol) {

        // generate and apply the column operations
        var promise = activeSheetModel.createAndApplyOperations(function (generator) {
            var intervals = getSelectedIntervals(true, { target: targetCol, visible: true });
            return generateOptimalColumnWidthOperations(generator, intervals);
        }, { applyImmediately: true, storeSelection: true });

        // show error alert on failure
        return this.yellOnFailure(promise);
    };

    // row operations -----------------------------------------------------

    /**
     * Returns the mixed formatting attributes of all rows in the active
     * sheet covered by the current selection.
     *
     * @returns {Object}
     *  The mixed attributes of all selected rows, as simple object. All
     *  attributes that could not be resolved unambiguously will be set to
     *  the value null.
     */
    this.getRowAttributes = function () {
        var intervals = getSelectedIntervals(false);
        return rowCollection.getMixedAttributes(intervals);
    };

    /**
     * Returns the settings of the row the active cell is currently located
     * in.
     *
     * @returns {ColRowDescriptor}
     *  The row descriptor of the active cell.
     */
    this.getActiveRowDescriptor = function () {
        return rowCollection.getEntry(activeAddress.r);
    };

    /**
     * Returns whether the current selection contains hidden rows, or is
     * located next to hidden rows, that can be made visible.
     *
     * @returns {Boolean}
     *  Whether the current selection contains hidden rows.
     */
    this.canShowRows = function () {
        return getSelectedHiddenIntervals(false).length > 0;
    };

    /**
     * Inserts new rows into the active sheet, according to the current
     * selection.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.insertRows = function () {
        return insertIntervals(false);
    };

    /**
     * Deletes existing rows from the active sheet, according to the
     * current selection.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.deleteRows = function () {
        return deleteIntervals(false);
    };

    /**
     * Toggles the visibility of the rows in the current selection.
     *
     * @param {boolean} visible
     *  Whether to show (`true`) or hide (`false`) the selected rows.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully, or that will be rejected on any error.
     */
    this.toggleRows = function (visible) {
        return toggleIntervals(false, visible);
    };

    /**
     * Changes the height of rows in the active sheet, according to the
     * current selection.
     *
     * @param {Number} height
     *  The row height in 1/100 mm. If this value is less than 1, the rows
     *  will be hidden, and their original height will not be changed.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Number} [options.target]
     *      The target row to be changed. If specified, the row height will
     *      be set to that row only. If the current selection contains any
     *      ranges covering this row completely, the row height will be set
     *      to all rows contained in these ranges.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setRowHeight = function (height, options) {

        var promise = this.ensureUnlockedSheet().then(function () {
            return activeSheetModel.createAndApplyOperations(function (generator) {

                // the target row to be modified
                var targetRow = getIntegerOption(options, 'target');
                // the row intervals to be modified
                var intervals = getSelectedIntervals(false, { target: targetRow });
                // hide rows if passed row height is zero; show hidden rows when height is changed
                var attributes = (height === 0) ? { visible: false } : { visible: true, height, customHeight: true };

                return rowCollection.generateFillOperations(generator, intervals, { attrs: attributes }, { skipFiltered: true });
            }, { applyImmediately: true, storeSelection: true });
        });

        return this.yellOnFailure(promise);
    };

    /**
     * Sets optimal row height based on the content of cells.
     *
     * @param {Number} [targetRow]
     *  The target row to be changed. If the current selection contains any
     *  ranges covering this row completely, the optimal row height will be
     *  set to all rows contained in these ranges.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setOptimalRowHeight = function (targetRow) {

        // generate and apply the row operations
        var promise = activeSheetModel.createAndApplyOperations(function (generator) {
            var intervals = getSelectedIntervals(false, { target: targetRow, visible: true });
            return generateOptimalRowHeightOperations(generator, intervals);
        }, { applyImmediately: true, storeSelection: true });

        // show error alert on failure
        return this.yellOnFailure(promise);
    };

    /**
     * Generates row operations to update the optimal row height in the
     * specified changed cell ranges, based on the content of cells.
     *
     * @param {SheetOperationGenerator} generator
     *  The operations generator to be filled with the operations.
     *
     * @param {RangeSource} ranges
     *  An array of range addresses, or a single range address.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.generateUpdateRowHeightOperations = function (generator, ranges) {

        // the merged and sorted row intervals
        var rowIntervals = RangeArray.cast(ranges).rowIntervals().merge();

        // restrict to visible area (performance)
        // TODO: iterate all row intervals, not single rows, important when updating entire column
        rowIntervals = this.getVisibleIntervals(rowIntervals, false);

        // set update mode (do not modify rows with custom height)
        return generateOptimalRowHeightOperations(generator, rowIntervals, ranges);
    };

    // cell operations ----------------------------------------------------

    /**
     * Returns the value of the active cell in the current selection.
     *
     * @returns {ScalarType}
     *  The value of the active cell.
     */
    this.getActiveCellValue = function () {
        return activeCellValue;
    };

    /**
     * Returns the parsed number format of the active cell in the current
     * selection.
     *
     * @returns {ParsedFormat}
     *  The parsed number format of the active cell.
     */
    this.getActiveParsedFormat = function () {
        return activeParsedFormat;
    };

    /**
     * Returns the URL of a hyperlink at the current active cell.
     *
     * @returns {string|null}
     *  The URL of a hyperlink attached to the active cell; or null, if the
     *  active cell does not contain a hyperlink.
     */
    this.getCellURL = function () {
        return activeSheetModel.hlinkCollection.getCellURL(activeAddress);
    };

    /**
     * Returns the URL of a hyperlink as returned by a formula (via the
     * function HYPERLINK) of the current active cell.
     *
     * @returns {string|null}
     *  The URL of a hyperlink as returned by a cell formula (via the
     *  function HYPERLINK) of the current active cell.
     */
    this.getFormulaURL = function () {
        return cellCollection.getFormulaURL(activeAddress);
    };

    /**
     * Returns the effective URL of a hyperlink at the current active cell.
     * If the cell contains a regular hyperlink, and a cell formula with a
     * HYPERLINK function, the regular hyperlink will be preferred.
     *
     * @returns {string|null}
     *  The effective URL of a hyperlink at the active cell.
     */
    this.getEffectiveURL = function () {
        return cellCollection.getEffectiveURL(activeAddress);
    };

    /**
     * Sets the contents of the specified cell to an edit text. Tries to
     * convert to an appropriate data type, or to calculate a formula
     * string, and updates the view according to the new cell value and
     * formatting.
     *
     * @param {String} parseText
     *  The raw text to be parsed. May result in a number, a boolean value,
     *  an error code, or a formula expression whose result will be
     *  calculated. The empty string will remove the current cell value
     *  (will create a blank cell).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Address} [options.address]
     *    The address of the cell to be changed. If omitted, the address of
     *    the active cell will be used.
     *  - {SelectionState} [options.selection]
     *    The selection state to be inserted into the undo stack. If
     *    omitted, the current selection will be used.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully.
     */
    this.parseCellText = function (parseText, options) {

        // fall-back to active address
        var address = (options && options.address) || activeAddress;

        // do nothing if the specified cell is locked
        var promise = this.ensureUnlockedCell(address, { lockTables: 'header', lockMatrixes: 'partial' });

        // create and apply the operations
        promise = jpromise.fastThen(promise, function () {
            return implParseCellText(address, parseText, { selection: options && options.selection });
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Changes the contents and/or formatting of the specified cell, and
     * updates the view according to the new cell value and formatting.
     *
     * @param {CellChangeSet} changeSet
     *  The new properties to be applied at the cell.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Address} [options.address]
     *    The address of the cell to be changed. If omitted, the address of
     *    the active cell will be used.
     *  - {SelectionState} [options.selection]
     *    The selection state to be inserted into the undo stack. If
     *    omitted, the current selection will be used.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully.
     */
    this.changeCell = function (changeSet, options) {

        // fall-back to active address
        var address = (options && options.address) || activeAddress;

        // do nothing if the specified cell is locked (not for formatting-only operations)
        var changeValues = (changeSet.u === true) || ('v' in changeSet) || ('f' in changeSet);
        var promise = this.ensureUnlockedCell(address, changeValues ? { lockTables: 'header', lockMatrixes: 'partial' } : null);

        // create and apply the operations
        promise = jpromise.fastThen(promise, function () {
            return implChangeCell(address, changeSet, options);
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Generates the cell operations, and the undo operations, to change
     * multiple cells in the active sheet.
     *
     * @param {AddressChangeSet[]} changeSets
     *  A list of change sets for all cells to be changed, including the
     *  addresses of the cells.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {SelectionState} [options.selection]
     *    The selection state to be inserted into the undo stack. If
     *    omitted, the current selection will be used.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the generated operations have been
     *  applied successfully.
     */
    this.changeCells = function (changeSets, options) {

        // convert all cell addresses to a merged range array
        var ranges = RangeArray.mergeAddresses(changeSets, changeSet => changeSet.address);

        // do nothing if the cell ranges contain locked cells
        var promise = this.ensureUnlockedRanges(ranges, { lockTables: 'header', lockMatrixes: 'partial' });

        // create and apply the operations
        promise = jpromise.fastThen(promise, function () {
            return implChangeCells(changeSets, options);
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Changes the contents and/or formatting of all cells in the specified
     * cell ranges, and updates the view according to the new cell values
     * and formatting.
     *
     * @param {CellChangeSet} changeSet
     *  The new properties to be applied at all cells.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {RangeArray|Range} [options.ranges]
     *    The addresses of the cell ranges to be changed. If omitted, the
     *    current selection will be used.
     *  - {SelectionState} [options.selection]
     *    The selection state to be inserted into the undo stack. If
     *    omitted, the current selection will be used.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.fillCells = function (changeSet, options) {

        // fall-back to current selection
        var ranges = (options && options.ranges) || this.selectionEngine.getSelectedRanges();

        // do nothing if current selection contains locked cells (do not allow to change table header texts)
        var changeValues = (changeSet.u === true) || ('v' in changeSet) || ('f' in changeSet);
        var promise = this.ensureUnlockedRanges(ranges, changeValues ? { lockTables: 'header', lockMatrixes: 'partial' } : null);

        // create and apply the operations
        promise = jpromise.fastThen(promise, function () {
            return implFillRanges(ranges, changeSet, { refAddress: activeAddress, selection: options && options.selection });
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Removes the values, formulas, and formatting of all cells in the
     * specified cell ranges (deletes the cell definitions from the
     * document model), and updates the view.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {RangeArray|Range} [options.ranges]
     *    The addresses of the cell ranges to be cleared. If omitted, the
     *    current selection will be used.
     *  - {SelectionState} [options.selection]
     *    The selection state to be inserted into the undo stack. If
     *    omitted, the current selection will be used.
     *
     * @returns {JPromise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.clearCells = function (options) {

        // fall-back to current selection
        var ranges = options?.ranges || this.selectionEngine.getSelectedRanges();

        // do nothing if current selection contains locked cells (do not allow to delete table header texts)
        var promise = this.ensureUnlockedRanges(ranges, { lockTables: 'header', lockMatrixes: 'partial' });

        // create and apply the operations
        promise = jpromise.fastThen(promise, function () {
            return implFillRanges(ranges, { u: true }, { deleteComments: true, selection: options && options.selection });
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Inserts a matrix formula at the specified position.
     *
     * @param {Range} matrixRange
     *  The cell range to be filled with the matrix formula.
     *
     * @param {string} formula
     *  The formula expression of the matrix formula.
     *
     * @param {Matrix|ErrorCode} result
     *  The calculated result values.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {SelectionState} [options.selection]
     *    The selection state to be inserted into the undo stack. If
     *    omitted, the current selection will be used.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setMatrixFormula = function (matrixRange, formula, result, options) {

        // invalid formula result (e.g. circular reference, oversized matrix) results in #N/A
        var matrix = (result instanceof Matrix) ? result : new Matrix(result);
        // whether to update an existing matrix (prevent auto-expansion of 1x1 matrixes)
        var updateMatrix = !!cellCollection.getMatrixRange(matrixRange.a1);

        // automatically expand single-cell selection to size of result matrix
        if (!updateMatrix && matrixRange.single()) {
            matrixRange = Range.fromAddressAndSize(matrixRange.a1, matrix.cols(), matrix.rows());
        }

        // check the resulting matrix range:
        // - do not allow to insert oversized matrixes
        // - do not insert matrix formulas over merged ranges
        var matrixDim = Dimension.createFromRange(matrixRange);
        var errorCode = null;
        if (!isValidMatrixDim(matrixDim)) {
            errorCode = 'formula:matrix:size';
        } else if (mergeCollection.coversAnyMergedRange(matrixRange)) {
            errorCode = 'formula:matrix:merged';
        }

        // do not insert matrix formulas into table ranges, do not allow to change another existing matrix partially
        if (!errorCode) {
            var lockMatrixes = updateMatrix ? 'none' : 'partial';
            errorCode = this.ensureUnlockedRanges(matrixRange, { lockTables: 'full', lockMatrixes, sync: true });
        }

        // generate and apply the 'changeCells' operation for the matrix formula
        var promise = jpromise.fastThen(createEnsureResult(errorCode), function () {

            // DOCS-1960: build the change sets for all elements of the matrix that are in the target range
            // - do not create change sets for elements outside the target range (oversized matrix)
            // - fill up the target range with #N/A values outside matrix to get correct undo operations
            var col0 = matrixRange.a1.c;
            var row0 = matrixRange.a1.r;
            var changeSets = Array.from(matrixRange.addresses(), address => {
                var col = address.c - col0;
                var row = address.r - row0;
                return { address, v: getCellValue(matrix.get(row, col, ErrorCode.NA)), f: null };
            });

            // add matrix formula and bounding range to the anchor cell
            changeSets[0].f = formula;
            changeSets[0].mr = matrixRange;
            return implChangeCells(changeSets, { selectRange: matrixRange, selection: options && options.selection });
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Changes the attributes of the attribute family 'cell' for all cells
     * in the specified cell ranges.
     *
     * @param {Object} cellAttrs
     *  A simple map with cell attributes to be changed. All attributes set
     *  to the value null will be removed from the cells.
     *
     * @param {Object} [options]
     *  Optional parameters.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setCellAttributes = function (cellAttrs, options) {
        return this.fillCells({ a: { cell: cellAttrs } }, options);
    };

    /**
     * Returns the number format category of the active cell.
     *
     * @returns {FormatCategory}
     *  The current number format category.
     */
    this.getNumberFormatCategory = function () {
        return activeParsedFormat.category;
    };

    /**
     * Changes the number format category in all cell in the specified cell
     * ranges.
     *
     * @param {FormatCategory} category
     *  The category of the number format to be set.
     *
     * @param {Object} [options]
     *  Optional parameters.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setNumberFormatCategory = function (category, options) {
        var parsedFormat = numberFormatter.getCategoryFormat(category);
        return this.fillCells({ format: parsedFormat.formatCode }, options);
    };

    /**
     * Returns the number format code of the active cell.
     *
     * @returns {String}
     *  The format code of the active cell, as string.
     */
    this.getNumberFormat = function () {
        return activeParsedFormat.formatCode;
    };

    /**
     * Changes the number format of all cells in the specified cell ranges.
     *
     * @param {Number|String} format
     *  The identifier of a number format as integer, or a format code as
     *  string, to be set to the current selection.
     *
     * @param {Object} [options]
     *  Optional parameters.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have been
     *  applied successfully, or that will be rejected on any error.
     */
    this.setNumberFormat = function (format, options) {
        return this.fillCells({ format }, options);
    };

    /**
     * Checks if decimal places can be changed
     *
     * Decimal places can be changed on cells of the category standard, number, currency,
     * percent and scientific. returns true if at least one defined cell can be changed
     *
     * @returns {Boolean}
     *  If true if changing decimal places is possible.
     */
    this.isChangeDecimalPlaces = function (increase) {

        if (this.ensureUnlockedRanges(ranges, { sync: true })) {
            return false;
        }

        var ranges = this.selectionEngine.getSelectedRanges();

        var styleIdAddressesMap = implGetChangeDecimalPlacesStyleMap(ranges);
        for (const parsedFormat of styleIdAddressesMap.keys()) {
            switch (parsedFormat.category) {
                // enable control only if all cells are of following format category and if they are transformable
                case FormatCategory.STANDARD:
                case FormatCategory.NUMBER:
                case FormatCategory.CURRENCY:
                case FormatCategory.PERCENT:
                case FormatCategory.SCIENTIFIC: {
                    if (FormatCodeParser.get().isTransformable(parsedFormat.numberSections, { increaseDecimalPlaces: increase })) {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    /**
     * Changing decimal places of defined cells in the selection.
     *
     * Decimal places can be increased / decreased on cells of the category
     * standard, number, currency, percent and scientific.
     *
     * @param {Boolean} increase
     *  Specifies if decimal places should be increased or decreased.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setChangeDecimalPlaces = function (increase) {

        // fall-back to current selection
        var ranges = this.selectionEngine.getSelectedRanges(),
            promise = this.ensureUnlockedRanges(ranges, null),
            styleIdAddressesMap = implGetChangeDecimalPlacesStyleMap(ranges);

        promise = promise.then(function () {
            return activeSheetModel.buildOperations(function (sheetCache) {
                var cellCache = sheetCache.cellCache;
                for (const [parsedFormat, addresses] of styleIdAddressesMap) {
                    var changedFormatCode = FormatCodeParser.get().transform(parsedFormat, { increaseDecimalPlaces: increase });
                    cellCache.changeCells(RangeArray.mergeAddresses(addresses), { format: changedFormatCode });
                }
            });
        });
        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * This is a helper function that collects styleIds and its usage in
     * the given range.
     *
     * @param {RangeArray} ranges
     *  The ranges from which the map should be created.
     *
     * @returns {Map<ParsedFormat, Address[]>}
     *  The key of the returned map is the parsed format code, the value is an
     *  array with cell addresses.
     */
    function implGetChangeDecimalPlacesStyleMap(ranges) {

        const styleIdAddressesMap = new Map/*<ParsedFormat, Address[]>*/();

        // collecting selected cell styles and its addresses
        for (const { address, model } of cellCollection.cellEntries(ranges, { type: 'defined' })) {
            map.upsert(styleIdAddressesMap, model.pf, () => []).push(address);
        }
        return styleIdAddressesMap;
    }

    /**
     * Returns the border flags of the current selection.
     *
     * @returns {Dict<boolean>}
     *  The border flags representing the visibility of the borders in all
     *  ranges of the current selection. See `MixedBorder.getBorderFlags()`
     *  for more details.
     */
    this.getBorderFlags = function () {
        return getBorderFlags(selectedMixedBorders);
    };

    /**
     * Changes the visibility of the borders in all ranges of the current
     * selection.
     *
     * @param {Dict<boolean>} borderFlags
     *  The border flags representing the visibility of the borders in all
     *  ranges of the current selection. See `MixedBorder.getBorderFlags()`
     *  for more details.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.setBorderFlags = function (borderFlags) {

        // wait for the background loop that collects the mixed borders of the selection
        var promise = mixedBordersDef.then(function () {

            // convert border flags to real border attributes
            var borderAttributes = getBorderAttributes(borderFlags, selectedMixedBorders, DEFAULT_SINGLE_BORDER);

            // do nothing if current selection contains locked cells
            var promise2 = self.ensureUnlockedSelection().then(function () {
                return activeSheetModel.createAndApplyOperations(function (generator) {
                    return cellCollection.generateBorderOperations(generator, self.selectionEngine.getSelectedRanges(), borderAttributes);
                }, { applyImmediately: true, storeSelection: true });
            });

            // bug 34021: immediately update the cached mixed borders of the selection for fast GUI feedback
            promise2.done(function () {
                Object.assign(selectedMixedBorders, borderAttributes);
            });

            return promise2;
        });

        // show warning alerts if necessary
        return this.yellOnFailure(promise);
    };

    /**
     * Returns the mixed border attributes of the current selection.
     *
     * @returns {Object}
     *  An attribute map containing the mixed borders of all available
     *  border attributes ('borderTop', 'borderLeft', etc.), as well as the
     *  pseudo attributes 'borderInsideHor' and 'borderInsideVert' for
     *  inner borders in the current selection. Each border attribute value
     *  contains the properties 'style', 'width', and 'color'. If the
     *  border property is equal in all affected cells, it will be set as
     *  property value in the border object, otherwise the property will be
     *  set to the value null.
     */
    this.getBorderAttributes = function () {
        return json.deepClone(selectedMixedBorders);
    };

    /**
     * Changes the specified border properties (line style, line color, or
     * line width) of all visible cell borders in the current selection.
     *
     * @param {Border} border
     *  A border attribute, may be incomplete. All properties contained in
     *  this object will be set for all visible borders in the current
     *  selection. Omitted border properties will not be changed.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the generated operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    this.changeVisibleBorders = function (border) {

        // do nothing if current selection contains locked cells
        var promise = this.ensureUnlockedSelection().then(function () {
            return activeSheetModel.createAndApplyOperations(function (generator) {
                return cellCollection.generateVisibleBorderOperations(generator, self.selectionEngine.getSelectedRanges(), border);
            }, { applyImmediately: true, storeSelection: true });
        });

        // bug 34021: immediately update the cached mixed borders of the selection for fast GUI feedback
        promise.done(function () {
            _.forEach(selectedMixedBorders, function (mixedBorder) {
                if (isVisibleMixedBorder(mixedBorder)) {
                    Object.assign(mixedBorder, border);
                }
            });
        });

        // show warning messages
        return this.yellOnFailure(promise);
    };

    /**
     * Fills or clears a cell range according to the position and contents
     * of the cell range currently selected (the 'auto-fill' feature).
     *
     * @param {Direction} direction
     *  The direction in which the selected range will be expanded.
     *
     * @param {Number} count
     *  The number of columns/rows to extend the selected range into the
     *  specified direction (positive values), or to shrink the selected
     *  range (negative values).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Boolean} [options.copy=false]
     *      If set to true, source values will be copied, instead of
     *      incrementing the existing values automatically.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the auto-fill target range is
     *  editable, and the auto-fill operation has been applied. Otherwise,
     *  the promise will be rejected.
     */
    this.autoFill = function (direction, count, options) {

        // all cell ranges selected in the sheet
        var ranges = this.selectionEngine.getSelectedRanges();
        // the source range to copy values/formatting from
        var srcRange = ranges.first();
        // the promise of the entire auto-fill operations
        var promise = null;

        // for safety: do nothing if called in a multi-selection
        // (no error message, should not happen as the GUI does not offer auto-fill)
        if (ranges.length !== 1) {
            return jpromise.reject();
        }

        // fill cells outside the selected range
        if (count > 0) {

            // build the target range (adjacent to the selected range)
            var targetRange = getAdjacentRange(srcRange, direction, count);

            // only the target range must be editable (not the source cells)
            promise = this.ensureUnlockedRanges(targetRange, { lockTables: 'header', lockMatrixes: 'partial' });

            // do not partially overwrite merged ranges
            promise = promise.then(function () {
                if (mergeCollection.coversAnyMergedRange(targetRange, FindMatchType.PARTIAL)) {
                    return makeRejected('autofill:merge:overlap');
                }
            });

            // apply auto-fill, and update optimal row heights
            promise = promise.then(function () {
                return activeSheetModel.createAndApplyOperations(function (generator) {

                    var promise2 = cellCollection.generateAutoFillOperations(generator, ranges.first(), direction, count, options);

                    promise2 = promise2.then(function () {
                        if (!docModel.addressFactory.isRowRange(targetRange)) {
                            return self.generateUpdateRowHeightOperations(generator, targetRange);
                        }
                    });

                    // expand the selected range
                    return promise2.always(function () {
                        self.selectionEngine.changeActiveRange(ranges.first().boundary(targetRange));
                    });
                }, { applyImmediately: true, storeSelection: true });
            });

        // delete cells inside the selected range
        } else if (count < 0) {

            // whether to expand/delete columns or rows
            var columns = !isVerticalDir(direction);
            // whether to expand/shrink the leading or trailing border
            var leading = isLeadingDir(direction);

            // build the target range (part of the selected range, may be the entire selected range)
            var deleteRange = srcRange.clone();
            if (leading) {
                deleteRange.setEnd(srcRange.getStart(columns) - count - 1, columns);
            } else {
                deleteRange.setStart(srcRange.getEnd(columns) + count + 1, columns);
            }

            // the target range must be editable (not the entire selected range)
            promise = this.ensureUnlockedRanges(deleteRange, { lockTables: 'header', lockMatrixes: 'partial' });

            // remove all values and formulas from the cell range (but not the formatting),
            // and update optimal row heights
            promise = promise.then(function () {
                return implFillRanges(deleteRange, { v: null, f: null });
            });

            // update selection
            promise.always(function () {
                // do not change selection after deleting the entire range
                if (srcRange.equals(deleteRange)) { return; }
                // select the unmodified part of the original range
                deleteRange = srcRange.clone();
                if (leading) {
                    deleteRange.a1.move(-count, columns);
                } else {
                    deleteRange.a2.move(count, columns);
                }
                self.selectionEngine.changeActiveRange(deleteRange);
            });

        // do nothing if no cells will be filled or cleared
        } else {
            promise = this.createResolvedPromise();
        }

        return this.yellOnFailure(promise);
    };

    /**
     * Inserts one or more formulas calculating subtotal results into the,
     * or next to the current selection.
     *
     * @param {String} funcKey
     *  The resource key of the subtotal function to be inserted into the
     *  formulas. MUST be a function that takes an argument of reference
     *  type.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the formulas have been
     *  created successfully.
     */
    this.insertAutoFormula = function (funcKey) {

        // the currently selected cells
        var ranges = this.selectionEngine.getSelectedRanges();

        // DOCS-1653: start edit mode without target range, if the entire sheet is selected
        if ((ranges.length !== 1) || docModel.addressFactory.isSheetRange(ranges[0])) {
            var formula1 = '=' + docModel.formulaGrammarUI.generateAutoFormula(docModel, funcKey, null, activeAddress);
            // cancel current cell edit mode, and restart edit mode
            return this.enterTextEditMode('cell', { text: formula1, pos: -1, restart: true });
        }

        // start cell edit mode, if a single cell is selected
        if (this.selectionEngine.isSingleCellSelection()) {
            var sourceRange = cellCollection.findAutoFormulaRange(activeAddress);
            var formula2 = '=' + docModel.formulaGrammarUI.generateAutoFormula(docModel, funcKey, sourceRange, activeAddress);
            // cancel current cell edit mode, and restart edit mode
            return this.enterTextEditMode('cell', { text: formula2, pos: -1, restart: true });
        }

        // bug 56321: require unlocked sheet (resulting locations of formula cells
        // is unpredictable with the current implementation)
        var promise = this.ensureUnlockedSheet();

        // create and apply the operations
        promise = promise.then(function () {
            return activeSheetModel.createAndApplyOperations(function (generator) {
                // generate the 'changeCells' operations, and the undo operations
                return cellCollection.generateAutoFormulaOperations(generator, funcKey, ranges[0]).done(function (targetRange) {
                    if (targetRange) { self.selectionEngine.selectRange(targetRange); }
                });
            }, { applyImmediately: true, storeSelection: true });
        });

        return this.yellOnFailure(promise);
    };

    /**
     * Merges or unmerges all cells of the current selection.
     *
     * @param {MergeMode | "toggle"} mergeMode
     *  The merge mode to be applied, or the special string "toggle" to
     *  toggle the current merge state of the selection.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after the ranges have been merged or
     *  unmerged successfully, or that will reject on any error.
     */
    this.mergeRanges = function (mergeMode) {

        // the selected ranges to be merged/unmerged
        var ranges = this.selectionEngine.getSelectedRanges();

        // toggle merge state
        if (mergeMode === 'toggle') {
            mergeMode = mergeCollection.coversAnyMergedRange(ranges) ? MergeMode.UNMERGE : MergeMode.MERGE;
        }

        // do nothing, if current selection contains locked cells
        // bug 39869: do not allow to merge any cells in a table range (except auto-filter)
        // DOCS-196: do not allow to merge matrix formulas
        var unmerge = mergeMode === MergeMode.UNMERGE;
        var promise = this.ensureUnlockedSelection(unmerge ? null : { lockTables: 'full', lockMatrixes: 'full' });

        // create and apply the operations
        promise = promise.then(function () {
            return activeSheetModel.createAndApplyOperations(function (generator) {
                return mergeCollection.generateMergeCellsOperations(generator, ranges, mergeMode);
            }, { applyImmediately: true, storeSelection: true });
        });

        // show warning alerts if necessary
        return this.yellOnFailure(promise);
    };

    // hyperlink operations -----------------------------------------------

    /**
     * Inserts new hyperlink ranges covering the selected cells, or the
     * active cell.
     *
     * @param {String} url
     *  The URL of the hyperlink to be inserted.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {RangeArray|Range} [options.ranges]
     *    The cell ranges to be filled. By default, the current selection
     *    will be filled.
     *  - {boolean} [options.createStyles=false]
     *    If set to `true`, the cells covered by the new hyperlink will be
     *    set to underline style, and to the current hyperlink text color.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the hyperlink ranges have
     *  been inserted successfully, ot that will be rejected on any error.
     */
    this.insertHyperlink = function (url, options) {

        // the change set used to create the hyperlink
        var changeSet = { url };

        // the character attributes for the cells covered by the hyperlink
        if (options && options.createStyles) {
            changeSet.a = { character: { underline: true, color: docModel.createHlinkColor() } };
        }

        // fill the specified ranges, or the entire cell selection
        return this.fillCells(changeSet, options);
    };

    /**
     * Deletes the hyperlink ranges covering the selected cells.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the hyperlink ranges have
     *  been deleted successfully, ot that will be rejected on any error.
     */
    this.deleteHyperlink = function () {

        // the change set (remove hyperlinks, and the character formatting)
        var changeSet = { a: { character: { underline: null, color: null } }, url: '' };

        // fill the entire cell selection
        return this.fillCells(changeSet);
    };

    /**
     * Opens the "Edit Hyperlink" dialog, and applies the settings made in
     * the dialog to the current cell selection.
     *
     * @returns {JPromise<HyperlinkDialogResult>}
     *  A promise that will be resolved after the dialog has been closed,
     *  and the hyperlink settings have been applied successfully; or that
     *  will be rejected after canceling the dialog, or on any error.
     */
    this.editHyperlink = function () {

        // whether the cell contained a hyperlink before
        var hasHyperlink = false;

        // try to leave text edit mode
        var promise = this.leaveTextEditMode();

        // active cell must be editable (perform additional check for tables)
        promise = promise.then(function () {
            return self.ensureUnlockedActiveCell();
        });

        // create and show the dialog
        promise = promise.then(function () {

            // the current hyperlink of the active cell
            var currUrl = self.getCellURL();
            // the initial display string for the URL (not for formula cells)
            var displayText = activeTokenDesc ? null : cellCollection.getEditString(activeAddress);

            // remember existence of hyperlink for character formatting (see below)
            hasHyperlink = !!currUrl;

            // if there is no real hyperlink, try the literal cell string
            if (!currUrl && displayText) {
                currUrl = checkForHyperlink(displayText);
            }

            // do not duplicate equal URL and display string in the dialog
            if (currUrl === displayText) { displayText = null; }

            // show the 'Edit Hyperlink' dialog
            return new HyperlinkDialog(self, currUrl, displayText, {
                // do not allow to enter display string into formula cells
                disableDisplay: !!activeTokenDesc,
                // close automatically, if active sheet will be deleted or hidden by another client
                autoCloseScope: 'activeSheet'
            }).show();
        });

        // apply hyperlink settings after closing the dialog
        promise = promise.then(function (result) {

            // missing result.url indicates to delete a hyperlink ('Remove' button)
            if (!result.url) {
                return self.deleteHyperlink();
            }

            // collect multiple complex sheet operations in a single undo action
            return docModel.undoManager.enterUndoGroup(function () {

                // add character formatting for a new hyperlink, and generate the operation
                var promise2 = self.insertHyperlink(result.url, { createStyles: !hasHyperlink });

                // create a text cell, if user has entered a display string in the dialog;
                // or if the cell was blank, and a valid URL has been entered (do not change
                // existing cells, e.g. numbers, formulas, etc.)
                if (result.text) {
                    promise2 = jpromise.fastThen(promise2, function () {
                        // DOCS-3021: ODF does not support non-string cells with hyperlinks
                        return docModel.docApp.isODF() ?
                            implChangeCell(activeAddress, { v: result.text }) :
                            implParseCellText(activeAddress, result.text, { skipHyperlink: true });
                    });
                } else if (!activeTokenDesc && (activeCellValue === null)) {
                    promise2 = jpromise.fastThen(promise2, function () {
                        return implChangeCell(activeAddress, { v: result.url });
                    });
                }

                return promise2;
            });
        });

        // show warning alert if necessary
        return this.yellOnFailure(promise);
    };

    // initialization -----------------------------------------------------

    // initialize selection settings, used until first real update
    activeAttributeSet = docModel.cellStyles.getDefaultStyleAttributeSet();
    activeParsedFormat = numberFormatter.getParsedFormatForAttributes(activeAttributeSet.cell);

    // initialize sheet-dependent class members according to the active sheet
    this.on('change:activesheet', changeActiveSheetHandler);

    // refresh selection settings after changing the selection
    this.on('change:selection:prepare', function () { updateSelectionSettings(false); });

    // update selection settings after any operational changes in the document
    this.one('change:activesheet', function () {
        self.listenTo(docModel, 'operations:success', function () { updateSelectionSettings(true); });
    });
}
