/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { Button } from "@/io.ox/office/tk/controls";
import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";
import { hasSupportedProtocol } from "@/io.ox/office/editframework/utils/hyperlinkutils";
import { OPEN_HYPERLINK_LABEL } from "@/io.ox/office/spreadsheet/view/labels";

// class HyperlinkContextMenu =================================================

/**
 * A context menu for grid panes. Provides menu actions to access a hyperlink.
 *
 * @param {GridPane} gridPane
 *  The grid pane that contains this instance.
 */
export class HyperlinkContextMenu extends ContextMenu {

    constructor(gridPane) {

        // base constructor
        super(gridPane.docView, gridPane.$el, {
            // do not show context menu during cell edit mode
            enableKey: "document/readonly/cell",
            repaintAlways: true,
            delay: 200
        });

        this.gridPane = gridPane;

        // hide contextmenu if the user start scrolling
        this.listenTo(this.docView, "change:scrollpos", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /**
     * Selects the column/row that has been clicked with the right mouse
     * button, before the context menu will be shown.
     */
    /*protected override*/ implPrepareContext() {
        // do not show the context menu, if the hyperlink is not valid
        const effectiveUrl = this.docView.getEffectiveURL();
        return !!effectiveUrl && hasSupportedProtocol(effectiveUrl);
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.destroyAllControls();

        // the URL of a hyperlink covering the active cell (check for supported protocol)
        let cellUrl = this.docView.getCellURL();
        if (!hasSupportedProtocol(cellUrl)) { cellUrl = null; }

        // the URL returned by a formula cell (HYPERLINK function) if available
        let effectiveUrl = this.docView.getEffectiveURL();
        if (!hasSupportedProtocol(effectiveUrl)) { effectiveUrl = null; }

        // show the open hyperlink button for any available hyperlink
        if (effectiveUrl) {
            this.addControl(null, new Button({ label: OPEN_HYPERLINK_LABEL, href: effectiveUrl }));
        }

        this.setToolTip(_.noI18n(effectiveUrl || ""), { top: true });
    }
}
