/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { str } from "@/io.ox/office/tk/algorithms";
import { detachChildren, createDiv, createSpan } from "@/io.ox/office/tk/dom";
import { ToolTip } from "@/io.ox/office/tk/popup/tooltip";

import "@/io.ox/office/spreadsheet/view/popup/signaturetooltip.less";

// class SignatureToolTip =====================================================

/**
 * A tooltip node attached to the text area shown in cell edit mode, that
 * shows help for the parameters of the sheet function currently edited.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 *
 * @param {PopupAnchorSpec} anchorSpec
 *  The element used as anchor for the tooltip node.
 */
export class SignatureToolTip extends ToolTip {

    constructor(docView, anchorSpec) {

        // base constructor
        super({ anchor: anchorSpec, classes: "signature-tooltip noI18n", maxSize: 600 });

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // the <div> element with the function signature
        this._funcNode = createDiv("func-signature");
        // the <div> element with the description of a function parameter
        this._descNode = createDiv("param-help");

        this.$body.append(this._funcNode, this._descNode);
    }

    // public methods ---------------------------------------------------------

    /**
     * Updates and shows or hides the tooltip attached to the text area,
     * containing the name, parameter list, and description of the current
     * function.
     *
     * @param {string} funcKey
     *  The unique resource key of a sheet function.
     *
     * @param {number} paramIndex
     *  The zero-based index of a parameter of the specified function that will
     *  be shown highlighted, and whose help text will be added to the tooltip.
     *  If set to an invalid number, no parameter will be highlighted.
     */
    updateSignature(funcKey, paramIndex) {

        // the help descriptor of the function, containing the description and parameter names
        // (do nothing if no help is available for the function)
        const functionHelp = this.docModel.formulaResource.getFunctionHelp(funcKey);
        if (!functionHelp) { this.hide(); return; }

        // info descriptor of the active parameter
        const activeParamHelp = (paramIndex >= 0) ? this._getParameterHelp(functionHelp, paramIndex) : undefined;

        // clear the function signature
        detachChildren(this._funcNode);

        // create the leading <span> element with the function name
        this._appendSpan(functionHelp.name, { "function-name": true, "param-active": paramIndex < 0 });

        // opening parenthesis for parameter list
        this._appendSpan("(");

        // add <span> elements for all function parameters
        for (let index = 0; index < functionHelp.params.length; index += 1) {
            this._appendParameter(functionHelp, index, paramIndex);
        }

        // special handling for repeated parameters
        if (functionHelp.repeatStart >= 0) {

            // repetition cycle #0 is part of functionHelp.params, add mark-up for
            // repetition cycle #1, e.g.: "SUM(value1" becomes "SUM(value1,[value2]"
            this._appendParameterCycle(functionHelp, 1, paramIndex);

            // add more repetition cycles around the active repeated parameter
            if (activeParamHelp && (activeParamHelp.cycleIndex >= 0)) {

                // cycles #0 and #1 have been generated already; if active cycle is
                // cycle #4, then cycle #2 is omitted, and an ellipsis will be added
                if (activeParamHelp.cycleIndex >= 4) {
                    this._appendSkippedParameter();
                }
                // if active cycle is cycle #3 or higher, add preceding cycle
                if (activeParamHelp.cycleIndex >= 3) {
                    this._appendParameterCycle(functionHelp, activeParamHelp.cycleIndex - 1, paramIndex);
                }
                // if active cycle is cycle #2 or higher, generate it
                if (activeParamHelp.cycleIndex >= 2) {
                    this._appendParameterCycle(functionHelp, activeParamHelp.cycleIndex, paramIndex);
                }
                // if active cycle is cycle #1 or higher, add following cycle
                if (activeParamHelp.cycleIndex >= 1) {
                    this._appendParameterCycle(functionHelp, activeParamHelp.cycleIndex + 1, paramIndex);
                }
            }

            // add trailing ellipsis, unless the active or the following cycle is the last one
            if (!activeParamHelp || (activeParamHelp.cycleIndex < 0) || (activeParamHelp.cycleIndex + 2 < functionHelp.cycleCount)) {
                this._appendSkippedParameter();
            }
        }

        // closing parenthesis for parameter list
        this._appendSpan(")");

        // add detailed description of current parameter
        const help = (paramIndex < 0) ? functionHelp : activeParamHelp;
        this._descNode.textContent = help?.description || "";

        // finally, show the tooltip node
        this.show();
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a descriptor object for the specified function parameter.
     *
     * @param {FunctionHelpDescriptor} functionHelp
     *  The help descriptor of a function.
     *
     * @param {number} paramIndex
     *  The zero-based index of a parameter of the specified function.
     *
     * @returns {object|null}
     *  A help descriptor if the specified function parameter is available,
     *  with the following properties:
     *  - {string} name
     *    The name of the function parameter.
     *  - {string} description
     *    A short description text for the function parameter.
     *  - {boolean} optional
     *    Whether the parameter is optional.
     *  - {number} cycleIndex
     *    The zero-based index of the repetition cycle, if the parameter can be
     *    used repeatedly; otherwise -1.
     *  - {number} relIndex
     *    The relative index of the parameter in the current repetition cycle,
     *    if the parameter can be used repeatedly; otherwise -1.
     */
    /*private*/ _getParameterHelp(functionHelp, paramIndex) {

        // whether the parameter is part of a repetition cycle
        const repeated = (functionHelp.repeatStart >= 0) && (functionHelp.repeatStart <= paramIndex);
        // index of the repetition cycle (used as suffix for parameter names)
        const cycleIndex = repeated ? Math.floor((paramIndex - functionHelp.repeatStart) / functionHelp.cycleLength) : -1;
        // relative parameter index in current repetition cycle
        const relIndex = repeated ? ((paramIndex - functionHelp.repeatStart) % functionHelp.cycleLength) : -1;
        // the help descriptor of the current parameter
        const paramHelp = functionHelp.params[repeated ? (functionHelp.repeatStart + relIndex) : paramIndex];
        // whether the passed parameter index is valid
        const valid = !!paramHelp && (!repeated || (cycleIndex < functionHelp.cycleCount));

        // build and return the parameter descriptor
        return valid ? {
            name: paramHelp.name,
            description: paramHelp.description,
            // parameters in the second and following repetition cycles are always optional
            optional: paramHelp.optional || (cycleIndex > 0),
            cycleIndex,
            relIndex
        } : null;
    }

    /**
     * Returns the parameter separator with following space character.
     */
    /*private*/ _getSeparator() {
        return this.docModel.formulaResource.getSeparator(true) + " ";
    }

    /*private*/ _appendSpan(label, classes) {
        this._funcNode.appendChild(createSpan({ label, classes }));
    }

    /**
     * Appends a `<span>` element for a skipped parameter (parameter separator
     * and an ellipsis character).
     */
    /*private*/ _appendSkippedParameter() {
        this._appendSpan(this._getSeparator() + str.ELLIPSIS_CHAR);
    }

    /**
     * Appends the `<span>` element for a single function parameter.
     */
    /*private*/ _appendParameter(functionHelp, paramIndex, activeIndex) {

        // complete parameter info descriptor
        const paramHelp = this._getParameterHelp(functionHelp, paramIndex);
        if (!paramHelp) { return; }

        // add a parameter separator between parameters
        if (paramIndex > 0) { this._appendSpan(this._getSeparator()); }

        // enclose optional parameters and first parameter in (optional) repetition cycle with brackets
        let label = (paramHelp.optional && (paramHelp.relIndex <= 0)) ? "[" : "";
        // add the parameter name and repetition index
        label += paramHelp.name;
        if (paramHelp.cycleIndex >= 0) { label += String(paramHelp.cycleIndex + 1); }
        // enclose optional parameters and last parameter in (optional) repetition cycle with brackets
        if (paramHelp.optional && ((paramHelp.relIndex < 0) || (paramHelp.relIndex + 1 === functionHelp.cycleLength))) { label += "]"; }

        // the span element for the parameter (highlight active parameter)
        this._appendSpan(label, { "param-active": paramIndex === activeIndex });
    }

    /**
     * Appends the `<span>` elements for a complete parameter repetition cycle.
     */
    /*private*/ _appendParameterCycle(functionHelp, cycleIndex, activeIndex) {
        if (cycleIndex < functionHelp.cycleCount) {
            for (let firstIndex = functionHelp.repeatStart + cycleIndex * functionHelp.cycleLength, index = 0; index < functionHelp.cycleLength; index += 1) {
                this._appendParameter(functionHelp, firstIndex + index, activeIndex);
            }
        }
    }
}
