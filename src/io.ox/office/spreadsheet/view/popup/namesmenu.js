/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { ary } from "@/io.ox/office/tk/algorithms";
import { createDiv, createSpan } from "@/io.ox/office/tk/dom";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { Button } from "@/io.ox/office/tk/control/button";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";
import { FloatingMenu } from "@/io.ox/office/baseframework/view/popup/floatingmenu";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";

import "@/io.ox/office/spreadsheet/view/popup/namesmenu.less";

// class NameItem =============================================================

/**
 * A DOM container displaying properties of a defined name.
 */
class NameItem extends ViewObject {

    constructor(docView, nameModel) {

        // base constructor
        super(docView);

        // the model of the defined name
        this.nameModel = nameModel;
        // unique identifier of the current range highlighting mode for mouse-hover
        this.highlightUid = null;

        // the DOM root node
        this.$el = $(createDiv("name-item")).attr("role", "dialog");

        // the DOM nodes for the name's label and formula expression
        this.$label = $(createSpan("text"));
        this.$formula = $(createSpan("formula"));

        // container for the label and formula (click selects the cell ranges)
        this.$name = $(createDiv("name")).append(this.$label, this.$formula).appendTo(this.$el);
        this.$name.on("click", () => this._selectSingleRange());

        // the edit button that will open the "Edit Name" dialog
        this.editButton = this.member(new Button({ icon: "bi:pencil-square", tooltip: gt("Change name"), value: nameModel }));
        // the delete button to remove a defined name from the document
        this.deleteButton = this.member(new Button({ icon: "bi:trash", tooltip: gt("Delete name"), value: nameModel }));

        // create and insert a container node for the action buttons
        this.$el.append($(createDiv("actions")).append(this.editButton.$el, this.deleteButton.$el));

        // bind the action buttons to controller items
        docView.bindGroupToControllerItem("name/edit/dialog", this.editButton);
        docView.bindGroupToControllerItem("name/delete", this.deleteButton);

        // add event listeners to highlight the cell ranges
        this.listenTo(this.$el, "mouseenter", this._startHoverHighlighting);
        this.listenTo(this.$el, "mouseleave", this._endHoverHighlighting);

        // update the relative references in the formula expression according to the selection
        this.listenTo(docView, "change:selection", this.debounce(this._updateLabels, { delay: 500 }));

        // initialize label and formula expression
        this._updateLabels();
    }

    destructor() {
        this._endHoverHighlighting();
        this.docView.unbindGroupFromControllerItem(this.editButton);
        this.docView.unbindGroupFromControllerItem(this.deleteButton);
        super.destructor();
    }

    // private methods --------------------------------------------------------

    /*private*/ _resolveSingleRange() {

        // all ranges contained in the defined name
        const activeCell = this.docView.selectionEngine.getActiveCell();
        const rangeInfos = this.nameModel.tokenArray.extractRanges(Address.A1, activeCell, { resolveNames: true });

        // only select a single range in the token array
        return (rangeInfos.length === 1) ? rangeInfos[0].range : undefined;
    }

    /*private*/ _selectSingleRange() {

        // do not leave cell edit mode (but drawing edit mode can be left safely)
        if (this.docView.isTextEditMode("cell")) { return; }

        // only select a single range in the token array
        const range = this._resolveSingleRange();
        if (!range) { return; }

        // switch to the first visible sheet referred by the range
        if (!range.containsSheet(this.docModel.getActiveSheet())) {
            const sheet = this.docModel.sheetCollection.findVisibleSheet(range.sheet1, "next");
            if (range.containsSheet(sheet)) {
                this.docModel.setActiveSheet(sheet);
            }
        }

        // scroll to start cell of the range (do not change the active grid pane in frozen split mode)
        if (range.containsSheet(this.docModel.getActiveSheet())) {
            this.docView.scrollToCell(range.a1, { preserveFocus: true });
        }
    }

    /*private*/ _updateLabels() {

        // set the label of the defined name
        const label = _.noI18n(this.nameModel.label);
        this.$label.attr("title", label).text(label);

        // set the formula expression of the defined name
        const formula = _.noI18n("=" + this.nameModel.getFormula("ui", this.docView.selectionEngine.getActiveCell()));
        this.$formula.attr("title", formula).text(formula);

        // single range marker for click+select
        this.$name.toggleClass("single-range", !!this._resolveSingleRange());
    }

    /*private*/ _startHoverHighlighting() {
        const { tokenArray } = this.nameModel;
        this.highlightUid = this.docView.rangeHighlighter.startHighlighting(tokenArray, {
            targetAddress: this.docView.selectionEngine.getActiveCell(),
            resolveNames: true
        });
    }

    /*private*/ _endHoverHighlighting() {
        this.docView.rangeHighlighter.endHighlighting(this.highlightUid);
    }
}

// class NamesMenu ============================================================

/**
 * A floating menu with all named ranges.
 */
export class NamesMenu extends FloatingMenu {

    constructor(docView) {

        // base constructor
        super(docView, /*#. dialog title */ gt.pgettext("named ranges", "Named ranges"), {
            classes: "defined-names"
        });

        // root container for all name items
        this.items = this.member/*<NameItem[]>*/([]);
        this.addSection("items");

        // add an extra button to define new named ranges
        this.addSection("controls", { inline: true, boxed: true });
        this.addControl("name/insert/dialog", new Button({ icon: "svg:add", label: gt.pgettext("named ranges", "Add new named range") }));

        // lazy repaint after defined names have been changed
        this.requestMenuRepaintOnEvent(this.docModel, ["insert:name", "change:name", "delete:name"]);

        // hide the menu automatically in read-only mode
        this.listenTo(docView.docApp, "docs:editmode:leave", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // clean up all old contents
        ary.destroy(this.items);
        this.$el.find('[data-section="items"]').empty();
        this.addContainer({ section: "items" });

        // the models of all visible defined names in the document (TODO: sheet-local names)
        const nameModels = Array.from(this.docModel.nameCollection.yieldNameModels({ skipHidden: true }));
        LOCALE_DATA.getCollator({ numeric: true }).sortByProp(nameModels, "label");

        // add a special entry, if no names are available at all
        if (nameModels.length === 0) {
            this.addNodes($('<div class="no-names-message">').text(gt.pgettext("named ranges", "No named ranges defined.")), { section: "items" });
        } else {
            for (const nameModel of nameModels) {
                const nameItem = new NameItem(this.docView, nameModel);
                this.items.push(nameItem);
                this.addNodes(nameItem.$el, { section: "items" });
            }
        }
    }
}
