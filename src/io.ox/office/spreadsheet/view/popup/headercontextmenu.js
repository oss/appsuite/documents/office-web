/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { parseIntAttribute } from "@/io.ox/office/tk/dom";
import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";
import { SelectionMode } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { Button, ColRowOpLabelButton } from "@/io.ox/office/spreadsheet/view/controls";

// class HeaderContextMenu ====================================================

/**
 * A context menu for header panes. Provides menu actions to manipulate entire
 * columns/rows in the active sheet.
 *
 * @param {HeaderPane} headerPane
 *  The header pane that contains this instance.
 *
 * @param {Boolean} columns
 *  Whether this context menu is for columns (true), or for rows (false).
 */
export class HeaderContextMenu extends ContextMenu {

    constructor(headerPane, columns) {

        // base constructor
        super(headerPane.docView, headerPane.$el, {
            selector: ".cell", // only on cell header nodes (not on resizers)
            // do not show context menu at all in locked sheets, or during cell edit mode
            enableKey: "sheet/operation/unlocked/cell",
            repaintAlways: true,
            delay: 200
        });

        this.headerPane = headerPane;
        this.columns = columns;

        // hide contextmenu if the user start scrolling
        this.listenTo(this.docView, "change:scrollpos", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /**
     * Selects the column/row that has been clicked with the right mouse
     * button, before the context menu will be shown.
     */
    /*protected override*/ implPrepareContext(sourceEvent) {

        // the index of the column/row to be selected
        const index = parseIntAttribute(sourceEvent.target, "data-index", -1);
        if (index < 0) { return false; }

        // checks whether the column/row index is contained in a range address
        const rangeContainsIndex = range => this.docModel.addressFactory.isFullRange(range, this.columns) && range.containsIndex(index, this.columns);

        // select the clicked column/row if it is not contained in the selection
        if (!this.docView.selectionEngine.getSelectedRanges().some(rangeContainsIndex)) {
            this.docView.grabFocus();
            this.headerPane.trigger("select:start", index, SelectionMode.SELECT);
            this.headerPane.trigger("select:end", index, true);
        }
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        const isRange = this.docView.selectionEngine.getSelectedRanges().intervals(this.columns).size() > 1;

        this.destroyAllControls();

        this.addSection("move");
        if (this.columns) {
            this.addControl("column/insert", new ColRowOpLabelButton(true, true, isRange));
            this.addControl("column/delete", new ColRowOpLabelButton(false, true, isRange));
        } else {
            this.addControl("row/insert", new ColRowOpLabelButton(true, false, isRange));
            this.addControl("row/delete", new ColRowOpLabelButton(false, false, isRange));
        }

        this.addSection("options");
        if (this.columns) {
            this.addControl("column/width/optimal", new Button({ label: gt("Set optimal column width") }));
            this.addControl("column/hide",          new Button({ label: isRange ? gt("Hide columns") : gt("Hide column") }));
            this.addControl("column/show",          new Button({ label: gt("Show hidden columns") }));
        } else {
            this.addControl("row/height/optimal", new Button({ label: gt("Set optimal row height") }));
            this.addControl("row/hide",           new Button({ label: isRange ? gt("Hide rows") : gt("Hide row") }));
            this.addControl("row/show",           new Button({ label: gt("Show hidden rows") }));
        }
    }
}
