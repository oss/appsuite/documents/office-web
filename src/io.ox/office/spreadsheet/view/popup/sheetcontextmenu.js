/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { OPTION_BUTTON_SELECTOR, isCheckedButtonNode } from "@/io.ox/office/tk/forms";
import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";

// class SheetContextMenu =====================================================

/**
 * A context menu for sheet tab buttons. Provides menu actions to manipulate
 * the active sheet, or the sheet collection of the document.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class SheetContextMenu extends ContextMenu {

    constructor(docView, rootNode) {

        // base constructor
        super(docView, rootNode, {
            selector: OPTION_BUTTON_SELECTOR,
            enableKey: "document/editable", // do not show context menu in read-only mode
            hideDisabled: false, // override `true` option of base class
            delay: 200
        });

        // hide contextmenu if the user start scrolling
        this.listenTo(docView, "change:scrollpos", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /**
     * Activate the clicked sheet, before the context menu will be shown.
     */
    /*protected override*/ implPrepareContext(sourceEvent) {
        // click the sheet button if it is not selected (activate the sheet)
        if (!isCheckedButtonNode(sourceEvent.target)) {
            $(sourceEvent.target).click();
        }
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();
        this.docView.addSheetActionButtons(this, { insertSheet: true });
    }
}
