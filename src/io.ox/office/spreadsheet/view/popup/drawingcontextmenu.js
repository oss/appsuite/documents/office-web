/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { EditContextMenu } from "@/io.ox/office/editframework/view/popup/editcontextmenu";
import { DELETE_DRAWING_LABEL, DELETE_DRAWING_TOOLTIP } from "@/io.ox/office/spreadsheet/view/labels";
import { Button, DrawingArrangementPicker } from "@/io.ox/office/spreadsheet/view/controls";

// class DrawingContextMenu ===================================================

/**
 * A context menu for drawing objects. Provides menu actions to manipulate the
 * selected drawing objects in the active sheet.
 */
export class DrawingContextMenu extends EditContextMenu {

    constructor(gridPane, drawingLayerNode) {

        // base constructor
        super(gridPane.docView, drawingLayerNode, {
            enableKey: "view/selection/drawing", // show at least "copy" for read-only drawings
            delay: 200
        });

        this.gridPane = gridPane;

        // hide contextmenu if the user start scrolling
        this.listenTo(this.docView, "change:scrollpos", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /**
     * Selects the drawing object that has been clicked with the right mouse
     * button, before the context menu will be shown.
     */
    /*protected override*/ implPrepareContext(sourceEvent) {

        // do not show the drawing context menu if no drawing frame can be found
        return !!this.gridPane.getDrawingFrameForEvent(sourceEvent);
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // add clipboard commands
        this.addClipboardEntries();

        this.addSection("delete");
        this.addControl("drawing/delete", new Button({ label: DELETE_DRAWING_LABEL, tooltip: DELETE_DRAWING_TOOLTIP }));

        this.addSection("order");
        this.addControl("drawing/order", new DrawingArrangementPicker(this.docView, { anchorBorder: ["right", "left"] }));
    }
}
