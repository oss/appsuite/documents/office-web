/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import _ from "$/underscore";
import $ from "$/jquery";

import { is } from "@/io.ox/office/tk/algorithms";
import { BUTTON_SELECTOR, getButtonValue, touchAwareListener } from "@/io.ox/office/tk/forms";
import { ListMenu } from "@/io.ox/office/tk/popup/listmenu";

import { makeRejected } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { getCellMoveDirection } from "@/io.ox/office/spreadsheet/utils/paneutils";

// class ValidationListMenu ===================================================

/**
 * A dropdown list for data validation of type "source" or "list" in a
 * spreadsheet cell.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view that contains this instance.
 */
export class ValidationListMenu extends ListMenu {

    constructor(docView) {

        // base constructor
        super({
            autoFocus: false,
            anchorAlign: "trailing",
            stretchToAnchor: true,
            valueSerializer: entry => entry.display
        });

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // handle click events on the option buttons
        touchAwareListener(this.$el, BUTTON_SELECTOR, event => {
            this._applyListValue($(event.currentTarget));
        });
    }


    // public methods ---------------------------------------------------------

    /**
     * Collects and inserts the list entries for this validation list. Called
     * by grid pane instances from the click handler of cell dropdown buttons.
     *
     * @param {Address} address
     *  The address of the cell containing the validation data.
     */
    initialize(address) {

        // receive source data from document (close the menu, if no data is available for the list)
        const contentsList = this.docView.dvRuleCollection.queryListContents(address);
        if (contentsList.length === 0) { return makeRejected("validation:source"); }

        // insert the data into the list
        this.clearOptions();
        contentsList.forEach(entry => this.addOption(entry, { label: _.noI18n(entry.display) }));
    }

    /**
     * Returns a tooltip for cell dropdown buttons attached to this menu.
     */
    getTooltip() {
        return gt("Select a value for this cell");
    }

    /**
     * Processes the passed "keydown" event for this list menu, if it is
     * currently visible. Moves the selected option button, if cursor keys or
     * similar keys have been pressed, or applies the selected option button,
     * if the TAB or ENTER keys have been pressed.
     *
     * @param {AnyKeyEvent} event
     *  The "keydown" event object to be processed.
     *
     * @returns {boolean}
     *  Whether the key event represents a supported key and has been processed
     *  successfully.
     */
    handleKeyDownEvent(event) {

        // do nothing in hidden pop-up menu
        if (!this.isVisible()) { return false; }

        // the selected option button
        const $button = this.getSelectedOptions().first();

        // apply the selected value on TAB or ENTER (optionally with SHIFT)
        const moveDir = getCellMoveDirection(event);
        if (moveDir) {
            this._applyListValue($button);
            this.docView.selectionEngine.moveActiveCell(moveDir);
            return true;
        }

        // change selection in the list
        if (this.selectOptionForKey(event, $button)) {
            return true;
        }

        // unsupported key
        return false;
    }

    // private methods --------------------------------------------------------

    /**
     * Inserts the value represented by the passed option button of the
     * validation list menu into the active cell.
     */
    /*private*/ _applyListValue($button) {

        // the list entry data of the selected option button
        const entry = ($button.length > 0) ? getButtonValue($button) : null;

        // always hide the pop-up menu
        this.hide();

        // insert the value into the cell (via controller for busy screen etc.)
        if (entry) {
            if ("value" in entry) {
                this.docView.executeControllerItem("cell/active/contents", { v: entry.value, f: null });
            } else if (is.string(entry.display)) {
                this.docView.executeControllerItem("cell/active/parse", entry.display);
            }
        }
    }
}
