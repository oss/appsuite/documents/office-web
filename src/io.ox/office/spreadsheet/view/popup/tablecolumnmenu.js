/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { RadioGroup } from "@/io.ox/office/tk/control/radiogroup";
import { CompoundMenu } from "@/io.ox/office/baseframework/view/popup/compoundmenu";

import { OK_LABEL, FILTER_HEADER_LABEL, SORT_HEADER_LABEL, SORT_ASC_BUTTON_OPTIONS, SORT_DESC_BUTTON_OPTIONS } from "@/io.ox/office/spreadsheet/view/labels";
import { DiscreteFilterList } from "@/io.ox/office/spreadsheet/view/control/discretefilterlist";

// types ======================================================================

const /*enum*/ SortOrder = { NONE: "none", ASC: "ascending", DESC: "descending" };

// class TableColumnMenu ======================================================

/**
 * A dropdown menu for a header cell in a table range, containing options for
 * sorting and filtering.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view that contains this instance.
 */
export class TableColumnMenu extends CompoundMenu {

    constructor(docView) {

        // base constructor
        super(docView, {
            anchorAlign: "trailing",
            cancelButton: true
        });

        // the model of the table range represented by this menu
        this._tableModel = null;
        // the table column model represented by this menu
        this._columnModel = null;
        // the column index in the table range
        this._tableCol = 0;

        // create the section with the sorting controls
        this.addSection("sort", SORT_HEADER_LABEL);
        this._sortGroup = this.addControl(null, new RadioGroup({ toggleValue: SortOrder.NONE }));
        this._sortGroup.addOption(SortOrder.ASC,  SORT_ASC_BUTTON_OPTIONS);
        this._sortGroup.addOption(SortOrder.DESC, SORT_DESC_BUTTON_OPTIONS);
        this._sortGroup.el.dataset.key = "table/sort/order/list";

        // create the section with the filtering controls
        this.addSection("filter", FILTER_HEADER_LABEL);
        this._filterList = this.addControl(null, new DiscreteFilterList());
        this._filterList.el.dataset.key = "table/filter/discrete/list";

        // create the OK button in the menu footer
        this.addActionButton({
            action: "ok",
            label: OK_LABEL,
            enabled: () => this._isCommitEnabled(),
            click: () => this._commit()
        });

        // clear filter list when closing the menu (release DOM elements)
        this.on("popup:hide", () => {
            this._filterList.clearEntries();
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Collects and inserts the cell entries of a table column into this menu,
     * to be used to define discrete filters. Called by grid pane instances
     * from the click handler of cell dropdown buttons.
     *
     * @param {Address} _address
     *  The address of the header cell of the table column.
     *
     * @param {TableColumnMenuData} userData
     *  The user data stored during creation of the dropdown button associated
     *  to this menu. Contains the properties "tableName" (name of the table
     *  model in the active sheet to be filtered), and "tableCol" (zero-based
     *  column index relative to the table range).
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the menu has been initialized.
     */
    initialize(_address, userData) {

        // store the table model and table column model for usage in private methods
        this._tableModel = userData.tableModel;
        this._tableCol = userData.tableCol;
        this._columnModel = this._tableModel.getColumnModel(this._tableCol);

        // initialize sorting options
        const order = !this._columnModel.isSorted() ? SortOrder.NONE : this._columnModel.isDescending() ? SortOrder.DESC : SortOrder.ASC;
        this._sortGroup.setValue(order);

        // intitialize the filter entries in a background loop (shows a busy spinner for large lists)
        return this._filterList.initialize(this._tableModel, this._tableCol);
    }

    /**
     * Returns a tooltip for cell dropdown buttons attached to this menu.
     */
    getTooltip() {
        return gt.pgettext("filter", "Select values for filtering or sorting");
    }

    // required by `IListMenu` interface
    handleKeyDownEvent() {
        return false;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether the OK button of this dropdown menu is currently enabled
     * (it is not allowed to apply a discrete filter without any visible
     * entries).
     */
    /*private*/ _isCommitEnabled() {
        return !this._filterList.noneChecked;
    }

    /**
     * Executes the controller item that will filter the table column with the
     * current settings in this menu.
     */
    /*private*/ _commit() {

        // the attribute set to be sent as result value
        const attrSet = {};

        // create discrete filter, or clear filter if all entries are selected
        const filtered = !this._filterList.allChecked;
        attrSet.filter = {
            type: filtered ? "discrete" : "none",
            entries: filtered ? this._filterList.value : []
        };

        // create the sorting attributes
        const order = this._sortGroup.getValue();
        attrSet.sort = {
            type: (order === SortOrder.NONE) ? "none" : "value",
            descending: order === SortOrder.DESC
        };

        // execute the controller item
        const value = { tableName: this._tableModel.name, tableCol: this._tableCol, attributes: attrSet };
        jpromise.floating(this.docView.executeControllerItem("table/column/attributes", value));
    }
}
