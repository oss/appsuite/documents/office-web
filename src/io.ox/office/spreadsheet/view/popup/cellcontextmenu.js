/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { EditContextMenu } from "@/io.ox/office/editframework/view/popup/editcontextmenu";
import { hasSupportedProtocol } from "@/io.ox/office/editframework/utils/hyperlinkutils";
import { ImageSourceType } from "@/io.ox/office/drawinglayer/utils/imageutils";

import { MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import {
    INSERT_HEADER_LABEL, OPEN_HYPERLINK_LABEL, EDIT_HYPERLINK_LABEL, REMOVE_HYPERLINK_LABEL, INSERT_HYPERLINK_OPTIONS,
    INSERT_IMAGE_OPTIONS, INSERT_CHART_OPTIONS, REPLY_COMMENT_OPTIONS, CLEAN_OPTIONS
} from "@/io.ox/office/spreadsheet/view/labels";

import {
    Button, CompoundButton, ColRowOpLabelButton,
    InsertAutoSumButton, InsertFunctionButton, FilterButton, CellProtectionMenuButton, NoteMenuButton
} from "@/io.ox/office/spreadsheet/view/controls";

// class CellContextMenu ======================================================

/**
 * A context menu for spreadsheet cells.
 *
 * @param {GridPane} gridPane
 *  The grid pane that contains this instance.
 */
export class CellContextMenu extends EditContextMenu {

    constructor(gridPane) {

        // base constructor
        super(gridPane.docView, gridPane.$el, {
            // do not show context menu during cell edit mode
            enableKey: "view/editmode/off",
            repaintAlways: true,
            delay: 200
        });

        this.gridPane = gridPane;

        // the address of the current cell this context menu is shown for
        this._address = null;

        // hide context menu if scrolling starts
        this.listenTo(this.docView, "change:scrollpos", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /**
     * Selects the cell that has been clicked with the right mouse button,
     * before the context menu will be shown.
     */
    /*protected override*/ implPrepareContext(sourceEvent) {

        // do not show the cell context menu if a drawing frame has been clicked
        // DOCS-1660: `getCellDataForPagePoint` may return `null` (e.g. all columns are hidden)
        const drawingFrame = this.gridPane.getDrawingFrameForEvent(sourceEvent);
        const cellData = this.gridPane.getCellDataForPagePoint(sourceEvent);
        if (drawingFrame || !cellData) { return false; }

        // store the address of the clicked cell, needed in method `implRepaintMenu`
        this._address = cellData.address;
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // special behavior for OOXML documents
        const ooxml = this.docApp.isOOXML();
        const selectedRanges = this.docView.selectionEngine.getSelectedRanges();
        const multiCols = selectedRanges.colIntervals().merge().size() > 1;
        const multiRows = selectedRanges.rowIntervals().merge().size() > 1;
        const isSingleCell = this.docView.selectionEngine.isSingleCellSelection();
        const isMergedCell = this.docView.mergeCollection.isMergedCell(this._address);
        const containsMerge = this.docView.selectionEngine.hasMergedRangeSelected();

        this.destroyAllControls();

        // add clipboard commands
        this.addClipboardEntries();

        // the URL of a hyperlink covering the active cell (check for supported protocol)
        let cellUrl = this.docView.getCellURL();
        if (!hasSupportedProtocol(cellUrl)) { cellUrl = null; }

        // the URL returned by a formula cell (HYPERLINK function) if available
        let effectiveUrl = this.docView.getEffectiveURL();
        if (!hasSupportedProtocol(effectiveUrl)) { effectiveUrl = null; }

        if (cellUrl || effectiveUrl) {
            this.addSection("hyperlink");

            // add the edit and delete hyperlink buttons for "real" hyperlinks only (not for URLs from HYPERLINK function)
            if (cellUrl) {
                this.addControl("hyperlink/edit/dialog", new Button({ label: EDIT_HYPERLINK_LABEL }));
                this.addControl("hyperlink/delete", new Button({ label: REMOVE_HYPERLINK_LABEL }));
            }

            // show the open hyperlink button for any available hyperlink
            if (effectiveUrl) {
                this.addControl(null, new Button({ label: OPEN_HYPERLINK_LABEL, href: effectiveUrl }));
            }
        }

        this.addSection("actions");

        // sub-menu for all row operations
        const rowMenu = new CompoundButton(this.docView, { autoCloseParent: false, hideDisabled: true, label: multiRows ? gt("Rows") : gt("Row"), anchorBorder: ["right", "left"] });
        this.addControl("view/row/submenu", rowMenu, { visibleKey: "view/row/submenu" });
        rowMenu.addControl("row/insert", new ColRowOpLabelButton(true, false, multiRows));
        rowMenu.addControl("row/delete", new ColRowOpLabelButton(false, false, multiRows));

        // sub-menu for all column operations
        const colMenu = new CompoundButton(this.docView, { autoCloseParent: false, hideDisabled: true, label: multiCols ? gt("Columns") : gt("Column"), anchorBorder: ["right", "left"] });
        this.addControl("view/column/submenu", colMenu, { visibleKey: "view/column/submenu" });
        colMenu.addControl("column/insert", new ColRowOpLabelButton(true, true, multiCols));
        colMenu.addControl("column/delete", new ColRowOpLabelButton(false, true, multiCols));

        // sub-menu for all insert operations (except columns/rows)
        const insertMenu = new CompoundButton(this.docView, { autoCloseParent: false, hideDisabled: true, label: INSERT_HEADER_LABEL, anchorBorder: ["right", "left"] });
        this.addControl("view/insert/submenu", insertMenu, { visibleKey: "view/insert/submenu" });
        insertMenu.addControl("cell/autoformula",       new InsertAutoSumButton({ icon: null }));
        insertMenu.addControl("function/insert/dialog", new InsertFunctionButton({ icon: null }));
        insertMenu.addControl("hyperlink/edit/dialog",  new Button(INSERT_HYPERLINK_OPTIONS));
        insertMenu.addControl("name/insert/dialog",     new Button({ label: gt("Named range") }));
        insertMenu.addControl("image/insert/dialog",    new Button({ value: ImageSourceType.DRIVE, ...INSERT_IMAGE_OPTIONS }));
        insertMenu.addControl("chart/insert",           new Button({ ...INSERT_CHART_OPTIONS, value: "column standard" }));
        insertMenu.addControl("threadedcomment/insert", new Button({ label: /*#. create a new user comment (as noun, sublabel of "Insert" menu) */ gt.pgettext("comment-actions", "Comment") }), { visibleKey: "threadedcomment/hasnothread" });

        // sub-menu for comment threads
        this.addSection("comments");
        if (ooxml) {
            const commentMenu = new CompoundButton(this.docView, { autoCloseParent: false, hideDisabled: true, label: gt("Comment"), anchorBorder: ["right", "left"] });
            this.addControl("view/comment/submenu", commentMenu, { visibleKey: "view/comment/submenu" });
            commentMenu.addControl("threadedcomment/insert",        new Button({ ...REPLY_COMMENT_OPTIONS, icon: null }));
            commentMenu.addControl("threadedcomment/delete/thread", new Button({ label: /*#. delete a user comment */ gt.pgettext("comment-actions", "Delete") }));
        }

        // sub-menu for cell notes
        const noteMenu = new NoteMenuButton(this.docView, { autoCloseParent: false, anchorBorder: ["right", "left"] });
        this.addControl("view/note/submenu", noteMenu, { visibleKey: "view/note/submenu" });

        // table operations (filtering and sorting)
        this.addSection("table");
        this.addControl("table/filter",      new FilterButton({ icon: null }));
        this.addControl("table/sort/dialog", new Button({ label: gt("Sort") }));

        // sub-menu for cell protection (lock cells / hide formula)
        this.addSection("protect");
        const protectMenu = new CellProtectionMenuButton(this.docView, { autoCloseParent: false, anchorBorder: ["right", "left"] });
        this.addControl("view/protect/submenu", protectMenu, { visibleKey: "view/protect/submenu" });

        this.addSection("actions");
        this.addControl("cell/clear/values", new Button(CLEAN_OPTIONS));

        const frozenLabel = this.docView.sheetModel.hasFrozenSplit() ? gt("Unfreeze sheet") : gt("Freeze sheet");
        this.addControl("view/split/frozen", new Button({ label: frozenLabel, value: "toggle" }));

        if (containsMerge || isMergedCell) {
            this.addControl("cell/merge", new Button({ label: gt("Unmerge cells"), value: MergeMode.UNMERGE }));
        } else if (!isSingleCell) {
            this.addControl("cell/merge", new Button({ label: gt("Merge cells"), value: MergeMode.MERGE }));
        }

        this.setToolTip(_.noI18n(effectiveUrl || ""), { top: true });
    }
}
