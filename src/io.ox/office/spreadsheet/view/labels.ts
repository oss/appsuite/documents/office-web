/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { str } from "@/io.ox/office/tk/algorithms";
import type { ControlCaptionOptions } from "@/io.ox/office/tk/dom";
import type { ButtonConfig } from "@/io.ox/office/tk/controls";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { DOM_TEXT_EDIT_ALERT } from "@/io.ox/office/baseframework/app/extensionregistry";
import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { type TextFrameworkDialogConfig, FORMAT_PAINTER_LABEL } from "@/io.ox/office/textframework/view/labels";

import { type Address, stringifyCol, stringifyRow } from "@/io.ox/office/spreadsheet/utils/address";
import { MAX_CHANGE_COLS_COUNT, MAX_CHANGE_ROWS_COUNT, MAX_FILL_CELL_COUNT, MAX_MERGED_RANGES_COUNT, MAX_SORT_LINES_COUNT } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { MAX_MATRIX_SIZE } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import type { FuncCategorySpec, ReqFileFormatRecord } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";

// re-exports =================================================================

export * from "@/io.ox/office/textframework/view/labels";

// types ======================================================================

export type YellMessageCode = "readonly" | "image:insert" | keyof typeof RESULT_MESSAGES;

/**
 * Optional parameters for generating column, row, or cell address labels.
 */
export interface AddressLabelOptions {

    /**
     * If set to `true`, an appropriate prefix word will be added, e.g. "Row 1"
     * instead of "1", or "Cell A1" instead of "A1".
     */
    prefix?: boolean;

    /**
     * If set to `true`, the label will contain zero-with spaces between column
     * letters for improved screen reader support (e.g. reads "Cell A-T-one"
     * for cell AT1 instead of "Cell at one".
     */
    a11y?: boolean;
}

export interface FormatCategoryLabel {
    category: FormatCategory;
    label: string;
}

/**
 * Additional configuration options for `BaseDialog` used in Spreadsheet.
 */
export interface SpreadsheetDialogConfig extends TextFrameworkDialogConfig {

    /**
     * Specifies when to close a dialog automatically.
     *
     * - "activeSheet": If active sheet will be deleted or hidden by another
     *   client.
     * - "activeCell": Like "activeSheet", but additionally if the active cell
     *   will be hidden.
     */
    autoCloseScope?: "activeSheet" | "activeCell";
}

// constants ==================================================================

// header labels --------------------------------------------------------------

/**
 * Header label for column/row operations.
 */
//#. Header label insert/delete/resize rows and columns in a spreadsheet
export const COL_ROW_HEADER_LABEL = gt.pgettext("menu-title", "Rows/Columns");

/**
 * Header label for cell operations.
 */
//#. Header label for operations to change cells in a spreadsheet
export const CELL_HEADER_LABEL = gt.pgettext("menu-title", "Cells");

/**
 * Header label for generic data operations.
 */
//#. Header label for sorting, filtering, and other data operations
export const DATA_HEADER_LABEL = gt.pgettext("menu-title", "Data");

/**
 * Header label for data filter operations.
 */
//#. Header label for data filter options
export const FILTER_HEADER_LABEL = gt.pgettext("menu-title", "Filter");

/**
 * Header label for data sort operations.
 */
//#. Header label for data sort options
export const SORT_HEADER_LABEL = gt.pgettext("menu-title", "Sort");

// control labels -------------------------------------------------------------

/**
 * Label for an "Insert sheet" button.
 */
export const INSERT_SHEET_LABEL = gt("Insert sheet");

/**
 * Label for a "Rename sheet" button.
 */
export const RENAME_SHEET_LABEL = gt("Rename sheet");

/**
 * Label for a "Copy sheet" button.
 */
export const COPY_SHEET_LABEL = gt("Copy sheet");

/**
 * Label for a "Delete sheet" button.
 */
export const DELETE_SHEET_LABEL = gt("Delete sheet");

/**
 * Label for a "Unprotect sheet" button.
 */
export const UNPROTECT_SHEET_LABEL = gt("Unprotect sheet");

/**
 * Label for a "Protect sheet" button.
 */
export const PROTECT_SHEET_LABEL = gt("Protect sheet");

/**
 * Label for a "Show sheet" button.
 */
//#. make a sheet visible that is currently hidden
export const SHOW_SHEET_LABEL = gt("Show sheet");

/**
 * Label for a "Hide sheet" button.
 */
export const HIDE_SHEET_LABEL = gt("Hide sheet");

/**
 * Label for a "Reorder sheets" button.
 */
export const REORDER_SHEETS_LABEL = gt("Reorder sheets");

/**
 * Label for a "Show all hidden sheets" button.
 */
export const UNHIDE_SHEETS_LABEL = gt("Unhide sheets");

/**
 * Label for a line-break toggle button.
 */
//#. check box: automatic word wrapping (multiple text lines) in spreadsheet cells
export const LINEBREAK_LABEL = gt("Automatic word wrap");

/**
 * Label for a decimal decrease button.
 */
export const DECIMAL_DECREASE = gt("Delete decimal place");

/**
 * Label for a decimal increase button.
 */
export const DECIMAL_INCREASE = gt("Add decimal place");

/**
 * An map containing GUI labels for all function categories.
 */
export const FUNCTION_CATEGORY_LABELS: RoRecord<FuncCategorySpec, string> = {
    complex: /*#. Category for spreadsheet functions: complex numbers (e.g. IMSUM, IMSIN, IMPOWER) */ gt.pgettext("function-category", "Complex numbers"),
    conversion: /*#. Category for spreadsheet functions: conversion (e.g. DEC2HEX, ROMAN, CONVERT) */ gt.pgettext("function-category", "Conversion"),
    database: /*#. Category for spreadsheet functions: database-like (e.g. DSUM, DCOUNT) */ gt.pgettext("function-category", "Database"),
    datetime: /*#. Category for spreadsheet functions: date/time calculation (e.g. TODAY, WEEKDAY) */ gt.pgettext("function-category", "Date and time"),
    engineering: /*#. Category for spreadsheet functions: engineering (e.g. BESSELI, ERF) */ gt.pgettext("function-category", "Engineering"),
    financial: /*#. Category for spreadsheet functions: financial functions (e.g. ACCRINT, COUPDAYS) */ gt.pgettext("function-category", "Financial"),
    information: /*#. Category for spreadsheet functions: information about cells (e.g. ISNUMBER, ISTEXT) */ gt.pgettext("function-category", "Information"),
    logical: /*#. Category for spreadsheet functions: logical functions (e.g. AND, OR, NOT, IF) */ gt.pgettext("function-category", "Logical"),
    math: /*#. Category for spreadsheet functions: common mathematical functions (e.g. SUM, SIN, ROUND) */ gt.pgettext("function-category", "Mathematical"),
    matrix: /*#. Category for spreadsheet functions: matrix functions (e.g. MMULT, MINVERSE, MDETERM) */ gt.pgettext("function-category", "Matrix"),
    reference: /*#. Category for spreadsheet functions: cell references (e.g. ADDRESS, HLOOKUP, COLUMN) */ gt.pgettext("function-category", "Reference"),
    statistical: /*#. Category for spreadsheet functions: statistical functions (e.g. COUNT, STDEV, HYPGEOMDIST) */ gt.pgettext("function-category", "Statistical"),
    text: /*#. Category for spreadsheet functions: text functions (e.g. LEN, MID, REPLACE) */ gt.pgettext("function-category", "Text"),
    web: /*#. Category for spreadsheet functions: web functions (e.g. ENCODEURL, HYPERLINK, WEBSERVICE) */ gt.pgettext("function-category", "Web")
};

/**
 * The label of the "Custom" entry in the "Number Format Code" dropdown list.
 */
//#. Spreadsheet: button label to select custom number format in a modal dialog
export const CUSTOM_FORMAT_LABEL = gt.pgettext("number-format", "Custom ...");

/**
 * The label of an additional entry in the "Number Format Code" dropdown list
 * for custom currencies.
 */
//#. Spreadsheet: button label to select custom currency format in a modal dialog
export const CUSTOM_CURRENCY_LABEL = gt.pgettext("number-format", "Currency ...");

// control options ------------------------------------------------------------

/**
 * Standard options for a line-break toggle button.
 */
export const LINEBREAK_BUTTON_OPTIONS = {
    icon: "png:insert-linebreak",
    tooltip: LINEBREAK_LABEL,
    toggle: true,
    dropDownVersion: { label: LINEBREAK_LABEL }
} satisfies ButtonConfig;

/**
 * Standard options for a "Format painter" button.
 */
export const FORMAT_PAINTER_BUTTON_OPTIONS = {
    icon: "png:format-painter",
    tooltip: FORMAT_PAINTER_LABEL,
    toggle: true,
    dropDownVersion: { label: FORMAT_PAINTER_LABEL }
} satisfies ButtonConfig;

/**
 * Standard options for "Sort ascending" buttons.
 */
export const SORT_ASC_BUTTON_OPTIONS = {
    icon: "png:sort-up",
    label: gt.pgettext("sort", "Ascending"),
    tooltip: gt.pgettext("sort", "Sort from smallest to largest value")
} satisfies ControlCaptionOptions;

/**
 * Standard options for "Sort descending" buttons.
 */
export const SORT_DESC_BUTTON_OPTIONS = {
    icon: "png:sort-down",
    label: gt.pgettext("sort", "Descending"),
    tooltip: gt.pgettext("sort", "Sort from largest to smallest value")
} satisfies ControlCaptionOptions;

/**
 * Standard options for "Custom sort" buttons.
 */
export const SORT_CUSTOM_BUTTON_OPTIONS = {
    //#. sorting: label of the button opening a menu with custom sort options
    label: gt.pgettext("sort", "Custom"),
    tooltip: gt.pgettext("sort", "Settings for sorting")
} satisfies ControlCaptionOptions;

/**
 * Standard options for "Clear contents" buttons.
 */
export const CLEAN_OPTIONS = {
    label: gt("Clear contents")
    // TODO: missing tooltip
} satisfies ControlCaptionOptions;

// style sets for list controls -----------------------------------------------

/**
 * An list containing the default format codes, and the GUI labels for all
 * predefined number format categories.
 */
export const FORMAT_CATEGORY_LIST: FormatCategoryLabel[] = [
    { category: FormatCategory.STANDARD,   label: /*#. number format category in spreadsheets: no special format, show numbers as they are */ gt.pgettext("number-format", "General") },
    { category: FormatCategory.NUMBER,     label: /*#. number format category in spreadsheets: numbers with specific count of decimal places */ gt.pgettext("number-format", "Number") },
    { category: FormatCategory.CURRENCY,   label: /*#. number format category in spreadsheets: number with currency symbol */ gt.pgettext("number-format", "Currency") },
    { category: FormatCategory.ACCOUNTING, label: /*#. number format category in spreadsheets: number with currency symbol, and left aligned minus sign */ gt.pgettext("number-format", "Accounting") },
    { category: FormatCategory.DATE,       label: /*#. number format category in spreadsheets: date formats */ gt.pgettext("number-format", "Date") },
    { category: FormatCategory.TIME,       label: /*#. number format category in spreadsheets: time formats */ gt.pgettext("number-format", "Time") },
    { category: FormatCategory.DATETIME,   label: /*#. number format category in spreadsheets: combined date/time formats */ gt.pgettext("number-format", "Date and time") },
    { category: FormatCategory.PERCENT,    label: /*#. number format category in spreadsheets: numbers with percent sign */ gt.pgettext("number-format", "Percentage") },
    { category: FormatCategory.SCIENTIFIC, label: /*#. number format category in spreadsheets: scientific notation (e.g. 1.23E+10) */ gt.pgettext("number-format", "Scientific") },
    { category: FormatCategory.FRACTION,   label: /*#. number format category in spreadsheets: fractional numbers (e.g. 3 1/4) */ gt.pgettext("number-format", "Fraction") },
    { category: FormatCategory.TEXT,       label: /*#. number format category in spreadsheets: text only */ gt.pgettext("number-format", "Text") },
    { category: FormatCategory.CUSTOM,     label: /*#. number format category in spreadsheets: all user-defined number formats */ gt.pgettext("number-format", "Custom") }
];

// other spreadsheet specific labels ------------------------------------------

/**
 * The "Calculating" label for the toppane while calculating formulas.
 */
//#. A label shown in the top-bar of a spreadsheet while calculating the cell formulas
export const CALCULATING_LABEL = gt("Calculating");

/**
 * Returns the name of the passed column together with the word "Column", e.g.
 * "Column A".
 *
 * @param col
 *  The zero-based column index to be converted to a column label.
 *
 * @returns
 *  The label for the specified column.
 */
export function getColLabel(col: number, options?: AddressLabelOptions): string {
    const name = options?.a11y ? stringifyCol(col).split("").join("\u200b") : stringifyCol(col);
    //#. UI label for a spreadsheet column, e.g. "Column A", "Column B", etc.
    //#. %1$s is the name of the column
    return options?.prefix ? gt("Column %1$s", name) : _.noI18n(name);
}

/**
 * Returns the name of the passed row together with the word "Row", e.g.
 * "Row 1".
 *
 * @param row
 *  The zero-based row index to be converted to a row label.
 *
 * @returns
 *  The label for the specified row.
 */
export function getRowLabel(row: number, options?: AddressLabelOptions): string {
    const name = stringifyRow(row);
    //#. UI label for a spreadsheet row, e.g. "Row 1", "Row 2", etc.
    //#. %1$s is the name of the row
    return options?.prefix ? gt("Row %1$s", name) : _.noI18n(name);
}

/**
 * Returns the name of the passed column or row together with the word "Column"
 * or "Row" respectively, e.g. "Column A", or "Row 1".
 *
 * @param index
 *  The zero-based column or row index to be converted to a label.
 *
 * @param columns
 *  Whether to return a column label (true), or a row label (false).
 *
 * @returns
 *  The label for the specified column or row.
 */
export function getColRowLabel(index: number, columns: boolean, options?: AddressLabelOptions): string {
    return columns ? getColLabel(index, options) : getRowLabel(index, options);
}

/**
 * Returns the name of the passed cell together with the word "Cell", e.g.
 * "Cell A1".
 *
 * @param address
 *  The address of a cell to be converted to a label.
 *
 * @returns
 *  The label for the specified cell address.
 */
export function getCellLabel(address: Address, options?: AddressLabelOptions): string {
    const a11y = options?.a11y;
    let name = getColLabel(address.c, { a11y });
    if (a11y) { name += "\u200b"; }
    name += getRowLabel(address.r, { a11y });
    //#. UI label for a spreadsheet cell, e.g. "Cell A1", "Cell XY42", etc.
    //#. %1$s is the name of the cell
    return options?.prefix ? gt("Cell %1$s", name) : _.noI18n(name);
}

/**
 * Returns the name of the passed series together with the word "Series", e.g.
 * "Series 1".
 *
 * @param seriesIndex
 *  The zero-based series index to be converted to a series label.
 *
 * @returns
 *  The label for the specified series.
 */
export function getSeriesLabel(seriesIndex: number): string {
    //#. A data series in a chart object (a sequence of multiple data points with a title)
    //#. %1$d is the numeric index of the series (e.g. "Series 1", "Series 2", etc.)
    //#. This label should match the default title for data series in other spreadsheet
    //#. applications (OpenOffice/LibreOffice Calc, Microsoft Excel).
    return gt("Series %1$d", seriesIndex + 1);
}

/**
 * Returns the localized name of the specified function category.
 *
 * @param category
 *  The internal identifier of a function category.
 *
 * @returns
 *  The localized name of the specified function category.
 */
export function getCategoryLabel(category: FuncCategorySpec): string {

    // return labels for valid categories
    if (category in FUNCTION_CATEGORY_LABELS) {
        return FUNCTION_CATEGORY_LABELS[category];
    }

    // assert an unknown category, return it capitalized
    globalLogger.error(`$badge{SpreadsheetLabels} getCategoryLabel: unknown function category: "${category}"`);
    return _.noI18n(str.capitalizeFirst(category));
}

// alert messages -------------------------------------------------------------

/**
 * Codes for various message texts used in alert boxes.
 */
export const RESULT_MESSAGES = {

    "sheet:name:empty":
        gt("The sheet name must not be empty. Please enter a name."),

    "sheet:name:invalid":
        gt("This sheet name contains invalid characters. Please ensure that the name does not contain / \\ * ? : [ or ], and does neither start nor end with an apostrophe."),

    "sheet:name:used":
        gt("This sheet name is already used. Please enter another name."),

    "sheet:locked":
        gt("You cannot use this command on a protected sheet."),

    "cols:selection":
        gt("Please select only entire columns, or just some cells in the sheet."),

    "cols:overflow":
        gt("It is not possible to modify more than %1$d columns at the same time.", MAX_CHANGE_COLS_COUNT),

    "rows:selection":
        gt("Please select only entire rows, or just some cells in the sheet."),

    "rows:overflow":
        gt("It is not possible to modify more than %1$d rows at the same time.", MAX_CHANGE_ROWS_COUNT),

    "cells:locked":
        gt("Protected cells cannot be modified."),

    "cells:overflow":
        gt("It is not possible to modify more than %1$d cells at the same time.", MAX_FILL_CELL_COUNT),

    "cells:pushoff":
        gt("It is not possible to insert new cells because non-empty cells would be pushed off the end of the sheet."),

    "merge:overlap":
        //#. Warning text: trying to merge multiple cell ranges which overlap each other
        gt("Overlapping ranges cannot be merged."),

    "merge:overflow":
        //#. Warning text: trying to merge too many cell ranges at the same time
        //#. %1$d is the maximum number of cell ranges that can be merged at a time
        gt("It is not possible to merge more than %1$d ranges at the same time.", MAX_MERGED_RANGES_COUNT),

    "autofill:merge:overlap":
        //#. Warning text: trying to trigger autofill over only a part of at least one merged cell
        gt("Autofill over merged cells is not allowed."),

    "name:invalid":
        // #. Warning text: the user-defined name of a cell range contains invalid characters
        gt("This name contains invalid characters. Please enter another name."),

    "name:address":
        // #. Warning text: the user-defined name of a cell range cannot look like a cell, e.g. "A1", or "R1C1"
        gt("A cell address cannot be used as name. Please enter another name."),

    "name:used":
        // #. Warning text: the user-defined name of a cell range is already used
        gt("This name is already used. Please enter another name."),

    "table:multiselect":
        //#. Warning text: tried to create a table range from a cell multi-selection
        gt("Table cannot be created on multiple cell ranges."),

    "table:overlap":
        //#. Warning text: tried to create a table range above cells that are already filtered
        gt("Table cannot be created on another filtered range."),

    "table:move":
        //#. Warning text: tried to move a few cells of a table range to the right, or down, etc.
        gt("Parts of a table cannot be moved."),

    "table:change":
        //#. Warning text: tried to change (overwrite, merge, etc.) parts of a table range
        gt("Parts of a table cannot be changed."),

    "table:header:change":
        //#. Warning text: tried to change (overwrite, merge, etc.) the header cells of a table range
        gt("The header cells of a table cannot be changed."),

    "autofilter:multiselect":
        //#. Warning text: tried to create an auto filter from a cell multi-selection
        gt("Filter cannot be applied to multiple cell ranges."),

    "autofilter:overlap":
        //#. Warning text: tried to create an auto filter above cells that are already filtered
        gt("Filter cannot be applied to another filtered range."),

    "autofilter:blank":
        //#. Warning text: tried to filter an empty cell range
        gt("Filtering cannot be performed on blank cells."),

    "autofilter:move":
        //#. Warning text: tried to move a few cells of an auto filter to the right, or down, etc.
        gt("Parts of the filter cannot be moved."),

    "sort:multiselect":
        //#. Warning text: tried to sort data of a cell multi-selection
        gt("Sorting cannot be performed on multiple cell ranges."),

    "sort:blank":
        //#. Warning text: tried to sort an empty cell range
        gt("Sorting cannot be performed on blank cells."),

    "sort:overflow":
        //#. Warning text: tried to sort too many rows or columns
        gt("Sorting cannot be performed on more than %1$d columns or rows.", MAX_SORT_LINES_COUNT),

    "sort:merge:overlap":
        //#. Warning text: trying to sort over merged cells is not allowed
        gt("Sorting over merged cells is not allowed."),

    "search:nothing":
        //#. Information text: no matching text in document
        gt("No results for your search."),

    "search:finished":
        //#. Information text: no more matches in document (after something has been found)
        gt("No more results for your search."),

    "replace:nothing":
        //#. Information text: search&replace did not find anything to replace
        gt("Nothing to replace."),

    "formula:invalid":
        //#. Warning text: a spreadsheet formula entered in a cell or in a dialog text field contains an error
        gt("The formula contains an error."),

    "formula:matrix:change":
        gt("Parts of a matrix formula cannot be changed."),

    "formula:matrix:insert":
        gt("It is not possible to insert cells into a matrix formula."),

    "formula:matrix:delete":
        gt("Parts of a matrix formula cannot be deleted."),

    "formula:matrix:size":
        //#. Warning text: new matrix formulas must not exceed specific height and width
        gt("It is not possible to insert a matrix formula with more than %1$d elements.", MAX_MATRIX_SIZE),

    "formula:matrix:merged":
        gt("It is not possible to insert a matrix formula over merged cells."),

    "validation:source":
        //#. Warning text: the values for a drop-down list attached to a spreadsheet cell could not be found
        gt("No source data available for the drop-down list."),

    "copy:overflow":
        gt("It is not possible to copy more than %1$d cells at the same time.", MAX_FILL_CELL_COUNT),

    "paste:overflow":
        gt("It is not possible to paste more than %1$d cells at the same time.", MAX_FILL_CELL_COUNT),

    "paste:outside":
        gt("It is not possible to paste outside of the valid sheet area."),

    "paste:locked":
        gt("Pasting into protected cells is not allowed."),

    "paste:ranges:unfit":
        gt("You cannot paste this here because the copy area and paste area are not the same size. Select just one cell in the paste area or an area with the same size, and try pasting again."),

    "drop:unsupported":
        //#. Warning text: file type dropped from outside into the document not supported
        gt("File type not supported."),

    "drawing:text:editing":
        DOM_TEXT_EDIT_ALERT,

    "drawing:insert:locked":
        gt("Objects cannot be inserted into a protected sheet."),

    "drawing:delete:locked":
        gt("Objects on a protected sheet cannot be deleted."),

    "drawing:change:locked":
        gt("Objects on a protected sheet cannot be changed."),

    "note:insert:locked": {
        ooxml: gt("Notes cannot be inserted into a protected sheet."),
        odf: gt("Comments cannot be inserted into a protected sheet.")
    },

    "note:delete:locked": {
        ooxml: gt("Notes on a protected sheet cannot be deleted."),
        odf: gt("Comments on a protected sheet cannot be deleted.")
    },

    "note:change:locked": {
        ooxml: gt("Notes on a protected sheet cannot be changed."),
        odf: gt("Comments on a protected sheet cannot be changed.")
    },

    "note:search:finished": {
        ooxml: gt("No more notes found."),
        odf: gt("No more comments found.")
    },

    "comment:search:docbeginning":
        gt("The search continues from the beginning of this document.")

} as const satisfies Dict<ReqFileFormatRecord<string>>;
