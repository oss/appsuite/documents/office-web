/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import {
    Button, CheckBox, LengthField, CompoundButton, CompoundCheckBox,
    FillColorPicker, BorderFlagsPicker, BorderStylePicker, TableStylePicker
} from "@/io.ox/office/editframework/view/editcontrols";

import {
    CELL_BORDERS_LABEL, BORDER_STYLE_LABEL, HIDE_SHEET_LABEL, SHOW_SHEET_LABEL,
    UNPROTECT_SHEET_LABEL, PROTECT_SHEET_LABEL
} from "@/io.ox/office/spreadsheet/view/labels";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/view/editcontrols";
export * from "@/io.ox/office/spreadsheet/view/control/alignmentpicker";
export * from "@/io.ox/office/spreadsheet/view/control/formatcategorygroup";
export * from "@/io.ox/office/spreadsheet/view/control/formatcategorypicker";
export * from "@/io.ox/office/spreadsheet/view/control/formatcodepicker";
export * from "@/io.ox/office/spreadsheet/view/control/cellbordercolorpicker";
export * from "@/io.ox/office/spreadsheet/view/control/cellstylepicker";
export * from "@/io.ox/office/spreadsheet/view/control/mergecellspicker";
export * from "@/io.ox/office/spreadsheet/view/control/namesmenubutton";
export * from "@/io.ox/office/spreadsheet/view/control/sortmenubutton";
export * from "@/io.ox/office/spreadsheet/view/control/activesheetgroup";
export * from "@/io.ox/office/spreadsheet/view/control/activesheetlist";
export * from "@/io.ox/office/spreadsheet/view/control/subtotallist";

// class ColRowOpIconButton ===================================================

// icons and tooltips for all column/row operations
const COL_ROW_ICON_OPTIONS = [
    /* 0 = 0b00 = delete row */    { icon: "png:table-delete-row",    tooltip: gt("Delete selected rows") },
    /* 1 = 0b01 = delete column */ { icon: "png:table-delete-column", tooltip: gt("Delete selected columns") },
    /* 2 = 0b10 = insert row */    { icon: "png:table-insert-row",    tooltip: gt("Insert row") },
    /* 3 = 0b10 = insert column */ { icon: "png:table-insert-column", tooltip: gt("Insert column") }
];

/**
 * A button control consisting of a single icon used to insert or delete
 * columns or rows.
 */
export class ColRowOpIconButton extends Button {
    constructor(insert, columns) {
        // build the array index into COL_ROW_ICON_OPTIONS according to the passed boolean flags
        super(COL_ROW_ICON_OPTIONS[2 * insert + columns]);
    }
}

// class ColRowOpLabelButton ==================================================

// labels for all column/row operations
const COL_ROW_LABELS = [
    /* 0 = 0b000 = delete one row */    gt("Delete row"),
    /* 1 = 0b001 = delete rows */       gt("Delete rows"),
    /* 2 = 0b010 = delete one column */ gt("Delete column"),
    /* 3 = 0b011 = delete columns */    gt("Delete columns"),
    /* 4 = 0b100 = insert one row */    gt("Insert row"),
    /* 5 = 0b101 = insert rows */       gt("Insert rows"),
    /* 6 = 0b110 = insert one column */ gt("Insert column"),
    /* 7 = 0b111 = insert columns */    gt("Insert columns")
];

/**
 * A button control consisting of a label used to insert or delete columns or
 * rows.
 */
export class ColRowOpLabelButton extends Button {
    constructor(insert, columns, plural) {
        // build the array index into COL_ROW_LABELS according to the passed boolean flags
        super({ label: COL_ROW_LABELS[4 * insert + 2 * columns + plural] });
    }
}

// class ColRowSizeField ======================================================

/**
 * A unit field control used to manipulate the width of a column, or the height
 * of a row.
 */
export class ColRowSizeField extends LengthField {
    constructor(docView, columns) {
        super({
            width: "8.5em",
            tooltip: columns ? gt("Column width") : gt("Row height"),
            icon: columns ? "png:table-resize-column" : "png:table-resize-row",
            min: 0,
            max: docView.docModel.fontMetrics.getMaxColRowSizeHmm(columns),
            lazyCommit: true, // do not commit immediately when using the spin buttons
            shrinkVisible: false
        });
    }
}

// class CellFillColorPicker ==================================================

/**
 * A picker control for the cell background color.
 */
export class CellFillColorPicker extends FillColorPicker {
    constructor(docView) {
        super(docView, { label: null, tooltip: gt("Fill color") });
    }
}

// class CellBorderFlagsPicker ================================================

/**
 * The selector for cell borders. Hides specific options in the dropdown menu,
 * according to the current cell selection.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class CellBorderFlagsPicker extends BorderFlagsPicker {
    constructor(docView) {
        super(docView, {
            tooltip: CELL_BORDERS_LABEL,
            showInnerH() { return docView.selectionEngine.hasMultipleRowsSelected(); },
            showInnerV() { return docView.selectionEngine.hasMultipleColumnsSelected(); },
            dropDownVersion: { label: CELL_BORDERS_LABEL }
        });
    }
}

// class CellBorderStylePicker ================================================

// predefined border styles (line width, line style) supported by OOXML
const BORDER_OOXML_PRESET_STYLES = [
    { value: "single:hair",       label: /*#. border line style (width 0.5 pixels, solid) */ gt.pgettext("borders", "Hair line"),                         style: "solid",      width: 0 },
    { value: "single:thin",       label: /*#. border line style (width 1 pixel, solid) */ gt.pgettext("borders", "Thin line"),                            style: "solid",      width: 1 },
    { value: "dashed:thin",       label: /*#. border line style (width 1 pixel, dashed) */ gt.pgettext("borders", "Dashed thin line"),                    style: "dashed",     width: 1 },
    { value: "dotted:thin",       label: /*#. border line style (width 1 pixel, dotted) */ gt.pgettext("borders", "Dotted thin line"),                    style: "dotted",     width: 1 },
    { value: "dashDot:thin",      label: /*#. border line style (width 1 pixel, dash-dot) */ gt.pgettext("borders", "Dot-and-dash thin line"),            style: "dashDot",    width: 1 },
    { value: "dashDotDot:thin",   label: /*#. border line style (width 1 pixel, dash-dot-dot) */ gt.pgettext("borders", "Two-dots-and-dash thin line"),   style: "dashDotDot", width: 1 },
    { value: "single:medium",     label: /*#. border line style (width 2 pixels, solid) */ gt.pgettext("borders", "Medium line"),                         style: "solid",      width: 2 },
    { value: "dashed:medium",     label: /*#. border line style (width 2 pixels, dashed) */ gt.pgettext("borders", "Dashed medium line"),                 style: "dashed",     width: 2 },
    { value: "dashDot:medium",    label: /*#. border line style (width 2 pixel, dash-dot) */ gt.pgettext("borders", "Dot-and-dash medium line"),          style: "dashDot",    width: 2 },
    { value: "dashDotDot:medium", label: /*#. border line style (width 2 pixel, dash-dot-dot) */ gt.pgettext("borders", "Two-dots-and-dash medium line"), style: "dashDotDot", width: 2 },
    { value: "single:thick",      label: /*#. border line style (width 3 pixels, solid) */ gt.pgettext("borders", "Thick line"),                          style: "solid",      width: 3 },
    { value: "double:thick",      label: /*#. border line style (width 3 pixels, double line) */ gt.pgettext("borders", "Double line"),                   style: "solid",      width: 1, count: 2 }
];

// all available simple border line styles for ODF files
const BORDER_ODF_LINE_STYLES = [
    { value: "single", label: /*#. Border line style: a solid single line */ gt.pgettext("borders", "Single"),  style: "solid" },
    { value: "double", label: /*#. Border line style: a solid double line */ gt.pgettext("borders", "Double"),  style: "solid", count: 2 },
    { value: "dotted", label: /*#. Border line style: a dotted single line */ gt.pgettext("borders", "Dotted"), style: "dotted" },
    { value: "dashed", label: /*#. Border line style: a dashed single line */ gt.pgettext("borders", "Dashed"), style: "dashed" }
];

/**
 * A picker control for cell border styles. The available list entries will
 * depend on the current file format.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class CellBorderStylePicker extends BorderStylePicker {
    constructor(docView) {

        const PRESET_STYLES = docView.docApp.isOOXML() ? BORDER_OOXML_PRESET_STYLES : BORDER_ODF_LINE_STYLES;

        super(PRESET_STYLES, {
            label: null,
            dropDownVersion: { label: BORDER_STYLE_LABEL }
        });
    }
}

// class CellProtectionMenuButton =============================================

/**
 * A compound button with options to change the cell protection settings.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class CellProtectionMenuButton extends CompoundButton {
    constructor(docView, config) {

        super(docView, {
            icon: "bi:lock-fill",
            label: gt("Cell protection"),
            tooltip: gt("Change cell protection for the selected cells"),
            hideDisabled: true,
            ...config
        });

        this.addControl("cell/unlocked",      new Button({ label: gt("Unlock cell"),    tooltip: gt("Unlock the cell in protected sheets") }));
        this.addControl("cell/locked",        new Button({ label: gt("Lock cell"),      tooltip: gt("Lock the cell in protected sheets") }));
        this.addControl("cell/unhideformula", new Button({ label: gt("Unhide formula"), tooltip: gt("Unhide the formula in protected sheets") }));
        this.addControl("cell/hideformula",   new Button({ label: gt("Hide formula"),   tooltip: gt("Hide the formula in protected sheets") }));
    }
}

// class NoteMenuButton =======================================================

/**
 * A compound button with actions for cell notes.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class NoteMenuButton extends CompoundButton {

    constructor(docView, config) {

        const ooxml = docView.docApp.isOOXML();
        const label = ooxml ? gt("Note") : gt("Comment");
        const tooltip = ooxml ? gt("Note-Handling") : gt("Comment-Handling");

        super(docView, {
            label,
            tooltip,
            hideDisabled: true,
            ...config
        });

        const showHideTooltip = ooxml ? gt("Toggle visibility of selected note") : gt("Toggle visibility of selected comment");
        const deleteTooltip = ooxml ? gt("Delete selected note") : gt("Delete selected comment");

        this.addControl("note/visible", new Button({ label: gt("Show/Hide"), tooltip: showHideTooltip, toggle: true }));
        this.addControl("note/delete",  new Button({ label: gt("Delete"), tooltip: deleteTooltip }));
    }
}

// class DynamicSplitCheckBox =================================================

/**
 * A check box control for a dynamic (movable) column/row split in a sheet.
 */
export class DynamicSplitCheckBox extends CheckBox {
    constructor(config) {
        super({
            //#. check box: split a spreadsheet into 2 or 4 different parts that can be scrolled independently
            label: gt.pgettext("sheet-split", "Split sheet"),
            tooltip: gt.pgettext("sheet-split", "Split the sheet above and left of the current cursor position"),
            ...config
        });
    }
}

// class FrozenSplitButton ====================================================

/**
 * A button for activating frozen columns and/or rows in a sheet.
 */
class FrozenSplitButton extends Button {
    constructor(cols, rows, label, tooltip) {
        super({
            label,
            tooltip,
            value: { cols, rows },
            valueSerializer: () => `${cols},${rows}`
        });
    }
}

// class FrozenSplitCheckBox ==================================================

/**
 * A check box control with a combined drop-down menu with different settings
 * for frozen columns and rows in a sheet.
 */
export class FrozenSplitCheckBox extends CompoundCheckBox {
    constructor(docView, config) {

        super(docView, {
            //#. check box: split a spreadsheet into 2 or 4 different parts, the leading (left/upper) parts are frozen and cannot be scrolled
            label: gt.pgettext("sheet-split", "Freeze sheet"),
            tooltip: gt.pgettext("sheet-split", "Freeze the rows above and the columns left of the cursor"),
            anchorBorder: ["right", "left", "bottom", "top"],
            ...config
        });

        this.addControl("view/split/frozen/fixed", new FrozenSplitButton(0, 1, gt("Freeze first row"),            gt("Freeze the first visible row")));
        this.addControl("view/split/frozen/fixed", new FrozenSplitButton(1, 0, gt("Freeze first column"),         gt("Freeze the first visible column")));
        this.addControl("view/split/frozen/fixed", new FrozenSplitButton(1, 1, gt("Freeze first row and column"), gt("Freeze the first visible row and the first visible column")));
    }
}

// class ShowSheetButton ======================================================

/**
 * A toggle button that allows to show or hide a sheet. The label of the button
 * will change automatically according to its current state.
 */
export class ShowSheetButton extends Button {

    constructor() {
        super({ toggle: true, highlight: false });
    }

    /*protected override*/ implUpdate(state) {
        super.implUpdate(state);
        this.setLabel(state ? HIDE_SHEET_LABEL : SHOW_SHEET_LABEL);
    }
}

// class LockSheetButton ======================================================

/**
 * A toggle button that allows to lock or unlock a sheet. The label of the
 * button will change automatically according to its current state.
 */
export class LockSheetButton extends Button {

    constructor() {
        super({ toggle: true, highlight: false });
    }

    /*protected override*/ implUpdate(state) {
        super.implUpdate(state);
        this.setLabel(state ? UNPROTECT_SHEET_LABEL : PROTECT_SHEET_LABEL);
    }
}

// class InsertAutoSumButton ==================================================

/**
 * A button that allows to insert SUM formulas into the selected cells.
 */
export class InsertAutoSumButton extends Button {
    constructor(config) {
        super({
            icon: "png:auto-sum",
            label: gt("Sum"),
            //#. automatically create a SUM function for selected cells
            tooltip: gt("Sum automatically"),
            value: "SUM",
            ...config
        });
    }
}

// class InsertFunctionButton =================================================

/**
 * A button that allows to insert a new function into the selected cell.
 */
export class InsertFunctionButton extends Button {
    constructor(config) {
        super({
            icon: "png:functions",
            label: gt("Function"),
            //#. Insert a function at the current cursor location
            tooltip: gt("Insert function at cursor location"),
            ...config
        });
    }
}

// class FilterButton =========================================================

/**
 * A button that allows to toggle filtering drop-downs on a cell range.
 */
export class FilterButton extends Button {
    constructor(config) {
        super({
            icon: "png:filter",
            //#. Button label: filter a cell range
            label: gt.pgettext("filter", "Filter"),
            tooltip: gt("Filter the selected cells"),
            toggle: true,
            ...config
        });
    }
}

// class RefreshButton ========================================================

/**
 * A button that allows to refresh filtering and sorting in a table range.
 */
export class RefreshButton extends Button {
    constructor(config) {
        super({
            icon: "bi:arrow-repeat",
            //#. Button label: refresh a filtered cell range
            label: gt.pgettext("filter", "Reapply"),
            tooltip: gt("Reapply the filter in the selected cells"),
            ...config
        });
    }
}

// class SheetTableStylePicker ================================================

// an entry for the table style picker for a bare table style
const NO_TABLE_STYLE = {
    id: "None",
    category: "",
    //#. name of a table stylesheet without formatting
    name: gt("No Style, No Grid"),
    priority: -1
};

/**
 * A style sheet picker for table ranges in spreadsheets.
 */
export class SheetTableStylePicker extends TableStylePicker {
    constructor(docView) {

        const getStyleFlags = () => docView.getControllerItemValue("table/flags");

        super(docView, getStyleFlags, {
            additionalStyles: [NO_TABLE_STYLE]
        });
    }
}
