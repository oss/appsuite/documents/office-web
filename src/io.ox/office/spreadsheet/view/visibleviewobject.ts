/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type {
    ResolveDOMEventKeys, JQueryEmitter, CoreEmitter, TypedEmitter, AnyEmitter,
    TypedEventHandlerFn, DOMEventHandlerFn, JQueryEventHandlerFn, CoreEventHandlerFn
} from "@/io.ox/office/tk/events";
import type { PropertySet } from "@/io.ox/office/tk/containers";
import type { ChangedPropHandlerFn } from "@/io.ox/office/tk/objects";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";
import type { ISheetProps } from "@/io.ox/office/spreadsheet/model/sheetpropset";
import type { IModelProps } from "@/io.ox/office/spreadsheet/model/modelpropset";
import type { IViewProps } from "@/io.ox/office/spreadsheet/view/viewpropset";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// class VisibleViewObject ====================================================

/**
 * Base class for objects in a spreadsheet view that maintain a visibility
 * state, and need to handle events from various emitters only when visible.
 */
export abstract class VisibleViewObject<EvtMapT = Empty> extends ViewObject<SpreadsheetView, EvtMapT> {

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view that owns this instance.
     */
    protected constructor(docView: SpreadsheetView) {
        super(docView);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this instance is currently in visible state.
     *
     * @returns
     *  Whether this instance is currently in visible state.
     */
    abstract isVisible(): boolean;

    // protected methods ------------------------------------------------------

    /**
     * Returns whether events can currently be processed. By default, events
     * will be processed when method `isVisible` returns `true`. Subclasses may
     * override ths method to implement more specific behavior.
     *
     * @returns
     *  Whether events can currently be processed.
     */
    protected canProcessEvents(): boolean {
        return this.isVisible();
    }

    /**
     * Registers an event handler at the specified event emitter that will only
     * be invoked when this instance is visible.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param type
     *  The type name of the event to start listening to. Can also be a space
     *  separated list of event names.
     *
     * @param handlerFn
     *  The callback function to be invoked if the event emitter fires. Event
     *  handlers must be synchronous, and MUST NOT throw exceptions. Will be
     *  invoked with this instance as calling context, and receives all
     *  parameters fired by the event emitter.
     */
    protected listenToWhenVisible<T extends EventTarget, K extends ResolveDOMEventKeys<T>>(emitter: T, type: OrArray<K>, handler: DOMEventHandlerFn<this, T, K>): void;
    protected listenToWhenVisible<K extends keyof JTriggeredEventMap>(emitter: JQueryEmitter, type: OrArray<K>, handler: JQueryEventHandlerFn<this, K>): void;
    protected listenToWhenVisible<E, K extends keyof E>(emitter: TypedEmitter<E>, type: OrArray<K>, handler: TypedEventHandlerFn<this, E, K>): void;
    protected listenToWhenVisible<E, K extends keyof E>(emitter: CoreEmitter<E>, type: OrArray<K>, handler: CoreEventHandlerFn<this, E, K>): void;
    // implementation
    protected listenToWhenVisible<E>(emitter: AnyEmitter<E>, type: OrArray<string>, handler: AnyFunction): void {
        this.listenTo(emitter as EventTarget, type, (...args): void | false => {
            if (this.canProcessEvents()) { return handler.apply(this, args) as void | false; }
        });
    }

    /**
     * Registers an event handler for a specific property that will only be
     * invoked when this instance is visible.
     */
    protected listenToPropWhenVisible<ST extends object, KT extends keyof ST>(propSet: PropertySet<ST>, propKey: KT, handler: ChangedPropHandlerFn<this, ST, KT>): void {
        this.listenToProp(propSet, propKey, (...args) => {
            if (this.canProcessEvents()) { handler.apply(this, args); }
        });
    }

    /**
     * Registers an event handler for a specific view property that will only
     * be invoked when this instance is visible.
     */
    protected listenToViewPropWhenVisible<KT extends keyof IViewProps>(propKey: KT, handler: ChangedPropHandlerFn<this, IViewProps, KT>): void {
        this.listenToPropWhenVisible(this.docView.propSet, propKey, handler);
    }

    /**
     * Registers an event handler for a specific document model property that
     * will only be invoked when this instance is visible.
     */
    protected listenToModelPropWhenVisible<KT extends keyof IModelProps>(propKey: KT, handler: ChangedPropHandlerFn<this, IModelProps, KT>): void {
        this.listenToPropWhenVisible(this.docModel.propSet, propKey, handler);
    }

    /**
     * Registers an event handler for a specific sheet model property (of the
     * active sheet) that will only be invoked when this instance is visible.
     */
    protected listenToSheetPropWhenVisible<KT extends keyof ISheetProps>(propKey: KT, handler: ChangedPropHandlerFn<this, ISheetProps, KT>): void {
        this.listenToWhenVisible(this.docView, "change:sheetprops", event => {
            if (propKey in event.newProps) {
                handler.call(this, event.newProps[propKey]!, event.oldProps[propKey]!);
            }
        });
    }
}
