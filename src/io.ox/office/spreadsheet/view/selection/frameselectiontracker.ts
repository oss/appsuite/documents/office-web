/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Point } from "@/io.ox/office/tk/algorithms";
import type { Rectangle } from "@/io.ox/office/tk/dom";
import type { TrackingRecord } from "@/io.ox/office/tk/tracking";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import { TrackingRectangle, getTrackingRectangle } from "@/io.ox/office/drawinglayer/utils/drawingutils";

import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * Configuration options for frame selection mode (select a pixel-exact sheet
 * rectangle).
 */
export interface FrameSelectionModeConfig {

    /**
     * A callback function that will be invoked when the frame rectangle has
     * been selected successfully (not canceled).
     */
    applySelected?: (frameRect: TrackingRectangle) => void;

    /**
     * A callback function that can be used to render something into the
     * selected frame rectangle while tracking mode is active. Receives the DOM
     * node representing the selected frame rectangle, as JQuery collection.
     */
    renderPreview?: (frameNode: HTMLElement, frameRect: TrackingRectangle) => void;

    /**
     * The status label to be shown in the status pane while the frame
     * selection mode is active.
     */
    statusLabel?: string;

    /**
     * The aspect ratio to be used for locked aspect mode (pressed SHIFT key),
     * as quotient of width and height (a value greater than 1 will create a
     * wide rectangle). MUST be a positive floating-point number. Default value
     * is `1`.
     */
    aspectRatio?: number;

    /**
     * Whether to operate in two-point mode (used e.g. for line and connector
     * shapes) instead of the default rectangle mode. Default value is `false`.
     */
    twoPointMode?: boolean;

    /**
     * If set to `true`, the frame selection mode can be activated while the
     * document is in read-only mode; or remains active, when the document
     * switches to read-only mode. By default, the frame selection mode will be
     * canceled immediately when the read-only mode will be activated.
     */
    readOnly?: boolean;
}

// class FrameSelectionTracker ================================================

/**
 * Handles active frame selection tracking cycles (used to select pixel-exact
 * rectangles in the sheet).
 */
export class FrameSelectionTracker extends ViewObject<SpreadsheetView> {

    /** The type identifier of the frame selection tracking cycle. */
    #type: Opt<string>;
    /** Configuration of the frame selection tracking cycle. */
    #config: Opt<FrameSelectionModeConfig>;
    /** The frame rectangle currently selected. */
    #rectangle: Opt<TrackingRectangle>;

    /** Tracking start position (absolute position in the sheet, in pixels). */
    #anchor: Opt<Point>;
    /** Sheet area as bounding rectangle. */
    #boundRect!: Rectangle;
    /** Whether the mouse or touch point has really been moved (with threshold distance). */
    #mouseMoved = false;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView) {
        super(docView);

        // register handlers at all grid panes (after full construction of the view)
        this.docApp.onInit(() => {
            for (const gridPane of docView.getGridPanes()) {

                // register mouse/touch tracking handler for frame selection mode
                gridPane.registerTrackingCallbacks("frame-select", {
                    prepare: () => this.isActive(),
                    start: record => this.#trackingStartHandler(gridPane, record),
                    move: record => this.#trackingMoveHandler(gridPane, record),
                    scroll: record => this.#trackingScrollHandler(gridPane, record),
                    finally: record => this.leaveTracking(record.type === "end")
                }, { prepend: true, readOnly: true });

                // render something into the selected frame rectangles in every grid pane
                this.listenTo(gridPane, "render:frameselection", (frameNode, frameRect) => {
                    this.#config?.renderPreview?.(frameNode, frameRect);
                });
            }
        });

        // automatically cancel frame selection mode when document loses edit rights
        this.listenTo(this.docApp, "docs:editmode:leave", () => {
            if (!this.#config?.readOnly) { this.leaveTracking(false); }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether a frame selection tracking cycle is currently active.
     *
     * @param [type]
     *  The type identifier for the frame selection mode, as passed to the
     *  method `enterTracking`. If omitted, this method returns whether any
     *  frame selection tracking cycle is active.
     */
    isActive(type?: string): boolean {
        return type ? (type === this.#type) : !!this.#type;
    }

    /**
     * Initializes a new frame selection tracking cycle. This instance will
     * start to process tracking events triggered by the grid pane, and will
     * construct and notify a frame rectangle with the exact pixel position in
     * the sheet.
     *
     * @param type
     *  An arbitrary string that identifies the type of frame selection mode
     *  that is currently active.
     *
     * @param [config]
     *  Configuration options for frame selection mode.
     */
    enterTracking(type: string, config?: FrameSelectionModeConfig): void {

        // do not start selection mode in readonly mode (unless configured)
        if (!config?.readOnly && !this.docView.isEditable()) { return; }

        // initialize members
        this.#type = type;
        this.#config = config;
        this.#rectangle = undefined;

        // set status label text
        this.docView.setStatusLabel(config?.statusLabel || null);

        // trigger zero-size rectangle to show cross-hairs in all grid panes at the current mouse position
        this.listenTo(document, "mousemove", event => {
            const sheetPoint = this.docView.getGridPaneForPagePoint(event)?.getSheetPointForPagePoint(event, { restricted: true });
            const trackingRect = sheetPoint ? new TrackingRectangle(sheetPoint.x, sheetPoint.y, 0, 0, 0, 0) : null;
            this.#triggerTrackingRect(trackingRect);
        });
    }

    /**
     * Invokes the `applySelected` callback handler passed to the method
     * `enterTracking`, if a frame rectangle has been selected in the meantime.
     *
     * @param apply
     *  If set to `true`, the `applySelected` callback passed to the method
     *  `enterTracking` will be invoked. Otherwise, frame selection mode will
     *  be canceled.
     */
    leaveTracking(apply: boolean): void {

        // frame selection mode must be active
        if (!this.#type) { return; }

        // reset status label text
        this.docView.setStatusLabel(null);

        // stop listening to "mousemove" events for the cross-hair
        this.stopListeningTo(document);

        // notify renderer listeners to hide the frame rectangle
        this.#triggerTrackingRect(null);

        // invoke callback handler
        if (apply && this.#rectangle) {
            this.#config?.applySelected?.(this.#rectangle);
        }

        // reset all members for next tracking cycle
        this.#type = this.#config = this.#rectangle = undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Sets the current frame rectangle as view property. Used by renderers to
     * display the frame selection. Zero-sized rectangles will be displayed as
     * cross-hair.
     */
    #triggerTrackingRect(trackingRect: TrackingRectangle | null): void {
        this.docView.propSet.set("frameTrackingRect", trackingRect);
    }

    /**
     * Handler for "start" tracking notifications.
     */
    #trackingStartHandler(gridPane: GridPane, record: TrackingRecord): void {

        // get the start position for the rectangle in the sheet
        this.#anchor = gridPane.getSheetPointForPagePoint(record.point, { restricted: true });
        if (!this.#anchor) {
            record.cancel();
            return;
        }

        this.#rectangle = new TrackingRectangle(this.#anchor.x, this.#anchor.y, 0, 0, 0, 0);
        // restrict the frame rectangle to the sheet area
        this.#boundRect = this.docView.getSheetRectangle();
        this.#mouseMoved = false;

        // stop listening to "mousemove" events for the cross-hair
        this.stopListeningTo(document);
    }

    /**
     * Handler for "move" tracking notifications.
     */
    #trackingMoveHandler(gridPane: GridPane, record: TrackingRecord): void {

        // current tracking position (absolute sheet position, in pixels)
        const point = gridPane.getSheetPointForPagePoint(record.point);
        if (!point) {
            record.cancel();
            return;
        }

        // the current location of the selected frame rectangle
        const frameRect = getTrackingRectangle(this.#anchor!, point, {
            modifiers: record.modifiers,
            boundRect: this.#boundRect,
            aspectRatio: this.#config?.aspectRatio,
            twoPointMode: this.#config?.twoPointMode
        });

        // start tracking after a threshold of 3 pixels
        this.#mouseMoved ||= (frameRect.origShift >= 3);
        if (this.#mouseMoved) {
            this.#rectangle = frameRect;
            this.#triggerTrackingRect(frameRect);
        }
    }

    /**
     * Handler for "scroll" tracking notifications.
     */
    #trackingScrollHandler(gridPane: GridPane, record: TrackingRecord): void {

        // adjust scroll position of grid pane
        gridPane.scrollRelative(record.scroll.x, record.scroll.y);

        // handle moved tracking point due to scrolling
        this.#trackingMoveHandler(gridPane, record);
    }
}
