/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, itr, ary, set, jpromise } from "@/io.ox/office/tk/algorithms";
import { getFocus, matchModifierKeys, matchKeyCode, KeyCode, isSelectAllKeyEvent } from "@/io.ox/office/tk/dom";
import type { TypedEventHandlerFn, DocsEmitter } from "@/io.ox/office/tk/events";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import type { YieldIndexedDrawingModelsOptions } from "@/io.ox/office/drawinglayer/model/indexedcollection";

import { type Position, clonePositions } from "@/io.ox/office/editframework/utils/operations";
import type Selection from "@/io.ox/office/textframework/selection/selection";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Direction, MULTI_SELECTION, isLeadingDir, isVerticalDir } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { PanePos, SelectionMode, isColumnSide, getCellMoveDirection } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";

import type { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import type { EntryOffsetDescriptor, ColRowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import { VisibleEntryMethod } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import { MergeMatchType } from "@/io.ox/office/spreadsheet/model/mergecollection";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray";
import type { SheetDrawingModelType, SheetDrawingModelIterator } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import type { SheetChartModel } from "@/io.ox/office/spreadsheet/model/drawing/chart/chartmodel";

import { type CustomSelectionModeConfig, CustomSelectionActor } from "@/io.ox/office/spreadsheet/view/selection/customselectionactor";
import { type FrameSelectionModeConfig, FrameSelectionTracker } from "@/io.ox/office/spreadsheet/view/selection/frameselectiontracker";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * Interface for objects that can be registered at `SelectionEngine` instances
 * to provide and consume a custom cell selection.
 */
export interface CellSelectionProvider {

    /**
     * Will be invoked by the selection engine to receive a cell selection to
     * be used for all selection code, especially changing the selection with
     * keyboard shortcuts.
     *
     * @returns
     *  A sheet selection to be used by the selection engine, or nullish to
     *  indicate to fallback to the current cell selection of the active sheet.
     */
    get(): Nullable<SheetSelection>;

    /**
     * Will be invoked by the selection engine before actually changing the
     * current cell selection in the active sheet.
     *
     * @param selection
     *  The new selection intended to be set at the active sheet.
     *
     * @returns
     *  Whether the selection has been processed successfully. If this function
     *  returns `true`, the cell selection of the active sheet will not be
     *  changed.
     */
    set(selection: SheetSelection): boolean;
}

/**
 * Shared optional parameters for different methods of `SelectionEngine`.
 */
export interface UniqueSelectionOptions {

    /**
     * If set to `true`, duplicate cell range addresses will be removed from
     * the selection. Default value is `false`.
     */
    unique?: boolean;
}

/**
 * Optional parameters for method `SelectionEngine#setCellSelection`.
 */
export interface SetCellSelectionOptions extends UniqueSelectionOptions {

    /**
     * If set to `true`, and if the active cell in the new selection is hidden
     * by a merged range, the address of the original active cell will be
     * stored in the property "origin" of the sheet selection. Default value is
     * `false`.
     */
    storeOrigin?: boolean;
}

/**
 * Optional parameters for method `SelectionEngine#selectCell` and
 * `SelectionEngine#selectRange`.
 */
export interface SelectRangeOptions extends SetCellSelectionOptions {

    /**
     * If set to `true`, the new cell address or cell range address will be
     * appended to the current selection. By default, the old selection will be
     * removed. When used together with the option "unique", an existing
     * selected cell or range will not be duplicated.
     */
    append?: boolean;

    /**
     * The address of the active cell in the range to be selected. If omitted,
     * the top-left cell of the range will be activated. Must be located inside
     * the passed range address.
     */
    active?: Address;
}

/**
 * Optional parameters for method `SelectionEngine#selectDrawing`.
 */
export interface SelectDrawingOptions {

    /**
     * If set to `true`, the selection state of the addressed drawing frame
     * will be toggled. By default, the old drawing selection will be removed,
     * and the addressed drawing frame will be selected.
     */
    toggle?: boolean;
}

// constants ==================================================================

/**
 * The types of all drawing objects that can be copied into and pasted from the
 * clipboard.
 */
const CLIPBOARD_DRAWING_TYPES = new Set<DrawingType>([DrawingType.IMAGE, DrawingType.CHART]);

// class SelectionEngine ======================================================

/**
 * A cell selection engine manages the current cell/range/drawing selection in
 * a spreadsheet view.
 */
export class SelectionEngine extends ViewObject<SpreadsheetView> {

    /** The address factory of the document model. */
    readonly #addressFactory: AddressFactory;
    /** The text selection engine (from text framework). */
    readonly #textSelection: Selection;
    /** The token array containing all source ranges of a selected drawing object. */
    readonly #highlightTokenArray: TokenArray;
    /** Actor for current custom selection mode. */
    readonly #customSelectActor: CustomSelectionActor;
    /** Tracker for current frame selection mode. */
    readonly #frameSelectTracker: FrameSelectionTracker;

    /** Provider and consumer for custom cell selections. */
    #cellSelectionProvider: Opt<CellSelectionProvider>;

    /** The selected chart model with highlighted source ranges. */
    #highlightedChart: Opt<SheetChartModel>;
    /** The unique identifier of highlighting mode for selected chart. */
    #chartHighlightUid: string | null = null;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView, rootEl: HTMLDivElement) {
        super(docView);

        this.#addressFactory = this.docModel.addressFactory;
        this.#textSelection = this.docModel.getSelection();
        this.#highlightTokenArray = new TokenArray(this.docModel, "link");

        // custom selection actor (handler for selecting additional cell ranges in the view)
        this.#customSelectActor = this.member(new CustomSelectionActor(docView));
        // frame selection tracker (handler for tracking cycles to select pixel rectangles)
        this.#frameSelectTracker = this.member(new FrameSelectionTracker(docView));

        // update all settings depending on the current sheet selection
        this.listenTo(docView, "change:selection", () => {
            // register the drawing objects in the selection engine of the text framework
            this.#refreshTextSelection();
            // update highlighting of selected chart object
            this.#refreshChartHighlighting();
        });

        // restore the correct text framework selection after leaving drawing edit mode
        this.listenTo(docView, "textedit:leave", editMode => {
            if (editMode === "drawing") { this.#refreshTextSelection(); }
        });

        // further initialization depending on fully constructed document view
        this.docApp.onInit(() => {

            // listen to events from corner pane to adjust the selection
            this.#listenToWithFocus(docView.cornerPane, "select:all", mode => {

                // DOCS-2919: allow to toggle full selection
                if (this.isSheetSelected()) {
                    this.selectCell(this.getActiveCell());
                    return;
                }

                // select mode: move active cell the top-left cell in the first visible grid pane
                // extend mode: do not change the current active cell
                const topLeftAddress = (mode === SelectionMode.SELECT) ? docView.getVisibleGridPane(PanePos.TL).getTopLeftAddress() : this.#getEffectiveSelection().address;

                this.selectRange(this.#addressFactory.getSheetRange(), { active: topLeftAddress });
                this.#customSelectActor.leaveCustomMode(false);
            });

            // listen to events from all header panes to adjust the selection
            for (const headerPane of docView.getHeaderPanes()) {

                // whether the header pane shows column headers
                const columns = isColumnSide(headerPane.paneSide);
                // the column/row index where selection tracking has started
                let startIndex = 0;

                // makes an adjusted range between start index and passed index
                const makeRange = (index: number): Range => this.#addressFactory.getFullRange(Interval.create(startIndex, index), columns);

                // start selection tracking mode, e.g. "mousedown" on header cell
                this.#listenToWithFocus(headerPane, "select:start", (index, mode) => {

                    const selection = this.#getEffectiveSelection(true);
                    const customMode = this.isCustomSelectionMode();

                    if (!customMode && (mode === SelectionMode.EXTEND) && !selection.ranges.empty()) {
                        // modify the active range (from active cell to tracking start index)
                        startIndex = selection.address.get(columns);
                        this.changeActiveRange(makeRange(index));
                    } else {
                        // create/append a new selection range
                        startIndex = index;
                        // set active cell to current first visible cell in the column/row
                        this.selectRange(makeRange(index), {
                            append: !customMode && (mode === SelectionMode.APPEND),
                            active: Address.fromDir(columns, startIndex, Math.max(0, docView.getVisibleHeaderPane(!columns).getFirstVisibleIndex()))
                        });
                    }
                });

                // move during selection tracking mode, e.g. "mousemove" on header cells
                this.#listenToWithFocus(headerPane, "select:move", index => {
                    this.#setTrackingRange(makeRange(index), true);
                });

                // finish selection tracking mode, e.g. "mouseup" on header cell
                this.#listenToWithFocus(headerPane, "select:end", (index, success) => {
                    this.#setTrackingRange(makeRange(index), false);
                    this.#customSelectActor.leaveCustomMode(success);
                });
            }

            // listen to events from all grid panes to adjust the selection
            for (const gridPane of docView.getGridPanes()) {

                // the address of the cell where selection tracking has started
                let startAddress!: Address;

                // makes an adjusted range from the start address to the passed address
                const makeRange = (address: Address): Range => Range.fromAddresses(startAddress, address);

                // start selection tracking mode, e.g. "mousedown" on cell
                this.#listenToWithFocus(gridPane, "select:start", (address, mode) => {

                    const selection = this.#getEffectiveSelection(true);
                    const customMode = this.isCustomSelectionMode();

                    if (!customMode && (mode === SelectionMode.EXTEND) && !selection.ranges.empty()) {
                        // modify the active range (from active cell to tracking start position)
                        startAddress = selection.address;
                        this.changeActiveRange(makeRange(address));
                    } else {
                        // create/append a new selection range
                        startAddress = address;
                        this.selectCell(address, { append: !customMode && (mode === SelectionMode.APPEND) });
                    }
                });

                // move during selection tracking mode, e.g. "mousemove" on cells
                this.#listenToWithFocus(gridPane, "select:move", address => {
                    this.#setTrackingRange(makeRange(address), true);
                });

                // finish selection tracking mode, e.g. "mouseup" on cell
                this.#listenToWithFocus(gridPane, "select:end", (address, success) => {
                    this.#setTrackingRange(makeRange(address), false);
                    this.#customSelectActor.leaveCustomMode(success);
                });

                // select entire range, e.g. touch resizer in grid pane
                this.#listenToWithFocus(gridPane, "select:range", (index, range) => {
                    this.#customSelectActor.leaveCustomMode(false);
                    this.changeRange(index, range);
                });

                // change active cell in selected range, e.g. touch tap in grid pane
                this.#listenToWithFocus(gridPane, "select:active", (index, address) => {
                    this.#customSelectActor.leaveCustomMode(false);
                    this.changeActiveCell(index, address);
                });

                // double click: start cell edit mode (after focus returned to application)
                this.#listenToWithFocus(gridPane, "select:dblclick", () => {
                    jpromise.floating(docView.enterTextEditMode("cell"));
                });
            }
        });

        // listen to key events from all panes to adjust the selection
        rootEl.addEventListener("keydown", event => {
            const handled = this.#handleKeyDownEvent(event);
            if (handled) {
                event.preventDefault();
                event.stopPropagation();
            }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a handler object with callback functions that will change the
     * behavior of this selection engine when selecting cell ranges with mouse
     * or keyboard.
     *
     * @param provider
     *  The implementation of the `CellSelectionProvider` interface.
     */
    registerCellSelectionProvider(provider: CellSelectionProvider): void {
        this.#cellSelectionProvider = provider;
    }

    /**
     * Unregisters the selection handler that have been registered with the
     * method `registerCellSelectionProvider`.
     */
    unregisterCellSelectionProvider(): void {
        this.#cellSelectionProvider = undefined;
    }

    /**
     * Switches this selection engine to a special mode where selecting the
     * next cell range in the document will be displayed as "active selection"
     * (with a marching ants effect), and that can be handled afterwards via
     * the promise returned by this method.
     *
     * @param type
     *  An arbitrary string that identifies the type of custom selection mode
     *  that is currently active.
     *
     * @param [config]
     *  Configuration options for custom selection mode.
     */
    enterCustomSelectionMode(type: string, config?: CustomSelectionModeConfig): void {

        // cancel previous selection modes if active
        this.#customSelectActor.leaveCustomMode(false);
        this.#frameSelectTracker.leaveTracking(false);

        // initialize custom selection mode
        this.#customSelectActor.enterCustomMode(type, config);
    }

    /**
     * Returns whether the specified custom selection mode is currently active.
     *
     * @param [type]
     *  A type identifier for the custom selection mode, as passed to the
     *  method `enterCustomSelectionMode`. If omitted, this method returns
     *  whether any custom selection mode is active.
     *
     * @returns
     *  Whether the specified custom selection mode is currently active.
     */
    isCustomSelectionMode(type?: string): boolean {
        return this.#customSelectActor.isActive(type);
    }

    /**
     * Cancels the custom selection mode that has been started with the method
     * `enterCustomSelectionMode` before.
     *
     * @returns
     *  Whether the custom selection mode was active and has been left.
     */
    cancelCustomSelectionMode(): boolean {
        if (this.#customSelectActor.isActive()) {
            this.#customSelectActor.leaveCustomMode(false);
            return true;
        }
        return false;
    }

    /**
     * Switches this selection engine to a special selection mode that allows
     * to select an arbitrary pixel-exact rectangle in the active sheet.
     *
     * @param type
     *  An arbitrary string that identifies the type of frame selection mode
     *  that is currently active.
     *
     * @param [config]
     *  Configuration options for frame selection mode.
     */
    enterFrameSelectionMode(type: string, config?: FrameSelectionModeConfig): void {

        // cancel previous selection modes if active
        this.#customSelectActor.leaveCustomMode(false);
        this.#frameSelectTracker.leaveTracking(false);

        // initialize frame selection mode
        this.#frameSelectTracker.enterTracking(type, config);
    }

    /**
     * Returns whether the specified frame selection mode is currently active.
     *
     * @param [type]
     *  The type identifier for the frame selection mode, as passed to the
     *  method `enterFrameSelectionMode`. If omitted, this method returns
     *  whether any frame selection mode is active.
     *
     * @returns
     *  Whether the specified frame selection mode is currently active.
     */
    isFrameSelectionMode(type?: string): boolean {
        return this.#frameSelectTracker.isActive(type);
    }

    /**
     * Cancels the frame selection mode that has been started with the method
     * `enterFrameSelectionMode` before.
     *
     * @returns
     *  Whether the frame selection mode was active and has been left.
     */
    cancelFrameSelectionMode(): boolean {
        if (this.#frameSelectTracker.isActive()) {
            this.#frameSelectTracker.leaveTracking(false);
            return true;
        }
        return false;
    }

    /**
     * Returns the current selection in the active sheet. This method returns a
     * clone of the selection that can be freely manipulated.
     *
     * @returns
     *  The current selection in the active sheet.
     */
    getSelection(): SheetSelection {
        return this.#getSheetSelection().clone();
    }

    /**
     * Returns the addresses of all selected cell ranges in the active sheet.
     * This method returns a clone of the selected ranges that can be freely
     * manipulated.
     *
     * @returns
     *  The addresses of all selected cell ranges in the active sheet.
     */
    getSelectedRanges(): RangeArray {
        return this.#getSheetSelection().ranges.deepClone();
    }

    /**
     * Returns the address of the active cell range in the selection of the
     * active sheet. This method returns a clone of the active range that can
     * be freely manipulated.
     *
     * @returns
     *  The address of the active cell range in the selection of the active
     *  sheet.
     */
    getActiveRange(): Range {
        return this.#getSheetSelection().activeRange()!.clone();
    }

    /**
     * Returns the address of the active cell in the selection of the active
     * sheet (the highlighted cell in the active range that will be edited when
     * switching to text edit mode). This method returns a clone of the active
     * cell address that can be freely manipulated.
     *
     * @returns
     *  The address of the active cell in the selection of the active sheet.
     */
    getActiveCell(): Address {
        return this.#getSheetSelection().address.clone();
    }

    /**
     * Returns whether the current selection consists of a single cell range
     * (no multi-selection).
     *
     * @returns
     *  Whether the current selection consists of a single cell range.
     */
    isSingleRangeSelection(): boolean {
        return this.#getSheetSelection().ranges.length === 1;
    }

    /**
     * Returns whether the current selection consists of the active cell only,
     * either as single cell, or exactly covering a merged range.
     *
     * @returns
     *  Whether the current selection consists of the active cell only.
     */
    isSingleCellSelection(): boolean {
        const ranges = this.#getSheetSelection().ranges;
        return (ranges.length === 1) && this.docView.sheetModel.isSingleCellInRange(ranges[0]);
    }

    /**
     * Returns whether the entire sheet is selected.
     *
     * @returns
     *  Whether the entire sheet is selected.
     */
    isSheetSelected(): boolean {
        return this.#getSheetSelection().ranges.some(range => this.#addressFactory.isSheetRange(range));
    }

    /**
     * Returns whether the current cell selection contains at least one range
     * spanning over multiple columns.
     *
     * @returns
     *  Whether the current selection contains at least one range spanning over
     *  multiple columns.
     */
    hasMultipleColumnsSelected(): boolean {
        if (this.isSingleCellSelection()) { return false; } // single cell checks also the merged range
        return this.#getSheetSelection().ranges.some(range => !range.singleCol());
    }

    /**
     * Returns whether the current cell selection contains at least one range
     * spanning over multiple columns.
     *
     * @returns
     *  Whether the current selection contains at least one range spanning over
     *  multiple columns.
     */
    hasMultipleRowsSelected(): boolean {
        if (this.isSingleCellSelection()) { return false; } // single cell checks also the merged range
        return this.#getSheetSelection().ranges.some(range => !range.singleRow());
    }

    /**
     * Returns whether the current cell selection contains at least one range
     * spanning over multiple cells.
     *
     * @returns
     *  Whether the current selection contains at least one range spanning over
     *  multiple cells.
     */
    hasAnyRangeSelected(): boolean {
        return this.#getSheetSelection().ranges.some(range => !range.single());
    }

    /**
     * Returns whether the current cell selection contains or partly overlaps
     * at least one merged cell range.
     *
     * @returns
     *  Whether the current selection contains or partly overlaps at least one
     *  merged cell range.
     */
    hasMergedRangeSelected(): boolean {
        return this.docView.mergeCollection.coversAnyMergedRange(this.#getSheetSelection().ranges);
    }

    /**
     * Returns, whether a drawing object is selected in the current active
     * sheet.
     *
     * @returns
     *  Whether a drawing object is selected in the active sheet.
     */
    hasDrawingSelection(): boolean {
        return this.#getSheetSelection().drawings.length > 0;
    }

    /**
     * Returns, whether exactly one drawing object is selected in the current
     * active sheet.
     *
     * @returns
     *  Whether exactly one drawing object is selected in the active sheet.
     */
    hasSingleDrawingSelection(): boolean {
        return this.#getSheetSelection().drawings.length === 1;
    }

    /**
     * Returns the positions of all selected drawing frames in the active
     * sheet.
     *
     * @returns
     *  The selected drawing frames, as array of drawing positions.
     */
    getSelectedDrawings(): Position[] {
        return clonePositions(this.#getSheetSelection().drawings);
    }

    /**
     * Returns the drawing model instances of all selected drawing objects.
     *
     * @returns
     *  The models of all selected drawing objects, in order of the current
     *  selection.
     */
    getSelectedDrawingModels(): SheetDrawingModelType[] {
        const { drawingCollection } = this.docView;
        return ary.from(this.getSelectedDrawings(), position => drawingCollection.getModel(position));
    }

    /**
     * Returns the types of all selected drawing objects, as a set.
     *
     * @returns
     *  The types of all selected drawing objects.
     */
    getSelectedDrawingTypes(): Set<DrawingType> {
        const { drawingCollection } = this.docView;
        return set.from(this.getSelectedDrawings(), position => drawingCollection.getModel(position)?.drawingType);
    }

    /**
     * Returns whether the current selection contains only drawings that can be
     * copied into and pasted from the clipboard.
     *
     * @returns
     *  Whether the drawing selection can be copied and pasted.
     */
    canCopyPasteDrawingSelection(): boolean {
        if (!this.hasDrawingSelection()) { return false; }
        return set.subset(this.getSelectedDrawingTypes(), CLIPBOARD_DRAWING_TYPES);
    }

    /**
     * Changes the cell selection in the active sheet, and deselects all
     * drawing frames.
     *
     * @param selection
     *  The new sheet selection. The array of cell range addresses must not be
     *  empty. The array of selected drawing objects must be empty.
     *
     * @param [options]
     *  Optional parameters.
     */
    setCellSelection(selection: SheetSelection, options?: SetCellSelectionOptions): void {

        // do not change the selection in chart sheets etc.
        if (!this.docView.sheetModel.isCellType) { return; }

        // a merged range hiding the active cell
        const mergedRange = this.docView.mergeCollection.getMergedRange(selection.address, MergeMatchType.HIDDEN);
        // the new active range
        const activeRange = selection.activeRange()!;

        // deselect all drawing frames
        selection = selection.clone();
        selection.drawings = [];
        selection.origin = null;

        // adjust active cell if it is hidden by a merged range
        if (mergedRange) {
            if (options?.storeOrigin) {
                selection.origin = selection.address;
            }
            selection.address = mergedRange.a1.clone();
        }

        // restrict to single selection if multi-selection is not supported
        if (!MULTI_SELECTION) {
            selection.ranges.length = 0;
            selection.ranges.push(activeRange);
            selection.active = 0;
        }

        // remove duplicates
        if (options?.unique && (selection.ranges.length > 1)) {
            selection.ranges = selection.ranges.unify();
            selection.active = selection.ranges.findIndex(range => range.equals(activeRange));
        }

        // apply the new sheet selection
        this.#setEffectiveSelection(selection);
    }

    /**
     * Selects a single cell in the active sheet. The cell will become the
     * active cell of the sheet.
     *
     * @param address
     *  The address of the cell to be selected.
     *
     * @param [options]
     *  Optional parameters.
     */
    selectCell(address: Address, options?: SelectRangeOptions): void {
        // DOCS-5047: pass option "active: address" to retain column/row when traversing over merged ranges
        this.selectRange(new Range(address), { active: address, ...options });
    }

    /**
     * Selects a single cell range in the active sheet.
     *
     * @param range
     *  The address of the cell range to be selected.
     *
     * @param [options]
     *  Optional parameters.
     */
    selectRange(range: Range, options?: SelectRangeOptions): void {

        // do not change the selection in chart sheets etc.
        if (!this.docView.sheetModel.isCellType) { return; }

        // the initial selection to append the new range to
        const selection = options?.append ? this.#getEffectiveSelection(true) : new SheetSelection();
        // the new active cell
        let activeCell = options?.active;
        // next cell tested while validating the active cell
        let nextCell = activeCell?.clone();

        // returns a cell address object, if the specified cell position is inside the passed selection range
        const getValidCellAddress = (col: number, row: number): Opt<Address> => {
            const address = new Address(col, row);
            return range.containsAddress(address) ? address : undefined;
        };

        // expand range to merged ranges
        range = this.#expandRangeToMergedRanges(range);

        // bug 33313: move active cell if it is outside the active range due to a partly covered merged range
        let mergedRange: Opt<Range>;
        while (nextCell && (mergedRange = this.docView.mergeCollection.getMergedRange(nextCell)) && !range.contains(mergedRange)) {
            nextCell = getValidCellAddress(nextCell.c, mergedRange.a2.r + 1) ?? getValidCellAddress(mergedRange.a2.c + 1, nextCell.r);
        }
        if (nextCell) { activeCell = nextCell; }

        // initialize and set the new selection
        selection.ranges.push(range);
        selection.active = selection.ranges.length - 1;

        // set active cell
        if (activeCell && range.containsAddress(activeCell)) {
            selection.address = activeCell.clone();
        } else {
            selection.address = range.a1.clone();
        }

        // commit new selection
        this.setCellSelection(selection, options);
    }

    /**
     * Modifies the position of the specified selection range.
     *
     * @param index
     *  The array index of the range to be changed.
     *
     * @param range
     *  The new address of the selected cell range. If the specified range
     *  index refers to the active range, but the new range does not contain
     *  the current active cell, the active cell will be moved inside the new
     *  cell range.
     *
     * @param [options]
     *  Optional parameters.
     */
    changeRange(index: number, range: Range, options?: UniqueSelectionOptions): void {

        // the selection to be modified
        const selection = this.#getEffectiveSelection();

        // expand range to merged ranges
        range = this.#expandRangeToMergedRanges(range);

        // insert the new range into the selection object
        selection.ranges[index] = range;
        if ((index === selection.active) && !range.containsAddress(selection.address)) {
            selection.address.c = math.clamp(selection.address.c, range.a1.c, range.a2.c);
            selection.address.r = math.clamp(selection.address.r, range.a1.r, range.a2.r);
        }

        // commit new selection
        this.setCellSelection(selection, options);
    }

    /**
     * Modifies the position of the current active selection range.
     *
     * @param range
     *  The new address of the active selection range. If the passed range does
     *  not contain the current active cell, the top-left cell of the range
     *  will be activated.
     *
     * @param [options]
     *  Optional parameters.
     */
    changeActiveRange(range: Range, options?: UniqueSelectionOptions): void {
        this.changeRange(this.#getEffectiveSelection().active, range, options);
    }

    /**
     * Modifies the position of the current active cell.
     *
     * @param index
     *  The array index of the range to be activated.
     *
     * @param address
     *  The new address of the active cell.
     */
    changeActiveCell(index: number, address: Address): void {

        // the selection to be modified
        const selection = this.#getEffectiveSelection();

        // update index of active range, and active cell
        selection.active = math.clamp(index, 0, selection.ranges.length - 1);
        selection.address = address.clone();

        // commit new selection
        this.setCellSelection(selection);
    }

    /**
     * Moves the active cell in the current selection into the specified
     * direction. Cycles through all ranges contained in the selection. In case
     * the selection is a single cell, moves it in the entire sheet.
     *
     * @param direction
     *  The direction the active cell will be moved to.
     */
    moveActiveCell(direction: Direction): void {

        // In the following, variable names and code comments will use the term "line" for cells in the main
        // moving direction, i.e. for a row when using the TAB key (direction LEFT or RIGHT), or for a column
        // when using the ENTER key (direction UP or DOWN). Thus, the active cell will always be moved inside
        // its current line (independently in what direction this results), and will then wrap to the next or
        // previous line. Similarly, the term "step" will be used for the single cells in a line (the TAB or
        // ENTER key moves the cell by one step in the current line).
        //
        // Example: If moving horizontally using the TAB key, the "line index" of a cell refers to its row
        // index, and the "step index" refers to its column index (moving the cell by one step will increase
        // the column index, moving to the next line will increase the row index).

        type MoveResult = "step" | "line" | "range";

        // current selection in the active sheet
        const selection = this.#getEffectiveSelection();
        // shortcut to the 'address' property in the selection
        let address = selection.address;
        // move in entire sheet, if the selection consists of a single cell only
        const singleCell = this.isSingleCellSelection();
        // whether to move the active cell forwards (right or down)
        const forward = !isLeadingDir(direction);
        // whether to move the active cell horizontally
        const vertical = isVerticalDir(direction);
        // readable flags for address index getters/setters (e.g. address.get(line) for the line index)
        const line = vertical;
        const step = !vertical;
        // models of the active sheet
        const { sheetModel, colCollection, rowCollection, cellCollection, mergeCollection } = this.docView;
        // the column/row collections for lines and steps
        const lineCollection = vertical ? colCollection : rowCollection;
        const stepCollection = vertical ? rowCollection : colCollection;
        // whether to jump to unlocked cells only
        const unlockedCells = !vertical && sheetModel.isLocked();

        // Increases the passed range array index, while cycling in the available selected ranges.
        const incRangeIndex = (index: number): number => math.modulo(index + 1, selection.ranges.length);
        // Decreases the passed range array index, while cycling in the available selected ranges.
        const decRangeIndex = (index: number): number => math.modulo(index - 1, selection.ranges.length);

        // Returns the index of the first available visible entry in the passed column/row collection
        // including the passed index, regardless whether that index is located inside the selected ranges.
        const findVisibleIndex = (collection: ColRowCollection, index: number): number => {
            return (forward ? collection.findNextVisibleIndex(index) : collection.findPrevVisibleIndex(index)) ?? -1;
        };

        // Returns the index of the preceding or following visible entry (depending on the direction) in the
        // passed column/row collection, regardless whether that index is located inside the selected ranges.
        const findNextVisibleIndex = (collection: ColRowCollection, index: number): number => {
            return (forward ? collection.findNextVisibleIndex(index + 1) : collection.findPrevVisibleIndex(index - 1)) ?? -1;
        };

        // Moves the active cell to the first or last visible cell of the previous or next selected range
        // (depending on the direction). The active cell may be located inside a merged range afterwards!
        const moveToNextRange = (): MoveResult => {
            let range: Range;
            do {
                selection.active = forward ? incRangeIndex(selection.active) : decRangeIndex(selection.active);
                range = selection.activeRange()!;
                address.c = findVisibleIndex(colCollection, forward ? range.a1.c : range.a2.c);
                address.r = findVisibleIndex(rowCollection, forward ? range.a1.r : range.a2.r);
            } while (!range.containsAddress(address));
            return "range";
        };

        // Moves the active cell to the preceding or next visible line of the current selected range (depending
        // on the direction). If there are no more visible lines in the current range, the first or last cell
        // of the next selected range will be activated. The active cell may be located inside a merged range
        // afterwards!
        const moveToNextLine = (): MoveResult => {
            const range = selection.activeRange()!;
            address.set(findNextVisibleIndex(lineCollection, address.get(line)), line);
            address.set(findVisibleIndex(stepCollection, (forward ? range.a1 : range.a2).get(step)), step);
            return range.containsAddress(address) ? "line" : moveToNextRange();
        };

        // Moves the active cell to the preceding or next visible step of the current line in the selected
        // range (depending on the direction). If there are no more visible steps in the line, the preceding
        // or next line will be activated. The active cell may be located inside a merged range afterwards!
        const moveToNextStep = (): MoveResult => {
            const range = selection.activeRange()!;
            address.set(findNextVisibleIndex(stepCollection, address.get(step)), step);
            return range.containsAddress(address) ? "step" : moveToNextLine();
        };

        // Returns the address of a merged range that contains the current active cell. The merged range will
        // be shrunken to its visible area.
        const getVisibleMergedRange = (): Opt<Range> => {
            const mergedRange = mergeCollection.getMergedRange(address);
            return mergedRange && sheetModel.shrinkRangeToVisible(mergedRange);
        };

        // Single cell selected: Simple movement without wrapping at sheet borders. Happens always for
        // vertical movement, but only in unlocked sheet for horizontal movement (see below). The flag
        // "unlockedCells" is only set to `true` for horizontal movement in locked sheets.
        if (singleCell && !unlockedCells) {
            this.#selectNextCell(direction, "cell");
            return;
        }

        // Moving horizontally through a locked sheet without selection wraps inside used area of sheet.
        if (singleCell && unlockedCells) {
            selection.ranges = new RangeArray(sheetModel.getUsedRange() ?? new Range(Address.A1.clone()));
            selection.active = 0;
        }

        // Do nothing, if the entire selection is hidden.
        if (sheetModel.areRangesHidden(selection.ranges)) { return; }

        // Ensure to start at the top-left cell of a merged range.
        let mergedRange = getVisibleMergedRange();
        if (mergedRange) { selection.address = address = mergedRange.a1.clone(); }

        // repeat advancing the selection until a valid result has been found
        for (;;) {

            // Find the next active cell. This may involve stepping into the next line, or even into the next
            // selected range in a multi-selection.
            const result = moveToNextStep();

            // Active cell did not land inside a merged range: Step was successful, exit the endless loop.
            if (!(mergedRange = getVisibleMergedRange())) { break; }

            // If the first visible line of the merged range has been entered, the range will actually be
            // selected as active cell. For backward movement, it is important to insert the leading step
            // index of the range into the active cell. Example: Stepping backwards into the merged range
            // B2:D4 activates cell D2 or B4, but must be corrected to cell B2.
            if (mergedRange.a1.get(line) === address.get(line)) {
                if ((forward ? mergedRange.a1 : mergedRange.a2).get(step) === address.get(step)) {
                    address.set(mergedRange.a1.get(step), step);
                    break;
                }
            }

            // Move the active cell to the last or first step in the line, to prevent looping over all inner
            // cells of the merged range in the current line. Example: Stepping forwards from cell A3 into
            // the merged range B2:Z4 moves active cell to B3. The following correction to cell Z3 will
            // prevent to loop over all covered cells in row 3.
            address.set((forward ? mergedRange.a2 : mergedRange.a1).get(step), step);

            // If a new line has been entered, check if the entire line consists of merged ranges only (e.g.
            // lots of vertical merged ranges with many rows when stepping horizontally). It would cause a
            // performance bottleneck when iterating unsuccessfully through all the single lines covered by
            // these merged ranges. Instead, the line index will be increased to the 'shortest' of all the
            // merged ranges in the line, to be able to reach the new cells above or below the merged ranges
            // faster.
            if (result === "line") {

                // Calculate the step intervals covered by the merged ranges, and by the current line.
                const lineRange = selection.activeRange()!.clone().setBoth(address.get(line), line);
                const mergedRanges = RangeArray.map(mergeCollection.getMergedRanges(lineRange), range => sheetModel.shrinkRangeToVisible(range));
                const mergedIntervals = mergedRanges.intervals(step).merge();
                const visibleIntervals = stepCollection.getVisibleIntervals(lineRange.interval(step));

                // Nothing to do, if there are cells in the line not covered by a merged range.
                if (mergedIntervals.contains(visibleIntervals)) {

                    // Sort the merged ranges according to move direction.
                    mergedRanges.sortBy(range => range.a1.get(step) * (forward ? 1 : -1));

                    // Find the merged range with maximum start line index, and minimum end line index.
                    let maxStartRange = mergedRanges[0];
                    let minEndRange = mergedRanges[0];
                    for (const range of mergedRanges) {
                        if (range.a1.get(line) > maxStartRange.a1.get(line)) { maxStartRange = range; }
                        if (range.a2.get(line) < minEndRange.a2.get(line)) { minEndRange = range; }
                    }

                    // In backward direction, always move to the range with the maximum start line index.
                    // In forward direction, if a merged range starts in the current line, select it.
                    if (!forward || (maxStartRange.a1.get(line) === address.get(line))) {
                        selection.address = address = maxStartRange.a1.clone();
                        break;
                    }

                    // Otherwise (in forward direction), move to the end of the line that contains the end
                    // of the shortest merged range. This is needed to be able to step into the next line,
                    // or the next selected range as needed (the merged range may end together with the
                    // selected range).
                    address.set(minEndRange.a2.get(line), line);
                    address.set(lineRange.a2.get(step), step);
                }
            }
        }

        // When traversing unlocked cells in a locked sheet, and the new cell is locked, the next unlocked
        // cell in the selection must be searched now.
        if (unlockedCells && !cellCollection.getAttributeSet(address).cell.unlocked) {

            // Use the cell collection to search for the next cell with unlocked flag.
            const match = cellCollection.findCellWithAttributes(selection.ranges, { cell: { unlocked: true } }, {
                visible: true,
                reverse: !forward,
                startIndex: selection.active,
                startAddress: selection.address,
                skipStart: true,
                wrap: true
            });

            // If there is no unlocked cell available, do not change the current selection.
            if (!match) { return; }

            // Update the selection object according to the found cell. The cell descriptor returned by the
            // cell collection contains the array index of the cell range address.
            selection.address = address = match.address;
            selection.active = match.index;
        }

        // In single cell mode (stepping through unlocked cells), select the new cell, otherwise set the entire
        // selection instance with the new active cell. Always scroll to the new active cell.
        if (singleCell) {
            this.selectCell(address, { storeOrigin: true });
        } else {
            this.setCellSelection(selection);
        }
        this.docView.scrollToCell(address);
    }

    /**
     * Selects the specified drawing frames in the active sheet.
     *
     * @param positions
     *  The positions of all drawing frames to be selected.
     */
    setDrawingSelection(positions: Position[]): void {

        // the current selection to be modified (keep cell selection as it is)
        const selection = this.getSelection();
        selection.drawings = positions;

        // commit new selection
        this.#setSheetSelection(selection);
    }

    /**
     * Selects a single drawing frame in the active sheet.
     *
     * @param position
     *  The position of the drawing frame to be selected.
     *
     * @param [options]
     *  Optional parameters.
     */
    selectDrawing(position: Position, options?: SelectDrawingOptions): void {

        // do not change the selection in chart sheets etc.
        if (!this.docView.sheetModel.isCellType) { return; }

        // the current selection to be modified (keep cell selection as it is)
        const selection = this.getSelection();

        // construct the new drawing selection
        if (!options?.toggle) {
            selection.drawings = [position];
        } else if (MULTI_SELECTION) {
            selection.toggleDrawing(position);
        }

        // commit new selection
        this.#setSheetSelection(selection);

        // leave text edit mode; back to cell selection if that fails (e.g. syntax error in formula)
        this.docView.leaveTextEditMode().fail(() => this.removeDrawingSelection());
    }

    /**
     * Removes selection from all selected drawing objects and restores the
     * cell selection.
     */
    removeDrawingSelection(): void {

        // the current selection to be modified (keep cell selection as it is)
        const selection = this.getSelection();

        // remove drawing selection
        selection.drawings.length = 0;

        // commit new selection
        this.#setSheetSelection(selection);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a reference to the original selection of the active sheet.
     */
    #getSheetSelection(): SheetSelection {
        const { sheetModel, sheetPropSet } = this.docView;
        // bug 57991: sheet model is not available after document import error
        return sheetModel ? sheetPropSet.get("selection") : new SheetSelection();
    }

    /**
     * Changes the selection of the active sheet.
     */
    #setSheetSelection(selection: SheetSelection): void {
        this.docView.sheetModel.setSelection(selection);
    }

    /**
     * Used in tracking mode for header panes and grid panes. Updates the
     * active range in the current selection, and updates the view property
     * "trackingRange".
     */
    #setTrackingRange(trackingRange: Range, trackingActive: boolean): void {
        this.changeActiveRange(trackingRange, trackingActive ? undefined : { unique: true });
        this.docView.propSet.set("trackingRange", trackingActive ? trackingRange : null);
    }

    /**
     * Returns a selection object that is suitable for keyboard navigation.
     * Prefers the current cell selection provider if available.
     *
     * @param [allowEmpty=false]
     *  If set to `true`, and the registered cell selection handler returns a
     *  selection without any ranges (property "ranges" set to an empty array),
     *  this selection object will be returned unmodified. Otherwise, an empty
     *  selection returned by that handler will be replaced by a selection
     *  consisting of the current active cell.
     *
     * @returns
     *  The active sheet selection.
     */
    #getEffectiveSelection(allowEmpty = false): SheetSelection {

        // the custom selection provided by the registered cell selection handler
        const selection = this.#cellSelectionProvider?.get();

        // no custom selection: use real selection of this sheet
        if (!selection) {
            return this.getSelection();
        }

        // replace empty custom selection with active cell
        if (selection.ranges.empty() && !allowEmpty) {
            return SheetSelection.createFromAddress(this.getActiveCell());
        }

        // bug 51024: do not modify original selection object in-place
        return selection.clone();
    }

    /**
     * Changes the current selection of the active sheet. Prefers the current
     * cell selection provider if available.
     *
     * @param selection
     *  The new sheet selection.
     */
    #setEffectiveSelection(selection: SheetSelection): void {

        // prefer the registered cell selection handler
        const result = this.#cellSelectionProvider?.set(selection);
        if (result === true) { return; }

        // leave text edit mode before changing the selection
        this.docView.leaveTextEditMode().done(() => this.#setSheetSelection(selection));
    }

    /**
     * Returns the address of the cell that will be selected with the HOME key.
     * In unfrozen views, this is always cell A1. In frozen views, this is the
     * first available cell in the unfrozen bottom-right grid pane.
     *
     * @returns
     *  The address of the cell to be selected with the HOME key.
     */
    #getHomeCell(): Address {
        const { sheetModel } = this.docView;
        const frozenSplit = sheetModel.hasFrozenSplit();
        const colInterval = frozenSplit ? sheetModel.getSplitColInterval() : null;
        const rowInterval = frozenSplit ? sheetModel.getSplitRowInterval() : null;
        return new Address(colInterval ? (colInterval.last + 1) : 0, rowInterval ? (rowInterval.last + 1) : 0);
    }

    /**
     * Returns the address of the cell that will be selected with the END key
     * (the bottom-right cell in the used area of the sheet).
     *
     * @returns
     *  The address of the cell to be selected with the END key.
     */
    #getEndCell(): Address {
        const usedRange = this.docView.sheetModel.getUsedRange();
        return usedRange ? usedRange.a2 : Address.A1;
    }

    /**
     * Expands the passed range address so that it completely includes all
     * merged ranges it currently overlaps. In case the passed range is a full
     * column or full row range, it will be expanded to full-column or full-row
     * merged ranges only.
     *
     * @param range
     *  A cell range address.
     *
     * @returns
     *  The address of the expanded cell range.
     */
    #expandRangeToMergedRanges(range: Range): Range {

        const { mergeCollection } = this.docView;

        // full column/row ranges: expand to full-size merged ranges only (bug 33313)
        if (this.#addressFactory.isColRange(range) || this.#addressFactory.isRowRange(range)) {
            return mergeCollection.getBoundingMergedRange(range) ?? range;
        }

        // expand other ranges to all the merged ranges it overlaps
        return mergeCollection.expandRangeToMergedRanges(range);
    }

    /**
     * Clears the current selection and selects the specified cell. If the cell
     * is hidden, tries to select the nearest visible cell.
     */
    #selectVisibleCell(address: Address): void {
        const visAddress = this.docView.sheetModel.getVisibleCell(address, VisibleEntryMethod.PREV_NEXT);
        if (visAddress) {
            this.selectCell(visAddress);
            this.docView.scrollToCell(visAddress);
        }
    }

    /**
     * Expands the active range of the current selection to the passed cell
     * address.
     */
    #expandToVisibleCell(address: Address, shrinkEnd: boolean): void {

        // current selection in the active sheet
        const selection = this.#getEffectiveSelection();
        // the active range in the selection
        const activeRange = selection.activeRange()!;

        const visAddress = this.docView.sheetModel.getVisibleCell(address, VisibleEntryMethod.PREV_NEXT);
        if (visAddress) {

            // start position of active range: extend to passed address,
            // if it is located before, but do not shrink the active range
            activeRange.a1.c = Math.min(visAddress.c, activeRange.a1.c);
            activeRange.a1.r = Math.min(visAddress.r, activeRange.a1.r);

            // end position of active range: extend to passed address, if it
            // is located after the range, but shrink to active cell if not
            activeRange.a2.c = Math.max(visAddress.c, shrinkEnd ? selection.address.c : activeRange.a2.c);
            activeRange.a2.r = Math.max(visAddress.r, shrinkEnd ? selection.address.r : activeRange.a2.r);

            // commit new selection, adjust scrolling position
            this.changeActiveRange(activeRange);
            this.docView.scrollToCell(visAddress);
        }
    }

    /**
     * Searches for the next/last filled cell in the current cell collection.
     */
    #findNextUsedCell(address: Address, direction: Direction): Address {

        // whether to move the active cell forwards (right or down)
        const forward = !isLeadingDir(direction);
        // whether to move the active cell horizontally
        const columns = !isVerticalDir(direction);
        // the resulting address
        const resultAddress = address.clone();

        // range address of the used area
        const { cellCollection } = this.docView;
        const usedRange = cellCollection.getUsedRange();
        const lastCell = usedRange ? usedRange.a2 : Address.A1;

        // maximum column/row index in the sheet (in move direction)
        const maxIndex = this.#addressFactory.getMaxIndex(columns);
        // column/row index of the start cell
        const startIndex = address.get(columns);
        // column/row index of last visited cell
        let lastIndex: Opt<number>;

        // forward mode: jump to end of sheet, if start cell is outside used area,
        // also if inside the last column/row of the used area, according to direction
        if (forward && ((address.get(!columns) > lastCell.get(!columns)) || (address.get(columns) >= lastCell.get(columns)))) {
            resultAddress.set(maxIndex, columns);
            return resultAddress;
        }

        // backward mode: jump to beginning of sheet, if start cell is outside
        // used area, and the new address will also be outside the used area
        if (!forward && (address.get(!columns) > lastCell.get(!columns))) {
            resultAddress.set(0, columns);
            return resultAddress;
        }

        // find next available content cell in the cell collection
        for (const cellEntry of cellCollection.linearCellEntries(address, direction, { type: "value", visible: true, skipCovered: true })) {

            // column/row index of the current cell
            const currIndex = cellEntry.address.get(columns);

            // started from an empty cell: stick to first content cell
            if (!is.number(lastIndex) && (startIndex !== currIndex)) {
                resultAddress.set(currIndex, columns);
                break;
            }

            // found a content cell following an empty cell
            if (is.number(lastIndex) && (Math.abs(currIndex - lastIndex) > 1)) {

                // use the current cell, if iteration has been started at the cell
                // before the gap; otherwise use the cell preceding the gap
                resultAddress.set((startIndex === lastIndex) ? currIndex : lastIndex, columns);
                break;
            }

            // remember address of current content cell for next iteration
            lastIndex = currIndex;
        }

        // jump to last found content cell if it is not the start cell;
        // or to sheet boundaries, if no valid content cell could be found
        if (address.equals(resultAddress)) {
            resultAddress.set(is.number(lastIndex) && (lastIndex !== startIndex) ? lastIndex : forward ? maxIndex : 0, columns);
        }

        return resultAddress;
    }

    /**
     * Clears the current selection and moves the active cell into the
     * specified direction. If the target cell is hidden, moves the cursor to
     * the next available visible cell.
     *
     * @param direction
     *  The direction the active cell will be moved to.
     *
     * @param mode
     *  The move mode. Supported values are:
     *  - "cell": Moves the active cell to the next visible cell.
     *  - "page": Moves the active cell by the width/height of the visible
     *    area, and adjusts the scroll position in a way that the new active
     *    cell remains on the same screen position if possible.
     *  - "used": Moves the active cell to the next available non-empty cell;
     *    or, if none available, to the first or last cell in the sheet.
     */
    #selectNextCell(direction: Direction, mode: "cell" | "page" | "used"): void {

        // current selection in the active sheet
        const selection = this.#getEffectiveSelection();
        // the cell to start movement from
        const activeCell = selection.origin ?? selection.address;
        // the merged range covering the active cell
        const mergedRange = this.docView.mergeCollection.getMergedRange(activeCell);
        // whether to move the active cell forwards (right or down)
        const forward = !isLeadingDir(direction);
        // whether to move the active cell horizontally
        const columns = !isVerticalDir(direction);
        // the column/row collection in the move direction
        const collection = columns ? this.docView.colCollection : this.docView.rowCollection;

        switch (mode) {
            case "cell": {
                // start from leading/trailing border of a merged range
                let index = !mergedRange ? activeCell.get(columns) : forward ? mergedRange.getEnd(columns) : mergedRange.getStart(columns);
                index += (forward ? 1 : -1);

                // insert new column/row entry (old selection will collapse to active cell, if no other cell has been found)
                const method = forward ? VisibleEntryMethod.NEXT : VisibleEntryMethod.PREV;
                const result = collection.findVisibleIndex(index, method);
                if (is.number(result)) { activeCell.set(result, columns); }

                // set selection to the new active cell, scroll to the cell
                this.selectCell(activeCell, { storeOrigin: true });
                this.docView.scrollToCell(activeCell);
                break;
            }

            case "page": {
                // the active (focused) grid pane
                const gridPane = this.docView.getActiveGridPane();
                // the associated header pane in moving direction
                const headerPane = gridPane.getHeaderPane(columns);

                // bug 37023: correctly jump out of a frozen pane
                const leaveFrozen = forward && headerPane.isFrozen();
                const interval = leaveFrozen && headerPane.getAvailableInterval();
                if (interval) {
                    activeCell.set(interval.last + 1, columns);
                    this.selectCell(activeCell, { storeOrigin: true });
                    this.docView.scrollToCell(activeCell);
                    return;
                }

                // get position and size of the active cell
                const cellRect = mergedRange ? this.docView.getRangeRectangle(mergedRange) : this.docView.getCellRectangle(activeCell);
                const cellOffset = columns ? cellRect.left : cellRect.top;
                const cellSize = columns ? cellRect.width : cellRect.height;
                // get position and size of the visible area in this grid pane
                const visibleRect = gridPane.getVisibleRectangle();
                const visibleOffset = columns ? visibleRect.left : visibleRect.top;
                const visibleSize = columns ? visibleRect.width : visibleRect.height;

                // returns the descriptor of a new entry when moving in 'page' mode
                const getPageTargetEntry = (offset: number): EntryOffsetDescriptor<"column" | "row"> => {
                    return collection.getEntryByOffset(offset + (forward ? visibleSize : -visibleSize) * 0.98, { pixel: true });
                };

                // changes the column/row index of the active cell according to move direction without scrolling
                const selectCellForPage = (newIndex: number): void => {
                    const address = activeCell.clone();
                    address.set(newIndex, columns);
                    this.selectCell(address, { storeOrigin: true });
                    this.docView.grabFocus({ targetAddress: address });
                };

                // check if the active cell is located inside the visible area
                if ((visibleOffset <= cellOffset) && (cellOffset + cellSize <= visibleOffset + visibleSize)) {
                    // find the column/row one page away
                    const entryData = getPageTargetEntry(cellOffset + cellSize / 2);
                    selectCellForPage(entryData.index);
                    headerPane.scrollRelative(entryData.offset - cellOffset);
                } else {
                    // otherwise, scroll one page and select the top visible entry
                    const entryData = getPageTargetEntry(visibleOffset);
                    selectCellForPage(entryData.index);
                    headerPane.scrollTo(entryData.offset);
                }
                break;
            }

            case "used": {
                // search for a target cell
                const targetCell = this.#findNextUsedCell(activeCell, direction);

                // fail-safety: if cell address could not be determined or did not change, make a single cell step
                if (!targetCell || activeCell.equals(targetCell)) {
                    this.#selectNextCell(direction, "cell");
                } else {
                    // set selection to the new active cell
                    this.selectCell(targetCell, { storeOrigin: true });
                    this.docView.scrollToCell(targetCell);
                }
                break;
            }
        }
    }

    /**
     * Extends the active range in the current selection into the specified
     * direction. If the target column/row is hidden, extends the active range
     * to the next available visible column/row.
     *
     * @param direction
     *  The direction the active cell will be moved to.
     *
     * @param mode
     *  The move mode. Supported values are:
     *  - "cell": Expands the active range to the next visible column/row.
     *  - "page": Expands the active range by the width/height of the visible
     *    area, and adjusts the scroll position in a way that the range border
     *    remains on the same screen position if possible.
     *  - "used": Expands the active range to the next available non-empty
     *    cell; or, if none available, to the first or last column/row in the
     *    sheet.
     */
    #expandToNextCell(direction: Direction, mode: "cell" | "page" | "used"): void {

        const { mergeCollection } = this.docView;

        // current selection in the active sheet
        const selection = this.#getEffectiveSelection();
        // the cell to start movement from
        const activeCell = (selection.origin ?? selection.address).clone();
        // the current active range address
        const activeRange = selection.activeRange()!;
        // the merged range covering the active cell, or the active cell as range object
        const mergedRange = mergeCollection.getMergedRange(activeCell) ?? Range.fromAddresses(activeCell);
        // the new active range address
        let newRange: Opt<Range>;

        // whether to expand the active range forwards (right or down)
        const forward = !isLeadingDir(direction);
        // whether to expand the active range horizontally
        const columns = !isVerticalDir(direction);

        // the column/row collection in the move direction
        const collection = columns ? this.docView.colCollection : this.docView.rowCollection;
        // the active (focused) grid pane
        const gridPane = this.docView.getActiveGridPane();

        // returns the descriptor of a new entry when moving in 'page' mode
        const getPageTargetIndex = (index: number): Opt<number> => {

            // the descriptor for the start entry
            const entryData = collection.getEntry(index);
            // the position in the middle of the entry
            const entryOffset = entryData.offset + entryData.size / 2;
            // the size of the visible area, in pixels
            const visibleRect = gridPane.getVisibleRectangle();
            const visibleSize = columns ? visibleRect.width : visibleRect.height;

            // return the target index
            return collection.getEntryByOffset(entryOffset + (forward ? visibleSize : -visibleSize) * 0.98, { pixel: true })?.index;
        };

        // returns the descriptor of a new entry when moving in 'used' mode
        const getUsedTargetIndex = (address: Address): Opt<number> => {
            const targetCell = this.#findNextUsedCell(address, direction);
            const method = forward ? VisibleEntryMethod.NEXT : VisibleEntryMethod.PREV;
            return collection.findVisibleIndex(targetCell.get(columns), method);
        };

        // first, try to shrink the range at the correct border according to move direction
        activeCell.set(forward ? activeRange.getStart(columns) : activeRange.getEnd(columns), columns);
        let targetIndex: Opt<number>;
        switch (mode) {
            case "cell":
                targetIndex = forward ?
                    collection.findVisibleIndex(activeCell.get(columns) + 1, VisibleEntryMethod.NEXT) :
                    collection.findVisibleIndex(activeCell.get(columns) - 1, VisibleEntryMethod.PREV);
                break;

            case "page":
                targetIndex = getPageTargetIndex(activeCell.get(columns));
                break;

            case "used":
                targetIndex = getUsedTargetIndex(activeCell);
                break;
        }

        // build the new range address according to the collection entry
        if (is.number(targetIndex) && (forward ? (targetIndex <= mergedRange.getStart(columns)) : (mergedRange.getEnd(columns) <= targetIndex))) {
            newRange = activeRange.clone();
            (forward ? newRange.a1 : newRange.a2).set(targetIndex, columns);
            // shrink the range until it does not contain any merged ranges partly
            newRange = mergeCollection.shrinkRangeFromMergedRanges(newRange, columns);
        }

        // if shrinking the range is not possible or has failed, expand the range away from active cell
        if (!newRange) {
            activeCell.set(forward ? activeRange.getEnd(columns) : activeRange.getStart(columns), columns);

            switch (mode) {
                case "cell":
                    targetIndex = forward ?
                        collection.findVisibleIndex(activeCell.get(columns) + 1, VisibleEntryMethod.NEXT) :
                        collection.findVisibleIndex(activeCell.get(columns) - 1, VisibleEntryMethod.PREV);
                    break;

                case "page":
                    targetIndex = getPageTargetIndex(activeCell.get(columns));
                    break;

                case "used":
                    targetIndex = getUsedTargetIndex(activeCell);
                    break;
            }

            // build the new range address according to the collection entry
            if (is.number(targetIndex)) {
                newRange = activeRange.clone();
                // expand in move direction, restrict to original active cell on the other border
                (forward ? newRange.a2 : newRange.a1).set(targetIndex, columns);
                (forward ? newRange.a1 : newRange.a2).set(selection.address.get(columns), columns);
                // expand the range until it contains all merged ranges completely
                newRange = mergeCollection.expandRangeToMergedRanges(newRange);
            }
        }

        // change the active range, if a new range has been found
        if (newRange?.differs(activeRange)) {
            this.changeActiveRange(newRange);
        }

        // scroll to target entry (this allows e.g. to scroll inside a large merged range without changing the selection)
        if (is.number(targetIndex)) {
            // activate correct grid pane in frozen split mode
            activeCell.set(targetIndex, columns);
            this.docView.grabFocus({ targetAddress: activeCell });
            this.docView.getActiveHeaderPane(columns).scrollToEntry(targetIndex);
        }
    }

    /**
     * Expands the selected ranges to entire columns or rows.
     *
     * @param columns
     *  If set to `true`, the current selection will be expanded to full
     *  columns. If set to `false`, the current selection will be expanded to
     *  full rows.
     */
    #expandSelectionToSheet(columns: boolean): void {

        // current selection in the active sheet
        const selection = this.#getEffectiveSelection();
        // the last available column/row index in the sheet
        const lastIndex = this.#addressFactory.getMaxIndex(!columns);

        // expand all ranges to the sheet borders
        const { mergeCollection } = this.docView;
        selection.ranges.forEach((range, index) => {
            range.setStart(0, !columns);
            range.setEnd(lastIndex, !columns);
            selection.ranges[index] = mergeCollection.expandRangeToMergedRanges(range);
        });

        this.setCellSelection(selection, { unique: true });
    }

    /**
     * Selects the content range (the entire range containing consecutive
     * content cells) surrounding the current active cell (ignoring the current
     * selected ranges).
     */
    #selectContentRange(): void {

        // current active cell in the active sheet
        const activeCell = this.#getEffectiveSelection().address;
        // the content range to be selected
        const contentRange = this.docView.cellCollection.findContentRange(new Range(activeCell));

        // select the resulting expanded range
        this.selectRange(contentRange, { active: activeCell });
    }

    /**
     * Selects the specified drawing frame in the active sheet, and scrolls the
     * active grid pane to that drawing frame.
     */
    #selectAndScrollToDrawing(position: Position): void {
        this.selectDrawing(position);
        this.docView.scrollToDrawingFrame(position);
    }

    /**
     * Creates an iterator for drawing objects that are not hidden by any
     * formatting attributes, nor are located in hidden columns/rows.
     *
     * @yields
     *  The visible drawing models.
     */
    *#yieldVisibleDrawingModels(options?: YieldIndexedDrawingModelsOptions): SheetDrawingModelIterator {

        // skip drawing objects that are located in hidden columns/rows
        for (const drawingModel of this.docView.drawingCollection.yieldModels({ visible: true, ...options })) {
            if (drawingModel.getRectangle()) {
                yield drawingModel;
            }
        }
    }

    /**
     * Selects the first visible drawing frame in the active sheet.
     */
    #selectFirstDrawing(): void {

        // find the first visible drawing model located in the sheet
        const drawingModel = itr.shift(this.#yieldVisibleDrawingModels());

        // select the drawing object if available
        if (drawingModel) {
            this.#selectAndScrollToDrawing(drawingModel.getPosition());
        }
    }

    /**
     * Selects the previous or next visible drawing frame.
     */
    #selectNextDrawing(reverse: boolean): void {

        // last drawing selected in the active sheet
        const position = ary.at(this.getSelectedDrawings(), -1);
        // TODO: selection in embedded objects
        if (!position || (position.length > 1)) { return; }

        // index of the drawing object currently selected
        const index = ary.at(position, -1)!;

        // index of the next drawing object to be selected
        const begin = math.modulo(index + (reverse ? -1 : 1), this.docView.drawingCollection.getModelCount());
        let drawingModel = itr.shift(this.#yieldVisibleDrawingModels({ reverse, begin }));

        // no more drawings available: wrap to first drawing (reverse mode: wrap to last drawing)
        if (!drawingModel) {
            drawingModel = itr.shift(this.#yieldVisibleDrawingModels({ reverse, end: index }));
        }

        // select the drawing object if available
        if (drawingModel) {
            this.#selectAndScrollToDrawing(drawingModel.getPosition());
        }
    }

    /**
     * Selects all visible drawing frames.
     */
    #selectAllDrawings(): void {

        // iterator that visits all visible drawing objects (TODO: selection in embedded objects)
        const iterator = this.#yieldVisibleDrawingModels();
        // the positions of all visible drawing objects
        const positions = Array.from(iterator, drawingModel => drawingModel.getPosition());

        // select the drawing objects if available
        if (positions.length > 0) {
            this.setDrawingSelection(positions);
        }
    }

    /**
     * Registers the selected drawing objects with in the selection engine of
     * the text framework.
     */
    #refreshTextSelection(): void {
        const positions = this.getSelectedDrawings();
        if (positions.length > 0) {
            // add sheet index to the positions, as expected by the text framework
            positions.forEach(position => position.unshift(this.docView.activeSheet));
            this.#textSelection.setMultiDrawingSelectionByPosition(positions);
        } else {
            this.#textSelection.clearMultiSelection();
            this.#textSelection.setEmptySelection({ preserveFocus: true });
        }
    }

    /**
     * Inserts the source ranges of the cached chart model into the token array
     * used for highlighting.
     */
    #refreshHighlightingTokenArray(): void {
        if (this.#highlightedChart) {
            const { activeSheet } = this.docView;
            const sourceRanges3d = this.#highlightedChart.resolveRanges();
            const sourceRanges2d = sourceRanges3d.map(range => range.isSheet(activeSheet) ? range.toRange() : undefined);
            this.#highlightTokenArray.clearTokens().appendRangeList(sourceRanges2d, { abs: true, sheet: activeSheet });
        }
    }

    /**
     * Updates range highlighting of the selected chart object.
     */
    #refreshChartHighlighting(): void {

        const { drawingCollection, rangeHighlighter } = this.docView;

        // stop listening to the previous chart model
        if (this.#highlightedChart) {
            this.stopListeningTo(this.#highlightedChart);
            this.#highlightedChart = undefined;
        }

        // stop highlighting source ranges of a previously selected chart
        if (this.#chartHighlightUid) {
            rangeHighlighter.endHighlighting(this.#chartHighlightUid);
            this.#chartHighlightUid = null;
        }

        // highlight the source ranges of a single selected chart object
        const positions = this.getSelectedDrawings();
        if (positions.length !== 1) { return; }
        const chartModel = drawingCollection.getModel(positions[0], { type: DrawingType.CHART }) as Opt<SheetChartModel>;
        if (!chartModel) { return; }

        // initialize the token array used for highlighting
        this.#highlightedChart = chartModel;
        this.#refreshHighlightingTokenArray();
        this.listenTo(chartModel, "change:drawing", this.#refreshHighlightingTokenArray);

        // start highlighting the chart source ranges
        this.#chartHighlightUid = rangeHighlighter.startHighlighting(this.#highlightTokenArray, { priority: true });
    }

    /**
     * Handles "keydown" events from all grid panes to adjust the current
     * selection.
     *
     * @returns
     *  Whether the key event has been handled and must not need to be
     *  processed anymore.
     */
    #handleKeyDownEvent(event: KeyboardEvent): boolean {

        // nothing to do in chart sheets etc.
        if (!this.docView.sheetModel.isCellType) { return false; }

        // special handling for drawing selection
        if (this.hasDrawingSelection()) {

            // traverse through drawing frames with TAB key
            if (matchKeyCode(event, "TAB", { shift: null })) {
                this.#selectNextDrawing(event.shiftKey);
                return true;
            }

            // select all drawing objects
            if (isSelectAllKeyEvent(event)) {
                this.#selectAllDrawings();
                return true;
            }

            return false;
        }

        // TAB/ENTER: move active cell to its neighbor according to the pressed key
        const moveDir = getCellMoveDirection(event);
        if (moveDir) {
            this.moveActiveCell(moveDir);
            return true;
        }

        // if OX Viewer uses Spreadsheet, cursor key are handled by OX Viewer
        // and cursor up/down/left/right with CTRL/META are used for simple cell movement.
        const viewerMode = this.docApp.isViewerMode();

        const selectOrExpandNextCell = (direction: Direction, mode: "cell" | "page" | "used", skipViewerMode = false): boolean => {
            if (viewerMode && skipViewerMode) { return false; }
            if (event.shiftKey) {
                this.#expandToNextCell(direction, mode);
            } else {
                this.#selectNextCell(direction, mode);
            }
            return true;
        };

        const selectOrExpandToVisibleCell = (address: Address, shrinkEnd: boolean): boolean => {
            if (event.shiftKey) {
                this.#expandToVisibleCell(address, shrinkEnd);
            } else {
                this.#selectVisibleCell(address);
            }
            return true;
        };

        // simple movements without modifier keys
        if (matchModifierKeys(event, { shift: null })) {
            switch (event.keyCode) {
                // if OX Spreadsheet is plugged into OX Viewer, cursor left/right switches between view items
                case KeyCode.LEFT_ARROW:    return selectOrExpandNextCell(Direction.LEFT, "cell", true);
                case KeyCode.RIGHT_ARROW:   return selectOrExpandNextCell(Direction.RIGHT, "cell", true);
                // if OX Spreadsheet is plugged into OX Viewer, cursor up/down has no function
                case KeyCode.UP_ARROW:      return selectOrExpandNextCell(Direction.UP, "cell", true);
                case KeyCode.DOWN_ARROW:    return selectOrExpandNextCell(Direction.DOWN, "cell", true);
                case KeyCode.PAGE_UP:       return selectOrExpandNextCell(Direction.UP, "page");
                case KeyCode.PAGE_DOWN:     return selectOrExpandNextCell(Direction.DOWN, "page");
                case KeyCode.HOME:          return selectOrExpandToVisibleCell(new Address(this.#getHomeCell().c, this.#getEffectiveSelection().address.r), false);
                case KeyCode.END:           return selectOrExpandToVisibleCell(new Address(this.#getEndCell().c, this.#getEffectiveSelection().address.r), true);
            }
        }

        // additional selection in page mode with ALT modifier key
        if (matchModifierKeys(event, { shift: null, alt: true })) {
            switch (event.keyCode) {
                case KeyCode.PAGE_UP:   return selectOrExpandNextCell(Direction.LEFT, "page");
                case KeyCode.PAGE_DOWN: return selectOrExpandNextCell(Direction.RIGHT, "page");
            }
        }

        // additional selection with CTRL or META modifier key
        if (matchModifierKeys(event, { shift: null, ctrlOrMeta: true })) {
            const cellMoveMode = viewerMode ? "cell" : "used";
            switch (event.keyCode) {
                case KeyCode.LEFT_ARROW:    return selectOrExpandNextCell(Direction.LEFT, cellMoveMode);
                case KeyCode.RIGHT_ARROW:   return selectOrExpandNextCell(Direction.RIGHT, cellMoveMode);
                case KeyCode.UP_ARROW:      return selectOrExpandNextCell(Direction.UP, cellMoveMode);
                case KeyCode.DOWN_ARROW:    return selectOrExpandNextCell(Direction.DOWN, cellMoveMode);
                case KeyCode.HOME:          return selectOrExpandToVisibleCell(this.#getHomeCell(), false);
                case KeyCode.END:           return selectOrExpandToVisibleCell(this.#getEndCell(), true);
            }
        }

        // CTRL+asterisk (numeric pad): select content range around active cell
        if (matchKeyCode(event, "NUM_MULTIPLY", { ctrlOrMeta: true })) {
            this.#selectContentRange();
            return true;
        }

        // bug 31247: remaining shortcuts not in cell edit mode
        if (this.docView.isTextEditMode("cell")) {
            return false;
        }

        // convert selection to entire columns
        if (matchKeyCode(event, "SPACE", { ctrlOrMeta: true })) {
            this.#expandSelectionToSheet(true);
            return true;
        }

        // convert selection to entire rows
        if (matchKeyCode(event, "SPACE", { shift: true })) {
            this.#expandSelectionToSheet(false);
            return true;
        }

        // select entire sheet
        if (matchKeyCode(event, "SPACE", { shift: true, ctrlOrMeta: true }) || isSelectAllKeyEvent(event)) {
            this.selectRange(this.#addressFactory.getSheetRange(), { active: this.getActiveCell() });
            return true;
        }

        // SHIFT+F4: select the first visible drawing frame
        if (matchKeyCode(event, "F4", { shift: true })) {
            this.#selectFirstDrawing();
            return true;
        }

        return false;
    }

    /**
     * Registers a listener at the specified event emitter. Execution of the
     * event handler will be deferred until the browser focus is located inside
     * the application pane.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param type
     *  The type of the event to listen to. Can be a space-separated list of
     *  event type names.
     *
     * @param fn
     *  The event listener that will be invoked for the events fired by the
     *  specified event emitter.
     */
    #listenToWithFocus<E, K extends keyof E>(emitter: DocsEmitter<E>, type: K, fn: TypedEventHandlerFn<this, E, K>): void {

        // This is a fix for bug 30513: When entering something into a text field (e.g. a new font size), and directly
        // clicking into a new cell without pressing ENTER key, the following events have been occurred before:
        //
        // 1. "mousedown" in the grid pane will start to select the clicked cell.
        // 2. "blur" in the text field will trigger the action attached to the text field.
        //
        // This results in setting the font size to the wrong (now selected) cell.
        //
        // With this pipeline, all selection events triggered by the grid panes, header panes, and corner pane will be
        // deferred until the browser focus returns to the application pane (i.e. any grid pane therein), or to the
        // text area (also in the formula pane), if cell text edit mode is currently active.

        // start listening to the specified event
        this.listenTo(emitter, type, (...args) => {

            // focusable elements to be observed
            const appPaneNode = this.docView.$appPaneNode[0];
            const textFocusNode = this.docView.getTextEditFocusNode();

            // nothing to wait for, if focus is in current text edit node (may not be part of
            // application pane), or somewhere in the application pane (grid panes, header panes)
            if ((textFocusNode && (getFocus() === textFocusNode)) || this.docView.hasAppFocus()) {
                fn.call(this, ...args);
                return;
            }

            // scope symbol for event handlers
            const scope = Symbol("scope");

            // wait for browser focus to return into the application pane
            const promise = new Promise<void>(resolve => {
                // convert to a function that ignores its parameters (do not feed promise with event data)
                const handler: VoidFunction = () => resolve();
                // fail-safety (prevent deferring selection for too long): timeout after 200ms
                // (intentionally calling `window.setTimeout` directly here does not harm during quit process)
                window.setTimeout(resolve, 200);
                // listen to the next "focusin" event as long as the promise is pending (timeout, see above)
                this.listenTo(appPaneNode, "focusin", handler, { scope });
                // same for the current text focus node (e.g., some <textarea> attached somewhere in the DOM)
                if (textFocusNode) { this.listenTo(textFocusNode, "focusin", handler, { scope }); }
            });

            // invoke specified event handler callback when promise filfils
            this.onFulfilled(promise, () => {
                this.stopListeningToScope(scope);
                fn.call(this, ...args);
            });
        });
    }
}
