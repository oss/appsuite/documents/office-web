/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import type { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * Configuration options for custom selection mode (select a custom cell range
 * independently from sheet selection with marching-ants effect).
 */
export interface CustomSelectionModeConfig {

    /**
     * A callback function that will be invoked when the custom cell range has
     * been selected successfully (not canceled).
     */
    applySelected?: (selection: SheetSelection) => void;

    /**
     * The initial selection shown when starting the custom selection mode.
     */
    selection?: SheetSelection;

    /**
     * The status label to be shown in the status pane while custom selection
     * mode is active.
     */
    statusLabel?: string;
}

// class CustomSelectionActor =================================================

/**
 * Handles active custom selection mode (used to select additional cell ranges
 * independently from the current sheet selection).
 */
export class CustomSelectionActor extends ViewObject<SpreadsheetView> {

    /** The type identifier of the custom selection mode. */
    #type: Opt<string>;
    /** Configuration of the custom selection tracking cycle. */
    #config: Opt<CustomSelectionModeConfig>;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView) {
        super(docView);

        // cancel custom selection mode when losing edit rights, or before applying any operations
        this.listenTo(this.docApp, "docs:editmode:leave", () => this.leaveCustomMode(false));
        this.listenTo(this.docModel, "operations:before", () => this.leaveCustomMode(false));

        // cancel the custom selection mode when the active sheet changes (TODO: allow on multiple sheets)
        this.listenTo(docView, "change:activesheet", () => this.leaveCustomMode(false));

        // cancel custom selection mode when selecting drawings
        this.listenTo(docView, "change:selection", selection => {
            if (selection.drawings.length) { this.leaveCustomMode(false); }
        });

        // cancel custom selection mode when entering text edit mode
        this.listenTo(docView, "textedit:enter", () => this.leaveCustomMode(false));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the custom selection mode is currently active.
     *
     * @param [type]
     *  The type identifier for the custom selection mode, as passed to the
     *  method `enterCustomMode`. If omitted, this method returns whether any
     *  custom selection mode is active.
     */
    isActive(type?: string): boolean {
        return type ? (type === this.#type) : !!this.#type;
    }

    /**
     * Initializes a new custom selection mode.
     *
     * @param type
     *  An arbitrary string that identifies the type of custom selection mode
     *  that is currently active.
     *
     * @param [config]
     *  Configuration options for custom selection mode.
     */
    enterCustomMode(type: string, config?: CustomSelectionModeConfig): void {

        // initialize members
        this.#type = type;
        this.#config = config;

        // set status label text
        this.docView.setStatusLabel(config?.statusLabel || null);

        // register custom selection handler to draw the active selection while user selects
        this.docView.selectionEngine.registerCellSelectionProvider({
            get: this.#getActiveSelection.bind(this),
            set: this.#setActiveSelection.bind(this)
        });
    }

    /**
     * Invokes the `applySelected` callback handler passed to the method
     * `enterCustomMode`, if a cell range has been selected in the meantime.
     *
     * @param apply
     *  If set to `true`, the `applySelected` callback passed to the method
     *  `enterCustomMode` will be invoked. Otherwise, custom selection mode
     *  will be canceled.
     */
    leaveCustomMode(apply: boolean): void {

        // custom selection mode must be active
        if (!this.#type) { return; }

        // reset status label text
        this.docView.setStatusLabel(null);

        // the active selection to be processed
        const selection = this.#getActiveSelection();
        this.#setActiveSelection(null);

        // unregister custom selection handler
        this.docView.selectionEngine.unregisterCellSelectionProvider();

        // invoke callback handler
        if (apply && selection) {
            this.#config?.applySelected?.(selection);
        }

        // reset all members for next tracking cycle
        this.#type = this.#config = undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a reference to the active selection of the document.
     */
    #getActiveSelection(): SheetSelection | null {
        return this.docView.propSet.get("activeSelection");
    }

    /**
     * Changes the active selection of the document.
     */
    #setActiveSelection(selection: SheetSelection | null): true {
        this.docView.propSet.set("activeSelection", selection);
        return true; // required for usage in `registerCellSelectionProvider`
    }
}
