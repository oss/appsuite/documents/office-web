/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { SMALL_DEVICE, createDiv, setFocus, createTextArea, setInputSelection } from "@/io.ox/office/tk/dom";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { Button } from "@/io.ox/office/tk/controls";

import { ToolPane } from "@/io.ox/office/baseframework/view/pane/toolpane";
import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';

import "@/io.ox/office/spreadsheet/view/pane/formulapane.less";

// class LeadingToolBar =======================================================

/**
 * The toolbar in front of the text input area with additional controls to
 * insert formulas.
 */
class LeadingToolBar extends ToolBar {

    constructor(docView) {
        super(docView);

        // create the controls for this toolbar
        this.addControl("cell/autoformula",       new Button({ icon: "png:auto-sum", tooltip: gt("Sum"), value: "SUM" }));
        this.addControl("function/insert/dialog", new Button({ icon: "png:functions", tooltip: gt("Functions") }));
    }
}

// class ExpandButton =========================================================

/**
 * A toggle button for expanding and collapsing the formula pane.
 */
class ExpandButton extends Button {

    constructor() {
        super({ toggle: true, highlight: false });
    }

    /*protected override*/ implUpdate(state) {
        super.implUpdate(state);
        this.setIcon(state ? "bi:caret-up-fill" : "bi:caret-down-fill");
        this.setToolTip(state ? gt("Collapse formula bar") : gt("Expand formula bar"));
    }
}

// class TrailingToolBar ======================================================

/**
 * The toolbar at the right border containing the button for expanding and
 * collapsing the height of the formula pane.
 */
class TrailingToolBar extends ToolBar {

    constructor(docView) {
        super(docView);

        // create the controls for this toolbar
        this.addControl("view/formulapane/toggle/height", new ExpandButton());
    }
}

// class FormulaPane ==========================================================

/**
 * The formula pane in spreadsheet documents containing the cell values
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class FormulaPane extends ToolPane {

    /** Current state of the formula pane. */
    #expanded = false;

    // constructor ------------------------------------------------------------

    constructor(docView) {

        // base constructor
        super(docView, {
            position: "top",
            classes: "formula-pane"
        });

        // node for displaying the cell address, or the number of selected columns/rows
        this.$addressBox = $(createDiv("cell-address-box"));
        // the underlay node containing highlighting for formulas
        this.$textUnderlay = $(createDiv("underlay"));
        // the text input control (<textarea> element for multi-line editing)
        this.$textInput = $(createTextArea({ disabled: true }));
        // the container element for the text underlay node and the text input element
        this.$inputContainer = $(createDiv("textarea-container noI18n")).attr("data-focus-role", "textarea").append(this.$textUnderlay, this.$textInput);

        // initialize the areas of the formula pane
        this.$leadingSection.append(this.$addressBox);
        this.$centerSection.append(this.$inputContainer);

        // create the toolbars
        this.addToolBar(new LeadingToolBar(docView), { targetSection: "leading" });
        this.addToolBar(new TrailingToolBar(docView), { targetSection: "trailing" });

        // synchronize scroll positions of input element and underlay element
        this.listenTo(this.$textInput, "scroll input keydown", this._syncScrollPosition);

        // enter cell edit mode when focusing the input element
        this.$textInput.on("focus", () => {
            if (!docView.isTextEditMode("cell")) {
                docView.enterTextEditMode("cell", { formulaBarMode: true });
                if (!SMALL_DEVICE) { this._syncScrollPosition(); }
            }
        });

        // start listening to various events when activating the first sheet
        this.listenTo(docView, "change:activesheet", () => {
            this.listenTo(this.docApp, "docs:editmode", this.#updateInputNode);
            this.listenTo(docView, "textedit:enter textedit:leave change:sheetattrs change:cells", this.#updateInputNode);
            this.listenTo(this.docModel, "change:locale", this.#updateInputNode);
            // when protecting the active sheet, leaving busy mode sometimes removes "disabled" attribute from <textarea>
            this.listenTo(docView, "view:idle", this.#updateInputNodeState);
            this.waitForImportSuccess(this.#updateInputNode);
        }, { once: true });

        // update contents and enabled state of text input element when changing sheets
        this.listenTo(docView, "change:activesheet", this.#updateInputNode);

        // update contents of the formula pane according to selection state
        this.listenTo(docView, "change:selection", this.#handleSheetSelection);
        this.listenToProp(docView.propSet, "trackingRange", this.#handleSheetSelection);

        // initialize visibility of this tool pane
        this.toggle(!this.docApp.isViewerMode() && this.docApp.getUserSettingsValue("showFormulaBar", true));

        // always hide the formula pane when an internal application error occurs
        this.listenTo(this.docApp, "docs:state:error", () => this.hide());
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets the browser focus into the text input element.
     */
    grabFocus() {
        if (this.#updateInputNodeState()) {
            setFocus(this.$textInput);
            setInputSelection(this.$textInput[0], 0);
            this.$textInput[0].scrollTop = 0;
        }
    }

    /**
     * Returns whether the height of this formula pane has been expanded.
     *
     * @returns {boolean}
     *  Whether the height of this formula pane has been expanded.
     */
    isExpanded() {
        return this.#expanded;
    }

    /**
     * Expands or collapses the height of this formula pane.
     *
     * @param {boolean} state
     *  The new state for this formula pane: `true` to expand, `false` to
     *  collapse.
     */
    toggleHeight(state) {
        if (this.#expanded !== state) {
            this.#expanded = state;
            this.$el.toggleClass("expanded", this.#expanded);
            this.refresh({ immediate: true });
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Enables or disables the text input element, according to the current
     * cell selection.
     *
     * @returns {boolean}
     *  Whether the text input element is enabled.
     */
    #updateInputNodeState() {
        const enabled = !this.docView.selectionEngine.hasDrawingSelection() && (this.docView.ensureUnlockedActiveCell({ sync: true }) === null);
        this.$textInput.prop("disabled", !enabled);
        return enabled;
    }

    /**
     * Updates the text contents and enabled state of the text input element,
     * according to the current application state and cell selection.
     */
    #updateInputNode() {

        // enable or disable the text input element
        this.#updateInputNodeState();

        // do not disturb cell edit mode
        if (this.docView.isTextEditMode("cell")) { return; }

        // no text contents for selected drawings (TODO: later, the link formula could be shown)
        if (this.docView.selectionEngine.hasDrawingSelection()) {
            this.$textInput.val("");
        } else {
            const address = this.docView.selectionEngine.getActiveCell();
            this.$textInput.val(this.docView.cellCollection.getEditString(address, { matrixBraces: true }));
        }
    }

    /**
     * Updates the formula pane after the state of the current sheet selection
     * has been changed.
     */
    #handleSheetSelection() {

        // the resulting caption text for the address label
        let address = "\xa0";

        // during tracking, show the size of the tracked range
        const trackingRange = this.docView.propSet.get("trackingRange");
        if (trackingRange) {
            const { addressFactory } = this.docModel;
            const cols = trackingRange.cols(), rows = trackingRange.rows();
            if (addressFactory.isColRange(trackingRange)) {
                //#. info label shown while selecting entire columns in a spreadsheet
                address = gt.ngettext("%1$d column", "%1$d columns", cols, cols);
            } else if (addressFactory.isRowRange(trackingRange)) {
                //#. info label shown while selecting entire rows in a spreadsheet
                address = gt.ngettext("%1$d row", "%1$d rows", rows, rows);
            } else {
                address = _.noI18n(`${rows} \xd7 ${cols}`);
            }
        } else if (!this.docView.selectionEngine.hasDrawingSelection()) {
            address = _.noI18n(this.docView.selectionEngine.getActiveCell().toString());
        }

        // show the address of the active cell in the leading address box
        this.$addressBox.text(address);

        // scroll text input area to top
        this.$inputContainer.scrollTop(0);
        this.$textInput.scrollTop(0);

        this.#updateInputNode();
    }

    /**
     * Sets the scroll position of the underlay node to the current scroll
     * position of the text input element.
     */
    @debounceMethod({ delay: 100, immediate: true })
    /*private*/ _syncScrollPosition() {
        this.$textUnderlay[0].scrollTop = this.$textInput[0].scrollTop;
    }
}
