/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import type { TrackingInputEvent, TrackingRecord, TrackingPrepareFn, TrackingCallbackFn, TrackingCallbackConfig, TrackingConfig, TrackingDispatcherRegisterOptions } from "@/io.ox/office/tk/tracking";
import { resolveTrackingCallbacks, invokeTrackingPrepare, TrackingDispatcher } from "@/io.ox/office/tk/tracking";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { VisibleViewObject } from "@/io.ox/office/spreadsheet/view/visibleviewobject";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * A predicate function that will be invoked before a tracking sequence starts,
 * and decides whether the associated tracking pane handler will be used.
 *
 * @param record
 *  The "start" notification record of a new tracking sequence.
 *
 * @param lastId
 *  The type identifier of the last finished tracking sequence.
 *
 * @returns
 *  Whether to use the associated tracking pane handler.
 */
export type TrackingPaneSelectorFn = (record: TrackingRecord, lastId: string | null) => boolean;

/**
 * Optional parameters for a set of tracking pane callbacks.
 */
export interface TrackingPaneCallbackOptions extends TrackingDispatcherRegisterOptions {

    /**
     * If set to `true`, tracking will be available if the document is in
     * readonly mode. By default, the tracking sequence will not be started in
     * readonly mode, and an active tracking sequence will be canceled
     * immediately if the document loses edit rights. Default value is `false`.
     */
    readOnly?: boolean;

    /**
     * If set to `true`, the current text edit mode will remain active during
     * the tracking sequence. By default, the edit mode will be left before the
     * tracking sequence will be started, and if this fails (e.g. when editing
     * a formula with a syntax error) and the edit mode remains active, the
     * tracking sequence will not be started. Default value is `false`.
     */
    keepTextEdit?: boolean;
}

// class TrackingPane =========================================================

/**
 * Base class for all split panes shown in the application edit area (grid
 * panes, and header panes). Provides generic methods to be used from all sub
 * classes, especially the support of tracking sequences.
 */
export abstract class TrackingPane<EvtMapT> extends VisibleViewObject<EvtMapT> {

    // the DOM element used as tracking event source
    readonly #trackingEl: HTMLElement;
    // the tracking dispatcher
    readonly #dispatcher = this.member(new TrackingDispatcher());

    // promise that waits to leave the current text edit mode (operation generator may run asynchronously)
    #textEditPromise: Opt<JPromise>;
    // type identifier of the current tracking sequence
    #currId = "";
    // type identifier of the last finished tracking sequence
    #lastId = "";

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The spreadsheet view that contains this pane instance.
     *
     * @param trackingEl
     *  The DOM element to be observed by the tracking observer for tracking
     *  sequences.
     */
    constructor(docView: SpreadsheetView, trackingEl: HTMLElement) {
        super(docView);
        this.#trackingEl = trackingEl;
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets the browser focus so that this tracking pane becomes active.
     */
    abstract grabFocus(): void;

    /**
     * Registers a set of callback callbacks for mouse/touch tracking sequences
     * in this tracking pane. The callback handlers will be invoked for all
     * tracking notification records of a matching tracking sequence until the
     * related "end" or "cancel" notification has been received.
     *
     * @param id
     *  A unique type identifier for the tracking callbacks.
     *
     * @param callbacks
     *  The callback function that will handle all tracking notifications for
     *  the target node matched by the specified selector.
     *
     * @param [options]
     *  Optional parameters.
     */
    registerTrackingCallbacks(id: string, callbacks: TrackingCallbackConfig, options?: TrackingPaneCallbackOptions): void {
        const callbackMap = resolveTrackingCallbacks(callbacks);
        this.#dispatcher.register(id, {
            prepare: event => this.#prepareTracking(event, callbackMap.prepare!, options),
            start:   record => this.#startTracking(record, callbackMap.start),
            move:    callbackMap.move,
            scroll:  callbackMap.scroll,
            repeat:  callbackMap.repeat,
            end:     record => this.#finalizeTracking("end", record, callbackMap.end),
            cancel:  record => this.#finalizeTracking("cancel", record, callbackMap.cancel),
            finally: record => this.#finalizeTracking("finally", record, callbackMap.finally)
        }, options);
    }

    // protected methods ------------------------------------------------------

    /**
     * Enables processing tracking sequences for the wrapped tracking node.
     *
     * @param [config]
     *  Additional tracking configuration options on top of the default options
     *  of the tracking observer.
     */
    protected enableTracking(config?: TrackingConfig): void {
        this.#dispatcher.observe(this.#trackingEl, {
            trackModifiers: true,
            scrollDirection: "all",
            scrollMinSpeed: 5,
            scrollMaxSpeed: 500,
            scrollAcceleration: 1.5,
            scrollAccelBand: [-10, 30], // start scrolling before reaching the container borders
            ...config
        });
    }

    /**
     * Disables processing tracking sequences for the wrapped tracking node.
     */
    protected disableTracking(): void {
        this.#dispatcher.unobserve(this.#trackingEl);
    }

    /**
     * Returns the type identifier of the current tracking sequence.
     *
     * @returns
     *  The type identifier of the current tracking sequence; or the empty
     *  string, if no tracking sequence is currently active.
     */
    protected getTrackingId(): string {
        return this.#currId;
    }

    /**
     * Returns the type identifier of the last finished tracking sequence.
     *
     * @returns
     *  The type identifier of the last finished tracking sequence; or the
     *  empty string, if this pane did not process any tracking sequence yet.
     */
    protected getLastTrackingId(): string {
        return this.#lastId;
    }

    // private methods --------------------------------------------------------

    /**
     * Global preparation of a tracking sequence, regardless of target element.
     */
    #prepareTracking(event: TrackingInputEvent, callback: TrackingPrepareFn, options?: TrackingPaneCallbackOptions): boolean {

        // do not start any other tracking sequence while still waiting to leave the preceding tracking sequence
        if (this.#currId) {
            globalLogger.warn("$badge{TrackingPane} prepareTracking: input event ignored while leaving tracking sequence");
            return false;
        }

        // Always activate (focus) the tracked view pane. Done in a timeout as workaround for Firefox,
        // it sends "focusout" events after the old DOM node that has been clicked is removed (the
        // "focusout" event causes canceling the tracking sometimes).
        this.setTimeout(this.grabFocus, 0);

        // exit if in read-only mode
        if (!options?.readOnly && !this.docView.isEditable()) {
            return false;
        }

        // ignore nodes that handle clicks internally (CSS class "skip-tracking")
        if ($(event.target!).closest(".skip-tracking").length > 0) {
            return false;
        }

        // exit if provided "prepare" callback fails
        if (!invokeTrackingPrepare(callback, event)) {
            return false;
        }

        // leave text edit mode and wait for the result
        if (!options?.keepTextEdit && this.docView.isTextEditMode()) {
            // try to leave text edit mode
            this.#textEditPromise = this.docView.leaveTextEditMode();
            // cancel tracking sequence if leaving text edit mode fails (e.g. syntax error in cell formula)
            this.#textEditPromise.fail(() => { if (this.#currId) { TrackingDispatcher.cancelActive(); } });
            // clean-up after leaving text edit mode has succeeded or failed
            this.#textEditPromise.always(() => { this.#textEditPromise = undefined; });
            // nothing more to do, if tracking has been canceled immediately (synchronously)
            if (!this.#currId) { return false; }
        }

        // finally, allow to start the tracking sequence
        return true;
    }

    /**
     * Processes the "start" notification of the current tracking sequence.
     */
    #startTracking(record: TrackingRecord, callback?: TrackingCallbackFn): void {
        this.#currId = record.id;
        callback?.(record);
    }

    /**
     * Processes an "end" or "cancel" notification and finishes the current
     * tracking sequence.
     */
    #finalizeTracking(key: "end" | "cancel" | "finally", record: TrackingRecord, callback?: TrackingCallbackFn): void {

        // create the promise that will be resolved with `true` on tracking success, or `false` when canceling the sequence
        let promise: Promise<boolean>;
        if (record.type === "cancel") {
            promise = Promise.resolve(false);
        } else if (this.#textEditPromise) {
            promise = new Promise(resolve => {
                this.onSettled(this.#textEditPromise!, result => resolve(result.fulfilled));
            });
        } else {
            promise = Promise.resolve(true);
        }

        // invoke the tracking callback after the text edit mode has been left
        this.onFulfilled(promise, success => {
            if ((success && (key !== "cancel")) || (!success && (key !== "end"))) {
                record.type = success ? "end" : "cancel";
                callback?.(record);
            }
        });

        // final cleanup for the tracking sequence
        if (key === "finally") {
            this.onSettled(promise, () => {
                this.#currId = "";
                this.#lastId = record.id;
            });
        }
    }
}
