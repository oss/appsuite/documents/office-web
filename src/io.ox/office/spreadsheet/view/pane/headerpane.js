/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is, math } from "@/io.ox/office/tk/algorithms";
import {
    TOUCH_DEVICE, SCROLLBAR_WIDTH, SCROLLBAR_HEIGHT,
    createFragment, createDiv, createIcon, replaceChildren,
    addDeviceMarkers, parseIntAttribute, convertHmmToLength
} from "@/io.ox/office/tk/dom";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { FOCUSED_CLASS, selectNodes } from "@/io.ox/office/tk/forms";

import { Interval } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { ADDITIONAL_SIZE_RATIO, isColumnSide, isLeadingSide, getScrollAnchorName, getColPaneSide, getRowPaneSide, getNextPanePos, getSelectionMode } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { TrackingPane } from "@/io.ox/office/spreadsheet/view/pane/trackingpane";
import { getColRowLabel } from "@/io.ox/office/spreadsheet/view/labels";
import { HeaderContextMenu } from "@/io.ox/office/spreadsheet/view/popup/headercontextmenu";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";

import "@/io.ox/office/spreadsheet/view/pane/headerpane.less";

// constants ==================================================================

// margin at trailing border of the entire sheet area, in pixels
const SCROLL_AREA_MARGIN = 12;

// size of the resizer nodes, in pixels
const RESIZER_SIZE = 7;

// tooltip labels for resizers
const COL_RESIZER_VISIBLE = gt("Drag to change column width");
const COL_RESIZER_HIDDEN = gt("Drag to show hidden column");
const ROW_RESIZER_VISIBLE = gt("Drag to change row height");
const ROW_RESIZER_HIDDEN = gt("Drag to show hidden row");

// class HeaderPane ===========================================================

/**
 * Represents a single row or column header pane in the spreadsheet view. If
 * the view has been split or frozen at a specific cell position, the view will
 * consist of up to four header panes (left and right column header panes, and
 * top and bottom row header panes).
 */
export class HeaderPane extends TrackingPane {

    // constructor ------------------------------------------------------------

    /**
     * @param {SpreadsheetView} docView
     *  The spreadsheet view that contains this header pane.
     *
     * @param {PaneSide} paneSide
     *  The position of this header pane.
     */
    constructor(docView, paneSide) {

        // whether this header pane contains columns
        const columns = isColumnSide(paneSide);
        // the container node of this header pane (must be focusable for tracking support)
        const el = createDiv({ classes: "header-pane", dataset: { side: paneSide, orientation: columns ? "columns" : "rows" } });

        // base constructor
        super(docView, el);

        // the position of this header pane
        this.paneSide = paneSide;

        // the root element of this header pane
        this.el = el;
        this.$el = $(el);
        // the content node with the header cell and resizer nodes
        this.$contents = $(el.appendChild(createDiv("header-content noI18n")));

        // whether this header pane contains columns
        this._columns = columns;
        // whether this header pane precedes another header pane
        this._leading = isLeadingSide(paneSide);
        // the name of the scroll anchor sheet property
        this._anchorName = getScrollAnchorName(paneSide);
        // size of the trailing scrollbar shown in associated grid panes (trailing panes only)
        this._scrollBarSize = this._leading ? 0 : columns ? SCROLLBAR_WIDTH : SCROLLBAR_HEIGHT;

        // the model and collections of the active sheet
        this._sheetModel = null;
        this._sheetPropSet = null;
        this._collection = null;

        // the size of this header pane usable to show column/row header cells
        this._paneSize = 0;
        // whether this header pane is frozen (not scrollable)
        this._frozen = false;
        // distance between cell A1 and the top-left visible cell of this pane
        this._hiddenSize = 0;

        // the boundary (column/row interval and pixel position) to be displayed in the DOM (`null` indicates hidden state)
        this._boundary = null;
        // the absolute scroll position in the associated grid panes
        this._scrollPos = 0;
        // the total size of the scroll area
        this._scrollSize = 0;

        // marker for touch devices and browser types
        addDeviceMarkers(el);

        // create the context menu
        this.contextMenu = this.member(new HeaderContextMenu(this, columns));

        // initialize sheet-dependent class members according to the active sheet
        this.listenTo(docView, "change:activesheet", this._changeActiveSheetHandler);

        // update scroll area size according to the size of the used area in the sheet
        this.listenTo(docView, "change:usedarea", () => this._updateScrollAreaSize(this._scrollPos));

        // update focus display according to active panes (only, if this grid pane is visible)
        this.listenToSheetPropWhenVisible("activePane", this._updateFocusDisplay);
        this.listenToSheetPropWhenVisible("activePaneSide", this._updateFocusDisplay);

        // repaint the selected header cells, cancel resize tracking (only if this pane is visible)
        this.listenToSheetPropWhenVisible("selection", () => {
            this._renderSelection();
            if (this.getTrackingId() === "resize") { TrackingObserver.cancelActive(); }
        });

        // set new selection, or extend current selection with modifier keys
        this.registerTrackingCallbacks("select", this._createSelectionCallbacks(), { readOnly: true, keepTextEdit: true });
        // resize, hide, or show columns/rows
        this.registerTrackingCallbacks("resize", this._createResizeCallbacks());

        // handle double click on resizer for columns/rows
        this.listenTo(this.$el, "dblclick", this._doubleClickHandler, { delegated: ".resizer" });

        // handle wheel events and scroll this header pane manually
        el.addEventListener("wheel", event => {
            this.scrollRelative(columns ? (event.deltaX * 2) : (event.deltaY / 2));
        }, { passive: true });
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets the browser focus into a visible grid pane associated to this header
     * pane, unless text edit mode is currently active in another grid pane.
     */
    grabFocus() {
        const panePos = getNextPanePos(this._sheetPropSet.get("activePane"), this.paneSide);
        this.docView.getVisibleGridPane(panePos).grabFocus();
    }

    /**
     * Initializes the settings and layout of this header pane.
     *
     * @param {HeaderPaneLayout} headerLayout
     *  The layout settings of the pane side represented by this header pane.
     */
    initializePaneLayout(headerLayout) {

        // copy passed settings
        const paneSize = this._paneSize = headerLayout.size;
        const frozen = this._frozen = headerLayout.frozen;
        const hiddenSize = this._hiddenSize = headerLayout.hiddenSize;

        // debug logging
        renderLogger.log(() => `$badge{HeaderPane} initializePaneLayout: side=${this.paneSide} size=${paneSize} offset=${headerLayout.offset} hiddenSize=${hiddenSize}`);

        // initialize according to visibility
        if (paneSize > 0) {

            // show pane root node and initialize auto-scrolling
            this.el.classList.remove("hidden");
            this.enableTracking({
                scrollDirection: frozen ? "none" : this._columns ? "horizontal" : "vertical",
                scrollBoundary: this.el
            });

            // position and size
            this.$el.css(this._columns ?
                { left: headerLayout.offset, width: paneSize, height: this.docView.getHeaderHeight() } :
                { top: headerLayout.offset, width: this.docView.getHeaderWidth(), height: paneSize }
            );

            // DOCS-3597: data attributes for automated testing
            this.$el.attr("data-frozen", frozen);

            // update CSS classes at root node for focus display
            this._updateFocusDisplay();

        } else {

            // hide pane root node and deinitialize auto-scrolling
            this.el.classList.add("hidden");
            this.disableTracking();
        }
    }

    /**
     * Recalculates the current row/column interval and its position and size
     * in the sheet, and renders all visible header cells and the selection on
     * demand.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.force=false]
     *    If set to `true`, the layer interval will always be recalculated
     *    regardless of the current scroll position. This option will be
     *    ignored, if the option `hide` has been set.
     *
     * @returns {Opt<HeaderBoundarySpec>}
     *  One of the following values:
     *  - An object of type `HeaderBoundary` containing the new layer interval
     *    and pixel position, if the header pane was hidden before and became
     *    visible, or if the layer interval in the visible header pane has been
     *    changed.
     *  - The string "hidden", if the layer interval was visible before, and
     *    has been hidden.
     *  - The value `undefined`, if nothing has changed in this header pane.
     */
    updateLayerInterval(options) {

        // invalidates the layer interval and other members, and returns the descriptor for the caller
        const invalidateIntervalAndReturn = () => {

            // nothing to do, if the interval is already invalid
            if (!this._boundary) { return undefined; }

            // reset members, return "hidden" flag
            this._boundary = null;
            this._scrollPos = this._scrollSize = 0;
            return "hidden";
        };

        // destructure layout settings
        const paneSize = this._paneSize;
        const hiddenSize = this._hiddenSize;

        // invalidate existing layer interval (header pane hidden)
        if (paneSize === 0) {
            return invalidateIntervalAndReturn();
        }

        // the total size of the sheet in pixels
        const totalSize = this._collection.getTotalSize({ pixel: true });
        // the current scroll anchor from the sheet properties
        const scrollAnchor = this._sheetPropSet.get(this._anchorName);
        // the current effective scroll position
        const newScrollPos = this._getValidScrollPos(this._collection.convertScrollAnchorToPixel(scrollAnchor) - hiddenSize);
        // the absolute position of the first visible pixel
        const visibleOffset = newScrollPos + hiddenSize;
        // the absolute offset behind the last visible pixel
        const visibleEndOffset = visibleOffset + paneSize;
        // the additional size before and after the visible area
        const additionalSize = this._frozen ? 0 : Math.ceil(paneSize * ADDITIONAL_SIZE_RATIO);

        // check if the layer interval does not need to be recalculated: layer interval exists, update not forced,
        // and the visible area of the header pane is inside the layer interval (with a small distance)
        if (this._boundary && !options?.force) {
            const { offset, size } = this._boundary.position;
            if (this._frozen || (
                ((offset <= 0) || (visibleOffset - offset >= additionalSize / 3)) &&
                ((totalSize <= offset + size) || (offset + size - visibleEndOffset >= additionalSize / 3))
            )) {
                this._updateScrollPosition(newScrollPos);
                return undefined;
            }
        }

        // exact pixel size for the new interval with additional buffer size
        // (round up to multiples of 100 to reduce expensive resizing while rendering)
        const boundSize = math.ceilp(paneSize + 2 * additionalSize, 100);
        // exact pixel position for the new interval
        const boundOffset = visibleOffset - Math.round((boundSize - paneSize) / 2);
        // the available interval
        const availableInterval = this._getAvailableInterval();
        // the data of the new first column/row
        const firstEntryData = this._collection.getEntryByOffset(boundOffset, { pixel: true });
        // the data of the new last column/row
        const lastEntryData = this._collection.getEntryByOffset(boundOffset + boundSize - 1, { pixel: true });

        // calculate the new visible column/row interval
        const interval = new Interval(
            Math.max(availableInterval.first, (firstEntryData.offset === 0) ? 0 : firstEntryData.index),
            Math.min(availableInterval.last, (lastEntryData.offset + lastEntryData.size >= totalSize) ? this._collection.maxIndex : lastEntryData.index)
        );

        // sanity check
        if (interval.last < interval.first) {
            renderLogger.warn(`$badge{HeaderPane} updateLayerInterval: invalid interval: side=${this.paneSide} interval=${interval.toOpStr(this._columns)}`);
            return invalidateIntervalAndReturn();
        }

        // debug logging
        renderLogger.log(() => `$badge{HeaderPane} updateLayerInterval: side=${this.paneSide} size=${paneSize} interval=${interval.toOpStr(this._columns)} boundOffset=${boundOffset} boundSize=${boundSize}`);

        // store boundary as instance member (before calling more update methods)
        const position = { offset: boundOffset, size: boundSize };
        this._boundary = { position, interval };

        // update scroll size and position according to the new interval
        this._updateScrollAreaSize(newScrollPos);
        this._updateScrollPosition(newScrollPos);

        // render header cells and the selection
        this._renderCells();
        this._renderSelection();

        // return the new header boundary
        return this._boundary;
    }

    /**
     * Returns whether this header pane is currently visible (depending on its
     * position, and the view split settings of the active sheet).
     *
     * @returns {boolean}
     *  Whether this header pane is currently visible.
     */
    isVisible() {
        return (this._paneSize > 0) && !!this._boundary;
    }

    /**
     * Returns whether this header pane is frozen (cannot be scrolled).
     *
     * @returns {boolean}
     *  Whether this header pane is frozen.
     */
    isFrozen() {
        return this._frozen;
    }

    /**
     * Returns the complete column/row interval that can be displayed in this
     * header pane. If the active sheet does not contain a frozen column/row
     * split, this interval will contain all columns/rows of the sheet,
     * otherwise the interval will contain the available columns or rows
     * according to the position of this header pane, and the current split
     * settings.
     *
     * @returns {Interval|null}
     *  The complete column/row interval available in this header pane, in the
     *  zero-based index properties "first" and "last"; or `null`, if this
     *  header pane is currently hidden.
     */
    getAvailableInterval() {
        return this.isVisible() ? this._getAvailableInterval() : null;
    }

    /**
     * Returns the column/row interval currently shown in this header pane.
     *
     * @returns {Opt<Interval>}
     *  The column/row interval currently displayed in this header pane; or
     *  `undefined`, if this header pane is currently hidden.
     */
    getRenderInterval() {
        return this._boundary?.interval.clone();
    }

    /**
     * Returns the offset and size of the entire column/row interval currently
     * covered by this header pane, including the additional columns/rows
     * around the visible area.
     *
     * @returns {Opt<IntervalPosition>}
     *  The offset and size of the displayed column/row interval in pixels; or
     *  `undefined`, if this header pane is currently hidden.
     */
    getRenderPosition() {
        return this._boundary ? { ...this._boundary.position } : undefined;
    }

    /**
     * Returns the offset and size of the visible area currently shown in this
     * header pane.
     *
     * @returns {IntervalPosition}
     *  The offset and size of the visible area in this header pane, in pixels.
     */
    getVisiblePosition() {
        return { offset: this._hiddenSize + this._scrollPos, size: this._paneSize };
    }

    /**
     * Returns the current scroll position of this header pane.
     *
     * @returns {number}
     *  The current scroll position, in pixels.
     */
    getScrollPos() {
        return this._scrollPos;
    }

    /**
     * Returns the size of the scrollable area according to the used area in
     * the sheet, and the current scroll position.
     *
     * @returns {number}
     *  The size of the scrollable area, in pixels.
     */
    getScrollSize() {
        return this._scrollSize;
    }

    /**
     * Returns the zero-based index of the first visible column/row in this
     * header pane. The column/row is considered visible, it at least half of
     * its total size is really visible.
     *
     * @returns {number}
     *  The zero-based index of the first visible column/row.
     */
    getFirstVisibleIndex() {

        // the current scroll anchor
        const scrollAnchor = this._sheetPropSet.get(this._anchorName);
        // the column/row index of the scroll anchor
        const scrollIndex = Math.floor(scrollAnchor);
        // entry visible less than half: jump to the next entry
        const useNext = (scrollAnchor % 1 > 0.5) || !this._collection.isVisibleIndex(scrollIndex);
        // the descriptor of the next available column/row
        const nextIndex = useNext ? this._collection.findNextVisibleIndex(scrollIndex + 1) : null;

        // use next entry if available, otherwise stick to current entry
        return nextIndex ?? scrollIndex;
    }

    /**
     * Changes the scroll position of this header pane, and triggers a
     * "change:scrollpos" event that causes all associated grid panes to update
     * their own scroll position, and optionally a debounced
     * "change:scrollsize" event, if the total size of the scroll area has been
     * changed due to the new scroll position.
     *
     * @param {number} pos
     *  The new scroll position in the sheet, in pixels (the value `0`
     *  corresponds to the first available position; in frozen panes this value
     *  does NOT include the leading hidden part of the sheet).
     */
    scrollTo(pos) {

        // nothing to do in frozen header panes
        if (this._frozen) { return; }

        // the new scroll position (do nothing if scroll position does not change)
        const newScrollPos = this._getValidScrollPos(pos);
        if (this._scrollPos === newScrollPos) { return; }

        // recalculate scroll area size (will be done immediately, if new scroll position is outside)
        this._updateScrollAreaSize(newScrollPos);

        // immediately update internal scroll position, this prevents unnecessary updates
        this._scrollPos = newScrollPos;

        // update scroll anchor view properties, this will trigger change
        // handler from the view that repositions the content node
        const scrollAnchor = this._collection.getScrollAnchorByOffset(newScrollPos + this._hiddenSize, { pixel: true });
        this._sheetPropSet.set(this._anchorName, scrollAnchor);
    }

    /**
     * Changes the scroll position of this header pane relative to the current
     * scroll position.
     *
     * @param {number} diff
     *  The difference for the scroll position, in pixels.
     */
    scrollRelative(diff) {
        this.scrollTo(this._scrollPos + diff);
    }

    /**
     * Scrolls this header pane to make the specified area visible.
     *
     * @param {number} offset
     *  The start offset of the area to be made visible, in pixels.
     *
     * @param {number} size
     *  The size of the area to be made visible, in pixels.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.forceLeading=false]
     *    If set to `true`, the passed offset will be scrolled to the leading
     *    border of the header pane, also if the passed area is already
     *    visible.
     */
    scrollToPosition(offset, size, options) {

        // move the area offset to the leading border if specified
        if (options?.forceLeading) {
            return this.scrollTo(offset - this._hiddenSize);
        }

        // the end offset of the area (with little extra space to make selection border visible)
        const endOffset = offset + size + 2;
        // the absolute offset and size of the visible area
        const visibleOffset = this.getVisiblePosition().offset;
        // the inner pane side (without scroll bars in grid panes)
        const innerSize = this._paneSize - this._scrollBarSize;
        // whether the leading border is outside the visible area
        const leadingOutside = offset < visibleOffset;
        // whether the trailing border is outside the visible area
        const trailingOutside = endOffset > visibleOffset + innerSize;
        // new scrolling position
        let newScrollPos = this._scrollPos;

        if (leadingOutside && !trailingOutside) {
            newScrollPos = Math.max(offset, endOffset - innerSize) - this._hiddenSize;
        } else if (!leadingOutside && trailingOutside) {
            newScrollPos = Math.min(offset, endOffset - innerSize) - this._hiddenSize;
        }

        // update scroll position
        this.scrollTo(newScrollPos);
    }

    /**
     * Scrolls this header pane to make the specified column/row visible.
     *
     * @param {number} index
     *  The zero-based column/row index to be made visible.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.forceLeading=false]
     *    If set to `true`, the passed offset will be scrolled to the leading
     *    border of the header pane, also if the passed area is already
     *   visible.
     */
    scrollToEntry(index, options) {

        // the position and size of the target column/row
        const entryData = this._collection.getEntry(index);

        // update scroll position
        this.scrollToPosition(entryData.offset, entryData.size, options);
    }

    // private methods --------------------------------------------------------

    /**
     * Initializes all sheet-dependent class members according to the current
     * active sheet.
     */
    /*private*/ _changeActiveSheetHandler() {
        this._sheetModel = this.docView.sheetModel;
        this._sheetPropSet = this.docView.sheetPropSet;
        this._collection = this._columns ? this.docView.colCollection : this.docView.rowCollection;
    }

    /**
     * Refreshes the appearance of this header pane according to the position
     * of the grid pane that is currently active.
     */
    /*private*/ _updateFocusDisplay() {

        // which header pane is currently focused (while selection tracking is active)
        const focusPaneSide = this._sheetPropSet.get("activePaneSide");
        // which grid pane is currently focused
        const focusPanePos = this._sheetPropSet.get("activePane");
        // whether this grid pane appears focused
        let focused = false;

        if (this._sheetModel.hasFrozenSplit()) {
            // always highlight all header panes in frozen split mode
            focused = true;
        } else if (is.string(focusPaneSide)) {
            // a header pane is active (selection tracking): focus this
            // header pane if it is active or in the opposite direction
            focused = (focusPaneSide === this.paneSide) || (isColumnSide(focusPaneSide) !== this._columns);
        } else {
            // a single grid pane is focused
            focused = (getColPaneSide(focusPanePos) === this.paneSide) || (getRowPaneSide(focusPanePos) === this.paneSide);
        }

        this.$el.toggleClass(FOCUSED_CLASS, focused);
    }

    /**
     * Refreshes the position of the touch resizer handle node.
     *
     * @param {number} [size]
     *  A custom size of the column/row the resizer node is attached to. If
     *  omitted, uses the current size of the column/row.
     */
    /*private*/ _updateTouchResizerPosition(size) {

        const resizerNode = this.$contents.children(".touchresizer");
        const index = parseIntAttribute(resizerNode[0], "data-index");
        const entry = this._collection.getEntry(index);
        const offset = entry.offset + (is.number(size) ? size : entry.size) - this._boundary.position.offset;

        resizerNode.css(this._columns ? "left" : "top", offset);
    }

    /**
     * Redraws the selection highlighting in the header cells of this header
     * pane.
     */
    /*private*/ _renderSelection() {

        // the selected ranges
        const ranges = this.docView.selectionEngine.getSelectedRanges();
        // the merged ranges of selected columns/rows
        const selectionIntervals = ranges.intervals(this._columns).merge();

        // find the first interval, and iterate all cell nodes
        for (const cellNode of this.$contents.children(".cell").get()) {
            const index = parseInt(cellNode.dataset.index, 10);
            const selected = selectionIntervals.containsIndex(index);
            selectNodes(cellNode, selected);
        }

        // on touch devices, update the resize handle for entire columns/rows
        if (TOUCH_DEVICE) {
            if ((ranges.length === 1) && this.docModel.addressFactory.isFullRange(ranges.first(), this._columns)) {
                this.$contents.children(".touchresizer").show().attr("data-index", ranges.first().getEnd(this._columns));
                this._updateTouchResizerPosition();
            } else {
                this.$contents.children(".touchresizer").hide();
            }
        }
    }

    /**
     * Renders all header cells in the current column/row interval.
     */
    /*private*/ _renderCells() {

        // the entry descriptors needed for rendering
        const entries = [];
        // name of specific CSS attribute
        const OFFSET_NAME = this._columns ? "left" : "top";
        const SIZE_NAME = this._columns ? "width" : "height";
        // tooltips for resizers
        const VISIBLE_TOOLTIP = this._columns ? COL_RESIZER_VISIBLE : ROW_RESIZER_VISIBLE;
        const HIDDEN_TOOLTIP = this._columns ? COL_RESIZER_HIDDEN : ROW_RESIZER_HIDDEN;

        // destructure boundary properties
        const { interval, position } = this._boundary;

        // returns the CSS offset and (optional) size properties as dictionary
        const createPositionStyle = (offset, size) => {
            const style = { [OFFSET_NAME]: offset };
            if (size) { style[SIZE_NAME] = size; }
            return style;
        };

        // creates a new entry in the "entries" array according to the passed column/row descriptor
        const createEntry = entryDesc => {

            // the descriptor for the current entry
            const entry = { index: entryDesc.index, size: entryDesc.size };

            // start offset of the cell node and resizer node
            entry.offset = entryDesc.offset - position.offset;
            entry.resizerOffset = entry.offset + entryDesc.size;

            // adjust resizer position according to hidden state
            if (entry.size > 0) {
                entry.resizerOffset -= Math.ceil(RESIZER_SIZE / 2);
            } else if (entries.length > 0) {
                _.last(entries).resizerOffset -= Math.ceil(RESIZER_SIZE / 2);
            }

            // store new descriptor in result array
            entries.push(entry);
        };

        // create a descriptor array first (needed to manipulate preceding entries before rendering)
        for (const entryDesc of this._collection.indexEntries(interval, { visible: true })) {

            // if the current column/row follows a hidden column/row, create an extra data element for that
            if ((entryDesc.index > 0) && (entryDesc.index === entryDesc.uniqueInterval.first)) {
                const prevEntryDesc = this._collection.getEntry(entryDesc.index - 1);
                if (prevEntryDesc.size === 0) {
                    createEntry(prevEntryDesc);
                }
            }

            // create the data object for the current column/row
            createEntry(entryDesc);
        }

        // add another entry for the trailing hidden columns/rows
        if ((entries.length === 0) || (_.last(entries).index < interval.last)) {
            createEntry(this._collection.getEntry(interval.last));
        }

        // set font size for all cell nodes at content node
        this.$contents.css("font-size", this.docView.getHeaderFontSize() + "pt");

        // generate HTML for all header cells
        const fragment = createFragment();
        for (const entry of entries) {

            // whether the current entry is hidden and precedes a visible entry
            const collapsed = entry.size === 0;
            // entry index as element attribute
            const dataset = { index: entry.index };

            // generate mark-up for the header cell node
            if (collapsed) {
                const style = createPositionStyle(entry.offset);
                fragment.appendChild(createDiv({ classes: "marker", style, dataset }));
            } else {
                const style = createPositionStyle(entry.offset - 1, entry.size + 1);
                const label = getColRowLabel(entry.index, this._columns, { a11y: true });
                fragment.appendChild(createDiv({ classes: "cell", style, dataset, label }));
            }

            // column/row resizer node
            if (!TOUCH_DEVICE) {
                const classes = { resizer: true, collapsed };
                const style = createPositionStyle(entry.resizerOffset, RESIZER_SIZE);
                const tooltip = collapsed ? HIDDEN_TOOLTIP : VISIBLE_TOOLTIP;
                fragment.appendChild(createDiv({ classes, style, dataset, tooltip }));
            }
        }

        // global resizer node for touch devices
        if (TOUCH_DEVICE) {
            const resizerNode = fragment.appendChild(createDiv("touchresizer"));
            resizerNode.appendChild(createIcon(this._columns ? "bi:arrow-left-right" : "bi:arrow-down-up"));
        }

        // insert entire HTML fragment into the cell container node
        replaceChildren(this.$contents[0], fragment);
    }

    /**
     * Returns the maximum possible size of the scrolling area, according to
     * the size of the entire sheet, ignoring the current used area.
     */
    /*private*/ _getMaxScrollSize() {
        // add fixed margin after the sheet, add scroll bar size in trailing header panes
        return this._collection.getTotalSize({ pixel: true }) - this._hiddenSize + SCROLL_AREA_MARGIN + this._scrollBarSize;
    }

    /**
     * Returns the scrolling position reduced to the valid scrolling range,
     * according to the size of the entire sheet, ignoring the current used
     * area.
     */
    /*private*/ _getValidScrollPos(pos) {
        return Math.max(0, Math.min(pos, this._getMaxScrollSize() - this._paneSize));
    }

    /**
     * Calculates the new scroll area size, and triggers a "change:scrollsize"
     * event.
     */
    /*private*/ _calcScrollAreaSize(newScrollPos) {

        // the used area of the sheet (zero for empty sheets)
        const usedRange = this._sheetModel.getUsedRange();
        // number of used columns/rows in the sheet
        const usedCount = usedRange ? (usedRange.a2.get(this._columns) + 1) : 0;
        // the total size of the used area in the sheet, in pixels
        const usedSize = Math.max(0, this._collection.getEntry(usedCount).offset - this._hiddenSize);
        // the absolute size of the scrollable area
        let newScrollSize = newScrollPos + this._paneSize;

        // scrollable area includes the used area, and the passed scroll position,
        // expanded by the own client size to be able to scroll forward
        newScrollSize = Math.max(usedSize, newScrollSize) + this._paneSize;

        // restrict to the maximum scrolling size of the sheet
        newScrollSize = Math.min(newScrollSize, this._getMaxScrollSize());

        // trigger event, if the new scroll area size has changed
        if (this._scrollSize !== newScrollSize) {
            this._scrollSize = newScrollSize;
            this.trigger("change:scrollsize", newScrollSize);
        }
    }

    /**
     * Updates the size of the scrollable area according to the used area in
     * the sheet, and the current scroll position. Tries to reserve additional
     * space for the full size of this header pane at the trailing border, to
     * be able to scroll page-by-page into the unused area of the sheet.
     */
    /*private*/ _updateScrollAreaSize(newScrollPos) {

        // update scroll size immediately, if the new scroll position would be outside
        if (newScrollPos >= this._scrollSize - this._paneSize) {
            this._calcScrollAreaSize(newScrollPos);
        }

        // debounced update of scroll size according to the current scroll position (with trailing margin)
        this._updateScrollAreaSizeDebounced();
    }

    /**
     * Debounced version of method `calcScrollAreaSize` using the current
     * scroll position.
     */
    @debounceMethod({ delay: 500 })
    /*private*/ _updateScrollAreaSizeDebounced() {
        this._calcScrollAreaSize(this._scrollPos);
    }

    /**
     * Updates the relative scroll position and size of the content node, and
     * notifies listeners about the changed scroll position.
     */
    /*private*/ _updateScrollPosition(newScrollPos) {

        // restrict scroll position to the maximum possible scroll position
        // (prevent that header pane scrolls more than the grid panes can scroll)
        this._scrollPos = this._getValidScrollPos(newScrollPos);

        // pixel position and size of the rendering interval
        const { offset, size } = this._boundary.position;
        // the relative offset of the content node
        const contentOffset = offset - (this._scrollPos + this._hiddenSize);
        // maximum size (restrict to sheet size)
        const contentSize = Math.min(size, this._collection.getTotalSize({ pixel: true }) - offset);

        // set the position and size of the content node
        if (this._columns) {
            this.$contents.css({ left: contentOffset, width: contentSize });
        } else {
            this.$contents.css({ top: contentOffset, height: contentSize });
        }

        // notify listeners (do not check whether the scroll position really has
        // changed, depending grid panes may have a wrong scroll position initially
        // due to wrong size of the scroll area)
        this.trigger("change:scrollpos", this._scrollPos);
    }

    /**
     * Returns the complete column/row interval that can be displayed in this
     * header pane. If the active sheet does not contain a frozen column/row
     * split, this interval will contain all columns/rows of the sheet,
     * otherwise the interval will contain the available columns or rows
     * according to the position of this header pane, and the current split
     * settings.
     *
     * @returns {Interval}
     *  The complete column/row interval available in this header pane.
     */
    /*private*/ _getAvailableInterval() {

        // the frozen column/row interval
        let frozenInterval = null;

        // get frozen interval from sheet model
        if (this._sheetModel.hasFrozenSplit()) {
            frozenInterval = this._sheetModel.getSplitInterval(this._columns);
        }

        // return entire column/row interval of the sheet in unfrozen views
        if (!frozenInterval) {
            return this.docModel.addressFactory.getFullInterval(this._columns);
        }

        // build the available interval according to the own position
        return this._leading ? frozenInterval : this.docModel.addressFactory.getEndInterval(frozenInterval.last + 1, this._columns);
    }

    /**
     * Returns the layout data of the column/row matching the "data-index"
     * attribute of the target DOM node in the passed tracking notification.
     *
     * @param {TrackingRecord} record
     *  The tracking notification record.
     *
     * @returns {ColRowDescriptor}
     *  The layout data of the matching column/row, in the properties "index",
     *  "offset", and "size".
     */
    /*private*/ _getTrackingStartEntryData(record) {
        const $index = $(record.target).closest("[data-index]");
        return this._collection.getEntry(parseIntAttribute($index[0], "data-index", -1));
    }

    /**
     * Returns the current absolute sheet position in the header pane for the
     * passed tracking notification record.
     *
     * @param {TrackingRecord} record
     *  The tracking notification record.
     *
     * @returns {number}
     *  The absolute sheet position, in pixels.
     */
    /*private*/ _getTrackingOffset(record) {
        const offset = this.$contents.offset();
        const pos = this._columns ? (record.point.x - offset.left) : (record.point.y - offset.top);
        return pos + this._boundary.position.offset;
    }

    /**
     * Creates the set of callback functions to handle tracking notifications
     * while cell selection is active, and triggers the appropriate selection
     * events to the own listeners.
     */
    /*private*/ _createSelectionCallbacks() {

        // index of tracked header cell (selection tracking)
        let trackedIndex = 0;

        const prepareTracking = event => {
            return $(event.target).closest(".cell").length > 0;
        };

        const startTracking = record => {
            trackedIndex = this._getTrackingStartEntryData(record).index;
            this.scrollToEntry(trackedIndex);
            this._sheetPropSet.set("activePaneSide", this.paneSide);
            this.trigger("select:start", trackedIndex, getSelectionMode(record.modifiers));
        };

        const moveTracking = record => {
            const entry = this._collection.getEntryByOffset(this._getTrackingOffset(record), { outerHidden: true, pixel: true });
            if (trackedIndex !== entry.index) {
                trackedIndex = entry.index;
                this.trigger("select:move", entry.index);
            }
        };

        const scrollTracking = record => {
            this.scrollRelative(this._columns ? record.scroll.x : record.scroll.y);
            moveTracking(record);
        };

        const finalizeTracking = record => {
            this.scrollToEntry(trackedIndex);
            this._sheetPropSet.set("activePaneSide", null);
            this.trigger("select:end", trackedIndex, record.type === "end");

            // fire the contextmenu event
            if (TOUCH_DEVICE && (record.type === "end") && (record.duration > 750)) {
                this.$contents.find(`.cell[data-index="${trackedIndex}"]`).trigger("contextmenu");
            }
        };

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking,
            finally: finalizeTracking
        };
    }

    /**
     * Creates the set of callback functions to handle tracking notifications
     * while resizing a column/row is active.
     */
    /*private*/ _createResizeCallbacks() {

        // additional data needed in resize tracking sequences
        let resizeData = null;

        const prepareTracking = event => {
            return $(event.target).closest(".resizer,.touchresizer").length > 0;
        };

        const startTracking = record => {

            // the layout data of the resized column/row
            const entryData = this._getTrackingStartEntryData(record);
            // maximum size of columns/rows
            const maxSize = convertHmmToLength(this.docModel.fontMetrics.getMaxColRowSizeHmm(this._columns), "px", 1);

            // calculate difference between exact tracking start position and end of column/row
            // (to prevent unwanted resize actions without moving the tracking point)
            const position = this.getVisiblePosition();
            const hiddenSize = position.offset - this.getScrollPos();
            // difference between exact start position and trailing position of column/row
            const correction = this._getTrackingOffset(record) - (entryData.offset + entryData.size);

            // the current size of the column/row while tracking, in pixels
            const currentSize = entryData.size;
            this.trigger("resize:start", entryData.offset, currentSize);

            // minimum/maximum scroll position allowed while resizing
            const minScrollPos = Math.max(0, entryData.offset - 20 - hiddenSize);
            const maxScrollPos = Math.max(minScrollPos, entryData.offset + maxSize + 20 - position.size - hiddenSize);

            // store all data needed in the following tracking events
            resizeData = { entryData, correction, maxSize, currentSize, minScrollPos, maxScrollPos };
        };

        const moveTracking = record => {
            const { entryData, correction, maxSize } = resizeData;
            const currentSize = math.clamp(this._getTrackingOffset(record) - correction - entryData.offset, 0, maxSize);
            resizeData.currentSize = currentSize;
            if (TOUCH_DEVICE) { this._updateTouchResizerPosition(currentSize); }
            this.trigger("resize:move", entryData.offset, currentSize);
        };

        const scrollTracking = record => {
            const { minScrollPos, maxScrollPos } = resizeData;
            const newScrollPos = this._scrollPos + (this._columns ? record.scroll.x : record.scroll.y);
            if ((newScrollPos < this._scrollPos) && (this._scrollPos > minScrollPos)) {
                this.scrollTo(Math.max(newScrollPos, minScrollPos));
            } else if ((newScrollPos > this._scrollPos) && (this._scrollPos < maxScrollPos)) {
                this.scrollTo(Math.min(newScrollPos, maxScrollPos));
            }
            moveTracking(record);
        };

        const finalizeTracking = record => {
            const { entryData, currentSize } = resizeData;
            if (TOUCH_DEVICE) { this._updateTouchResizerPosition(); }
            this.trigger("resize:end");
            if ((record.type === "end") && (currentSize !== entryData.size)) {
                // pass tracked column/row as target index, will cause to modify all columns/rows selected completely
                const newSize = this.docView.convertPixelToHmm(currentSize);
                this.docView[this._columns ? "setColumnWidth" : "setRowHeight"](newSize, { custom: true, target: entryData.index });
            }
        };

        return {
            prepare: prepareTracking,
            start: startTracking,
            move: moveTracking,
            scroll: scrollTracking,
            finally: finalizeTracking
        };
    }

    /**
     * Handles "dblclick" events on resizer nodes and resizes the column or row
     * to the optimal size depending on cell content.
     */
    /*private*/ _doubleClickHandler(event) {

        // the column/row index
        const entry = this._collection.getEntry(parseIntAttribute(event.target, "data-index", -1));

        if (this._columns) {
            this.docView.setOptimalColumnWidth(entry.index);
        } else {
            this.docView.setOptimalRowHeight(entry.index);
        }

        // return focus to active grid pane, after the event has been processed
        this.setTimeout(() => this.docView.grabFocus(), 0);
    }
}
