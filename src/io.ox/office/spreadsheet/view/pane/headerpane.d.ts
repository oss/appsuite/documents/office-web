/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ElementWrapper } from "@/io.ox/office/tk/dom";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { PaneSide, SelectionMode } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { TrackingPane } from "@/io.ox/office/spreadsheet/view/pane/trackingpane";

// types ======================================================================

/**
 * Type mapping for the events emitted by `HeaderPane` instances.
 */
export interface HeaderPaneEventMap {

    /**
     * Will be emitted after the size of the scrollable area in this header
     * pane has been changed.
     *
     * @param scrollSize
     *  The new scroll area size.
     */
    "change:scrollsize": [scrollSize: number];

    /**
     * Will be emitted after the absolute scroll position of this header pane
     * has been changed.
     *
     * @param scrollPos
     *  The new scroll position.
     */
    "change:scrollpos": [scrollPos: number];

    /**
     * Will be emitted after selection tracking has been started.
     *
     * @param index
     *  The zero-based index of the start column/row.
     *
     * @param mode
     *  The selection mode according to the keyboard modifier keys.
     */
    "select:start": [index: number, mode: SelectionMode];

    /**
     * Will be emitted after the index of the tracked column/row has changed
     * while selection tracking is active.
     *
     * @param index
     *  The zero-based index of the current column/row.
     */
    "select:move": [index: number];

    /**
     * Will be emitted after selection tracking has been finished (either
     * successfully or not).
     *
     * @param index
     *  The zero-based index of the last column/row.
     *
     * @param success
     *  The value `true` if selecting has finished successfully, `false` if
     *  selecting has been canceled.
     */
    "select:end": [index: number, success: boolean];

    /**
     * Will be emitted after column/row resize tracking has been started.
     *
     * @param offset
     *  The current offset of the column/row.
     *
     * @param size
     *  The initial size of the column/row.
     */
    "resize:start": [offset: number, size: number];

    /**
     * Will be emitted after the new column/row size has changed while resize
     * tracking is active.
     *
     * @param offset
     *  The current offset of the column/row.
     *
     * @param size
     *  The initial size of the column/row.
     */
    "resize:move": [offset: number, size: number];

    /**
     * Will be emitted after column/row resize tracking has been finished
     * (either successfully or not).
     */
    "resize:end": [];
}

// class HeaderPane ===========================================================

export class HeaderPane extends TrackingPane<HeaderPaneEventMap> implements ElementWrapper {

    // interface `ElementWrapper`
    readonly el: HTMLDivElement;
    readonly $el: JQuery<HTMLDivElement>;

    readonly paneSide: PaneSide;

    grabFocus(): void;
    isVisible(): boolean;
    isFrozen(): boolean;
    getAvailableInterval(): Interval | null;
    getFirstVisibleIndex(): number;
    scrollTo(offset: number): void;
    scrollRelative(diff: number): void;
    scrollToEntry(index: number): void;
}
