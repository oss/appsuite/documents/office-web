/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { is, coord } from "@/io.ox/office/tk/algorithms";
import { AUTOTEST } from "@/io.ox/office/tk/config";
import { IOS_SAFARI_DEVICE, MAX_NODE_SIZE, SCROLLBAR_WIDTH, SCROLLBAR_HEIGHT, setElementStyles, createDiv, addDeviceMarkers, setFocus, isEscapeKey, Rectangle } from "@/io.ox/office/tk/dom";
import { findClosest, findFarthest, getClientPositionInPage } from "@/io.ox/office/tk/utils";
import { FOCUSED_CLASS } from "@/io.ox/office/tk/forms";
import { debounceMethod } from "@/io.ox/office/tk/objects";

import { NODE_SELECTOR, getDrawingType, isSelected } from "@/io.ox/office/drawinglayer/view/drawingframe";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { getColPaneSide, getRowPaneSide } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { TrackingPane } from "@/io.ox/office/spreadsheet/view/pane/trackingpane";
import { CellContextMenu } from "@/io.ox/office/spreadsheet/view/popup/cellcontextmenu";
import { HyperlinkContextMenu } from "@/io.ox/office/spreadsheet/view/popup/hyperlinkcontextmenu";
import GridTrackingMixin from "@/io.ox/office/spreadsheet/view/mixin/gridtrackingmixin";
import ClipboardMixin from "@/io.ox/office/spreadsheet/view/mixin/clipboardmixin";

import { renderLogger, getCanvasResolution } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { CellRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/cellrenderer";
import { SelectionRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/selectionrenderer";
import { DrawingRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/drawingrenderer";
import FormRenderer from "@/io.ox/office/spreadsheet/view/render/layer/formrenderer";
import { HighlightRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/highlightrenderer";

import "@/io.ox/office/spreadsheet/view/pane/gridpane.less";

const { min, max, round } = Math;

// class GridPane =============================================================

/**
 * Represents a single scrollable grid pane in the spreadsheet view. If the
 * view has been split or frozen at a specific cell position, the view will
 * consist of up to four grid panes (top-left, top-right, bottom-left, and
 * bottom-right pane).
 *
 * Each grid pane contains a set of renderer instances (cell renderer,
 * selection renderer, drawing renderer, etc.), listens independently to model
 * and view events to update its appearance, and handles all user input
 * (keyboard input, mouse/touch tracking, clipboard). Therefore, the document
 * view does not have to care about how many and which grid panes are currently
 * visible in split mode.
 *
 * @mixes GridTrackingMixin
 * @mixes ClipboardMixin
 *
 * @property {HTMLElement} scrollNode
 *  The scrollable container node of all content layers in this grid pane.
 *
 * @property {HTMLElement} scrollSizeNode
 *  The client area inside `scrollNode` used to set the total scrolling size of
 *  the sheet (independently of the rendered portion of the sheet).
 *
 * @property {HTMLElement} layerRootNode
 *  The root container node of all rendering layers in this grid pane.
 *
 * @property {HTMLElement} staticRootNode
 *  The root container node of statically positioned contents in this grid
 *  pane.
 */
export class GridPane extends TrackingPane {

    /**
     * @param {SpreadsheetView} docView
     *  The spreadsheet view that contains this header pane.
     *
     * @param {PanePos} panePos
     *  The position of this grid pane.
     */
    constructor(docView, panePos) {

        // base constructor
        const layerRootNode = createDiv("grid-layer-root noI18n");
        super(docView, layerRootNode);

        // the root element of this grid pane
        this.el = createDiv({ classes: "grid-pane", dataset: { pos: panePos } });
        this.$el = $(this.el);

        // child element of this grid pane
        this.staticBackNode = this.el.appendChild(createDiv("grid-static-back"));
        this.scrollNode = this.el.appendChild(createDiv("grid-scroll"));
        this.scrollSizeNode = this.scrollNode.appendChild(createDiv("grid-scroll-size"));
        this.layerRootNode = this.scrollSizeNode.appendChild(layerRootNode);
        this.staticRootNode = this.el.appendChild(createDiv("grid-static-root"));

        // the header panes associated to the grid pane
        this.colHeaderPane = docView.getColHeaderPane(panePos);
        this.rowHeaderPane = docView.getRowHeaderPane(panePos);

        // grid pane identifier, and column/row pane side identifiers
        this.panePos = panePos;
        this.colPaneSide = getColPaneSide(panePos);
        this.rowPaneSide = getRowPaneSide(panePos);

        // the ratio between (public) absolute scroll position, and internal scroll bars
        this._scrollLeftRatio = 1;
        this._scrollTopRatio = 1;

        // the ratio between (public) absolute scroll position, and internal scroll bars
        this._domScrollLeft = 0;
        this._domScrollTop = 0;

        // distance between cell A1 and the top-left visible cell of this pane
        this._hiddenWidth = 0;
        this._hiddenHeight = 0;

        // the registered layer renderers, mapped by object UIDs (the set takes ownership)
        this._rendererRegistry = this.member(new Set/*<BaseRenderer>*/());
        // the cell range and absolute position covered by the layer nodes in the sheet area
        this._layerRange = null;
        this._layerRect = null;

        // current sheet zoom factor (changed zoom factors will be passed to renderes for fast updates)
        this._sheetZoom = 0;
        // current canvas resolution (changed resolutions will be passed to renderes for fast updates)
        this._canvasRes = 0;

        // add mix-in classes
        GridTrackingMixin.call(this);
        ClipboardMixin.call(this);

        // marker for touch devices and browser types
        addDeviceMarkers(this.el);

        // create the DOM layer nodes and layer renderers
        this.cellRenderer = this._registerLayerRenderer(new CellRenderer(this));
        this.selectionRenderer = this._registerLayerRenderer(new SelectionRenderer(this));
        this.formRenderer = this._registerLayerRenderer(new FormRenderer(this));
        this.drawingRenderer = this._registerLayerRenderer(new DrawingRenderer(this));
        this.highlightRenderer = this._registerLayerRenderer(new HighlightRenderer(this));

        // create the context menus for cells and drawing objects
        this.member(new CellContextMenu(this));
        this.member(new HyperlinkContextMenu(this));

        // bug 53205: restore scroll position when showing the entire application
        this.listenToWhenVisible(this.docApp.getWindow(), "show", this._updateScrollPosition);

        // prepare this grid pane for entering the text edit mode
        this.listenTo(docView, "textedit:enter", () => {
            // disable global F6 focus traveling into this grid pane while edit mode is active in another grid pane
            this.el.classList.toggle("f6-target", this === docView.getActiveGridPane());
            // update colored highlighting of selection frames etc.
            this._updateFocusDisplay();
        });

        // prepare this grid pane for leaving the text edit mode
        this.listenTo(docView, "textedit:leave", () => {
            // enable global F6 focus traveling into this grid pane
            this.el.classList.add("f6-target");
            // update colored highlighting of selection frames etc.
            this._updateFocusDisplay();
        });

        // update focus display according to active panes (only, if this grid pane is visible)
        this.listenToSheetPropWhenVisible("activePane", this._updateFocusDisplay);
        this.listenToSheetPropWhenVisible("activePaneSide", this._updateFocusDisplay);

        // listen to changed scroll position/size from header panes
        this.listenToWhenVisible(this.colHeaderPane, "change:scrollsize", this._updateScrollAreaSize);
        this.listenToWhenVisible(this.colHeaderPane, "change:scrollpos", this._updateScrollPosition);
        this.listenToWhenVisible(this.rowHeaderPane, "change:scrollsize", this._updateScrollAreaSize);
        this.listenToWhenVisible(this.rowHeaderPane, "change:scrollpos", this._updateScrollPosition);

        // listen to DOM scroll events, and forward the scroll position to the header panes
        // for synchronous scrolling in all visible header panes and grid panes
        this.listenTo(this.scrollNode, "scroll", this._scrollHandler, { passive: true });

        // activate this pane on focus change
        this.$el.on("focusin", () => docView.updateActiveGridPane(panePos));

        // handle key and drop events
        this.listenTo(this.$el, "mousedown", this._mouseDownHandler);
        this.listenTo(this.$el, "keydown", this._keyDownHandler);

        // additional data attributes at cell selection for automated testing
        if (AUTOTEST) {
            this.listenTo(this.selectionRenderer, "render:cellselection", this._updateCellTestAttributes);
            this.listenTo(this.cellRenderer, "render:cells:finished", this._updateCellTestAttributes);
            this.listenToWhenVisible(docView, "change:cells change:selection", this._updateCellTestAttributes);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the specified header pane associated to this grid pane.
     *
     * @param {boolean} columns
     *  Whether to return the column header pane (`true`), or the row header
     *  pane (`false`).
     *
     * @returns {HeaderPane}
     *  The column or row header pane associated to this grid pane.
     */
    getHeaderPane(columns) {
        return columns ? this.colHeaderPane : this.rowHeaderPane;
    }

    /**
     * Creates a new DOM rendering layer node, and inserts it into the layer
     * root node of this grid pane.
     *
     * @param {string} className
     *  Name of a CSS class that will be added to the created layer node.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.before]
     *    If set to the class name of another (already existing) layer node,
     *    the new layer node will be inserted before it (so that it will be
     *    covered by that layer).
     *  - {string} [options.after]
     *    If set to the class name of another (already existing) layer node,
     *    the new layer node will be inserted behind it (so that it will cover
     *    that layer).
     *
     * @returns {HTMLDivElement}
     *  The new DOM rendering layer node.
     */
    createLayerNode(className, options) {
        const layerNode = createDiv(`grid-layer ${className}`);
        const nextNode = options?.before ? this.layerRootNode.querySelector(`:scope>.${options.before}`) : null;
        if (nextNode) {
            nextNode.before(layerNode);
        } else {
            const prevNode = options?.after ? this.layerRootNode.querySelector(`:scope>.${options.after}`) : null;
            if (prevNode) {
                prevNode.after(layerNode);
            } else {
                this.layerRootNode.append(layerNode);
            }
        }
        return layerNode;
    }

    /**
     * Sets the browser focus into this grid pane, unless text edit mode is
     * currently active in another grid pane.
     */
    grabFocus() {
        setFocus(this.docView.getTextEditFocusNode() || this.getClipboardNode());
    }

    /**
     * Initializes the settings and layout of this grid pane.
     *
     * @param {HeaderPaneLayout} colHeaderLayout
     *  The layout settings of the horizontal pane side (left or right).
     *
     * @param {HeaderPaneLayout} rowHeaderLayout
     *  The view settings of the vertical pane side (top or bottom).
     */
    initializePaneLayout(colHeaderLayout, rowHeaderLayout) {

        // initialize according to visibility
        if ((colHeaderLayout.size > 0) && (rowHeaderLayout.size > 0)) {

            // show pane root node and initialize auto-scrolling
            this.el.classList.remove("hidden");

            // enable mouse/touch tracking
            this.enableTracking({
                scrollDirection: colHeaderLayout.frozen ? (rowHeaderLayout.frozen ? "none" : "vertical") : (rowHeaderLayout.frozen ? "horizontal" : "all"),
                scrollBoundary: this.el,
                dblClickDevice: "all",
                dblClickDistance: 20
            });

            // position and size
            setElementStyles(this.el, {
                left: colHeaderLayout.offset,
                top: rowHeaderLayout.offset,
                width: colHeaderLayout.size,
                height: rowHeaderLayout.size
            });

            // initialize the scroll node containing the scroll bars
            setElementStyles(this.scrollNode, {
                overflowX: colHeaderLayout.frozen ? "hidden" : null,
                overflowY: rowHeaderLayout.frozen ? "hidden" : null,
                // ensure to hide scroll bars overlaying the DOM node, e.g. MacOS
                right: (rowHeaderLayout.frozen || colHeaderLayout.showOppositeScroll) ? 0 : -max(20, SCROLLBAR_WIDTH),
                bottom: (colHeaderLayout.frozen || rowHeaderLayout.showOppositeScroll) ? 0 : -max(20, SCROLLBAR_HEIGHT)
            });

            // reduce size of static root node by the visible size of the scroll bars
            setElementStyles(this.staticRootNode, {
                right: rowHeaderLayout.showOppositeScroll ? SCROLLBAR_WIDTH : 0,
                bottom: colHeaderLayout.showOppositeScroll ? SCROLLBAR_HEIGHT : 0
            });

            // additional layout information for frozen splits
            this._hiddenWidth = colHeaderLayout.hiddenSize;
            this._hiddenHeight = rowHeaderLayout.hiddenSize;

            // update scroll area size and CSS classes at root node for focus display
            this._updateScrollAreaSize();
            this._updateFocusDisplay();

        } else {

            // hide pane root node and deinitialize auto-scrolling
            this.el.classList.add("hidden");
            this.disableTracking();
        }
    }

    /**
     * Updates the DOM layer nodes according to the passed layer range.
     *
     * @param {LayerBoundarySpec} layerBoundary
     *  The address of the new cell range covered by this grid pane, and the
     *  exact rectangle to be covered by the DOM layers; or the string "hidden"
     *  to hide the rendering layers.
     */
    setLayerRange(layerBoundary, options) {

        const visible = layerBoundary !== "hidden";

        // hide layer ranges (also on option "hide", e.g. when switching sheets)
        if (!visible || options?.hide) {

            // clear the layer rectangle
            this._layerRange = this._layerRect = null;
            this._sheetZoom = this._canvasRes = 0;

            // invoke all layer renderers
            for (const renderer of this._rendererRegistry) {
                renderer.hideLayerRange();
            }

            // hide the scroll node to prevent any leftovers if all columns/rows are hidden in a visible grid pane
            this.scrollNode.classList.add("hidden");
        }

        // initialize layer ranges
        if (visible) {

            // update visible sheet contents according to the new layer range
            const wasVisible = !!this._layerRange;
            this._layerRange = layerBoundary.range;
            this._layerRect = layerBoundary.rectangle;

            // update the scroll position (exact position of the layer root node in the DOM)
            this.scrollNode.classList.remove("hidden");
            this._updateScrollAreaSize();
            this._updateScrollPosition();

            // detect changed zoom factor
            const newSheetZoom = this.docView.getZoomFactor();
            const zoomScale = this._sheetZoom ? (newSheetZoom / this._sheetZoom) : 1;
            this._sheetZoom = newSheetZoom;

            // detect changed canvas resolution (e.g. native browser zoom)
            const newCanvasRes = getCanvasResolution();
            const canvasScale = this._canvasRes ? (newCanvasRes / this._canvasRes) : 1;
            this._canvasRes = newCanvasRes;

            // invoke all layer renderers
            renderLogger.takeTime(() => `$badge{GridPane} setLayerRange: update rendering layers, pos=${this.panePos}`, () => {
                renderLogger.log(() => `range=${this._layerRange} rectangle=${this._layerRect}`);
                const layerSettings = { zoomScale, canvasScale, wasVisible };
                for (const renderer of this._rendererRegistry) {
                    renderer.setLayerRange(this._layerRange, this._layerRect, layerSettings);
                }
            });
        }
    }

    /**
     * Returns whether a layer range is currently available (and the DOM
     * rendering layers are visible).
     *
     * @returns {boolean}
     *  Whether a layer range is currently available.
     */
    isVisible() {
        return !!this._layerRange;
    }

    /**
     * Returns the address of the cell range currently covered by the rendering
     * layer nodes, including the ranges around the visible area.
     *
     * @returns {Range|null}
     *  The address of the cell range covered by the layer nodes, if the layers
     *  are visible; otherwise `null`.
     */
    getLayerRange() {
        return this._layerRange?.clone() ?? null;
    }

    /**
     * Returns the location of the entire sheet rectangle covered by the
     * rendering layer nodes, including the ranges around the visible area.
     *
     * @returns {Rectangle|null}
     *  The location of the sheet area covered by the layer nodes (in pixels),
     *  if the layers are visible; otherwise `null`.
     */
    getLayerRectangle() {
        return this._layerRect?.clone() ?? null;
    }

    /**
     * Converts the passed absolute sheet rectangle to a rectangle relative to
     * the current rendering layer nodes.
     *
     * @param {RectangleLike} rectangle
     *  An absolute sheet rectangle, in pixels.
     *
     * @returns {Rectangle}
     *  The resulting layer rectangle, relative to the current layer nodes.
     */
    convertToLayerRectangle(rectangle) {
        return new Rectangle(
            rectangle.left - (this._layerRect?.left ?? 0),
            rectangle.top - (this._layerRect?.top ?? 0),
            this._layerRect ? rectangle.width : 0,
            this._layerRect ? rectangle.height : 0
        );
    }

    /**
     * Creates an iterator that visits all cell ranges in the passed range
     * address source that are located inside the current layer range. The
     * iterator result value will contain additional data needed for rendering
     * those ranges in some way.
     *
     * @param {RangeSource} ranges
     *  An array of cell range addresses, or a single cell range address.
     *
     * @param {RenderRangeIteratorOptions} [options]
     *  Optional parameters.
     *
     * @yields {RenderRangeData}
     *  The rendering ranges.
     */
    *renderRanges(ranges, options) {

        // nothing to visit if all columns or rows are hidden
        if (!this.isVisible()) { return; }

        // filter and convert the ranges
        for (const [index, range] of RangeArray.cast(ranges).entries()) {

            // bug 41353, bug 57843: restrict to ranges in the layer rectangle to prevent oversized
            // DOM nodes that would collapse to zero size, e.g. when entire columns or rows are selected
            let rectangle = this.docView.getRangeRectangle(range).intersect(this._layerRect);
            if (!rectangle) { continue; }

            // enlarge rectangle to match the grid lines between the cells
            if (options?.alignToGrid) { rectangle.expandSelf(1, 1, 0, 0); }

            // pass a rectangle that is located relatively to the layer rectangle
            rectangle = this.convertToLayerRectangle(rectangle);

            // create the `RenderRangeData` object
            yield { range, rectangle, index };
        }
    }

    /**
     * Returns the size of the leading hidden area that cannot be shown in this
     * grid pane in a frozen split view.
     *
     * @returns {Size}
     *  The size of the leading hidden area that cannot be shown in this grid
     *  pane, due to a frozen split pane in front of this grid pane, in pixels.
     */
    getHiddenSize() {
        return { width: this._hiddenWidth, height: this._hiddenHeight };
    }

    /**
     * Returns the effective offset and size of the sheet area that is really
     * visible in this grid pane.
     *
     * @returns {Rectangle}
     *  The location of the sheet area that is visible in this grid pane, in
     *  pixels.
     */
    getVisibleRectangle() {
        return new Rectangle(
            this.colHeaderPane.getVisiblePosition().offset,
            this.rowHeaderPane.getVisiblePosition().offset,
            this.scrollNode.clientWidth,
            this.scrollNode.clientHeight
        );
    }

    /**
     * Returns the address of the top-left cell visible in this grid pane. The
     * cell is considered visible, it its column and row are at least half
     * visible.
     *
     * @returns {Address}
     *  The address of the top-left cell in this grid pane.
     */
    getTopLeftAddress() {
        return new Address(
            this.colHeaderPane.getFirstVisibleIndex(),
            this.rowHeaderPane.getFirstVisibleIndex()
        );
    }

    /**
     * Changes the scroll position of this grid pane relative to the current
     * scroll position.
     *
     * @param {number} leftDiff
     *  The difference for the horizontal scroll position, in pixels.
     *
     * @param {number} topDiff
     *  The difference for the vertical scroll position, in pixels.
     */
    scrollRelative(leftDiff, topDiff) {
        this.colHeaderPane.scrollRelative(leftDiff);
        this.rowHeaderPane.scrollRelative(topDiff);
    }

    /**
     * Scrolls this grid pane to make the specified rectangle in the sheet
     * visible.
     *
     * @param {RectangleLike} rectangle
     *  The sheet rectangle to be made visible, in pixels.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.forceTop=false]
     *    If set to `true`, the rectangle will always be scrolled to the upper
     *    border of the grid pane, also if it is already visible in the bottom
     *    area.
     */
    scrollToRectangle(rectangle, options) {
        this.colHeaderPane.scrollToPosition(rectangle.left, rectangle.width);
        this.rowHeaderPane.scrollToPosition(rectangle.top, rectangle.height, { forceLeading: options?.forceTop });
    }

    /**
     * Scrolls this grid pane to make the specified cell visible.
     *
     * @param {Address} address
     *  The cell position to be made visible.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.forceTop=false]
     *    If set to `true`, the cell will always be scrolled into the upper
     *    border of the grid pane, also if it is already visible in the bottom
     *    area.
     */
    scrollToCell(address, options) {
        var rectangle = this.docView.getCellRectangle(address, { expandMerged: true });
        this.scrollToRectangle(rectangle, options);
    }

    // event page coordinates -------------------------------------------------

    /**
     * Returns whether the passed page coordinates are visible inside the root
     * node of this grid pane.
     *
     * @param {Point|MouseEvent|JEvent} point
     *  The page coordinates (in pixels), or a browser event, expected to
     *  contain the numeric properties "pageX" and "pageY".
     *
     * @returns {boolean}
     *  Whether the page coordinates are visible inside the root node of this
     *  grid pane.
     */
    containsPagePoint(point) {

        // invisible grid panes cannot contain a page offset
        if (!this.isVisible()) { return false; }

        // the page position of the root node and scroll node
        const rootRect = Rectangle.from(getClientPositionInPage(this.el));
        const scrollRect = Rectangle.from(getClientPositionInPage(this.scrollNode));
        // the area that is really visible
        const visibleRect = rootRect.intersect(scrollRect);

        // convert event page coordinates to point
        if ((point instanceof window.Event) || (point instanceof $.Event)) {
            point = coord.point(point.pageX, point.pageY);
        }

        // return whether the event pixel is contained
        return visibleRect.containsPixel(point);
    }

    /**
     * Converts page coordinates to the absolute position in the active sheet
     * (in pixels), according to the scroll position of this grid pane, and the
     * current zoom factor of the active sheet. It does not matter whether the
     * page position is visible in the scroll node.
     *
     * @param {Point|MouseEvent|JEvent} point
     *  The page coordinates (in pixels), or a browser event, expected to
     *  contain the numeric properties "pageX" and "pageY".
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.restricted=false]
     *    If set to `true`, this method will NOT return sheet coordinates that
     *    are outside the actual area of the active sheet.
     *
     * @returns {Opt<Point>}
     *  The value `undefined`, if this grid pane is currently hidden, or if the
     *  option "restricted" has been set and the page coordinates are not
     *  covering the sheet area; otherwise the absolute sheet coordinates.
     */
    getSheetPointForPagePoint(point, options) {

        // nothing to return in hidden grid panes
        if (!this._layerRect) { return undefined; }

        // the total size of the sheet
        const sheetRect = this.docView.getSheetRectangle();
        // the absolute position of the layer root node
        const layerOffset = $(this.layerRootNode).offset();

        // convert event page coordinates to point
        if ((point instanceof window.Event) || (point instanceof $.Event)) {
            point = coord.point(point.pageX, point.pageY);
        }

        // calculate horizontal sheet offset in pixels, test for overflow
        const x = point.x - layerOffset.left + this._layerRect.left;
        if (options?.restricted && ((x < 0) || (x >= sheetRect.width))) { return null; }

        // calculate vertival sheet offset in pixels, test for overflow
        const y = point.y - layerOffset.top + this._layerRect.top;
        if (options?.restricted && ((y < 0) || (y >= sheetRect.height))) { return null; }

        return coord.point(x, y);
    }

    /**
     * Returns all data for the cell covered by a pixel specified in page
     * coordinates. It does not matter whether the page position is visible in
     * the scroll node.
     *
     * @param {Point|MouseEvent|JEvent} point
     *  The page coordinates (in pixels), or a browser event, expected to
     *  contain the numeric properties "pageX" and "pageY".
     *
     * @param {object} [options]
     *  Optional parameters. Supports all options that are supported by the
     *  method `ColRowCollection#getEntryByOffset`, and the following options:
     *  - {boolean} [options.overflow=false]
     *    If set to `true`, this method will return sheet coordinates that are
     *    outside the actual area of the active sheet.
     *
     * @returns {object|null}
     *  The value `null`, if this grid pane is currently hidden; otherwise a
     *  result descriptor with the following properties:
     *  - {Address} address
     *    The address of the cell covering the specified page position.
     *  - {Point} point
     *    The absolute sheet position, in pixels.
     *  - {ColRowDescriptor} colDesc
     *    The column descriptor received from the column collection.
     *  - {ColRowDescriptor} rowDesc
     *    The row descriptor received from the row collection.
     */
    getCellDataForPagePoint(point, options) {

        // get absolute sheet coordinates (nothing to return in hidden grid panes)
        point = this.getSheetPointForPagePoint(point, options);
        if (!point) { return null; }

        // get the column and row collection entry
        options = { ...options, pixel: true };
        const colDesc = this.docView.colCollection.getEntryByOffset(point.x, options);
        const rowDesc = this.docView.rowCollection.getEntryByOffset(point.y, options);

        // add the cell address
        const address = new Address(colDesc.index, rowDesc.index);
        return { address, point, colDesc, rowDesc };
    }

    // drawing layer ----------------------------------------------------------

    /**
     * Scrolls this grid pane to make the specified drawing frame visible.
     *
     * @param {Position} position
     *  The document position of the drawing frame.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.forceTop=false]
     *    If set to `true`, the cell will always be scrolled into the upper
     *    border of the grid pane, also if it is already visible in the bottom
     *    area.
     */
    scrollToDrawingFrame(position, options) {

        // the drawing model at the specified document position
        const drawingModel = this.docView.drawingCollection.getModel(position);

        // do not scroll to invisible drawing objects
        if (drawingModel?.isEffectivelyVisible()) {
            const rectangle = drawingModel.getRectangle();
            if (rectangle) { this.scrollToRectangle(rectangle, options); }
        }
    }

    /**
     * Returns the drawing frame to be selected or manipulated by the passed
     * tracking event.
     *
     * @param {Event} event
     *  The event to be evaluated.
     *
     * @returns {Opt<JQuery>}
     *  The drawing frame to be selected or manipulated by the passed tracking
     *  event if existing; otherwise `undefined`.
     */
    getDrawingFrameForEvent(event) {

        // ensure that a drawing frame is targeted
        const closestEl = findClosest(this.layerRootNode, event.target, NODE_SELECTOR);
        if (!closestEl) { return undefined; }

        // ignore the empty area of group objects, unless the group object is selected
        if (!isSelected(closestEl) && (getDrawingType(closestEl) === "group")) {
            return undefined;
        }

        // always track the root parent of grouped drawing objects (TODO: handle activated group objects)
        const farthestEl = findFarthest(this.layerRootNode, closestEl, NODE_SELECTOR);
        return farthestEl ? $(farthestEl) : undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Registers a new layer renderer.
     *
     * @param {RT extends BaseRenderer} renderer
     *  The new renderer instance to be registered. This grid pane takes
     *  ownership!
     *
     * @returns {RT}
     *  The passed renderer instance.
     */
    /*private*/ _registerLayerRenderer(renderer) {

        // create and store the new layer renderer
        this._rendererRegistry.add(renderer);

        // forward renderer events to own listeners
        this.listenToAllEvents(renderer, this.trigger);

        return renderer;
    }

    /**
     * Refreshes the appearance of this grid pane according to the position of
     * the grid pane that is currently active.
     */
    /*private*/ _updateFocusDisplay() {

        // model of the active sheet
        const { sheetModel } = this.docView;
        // which header pane is currently focused (while selection tracking is active)
        const focusPaneSide = sheetModel.propSet.get("activePaneSide");
        // which grid pane is currently focused
        const focusPanePos = sheetModel.propSet.get("activePane");
        // whether this grid pane appears focused
        let focused = false;

        if (this.docView.isTextEditMode("cell")) {
            // never highlight selection in cell edit mode
            focused = false;
        } else if (sheetModel.hasFrozenSplit()) {
            // always highlight all panes in frozen split mode
            focused = true;
        } else if (is.string(focusPaneSide)) {
            // a header pane is active (selection tracking): focus this
            // grid pane if it is related to the active header pane
            focused = (this.colPaneSide === focusPaneSide) || (this.rowPaneSide === focusPaneSide);
        } else {
            // a single grid pane is focused
            focused = focusPanePos === this.panePos;
        }

        this.el.classList.toggle(FOCUSED_CLASS, focused);
    }

    /**
     * Handles changed size of the scroll area received from the column or row
     * header pane.
     */
    /*private*/ _updateScrollAreaSize() {

        // model of the active sheet
        const { sheetModel } = this.docView;

        // the available size according to existence of scroll bars
        const clientWidth = this.scrollNode.clientWidth;
        const clientHeight = this.scrollNode.clientHeight;
        // difference of scroll area size between header pane and this grid pane
        const widthCorrection = this.el.offsetWidth - clientWidth;
        const heightCorrection = this.el.offsetHeight - clientHeight;

        // the new absolute width and height of the scrollable area
        let scrollWidth = 0;
        let scrollHeight = 0;

        // special case for chart sheets: enlarge scroll area size to include the entire chart
        if (sheetModel.isChartsheet) {
            const chartModel = sheetModel.drawingCollection.getModel([0], { type: "chart" });
            const rectangle = chartModel ? chartModel.getRectangle() : { width: 0, height: 0 };
            scrollWidth = max(rectangle.width, this.el.offsetWidth - widthCorrection);
            scrollHeight = max(rectangle.height, this.el.offsetHeight - heightCorrection);
        } else {
            scrollWidth = max(0, this.colHeaderPane.getScrollSize() - widthCorrection);
            scrollHeight = max(0, this.rowHeaderPane.getScrollSize() - heightCorrection);
        }

        // restrict to maximum allowed node size according to current browser
        const domWidth = min(scrollWidth, MAX_NODE_SIZE);
        const domHeight = min(scrollHeight, MAX_NODE_SIZE);
        setElementStyles(this.scrollSizeNode, { width: domWidth, height: domHeight });

        // recalculate ratio between absolute and effective scroll area size
        this._scrollLeftRatio = (domWidth > clientWidth) ? ((scrollWidth - clientWidth) / (domWidth - clientWidth)) : 0;
        this._scrollTopRatio = (domHeight > clientHeight) ? ((scrollHeight - clientHeight) / (domHeight - clientHeight)) : 0;
        renderLogger.trace(() => `scroll area size: sheet-w=${scrollWidth} sheet-h=${scrollHeight} dom-w=${domWidth} dom-h=${domHeight} ratio-w=${this._scrollLeftRatio} ratio-h=${this._scrollTopRatio}`);
    }

    /**
     * Handles changed scroll position received from the column or row header
     * pane.
     */
    /*private*/ _updateScrollPosition() {

        // the scroll position of the header panes (unscaled sheet pixels)
        const sheetScrollLeft = this.colHeaderPane.getScrollPos();
        const sheetScrollTop = this.rowHeaderPane.getScrollPos();

        // the new DOM scroll position (scaled down according to maximum DOM node size)
        this._domScrollLeft = (this._scrollLeftRatio > 0) ? round(sheetScrollLeft / this._scrollLeftRatio) : 0;
        this._domScrollTop = (this._scrollTopRatio > 0) ? round(sheetScrollTop / this._scrollTopRatio) : 0;

        // restrict size of layer root node to size of sheet
        const sheetRect = this.docView.getSheetRectangle();
        const layerRect = this._layerRect;
        const layerRootWidth = min(layerRect.width, sheetRect.width - layerRect.left);
        const layerRootHeight = min(layerRect.height, sheetRect.height - layerRect.top);

        // calculate the effective offsets according to current scroll position
        const offsetLeft = max(-2 * layerRootWidth, layerRect.left - sheetScrollLeft + this._domScrollLeft - this._hiddenWidth);
        const offsetTop = max(-2 * layerRootHeight, layerRect.top - sheetScrollTop + this._domScrollTop - this._hiddenHeight);

        // set position and size of the layer root node (should always be inside the node size limits)
        setElementStyles(this.layerRootNode, { left: offsetLeft, top: offsetTop, width: layerRootWidth, height: layerRootHeight });

        // update the DOM scroll properties of the root node
        this.scrollNode.scrollLeft = this._domScrollLeft;
        this.scrollNode.scrollTop = this._domScrollTop;

        // notify listeners
        this.trigger("change:scrollpos");
    }

    /**
     * Sends the scroll position of the scroll container node to the header
     * panes (which causes to update the scrollpositions of adjacent grid panes
     * too), and stores the scroll position in the private fields.
     */
    @renderLogger.profileMethod("$badge{GridPane} applyScrollPosition")
    /*private*/ _applyScrollPosition() {
        this._domScrollLeft = this.scrollNode.scrollLeft;
        this._domScrollTop = this.scrollNode.scrollTop;
        this.colHeaderPane.scrollTo(round(this._domScrollLeft * this._scrollLeftRatio));
        this.rowHeaderPane.scrollTo(round(this._domScrollTop * this._scrollTopRatio));
    }

    /**
     * Checks that the scroll position stored in the private fields matches the
     * scroll position of the scroll container node, and updates the global
     * scroll position on mismatch.
     */
    @debounceMethod({ delay: 1000 })
    /*private*/ _verifyScrollPosition() {
        if ((this.scrollNode.scrollLeft !== this._domScrollLeft) || (this.scrollNode.scrollTop !== this._domScrollTop)) {
            renderLogger.warn(() => `$badge{GridPane} pos=${this.panePos} verifyScrollPosition: detected scroll position mismatch`);
            this._applyScrollPosition();
            this._verifyScrollPosition();
        }
    }

    /**
     * Handles "scroll" events from the scroll container node.
     */
    /*private*/ _scrollHandler() {
        // bug 61438: ignore synthetic DOM "scroll" event caused by updating the scroll position in the method `_updateScrollPosition`
        if ((this.scrollNode.scrollLeft !== this._domScrollLeft) || (this.scrollNode.scrollTop !== this._domScrollTop)) {
            renderLogger.trace(() => `$badge{GridPane} $event{scroll} pos=${this.panePos}`);
            this._applyScrollPosition();
        } else {
            renderLogger.trace(() => `$badge{GridPane} $event{scroll} pos=${this.panePos}: event ignored`);
        }
        this._verifyScrollPosition();
    }

    /**
     * Handles "mousedown" events from the root node.
     */
    /*private*/ _mouseDownHandler(event) {

        // the target node of the event, as jQuery object
        var targetNode = $(event.target);

        // bug 41094: user wants to enter text, keyboard shows then hides immediately
        if (IOS_SAFARI_DEVICE && targetNode.is(".cell-layer")) {
            event.preventDefault();
        }

        // special handling for right mouse button
        if (event.button === 2) {

            // select a drawing object if available, otherwise select the clicked cell
            const drawingFrame = this.getDrawingFrameForEvent(event);
            if (drawingFrame) {
                if (!isSelected(drawingFrame)) {
                    const drawingPos = this.drawingRenderer.getDrawingModelForFrame(drawingFrame).getPosition();
                    this.docView.selectionEngine.selectDrawing(drawingPos);
                    this.docView.scrollToDrawingFrame(drawingPos);
                }
            } else {
                // DOCS-1660: may return `null` (e.g. all columns are hidden)
                const cellData = this.getCellDataForPagePoint(event);
                if (cellData && !this.docView.selectionEngine.getSelectedRanges().containsAddress(cellData.address)) {
                    this.docView.selectionEngine.selectCell(cellData.address);
                    this.docView.scrollToCell(cellData.address);
                }
            }
        }
    }

    /**
     * Handles "keydown" events from the root node.
     */
    /*private*/ _keyDownHandler(event) {

        // do not evaluate any key event during drawing text edit mode
        if (this.docView.isTextEditMode("drawing")) {
            return;
        }

        // ESCAPE key (only one action per key event)
        if (isEscapeKey(event)) {

            // close an embedded drop-down menu
            if (this.formRenderer.handleKeyDownEvent(event)) {
                return false;
            }

            const { selectionEngine } = this.docView;

            // cancel various selection modes (before deselecting drawings)
            if (selectionEngine.cancelFrameSelectionMode() || selectionEngine.cancelCustomSelectionMode()) {
                return false;
            }

            // drawing selection: back to cell selection
            if (selectionEngine.hasDrawingSelection()) {
                this.docView.selectionEngine.removeDrawingSelection();
                return false;
            }

            // otherwise: let ESCAPE key bubble up
            return;
        }

        // let the form control layer process the key event (embedded drop-down menus)
        if (this.formRenderer.handleKeyDownEvent(event)) {
            return false;
        }
    }

    @debounceMethod({ delay: 25 })
    /*private*/ _updateCellTestAttributes() {

        // DOM element of the active cell in the selection
        const activeCellNode = $(this.layerRootNode).find(".selection-layer .active-cell")[0];
        if (!activeCellNode) { return; }

        // annotate DONM element of active cell with rendering data
        const address = this.docView.selectionEngine.getActiveCell();
        this.docView.renderCache.annotateElement(activeCellNode, address);
    }
}
