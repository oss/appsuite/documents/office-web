/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Point, Size } from "@/io.ox/office/tk/algorithms";
import { RectangleLike, Rectangle, ElementWrapper } from "@/io.ox/office/tk/dom";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeSource } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { SelectionMode } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { TrackingPane } from "@/io.ox/office/spreadsheet/view/pane/trackingpane";
import { HeaderPane } from "@/io.ox/office/spreadsheet/view/pane/headerpane";
import { CellRendererEventMap, CellRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/cellrenderer";
import { DrawingRendererEventMap, DrawingRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/drawingrenderer";
import { SelectionRendererEventMap } from "@/io.ox/office/spreadsheet/view/render/layer/selectionrenderer";
import { HighlightRendererEventMap } from "@/io.ox/office/spreadsheet/view/render/layer/highlightrenderer";

// types ======================================================================

export interface LayerNodeOptions {
    before?: string;
    after?: string;
}

export interface RenderRangeIteratorOptions {

    /**
     * If set to `true`, the rectangles passed to the iterator will be expanded
     * by one pixel to the left and to the top, so that all its outer pixels
     * are located on the grid lines surrounding the range. Default value is
     * `false`.
     */
    alignToGrid?: boolean;
}

/**
 * Information for a cell range to be rendered.
 */
export interface RenderRangeData {

    /**
     * The address of the cell range to be rendered.
     */
    range: Range;

    /**
     * The location of the current range, relative to the layer root node of
     * the grid pane.
     */
    rectangle: Rectangle;

    /**
     * The array index of the cell range.
     */
    index: number;
}

/**
 * Type mapping for the events emitted by `GridPane` instances.
 */
export interface GridPaneEventMap extends CellRendererEventMap, SelectionRendererEventMap, HighlightRendererEventMap, DrawingRendererEventMap {

    /**
     * Will be emitted after the scroll position of the grid pane has been
     * changed in any direction.
     */
    "change:scrollpos": [];

    /**
     * Will be emitted after selection tracking has been started.
     *
     * @param address
     *  The address of the start cell.
     *
     * @param mode
     *  The selection mode according to the keyboard modifier keys.
     */
    "select:start": [address: Address, mode: SelectionMode];

    /**
     * Will be emitted after the address of the tracked cell has changed while
     * selection tracking is active.
     *
     * @param address
     *  The address of the current cell.
     */
    "select:move": [Address: Address];

    /**
     * Will be emitted after selection tracking has been finished (either
     * successfully or not)
     *
     * @param address
     *  The address of the last cell.
     *
     * @param success
     *  The value `true` if selecting has finished successfully, `false` if
     *  selecting has been canceled.
     */
    "select:end": [address: Address, success: boolean];

    /**
     * Will be emitted while selection tracking on touch devices is active, and
     * a selected range has been modified by tracking the resizer handles.
     *
     * @param rangeIndex
     *  The array index of the modified range in the current selection.
     *
     * @param range
     *  The new address of the specified cell range.
     */
    "select:range": [rangeIndex: number, range: Range];

    /**
     * Will be emitted after the active address of the active range has been
     * modified.
     *
     * @param rangeIndex
     *  The array index of the modified range in the current selection.
     *
     * @param activeCell
     *  The new address of the active cell.
     */
    "select:active": [rangeIndex: number, activeCell: Address];

    /**
     * Will be emitted after a double click has been received for the current
     * active cell.
     */
    "select:dblclick": [];
}

// class GridPane =============================================================

export class GridPane extends TrackingPane<GridPaneEventMap> implements ElementWrapper {

    // interface `ElementWrapper`
    readonly el: HTMLDivElement;
    readonly $el: JQuery<HTMLDivElement>;

    readonly colHeaderPane: HeaderPane;
    readonly rowHeaderPane: HeaderPane;
    readonly cellRenderer: CellRenderer;
    readonly drawingRenderer: DrawingRenderer;

    readonly scrollNode: HTMLElement;
    readonly scrollSizeNode: HTMLElement;
    readonly layerRootNode: HTMLElement;
    readonly staticRootNode: HTMLElement;

    getHeaderPane(columns: boolean): HeaderPane;
    createLayerNode(className: string, options?: LayerNodeOptions): HTMLDivElement;
    grabFocus(): void;
    isVisible(): boolean;
    getLayerRange(): Range | null;
    getLayerRectangle(): Rectangle | null;
    convertToLayerRectangle(rectangle: RectangleLike): Rectangle;
    renderRanges(ranges: RangeSource, options?: RenderRangeIteratorOptions): IterableIterator<RenderRangeData>;
    getHiddenSize(): Size;
    getVisibleRectangle(): Rectangle;
    getTopLeftAddress(): Address;
    scrollRelative(leftDiff: number, topDiff: number): void;
    scrollToCell(address: Address): void;
    getSheetPointForPagePoint(point: Point | MouseEvent, options?: object): Opt<Point>;
}
