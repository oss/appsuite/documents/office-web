/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { createDiv } from "@/io.ox/office/tk/dom";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import { SelectionMode } from "@/io.ox/office/spreadsheet/utils/paneutils";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

import "@/io.ox/office/spreadsheet/view/pane/cornerpane.less";

const { ceil } = Math;

// types ======================================================================

/**
 * Type mapping for the events emitted by `CornerPane` instances.
 */
export interface CornerPaneEventMap {

    /**
     * Will e emitted after the corner pane has been clicked which will select
     * the entire sheet.
     *
     * @param mode
     *  The selection mode according to the keyboard modifier keys. Triggers
     *  only `SELECT` (should move the current active cell in the selection to
     *  the first visible cell in the selection), or `EXTEND` (should keep the
     *  current active cell in the selection unmodified), but never `APPEND`.
     */
    "select:all": [mode: SelectionMode];
}

// class CornerPane ===========================================================

/**
 * Represents the top-left corner in the spreadsheet view.
 */
export class CornerPane extends ViewObject<SpreadsheetView, CornerPaneEventMap> {

    /**
     * The root DOM element of the corner pane.
     */
    readonly el = createDiv("header-pane corner-pane noI18n");
    readonly $el = $(this.el);

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The spreadsheet view that contains this corner pane.
     */
    constructor(docView: SpreadsheetView) {

        // base constructor
        super(docView);

        this.el.setAttribute("aria-hidden", "true");

        // immediately react on mousedown (do not wait for click)
        this.$el.on("mousedown touchstart", event => {

            if ((event.type === "touchstart") || (event.button === 0)) {
                this.trigger("select:all", event.shiftKey ? SelectionMode.EXTEND : SelectionMode.SELECT);
            }

            // return focus to active grid pane, after the event has been processed
            this.setTimeout(() => docView.grabFocus(), 0);

            // do not process the event further
            event.preventDefault();
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Initializes the size of this corner pane.
     *
     * @param digitCount
     *  The number of digits to be reserved for row numbers displayed in the
     *  row header panes.
     */
    initializePaneLayout(digitCount: number): void {

        // initialize according to visibility
        if (this.docView.sheetModel.isCellType) {

            // show the corner pane in worksheets
            this.$el.show();

            // prepare the cell node text and font size
            const cellNode = createDiv({ classes: "cell", label: "9".repeat(digitCount) });
            cellNode.style.fontSize = `${this.docView.getHeaderFontSize()}pt`;
            this.$el.append(cellNode);

            // copy size of cell node to root node
            const nodeRect = cellNode.getBoundingClientRect();
            this.$el.empty().css({ width: ceil(nodeRect.width) + 6, height: ceil(nodeRect.height) + 2 });

        } else {

            // hide corner pane in chart sheets etc.
            this.$el.hide();
        }
    }
}
