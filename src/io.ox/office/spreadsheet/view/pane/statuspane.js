/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { getScreenHeight, getWindowHeight } from "@/io.ox/office/tk/dom";
import { isSoftKeyboardOpen } from "@/io.ox/office/tk/utils";

import { ToolPane } from "@/io.ox/office/baseframework/view/pane/toolpane";
import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';

import { MORE_LABEL } from "@/io.ox/office/spreadsheet/view/labels";
import { Button, DynamicLabel, CompoundButton, ActiveSheetList, ActiveSheetGroup, SubtotalList } from "@/io.ox/office/spreadsheet/view/controls";

// class SheetToolBar =========================================================

class SheetToolBar extends ToolBar {

    constructor(docView) {

        super(docView);

        // the sheet selector controls
        this.largeSheetList = new ActiveSheetList(docView, { showNames: true });
        this.smallSheetList = new ActiveSheetList(docView);
        this.sheetGroup = new ActiveSheetGroup(docView);

        // create the "More" drop-down menu with all standard actions for sheets
        const moreMenuGroup = new CompoundButton(docView, { label: MORE_LABEL, tooltip: gt("More sheet operations") });
        docView.addSheetActionButtons(moreMenuGroup);

        // add all sheet controls to the toolbar
        this.addControl("view/sheet/active", this.largeSheetList);
        this.addControl("view/sheet/active", this.smallSheetList);
        this.addContainer();
        this.addControl("view/sheet/active", this.sheetGroup);
        this.addContainer();
        this.addControl("document/insertsheet", new Button({ icon: "svg:add", tooltip: gt("Insert sheet") }), { visibleKey: "!app/viewermode" });
        this.addControl("view/sheet/more", moreMenuGroup, { visibleKey: "!app/viewermode" });
    }

    // public methods ---------------------------------------------------------

    /**
     * Adjusts the leading padding to fit the size of the cell row headers.
     */
    toggleHeaderPadding(state) {
        $(this.el.firstChild).css("padding-left", state ? this.docView.getHeaderWidth() : "");
    }

    /**
     * Hides the sheet dropdown menus, shows the button group with full width
     * (all sheets, no scroll buttons).
     */
    setFullWidth() {

        // reset all controls to full-width mode
        this.largeSheetList.hide();
        this.smallSheetList.hide();
        this.sheetGroup.show();
        this.sheetGroup.setFullWidth();

        // add leading padding with the width of the row header panes to align sheet tabs with cell area
        this.toggleHeaderPadding(true);
    }

    /**
     * Shrinks the sheet controls to make them fit into the specified width.
     */
    shrinkToWidth(availableWidth) {

        // small devices: hide sheet tabs completely, and show dropdown menu with sheet name
        let dynamicGroup;
        if (availableWidth < 450) {
            // show large sheet dropdown menu only, hide sheet button group
            this.sheetGroup.hide();
            this.largeSheetList.show();
            // calculate and set remaining space for the sheet dropdown button
            dynamicGroup = this.largeSheetList;
        } else {
            // show additional dropdown menu for all sheets before the sheet button group
            this.smallSheetList.show();
            // calculate and set remaining space for the sheet tabs
            dynamicGroup = this.sheetGroup;
        }

        // expand or shrink the specified form control to fit into the remaining space in sheet toolbar
        const toolBarWidth = this.$el.outerWidth();
        const controlWidth = dynamicGroup.$el.outerWidth();
        dynamicGroup.setWidth(Math.floor(controlWidth + availableWidth - toolBarWidth));
    }
}

// class StatusToolBar ========================================================

class StatusToolBar extends ToolBar {

    constructor(docView) {

        super(docView, { hideDisabled: true });

        this.addControl("view/status/label",     new DynamicLabel());
        this.addControl("view/status/formula",   new DynamicLabel());
        this.addControl("view/status/subtotals", new SubtotalList(docView));
    }
}

// class StatusPane ===========================================================

/**
 * The status pane in spreadsheet documents containing the sheet tabs and the
 * subtotal results of the current selection.
 *
 * @param {SpreadsheetView} docView
 *  The spreadsheet view containing this instance.
 */
export class StatusPane extends ToolPane {

    constructor(docView) {

        // base constructor
        super(docView, {
            classes: "status-pane",
            position: "bottom"
        });

        // the leading toolbar for all sheet controls
        this._sheetToolBar = this.addToolBar(new SheetToolBar(docView));

        // the trailing toolbar for status controls
        this.addToolBar(new StatusToolBar(docView), { targetSection: "trailing" });

        // repaint after sheet tabs have been inserted, deleted, or changed
        this.listenTo(this._sheetToolBar.sheetGroup, "change:sheets", () => this.refresh());

        // repaint once when activating a sheet after/during import
        this.listenTo(docView, "change:activesheet", () => this.refresh(), { once: true });

        // initialize visibility
        this.toggle(this.docApp.getUserSettingsValue("showStatusBar", true));

        // always hide the status pane when an internal application error occurs
        this.listenTo(this.docApp, "docs:state:error", () => this.hide());

        //".window-container" is oriented in screen with "top left right bottom" set to zero,
        //when the soft keyboard on Android devices pops up, the status bar stays over the keyboard
        //this makes no sense and looks ugly, so we detect the open keyboard and hide the status pane
        if (_.browser.Android) {

            // old visible boolean for hiding this bar when soft keyboard is open on Android devices
            let manuallyHidden = false;

            this.listenToWindowResizeWhenVisible(() => {
                if (isSoftKeyboardOpen() && (getWindowHeight() / getScreenHeight()) < 0.7) {
                    if (!manuallyHidden && this.isVisible()) {
                        manuallyHidden = true;
                        this.hide();
                    }
                } else {
                    if (manuallyHidden) { this.show(); }
                    manuallyHidden = false;
                }
            });
        }

        if (_.browser.iOS) {
            this.$el.on("taphold", event => {
                $(event.target).trigger("contextmenu");
                this.$el.one("touchend", false);
                return false;
            });
        }
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRefresh() {
        super.implRefresh();

        // reset sheet toolbar to full-width mode, exit if everything fits
        this._sheetToolBar.setFullWidth();
        if (!this.isContentOversized()) { return; }

        // remove leading padding (sheet dropdown menu will be shown at beginning of toolbar)
        this._sheetToolBar.toggleHeaderPadding(false);
        if (!this.isContentOversized()) { return; }

        // available width for the sheet toolbar
        const availableWidth = this.$centerSection.outerWidth() - this.getContentExcessSize();
        this._sheetToolBar.shrinkToWidth(availableWidth);
    }
}
