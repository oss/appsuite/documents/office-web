/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";

/* ========================================================================= */
/* class GridPane                                                            */
/* ========================================================================= */

.io-ox-office-main.io-ox-office-spreadsheet-main .pane-root {

    // z-index of the overlay nodes used while tracking column/row size, and split tracking
    @overlay-z-index: 100;

    > .grid-pane {

        position: absolute;
        overflow: hidden;
        background: var(--pane-root-background);
        user-select: none;
        .focus-no-outline();
        font-size: var(--std-font-size);
        color-scheme: light; // for light scrollbars, also in "dark" themes

        .clipboard {
            left: -1px;
            top: -1px;
        }

        .grid-static-back {
            position: absolute;
            inset: 0;
            background-color: white;
        }

        .grid-scroll {
            .absolute-with-distance(0);
            overflow: scroll;
            z-index: 1; // bug 36003: No scroll bar in split views

            :root.dark & {
                scrollbar-color: #ccc #eee; // reset dark scrollbars in Firefox
            }

            .grid-scroll-size {
                position: relative;
                display: inline-block;
                overflow: hidden;
                cursor: cell;
            }
        }

        .grid-layer-root {
            position: absolute;
            overflow: visible;
            left: 0;
            top: 0;
        }

        .grid-layer {
            position: absolute;
            left: 0;
            top: 0;
            width: 1px;
            height: 1px;
            overflow: visible;
        }

        .grid-static-root {
            position: absolute;
            left: 0;
            top: 0;
            overflow: hidden;
        }

        // generic formatting for the user name bagde in remote selection
        [data-remote-user][data-remote-color] {

            &::after {
                content: attr(data-remote-user);
                position: absolute;
                z-index: @overlay-z-index;
                opacity: 0;
                transition: opacity 0.2s linear;
                padding: 2px 7px;
                line-height: normal;
                color: white;
                white-space: nowrap;
                pointer-events: none;
            }

            // set fill color of the user badge
            .generate-scheme-color-rules({
                &[data-remote-color="@{scheme-index}"]::after {
                    background-color: fade(@scheme-color, 80%);
                }
            });

            // show the user badge when hovering the element
            &:hover::after {
                opacity: 1;
            }

            // X position (horizontally flipped drawings)
            &:not(.flipH)::after { left: 0; }
            &.flipH::after { right: 0; }

            // Y position (inside, vertically flipped drawings)
            &:not(.remote-badge-outside) {
                &:not(.flipV)::after { top: 0; }
                &.flipV::after { bottom: 0; }
            }

            // Y position (outside, vertically flipped drawings)
            &.remote-badge-outside {
                &:not(.flipV)::after { bottom: 100%; }
                &.flipV::after { top: 100%; }
            }

            // flipped drawings: revert mirroring
            &.flipH::after { transform: scaleX(-1); }
            &.flipV::after { transform: scaleY(-1); }
            &.flipH.flipV::after { transform: scale(-1); }
        }

        // cell renderer (cells and grid lines) -------------------------------

        // canvas elements of background layer and text layer
        .grid-layer-root > canvas.cell-layer {
            position: absolute;
            left: 0;
            top: 0;
        }

        // outlines of table ranges
        .table-layer > .range {
            position: absolute;
            box-sizing: border-box;
            border: 1px solid fade(#0000ff, 20%);
        }

        // remote layer (remote user selections) ------------------------------

        // remote selection for cell ranges
        .remote-layer > .range {
            position: absolute;
            border: 1px solid transparent;

            > .fill {
                .absolute-with-distance(1px);
            }

            // set different colors (as fill color and as border color)
            .generate-scheme-color-rules({
                &[data-remote-color="@{scheme-index}"] {
                    border-color: fade(@scheme-color, 50%);
                    > .fill { background-color: fade(@scheme-color, 10%); }
                }
            });
        }

        // selection layer (cell selection) -----------------------------------

        .selection-layer {

            > .range {
                position: absolute;
                box-sizing: border-box;
                .box-shadow();

                &.active {
                    .box-shadow(10px);
                }

                > .border {
                    .absolute-with-distance(0);
                    border: 1px solid transparent;
                }

                &:not(.collapsed) > .border::before {
                    content: "";
                    .absolute-with-distance(0);
                    border: 1px solid fade(white, 50%);
                }

                > .fill {
                    position: absolute;
                }

                // no native touch scrolling when dragging the resizer elements
                > .resizers > [data-pos] {
                    touch-action: none;
                }

                // auto-fill tracking handles for entire columns/rows
                > .autofill.resizers {
                    > [data-pos="r"] { margin-top: 0 !important; }
                    > [data-pos="b"] { margin-left: 0 !important; }
                }
            }

            > .active-cell {
                position: absolute;
                box-sizing: border-box;

                &::before {
                    content: "";
                    .absolute-with-distance(0);
                    border: 1px solid transparent;
                }
            }
        }

        .cell-selection-styles(@selection-color; @delete-color) {

            .selection-layer {

                // colors for range nodes
                > .range {

                    // range borders, always with passed selection color
                    > .border { border-color: fade(@selection-color, 70%); }
                    &.tracking-active > .border {
                        box-shadow: 0 0 3px @selection-color, 0 0 2px @selection-color inset;
                    }

                    // fill element, color according to autofill deletion state
                    .fill-colors(@color) {
                        @fill-color: fade(@color, 20%);
                        > .fill { background-color: @fill-color; }
                    }
                    .fill-colors(@selection-color);
                    &.autofill-delete { .fill-colors(@delete-color); }

                    // selection/auto-fill tracking handles, always with passed selection color
                    .resizer-handle-styles(7px; 3px; 1px; -1px; @selection-color; true; true; true);
                }

                // colors for the active cell node
                > .active-cell {
                    @border-color: fade(@selection-color, 35%);
                    &.left::before { border-left-color: @border-color; }
                    &.top::before { border-top-color: @border-color; }
                    &.right::before { border-right-color: @border-color; }
                    &.bottom::before { border-bottom-color: @border-color; }
                }
            }

            &.touch .selection-layer > .range {
                .resizer-handle-styles(17px; 5px; 1px; -2px; @selection-color; true; true; true);
            }
        }
        &.focused { .cell-selection-styles(@doc-selection-color; @doc-selection-color-collapsed); }
        &:not(.focused) { .cell-selection-styles(@doc-selection-color-inactive; @doc-selection-color-inactive); }

        // form layer (drop-down buttons) -------------------------------------

        .form-layer > .cell-dropdown-button {
            position: absolute;
            box-sizing: border-box;
            padding: 0;
            background: var(--background-50);
            border: 1px solid var(--border);
            border-radius: 0;
            z-index: 1;
            color: var(--text);
            .hover-overlay();

            &[data-icon="none"] [data-icon-id] {
                width: calc(var(--caret-width) + 2px);
                height: calc(var(--caret-width) + 2px);
            }
        }

        // drawing layer (drawing objects and selection) ----------------------

        .drawing-layer {

            .page, .pagecontent, .sheet {
                .absolute-with-distance(0);
            }

            .drawing {
                position: absolute;
                // bug 56713: all drawings need a specified z-index, required after introduction of rotation
                z-index: 1;
                // no native touch scrolling when tapping/dragging drawing objects
                touch-action: none;

                // placeholder frame for new unformatted drawing frames
                &.uninitialized {
                    box-shadow: 0 0 0 1px fade(@doc-selection-color, 50%);
                    > .content { display: none; }
                }

                // crop text contents at outer text frame
                > .content > .textframe {
                    overflow: hidden;

                    // DOCS-2763: single click on text contents starts edit mode
                    > * { cursor: text; }
                }

                // hide the own text contents while text edit mode is active (text
                // edit mode renders its own text contents over the drawing frame)
                &.edit-active > .content > .textframe > * {
                    display: none;
                }

                // remote selection border for drawing objects
                > .remote-selection {
                    .absolute-with-distance(-2px);
                    border: 2px solid transparent;
                }

                // set different colors (as fill color and as border color)
                .generate-scheme-color-rules({
                    &[data-remote-color="@{scheme-index}"] > .remote-selection {
                        border-color: @scheme-color;
                    }
                });
            }
        }

        &.focused .drawing-layer .drawing > .selection {
            .border-handle-color-styles(@doc-selection-color);
            .resizer-handle-color-styles(@doc-selection-color; true);
            .rotate-handle-color-styles(@doc-selection-color; true);
        }

        &:not(.focused) .drawing-layer .drawing > .selection {
            .border-handle-color-styles(@doc-selection-color-inactive);
            .resizer-handle-color-styles(@doc-selection-color-inactive; true);
            .rotate-handle-color-styles(@doc-selection-color-inactive; true);
        }

        // global drawing frame container used in drawing edit mode
        .grid-static-root > .formatted-content .drawing {
            position: absolute;

            > .content > :not(.textframe) {
                display: none;
            }

            &:not(.edit-active) {
                display: none;
            }

            &.edit-active > .content > .textframe {
                position: relative;
                cursor: text;
            }
        }

        .grid-static-root > .formatted-content .drawing[data-type="shape"] {

            pointer-events: none;

            > .content > .textframe {
                pointer-events: all;
            }
        }

        // note layer (cell notes and connectors) ----------------------------

        .note-layer {

            // special formatting for text frames of cell note
            .drawing[data-type="note"] {
                display: block;
                position: absolute;
                box-shadow: 1px 2px 6px fade(black, 25%);
                cursor: cell;
                pointer-events: none;

                > .content > .textframe {
                    height: 100%;
                    line-height: normal;
                }
            }
        }

        // rectanguar boxes for mouse hover detection and hint indicators
        .note-overlay-layer > div {
            position: absolute;
            z-index: 1;

            > .hint {
                position: absolute;
                right: 0;
                top: 0;
                width: 0;
                height: 0;
                border: 1px solid transparent;
            }

            &.note > .hint {
                border-top-color: red;
                border-right-color: red;
            }

            &.thread > .hint {
                border-top-color: var(--comment-bubble);
                border-right-color: var(--comment-bubble);
            }

            &.thread > .bubble {
                position: absolute;
                right: -3px;
                top: 0;
                width: 1px;
                height: 1px;
                color: var(--comment-bubble);

                .generate-scheme-color-rules({
                    &[data-scheme-color="@{scheme-index}"] {
                        color: @scheme-color;
                    }
                });

                cursor: pointer;

                .icon-bubble-wrapper {
                    position: absolute;
                    left: 3px;
                    bottom: 1px;

                    .icon-bubble-tapper {
                        position: absolute;
                        width: 100%;
                        height: 100%;
                        top: 0;
                    }
                }

                &.t {
                    .icon-bubble-wrapper {
                        position: absolute;
                        top: -1px;
                        bottom: auto;
                    }
                }
            }
        }

        // all connector lines of cell comments (below connector text frames)
        .note-connector-layer > svg {
            position: absolute;
            pointer-events: none;
            z-index: 1;
        }

        // override the "hidden" class to temporarily show comments (search, mouse hover)
        .show-temp {
            display: block !important;
        }

        // highlight layer (highlighted formula ranges) -----------------------

        .highlight-layer > .range {
            position: absolute;

            > .fill {
                .absolute-with-distance(2px);
            }

            > .static-border {
                .absolute-with-distance(0);
                z-index: 1;
                border: 1px solid black;
            }

            :not(.touch) & {
                .border-handle-styles(2px; 3px; 0; null; true; true; true);
                .resizer-handle-styles(6px; 3px; 1px; 0; null; true; true; true);
            }
            .touch & {
                .border-handle-styles(2px; 5px; 0; null; true; true; true);
                .resizer-handle-styles(12px; 5px; 1px; 0; null; true; true; true);
            }
            .border-handle-z-index(2);
            .resizer-handle-z-index(3);

            // set different colors (as fill color and as border color)
            .generate-scheme-color-rules({
                &[data-style="@{scheme-index}"] {
                    @border-color: fade(@scheme-color, 70%);
                    .border-handle-color-styles(@border-color; true; true);
                    .resizer-handle-color-styles(@scheme-color; true; true);
                    > .fill { background-color: fade(@scheme-color, 10%); }
                    > .static-border { border-color: @border-color; }
                }
            });
        }

        // active layer (active cell range) ----------------------------------

        .active-layer {

            > .range {
                position: absolute;
                .border-handle-styles(2px; 0; 0; white);
            }

            // create wireframe style ("marching ants") for active range
            each(range(0, 5), {
                &[data-frame="@{value}"] > .range {
                    @url: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAIAAABvrngfAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAJUlEQVR42l3HsREAIACDQHD/nbHwbELFy69641odA46BM67OGLgDJhIGQ+H2HgAAAABJRU5ErkJggg==");
                    .border-handle-background-style(@url repeat 0 unit(@value, px));
                }
            });
        }

        // frame selection layer ---------------------------------------------

        .frame-select-layer:not(.active) {
            overflow: hidden;
        }

        .frame-select-layer.active {
            width: 100%;
            height: 100%;
            cursor: crosshair;
            z-index: @overlay-z-index;

            > .frame-line {
                position: absolute;
                top: 0;
                left: 0;
                box-sizing: border-box;
                background-color: @doc-frame-line-color;

                &.v {
                    width: 1px;
                    height: 100%;
                }

                &.h {
                    width: 100%;
                    height: 1px;
                }
            }

            > .frame-rect {
                position: absolute;
                box-sizing: border-box;
            }
        }
    }

    // special cell cursor in cell-type sheets
    &.cell-sheet > .grid-pane .grid-layer-root {
        cursor: cell;
    }

    // hide auto-fill handle in read-only mode
    &:not(.edit-mode) > .grid-pane .selection-layer .autofill.resizers {
        display: none;
    }

    // bold border for active selection range (not when editing cells)
    &:not([data-text-edit="cell"]) > .grid-pane .selection-layer {
        .range.active > .border {
            .absolute-with-distance(-2px);
            border-width: 3px;
        }
    }

    // special formatting in grid panes during cell edit mode
    &[data-text-edit="cell"] > .grid-pane {

        // do not show auto-fill or touch selection handles while cell edit mode is active
        .selection-layer {
            .range { box-shadow: none; }
            .resizers { display: none; }
        }

        // do not show remote selections while cell edit mode is active
        .remote-layer {
            display: none;
        }
    }

    // hide cell selection and form layer while drawing selection is active
    &.drawing-selection > .grid-pane {
        .selection-layer, .form-layer {
            display: none;
        }
    }

    // hide form layer in read-only mode
    &:not(.edit-mode) > .grid-pane .form-layer {
        display: none;
    }

    // show move cursor over unselected drawing frames
    &.edit-mode:not(.sheet-locked) > .grid-pane .drawing-layer {

        .drawing:not([data-type="group"], .selected) {
            cursor: move;
        }

        /* no move cursor over the empty area of unselected group objects */
        .drawing[data-type="group"]:not(.selected) {
            cursor: cell;
        }

        /* but show the move cursor on empty area of group objects inside selected groups */
        .drawing[data-type="group"].selected.movable .drawing[data-type="group"] {
            cursor: move;
        }
    }

    // Bug 34978: Safari on iOS 8: If the clipboard node is out of the visible
    // window (top: 110%) the focus gets lost. Don't know why, but then there
    // is a great area which is not clickable. This behavior is reproducible
    // but not really explainable.
    > .grid-pane.browser-safari.touch .clipboard {
        top: 0;
    }
}
