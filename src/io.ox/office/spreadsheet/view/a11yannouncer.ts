/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import type { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import { getColLabel, getRowLabel, getCellLabel } from "@/io.ox/office/spreadsheet/view/labels";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// class A11YAnnouncer ========================================================

/**
 * Generates A11Y announcements for the current selection.
 */
export class A11YAnnouncer extends ViewObject<SpreadsheetView> {

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView) {
        super(docView);

        // announce changed cell/drawing selection
        this.listenTo(docView, "change:selection", (newSel, oldSel, sheetChanged) => {
            this.#handleSelectionChange(newSel, oldSel, sheetChanged);
        });
    }

    // private methods --------------------------------------------------------

    #handleSelectionChange(newSel: SheetSelection, oldSel: SheetSelection, sheetChanged: boolean): void {

        const { addressFactory } = this.docModel;
        const { sheetModel } = this.docView;
        const { ranges, address, drawings } = newSel;
        const range = ranges[0];

        const tokens: string[] = [];

        // announce changed sheet
        if (sheetChanged) {
            //#. screen reader announcement: another sheet has been activated, e.g.: Sheet "Invoices" activated.
            //#. %1$s is the name of the active sheet
            tokens.push(gt('Sheet "%1$s" activated.', sheetModel.getName()));
        }

        // add additional announcement for cell selection
        switch (true) {

            // announce selected drawing objects
            case drawings.length > 0:
                //#. screen reader announcement: multiple drawing objects selected
                //#. %1$d is the number of drawing objects
                tokens.push(gt.ngettext("%1$d drawing object selected.", "%1$d drawing objects selected.", drawings.length, drawings.length));
                break;

            // multiple cell ranges selected
            case ranges.length > 1:
                //#. screen reader announcement: multiple cell ranges selected
                //#. %1$d is the number of cell ranges
                tokens.push(gt.ngettext("%1$d cell range selected.", "%1$d cell ranges selected.", ranges.length, ranges.length));
                break;

            // active cell has moved, or active cell in selected range has changed
            case ranges.deepEquals(oldSel.ranges) || sheetModel.isSingleCellInRange(range): {
                const display = this.docView.cellCollection.getDisplayString(address);
                const label = getCellLabel(address, { prefix: true, a11y: true });
                tokens.push(display ? (display + " " + label) : label);
                break;
            }

            // entire column(s) selected
            case addressFactory.isColRange(range): {
                if (range.singleCol()) {
                    tokens.push(getColLabel(range.a1.c, { prefix: true, a11y: true }));
                } else {
                    const label1 = getColLabel(range.a1.c, { a11y: true });
                    const label2 = getColLabel(range.a2.c, { a11y: true });
                    //#. screen reader announcement: multiple columns selected, e.g. "Columns A to C selected."
                    //#. %1$s is the name of the first column
                    //#. %2$s is the name of the last column
                    tokens.push(gt("Columns %1$s to %2$s selected.", label1, label2));
                }
                break;
            }

            // entire row(s) selected
            case addressFactory.isRowRange(range): {
                if (range.singleRow()) {
                    tokens.push(getRowLabel(range.a1.r, { prefix: true, a11y: true }));
                } else {
                    const label1 = getRowLabel(range.a1.r, { a11y: true });
                    const label2 = getRowLabel(range.a2.r, { a11y: true });
                    //#. screen reader announcement: multiple rows selected, e.g. "Rows 1 to 5 selected."
                    //#. %1$s is the name of the first row
                    //#. %2$s is the name of the last row
                    tokens.push(gt("Rows %1$s to %2$s selected.", label1, label2));
                }
                break;
            }

            // single cell range selected
            default: {
                const label1 = getCellLabel(range.a1, { a11y: true });
                const label2 = getCellLabel(range.a2, { a11y: true });
                //#. screen reader announcement: start/end addresses of selected cell range, e.g. "A1 to C5 selected."
                //#. %1$s is the first cell address
                //#. %2$s is the last cell address
                tokens.push(gt("%1$s to %2$s selected.", label1, label2));
            }
        }

        this.docView.sendA11YAnnouncement(tokens.join(" "));
    }
}
