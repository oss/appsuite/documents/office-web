/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is, math, itr, map, set, dict, pick, jpromise } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";
import { COMPACT_DEVICE, SCROLLBAR_WIDTH, SCROLLBAR_HEIGHT, addDeviceMarkers, hasModifierKeys, matchKeyCode, getFocus, setElementStyle, setElementStyles, createDiv } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { getStringOption, isContextEventTriggeredByKeyboard } from "@/io.ox/office/tk/utils";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { DObject, debounceMethod } from "@/io.ox/office/tk/objects";

import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";
import { CommentsPane } from "@/io.ox/office/editframework/view/pane/commentspane";
import { debounceAfterActionsMethod } from "@/io.ox/office/editframework/app/decorators";
import { ChartLabelsMenu } from "@/io.ox/office/drawinglayer/view/popup/chartlabelsmenu";
import { isCommentTextframeNode } from "@/io.ox/office/textframework/utils/dom";
import TextBaseView from "@/io.ox/office/textframework/view/view";

import { COMBINED_TOOL_PANES } from "@/io.ox/office/spreadsheet/utils/config";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import {
    ALL_PANE_SIDES, ALL_PANE_POSITIONS, MIN_PANE_SIZE,
    PaneSide, PanePos, isColumnSide,
    getColPaneSide, getRowPaneSide, getNextColPanePos, getNextRowPanePos, getPanePos
} from "@/io.ox/office/spreadsheet/utils/paneutils";

import { DeleteSheetUpdateTask, RenameSheetUpdateTask, RelabelNameUpdateTask, RenameTableUpdateTask, MoveCellsUpdateTask } from "@/io.ox/office/spreadsheet/model/formula/parser/updatetask";
import { SHEET_FORWARD_EVENTS } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

import { ViewPropertySet } from "@/io.ox/office/spreadsheet/view/viewpropset";
import { StatusPane } from "@/io.ox/office/spreadsheet/view/pane/statuspane";
import { FormulaPane } from "@/io.ox/office/spreadsheet/view/pane/formulapane";
import { CornerPane } from "@/io.ox/office/spreadsheet/view/pane/cornerpane";
import { HeaderPane } from "@/io.ox/office/spreadsheet/view/pane/headerpane";
import { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";

import {
    CALCULATING_LABEL, RESULT_MESSAGES,
    INSERT_SHEET_LABEL, RENAME_SHEET_LABEL, COPY_SHEET_LABEL, DELETE_SHEET_LABEL, REORDER_SHEETS_LABEL, UNHIDE_SHEETS_LABEL,
    FORMAT_HEADER_LABEL, FONT_HEADER_LABEL, ALIGNMENT_HEADER_LABEL, CELL_HEADER_LABEL, NUMBERFORMAT_HEADER_LABEL,
    DATA_HEADER_LABEL, INSERT_HEADER_LABEL, COL_ROW_HEADER_LABEL, TABLE_HEADER_LABEL, REVIEW_HEADER_LABEL,
    OPTIONS_LABEL, SHOW_TOOLBARS_CHECKBOX_OPTIONS, SHOW_COMMENTSPANE_CHECKBOX_OPTIONS, SHOW_COLLABORATORS_CHECKBOX_OPTIONS,
    DELETE_DRAWING_BUTTON_OPTIONS, CHART_LABELS_BUTTON_OPTIONS,
    CHART_DATA_POINTS_BUTTON_OPTIONS, CHART_SHOW_POINT_LABELS_BUTTON_OPTIONS, CHART_VARY_POINT_COLORS_BUTTON_OPTIONS
} from "@/io.ox/office/spreadsheet/view/labels";

import {
    Button, CheckBox, CompoundButton,
    ShowSheetButton, LockSheetButton,
    TextColorPicker, CellFillColorPicker,
    DynamicSplitCheckBox, FrozenSplitCheckBox,
    InsertAutoSumButton, InsertFunctionButton,
    SortMenuButton, FilterButton, RefreshButton,
    NamesMenuButton, CellProtectionMenuButton,
    DrawingLineStylePicker, DrawingArrowPresetPicker,
    DrawingLineColorPicker, DrawingFillColorPicker,
    ShapeTypePicker, DrawingArrangementPicker,
    ChartTypePicker, ChartColorSetPicker, ChartStyleSetPicker, ChartLegendPicker
} from "@/io.ox/office/spreadsheet/view/controls";

import {
    FontStyleToolBar, FontFamilyToolBar, CellFormatPainterToolBar,
    CellColorToolBar, CellAlignmentToolBar, NumberFormatToolBar,
    CellBorderToolBar, CellStyleToolBar, BoxAlignmentToolBar, ListStyleToolBar,
    ColRowToolBar, TableToolBar, InsertContentToolBar,
    InsertDrawingToolBar, InsertCommentToolBar, CommentsToolBar, NotesToolBar
} from "@/io.ox/office/spreadsheet/view/toolbars";

import { NamesMenu } from "@/io.ox/office/spreadsheet/view/popup/namesmenu";
import { ViewFuncMixin } from "@/io.ox/office/spreadsheet/view/mixin/viewfuncmixin";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";

import { RangeHighlighter } from "@/io.ox/office/spreadsheet/view/edit/rangehighlighter";
import { editLogger } from "@/io.ox/office/spreadsheet/view/edit/texteditorbase";
import { CellTextEditor } from "@/io.ox/office/spreadsheet/view/edit/celltexteditor";
import { DrawingTextEditor } from "@/io.ox/office/spreadsheet/view/edit/drawingtexteditor";
import { SelectionEngine } from "@/io.ox/office/spreadsheet/view/selection/selectionengine";
import { A11YAnnouncer } from "@/io.ox/office/spreadsheet/view/a11yannouncer";

import "@/io.ox/office/spreadsheet/view/style.less";

// constants ==================================================================

// predefined zoom factors
const ZOOM_STEPS = [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.5, 3, 4, 5, 6, 8];

// the size of the freeze separator nodes, in pixels
const FROZEN_SPLIT_SIZE = 1;

// the size of the split separator nodes, in pixels
const DYNAMIC_SPLIT_SIZE = 2;

// the highlighted inner size of the split tracking node, in pixels
const TRACKING_SPLIT_SIZE = 4;

// the additional margin of the split tracking nodes, in pixels
const TRACKING_MARGIN = 3;

// the position offset of tracking nodes compared to split lines
const TRACKING_OFFSET = (TRACKING_SPLIT_SIZE - DYNAMIC_SPLIT_SIZE) / 2 + TRACKING_MARGIN;

// private functions ==========================================================

function initLineElement(elem, visible, styles) {
    elem.classList.toggle("hidden", !visible);
    setElementStyles(elem, styles);
}

// class HeaderPaneEntry ======================================================

/**
 * Wrapper for a header pane instance, and additional settings needed for
 * DOM layout, intended to be used as map entry for the header pane map.
 *
 * @implements HeaderPaneLayout
 */
class HeaderPaneEntry extends DObject {

    constructor(docView, paneSide) {
        super();
        this.headerPane = this.member(new HeaderPane(docView, paneSide));
        this.paneSide = paneSide;
        this.offset = 0;
        this.size = 1;
        this.frozen = false;
        this.showOppositeScroll = false;
        this.hiddenSize = 0;
    }
}

// class SpreadsheetView ======================================================

/**
 * Represents the entire view of a spreadsheet document. Contains the view
 * panes of the sheet currently shown (the "active sheet"), which contain the
 * scrollable cell grids (there will be several view panes, if the sheet view
 * is split or frozen); and the selections of all existing sheets.
 *
 * @mixes HighlightMixin
 * @mixes ViewFuncMixin
 *
 * @param {SpreadsheetApp} docApp
 *  The application instance containing this spreadsheet view.
 *
 * @param {SpreadsheetModel} docModel
 *  The document model created by the passed application.
 *
 * @property {RenderCache} renderCache
 *  The rendering cache of the active sheet containing processed rendering data
 *  for the visible columns, rows, and cells.
 *
 * @property {CornerPane} cornerPane
 *  The top-left corner pane used to select the entire sheet.
 *
 * @property {NamesMenu} namesMenu
 *  The floating menu containing all defined names.
 *
 * @property {ChartLabelsMenu} chartLabelsMenu
 *  The floating menu for axis and label formatting of chart objects.
 *
 * @property {RangeHighlighter} rangeHighlighter
 *  The container for cell range addresses currently highlighted in the GUI.
 */
export class SpreadsheetView extends TextBaseView {

    constructor(docApp, docModel) {

        // base constructor
        super(docApp, docModel, {
            zoomSteps: ZOOM_STEPS,
            grabFocusHandler: options => this._grabFocusHandler(options)
        });

        // dynamic property set for the document view
        this.propSet = this.member(new ViewPropertySet(this));

        // [readonly] the index, model, and property set of the active sheet
        this.activeSheet = -1;
        this.sheetModel = null;
        this.sheetPropSet = null;

        // [readonly] collections of the active sheet
        this.colCollection = null;
        this.rowCollection = null;
        this.cellCollection = null;
        this.mergeCollection = null;
        this.hlinkCollection = null;
        this.dvRuleCollection = null;
        this.cfRuleCollection = null;
        this.tableCollection = null;
        this.drawingCollection = null;
        this.noteCollection = null;
        this.commentCollection = null;

        // the row/column header panes, mapped by pane side identifiers (map has ownership)
        this._headerPaneMap = this.member(new Map/*<PaneSide, HeaderPaneEntry>*/());
        // the grid panes, mapped by pane position identifiers (map has ownership)
        this._gridPaneMap = this.member(new Map/*<PanePos, GridPane>*/());

        // the DOM root node of this view containing the headers and grid panes
        this._paneRootEl = createDiv("abs pane-root");
        // the split line separating the left and right panes
        this._colSplitLineEl = createDiv("split-line columns");
        // the split line separating the top and bottom panes
        this._rowSplitLineEl = createDiv("split-line rows");
        // the split tracking node covering the intersection of the other tracking points
        this._crossSplitTrackingEl = createDiv({ classes: "abs split-tracking cross", style: { width: TRACKING_SPLIT_SIZE, height: TRACKING_SPLIT_SIZE, padding: TRACKING_MARGIN } });
        // the split tracking node separating left and right panes
        this._colSplitTrackingEl = createDiv({ classes: "abs split-tracking columns", style: { width: TRACKING_SPLIT_SIZE, padding: `0 ${TRACKING_MARGIN}px` } });
        // the split tracking node separating top and bottom panes
        this._rowSplitTrackingEl = createDiv({ classes: "abs split-tracking rows", style: { height: TRACKING_SPLIT_SIZE, padding: `${TRACKING_MARGIN}px 0` } });

        // number of digits to be shown in row header panes
        this._rowDigitCount = 0;
        // false while grid panes are not initialized (influences focus handling)
        this._panesInitialized = false;
        // collect flags from multiple calls of `_requestInitializePanes`
        this._initPaneOptions = dict.create();
        // last known touch position (to open context menu on the correct position); with properties "pageX" and "pageY"
        this._lastTouchPos = null;

        // map of rendering caches per sheet model
        this._renderCacheMap = this.member(new Map/*<SheetModel, RenderCache>*/());

        // all registered text editors (container takes ownership)
        this._textEditorRegistry = this.member(new Map/*<string, TextEditorBase>*/());
        // reference to the editor instance that is currently active
        this._activeTextEditor = null;
        // the current status text label
        this._statusText = null;

        // marker for touch devices and browser types
        addDeviceMarkers(this._paneRootEl);

        // create floating menus
        this.namesMenu = this.member(new NamesMenu(this));
        this.chartLabelsMenu = this.member(new ChartLabelsMenu(this));

        // a container for cell ranges currently highlighted in the GUI
        this.rangeHighlighter = this.member(new RangeHighlighter(docModel));

        // the selection engine that manages cell and drawing selections
        this.selectionEngine = this.member(new SelectionEngine(this, this._paneRootEl));

        // A11Y announcements for current selection
        this.member(new A11YAnnouncer(this));

        // create the header pane instances
        for (const paneSide of ALL_PANE_SIDES) {
            this._headerPaneMap.set(paneSide, new HeaderPaneEntry(this, paneSide));
        }

        // create the grid pane instances
        for (const panePos of ALL_PANE_POSITIONS) {
            const gridPane = new GridPane(this, panePos);
            this._gridPaneMap.set(panePos, gridPane);
            // forward selection rendering events of the *active* grid pane
            for (const eventType of ["render:cellselection", "render:drawingselection"]) {
                this.listenTo(gridPane, eventType, (...args) => {
                    if (gridPane === this.getActiveGridPane()) {
                        this.trigger(eventType, ...args);
                    }
                });
            }
        }

        // create the corner pane instance
        this.cornerPane = this.member(new CornerPane(this));

        // create the implementation of the text edit modes
        this._registerTextEditor(new CellTextEditor(this));
        this._registerTextEditor(new DrawingTextEditor(this));

        // mix-in classes expecting fully initialized document view
        ViewFuncMixin.call(this);
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts generic sheet actions to the passed compound menu.
     *
     * @param {ICompoundContainer} container
     *  The compound menu to be extended.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {boolean} [options.insertSheet=false]
     *    If set to `true`, a button for the action "Insert Sheet" will be
     *    created.
     */
    addSheetActionButtons(container, options) {

        // actions for new sheets
        if (options?.insertSheet) {
            container.addSection("insertsheet");
            container.addControl("document/insertsheet", new Button({ label: INSERT_SHEET_LABEL }));
        }

        // actions for active sheet
        container.addSection("activesheet");
        container.addControl("sheet/rename/dialog",       new Button({ label: RENAME_SHEET_LABEL }));
        container.addControl("document/copysheet/dialog", new Button({ label: COPY_SHEET_LABEL }), { visibleKey: "document/ooxml" });
        container.addControl("document/deletesheet",      new Button({ label: DELETE_SHEET_LABEL }));
        container.addControl("sheet/visible",             new ShowSheetButton());
        container.addControl("sheet/locked",              new LockSheetButton());

        // actions for entire sheet collection
        container.addSection("sheetcollection");
        container.addControl("document/movesheets/dialog", new Button({ label: REORDER_SHEETS_LABEL }));
        container.addControl("document/showsheets/dialog", new Button({ label: UNHIDE_SHEETS_LABEL }));
    }

    /**
     * Returns the specified grid pane.
     *
     * @param {PanePos} panePos
     *  The position identifier of the grid pane.
     *
     * @returns {GridPane}
     *  The grid pane instance at the specified pane position.
     */
    getGridPane(panePos) {
        return this._gridPaneMap.get(panePos);
    }

    /**
     * Returns the specified grid pane, if it is visible. Otherwise, returns
     * the nearest visible grid pane.
     *
     * @param {PanePos} panePos
     *  The position identifier of the preferred grid pane. If only one grid
     *  pane is visible, it will be returned regardless of this parameter. If
     *  one of the pane sides contained in the passed pane position is not
     *  visible, returns the grid pane from the other visible pane side (for
     *  example, if requesting the top-left pane while only bottom panes are
     *  visible, returns the bottom-left pane).
     *
     * @returns {GridPane}
     *  The grid pane instance at the specified pane position.
     */
    getVisibleGridPane(panePos) {

        // jump to other column pane, if the pane side is hidden
        if (this._headerPaneMap.get(getColPaneSide(panePos)).size === 0) {
            panePos = getNextColPanePos(panePos);
        }

        // jump to other row pane, if the pane side is hidden
        if (this._headerPaneMap.get(getRowPaneSide(panePos)).size === 0) {
            panePos = getNextRowPanePos(panePos);
        }

        // now, panePos points to a visible grid pane (at least one pane is always visible)
        return this._gridPaneMap.get(panePos);
    }

    /**
     * Returns the grid pane that is currently focused.
     *
     * @returns {GridPane}
     *  The focused grid pane instance.
     */
    getActiveGridPane() {
        return this.getVisibleGridPane(this.sheetPropSet.get("activePane"));
    }

    /**
     * Returns the grid pane that contains the specified target cell. In a
     * frozen split view, only one grid pane can contain the specified cell. In
     * an unfrozen split view, any visible grid pane can contain the cell. In
     * the latter case, the active grid pane will be returned.
     *
     * @param {Address} address
     *  The address of the target cell.
     *
     * @returns {GridPane}
     *  The target grid pane instance.
     */
    getTargetGridPane(address) {

        // return active grid pane, if the view is not frozen
        if (!this.sheetModel.hasFrozenSplit()) { return this.getActiveGridPane(); }

        // the frozen column and row intervals
        const colInterval = this.sheetModel.getSplitColInterval();
        const rowInterval = this.sheetModel.getSplitRowInterval();
        // preferred pane side identifiers
        const colSide = (colInterval && (address.c <= colInterval.last)) ? PaneSide.L : PaneSide.R;
        const rowSide = (rowInterval && (address.r <= rowInterval.last)) ? PaneSide.T : PaneSide.B;

        // return the grid pane containing the cell address
        return this.getVisibleGridPane(getPanePos(colSide, rowSide));
    }

    /**
     * Returns the grid pane covered by the specified page coordinates.
     *
     * @param {Point|MouseEvent|JEvent} point
     *  The page coordinates (in pixels), or a browser event, expected to
     *  contain the numeric properties "pageX" and "pageY".
     *
     * @returns {Opt<GridPane>}
     *  The grid pane covering the specified page coordinates; or `undefined`,
     *  if the page coordinates are outside of all grid panes.
     */
    getGridPaneForPagePoint(point) {
        return map.find(this._gridPaneMap, gridPane => gridPane.containsPagePoint(point));
    }

    /**
     * Activates the specified grid pane, unless the view is currently in an
     * internal initialization phase, or the cell edit mode is currently
     * active.
     *
     * @param {PanePos} panePos
     *  The identifier of the grid pane to be activated.
     */
    updateActiveGridPane(panePos) {
        // do not change active pane while initializing panes, or while in cell
        // edit mode (range selection mode in formulas)
        if (this._panesInitialized && !this.isTextEditMode("cell")) {
            this.sheetPropSet.set("activePane", panePos);
        }
    }

    /**
     * Returns all grid panes contained in this view.
     *
     * @returns {readonly GridPane[]}
     *  All grid panes contained in this view.
     */
    getGridPanes() {
        return Array.from(this._gridPaneMap.values());
    }

    /**
     * Returns the specified header pane.
     *
     * @param {PaneSide} paneSide
     *  The identifier of the pane side.
     *
     * @returns {HeaderPane}
     *  The header pane instance at the specified pane side.
     */
    getHeaderPane(paneSide) {
        return this._headerPaneMap.get(paneSide).headerPane;
    }

    /**
     * Returns the horizontal header pane (left or right) for the specified
     * grid pane position.
     *
     * @param {PanePos} panePos
     *  The position identifier of the grid pane.
     *
     * @returns {HeaderPane}
     *  The horizontal header pane instance at the corresponding pane side.
     */
    getColHeaderPane(panePos) {
        return this.getHeaderPane(getColPaneSide(panePos));
    }

    /**
     * Returns the vertical header pane (top or bottom) for the specified grid
     * pane position.
     *
     * @param {PanePos} panePos
     *  The position identifier of the grid pane.
     *
     * @returns {HeaderPane}
     *  The vertical header pane instance at the corresponding pane side.
     */
    getRowHeaderPane(panePos) {
        return this.getHeaderPane(getRowPaneSide(panePos));
    }

    /**
     * Returns the first visible header pane in the specified direction (for
     * example, for columns returns the left header pane, if it is visible,
     * otherwise the right header pane).
     *
     * @param {boolean} columns
     *  If set to `true`, returns the first visible column header pane (left or
     *  right); otherwise returns the first visible row header pane (top or
     *  bottom).
     *
     * @returns {HeaderPane}
     *  The first visible header pane instance in the specified direction.
     */
    getVisibleHeaderPane(columns) {
        const paneSide = columns ?
            ((this._headerPaneMap.get(PaneSide.L).size > 0) ? PaneSide.L : PaneSide.R) :
            ((this._headerPaneMap.get(PaneSide.T).size > 0) ? PaneSide.T : PaneSide.B);
        return this.getHeaderPane(paneSide);
    }

    /**
     * Returns a header pane associated to the grid pane that is currently
     * focused.
     *
     * @param {boolean} columns
     *  If set to `true`, returns the column header pane (left or right);
     *  otherwise returns the row header pane (top or bottom).
     *
     * @returns {HeaderPane}
     *  A header pane instance associated to the grid pane that is currently
     *  focused.
     */
    getActiveHeaderPane(columns) {
        return this.getActiveGridPane().getHeaderPane(columns);
    }

    /**
     * Returns all header panes contained in this view, as array.
     *
     * @returns {HeaderPane[]}
     *  All header panes contained in this view.
     */
    getHeaderPanes() {
        return Array.from(this._headerPaneMap.values(), entry => entry.headerPane);
    }

    /**
     * Returns the current width of the row header panes.
     *
     * @returns {number}
     *  The current width of the row header panes, in pixels.
     */
    getHeaderWidth() {
        return this.cornerPane.el.offsetWidth ?? 0;
    }

    /**
     * Returns the current height of the column header panes.
     *
     * @returns {number}
     *  The current height of the column header panes, in pixels.
     */
    getHeaderHeight() {
        return this.cornerPane.el.offsetHeight ?? 0;
    }

    /**
     * Returns the effective font size for the current zoom factor to be used
     * for the column and row labels in all header panes.
     *
     * @returns {number}
     *  The effective font size for all header panes, in points.
     */
    getHeaderFontSize() {
        return math.clamp(math.roundp(5 * this.getZoomFactor() + 6, 0.1), 8, COMPACT_DEVICE ? 12 : 30);
    }

    /**
     * Returns the intersection of the passed column/row intervals with the
     * visible intervals of the header panes in the specified direction.
     *
     * @param {IntervalSource} intervals
     *  An array of index intervals, or a single index interval.
     *
     * @param {boolean} columns
     *  Whether to return the column intervals of the left and right header
     *  panes (`true`), or the row intervals of the top and bottom header panes
     *  (`false`).
     *
     * @returns {IntervalArray}
     *  The parts of the passed column/row intervals which are covered by the
     *  respective visible header panes.
     */
    getVisibleIntervals(intervals, columns) {

        // convert parameter to an array
        intervals = IntervalArray.cast(intervals);

        // pick the intervals of all visible header panes in the correct orientation
        const resultIntervals = new IntervalArray();
        for (const [paneSide, { headerPane }] of this._headerPaneMap) {
            if ((isColumnSide(paneSide) === columns) && headerPane.isVisible()) {
                resultIntervals.append(intervals.intersect(headerPane.getRenderInterval()));
            }
        }

        // merge intervals in case the header panes cover the same sheet area
        return resultIntervals.merge();
    }

    /**
     * Shows a notification alert box, if a corresponding message text exists
     * for the passed message code.
     *
     * @param {YellMessageCode} msgCode
     *  The message code to show a notification alert box for.
     *
     * @param {boolean} [success=false]
     *  If set to true, a success alert box will be shown. By default, an
     *  information alert will be shown.
     *
     * @returns {boolean}
     *  Whether the alert banner has been shown. The return value `false` means
     *  there is already a high-priority alert banner visible, and the new
     *  alert banner does not contain the `priority` flag.
     */
    yellMessage(msgCode, success) {

        // missing edit mode shown via special application method "rejectEditAttempt()"
        if (msgCode === "readonly") {
            return this.docApp.rejectEditAttempt();
        }

        // failed image insertion shown via special application method "rejectEditAttempt()"
        if (msgCode === "image:insert") {
            return this.docApp.rejectEditAttempt("image");
        }

        // resolve error message text, and show an alert banner
        if (msgCode in RESULT_MESSAGES) {
            const type = success ? "success" : "info";
            let message = RESULT_MESSAGES[msgCode];
            if (is.dict(message)) {
                message = message[this.docApp.getFileFormat()];
            }
            if (is.string(message) && message) {
                // do not show the alert in the viewer app
                return this.yell({ type, message, skipPlugged: true });
            }
        }

        return false;
    }

    /**
     * Shows a notification message box, if the passed promise rejects with a
     * message code that has a corresponding message text.
     *
     * @param {PromiseT extends AnyPromise<FT>} promise
     *  A promise representing an asynchronous document model operation. If the
     *  promise will be rejected with an object containing the property "cause"
     *  with a supported message code, an alert box will be shown to the user.
     *
     * @returns {PromiseT}
     *  The promise passed to this method, for convenience.
     */
    yellOnFailure(promise) {

        // show information alert on error
        this.onRejected(promise, result => {
            this.yellMessage(getStringOption(result, "cause", ""));
        });

        // return the promise for convenience
        return promise;
    }

    /**
     * Shows a notification alert to the user, if the document is in read-only
     * mode.
     *
     * @returns {boolean}
     *  Whether modifying the document is posssible.
     */
    requireEditMode() {
        const editMode = this.isEditable();
        if (!editMode) { this.yellMessage("readonly"); }
        return editMode;
    }

    /**
     * Returns the current status text label.
     *
     * @returns {string}
     *  The current status text.
     */
    getStatusLabel() {
        return this._statusText;
    }

    /**
     * Sets the passed text as current status text label, shown in the status
     * pane at the bottom border of the application pane.
     *
     * @param {string} text
     *  The new status text.
     */
    setStatusLabel(text) {
        if (this._statusText !== text) {
            this._statusText = text;
            this.trigger("change:statuslabel", text);
        }
    }

    /**
     * Returns the zoom factor to be used for the active sheet.
     *
     * @returns {number}
     *  The zoom factor for the active sheet.
     */
    /*override*/ getZoomFactor() {
        // overridden from `BaseView` to take zoom correction scale for touch devices into account
        // may be called early, need to check existence of sheet model
        return this.sheetModel?.getEffectiveZoom() ?? 1;
    }

    /**
     * Registers a canvas renderer for all grid panes that will always be
     * invoked after the regular canvas cell contents have been rendered.
     *
     * @param {CanvasBaseRenderer} canvasRenderer
     *  The cancas renderer to be invoked for rendering into the cell canvas.
     */
    registerCanvasRenderer(canvasRenderer) {
        this.member(canvasRenderer);
        for (const gridPane of this.getGridPanes()) {
            gridPane.cellRenderer.registerCanvasRenderer(canvasRenderer);
        }
    }

    // active sheet -----------------------------------------------------------

    /**
     * Returns the collection with all comment threads of the active sheet.
     *
     * @returns {CommentCollection|null}
     *  The collection with all comment threads of the active sheet.
     */
    /*override*/ getCommentCollection() {
        return this.commentCollection;
    }

    /**
     * Converts the passed length in pixels to a length in 1/100 of millimeters,
     * according to the current sheet zoom factor.
     *
     * @param {number} length
     *  The length in pixels.
     *
     * @returns {number}
     *  The converted length in 1/100 of millimeters.
     */
    convertPixelToHmm(length) {
        return this.sheetModel.convertPixelToHmm(length);
    }

    /**
     * Converts the passed length in 1/100 of millimeters to a length in pixels,
     * according to the current sheet zoom factor.
     *
     * @param {number} length
     *  The length in 1/100 of millimeters.
     *
     * @returns {number}
     *  The converted length in pixels.
     */
    convertHmmToPixel(length) {
        return this.sheetModel.convertHmmToPixel(length);
    }

    /**
     * Returns the area of the active sheet, in pixels according to the current
     * sheet zoom factor.
     *
     * @returns {Rectangle}
     *  The area of the active sheet, in pixels.
     */
    getSheetRectangle() {
        return this.sheetModel.getSheetRectangle({ pixel: true });
    }

    /**
     * Returns the location of the passed cell range in the active sheet, in
     * pixels according to the current sheet zoom factor.
     *
     * @param {Range} range
     *  The address of the cell range in the active sheet.
     *
     * @returns {Rectangle}
     *  The location of the cell range in the active sheet, in pixels.
     */
    getRangeRectangle(range) {
        return this.sheetModel.getRangeRectangle(range, { pixel: true });
    }

    /**
     * Returns the location of the specified cell in the active sheet, in
     * pixels according to the current sheet zoom factor.
     *
     * @param {Address} address
     *  The address of the cell in the active sheet.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.expandMerged=false]
     *    If set to `true`, and the cell is part of a merged range, the
     *    location of the entire merged range will be returned, and the address
     *    of the merged range will be stored in the property "mergedRange" of
     *    the result rectangle returned by this method.
     *
     * @returns {Rectangle}
     *  The location of the cell in the active sheet, in pixels.
     */
    getCellRectangle(address, options) {
        return this.sheetModel.getCellRectangle(address, { ...options, pixel: true });
    }

    /**
     * Visits the selection settings of the active sheet for all registered
     * remote users.
     *
     * @yields {RemoteSelectionData}
     *  The remote selections.
     */
    *remoteSelections(type) {

        for (const { userData, userName, colorIndex } of this.propSet.get("remoteClients")) {

            // skip users that have another sheet activated
            if (userData.sheet !== this.activeSheet) { continue; }

            // the array of cell range addresses (may be null)
            let ranges = pick.string(userData, "ranges");
            if (ranges) { ranges = this.docModel.addressFactory.parseRangeList(ranges); }

            // the positions of all selected drawing objects
            let drawings = pick.array(userData, "drawings");
            if (!drawings?.length) { drawings = null; }

            // return a selection descriptor if either ranges or drawings are available
            switch (type) {
                case "cell":
                    if (ranges && !drawings) {
                        yield { userName, colorIndex, ranges };
                    }
                    break;
                case "drawings":
                    if (drawings) {
                        yield { userName, colorIndex, drawings };
                    }
                    break;
            }
        }
    }

    // scrolling --------------------------------------------------------------

    /**
     * Scrolls the active grid pane to the specified cell. In frozen split
     * view, the best fitting grid pane for the passed cell address will be
     * picked instead (see `getTargetGridPane` for details).
     *
     * @param {Address} address
     *  The address of the cell to be scrolled to.
     *
     * @param {ScrollToCellOptions} [options]
     *  Optional parameters.
     */
    scrollToCell(address, options) {

        // the target grid pane to be scrolled (grid pane may change in frozen split mode)
        const gridPane = this.getTargetGridPane(address);

        // set focus to the grid pane containing the address (this will update the "activePane" view attribute)
        if (!options?.preserveFocus) {
            this.grabFocus({ targetAddress: address });
        }

        // scroll the resulting grid pane to the passed cell address
        gridPane.scrollToCell(address);
    }

    /**
     * Scrolls the active grid pane to the specified drawing frame. In frozen
     * split view, scrolls the bottom-right grid pane instead.
     *
     * @param {Position} position
     *  The position of the drawing frame to be scrolled to.
     */
    scrollToDrawingFrame(position) {
        // the resulting grid pane that will be scrolled
        const gridPane = this.sheetModel.hasFrozenSplit() ? this.getVisibleGridPane(PanePos.BR) : this.getActiveGridPane();
        gridPane.scrollToDrawingFrame(position);
    }

    // drawing frames ---------------------------------------------------------

    /**
     * Temporarily shows the DOM drawing frame associated to the specified
     * drawing model. This method counts its invocations internally. The method
     * `tempHideDrawingFrame` will hide the drawing frame after the respective
     * number of invocations.
     *
     * @param {DrawingModel} drawingModel
     *  The drawing model instance to show temporarily.
     */
    tempShowDrawingFrame(drawingModel) {
        this._gridPaneMap.forEach(gridPane => {
            gridPane.drawingRenderer.tempShowDrawingFrame(drawingModel);
        });
    }

    /**
     * Hides the DOM drawing frame associated to the specified drawing model,
     * after it has been made visible temporarily. See `tempShowDrawingFrame`
     * for more details.
     *
     * @param {DrawingModel} drawingModel
     *  The drawing model instance to hide.
     */
    tempHideDrawingFrame(drawingModel) {
        this._gridPaneMap.forEach(gridPane => {
            gridPane.drawingRenderer.tempHideDrawingFrame(drawingModel);
        });
    }

    /**
     * Activates the sheet that contains a comment with unsaved changes.
     *
     * This methods overwites the dummy method defined in `EditView` to
     * implement the specific behaviour for spreadsheets.
     *
     * @returns {OptAsync}
     *  An optional promise that will fulfil when this method has activated the
     *  sheet containing the unsaved comment asynchronously.
     */
    activateUnsavedCommentCollection() {

        // try to find a visible sheet with an unsaved comment
        const sheetIter = this.docModel.yieldSheetModels({ supported: true, visible: true });
        const sheetModel = itr.find(sheetIter, sheetModel => sheetModel.hasUnsavedComment());
        if (!sheetModel) { return undefined; }

        // nothing to do, if sheet is already active
        const sheetIndex = sheetModel.getIndex();
        if (sheetIndex === this.activeSheet) { return undefined; }

        // commit current edit text of cell edit mode ("force" mode without validation,
        // which always returns a fulfilled promise) before activating another sheet
        const promise = this.leaveTextEditMode({ force: true });

        // activate the sheet with the unsaved comment
        return promise.then(() => this.docModel.setActiveSheet(sheetIndex));
    }

    // text edit mode ---------------------------------------------------------

    /**
     * Returns whether any text edit mode, or the specified edit mode, is
     * currently active.
     *
     * @param {string} [editMode]
     *  If specified, this method checks if this edit mode is currently active.
     *  If omitted, this method checks if any edit mode is active.
     *
     * @returns {boolean}
     *  Whether the specified text edit mode is currently active.
     */
    isTextEditMode(editMode) {
        return !!this._activeTextEditor && (!editMode || (this._activeTextEditor.editMode === editMode));
    }

    /**
     * Returns the text editor implementation that is currently active.
     *
     * @param {string} [editMode]
     *  If specified, this method returns the active text editor only if its
     *  type matches. If omitted, this method always returns the active text
     *  editor.
     *
     * @returns {Opt<BaseTextEditor>}
     *  The text editor implementation that is currently active; or `null`, if
     *  text edit mode is currently not active.
     */
    getActiveTextEditor(editMode) {
        return this.isTextEditMode(editMode) ? this._activeTextEditor : undefined;
    }

    /**
     * Starts the specified text edit mode.
     *
     * @param {string} editMode
     *  The identifier of the text edit mode to be started. the following text
     *  edit modes are supported:
     *  - "cell": Starts editing the text contents of the active cell in the
     *    current cell selection of the active sheet.
     *  - "drawing": Starts editing the text contents of the selected drawing
     *    object in the active sheet.
     *
     * @param {object} [options]
     *  Optional parameters. All options will be passed to the text editor
     *  implementation. The supported options depend on the actual
     *  implementation. Additionally, the following optional parameters are
     *  supported:
     *  - {boolean} [options.restart=false]
     *    If set to `true`, and the specified text edit mode is currently
     *    active, it will be canceled and restarted. By default, the active
     *    text edit mode will simply be continued.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the current text edit mode has been
     *  started successfully; or reject, if the implementation has decided to
     *  reject the edit attempt.
     */
    @editLogger.profileMethod("$badge{SpreadsheetView} enterTextEditMode")
    enterTextEditMode(editMode, options) {

        // find the specified text editor implementation
        const textEditor = this._textEditorRegistry.get(editMode);
        if (!textEditor) { return jpromise.reject(); }

        // cancel active text edit mode if restart is requested
        if (options?.restart && (textEditor === this._activeTextEditor)) {
            this._activeTextEditor.cancelEditMode();
        }

        // leave active edit mode, if it is different from the specified edit mode
        const otherActive = this._activeTextEditor && (textEditor !== this._activeTextEditor);
        const editModeLeft = !otherActive || this._activeTextEditor.leaveEditMode();
        if (!editModeLeft) { return jpromise.reject(); }

        // try to start the new text edit mode
        const editModeEntered = textEditor.enterEditMode(options);
        return editModeEntered ? jpromise.resolve() : jpromise.reject();
    }

    /**
     * Leaves the current text edit mode, and commits the pending changes if
     * necessary.
     *
     * @param {object} [options]
     *  Optional parameters. All options will be passed to the text editor
     *  implementation. The supported options depend on the actual
     *  implementation. Additionally, the following optional parameters are
     *  supported:
     *  - {string} [options.editMode]
     *    Only the specified text edit mode will be left. If another text edit
     *    mode is currently active, it will remain active.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the current text edit mode has been
     *  left successfully; or reject, if the implementation has decided to keep
     *  the current edit mode active.
     */
    @editLogger.profileMethod("$badge{SpreadsheetView} leaveTextEditMode")
    leaveTextEditMode(options) {
        const isActive = this.isTextEditMode(options?.editMode);
        const editModeLeft = !isActive || this._activeTextEditor.leaveEditMode(options);
        // DOCS-1961: wait for operations being generated after edit mode (TODO: should be synchronous)
        return editModeLeft ? this.docModel.waitForActionsProcessed() : jpromise.reject();
    }

    /**
     * Cancels the current text edit mode (cell or drawing) immediately,
     * without committing any pending changes.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.editMode]
     *    Only the specified text edit mode will be canceled. If another text
     *    edit mode is currently active, it will remain active.
     */
    cancelTextEditMode(options) {
        const isActive = this.isTextEditMode(options?.editMode);
        if (isActive) { this._activeTextEditor.cancelEditMode(); }
    }

    /**
     * Returns the DOM node intended to contain the browser focus while text
     * edit mode is active.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.editMode]
     *    Only the focus node of the specified text edit mode will be returned.
     *    If another text edit mode is currently active, this method will
     *    return `undefined`.
     *
     * @returns {Opt<HTMLElement>}
     *  The DOM node intended to contain the browser focus while text edit mode
     *  is active; or `undefined`, if no text edit mode is active.
     */
    getTextEditFocusNode(options) {
        const isActive = this.isTextEditMode(options?.editMode);
        return isActive ? this._activeTextEditor.getFocusNode() : undefined;
    }

    // drawing text formatting ------------------------------------------------

    /**
     * Returns the merged formatting attributes provided by the drawing text
     * selection of the text framework.
     *
     * @returns {AttributeSet}
     *  The merged formatting attributes of the text selection engine of the
     *  document.
     */
    getTextAttributeSet() {
        const attrPool = this.docModel.defAttrPool;
        const attrSet = dict.create();
        attrPool.extendAttrSet(attrSet, this.docModel.getAttributes("paragraph"));
        attrPool.extendAttrSet(attrSet, this.docModel.getAttributes("character"));
        return attrSet;
    }

    /**
     * Changes the formatting attributes of the drawing text selection of the
     * text framework.
     *
     * @param {PtAttrSet<AttributeSet>} attrSet
     *  An incomplete attribute set that will be applied to the current text
     *  selection.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the passed attributes have been applied
     *  successfully.
     */
    setTextAttributeSet(attrSet) {

        // drawing objects in locked sheets cannot be modified
        let promise = this.ensureUnlockedDrawings();

        // let the text framework implementation do all the work
        promise = promise.then(() => this.docModel.setAttributes("character", attrSet));

        // show warning alert if needed
        return this.yellOnFailure(promise);
    }

    // protected methods ------------------------------------------------------

    /**
     * Initialization after construction.
     */
    /*protected override*/ async implInitialize() {

        // base class initialization
        await super.implInitialize();

        // handle events of the formula dependency manager (show alerts, etc.)
        const { dependencyManager } = this.docModel;
        // collect the addresses of all formula cells changed locally (for circular reference alert)
        const changedFormulas = new Map/*<string, KeySet<Address>>*/();

        // insert the root node into the application pane
        this.insertContents(this._paneRootEl);
        // keep the text contents of drawing objects in the hidden DOM
        const pageNode = this.docModel.getNode()[0];
        this.insertHiddenNodes(pageNode);

        // insert column/row resize overlay nodes first (will increase its z-index while active)
        const resizeOverlayEl = this._paneRootEl.appendChild(createDiv("abs resize-tracking"));
        const resizeOverlayLeadingEl = resizeOverlayEl.appendChild(createDiv("abs leading"));
        const resizeOverlayTrailingEl = resizeOverlayEl.appendChild(createDiv("abs trailing"));

        // insert the pane nodes into the DOM
        this._paneRootEl.append(this.cornerPane.el);
        this._headerPaneMap.forEach(mapEntry => this._paneRootEl.append(mapEntry.headerPane.el));
        this._gridPaneMap.forEach(gridPane => this._paneRootEl.append(gridPane.el));

        // append the split lines and split tracking nodes
        this._paneRootEl.append(this._colSplitLineEl, this._rowSplitLineEl, this._crossSplitTrackingEl, this._colSplitTrackingEl, this._rowSplitTrackingEl);

        // to save the position from "touchstart", we have to register some global event handlers
        // we need this position to open the context menu on the correct location
        if (_.browser.iOS || _.browser.Android) {
            this.listenToDOMWhenVisible($(document), "touchstart", event => {
                this._lastTouchPos = event.originalEvent.changedTouches[0];
            });
            this.listenToDOMWhenVisible($(document), "touchend", () => {
                // delay, because some contextmenus open only on "touchend" (drawing context menus for example)
                this.setTimeout(() => { this._lastTouchPos = null; }, 100);
            });
        }

        // synchronize view's zoom state with zoom factor of active sheet
        this.listenTo(this.zoomState, "change:state", event => this.sheetModel?.setZoom(event.factor));

        // set focus into first active sheet after import
        this.one("change:activesheet", () => this.grabFocus());

        // update view when document edit mode changes
        this.listenTo(this.docApp, "docs:editmode", editMode => {
            this._paneRootEl.classList.toggle("edit-mode", editMode);
        });

        // application notifies changed data of remote clients
        this.listenTo(this.docApp, "docs:users:selection", activeClients => {
            // filter for remote clients with existing selection data
            const remoteClients = activeClients.filter(client => {
                return client.remote && is.dict(client.userData) && is.number(client.userData.sheet) && is.string(client.userData.ranges);
            });
            // store selections in dynamic properties (this triggers a change event which causes rendering)
            this.propSet.set("remoteClients", remoteClients);
        });

        // set a custom application state during background formula calculation, and show a busy label
        let recalcAttrTimer = null;
        this.listenTo(dependencyManager, "recalc:start", () => {
            this.docApp.setUserState("recalc");
            // root element attribute will be used in automated tests
            this.docApp.setRootAttribute("data-recalc-formulas", true);
            recalcAttrTimer?.abort();
        });
        this.listenTo(dependencyManager, "recalc:end recalc:cancel", () => {
            this.docApp.setUserState(null);
            // defer deleting root element attribute for automated tests
            recalcAttrTimer = this.setTimeout(() => {
                this.docApp.setRootAttribute("data-recalc-formulas", null);
            }, 500);
        });
        this.registerAppStateConfig("recalc", { label: CALCULATING_LABEL, busy: true, delay: 1000 });

        // collect all formula cells that have been changed locally
        this.listenTo(this.docModel, "change:cells", event => {
            if (!event.external) {
                const addrSet = map.upsert(changedFormulas, event.sheetModel.uid, () => new KeySet());
                set.assign(addrSet, event.formulaCells);
            }
        });

        // show a warning alert when reference cycles have been found during calculation
        this.listenTo(dependencyManager, "recalc:end", referenceCycles => {

            // check if the reference cycles cover a formula cell changed locally
            const hasCycle = referenceCycles.some(refCycle => refCycle.some(entry => {
                return !!changedFormulas.get(entry.sheetUid)?.has(entry.address);
            }));
            changedFormulas.clear();

            // show an alert box if a new cycle has been found
            if (hasCycle) {
                this.yell({
                    //#. title for a warning message: a formula addresses its own cell, e.g. =A1 in cell A1
                    headline: gt("Circular Reference"),
                    message: gt("A reference in the formula is dependent on the result of the formula cell."),
                    skipPlugged: true // do not show the alert in the viewer app
                });
            }
        });

        // show an error message (without timeout) if the document contains too many formulas
        this.listenTo(dependencyManager, "recalc:overflow", () => {
            this.yell({
                type: "error",
                //#. The number of spreadsheet formulas is limited in the configuration. If a document exceeds this limit, the user will see this alert.
                message: gt("This document exceeds the spreadsheet size and complexity limits. Calculation has been stopped. Formula results are not updated anymore."),
                skipPlugged: true // do not show the alert in the viewer app
            });
        }, { once: true });

        // show warning message if the document contains any formulas with unimplemented functions
        this.listenTo(dependencyManager, "recalc:end", () => {

            // the formula grammar needed to receive translated function names
            const grammar = this.docModel.formulaGrammarUI;
            // the resource keys of all unimplemented functions occurring in the document
            const funcKeys = dependencyManager.getMissingFunctionKeys();
            // the translated function names of all unsupported functions
            let funcNames = funcKeys.map(grammar.getFunctionName, grammar).filter(Boolean).sort();

            // show a warning message if the document contains any unsupported functions
            if (funcNames.length > 0) {
                funcNames = funcNames.map(funcName => `\n\xa0\u2022\xa0${funcName}`).join();
                this.yell({
                    type: "warning",
                    //#. A list with all unsupported functions will be shown below this message.
                    message: _.noI18n(gt("This spreadsheet contains formulas with unsupported functions:") + funcNames),
                    duration: 30000,
                    skipPlugged: true // do not show the alert in the viewer app
                });
            }
        }, { once: true });

        // handle changed active sheet
        this.listenToProp(this.docModel.propSet, "activeSheet", activeSheet => {
            this._triggerChangedActiveSheet(activeSheet);
        });

        // emit additional "change:selection(:prepare)" events for "change:sheetprops" event
        this.listenTo(this.docModel, "change:sheetprops", event => {
            if (event.sheetModel === this.sheetModel) {
                const newSel = event.newProps.selection;
                const oldSel = event.oldProps.selection;
                if (newSel) { this.trigger("change:selection:prepare", newSel, oldSel, false); }
                this.trigger("change:sheetprops", event);
                if (newSel) { this.trigger("change:selection", newSel, oldSel, false); }
            }
        });

        // foward all change events in the active sheet based on `SheetEventBase`
        for (const eventType of SHEET_FORWARD_EVENTS) {
            if (eventType !== "change:sheetprops") {
                this.listenTo(this.docModel, eventType, event => {
                    if (event.sheetModel === this.sheetModel) {
                        this.trigger(eventType, event);
                    }
                });
            }
        }

        // generate "update:formulas" events for renamed sheets (before renaming the sheet!)
        this.listenTo(this.docModel, "rename:sheet:before", event => {
            this.trigger("update:formulas", new RenameSheetUpdateTask(event.sheet, event.sheetName));
        });

        // generate "update:formulas" events for deleted sheets (before deleting the sheet!)
        this.listenTo(this.docModel, "delete:sheet:before", event => {
            this.trigger("update:formulas", new DeleteSheetUpdateTask(event.sheet));
        });

        // generate "update:formulas" events for relabed defined names
        this.listenTo(this.docModel, "change:name:before", event => {
            if (event.label) {
                this.trigger("update:formulas", new RelabelNameUpdateTask(event.sheet, event.label[0], event.label[1]));
            }
        });

        // generate "update:formulas" events for renamed table ranges
        this.listenTo(this.docModel, "change:table:before", event => {
            if (event.name) {
                this.trigger("update:formulas", new RenameTableUpdateTask(event.name[0], event.name[1]));
            }
        });

        // generate "update:formulas" events for moved columns/rows/cells
        this.listenTo(this.docModel, "move:cells", event => {
            this.trigger("update:formulas", new MoveCellsUpdateTask(event.sheet, event.transformer));
        });

        // delete rendering cache of deleted sheets
        this.listenTo(this.docModel, "delete:sheet:before", event => {
            map.remove(this._renderCacheMap, event.sheetModel)?.destroy();
        });

        // register GUI event handlers after successful import
        this.waitForImportSuccess(() => {

            // register the context menu handler
            // DOCS-1573 - if plugged by the Viewer, don't register on document to preserve Viewer's browser context menu
            const targetNode = this.docApp.isViewerMode() ? this.docApp.getRootNode() : document;
            this.listenToDOMWhenVisible($(targetNode), "contextmenu", this._globalContextMenuHandler);

            // initialize tracking observer for split lines
            this._initSplitTracking();

            // handle generic key events (use JQuery object to be able to return `false` from handlers)
            this.listenTo($(this._paneRootEl), "keydown", this._keyDownHandler);
            this.listenTo($(this._paneRootEl), "keypress", this._keyPressHandler);

            // jump to a comment thread specified in launch options
            const commentId = this.docApp.getLaunchOption("comment_id");
            if (is.string(commentId) && commentId) {
                const timestamp = parseInt(commentId, 10);
                for (const sheetModel of this.docModel.yieldSheetModels()) {
                    const targetModel = sheetModel.commentCollection.getCommentModels().find(
                        threadModel => new Date(threadModel.getDate()).getTime() === timestamp
                    );
                    if (targetModel) {
                        this.docModel.setActiveSheet(sheetModel.getIndex());
                        this.commentsPane.selectThread(targetModel);
                        break;
                    }
                }
            }
        });

        // hide everything, if import fails
        this.waitForImportFailure(() => {
            this._paneRootEl.classList.add("hidden");
            this.statusPane.hide();
            this.commentsPane.hide();
        });

        // create the additional view panes
        this.formulaPane = this.addPane(new FormulaPane(this));
        this.statusPane = this.addPane(new StatusPane(this));
        this.commentsPane = this.addPane(new CommentsPane(this, gt("There are no comments on this sheet.")));

        // repaint all grid and header panes when the size of the root node has changed
        let rootNodeSize = { width: 0, height: 0 };
        this.on("refresh:layout", () => {
            const newSize = { width: this._paneRootEl.clientWidth, height: this._paneRootEl.clientHeight };
            if ((rootNodeSize.width !== newSize.width) || (rootNodeSize.height !== newSize.height)) {
                rootNodeSize = newSize;
                this._requestInitializePanes();
            }
        });

        // update locked state of active sheet in the DOM
        this.on("change:activesheet change:sheetattrs", () => {
            this._paneRootEl.classList.toggle("sheet-locked", this.sheetModel.isLocked());
            this._paneRootEl.classList.toggle("cell-sheet", this.sheetModel.isCellType);
        });

        // update global settings according to view attributes of active sheet
        this.on("change:sheetprops", ({ newProps }) => {
            if (dict.someKey(newProps, key => key.startsWith("split"))) {
                // split/freeze settings have changed, refresh all panes
                this._requestInitializePanes({ force: true });
            } else if (dict.someKey(newProps, key => key.startsWith("anchor"))) {
                // update visible column/row interval immediately while scrolling, but debounce multiple anchor events,
                // e.g. when scrolling in X and Y direction at the same time (via `GridPane::scrollToRectangle`)
                this._updateRenderingLayersDebounced();
            }
        });

        // full initialization of all panes when changing the browser zoom
        this.listenToGlobal("change:resolution", () => {
            this._requestInitializePanes({ force: true });
        });

        // hide cell selection while drawings are selected
        this.on("change:selection", selection => {
            this._paneRootEl.classList.toggle("drawing-selection", selection.drawings.length > 0);
        });

        // repaint all comment threads when switching sheets
        this.on("change:activesheet", () => {
            this.commentsPane.repaintAllComments();
        });

        // update attribute marker for text edit modes
        this.on("textedit:enter", editMode => {
            this._paneRootEl.dataset.textEdit = editMode;
        });

        // restore the correct text framework selection after leaving drawing edit mode
        this.on("textedit:leave", editMode => {
            delete this._paneRootEl.dataset.textEdit;
            if (editMode === "drawing") { this.insertHiddenNodes(pageNode); }
        });

        // repaint moved comments in comments pane
        this.on("move:comments", event => {
            this.commentsPane.moveComments(event.commentModels);
        });

        // process events triggered by header panes
        this._headerPaneMap.forEach(mapEntry => {

            const headerPane = mapEntry.headerPane;
            const columns = isColumnSide(mapEntry.paneSide);
            const offsetAttr = columns ? "left" : "top";
            const sizeAttr = columns ? "width" : "height";
            let showResizeTimer = null;

            const showResizerOverlayNode = (offset, size) => {
                resizeOverlayEl.dataset.orientation = columns ? "columns" : "rows";
                updateResizerOverlayNode(offset, size);
                // Delay displaying the overlay nodes, otherwise they cover the resizer drag
                // nodes of the header pane which will interfere with double click detection.
                showResizeTimer = this.setTimeout(() => this._paneRootEl.classList.add("tracking-active"), 200);
            };

            const updateResizerOverlayNode = (offset, size) => {
                const relativeOffset = mapEntry.offset + offset - headerPane.getVisiblePosition().offset;
                resizeOverlayEl.classList.toggle("collapsed", size === 0);
                setElementStyle(resizeOverlayLeadingEl, sizeAttr, Math.max(0, relativeOffset) + 10);
                setElementStyle(resizeOverlayTrailingEl, offsetAttr, relativeOffset + size);
            };

            const hideResizerOverlayNode = () => {
                showResizeTimer?.abort();
                this._paneRootEl.classList.remove("tracking-active");
                setElementStyle(resizeOverlayLeadingEl, sizeAttr, null);
                setElementStyle(resizeOverlayTrailingEl, offsetAttr, null);
            };

            // visualize resize tracking from header panes
            this.listenTo(headerPane, "resize:start", (offset, size) => showResizerOverlayNode(offset, size));
            this.listenTo(headerPane, "resize:move", (offset, size) => updateResizerOverlayNode(offset, size));
            this.listenTo(headerPane, "resize:end", hideResizerOverlayNode);

            // forward the "change:scrollpos" event with leading "paneSide" parameter
            this.listenTo(headerPane, "change:scrollpos", scrollPos => {
                this.trigger("change:scrollpos", mapEntry.paneSide, scrollPos);
            });
        });
    }

    /*protected override*/ implInitToolPanes() {
        super.implInitToolPanes();

        this.registerToolPane("format",       this._initFormatToolPane,       { label: FORMAT_HEADER_LABEL,       visibleKey: "view/toolpane/format/visible",       priority: 1 });
        this.registerToolPane("font",         this._initFontToolPane,         { label: FONT_HEADER_LABEL,         visibleKey: "view/toolpane/font/visible",         priority: 1 });
        this.registerToolPane("alignment",    this._initAlignmentToolPane,    { label: ALIGNMENT_HEADER_LABEL,    visibleKey: "view/toolpane/alignment/visible",    priority: 1 });
        this.registerToolPane("cell",         this._initCellToolPane,         { label: CELL_HEADER_LABEL,         visibleKey: "view/toolpane/cell/visible",         priority: 1 });
        this.registerToolPane("numberformat", this._initNumberFormatToolPane, { label: NUMBERFORMAT_HEADER_LABEL, visibleKey: "view/toolpane/numberformat/visible", priority: 1 });
        this.registerToolPane("data",         this._initDataToolPane,         { label: DATA_HEADER_LABEL,         visibleKey: "view/toolpane/data/visible",         priority: 1 });
        this.registerToolPane("insert",       this._initInsertToolPane,       { label: INSERT_HEADER_LABEL,       visibleKey: "view/toolpane/insert/visible",       priority: 1 });
        this.registerToolPane("colrow",       this._initColRowToolPane,       { label: COL_ROW_HEADER_LABEL,      visibleKey: "view/toolpane/colrow/visible",       priority: 1 });
        this.registerToolPane("table",        this._initTableToolPane,        { label: TABLE_HEADER_LABEL,        visibleKey: "view/toolpane/table/visible" });
        this.registerToolPane("drawing",      this._initDrawingToolPane,      { labelKey: "drawing/type/label",   visibleKey: "view/toolpane/drawing/visible" });
        this.registerToolPane("comments",     this._initCommentsToolPane,     { label: REVIEW_HEADER_LABEL,       visibleKey: "view/toolpane/comments/visible",     priority: 1 });
    }

    /*protected override*/ implInitViewMenu(viewButton) {
        super.implInitViewMenu(viewButton);

        viewButton.addSection("split", /*#. menu title: settings for splits and frozen columns/rows in a spreadsheet */ gt.pgettext("sheet-split", "Split and freeze"));
        viewButton.addControl("view/split/dynamic", new DynamicSplitCheckBox());
        viewButton.addControl("view/split/frozen",  new FrozenSplitCheckBox(this));

        viewButton.addSection("options", OPTIONS_LABEL);
        // note: we do not set aria role to "menuitemcheckbox" or "button" due to Safari just working correctly with "checkox". CheckBox constructor defaults aria role to "checkbox"
        viewButton.addControl("view/toolpane/show",     new CheckBox(SHOW_TOOLBARS_CHECKBOX_OPTIONS),     { visibleKey: "view/toolpane/show" });
        viewButton.addControl("view/commentspane/show", new CheckBox(SHOW_COMMENTSPANE_CHECKBOX_OPTIONS), { visibleKey: "document/ooxml" });
        viewButton.addControl("document/users",         new CheckBox(SHOW_COLLABORATORS_CHECKBOX_OPTIONS));
        viewButton.addControl("view/formulapane/show",  new CheckBox({ label: /*#. check box label: show/hide formula-bar */ gt("Show formula bar"), tooltip: gt("Show or hide the formula bar") }));
        viewButton.addControl("view/grid/show",         new CheckBox({ label: /*#. check box label: show/hide cell grid in the sheet */ gt("Show grid lines"), tooltip: gt("Show or hide the cell grid lines in the current sheet") }));
        viewButton.addControl("view/statuspane/show",   new CheckBox({ label: /*#. check box label: show/hide the sheet tabs at the bottom border */ gt("Show sheet tabs"), tooltip: gt("Show or hide the sheet tabs below the sheet area") }));
    }

    // private methods --------------------------------------------------------

    /*private*/ _createFontStyleToolBar() {
        // TODO: bug 64725: "hideTextPosition" can be removed if Spreadsheet supports vertical text position (subscript/superscript)
        return new FontStyleToolBar(this, { hideTextPosition: true });
    }

    /*private*/ _initFormatToolPane() {
        this.insertToolBar(new FontFamilyToolBar(this));
        this.insertToolBar(this._createFontStyleToolBar());
        this.insertToolBar(new CellFormatPainterToolBar(this));
        this.insertToolBar(new CellColorToolBar(this));
        this.insertToolBar(new CellAlignmentToolBar(this));
        this.insertToolBar(new NumberFormatToolBar(this));
        this.insertToolBar(new CellBorderToolBar(this));
        this.insertToolBar(new CellStyleToolBar(this));
        this.insertToolBar(new BoxAlignmentToolBar(this), { visibleKey: "view/selection/drawing" });
        this.insertToolBar(new ListStyleToolBar(this),    { visibleKey: "view/selection/drawing" });
    }

    /*private*/ _initFontToolPane() {
        this.insertToolBar(new FontFamilyToolBar(this));
        this.insertToolBar(this._createFontStyleToolBar());
        const colorToolBar = this.createToolBar();
        colorToolBar.addControl("character/color", new TextColorPicker(this));
    }

    // "Alignment" tab with cell alignment controls, and merge picker
    /*private*/ _initAlignmentToolPane() {
        this.insertToolBar(new CellAlignmentToolBar(this));
    }

    // "Cell" tab with cell fill color (but not text color), and cell border controls
    /*private*/ _initCellToolPane() {
        const cellToolBar = this.createToolBar();
        cellToolBar.addControl("cell/fillcolor", new CellFillColorPicker(this));
        this.insertToolBar(new CellBorderToolBar(this));
    }

    // "Number format" tab with all number formatting controls
    /*private*/ _initNumberFormatToolPane() {
        this.insertToolBar(new NumberFormatToolBar(this));
    }

    // data manipulation (filter/sorting, named ranges)
    /*private*/ _initDataToolPane() {
        const dataToolBar = this.createToolBar();
        if (COMBINED_TOOL_PANES) {
            dataToolBar.addSection("formulas");
            dataToolBar.addControl("cell/autoformula",       new InsertAutoSumButton());
            dataToolBar.addControl("function/insert/dialog", new InsertFunctionButton());
        }
        dataToolBar.addSection("sort");
        dataToolBar.addControl("table/sort", new SortMenuButton(this));
        dataToolBar.addSection("filter");
        dataToolBar.addControl("table/filter", new FilterButton());
        dataToolBar.addControl("table/refresh", new RefreshButton());
        if (!COMBINED_TOOL_PANES) {
            dataToolBar.addSection("names");
            dataToolBar.addControl("view/namesmenu/toggle", new NamesMenuButton(this));
        }
        dataToolBar.addSection("protect");
        dataToolBar.addControl("sheet/operation/unlocked/cell", new CellProtectionMenuButton(this));
    }

    // "Insert" tab with controls for cell contents (auto-sum etc.), and drawings
    /*private*/ _initInsertToolPane() {
        this.insertToolBar(new InsertContentToolBar(this));
        this.insertToolBar(new InsertDrawingToolBar(this));
        this.insertToolBar(new InsertCommentToolBar(this));
    }

    // "Column/row" tab for column and row operations
    /*private*/ _initColRowToolPane() {
        this.insertToolBar(new ColRowToolBar(this, false));
        this.insertToolBar(new ColRowToolBar(this, true));
    }

    // "Table" tab for table ranges (filter/sorting)
    /*private*/ _initTableToolPane() {
        this.insertToolBar(new TableToolBar(this));
    }

    // "Drawing" tab for drawing objects
    /*private*/ _initDrawingToolPane() {

        const drawingToolBar = this.createToolBar();
        drawingToolBar.addControl("shape/insert",   new ShapeTypePicker(this, { label: null }), { visibleKey: "drawing/type/shape" });
        drawingToolBar.addControl("drawing/delete", new Button(DELETE_DRAWING_BUTTON_OPTIONS));

        if (!COMBINED_TOOL_PANES) {

            const drawingConnectorToolBar = this.createToolBar({ visibleKey: "view/toolbar/drawing/connector/visible" });
            drawingConnectorToolBar.addControl("drawing/border/style",     new DrawingLineStylePicker(this, { line: true }));
            drawingConnectorToolBar.addControl("drawing/border/color",     new DrawingLineColorPicker(this, { line: true }));
            drawingConnectorToolBar.addControl("drawing/connector/arrows", new DrawingArrowPresetPicker());

            const drawingBorderToolBar = this.createToolBar({ visibleKey: "view/toolbar/drawing/border/visible" });
            drawingBorderToolBar.addControl("drawing/border/style", new DrawingLineStylePicker(this));
            drawingBorderToolBar.addControl("drawing/border/color", new DrawingLineColorPicker(this));

            const drawingFillToolBar = this.createToolBar({ visibleKey: "view/toolbar/drawing/fill/visible", shrinkHide: true });
            drawingFillToolBar.addControl("drawing/fill/color", new DrawingFillColorPicker(this));

            const chartToolBar = this.createToolBar({ visibleKey: "drawing/chart" });
            chartToolBar.addSection("type");
            chartToolBar.addControl("drawing/charttype", new ChartTypePicker(this));
            chartToolBar.addContainer();
            chartToolBar.addControl("drawing/chartlabels", new Button({ "aria-owns": this.chartLabelsMenu.uid, ...CHART_LABELS_BUTTON_OPTIONS }));
            chartToolBar.addSection("style");
            chartToolBar.addControl("drawing/chartcolorset", new ChartColorSetPicker(this));
            chartToolBar.addControl("drawing/chartstyleset", new ChartStyleSetPicker(this));

            const chartSeriesToolBar = this.createToolBar({ visibleKey: "drawing/chart", shrinkHide: true });
            const pointsMenu = chartSeriesToolBar.addControl("view/chart/datapoints/menu", new CompoundButton(this, CHART_DATA_POINTS_BUTTON_OPTIONS));
            pointsMenu.addControl("drawing/chartdatalabel", new CheckBox(CHART_SHOW_POINT_LABELS_BUTTON_OPTIONS));
            pointsMenu.addControl("drawing/chartvarycolor", new CheckBox(CHART_VARY_POINT_COLORS_BUTTON_OPTIONS));

            const chartLegendToolBar = this.createToolBar({ visibleKey: "drawing/chart", shrinkHide: true });
            chartLegendToolBar.addControl("drawing/chartlegend/pos", new ChartLegendPicker(this));

            const chartSourceToolBar = this.createToolBar({ visibleKey: "drawing/chart", shrinkHide: true });
            const dataSourceMenu = chartSourceToolBar.addControl("drawing/chartdatasource", new CompoundButton(this, { label: /*#. menu title: options to modify the data source of a chart object */ gt.pgettext("chart-source", "Data source") }));
            dataSourceMenu.addControl("drawing/chartsource",   new Button({ label: /*#. change source data for a chart object in a spreadsheet */ gt.pgettext("chart-source", "Edit data references") }));
            dataSourceMenu.addControl("drawing/chartexchange", new Button({ label: /*#. switch orientation of data series in rectangular source data of a chart object */ gt.pgettext("chart-source", "Switch rows and columns") }));
            dataSourceMenu.addControl("drawing/chartfirstrow", new Button({ toggle: true, label: /*#. decide if first row of source data is handled as label (headline) or as contentvalues */ gt.pgettext("chart-source", "First row as label") }));
            dataSourceMenu.addControl("drawing/chartfirstcol", new Button({ toggle: true, label: /*#. decide if first column of source data is handled as label or as contentvalues */ gt.pgettext("chart-source", "First column as label") }));
        }

        const drawingOrderToolBar = this.createToolBar();
        // DOCS-3802 for disabling the rotation controls for ODS files, DOCS-2505 for enabling it again in 8.1
        drawingOrderToolBar.addControl("drawing/order", new DrawingArrangementPicker(this, { rotationControls: this.docApp.isOOXML() }));
    }

    /*private*/ _initCommentsToolPane() {
        this.insertToolBar(new CommentsToolBar(this), { visibleKey: "document/ooxml" });
        this.insertToolBar(new NotesToolBar(this),    { visibleKey: "note/available/any" });
    }

    /**
     * Updates the visibility, position, and size of all header panes and grid
     * panes.
     *
     * @param {boolean} [onRowDigitsOnly=false]
     *  If set to `true`, the header panes and grid panes will be initialized
     *  only, if the number of digits needed to show the row numbers (and thus
     *  the width of the row header panes) will change.
     */
    @renderLogger.profileMethod("$badge{SpreadsheetView} updateHeaderAndGridPanes")
    /*private*/ _updateHeaderAndGridPanes(onRowDigitsOnly) {

        // calculate the digit count according to the maximum row number of visible row header panes
        const rowDigitCount = itr.reduce(this._headerPaneMap.values(), 3, (digitCount, mapEntry) => {
            const interval = mapEntry.headerPane.getRenderInterval();
            if (interval && !isColumnSide(mapEntry.paneSide)) {
                digitCount = Math.max(digitCount, String(interval.last + 1).length);
            }
            return digitCount;
        });

        // update the cached row digit count, return the changed state
        const countChanged = this._rowDigitCount !== rowDigitCount;
        this._rowDigitCount = rowDigitCount;

        // return early, if nothing needs to be updated
        if (!countChanged && onRowDigitsOnly) { return; }

        // whether frozen split mode is active
        const frozenSplit = this.sheetModel.hasFrozenSplit();
        // whether dynamic split mode is really active
        const dynamicSplit = !frozenSplit && this.sheetModel.hasSplit();
        // the size of the split lines
        const splitLineSize = frozenSplit ? FROZEN_SPLIT_SIZE : DYNAMIC_SPLIT_SIZE;
        // start position of the split lines
        let splitLineLeft = this.sheetModel.getSplitWidth();
        let splitLineTop = this.sheetModel.getSplitHeight();

        // expand dynamic splits to minimum split size
        if (dynamicSplit) {
            if (splitLineLeft > 0) { splitLineLeft = Math.max(splitLineLeft, MIN_PANE_SIZE); }
            if (splitLineTop > 0) { splitLineTop = Math.max(splitLineTop, MIN_PANE_SIZE); }
        }

        // whether the left and top panes are visible
        const visibleSides = {};
        visibleSides.left = (dynamicSplit || frozenSplit) && (splitLineLeft > 0);
        visibleSides.top = (dynamicSplit || frozenSplit) && (splitLineTop > 0);

        // calculate current size of header nodes
        this.cornerPane.initializePaneLayout(rowDigitCount);
        const headerWidth = this.getHeaderWidth();
        const headerHeight = this.getHeaderHeight();

        // calculate inner width of left panes
        const leftEntry = this._headerPaneMap.get(PaneSide.L);
        if (visibleSides.left) {
            leftEntry.offset = headerWidth;
            leftEntry.size = splitLineLeft;
            leftEntry.hiddenSize = frozenSplit ? this.colCollection.convertScrollAnchorToPixel(this.sheetPropSet.get("anchorLeft")) : 0;
        } else {
            leftEntry.offset = leftEntry.size = leftEntry.hiddenSize = 0;
        }

        // calculate inner height of top panes
        const topEntry = this._headerPaneMap.get(PaneSide.T);
        if (visibleSides.top) {
            topEntry.offset = headerHeight;
            topEntry.size = splitLineTop;
            topEntry.hiddenSize = frozenSplit ? this.rowCollection.convertScrollAnchorToPixel(this.sheetPropSet.get("anchorTop")) : 0;
        } else {
            topEntry.offset = topEntry.size = topEntry.hiddenSize = 0;
        }

        // calculate effective position of split lines
        splitLineLeft = leftEntry.offset + leftEntry.size;
        splitLineTop = topEntry.offset + topEntry.size;

        // determine whether right and bottom panes are visible (must have enough room in split and frozen mode)
        visibleSides.right = !visibleSides.left || (splitLineLeft + splitLineSize + MIN_PANE_SIZE + SCROLLBAR_WIDTH <= this._paneRootEl.offsetWidth);
        visibleSides.bottom = !visibleSides.top || (splitLineTop + splitLineSize + MIN_PANE_SIZE + SCROLLBAR_HEIGHT <= this._paneRootEl.offsetHeight);

        // visibility of the split lines
        const colSplit = visibleSides.left && visibleSides.right;
        const rowSplit = visibleSides.top && visibleSides.bottom;

        // calculate the resulting grid pane positions and sizes of the right panes
        const rightEntry = this._headerPaneMap.get(PaneSide.R);
        if (visibleSides.right) {
            rightEntry.offset = colSplit ? (splitLineLeft + splitLineSize) : headerWidth;
            rightEntry.size = this._paneRootEl.offsetWidth - rightEntry.offset;
            rightEntry.hiddenSize = frozenSplit ? (leftEntry.hiddenSize + leftEntry.size) : 0;
        } else if (visibleSides.left) {
            leftEntry.size = this._paneRootEl.offsetWidth - headerWidth;
            rightEntry.offset = rightEntry.size = rightEntry.hiddenSize = 0;
        }

        // calculate the resulting grid pane positions and sizes of the bottom panes
        const bottomEntry = this._headerPaneMap.get(PaneSide.B);
        if (visibleSides.bottom) {
            bottomEntry.offset = rowSplit ? (splitLineTop + splitLineSize) : headerHeight;
            bottomEntry.size = this._paneRootEl.offsetHeight - bottomEntry.offset;
            bottomEntry.hiddenSize = frozenSplit ? (topEntry.hiddenSize + topEntry.size) : 0;
        } else if (visibleSides.top) {
            topEntry.size = this._paneRootEl.offsetHeight - headerHeight;
            bottomEntry.offset = bottomEntry.size = bottomEntry.hiddenSize = 0;
        }

        // set frozen mode (left/top panes are not scrollable in frozen mode in their
        // own direction, e.g. left frozen panes are not scrollable to left/right)
        leftEntry.frozen = topEntry.frozen = frozenSplit;
        rightEntry.frozen = bottomEntry.frozen = false;

        // set up scroll bar visibility in the opposite direction (e.g. left/right
        // scroll bars of top panes are hidden, if bottom panes are visible)
        leftEntry.showOppositeScroll = !visibleSides.right;
        topEntry.showOppositeScroll = !visibleSides.bottom;
        rightEntry.showOppositeScroll = bottomEntry.showOppositeScroll = true;

        // initialize the header panes
        for (const mapEntry of this._headerPaneMap.values()) {
            mapEntry.headerPane.initializePaneLayout(mapEntry);
        }

        // initialize the grid panes
        for (const [panePos, gridPane] of this._gridPaneMap) {
            const colLayout = this._headerPaneMap.get(getColPaneSide(panePos));
            const rowLayout = this._headerPaneMap.get(getRowPaneSide(panePos));
            gridPane.initializePaneLayout(colLayout, rowLayout);
        }

        // visibility and position of the split lines
        initLineElement(this._colSplitLineEl, colSplit, { left: splitLineLeft, width: splitLineSize });
        initLineElement(this._rowSplitLineEl, rowSplit, { top: splitLineTop, height: splitLineSize });

        // visibility and position of the split tracking nodes
        const trackingLeft = splitLineLeft - TRACKING_OFFSET;
        const trackingTop = splitLineTop - TRACKING_OFFSET;
        initLineElement(this._colSplitTrackingEl, dynamicSplit && colSplit, { left: trackingLeft });
        initLineElement(this._rowSplitTrackingEl, dynamicSplit && rowSplit, { top: trackingTop });
        initLineElement(this._crossSplitTrackingEl, dynamicSplit && colSplit && rowSplit, { left: trackingLeft, top: trackingTop });
    }

    /**
     * Updates the layer intervals in all header panes, passes the changed
     * layer intervals to the rendering cache, and forwards the new layer
     * ranges received from the rendering cache to all grid panes.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.initialize=false]
     *    Initialize the visibility, location, and size of all header panes and
     *    grid panes.
     *  - {boolean} [options.force=false]
     *    If set to `true`, the layer intervals will always be recalculated
     *    regardless of the current scroll position. In difference to the
     *    option "full", this option does not cause a full repaint.
     *  - {boolean} [options.hide=false]
     *    If set to `true`, all panes will be switched to hidden state.
     */
    @renderLogger.profileMethod("$badge{SpreadsheetView} updateRenderingLayers")
    /*private*/ _updateRenderingLayers(options) {

        // initialize the location and size of all header panes and grid panes
        this._updateHeaderAndGridPanes(!options?.initialize);

        // update the layer intervals of all header panes
        const headerBoundaryMap = new Map/*<PaneSide, HeaderBoundarySpec>*/();
        for (const mapEntry of this._headerPaneMap.values()) {
            const boundary = mapEntry.headerPane.updateLayerInterval(options);
            if (boundary) { headerBoundaryMap.set(mapEntry.paneSide, boundary); }
        }

        // after the layer intervals have changed, the size of the row header panes may need to be updated again
        this._updateHeaderAndGridPanes(true);

        // nothing to do without changed intervals
        if (!headerBoundaryMap.size) { return; }

        // pass all layer boundaries to the rendering cache, result contains all changed layer ranges
        const layerBoundaryMap = this.renderCache.updatePaneBoundaries(headerBoundaryMap);

        // distribute all changed layer ranges to the grid panes
        for (const [panePos, boundary] of layerBoundaryMap) {
            this._gridPaneMap.get(panePos).setLayerRange(boundary, options);
        }
    }

    @debounceMethod({ delay: "microtask" })
    /*private*/ _updateRenderingLayersDebounced() {
        this._updateRenderingLayers();
    }

    /**
     * Refreshes the visibility, position, and size of all header panes, grid
     * panes, and tracking nodes.
     *
     * @param {object} [options]
     *  Optional parameters:
     *    recalculated and repainted (e.g. when zooming).
     *  - {boolean} [options.force=false]
     *    If set to `true`, the layer intervals will always be recalculated
     *    regardless of the current scroll position. In difference to the
     *    option "full", this option does not cause a full repaint.
     *  - {boolean} [options.hide=false]
     *    If set to `true`, all panes will be switched to hidden state.
     */
    /*private*/ _requestInitializePanes(options) {

        // collect passed options (no spread operator: `true` wins over `false` but not vice versa)
        const paneOptions = this._initPaneOptions;
        paneOptions.force ||= options?.force;
        paneOptions.hide ||= options?.hide;

        // ignore updates of active pane caused by focus changes during pane initialization
        this._panesInitialized = false;

        // execute initialization debounced
        this._initializePanesDebounced();
    }

    /**
     * Debounced initialization of header and grid panes, waiting for document
     * actions/operations.
     */
    @debounceAfterActionsMethod()
    @renderLogger.profileMethod("$badge{SpreadsheetView} initializePanesDebounced")
    /*private*/ _initializePanesDebounced() {

        // view layout updates may be requested in case the import has failed, and no sheet is available
        if (!this.sheetModel) { return; }

        // whether the focus is located in the grid panes
        const hasFocus = this.hasAppFocus();

        // update layer intervals and ranges, and the rendering cache
        this._initPaneOptions.initialize = true;
        this._updateRenderingLayers(this._initPaneOptions);

        // reset cached settings for next debounced invocations
        this._initPaneOptions = dict.create();
        this._panesInitialized = true;

        // update focus (active grid pane may have been changed)
        if (hasFocus) { this.grabFocus(); }
    }

    /**
     * Updates internal settings after changing the active sheet, and triggers
     * the events "before:activesheet", and "change:activesheet".
     */
    /*private*/ _triggerChangedActiveSheet(activeSheet) {

        // nothing to do if the sheet index is not valid (e.g. initially after import,
        // or during sheet operations such as inserting or deleting a sheet)
        if (this.activeSheet >= 0) {
            // cancel text edit mode before switching sheets
            this.cancelTextEditMode();
            // notify listeners with the index of the old active sheet
            this.trigger("before:activesheet", this.activeSheet);
            // lock the rendering cache of the old sheet (no cache updates on model changes)
            this.renderCache.lockCache();
        }

        // initialize the view according to the new active sheet, and notify listeners
        // (nothing to do when invalidating the active sheet temporarily, e.g. during
        // sheet operations such as inserting or deleting a sheet)
        if (activeSheet >= 0) {

            // get model instance and collections of the new active sheet
            this.activeSheet = activeSheet;
            this.sheetModel = this.docModel.getSheetModel(activeSheet);
            this.sheetPropSet = this.sheetModel.propSet;
            this.colCollection = this.sheetModel.colCollection;
            this.rowCollection = this.sheetModel.rowCollection;
            this.cellCollection = this.sheetModel.cellCollection;
            this.mergeCollection = this.sheetModel.mergeCollection;
            this.hlinkCollection = this.sheetModel.hlinkCollection;
            this.dvRuleCollection = this.sheetModel.dvRuleCollection;
            this.cfRuleCollection = this.sheetModel.cfRuleCollection;
            this.tableCollection = this.sheetModel.tableCollection;
            this.drawingCollection = this.sheetModel.drawingCollection;
            this.noteCollection = this.sheetModel.noteCollection;
            this.commentCollection = this.sheetModel.commentCollection;

            // synchronize view's zoom state with zoom factor of active sheet
            this.zoomState.set("fixed", this.sheetModel.getZoom(), { silent: true });

            // create the rendering cache for the new active sheet
            this.renderCache = map.upsert(this._renderCacheMap, this.sheetModel, () => {
                const renderCache = new RenderCache(this.sheetModel);
                // forward the "cache:ready" event to cell renderers
                renderCache.on("cache:ready", renderRanges => {
                    if (renderCache.sheetModel === this.sheetModel) {
                        renderLogger.trace("$badge{SpreadsheetView} $event{cache:ready}");
                        this.trigger("rendercache:ready", renderRanges);
                    }
                });
                // refresh header/grid panes if rendering caches have changed
                renderCache.on("cache:dirty", () => {
                    if (renderCache.sheetModel === this.sheetModel) {
                        renderLogger.trace("$badge{SpreadsheetView} $event{cache:dirty}");
                        this._requestInitializePanes({ force: true });
                    }
                });
                return renderCache;
            });

            // unlock the rendering cache of the new sheet (update cache entries on model changes)
            this.renderCache.unlockCache();

            // notify listeners
            this.trigger("change:activesheet", activeSheet, this.sheetModel);

            // another sheet with same cell selection settings counts as changed
            // selection too, therefore explicitly trigger "change:selection" events
            const selection = this.sheetModel.getSelection();
            this.trigger("change:selection", selection, selection, true);

            // refresh the layout of the header and grid panes
            this._requestInitializePanes({ force: true, hide: true });
        }
    }

    /**
     * Initializes a tracking observer for the split separators.
     */
    /*private*/ _initSplitTracking() {

        // the header pane layout settings
        const leftLayout = this._headerPaneMap.get(PaneSide.L);
        const topLayout = this._headerPaneMap.get(PaneSide.T);

        // return `true` for column split line and split crossing point
        const isColSplit = record => record.id !== "rows";
        // return `true` for row split line and split crossing point
        const isRowSplit = record => record.id !== "cols";

        // returns the X position and min/max limits of a split line according to the passed record
        const getSplitLeftMetrics = record => {
            const min = this.getHeaderWidth();
            const max = this._paneRootEl.offsetWidth - SCROLLBAR_WIDTH - DYNAMIC_SPLIT_SIZE;
            let offset = leftLayout.offset + leftLayout.size + (isColSplit(record) ? record.offset.x : 0);
            offset = (offset - MIN_PANE_SIZE < min) ? min : (offset + MIN_PANE_SIZE > max) ? max : offset;
            return [offset, min, max];
        };

        // returns the Y position and min/max limits of a split line according to the passed record
        const getSplitTopMetrics = record => {
            const min = this.getHeaderHeight();
            const max = this._paneRootEl.offsetHeight - SCROLLBAR_HEIGHT - DYNAMIC_SPLIT_SIZE;
            let offset = topLayout.offset + topLayout.size + (isRowSplit(record) ? record.offset.y : 0);
            offset = (offset - MIN_PANE_SIZE < min) ? min : (offset + MIN_PANE_SIZE > max) ? max : offset;
            return [offset, min, max];
        };

        const updateNodeOffset = (node, propName, [offset, min, max]) => {
            node.classList.toggle("collapsed", (offset <= min) || (offset >= max));
            setElementStyle(node, propName, offset - TRACKING_OFFSET);
            setElementStyle(this._crossSplitTrackingEl, propName, offset - TRACKING_OFFSET);
        };

        // initialize tracking observer
        const observer = this.member(new TrackingObserver({
            start: record => {
                this._colSplitTrackingEl.classList.toggle("tracking-active", isColSplit(record));
                this._rowSplitTrackingEl.classList.toggle("tracking-active", isRowSplit(record));
            },
            move: record => {
                if (isColSplit(record)) { updateNodeOffset(this._colSplitTrackingEl, "left", getSplitLeftMetrics(record)); }
                if (isRowSplit(record)) { updateNodeOffset(this._rowSplitTrackingEl, "top", getSplitTopMetrics(record)); }
            },
            end: record => {
                let splitWidth = null, splitHeight = null;
                if (isColSplit(record)) {
                    const [offset, min, max] = getSplitLeftMetrics(record);
                    splitWidth = ((min < offset) && (offset < max)) ? this.sheetModel.convertPixelToHmm(offset - this.getHeaderWidth()) : 0;
                } else {
                    splitWidth = this.sheetModel.getSplitWidthHmm();
                }
                if (isRowSplit(record)) {
                    const [offset, min, max] = getSplitTopMetrics(record);
                    splitHeight = ((min < offset) || (offset < max)) ? this.sheetModel.convertPixelToHmm(offset - this.getHeaderHeight()) : 0;
                } else {
                    splitHeight = this.sheetModel.getSplitHeightHmm();
                }
                this.sheetModel.setDynamicSplit(splitWidth, splitHeight);
            },
            cancel: () => {
                this._requestInitializePanes();
            },
            finally: () => {
                this._crossSplitTrackingEl.classList.remove("tracking-active");
                this._colSplitTrackingEl.classList.remove("tracking-active");
                this._rowSplitTrackingEl.classList.remove("tracking-active");
                this.grabFocus();
            }
        }));

        // start observing the split line elements
        observer.observe(this._crossSplitTrackingEl, { id: "cross" });
        observer.observe(this._colSplitTrackingEl,   { id: "cols" });
        observer.observe(this._rowSplitTrackingEl,   { id: "rows" });
    }

    /**
     * Handles "keydown" events from the view root node.
     */
    /*private*/ _keyDownHandler(event) {

        // do nothing while text edit mode (cell or drawing) is active
        if (this.isTextEditMode()) { return; }

        // special handling for drawing selection
        if (this.selectionEngine.hasDrawingSelection()) {

            // enter text edit mode in first selected drawing
            if (matchKeyCode(event, "F2")) {
                this.enterTextEditMode("drawing");
                return false;
            }

            // delete selected drawings
            if (matchKeyCode(event, "DELETE") || matchKeyCode(event, "BACKSPACE")) {
                if (this.requireEditMode()) {
                    // bug 39533: delete drawings via controller for busy screen etc.
                    this.executeControllerItem("drawing/delete");
                }
                return false;
            }

            return;
        }

        // enter cell edit mode with current cell contents
        if (matchKeyCode(event, "F2")) {
            this.enterTextEditMode("cell");
            return false;
        }

        // open context menu
        if (matchKeyCode(event, "F10")) {
            this._globalContextMenuHandler(event);
            return false;
        }

        // enter cell edit mode with empty text area
        if (!_.browser.MacOS && matchKeyCode(event, "BACKSPACE")) {
            this.enterTextEditMode("cell", { text: "" });
            return false;
        }

        // clear contents (not formatting) of all cells in the selection
        if (matchKeyCode(event, "DELETE") || (_.browser.MacOS && matchKeyCode(event, "BACKSPACE"))) {
            if (this.requireEditMode()) {
                // bug 39533: clear cell range via controller for busy screen etc.
                this.executeControllerItem("cell/clear/values");
            }
            return false;
        }
    }

    /**
     * Handles "keypress" events from the view root node. Starts the cell edit
     * mode on-the-fly for valid Unicode characters.
     */
    /*private*/ _keyPressHandler(event) {

        // the initial text to be inserted into the text area
        let initialText = null;

        // do not handle "keypress" events bubbled up from active text edit mode
        if (this.isTextEditMode()) { return; }

        // ignore key events where either CTRL/META or ALT is pressed, ignore
        // SPACE keys with any control keys (used as shortcuts for column/row selection)
        // - Allow Alt+L for "@" sign on OSX devices
        const ctrlKey = !!event.ctrlKey;
        const metaKey = !!event.metaKey;
        const altKey = !!event.altKey;
        if (
            ((event.charCode > 32) && (_.browser.MacOS ? (ctrlKey === metaKey) : ((ctrlKey || metaKey) === altKey))) ||
            ((event.charCode === 32) && !hasModifierKeys(event))
        ) {

            // build string from character code
            initialText = String.fromCharCode(event.charCode);

            // decide which text edit mode will be started
            const editMode = this.selectionEngine.hasDrawingSelection() ? "drawing" : "cell";
            // the options passed to the text editor
            const options = { text: initialText };

            // special handling for cell edit mode
            if (editMode === "cell") {
                // start with quick edit mode (cursor keys will move cell cursor instead of text cursor)
                options.quick = true;
                // bug 52949: immediately start auto-completion after first character
                options.autoComplete = true;
                // percentage number format: add percent sign to number-like first character
                if ((this.getNumberFormatCategory() === "percent") && (/^[-+0-9]$/.test(initialText) || (initialText === LOCALE_DATA.dec))) {
                    options.text += "%";
                    options.pos = 1;
                }
            }

            // start text edit mode, and drop the key event
            this.enterTextEditMode(editMode, options);
            return false;
        }
    }

    /*private*/ _globalContextMenuHandler(event) {

        // the event target node, as jQuery object
        const targetNode = $(event.target);

        // quit if the contextmenu event was triggered on an input-/textfield
        if (targetNode.is("input, textarea")) { return; }

        // was the event triggered by keyboard?
        const isGlobalTrigger = isContextEventTriggeredByKeyboard(event);
        const selectedDrawings = this.selectionEngine.getSelectedDrawings();
        const activeGridPane = this.getActiveGridPane();
        const layerRootNode = $(activeGridPane.layerRootNode);
        const scrollNode = layerRootNode.parent();
        const sheetOffset = layerRootNode.offset();
        const sheetPosition = layerRootNode.position();
        // scroll positions needed for scrolling hacking in IE & Firefox
        const scrollLeft = scrollNode.scrollLeft();
        const scrollTop = scrollNode.scrollTop();
        // the node, on which the event should be triggered later
        let triggerNode = null;

        // contextmenu event on the header-pane
        if (targetNode.is(".header-pane .cell")) {
            triggerNode = targetNode;

        // contextmenu event on the status-pane
        } else if (targetNode.is(".status-pane .button, .status-pane .button *") || (isGlobalTrigger && $(".status-pane .active-sheet-group.focused .selected").length > 0)) {
            if (isGlobalTrigger && $(".status-pane .active-sheet-group.focused .selected").length > 0) {
                triggerNode = $(".status-pane .active-sheet-group.focused .button.selected");
                event.pageX += Math.round(triggerNode.width() / 2);
                event.pageY += Math.round(triggerNode.height() / 2);
            } else {
                if (!event.pageX && !event.pageY) {
                    event.pageX = targetNode.offset().left;
                    event.pageY = targetNode.offset().top;
                }
                triggerNode = targetNode;
            }

        // contextmenu event on a drawing
        } else if (selectedDrawings.length > 0) {
            triggerNode = activeGridPane.drawingRenderer.getDrawingFrame(selectedDrawings[0]);

            // if we have a last known touch position, use it
            if (this._lastTouchPos) {
                event.pageX = this._lastTouchPos.pageX;
                event.pageY = this._lastTouchPos.pageY;

            // otherwise, locate a meaningful position
            } else if (isGlobalTrigger) {
                const rectangle = this.drawingCollection.getModel(selectedDrawings[0]).getRectangle();

                event.target = triggerNode;
                event.pageX = Math.round(rectangle.centerX()) - sheetPosition.left + sheetOffset.left - scrollLeft;
                event.pageY = Math.round(rectangle.centerY()) - sheetPosition.top + sheetOffset.top - scrollTop;
            }

        // contextmenu event on a cell
        } else if (targetNode.is(".grid-pane *, html")) {
            triggerNode = activeGridPane.$el;

            // if we have a last known touch position, use it
            if (this._lastTouchPos) {
                event.pageX = this._lastTouchPos.pageX;
                event.pageY = this._lastTouchPos.pageY;

            // otherwise, locate a meaningful position
            } else if (isGlobalTrigger) {
                const activeCellRect = this.sheetModel.getCellRectangle(this.selectionEngine.getActiveCell(), { pixel: true, expandMerged: true });
                event.pageX = Math.round(activeCellRect.centerX()) - sheetPosition.left + sheetOffset.left - scrollLeft;
                event.pageY = Math.round(activeCellRect.centerY()) - sheetPosition.top + sheetOffset.top - scrollTop;
            }
        }

        if (triggerNode) {
            ContextMenu.triggerShowEvent(triggerNode, event);
        }

        return false;
    }

    /**
     * Registers a new text editor for a specific text edit mode.
     *
     * @param {TextEditorBase} textEditor
     *  The text editor to be registered.
     */
    /*private*/ _registerTextEditor(textEditor) {

        // insert the editor into the regisrty
        this._textEditorRegistry.set(textEditor.editMode, textEditor);

        // forward all editor events to the listeners of this instance
        this.listenToAllEvents(textEditor, this.trigger);

        // store the active editor instance in `activeTextEditor`
        this.listenTo(textEditor, "textedit:enter", () => { this._activeTextEditor = textEditor; });
        this.listenTo(textEditor, "textedit:leave", () => { this._activeTextEditor = null; });
    }

    /**
     * Moves the browser focus into the active sheet pane.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Address} [options.targetAddress]
     *    The address of a target cell. In a frozen split view, the grid pane
     *    that contains this address will be focused. See `getTargetGridPane`
     *    for more details.
     */
    /*private*/ _grabFocusHandler(options) {

        // prevent JS errors when importing the document fails, and the view does not have activated a sheet yet
        if (!this.sheetModel) { return; }

        // do not steel focus from active comment editor
        if (isCommentTextframeNode(getFocus())) { return; }

        // text edit mode has own focus handling
        if (this._activeTextEditor?.isActive()) {
            this._activeTextEditor.grabFocus();
            return;
        }

        // focus the target grid pane if specified
        const targetAddress = options?.targetAddress;
        const gridPane = targetAddress ? this.getTargetGridPane(targetAddress) : this.getActiveGridPane();
        gridPane.grabFocus();
    }
}
