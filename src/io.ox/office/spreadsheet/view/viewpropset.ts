/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { PropertySet } from "@/io.ox/office/tk/container/propertyset";
import type { RemoteClientData } from "@/io.ox/office/editframework/app/editapplication";
import type { TrackingRectangle } from "@/io.ox/office/drawinglayer/utils/drawingutils";

import type { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * Type of the view property "autoFillData".
 */
export interface AutoFillData {

    /**
     * The direction in which the selected range will be expanded.
     */
    direction: Direction;

    /**
     * The number of columns/rows to extend the selected range into the
     * specified direction (positive values), or to shrink the selected range
     * (negative values).
     */
    count: number;
}

/**
 * All dynamic properties for a spreadsheet document view (dynamic runtime
 * properties that are not backed by document operations).
 */
export interface IViewProps {

    /**
     * An array of active remote clients with selection settings. The local
     * client MUST NOT be part of the array.
     */
    remoteClients: RemoteClientData[];

    /**
     * Specific cell ranges that are in a special activated state, used for
     * example for ranges that will be inserted into a formula while in formula
     * edit mode. These ranges will be rendered in a special way over the
     * regular cell selection (with a marching ants effect). Allowed values are
     * `null` (no active ranges available), or a sheet selection. The array of
     * selected drawing objects MUST be empty.
     */
    activeSelection: SheetSelection | null;

    /**
     * The address of the cell range currently selected during a tracking
     * cycle.
     */
    trackingRange: Range | null;

    /**
     * Additional data for the current cell range while auto-fill tracking is
     * active. Allowed values are `null` (auto-fill tracking not active), or an
     * `AutoFillData` structure. This property will be ignored, if the current
     * sheet selection consists of more than one cell range.
     */
    autoFillData: AutoFillData | null;

    /**
     * The sheet rectangle currently selected while frame selection mode is
     * active, in pixels. The rectangle object will contain the additional
     * boolean properties `reverseX` and `reverseY`.
     */
    frameTrackingRect: TrackingRectangle | null;
}

// private functions ==========================================================

function equals<T extends Equality<T>>(value1: T | null, value2: T | null): boolean {
    return (!value1 && !value2) || !!(value1 && value2 && value1.equals(value2));
}

// class ViewPropertySet ======================================================

/**
 * A dynamic property set for a document view (dynamic runtime properties that
 * are not backed by document operations).
 */
export class ViewPropertySet extends PropertySet<IViewProps> {
    public constructor(_docView: SpreadsheetView) {
        super({
            remoteClients:     { def: [], equals: _.isEqual },
            activeSelection:   { def: null, equals },
            trackingRange:     { def: null, equals },
            autoFillData:      { def: null, equals: _.isEqual },
            frameTrackingRect: { def: null, equals }
        });
    }
}
