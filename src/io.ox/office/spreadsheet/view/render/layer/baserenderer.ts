/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Rectangle } from "@/io.ox/office/tk/dom";

import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import { VisibleViewObject } from "@/io.ox/office/spreadsheet/view/visibleviewobject";

// types ======================================================================

/**
 * The settings for updating the layer range of a grid pane.
 */
export interface RenderLayerSettings {

    /**
     * The ratio between old and new zoom factor. If this value is 1, the zoom
     * factor has not been changed.
     */
    zoomScale: number;

    /**
     * The ratio between old and new canvas resolution (device pixels per CSS
     * pixel). If this value is 1, the canvas resolution has not been changed.
     */
    canvasScale: number;

    /**
     * Whether the gridpane was visible before (the size of the rendering
     * layers have changed).
     */
    wasVisible: boolean;
}

// class BaseRenderer =========================================================

/**
 * Base class for the layer renderers of a grid pane in a spreadsheet view.
 *
 * @param gridPane
 *  The grid pane instance that owns this layer renderer.
 */
export abstract class BaseRenderer<EvtMapT = Empty> extends VisibleViewObject<EvtMapT> {

    readonly gridPane: GridPane;

    /**
     * The address of the cell range covered by this rendering layer.
     */
    protected layerRange: Opt<Range>;

    /**
     * The rectangle covered by this rendering layer, in pixels.
     */
    protected layerRect: Opt<Rectangle>;

    /**
     * Will become `true` when switching to another sheet, will be switched
     * back to `false` when the renderers start to render the new sheet. Used
     * to suppress processing events until the renderer will be initialized
     * (debounced) for the new sheet.
     */
    #switchingSheets = false;

    // constructor ------------------------------------------------------------

    protected constructor(gridPane: GridPane) {
        super(gridPane.docView);

        this.gridPane = gridPane;

        // do not process events while switching to another sheet
        this.listenTo(gridPane.docView, "before:activesheet", () => {
            this.#switchingSheets = true;
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the grid pane containing this instance is currently in
     * visible state.
     *
     * @returns
     *  Whether the parent grid pane is currently in visible state.
     */
    override isVisible(): boolean {
        return this.gridPane.isVisible();
    }

    /**
     * Called after the layer range of the parent grid pane has been changed.
     *
     * @param layerRange
     *  The address of the cell range covered by the rendering layer.
     *
     * @param layerRect
     *  The rectangle covered by the rendering layer, in pixels.
     *
     * @param layerSettings
     *  Additional settings of the new layer range of the grid pane.
     */
    setLayerRange(layerRange: Range, layerRect: Rectangle, layerSettings: RenderLayerSettings): void {
        this.#switchingSheets = false;
        this.layerRange = layerRange;
        this.layerRect = layerRect;
        this.implSetLayerRange(layerSettings);
    }

    /**
     * Called after the parent grid pane hides itself.
     */
    hideLayerRange(): void {
        this.#switchingSheets = false;
        this.implHideLayerRange();
        this.layerRange = this.layerRect = undefined;
    }

    // protected methods ------------------------------------------------------

    /**
     * Do not process events while switching to another sheet.
     */
    protected override canProcessEvents(): boolean {
        return !this.#switchingSheets && super.canProcessEvents();
    }

    /**
     * Called after the layer range of the parent grid pane has been changed.
     * Subclasses MUST overwrite this method to refresh themselves accordingly.
     *
     * @param layerSettings
     *  The settings of the new layer range of the grid pane.
     */
    protected abstract implSetLayerRange(_layerSettings: RenderLayerSettings): void;

    /**
     * Called after the parent grid pane hides itself. Subclasses MUST
     * overwrite this method and deinitialize this renderer accordingly.
     */
    protected abstract implHideLayerRange(): void;
}
