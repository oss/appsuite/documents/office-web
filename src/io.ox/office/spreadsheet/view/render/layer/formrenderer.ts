/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { math, ary, dict, jpromise } from "@/io.ox/office/tk/algorithms";
import { type AnyKeyEvent, type IconId, setCssRectToElement, detachChildren, createButton, matchKeyCode, isEscapeKey, Rectangle } from "@/io.ox/office/tk/dom";
import { type JOptAbortablePromise, debounceMethod } from "@/io.ox/office/tk/objects";
import type { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { BaseRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/baserenderer";
import { TableColumnMenu } from "@/io.ox/office/spreadsheet/view/popup/tablecolumnmenu";
import { ValidationListMenu } from "@/io.ox/office/spreadsheet/view/popup/validationlistmenu";
import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";

// types ======================================================================

/**
 * A popup menu class to be used as cell dropdown menu, with additional methods
 * to be implemented.
 */
export interface ICellMenu<DataT> extends BaseMenu {

    /**
     * Initialization method that will be invoked before opening the cell menu.
     *
     * @param address
     *  The address of the cell containing the button node that caused opening
     *  the menu.
     *
     * @param data
     *  Additional data needed to initialize the menu.
     *
     * @returns
     *  This method may return a promise indicating that initialization of the
     *  dropdown menu has been finished. The promise returned by this method
     *  may be abortable. In this case, when opening the dropdown menu
     *  repeatedly, or opening another cell menu quickly, old initialization
     *  requests will be aborted, before calling the initialization method
     *  again.
     */
    initialize(address: Address, data: DataT): MaybeAsync;

    /**
     * Returns a tooltip to be shown on the cell button connected with the cell
     * dropdown menu.
     *
     * @returns
     *  A tooltip for the associated cell button.
     */
    getTooltip(address: Address): string;

    /**
     * Handler for "keydown" events received in the grid pane containing this
     * cell menu.
     *
     * @param event
     *  The "keydown" event to be processed.
     *
     * @returns
     *  Whether the key event represents a supported key and has been processed
     *  successfully.
     */
    handleKeyDownEvent(event: AnyKeyEvent): boolean;
}

// private types --------------------------------------------------------------

/**
 * Optional parameters for cell dropdown menu buttons.
 */
interface CellButtonOptions {

    /**
     * The key of a specific icon to be shown in the button instead of the
     * default dropdown caret icon.
     */
    iconKey?: IconKey;

    /**
     * If set to `true`, the button will be positioned outside the cell
     * rectangle. By default, the button will be positioned inside.
     */
    outside?: boolean;

    /**
     * Distance of the button to the border of the cell rectangle. Default
     * value is `0`.
     */
    distance?: number;
}

interface CellButtonData<DataT> {

    cellButton: HTMLButtonElement;
    address: Address;
    menuData: DataT;
    anchorWidth: number;

    menuId: MenuType;
    cellMenu: ICellMenu<DataT>;
    menuPrio: number;
}

// constants ==================================================================

type IconKey = "sort_asc" | "sort_desc" | "filter" | "filter_sort_asc" | "filter_sort_desc";

// maps filter/sort states to dropdown button icons
const ICON_MAP: Record<IconKey, IconId> = {
    sort_asc: "png:sort-up",
    sort_desc: "png:sort-down",
    filter: "png:filter",
    filter_sort_asc: "png:sort-up-filter",
    filter_sort_desc: "png:sort-down-filter"
};

type MenuType = "table" | "validation";

const BUTTON_PRIORITIES: Record<MenuType, number> = {
    table: 1,
    validation: 2
};

// class FormRenderer =========================================================

/**
 * Renders the form controls (especially cell dropdown buttons) into the DOM
 * layers of a single grid pane.
 */
export default class FormRenderer extends BaseRenderer {

    // the form control layer (container for dropdown buttons and other controls)
    readonly #layerNode: HTMLElement;

    // dropdown menu for table column filter/sorting
    readonly #tableMenu: TableColumnMenu;
    // dropdown menu for list validation
    readonly #dvListMenu: ValidationListMenu;
    // all existing cell dropdown buttons with additional metadata
    readonly #buttonRegistry = new Map<HTMLButtonElement, CellButtonData<unknown>>();

    // the button metadata of the active cell menu
    #activeData: Opt<CellButtonData<unknown>>;

    // current asynchronous initialization of the cell menu contents
    #initPromise: Opt<JOptAbortablePromise>;

    // constructor ------------------------------------------------------------

    /**
     * @param gridPane
     *  The grid pane instance that owns this form layer renderer.
     */
    constructor(gridPane: GridPane) {

        // base constructor
        super(gridPane);

        // the form control layer (container for dropdown buttons and other controls)
        this.#layerNode = gridPane.createLayerNode("form-layer");

        // dropdown menu for table columns (sorting and filtering)
        this.#tableMenu = this.#registerCellMenu(new TableColumnMenu(this.docView));
        // dropdown menu for data validation of active cell
        this.#dvListMenu = this.#registerCellMenu(new ValidationListMenu(this.docView));

        // hide the active cell menu, if:
        // - the document changes to readonly mode
        // - selection tracking starts
        // - text edit mode (cell editor, drawing editor) starts
        this.listenToWhenVisible(this.docApp, "docs:editmode:leave", this.#hideActiveCellMenu);
        this.listenToWhenVisible(this.gridPane, "select:start", this.#hideActiveCellMenu);
        this.listenToWhenVisible(this.docView, "textedit:enter", this.#hideActiveCellMenu);

        // render the filter dropdown buttons (visibility depends on locked state of sheet too)
        this.listenToWhenVisible(this.docView, ["change:sheetattrs", "insert:table", "change:table", "change:table:attrs", "change:table:column", "delete:table"], this.#renderFormControlsDebounced);

        // hide the active cell menu after specific sheet properties have been changed
        this.listenToWhenVisible(this.docView, "change:sheetprops", ({ newProps }) => {
            if (newProps.selection || newProps.activePane || dict.someKey(newProps, key => key.startsWith("split"))) {
                this.#hideActiveCellMenu();
            }
        });

        // hide the active cell menu after specific sheet properties have been changed
        this.listenToViewPropWhenVisible("autoFillData", this.#hideActiveCellMenu);

        // render validation dropdown button after selection changes
        this.listenToWhenVisible(gridPane, "render:cellselection", this.#renderValidationButton);
    }

    // public methods ---------------------------------------------------------

    /**
     * Handler for "keydown" events received in the grid pane containing this
     * instance.
     *
     * @param event
     *  The "keydown" event to be processed.
     *
     * @returns
     *  Whether the key event represents a supported key and has been processed
     *  successfully.
     */
    handleKeyDownEvent(event: AnyKeyEvent): boolean {

        // close cell menu currently open
        if (isEscapeKey(event) && this.#hideActiveCellMenu()) {
            return true;
        }

        // show cell dropdown menu of active cell on ALT+DOWN
        if (matchKeyCode(event, "DOWN_ARROW", { alt: true })) {
            return this.#activateCellMenu();
        }

        // forward keyboard shortcuts to the cell menu currently visible
        return !!this.#activeData?.cellMenu.handleKeyDownEvent(event);
    }

    // protected methods ------------------------------------------------------

    /**
     * Changes the layers according to the passed layer range.
     */
    @renderLogger.profileMethod("$badge{FormRenderer} implSetLayerRange")
    protected override implSetLayerRange(): void {
        this.#renderFormControls();
    }

    /**
     * Resets this renderer, clears the DOM layer nodes.
     */
    protected override implHideLayerRange(): void {
        detachChildren(this.#layerNode);
        this.#buttonRegistry.clear();
    }

    // private methods --------------------------------------------------------

    // dynamic anchor position according to the clicked dropdown button
    #getCellMenuAnchor(): Opt<Rectangle> {

        // nothing to do without active cell button
        if (!this.#activeData) { return undefined; }
        const button = this.#activeData.cellButton;

        // current page position of the button node
        const buttonRect = button.getBoundingClientRect();
        // the width of cell and button rectangle, used for anchor of the dropdown menu node
        const anchorWidth = this.#buttonRegistry.get(button)!.anchorWidth;
        // left offset of the anchor
        const anchorLeft = buttonRect.left + buttonRect.width - anchorWidth;

        // create the anchor rectangle according to the current position of the button node
        return new Rectangle(anchorLeft, buttonRect.top, anchorWidth, buttonRect.height);
    }

    /**
     * Registers a cell menu instance to be shown for specific dropdown buttons
     * embedded in the active sheet.
     *
     * @param cellMenu
     *  The cell menu that will be shown when clicking the specified cell
     *  dropdown buttons.
     */
    #registerCellMenu<DataT, MenuT extends ICellMenu<DataT>>(cellMenu: MenuT): MenuT {

        // take ownership
        this.member(cellMenu);

        // only the active button must be included in the focus handling of the menu
        cellMenu.registerFocusableNodes(".clipboard");
        cellMenu.registerFocusableNodes(() => this.#activeData?.cellButton);

        // dynamic anchor position according to the clicked dropdown button
        // ("anchorBox" hides the menu automatically, if the anchor leaves the visible area of the grid pane)
        cellMenu.setAnchor(() => this.#getCellMenuAnchor(), { anchorBox: this.gridPane.scrollNode });

        // remember active menu for usage in other methods
        this.listenTo(cellMenu, "popup:show", () => {
            this.#activeData?.cellButton.classList.add("dropdown-open");
        });

        // abort initialization if popup menu will be closed quickly
        this.listenTo(cellMenu, "popup:hide", () => {
            this.#activeData?.cellButton.classList.remove("dropdown-open");
            this.#activeData = undefined;
        });

        return cellMenu;
    }

    /**
     * Opens or closes the embedded cell popup menu associated to the passed
     * cell dropdown button.
     */
    #toggleCellMenu(button: HTMLButtonElement): void {

        // abort another initialization of a cell menu currently running
        this.#initPromise?.abort?.();
        this.#initPromise = undefined;

        // always leave text edit mode (do not open the menu, if formula validation fails)
        this.docView.leaveTextEditMode().done(() => {

            // resolve metadata for the button
            const activeData = this.#buttonRegistry.get(button)!;
            const { cellMenu, address, menuData } = activeData;

            // if the menu is currently visible, close it and return
            if (cellMenu.isVisible()) {
                cellMenu.hide();
                this.docView.grabFocus();
                return;
            }

            // remember current button node, will be returned as focusable node to prevent
            // closing the menu too fast (the menu may be associated to different buttons)
            this.#activeData = activeData;

            // set cell menu to busy state for asynchronous initialization, show the menu
            cellMenu.busy();
            cellMenu.show();

            // start contents initialization
            this.#initPromise = jpromise.from(cellMenu.initialize(address, menuData));
            const promise = this.#initPromise;

            // leave busy mode after initialization
            this.onSettled(promise, () => cellMenu.idle());

            // hide the cell menu if initialization fails
            this.onRejected(promise, () => cellMenu.hide());

            // abort initialization if menu will be closed manually
            cellMenu.one("popup:hide", () => promise.abort?.());

            // show warning alerts if available
            jpromise.floating(this.docView.yellOnFailure(promise));
        });
    }

    /**
     * Hides the cell popup menu currently shown.
     *
     * @returns
     *  Whether a cell list menu was actually open and has been hidden.
     */
    #hideActiveCellMenu(): boolean {
        if (this.#activeData?.cellMenu.isVisible()) {
            this.#activeData.cellMenu.hide();
            return true;
        }
        return false;
    }

    /**
     * Returns all cell dropdown buttons attached to the specified cell.
     *
     * @param address
     *  The address of the cell whose dropdown menu buttons will be returned.
     *
     * @returns
     *  All cell dropdown menu buttons at the passed cell address.
     */
    #getCellButtons(address: Address): HTMLButtonElement[] {
        return ary.from(this.#buttonRegistry, ([button, data]) => data.address.equals(address) ? button : undefined);
    }

    /**
     * Removes all cell dropdown button elements of the specified menu.
     *
     * @param menuId
     *  The identifier of the popup menu whose existing dropdown buttons will
     *  be removed.
     */
    #removeCellButtons(menuId: MenuType): void {
        for (const [button, data] of this.#buttonRegistry) {
            if (data.menuId === menuId) {
                button.remove();
                this.#buttonRegistry.delete(button);
            }
        }
    }

    /**
     * Creates a dropdown button element intended to be attached to the
     * trailing border of the specified cell, and inserts it into the form
     * control layer.
     *
     * @param menuId
     *  The identifier of a registered popup cell menu. If the created button
     *  will be clicked, the menu will be shown.
     *
     * @param cellMenu
     *  The dropdown menu to be attached to the menu button.
     *
     * @param address
     *  The address of the cell the button will be associated to.
     *
     * @param rectangle
     *  The position of the cell range the button is attached to (pixels). This
     *  rectangle may be larger than the area covered by the specified cell,
     *  e.g. if the cell is part of a merged range, and the dropdown button has
     *  to be displayed in the bottom-right corner of the range.
     *
     * @param menuData
     *  Additional data for the button.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created and inserted button element.
     */
    #createCellButton<DataT>(menuId: MenuType, cellMenu: ICellMenu<DataT>, address: Address, rectangle: Rectangle, menuData: DataT, options?: CellButtonOptions): HTMLButtonElement {

        // whether to place the button element outside the cell rectangle
        const outside = options?.outside;
        // distance of the button element to the cell rectangle
        const distance = options?.distance || 0;
        // outer size of the button element (according to cell height)
        const size = math.clamp(rectangle.height + 1, 18, 30);
        // size of the sheet area hidden by frozen split
        const hiddenSize = this.gridPane.getHiddenSize();
        // position of the button in the sheet area
        const buttonRect = new Rectangle(
            Math.max(rectangle.right() + (outside ? distance : -size), hiddenSize.width),
            Math.max(rectangle.bottom() - size, hiddenSize.height - 1),
            size,
            size
        );
        // the width of the anchor rectangle to be used for attached dropdown menu
        const anchorWidth = rectangle.boundary(buttonRect).width;

        // create the button element
        const iconKey = options?.iconKey;
        const cellButton = createButton({
            type: "bare",
            classes: "cell-dropdown-button skip-tracking",
            dataset: { menu: menuId, address: address.toOpStr(), icon: iconKey || "none" },
            style: { lineHeight: buttonRect.height - 2 },
            icon: (iconKey && ICON_MAP[iconKey]) || "bi:caret-down-fill",
            tooltip: cellMenu.getTooltip(address),
            click: () => this.#toggleCellMenu(cellButton)
        });

        // additional element attributes
        setCssRectToElement(cellButton, this.gridPane.convertToLayerRectangle(buttonRect));
        cellButton.tabIndex = -1;

        // add button into DOM, and its metadata into button registry
        this.#layerNode.append(cellButton);
        const menuPrio = BUTTON_PRIORITIES[menuId];
        this.#buttonRegistry.set(cellButton, { cellButton, address, menuData, anchorWidth, menuId, cellMenu, menuPrio });

        // suppress double-clicks
        $(cellButton).on("dblclick", false);
        return cellButton;
    }

    /**
     * Tries to activate a cell menu attached to the active cell. If the cell
     * contains multiple dropdown menus, the method searches for a cell menu
     * currently activated for that cell, and activates the next registered
     * menu. The order of the menus is equal to the registration order via the
     * method `registerCellMenu`.
     *
     * @returns
     *  Whether a dropdown menu has been found and activated for the cell.
     */
    #activateCellMenu(): boolean {

        // the address of the active cell
        const address = this.docView.selectionEngine.getActiveCell();

        // all cell button nodes attached to the specified cell (nothing to do, if no cell dropdown button found)
        const cellButtons = this.#getCellButtons(address);
        if (!cellButtons.length) { return false; }

        // always scroll to the active cell
        this.gridPane.scrollToCell(address);

        // sort cell buttons by priority
        ary.sortBy(cellButtons, btn => this.#buttonRegistry.get(btn)!.menuPrio);

        // do nothing if the activated menu is the only menu for the current cell
        if ((cellButtons.length === 1) && (this.#activeData?.cellButton === cellButtons[0])) { return true; }

        // always hide the active cell menu before opening the next menu
        this.#hideActiveCellMenu();

        // pick the next cell button to be activated if multiple are available
        const index = this.#activeData ? cellButtons.indexOf(this.#activeData.cellButton) : -1;
        const cellButton = cellButtons[(index + 1) % cellButtons.length];

        // open the menu
        this.#toggleCellMenu(cellButton);
        return true;
    }

    /**
     * Creates the form controls (cell dropdown buttons) currently visible in
     * this grid pane.
     */
    #renderFormControls(): void {

        // nothing to do, if all columns/rows are hidden (method may be called debounced)
        if (!this.gridPane.isVisible() || !this.layerRange) {
            this.implHideLayerRange();
            return;
        }

        // remove all filter buttons, do not render them in locked sheets
        this.#removeCellButtons("table");
        if (this.docView.isSheetLocked()) { return; }

        // the collections of the active sheet
        const { colCollection, rowCollection, tableCollection } = this.docView;

        // create dropdown buttons for all visible table ranges with activated filter/sorting
        for (const tableModel of tableCollection.yieldTableModels({ overlapRange: this.layerRange, autoFilter: true })) {

            // bug 51782, bug 51456: do not render buttons without header row, or if buttons are disabled explicitly
            if (!tableModel.areButtonsVisible()) { continue; }

            // the cell range covered by the table
            const tableRange = tableModel.getRange();
            // the settings of the header row
            const rowDesc = rowCollection.getEntry(tableRange.a1.r);

            // do nothing if the header row is outside the layer range, or hidden
            if ((tableRange.a1.r < this.layerRange.a1.r) || (rowDesc.size === 0)) { continue; }

            // visit all visible columns of the table in the layer range
            const colInterval = tableRange.colInterval().intersect(this.layerRange.colInterval())!;
            for (const colDesc of colCollection.indexEntries(colInterval, { visible: true })) {

                // relative table column index
                let tableCol = colDesc.index - tableRange.a1.c;
                const columnModel = tableModel.getColumnModel(tableCol);

                // bug 36152: do not render buttons covered by merged ranges
                if (!columnModel.isButtonVisible()) { continue; }

                // bug 36152: visible dropdown button in a merged range is placed in the last column of the
                // merged range, but is associated to (shows cell data of) the first column in the merged range
                while ((tableCol > 0) && !tableModel.getColumnModel(tableCol - 1).isButtonVisible()) {
                    tableCol -= 1;
                }

                const address = new Address(colDesc.index, rowDesc.index);
                const rectangle = new Rectangle(colDesc.offset, rowDesc.offset, colDesc.size, rowDesc.size);

                // determine the custom filter/sort icon for the dropdown button
                const filterKey = columnModel.isFiltered() ? "filter" : undefined;
                const sortKey = !columnModel.isSorted() ? undefined : columnModel.isDescending() ? "sort_desc" : "sort_asc";
                const iconKey: Opt<IconKey> = ((filterKey && sortKey) ? `${filterKey}_${sortKey}` : (filterKey || sortKey));

                // create the button element
                this.#createCellButton("table", this.#tableMenu, address, rectangle, { tableModel, tableCol }, { iconKey });
            }
        }
    }

    /**
     * Debounced version of `renderFormControls`.
     */
    @debounceMethod()
    #renderFormControlsDebounced(): void {
        this.#renderFormControls();
    }

    /**
     * Creates or updates the dropdown button for list validation, after the
     * selection layer has been rendered.
     */
    #renderValidationButton(): void {

        // remove the old button element
        this.#removeCellButtons("validation");

        // do not show in read-only mode, or while using auto-fill feature
        if (!this.docView.isEditable() || this.docView.propSet.get("autoFillData")) { return; }

        // do not show the dropdown button in locked cells
        const msgCode = this.docView.ensureUnlockedActiveCell({ lockMatrixes: "full", sync: true });
        if (msgCode !== null) { return; }

        // the current selection in the document view
        const selection = this.docView.selectionEngine.getSelection();
        // active cell in the current selection
        const activeCell = selection.address;
        // active cell, expanded to a merged range
        const activeRange = this.docView.mergeCollection.expandRangeToMergedRanges(new Range(activeCell));

        // check that the active range is inside the row interval covered by this grid pane (needed to prevent
        // painting the upper part of the button, if cell is located in the top row of the lower part of a frozen split)
        const availableInterval = this.gridPane.rowHeaderPane.getAvailableInterval();
        if (!availableInterval || (activeRange.a2.r < availableInterval.first)) { return; }

        // check whether to show a validation dropdown button
        if (!this.docView.dvRuleCollection.showDropDownButton(activeCell)) { return; }

        // whether the right border of the active cell touches the right border of any selection range
        const rightBorder = selection.ranges.some(range => activeRange.a2.c === range.a2.c);

        // create and insert the button node
        const rangeRect = this.docView.getRangeRectangle(activeRange);
        const distance = rightBorder ? 2 : 0;
        this.#createCellButton("validation", this.#dvListMenu, activeCell, rangeRect, undefined, { outside: true, distance });
    }
}
