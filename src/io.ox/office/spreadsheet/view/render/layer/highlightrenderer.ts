/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createDiv, detachChildren, replaceChildren, setCssRectToElement, getSchemeColorIndex } from "@/io.ox/office/tk/dom";
import { IntervalRunner } from "@/io.ox/office/tk/workers";

import { TrackingRectangle } from "@/io.ox/office/drawinglayer/utils/drawingutils";

import { OUTER_BORDER_KEYS, BORDER_CORNER_KEYS } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import type { HighlightRangeDescriptor } from "@/io.ox/office/spreadsheet/view/edit/rangehighlighter";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { BaseRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/baserenderer";

// types ======================================================================

/**
 * Type mapping for the events emitted by `HighlightRenderer` instances.
 */
export interface HighlightRendererEventMap {

    /**
     * Will be emitted after the current frame selection has actually been
     * rendered into the DOM highlighting layer.
     *
     * @param frameNode
     *  The DOM element representing the frame rectangle.
     *
     * @param frameRect
     *  The location of the selected frame, in pixels.
     */
    "render:frameselection": [frameNode: HTMLElement, frameRect: TrackingRectangle];
}

// class HighlightRenderer ====================================================

/**
 * Renders the highlighted cell ranges (shown e.g. during editing formulas with
 * references into the active sheet) into the DOM layers of a single grid pane.
 *
 * @param gridPane
 *  The grid pane instance that owns this highlighting layer renderer.
 */
export class HighlightRenderer extends BaseRenderer<HighlightRendererEventMap> {

    // properties -------------------------------------------------------------

    // the range highlight layer (container for the highlighted ranges of formulas)
    readonly #highlightLayerNode: HTMLDivElement;

    // the active range layer (container for the active range selected in cell edit mode)
    readonly #activeRangeLayerNode: HTMLDivElement;

    // the frame selection layer with cross-hair lines
    readonly #frameSelectLayerNode: HTMLDivElement;

    // line elements in the cross-hair frame selection layer
    readonly #frameLineLNode: HTMLDivElement;
    readonly #frameLineRNode: HTMLDivElement;
    readonly #frameLineTNode: HTMLDivElement;
    readonly #frameLineBNode: HTMLDivElement;
    readonly #frameRectNode: HTMLDivElement;

    // background worker for the "marching ants" effect in active selections
    readonly #antsRunner: IntervalRunner;

    // constructor ------------------------------------------------------------

    constructor(gridPane: GridPane) {
        super(gridPane);

        // create the layer DOM nodes
        this.#highlightLayerNode = gridPane.createLayerNode("highlight-layer", { after: "selection-layer" });
        this.#activeRangeLayerNode = gridPane.createLayerNode("active-layer");
        this.#frameSelectLayerNode = gridPane.createLayerNode("frame-select-layer");

        // create the line elements in the cross-hair frame selection layer
        this.#frameLineLNode = this.#frameSelectLayerNode.appendChild(createDiv("frame-line v"));
        this.#frameLineRNode = this.#frameSelectLayerNode.appendChild(createDiv("frame-line v"));
        this.#frameLineTNode = this.#frameSelectLayerNode.appendChild(createDiv("frame-line h"));
        this.#frameLineBNode = this.#frameSelectLayerNode.appendChild(createDiv("frame-line h"));
        this.#frameRectNode = this.#frameSelectLayerNode.appendChild(createDiv("frame-rect"));

        // background worker for the "marching ants" effect in active selections
        this.#antsRunner = this.member(new IntervalRunner(index => {
            this.#activeRangeLayerNode.dataset.frame = String(index % 6);
        }, 150));

        // update highlighting when document edit mode or view attributes have changed
        this.listenToWhenVisible(this.docApp, "docs:editmode", this.#renderHighlightedRanges);
        this.listenToWhenVisible(this.docView.rangeHighlighter, "change:ranges", this.#renderHighlightedRanges);

        // render the active selection of the sheet after it has been changed
        this.listenToViewPropWhenVisible("activeSelection", this.#renderActiveSelection);

        // render the cross-hair or the frame rectangle while frame selection mode is active
        this.listenToViewPropWhenVisible("frameTrackingRect", this.#renderFrameSelection);
    }

    // protected methods ------------------------------------------------------

    /**
     * Changes the layers according to the passed layer range.
     */
    @renderLogger.profileMethod("$badge{HighlightRenderer} implSetLayerRange")
    protected implSetLayerRange(): void {
        this.#renderHighlightedRanges();
        this.#renderActiveSelection();
    }

    /**
     * Resets this renderer, clears the DOM layer nodes.
     */
    protected implHideLayerRange(): void {
        detachChildren(this.#highlightLayerNode);
        detachChildren(this.#activeRangeLayerNode);
    }

    // private methods --------------------------------------------------------

    /**
     * Renders all visible highlighted cell ranges.
     */
    #renderHighlightedRanges(): void {

        interface HighlightRangeEntry {
            rangeDesc: HighlightRangeDescriptor;
            rangeIndex: number;
        }

        // the address factory needed to check for column/row ranges
        const { addressFactory } = this.docModel;
        // the fragment with all range elements
        const fragment = document.createDocumentFragment();

        // render all ranges that are visible in this grid pane
        if (this.gridPane.isVisible()) {

            // current document edit mode
            const editMode = this.docView.isEditable();
            // the index of the active sheet
            const sheet = this.docView.activeSheet;
            // all range descriptors of the active sheet
            const highlightRanges: HighlightRangeEntry[] = [];
            // the cell range addresses of all ranges to be rendered
            const ranges = new RangeArray();
            // maps range keys to zero-based color indexes
            const rangeColors = new Map<string, number>();
            // next free zero-based color index
            let nextColor = 0;

            // collect all range descriptors of the active sheet
            this.docView.rangeHighlighter.forEachRange((rangeDesc, rangeIndex) => {
                if (rangeDesc.range.containsSheet(sheet)) {
                    highlightRanges.push({ rangeDesc, rangeIndex });
                    ranges.push(rangeDesc.range.toRange()); // push simple range without sheet indexes
                }
            });

            // render the highlighted ranges
            for (const { range, rectangle, index } of this.gridPane.renderRanges(ranges)) {

                // the current range descriptor, and its original array index
                const { rangeDesc, rangeIndex } = highlightRanges[index];
                // the unique key of the formula token containing the range (will be resolved to a scheme color)
                const { tokenKey } = rangeDesc;
                // whether the range will be rendered with tracking handle elements
                const draggable = editMode && rangeDesc.draggable;
                // whether the range is currently tracked (rendered with special glow effect)
                const tracking = draggable && rangeDesc.tracking;

                // register a new range key in the 'rangeColors' map
                if (!rangeColors.has(tokenKey)) {
                    rangeColors.set(tokenKey, nextColor);
                    nextColor += 1;
                }

                // adjust position of static ranges
                if (!draggable) { rectangle.expandSelf(1, 1, 0, 0); }

                // generate the DOM element for the highlighted range
                const classes = { range: true, "tracking-active": tracking };
                const colorId = getSchemeColorIndex(rangeColors.get(tokenKey)!);
                const dataset = { style: colorId, index: rangeIndex, ...addressFactory.getRangeDataAttrs(range) };
                const rangeNode = fragment.appendChild(createDiv({ classes, dataset, style: rectangle.toCSS() }));
                rangeNode.appendChild(createDiv("fill"));

                // create elements for static or draggable borders
                if (draggable) {
                    const bordersNode = rangeNode.appendChild(createDiv("borders"));
                    for (const pos of OUTER_BORDER_KEYS) {
                        bordersNode.appendChild(createDiv({ dataset: { pos } }));
                    }
                    const resizersNode = rangeNode.appendChild(createDiv("resizers"));
                    for (const pos of BORDER_CORNER_KEYS) {
                        resizersNode.appendChild(createDiv({ dataset: { pos } }));
                    }
                } else {
                    rangeNode.appendChild(createDiv("static-border"));
                }
            }
        }

        // insert all range elements into the container node
        replaceChildren(this.#highlightLayerNode, fragment);
    }

    /**
     * Renders the activated cell ranges. These ranges are used for example
     * while selecting new ranges for a formula while in cell edit mode.
     */
    #renderActiveSelection(): void {

        // the active ranges
        const selection = this.docView.propSet.get("activeSelection");
        // the fragment with all range elements
        const fragment = document.createDocumentFragment();

        // generate the HTML mark-up for the ranges
        if (this.gridPane.isVisible() && selection) {
            for (const { range, rectangle } of this.gridPane.renderRanges(selection.ranges)) {

                // create the range element
                const dataset = { range: range.toOpStr() };
                const rangeNode = fragment.appendChild(createDiv({ classes: "range", dataset, style: rectangle.toCSS() }));

                // create the border line elements
                const bordersNode = rangeNode.appendChild(createDiv("borders"));
                for (const pos of OUTER_BORDER_KEYS) {
                    bordersNode.appendChild(createDiv({ dataset: { pos } }));
                }
            }
        }

        // start or stop the background worker for the "marching ants" effect
        this.#antsRunner.toggle(fragment.hasChildNodes());

        // insert all range elements into the container node
        replaceChildren(this.#activeRangeLayerNode, fragment);
    }

    /**
     * Renders the cross-hair and rectangle for activated frame selection mode.
     *
     * @param trackingRect
     *  The rectangle (absolute sheet location) to be rendered, in pixels.
     */
    #renderFrameSelection(trackingRect: TrackingRectangle | null): void {

        // hide DOM layer and exit, if frame selection mode is not active
        this.#frameSelectLayerNode.classList.toggle("active", !!trackingRect);
        if (!trackingRect) { detachChildren(this.#frameRectNode); return; }

        // convert rectangle position relative to the layer nodes
        const layerRect = this.gridPane.convertToLayerRectangle(trackingRect);
        const isVisible = layerRect.area() > 0;

        // set the effective coordinates to the frame nodes
        this.#frameLineLNode.style.left = `${layerRect.left}px`;
        this.#frameLineRNode.style.display = (layerRect.width > 0) ? "" : "none";
        this.#frameLineRNode.style.left = `${layerRect.right() - 1}px`;
        this.#frameLineTNode.style.top = `${layerRect.top}px`;
        this.#frameLineBNode.style.display = (layerRect.height > 0) ? "" : "none";
        this.#frameLineBNode.style.top = `${layerRect.bottom() - 1}px`;
        this.#frameRectNode.style.display = isVisible ? "" : "none";
        setCssRectToElement(this.#frameRectNode, layerRect);

        // notify render listeners
        if (isVisible) {
            const relTrackingRect = new TrackingRectangle(layerRect.left, layerRect.top, layerRect.width, layerRect.height, 0, 0);
            relTrackingRect.reverseX = trackingRect.reverseX;
            relTrackingRect.reverseY = trackingRect.reverseY;
            this.trigger("render:frameselection", this.#frameRectNode, relTrackingRect);
        }
    }
}
