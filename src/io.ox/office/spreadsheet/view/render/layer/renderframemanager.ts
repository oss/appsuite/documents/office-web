/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { SELECTED_SELECTOR } from "@/io.ox/office/tk/forms";
import { insertChildren } from "@/io.ox/office/tk/dom";
import { NODE_SELECTOR } from "@/io.ox/office/drawinglayer/view/drawingframe";
import { createPageNode, getPageContentNode } from "@/io.ox/office/textframework/utils/dom";
import { FrameNodeManager } from "@/io.ox/office/spreadsheet/model/drawing/framenodemanager";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// class RenderFrameManager ===================================================

/**
 * Extends the class `FrameNodeManager` with functionality needed to actually
 * render a specific drawing layer (either regular drawing objects, or cell
 * notes).
 */
export class RenderFrameManager extends FrameNodeManager {

    readonly #docView: SpreadsheetView;

    // the page node serving as root node for the text framework
    readonly #pageRootNode: HTMLElement;

    // the content node of the page node (child of page node)
    readonly #pageContentNode: HTMLElement;

    // constructor ------------------------------------------------------------

    constructor(docView: SpreadsheetView, layerNode: HTMLDivElement) {
        super(docView.docModel);

        // properties
        this.#docView = docView;
        this.#pageRootNode = layerNode.appendChild(createPageNode()[0]);
        this.#pageContentNode = getPageContentNode(this.#pageRootNode)[0];

        // insert the frame root node into the page
        this.#pageContentNode.appendChild(this.rootNode);

        // bug 31479: suppress double-clicks for drawings
        $(layerNode).on("dblclick", false);
    }

    // public methods ---------------------------------------------------------

    /**
     * Initializes the page node containing the drawing frames. Prepares the
     * leading empty container nodes for the preceding sheets (needed by the
     * text framework to be able to generate correct text positions in document
     * operations).
     */
    initializePageNode(): void {

        this.rootNode.remove();

        const { activeSheet } = this.#docView;
        const childCount = this.#pageContentNode.childNodes.length;

        for (let missingNodes = activeSheet - childCount; missingNodes > 0; missingNodes -= 1) {
            this.#pageContentNode.appendChild(FrameNodeManager.createRootNode());
        }

        insertChildren(this.#pageContentNode, activeSheet, this.rootNode);
    }

    /**
     * Returns all selected DOM drawing frame nodes.
     *
     * @returns
     *  The selected drawing frames.
     */
    getSelectedDrawingFrames(): JQuery {
        return $(this.rootNode).find(NODE_SELECTOR + SELECTED_SELECTOR);
    }
}
