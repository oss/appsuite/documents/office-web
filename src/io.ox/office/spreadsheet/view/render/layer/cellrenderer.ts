/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { itr } from "@/io.ox/office/tk/algorithms";
import { type Rectangle, detachChildren, setElementStyle, createDiv } from "@/io.ox/office/tk/dom";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { Canvas } from "@/io.ox/office/tk/canvas";

import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { MAX_BORDER_WIDTH } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";

import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import type { GridBoundary } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { renderLogger, getCanvasCoord, getCanvasRect, getCssSize, getCssRect } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { type RenderLayerSettings, BaseRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/baserenderer";

import type { CanvasRenderSettings, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";
import { CanvasGridRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasgridrenderer";
import { CanvasFillRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasfillrenderer";
import { CanvasBorderRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasborderrenderer";
import { CanvasTextRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvastextrenderer";
import { CanvasDataBarRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasdatabarrenderer";
import { CanvasIconSetRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasiconsetrenderer";

// types ======================================================================

/**
 * Type mapping for the events emitted by `CellRenderer` instances.
 */
export interface CellRendererEventMap {

    /**
     * Will be emitted after all visible cells have been rendered.
     */
    "render:cells:finished": [];
}

// class CellRenderer =========================================================

export class CellRenderer extends BaseRenderer<CellRendererEventMap> {

    /** The canvas element for all cell contents. */
    readonly #cellCanvas: Canvas;

    /** All canvas layer renderers, in expected rendering order. */
    readonly #canvasRenderers: CanvasBaseRenderer[] = [];

    /** All canvas layer renderers for fast pre-rendering. */
    readonly #canvasFastRenderers: CanvasBaseRenderer[] = [];

    /** The layer node containing outlines for matrix formulas. */
    readonly #matrixLayerNode: HTMLDivElement;

    /** The layer node containing outlines for table ranges. */
    readonly #tableLayerNode: HTMLDivElement;

    /** All pending cell ranges to be rendered debounced. */
    readonly #pendingRanges = new RangeArray();

    /** The outline element for the autofilter. */
    #autoFilterOutline: Opt<HTMLDivElement>;

    // constructor ------------------------------------------------------------

    constructor(gridPane: GridPane) {
        super(gridPane);

        // create the cell canvas (no backdrop alpha channel may cause faster rendering)
        this.#cellCanvas = this.member(new Canvas({ classes: "cell-layer", settings: { alpha: false } }));

        // create and register all canvas layer renderers
        this.registerCanvasRenderer(this.member(new CanvasFillRenderer(this.docView)), true);
        this.registerCanvasRenderer(this.member(new CanvasGridRenderer(this.docView)), true);
        this.registerCanvasRenderer(this.member(new CanvasBorderRenderer(this.docView)));
        this.registerCanvasRenderer(this.member(new CanvasDataBarRenderer(this.docView)));
        this.registerCanvasRenderer(this.member(new CanvasTextRenderer(this.docView)));
        this.registerCanvasRenderer(this.member(new CanvasIconSetRenderer(this.docView)));

        // create additional DOM layer nodes
        this.#matrixLayerNode = gridPane.createLayerNode("matrix-layer");
        this.#tableLayerNode = gridPane.createLayerNode("table-layer");

        // insert the canvas element into the DOM
        this.#matrixLayerNode.before(this.#cellCanvas.el);

        // start rendering after any cell elements in the rendering cache have changed
        this.listenToWhenVisible(this.docView, "rendercache:ready", this.#registerPendingRanges);

        // update cell layer after changing visibility of cell grid (grid color already handled via rendering cache)
        this.listenToSheetPropWhenVisible("showGrid", showGrid => {
            this.#registerPendingRanges(this.layerRange!);
            this.#matrixLayerNode.classList.toggle("hidden", !showGrid);
            this.#tableLayerNode.classList.toggle("hidden", !showGrid);
        });

        // render table range outlines
        this.listenToWhenVisible(this.docView, ["insert:table", "delete:table", "change:table"], this.#renderTableOutlinesDebounced);
    }

    // public methods ---------------------------------------------------------

    /**
     * Repaints all contents, if this renderer is visible.
     */
    invalidateLayerRange(): void {
        if (this.layerRange) {
            this.#registerPendingRanges(this.layerRange);
        }
    }

    /**
     * Registers a canvas layer renderer that will be invoked when rendering
     * contents into the cell canvas.
     *
     * @param renderer
     *  The canvas layer renderer.
     *
     * @param [fastMode]
     *  If set to `true`, the renderer will be used for fast pre-rendering
     *  during scrolling.
     */
    registerCanvasRenderer(renderer: CanvasBaseRenderer, fastMode = false): void {
        this.#canvasRenderers.push(renderer);
        if (fastMode) { this.#canvasFastRenderers.push(renderer); }
    }

    // protected methods ------------------------------------------------------

    /**
     * Changes the layers according to the passed layer range.
     */
    @renderLogger.profileMethod("$badge{CellRenderer} implSetLayerRange")
    protected override implSetLayerRange(layerSettings: RenderLayerSettings): void {

        // initialize canvas element (automatic scrolling of retained contents)
        for (const { rectangle, range } of this.#initCanvasElement(layerSettings)) {

            // immediately render the grid lines and column/row fill styles to reduce the "empty space"
            // effect when scrolling quickly
            this.#renderCanvasLayers(rectangle, range, true);

            // register the new ranges for debounced rendering (needed to render missing parts of existing
            // cells at the old layer boundaries which will not be notified from the rendering cache)
            this.#registerPendingRanges(range);
        }

        // repaint all table range outlines
        this.#renderTableOutlines();

        // notify external listeners
        this.trigger("render:cells:finished");
    }

    /**
     * Resets this renderer, clears the DOM layer nodes.
     */
    protected override implHideLayerRange(): void {
        // bug 47674: do not touch canvas element (performance problem when switching sheets)
        // this.#cellCanvas.initialize({ width: 0, height: 0 });
        detachChildren(this.#matrixLayerNode);
        detachChildren(this.#tableLayerNode);
        this.#pendingRanges.clear();
    }

    // private methods --------------------------------------------------------

    /**
     * Initializes the canvas element. Scrolls retained contents to their new
     * positions, and determines blank canvas areas to be rendered.
     */
    @renderLogger.profileMethod("$badge{CellRenderer} initCanvasElement")
    #initCanvasElement(layerSettings: RenderLayerSettings): GridBoundary[] {

        // the boundaries of all new ranges in the canvas to be rendered
        const newBoundaries: GridBoundary[] = [];
        // the rectangle to be covered by the cell canvas, in device pixels
        const layerCanvasRect = getCanvasRect(this.layerRect!);

        // render entire rectangle, if rendering layer was hidden before
        if (!layerSettings.wasVisible) {
            this.#cellCanvas.initialize(layerCanvasRect);
            newBoundaries.push({ rectangle: layerCanvasRect, range: this.layerRange! });
        } else {
            // "scroll" canvas element to the new layer rectangle, calculate cell ranges to be rendered
            // from empty rectangles (layer rectangle does not match range boundaries)
            for (const rectangle of this.#cellCanvas.relocate(layerCanvasRect, layerSettings.zoomScale * layerSettings.canvasScale)) {
                const range = this.docView.sheetModel.getRangeFromRectangle(getCssRect(rectangle), { pixel: true });
                newBoundaries.push({ rectangle, range });
            }
        }

        // set the CSS size of the canvas to the layer size
        setElementStyle(this.#cellCanvas.el, "width", Math.round(getCssSize(layerCanvasRect.width)));
        setElementStyle(this.#cellCanvas.el, "height", Math.round(getCssSize(layerCanvasRect.height)));

        return newBoundaries;
    }

    /**
     * Renders the outline range elements of the passed table ranges.
     *
     * @param tableModels
     *  An iterable with the models of the table range to be rendered.
     */
    #appendTableOutlines(tableModels: Iterable<TableModel>): void {

        // collect all table ranges (expand auto-filter to available content range)
        const tableRanges = new RangeArray();
        const tableNames: string[] = [];
        for (const tableModel of tableModels) {

            // the data range, expanded at bottom border for auto filters
            const dataRange = tableModel.getDataRange();

            // expand table range to data range (dynamically expanded for autofilters)
            const tableRange = tableModel.getRange();
            tableRanges.push(dataRange ? tableRange.boundary(dataRange) : tableRange);

            // add name of the table range
            tableNames.push(tableModel.name);
        }

        // create the outline elements for all visible table ranges
        for (const { range, rectangle, index } of this.gridPane.renderRanges(tableRanges, { alignToGrid: true })) {
            const tableName = tableNames[index];
            const dataset = { range: range.toOpStr(), name: tableName ?? ":autofilter" };
            const outlineEl = createDiv({ classes: "range", dataset, style: rectangle.toCSS() });
            if (tableName) {
                outlineEl.title = _.noI18n(tableName);
            } else {
                this.#autoFilterOutline = outlineEl;
            }
            this.#tableLayerNode.appendChild(outlineEl);
        }
    }

    /**
     * Clears the table range layer, and renders the DOM elements for the
     * outlines of all visible table ranges.
     */
    #renderTableOutlines(): void {
        detachChildren(this.#tableLayerNode);
        this.#autoFilterOutline = undefined;
        this.#appendTableOutlines(this.docView.tableCollection.yieldTableModels({ autoFilter: true }));
    }

    /**
     * Debounced version of method `renderTableOutlines`.
     */
    @debounceMethod({ delay: "microtask" })
    #renderTableOutlinesDebounced(): void {
        this.#renderTableOutlines();
    }

    /**
     * Updates the outline node of the auto filter. The auto filter changes
     * dynamically after adding or deleting cell contents below at its bottom
     * border.
     */
    #updateAutoFilterOutline(): void {

        // remove the current auto filter outline node
        this.#autoFilterOutline?.remove();
        this.#autoFilterOutline = undefined;

        // render the outline element for the auto filter
        const tableModel = this.docView.tableCollection.getAutoFilterModel();
        if (tableModel) { this.#appendTableOutlines(itr.once(tableModel)); }
    }

    /**
     * Invokes the registered canvas renderers.
     */
    #renderCanvasLayers(clipRect: Rectangle, renderRange: Range, fastMode: boolean): void {

        // rendering cache of the active sheet
        const cache = this.docView.renderCache;

        // all settings to be passed to the renderers
        const cssSheetRect = cache.sheetModel.getSheetRectangle({ pixel: true });
        const settings: CanvasRenderSettings = {
            renderRange,
            canvasClipRect: clipRect,
            cssLayerRect: this.layerRect!,
            canvasLayerRect: getCanvasRect(this.layerRect!),
            cssSheetRect,
            canvasSheetRect: getCanvasRect(cssSheetRect),
            fastMode
        };

        // invoke all canvas renderers
        const canvasRenderers = fastMode ? this.#canvasFastRenderers : this.#canvasRenderers;
        for (const canvasRenderer of canvasRenderers) {
            this.#cellCanvas.render(clipRect, context => {

                // each renderer in its own try/catch to be able to render as much as possible
                // (even if there is a bug in one of the renderers)
                try {
                    canvasRenderer.render(context, cache, settings);
                } catch (err) {
                    renderLogger.exception(err);
                }
            });
        }
    }

    /**
     * Renders all registered ranges into the canvas element.
     */
    @debounceMethod({ delay: 50 })
    @renderLogger.profileMethod("$badge{CellRenderer} renderPendingRanges")
    _renderPendingRanges(): void {

        // wait for the rendering cache to finish building its cell entries
        // (it will trigger an update event that causes to run this method again)
        const { renderCache } = this.docView;
        if (!this.layerRange || this.#pendingRanges.empty() || renderCache.isCacheBusy()) { return; }

        // always render entire rows to properly support excessive text overflow
        let rowIntervals = this.#pendingRanges.rowIntervals().merge();

        // expand all row intervals by one row up and down to be able to paint or delete thick borders
        // covering the pixels of adjacent cells
        const { rowCollection } = renderCache.sheetModel;
        for (const rowInterval of rowIntervals) {
            rowInterval.first = rowCollection.findPrevVisibleIndex(rowInterval.first - 1) ?? rowInterval.first;
            rowInterval.last = rowCollection.findNextVisibleIndex(rowInterval.last + 1) ?? rowInterval.last;
        }

        // merge expanded rows again, restrict rows to the visible layer range
        rowIntervals = rowIntervals.merge().intersect(this.layerRange.rowInterval());
        renderLogger.trace(() => `rowIntervals=${rowIntervals.toOpStr(false)}`);

        // render the rows (restrict to the visible layer range)
        const colInterval = this.layerRange.colInterval();
        for (const rowInterval of rowIntervals) {

            // the rendering range, and its rectangle in canvas pixels, for clipping
            const renderRange = Range.fromIntervals(colInterval, rowInterval);
            const clipRect = renderCache.getCanvasRectForRange(renderRange);

            // Reduce the clipping rectangle by half of the maximum border width above/below to not clear the outer
            // borders of the rendered range (row intervals have been expanded by one row up/down, see above).
            // If the row interval hits the boundary of the entire layer range (e.g. last row in sheet), expand the
            // clipping area instead to be able to render thick border lines exceeding the sheet boundary.
            const borderWidth = Math.max(1, getCanvasCoord(MAX_BORDER_WIDTH));
            const expandT = (this.layerRange.a1.r < rowInterval.first) ? -Math.floor(borderWidth / 2) : Math.ceil(borderWidth / 2);
            const expandB = (rowInterval.last < this.layerRange.a2.r) ? -Math.ceil(borderWidth / 2) : Math.floor(borderWidth / 2);
            clipRect.expandSelf(0, expandT, 0, expandB);

            // render all layers into the cell canvas
            this.#renderCanvasLayers(clipRect, renderRange, false);
        }

        // reset the pending ranges for next rendering cycle
        this.#pendingRanges.clear();

        // update the auto filter outline range which may change after every cell change
        this.#updateAutoFilterOutline();

        // notify external listeners
        this.trigger("render:cells:finished");
    }

    /**
     * Registers the specified cell ranges to be rendered in a background task.
     *
     * @param ranges
     *  The addresses of all cell ranges to be rendered, or the address of a
     *  single cell range.
     */
    #registerPendingRanges(ranges: OrArray<Range>): void {
        this.#pendingRanges.append(ranges);
        this._renderPendingRanges();
    }
}
