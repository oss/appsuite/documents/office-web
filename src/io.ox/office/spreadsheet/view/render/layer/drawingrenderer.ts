/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { math, itr, ary, map, jpromise } from "@/io.ox/office/tk/algorithms";
import type { RectangleLike, Rectangle, NodeOrJQuery } from "@/io.ox/office/tk/dom";
import {
    SMALL_DEVICE, MAX_NODE_SIZE,
    toNode, detachChildren, createDiv, createSvg, createIcon,
    setCssRectToElement, parseCssLength
} from "@/io.ox/office/tk/dom";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { AsyncWorker } from "@/io.ox/office/tk/workers";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { type ShapeAttributeSet, DrawingModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import {
    FLIPPED_HORIZONTALLY_CLASSNAME, FLIPPED_VERTICALLY_CLASSNAME, ROTATED_DRAWING_CLASSNAME,
    getAndClearContentNode, getCanvasNode, isConnectorDrawingFrame, isFlippedHorz, isFlippedVert,
    updateFormatting, setCssTransform, isSelected, clearSelection, drawSelection
} from "@/io.ox/office/drawinglayer/view/drawingframe";
import { Color } from "@/io.ox/office/editframework/utils/color";
import { setExplicitAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { CommentPopup } from "@/io.ox/office/editframework/view/popup/commentpopup";
import { debounceAfterActionsMethod } from "@/io.ox/office/editframework/app/decorators";

import { SHOW_REMOTE_SELECTIONS } from "@/io.ox/office/spreadsheet/utils/config";
import { splitDisplayString } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Position } from "@/io.ox/office/spreadsheet/utils/operations";
import type { SheetDrawingModelType } from "@/io.ox/office/spreadsheet/model/drawing/xsheetdrawingmodel";
import { SheetShapeModel } from "@/io.ox/office/spreadsheet/model/drawing/shapemodel";
import { NoteModel } from "@/io.ox/office/spreadsheet/model/drawing/notemodel";
import { CommentModel } from "@/io.ox/office/spreadsheet/model/drawing/commentmodel";
import { getTextFrame, withTextFrame, copyTextFrame } from "@/io.ox/office/spreadsheet/model/drawing/text/textframeutils";

import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { type RenderLayerSettings, BaseRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/baserenderer";
import { RenderFrameManager } from "@/io.ox/office/spreadsheet/view/render/layer/renderframemanager";
import { DrawingContextMenu } from "@/io.ox/office/spreadsheet/view/popup/drawingcontextmenu";

// types ======================================================================

/**
 * Type mapping for the events emitted by `DrawingRenderer` instances.
 */
export interface DrawingRendererEventMap {

    /**
     * Will be emitted after a single drawing frame has been rendered.
     *
     * @param drawingFrame
     *  The DOM drawing frame that has been rendered.
     *
     * @param drawingModel
     *  The model of the drawing object that has been rendered.
     */
    "render:drawingframe": [drawingFrame: JQuery, drawingModel: SheetDrawingModelType];

    /**
     * Will be emitted after the current drawing selection has actually been
     * rendered into the DOM drawing layer (rendering may happen debounced
     * after several "change:selection" view events).
     *
     * @param drawingFrames
     *  The DOM drawing frames currently selected in this grid pane.
     */
    "render:drawingselection": [drawingFrames: JQuery];
}

// private types --------------------------------------------------------------

interface PendingFrameEntry {
    model: SheetDrawingModelType;
    options: Opt<RenderDrawingFrameOptions>;
}

interface FormatDrawingFrameOptions {

    /**
     * If set to `true`, the formatting of the drawing frame will be updated
     * immediately instead of using the background task. Default value is
     * `false`.
     */
    immediate?: boolean;
}

interface UpdateDrawingFrameOptions {

    /**
     * If set to `true`, the formatting of the drawing frame will always be
     * updated. By default, the formatting will only be updated, if the
     * visibility or size of the drawing frame has been changed.
     */
    format?: boolean;

    /**
     * If set to `true`, the text contents of the drawing frame will always be
     * updated. By default, the text contents will only be updated after
     * updating the formatting of the drawing frame.
     */
    text?: boolean;
}

interface RenderDrawingFrameOptions extends FormatDrawingFrameOptions, UpdateDrawingFrameOptions {

    /**
     * Whether the drawing frame or any of its ancestors were hidden, and
     * therefore need to be fully updated. Default value is `false`.
     */
    hidden?: boolean;

    /**
     * Whether the zoom factor has been changed, i.e. whether all drawing
     * frames need to be updated completely regardless of their current state.
     * Default value is `false`.
     */
    zoom?: boolean;
}

type NoteOrCommentModel = NoteModel | CommentModel;

type DrawingModelSource = SheetDrawingModelType | Iterable<SheetDrawingModelType>;

// private functions ==========================================================

/**
 * Returns whether the DOM drawing frame of the passed drawing model needs to
 * be refreshed after it has been created or changed.
 *
 * @param drawingModel
 *  The model of a drawing object.
 *
 * @returns
 *  Whether the DOM drawing frame of the passed drawing model needs to be
 *  refreshed.
 */
function supportsFormatting(drawingModel: SheetDrawingModelType): boolean {
    return drawingModel.drawingType !== DrawingType.GROUP;
}

/**
 * Returns whether the passed drawing model represents a cell note, or a
 * comment thread.
 *
 * @param drawingModel
 *  The model of a drawing object.
 *
 * @returns
 *  Whether the passed drawing model represents a cell note, or a comment
 *  thread.
 */
function isAnyComment(drawingModel: SheetDrawingModelType): drawingModel is NoteOrCommentModel {
    return (drawingModel instanceof NoteModel) || (drawingModel instanceof CommentModel);
}

// class DrawingRenderer ======================================================

/**
 * Renders the drawing objects and cell notes of the active sheet into the
 * DOM drawing layers shown in a single grid pane.
 *
 * @param gridPane
 *  The grid pane instance that owns this drawing layer renderer.
 */
export class DrawingRenderer extends BaseRenderer<DrawingRendererEventMap> {

    // properties -------------------------------------------------------------

    // the container node containing all cell overlay nodes with indicators (below drawing objects and form controls)
    readonly #overlayLayerNode: HTMLDivElement;

    // the root layer node containing the page node
    readonly #drawingLayerNode: HTMLDivElement;

    // the container node containing all connector lines between comments and anchor cells
    // (above drawing obejsts, but below all note frames)
    readonly #connectorLayerNode: HTMLDivElement;

    // the root layer node containing the page node
    readonly #noteLayerNode: HTMLDivElement;

    // the manager for the DOM drawing frames of the drawing objects to be rendered
    readonly #drawingFrameManager: RenderFrameManager;

    // the manager for the DOM drawing frames of the cell notes to be rendered
    readonly #noteFrameManager: RenderFrameManager;

    // the pop-up node for comment threads on small devices
    readonly #commentPopup: CommentPopup;

    // all drawing models, mapped by drawing frame element
    readonly #drawingModelMap = new Map<HTMLElement, SheetDrawingModelType>();

    // all drawing models whose drawing frames need to be rendered (position and format)
    readonly #pendingRenderMap = new Map<SheetDrawingModelType, PendingFrameEntry>();

    // all drawing models whose drawing frames need to be formatted
    readonly #pendingFormatSet = new Set<SheetDrawingModelType>();

    // a background worker that processes all drawing models asynchronously
    readonly #formatWorker = this.member(new AsyncWorker({ interval: 0 }));

    // the drawing models that have been made visible temporarily
    readonly #tempShowModelSet = new Set<SheetDrawingModelType>();

    // cache for cell overlay nodes for notes/comments
    readonly #overlayNodeMap = new Map<NoteOrCommentModel, HTMLElement>();

    // all comment and note models, mapped by overlay cell element (reverse of `_overlayNodeMap`)
    readonly #overlayModelMap = new Map<HTMLElement, NoteOrCommentModel>();

    // cache for connector line nodes for notes
    readonly #connectorNodeMap = new Map<NoteModel, SVGElement>();

    // the model of the cell note whose overlay node is currently hovered
    #hoverNoteModel: Opt<NoteModel>;

    // delay timer to show the cell note whose overlay node is currently hovered
    #hoverNoteTimer: Opt<Abortable>;

    // the bubble container node of the selected comment thread
    #commentBubbleNode: Opt<HTMLElement>;

    // all drawing frames selected by a remote user
    readonly #remoteDrawingFrames: JQuery[] = [];

    // constructor ------------------------------------------------------------

    constructor(gridPane: GridPane) {
        super(gridPane);

        // create the layer DOM nodes
        this.#overlayLayerNode = gridPane.createLayerNode("note-overlay-layer", { before: "remote-layer" });
        this.#drawingLayerNode = gridPane.createLayerNode("drawing-layer");
        this.#connectorLayerNode = gridPane.createLayerNode("note-connector-layer");
        this.#noteLayerNode = gridPane.createLayerNode("drawing-layer note-layer");

        // create the frame managers for drawing objects and cell notes
        this.#drawingFrameManager = this.member(new RenderFrameManager(this.docView, this.#drawingLayerNode));
        this.#noteFrameManager = this.member(new RenderFrameManager(this.docView, this.#noteLayerNode));

        // create the pop-up menus
        this.member(new DrawingContextMenu(gridPane, this.#drawingLayerNode));
        this.#commentPopup = this.member(new CommentPopup({ anchorPadding: 4 }));

        // create and initialize the background worker for drawing frame formatting
        this.#formatWorker.steps.addDelay(200);
        this.#formatWorker.steps.addStep(runner => runner.iterate(
            itr.yieldFirst(this.#pendingFormatSet),
            drawingModel => this.#formatDrawingFrame(drawingModel),
            { slice: 200, interval: 200 }
        ));

        // repaint drawing selection (also when document edit mode changes, e.g. existence of resize handles)
        this.listenToWhenVisible(this.docApp, "docs:editmode", this.#renderDrawingSelectionDebounced);
        this.listenToWhenVisible(this.docView, ["change:selection", "change:sheetattrs"], this.#renderDrawingSelectionDebounced);

        // trigger a "render:drawingselection" event if active pane changes
        // (no actual rendering of the selection needed, border colors will change via CSS)
        this.listenToSheetPropWhenVisible("activePane", () => {
            this.trigger("render:drawingselection", this.#drawingFrameManager.getSelectedDrawingFrames());
        });

        // repaint remote drawing selections
        this.listenToViewPropWhenVisible("remoteClients", this.#renderRemoteSelectionDebounced);

        // process new drawing objects inserted into the drawing collection
        this.listenToWhenVisible(this.docView, "insert:drawing", event => {
            this.#createDrawingFrames(event.drawingModel);
            this.#renderDrawingSelectionDebounced();
            this.#renderRemoteSelectionDebounced();
        });

        // process drawing objects removed from the drawing collection
        this.listenToWhenVisible(this.docView, "delete:drawing", event => {
            this.#removeDrawingFrame(event.drawingModel);
            this.#renderDrawingSelectionDebounced();
            this.#renderRemoteSelectionDebounced();
        });

        // process drawing objects that have been changed in any way (position, size, attributes, text contents)
        this.listenToWhenVisible(this.docView, "change:drawing", event => {
            this.#renderDrawingFramesDebounced(event.drawingModel, { format: event.changeType !== "text", text: true });
        });

        // process drawing objects that have been moved to a new document position (Z order)
        this.listenToWhenVisible(this.docView, "move:drawing", event => {
            this.#drawingFrameManager.moveDrawingFrame(event.drawingModel);
            this.#renderDrawingSelectionDebounced();
            this.#renderRemoteSelectionDebounced();
        });

        // process new notes inserted into the drawing collection
        this.listenToWhenVisible(this.docView, "insert:note", event => {
            this.#createDrawingFrames(event.noteModel);
        });

        // process notes removed from the drawing collection
        this.listenToWhenVisible(this.docView, "delete:note", event => {
            this.#removeDrawingFrame(event.noteModel);
        });

        // process notes that have been moved to new anchor cells
        this.listenToWhenVisible(this.docView, "move:notes", event => {
            this.#renderDrawingFramesDebounced(event.noteModels);
        });

        // process notes that have been changed in any way (position, size, attributes, text contents)
        this.listenToWhenVisible(this.docView, "change:note", event => {
            this.#renderDrawingFramesDebounced(event.noteModel, { format: event.changeType !== "text", text: true });
        });

        // process new comment threads inserted into the document
        this.listenToWhenVisible(this.docModel, ["insert:threadedcomment", "new:threadedcomment:thread", "new:threadedcomment:comment"], commentModel => {
            if (!commentModel.isReply()) { this.#createDrawingFrames(commentModel); }
            this.#renderCommentBubbleDebounced();
        });

        // process comment threads removed from the drawing collection
        this.listenToWhenVisible(this.docModel, "delete:threadedcomment:end", commentThread => {
            commentThread.forEach(commentModel => {
                if (!commentModel.isReply()) {
                    this.#removeDrawingFrame(commentModel);
                }
            });
            this.#renderCommentBubbleDebounced();
        });

        // process comments that have been moved to a new anchor cell
        this.listenToWhenVisible(this.docView, "move:comments", event => {
            this.#renderDrawingFramesDebounced(event.commentModels);
            this.#renderCommentBubbleDebounced();
        });

        // repaint note notifications when merging/unmerging cell ranges
        this.listenToWhenVisible(this.docView, "merge:cells", event => {
            const { noteCollection, commentCollection } = event.sheetModel;
            const ranges = event.deleteRanges.concat(event.insertRanges).merge();
            this.#renderDrawingFramesDebounced(noteCollection.yieldModels({ ranges }));
            this.#renderDrawingFramesDebounced(commentCollection.yieldCommentModels({ ranges }));
        });

        // show notes temporarily when hovering their anchor cell
        $(this.#overlayLayerNode).on("mouseenter", ">.note", event => {
            this.#hideHoverNote();
            const noteModel = this.#overlayModelMap.get(event.target as HTMLElement);
            if (noteModel instanceof NoteModel) { this.#showHoverNote(noteModel); }
        });

        // hide notes when leaving their anchor cell
        $(this.#overlayLayerNode).on("mouseleave", ">.note", () => this.#hideHoverNote());

        // toggle comments pane on click on a bubble icon
        $(this.#overlayLayerNode).on("mousedown", ".icon-bubble-tapper", () => {
            if (!SMALL_DEVICE) { this.docView.commentsPane.toggle(); }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the DOM drawing frame node at the passed document position.
     *
     * @param position
     *  The document position of the drawing model. May specify a position
     *  inside another drawing object.
     *
     * @returns
     *  The DOM drawing frame, as JQuery collection with a single element; or
     *  `undefined`, if the drawing frame does not exist.
     */
    getDrawingFrame(position: Position): Opt<JQuery> {
        const drawingModel = this.docView.drawingCollection.getModel(position);
        return drawingModel && this.#drawingFrameManager.getDrawingFrame(drawingModel);
    }

    /**
     * Returns the DOM drawing frame nodes for all specified document
     * positions.
     *
     * @param positions
     *  The document positions of the drawing models.
     *
     * @returns
     *  The DOM nodes of the drawing frames, as JQuery collection.
     */
    getDrawingFrames(positions: Position[]): JQuery {
        const drawingFrames: HTMLElement[] = [];
        for (const position of positions) {
            const drawingFrame = this.getDrawingFrame(position);
            if (drawingFrame) { drawingFrames.push(drawingFrame[0]); }
        }
        return $(drawingFrames);
    }

    /**
     * Returns the DOM drawing frame that represents the passed drawing model.
     *
     * @param drawingModel
     *  The drawing model instance to return the DOM drawing frame for.
     *
     * @returns
     *  The DOM drawing frame for the passed drawing model; or `undefined`,` if
     *  no drawing frame could be found.
     */
    getDrawingFrameForModel(drawingModel: SheetDrawingModelType): Opt<JQuery> {
        return this.#drawingFrameManager.getDrawingFrame(drawingModel);
    }

    /**
     * Returns the drawing model associated with the passed drawing frame.
     *
     * @param drawingFrame
     *  The DOM drawing frame.
     *
     * @returns
     *  The drawing model associated with the passed DOM drawing frame; or
     *  `undefined`, if no drawing model could be found
     */
    getDrawingModelForFrame(drawingFrame: NodeOrJQuery): Opt<SheetDrawingModelType> {
        const node = toNode(drawingFrame);
        return node ? this.#drawingModelMap.get(node) : undefined;
    }

    /**
     * Temporarily shows the DOM drawing frame associated to the specified
     * drawing model. This method counts its invocations internally. The method
     * `tempHideDrawingFrame()` will hide the drawing frame after the
     * respective number of invocations.
     *
     * @param drawingModel
     *  The drawing model instance to show temporarily.
     */
    tempShowDrawingFrame(drawingModel: SheetDrawingModelType): void {

        // update the set also in invisible grid panes (drawing frame will become visible after splitting the view)
        this.#tempShowModelSet.add(drawingModel);

        // render the drawing frame, if the grid pane is visible
        if (this.gridPane.isVisible()) {
            this.#renderDrawingFrame(drawingModel, { immediate: true });
        }
    }

    /**
     * Hides the DOM drawing frame associated to the specified drawing model,
     * after it has been made visible temporarily. See `tempShowDrawingFrame()`
     * for more details.
     *
     * @param drawingModel
     *  The drawing model instance to hide.
     */
    tempHideDrawingFrame(drawingModel: SheetDrawingModelType): void {

        // update the set also in invisible grid panes
        this.#tempShowModelSet.delete(drawingModel);

        // hide the drawing frame, if the grid pane is visible
        if (this.gridPane.isVisible()) {
            this.#renderDrawingFrame(drawingModel);
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Changes the drawing layer according to the passed layer range.
     */
    @renderLogger.profileMethod("$badge{DrawingRenderer} implSetLayerRange")
    protected implSetLayerRange(layerSettings: RenderLayerSettings): void {

        // repaint all visible drawing frames and notes; or create all drawing frames
        // if not done yet (e.g. active sheet changed, enabled split in current sheet)
        const iterator = itr.chain(
            this.docView.drawingCollection.yieldModels(),
            this.docView.noteCollection.yieldModels(),
            this.docView.commentCollection.yieldCommentModels()
        );

        // render all existing drawing frames, if the layer range has changed
        if (layerSettings.wasVisible) {
            const options: Opt<RenderDrawingFrameOptions> = (layerSettings.zoomScale !== 1) ? { format: true, zoom: true } : undefined;
            this.#renderDrawingFramesDebounced(iterator, options);
            this.#renderCommentBubbleDebounced();
            return;
        }

        // prepare the page containers
        this.#drawingFrameManager.initializePageNode();
        this.#noteFrameManager.initializePageNode();

        // create all DOM drawing frames in this sheet, schedule for deferred rendering
        this.#createDrawingFrames(iterator);

        // paint the entire drawing selection
        this.#renderDrawingSelectionDebounced();
    }

    /**
     * Resets this renderer, and clears the DOM layer node (called e.g. before
     * switching the active sheet).
     */
    protected implHideLayerRange(): void {

        // clear the cache maps
        this.#drawingModelMap.clear();
        this.#pendingRenderMap.clear();
        this.#pendingFormatSet.clear();
        this.#overlayNodeMap.clear();
        this.#overlayModelMap.clear();
        this.#connectorNodeMap.clear();

        // hide the cell note hovered with the mouse
        this.#hideHoverNote();

        // reset internal settings
        this.#remoteDrawingFrames.length = 0;

        // hide all drawing frames
        this.#drawingFrameManager.removeAllDrawingFrames();
        this.#noteFrameManager.removeAllDrawingFrames();

        // remove the cell overlay nodes and connector lines of all cell notes
        detachChildren(this.#overlayLayerNode);
        detachChildren(this.#connectorLayerNode);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the frame node manager that is responsible for the passed
     * drawing model.
     *
     * @param drawingModel
     *  The model of a drawing object.
     *
     * @returns
     *  The drawing frame manager responsible for the passed drawing model.
     */
    #getFrameManager(drawingModel: SheetDrawingModelType): RenderFrameManager {
        return (drawingModel.drawingType === DrawingType.NOTE) ? this.#noteFrameManager : this.#drawingFrameManager;
    }

    /**
     * Returns whether the drawing frame of the specified drawing model needs
     * to be painted (independently of its position in the sheet), including
     * hidden cell notes that are currently hovered with the mouse.
     *
     * @param drawingModel
     *  The model of a drawing object.
     *
     * @returns
     *  Whether the drawing frame of the specified drawing model needs to be
     *  painted.
     */
    #isModelVisible(drawingModel: SheetDrawingModelType): boolean {

        // threaded comments do not have an associated drawing frame
        if (drawingModel instanceof CommentModel) { return false; }

        // do not render hidden drawing objects (but drawing frames that have been made visible temporarily)
        return (drawingModel === this.#hoverNoteModel) || this.#tempShowModelSet.has(drawingModel) || drawingModel.isEffectivelyVisible();
    }

    /**
     * Renders a selection frame into the passed drawing frame.
     *
     * @param drawingFrame
     *  The drawing frame to be selected.
     *
     * @param [force=false]
     *  If set to `true`, an existing selection frame will be removed.
     */
    #updateSelectionFrame(drawingFrame: JQuery, force?: boolean): void {

        const editMode = this.docView.isEditable() && !this.docView.isSheetLocked();
        const drawingModel = this.getDrawingModelForFrame(drawingFrame);
        if (!drawingModel) { return; }

        if (force) { clearSelection(drawingFrame); }

        const singleSelection = this.docView.selectionEngine.hasSingleDrawingSelection();
        drawSelection(drawingFrame, {
            movable: editMode,
            resizable: editMode,
            rotatable: editMode && !this.docApp.isODF() && drawingModel.isDeepRotatable(), // bug 52930: disable rotation in ODS
            rotation: drawingModel.getRotationDeg(),
            adjustable: editMode && singleSelection,
            isMultiSelection: !singleSelection
        });
    }

    /**
     * Updates the dirty flags of the passed drawing frame according to the
     * passed options.
     */
    #updateDirtyFlags(drawingFrame: JQuery, options?: UpdateDrawingFrameOptions): void {
        const dirtyFormat = !!(options?.format || drawingFrame.data("dirtyFormat"));
        const dirtyText = !!(options?.text || drawingFrame.data("dirtyText"));
        drawingFrame.data({ dirtyFormat, dirtyText });
    }

    /**
     * Updates the CSS position of the passed text frame node.
     *
     * @param shapeModel
     *  The model of a shape object.
     *
     * @param textFrame
     *  The DOM text frame of the passed shape model.
     *
     * @param _textPosition
     *  The original position of the text frame at 100% zoom level without
     *  flipping or rotation, as received from the rendering cache.
     */
    #updateTextFramePosition(shapeModel: SheetShapeModel, textFrame: JQuery, _textPosition: RectangleLike): void {

        // the current zoom factor of the active sheet
        const zoom = this.docView.getZoomFactor();
        // text needs to be flipped horizontally, if horizontal/vertical flipping does not match
        const flipText = shapeModel.getEffectiveFlipping().reverseRot;
        // text direction property of drawing shape
        const { vert } = shapeModel.getMergedAttributeSet(true).shape;
        // text direction angle
        const dirAngle = (vert === "vert270") ? 270 : ((vert === "vert") || (vert === "eaVert")) ? 90 : 0;
        // reverse horizontal scaling to flip the text back
        const scaleX = flipText ? -1 : 1;

        textFrame.css({
            fontSize: `${Math.round(16 * zoom)}px`,
            transform: `scaleX(${scaleX}) rotate(${dirAngle}deg)`
        });
    }

    /**
     * Updates the formatting and contents of cell note frames.
     *
     * @param noteModel
     *  The model of a cell note whose drawing frame will be updated.
     *
     * @param drawingFrame
     *  The drawing frame of a cell note.
     */
    #updateNoteFrame(noteModel: NoteModel, drawingFrame: JQuery): void {

        const zoom = this.docView.getZoomFactor();
        const attrSet = noteModel.getMergedAttributeSet(true);
        //const charAttrs = attrSet.character;
        //const fontSize = round(charAttrs.fontSize * zoom);
        const fontSize = Math.round(9 * zoom);

        // the container element for all text lines
        const paddingX = Math.round(3 * zoom);
        const paddingY = Math.round(zoom);
        const textFrame = createDiv("textframe");
        textFrame.style.padding = `${paddingY}px ${paddingX}px`;

        // TODO: import and evaluate all font settings
        const fontStyle: Partial<CSSStyleDeclaration> = {
            fontFamily: "Tahoma,sans-serif",
            fontSize: `${fontSize}pt`,
            color: this.docModel.parseAndResolveTextColor(Color.AUTO, [attrSet.fill.color]).css
        };

        // append all text lines as separate paragraph elements
        for (const textLine of splitDisplayString(noteModel.getText())) {
            textFrame.appendChild(createDiv({ classes: "p", style: fontStyle, label: textLine }));
        }

        // insert the text contents into the drawing frame
        getAndClearContentNode(drawingFrame).append(textFrame);
    }

    /**
     * Updates the position and visibility of the overlay node for a cell note
     * or comment.
     *
     * @param noteModel
     *  The model of a cell note or comment.
     *
     * @returns
     *  Whether the anchor is located in visible columns and rows.
     */
    #updateCellOverlayNode(noteModel: NoteOrCommentModel): boolean {

        // the cell overlay node with the indicator corner
        const overlayNode = this.#overlayNodeMap.get(noteModel);
        if (!overlayNode) {
            renderLogger.warn(`$badge{DrawingRenderer} updateCellOverlayNode: missing overlay node for cell ${noteModel.getAnchor()}`);
            return false;
        }

        // the location of the anchor cell (with merged range), in pixels
        const anchorRect = noteModel.getAnchorRectangle();

        // hide overlay nodes of comments in hidden columns/rows
        const anchorHidden = anchorRect.area() === 0;
        overlayNode.classList.toggle("hidden", anchorHidden);

        // resolve connector line between cell and note frame
        const connectorNode = (noteModel instanceof NoteModel) ? this.#connectorNodeMap.get(noteModel) : undefined;

        // comment in hidden cell: immediately hide the drawing frame and connector line
        if (anchorHidden) {
            const drawingFrame = this.#noteFrameManager.getDrawingFrame(noteModel);
            drawingFrame?.[0].classList.add("hidden");
            connectorNode?.classList.add("hidden");
            return false;
        }

        // update the position and size of the overlay node
        const cellRect = this.gridPane.convertToLayerRectangle(anchorRect);
        setCssRectToElement(overlayNode, cellRect.expandSelf(0, 0, -1, -1));
        overlayNode.dataset.address = noteModel.getAnchor().toOpStr();

        // update the size of the cell hint node
        const hintSize = Math.max(2, Math.round(3 * this.docView.getZoomFactor()));
        (overlayNode.firstChild as HTMLElement).style.borderWidth = `${hintSize}px`;

        // update the position and visibility of the SVG connector node of a cell note
        // (DOCS-4459: also if the note's text frame is currently not inside the viewport)
        if (connectorNode) {

            let frameRect = noteModel.getRectangle({ absolute: true });
            const modelHidden = !this.#isModelVisible(noteModel);
            connectorNode.classList.toggle("hidden", !frameRect || modelHidden);

            if (frameRect && !modelHidden) {
                frameRect = this.gridPane.convertToLayerRectangle(frameRect);

                // the pixel position of the connector end point (top-right cell corner)
                const hintX = cellRect.right();
                const hintY = cellRect.top;

                // the pixel position of the connector start point (top-left or top-right note frame corner)
                const frameX = (hintX < frameRect.left) ? frameRect.left : frameRect.right();
                const frameY = frameRect.top;

                // the effective bounding rectangle of the connector line
                const x = Math.min(hintX, frameX);
                const y = Math.min(hintY, frameY);
                const w = Math.abs(hintX - frameX);
                const h = Math.abs(hintY - frameY);

                // set the position of the bounding box for the line
                $(connectorNode).css({ left: x, top: y }).attr({
                    width: w + 1,
                    height: h + 1,
                    viewBox: `0 0 ${w + 1} ${h + 1}`
                });

                // set the connector line formatting
                $(connectorNode.firstChild!).attr({
                    x1: frameX - x + 0.5,
                    y1: frameY - y + 0.5,
                    x2: hintX - x + 0.5,
                    y2: hintY - y + 0.5,
                    stroke: "black",
                    "stroke-width": 0.75
                });
            }
        }

        return true;
    }

    /**
     * Returns the drawing frame associated to the passed drawing model, if it
     * needs to be rendered, i.e. it covers the current layer rectangle of this
     * renderer, or it is selected (the latter is needed for move tracking to
     * be able to see the move box independent of the source position of the
     * drawing object).
     *
     * @param drawingModel
     *  The drawing model to be checked.
     *
     * @returns
     *  The drawing frame associated to the passed drawing model, if it needs
     *  to be rendered; otherwise `undefined`.
     */
    #getRenderDrawingFrame(drawingModel: SheetDrawingModelType): Opt<JQuery> {

        // the drawing frame manager responsible for the passed model
        const frameManager = this.#getFrameManager(drawingModel);

        // get drawing frame from collection
        const drawingFrame = frameManager.getDrawingFrame(drawingModel);
        if (!drawingFrame) { return undefined; }

        // do not render hidden drawing objects (but drawing frames that have been made visible temporarily)
        if (this.#isModelVisible(drawingModel)) {

            // resolve absolute location of the drawing object (do not render hidden drawing objects, e.g. in hidden columns/rows)
            const rectangle = drawingModel.getRectangle({ absolute: true });

            // add cell range to frame element for automated testing
            const range = drawingModel.getRange();
            if (range) { drawingFrame.attr("data-range", range.toOpStr()); }

            // check that the drawing frame is in the visible area of the grid pane, or selected
            if (rectangle && (this.layerRect?.overlaps(rectangle) || isSelected(drawingFrame))) { return drawingFrame; }
        }

        // hide drawing frame in the DOM
        drawingFrame[0].classList.add("hidden");
        return undefined;
    }

    /**
     * Refreshes the contents and formatting of the DOM drawing frame
     * associated to the specified drawing model.
     *
     * @param drawingModel
     *  The model of the drawing object to be rendered.
     */
    @renderLogger.profileMethod("$badge{DrawingRenderer} formatDrawingFrame")
    #formatDrawingFrame(drawingModel: SheetDrawingModelType): void {

        renderLogger.log(() => `uid=${drawingModel.uid} type=${drawingModel.drawingType} range=${drawingModel.getRange()}`);

        // immediately remove drawing model from the set of pending models
        this.#pendingFormatSet.delete(drawingModel);

        // do not render the drawing frame if it has been scrolled/moved outside in the meantime
        const drawingFrame = this.#getRenderDrawingFrame(drawingModel);
        if (!drawingFrame) { return; }

        // the model frame (drawing frame used as source for the text contents)
        const frameSettings = drawingModel.refreshModelFrame();
        if (!frameSettings) { return; }
        const modelFrame = frameSettings.drawingFrame;

        // whether to refresh the formatting or text contents of the drawing frame
        const dirtyFormat = !!drawingFrame.data("dirtyFormat");
        const dirtyText = !!drawingFrame.data("dirtyText");

        // remove the special display for uninitialized drawing frames, and the dirty markers
        drawingFrame.removeClass("uninitialized").data({ dirtyFormat: false, dirtyText: false });

        // text contents need to be copied for new drawing frames and for changed text contents
        const textFrame = dirtyText ? copyTextFrame(modelFrame, drawingFrame) : dirtyFormat ? getTextFrame(drawingFrame) : undefined;

        // reset all explicit CSS formatting of the textbox before formatting the drawing frame
        if (textFrame) {
            textFrame.css("transform", "");
        }

        // special handling for cell notes (TODO: treat them as regular text shapes)
        if (drawingModel instanceof NoteModel) {
            this.#updateNoteFrame(drawingModel, drawingFrame);
        }

        // update formatting of the drawing frame
        if (dirtyFormat) {
            renderLogger.takeTime("update formatting", () => {

                const attrSet = drawingModel.getMergedAttributeSet(true);
                updateFormatting(this.docApp, drawingFrame, attrSet, { skipFlipping: true });

                // if selected drawing contains adjustment values, reposition adjustment points
                if (isSelected(drawingFrame) && ("geometry" in attrSet) && (attrSet as ShapeAttributeSet).geometry.avList) {
                    this.#updateSelectionFrame(drawingFrame, true);
                }
            });
        }

        // update paragraph/character formatting of the text contents
        if ((drawingModel instanceof SheetShapeModel) && textFrame && frameSettings.textPosition) {
            renderLogger.takeTime("update text", () => {
                this.#updateTextFramePosition(drawingModel, textFrame, frameSettings.textPosition!);
                this.#getFrameManager(drawingModel).updateTextFormatting(drawingModel);
            });
        }

        // notify all listeners
        this.trigger("render:drawingframe", drawingFrame, drawingModel);
    }

    /**
     * Registers the passed drawing model for deferred rendering of its
     * formatting, and starts a background task on demand that refreshes
     * the formatting of all registered pending drawing frames.
     *
     * @param drawingModel
     *  The model of the drawing object to be rendered.
     *
     * @param [options]
     *  Optional parameters.
     */
    #formatDrawingFrameDebounced(drawingModel: SheetDrawingModelType, options?: FormatDrawingFrameOptions): void {

        // `immediate` option: immediately format the drawing, no background worker needed
        if (options?.immediate) {
            this.#formatDrawingFrame(drawingModel);
            return;
        }

        // insert the passed drawing model into the set, start background loop
        this.#pendingFormatSet.add(drawingModel);
        this.#startFormatBackgroundWorker();
    }

    /**
     * INTERNAL! Debounced processor for `_formatDrawingFrameDebounced()`.
     */
    @debounceAfterActionsMethod()
    #startFormatBackgroundWorker(): void {
        if (!this.#formatWorker.running && this.#pendingFormatSet.size) {
            jpromise.floating(this.#formatWorker.start());
        }
    }

    /**
     * Renders the DOM drawing frame that represents the passed drawing model.
     * The position and visibility of the drawing frame will be updated
     * immediately, the (expensive) formatting will be updated in a background
     * loop.
     *
     * @param drawingModel
     *  The drawing model instance whose drawing frame will be updated.
     *
     * @param [options]
     *  Optional parameters.
     */
    #renderDrawingFrame(drawingModel: SheetDrawingModelType, options?: RenderDrawingFrameOptions): void {

        // update cell overlay node of notes and comments (also for invisible notes!)
        if (isAnyComment(drawingModel)) {
            const visibleAnchor = this.#updateCellOverlayNode(drawingModel);
            if (!visibleAnchor) { return; }
        }

        // the DOM drawing frame (skip drawing objects that are not inside the viewport)
        const drawingFrame = this.#getRenderDrawingFrame(drawingModel);
        if (!drawingFrame) { return; }

        // settings for the model frame (drawing frame used as source for the text contents)
        const frameSettings = drawingModel.refreshModelFrame();
        if (!frameSettings) { return; }

        // copy explicit formatting attributes (expected by text framework, and drawing framework)
        setExplicitAttributeSet(drawingFrame, frameSettings.explicitAttrSet);

        const $canvasNode = getCanvasNode(drawingFrame);
        const oldCanvasWidth = ($canvasNode.length === 1) ? $canvasNode.width() : null;
        const oldCanvasHeight = ($canvasNode.length === 1) ? $canvasNode.height() : null;

        // old location of the drawing frame in pixels (unless it was hidden)
        const oldHidden = options?.hidden || drawingFrame.hasClass("hidden");
        const oldRectangle = oldHidden ? undefined : drawingFrame.data("rectangle") as Opt<Rectangle>;
        drawingFrame[0].classList.remove("hidden");

        // new location of the drawing frame in pixels (cannot be `null` due to preparations above)
        const newRectangle = drawingModel.getRectangle()!;
        drawingFrame.data("rectangle", newRectangle);

        // special display for new drawing frames that have not been formatted yet
        const formatSupported = supportsFormatting(drawingModel);
        if (!oldRectangle && formatSupported) { drawingFrame.addClass("uninitialized"); }

        // formatting and text contents need to be updated if specified, or if shown initially
        this.#updateDirtyFlags(drawingFrame, oldRectangle ? options : { format: true, text: true });

        // set current position and size of the drawing frame as CSS properties
        const cssRectangle = drawingModel.isEmbedded ? newRectangle : this.gridPane.convertToLayerRectangle(newRectangle);
        cssRectangle.left = math.clamp(cssRectangle.left, -MAX_NODE_SIZE, MAX_NODE_SIZE);
        cssRectangle.top = math.clamp(cssRectangle.top, -MAX_NODE_SIZE, MAX_NODE_SIZE);
        drawingFrame.css(cssRectangle.toCSS());

        // update flipping attributes manually
        const oldFlipH = isFlippedHorz(drawingFrame);
        const oldFlipV = isFlippedVert(drawingFrame);
        const newFlipH = drawingModel.isFlippedH();
        const newFlipV = drawingModel.isFlippedV();
        const isRotatable = drawingModel.isDeepRotatable();
        const flipData = drawingModel.getEffectiveFlipping();
        drawingFrame.toggleClass(FLIPPED_HORIZONTALLY_CLASSNAME, isRotatable ? newFlipH : flipData.flipH);
        drawingFrame.toggleClass(FLIPPED_VERTICALLY_CLASSNAME, isRotatable ? newFlipV : flipData.flipV);

        // apply rotation transformation
        const degrees = drawingModel.getRotationDeg();
        setCssTransform(drawingFrame, degrees, newFlipH, newFlipV);
        drawingFrame.toggleClass(ROTATED_DRAWING_CLASSNAME, degrees !== 0);

        // the selection needs to be redrawn, if the flip state of a connector drawing has changed
        if (((oldFlipH !== newFlipH) || (oldFlipV !== newFlipV)) && isSelected(drawingFrame) && isConnectorDrawingFrame(drawingFrame)) {
            this.#updateSelectionFrame(drawingFrame, true);
        }

        // the resulting dirty flags
        const zoomChanged = !!options?.zoom;
        const dirtyFormat = !!drawingFrame.data("dirtyFormat");
        const dirtyText = !!drawingFrame.data("dirtyText");

        // fast refresh of DOM positions and size while modifying or zooming visible drawings
        if (formatSupported && oldRectangle && (zoomChanged || dirtyFormat)) {

            // update position of the embedded canvas node for shapes
            if ($canvasNode.length === 1) {
                const widthScale = newRectangle.width / oldRectangle.width;
                const heightScale = newRectangle.height / oldRectangle.height;
                $canvasNode.css({
                    left: Math.round(parseCssLength($canvasNode, "left") * widthScale),
                    top: Math.round(parseCssLength($canvasNode, "top") * heightScale),
                    width: Math.round(oldCanvasWidth! * widthScale),
                    height: Math.round(oldCanvasHeight! * heightScale)
                });
            }

            // set correct padding of the text frame according to current zoom factor
            if ((drawingModel instanceof SheetShapeModel) && frameSettings.textPosition) {
                withTextFrame(drawingFrame, textFrame => this.#updateTextFramePosition(drawingModel, textFrame, frameSettings.textPosition!));
            }
        }

        // immediately notify all listeners after the position or size has been changed
        this.trigger("render:drawingframe", drawingFrame, drawingModel);

        // start deferred update of formatting and text contents
        if (formatSupported && (dirtyFormat || dirtyText)) {
            this.#formatDrawingFrameDebounced(drawingModel, options);
        } else {
            this.#pendingFormatSet.delete(drawingModel);
        }

        // propagate dirty formatting to all embedded frames
        if (oldHidden) { options = { ...options, hidden: true }; }
        if (dirtyFormat) { options = { ...options, format: true }; }

        // update existing embedded drawing frames too
        drawingModel.forEachChildModel(childModel => this.#renderDrawingFrame(childModel, options));
    }

    /**
     * Registers the passed drawing models for deferred rendering, if the
     * document is currently processing operations.
     *
     * This prevents flicker effects when the drawing objects will be touched
     * multiple times, e.g. while moving columns/rows in the sheet where the
     * drawing objects will be rendered by the "changeDrawing" operations, and
     * when rerendering the entire drawing layer due to the move operation.
     *
     * @param drawingModelSource
     *  A single drawing model, or a data source with multiple drawing models
     *  (an array, a dictionary, a map, or an iterator) to be rendered.
     *
     * @param [options]
     *  Optional parameters.
     */
    #renderDrawingFramesDebounced(drawingModelSource: DrawingModelSource, options?: RenderDrawingFrameOptions): void {

        // convert single drawing model to array
        if (drawingModelSource instanceof DrawingModel) {
            drawingModelSource = [drawingModelSource];
        }

        // debounce synchronous rendering of the frame position while processing operations
        const isProcessing = this.docModel.isProcessingActions();
        for (const drawingModel of drawingModelSource) {
            if (isProcessing) {
                map.update(this.#pendingRenderMap, drawingModel, entry => {
                    if (!entry) { return { model: drawingModel, options }; }
                    if (options) { entry.options = { ...entry.options, ...options }; }
                    return entry;
                });
            } else {
                this.#renderDrawingFrame(drawingModel, options);
            }
        }

        // process the cached drawing models debounced
        this.#renderPendingDrawingFramesDebounced();
    }

    /**
     * INTERNAL! Debounced processor for `_renderDrawingFramesDebounced()`.
     */
    @debounceAfterActionsMethod()
    #renderPendingDrawingFramesDebounced(): void {
        for (const entry of map.shiftValues(this.#pendingRenderMap)) {
            this.#renderDrawingFrame(entry.model, entry.options);
        }
    }

    /**
     * Creates and inserts new DOM drawing frame nodes that represent the
     * passed drawing models. The position and visibility of the drawing frames
     * will be initialized immediately, the (expensive) formatting will be
     * updated in a background loop.
     *
     * @param drawingModelSource
     *  A single drawing model, or a data source with multiple drawing models
     *  (an array, a dictionary, a map, or an iterator).
     */
    #createDrawingFrames(drawingModelSource: DrawingModelSource): void {

        // new cell overlay nodes to be inserted
        const overlayFragment = document.createDocumentFragment();
        // new connector line nodes to be inserted
        const connectorFragment = document.createDocumentFragment();
        // drawing models collected from the data source (may be a one-way iterator)
        const drawingModels: SheetDrawingModelType[] = [];

        // convert single drawing model to array
        if (drawingModelSource instanceof DrawingModel) {
            drawingModelSource = [drawingModelSource];
        }

        // create the drawing frame DOM elements, collect additional markup
        for (const drawingModel of drawingModelSource) {

            // the drawing frame manager responsible for the passed model
            const frameManager = this.#getFrameManager(drawingModel);
            // the new drawing representation, as jQuery object
            const drawingFrame = frameManager.createDrawingFrame(drawingModel)?.[0];
            if (!drawingFrame) { return; }

            // collect all drawing models from the generic data source
            this.#drawingModelMap.set(drawingFrame, drawingModel);
            drawingModels.push(drawingModel);

            // create additional DOM nodes for comments
            if (isAnyComment(drawingModel)) {

                // whether the passed model is a cell note
                const isNote = drawingModel instanceof NoteModel;

                // create the cell overlay node
                const overlayNode = createDiv(isNote ? "note" : "thread");
                overlayFragment.appendChild(overlayNode);
                this.#overlayNodeMap.set(drawingModel, overlayNode);
                this.#overlayModelMap.set(overlayNode, drawingModel);

                // create the inner markup for the cell overlay node
                overlayNode.appendChild(createDiv("hint"));

                // create the connector line for cell notes, or the bubble container for comments
                if (isNote) {
                    const connectorNode = createSvg("svg");
                    connectorNode.appendChild(createSvg("line"));
                    connectorFragment.appendChild(connectorNode);
                    this.#connectorNodeMap.set(drawingModel, connectorNode);
                } else {
                    // container for a bubble icon next to the hint node
                    overlayNode.appendChild(createDiv("bubble skip-tracking"));
                }
            }
        }

        // add the new cell overlay nodes into the overlay layer node
        this.#overlayLayerNode.append(overlayFragment);
        // add the new connector line nodes into the connector layer node
        this.#connectorLayerNode.append(connectorFragment);

        // initially render the drawing frame
        const options: UpdateDrawingFrameOptions = { format: true, text: true };
        for (const drawingModel of drawingModels) {
            this.#renderDrawingFrame(drawingModel, options);
        }
    }

    /**
     * Removes the drawing frame that represents the passed drawing model.
     *
     * @param drawingModel
     *  The drawing model instance to remove the DOM drawing frame for.
     */
    #removeDrawingFrame(drawingModel: SheetDrawingModelType): void {

        // the drawing frame manager responsible for the passed model
        const frameManager = this.#getFrameManager(drawingModel);

        // removes the drawing frame and all embedded child frames from the renderer
        const removeDrawingFrameFromMaps = (parentModel: SheetDrawingModelType): void => {

            // the drawing frame to be removed
            const drawingFrame = frameManager.getDrawingFrame(parentModel)?.[0];
            if (!drawingFrame) { return; }

            // remove the drawing model form the maps
            this.#drawingModelMap.delete(drawingFrame);
            this.#pendingRenderMap.delete(parentModel);
            this.#pendingFormatSet.delete(parentModel);

            // remove the cell overlay nodes for notes/comments
            if (isAnyComment(parentModel)) {
                map.visit(this.#overlayNodeMap, parentModel, overlayNode => {
                    this.#overlayNodeMap.delete(parentModel);
                    this.#overlayModelMap.delete(overlayNode);
                    overlayNode.remove();
                });
            }

            // remove note connector lines
            if (parentModel instanceof NoteModel) {
                map.visit(this.#connectorNodeMap, parentModel, connectorNode => {
                    this.#connectorNodeMap.delete(parentModel);
                    connectorNode.remove();
                });
            }

            // do not try to access temporarily shown drawing frames after deletion
            this.#tempShowModelSet.delete(parentModel);
            if (parentModel === this.#hoverNoteModel) { this.#hoverNoteModel = undefined; }

            // remove the drawing frames of all embedded drawing models
            parentModel.forEachChildModel(removeDrawingFrameFromMaps);
        };

        // remove the passed drawing frame (will remove all child frames recursively)
        removeDrawingFrameFromMaps(drawingModel);

        // remove the drawing frame from the manager (processes child objects internally)
        frameManager.removeDrawingFrame(drawingModel);
    }

    /**
     * Updates the comment bubble icon attached to the selected cell.
     */
    @debounceMethod({ delay: "animationframe" })
    #renderCommentBubbleDebounced(): void {

        // hide the old comment pop-up menu
        this.#commentPopup.hide();

        // remove the old comment bubble icon
        if (this.#commentBubbleNode) {
            detachChildren(this.#commentBubbleNode);
            this.#commentBubbleNode = undefined;
        }

        // resolve the comment thread from the active cell
        const commentThread = this.docView.commentCollection.getByAddress(this.docView.selectionEngine.getActiveCell());
        if (!commentThread) { return; }

        // find the cell overlay node (containing the cell hint, and comment bubble icon)
        const commentModel = commentThread[0];
        const overlayNode = this.#overlayNodeMap.get(commentModel);
        if (!overlayNode) { return; }

        // create a pop-up menu on small devices
        if (SMALL_DEVICE) {
            const commentFrame = this.docView.commentsPane.detachOrCreateCommentFrame(commentModel);
            if (commentFrame) {
                this.#commentPopup.setCommentFrame(commentFrame);
                this.#commentPopup.setAnchor(overlayNode);
                this.#commentPopup.show();
            }
            return;
        }

        // create the bubble icon node (large devices)
        const iconWrapperNode = createDiv("icon-bubble-wrapper");
        const iconId = (commentThread.length > 1) ? "svg:comment-reply-o" : "svg:comment-o";
        const iconSize = Math.max(12, Math.round(18 * this.docView.getZoomFactor()));
        iconWrapperNode.appendChild(createIcon(iconId, iconSize));
        // bug 67559: issue with click event on <svg> in IE11
        iconWrapperNode.appendChild(createDiv("icon-bubble-tapper"));

        // add the icon wrapper nodes to the bubble root
        this.#commentBubbleNode = overlayNode.querySelector<HTMLElement>(".bubble")!;
        this.#commentBubbleNode.appendChild(iconWrapperNode);

        // add CSS class to give the icon a specific theme color
        const authorColorId = this.docApp.getAuthorColorIndex(commentModel.getAuthor());
        this.#commentBubbleNode.dataset.schemeColor = String(authorColorId);

        // if the cell is in the first visible row, the bubble icon must shift down
        const firstIndex = this.gridPane.rowHeaderPane.getFirstVisibleIndex();
        this.#commentBubbleNode.classList.toggle("t", commentModel.getAnchor().r === firstIndex);
    }

    /**
     * Updates the selection of all drawing frames, and triggers the event
     * "render:drawingselection", if the drawing selection has changed.
     */
    @debounceMethod({ delay: "animationframe" })
    #renderDrawingSelectionDebounced(): void {

        // all drawing frames currently selected in the DOM
        const oldSelectedFrames = this.#drawingFrameManager.getSelectedDrawingFrames();
        // all drawing frames to be selected in the DOM, according to current selection state
        const newSelectedFrames = this.getDrawingFrames(this.docView.selectionEngine.getSelectedDrawings());

        // create or update the selection frame for all selected drawings
        for (const drawingNode of newSelectedFrames.get()) {
            this.#updateSelectionFrame($(drawingNode));
        }

        // remove selection border from all drawing frames not selected anymore
        for (const drawingNode of oldSelectedFrames.not(newSelectedFrames).get()) {
            clearSelection(drawingNode);
        }

        // notify listeners
        if (!ary.equals(oldSelectedFrames.get(), newSelectedFrames.get())) {
            this.trigger("render:drawingselection", newSelectedFrames);
        }

        // render the comment bubble into the selected cell
        this.#renderCommentBubbleDebounced();
    }

    /**
     * Updates the remote selection of all drawing frames.
     */
    @debounceMethod({ delay: 100 })
    #renderRemoteSelectionDebounced(): void {

        // nothing to do, if all columns/rows are hidden (method may be called debounced)
        if (!SHOW_REMOTE_SELECTIONS || !this.gridPane.isVisible()) { return; }

        // the drawing collection of the active sheet
        const { drawingCollection } = this.docView;

        // remove the old remote selection nodes from the drawing frames
        for (const drawingFrame of this.#remoteDrawingFrames) {
            drawingFrame.removeAttr("data-remote-user data-remote-color").children(".remote-selection").remove();
        }
        this.#remoteDrawingFrames.length = 0;

        // generate selections of all remote users on the same sheet
        for (const { userName, colorIndex, drawings } of this.docView.remoteSelections("drawing")) {

            // add a remote selection node to all selected drawings of the user
            for (const position of drawings) {

                // bug 61811: remote selection may be outdated (deleted drawings)
                const drawingModel = drawingCollection.getModel(position);
                const drawingFrame = drawingModel && this.#drawingFrameManager.getDrawingFrame(drawingModel);
                if (!drawingFrame) { return; }

                // do not insert multiple selection nodes into a drawing frame
                if (drawingFrame.children(".remote-selection").length > 0) { return; }

                // create the child node to display the border around the frame, and the data attributes for the user badge
                const dataset = { remoteUser: _.noI18n(userName), remoteColor: colorIndex };
                const selectionNode = createDiv({ classes: "remote-selection", dataset });
                this.#remoteDrawingFrames.push(drawingFrame.prepend(selectionNode));
            }
        }
    }

    /**
     * Shows the cell note frame associated to the passed note model
     * temporarily while its anchor cell is hovered with the mouse pointer.
     *
     * @param noteModel
     *  The note model to be shown temporarily.
     */
    #showHoverNote(noteModel: NoteModel): void {

        // nothing to be done, if the cell note is permanently visible
        if (!noteModel.isVisible()) {
            this.#hoverNoteTimer = this.setTimeout(() => {
                this.#hoverNoteModel = noteModel;
                this.#renderDrawingFrame(noteModel, { immediate: true });
            }, 500);
        }
    }

    /**
     * Hides the cell note frame that has been made visible with the method
     * `showHoverNote()`.
     */
    #hideHoverNote(): void {

        // abort the delay timer
        this.#hoverNoteTimer?.abort();
        this.#hoverNoteTimer = undefined;

        // hide the cell note
        const noteModel = this.#hoverNoteModel;
        this.#hoverNoteModel = undefined;
        if (noteModel) { this.#renderDrawingFrame(noteModel); }
    }
}
