/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { dict } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE, createDiv, detachChildren, replaceChildren } from "@/io.ox/office/tk/dom";
import { debounceMethod } from "@/io.ox/office/tk/objects";

import { SHOW_REMOTE_SELECTIONS } from "@/io.ox/office/spreadsheet/utils/config";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { isLeadingDir, isVerticalDir } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { GridPane } from "@/io.ox/office/spreadsheet/view/pane/gridpane";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { BaseRenderer } from "@/io.ox/office/spreadsheet/view/render/layer/baserenderer";

// types ======================================================================

/**
 * Type mapping for the events emitted by `SelectionRenderer` instances.
 */
export interface SelectionRendererEventMap {

    /**
     * Will be emitted after the current cell selection has actually been
     * rendered into the DOM selection layer node (rendering may happen
     * debounced after several "change:selection" view events).
     */
    "render:cellselection": [];
}

// class SelectionRenderer ====================================================

/**
 * Renders the own selection, and the selections of remote users into the DOM
 * layers of a single grid pane.
 *
 * @param gridPane
 *  The grid pane instance that owns this selection layer renderer.
 */
export class SelectionRenderer extends BaseRenderer<SelectionRendererEventMap> {

    // the remote layer node for displaying selections of other users
    readonly #remoteLayerNode: HTMLDivElement;

    // the selection layer (container for the selected ranges)
    readonly #selectionLayerNode: HTMLDivElement;

    // constructor ------------------------------------------------------------

    constructor(gridPane: GridPane) {
        super(gridPane);

        // create the layer DOM nodes
        this.#remoteLayerNode = gridPane.createLayerNode("remote-layer");
        this.#selectionLayerNode = gridPane.createLayerNode("selection-layer");

        // render cell selection again when document edit mode changes
        this.listenToWhenVisible(this.docApp, "docs:editmode", this.#renderCellSelection);

        // render cell selection after merging/unmerging cell ranges (size of active cell may change)
        this.listenToWhenVisible(this.docView, "merge:cells", this.#renderCellSelectionDebounced);

        // update layers according to changed view properties
        this.listenToViewPropWhenVisible("remoteClients", this.#renderRemoteSelection);

        // render cell selection (triggers a "render:cellselection" event)
        this.listenToWhenVisible(this.docView, "change:sheetprops", ({ newProps }) => {
            if (newProps.selection || dict.someKey(newProps, key => key.startsWith("split"))) {
                this.#renderCellSelection();
            }
        });

        // render cell selection in auto-fill mode (triggers a "render:cellselection" event)
        this.listenToViewPropWhenVisible("autoFillData", this.#renderCellSelection);

        // trigger a "render:cellselection" event if active pane changes
        // (no actual rendering needed, border colors will change via CSS)
        this.listenToSheetPropWhenVisible("activePane", () => this.trigger("render:cellselection"));

        // post-processing after scrolling
        this.listenTo(gridPane, "change:scrollpos", this.#updateResizeHandlePositions);
    }

    // protected methods ------------------------------------------------------

    /**
     * Changes the layers according to the passed layer range.
     */
    @renderLogger.profileMethod("$badge{SelectionRenderer} implSetLayerRange")
    protected implSetLayerRange(): void {

        // render own selection, and selections of all remote editors
        this.#renderCellSelection();
        this.#renderRemoteSelection();
    }

    /**
     * Resets this renderer, clears the DOM layer nodes.
     */
    protected implHideLayerRange(): void {
        detachChildren(this.#selectionLayerNode);
        detachChildren(this.#remoteLayerNode);
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the position of the auto-fill handle and the resize handles
     * for cell selection, when entire columns or rows are selected.
     */
    #updateResizeHandlePositions(): void {

        // converts the passed offset in the visible area to an offset relative to 'parentNode'
        const setRelativeOffset = (targetNode: HTMLElement, offsetName: "top" | "left", visibleOffset: number): void => {
            const parentPos = $(targetNode).closest(".range").position();
            const offset = visibleRect[offsetName] - this.layerRect![offsetName] - parentPos[offsetName] + visibleOffset;
            targetNode.style[offsetName] = `${offset}px`;
        };

        // the visible area of the grid pane
        const visibleRect = this.gridPane.getVisibleRectangle();

        // adjust position of the auto-fill handle node for entire column or row selection
        const autoFillHandleNode = this.#selectionLayerNode.querySelector<HTMLElement>(".autofill.resizers>[data-pos]");
        switch (autoFillHandleNode?.dataset.pos) {
            case "r": setRelativeOffset(autoFillHandleNode, "top", 0);  break;
            case "b": setRelativeOffset(autoFillHandleNode, "left", 0); break;
            default:  break;
        }

        // adjust position of the selection handle node for entire column or row selection
        $(this.#selectionLayerNode).find(".select.resizers>[data-pos]").get().forEach(handleNode => {
            switch (handleNode.dataset.pos) {
                case "l":
                case "r": setRelativeOffset(handleNode, "top", visibleRect.height / 2); break;
                case "t":
                case "b": setRelativeOffset(handleNode, "left", visibleRect.width / 2); break;
                default:  break;
            }
        });
    }

    /**
     * Renders all visible selection ranges according to the current pane
     * layout data received from a view layout update notification.
     */
    #renderCellSelection(): void {

        // nothing to do in chart sheets etc.
        if (!this.docView.sheetModel.isCellType) { return; }

        // nothing to do, if all columns/rows are hidden (method may be called debounced)
        if (!this.gridPane.isVisible()) {
            detachChildren(this.#selectionLayerNode);
            return;
        }

        // type of a cell range address with additional properties
        interface ExtRange extends Range {
            active?: boolean;
            tracking?: boolean;
            autoFillDelete?: boolean;
        }

        // the address factory needed to check for column/row ranges
        const { addressFactory } = this.docModel;
        // the entire cell selection
        const selection = this.docView.selectionEngine.getSelection();
        // the selected ranges as `ExtRange` array
        const ranges = selection.ranges as ExtRange[];
        // the range occupied by the active cell (will not be filled)
        let activeRangeSpec: Range | "delete" | null = null;

        // add `active` flag to range address of active range
        ranges[selection.active].active = true;

        // adjust single range for auto-fill tracking
        const autoFillData = this.docView.propSet.get("autoFillData");
        if ((ranges.length === 1) && autoFillData) {

            // the selected cell range
            const firstRange = ranges[0];
            // auto-fill direction and count
            const fillDir = autoFillData.direction;
            const fillCount = autoFillData.count;
            // whether to expand/shrink columns or rows
            const vertical = isVerticalDir(fillDir);
            // whether to expand/shrink the leading or trailing border
            const leading = isLeadingDir(fillDir);

            activeRangeSpec = firstRange.clone();
            if (fillCount >= 0) {
                // adjust range for auto-fill mode
                if (leading) {
                    firstRange.a1.move(-fillCount, !vertical);
                } else {
                    firstRange.a2.move(fillCount, !vertical);
                }
            } else {
                // adjust range for deletion mode
                firstRange.autoFillDelete = true;
                if (-fillCount < firstRange.size(!vertical)) {
                    // range partly covered
                    if (leading) {
                        activeRangeSpec.a1.move(-fillCount, !vertical);
                    } else {
                        activeRangeSpec.a2.move(fillCount, !vertical);
                    }
                } else {
                    // special marker for deletion of the entire range
                    activeRangeSpec = "delete";
                }
            }

            // add tracking style effect
            firstRange.tracking = true;
        }

        // the range covered by the active cell (or any other range in auto-fill tracking mode)
        const activeRange = (activeRangeSpec === "delete") ? null : (activeRangeSpec ?? this.docView.mergeCollection.expandRangeToMergedRanges(new Range(selection.address)));

        // convert active range to a rectangle relative to the layer root node
        const iterResult = this.gridPane.renderRanges(activeRange, { alignToGrid: true }).next();
        const activeRectangle = iterResult.done ? undefined : iterResult.value.rectangle;

        // partial record of CSS position attributes
        type PositionSpec = PtRecord<"left" | "right" | "top" | "bottom" | "width" | "height", number>;

        // creates and appends a fill element with specific position
        const appendFillNode = (parentNode: HTMLElement, pos: PositionSpec): void => {
            const fillNode = parentNode.appendChild(createDiv("fill"));
            dict.forEach(pos, (value, key) => (fillNode.style[key] = `${value}px`));
        };

        // creates and appends a resizer handle element according to the type of the passed range
        const appendResizerNode = (parentNode: HTMLElement, range: Range, leadingCorner: boolean): void => {
            const hPos = leadingCorner ? "l" : "r", vPos = leadingCorner ? "t" : "b";
            const pos = addressFactory.isColRange(range) ? hPos : addressFactory.isRowRange(range) ? vPos : (vPos + hPos);
            parentNode.appendChild(createDiv({ dataset: { pos } }));
        };

        // hide left/top border of selection ranges, if first column/row is hidden
        const firstColHidden = !this.docView.colCollection.isVisibleIndex(0);
        const firstRowHidden = !this.docView.rowCollection.isVisibleIndex(0);

        // whether the active cell touches the borders of selection ranges
        const activeBorders = dict.fillRec(["left", "top", "right", "bottom"], false);

        // fragment for all range elements
        const rangesFragment = document.createDocumentFragment();
        // fragment for the active cell element (must be the last element)
        const activeFragment = document.createDocumentFragment();

        // render all ranges that are visible in this grid pane
        for (const { range, rectangle, index } of this.gridPane.renderRanges(ranges, { alignToGrid: true })) {

            // hide left/top border of the range, if the first column/row is hidden
            const expandL = (firstColHidden && (range.a1.c === 0)) ? 5 : 0;
            const expandT = (firstRowHidden && (range.a1.r === 0)) ? 5 : 0;
            rectangle.expandSelf(expandL, expandT, 0, 0);

            // create a range element
            const { active, tracking, autoFillDelete } = range as ExtRange;
            const collapsed = (rectangle.width <= 1) || (rectangle.height <= 1);
            const classes = { range: true, active, collapsed, "tracking-active": tracking, "autofill-delete": autoFillDelete };
            const dataset = { index, ...addressFactory.getRangeDataAttrs(range) };
            const rangeNode = createDiv({ classes, dataset, style: rectangle.toCSS() });

            // insert the semi-transparent fill elements (never cover the active range with the fill elements)
            if (activeRange && activeRectangle && range.overlaps(activeRange)) {

                // initialize position of active cell, relative to selection range
                const relActiveRect = activeRectangle.clone();
                relActiveRect.left -= rectangle.left;
                relActiveRect.top -= rectangle.top;
                const rightDist = rectangle.width - relActiveRect.right();
                const bottomDist = rectangle.height - relActiveRect.bottom();

                // insert fill element above active cell
                if (relActiveRect.top > 0) {
                    appendFillNode(rangeNode, { left: 0, right: 0, top: 0, height: relActiveRect.top });
                }

                // insert fill element left of active cell
                if (relActiveRect.left > 0) {
                    appendFillNode(rangeNode, { left: 0, width: relActiveRect.left, top: relActiveRect.top, height: relActiveRect.height });
                }

                // insert fill element right of active cell
                if (rightDist > 0) {
                    appendFillNode(rangeNode, { left: relActiveRect.right(), right: 0, top: relActiveRect.top, height: relActiveRect.height });
                }

                // insert fill element below active cell
                if (bottomDist > 0) {
                    appendFillNode(rangeNode, { left: 0, right: 0, top: relActiveRect.bottom(), bottom: 0 });
                }

                // update border flags for the active cell
                activeBorders.left   ||= range.a1.c === activeRange.a1.c;
                activeBorders.top    ||= range.a1.r === activeRange.a1.r;
                activeBorders.right  ||= range.a2.c === activeRange.a2.c;
                activeBorders.bottom ||= range.a2.r === activeRange.a2.r;

            } else {
                rangeNode.appendChild(createDiv("abs fill"));
            }

            // add border element for the selection range
            rangeNode.appendChild(createDiv("border"));

            // additional mark-up for single-range selection (DOCS-4967: not if entire range is hidden)
            if ((ranges.length === 1) && !this.docView.sheetModel.areRangesHidden(ranges[0])) {
                if (TOUCH_DEVICE && !autoFillData) {
                    // add resize handlers for selection on touch devices
                    const resizersNode = rangeNode.appendChild(createDiv("select resizers"));
                    appendResizerNode(resizersNode, range, true);
                    appendResizerNode(resizersNode, range, false);
                } else {
                    // add the auto-fill handler in the bottom right corner of the selection
                    const resizersNode = rangeNode.appendChild(createDiv("autofill resizers"));
                    appendResizerNode(resizersNode, range, false);
                }
            }

            // add the range element to the appropriate fragment
            (active ? activeFragment : rangesFragment).appendChild(rangeNode);
        }

        // additions for the active cell (or active range in auto-fill)
        if (activeRectangle) {
            const cellNode = rangesFragment.appendChild(createDiv({ classes: "active-cell", style: activeRectangle.toCSS() }));
            // add thin border for active cell on top of the selection (only for cell border lines NOT covering range border)
            dict.forEach(activeBorders, (isBorder, borderName) => isBorder || cellNode.classList.add(borderName));
        }

        // insert all fragments into the selection container node
        replaceChildren(this.#selectionLayerNode, rangesFragment, activeFragment);

        // update the visibility/position of additional DOM elements
        this.#updateResizeHandlePositions();

        // notify listeners for additional rendering depending on the selection
        this.trigger("render:cellselection");
    }

    /**
     * Debounced version of the method `renderCellSelection()`.
     */
    @debounceMethod({ delay: 100 })
    #renderCellSelectionDebounced(): void {
        this.#renderCellSelection();
    }

    /**
     * Renders the selections of all remote users which have the same sheet
     * activated.
     */
    #renderRemoteSelection(): void {

        // nothing to do, if all columns/rows are hidden (method may be called debounced)
        if (!SHOW_REMOTE_SELECTIONS || !this.gridPane.isVisible()) {
            detachChildren(this.#remoteLayerNode);
            return;
        }

        // the address factory needed to check for column/row ranges
        const { addressFactory } = this.docModel;
        // top-left visible address of the grid pane, to detect if the name-layer outside of the view
        const firstRow = this.gridPane.getTopLeftAddress().r;
        // the fragment with all range elements
        const fragment = document.createDocumentFragment();

        // generate selections of all remote users on the same sheet
        for (const { userName, colorIndex, ranges } of this.docView.remoteSelections("cell")) {

            // render all ranges of the remote user
            for (const { range, rectangle } of this.gridPane.renderRanges(ranges, { alignToGrid: true })) {
                // bug 35604: put name badge inside range if on top of the grid pane
                const classes = { range: true, "remote-badge-outside": range.a1.r !== firstRow };
                const dataset = { ...addressFactory.getRangeDataAttrs(range), remoteUser: _.noI18n(userName), remoteColor: colorIndex };
                const rangeNode = fragment.appendChild(createDiv({ classes, dataset, style: rectangle.toCSS() }));
                rangeNode.appendChild(createDiv("fill"));
            }
        }

        // insert entire HTML mark-up into the container node
        replaceChildren(this.#remoteLayerNode, fragment);
    }
}
