/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Rectangle } from "@/io.ox/office/tk/dom";
import type { Canvas2DContext } from "@/io.ox/office/tk/canvas";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderCache, RenderTextCellModel } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// class CanvasTextRenderer ===================================================

/**
 * Renderer for the cell text contents.
 */
export class CanvasTextRenderer extends CanvasBaseRenderer {

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasTextRenderer} render")
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        const { renderRange, canvasLayerRect } = settings;

        // Collect text cells separated by writing direction, to be able to render BiDi text correctly.
        // In order to prevent changing that attribute per text cell (performance!), texts will be
        // rendered separated by writing direction, resulting in two DOM modifications only.
        const ltrRenderModels: RenderTextCellModel[] = [];
        const rtlRenderModels: RenderTextCellModel[] = [];
        for (const renderModel of cache.yieldTextCellModels(renderRange)) {
            const renderModels = renderModel.renderText.font.rtl ? rtlRenderModels : ltrRenderModels;
            renderModels.push(renderModel);
        }

        // render text cells separated by writing direction
        this.#renderTexts(context, cache, canvasLayerRect, ltrRenderModels, false);
        this.#renderTexts(context, cache, canvasLayerRect, rtlRenderModels, true);
    }

    // private methods --------------------------------------------------------

    #renderTexts(context: Canvas2DContext, cache: RenderCache, canvasRect: Rectangle, renderModels: readonly RenderTextCellModel[], rtl: boolean): void {
        if (!renderModels.length) { return; }

        // ensure to render the text at font baseline position
        context.setFontStyle({ align: "left", baseline: "alphabetic", rtl });

        // draw the texts and additional decorations into the canvas
        for (const renderModel of renderModels) {

            const { renderText } = renderModel;
            const textRect = renderText.rect;

            // initialize all settings for canvas rendering (text will be rendered in fill mode, without outline)
            context.setFillStyle(renderText.fill).setFontStyle(renderText.font);

            // determine left clipping boundary
            let clipL = textRect.left;
            if (renderText.overflowL) {
                const clipEntry = cache.findAdjacentTextClipEntry(renderModel, true);
                clipL = clipEntry?.renderRect.right() ?? canvasRect.left;
            }

            // determine right clipping boundary
            let clipR = textRect.right();
            if (renderText.overflowR) {
                const clipEntry = cache.findAdjacentTextClipEntry(renderModel, false);
                clipR = clipEntry?.renderRect.left ?? canvasRect.right();
            }

            // initialize clipping rectangle for the text
            const clipRect = new Rectangle(clipL, textRect.top, clipR - clipL, textRect.height);
            context.clip(clipRect, () => {

                // draw the text lines (or single words in justified alignment mode)
                for (const lineDesc of renderText.lines) {
                    if (lineDesc.words) {
                        for (const wordDesc of lineDesc.words) {
                            context.drawText(wordDesc.text, lineDesc.x + wordDesc.x, lineDesc.y, "fill");
                        }
                    } else {
                        context.drawText(lineDesc.text, lineDesc.x, lineDesc.y, "fill");
                    }
                }

                // render text decoration
                if (renderText.decoration) {
                    context.setLineStyle(renderText.decoration.line);
                    context.drawPath(renderText.decoration.path, "stroke");
                }
            });
        }
    }
}
