/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Rectangle } from "@/io.ox/office/tk/dom";
import type { Canvas2DContext } from "@/io.ox/office/tk/canvas";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";

// types ======================================================================

/**
 * Layout settings passed to the method `CanvasBaseRenderer::render`.
 */
export interface CanvasRenderSettings {

    /**
     * The address of the cell range to be rendered.
     */
    readonly renderRange: Range;

    /**
     * The clipping rectangle (according to "renderRange"), in canvas
     * coordinates.
     */
    readonly canvasClipRect: Rectangle;

    /**
     * The position and size of the (entire) visible canvas area in the grid
     * pane, in CSS coordinates.
     */
    readonly cssLayerRect: Rectangle;

    /**
     * The position and size of the (entire) visible canvas area in the grid
     * pane, in canvas coordinates.
     */
    readonly canvasLayerRect: Rectangle;

    /**
     * The position and size of the entire active sheet, in CSS coordinates.
     */
    readonly cssSheetRect: Rectangle;

    /**
     * The position and size of the entire active sheet, in canvas coordinates.
     */
    readonly canvasSheetRect: Rectangle;

    /**
     * Whether fast mode is active (fast partially rendering during scrolling).
     */
    readonly fastMode: boolean;
}

/**
 * Type of a helper function for drawing horizontal or vertical lines into the
 * canvas. Parameter order is:
 *
 * - Coordinate in drawing direction (e.g. X for vertical lines).
 * - Start coordinate (e.g. Y1 for vertical lines).
 * - End coordinate (e.g. Y2 for vertical lines).
 */
export type DrawCanvasLineFn = FuncType<void, [number, number, number]>;

// class CanvasBaseRenderer ===================================================

/**
 * Base class for canvas layer renderers that implement rendering a specific
 * subset of the cell canvas.
 */
export abstract class CanvasBaseRenderer extends ViewObject<SpreadsheetView> {

    // public methods ---------------------------------------------------------

    /**
     * Sub classes implement rendering a subset of the current sheet contents
     * into the specified canvas rendering context.
     *
     * @param context
     *  The rendering context of the cell canvas.
     *
     * @param cache
     *  The rendering cache of the active sheet.
     *
     * @param settings
     *  Additional useful settings for rendering.
     */
    abstract render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void;
}
