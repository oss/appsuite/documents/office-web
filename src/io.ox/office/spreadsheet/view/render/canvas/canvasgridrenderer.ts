/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Canvas2DContext } from "@/io.ox/office/tk/canvas";

import { Color } from "@/io.ox/office/editframework/utils/color";

import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";

import { renderLogger, getCanvasCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, type DrawCanvasLineFn, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// constants ==================================================================

/**
 * Automatic grid color (dark gray instead of black, to make it visible on
 * white and black backgrounds.
 */
const AUTO_GRID_COLOR = Color.fromRgb("666666");

// class CanvasGridRenderer ===================================================

/**
 * Renderer for the cell grid lines.
 */
export class CanvasGridRenderer extends CanvasBaseRenderer {

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasGridRenderer} render")
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        // nothing to do, if grid lines are not enabled in the active sheet
        const { sheetModel } = cache;
        if (!sheetModel.isGridVisible()) { return; }

        const { renderRange, cssLayerRect } = settings;

        // the current grid color
        const gridColor = sheetModel.getGridColor();
        // the path object containing all grid line positions
        const path = context.createPath();

        // add all grid lines to the path
        for (const columns of [false, true]) {

            // start index of the range to be rendered
            const index0 = renderRange.a1.get(columns);
            // the column/row collection in the other direction (for intervals of merged ranges)
            const collection = columns ? sheetModel.rowCollection : sheetModel.colCollection;
            // layer start offset in rendering direction
            const layerOffset = columns ? cssLayerRect.top : cssLayerRect.left;
            // layer size in rendering direction
            let layerSize = columns ? cssLayerRect.height : cssLayerRect.width;
            // function to push a line to the path
            const pushLineFunc: DrawCanvasLineFn = columns ?
                (x, y1, y2) => path.pushLine(x, y1, x, y2) :
                (y, x1, x2) => path.pushLine(x1, y, x2, y);

            // restrict layer size to sheet size
            layerSize = Math.min(layerSize, collection.getTotalSize({ pixel: true }) - layerOffset);

            // add the grid lines for all visible columns/rows to the path
            for (const { index, renderPos } of cache.yieldHeaderModels(renderRange, columns)) {

                // collect the column/row intervals of the merged ranges that will break the grid line
                const mergedIntervals = new IntervalArray();
                const lineRange = renderRange.lineRange(columns, index - index0);
                for (const mergedData of cache.yieldMergedRangeData(lineRange)) {
                    if (index < mergedData.shrunken.a2.get(columns)) {
                        mergedIntervals.push(mergedData.shrunken.interval(!columns));
                    }
                }

                // calculate the line start/end coordinates of the merged range gaps
                const lineCoords = [];
                for (const interval of mergedIntervals.merge()) {
                    const { offset, size } = collection.getIntervalPosition(interval, { pixel: true });
                    lineCoords.push(offset, offset + size);
                }

                // add start and end position of the layer rectangle
                lineCoords.unshift(layerOffset);
                lineCoords.push(layerOffset + layerSize);

                // draw all line segments
                const offset = renderPos.offset2 - 0.5;
                for (let ai = 0, length = lineCoords.length; ai < length; ai += 2) {
                    if (lineCoords[ai] < lineCoords[ai + 1]) {
                        pushLineFunc(offset, getCanvasCoord(lineCoords[ai]) - 1, getCanvasCoord(lineCoords[ai + 1]));
                    }
                }
            }
        }

        // paint everything with 20% opacity
        context.setGlobalAlpha(0.2);
        context.setLineStyle({ style: this.docModel.resolveColor(gridColor, AUTO_GRID_COLOR).css, width: 1 });
        context.drawPath(path, "stroke");
    }
}
