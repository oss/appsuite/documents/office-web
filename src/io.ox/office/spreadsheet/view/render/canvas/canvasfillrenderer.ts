/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Canvas2DContext } from "@/io.ox/office/tk/canvas";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderFillStyle } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylemodel";
import type { RenderCellModel } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellmodel";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// types ======================================================================

type FillArgs = [left: number, top: number, width: number, height: number, style: RenderFillStyle];

// class CanvasFillRenderer ===================================================

/**
 * Renderer for the cell background fill styles.
 */
export class CanvasFillRenderer extends CanvasBaseRenderer {

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasFillRenderer} render")
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        const { renderRange, canvasLayerRect, canvasSheetRect, fastMode } = settings;

        // fill color for "transparent" areas
        const defaultFill = "white";

        // all columns with an explicit fill color, mapped by column index
        const colFills = new Map<number, RenderFillStyle>();
        // all rows with an explicit fill color (or transparent), mapped by row index
        const rowFills = new Map<number, RenderFillStyle>();

        // paints or clears a rectangle in the canvas (also fills the trailing pixel of the preceding left/top cells)
        const drawRect = (left: number, top: number, width: number, height: number, fillStyle: RenderFillStyle): void => {
            context.setFillStyle(fillStyle || defaultFill);
            context.drawRect(left, top, width, height, "fill");
        };

        // clean the rendering area
        context.setFillStyle(defaultFill);
        context.drawRect(settings.canvasClipRect, "fill");

        // render entire filled columns
        for (const { index, renderPos, renderStyle } of cache.yieldStyleHeaderModels(renderRange, true)) {
            if (renderStyle.fill) {
                drawRect(renderPos.offset - 1, canvasLayerRect.top, renderPos.size + 1, canvasLayerRect.height, renderStyle.fill);
                colFills.set(index, renderStyle.fill);
            }
        }

        // render entire filled rows
        for (const { index, renderPos, renderStyle } of cache.yieldStyleHeaderModels(renderRange, false)) {
            drawRect(canvasLayerRect.left, renderPos.offset - 1, canvasLayerRect.width, renderPos.size + 1, renderStyle.fill);
            rowFills.set(index, renderStyle.fill);
        }

        // fill canvas areas that are outside the sheet limits
        const oversizeWidth = canvasLayerRect.right() - canvasSheetRect.width;
        const oversizeHeight = canvasLayerRect.bottom() - canvasSheetRect.height;
        if ((oversizeWidth > 0) || (oversizeHeight > 0)) {
            context.setFillStyle("--pane-root-background");
            if (oversizeWidth > 0) {
                context.drawRect(canvasSheetRect.width, canvasLayerRect.top, oversizeWidth, canvasLayerRect.height, "fill");
            }
            if (oversizeHeight > 0) {
                context.drawRect(canvasLayerRect.left, canvasSheetRect.height, canvasLayerRect.width, oversizeHeight, "fill");
            }
        }

        // do not render single cells in fast mode
        if (fastMode) { return; }

        // all cells that need to be cleared (delete default column/row formatting)
        const clearCellArgs: FillArgs[] = [];
        // all cells that need to be filled
        const fillCellArgs: FillArgs[] = [];

        // creates an entry in one of the cell arrays (`clearCellArgs` or `fillCellArgs`)
        const pushRect = (renderModel: RenderCellModel, fillStyle: RenderFillStyle): void => {
            const rect = renderModel.renderRect.clone().expandSelf(1, 1, 0, 0).intersect(canvasLayerRect);
            if (rect) {
                const targetArgs = fillStyle ? fillCellArgs : clearCellArgs;
                targetArgs.push([rect.left, rect.top, rect.width, rect.height, fillStyle]);
            }
        };

        // Collect all cells to be filled or cleared (transparent cells may need to clear the default column/row
        // background). Collect cells to be cleared in a separate array to be able to draw them before the filled
        // cells which want to expand the filled area by 1 pixel to the left/top into their predecessors.
        for (const renderModel of cache.yieldStyleCellModels(renderRange, { origins: true })) {

            // the fill style to be used (prefer color scale from conditional formatting)
            const fillStyle = renderModel.getFillStyle();

            // bug 40429: always render merged ranges over default column/row formatting
            if (renderModel.mergedData) {
                pushRect(renderModel, fillStyle);
                continue;
            }

            // column and row index of the current cell
            const { c, r } = renderModel.address;
            // current default fill color from column or row (already rendered above)
            const defaultFillStyle = rowFills.has(r) ? rowFills.get(r)! : (colFills.get(c) ?? null);

            // Bug 37041: Improve performance by checking the default column/row fill color
            // (do not render cells that do not change the existing fill color).
            if (defaultFillStyle !== fillStyle) {
                pushRect(renderModel, fillStyle);
            }
        }

        // first, clear all transparent cells (done before drawing all filled cells, otherwise
        // the trailing pixel of preceding filled cells would be cleared by these cells)
        for (const drawArgs of clearCellArgs) { drawRect(...drawArgs); }

        // // afterwards, draw the cells with existing fill style
        for (const drawArgs of fillCellArgs) { drawRect(...drawArgs); }
    }
}
