/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun, map } from "@/io.ox/office/tk/algorithms";
import { type Canvas2DContext, type PathMode, Path } from "@/io.ox/office/tk/canvas";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// types ======================================================================

/**
 * Generic rendering function for an icon particle.
 */
type ParticleFunc = (renderer: ParticleRenderer, context: Canvas2DContext) => void;

// constants ==================================================================

// mathematical constants
const PI = Math.PI;
const PI16 = PI / 6;
const PI14 = PI / 4;
const PI12 = PI / 2;
const PI34 = 3 * PI14;
const PI54 = 5 * PI14;
const PI56 = 5 * PI16;
const PI74 = 7 * PI14;

// RGB values of all colors used in icon sets
const BLACK = "#555";
const BLUE = "#46f";
const GRAY = "#bbb";
const GREEN = "#2a4";
const PINK = "#faa";
const RED = "#e42";
const WHITE = "#fff";
const YELLOW = "#dc2";

// smiley types
type SmileyType = "good" | "neutral" | "bad";

// private functions ==========================================================

/**
 * Returns the effective line width for the specified icon size, and base line
 * width, to be used to render the outlines of icons.
 *
 * @param size
 *  The icon size, in pixels.
 *
 * @param width
 *  The base width of the lines to be drawn, in pixels.
 *
 * @returns
 *  The effective line width to be used to render the outlines of icons.
 */
function getIconLineWidth(size: number, width: number): number {
    return Math.ceil(size * width / 25);
}

/**
 * Returns the effective size used to render the icons for the passed size. The
 * passed size will be restricted to a minimum of 7 pixels, and may be reduced
 * by one pixel to keep the horizontal/vertical center lines on entire pixels
 * (this prevents blurry lines in the icons).
 *
 * @param size
 *  The maximum size available for rendering the icons, in pixels.
 *
 * @returns
 *  The effective icon size used to render the icons.
 */
function getEffectiveIconSize(size: number): number {

    // use a minimum size of 7 pixels
    const effSize = Math.max(size, 7);
    // get the base line width for outlines
    const lineWidth = getIconLineWidth(effSize, 1);

    // reduce icon size to keep the horizontal/vertical center lines on entire pixels
    // (odd icon size for odd line widths; and even size for even line widths)
    const pad = (lineWidth & 1) ? (1 - (effSize & 1)) : (effSize & 1);
    return effSize - pad;
}

/**
 * Wraps the passed particle rendering function, returns a rendering function
 * that rotates the rendering context by 180 degrees.
 *
 * @param partFunc
 *  The particle rendering function.
 *
 * @returns
 *  The rendering function for the particle with bound arguments, that rotates
 *  the rendering context by 180 degrees.
 */
function rotate180(partFunc: ParticleFunc): ParticleFunc {
    return (renderer, context) => context.render(() => {
        context.translate(0, renderer.size).scale(1, -1);
        partFunc(renderer, context);
    });
}

/**
 * Wraps the passed particle rendering function, returns a rendering function
 * that rotates the rendering context by 270 degrees.
 *
 * @param partFunc
 *  The particle rendering function.
 *
 * @returns
 *  The rendering function for the particle with bound arguments, that rotates
 *  the rendering context by 270 degrees.
 */
function rotate270(partFunc: ParticleFunc): ParticleFunc {
    return (renderer, context) => context.render(() => {
        context.translate(0, renderer.size).rotate(-PI12);
        partFunc(renderer, context);
    });
}

// particle renderers ---------------------------------------------------------

function arrowDown(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c3 = rend.coord(0.3);
        const c5 = rend.CC;
        const c7 = rend.mirror(c3);
        const ce = rend.CE;
        rend.drawPath(context, new Path().pushPolygon(c5, ce, c0, c5, c3, c5, c3, c0, c7, c0, c7, c5, ce, c5), color);
    };
}

function arrowDiag(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c15 = rend.coord(0.15);
        const c28 = rend.coord(0.28);
        const c85 = rend.mirror(c15);
        const d = (c85 - c28 - c15 + c0) / 2;
        rend.drawPath(context, new Path().pushPolygon(c15, c85, c85, c85, c85, c15, c85 - d, c15 + d, c28, c0, c0, c28, c15 + d, c85 - d), color);
    };
}

function flag(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c1 = rend.coord(0.1);
        const c2 = rend.coord(0.2);
        const c3 = rend.coord(0.3);
        const c6 = 2 * c3 - c0;
        const c9 = rend.mirror(c1);
        const ce = rend.CE;
        rend.drawPath(context, new Path().pushLineChain(c2, c0, c9, c3, c2, c6), color);
        rend.drawPath(context, new Path().pushRect(c1, c0, c2 - c1, ce - c0), BLACK);
    };
}

function star(color1: string, color2: string): ParticleFunc {
    return (rend, context) => {

        // uppermost tip
        const c1x = rend.CC, c1y = rend.C0;
        // upper left/right tips
        const c2xl = rend.C0, c2xr = rend.CE, c2y = rend.coord(0.363);
        // lower left/right tips
        const c3xl = rend.coord(0.155), c3xr = rend.mirror(c3xl), c3y = rend.coord(0.951);
        // upper left/right inner angles
        const c4xl = rend.exact(0.345), c4xr = rend.mirror(c4xl), c4y = rend.exact(0.313);
        // lower left/right inner angles
        const c5xl = rend.exact(0.25), c5xr = rend.mirror(c5xl), c5y = rend.exact(0.607);
        // lowermost inner angle
        const c6x = rend.CC, c6y = rend.exact(0.789);

        // path for right background area
        const pathR = new Path().pushPolygon(c1x, c1y, c4xr, c4y, c2xr, c2y, c5xr, c5y, c3xr, c3y, c6x, c6y);
        // path of the full star
        const pathO = new Path().pushPolygon(c1x, c1y, c4xr, c4y, c2xr, c2y, c5xr, c5y, c3xr, c3y, c6x, c6y, c3xl, c3y, c5xl, c5y, c2xl, c2y, c4xl, c4y);

        rend.drawPath(context, pathO, color1, "fill");
        rend.drawPath(context, pathR, color2, "fill");
        rend.drawPath(context, pathO, null, "stroke");
    };
}

function circle(color: string): ParticleFunc {
    return (rend, context) => rend.drawPath(context, new Path().pushCircle(rend.CC, rend.CC, rend.R), color);
}

function quarter(quarters: number, color1: string, color2: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c5 = rend.CC;
        const ce = rend.CE;
        const fillPath = new Path().moveTo(c5, c0);
        const strokePath = new Path().pushCircle(c5, c5, rend.R).moveTo(c5, c0);
        switch (quarters) {
            case 1:
                fillPath.arc(c5, c5, rend.R, -PI12, 0).lineTo(c5, c5);
                strokePath.lineTo(c5, c5).lineTo(ce, c5);
                break;
            case 2:
                fillPath.arc(c5, c5, rend.R, -PI12, PI12);
                strokePath.lineTo(c5, ce);
                break;
            case 3:
                fillPath.arc(c5, c5, rend.R, -PI12, PI).lineTo(c5, c5);
                strokePath.lineTo(c5, c5).lineTo(c0, c5);
                break;
        }
        rend.drawPath(context, new Path().pushCircle(c5, c5, rend.R), color1, "fill");
        rend.drawPath(context, fillPath.close(), color2, "fill");
        rend.drawPath(context, strokePath, null, "stroke");
    };
}

function trafficLight(color: string): ParticleFunc {
    return (rend, context) => {
        const c15 = rend.coord(0.15);
        const c85 = rend.mirror(c15);
        const r1 = c15 - rend.C0;
        const r2 = rend.CC - rend.coord(0.1);
        rend.drawPath(context, new Path().arc(c15, c15, r1, PI, -PI12).arc(c85, c15, r1, -PI12, 0).arc(c85, c85, r1, 0, PI12).arc(c15, c85, r1, PI12, PI).close(), BLACK);
        rend.drawPath(context, new Path().pushCircle(rend.CC, rend.CC, r2), color);
    };
}

function rating(color1: string, color2: string, color3: string, color4: string): ParticleFunc {
    return (rend, context) => {
        const c5 = rend.CC;
        const w = Math.floor((c5 - rend.C0) / 2);
        const c0 = c5 - 2 * w;
        const c25 = c5 - w;
        const c75 = c5 + w;
        const ce = c5 + 2 * w;
        const h = ce - c0;
        rend.drawPath(context, new Path().pushRect(c0, c0 + 3 * w, w, h - 3 * w), color1);
        rend.drawPath(context, new Path().pushRect(c25, c0 + 2 * w, w, h - 2 * w), color2);
        rend.drawPath(context, new Path().pushRect(c5, c0 + w, w, h - w), color3);
        rend.drawPath(context, new Path().pushRect(c75, c0, w, h), color4);
    };
}

function boxes(color1: string, color2: string, color3: string, color4: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c15 = rend.coord(0.15);
        const c5 = rend.CC;
        const c85 = rend.mirror(c15);
        const ce = rend.CE;
        const r = c15 - rend.C0;
        rend.drawPath(context, new Path().moveTo(c5, c5).lineTo(c5, ce).arc(c15, c85, r, PI12, PI).lineTo(c0, c5).close(), color1);
        rend.drawPath(context, new Path().moveTo(c5, c5).lineTo(ce, c5).arc(c85, c85, r, 0, PI12).lineTo(c5, ce).close(), color2);
        rend.drawPath(context, new Path().moveTo(c5, c5).lineTo(c0, c5).arc(c15, c15, r, PI, -PI12).lineTo(c5, c0).close(), color3);
        rend.drawPath(context, new Path().moveTo(c5, c5).lineTo(c5, c0).arc(c85, c15, r, -PI12, 0).lineTo(ce, c5).close(), color4);
    };
}

function squareSign(color: string): ParticleFunc {
    return (rend, context) => {
        const c15 = rend.coord(0.15);
        const c5 = rend.CC;
        const c85 = rend.mirror(c15);
        const r = c15 - rend.C0;
        rend.drawPath(context, new Path().arc(c5, c15, r, -PI34, -PI14).arc(c85, c5, r, -PI14, PI14).arc(c5, c85, r, PI14, PI34).arc(c15, c5, r, PI34, -PI34).close(), color);
    };
}

function triangleSign(color: string): ParticleFunc {
    return (rend, context) => {
        const c15 = rend.coord(0.15);
        const c5 = rend.CC;
        const c75 = rend.coord(0.75);
        const c85 = rend.mirror(c15);
        const r = c15 - rend.C0;
        rend.drawPath(context, new Path().arc(c5, c15, r, -PI56, -PI16).arc(c85, c75, r, -PI16, PI12).arc(c15, c75, r, PI12, -PI56).close(), color);
    };
}

function cross(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c2 = rend.coord(0.2);
        const c5 = rend.CC;
        const c8 = rend.mirror(c2);
        const ce = rend.CE;
        const d = c2 - c0;
        const c3 = c5 - d;
        const c7 = c5 + d;
        rend.drawPath(context, new Path().pushPolygon(c2, c0, c0, c2, c3, c5, c0, c8, c2, ce, c5, c7, c8, ce, ce, c8, c7, c5, ce, c2, c8, c0, c5, c3), color);
    };
}

function exclamation(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c35 = rend.coord(0.35);
        const c5 = rend.CC;
        const c6 = rend.coord(0.6);
        const ce = rend.CE;
        const r = c5 - c35;
        rend.drawPath(context, new Path().pushRect(c35, c0, 2 * r, c6 - c0).pushCircle(c5, ce - r, r), color);
    };
}

function check(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c4 = rend.coord(0.4);
        const c6 = rend.mirror(c4);
        const ce = rend.CE;
        const d = rend.coord(0.2) - c0;
        const o = Math.floor(d / 2);
        rend.drawPath(context, new Path().pushPolygon(c0, c6 - o, c4, ce - o, ce, c4 - o, ce - d, c4 - d - o, c4, ce - 2 * d - o, c0 + d, c6 - d - o), color);
    };
}

function minus(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c4 = rend.coord(0.38);
        const c6 = rend.mirror(c4);
        rend.drawPath(context, new Path().pushRect(c0, c4, rend.SI, c6 - c4), color);
    };
}

function triangleDown(color: string): ParticleFunc {
    return (rend, context) => {
        const c0 = rend.C0;
        const c25 = rend.coord(0.25);
        const c5 = rend.CC;
        const c75 = c25 + c5 - c0;
        const ce = rend.CE;
        rend.drawPath(context, new Path().pushPolygon(c0, c25, c5, c75, ce, c25), color);
    };
}

function crossLabel(rend: ParticleRenderer, context: Canvas2DContext): void {
    const c3 = rend.coord(0.3);
    const c7 = rend.mirror(c3);
    rend.drawLabel(context, new Path().pushLine(c3, c3, c7, c7).pushLine(c3, c7, c7, c3), WHITE, true);
}

function exclamationLabel(rend: ParticleRenderer, context: Canvas2DContext): void {
    const c25 = rend.coord(0.25);
    const c5 = rend.CC;
    const c55 = rend.coord(0.55);
    const c75 = rend.mirror(c25);
    rend.drawLabel(context, new Path().pushLine(c5, c25, c5, c55).pushLine(c5, c75, c5, c75 + 1), WHITE, true);
}

function checkLabel(rend: ParticleRenderer, context: Canvas2DContext): void {
    const c25 = rend.coord(0.25);
    const c75 = rend.mirror(c25);
    const c5 = (c75 + c25) / 2;
    const d = (c75 - c25) / 3;
    rend.drawLabel(context, new Path().pushLineChain(c25, c5, c25 + d, c5 + d, c75, c5 - d), WHITE, true);
}

function smileyFace(type: SmileyType): ParticleFunc {
    return (rend, context) => {

        // eyes
        const cxl = rend.coord(0.35);
        const cxr = rend.mirror(cxl);
        const cy = rend.coord(0.4);
        rend.drawPath(context, new Path().pushCircle(cxl, cy, 1).pushCircle(cxr, cy, 1), BLACK);

        // mouth
        const r = rend.coord(0.35);
        const path = new Path();
        switch (type) {
            case "neutral": {
                const cy2 = rend.coord(0.65);
                path.pushLine(cxl, cy2, cxr, cy2);
                break;
            }
            case "good":
                path.pushArc(rend.CC, rend.coord(0.4), r, PI14, PI34);
                break;
            case "bad":
                path.pushArc(rend.CC, rend.CE, r, PI54, PI74);
                break;
        }
        rend.drawPath(context, path, null, "stroke");
    };
}

// icon definitions ===========================================================

// An icon definition is an array of particle rendering functions bound to
// their actual arguments (colors etc.).

// arrows
const RED_ARROW_DOWN       = [arrowDown(RED)];
const YELLOW_ARROW_DG_DOWN = [arrowDiag(YELLOW)];
const YELLOW_ARROW_RIGHT   = [rotate270(arrowDown(YELLOW))];
const YELLOW_ARROW_DG_UP   = [rotate270(arrowDiag(YELLOW))];
const GREEN_ARROW_UP       = [rotate180(arrowDown(GREEN))];
const GRAY_ARROW_DOWN      = [arrowDown(GRAY)];
const GRAY_ARROW_DG_DOWN   = [arrowDiag(GRAY)];
const GRAY_ARROW_RIGHT     = [rotate270(arrowDown(GRAY))];
const GRAY_ARROW_DG_UP     = [rotate270(arrowDiag(GRAY))];
const GRAY_ARROW_UP        = [rotate180(arrowDown(GRAY))];

// flags
const RED_FLAG    = [flag(RED)];
const YELLOW_FLAG = [flag(YELLOW)];
const GREEN_FLAG  = [flag(GREEN)];

// circles
const RED_CIRCLE         = [circle(RED)];
const YELLOW_CIRCLE      = [circle(YELLOW)];
const GREEN_CIRCLE       = [circle(GREEN)];
const GRAY_CIRCLE        = [circle(GRAY)];
const BLACK_CIRCLE       = [circle(BLACK)];
const PINK_CIRCLE        = [circle(PINK)];
const RED_CROSS_CIRCLE   = [circle(RED),    crossLabel];
const YELLOW_EXCL_CIRCLE = [circle(YELLOW), exclamationLabel];
const GREEN_CHECK_CIRCLE = [circle(GREEN),  checkLabel];
const GRAY_QUARTER_0     = [circle(WHITE)];
const GRAY_QUARTER_1     = [quarter(1, WHITE, GRAY)];
const GRAY_QUARTER_2     = [quarter(2, WHITE, GRAY)];
const GRAY_QUARTER_3     = [quarter(3, WHITE, GRAY)];
const GRAY_QUARTER_4     = [circle(GRAY)];

// smileys
const GREEN_SMILEY_GOOD     = [circle(GREEN),  smileyFace("good")];
const YELLOW_SMILEY_GOOD    = [circle(YELLOW), smileyFace("good")];
const YELLOW_SMILEY_NEUTRAL = [circle(YELLOW), smileyFace("neutral")];
const YELLOW_SMILEY_BAD     = [circle(YELLOW), smileyFace("bad")];
const RED_SMILEY_BAD        = [circle(RED),    smileyFace("bad")];

// traffic lights
const RED_TRAFFIC_LIGHT    = [trafficLight(RED)];
const YELLOW_TRAFFIC_LIGHT = [trafficLight(YELLOW)];
const GREEN_TRAFFIC_LIGHT  = [trafficLight(GREEN)];

// ratings
const WHITE_STAR       = [star(WHITE,  WHITE)];
const HALF_YELLOW_STAR = [star(YELLOW, WHITE)];
const YELLOW_STAR      = [star(YELLOW, YELLOW)];
const BLUE_RATING_0    = [rating(GRAY, GRAY, GRAY, GRAY)];
const BLUE_RATING_1    = [rating(BLUE, GRAY, GRAY, GRAY)];
const BLUE_RATING_2    = [rating(BLUE, BLUE, GRAY, GRAY)];
const BLUE_RATING_3    = [rating(BLUE, BLUE, BLUE, GRAY)];
const BLUE_RATING_4    = [rating(BLUE, BLUE, BLUE, BLUE)];
const BLUE_BOXES_0     = [boxes(GRAY, GRAY, GRAY, GRAY)];
const BLUE_BOXES_1     = [boxes(BLUE, GRAY, GRAY, GRAY)];
const BLUE_BOXES_2     = [boxes(BLUE, BLUE, GRAY, GRAY)];
const BLUE_BOXES_3     = [boxes(BLUE, BLUE, BLUE, GRAY)];
const BLUE_BOXES_4     = [boxes(BLUE, BLUE, BLUE, BLUE)];

// signs
const RED_SQUARE_SIGN      = [squareSign(RED)];
const YELLOW_TRIANGLE_SIGN = [triangleSign(YELLOW)];
const RED_CROSS            = [cross(RED)];
const YELLOW_EXCL          = [exclamation(YELLOW)];
const GREEN_CHECK          = [check(GREEN)];
const YELLOW_MINUS         = [minus(YELLOW)];
const RED_TRIANGLE_DOWN    = [triangleDown(RED)];
const GREEN_TRIANGLE_UP    = [rotate180(triangleDown(GREEN))];

// maps identifiers of all supported icon sets (lower-case!) to icon definitions
const ICON_SET_MAP = new Map<string, ParticleFunc[][]>([

    // 3 icons
    ["3arrows",         [RED_ARROW_DOWN,    YELLOW_ARROW_RIGHT,   GREEN_ARROW_UP]],
    ["3arrowsgray",     [GRAY_ARROW_DOWN,   GRAY_ARROW_RIGHT,     GRAY_ARROW_UP]],
    ["3flags",          [RED_FLAG,          YELLOW_FLAG,          GREEN_FLAG]],
    ["3trafficlights1", [RED_CIRCLE,        YELLOW_CIRCLE,        GREEN_CIRCLE]],
    ["3trafficlights2", [RED_TRAFFIC_LIGHT, YELLOW_TRAFFIC_LIGHT, GREEN_TRAFFIC_LIGHT]],
    ["3signs",          [RED_SQUARE_SIGN,   YELLOW_TRIANGLE_SIGN, GREEN_CIRCLE]],
    ["3symbols",        [RED_CROSS_CIRCLE,  YELLOW_EXCL_CIRCLE,   GREEN_CHECK_CIRCLE]],
    ["3symbols2",       [RED_CROSS,         YELLOW_EXCL,          GREEN_CHECK]],
    ["3stars",          [WHITE_STAR,        HALF_YELLOW_STAR,     YELLOW_STAR]],
    ["3triangles",      [RED_TRIANGLE_DOWN, YELLOW_MINUS,         GREEN_TRIANGLE_UP]],

    // smileys in ODF only, with wrong spelling "Smilies" in the identifiers!
    ["3smilies",        [YELLOW_SMILEY_GOOD, YELLOW_SMILEY_NEUTRAL, YELLOW_SMILEY_BAD]],
    ["3colorsmilies",   [GREEN_SMILEY_GOOD,  YELLOW_SMILEY_NEUTRAL, RED_SMILEY_BAD]],

    // 4 icons
    ["4arrows",         [RED_ARROW_DOWN,  YELLOW_ARROW_DG_DOWN, YELLOW_ARROW_DG_UP, GREEN_ARROW_UP]],
    ["4arrowsgray",     [GRAY_ARROW_DOWN, GRAY_ARROW_DG_DOWN,   GRAY_ARROW_DG_UP,   GRAY_ARROW_UP]],
    ["4redtoblack",     [BLACK_CIRCLE,    GRAY_CIRCLE,          PINK_CIRCLE,        RED_CIRCLE]],
    ["4rating",         [BLUE_RATING_1,   BLUE_RATING_2,        BLUE_RATING_3,      BLUE_RATING_4]],
    ["4trafficlights",  [BLACK_CIRCLE,    RED_CIRCLE,           YELLOW_CIRCLE,      GREEN_CIRCLE]],

    // 5 icons
    ["5arrows",         [RED_ARROW_DOWN,  YELLOW_ARROW_DG_DOWN, YELLOW_ARROW_RIGHT, YELLOW_ARROW_DG_UP, GREEN_ARROW_UP]],
    ["5arrowsgray",     [GRAY_ARROW_DOWN, GRAY_ARROW_DG_DOWN,   GRAY_ARROW_RIGHT,   GRAY_ARROW_DG_UP,   GRAY_ARROW_UP]],
    ["5rating",         [BLUE_RATING_0,   BLUE_RATING_1,        BLUE_RATING_2,      BLUE_RATING_3,      BLUE_RATING_4]],
    ["5quarters",       [GRAY_QUARTER_0,  GRAY_QUARTER_1,       GRAY_QUARTER_2,     GRAY_QUARTER_3,     GRAY_QUARTER_4]],
    ["5boxes",          [BLUE_BOXES_0,    BLUE_BOXES_1,         BLUE_BOXES_2,       BLUE_BOXES_3,       BLUE_BOXES_4]]
]);

// class ParticleRenderer =====================================================

/**
 * Renderer for the single particles of all icons of a specific icon size.
 */
class ParticleRenderer {

    /** Effective icon size. */
    readonly size: number;

    // line width for outlines, according to icon size
    readonly LW: number;
    // line width for thick lines used for signs with shadow effect
    readonly LW2: number;
    // line offset needed to keep the outlines on entire pixels
    readonly LO: number;

    // inner size (maximum dimension of the path)
    readonly SI: number;

    // leading position for outlines (top or left lines)
    readonly C0: number;
    // center position for outlines (centered horizontal or vertical lines)
    readonly CC: number;
    // trailing position for outlines (for bottom or right lines)
    readonly CE: number;

    // radius of a full-size circle
    readonly R: number;

    // constructor ------------------------------------------------------------

    /**
     * @param size
     *  The effective icon size (width and height) that will be used to render
     *  the particles, in pixels. See function `getEffectiveIconSize` for
     *  more details.
     */
    constructor(size: number) {

        this.size = size;

        // line widths and offsets
        this.LW = this.lineWidth(1);
        this.LW2 = this.lineWidth(2.5);
        this.LO = this.LW / 2;

        // inner size
        this.SI = this.size - this.LW;

        // coordinates
        this.C0 = this.coord(0);
        this.CC = this.size / 2;
        this.CE = this.mirror(this.C0);

        // radius of a full-size circle
        this.R = this.CC - this.C0;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the effective line width for the specified base width, according
     * to the size of the icons to be rendered.
     *
     * @param width
     *  The base width of the lines to be drawn, in pixels.
     *
     * @returns
     *  The effective line width to be used to render the outlines of icons.
     */
    lineWidth(width: number): number {
        return getIconLineWidth(this.size, width);
    }

    /**
     * Returns the effective value of a path coordinate. The resulting position
     * will be adjusted so that horizontal/vertical outlines drawn at these
     * coordinate will be kept on entire pixels to prevent blurry lines.
     *
     * @param pos
     *  The relative position inside the icon, in the interval `[0;1]`. The
     *  value `0` represents the leading border (the value of the property
     *  `C0`). The value `1` represents the trailing border (the value of the
     *  property `CE`).
     *
     * @returns
     *  The effective value of a path coordinate.
     */
    coord(pos: number): number {
        return this.LO + Math.round(this.SI * pos);
    }

    /**
     * Returns the effective value of a path coordinate. Works similarly as the
     * method `coord()`, but does not adjust the effective position to entire
     * pixels.
     *
     * @param pos
     *  The relative position inside the icon, in the interval `[0;1]`. The
     *  value `0` represents the leading border (the value of the property
     *  `C0`). The value `1` represents the trailing border (the value of the
     *  property `CE`).
     *
     * @returns
     *  The effective value of a path coordinate.
     */
    exact(pos: number): number {
        return this.LO + this.SI * pos;
    }

    /**
     * Mirrors the passed effective coordinate which helps to create symmetric
     * icons. Calling the method `coord()` with mirrored relative positions may
     * not result in the desired symmetric effective coordinates due to
     * internal rounding effects.
     *
     * @param coord
     *  The effective corrdinate, as returned for example by the methods
     *  `coord()` or `exact()`.
     *
     * @returns
     *  The opposite coordinate.
     */
    mirror(coord: number): number {
        return this.size - coord;
    }

    /**
     * Draws (fills and/or strokes) the specified path into the rendering
     * context.
     *
     * @param context
     *  The rendering context of a canvas.
     *
     * @param path
     *  The canvas path to be drawn.
     *
     * @param color
     *  The CSS fill color; or `null` to retain the current fill color.
     *
     * @param [mode]
     *  The rendering mode for the path.
     *
     * @returns
     *  A reference to this instance.
     */
    drawPath(context: Canvas2DContext, path: Path, color: string | null, mode: PathMode = "all"): this {
        if (color) { context.setFillStyle(color); }
        context.setLineStyle({ style: "black", width: this.LW });
        context.drawPath(path, mode);
        return this;
    }

    /**
     * Draws the specified label into the rendering context, as thick lines
     * with a black shadow effect.
     *
     * @param context
     *  The rendering context of a canvas.
     *
     * @param path
     *  The canvas path to be drawn.
     *
     * @param color
     *  The CSS line color.
     *
     * @param [shadow=false]
     *  Whether to draw the shadow effect.
     *
     * @returns
     *  A reference to this instance.
     */
    drawLabel(context: Canvas2DContext, path: Path, color: string, shadow?: boolean): this {
        context.setLineStyle({ style: color, width: this.LW2 });
        if (shadow) { context.setShadowStyle({ color: "black", blur: this.LW2 }); }
        context.drawPath(path, "stroke");
        return this;
    }
}

// global renderer cache (mapped by icon size in pixels)
const rendererCache = new Map<number, ParticleRenderer>();

// class CanvasIconSetRenderer ================================================

/**
 * Renderer for the icons of conditional formatting rules.
 */
export class CanvasIconSetRenderer extends CanvasBaseRenderer {

    // bug 48857: in ODS, icon size depends on row height, not on font size (icons always top-aligned)
    readonly #odf = this.docApp.isODF();

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasIconSetRenderer} render")
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        context.setLineStyle({ cap: "round", join: "round" });

        // render icon set cells into the canvas
        for (const renderModel of cache.yieldStyleCellModels(settings.renderRange)) {

            // the icon set properties
            const iconProps = renderModel.renderProps?.iconSet;
            if (!iconProps) { continue; }

            // the particle rendering functions for the icon
            const particleFuncs = ICON_SET_MAP.get(iconProps.id.toLowerCase())?.[iconProps.index];
            if (!particleFuncs) { continue; }

            const { renderRect: canvasRect, renderStyle } = renderModel;

            // the effective size of the available space, according to file format
            const availSize = this.#odf ? Math.min(canvasRect.width, canvasRect.height) : renderStyle.canvasRowHeight;
            // leave some space to the cell borders (width of lines used in the icon)
            const padding = getIconLineWidth(availSize, 1);
            // the effective icon size for the available size
            const iconSize = getEffectiveIconSize(availSize - 2 * padding - 1);

            // get or create the particle renderer for the passed icon size
            const renderer = map.upsert(rendererCache, iconSize, () => new ParticleRenderer(iconSize));

            // calculate the horizontal position (TODO: RTL layout)
            const offsetX = padding;

            // calculate the vertical position (always top-aligned in ODF documents)
            const offsetY = this.#odf ? padding : fun.do(() => {
                switch (renderStyle.attrSet.cell.alignVert) {
                    case "top":
                    case "justify":
                        return padding;
                    case "middle":
                        return Math.ceil((canvasRect.height - iconSize - 1) / 2);
                    default: // bottom alignment, and fall-back for unknown alignments
                        return canvasRect.height - padding - iconSize - 1;
                }
            });

            // render all particles the icon consists of
            context.clip(canvasRect, () => {
                context.translate(canvasRect.left + offsetX, canvasRect.top + offsetY);
                particleFuncs.forEach(particleFunc => particleFunc(renderer, context));
            });
        }
    }
}
