/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math } from "@/io.ox/office/tk/algorithms";
import type { Canvas2DContext } from "@/io.ox/office/tk/canvas";

import { renderLogger, getCanvasCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// private functions ==========================================================

/**
 * Returns the rounded pixel position inside the specified pixel interval.
 */
function getPixelX(firstX: number, lastX: number, offset: number): number {
    return Math.round(firstX + (lastX - firstX) * math.clamp(offset, 0, 1));
}

// class CanvasDataBarRenderer ================================================

/**
 * Renderer for the data bars of conditional formatting rules.
 */
export class CanvasDataBarRenderer extends CanvasBaseRenderer {

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasDataBarRenderer} render")
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        // line size and padding (one CSS pixel)
        const lineSize = getCanvasCoord(1);

        // render data bar cells into the canvas
        for (const renderModel of cache.yieldCellModels(settings.renderRange)) {

            // the data bar properties
            const dataBar = renderModel.renderProps?.dataBar;
            if (!dataBar) { continue; }

            // the full rectangle of the cell in the canvas
            const rect = renderModel.renderRect;
            // the first and last pixel index in X direction to be drawn (keep one CSS pixel padding in the cell)
            const firstX = rect.left + lineSize;
            const lastX = rect.right() - lineSize - 2;
            // the rounded pixel position of the axis line
            let axisX = getPixelX(firstX, lastX, -dataBar.min / (dataBar.max - dataBar.min));
            // the rounded pixel position of the first/last filled bar pixel
            const barX = getPixelX(firstX, lastX, (dataBar.num - dataBar.min) / (dataBar.max - dataBar.min));

            // draw the axis line unless it is located on the first or last visible pixel
            if (axisX === firstX) {
                axisX = firstX - 1;
            } else if (axisX === lastX) {
                axisX = lastX + 1;
            } else {
                context.setLineStyle({ style: "rgba(0,0,0,.5)", width: 1, pattern: 2 });
                context.drawLine(axisX + 0.5, rect.top, axisX + 0.5, rect.top + rect.height);
            }

            // calculate the left X coordinates and absolute width according to bar direction
            const x = Math.min(barX, axisX + 1);
            const width = Math.abs(axisX - barX);

            // draw the data bar if it is visible (covers at least one pixel next to the axis)
            if (width > 0) {
                if (dataBar.color2) {
                    context.setFillStyle(context.createLinearGradient(axisX, 0, barX, 0, [
                        { offset: 0, color: dataBar.color1 },
                        { offset: 1, color: dataBar.color2 }
                    ]));
                } else {
                    context.setFillStyle(dataBar.color1);
                }
                context.drawRect(x, rect.top + lineSize, width, rect.height - 2 * lineSize - 1, "fill");
            }
        }
    }
}
