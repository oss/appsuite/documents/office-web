/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Canvas2DContext } from "@/io.ox/office/tk/canvas";

import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { RenderBorder } from "@/io.ox/office/spreadsheet/view/render/cache/renderborder";
import type { RenderCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercache";
import { type CanvasRenderSettings, type DrawCanvasLineFn, CanvasBaseRenderer } from "@/io.ox/office/spreadsheet/view/render/canvas/canvasbaserenderer";

// class CanvasBorderRenderer =================================================

/**
 * Renderer for the cell border lines.
 */
export class CanvasBorderRenderer extends CanvasBaseRenderer {

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CanvasBorderRenderer} render")
    override render(context: Canvas2DContext, cache: RenderCache, settings: CanvasRenderSettings): void {

        const shrinkRange = this.docView.sheetModel.shrinkRangeToVisible(settings.renderRange);
        if (!shrinkRange) { return; }

        // paint vertical borders of all cells (first left borders of left cells, then right borders of all cells)
        this.#renderBorderLines(context, cache, shrinkRange, true, true);
        this.#renderBorderLines(context, cache, shrinkRange, true, false);

        // paint horizontal borders of all cells (first top borders of top cells, then bottom borders of all cells)
        this.#renderBorderLines(context, cache, shrinkRange, false, true);
        this.#renderBorderLines(context, cache, shrinkRange, false, false);
    }

    // private methods --------------------------------------------------------

    /**
     * Renders all border lines in one direction.
     */
    #renderBorderLines(context: Canvas2DContext, cache: RenderCache, range: Range, columns: boolean, leading: boolean): void {

        // property name for the border style of the current cell
        const ownProp = columns ? (leading ? "l" : "r") : (leading ? "t" : "b");
        // property name for the opposite border style of the adjacent cell
        const adjProp = columns ? (leading ? "r" : "l") : (leading ? "b" : "t");

        // draws a single line into the current direction
        const drawLine: DrawCanvasLineFn = columns ?
            (x, y1, y2) => context.drawLine(x, y1 - 1, x, y2) :
            (y, x1, x2) => context.drawLine(x1 - 1, y, x2, y);

        // shrink to leading visible column/row in the range
        if (leading) { range = range.lineRange(columns, 0); }

        // visit all cells and process the border lines in the specified direction
        for (const renderModel of cache.yieldCellModels(range, { columns })) {

            // the border styles of the own cell, and the adjacent cell
            const ownBorder = renderModel.getRenderBorder(ownProp);
            const adjBorder = cache.getAdjacentCellModel(renderModel, !columns, leading)?.getRenderBorder(adjProp);

            // get the stronger of the two borders
            const border = RenderBorder.getStrongerBorder(ownBorder, adjBorder);
            if (!border) { continue; }

            // header cache entry in drawing direction
            const header1 = (columns ? renderModel.colHeader : renderModel.rowHeader).renderPos;
            // header cache entry in opposite direction (for start/end coordinate of the line)
            const header2 = (columns ? renderModel.rowHeader : renderModel.colHeader).renderPos;

            // line coordinate for main drawing direction (middle of border)
            const offset = (leading ? header1.offset : header1.offset2) - (border.width / 2) % 1;
            // start/end coordinates of the border line
            const pos1 = header2.offset;
            const pos2 = header2.offset2;

            // initialize context line style for the border line
            context.setLineStyle(border.canvasStyle);

            // add all border lines to the path
            if (border.double) {
                const shift = (border.width - border.canvasStyle.width!) / 2;
                drawLine(offset - shift, pos1, pos2);
                drawLine(offset + shift, pos1, pos2);
            } else {
                drawLine(offset, pos1, pos2);
            }
        }
    }
}
