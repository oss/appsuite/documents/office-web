/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary, map } from "@/io.ox/office/tk/algorithms";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";

import type { ColRowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { RenderHeaderModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadermodel";
import type { RenderStyleCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylecache";

// types ======================================================================

/**
 * Type mapping for the events emitted by `RenderHeaderCache` instances.
 */
export interface RenderHeaderCacheEventMap {

    /**
     * Will be emitted after new dirty column/row index intervals have been
     * registered.
     *
     * @param intervals
     *  The dirty intervals of the header cache.
     */
    "cache:dirty": [intervals: IntervalArray];
}

// constants ==================================================================

/**
 * Maximum number of entries in a column header cache.
 */
const MAX_COL_CACHE_SIZE = 500;

/**
 * Maximum number of entries in a row header cache.
 */
const MAX_ROW_CACHE_SIZE = 2000;

/**
 * Maximum number of entries in the boundary history used to remember oldest
 * visited column/row intervals.
 */
const MAX_HISTORY_SIZE = 100;

// class RenderHeaderCache ====================================================

export class RenderHeaderCache extends SheetChildModel<RenderHeaderCacheEventMap> {

    /** The column/row header collection of the sheet model. */
    readonly #collection: ColRowCollection;
    /** The cache for all rendering styles (pre-processed cell auto-styles). */
    readonly #styleCache: RenderStyleCache;
    /** Whether this instance caches column or row settings. */
    readonly #columns: boolean;
    /** Maximum number of header cache models before shrinking the cache. */
    readonly #MAX_CACHE_SIZE: number;

    /** Regular cache for all visible column/row headers. */
    readonly #headerModels = new Map<number, RenderHeaderModel>();
    /** Cache for intermediate headers (merged range origins) outside the boundaries. */
    readonly #tempModels = new Map<number, RenderHeaderModel>();

    /** Exact intervals of the existing header models (all entries in `#headerModels`). */
    #cacheIntervals = new IntervalArray();
    /** Boundaries of the known headers (including hidden headers, superset of `#cacheIntervals`). */
    #cacheBoundaries = new IntervalArray();
    /** History of parts of `#cacheBoundaries` not inside `#paneBoundaries` anymore (ordered by age). */
    #boundaryHistory = new IntervalArray();

    /** Merged and sorted boundary intervals of the header panes (subset of `#cacheBoundaries`). */
    #paneBoundaries = new IntervalArray();
    /** Intervals of all dirty columns/rows to be refreshed. */
    readonly #dirtyIntervals = new IntervalArray();

    /** Whether this instance is currently locked (no updates after change events). */
    #locked = true;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, styleCache: RenderStyleCache, columns: boolean) {
        super(sheetModel);

        // private properties
        this.#collection = columns ? sheetModel.colCollection : sheetModel.rowCollection;
        this.#styleCache = styleCache;
        this.#columns = columns;
        this.#MAX_CACHE_SIZE = columns ? MAX_COL_CACHE_SIZE : MAX_ROW_CACHE_SIZE;

        // start listening to model events after document import is finished
        this.waitForImportSuccess(this.#connectAfterImport);
    }

    // public getters ---------------------------------------------------------

    /**
     * The boundary column/row intervals shown in the header panes.
     */
    get paneBoundaries(): IntervalArray {
        return this.#paneBoundaries.deepClone();
    }

    /**
     * The exact intervals of the _visible_ column/row headers covered by this
     * instance, with existing header models.
     */
    get cacheIntervals(): IntervalArray {
        return this.#cacheIntervals.deepClone();
    }

    /**
     * The boundaries of all known column/row headers covered by this instance,
     * including hidden headers (a superset of `cacheIntervals`).
     */
    get cacheBoundaries(): IntervalArray {
        return this.#cacheBoundaries.deepClone();
    }

    // public methods ---------------------------------------------------------

    /**
     * Locks this instance, i.e. prevents updating the internal cache entries
     * after changes in the document or sheet model.
     */
    lockCache(): void {
        this.#locked = true;
    }

    /**
     * Unlocks this instance, i.e. resumes updating the internal cache entries
     * after changes in the document or sheet model.
     */
    unlockCache(): void {
        if (this.#locked) {
            this.#locked = false;
            this.refreshCache();
        }
    }

    /**
     * Creates missing header models, and updates the header models for all
     * visible columns/rows in the specified index intervals.
     *
     * @param [dirtyIntervals]
     *  The index intervals to be refreshed.
     */
    @renderLogger.profileMethod("$badge{RenderHeaderCache} refreshCache")
    refreshCache(dirtyIntervals?: Interval | IntervalArray): void {

        // refresh header models for all passed intervals
        const newDirtyIntervals = this.#refreshCache(dirtyIntervals);

        // notify listeners
        if (newDirtyIntervals?.length) {
            this.trigger("cache:dirty", newDirtyIntervals);
        }
    }

    /**
     * Invalidates all header cache entries.
     */
    invalidateCache(): void {
        this.refreshCache(this.#collection.getFullInterval());
    }

    /**
     * Initializes this instance for the passed header pane boundary intervals.
     *
     * @param boundIntervals
     *  The boundary intervals of the column/row header panes to be rendered.
     */
    @renderLogger.profileMethod("$badge{RenderHeaderCache} updatePaneBoundaries")
    updatePaneBoundaries(boundIntervals: IntervalArray): void {

        // merge and sort the passed interval array
        const paneBoundaries = boundIntervals.merge();
        renderLogger.trace(() => `paneBoundaries: old=${this.#paneBoundaries.toOpStr(this.#columns)} new=${paneBoundaries.toOpStr(this.#columns)}`);

        // expand the cache boundaries, create missing header models
        const newCacheIntervals = paneBoundaries.difference(this.#cacheBoundaries);
        if (newCacheIntervals.length) {
            this.#cacheBoundaries = this.#cacheBoundaries.concat(newCacheIntervals).merge();
            this.#refreshCache(newCacheIntervals);
        }

        // update the boundary history, needed to keep the cache below a maximum size
        this.#boundaryHistory = this.#boundaryHistory.concat(this.#paneBoundaries).difference(paneBoundaries);

        // store the new header pane boundaries
        this.#paneBoundaries = paneBoundaries;
    }

    /**
     * Returns a rendering header model for the specified index, even if it is
     * not part of the rendering intervals passed to `updateCache`. Used to
     * store the origin cells of large merged ranges that may start outside the
     * rendering area.
     *
     * @param index
     *  The column/row index.
     *
     * @returns
     *  The header model for the specified column/row.
     */
    createIntermediateModel(index: number): RenderHeaderModel {
        return this.#headerModels.get(index) ?? map.upsert(this.#tempModels, index, () => {
            // resolve the rendering style from cache (no formatting for rows without "customFormat" flag)
            const suid = this.#collection.getModel(index).getAutoStyleId(true);
            const style = is.string(suid) ? this.#styleCache.getStyleModel(suid) : undefined;
            // this will be a temporary unlinked model
            const position = this.#collection.getEntryPosition(index, { pixel: true });
            return new RenderHeaderModel(index, position.offset, position.size, style);
        });
    }

    /**
     * Returns an iterator that yields all rendering header models in the
     * specified index interval.
     *
     * @param interval
     *  The index interval to be rendered.
     *
     * @yields
     *  The header models contained in the passed index interval.
     */
    *yieldHeaderModels(interval: Interval): IterableIterator<RenderHeaderModel> {
        for (const index of this.#cacheIntervals.intersect(interval).indexes()) {
            yield this.#headerModels.get(index)!; // exists always (due to intersection with `#cacheIntervals`)
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Creates or updates the header models for all visible columns/rows in the
     * specified index intervals.
     *
     * @param [dirtyIntervals]
     *  The index intervals to be refreshed.
     */
    #refreshCache(dirtyIntervals?: Interval | IntervalArray): Opt<IntervalArray> {

        // add new dirty intervals to collected intervals
        this.#dirtyIntervals.append(dirtyIntervals);

        // wait for next refresh that will be triggered when leaving locked state
        if (this.#locked) { return; }

        // remember all dirty intervals (need to be returned from this method)
        dirtyIntervals = this.#dirtyIntervals.merge();

        // remove dirty intervals from boundary history (will be deleted anyways)
        this.#boundaryHistory = this.#boundaryHistory.difference(dirtyIntervals);

        // if cache exceeds the limit, shrink it by removing oldest visited boundaries
        const oldCount = this.#headerModels.size;
        const history = this.#boundaryHistory;
        if ((oldCount > this.#MAX_CACHE_SIZE) || (history.length > MAX_HISTORY_SIZE)) {
            renderLogger.trace(() => `shrinking oversized cache (${oldCount} entries)`);
            const removeCacheIntervals = new IntervalArray();
            let removeCount = oldCount - Math.floor(this.#MAX_CACHE_SIZE / 2);
            for (const interval of ary.shiftValues(history)) {
                removeCacheIntervals.push(interval);
                removeCount -= interval.size();
                if ((removeCount <= 0) && (history.length <= MAX_HISTORY_SIZE)) { break; }
            }
            dirtyIntervals = dirtyIntervals.concat(removeCacheIntervals).merge();
            this.#cacheBoundaries = this.#cacheBoundaries.difference(removeCacheIntervals);
        }

        // nothing to do without dirty intervals
        if (dirtyIntervals.empty()) { return; }

        // debug logging
        renderLogger.trace(() => `cacheBoundaries=${this.#cacheBoundaries.toOpStr(this.#columns)}`);
        renderLogger.trace(() => `dirtyIntervals=${dirtyIntervals.toOpStr(this.#columns)}`);

        // delete intermediate models of merged ranges (may be located somewhere outside of boundaries)
        this.#tempModels.clear();

        // delete all existing models covered by dirty intervals
        for (const removeInterval of this.#cacheIntervals.intersect(dirtyIntervals)) {
            // link the adjacent header models to each other (splice the entire `removeInterval`)
            const prevModel = this.#headerModels.get(removeInterval.first)?.prev;
            const nextModel = this.#headerModels.get(removeInterval.last)?.next;
            if (prevModel) { prevModel.next = nextModel; }
            if (nextModel) { nextModel.prev = prevModel; }
            // delete all header models from the map
            for (const index of removeInterval.indexes()) { this.#headerModels.delete(index); }
        }
        this.#cacheIntervals = this.#cacheIntervals.difference(dirtyIntervals);
        const keepCount = this.#headerModels.size;

        // create models for the visible columns/rows in the dirty parts of the cache boundaries
        let prevModel: Opt<RenderHeaderModel>;
        let nextModel = this.#cacheIntervals.length ? this.#headerModels.get(this.#cacheIntervals[0].first) : undefined;
        for (const interval of dirtyIntervals.intersect(this.#cacheBoundaries)) {
            // find the first existing header model following the interval being processed (for double-linking)
            while (nextModel && (nextModel.index < interval.last)) { prevModel = nextModel; nextModel = nextModel.next; }
            // process all visible columns/rows in the current interval
            for (const bandEntry of this.#collection.yieldBandModels(interval, { visible: true })) {
                // resolve the rendering style from cache (no formatting for rows without "customFormat" flag)
                const suid = bandEntry.model.getAutoStyleId(true);
                const renderStyle = is.string(suid) ? this.#styleCache.getStyleModel(suid) : undefined;
                // create header models for all indexes in the current band interval (all with the same render style)
                const { first } = bandEntry.interval;
                const { size } = bandEntry.model;
                for (const index of bandEntry.interval.indexes()) {
                    const offset = bandEntry.offset + (index - first) * size;
                    const newModel = map.toggle(this.#headerModels, index, new RenderHeaderModel(index, offset, size, renderStyle));
                    // double-link new header model with preceding header model
                    newModel.prev = prevModel;
                    if (prevModel) { prevModel.next = newModel; }
                    prevModel = newModel;
                }
                // collect visible intervals for iteration
                this.#cacheIntervals.push(bandEntry.interval);
            }
            // double-link last created model (stored in `prevModel`) with following existing model (stored in `nextModel`)
            if (prevModel) { prevModel.next = nextModel; }
            if (nextModel) { nextModel.prev = prevModel; }
        }

        // merge and sort the resulting model intervals
        this.#cacheIntervals = this.#cacheIntervals.merge();
        // shrink outer borders of header pane boundary intervals to visible model intervals
        this.#paneBoundaries = IntervalArray.map(this.#paneBoundaries, interval => this.#cacheIntervals.intersect(interval).boundary());
        // clear all processed dirty intervals
        this.#dirtyIntervals.clear();

        // debug logging
        renderLogger.trace(() => `${oldCount - keepCount} entries deleted, ${this.#headerModels.size - keepCount} entries created, ${keepCount} entries kept in cache`);

        return dirtyIntervals;
    }

    /**
     * Registers dirty column/row intervals received from a model event.
     */
    #refreshForEvent(intervals: IntervalArray, expand = false): void {
        if (expand) {
            const maxIndex = this.docModel.addressFactory.getMaxIndex(this.#columns);
            const minIndex = intervals.reduce((index, interval) => Math.min(index, interval.first), maxIndex);
            this.refreshCache(new Interval(minIndex, maxIndex));
        } else {
            this.refreshCache(intervals);
        }
    }

    /**
     * Connects this instance to the sheet model, after document import has
     * succeeded (registers all needed event handlers).
     */
    @renderLogger.profileMethod("$badge{RenderHeaderCache} connectAfterImport")
    #connectAfterImport(): void {

        // inserted or deleted columns
        this.listenTo(this.#collection, "move:intervals", event => {
            renderLogger.trace(() => `$badge{RenderHeaderCache} $event{move:intervals} ${event.intervals.toOpStr(this.#columns)}`);
            this.#refreshForEvent(event.intervals, true);
        });

        // changed columns (size, visibility, default cell formatting)
        this.listenTo(this.#collection, "change:intervals", event => {
            renderLogger.trace(() => `$badge{RenderHeaderCache} $event{change:intervals} ${event.intervals.toOpStr(this.#columns)}`);
            this.#refreshForEvent(event.intervals, event.size);
        });
    }
}
