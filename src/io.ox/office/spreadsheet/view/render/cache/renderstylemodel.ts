/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Font } from "@/io.ox/office/tk/dom";
import type { CanvasFillStyle } from "@/io.ox/office/tk/canvas";

import { Color } from "@/io.ox/office/editframework/utils/color";
import { createCanvasPattern } from "@/io.ox/office/editframework/utils/pattern";
import type { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

import type { CellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import type{ AttrBorderName, AttrBorderKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { ATTR_BORDER_KEYS, getBorderName } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { getCanvasSize, getCanvasCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { RenderBorder } from "@/io.ox/office/spreadsheet/view/render/cache/renderborder";

// types ======================================================================

/**
 * A resolved canvas fill style for a cell, or `null` for transparent ells.
 */
export type RenderFillStyle = CanvasFillStyle | null;

/**
 * A map with rendering priorities for all border lines of a cell, used to
 * resolve the effective border style of adjacent cells.
 */
export type RenderStyleBorderPriorityMap = Map<AttrBorderName, number>;

// class RenderStyleModel =====================================================

/**
 * Preprocessed rendering settings for a cell auto-style, such as the effective
 * CSS fill color of the cells resolved from complex color descriptors.
 */
export class RenderStyleModel {

    /**
     * The merged formatting attribute set represented by this instance.
     */
    readonly attrSet: CellAttributeSet;

    /**
     * The effective canvas fill style, or `null` for transparent cells.
     */
    readonly fill: RenderFillStyle;

    /**
     * The effective rendering styles of all borders surrounding a cell, as
     * instances of `RenderBorder`, mapped by the internal short border key
     * (e.g. "b" for bottom border). Either of these map elements may be set to
     * `undefined` which denotes a missing (invisible) border line.
     */
    readonly borders = new Map<AttrBorderKey, Opt<RenderBorder>>();

    /**
     * The parsed number format object, according to the format code in the
     * passed attribute set.
     */
    readonly format: ParsedFormat;

    /**
     * The effective font settings, with scaled font size according to the
     * current zoom factor of the sheet, and canvas resolution.
     */
    readonly font: Font;

    /**
     * The height of a single text line (effective height of an empty cell), in
     * CSS pixels.
     */
    readonly rowHeight: number;

    /**
     * The height of a single text line (effective height of an empty cell), in
     * canvas pixels.
     */
    readonly canvasRowHeight: number;

    /**
     * The effective CSS text color.
     */
    readonly textColor: string;

    /**
     * Whether the text color has been set by an explicit formatting attribute
     * (and not via the style sheet referred by the auto-style).
     */
    readonly explicitTextColor: boolean;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, attrSet: CellAttributeSet, borderPrios: RenderStyleBorderPriorityMap, explicitTextColor: boolean) {

        const { docModel } = sheetModel;
        const sheetZoom = sheetModel.getEffectiveZoom();

        // the merged character and cell attributes of the autostyle
        this.attrSet = attrSet;
        const charAttrs = attrSet.character;
        const cellAttrs = attrSet.cell;

        // resolve fill style (CSS color, canvas gradient, or canvas pattern)
        switch (cellAttrs.fillType) {
            case "pattern":
                this.fill = createCanvasPattern(cellAttrs.pattern, cellAttrs.fillColor, cellAttrs.foreColor, docModel.getThemeModel());
                break;
            case "gradient": // TODO
            default: {
                const fillColorDesc = docModel.resolveColor(Color.parseJSON(cellAttrs.fillColor), "fill");
                this.fill = (fillColorDesc.a === 0) ? null : fillColorDesc.css;
            }
        }

        // resolve style settings for all border lines
        for (const borderKey of ATTR_BORDER_KEYS) {
            const borderName = getBorderName(borderKey);
            const renderBorder = RenderBorder.create(sheetModel, cellAttrs[borderName], borderPrios.get(borderName));
            this.borders.set(borderKey, renderBorder);
        }

        // resolve the number format
        this.format = docModel.numberFormatter.getParsedFormatForAttributes(cellAttrs);

        // scaled font attributes according to current zoom factor
        this.font = docModel.getRenderFont(charAttrs, getCanvasSize(sheetZoom));

        // scaled height of a single text line, in CSS pixels (effective height of an empty cell)
        this.rowHeight = docModel.fontMetrics.getRowHeight(charAttrs, sheetZoom);

        // scaled height of a single text line, in canvas pixels
        this.canvasRowHeight = getCanvasCoord(this.rowHeight);

        // resolve cell text color
        this.textColor = docModel.getCssTextColor(charAttrs.color, [cellAttrs.fillColor]);
        this.explicitTextColor = explicitTextColor;
    }
}
