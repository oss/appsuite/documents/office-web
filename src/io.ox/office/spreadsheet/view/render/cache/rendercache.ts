/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Rectangle } from "@/io.ox/office/tk/dom";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import type { PaneSide, PanePos } from "@/io.ox/office/spreadsheet/utils/paneutils";
import { ALL_PANE_POSITIONS, isColumnSide, getColPaneSide, getRowPaneSide } from "@/io.ox/office/spreadsheet/utils/paneutils";

import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";

import type { HeaderBoundary, HeaderBoundarySpec, GridBoundary, GridBoundarySpec } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { renderLogger, getCanvasRect } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderStyleModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylemodel";
import { RenderStyleCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylecache";
import type { RenderHeaderModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadermodel";
import { RenderHeaderCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadercache";
import type { RenderTextModel } from "@/io.ox/office/spreadsheet/view/render/cache/rendertextmodel";
import type { MergedRangeData, RenderCellModel } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellmodel";
import { type YieldCellOptions, RenderCellCache } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellcache";

// re-exports =================================================================

export type { RenderHeaderModel, YieldCellOptions };

// types ======================================================================

/**
 * Type mapping for the events emitted by `RenderCache` instances.
 */
export interface RenderCacheEventMap {

    /**
     * Will be emitted after the boundary intervals of columns or rows became
     * dirty. The document view of the active sheet needs to refresh the header
     * and grid pane layout.
     */
    "cache:dirty": [];

    /**
     * Will be emitted after creating or updating the dirty cell cache entries
     * asynchronously.
     */
    "cache:ready": [renderRanges: RangeArray];
}

/**
 * An instance of the class `RenderHeaderModel` with existing "renderStyle"
 * property.
 */
export type RenderStyleHeaderModel = RenderHeaderModel & { renderStyle: RenderStyleModel };

/**
 * An instance of the class `RenderCellModel` with existing "renderStyle"
 * property.
 */
export type RenderStyleCellModel = RenderCellModel & { renderStyle: RenderStyleModel };

/**
 * An instance of the class `RenderCellModel` with existing "renderText"
 * property.
 */
export type RenderTextCellModel = RenderCellModel & { renderText: RenderTextModel };

// class RenderCache ==========================================================

/**
 * A cache that stores rendering settings for the visible areas in a sheet.
 */
export class RenderCache extends SheetChildModel<RenderCacheEventMap> {

    /** The cache for all rendering styles (pre-processed cell auto-styles). */
    readonly #styleCache: RenderStyleCache;

    /** The cache for visible column headers to be rendered. */
    readonly #colCache: RenderHeaderCache;

    /** The cache for visible row headers to be rendered. */
    readonly #rowCache: RenderHeaderCache;

    /** Cache entries for all cells to be rendered, and the visible merged ranges. */
    readonly #cellCache: RenderCellCache;

    /** The current column boundary intervals and pixel positions (missing: hidden). */
    readonly #colBoundaries = new Map<PaneSide, HeaderBoundary>();

    /** The current row boundary intervals and pixel positions (missing: hidden). */
    readonly #rowBoundaries = new Map<PaneSide, HeaderBoundary>();

    /** The current boundary ranges and rectangles (missing entry: hidden). */
    readonly #gridBoundaries = new Map<PanePos, GridBoundary>();

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel) {
        super(sheetModel);

        // private properties
        this.#styleCache = this.member(new RenderStyleCache(sheetModel));
        this.#colCache = this.member(new RenderHeaderCache(sheetModel, this.#styleCache, true));
        this.#rowCache = this.member(new RenderHeaderCache(sheetModel, this.#styleCache, false));
        this.#cellCache = this.member(new RenderCellCache(sheetModel, this.#styleCache, this.#colCache, this.#rowCache));

        // start listening to model events after document import is finished
        this.waitForImportSuccess(this.#connectAfterImport);

        // forward the "cache:dirty" events of the header caches debounced
        const emitCacheDirty = this.debounce(() => this.trigger("cache:dirty"), { delay: "microtask" });
        this.listenTo(this.#colCache, "cache:dirty", emitCacheDirty);
        this.listenTo(this.#rowCache, "cache:dirty", emitCacheDirty);

        // forward the events for asynchronous updates of cell cache entries
        this.listenTo(this.#cellCache, "cache:ready", renderRanges => this.trigger("cache:ready", renderRanges));
    }

    // public methods ---------------------------------------------------------

    /**
     * Locks this instance, i.e. prevents updating the internal cache entries
     * after changes in the document or sheet model.
     */
    lockCache(): void {
        this.#colCache.lockCache();
        this.#rowCache.lockCache();
        this.#cellCache.lockCache();
    }

    /**
     * Unlocks this instance, i.e. resumes updating the internal cache entries
     * after changes in the document or sheet model.
     */
    unlockCache(): void {
        this.#colCache.unlockCache();
        this.#rowCache.unlockCache();
        this.#cellCache.unlockCache();
    }

    /**
     * Returns whether this cache is currently busy updating its cell entries
     * asynchronously. A "cache:ready" event will be emitted when all cell
     * cache entries will be available.
     *
     * @returns
     *  Whether this cache is currently updating its cell entries.
     */
    isCacheBusy(): boolean {
        return this.#cellCache.isCacheBusy();
    }

    /**
     * Updates the boundaries of this rendering cache according to the passed
     * boundary intervals of the header panes, and returns descriptors for all
     * changed cell boundary ranges.
     *
     * @param headerBoundaryMap
     *  All changed boundary intervals, mapped by pane side identifiers. The
     *  boundary intervals of pane sides missing in this map will not be
     *  changed. The string "hidden" indicates that the respective layer ranges
     *  will be removed from the rendering cache.
     *
     * @returns
     *  A map containing descriptors for all changed layer ranges, mapped by
     *  pane position identifier. The boundary ranges of pane positions missing
     *  in this map have not been changed. The string "hidden" indicates that a
     *  layer range has been removed from the rendering cache.
     */
    @renderLogger.profileMethod("$badge{RenderCache} updatePaneBoundaries")
    updatePaneBoundaries(headerBoundaryMap: Map<PaneSide, HeaderBoundarySpec>): Map<PanePos, GridBoundarySpec> {

        // flags for changed intervals (per pane side, and per column/row orientation)
        const changedSides = new Set<PaneSide>();
        const changedDirs = new Set<boolean>();

        // process all changed header intervals
        for (const [paneSide, boundary] of headerBoundaryMap) {

            // collections according to direction
            const columns = isColumnSide(paneSide);
            const collection = columns ? this.sheetModel.colCollection : this.sheetModel.rowCollection;
            const boundaries = columns ? this.#colBoundaries : this.#rowBoundaries;

            // shrink the boundary interval to visible columns/rows
            const visible = boundary !== "hidden";
            const interval = visible && collection.shrinkIntervalToVisible(boundary.interval);
            if (visible && interval) {
                boundary.interval = interval;
                boundaries.set(paneSide, boundary);
            } else if (!boundaries.delete(paneSide)) {
                continue;
            }

            // update change flags
            changedSides.add(paneSide);
            changedDirs.add(columns);
        }

        // nothing more to do, if no boundary intervals have changed at all
        if (!changedSides.size) { return new Map(); }

        // calculate the resulting column/row boundary intervals of all visible panes
        const colBoundIntervals = IntervalArray.mergeIntervals(this.#colBoundaries.values(), boundary => boundary.interval);
        const rowBoundIntervals = IntervalArray.mergeIntervals(this.#rowBoundaries.values(), boundary => boundary.interval);

        // the pane positions of all changed boundary ranges
        const changedPanes = new Set<PanePos>();

        // calculate the new boundary ranges for all combinations of column/row intervals
        for (const panePos of ALL_PANE_POSITIONS) {

            // column/row side identifier (nothing to do, if neither interval has changed)
            const colPaneSide = getColPaneSide(panePos);
            const rowPaneSide = getRowPaneSide(panePos);
            if (!changedSides.has(colPaneSide) && !changedSides.has(rowPaneSide)) { continue; }

            // current column/row interval and position
            const colBoundary = this.#colBoundaries.get(colPaneSide);
            const rowBoundary = this.#rowBoundaries.get(rowPaneSide);

            // update boundary range
            if (colBoundary && rowBoundary) {
                this.#gridBoundaries.set(panePos, {
                    rectangle: Rectangle.fromIntervals(colBoundary.position, rowBoundary.position),
                    range: Range.fromIntervals(colBoundary.interval, rowBoundary.interval)
                });
            } else if (!this.#gridBoundaries.delete(panePos)) {
                continue;
            }

            // update change flags
            changedPanes.add(panePos);
        }

        // debug logging
        renderLogger.log(() => `changedSides=${Array.from(changedSides).join()} changedPanes=${Array.from(changedPanes).join()}`);

        // initialize header caches (geometry and formatting of all visible columns/rows)
        for (const columns of changedDirs) {
            const headerCache = columns ? this.#colCache : this.#rowCache;
            const boundIntervals = columns ? colBoundIntervals : rowBoundIntervals;
            headerCache.updatePaneBoundaries(boundIntervals);
        }

        // initialize cell cache (boundary ranges, merged ranges)
        this.#cellCache.updatePaneBoundaries();
        this.#cellCache.updateMergedRanges();

        // return a map with the changed range boundaries to caller
        const resultBoundaryMap = new Map<PanePos, GridBoundarySpec>();
        for (const panePos of changedPanes) {
            const boundary = this.#gridBoundaries.get(panePos);
            resultBoundaryMap.set(panePos, boundary ?? "hidden");
        }
        return resultBoundaryMap;
    }

    /**
     * Returns the position of the specified cell in device pixels as used for
     * canvas rendering. The resulting rectangle will be rounded down to
     * integers, to ensure to render the cell content into entire device pixels
     * without blur effects.
     *
     * @param address
     *  The address of a cell.
     *
     * @returns
     *  The location of the cell in the canvas, in device pixels.
     */
    getCanvasRectForCell(address: Address): Rectangle {
        return getCanvasRect(this.sheetModel.getCellRectangle(address, { pixel: true, expandMerged: true }));
    }

    /**
     * Returns the position of the specified cell range in device pixels as
     * used for canvas rendering. The resulting rectangle will be rounded down
     * to integers, to ensure to render the cell content into entire device
     * pixels without blur effects.
     *
     * @param range
     *  The address of a cell range.
     *
     * @returns
     *  The location of the cell range in the canvas, in device pixels.
     */
    getCanvasRectForRange(range: Range): Rectangle {
        return getCanvasRect(this.sheetModel.getRangeRectangle(range, { pixel: true }));
    }

    /**
     * Returns an iterator that yields all header models contained in the
     * specified cell range.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @param columns
     *  Whether to yield the column headers (`true`), or the row headers
     *  (`false`).
     *
     * @yields
     *  The header models contained in the passed cell range.
     */
    *yieldHeaderModels(range: Range, columns: boolean): IterableIterator<RenderHeaderModel> {
        const headerCache = columns ? this.#colCache : this.#rowCache;
        yield* headerCache.yieldHeaderModels(range.interval(columns));
    }

    /**
     * Returns an iterator that yields all header models with an existing
     * rendering style contained in the specified cell range.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @param columns
     *  Whether to yield the column headers (`true`), or the row headers
     *  (`false`).
     *
     * @yields
     *  The header models with a rendering styles contained in the passed cell
     *  range.
     */
    *yieldStyleHeaderModels(range: Range, columns: boolean): IterableIterator<RenderStyleHeaderModel> {
        const headerCache = columns ? this.#colCache : this.#rowCache;
        for (const headerModel of headerCache.yieldHeaderModels(range.interval(columns))) {
            if (headerModel.renderStyle) { yield headerModel as RenderStyleHeaderModel; }
        }
    }

    /**
     * Returns an iterator that yields the descriptors of all merge ranges that
     * overlap with the specified cell range.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @yields
     *  The descriptors of all merged range overlapping with the passed cell
     *  range.
     */
    *yieldMergedRangeData(range: Range): IterableIterator<MergedRangeData> {
        yield* this.#cellCache.yieldMergedRangeData(range);
    }

    /**
     * Returns an iterator that yields all cell cache entries contained in the
     * specified cell range.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The cell cache entries contained in the passed cell range.
     */
    *yieldCellModels(range: Range, options?: YieldCellOptions): IterableIterator<RenderCellModel> {
        yield* this.#cellCache.yieldCellModels(range, options);
    }

    /**
     * Returns an iterator that yields all cell cache entries contained in the
     * specified cell range with an existing render style (i.e., skips all
     * undefined cells).
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The cell cache entries with render style contained in the passed cell
     *  range.
     */
    *yieldStyleCellModels(range: Range, options?: YieldCellOptions): IterableIterator<RenderStyleCellModel> {
        for (const renderModel of this.#cellCache.yieldCellModels(range, options)) {
            if (renderModel.renderStyle) { yield renderModel as RenderStyleCellModel; }
        }
    }

    /**
     * Returns an iterator that yields the rendering text descriptors of
     * non-blank cells.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @yields
     *  The rendering text descriptors of all non-blank cells contained in the
     *  passed cell range.
     */
    *yieldTextCellModels(range: Range): IterableIterator<RenderTextCellModel> {
        for (const renderModel of this.#cellCache.yieldCellModels(range, { origins: true, overflow: true })) {
            if (renderModel.renderText) { yield renderModel as RenderTextCellModel; }
        }
    }

    /**
     * Returns the next visible rendering cell model in a specific direction.
     *
     * @param renderModel
     *  The source rendering cell model.
     *
     * @param columns
     *  Whether to move to a vertical neighbor (`true`, i.e. up or down), or to
     *  a horizontal neighbor (`false`, i.e. left or right).
     *
     * @param leading
     *  Whether to move to a leading neighbor (`true`, i.e. top or left), or to
     *  a trailing neighbor (`false`, i.e. down or right).
     *
     * @returns
     *  The adjacent rendering cell model, if available.
     */
    getAdjacentCellModel(renderModel: RenderCellModel, columns: boolean, leading: boolean): Opt<RenderCellModel> {
        const header1 = columns ? renderModel.rowHeader : renderModel.colHeader;
        const header2 = leading ? header1.prev : header1.next;
        if (!header2) { return undefined; }
        const col = columns ? renderModel.address.c : header2.index;
        const row = columns ? header2.index : renderModel.address.r;
        return this.#cellCache.getRenderCellModel(col, row);
    }

    /**
     * Returns an iterator that yields the next visible cell entries in the
     * specified direction.
     *
     * @param renderModel
     *  The source cell entry.
     *
     * @param columns
     *  Whether to move to the vertical neighbors (`true`, i.e. up or down), or
     *  to the horizontal neighbors (`false`, i.e. left or right).
     *
     * @param leading
     *  Whether to move to the leading neighbors (`true`, i.e. top or left), or
     *  to the trailing neighbors (`false`, i.e. down or right).
     *
     * @yields
     *  The available adjacent cell entries in the specified direction.
     */
    *yieldAdjacentCellModels(renderModel: RenderCellModel, columns: boolean, leading: boolean): IterableIterator<RenderCellModel> {
        let nextModel: Opt<RenderCellModel> = renderModel;
        while ((nextModel = this.getAdjacentCellModel(nextModel, columns, leading))) {
            yield nextModel;
        }
    }

    /**
     * Returns the next visible cell entry in the specified direction (in the
     * same row) that acts as text clipping boundary for the specified cell
     * (with own rendering text, or as part of a merged range).
     *
     * @param renderModel
     *  The source cell entry.
     *
     * @param leading
     *  whether to search in the leading neighbors (`true`, i.e. left), or in
     *  the trailing neighbors (`false`, i.e. right).
     *
     * @returns
     *  The cell entry acting as text clipping boundary, if available.
     */
    findAdjacentTextClipEntry(renderModel: RenderCellModel, leading: boolean): Opt<RenderCellModel> {
        for (const nextModel of this.yieldAdjacentCellModels(renderModel, false, leading)) {
            if (!nextModel.isBlank || nextModel.mergedData) {
                return nextModel;
            }
        }
        return undefined;
    }

    /**
     * Annotates a DOM element with data attributes about the rendering styles
     * of a cell. The data attributes will be used by automated tests.
     *
     * @param elem
     *  The DOM element to be annotated.
     *
     * @param address
     *  The address of a cell.
     */
    annotateElement(elem: HTMLElement, address: Address): void {
        this.#cellCache.annotateElement(elem, address);
    }

    // private methods --------------------------------------------------------

    /**
     * Invalidates all child caches.
     */
    #invalidateCaches(): void {
        this.#styleCache.invalidateCache();
        this.#colCache.invalidateCache();
        this.#rowCache.invalidateCache();
        this.#cellCache.invalidateCache();
    }

    /**
     * Connects this instance to the sheet model, after document import has
     * succeeded (registers all needed event handlers).
     */
    @renderLogger.profileMethod("$badge{RenderCache} connectAfterImport")
    #connectAfterImport(): void {

        // after import, entire cache needs to be refreshed (especially after preview mode of the
        // active sheet which lacks correct rendering of conditional formatting and table styling)
        this.#invalidateCaches();

        // browser zoom factor changes column/row geometry
        this.listenToGlobal("change:resolution", () => {
            renderLogger.trace("$badge{RenderCache} $event{change:resolution}");
            this.#invalidateCaches();
        });

        // sheet zoom factor changes column/row geometry
        this.listenToProp(this.sheetModel.propSet, "zoom", () => {
            renderLogger.trace("$badge{RenderCache} $event{change:zoom}");
            this.#invalidateCaches();
        });
    }
}
