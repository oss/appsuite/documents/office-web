/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary } from "@/io.ox/office/tk/algorithms";
import type { Rectangle } from "@/io.ox/office/tk/dom";
import type { CanvasLineStyle, CanvasFontStyle } from "@/io.ox/office/tk/canvas";
import { Path } from "@/io.ox/office/tk/canvas";

import type { DisplayText } from "@/io.ox/office/editframework/model/formatter/baseformatter";

import { hasWrappingAttributes } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { getTextOrientation, splitDisplayString, cleanDisplayString } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { CFRuleRenderProps } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

import { getCanvasCoord, getCanvasRect, getCssCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderStyleModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylemodel";
import type { MergedRangeData } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellmodel";

// types ======================================================================

/**
 * Descriptor for text decoration (underline, strikethrough).
 */
export interface TextDecoration {

    /**
     * The canvas line styles needed to render the text decoration.
     */
    line: CanvasLineStyle;

    /**
     * The canvas rendering path containing the line segments for all text
     * decorations in all text lines.
     */
    path: Path;
}

// class RenderTextWord =======================================================

/**
 * Represents all settings needed to render a single word with leading
 * white-space in a single line of text for a cell. Used especially for
 * horizontally justified text where the width of gaps between the words
 * depends on the total width available for the text line.
 */
export class RenderTextWord {

    /**
     * The width of all white-space characters preceding this word, in canvas
     * pixels, according to the font settings of the cell, and the sheet zoom
     * factor.
     */
    readonly space: number;

    /**
     * The word text.
     */
    readonly text: string;

    /**
     * The width of the word (without any white-space), in canvas pixels,
     * according to the font settings of the cell, and the sheet zoom factor.
     */
    readonly width: number;

    /**
     * The horizontal position of the word, in canvas pixels, relative to the
     * start position of the text line containing this word.
     */
    x: number;

    // constructor ------------------------------------------------------------

    constructor(space: number, text: string, width: number, offset = 0) {
        this.space = space;
        this.text = text;
        this.width = width;
        this.x = offset;
    }
}

// class RenderTextLine =======================================================

/**
 * Represents all settings needed to render a single line of text in a
 * (multi-line) cell.
 */
export class RenderTextLine {

    /**
     * The text to be rendered in this text line.
     */
    readonly text: string;

    /**
     * The total width of the text, in canvas pixels, according to the font
     * settings of the cell, and the sheet zoom factor.
     */
    readonly width: number;

    /**
     * The absolute horizontal position of the text line, in canvas pixels,
     * according to the position of the cell, the horizontal alignment, and the
     * length of the text.
     */
    x = 0;

    /**
     * The absolute vertical position of the font base line for this text line,
     * in canvas pixels, according to the position of the cell, the font
     * settings, the vertical alignment, and the number of text lines.
     */
    y = 0;

    /**
     * Detailed settings for the single words and the white-space characters
     * contained in this text line, e.g. for horizontally justified alignment.
     * If omitted, the entire text line as contained in the property "text"
     * must be rendered at once.
     */
    readonly words: Opt<readonly RenderTextWord[]>;

    // constructor ------------------------------------------------------------

    constructor(text: string, width: number, words?: RenderTextWord[]) {
        this.text = text;
        this.width = width;
        this.words = words;
    }
}

// class RenderTextModel ======================================================

/**
 * Represents all settings needed to render text contents of a single cell.
 */
export class RenderTextModel {

    /**
     * The complete formatted text to be rendered.
     */
    readonly text: DisplayText;

    /**
     * The location of the inner cell rectangle, without trailing grid lines,
     * expanded to the size of a merged range if existing.
     */
    readonly rect: Rectangle;

    /**
     * An array with descriptors for all text lines to be rendered.
     */
    readonly lines: RenderTextLine[] = [];

    /**
     * The canvas fill style needed to render the text contents with the color
     * specified in the cell's formatting attributes.
     */
    readonly fill: string;

    /**
     * The canvas font styles needed to render the text contents with the font
     * settings specified in the cell's formatting attributes.
     */
    readonly font: Partial<CanvasFontStyle>;

    /**
     * The text decoration styles (underline, and strike-through); or `null`,
     * if the text will not be decorated.
     */
    readonly decoration: Opt<TextDecoration>;

    /**
     * The required width of the text contents (regardless of the actual size
     * or inner padding of the cell), in CSS pixels. If set to zero, the
     * content width for this cell is not available (e.g., if the cell is
     * blank). Can be used to determine the optimal width of the column
     * containing this cell.
     */
    readonly contentWidth: number;

    /**
     * The required height of the text contents (regardless of the actual size
     * or inner padding of the cell), in CSS pixels. If set to zero, the
     * content height is not available (e.g., if the cell is undefined). Can be
     * used to determine the optimal height of the row containing this cell.
     */
    readonly contentHeight: number;

    /**
     * The horizontal padding size that cannot be used for text contents, in
     * CSS pixels. This is the sum of left and right padding, plus one pixel
     * for the trailing grid line.
     */
    readonly contentPadding: number;

    /**
     * Whether the text wants to float into the outer space left of the inner
     * cell area.
     */
    readonly overflowL: boolean = false;

    /**
     * Whether the text wants to float into the outer space right of the inner
     * cell area.
     */
    readonly overflowR: boolean = false;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, cellModel: CellModel, cssRect: Rectangle, mergedData: Opt<MergedRangeData>, renderStyle: RenderStyleModel, renderProps: Opt<CFRuleRenderProps>) {

        // the inner rectangle for text rendering (TODO: Currently, without an entire CSS pixel for grid lines
        // as long as this is used to calculate the optimal row height/column width. In the future, a single canvas
        // pixel can be removed as grid lines are always one pixel wide.)
        this.rect = getCanvasRect(cssRect.clone().expandSelf(0, 0, -1, -1));

        // the merged attribute set
        const attrSet = renderStyle.attrSet;
        const charAttrs = attrSet.character;
        const cellAttrs = attrSet.cell;

        // height of a single text line (effective height of an empty cell), in canvas pixels
        const rowHeight = renderStyle.canvasRowHeight;

        // the inner horizontal padding for text in cells (according to current zoom), in CSS pixels
        const cellPaddingR = sheetModel.getTextPadding(charAttrs);
        const cellPaddingL = renderProps?.iconSet ? renderStyle.rowHeight : cellPaddingR;
        const cellPadding = cellPaddingL + cellPaddingR + 1;

        // the inner horizontal padding for text in cells (according to current zoom), in canvas pixels
        const paddingL = getCanvasCoord(cellPaddingL);
        const paddingR = getCanvasCoord(cellPaddingR);

        // the available width for the cell text (without left/right padding and grid), in canvas pixels
        const availableWidth = getCanvasCoord(Math.max(2, cssRect.width - cellPadding));

        // resolved and scaled font settings
        const renderFont = renderStyle.font;
        // position of the font base line in the text lines (according to effective row height)
        const baseLineOffset = renderFont.getBaseLineOffset(rowHeight);

        // resulting width of text contents (maximum width of text lines), in canvas pixels
        let totalWidth = 0;

        // Format the value with the effective number format code, and to the available pixel width (except strings
        // which can exceed their cell area). If the `text` property in the result object is `null`, the cell will show
        // the "railroad track error".
        const { docModel } = sheetModel;
        const exceedText = cellModel.isText(); // booleans and error codes must not exceed the cell
        const formatResult = docModel.numberFormatter.formatValue(renderStyle.format, cellModel.v, { renderFont, maxWidth: availableWidth, exceedText });
        const displayText = this.text = formatResult.text;
        const formatError = displayText === null;

        // effective text orientation (alignment and writing direction)
        const { rtl, cssTextAlign, baseBoxAlign } = getTextOrientation(cellModel.v, cellAttrs.alignHor, displayText, renderStyle.format.isText());

        // The CSS text color, calculated from text color and fill color attributes.
        // - Text will be rendered in fill mode, without outline.
        // - Explicit color from a number format overrides cell text color.
        // - Use implicit hyperlink color if specified (ODF).
        if (formatResult.color) {
            this.fill = formatResult.color.css;
        } else if (!renderStyle.explicitTextColor && sheetModel.docApp.isODF() && !!sheetModel.hlinkCollection.getCellURL(cellModel.a)) {
            this.fill = "#" + docModel.getThemeModel().getSchemeColor("hyperlink", "00f")!;
        } else {
            this.fill = renderStyle.textColor;
        }

        // all font style properties for canvas rendering
        this.font = { font: renderFont.getCanvasFont(), rtl };

        // special processing for text cells: may span over multiple lines, or may overflow out of the cell area
        if (!formatError && cellModel.isText()) {

            // automatic text wrapping: may result in multiple text lines, trimmed whitespace (also in single line text)
            if (hasWrappingAttributes(attrSet)) {

                // whether text will be justified horizontally
                const justify = cssTextAlign === "justify";

                // create settings for each text line
                for (const paragraph of splitDisplayString(displayText)) {

                    // collect maximum width of entire paragraph texts before wrapping
                    totalWidth = Math.max(totalWidth, renderFont.getTextWidth(paragraph));

                    // split paragraph to text lines
                    const textLines = renderFont.getTextLines(paragraph, availableWidth);

                    // create settings for each text line in the paragraph
                    for (const [lineIndex, textLine] of textLines.entries()) {

                        // remove leading/trailing whitespace (but not leading whitespace in first line)
                        const trimLine = textLine.replace(lineIndex ? /^\s+|\s+$/ : /\s+$/, "");

                        // pixel width of the text line
                        let lineWidth = renderFont.getTextWidth(trimLine);

                        // split horizontally justified text into words (but not single characters, or the last line of the paragraph)
                        let renderWords: Opt<RenderTextWord[]>;
                        if (justify && (trimLine.length > 1) && (lineIndex + 1 < textLines.length) && (lineWidth < availableWidth)) {

                            // total width of white-space characters
                            let totalSpace = 0;
                            // total width of the words without white-space
                            let wordsWidth = 0;

                            // extract all words from the text line
                            renderWords = [];
                            for (let text = trimLine; text.length;) {

                                // extract leading white-space characters, and a following word
                                const matches = /^(\s*)(\S*)/.exec(text)!;
                                // width of leading white-space characters
                                const space = renderFont.getTextWidth(matches[1]);
                                // width of the word without white-space
                                const width = renderFont.getTextWidth(matches[2]);

                                // push a new word descriptor to the array
                                renderWords.push(new RenderTextWord(space, matches[2], width));
                                totalSpace += space;
                                wordsWidth += width;

                                // remove the processed part from the text line, and proceed with next word
                                text = text.slice(matches[0].length);
                            }

                            // nothing to do, if no white-space is available
                            if (totalSpace > 0) {

                                // expansion factor for white-space between words
                                const spaceFactor = (availableWidth - wordsWidth) / totalSpace;
                                // relative offset of the current word
                                let offset = rtl ? availableWidth : 0;

                                // calculate relative start position of all words (reverse the words in
                                // right-to-left writing mode - TODO: is this save in all cases?)
                                if (rtl) {
                                    for (const renderWord of renderWords) {
                                        offset -= renderWord.space * spaceFactor + renderWord.width;
                                        renderWord.x = Math.round(offset);
                                    }
                                } else {
                                    for (const renderWord of renderWords) {
                                        offset += renderWord.space * spaceFactor;
                                        renderWord.x = Math.round(offset);
                                        offset += renderWord.width;
                                    }
                                }

                                // set total line width (may be less than availableWidth, e.g. single word with leading space)
                                lineWidth = rtl ? (availableWidth - offset) : offset;
                            } else {
                                renderWords = undefined;
                            }
                        }

                        // push a new line descriptor into the array
                        this.lines.push(new RenderTextLine(trimLine, lineWidth, renderWords));
                    }
                }

            // text cells without wrapping (text may overflow out of the cell)
            } else {

                // remove line breaks from text, replace ASCII control characters with placeholders
                const cleanText = cleanDisplayString(displayText);

                // create a single text line
                totalWidth = renderFont.getTextWidth(cleanText);
                this.lines.push(new RenderTextLine(cleanText, totalWidth));

                // calculate width of the text that overflows from the cell (merged ranges always clip at cell boundaries)
                if (!mergedData && (availableWidth < totalWidth)) {

                    // store width of text overflowing from the cell area, according to alignment
                    const width = this.rect.width;
                    switch (baseBoxAlign) {
                        case "left":
                            this.overflowR = (paddingL + totalWidth) > width;
                            break;
                        case "center":
                            this.overflowL = (totalWidth + 2 * paddingL) > width;
                            this.overflowR = (totalWidth + 2 * paddingR) > width;
                            break;
                        case "right":
                            this.overflowL = (totalWidth + paddingR) > width;
                            break;
                        case "justify":
                            break;
                    }
                }
            }

        // other cells: numbers, booleans, error codes
        } else {

            // first, save the width of original display text (used for optimal column width)
            let effectiveWidth = totalWidth = formatResult.width;

            // show railroad track error when display string does not fit
            let effectiveText: string;
            if (formatError) {
                effectiveText = "#".repeat(Math.max(1, Math.floor(availableWidth / renderFont.getTextWidth("#"))));
                effectiveWidth = renderFont.getTextWidth(effectiveText);
            } else {
                effectiveText = displayText;
            }

            // distribute original format portions to distinct rendering words
            const renderWords = formatError ? undefined : ary.from(formatResult.portions, portion => {
                return (portion.type === "blind") ? undefined : new RenderTextWord(0, portion.text, portion.width, portion.offset);
            });

            // create a single text line
            this.lines.push(new RenderTextLine(effectiveText, effectiveWidth, renderWords));
        }

        // store content size (in CSS pixels!)
        this.contentWidth = getCssCoord(totalWidth);
        this.contentHeight = this.lines.length * renderStyle.rowHeight;
        this.contentPadding = cellPadding;

        // the resulting height of all text lines
        const totalHeight = this.lines.length * rowHeight;
        // vertical position of the first text line
        let offsetY: number;
        // rendering line height (enlarged for vertically adjusted text)
        let lineHeight = rowHeight;

        // calculate vertical start position of the first text line
        switch (cellAttrs.alignVert) {
            case "top":
                offsetY = this.rect.top;
                break;
            case "middle":
                offsetY = this.rect.top + Math.round((this.rect.height - totalHeight) / 2);
                break;
            case "justify":
                offsetY = this.rect.top;
                // enlarge rendering line height if possible
                if ((this.lines.length > 1) && (totalHeight < this.rect.height)) {
                    lineHeight = rowHeight + (this.rect.height - totalHeight) / (this.lines.length - 1);
                }
                break;
            default:
                // bottom alignment, and fall-back for unknown alignments
                offsetY = this.rect.bottom() - totalHeight;
        }

        // adjust vertical offset to font base line position (as used by canvas renderer)
        offsetY += baseLineOffset + 1;

        // calculate effective rendering positions for all text lines
        for (const [lineIndex, renderLine] of this.lines.entries()) {

            // calculate horizontal text position according to alignment
            switch (baseBoxAlign) {
                case "center":
                    renderLine.x = this.rect.left + paddingL + (availableWidth - renderLine.width) / 2;
                    break;
                case "right":
                    renderLine.x = this.rect.right() - paddingR - renderLine.width;
                    break;
                default:
                    // left alignment, and fall-back for unknown alignments
                    renderLine.x = this.rect.left + paddingL;
            }

            // calculate vertical text position
            renderLine.y = Math.round(offsetY + lineIndex * lineHeight);
        }

        // generate canvas paths for text decoration
        const underline = charAttrs.underline;
        const strike = charAttrs.strike !== "none";
        if (underline || strike) {

            // width of the decoration lines, depending on the font size
            const width = Math.max(1, Math.round(renderFont.size / 15));

            // vertical offsets for underline and strike-through styles
            const offsets: number[] = [];
            const pushOffset = (offset: number): void => { offsets.push(Math.round(rowHeight * offset) - width / 2); };
            if (underline) { pushOffset(0.1); }
            if (strike) { pushOffset(-0.2); }

            // generate canvas path with line segments for all text lines
            const path = new Path();
            for (const renderLine of this.lines) {
                const x1 = renderLine.x;
                const x2 = x1 + Math.ceil(renderLine.width);
                for (const offset of offsets) {
                    const y = renderLine.y + offset;
                    path.pushLine(x1, y, x2, y);
                }
            }

            // return the text decoration settings object (TODO: own underline color in ODF?)
            this.decoration = { line: { style: this.fill, width }, path };
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the render text will float outside the cell in the
     * specified direction.
     *
     * @param leading
     *  Whether to check for leading overflow into the preceding cell (`true`),
     *  or for trailing overflow into the following cell (`false`).
     */
    isOverflow(leading: boolean): boolean {
        return leading ? this.overflowL : this.overflowR;
    }
}
