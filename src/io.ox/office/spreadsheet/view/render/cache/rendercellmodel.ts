/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str } from "@/io.ox/office/tk/algorithms";
import { Rectangle, setElementDataAttr } from "@/io.ox/office/tk/dom";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { AttrBorderKey } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { type CellModel, getModelValue, isBlankCell } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { CFRuleRenderProps } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { isErrorCode } from "@/io.ox/office/spreadsheet/model/formula/formulautils";

import { getCanvasRect } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderFillStyle, RenderStyleModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylemodel";
import type { RenderStyleCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylecache";
import type { RenderHeaderModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadermodel";
import type { RenderBorder } from "@/io.ox/office/spreadsheet/view/render/cache/renderborder";
import { RenderTextModel } from "@/io.ox/office/spreadsheet/view/render/cache/rendertextmodel";
import type { CellData } from "@/io.ox/office/spreadsheet/view/render/cache/celldatacollector";

// types ======================================================================

/**
 * Descriptor for a merged range covering at least one cell element in the
 * rendering area.
 */
export interface MergedRangeData {

    /**
     * The address of the original merged range, as contained in the document.
     */
    range: Range;

    /**
     * The address of the visible part of the merged range (without leading and
     * trailing hidden columns and rows).
     */
    shrunken: Range;
}

// class RenderCellModel ======================================================

/**
 * An model for a visible cell that is part of a `RenderCellCache` instance.
 */
export class RenderCellModel {

    /**
     * The column header model of this cell.
     */
    readonly colHeader: RenderHeaderModel;

    /**
     * The row header model of this cell.
     */
    readonly rowHeader: RenderHeaderModel;

    /**
     * The address of the cell represented by this instance.
     */
    readonly address: Address;

    /**
     * The model of the cell represented by this instance.
     */
    readonly cellModel: CellModel | null;

    /**
     * Whether the cell is blank, i.e. does not contain a value. This state is
     * independent of whether the cell has a visible display text. Cells with
     * existing value may appear empty but are not blank, e.g. an empty string
     * as formula result, a number format resulting in an empty string, or
     * hidden text by conditional formatting rules.
     */
    readonly isBlank: boolean;

    /**
     * The descriptor for a merged range covering the cell.
     */
    readonly mergedData: Opt<MergedRangeData>;

    /**
     * The effective location of the cell, with the trailing border lines,
     * expanded to merged ranges, in CSS pixels.
     */
    readonly cssRect: Rectangle;

    /**
     * The effective location of the cell, with the trailing border lines,
     * expanded to merged ranges, in canvas pixels.
     */
    readonly renderRect: Rectangle;

    /**
     * A wrapper for the cell autostyle in the document used to format the
     * cell. If `undefined`, the cell is undefined (no cell model, i.e. blank
     * and unformatted), and nothing will be rendered.
     */
    readonly renderStyle: Opt<RenderStyleModel>;

    /**
     * Additional rendering properties next to the cell formatting attributes,
     * e.g. data bar settings from a conditional formatting rule.
     */
    readonly renderProps: Opt<CFRuleRenderProps>;

    /**
     * A descriptor object for the text contents to be rendered for this cell,
     * including single text lines for multi-line text, and all formatting
     * settings; or `undefined` for blank cells.
     */
    readonly renderText: Opt<RenderTextModel>;

    /**
     * A dictionary specifying which border attributes need to be processed
     * (used to skip the inner borders of merged ranges).
     */
    readonly #drawBorders: Opt<Record<AttrBorderKey, boolean>>;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, styleCache: RenderStyleCache, cellData: CellData) {

        const { colHeader, rowHeader, address, cellModel, mergedData, cfRulesResult, tableProps } = cellData;

        // initialize public properties
        this.colHeader = colHeader;
        this.rowHeader = rowHeader;
        this.address = address;
        this.cellModel = cellModel;
        this.isBlank = isBlankCell(this.cellModel);
        this.mergedData = mergedData;
        this.renderProps = cfRulesResult?.renderProps;

        // the auto-style identifier of the source cell (use default style for undefined cells with conditional formatting)
        const styleUid = cellData.baseSuid ?? ((cfRulesResult || tableProps) ? sheetModel.cellCollection.getDefaultStyleId(address, { useUid: true }) : undefined);
        this.renderStyle = is.string(styleUid) ? styleCache.getStyleModel(styleUid, cellData.borderSuid, cfRulesResult, tableProps) : undefined;

        // determine the cell rectangles
        if (this.mergedData) {
            this.cssRect = sheetModel.getRangeRectangle(this.mergedData.shrunken, { pixel: true });
            this.renderRect = getCanvasRect(this.cssRect);
        } else {
            this.cssRect = Rectangle.fromIntervals(this.colHeader.cssPos, this.rowHeader.cssPos);
            this.renderRect = Rectangle.fromIntervals(this.colHeader.renderPos, this.rowHeader.renderPos);
        }

        // do not paint inner borders in a merged range
        if (this.mergedData) {
            const { a1, a2 } = this.mergedData.shrunken;
            const l = this.address.c === a1.c;
            const r = this.address.c === a2.c;
            const t = this.address.r === a1.r;
            const b = this.address.r === a2.r;
            this.#drawBorders = { l, r, t, b, d: l && t, u: l && t };
        }

        // No render text for cells without visible display text (blank cells, cells covered by merged ranges).
        // Conditional formatting (data bars or icon sets) may specify to hide the cell display text as well.
        if (!this.isBlank && cellModel && this.renderStyle && !mergedData?.shrunken.a1.differs(this.address) && !this.renderProps?.hideValue) {
            this.renderText = new RenderTextModel(sheetModel, cellModel, this.cssRect, mergedData, this.renderStyle, this.renderProps);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the effective fill style of this cell. The fill color from a
     * conditional formatting rule of type "colorScale" will be preferred.
     *
     * @returns
     *  The effective fill style for this cell.
     */
    getFillStyle(): RenderFillStyle {
        // prefer color scale from conditional formatting
        return this.renderProps?.colorScale?.color ?? this.renderStyle?.fill ?? null;
    }

    /**
     * Returns the effective style of a single border line of this cell. If the
     * cell does not contain own formatting attributes, the default border
     * styles form the row or column will be returned.
     *
     * @param key
     *  The short key of the border line to be returned.
     *
     * @returns
     *  The effective rendering style of the specified border line; or
     *  `undefined`, if the border line is invisible.
     */
    getRenderBorder(key: AttrBorderKey): Opt<RenderBorder> {
        const borders = this.renderStyle?.borders ?? this.rowHeader.renderStyle?.borders ?? this.colHeader.renderStyle?.borders;
        return (borders && (!this.#drawBorders || this.#drawBorders[key])) ? borders.get(key) : undefined;
    }

    /**
     * The required width of the text contents, in CSS pixels.
     *
     * @returns
     *  The required width of the text contents, in CSS pixels.
     */
    getOptimalContentWidth(): number {
        return this.renderText?.contentWidth ? (this.renderText.contentWidth + this.renderText.contentPadding) : 0;
    }

    /**
     * The required height of the text contents, in CSS pixels.
     *
     * @returns
     *  The required height of the text contents, in CSS pixels.
     */
    getOptimalContentHeight(): number {
        return this.renderText?.contentHeight ?? this.renderStyle?.rowHeight ?? 0;
    }

    /**
     * Annotates a DOM element with data attributes about the rendering styles
     * of the cell represented by this instance. The data attributes will be
     * used by automated tests.
     *
     * @param sheetModel
     *  The model of the sheet containing the cell.
     *
     * @param elem
     *  The DOM element to be annotated.
     */
    annotateElement(sheetModel: SheetModel, elem: HTMLElement): void {

        // shortcuts to members
        const { cellCollection } = sheetModel;

        // helper function to change a data attribute
        const setDataAttr = setElementDataAttr.bind(null, elem);

        setDataAttr("address", this.address.toOpStr());
        setDataAttr("merged", this.mergedData?.range.toOpStr() ?? null);

        // type, value, display string, and formula of the active cell
        const value = getModelValue(this.cellModel);
        const cellDisplay = this.cellModel ? this.cellModel.d : "";         // column-independent display string (used in formulas)
        const renderDisplay = this.renderText ? this.renderText.text : "";  // actual display string rendered into column width
        const fmlaDesc = cellCollection.getTokenArray(this.address, { grammarType: "ui", fullMatrix: true });

        setDataAttr("cellType",             (value === null) ? "blank" : isErrorCode(value) ? "error" : typeof value);
        setDataAttr("cellValue",            (value === null) ? null : String(value));
        setDataAttr("cellDisplay",          cellDisplay);
        setDataAttr("cellDisplayError",     (cellDisplay === null) ? true : null);
        setDataAttr("renderDisplay",        renderDisplay);
        setDataAttr("renderDisplayError",   (renderDisplay === null) ? true : null);
        setDataAttr("cellFormula",          fmlaDesc?.formula || null);
        setDataAttr("sharedIndex",          fmlaDesc?.sharedIndex ?? null);
        setDataAttr("matrixRange",          fmlaDesc?.matrixRange?.toOpStr() ?? null);
        setDataAttr("dynamicMatrix",        fmlaDesc?.dynamicMatrix ?? null);
        setDataAttr("cellHlink",            cellCollection.getEffectiveURL(this.address));

        // effective rendering data from rendering cache
        if (this.renderStyle) {
            const { borders, font } = this.renderStyle;
            const fill = this.getFillStyle();

            setDataAttr("renderFill",      !fill ? "transparent" : (fill instanceof CanvasPattern) ? "pattern" : (fill instanceof CanvasGradient) ? "gradient" : fill.toLowerCase());
            setDataAttr("renderColor",     (this.renderText?.fill ?? this.renderStyle.textColor).toLowerCase());
            setDataAttr("renderFont",      font.family);
            setDataAttr("renderFontSize",  `${font.size}pt`);
            setDataAttr("renderFontStyle", str.concatTokens(font.bold ? "bold" : "", font.italic ? "italic" : "") || "normal");
            for (const [key, border] of borders) {
                const attrName = `renderBorder${key.toUpperCase()}`;
                if (border) {
                    const style = border.hair ? "hair" : border.double ? "double" : border.style;
                    setDataAttr(attrName, `${border.width}px ${style} ${border.color.toLowerCase()}`);
                } else {
                    setDataAttr(attrName, "none");
                }
            }
        }

        // additional rendering data for conditional formatting
        if (this.renderProps) {
            const { dataBar, iconSet } = this.renderProps;
            if (dataBar) { setDataAttr("cfDataBar", `${dataBar.min} ${dataBar.max} ${dataBar.color1.toLowerCase()}`); }
            if (iconSet) { setDataAttr("cfIconSet", `${iconSet.id} ${iconSet.index + 1}`); }
        }
    }
}
