/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun, map, json } from "@/io.ox/office/tk/algorithms";

import type { OpColor } from "@/io.ox/office/editframework/utils/color";
import { equalBorders } from "@/io.ox/office/editframework/utils/border";

import type { CellAttributeSet, PtCellAttributeSet } from "@/io.ox/office/spreadsheet/utils/cellattrs";
import { OUTER_BORDER_KEYS, getBorderName } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { CFRulesFormatResult } from "@/io.ox/office/spreadsheet/model/cfrulecollection";
import type { TableCellFormatResult, TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import { type RenderStyleBorderPriorityMap, RenderStyleModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylemodel";

// types ======================================================================

export interface TableRenderProps extends TableCellFormatResult {
    tableModel: TableModel;
}

// constants ==================================================================

// the attribute names of all outer borders
const OUTER_BORDER_NAMES = OUTER_BORDER_KEYS.map(k => getBorderName(k));

// class RenderStyleCache =====================================================

/**
 * A cache that stores rendering information for all used autostyles in a
 * sheet.
 */
export class RenderStyleCache extends SheetChildModel {

    /** All rendering style models already cached. */
    readonly #styleModels = new Map<string, RenderStyleModel>();

    /** All cache keys associated with autostyles (mapped by autostyle UIDs) */
    readonly #styleKeys = new Map<string, string[]>();

    /** All cache keys associated with CF rules (mapped by rule model UIDs) */
    readonly #ruleKeys = new Map<string, string[]>();

    /** All cache keys associated with table ranges (mapped by table model UIDs) */
    readonly #tableKeys = new Map<string, string[]>();

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel) {
        super(sheetModel);

        // start listening to model events after document import is finished
        this.waitForImportSuccess(this.#connectAfterImport);
    }

    // public methods ---------------------------------------------------------

    /**
     * Deletes all cached rendering styles.
     */
    invalidateCache(): void {
        this.#styleModels.clear();
        this.#styleKeys.clear();
        this.#ruleKeys.clear();
        this.#tableKeys.clear();
    }

    /**
     * Returns a render style with rendering information for the specified cell
     * auto-style.
     *
     * @param styleUid
     *  The unique style identifier of an auto-style.
     *
     * @param [borderStyleUid]
     *  The unique identifier of an additional auto-style that will be used to
     *  resolve the style settings of all outer border attributes.
     *
     * @param [cfRulesResult]
     *  A descriptor for matching conditional formatting rules with a partial
     *  attribute set to be merged over the attributes of the auto-style. The
     *  cell attribute set MUST NOT contain a style sheet identifier.
     *
     * @param [tableProps]
     *  A descriptor with a partial attribute set from a table stylesheet to be
     *  merged with the attributes of the auto-style. The cell attribute set
     *  MUST NOT contain a stylesheet identifier.
     *
     * @returns
     *  The render style for the specified auto-style.
     */
    getStyleModel(styleUid: string, borderStyleUid?: string, cfRulesResult?: CFRulesFormatResult, tableProps?: TableRenderProps): Readonly<RenderStyleModel> {

        // the cache key for the autostyle and additional styles from CF rules and tables
        let cacheKey = styleUid;

        // add key particle for border style of merged range
        if (borderStyleUid) {
            cacheKey += "," + borderStyleUid;
        }

        // add key particle for conditional formatting
        if (cfRulesResult) {
            for (const ruleModel of cfRulesResult.ruleModels) {
                cacheKey += "," + ruleModel.uid;
            }
        }

        // add key particle for table formatting
        if (tableProps) {
            cacheKey += "," + tableProps.tableModel.uid + "#" + String(tableProps.attrSetFlags);
        }

        // if style exists already in the cache, update it on demand
        return map.upsert(this.#styleModels, cacheKey, () => {

            // cache the complete style keys separated by auto-style identifiers
            // (needed to clean style models for changed or deleted auto-styles)
            this.#registerCacheKey(this.#styleKeys, styleUid, cacheKey);

            // cache the complete style keys separated by CF rule identifiers
            // (needed to clean style models for changed or deleted rules)
            if (cfRulesResult) {
                for (const ruleModel of cfRulesResult.ruleModels) {
                    this.#registerCacheKey(this.#ruleKeys, ruleModel.uid, cacheKey);
                }
            }

            // cache the complete style keys separated by table identifiers
            // (needed to clean style models for changed or deleted table ranges)
            if (tableProps) {
                this.#registerCacheKey(this.#tableKeys, tableProps.tableModel.uid, cacheKey);
            }

            // the auto-style collection of the document
            const { autoStyles } = this.docModel;
            // the merged attribute set of the auto-style
            let mergedAttrSet: CellAttributeSet = autoStyles.getMergedAttributeSet(styleUid, true);
            // the rendering priorities of the outer cell borders
            const borderPrios: RenderStyleBorderPriorityMap = new Map();
            // text color of an explicit formatting attribute
            let explicitTextColor: Opt<OpColor>;

            // creates a deep clone of the merged attribute set on first call
            const ensureClonedAttrSet = fun.once(() => { mergedAttrSet = json.deepClone(mergedAttrSet); });

            // updates the rendering priority of all border lines in the merged attribute set
            const updateBorderPrios = (attrSet: PtCellAttributeSet, borderPrio: number): void => {
                if (!attrSet.cell) { return; }
                const mergedCellAttrs = mergedAttrSet.cell;
                for (const borderName of OUTER_BORDER_NAMES) {
                    const border = attrSet.cell[borderName];
                    if (border && !borderPrios.has(borderName) && equalBorders(border, mergedCellAttrs[borderName])) {
                        borderPrios.set(borderName, borderPrio);
                    }
                }
            };

            // merge the outer borders of another auto-style
            if (borderStyleUid) {
                ensureClonedAttrSet();
                const borderAttrSet = autoStyles.getMergedAttributeSet(borderStyleUid, true);
                for (const borderName of OUTER_BORDER_NAMES) {
                    mergedAttrSet.cell[borderName] = borderAttrSet.cell[borderName];
                }
            }

            // merge the passed table attributes over the resulting attributes
            // DOCS-4139: conditional formatting wins over table style
            const tableAttrSet = tableProps?.cellAttrSet;
            if (tableAttrSet) {
                // update attribute set
                ensureClonedAttrSet();
                autoStyles.extendWithTableAttributeSet(mergedAttrSet, mergedAttrSet, tableAttrSet);
                // update border priority (cell border always wins against table style border)
                updateBorderPrios(tableAttrSet, -1);
            }

            // merge the passed explicit attributes over the resulting attributes
            // DOCS-4139: conditional formatting wins over table style
            const condAttrSet = cfRulesResult?.cellAttrSet;
            if (condAttrSet) {
                // update attribute set
                ensureClonedAttrSet();
                autoStyles.extendAttrSet(mergedAttrSet, condAttrSet);
                // update border priority (conditional border always wins against adjacent cell border)
                updateBorderPrios(condAttrSet, 1);
                // detect whether the text color has been set explicitly
                explicitTextColor = condAttrSet.character?.color;
            }

            // if the text color has not been set explicitly by the passed attributes, it may be part of the auto-style itself
            if (!explicitTextColor) {
                const styleAttrSet = autoStyles.getExplicitAttributeSet(styleUid, true);
                explicitTextColor = styleAttrSet.character?.color;
            }

            // create a new render style instance
            return new RenderStyleModel(this.sheetModel, mergedAttrSet, borderPrios, !!explicitTextColor);
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Registers the cache key of a style model for a specific model UID.
     *
     * @param keyMapping
     *  A map containing arrays of cache keys, mapped by model UIDs.
     *
     * @param modelUid
     *  The UID of a model object to be inserted into the cache.
     *
     * @param cacheKey
     *  The cache key of the rendering style model to be registered.
     */
    #registerCacheKey(keyMapping: Map<string, string[]>, modelUid: string, cacheKey: string): void {
        map.upsert(keyMapping, modelUid, () => []).push(cacheKey);
    }

    /**
     * Deletes the style models for a specific model UID.
     *
     * @param keyMapping
     *  A map containing arrays of cache keys, mapped by model UIDs.
     *
     * @param modelUid
     *  The UID of a deleted model object to be removed from the cache.
     */
    #deleteStyleModels(keyMapping: Map<string, string[]>, modelUid: string): void {
        const cacheKeys = keyMapping.get(modelUid);
        if (cacheKeys) {
            const styleModels = this.#styleModels;
            for (const cacheKey of cacheKeys) {
                styleModels.delete(cacheKey);
            }
        }
    }

    /**
     * Connects this instance to the sheet model, after document import has
     * succeeded (registers all needed event handlers).
     */
    @renderLogger.profileMethod("$badge{RenderStyleCache} connectAfterImport")
    #connectAfterImport(): void {

        // invalidate the cache after inserting new fonts (CSS font family chains may change)
        this.listenToAllEvents(this.docModel.fontCollection, this.invalidateCache);

        // invalidate the cache when changing locale (preset number formats may change)
        this.listenTo(this.docModel.numberFormatter, "change:presets", this.invalidateCache);

        // remove all cached data of changed and deleted table stylesheets
        this.listenTo(this.docModel.tableStyles, ["change:stylesheet", "delete:stylesheet"], this.invalidateCache);

        // invalidate the cache when changing the sheet grid color
        this.listenToProp(this.sheetModel.propSet, "gridColor", this.invalidateCache);

        // remove all cached data of changed and deleted autostyles
        this.listenTo(this.docModel.autoStyles, ["change:autostyle", "delete:autostyle"], autoStyle => {
            this.#deleteStyleModels(this.#styleKeys, autoStyle.uid);
        });

        // remove all cached data of changed and deleted CF rule models
        this.listenTo(this.sheetModel.cfRuleCollection, ["change:cfrule", "delete:cfrule"], event => {
            this.#deleteStyleModels(this.#ruleKeys, event.ruleModel.uid);
        });

        // remove all cached data of changed table ranges (attributes only)
        this.listenTo(this.sheetModel.tableCollection, "change:table:attrs", event => {
            if (event.oldAttrSet.styleId !== event.newAttrSet.styleId) {
                this.#deleteStyleModels(this.#tableKeys, event.tableModel.uid);
            }
        });

        // remove all cached data of deleted table ranges
        this.listenTo(this.sheetModel.tableCollection, "delete:table:before", event => {
            this.#deleteStyleModels(this.#tableKeys, event.tableModel.uid);
        });
    }
}
