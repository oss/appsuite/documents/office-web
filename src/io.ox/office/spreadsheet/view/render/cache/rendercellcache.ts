/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, set, map } from "@/io.ox/office/tk/algorithms";
import { debounceMethod } from "@/io.ox/office/tk/objects";

import { debounceAfterActionsMethod } from "@/io.ox/office/editframework/app/decorators";

import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { type Address, addressKey } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";

import { type SheetModel, SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderStyleCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylecache";
import type { RenderHeaderCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadercache";
import type { MergedRangeData } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellmodel";
import { RenderCellModel } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellmodel";
import { type CellData, CellDataCollector } from "@/io.ox/office/spreadsheet/view/render/cache/celldatacollector";

// types ======================================================================

/**
 * Type mapping for the events emitted by `RenderCellCache` instances.
 */
export interface RenderCellCacheEventMap {

    /**
     * Will be emitted after creating or updating the pending dirty cell cache
     * entries asynchronously.
     */
    "cache:ready": [renderRanges: RangeArray];
}

/**
 * Optional parameters for method `RenderCellCache::yieldCellModels`.
 */
export interface YieldCellOptions {

    /**
     * If set to `true`, the cells in the passed range will be visited in
     * vertical orientation (all cells top-down in first column, then all cells
     * top-down in second column, etc.). By default, the cells will be visited
     * in horizontal orientation (all cells left-to-right in first row, then
     * all cells left-to-right in second row, etc.).
     */
    columns?: boolean;

    /**
     * If set to `true`, the origin cells of all merged ranges covering the
     * rendered cell range will be visited, regardless if they are inside the
     * passed range; and the cells in the passed range that are covered by
     * these merged ranges will not be visited. Default value is `false`.
     */
    origins?: boolean;

    /**
     * If set to `true`, the adjacent text cells located outside the rendering
     * boundaries will be visited too. Needed to render overflowing text
     * starting in an external cell, but floating into the rendering area.
     */
    overflow?: boolean;
}

// private types --------------------------------------------------------------

/**
 * Cache entry for a single cell containing multiple `RenderCellModel`
 * instances for different purposes.
 */
interface CellCacheEntry {
    renderModel: RenderCellModel;
    originModel?: RenderCellModel;
    textModelL?: RenderCellModel;
    textModelR?: RenderCellModel;
}

// constants ==================================================================

/**
 * Maximum number of entries in a cell cache.
 */
const MAX_CELL_CACHE_SIZE = 100_000;

/**
 * Maximum number of entries in the boundary history used to remember oldest
 * visited cell ranges.
 */
const MAX_HISTORY_SIZE = 100;

// class RenderCellCache ======================================================

/**
 * A cache with rendering information for all visible cells to be rendered in
 * the grid panes.
 */
export class RenderCellCache extends SheetChildModel<RenderCellCacheEventMap> {

    /** The cache for all rendering styles (pre-processed cell auto-styles). */
    readonly #styleCache: RenderStyleCache;
    /** The cache for visible column headers. */
    readonly #colCache: RenderHeaderCache;
    /** The cache for visible row headers. */
    readonly #rowCache: RenderHeaderCache;

    /** Descriptors for all merged ranges to be rendered. */
    readonly #mergedRangeMap = new Map<string, MergedRangeData>();
    /** Range set for lookup by address and boundary range. */
    readonly #mergedRangeSet = new RangeSet();

    /** Cache entries for all visible cells to be rendered. */
    readonly #cacheEntries = new Map<string, CellCacheEntry>();

    /** The model intervals of the column header cache. */
    #colIntervals = new IntervalArray();
    /** The boundary intervals of the column header cache. */
    #colBoundaries = new IntervalArray();

    /** The model intervals of the row header cache. */
    #rowIntervals = new IntervalArray();
    /** The boundary intervals of the row header cache. */
    #rowBoundaries = new IntervalArray();

    /** History of parts of the cache not inside `#paneBoundaries` anymore (ordered by age). */
    #boundaryHistory = new RangeArray();

    /** The unified boundary ranges covered by all grid panes. */
    #paneBoundaries = new RangeArray();
    /** Addresses of all cell ranges to generate cache entries for. */
    #dirtyRanges = new RangeArray();

    /** Whether this instance is currently locked (no updates after change events). */
    #locked = true;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, styleCache: RenderStyleCache, colCache: RenderHeaderCache, rowCache: RenderHeaderCache) {
        super(sheetModel);

        // private properties
        this.#styleCache = styleCache;
        this.#colCache = colCache;
        this.#rowCache = rowCache;

        // start listening to model events after document import is finished
        this.waitForImportSuccess(this.#connectAfterImport);

        // request to update all cell cache entries of dirty columns/rows
        const { addressFactory } = this.docModel;
        this.listenTo(colCache, "cache:dirty", intervals => this.#registerDirtyRanges(addressFactory.getColRangeList(intervals)));
        this.listenTo(rowCache, "cache:dirty", intervals => this.#registerDirtyRanges(addressFactory.getRowRangeList(intervals)));
    }

    // public methods ---------------------------------------------------------

    /**
     * Locks this instance, i.e. prevents updating the internal cache entries
     * after changes in the document or sheet model.
     */
    lockCache(): void {
        this.#locked = true;
    }

    /**
     * Unlocks this instance, i.e. resumes updating the internal cache entries
     * after changes in the document or sheet model.
     */
    unlockCache(): void {
        if (this.#locked) {
            this.#locked = false;
            this.#requestUpdateCellModels();
        }
    }

    /**
     * Returns whether this cache is currently busy updating its cell entries
     * asynchronously. A "cache:ready" event will be emitted when all cell
     * cache entries will be available.
     *
     * @returns
     *  Whether this cache is currently updating its cell entries.
     */
    isCacheBusy(): boolean {
        return !this.#locked && this.#dirtyRanges.overlaps(this.#paneBoundaries);
    }

    /**
     * Invalidates all cell cache entries.
     */
    invalidateCache(): void {
        this.#dirtyRanges.clear();
        this.#registerDirtyRanges(this.docModel.addressFactory.getSheetRange());
    }

    /**
     * Initializes the boundary ranges according to the current boundaries of
     * the column and row header caches.
     */
    @renderLogger.profileMethod("$badge{RenderCellCache} updatePaneBoundaries")
    updatePaneBoundaries(): void {

        // calculate boundary ranges from column/row boundaries
        const colBoundaries = this.#colCache.paneBoundaries;
        const rowBoundaries = this.#rowCache.paneBoundaries;
        const paneBoundaries = RangeArray.fromIntervals(colBoundaries, rowBoundaries);
        renderLogger.trace(() => `paneBoundaries: old=${this.#paneBoundaries} new=${paneBoundaries}`);
        const oldCount = this.#cacheEntries.size;

        // delete existing cell entries that are not covered by the new column boundaries anymore
        const removeColIntervals = this.#colIntervals.difference(this.#colCache.cacheBoundaries);
        if (removeColIntervals.length) {
            renderLogger.trace(() => `removeColIntervals=${removeColIntervals.toOpStr(true)}`);
            this.#removeCacheEntries(removeColIntervals, this.#rowIntervals);
            this.#colIntervals = this.#colIntervals.difference(removeColIntervals);
        }

        // delete existing cell entries that are not covered by the new row boundaries anymore
        const removeRowIntervals = this.#rowIntervals.difference(this.#rowCache.cacheBoundaries);
        if (removeRowIntervals.length) {
            renderLogger.trace(() => `removeRowIntervals=${removeRowIntervals.toOpStr(false)}`);
            this.#removeCacheEntries(this.#colIntervals, removeRowIntervals);
            this.#rowIntervals = this.#rowIntervals.difference(removeRowIntervals);
        }

        // register new parts of column cache boundaries for debounced creation of cell entries
        const insertColIntervals = this.#colCache.cacheBoundaries.difference(this.#colBoundaries);
        if (insertColIntervals.length) {
            renderLogger.trace(() => `insertColIntervals=${insertColIntervals.toOpStr(true)}`);
            this.#registerDirtyRanges(RangeArray.fromIntervals(insertColIntervals, this.#rowBoundaries));
        }
        this.#colBoundaries = this.#colCache.cacheBoundaries;

        // register new parts of row cache boundaries for debounced creation of cell entries
        const insertRowIntervals = this.#rowCache.cacheBoundaries.difference(this.#rowBoundaries);
        if (insertRowIntervals) {
            renderLogger.trace(() => `insertRowIntervals=${insertRowIntervals.toOpStr(false)}`);
            this.#registerDirtyRanges(RangeArray.fromIntervals(this.#colBoundaries, insertRowIntervals));
        }
        this.#rowBoundaries = this.#rowCache.cacheBoundaries;

        // update the boundary history, needed to keep the cache below a maximum size
        this.#boundaryHistory = this.#boundaryHistory.concat(this.#paneBoundaries).difference(paneBoundaries);

        // store the new pane boundaries
        this.#paneBoundaries = paneBoundaries;

        // debug logging
        const keepCount = this.#cacheEntries.size;
        renderLogger.trace(() => `${oldCount - keepCount} entries deleted, ${keepCount} entries kept in cache`);
    }

    /**
     * Collects the merged ranges in the current boundary ranges.
     */
    @renderLogger.profileMethod("$badge{RenderCellCache} updateMergedRanges")
    updateMergedRanges(): void {

        // clear all cached data (no partial cache update, full update is fast enough)
        this.#mergedRangeMap.clear();
        this.#mergedRangeSet.clear();

        // collect all merged ranges covering the boundary ranges
        for (const mergedRange of this.sheetModel.mergeCollection.yieldMergedRanges(this.#paneBoundaries)) {

            // shrink range to visible columns/rows in the sheet (ignore hidden merged ranges)
            const shrunkenRange = this.sheetModel.shrinkRangeToVisible(mergedRange);
            if (!shrunkenRange) { continue; }

            // create and insert the cache entries
            this.#mergedRangeMap.set(mergedRange.a1.key, { range: mergedRange, shrunken: shrunkenRange });
            this.#mergedRangeSet.add(mergedRange);
        }

        renderLogger.trace(() => `${this.#mergedRangeMap.size} merged ranges processed`);
    }

    /**
     * Returns the cell model at the specified position.
     *
     * @param col
     *  The column index of the cell.
     *
     * @param row
     *  The row index of the cell.
     *
     * @returns
     *  The rendering cell model if available.
     */
    getRenderCellModel(col: number, row: number): Opt<RenderCellModel> {
        return this.#cacheEntries.get(addressKey(col, row))?.renderModel;
    }

    /**
     * Returns an iterator that yields the descriptors for all visible merged
     * ranges that overlap with the specified cell range.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @yields
     *  The descriptors for all visible merged range overlapping with the
     *  passed cell range.
     */
    *yieldMergedRangeData(range: Range): IterableIterator<MergedRangeData> {
        for (const mergedRange of this.#mergedRangeSet.yieldMatching(range)) {
            yield this.#mergedRangeMap.get(mergedRange.a1.key)!; // exists always (due to using `this.#mergedRangeSet`)
        }
    }

    /**
     * Returns an iterator that yields all rendering cell models contained in
     * the specified cell range.
     *
     * @param range
     *  The cell range to be rendered.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The rendering cell model contained in the passed cell range.
     */
    *yieldCellModels(range: Range, options?: YieldCellOptions): IterableIterator<RenderCellModel> {

        // the indexes of the boundary columns, needed for overflowing text support
        const colsL = set.from(this.#colCache.paneBoundaries, interval => interval.first);
        const colsR = set.from(this.#colCache.paneBoundaries, interval => interval.last);

        // helper generator that yields the cell model, and its adjacent text cell models if available
        function *yieldModels(cacheEntry: CellCacheEntry): IterableIterator<RenderCellModel> {
            // always yield the cell model itself
            yield cacheEntry.renderModel;
            // yield adjacent text cell models that may overflow into the cell
            if (options?.overflow) {
                if (cacheEntry.textModelL && colsL.has(cacheEntry.renderModel.address.c)) {
                    yield cacheEntry.textModelL;
                }
                if (cacheEntry.textModelR && colsR.has(cacheEntry.renderModel.address.c)) {
                    yield cacheEntry.textModelR;
                }
            }
        }

        // visit all cells in the specified range, also cells covered by merged ranges
        if (!options?.origins) {
            for (const addrKey of this.#yieldAddressKeys(range, options?.columns)) {
                yield* yieldModels(this.#cacheEntries.get(addrKey)!); // exists always
            }
            return;
        }

        // all yielded origin cells (do not yield them multiple times for every covered cell)
        const yieldedKeys = new Set<string>();

        // visit all cells with special handling for merged ranges
        for (const addrKey of this.#yieldAddressKeys(range, options?.columns)) {
            const cacheEntry = this.#cacheEntries.get(addrKey)!; // exists always
            if (!cacheEntry.originModel) {
                // visit regular cells that are not part of a merged range
                yield* yieldModels(cacheEntry);
            } else if (!yieldedKeys.has(cacheEntry.originModel.address.key)) {
                // visit the origin of the merged range _once_
                yield cacheEntry.originModel;
                yieldedKeys.add(cacheEntry.originModel.address.key);
            }
        }
    }

    /**
     * Annotates a DOM element with data attributes about the rendering styles
     * of a cell. The data attributes will be used by automated tests.
     *
     * @param elem
     *  The DOM element to be annotated.
     *
     * @param address
     *  The address of a cell.
     */
    annotateElement(elem: HTMLElement, address: Address): void {
        this.getRenderCellModel(address.c, address.r)?.annotateElement(this.sheetModel, elem);
    }

    // private methods --------------------------------------------------------

    /**
     * Creates an iterator that yields the cell address keys (intended to be
     * used as map keys) of all visible cells in the specified cell ranges.
     *
     * @param ranges
     *  One or more cell ranges to ve iterated.
     *
     * @param [columns]
     *  Whether to yield the cells row-by-row (`false`, default value), or
     *  column-by-column (`true`).
     *
     * @yields
     *  The cell address keys of all visible cells in the cell ranges.
     */
    *#yieldAddressKeys(ranges: OrArray<Range>, columns?: boolean): IterableIterator<string> {
        for (const range of this.#paneBoundaries.intersect(ranges)) {
            if (columns) {
                const rowHeaders = Array.from(this.#rowCache.yieldHeaderModels(range.rowInterval()));
                for (const colHeader of this.#colCache.yieldHeaderModels(range.colInterval())) {
                    for (const rowHeader of rowHeaders) {
                        yield addressKey(colHeader.index, rowHeader.index);
                    }
                }
            } else {
                const colHeaders = Array.from(this.#colCache.yieldHeaderModels(range.colInterval()));
                for (const rowHeader of this.#rowCache.yieldHeaderModels(range.rowInterval())) {
                    for (const colHeader of colHeaders) {
                        yield addressKey(colHeader.index, rowHeader.index);
                    }
                }
            }
        }
    }

    /**
     * Removes all cell cache entries in the specified column/row intervals.
     */
    #removeCacheEntries(colIntervals: Interval | IntervalArray, rowIntervals: Interval | IntervalArray): void {
        for (const col of colIntervals.indexes()) {
            for (const row of rowIntervals.indexes()) {
                this.#cacheEntries.delete(addressKey(col, row));
            }
        }
    }

    /**
     * Registers the specified cell ranges for rendering.
     *
     * @param rangesArgs
     *  The address of one or more cell ranges to be rerendered.
     */
    #registerDirtyRanges(...rangesArgs: Array<Range | Iterable<Range>>): void {

        // register the changed cell ranges
        for (const ranges of rangesArgs) {
            if (ranges instanceof Range) {
                this.#dirtyRanges.push(ranges);
            } else {
                this.#dirtyRanges.appendIterable(ranges);
            }
        }

        // refresh all cache entries asynchronously
        this.#requestUpdateCellModels();
    }

    /**
     * Requests debounced initialization of all dirty and missing cell cache
     * entries for the current boundary ranges.
     */
    @debounceMethod({ delay: "microtask" })
    #requestUpdateCellModels(): void {

        // try to merge the array if it becomes too large
        if (this.#dirtyRanges.length > 20) {
            this.#dirtyRanges = this.#dirtyRanges.merge();
        }

        // switch to boundary if array is still too large
        if (this.#dirtyRanges.length > 20) {
            this.#dirtyRanges.assign(this.#dirtyRanges.boundary());
        }

        renderLogger.trace(() => `$badge{RenderCellCache} requestUpdateCellModels: ${this.#dirtyRanges}`);

        // debounced update of the cell models
        this.#execUpdateCellModels();
    }

    /**
     * Updates all dirty and missing cell cache entries for the current
     * boundary ranges.
     */
    @debounceAfterActionsMethod({ delay: 25 })
    @renderLogger.profileMethod("$badge{RenderCellCache} execUpdateCellModels")
    #execUpdateCellModels(): void {

        // no updates while this instance is locked
        if (this.#locked) { return; }

        // all pending cell ranges to generate cell cache entries for
        const dirtyRanges = this.#dirtyRanges.intersect(this.#paneBoundaries).merge();
        this.#dirtyRanges = this.#dirtyRanges.difference(this.#paneBoundaries);
        renderLogger.trace(() => `dirtyRanges=${dirtyRanges}`);

        // remove dirty ranges from boundary history (will be deleted anyways)
        this.#boundaryHistory = this.#boundaryHistory.difference(dirtyRanges);

        // if cache exceeds the limit, shrink it by removing oldest visited boundaries
        const oldCount = this.#cacheEntries.size;
        const history = this.#boundaryHistory;
        if ((oldCount > MAX_CELL_CACHE_SIZE) || (history.length > MAX_HISTORY_SIZE)) {
            renderLogger.trace(() => `shrinking oversized cache (${oldCount} entries)`);
            let removeCount = oldCount - Math.floor(MAX_CELL_CACHE_SIZE / 2);
            for (const range of ary.shiftValues(history)) {
                // immediately delete all cell entries in the range
                this.#removeCacheEntries(range.colInterval(), range.rowInterval());
                removeCount -= range.cells();
                if ((removeCount <= 0) && (history.length <= MAX_HISTORY_SIZE)) { break; }
            }
        }

        // column/row intervals of potentially existing cell cache entries
        this.#colIntervals = this.#colCache.cacheIntervals;
        this.#rowIntervals = this.#rowCache.cacheIntervals;

        // immediately delete all cell entries in the dirty ranges (e.g. from column/row cache invalidation outside the bane boundaries)
        for (const dirtyRange of dirtyRanges) {
            const colIntervals = this.#colIntervals.intersect(dirtyRange.colInterval());
            const rowIntervals = this.#rowIntervals.intersect(dirtyRange.rowInterval());
            this.#removeCacheEntries(colIntervals, rowIntervals);
        }

        // collect cell style information from different sources (cell models, conditional formatting,
        // table ranges), before merging and resolving the rendering styles from the style cache
        const collector = new CellDataCollector(this.sheetModel, this.#colCache, this.#rowCache, this.#mergedRangeMap, this.#paneBoundaries);
        const renderRanges = collector.collectCellData(dirtyRanges);

        // finally, create the real cell cache entries from the collected cell data
        this.#createCacheEntries(collector);

        // notify renderers waiting for cell data
        renderLogger.trace(() => `renderRanges=${renderRanges}`);
        this.trigger("cache:ready", renderRanges);
    }

    /**
     * Creates the real cell cache entries from the collected data.
     */
    @renderLogger.profileMethod("create cache entries from collected cell data")
    #createCacheEntries(collector: CellDataCollector): void {

        // helper function that create a new `RenderCellModel` instance
        const createCellModel = (cellData: CellData): RenderCellModel => new RenderCellModel(this.sheetModel, this.#styleCache, cellData);

        // cell models of merged range origins already created
        const originModels = new Map<CellData, RenderCellModel>();

        // create the cache entries of all collected cells
        let createCount = 0, updateCount = 0;
        for (const [addrKey, cellData, originData, textDataL, textDataR] of collector.yieldCells()) {
            const cacheEntry = cellData ? map.toggle(this.#cacheEntries, addrKey, { renderModel: createCellModel(cellData) }) : this.#cacheEntries.get(addrKey)!;
            cacheEntry.originModel = originData && map.upsert(originModels, originData, () => createCellModel(originData));
            cacheEntry.textModelL = textDataL && createCellModel(textDataL);
            cacheEntry.textModelR = textDataR && createCellModel(textDataR);
            if (cellData) { createCount += 1; } else { updateCount += 1; }
        }

        renderLogger.trace(() => `${createCount} cell entries created, ${updateCount} cell entries updated`);
    }

    /**
     * Connects this instance to the sheet model, after document import has
     * succeeded (registers all needed event handlers).
     */
    @renderLogger.profileMethod("$badge{RenderCellCache} connectAfterImport")
    #connectAfterImport(): void {

        // repaint all cells when the UI language has changed (number formatting)
        this.listenTo(this.docModel, "change:locale", () => {
            renderLogger.trace("$badge{RenderCellCache} $event{change:locale}");
            this.invalidateCache();
        });

        // grid color used by cell borders with automatic line color
        this.listenToProp(this.sheetModel.propSet, "gridColor", () => {
            renderLogger.trace("$badge{RenderCellCache} $event{change:gridColor}");
            this.invalidateCache();
        });

        // update all cell ranges that have been invalidated in the sheet
        this.listenTo(this.sheetModel, "refresh:ranges", event => {
            renderLogger.trace("$badge{RenderCellCache} $event{refresh:ranges}");
            this.#registerDirtyRanges(event.ranges);
        });

        // update all cells that have been changed in the sheet
        this.listenTo(this.sheetModel, "change:cells", event => {
            renderLogger.trace("$badge{RenderCellCache} $event{change:cells}");
            // the changed cell ranges
            const cellRanges = RangeArray.mergeAddresses(event.allCells);
            // all merged ranges starting at the changed cells
            const mergedRanges = new Set<Range>();
            for (const address of event.allCells) {
                const mergedData = this.#mergedRangeMap.get(address.key);
                if (mergedData) { mergedRanges.add(mergedData.shrunken); }
            }
            // register all ranges at the cell cache
            this.#registerDirtyRanges(cellRanges, mergedRanges);
        });

        // update the cached merged ranges after new merged ranges have been inserted into
        // the sheet, or after existing merged ranges have been deleted from the sheet
        this.listenTo(this.sheetModel, "merge:cells", event => {
            renderLogger.trace("$badge{RenderCellCache} $event{merge:cells}");
            this.updateMergedRanges();
            this.#registerDirtyRanges(event.insertRanges, event.deleteRanges);
        });

        // render cells of inserted and deleted table ranges (table styling)
        for (const eventType of ["insert:table", "delete:table"] as const) {
            this.listenTo(this.sheetModel, eventType, event => {
                renderLogger.trace(() => `$badge{RenderCellCache} $event{${eventType}}`);
                this.#registerDirtyRanges(event.tableModel.getRange());
            });
        }

        // render table ranges with changed attributes (table stylesheet, style flags, etc.)
        this.listenTo(this.sheetModel, "change:table:attrs", event => {
            renderLogger.trace("$badge{RenderCellCache} $event{change:table:attrs}");
            this.#registerDirtyRanges(event.tableModel.getRange());
        });

        // bug 52483: refresh all table ranges using a changed table style sheet
        this.listenToAllEvents(this.docModel.tableStyles, (type, styleId) => {
            renderLogger.trace(() => `$badge{RenderCellCache} $event{${type}}`);
            for (const tableModel of this.sheetModel.tableCollection.yieldTableModels()) {
                if (tableModel.getStyleId() === styleId) {
                    this.#registerDirtyRanges(tableModel.getRange());
                }
            }
        });
    }
}
