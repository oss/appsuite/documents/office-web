/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { IntervalPosition } from "@/io.ox/office/tk/dom";

import { getCanvasCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { RenderStyleModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylemodel";

// types ======================================================================

/**
 * The pixel position (start offset, end offset, and size) of a header model.
 */
export interface HeaderPosition extends IntervalPosition {
    offset2: number;
}

// class RenderHeaderModel ====================================================

/**
 * An model for a visible column or row that is part of a `RenderHeaderCache`
 * instance.
 */
export class RenderHeaderModel {

    /**
     * Preceding header model (doubly linked list).
     */
    prev: Opt<RenderHeaderModel>;

    /**
     * Following header model (doubly linked list).
     */
    next: Opt<RenderHeaderModel>;

    /**
     * The zero-based column/row index.
     */
    readonly index: number;

    /**
     * The CSS pixel position of the column/row.
     */
    readonly cssPos: HeaderPosition;

    /**
     * The canvas pixel position of the column/row.
     */
    readonly renderPos: HeaderPosition;

    /**
     * A wrapper for the cell autostyle in the document used to format the
     * undefined cells of the column or row. If `undefined`, nothing will be
     * rendered.
     */
    readonly renderStyle: Opt<RenderStyleModel>;

    // constructor ------------------------------------------------------------

    constructor(index: number, offset: number, size: number, renderStyle: Opt<RenderStyleModel>) {
        this.index = index;
        this.cssPos = { offset, offset2: offset + size, size };
        const offset1 = getCanvasCoord(offset);
        const offset2 = getCanvasCoord(offset + size);
        this.renderPos = { offset: offset1, offset2, size: offset2 - offset1 };
        this.renderStyle = renderStyle;
    }
}
