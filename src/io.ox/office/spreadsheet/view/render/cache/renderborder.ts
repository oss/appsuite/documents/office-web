/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { getDeviceResolution, convertHmmToLength } from "@/io.ox/office/tk/dom";
import type { CanvasLineStyle } from "@/io.ox/office/tk/canvas";

import { Color } from "@/io.ox/office/editframework/utils/color";
import { type OpBorder, isVisibleBorder, getBorderPattern } from "@/io.ox/office/editframework/utils/border";

import { MAX_BORDER_WIDTH } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { getCanvasCoord } from "@/io.ox/office/spreadsheet/view/render/renderutils";

// constants ==================================================================

// border line styles, mapped to rendering priority (less numbers are higher priority)
const BORDER_STYLE_PRIOS: Dict<number> = { solid: 1, dashed: 2, dotted: 3, dashDot: 4, dashDotDot: 5 };

// class RenderBorder =========================================================

/**
 * Preprocessed rendering settings for a single cell border line.
 */
export class RenderBorder {

    // static functions -------------------------------------------------------

    /**
     * Creates a new instance, if the passed JSON border describes a visible
     * border line, otherwise returns `undefined`.
     *
     * @param sheetModel
     *  The sheet model used to resolve scheme colors, and the grid color for
     *  automatic cell borders.
     *
     * @param border
     *  The JSON border object, with additional optional priority field.
     *
     * @param [priority]
     *  The rendering priority of the border line. Border with higher priority
     *  are always stronger than borders with lower priority, regardless of
     *  their styles (line width etc.). Used when comparing overlapping borders
     *  of adjacent cells to decide which border will be rendered.
     *
     * @returns
     *  A new instance, if the passed JSON border describes a visible border
     *  line, otherwise `undefined`.
     */
    static create(sheetModel: SheetModel, border: OpBorder, priority = 0): Opt<RenderBorder> {
        return isVisibleBorder(border) ? new RenderBorder(sheetModel, border, priority) : undefined;
    }

    /**
     * Returns the border with stronger visual appearance.
     *
     * @param border1
     *  The first border style, or nullish for invisible borders.
     *
     * @param border2
     *  The second border style, or nullish for invisible borders.
     *
     * @returns
     *  One of the passed borders with stronger visual appearance. If both
     *  borders are missing (invisible), `undefined` will be returned. If one
     *  of the passed borders is missing (invisible), the other border will be
     *  returned. If both borders are visible, one of them will be selected by
     *  checking the following conditions (in the specified order):
     *  1. Compare border priority: Higher priority wins.
     *  2. Compare border width: Thicker border wins over thinner border.
     *  3. Compare line style: Double over solid, over dashed, over dotted,
     *     over dash-dot, over dash-dot-dot.
     *  4. Compare line color by luma: Darker color wins over lighter color.
     */
    static getStrongerBorder(border1: Nullable<RenderBorder>, border2: Nullable<RenderBorder>): Opt<RenderBorder> {

        // handle missing borders (return the other border)
        if (!border1) { return border2 ?? undefined; }
        if (!border2) { return border1; }

        // compare rendering priority (higher number wins)
        if (border1.prio > border2.prio) { return border1; }
        if (border1.prio < border2.prio) { return border2; }

        // compare border width (hair lines first)
        if (border1.hair && !border2.hair) { return border2; }
        if (!border1.hair && border2.hair) { return border1; }

        // compare pixel border width
        if (border1.width > border2.width) { return border1; }
        if (border1.width < border2.width) { return border2; }

        // compare border style (double border wins first)
        if (border1.double && !border2.double) { return border1; }
        if (!border1.double && border2.double) { return border2; }

        // compare remaining border styles by priority
        if (border1.style !== border2.style) {
            const prio1 = BORDER_STYLE_PRIOS[border1.style] || 9999;
            const prio2 = BORDER_STYLE_PRIOS[border2.style] || 9999;
            return (prio1 < prio2) ? border1 : border2;
        }

        // compare luma value (darker color wins!)
        return (border1.y < border2.y) ? border1 : border2;
    }

    // properties -------------------------------------------------------------

    /**
     * Rendering priority of the border line. Border with higher priority are
     * always stronger than borders with lower priority, regardless of their
     * styles (line width etc.). Used when comparing overlapping borders of
     * adjacent cells to decide which border will be rendered.
     */
    readonly prio: number;

    /**
     * Effective dash style of the border line: one of "solid", "dotted",
     * "dashed", "dashDot", or "dashDotDot".
     */
    readonly style: string;

    /**
     * The effective width of the border, in canvas pixels (positive integer).
     * In case of double lines, this value will not be less than `3`, and
     * specifies the total width of the entire border. In case of hair lines,
     * this value will be `1`.
     */
    readonly width: number;

    /**
     * The effective CSS line color.
     */
    readonly color: string;

    /**
     * The luma value (weighted lightness) of the border line color, as
     * positive floating-point number. The maximum value `1` represents the
     * color white.
     */
    readonly y: number;

    /**
     * The canvas rendering style.
     */
    readonly canvasStyle: CanvasLineStyle;

    /**
     * Whether the border lines will be rendered as solid lines (`true`), or
     * with a dashed line style (`false`).
     */
    readonly solid: boolean;

    /**
     * Whether the border consists of a hair line (original width is less than
     * one CSS pixel).
     */
    readonly hair: boolean;

    /**
     * Whether the border consists of two parallel lines.
     */
    readonly double: boolean;

    // constructor ------------------------------------------------------------

    private constructor(sheetModel: SheetModel, border: OpBorder, priority: number) {

        // rendering priority of the border
        this.prio = priority;

        // initial line width, in CSS pixels (zero is hair line), restricted to maximum border size
        // to prevent that left/right or top/bottom border lines of a cell overlay each other
        const cssWidth = Math.min(convertHmmToLength(border.width ?? 0, "px", 1), MAX_BORDER_WIDTH);
        // flag for double lines
        const double = border.style === "double";
        // flag for hair lines (initial line width is zero)
        const hair = !double && (cssWidth === 0);

        // effective line style (solid, dashed, etc.)
        this.style = double ? "solid" : border.style;

        // effective minimum width in canvas pixels (at least one pixel, or 3 pixels for double lines: two lines, one pixel gap)
        this.width = Math.max(getCanvasCoord(cssWidth), double ? 3 : 1);

        // resolve all color details (use semi-transparency for hair lines, but not on retina displays)
        const color = Color.parseJSON(border.color);
        if (hair && (getDeviceResolution() < 2)) { color.transform("alphaMod", 40000); }
        const gridColor = sheetModel.getGridColor();
        const colorDesc = sheetModel.docModel.resolveColor(color, gridColor.isAuto() ? "line" : gridColor);

        // effective CSS line color
        this.color = colorDesc.css;

        // luma of the line color
        this.y = colorDesc.y;

        // the resulting canvas rendering style
        const lineWidth = double ? Math.max(1, Math.round(this.width / 3)) : this.width;
        const pattern = getBorderPattern(this.style, lineWidth);
        this.canvasStyle = { style: colorDesc.css, width: lineWidth, pattern };

        // public boolean flags
        this.solid = !pattern;
        this.double = double;
        this.hair = hair;
    }
}
