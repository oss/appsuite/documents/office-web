/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, itr, map } from "@/io.ox/office/tk/algorithms";

import { addressKey, Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";

import type { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import type { CFRulesFormatResult } from "@/io.ox/office/spreadsheet/model/cfrulecollection";
import type { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

import { renderLogger } from "@/io.ox/office/spreadsheet/view/render/renderutils";
import type { TableRenderProps } from "@/io.ox/office/spreadsheet/view/render/cache/renderstylecache";
import type { RenderHeaderModel } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadermodel";
import type { RenderHeaderCache } from "@/io.ox/office/spreadsheet/view/render/cache/renderheadercache";
import type { MergedRangeData } from "@/io.ox/office/spreadsheet/view/render/cache/rendercellmodel";

// types ======================================================================

/**
 * Iterator value type for `CellDataCollector::#yieldHeaderModels`.
 */
type RenderHeaderModels = [addressKey: string, colHeader: RenderHeaderModel, rowHeader: RenderHeaderModel];

// class CellData =============================================================

/**
 * Intermediate data storage for a single cell to be rendered.
 */
export class CellData {

    readonly colHeader: RenderHeaderModel;
    readonly rowHeader: RenderHeaderModel;
    readonly address: Address;
    readonly origin: Address;
    readonly cellModel: CellModel | null;
    readonly mergedData: Opt<MergedRangeData>;
    readonly baseSuid: Opt<string>;
    readonly borderSuid: Opt<string>;

    cfRulesResult?: CFRulesFormatResult;
    tableProps?: TableRenderProps;

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, colHeader: RenderHeaderModel, rowHeader: RenderHeaderModel, mergedData?: MergedRangeData) {
        const collection = sheetModel.cellCollection;

        this.colHeader = colHeader;
        this.rowHeader = rowHeader;
        this.address = new Address(colHeader.index, rowHeader.index);
        this.origin = mergedData?.range.a1 ?? this.address;
        this.mergedData = mergedData;
        this.cellModel = collection.getCellModel(this.origin);

        // the identifier of the initial auto-style (bug 40429: always set a style descriptor to (blank) merged ranges)
        this.baseSuid = this.cellModel?.suid ?? (mergedData ? collection.getDefaultStyleId(this.origin, { useUid: true }) : undefined);

        // OOX: use the border style of the current cell in merged ranges (every cell stores its own border attributes)
        // ODF: use the border style of the origin cell for all cells of a merged range (distribute borders to merged range)
        const borderSuid = (mergedData && sheetModel.docApp.isOOXML()) ? collection.getCellModel(this.address)?.suid : undefined;
        this.borderSuid = (this.baseSuid !== borderSuid) ? borderSuid : undefined;
    }
}

// class CellDataCollector ====================================================

/**
 * Intermediate storage that collects style information of visible cell from
 * different sources (cell model, conditional formatting, table ranges), before
 * merging and resolving the effective rendering styles from the style cache.
 */
export class CellDataCollector {

    readonly #sheetModel: SheetModel;
    readonly #colCache: RenderHeaderCache;
    readonly #rowCache: RenderHeaderCache;
    readonly #mergedRanges: Map<string, MergedRangeData>;
    readonly #paneBoundaries: RangeArray;

    readonly #cellStore = new Map<string, CellData>();
    readonly #originStore = new Map<string, CellData>();
    readonly #textStoreL = new Map<string, CellData>();
    readonly #textStoreR = new Map<string, CellData>();

    // constructor ------------------------------------------------------------

    constructor(sheetModel: SheetModel, colCache: RenderHeaderCache, rowCache: RenderHeaderCache, mergedRanges: Map<string, MergedRangeData>, paneBoundaries: RangeArray) {
        this.#sheetModel = sheetModel;
        this.#colCache = colCache;
        this.#rowCache = rowCache;
        this.#mergedRanges = mergedRanges;
        this.#paneBoundaries = paneBoundaries;
    }

    // public methods ---------------------------------------------------------

    @renderLogger.profileMethod("$badge{CellDataCollector} collectCellData")
    collectCellData(dirtyRanges: RangeArray): RangeArray {

        this.#cellStore.clear();
        this.#originStore.clear();
        this.#textStoreL.clear();
        this.#textStoreR.clear();

        // intervals of all rows covered by the original pending ranges
        const rowIntervals = this.#rowCache.cacheIntervals.intersect(dirtyRanges.rowIntervals().merge());
        if (!rowIntervals.length) { return new RangeArray(); }

        // the rendering ranges to be refreshed (parts of the pending ranges overlapping with the boundaries)
        let renderRanges = dirtyRanges.intersect(this.#paneBoundaries).merge();

        // collect all cells in the passed rendering ranges covered by merged ranges
        const originAddresses = renderRanges.length ? this.#collectMergedRangeCells(renderRanges) : [];

        // collect all remaining cells in the passed rendering ranges
        if (renderRanges.length) {
            this.#collectRegularCells(renderRanges);
        }

        // collect adjacent text cells outside of the bounding ranges that may flow into the rendering area
        this.#collectAdjacentTextCells(rowIntervals);

        // add addresses of merged range origins to `renderRanges` for further processing (conditional formatting etc.)
        if (originAddresses?.length) {
            renderRanges = renderRanges.concat(originAddresses.map(address => new Range(address))).merge();
        }

        // Resolve all conditional formatting rules. For performance, rules will not be resolved for
        // every rendered cell, but the cell ranges covered by rule models will be determined first.
        if (renderRanges.length) {
            this.#collectCFRuleResults(renderRanges);
        }

        // Resolve all formatting attributes in table ranges. For performance, attributes will not be
        // resolved for every rendered cell, but the existing table ranges will be iterated instead.
        if (renderRanges.length) {
            this.#collectTableFormats(renderRanges);
        }

        // return the updated rendering ranges (including origins of merged ranges)
        return renderRanges;
    }

    *yieldCells(): IterableIterator<[addrKey: string, cellData: Opt<CellData>, originData: Opt<CellData>, textDataL: Opt<CellData>, textDataR: Opt<CellData>]> {

        // local copy of text data stores for dynamic reduction during iteration
        const textStoreL = new Map(this.#textStoreL);
        const textStoreR = new Map(this.#textStoreR);

        // yield existing cell data entries
        for (const [addrKey, cellData] of this.#cellStore) {
            const originData = this.#originStore.get(addrKey);
            const textDataL = map.remove(textStoreL, addrKey);
            const textDataR = map.remove(textStoreR, addrKey);
            yield [addrKey, cellData, originData, textDataL, textDataR];
        }

        // yield additional entries for external overflowing text for unprocessed left boundary cells
        for (const [addrKey, cellData] of textStoreL) {
            yield [addrKey, undefined, undefined, cellData, undefined];
        }

        // yield additional entries for external overflowing text for unprocessed right boundary cells
        for (const [addrKey, cellData] of textStoreR) {
            yield [addrKey, undefined, undefined, undefined, cellData];
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Creates an iterator that yields the cell address keys (intended to be
     * used as map keys) of all visible cells in the specified cell ranges.
     */
    *#yieldAddressKeys(ranges: RangeArray): IterableIterator<string> {
        for (const range of ranges) {
            const colHeaders = Array.from(this.#colCache.yieldHeaderModels(range.colInterval()));
            for (const rowHeader of this.#rowCache.yieldHeaderModels(range.rowInterval())) {
                for (const colHeader of colHeaders) {
                    yield addressKey(colHeader.index, rowHeader.index);
                }
            }
        }
    }

    /**
     * Creates an iterator that yields the cell address keys, and column and
     * row headers of all visible cells in the specified cell range.
     */
    *#yieldHeaderModels(ranges: RangeArray): IterableIterator<RenderHeaderModels> {
        for (const range of ranges) {
            const colHeaders = Array.from(this.#colCache.yieldHeaderModels(range.colInterval()));
            for (const rowHeader of this.#rowCache.yieldHeaderModels(range.rowInterval())) {
                for (const colHeader of colHeaders) {
                    yield [addressKey(colHeader.index, rowHeader.index), colHeader, rowHeader];
                }
            }
        }
    }

    #createTempCellData(address: Address, mergedData?: MergedRangeData): CellData {
        const colHeader = this.#colCache.createIntermediateModel(address.c);
        const rowHeader = this.#rowCache.createIntermediateModel(address.r);
        return new CellData(this.#sheetModel, colHeader, rowHeader, mergedData);
    }

    /**
     * Creates a `CellData` object for an adjacent text cell outside the
     * boundaries.
     */
    #createAdjacentTextCellData(cellStore: Map<string, CellData>, col: number, row: number, dir: Direction): void {

        // shortcuts to members
        const { colCollection, cellCollection, mergeCollection } = this.#sheetModel;

        // find next adjacent text cell in cell collection (for blank start cells only)
        const address = new Address(col, row);
        const cellInfo = itr.shift(cellCollection.linearCellEntries(address, dir, { type: "value", visible: true }));
        if (!cellInfo || (col === cellInfo.address.c) || !is.string(cellInfo.value)) { return; }

        // no visible merged ranges must be located between the cells
        const iter = mergeCollection.yieldMergedRanges(Range.fromAddresses(address, cellInfo.address));
        if (itr.some(iter, mergedRange => !colCollection.isIntervalHidden(mergedRange.colInterval()))) { return; }

        // create an intermediate cell data object for the external text cell
        cellStore.set(address.key, this.#createTempCellData(cellInfo.address));
    }

    @renderLogger.profileMethod("collect cells in merged ranges")
    #collectMergedRangeCells(renderRanges: RangeArray): Address[] {

        // shortcuts to members
        const sheetModel = this.#sheetModel;
        const cellStore = this.#cellStore;
        const originStore = this.#originStore;

        // origin addresses of all processed merged ranges
        const originAddresses = new AddressArray();

        // process all known merged ranges
        for (const mergedData of this.#mergedRanges.values()) {

            // the parts of the merged range covered by the rendering ranges
            const shrinkRanges = renderRanges.intersect(mergedData.shrunken);
            if (shrinkRanges.empty()) { continue; }

            // create an entry for the origin (also if it is outside the rendering ranges)
            const originAddr = mergedData.shrunken.a1;
            const originKey = originAddr.key;
            const originData = this.#createTempCellData(originAddr, mergedData);
            cellStore.set(originKey, originData);
            originStore.set(originKey, originData);
            originAddresses.push(originAddr);

            // create entries for all covered cells (origin may or may not be part of `shrinkRanges`)
            for (const [addrKey, colHeader, rowHeader] of this.#yieldHeaderModels(shrinkRanges)) {
                map.upsert(cellStore, addrKey, () => new CellData(sheetModel, colHeader, rowHeader, mergedData));
                originStore.set(addrKey, originData);
            }
        }

        return originAddresses;
    }

    @renderLogger.profileMethod("collect remaining regular cells")
    #collectRegularCells(renderRanges: RangeArray): void {

        // shortcuts to members
        const sheetModel = this.#sheetModel;
        const cellStore = this.#cellStore;

        // create all missing cell data objects
        for (const [addrKey, colHeader, rowHeader] of this.#yieldHeaderModels(renderRanges)) {
            map.upsert(cellStore, addrKey, () => new CellData(sheetModel, colHeader, rowHeader));
        }
    }

    @renderLogger.profileMethod("collect external text cells for column boundaries")
    #collectAdjacentTextCells(renderRows: IntervalArray): void {
        for (const { first, last } of this.#colCache.paneBoundaries) {
            for (const row of renderRows.indexes()) {
                this.#createAdjacentTextCellData(this.#textStoreL, first, row, Direction.LEFT);
                this.#createAdjacentTextCellData(this.#textStoreR, last, row, Direction.RIGHT);
            }
        }
    }

    /**
     * Resolves rendering settings for conditional formatting rules.
     */
    @renderLogger.profileMethod("collect conditional formatting results")
    #collectCFRuleResults(renderRanges: RangeArray): void {

        // shortcuts to members
        const collection = this.#sheetModel.cfRuleCollection;
        const cellStore = this.#cellStore;

        // find all cell ranges with conditional formatting in the rendering ranges
        const bucketRanges = Array.from(collection.yieldRuleBuckets(), ruleBucket => ruleBucket.ranges).flat();
        const targetRanges = RangeArray.mergeRanges(bucketRanges).intersect(renderRanges);

        // memoized version of `CFRuleCollection::resolveRules` for merged ranges (multiple calls with same origin)
        const resolveCfResult = fun.memoize(collection.resolveRules.bind(collection), address => address.key);

        // add conditional formatting data to the existing `CellData` objects
        let ruleCells = 0;
        for (const addrKey of this.#yieldAddressKeys(targetRanges)) {
            const cellData = cellStore.get(addrKey)!;
            cellData.cfRulesResult = resolveCfResult(cellData.origin); // always from origin of a merged range
            ruleCells += 1;
        }

        renderLogger.trace(() => `targetRanges=${targetRanges.slice(0, 5)}... (${targetRanges.length} ranges, ${ruleCells} cells processed)`);
    }

    /**
     * Resolves rendering settings in table ranges.
     */
    @renderLogger.profileMethod("collect attributes of table cells")
    #collectTableFormats(renderRanges: RangeArray): void {

        // shortcuts to members
        const { tableStyles } = this.#sheetModel.docModel;
        const cellStore = this.#cellStore;

        let tableCells = 0;
        for (const tableModel of this.#sheetModel.tableCollection.yieldTableModels()) {

            // the parts of the table range overlapping with the rendering ranges
            const tableRanges = renderRanges.intersect(tableModel.getRange());
            if (tableRanges.empty()) { continue; }

            // resolve the raw attribute set of the table stylesheet
            const tableStyleId = tableModel.getStyleId();
            if (!tableStyleId) { continue; }
            const rawStyleAttrSet = tableStyles.getStyleSheetAttributeMap(tableStyleId, true);
            if (is.empty(rawStyleAttrSet)) { continue; }

            // add table formatting data to the existing `CellData` objects
            for (const addrKey of this.#yieldAddressKeys(tableRanges)) {
                const cellData = cellStore.get(addrKey)!;
                const result = tableModel.resolveCellAttributeSet(cellData.address, rawStyleAttrSet);
                if (result) { cellData.tableProps = { tableModel, ...result }; }
                tableCells += 1;
            }
        }

        renderLogger.trace(() => `${tableCells} table cells processed`);
    }
}
