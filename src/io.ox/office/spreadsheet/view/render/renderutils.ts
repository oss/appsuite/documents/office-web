/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { globalEvents } from "@/io.ox/office/tk/events";
import { type IntervalPosition, getDeviceResolution, Rectangle } from "@/io.ox/office/tk/dom";
import { Logger } from "@/io.ox/office/tk/utils/logger";

import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";

// types ======================================================================

/**
 * DOM layout settings needed for a header pane in a spreadsheet view.
 *
 * The properties of the interface `IntervalPosition` specify the absolute
 * position of the header pane in the view's root node, and the size in pixels.
 * If the size is zero, the entire header pane will be hidden.
 */
export interface HeaderPaneLayout extends IntervalPosition {

    /**
     * Whether the header pane and the associated grid panes are frozen (i.e.,
     * not scrollable in the main direction).
     */
    frozen: boolean;

    /**
     * Whether to show the scroll bar for the opposite direction (e.g., the
     * horizontal scroll bar in a column header pane).
     */
    showOppositeScroll: boolean;

    /**
     * The total size of the hidden columns/rows in front of the sheet in
     * frozen view mode.
     */
    hiddenSize: number;
}

/**
 * Represents a bounding column/row interval in a header pane, together with
 * the exact pixel position of the bounding area. This pixel position may not
 * be aligned exactly to the start position of the first column/row, or the end
 * position of the last column/row in the interval.
 */
export interface HeaderBoundary {

    /**
     * The exact position and size of the bounding area, in pixels. May result
     * in a start position before or inside the first column/row of the
     * interval, or in an end position after or inside the last column/row.
     */
    position: IntervalPosition;

    /**
     * The column/row indexes of the bounding interval.
     */
    interval: Interval;
}

/**
 * The specification of an interval boundary for a header pane: Either an
 * object of type `HeaderBoundary` for a visible header pane, or the string
 * "hidden" for a hidden header pane.
 */
export type HeaderBoundarySpec = HeaderBoundary | "hidden";

/**
 * Represents the exact pixel position of the rendering layers in a grid pane,
 * together with the enclosing bounding cell range. This pixel position may not
 * be aligned exactly to the outer borders of the bounding range.
 */
export interface GridBoundary {

    /**
     * The exact pixel position of the bounding rectangle, in pixels. May
     * result in a start position before or inside the start cell of the range,
     * or in an end position after or inside the last cell.
     */
    rectangle: Rectangle;

    /**
     * The address of the bounding cell range.
     */
    range: Range;
}

/**
 * The specification of a layer boundary for a grid pane: Either an object of
 * type `LayerBoundary` for a visible grid pane, or the string "hidden" for a
 * hidden grid pane.
 */
export type GridBoundarySpec = GridBoundary | "hidden";

// globals ====================================================================

let resolution = getDeviceResolution();
globalEvents.on("change:resolution", newRes => { resolution = newRes; });

// public functions ===========================================================

/**
 * Returns the number of canvas pixels per CSS pixel according to the current
 * device resolution.
 *
 * @returns
 *  The number of canvas pixels per CSS pixel.
 */
export function getCanvasResolution(): number {
    return resolution;
}

/**
 * Converts the passed size from CSS pixels exactly (without rounding) to
 * canvas pixels.
 *
 * @param size
 *  The size in CSS pixels to be converted.
 *
 * @returns
 *  The converted size in canvas pixels.
 */
export function getCanvasSize(size: number): number {
    return size * resolution;
}

/**
 * Converts the passed coordinate from CSS pixels to canvas pixels. The
 * resulting coordinate will be rounded down to an integer, to ensure to render
 * the cell content into entire device pixels without blur effects.
 *
 * @param coord
 *  The coordinate in CSS pixels to be converted.
 *
 * @returns
 *  The converted coordinate in canvas pixels.
 */
export function getCanvasCoord(coord: number): number {
    return Math.floor(coord * resolution);
}

/**
 * Converts the coordinates in the passed rectangle from CSS pixels to canvas
 * pixels. The resulting rectangle will be rounded down to integers, to ensure
 * to render the cell content into entire device pixels without blur effects.
 *
 * @param rectangle
 *  The rectangle in CSS pixels to be converted.
 *
 * @returns
 *  The converted rectangle in canvas pixels.
 */
export function getCanvasRect(rectangle: Rectangle): Rectangle {
    const l = getCanvasCoord(rectangle.left);
    const t = getCanvasCoord(rectangle.top);
    const r = getCanvasCoord(rectangle.right());
    const b = getCanvasCoord(rectangle.bottom());
    return new Rectangle(l, t, r - l, b - t);
}

/**
 * Converts the passed size from canvas pixels exactly (without rounding) to
 * CSS pixels.
 *
 * @param size
 *  The size in canvas pixels to be converted.
 *
 * @returns
 *  The converted size in CSS pixels.
 */
export function getCssSize(size: number): number {
    return size / resolution;
}

/**
 * Converts the passed coordinate from canvas pixels to CSS pixels. The
 * resulting coordinate will be rounded up to an integer, to compensate
 * rounding down when converting to canvas pixels.
 *
 * @param coord
 *  The coordinate in canvas pixels to be converted.
 *
 * @returns
 *  The converted coordinate in CSS pixels.
 */
export function getCssCoord(coord: number): number {
    return Math.ceil(coord / resolution);
}

/**
 * Converts the coordinates in the passed rectangle from canvas pixels to CSS
 * pixels. The resulting rectangle will be rounded up to integers, to
 * compensate rounding down when converting to canvas pixels.
 *
 * @param rectangle
 *  The rectangle in canvas pixels to be converted.
 *
 * @returns
 *  The converted rectangle in CSS pixels.
 */
export function getCssRect(rectangle: Rectangle): Rectangle {
    const l = getCssCoord(rectangle.left);
    const t = getCssCoord(rectangle.top);
    const r = getCssCoord(rectangle.right());
    const b = getCssCoord(rectangle.bottom());
    return new Rectangle(l, t, r - l, b - t);
}

// singletons =================================================================

/**
 * A console logger that logs detailed information about the cell caching and
 * rendering process.
 *
 * Bound to the debug configuration item "spreadsheet:log-renderer".
 */
export const renderLogger = new Logger("spreadsheet:log-renderer", { tag: "RENDER", tagColor: 0x00FFFF });
