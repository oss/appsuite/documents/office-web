/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { SMALL_DEVICE } from '@/io.ox/office/tk/dom';

import { SplitMode, Interval } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { MIN_PANE_SIZE, PanePos } from '@/io.ox/office/spreadsheet/utils/paneutils';

// mix-in class ViewMixin =====================================================

/**
 * Implementations of all controller items for manipulating view settings
 * in the active sheet, intended to be mixed into a document controller
 * instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as zoom and split.
 */
export default function ViewMixin(docView) {

    const { selectionEngine } = docView;

    // self reference (the controller)
    var self = this;

    // the application instance
    var docApp = docView.docApp;

    // the document model
    var docModel = docView.docModel;

    // the model of the active sheet
    var sheetModel = null;

    // private methods ----------------------------------------------------

    /**
     * Toggles the dynamic view split mode according to the current view
     * split mode, and the current selection. Enabling dynamic split mode
     * in a frozen split view will thaw the frozen panes. Otherwise,
     * enabling the dynamic split mode will split the view at the position
     * left of and above the active cell. Disabling the dynamic split mode
     * will remove the split also if the view is in frozen split mode.
     *
     * @param {Boolean} state
     *  The new state of the dynamic view split mode.
     */
    function setDynamicSplit(state) {

        // whether the view is currently split
        var hasSplit = sheetModel.hasSplit();

        // disable dynamic or frozen split mode
        if (!state && hasSplit) {
            sheetModel.clearSplit();

        // convert frozen split to dynamic split
        } else if (state && sheetModel.hasFrozenSplit()) {
            sheetModel.setDynamicSplit(sheetModel.getSplitWidthHmm(), sheetModel.getSplitHeightHmm());

        // set dynamic split at active cell
        } else if (state && !hasSplit) {

            // the address of the active cell in the current selection
            var activeAddress = selectionEngine.getActiveCell();
            // additional scroll anchor attributes
            var attributes = {};

            // get position of active cell and visible area of the grid pane
            var cellRect = docView.getCellRectangle(activeAddress, { expandMerged: true });
            var visibleRect = docView.getActiveGridPane().getVisibleRectangle();

            // do not split vertically, if the active cell is shown at the left border
            var splitWidth = cellRect.left - visibleRect.left;
            if ((-cellRect.width < splitWidth) && (splitWidth < MIN_PANE_SIZE)) {
                splitWidth = 0;
            }

            // do not split horizontally, if the active cell is shown at the top border
            var splitHeight = cellRect.top - visibleRect.top;
            if ((-cellRect.height < splitHeight) && (splitHeight < MIN_PANE_SIZE)) {
                splitHeight = 0;
            }

            // split in the middle of the grid pane, if active cell is outside the visible area
            if (((splitWidth === 0) && (splitHeight === 0)) ||
                (splitWidth < 0) || (splitWidth > visibleRect.width - MIN_PANE_SIZE) ||
                (splitHeight < 0) || (splitHeight > visibleRect.height - MIN_PANE_SIZE)
            ) {
                splitWidth = Math.floor(visibleRect.width / 2);
                splitHeight = Math.floor(visibleRect.height / 2);
                attributes.anchorRight = sheetModel.colCollection.getScrollAnchorByOffset(visibleRect.left + splitWidth, { pixel: true });
                attributes.anchorBottom = sheetModel.rowCollection.getScrollAnchorByOffset(visibleRect.top + splitHeight, { pixel: true });
                attributes.activePane = PanePos.TL;
            } else {
                if (splitWidth > 0) { attributes.anchorRight = activeAddress.c; }
                if (splitHeight > 0) { attributes.anchorBottom = activeAddress.r; }
            }

            // activate the dynamic split view
            splitWidth = sheetModel.convertPixelToHmm(splitWidth);
            splitHeight = sheetModel.convertPixelToHmm(splitHeight);
            sheetModel.setDynamicSplit(splitWidth, splitHeight, attributes);
        }
    }

    /**
     * Toggles the frozen view split mode according to the current view
     * split mode, and the current selection. Enabling frozen split mode
     * in a dynamic split view will freeze the panes with their current
     * size (split mode 'frozenSplit'). Otherwise, enabling the frozen
     * split mode will split the view at the position left of and above the
     * active cell. Disabling the frozen split mode will either return to
     * dynamic split mode (if split mode was 'frozenSplit'), or will remove
     * the frozen split at all (if split mode was 'frozen').
     *
     * @param {Boolean|String} state
     *  The new state of the frozen view split mode (as boolean), or the
     *  string 'toggle' to toggle the current frozen split mode.
     */
    function setFrozenSplit(state) {

        if (state === 'toggle') { state = !sheetModel.hasFrozenSplit(); }

        // whether the view is currently split
        var hasSplit = sheetModel.hasSplit();

        // convert frozen split to dynamic split
        if (!state && hasSplit && (sheetModel.getSplitMode() === SplitMode.FROZEN_SPLIT)) {
            sheetModel.setDynamicSplit(sheetModel.getSplitWidthHmm(), sheetModel.getSplitHeightHmm());

        // disable split mode completely
        } else if (!state && hasSplit) {
            sheetModel.clearSplit();

        // convert dynamic split to frozen split
        } else if (state && hasSplit && !sheetModel.hasFrozenSplit()) {
            sheetModel.setFrozenSplit(sheetModel.getSplitColInterval(), sheetModel.getSplitRowInterval());

        // enable frozen split mode
        } else if (state && !hasSplit) {

            // the address of the active cell in the current selection
            var activeAddress = selectionEngine.getActiveCell();
            // the address of the top-left cell in the grid pane
            var topLeftAddress = docView.getActiveGridPane().getTopLeftAddress();

            // get position of active cell and visible area of the grid pane
            var cellRect = docView.getCellRectangle(activeAddress, { expandMerged: true });
            var visibleRect = docView.getActiveGridPane().getVisibleRectangle();

            // calculate frozen column interval (must result in at least one frozen column)
            var col1 = topLeftAddress.c;
            var col2 = (cellRect.left + MIN_PANE_SIZE <= visibleRect.right()) ? (activeAddress.c - 1) : -1;
            var validCols = col1 <= col2;

            // calculate frozen row interval (must result in at least one frozen row)
            var row1 = topLeftAddress.r;
            var row2 = (cellRect.top + MIN_PANE_SIZE <= visibleRect.bottom()) ? (activeAddress.r - 1) : -1;
            var validRows = row1 <= row2;

            // fall-back to single
            var colInterval = validCols ? new Interval(col1, col2) : validRows ? null : new Interval(col1);
            var rowInterval = validRows ? new Interval(row1, row2) : validCols ? null : new Interval(row1);

            // activate the frozen split view
            sheetModel.setFrozenSplit(colInterval, rowInterval);
        }
    }

    /**
     * Freezes the passed number of columns and rows in the active sheet,
     * regardless of the current split settings.
     *
     * @param {Number} cols
     *  The number of columns to freeze. The value zero will not freeze any
     *  column.
     *
     * @param {Number} rows
     *  The number of rows to freeze. The value zero will not freeze any
     *  row.
     */
    function setFixedFrozenSplit(cols, rows) {

        // the scroll anchors for the first visible column/row
        const colAnchor = sheetModel.propSet.get(sheetModel.hasColSplit() ? 'anchorLeft' : 'anchorRight');
        const rowAnchor = sheetModel.propSet.get(sheetModel.hasRowSplit() ? 'anchorTop' : 'anchorBottom');
        // the first visible column and row in the split view (reduce to be able to freeze the passed number of columns/rows)
        const { maxCol, maxRow } = docModel.addressFactory;
        const col = Math.max(0, Math.min(Math.round(colAnchor), maxCol - cols + 1));
        const row = Math.max(0, Math.min(Math.round(rowAnchor), maxRow - rows + 1));

        // activate the frozen split view
        sheetModel.setFrozenSplit((cols > 0) ? new Interval(col, col + cols - 1) : null, (rows > 0) ? new Interval(row, row + rows - 1) : null);
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        // parent item, enabled if the cell selection mode is active (no drawing object selected)
        // other document states do not care here (read-only mode, text edit mode, etc.)
        'view/selection/cell': {
            parent: 'app/valid',
            enable() { return !selectionEngine.hasDrawingSelection(); }
        },

        // parent item, enabled if the drawing selection mode is active
        // other document states do not care here (read-only mode, text edit mode, etc.)
        'view/selection/drawing': {
            parent: 'app/valid',
            enable() { return selectionEngine.hasDrawingSelection(); }
        },

        // parent item, enabled if the text edit mode is NOT active
        'view/editmode/off': {
            parent: 'app/valid',
            enable() { return !docView.isTextEditMode(); }
        },

        // parent item, enabled if any text edit mode is active
        'view/editmode/on': {
            parent: 'app/valid',
            enable() { return docView.isTextEditMode(); }
        },

        // parent item, enabled if the cell edit mode is active
        'view/editmode/cell': {
            parent: 'view/selection/cell',
            enable() { return docView.isTextEditMode('cell'); }
        },

        // parent item, enabled if the drawing edit mode is active
        'view/editmode/drawing': {
            parent: 'view/selection/drawing',
            enable() { return docView.isTextEditMode('drawing'); }
        },

        // represents the visibility state of the comments pane (setter expects a boolean state)
        'view/commentspane/show': {
            parent: ['app/valid', 'document/ooxml'],
            enable() { return !SMALL_DEVICE; },
            get() { return docView.commentsPane.isVisible(); },
            set() { docView.commentsPane.toggle(); }
        },

        // represents the visibility state of the formula pane (setter expects a boolean state)
        'view/formulapane/show': {
            parent: 'document/worksheet',
            get() { return docView.formulaPane.isVisible(); },
            set(state) {
                docApp.setUserSettingsValue('showFormulaBar', state); // save in background
                docView.formulaPane.toggle(state);
            }
        },

        // toggles the height of the formula pane (single text line vs. multiple text lines)
        'view/formulapane/toggle/height': {
            parent: 'app/valid',
            get() { return docView.formulaPane.isExpanded(); },
            set(state) { return docView.formulaPane.toggleHeight(state); }
        },

        // represents the visibility state of the status pane (setter expects a boolean state)
        'view/statuspane/show': {
            parent: 'app/valid',
            get() { return docView.statusPane.isVisible(); },
            set(state) {
                docApp.setUserSettingsValue('showStatusBar', state); // save in background
                docView.statusPane.toggle(state);
            }
        },

        // item value is the current status label to be shown in the status bar
        'view/status/label': {
            parent: 'app/valid',
            enable() { return docView.getStatusLabel() !== null; },
            get() { return docView.getStatusLabel(); }
        },

        // item value is the type of the subtotals result (e.g. 'sum', 'average') to be shown in the status pane
        'view/status/subtotals': {
            parent: 'document/worksheet',
            enable() {
                var subtotals = docView.getSubtotalResult();
                return (subtotals.cells > 1) && (this.value in subtotals);
            },
            get() { return docApp.getUserSettingsValue('subtotalType', 'sum'); },
            set(subtotalType) { return docApp.setUserSettingsValue('subtotalType', subtotalType); }
        },

        // item value is the formula expression of the active cell, with leading equality sign
        'view/status/formula': {
            parent: ['document/worksheet', 'view/selection/cell', 'view/editmode/off'],
            enable() {
                return !docView.formulaPane.isVisible() && selectionEngine.isSingleCellSelection() && (this.value !== null);
            },
            get() {
                return docView.cellCollection.getEditFormula(selectionEngine.getActiveCell());
            }
        },

        'view/grid/show': {
            parent: 'document/worksheet',
            get() { return sheetModel.isGridVisible(); },
            set(state) { sheetModel.toggleGridVisibility(state); }
        },

        'view/split/dynamic': {
            parent: ['document/worksheet', 'view/editmode/off'],
            get() { return sheetModel.hasDynamicSplit(); },
            set: setDynamicSplit
        },

        'view/split/frozen': {
            parent: ['document/worksheet', 'view/editmode/off'],
            get() { return sheetModel.hasFrozenSplit(); },
            set: setFrozenSplit
        },

        'view/split/frozen/fixed': {
            parent: ['document/worksheet', 'view/editmode/off'],
            set(value) { setFixedFrozenSplit(value.cols, value.rows); }
        },

        // dropdown menu buttons ------------------------------------------

        // dummy item for the cell alignment drop-down menu in "shrink-to-menu" mode
        'view/cell/alignment/menu': {
            parent: 'sheet/operation/cell' // can be used in locked sheets
        },

        // dummy item for the number format drop-down menu in "shrink-to-menu" mode
        'view/cell/numberformat/menu': {
            parent: 'sheet/operation/cell' // can be used in locked sheets
        },

        // dummy item for the border formatting drop-down menu in "shrink-to-menu" mode
        'view/cell/border/menu': {
            parent: 'sheet/operation/cell' // can be used in locked sheets
        },

        // dummy item for "Notes" dropdown menu
        'view/notes/menu': {
            parent: 'note/available/any'
        },

        // dummy item for the data points dropdown menu
        'view/chart/datapoints/menu': {
            parent: 'drawing/chart/valid'
        },

        // submenus in context menu ---------------------------------------

        // dummy item for a dropdown menu containing actions for columns
        'view/column/submenu': {
            parent: 'sheet/operation/unlocked/cell'
        },

        // dummy item for a dropdown menu containing actions for rows
        'view/row/submenu': {
            parent: 'sheet/operation/unlocked/cell'
        },

        // dummy item for "Insert" submenu in context menu
        'view/insert/submenu': {
            parent: 'document/editable'
        },

        // dummy item for "Cell protection" submenu in context menu
        'view/protect/submenu': {
            parent: 'sheet/operation/unlocked/cell'
        },

        // dummy item for "Comment" submenu in context menu (comment threads)
        'view/comment/submenu': {
            // DOCS-3837: visible in readonly documents, but not in viewer mode
            parent: ['threadedcomment/hasthread', '!app/viewermode']
        },

        // dummy item for "Note" submenu in context menu (cell notes)
        'view/note/submenu': {
            parent: 'note/model'
        },

        // tool bars and tool bar tabs ------------------------------------

        // base item for enabled state of toolpanes for cell formatting
        'view/toolpane/textformat/enabled': {
            parent: 'document/editable/worksheet',
            enable() { return self.isItemEnabled('view/selection/cell') || self.isItemEnabled('drawing/operation/text'); }
        },

        // enabled if the "Format" toolpane is available (regular toppane/main toolpane mode)
        'view/toolpane/format/visible': {
            parent: ['view/toolpane/textformat/enabled', '!view/combinedpanes']
        },

        // enabled if the "Font" toolpane is available (combined panes mode)
        'view/toolpane/font/visible': {
            parent: ['view/toolpane/textformat/enabled', 'view/combinedpanes']
        },

        // enabled if the "Alignemnt" toolpane is available (combined panes mode)
        'view/toolpane/alignment/visible': {
            parent: ['document/editable/worksheet', 'view/selection/cell', 'view/combinedpanes']
        },

        // enabled if the "Cell" toolpane is available (combined panes mode)
        'view/toolpane/cell/visible': {
            parent: ['document/editable/cell', 'view/combinedpanes']
        },

        // enabled if the "Number Format" toolpane is available (combined panes mode)
        'view/toolpane/numberformat/visible': {
            parent: ['document/editable/worksheet', 'view/selection/cell', 'view/combinedpanes']
        },

        // enabled if the "Data" toolpane is available
        'view/toolpane/data/visible': {
            parent: 'document/editable/cell'
        },

        // enabled if the "Insert" toolpane is available
        'view/toolpane/insert/visible': {
            parent: ['document/editable/worksheet', '!view/combinedpanes']
        },

        // enabled if the "Columns/Rows" toolpane is available
        'view/toolpane/colrow/visible': {
            parent: 'document/editable/cell'
        },

        // enabled if the "Table" toolpane is available
        'view/toolpane/table/visible': {
            parent: ['sheet/table/selected/real', 'document/ooxml', '!view/combinedpanes'],
            enable(tableModel) { return tableModel !== null; }
        },

        // enabled if the "Drawing" toolpane is available
        'view/toolpane/drawing/visible': {
            parent: 'document/editable/drawing'
        },

        // enabled if the "Comments" toolpane is available
        'view/toolpane/comments/visible': {
            parent: 'document/editable/cell',
            // bug 57332: hide toolbar as long as it is not possible to insert comment threads
            // bug 67688: in OOXML (only) it is now possible to insert comment threads
            enable() { return docApp.isOOXML() || self.isItemEnabled('note/available/any'); }
        },

        // tool bars ------------------------------------------------------

        // visibility item for the 'Connector' toolbar for drawing objects
        // (at least one selected connector object)
        'view/toolbar/drawing/connector/visible': {
            parent: 'drawing/operation/connector'
        },

        // visibility item for the 'Border' toolbar for drawing objects
        // (at least one selected drawing object that supports line attributes, but no connectors are selected)
        'view/toolbar/drawing/border/visible': {
            parent: ['drawing/operation/line', '!drawing/operation/connector']
        },

        // visibility item for the 'Fill' toolbar for drawing objects
        // (at least one selected drawing object that supports fill attributes)
        'view/toolbar/drawing/fill/visible': {
            parent: 'drawing/operation/fill'
        }
    });

    // initialization -----------------------------------------------------

    // update GUI after changing dynamic model properties
    this.updateOnAllEvents(docModel.propSet);

    // update GUI after changed selection, after receiving view updates, and when switching text edit modes
    this.updateOnEvent(docView, ["change:sheetprops", "update:selection:data", "textedit:enter", "textedit:change", "textedit:leave", "change:statuslabel"]);

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
    });
}
