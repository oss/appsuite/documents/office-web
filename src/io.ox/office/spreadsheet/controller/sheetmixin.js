/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import { str, jpromise } from '@/io.ox/office/tk/algorithms';
import { SheetType } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { SheetNameDialog, MoveSheetsDialog, UnhideSheetsDialog } from '@/io.ox/office/spreadsheet/view/dialogs';

// mix-in class SheetMixin ====================================================

/**
 * Implementations of all controller items for manipulating the collection
 * of sheets in the document, intended to be mixed into a document
 * controller instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the active sheet.
 */
export default function SheetMixin(docView) {

    // self reference
    var self = this;

    // the document model
    var docModel = docView.docModel;
    var sheetCollection = docModel.sheetCollection;

    // the model and index of the active sheet
    var sheetModel = null;
    var activeSheet = -1;

    // private methods ----------------------------------------------------

    /**
     * Returns the current number of supported and visible/hidden sheets.
     *
     * @param {boolean} visible
     *  Whether to return the number of visible or hidden sheets.
     *
     * @returns {number}
     *  The current number of supported and visible/hidden sheets.
     */
    function getSheetCount(visible) {
        return docModel.getSheetCount({ supported: true, visible });
    }

    /**
     * Activates the nearest visible sheet next to the specified sheet in
     * the document view.
     *
     * @param {number} sheet
     *  The zero-based index of the sheet to be activated.
     *
     * @param {string} [method="nearest"]
     *  The method how to find a visible sheet:
     *  - "next": If the specified sheet is hidden, searches for the
     *    nearest following visible sheet. All sheets preceding the
     *    specified sheet will be ignored.
     *  - "prev": If the specified sheet is hidden, searches for the
     *    nearest preceding visible sheet. All sheets following the
     *    specified sheet will be ignored.
     *  - "nearest" (default value): If the specified sheet is hidden,
     *    first searches with the method "next", and if this fails, with
     *    the method "prev".
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the new sheet has been activated.
     */
    function activateSheet(sheet, method) {

        // leave text edit mode before changing the sheet
        var promise = docView.leaveTextEditMode().then(function () {

            // Set the new active sheet at the document model. The handlers
            // of the `docModel.propSet` change events will do all further
            // processing and initialization for the new active sheet.
            var newSheet = sheetCollection.findVisibleSheet(sheet, method);
            if (newSheet >= 0) { docModel.setActiveSheet(newSheet); }
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Inserts a new sheet in the spreadsheet document, and activates it in
     * the view. In case of an error, shows a notification message.
     *
     * @returns {JPromise}
     *  A promise that will fulfil, if the new sheet has been inserted
     *  successfully.
     */
    function insertSheet() {

        // leave text edit mode before inserting the sheet
        var promise = docView.leaveTextEditMode().then(function () {

            // server-side configuration contains the maximum number of sheets per document
            if (!sheetCollection.canInsertSheet()) {
                return jpromise.reject();
            }

            // insert the new sheet directly after the active sheet
            var newSheet = activeSheet + 1;
            // create a new unused sheet name
            var sheetName = sheetCollection.generateUnusedSheetName();

            // attribute set of the new sheet (bug 57798: resolve and insert default row height)
            const { fontMetrics, autoStyles } = docModel;
            var defCharAttrs = autoStyles.getDefaultAttributeSet().character;
            var rowHeight = fontMetrics.convertRowHeightToUnit(fontMetrics.getRowHeightHmm(defCharAttrs));
            var newAttrSet = { row: { height: rowHeight } };

            // bug 57798: resolve and insert default column width according to file format
            switch (self.docApp.getFileFormat()) {
                case 'ooxml':
                    newAttrSet.sheet = { baseColWidth: sheetModel.getMergedAttributeSet(true).sheet.baseColWidth };
                    break;
                case 'odf':
                    newAttrSet.column = { width: 2258 };
                    break;
            }

            // generate and apply the 'insertSheet' operation
            return docModel.buildOperations(function (builder) {
                return sheetCollection.generateInsertSheetOperations(builder, newSheet, SheetType.WORKSHEET, sheetName, newAttrSet);
            }, { storeSelection() { docModel.setActiveSheet(newSheet); } });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Deletes the active sheet from the spreadsheet document, and
     * activates another sheet in the view. Does nothing, if the document
     * contains a single sheet only or if the user doesn't really want
     * to delete the sheet without undo after being asked for.
     *
     * @returns {JPromise}
     *  A promise that will fulfil, if the active sheet has been deleted.
     */
    function deleteSheet() {

        // bug 31772: cancel edit mode before deleting the sheet (do not commit something into the deleted sheet)
        docView.cancelTextEditMode();

        // do not delete the last visible sheet
        if (sheetModel.isVisible() && (getSheetCount(true) <= 1)) {
            return jpromise.reject();
        }

        // ask to proceed (deleting sheets cannot be undone)
        var title = gt('Delete sheet');
        var message = gt('Deleting a sheet cannot be undone. Proceed operation?');
        var promise = docView.showQueryDialog(title, message, {
            // close automatically, if active sheet will be deleted or hidden by another client
            autoCloseScope: 'deleteSheet',
            // DOCS-2135: close the dialog when only one visible sheet is left
            autoCloseKey: 'document/deletesheet'
        });

        // generate and apply the "deleteSheet" operation after confirmation
        promise = promise.then(function () {
            return docModel.buildOperations(function (builder) {
                return sheetCollection.generateDeleteSheetOperations(builder, activeSheet);
            }, { storeSelection: true, undoMode: 'clear' });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Copies the active sheet and inserts the copy with the provided sheet
     * name behind the active sheet. In case of an error, shows a warning
     * message.
     *
     * @param {string} sheetName
     *  The name of the new sheet.
     *
     * @returns {JPromise}
     *  A promise that will fulfil, if the sheet has been copied.
     */
    function copySheet(sheetName) {

        // leave text edit mode before copying the sheet
        var promise = docView.leaveTextEditMode();

        // generate and apply the "copySheet" operation
        promise = promise.then(function () {
            return docModel.buildOperations(function (builder) {
                return sheetCollection.generateCopySheetOperations(builder, activeSheet, activeSheet + 1, str.cleanNPC(sheetName));
            }, { storeSelection() { docModel.setActiveSheet(activeSheet + 1); } });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows a dialog that allows to enter a name of a new sheet copied
     * from the active sheet. The dialog will be kept open until a valid
     * sheet name has been entered.
     *
     * @returns {JPromise}
     *  A promise representing the dialog. Will fulfil, after the sheet has
     *  been copied successfully; or will reject, if the dialog has been
     *  canceled, or if the document has switched to read-only mode.
     */
    function copySheetWithDialog() {

        // leave text edit mode before copying the sheet
        return docView.leaveTextEditMode().then(function () {

            // create the dialog, initialize with an unused sheet name
            var dialog = new SheetNameDialog(docView, {
                title: gt('Copy sheet'),
                sheetName: sheetCollection.generateUnusedSheetName(),
                okLabel: gt('Copy'),
                actionHandler: copySheet
            });

            // show the dialog, copying the sheet will be done in the callback function
            return dialog.show();
        });
    }

    /**
     * Moves the specified sheet in the document to a new position. In
     * difference to most other methods of this class, this method does NOT
     * work on the active sheet, but on an arbitrary sheet in the document.
     *
     * @param {number} fromSheet
     *  The zero-based index of the sheet to be moved.
     *
     * @param {number} toSheet
     *  The new zero-based index of the sheet.
     *
     * @returns {JPromise}
     *  A promise that will fulfil, if the sheet has been moved.
     */
    function moveSheet(fromSheet, toSheet) {

        // nothing to do, if the sheet does not move
        if (fromSheet === toSheet) { return self.createResolvedPromise(); }

        // leave text edit mode before moving the sheet
        var promise = docView.leaveTextEditMode();

        // generate and apply the "moveSheet" operation
        promise = promise.then(function () {
            return docModel.buildOperations(function (builder) {
                return sheetCollection.generateMoveSheetOperations(builder, fromSheet, toSheet);
            });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows the "Move Sheets" dialog, and creates and applies the
     * respective "moveSheets" operation.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after the dialog has been closed, and
     *  the sheets have been moved.
     */
    function moveSheetsWithDialog() {

        // leave text edit mode before moving the sheets
        var promise = docView.leaveTextEditMode();

        // create and show the "Move Sheets" dialog
        promise = promise.then(function () {
            return new MoveSheetsDialog(docView).show();
        });

        // generate the "moveSheets" operations
        promise = promise.then(function (sortVector) {
            return docModel.buildOperations(function (builder) {
                return sheetCollection.generateMoveSheetsOperations(builder, sortVector);
            });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows a dialog that allows to make some or all hidden sheets visible
     * again.
     *
     * @returns {JPromise}
     *  A promise representing the dialog. Will fulfil, after the sheets
     *  have been shown successfully; or will reject, if the dialog has
     *  been canceled, or if the document has switched to read-only mode.
     */
    function showSheetsWithDialog() {

        // leave edit modes before displaying the dialog
        var promise = docView.leaveTextEditMode();

        // create and show the 'Show Sheets' dialog
        promise = promise.then(function () {
            return new UnhideSheetsDialog(docView).show();
        });

        // generate the "changeSheet" operations for all hidden sheets
        promise = promise.then(function (sheets) {
            return docModel.buildOperations(function (builder) {
                var attrSet = { sheet: { visible: true } };
                sheets.forEach(function (sheet) {
                    var sheetModel = docModel.getSheetModel(sheet);
                    var sheetCache = builder.getSheetCache(sheetModel);
                    sheetModel.generateChangeOperations(sheetCache, attrSet);
                });
            });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Renames the active sheet in the spreadsheet document, if possible.
     * In case of an error, shows a notification message.
     *
     * @param {string} sheetName
     *  The new name of the active sheet.
     *
     * @returns {JPromise}
     *  A promise that will fulfil, if the sheet has been renamed.
     */
    function renameSheet(sheetName) {

        // leave text edit mode before renaming the sheet (sheet name may be used in formulas)
        var promise = docView.leaveTextEditMode().then(function () {

            // remove all NPCs from passed sheet name
            sheetName = str.cleanNPC(sheetName);

            // build the operations to rename the sheet and update all formulas
            return docModel.buildOperations(function (builder) {
                return sheetCollection.generateRenameSheetOperations(builder, activeSheet, sheetName);
            });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows a dialog that allows to enter a new name for the active sheet.
     * The dialog will be kept open until a valid sheet name has been
     * entered.
     *
     * @returns {JPromise}
     *  A promise representing the dialog. Will fulfil, after the sheet has
     *  been renamed successfully; or will reject, if the dialog has been
     *  canceled, or if the document has switched to read-only mode.
     */
    function renameSheetWithDialog() {

        // leave text edit mode before renaming the sheet (sheet name may be used in formulas)
        return docView.leaveTextEditMode().then(function () {

            // create the dialog, initialize with the name of the active sheet
            var dialog = new SheetNameDialog(docView, {
                title: gt('Rename sheet'),
                sheetName: docModel.getSheetName(activeSheet),
                okLabel: gt('Rename'),
                actionHandler: renameSheet
            });

            // show the dialog, renaming the sheet will be done in the callback function
            return dialog.show();
        });
    }

    /**
     * Changes the formatting attributes of the active sheet.
     *
     * @param {Dict} sheetAttrs
     *  The sheet formatting attributes to be changed in the active sheet.
     *
     * @returns {JPromise}
     *  A promise that will fulfil, if the sheet has been changed.
     */
    function changeSheet(sheetAttrs) {

        // leave text edit mode before manipulating the sheet
        var promise = docView.leaveTextEditMode();

        // select unsaved comment (in the current sheet only!) instead of hiding the sheet
        promise = promise.then(function () {
            var processComment = (sheetAttrs.visible === false) && sheetModel.hasUnsavedComment();
            return processComment ? docView.processUnsavedComment() : null;
        });

        // generate and apply the operations
        promise = promise.then(function () {
            return sheetModel.buildOperations(function (sheetCache) {
                sheetModel.generateChangeOperations(sheetCache, { sheet: sheetAttrs });
            });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    // protected methods --------------------------------------------------

    /**
     * Activates a sheet for the import preview, while the document import
     * is still running.
     *
     * _Attention:_ Called from the preview handler of the application. MUST
     * NOT be called directly.
     *
     * @param {number} sheet
     *  The zero-based index of the initial active sheet.
     *
     * @returns {boolean}
     *  Whether a sheet has been activated successfully.
     */
    this.activatePreviewSheet = function (sheet) {

        // the sheet model (no preview possible, if the sheet does not exist or is not visible)
        var sheetModel = docModel.getSheetModel(sheet);
        if (!sheetModel || !sheetModel.isVisible() || !sheetModel.isSupported) { return false; }

        // initialize dynamic properties (scroll position, selection, zoom, split)
        sheetModel.initializePropSet();
        // activate the preview sheet in the view
        docModel.setActiveSheet(sheet);

        // return whether the sheet has been activated successfully (i.e. the event
        // handler for the 'change:activesheet' event has updated 'activeSheet')
        return activeSheet >= 0;
    };

    // item registration --------------------------------------------------

    // register important controller items immediately
    this.registerItems({

        // model index of the active sheet
        'view/sheet/active': {
            parent: 'app/valid',
            get() { return activeSheet; },
            set: activateSheet
        }
    });

    // register all controller items
    this.registerItemsOnSuccess({

        // view settings --------------------------------------------------

        'view/sheet/prev': {
            parent: 'app/valid',
            enable() { return this.value >= 0; },
            get() { return sheetCollection.findVisibleSheet(activeSheet - 1, 'prev'); },
            set() { return activateSheet(activeSheet - 1, 'prev'); },
            shortcut: { keyCode: 'PAGE_UP', ctrlOrMeta: true, alt: true }
        },

        'view/sheet/next': {
            parent: 'app/valid',
            enable() { return this.value >= 0; },
            get() { return sheetCollection.findVisibleSheet(activeSheet + 1, 'next'); },
            set() { return activateSheet(activeSheet + 1, 'next'); },
            shortcut: { keyCode: 'PAGE_DOWN', ctrlOrMeta: true, alt: true }
        },

        // unique identifier for the "More" dropdown menu for sheet actions (used in test automation)
        'view/sheet/more': {
            parent: 'document/editable'
        },

        // document operations --------------------------------------------

        // enabled if a worksheet is currently active (disabled for chart sheets, dialog sheets, etc.)
        'document/worksheet': {
            parent: 'app/valid',
            enable() { return sheetModel.isWorksheet; }
        },

        // enabled if the document is editable, and a worksheet is currently active
        'document/editable/worksheet': {
            parent: ['document/editable', 'document/worksheet']
        },

        // enabled if the document is in read-only mode, and a worksheet is currently active
        'document/readonly/worksheet': {
            parent: ['document/readonly', 'document/worksheet']
        },

        // parent item that is enabled if the document contains more than one visible sheet
        'document/editable/multisheets': {
            parent: 'document/editable',
            enable() { return getSheetCount(true) > 1; }
        },

        'document/insertsheet': {
            parent: 'document/editable',
            enable() { return sheetCollection.canInsertSheet(); },
            set: insertSheet
        },

        'document/deletesheet': {
            parent: 'document/editable/multisheets',
            set: deleteSheet
        },

        'document/copysheet/dialog': {
            parent: ['document/insertsheet', 'document/worksheet', 'document/ooxml'], // copy chart sheet not implemented in filter yet!
            set: copySheetWithDialog
        },

        'document/movesheet': {
            parent: 'document/editable',
            set(data) { return moveSheet(data.from, data.to); }
        },

        'document/movesheets/dialog': {
            parent: 'document/editable/multisheets',
            set: moveSheetsWithDialog
        },

        'document/showsheets/dialog': {
            parent: 'document/editable',
            enable() { return getSheetCount(false) > 0; },
            set: showSheetsWithDialog
        },

        // reference style in formulas (`false`: A1; `true`: R1C1)
        'document/formula/rcstyle': {
            parent: 'view/editmode/off',
            get() { return docModel.propSet.get('rcStyle'); },
            set(rcStyle) { docModel.propSet.set('rcStyle', rcStyle); }
        },

        // Parent item for controller items used for formula recalculation, and other formula dependency functionality.
        // The item is enabled, if the dependency manager is still active. It may become inactive if the document is too
        // complex (too many formulas).
        'document/formula/dependencies': {
            parent: 'document/editable',
            enable() { return docModel.dependencyManager.isConnected(); }
        },

        // recalculate all formulas contained in the document (also all conditional formattings, chart source ranges, etc.)
        'document/formula/recalc/all': {
            parent: 'document/formula/dependencies',
            set() { return docModel.dependencyManager.recalcFormulas({ all: true }); }
        },

        // single-sheet operations ----------------------------------------

        // returns/changes the name of the active sheet
        'sheet/name': {
            parent: 'document/editable',
            get() { return docModel.getSheetName(activeSheet); },
            set: renameSheet
        },

        'sheet/rename/dialog': {
            parent: 'document/editable',
            set: renameSheetWithDialog
        },

        // item value is the map of attributes of family 'sheet' of the active sheet
        'sheet/attributes': {
            parent: 'document/editable',
            get() { return sheetModel.getMergedAttributeSet(true).sheet; }
        },

        // represents the visibility state of the active sheet
        'sheet/visible': {
            parent: ['sheet/attributes', 'document/editable/multisheets'],
            get(attributes) { return attributes.visible; },
            set(visible) { return changeSheet({ visible }); }
        },

        // represents the locked state of the active sheet
        'sheet/locked': {
            parent: ['sheet/attributes', 'document/editable/worksheet'],
            get(attributes) { return attributes.locked; },
            set(locked) { return changeSheet({ locked }); }
        }
    });

    // initialization -----------------------------------------------------

    // no formula recalculation actions if the document is too complex
    this.updateOnEvent(docModel.dependencyManager, 'recalc:overflow');

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
        activeSheet = docView.activeSheet;
    });
}
