/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { itr, jpromise } from '@/io.ox/office/tk/algorithms';

import { OperationError } from '@/io.ox/office/editframework/utils/operationerror';
import { MAX_SORT_LINE_LENGTH, MAX_SORT_LINES_COUNT, makeRejected } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { QueryDialogButtonMode, CustomSortDialog } from '@/io.ox/office/spreadsheet/view/dialogs';

// mix-in class TableMixin ====================================================

/**
 * Implementations of all controller items for manipulating the selected
 * table in the active sheet (either a 'real' table range with styling, or
 * the auto-filter of the sheet), intended to be mixed into a document
 * controller instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the active sheet.
 */
export default function TableMixin(docView) {

    const { selectionEngine } = docView;

    // self reference (the controller instance)
    var self = this;

    // the model and collections of the active sheet
    var sheetModel = null;
    var colCollection = null;
    var rowCollection = null;
    var cellCollection = null;
    var mergeCollection = null;
    var tableCollection = null;

    // sort order to be used for next "toggle" request
    var nextDescending = false;

    // private methods ----------------------------------------------------

    /**
     * Generates and applies the operations to toggle the filter state of
     * the passed table model.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected, or `null` to
     *  create a new auto-filter for the selected cell range.
     *
     * @param {boolean} state
     *  The new filter state of the specified table model.
     *
     * @returns {JPromise}
     *  A promise that will fulfil if the filter state has been updated
     *  successfully.
     */
    function filterTable(tableModel, state) {

        // no filter manipulation in locked sheets
        var promise = docView.ensureUnlockedSheet();

        // toggle filter state of table range; or toggle entire auto-filter
        promise = promise.then(function () {
            return self.docModel.buildOperations(function (builder) {

                // create the operation cache for the active sheet
                var sheetCache = builder.getSheetCache(sheetModel);

                // toggle filter of a real table range (not auto-filter)
                if (tableModel && !tableModel.isAutoFilter()) {
                    // TODO
                    //return tableModel.generateChangeTableOperations(sheetCache, { table: { filtered: state } });
                    builder.throw('operation', '$badge{TableMixin} filterTable: missing implementation');
                }

                // remove existing auto-filter (regardless of passed state)
                if (tableModel) { tableModel.generateDeleteOperations(sheetCache); }

                // concatenate optional asynchronous methods
                var promise2 = self.createResolvedPromise();

                // create a new table for the auto-filter
                if (state) {
                    promise2 = promise2.then(function () {

                        // the current selection ranges (auto-filter cannot be created on a multi selection)
                        var ranges = selectionEngine.getSelectedRanges();
                        OperationError.assertCause(ranges.length === 1, 'autofilter:multiselect');

                        // expand single cell to content range
                        var range = sheetModel.getContentRangeForCell(ranges.first());
                        // bug 36606: restrict range to used area of the sheet (range may become null)
                        var usedRange = cellCollection.getUsedRange();
                        range = usedRange ? range.intersect(usedRange) : null;

                        // bug 36227: selection must not be entirely blank
                        OperationError.assertCause(range && !cellCollection.areRangesBlank(range), 'autofilter:blank');

                        // generate the operations to insert the new table range
                        return tableCollection.generateInsertTableOperations(sheetCache, '', range, { table: { headerRow: true } });
                    });
                }

                // translate error codes for auto-filter
                return promise2.catch(function (result) {
                    result.cause = result.cause.replace(/^table:/, 'autofilter:');
                    throw result;
                });
            }, { storeSelection: true });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Generates and applies the operations to refresh the filtering and
     * sorting of the passed table model.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected.
     *
     * @returns {JPromise}
     *  A promise that will fulfil if the filtering and sorting of the
     *  table model has been updated successfully.
     */
    function refreshTable(tableModel) {

        // ensure unlocked sheet (no filter manipulation in locked sheets)
        var promise = docView.ensureUnlockedSheet();

        // create and apply the operations to refresh the active table
        promise = promise.then(function () {
            return sheetModel.createAndApplyOperations(function (generator) {
                return tableModel.generateRefreshOperations(generator, { updateFilter: true, updateSort: true });
            }, { applyImmediately: true, storeSelection: true });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Returns whether sorting is currently available according to the
     * selection in the active sheet.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected.
     *
     * @returns {boolean}
     *  Whether sorting is currently available.
     */
    function isSortEnabled(tableModel) {

        // only one single range can be sorted
        var selectedRanges = selectionEngine.getSelectedRanges();
        if (selectedRanges.length !== 1) { return false; }
        var selectedRange = selectedRanges[0];

        // if the selection is completely inside a table range, it will be sorted
        if (tableModel) { return tableModel.getRange().contains(selectedRange); }

        // otherwise, the selection must not overlap with any table range
        return !itr.shift(tableCollection.yieldTableModels({ overlapRange: selectedRange }));
    }

    /**
     * Checks the size of the passed sorting range.
     *
     * @returns {boolean}
     *  Whether the passed sort range is valid.
     */
    function hasValidSortRangeSize(sortRange, sortVert) {

        // check the maximum supported sort size (columns)
        const vertical = sortVert === true;
        const cols = vertical ? sortRange.cols() : colCollection.countIndexes(sortRange.colInterval(), { visible: true });
        const maxCols = vertical ? MAX_SORT_LINE_LENGTH : MAX_SORT_LINES_COUNT;
        if (cols > maxCols) { return false; }

        // check the maximum supported sort size (rows)
        const horizontal = sortVert === false;
        const rows = horizontal ? sortRange.rows() : rowCollection.countIndexes(sortRange.rowInterval(), { visible: true });
        const maxRows = horizontal ? MAX_SORT_LINE_LENGTH : MAX_SORT_LINES_COUNT;
        if (rows > maxRows) { return false; }

        return true;
    }

    /**
     * Checks the passed sorting range for validity. Rejects the returned
     * promise, if the range is too large, or if it contains or overlaps
     * with matrix formulas or merged cell ranges.
     *
     * @returns {JPromise<Range>}
     *  A promise that will fulfil if the passed cell range can be used for
     *  sorting.
     */
    function checkSortRange(sortRange) {

        // check that the sort range does not contain matrix formulas
        return docView.ensureUnlockedRanges(sortRange, { lockMatrixes: 'full' }).then(function () {

            // check the maximum supported sort size
            if (!hasValidSortRangeSize(sortRange)) {
                return makeRejected('sort:overflow');
            }

            // bug 56313: check for merged ranges before generating the operations
            if (mergeCollection.coversAnyMergedRange(sortRange)) {
                return makeRejected('sort:merge:overlap');
            }

            return sortRange;
        });
    }

    /**
     * Returns the address of the effective cell range to be sorted. For
     * table range, returns its entire data range. For all other cell
     * selections, asks whether to expand the range to the entire content
     * range if necessary.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected.
     *
     * @returns {JPromise<object>}
     *  A promise that will fulfil with a descriptor object with the
     *  following properties:
     *  - {Range} sortRange
     *      The cell range to be sorted, with header cells).
     *  - {boolean} hasHeader
     *      Whether the sort range includes a header row (either from table
     *      model or detected from sorting range contents).
     */
    function getSortRange(tableModel) {
        // this function will be called from controller item setters, therefore the view
        // contains a valid single cell range selection according to isSortEnabled()

        // the selected range to be sorted
        var sortRange = tableModel ? tableModel.getDataRange() : selectionEngine.getActiveRange();
        if (!sortRange) { return makeRejected('sort:blank'); }

        // selection must not be blank (nothing to sort)
        if (!tableModel && cellCollection.areRangesBlank(sortRange)) {
            return makeRejected('sort:blank');
        }

        // check validity of the sort range (no matrix formulas, not oversized, etc.)
        var promise = checkSortRange(sortRange);

        // try to expand the sorting range (promise will return the new sorting range)
        promise = promise.then(function () {

            // resolve the promise with the table range; or with a 2D range (without expansion)
            if (tableModel || (!sortRange.singleCol() && !sortRange.singleRow())) {
                return sortRange;
            }

            // column/row vector, or single cell: try to expand to the enclosing content range
            var contentRange = cellCollection.findContentRange(sortRange);
            if (contentRange.equals(sortRange)) { return sortRange; }

            // column/row vector: ask whether to expand (always expand from single cell selection)
            var promise2 = sortRange.single() ? self.createResolvedPromise(true) :
                docView.showQueryDialog(gt('Sorting'), gt('Expand the selection?'), {
                    buttonMode: QueryDialogButtonMode.CANCEL_BUTTON,
                    // close automatically, if active sheet will be deleted or hidden by another client
                    autoCloseScope: 'activeSheet'
                });

            // dialog answered with "Yes": expand sort range to content range
            return promise2.then(function (response) {
                return response ? checkSortRange(contentRange) : sortRange;
            });
        });

        // resolve the promise with the effective sort range and table model
        return promise.then(function (effSortRange) {
            selectionEngine.selectRange(effSortRange, { active: selectionEngine.getActiveCell() });
            var hasHeader = tableModel ? tableModel.hasHeaderRow() : cellCollection.hasHeaderRow(effSortRange);
            return { sortRange: effSortRange, hasHeader };
        });
    }

    /**
     * Returns the identifier of the sort order to be applied for the next
     * "table/sort" controller command.
     *
     * @returns {string}
     *  The next sort order (either "ascending", or "descending").
     */
    function getNextSortOrder() {
        return nextDescending ? 'descending' : 'ascending';
    }

    /**
     * Sorts the specified cell range according to the sorting rules and
     * other settings.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after the selected cell range has been
     *  sorted.
     */
    function sortCellRange(sortRange, tableModel, sortVert, sortRules, hasHeader) {

        // check the maximum supported sort size according to sorting direction
        var validSize = hasValidSortRangeSize(sortRange, sortVert);
        var promise = validSize ? self.createResolvedPromise() : makeRejected('sort:overflow');

        // generate and apply the operations to sort a table range or a custom cell range
        promise = promise.then(function () {
            return sheetModel.createAndApplyOperations(function (generator) {

                // sort table range via its model
                if (tableModel) {
                    return tableModel.generateSortOperations(generator, sortRules);
                }

                // reduce sort range by header row/column
                if (hasHeader) {
                    sortRange = sortRange.clone();
                    sortRange.a1.move(1, !sortVert);
                }

                // generate sort operations via cell collection
                return cellCollection.generateSortOperations(generator, sortRange, sortVert, sortRules);

            }, { applyImmediately: true, storeSelection: true });
        });

        return promise;
    }

    /**
     * Sorts the selected cell range immediately (without showing the
     * custom sort dialog) by the column containing the active cell.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected.
     *
     * @param {string} sortOrder
     *  The order to be used for sorting. Possible values are "ascending",
     *  "descending", and "toggle" (use result of getNextSortOrder()).
     *
     * @returns {JPromise}
     *  A promise that will fulfil after the selected cell range has been
     *  sorted.
     */
    function sortBySingleColumn(tableModel, sortOrder) {

        // the effective sorting order (before calling getSortRange() which resets "nextDescending")
        var descending = (sortOrder === 'toggle') ? nextDescending : (sortOrder === 'descending');
        // the sort rule to be used (always sort in the column of the active cell)
        var sortRule = { index: selectionEngine.getActiveCell().c, descending };

        // resolve the effective sorting range, and the involved table model
        var promise = getSortRange(tableModel);

        // sort the cell range by the column containing the active cell
        promise = promise.then(function (settings) {
            return sortCellRange(settings.sortRange, tableModel, true, [sortRule], settings.hasHeader);
        });

        // update cached sorting order for toggle mode
        promise = promise.then(function () { nextDescending = !descending; });

        // notify user about any problems
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows the custom sorting dialog box, and sorts the selected cell
     * range according to the result of the dialog.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after the selected cell range has been
     *  sorted.
     */
    function showSortDialog(tableModel) {

        // resolve the effective sorting range, and the involved table model
        var promise = getSortRange(tableModel);

        // show the custom sort dialog box, sort the cell range according to the result of the dialog
        promise = promise.then(function (settings) {
            var sortRange = settings.sortRange;
            var dialog = new CustomSortDialog(docView, sortRange, tableModel, settings.hasHeader);
            return dialog.show().then(function (result) {
                return sortCellRange(sortRange, tableModel, result.sortVert, result.sortRules, result.hasHeader);
            });
        });

        // reset cached sorting order for toggle mode
        promise = promise.then(function () { nextDescending = false; });

        // notify user about any problems
        return docView.yellOnFailure(promise);
    }

    /**
     * Returns the style identifier of the table style sheet of the passed
     * table model.
     *
     * @param {TableModel|null} tableModel
     *  The model of the table range currently selected.
     *
     * @returns {string}
     *  The style identifier of the table style sheet of the passed table
     *  model; or `null`, if no table mdoel is available.
     */
    function getTableStyleId(tableModel) {
        return (tableModel && tableModel.getStyleId()) || '';
    }

    /**
     * Generates and applies the operations to change the table style sheet
     * of ALL table ranges overlapping with the selected cell range.
     *
     * @param {string} styleId
     *  The style identifier of the table style sheet to be applied for all
     *  selected table ranges.
     */
    function setTableStyleId(styleId) {

        // ensure unlocked sheet (no filter manipulation in locked sheets)
        var promise = docView.ensureUnlockedSheet();

        // create and apply the operations for all selected tables
        promise = promise.then(function () {
            return sheetModel.buildOperations(function (sheetCache) {
                var ranges = selectionEngine.getSelectedRanges();
                var iterator = tableCollection.yieldTableModels(); // without auto-filter
                styleId = styleId || null; // use value null to clear table style
                return self.asyncForEach(iterator, function (tableModel) {
                    if ((tableModel.getStyleId() !== styleId) && ranges.overlaps(tableModel.getRange())) {
                        tableModel.generateChangeOperations(sheetCache, { styleId });
                    }
                });
            }, { storeSelection: true });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Generates and applies the operations needed to modify the specified
     * table column.
     *
     * @param {string} tableName
     *  The unique identifier of a table range.
     *
     * @param {number} tableCol
     *  The relative index of the table column.
     *
     * @param {object} attrSet
     *  The (incomplete) attribute set to be applied at the table column.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after the table column has been modified
     *  successfully.
     */
    function setTableColumnAttributes(tableName, tableCol, attrSet) {

        // ensure unlocked sheet (no filter manipulation in locked sheets)
        var promise = docView.ensureUnlockedSheet();

        // create and apply the operations for the specified table
        promise = promise.then(function () {

            // get the specified table range
            var tableModel = tableCollection.getTableModel(tableName);
            if (!tableModel) { return jpromise.reject(); }

            // the data range of the table (may be null)
            var dataRange = tableModel.getDataRange();
            // the sorting attributes
            var sortAttrs = attrSet?.sort;
            // whether some sort attributes have been changed
            var sortAttrsChanged = false;

            // check whether sort attributes will change
            if (sortAttrs) {
                var columnModel = tableModel.getColumnModel(tableCol);
                var oldSortAttrs = columnModel.getMergedAttributeSet(true).sort;
                sortAttrsChanged = _.some(sortAttrs, function (value, name) {
                    return oldSortAttrs[name] !== value;
                });
            }

            // bug 56313: check for merged ranges before generating the operations
            if (sortAttrsChanged && dataRange && mergeCollection.coversAnyMergedRange(dataRange)) {
                return makeRejected('sort:merge:overlap');
            }

            // create and apply the operations for the specified table
            return sheetModel.createAndApplyOperations(function (generator) {
                // if changing sort settings of a column, reset the sort settings of all other columns
                var resetSort = sortAttrsChanged && (sortAttrs.type !== 'none');
                // add new settings to current column
                return tableModel.generateChangeColumnOperations(generator, tableCol, attrSet, { resetSort });
            }, { applyImmediately: true, storeSelection: true });
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        // returns the model instance of the table range containing the active cell
        'sheet/table/selected': {
            parent: 'sheet/operation/unlocked/cell',
            get() {
                return tableCollection.getTableModelAt(selectionEngine.getActiveCell());
            }
        },

        // returns the model instance of the table range containing the
        // active cell but excludes the auto-filter range ("real tables" only)
        'sheet/table/selected/real': {
            parent: 'sheet/table/selected',
            get(tableModel) {
                // ignore auto-filter range
                return (tableModel && !tableModel.isAutoFilter()) ? tableModel : null;
            }
        },

        // returns the model instance of the "active table" (either the selected
        // table range, or an existing auto-filter regardless of the selection)
        'sheet/table/active': {
            parent: 'sheet/table/selected',
            get(tableModel) {
                return tableModel || tableCollection.getAutoFilterModel();
            }
        },

        // the style flags of the table range
        'table/flags': {
            parent: 'sheet/table/selected/real',
            get(tableModel) {
                return tableModel ? tableModel.getStyleFlags() : null;
            }
        },

        // toggles the table filter, if a table is selected in the active
        // sheet, otherwise toggles the auto-filter of the active sheet
        'table/filter': {
            parent: 'sheet/table/active',
            // TODO: filtering real tables has been disabled, until OOXML filter supports import/export of filter settings
            enable(tableModel) {
                return !tableModel || tableModel.isAutoFilter();
            },
            get(tableModel) {
                return !!tableModel && tableModel.isAutoFilter();
            },
            set(state) {
                return filterTable(this.parentValue, state);
            }
        },

        // refreshes the filter and sorting of the selected table range
        'table/refresh': {
            parent: 'sheet/table/active',
            enable(tableModel) {
                return !!tableModel && tableModel.isRefreshable();
            },
            set() {
                return refreshTable(this.parentValue);
            }
        },

        // active sheet must be unlocked (regardless of locked state of selection)
        'table/sort/enabled': {
            parent: 'sheet/table/selected',
            enable: isSortEnabled
        },

        'table/sort': {
            parent: 'table/sort/enabled',
            get: getNextSortOrder,
            set(sortOrder) {
                return sortBySingleColumn(this.parentValue, sortOrder);
            }
        },

        'table/sort/dialog': {
            parent: 'table/sort/enabled',
            set() {
                return showSortDialog(this.parentValue);
            },
            preserveFocus: true
        },

        // getter returns the style sheet identifier of the active table (table containing the
        // active cell; empty string: no style);
        // setter will change the style sheet of ALL tables overlapping with the cell selection
        'table/stylesheet': {
            parent: 'sheet/table/selected/real',
            enable(tableModel) { return !!tableModel; },
            get: getTableStyleId,
            set: setTableStyleId
        },

        // Applies formatting attributes for the specified table column. Setter expects an object
        // with the properties 'tableName', 'tableCol', and 'attributes' (incomplete attribute set
        // with filter and sorting attributes).
        'table/column/attributes': {
            parent: 'sheet/operation/unlocked/cell', // this item does not depend on selected table!
            set(data) {
                return setTableColumnAttributes(data.tableName, data.tableCol, data.attributes);
            }
        }
    });

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
        colCollection = docView.colCollection;
        rowCollection = docView.rowCollection;
        cellCollection = docView.cellCollection;
        mergeCollection = docView.mergeCollection;
        tableCollection = docView.tableCollection;
    });
}
