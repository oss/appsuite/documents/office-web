/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, fun } from '@/io.ox/office/tk/algorithms';
import { convertHmmToLength, convertLengthToHmm } from '@/io.ox/office/tk/dom';

import { isVisibleBorder, mixBorders } from '@/io.ox/office/editframework/utils/mixedborder';
import { EditController } from '@/io.ox/office/editframework/app/editcontroller';

import { getPresetStyleForBorder, getBorderForPresetStyle } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import ViewMixin from '@/io.ox/office/spreadsheet/controller/viewmixin';
import SearchMixin from '@/io.ox/office/spreadsheet/controller/searchmixin';
import SheetMixin from '@/io.ox/office/spreadsheet/controller/sheetmixin';
import CellMixin from '@/io.ox/office/spreadsheet/controller/cellmixin';
import NameMixin from '@/io.ox/office/spreadsheet/controller/namemixin';
import TableMixin from '@/io.ox/office/spreadsheet/controller/tablemixin';
import DrawingMixin from '@/io.ox/office/spreadsheet/controller/drawingmixin';
import ChartMixin from '@/io.ox/office/spreadsheet/controller/chartmixin';
import NoteMixin from '@/io.ox/office/spreadsheet/controller/notemixin';
import CommentMixin from '@/io.ox/office/spreadsheet/controller/commentmixin';

// class SpreadsheetController ================================================

/**
 * The controller of a spreadsheet application.
 */
export class SpreadsheetController extends EditController {

    constructor(docApp, docModel, docView) {

        // base constructor
        super(docApp, docModel, docView);

        const { selectionEngine } = docView;

        // mix-ins
        ViewMixin.call(this, docView);
        SearchMixin.call(this, docView);
        SheetMixin.call(this, docView);
        CellMixin.call(this, docView);
        NameMixin.call(this, docView);
        TableMixin.call(this, docView);
        DrawingMixin.call(this, docView);
        ChartMixin.call(this, docView);
        NoteMixin.call(this, docView);
        CommentMixin.call(this, docView);

        // private methods ----------------------------------------------------

        /**
         * Returns the merged formatting attributes of the current selection.
         * According to the current selection mode and text edit mode, the
         * following attributes will be returned:
         *  - Cell selection mode: The attribute set of the active cell.
         *  - Cell edit mode: The attribute set of the edited cell, including
         *    pending edit attributes.
         *  - Drawing selection mode: The mixed attribute set of all selected
         *    drawing objects with text contents.
         *  - Drawing edit mode: The mixed attribute set of the current text
         *    selection in the edited drawing object.
         *
         * @returns {Dict}
         *  The merged formatting attributes of the current selection.
         */
        function getActiveAttributeSet() {

            // return the formatting attributes of the active text editor
            var textEditor = docView.getActiveTextEditor();
            if (textEditor) { return textEditor.getAttributeSet() || {}; }

            // selected drawings: return the mixed attributes of the text framework selection
            if (selectionEngine.hasDrawingSelection()) {
                return docView.getTextAttributeSet();
            }

            // otherwise, return attribute set of the active cell
            return docView.cellCollection.getAttributeSet(selectionEngine.getActiveCell());
        }

        /**
         * Changes the formatting attributes of the current selection.
         * According to the current selection mode and text edit mode, the
         * following attributes will be changed:
         *  - Cell selection mode: All selected cells.
         *  - Cell edit mode: The pending edit attributes of the edited cell.
         *  - Drawing selection mode: All selected drawing objects with text
         *    contents.
         *  - Drawing edit mode: The current text selection in the edited
         *    drawing object.
         *
         * @param {Dict} attrSet
         *  An incomplete attribute set that will be applied to the current
         *  selection.
         *
         * @returns {JPromise<void>}
         *  A promise that will fulfil when the passed attributes have been
         *  applied successfully.
         */
        function setAttributeSet(attrSet) {

            // send the formatting attributes to the active text editor
            var textEditor = docView.getActiveTextEditor();
            if (textEditor) { return textEditor.setAttributeSet(attrSet); }

            // selected drawings: send the attributes to the text framework
            if (selectionEngine.hasDrawingSelection()) {
                return docView.setTextAttributeSet(attrSet);
            }

            // otherwise, fill the entire cell selection with the attributes
            return docView.fillCells({ a: attrSet });
        }

        /**
         * Changes the character formatting of the current selection. See
         * method `setAttributeSet()` for details.
         *
         * @param {Dict} charAttrs
         *  The character attributes (simple key/value map) that will be
         *  applied to the current selection.
         *
         * @returns {JPromise<void>}
         *  A promise that will fulfil when the passed attributes have been
         *  applied successfully.
         */
        function setCharacterAttributes(charAttrs) {
            return setAttributeSet({ character: charAttrs });
        }

        /**
         * Returns the list style identifier for the passed mixed paragraph
         * attributes.
         *
         * @param {string} listType
         *  The list type (either "bullet" or "numbering").
         *
         * @param {Dict} paraAttrs
         *  The mixed paragraph attributes.
         *
         * @returns {string|null}
         *  The list style identifier for the passed paragraph attributes.
         */
        function getListStyleId(listType, paraAttrs) {
            return is.empty(paraAttrs) ? '' : docModel.getEffectiveListStyleId(listType, paraAttrs);
        }

        /**
         * Handler for modal dialogs. Automatically closes a modal dialog, if
         * the active sheet vanishes (deleted or hidden).
         */
        function showDialogHandler(dialog, config) {

            // do not handle generic dialogs
            const autoCloseScope = config?.autoCloseScope;
            if (!autoCloseScope) { return; }

            // local copy of the *current* state to be used later inside event handlers
            const activeModel = docView.sheetModel;
            let activeCell = selectionEngine.getActiveCell();

            // close the dialog when the active sheet will be deleted
            dialog.listenTo(docModel, 'delete:sheet:before', event => {
                if (activeModel === event.sheetModel) { dialog.close(); }
            });

            // close the dialog when the active sheet will be hidden
            dialog.listenTo(activeModel, 'change:attributes', () => {
                if (!activeModel.isVisible()) { dialog.close(); }
            });

            // active cell handling
            if (autoCloseScope === 'activeCell') {
                var cellCollection = activeModel.cellCollection;

                // close dialog if the active cell will be hidden
                dialog.listenTo(activeModel, 'change:columns change:rows', () => {
                    if (!cellCollection.isVisibleCell(activeCell)) { dialog.close(); }
                });

                // close dialog if the active cell will be deleted
                dialog.listenTo(cellCollection, "move:cells", event => {
                    activeCell = event.transformer.transformAddress(activeCell);
                    if (!activeCell) { dialog.close(); }
                });
            }
        }

        // item registration --------------------------------------------------

        // register all controller items not implemented in a plugin
        this.registerItemsOnSuccess({

            // selection mode -------------------------------------------------

            // enabled if the document is editable, and cell selection mode is active (no drawing objects are selected)
            'document/editable/cell': {
                parent: ['document/editable/worksheet', 'view/selection/cell']
            },

            // enabled if the document is in read-only state, and cell selection mode is active (no drawing objects are selected)
            'document/readonly/cell': {
                parent: ['document/readonly/worksheet', 'view/selection/cell']
            },

            // enabled if the document is editable, and at least one drawing object is selected;
            // item value is the drawing selection (array of drawing positions)
            'document/editable/drawing': {
                parent: ['document/editable/worksheet', 'view/selection/drawing'],
                get() { return selectionEngine.getSelectedDrawings(); }
            },

            // sheet operations -----------------------------------------------

            // Parent item for various sheet-level operations in the active sheet, enabled if:
            // - document is editable,
            // - cell selection (no drawing selection),
            // - text edit mode is not active.
            // Note that the active sheet may be locked.
            'sheet/operation/cell': {
                parent: ['document/editable/cell', 'view/editmode/off']
            },

            // Parent item for various sheet-level operations in the active sheet. Works similar
            // to 'sheet/operation/cell', but the active sheet must be unlocked to enable this item.
            'sheet/operation/unlocked/cell': {
                parent: 'sheet/operation/cell',
                enable() { return !docView.isSheetLocked(); }
            },

            // hyperlink operations -------------------------------------------

            'hyperlink/edit/dialog': {
                parent: 'sheet/operation/unlocked/cell',
                enable() { return selectionEngine.isSingleRangeSelection(); },
                set() { return docView.editHyperlink(); }
            },

            'hyperlink/delete': {
                parent: 'sheet/operation/unlocked/cell',
                enable() { return is.string(docView.getCellURL()); },
                set() { return docView.deleteHyperlink(); }
            },

            // column operations ----------------------------------------------

            // parent item to get the mixed column attributes
            'column/attributes/mixed': {
                get() { return docView.getColumnAttributes(); }
            },

            // parent item to get the column settings of the active cell
            'column/active': {
                get() { return docView.getActiveColumnDescriptor(); }
            },

            'column/insert': {
                parent: 'sheet/operation/unlocked/cell',
                set() { return docView.insertColumns(); }
            },

            'column/delete': {
                parent: 'sheet/operation/unlocked/cell',
                set() { return docView.deleteColumns(); }
            },

            'column/hide': {
                parent: ['column/attributes/mixed', 'sheet/operation/unlocked/cell'],
                // enabled when any visible columns are selected
                enable(attributes) { return attributes.visible !== false; },
                set() { return docView.toggleColumns(false); }
            },

            'column/show': {
                parent: 'sheet/operation/unlocked/cell',
                enable() { return docView.canShowColumns(); },
                set() { return docView.toggleColumns(true); }
            },

            'column/width/optimal': {
                parent: 'column/hide', // only enabled when any visible columns (that CAN BE hidden) are selected
                set() { return docView.setOptimalColumnWidth(); }
            },

            'column/width/active': {
                parent: ['column/active', 'sheet/operation/unlocked/cell'],
                get(colDesc) { return colDesc.sizeHmm; },
                set(value) { return docView.setColumnWidth(value); }
            },

            // row operations -------------------------------------------------

            // parent item to get the mixed row attributes
            'row/attributes/mixed': {
                get() { return docView.getRowAttributes(); }
            },

            // parent item to get the row settings of the active cell
            'row/active': {
                get() { return docView.getActiveRowDescriptor(); }
            },

            'row/insert': {
                parent: 'sheet/operation/unlocked/cell',
                set() { return docView.insertRows(); }
            },

            'row/delete': {
                parent: 'sheet/operation/unlocked/cell',
                set() { return docView.deleteRows(); }
            },

            'row/hide': {
                parent: ['row/attributes/mixed', 'sheet/operation/unlocked/cell'],
                // enabled when any visible rows are selected
                enable(attributes) { return attributes.visible !== false; },
                set() { return docView.toggleRows(false); }
            },

            'row/show': {
                parent: 'sheet/operation/unlocked/cell',
                enable() { return docView.canShowRows(); },
                set() { return docView.toggleRows(true); }
            },

            'row/height/optimal': {
                parent: 'row/hide', // only enabled when any visible rows (that CAN BE hidden) are selected
                set() { return docView.setOptimalRowHeight(); }
            },

            'row/height/active': {
                parent: ['row/active', 'sheet/operation/unlocked/cell'],
                get(rowDesc) { return rowDesc.sizeHmm; },
                set(value) { return docView.setRowHeight(value); }
            },

            // cell operations on active cell ---------------------------------

            // inserts ready-to-use cell contents into the active cell
            'cell/active/contents': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                set(changeSet) { return docView.changeCell(changeSet); }
            },

            // inserts some text (to be parsed) into the active cell
            'cell/active/parse': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                set(parseText) { return docView.parseCellText(parseText); }
            },

            // cell operations on selection -----------------------------------

            'cell/fill': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                set(changeSet) { return docView.fillCells(changeSet); }
            },

            'cell/clear/values': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                set() { return docView.fillCells({ v: null, f: null }); }
            },

            'cell/clear/all': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                set() { return docView.clearCells(); }
            },

            'cell/merge': {
                parent: 'sheet/operation/unlocked/cell',
                enable() { return selectionEngine.hasAnyRangeSelected(); },
                get() { return selectionEngine.hasMergedRangeSelected(); },
                set(mergeMode) { return docView.mergeRanges(mergeMode); }
            },

            'cell/autofill': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                set(fillData) { return docView.autoFill(fillData.direction, fillData.count, fillData.options); }
            },

            'cell/autoformula': {
                parent: 'document/editable/cell', // can be used in locked sheets
                enable() { return selectionEngine.isSingleRangeSelection(); },
                set(funcName) { return docView.insertAutoFormula(funcName); }
            },

            // formatting attributes ------------------------------------------

            // Parent iten for the attribute set of the current selection:
            // (1) Cell selection mode: The attribute set of the active cell.
            // (2) Cell edit mode: The attribute set of the edited cell (including pending edit attributes).
            // (3) Drawing selection mode: The mixed attribute set of all drawing objects with text contents.
            // (4) Drawing edit mode: The mixed attribute set of the text selection in the edited drawing object.
            'document/attributes': {
                parent: 'document/editable/worksheet',
                enable() { return this.value !== null; },
                get: getActiveAttributeSet
            },

            // cell attributes ------------------------------------------------

            'cell/attributes': {
                parent: 'document/editable/cell',
                get() { return docView.cellCollection.getAttributeSet(selectionEngine.getActiveCell()); },
                set(attributes) { return docView.fillCells({ a: attributes }); }
            },

            'cell/stylesheet': {
                parent: ['cell/attributes', 'view/editmode/off'],
                get(attributes) { return attributes.styleId; },
                set(styleId) { return docView.fillCells({ a: { styleId } }); }
            },

            // reset all explicit attributes in the selection (item key predetermined by text framework!)
            'character/reset': {
                parent: ['cell/attributes', 'view/editmode/off'],
                set() { return docView.fillCells({ s: '' }); }
            },

            // cell attributes (family 'cell') ----------------------------

            'cell/attributes/cell': {
                parent: ['cell/attributes', 'view/editmode/off'],
                get(attributes) { return attributes.cell || {}; }
            },

            'cell/fillcolor': {
                parent: 'cell/attributes/cell',
                get(attributes) { return attributes.fillColor; },
                set(color) { return docView.setCellAttributes({ fillType: 'solid', fillColor: color }); }
            },

            'cell/linebreak': {
                parent: 'cell/attributes/cell',
                get(attributes) { return attributes.wrapText; },
                set(wrap) { return docView.setCellAttributes({ wrapText: wrap }); }
            },

            'cell/unlocked': {
                parent: 'cell/attributes/cell',
                enable(attributes) { return !attributes.unlocked && !docView.isSheetLocked(); },
                set() { return docView.setCellAttributes({ unlocked: true }); }
            },

            'cell/locked': {
                parent: 'cell/attributes/cell',
                enable(attributes) { return attributes.unlocked && !docView.isSheetLocked(); },
                set() { return docView.setCellAttributes({ unlocked: false }); }
            },

            'cell/unhideformula': {
                parent: 'cell/attributes/cell',
                enable(attributes) { return attributes.hidden && !docView.isSheetLocked(); },
                set() { return docView.setCellAttributes({ hidden: false }); }
            },

            'cell/hideformula': {
                parent: 'cell/attributes/cell',
                enable(attributes) { return !attributes.hidden && !docView.isSheetLocked(); },
                set() { return docView.setCellAttributes({ hidden: true }); }
            },

            'cell/alignhor': {
                parent: 'cell/attributes/cell',
                get(attributes) { return attributes.alignHor; },
                set(alignment) { return docView.setCellAttributes({ alignHor: alignment }); }
            },

            'cell/alignvert': {
                parent: 'cell/attributes/cell',
                get(attributes) { return attributes.alignVert; },
                set(alignment) { return docView.setCellAttributes({ alignVert: alignment }); }
            },

            // border flags, as flag set with single-letter border keys
            'cell/border/flags': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                get() { return docView.getBorderFlags(); },
                set(borderFlags) { return docView.setBorderFlags(borderFlags); }
            },

            // all mixed border attributes of the selection
            'cell/border/attributes': {
                parent: 'document/editable/cell',
                get() { return docView.getBorderAttributes(); }
            },

            // enabled if at least one border in the selection is visible
            'cell/border/visible': {
                parent: 'cell/border/attributes',
                enable(borderAttributes) { return _.any(borderAttributes, isVisibleBorder); }
            },

            // a single mixed border for all borders in the selection
            'cell/border/value': {
                parent: ['cell/border/visible', 'view/editmode/off'],
                get(borderAttributes) { return mixBorders(_.values(borderAttributes)); }
            },

            'cell/border/style': {
                parent: 'cell/border/value',
                get(border) { return border.style; },
                set(value) { return docView.changeVisibleBorders({ style: value }); }
            },

            // width of the border lines, in points (rounded to 1/2 points)
            'cell/border/width': {
                parent: 'cell/border/value',
                get(border) { return !isVisibleBorder(border) ? 0 : is.number(border.width) ? convertHmmToLength(border.width, 'pt', 0.5) : null; },
                set(value) { return docView.changeVisibleBorders({ width: convertLengthToHmm(value, 'pt') }); }
            },

            'cell/border/color': {
                parent: 'cell/border/value',
                get(border) { return border.color; },
                set(value) { return docView.changeVisibleBorders({ color: value }); }
            },

            // combined style and width as enumeration as used by Excel
            'cell/border/style/preset': {
                parent: 'cell/border/value',
                get(border) { return getPresetStyleForBorder(border); },
                set(style) { return docView.changeVisibleBorders(getBorderForPresetStyle(style)); }
            },

            'cell/numberformat/category': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                get() { return docView.getNumberFormatCategory(); },
                set(category) { return docView.setNumberFormatCategory(category); },
                shortcut: [
                    { keyCode: '1', shift: true, ctrlOrMeta: true, value: 'number',     scope: '.app-pane' },
                    { keyCode: '2', shift: true, ctrlOrMeta: true, value: 'scientific', scope: '.app-pane' },
                    { keyCode: '3', shift: true, ctrlOrMeta: true, value: 'date',       scope: '.app-pane' },
                    { keyCode: '4', shift: true, ctrlOrMeta: true, value: 'currency',   scope: '.app-pane' },
                    { keyCode: '5', shift: true, ctrlOrMeta: true, value: 'percent',    scope: '.app-pane' },
                    { keyCode: '6', shift: true, ctrlOrMeta: true, value: 'standard',   scope: '.app-pane' }
                ]
            },

            'cell/numberformat/code': {
                parent: 'cell/numberformat/category',
                enable(category) { return !/^(standard|custom)$/.test(category); },
                get() { return docView.getNumberFormat(); },
                set(format) { return docView.setNumberFormat(format); }
            },

            'cell/numberformat/decimaldecrease': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                enable() { return docView.isChangeDecimalPlaces(false); },
                set() { docView.setChangeDecimalPlaces(false); }
            },

            'cell/numberformat/decimalincrease': {
                parent: 'sheet/operation/cell', // can be used in locked sheets
                enable() { return docView.isChangeDecimalPlaces(true); },
                set() { docView.setChangeDecimalPlaces(true); }
            },

            // cell attributes (family 'character') ---------------------------

            'document/attributes/character': {
                parent: 'document/attributes',
                get(attributes) { return attributes.character || {}; }
            },

            'character/bold': {
                parent: 'document/attributes/character',
                get(attributes) { return attributes.bold; },
                set(state) { return setCharacterAttributes({ bold: state }); },
                shortcut: { keyCode: 'B', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },

            'character/italic': {
                parent: 'document/attributes/character',
                get(attributes) { return attributes.italic; },
                set(state) { return setCharacterAttributes({ italic: state }); },
                shortcut: { keyCode: 'I', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },

            'character/underline': {
                parent: 'document/attributes/character',
                get(attributes) { return attributes.underline; },
                set(state) { return setCharacterAttributes({ underline: state }); },
                shortcut: { keyCode: 'U', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },

            'character/strike': {
                parent: 'document/attributes/character',
                get(attributes) { return is.string(attributes.strike) ? (attributes.strike !== 'none') : null; },
                set(state) { return setCharacterAttributes({ strike: state ? 'single' : 'none' }); }
            },

            'character/color': {
                parent: 'document/attributes/character',
                get(attributes) { return attributes.color; },
                set(color) {
                    if (color && color.type === 'auto' && !docApp.isODF() && docModel.getSelection().isAnyDrawingSelection()) { color = docModel.findTextAutoColorForTextframe(); }
                    return setCharacterAttributes({ color });
                }
            },

            'character/fillcolor': {
                parent: 'document/attributes/character',
                enable: fun.false // TODO
            },

            'character/fontname': {
                parent: 'document/attributes/character',
                get(attributes) { return attributes.fontName; },
                set(fontName) { return setCharacterAttributes({ fontName }); }
            },

            'character/fontsize': {
                parent: 'document/attributes/character',
                get(attributes) { return attributes.fontSize; },
                set(fontSize) { return setCharacterAttributes({ fontSize }); }
            },

            'character/vertalign': {
                parent: 'document/attributes/character',
                enable: fun.false // TODO
            },

            // paragraph attributes (family 'paragraph' in shape text) --------

            'document/attributes/paragraphs': {
                parent: ['document/attributes', 'view/selection/drawing'],
                get(attributes) { return attributes.paragraph || {}; }
            },

            'paragraph/alignment': {
                parent: 'document/attributes/paragraphs',
                get(attributes) { return attributes.alignment; },
                set(alignment) { docModel.setAttribute('paragraph', 'alignment', alignment); }
            },

            'paragraph/lineheight': {
                parent: 'document/attributes/paragraphs',
                get(attributes) { return attributes.lineHeight; },
                set(lineHeight) { docModel.setAttribute('paragraph', 'lineHeight', lineHeight); }
            },

            'paragraph/spacing': {
                parent: 'document/attributes/paragraphs',
                get(attributes) { return docModel.getParagraphSpacing(attributes); },
                set(multiplier) { return docModel.setParagraphSpacing(multiplier); }
            },

            // parent for controller items for bullet/numbered lists
            'paragraph/list/enabled': {
                parent: ['document/attributes/paragraphs', 'document/ooxml']
            },

            // toggle default bullet list, or select bullet type
            'paragraph/list/bullet': {
                parent: 'paragraph/list/enabled',
                get(paraAttrs) { return getListStyleId('bullet', paraAttrs); },
                set(value) { docModel.setListStyleId('bullet', value, this.value); }
            },

            // toggle default numbered list, or select numbering type
            'paragraph/list/numbered': {
                parent: 'paragraph/list/enabled',
                get(paraAttrs) { return getListStyleId('numbering', paraAttrs); },
                set(value) { docModel.setListStyleId('numbering', value, this.value); }
            },

            // list indentation level
            'paragraph/list/indent': {
                parent: 'paragraph/list/enabled',
                get(paraAttrs) { return paraAttrs.level || 0; }
            },

            // increase list level by one
            'paragraph/list/incindent': {
                parent: 'paragraph/list/indent',
                enable(paraAttrs) { return docModel.isListIndentChangeable(paraAttrs, { increase: true }); },
                set() { docModel.changeListIndent({ increase: true, validatedLevel: true }); }
            },

            // decrease list level by one
            'paragraph/list/decindent': {
                parent: 'paragraph/list/indent',
                enable(paraAttrs) { return docModel.isListIndentChangeable(paraAttrs, { increase: false }); },
                set() { docModel.changeListIndent({ increase: false, validatedLevel: true }); }
            },

            // clipboard ------------------------------------------------------

            'document/copy': {
                // enabled in read-only mode
                parent: 'app/valid',
                enable() {
                    return selectionEngine.hasDrawingSelection() ?
                        selectionEngine.canCopyPasteDrawingSelection() :
                        (selectionEngine.hasAnyRangeSelected() || selectionEngine.isSingleCellSelection());
                },
                set() { return docView.getActiveGridPane().copy(); }
            },

            'document/cut': {
                parent: ['document/copy', 'document/editable/worksheet'],
                set() { return docView.getActiveGridPane().cut(); }
            },

            'document/paste': {
                parent: 'document/editable/worksheet',
                set() { return docView.showClipboardNoticeDialog(); }
            }
        });

        // initialization -----------------------------------------------------

        // update GUI after changed text selection in drawing objects
        this.updateOnEvent(docModel, 'selection');

        // automatically close modal dialogs
        this.listenTo(docView, 'dialog:show', showDialogHandler);
    }
}
