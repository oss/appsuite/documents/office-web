/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { re, fun, itr } from '@/io.ox/office/tk/algorithms';
import { SearchCommand, ReplaceCommand } from '@/io.ox/office/editframework/app/editcontroller';
import { makeRejected, RangeArray } from '@/io.ox/office/spreadsheet/utils/sheetutils';

// mix-in class SearchMixin ===================================================

/**
 * Implementation of the search handler as required by the constructor of
 * the class EditController, intended to be mixed into a document
 * controller instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the current sheet
 *  selection.
 */
export default function SearchMixin(docView) {

    const { selectionEngine } = docView;

    // self reference (the document controller)
    var self = this;

    // the collections of the active sheet
    var cellCollection = null;

    // private methods ----------------------------------------------------

    /**
     * Creates a regular expression for the passed query string and search
     * configuration.
     */
    function createRegExp(query, config, global) {
        var flags = (global ? 'g' : '') + (config.withcase ? '' : 'i');
        // TODO: handle `config.regexp`
        return new RegExp(re.escape(query), flags);
    }

    /**
     * Creates a predicate function that returns whether the cell at the
     * specified address matches the query string.
     *
     * @param {string} query
     *  The query string to be matched.
     *
     * @param {SearchConfig} config
     *  The search configuration options.
     *
     * @returns {(address: Address) => boolean}
     *  A predicate function that returns whether the cell at the specified
     *  address matches the query string.
     */
    function createSearchPredicate(query, config) {

        // String.indexOf('') is always zero
        if (!query) { return fun.false; }

        // regular expression according to configuration
        var regexp = createRegExp(query, config);

        // return the predicate function taking a cell address
        return function (address) {
            return regexp.test(cellCollection.getEditString(address));
        };
    }

    function yieldCells(ranges, selection, options) {
        return cellCollection.cellEntries(ranges, {
            startAddress: selection.address,
            startIndex: selection.active,
            wrap: true,
            type: 'value',
            visible: true,
            skipCovered: true,
            ...options
        });
    }

    /**
     * Selects the next cell provided by the specified cell iterator.
     */
    function selectCell(cellEntry, singleCell, doneMsg) {

        if (!cellEntry) {
            return docView.yellMessage(doneMsg, true);
        }

        var address = cellEntry.address;
        if (singleCell) {
            selectionEngine.selectCell(address);
        } else {
            selectionEngine.changeActiveCell(cellEntry.index, address);
        }
        docView.scrollToCell(address, { preserveFocus: true });
    }

    /**
     * Returns the change descriptor with the replacement text for the
     * specified cell.
     */
    function getReplaceChangeSet(address, query, replace, config) {
        var oldText = cellCollection.getEditString(address);
        var newText = oldText.replace(createRegExp(query, config, true), replace);
        var parseResult = cellCollection.parseCellValue(newText, 'val', address);
        return { address, ..._.pick(parseResult, 'v', 'f', 'format') };
    }

    // protected methods --------------------------------------------------

    /**
     * Implementation for the "Search" command.
     */
    this.executeSearch = function (command, query, config) {

        var reverse = false;
        switch (command) {
            case SearchCommand.CONFIG:  return;
            case SearchCommand.PREVIEW: return;
            case SearchCommand.START:   break;
            case SearchCommand.NEXT:    break;
            case SearchCommand.PREV:    reverse = true; break;
            case SearchCommand.END:     return;
            default: throw new Error('$badge{SearchMixin} executeSearch: unsupported command "' + command + '"');
        }

        // TODO: drawing object selected
        if (docView.isTextEditMode()) { return; }

        // resolve cell ranges to search into
        var selection = selectionEngine.getSelection();
        var singleCell = selectionEngine.isSingleCellSelection();
        var ranges = singleCell ? new RangeArray(cellCollection.getUsedRange()) : selection.ranges;

        // no content available
        if (ranges.empty()) {
            return docView.yellMessage('search:nothing', true);
        }

        // create a search predicate function
        const predicateFn = createSearchPredicate(query, config);

        // find visible value/formula cell that matches the search query
        const cellIt = yieldCells(ranges, selection, { skipStart: true, reverse });
        const cellEntry = itr.find(cellIt, cellEntry => predicateFn(cellEntry.address));

        // select the cell (or show a "done" alert)
        selectCell(cellEntry, singleCell, 'search:nothing');
    };

    /**
     * Implementation for "Replace" command.
     */
    this.executeReplace = function (command, query, replace, config) {

        // whether to replace all occurrences of the search text at once
        var replaceAll = fun.do(function () {
            switch (command) {
                case ReplaceCommand.NEXT: return false;
                case ReplaceCommand.ALL:  return true;
            }
            throw new Error('$badge{SearchMixin} executeReplace: unsupported command "' + command + '"');
        });

        // no replace functionality in locked cells (TODO: replace in unlocked cells?);
        // additional checks for search query and replacement text
        var promise = docView.ensureUnlockedSheet().then(function () {
            if ((query === '') || (query === replace)) {
                return makeRejected('replace:nothing');
            }
        });

        // replace the next occurrence, or all occurrences, according to the passed options
        promise = promise.then(function () {

            // resolve cell ranges to search into
            var selection = selectionEngine.getSelection();
            var singleCell = selectionEngine.isSingleCellSelection();
            var ranges = singleCell ? new RangeArray(cellCollection.getUsedRange()) : selection.ranges;

            // create a search predicate function
            var predicateFn = createSearchPredicate(query, config);

            // search the next cell if the current cell value does not match the query
            if (!replaceAll && !predicateFn(selectionEngine.getActiveCell())) {
                return self.executeSearch(SearchCommand.NEXT, query, config);
            }

            // visit all visible value cells and formula cells, filter for cells that match the search query
            let cellIt = yieldCells(ranges, selection);
            cellIt = itr.filter(cellIt, cellEntry => predicateFn(cellEntry.address));

            // replace all occurrences of the search text at once
            if (replaceAll) {

                // generate the changesets for all matching cells
                const promise2 = self.asyncReduce(cellIt, [], (changeSets, cellEntry) => {
                    changeSets.push(getReplaceChangeSet(cellEntry.address, query, replace, config));
                });

                // generate the cell operations
                return promise2.then(changeSets => docView.changeCells(changeSets));
            }

            // exit silently, if no more search results are available
            var cellEntry = itr.shift(cellIt);
            if (!cellEntry) { return; }

            // generate the cell operations for the next matching cell only
            var changeSet = getReplaceChangeSet(cellEntry.address, query, replace, config);
            var promise3 = docView.changeCell(changeSet, { address: changeSet.address });

            // select the next search result (or show a message, if no more results are available)
            return promise3.done(() => selectCell(itr.shift(cellIt), singleCell, 'search:finished'));
        });

        return docView.yellOnFailure(promise);
    };

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        cellCollection = docView.cellCollection;
    });
}
