/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { SheetSelection } from '@/io.ox/office/spreadsheet/utils/sheetselection';
import { FORMAT_PAINTER_LABEL } from '@/io.ox/office/spreadsheet/view/labels';
import { FunctionDialog } from '@/io.ox/office/spreadsheet/view/dialogs';

// mix-in class CellMixin =====================================================

/**
 * Implementations of all controller items for manipulating cell contents
 * and formatting in the active sheet, intended to be mixed into a document
 * controller instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the cell selection.
 */
export default function CellMixin(docView) {

    const { selectionEngine } = docView;

    // the document model and style collections
    var docModel = docView.docModel;

    // the models of the active sheet
    var sheetModel = null;
    var cellCollection = null;
    var tableCollection = null;

    // private methods ----------------------------------------------------

    /**
     * Returns whether the format painter is currently active.
     *
     * @returns {Boolean}
     *  Whether the format painter is currently active.
     */
    function isFormatPainterActive() {
        return selectionEngine.isCustomSelectionMode('cell/painter');
    }

    /**
     * Activates or deactivates the format painter.
     *
     * @param {Boolean} state
     *  Whether to activate or deactivate the format painter.
     */
    function activateFormatPainter(state) {

        // deactivate format painter: cancel custom selection mode
        if (!state) {
            selectionEngine.cancelCustomSelectionMode();
            return;
        }

        // the address of the active cell (used as formatting source)
        var address = selectionEngine.getActiveCell();
        // fill the auto-style of the active cell into the range that will be selected
        var contents = { s: cellCollection.getStyleId(address) };

        // do not lock editable cells by copying the 'unlocked' attribute (in locked sheets only, as in Excel)
        if (sheetModel.isLocked()) {
            contents.a = { cell: { unlocked: true } };
        }

        // resolve table style attributes from table model covered by the active cell
        const result = tableCollection.getTableModelAt(address)?.resolveCellAttributeSet(address);
        if (result) { contents.table = result.cellAttrSet; }

        // callback handler for `enterCustomSelectionMode`
        const applyFormatPainter = selection => {
            // select the target range
            selectionEngine.setCellSelection(selection);
            // bug 35295: check that the cells are not locked
            if (!docView.ensureUnlockedSelection({ sync: true })) {
                // copy the formatting attributes to the target range
                this.executeItem('cell/fill', contents);
            }
        };

        // start custom selection mode (wait for range selection)
        selectionEngine.enterCustomSelectionMode('cell/painter', {
            applySelected: applyFormatPainter,
            selection: SheetSelection.createFromAddress(selectionEngine.getActiveCell()),
            statusLabel: FORMAT_PAINTER_LABEL
        });
    }

    /**
     * Shows the "Insert Function" dialog, and starts the cell edit mode
     * with a formula containing the function selected in the dialog; or
     * extends the formula expression while in cell edit mode.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when a function has been selected in the
     *  dialog, and the cell edit mode has been started; or that will
     *  reject when canceling the dialog.
     */
    function insertFunctionWithDialog() {

        // the active cell must be editable
        var promise = docView.ensureUnlockedActiveCell().then(function () {

            // name of a function to be selected in the dialog
            var funcName = null;

            // pick the formula string if available, otherwise the display text
            if (!docView.isTextEditMode('cell')) {
                var address = selectionEngine.getActiveCell();
                var formula = cellCollection.getFormula(address, "ui", { fullMatrix: true });
                var text = formula || cellCollection.getDisplayString(address);
                // try to extract a function name
                if (text) {
                    var result = text.match(/^=?\s*([^(]+)/);
                    funcName = result ? result[1] : null;
                }
            }

            // show the dialog, wait for the result
            return new FunctionDialog(docView, funcName).show();
        });

        // start cell edit mode, insert the selected function as formula into the active cell
        promise = promise.then(function (funcKey) {

            // create a formula expression with the function and an empty pair of parentheses
            var expr = docModel.formulaGrammarUI.getFunctionName(funcKey) + '()';

            // inject function into cell text editor if available
            const textEditor = docView.getActiveTextEditor('cell');
            if (textEditor?.hasFormulaExpression()) {
                textEditor.insertText(expr, -1);
                return;
            }

            // cancel current cell edit mode, and restart edit mode
            return docView.enterTextEditMode('cell', { text: '=' + expr, pos: -1, restart: true });
        });

        // show warning alerts if necessary
        return docView.yellOnFailure(promise);
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        'cell/painter': {
            parent: 'sheet/operation/cell', // can be used in locked sheets
            get: isFormatPainterActive,
            set: activateFormatPainter.bind(this)
        },

        'function/insert/dialog': {
            parent: 'document/editable/cell', // can be used in locked sheets
            set: insertFunctionWithDialog
        }
    });

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
        cellCollection = sheetModel.cellCollection;
        tableCollection = sheetModel.tableCollection;
    });
}
