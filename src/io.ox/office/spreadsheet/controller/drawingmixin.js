/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import { math, fun, ary, dict, jpromise } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE, Rectangle, convertLengthToHmm } from '@/io.ox/office/tk/dom';

import { Color } from '@/io.ox/office/editframework/utils/color';
import {
    createDefaultShapeAttributeSet, createDefaultSizeForShapeId, getMixedAttributeValue, getMixedBorderColor, getMixedFillColor,
    getMixedPresetBorder, getParagraphAttrsForDefaultShape, hasSwappedDimensions, hasVisibleBorder, resolveModelsDeeply, resolvePresetBorder
} from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { applyImageTransformation, getFileUrl } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { drawPreviewShape } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { PARA_INSERT } from '@/io.ox/office/spreadsheet/utils/operations';
import { AnchorMode, makeRejected } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { getTextFrame } from '@/io.ox/office/spreadsheet/model/drawing/text/textframeutils';
import {
    DELETE_CONTENTS_TITLE, DELETE_CONTENTS_QUERY,
    isPresetConnectorId, getPresetShape, getPresetAspectRatio, getDrawingTypeLabel
} from '@/io.ox/office/spreadsheet/view/labels';
import { showInsertImageDialog } from '@/io.ox/office/spreadsheet/view/dialogs';

// constants ==================================================================

// maximum size of an image in any direction, in 1/100 mm
const MAX_IMAGE_SIZE = 21000;

// private global functions ===================================================

/**
 * Returns whether the passed drawing model can be flipped and rotated.
 *
 * @param {DrawingModel} drawingModel
 *  The drawing model to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed drawing model can be flipped and rotated.
 */
function isRotatableModel(drawingModel) {
    return drawingModel.isDeepRotatable();
}

/**
 * Returns whether the passed drawing model supports line formatting.
 *
 * @param {DrawingModel} drawingModel
 *  The drawing model to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed drawing model supports line formatting.
 */
function isLineFormatModel(drawingModel) {
    // TODO: allow changing the border of chart objects
    return drawingModel.supportsFamily('line') && (drawingModel.drawingType !== 'chart');
}

/**
 * Returns whether the passed drawing model supports fill formatting.
 *
 * @param {DrawingModel} drawingModel
 *  The drawing model to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed drawing model supports fill formatting.
 */
function isFillFormatModel(drawingModel) {
    // TODO: allow changing the background of chart objects
    return drawingModel.supportsFamily('fill') && !isConnectorModel(drawingModel) && (drawingModel.drawingType !== 'chart');
}

/**
 * Returns whether the passed drawing model is a connector shape.
 *
 * @param {DrawingModel} drawingModel
 *  The drawing model to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed drawing model is a connector shape.
 */
function isConnectorModel(drawingModel) {
    var attrs = drawingModel.getExplicitAttributeSet(true);
    return drawingModel.supportsFamily('geometry') && attrs.geometry && isPresetConnectorId(attrs.geometry.presetShape);
}

/**
 * Returns whether the passed drawing model contains a DOM text frame.
 *
 * @param {DrawingModel} drawingModel
 *  The drawing model to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed drawing model contains a DOM text frame.
 */
function isTextFrameModel(drawingModel) {
    return drawingModel.supportsFamily('shape') && !!getTextFrame(drawingModel.getModelFrame());
}

// mix-in class DrawingMixin ==================================================

/**
 * Implementations of all controller items for manipulating drawing objects
 * in the active sheet, intended to be mixed into a document controller
 * instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the current drawing
 *  selection.
 */
export default function DrawingMixin(docView) {

    // self reference
    var self = this;

    const { selectionEngine } = docView;


    // the application instance containing this controller
    var docApp = this.docApp;
    var ooxml = docApp.isOOXML();
    var odf = docApp.isODF();

    // the document model
    var docModel = docView.docModel;

    // the attribute pool for drawing attribute handling
    var attrPool = docModel.defAttrPool;

    // the model and collections of the active sheet
    var sheetModel = null;
    var drawingCollection = null;

    // private methods ----------------------------------------------------

    /**
     * Generates and applies the operations needed to change the formatting
     * attributes for all drawing objects, and shows an error alert box, if
     * this fails.
     *
     * @param {Array<DrawingModel>} drawingModels
     *  An array with all drawing models to be changed.
     *
     * @param {Function|Object} callback
     *  A callback function that will be invoked for each drawing object,
     *  and that must return the attribute set to be applied for that
     *  drawing object. Receives the following parameters:
     *  (1) {DrawingModel} drawingModel
     *      The model of the drawing object to be changed.
     *  (2) {Object} mergedAttrSet
     *      The merged attribute set of the drawing object.
     *  Alternatively, a constant attribute set can be passed instead that
     *  will be applied at all drawing objects.
     *
     * @param {Object} [context]
     *  The calling context for the callback function.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when all operations have been
     *  created and applied successfully.
     */
    function implChangeDrawings(drawingModels, callback, context) {

        // allow to pass a constant attribute set instead of a callback function
        if (typeof callback !== 'function') { callback = fun.const(callback); }

        // build the new attributes for all selected drawing objects
        var drawingDescs = drawingModels.map(function (drawingModel) {

            // the current merged attribute set
            var oldAttrSet = drawingModel.getMergedAttributeSet(true);
            // the (incomplete) attribute set with the new attributes
            var newAttrSet = callback.call(context, drawingModel, oldAttrSet);

            // special adjustments for transformations (rotation and flipping)
            if ('drawing' in newAttrSet) {

                // the old and new drawing attributes (with flipping and rotation)
                var oldAttrs = oldAttrSet.drawing;
                var newAttrs = newAttrSet.drawing;

                // normalize the new rotation angle
                if ('rotation' in newAttrs) {
                    newAttrs.rotation = math.modulo(math.roundp(newAttrs.rotation, 0.1), 360);
                }

                // special anchor adjustment for rotated drawings in OOXML
                if (ooxml && (('flipH' in newAttrs) || ('flipV' in newAttrs) || ('rotation' in newAttrs))) {

                    // the original (unrotated) location of the drawing model (nothing to do for squares)
                    var rectangle = drawingModel.getRectangle();
                    if (rectangle && !rectangle.square()) {

                        // the old and new state of width/height swapping according to the transformation attributes
                        var oldSwappedDims = hasSwappedDimensions(oldAttrs);
                        var newSwappedDims = hasSwappedDimensions({ ...oldAttrs, ...newAttrs });

                        // if the width/height swapping changes due to new rotation angle, add the new anchor attributes
                        if (oldSwappedDims !== newSwappedDims) {
                            if (newSwappedDims) { rectangle.rotateSelf(); }
                            var anchorAttrs = drawingCollection.generateAnchorAttributesForRect(drawingModel, rectangle, { pixel: true });
                            if (anchorAttrs) { attrPool.extendAttrSet(newAttrSet, { drawing: anchorAttrs }); }
                        }
                    }
                }

                // ODF: vertical flipping needs to be simulated as horizontal flip and 180deg rotation
                if (odf && ('flipV' in newAttrs)) {
                    if (newAttrs.flipV !== drawingModel.isFlippedV()) {
                        newAttrs.flipH = ('flipH' in newAttrs) ? !newAttrs.flipH : !drawingModel.isFlippedH();
                        newAttrs.rotation = math.modulo((('rotation' in newAttrs) ? newAttrs.rotation : drawingModel.getRotationDeg()) + 180, 360);
                    }
                    delete newAttrs.flipV;
                }
            }

            return { position: drawingModel.getPosition(), attrs: newAttrSet };
        });

        // change the selected drawing objects
        var promise = self.execChangeDrawings(drawingDescs);

        // show warning messages if needed
        return docView.yellOnFailure(promise);
    }

    // public methods -----------------------------------------------------

    /**
     * Inserts new drawing objects into the active sheet, and selects them
     * afterwards. This method is an implementation helper that does not
     * show any error alerts on failure.
     *
     * @param {object[]} drawingDescs
     *  An array of descriptors with the types and the formatting attribute
     *  sets for the drawing objects to be inserted. Each descriptor MUST
     *  contain the following properties:
     *  - {string} drawingDesc.type -- The type identifier of the drawing
     *    object.
     *  - {Dict} drawingDesc.attrs -- Initial attribute set for the drawing
     *    object, especially the anchor attributes specifying the physical
     *    position of the drawing object in the sheet.
     *
     * @param {Function} [callback]
     *  A callback function that will be invoked everytime after an
     *  "insertDrawing" operation has been generated for a drawing object.
     *  Allows to create additional operations for the drawing objects,
     *  before all these operations will be applied at once. Receives the
     *  following parameters:
     *  1. {SheetOperationGenerator} generator -- The operations generator
     *     (already containing the initial "insertDrawing" operation of the
     *     current drawing).
     *  2. {number} sheet -- The zero-based index of this sheet.
     *  3. {Position} position -- The effective document position of the
     *     current drawing object in the sheet, as inserted into the
     *     "insertDrawing" operation.
     *  4. {object} drawingDesc -- The descriptor element from the array
     *     parameter `drawingDescs` that is currently processed.
     *
     *  The callback function may return a promise to defer processing of
     *  subsequent drawing objects.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all drawing objects have been
     *  created successfully, or that will reject on any error.
     */
    this.execInsertDrawings = function (drawingDescs, callback) {

        // leave text edit mode before inserting a drawing object
        var promise = docView.leaveTextEditMode();

        // check that the drawing objects in the active sheet are not locked
        promise = promise.then(function () {
            return docView.ensureUnlockedDrawings({ errorCode: 'drawing:insert:locked' });
        });

        // generate and apply the drawing operations
        return promise.then(function () {

            // the position of the first new drawing object
            var firstPosition = [drawingCollection.getModelCount()];
            // the positions of all drawing objects to be selected later
            var allPositions = [];

            // selects the new drawing objects
            function storeSelection() {
                selectionEngine.setDrawingSelection(allPositions);
            }

            // generate and apply the document operations
            return sheetModel.createAndApplyOperations(function (generator) {
                return drawingCollection.generateInsertDrawingOperations(generator, firstPosition, drawingDescs, function (generator, sheet, position, drawingDesc) {
                    allPositions.push(position);
                    return callback ? callback.call(self, generator, sheet, position, drawingDesc) : null;
                });
            }, { storeSelection });
        });
    };

    /**
     * Inserts new image objects into the active sheet, and selects them
     * afterwards. This method is an implementation helper that does not
     * show any error alerts on failure.
     *
     * @param {object|object[]} imageDescs
     *  A descriptor for the new image object, containing the properties
     *  `url` and `name`, or an array of such image descriptors to be able
     *  to insert multiple images at once.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the image objects have been created
     *  and inserted into the active sheet, or that will reject on any
     *  error.
     */
    this.execInsertImages = function (imageDescs) {

        // create the image DOM nodes for all images
        var promises = ary.wrap(imageDescs).map(imageDesc => {

            if (!imageDesc) { return self.createRejectedPromise(); } // in case of a server error

            // create the image DOM node, the promise waits for it to load the image data
            var promise = docApp.createImageNode(getFileUrl(docApp, imageDesc.url));

            // fail with empty resolved promise to be able to use $.when() which would
            // otherwise abort immediately if ANY of the promises fails
            return promise.then(imgNode => ({ node: imgNode[0], ...imageDesc }), fun.undef);
        });

        // wait for all images nodes, create and apply the 'insertDrawing' operations
        var promise = $.when(...promises).then(function (...args) {

            // $.when() passes the results as function arguments, filter out invalid images
            var results = args.filter(Boolean);
            if (results.length === 0) { return jpromise.reject(); }

            // the address of teh active cell
            var activeAddress = selectionEngine.getActiveCell();
            // all image nodes collected in a jQuery object
            var imgNodes = $();

            // the drawing attributes to be passed to execInsertDrawings()
            var drawingDescs = results.map(function (result) {

                // collect all image nodes for clean-up
                imgNodes = imgNodes.add(result.node);

                // current size of the image, in 1/100 mm
                var width = convertLengthToHmm(result.node.width, 'px');
                var height = convertLengthToHmm(result.node.height, 'px');

                // restrict size to fixed limit
                if (width > height) {
                    if (width > MAX_IMAGE_SIZE) {
                        height = Math.round(height * MAX_IMAGE_SIZE / width);
                        width = MAX_IMAGE_SIZE;
                    }
                } else {
                    if (height > MAX_IMAGE_SIZE) {
                        width = Math.round(width * MAX_IMAGE_SIZE / height);
                        height = MAX_IMAGE_SIZE;
                    }
                }

                // the generic drawing attributes (anchor position with zoom-independent size in 1/100 mm)
                var drawingAttrs = drawingCollection.getAnchorAttributesForCellAndSize(AnchorMode.ONE_CELL, activeAddress, width, height);
                drawingAttrs.name = result.name;
                drawingAttrs.aspectLocked = true;
                applyImageTransformation(drawingAttrs, result.transform);

                // additional specific image attributes
                var imageAttrs = { imageUrl: result.url };

                // create the drawing descriptor for execInsertDrawings()
                return { type: 'image', attrs: { drawing: drawingAttrs, image: imageAttrs } };
            });

            // destroy the temporary image nodes
            docApp.destroyImageNodes(imgNodes);

            // insert the image objects into the active sheet
            return self.execInsertDrawings(drawingDescs);
        });

        // on any error, return a specific error code
        return promise.catch(function () {
            return makeRejected('image:insert');
        });
    };

    /**
     * Inserts a new shape object into the active sheet, and selects it
     * afterwards. This method is an implementation helper that does not
     * show any error alerts on failure.
     *
     * @param {string} presetId
     *  The identifier of a predefined shape type.
     *
     * @param {Rectangle} rectangle
     *  The location of the shape. If the size of the rectangle is zero,
     *  the default size will be used.
     *
     * @param {object} [options]
     *  Optional parameters for the default formatting attributes. Will be
     *  passed to the method DrawingUtils.createDefaultShapeAttributeSet().
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the shape object has been created
     *  and inserted into the active sheet, or that will reject on any
     *  error.
     */
    this.execInsertShape = function (presetId, rectangle, options) {

        // predefined formatting attributes for the shape object
        var attrSet = createDefaultShapeAttributeSet(docModel, presetId, options);

        // calculate default size, if the passed rectangle is empty
        if (rectangle.area() === 0) {

            // default shape size is 1 inch (convert from 1/100 mm to pixels)
            var defSize = docView.convertHmmToPixel(2450);
            // the effective shape size, according to the shape type (may become portrait or landscape format)
            var shapeSize = createDefaultSizeForShapeId(presetId, defSize, defSize);

            // copy effective shape size into the rectangle
            rectangle = rectangle.clone();
            rectangle.width = shapeSize.width;
            rectangle.height = shapeSize.height;
        }

        // add the anchor position attributes
        var anchorAttrs = drawingCollection.getAnchorAttributesForRect(AnchorMode.TWO_CELL, rectangle, { pixel: true });
        attrPool.extendAttrSet(attrSet, { drawing: anchorAttrs });

        // insert the image objects into the active sheet
        var drawingType = getPresetShape(presetId).type;
        return self.execInsertDrawings([{ type: drawingType, attrs: attrSet }], function (generator, _sheet, position) {
            if (drawingType === 'shape') {
                attrSet = attrPool.extendAttrSet(attrSet, getParagraphAttrsForDefaultShape(docModel), { clone: true });
                generator.generateDrawingOperation(PARA_INSERT, position.concat(0), { attrs: attrSet, noUndo: true });
            }
        });
    };

    /**
     * Deletes the specified drawing objects in the active sheet.
     *
     * @param {Position[]} positions
     *  The document positions of all drawing objects to be deleted.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all drawing objects have been
     *  deleted successfully, or that will reject on any error.
     */
    this.execDeleteDrawings = function (positions) {

        // leave text edit mode before deleting a drawing object
        var promise = docView.leaveTextEditMode();

        // check that the drawing objects in the active sheet are not locked
        promise = promise.then(function () {
            return docView.ensureUnlockedDrawings({ errorCode: 'drawing:delete:locked' });
        });

        // ask user when unsupported drawing objects are going to be deleted
        promise = promise.then(function () {

            // bug 47948: check that no unrestorable drawing objects are selected
            var undoSupported = positions.every(function (position) {
                return drawingCollection.getModel(position).isDeepRestorable();
            });
            // return false to generate the undo operations in the next promise step
            if (undoSupported) { return false; }

            // show query dialog, promise resolves with value true (clear undo stack), or rejects (cancel)
            return docView.showQueryDialog(DELETE_CONTENTS_TITLE, DELETE_CONTENTS_QUERY);
        });

        // generate and apply the drawing operations (back to cell selection before deleting the drawing objects)
        return promise.then(function (unsupported) {
            return sheetModel.createAndApplyOperations(function (generator) {
                selectionEngine.removeDrawingSelection();
                return drawingCollection.generateDeleteDrawingOperations(generator, positions);
            }, { storeSelection: true, undoMode: unsupported ? 'clear' : 'generate' });
        });
    };

    /**
     * Changes the relative order of the specified drawing objects in the
     * active sheet.
     *
     * @param {Position[]} positions
     *  The document positions of all drawing objects to be reordered.
     *
     * @param {DrawingOrderType} orderType
     *  The move direction used to reorder the drawing objects.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all drawing objects have been
     *  reordered successfully, or that will reject on any error.
     */
    this.execReorderDrawings = function (positions, orderType) {

        // leave text edit mode before moving a drawing object (document position changes)
        var promise = docView.leaveTextEditMode();

        // check that the drawing objects in the active sheet are not locked
        promise = promise.then(function () {
            return docView.ensureUnlockedDrawings();
        });

        // generate and apply the drawing operations
        return promise.then(function () {
            return sheetModel.createAndApplyOperations(function (generator) {
                return drawingCollection.generateReorderDrawingOperations(generator, positions, orderType);
            }, { storeSelection: true });
        });
    };

    /**
     * Changes the formatting attributes of the specified drawing objects.
     *
     * @param {object[]} drawingDescs
     *  An array of descriptors with the positions and the formatting
     *  attribute sets for the drawing objects to be changed. Each
     *  descriptor MUST contain the following properties:
     *  - {Position} drawingDesc.position -- The document position of the
     *    drawing object to be changed.
     *  - {Dict} drawingDesc.attrs -- The (incomplete) attribute set to be
     *    applied at the drawing object.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all drawing objects have been
     *  modified successfully, or that will reject on any error.
     */
    this.execChangeDrawings = function (drawingDescs) {

        // check that the drawing objects in the active sheet are not locked
        var promise = docView.ensureUnlockedDrawings();

        // generate and apply the drawing operations
        return promise.then(function () {
            return sheetModel.createAndApplyOperations(function (generator) {
                return drawingCollection.generateChangeDrawingOperations(generator, drawingDescs);
            }, { storeSelection: true });
        });
    };

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        // parent item that is enabled if it is possible to insert drawing objects
        'drawing/insert/enable': {
            parent: 'document/editable/worksheet',
            enable() { return !sheetModel.isLocked(); }
        },

        // dialog to insert an image from file system, from Drive, or by URL
        'image/insert/dialog': {
            parent: 'drawing/insert/enable',
            async set(sourceType) {

                // leave text edit mode before showing the dialog
                await docView.leaveTextEditMode();

                // handler for the dialog result
                const actionHandler = imageDesc => docView.yellOnFailure(self.execInsertImages([imageDesc]));

                // show the "Insert Image" dialog
                await showInsertImageDialog(docView, sourceType, actionHandler, {
                    // close automatically, if active sheet will be locked
                    autoCloseKey: 'image/insert/dialog',
                    // close automatically, if active sheet will be deleted or hidden by another client
                    autoCloseScope: 'activeSheet'
                });
            }
        },

        // insert a shape object (via frame selection mode)
        'shape/insert': {
            parent: 'drawing/insert/enable',
            // return whether frame selection mode is active (as expected by ShapeTypePicker)
            get() {
                return selectionEngine.isFrameSelectionMode('shape/insert');
            },
            set(presetId, settings) {

                // insert a shape directly on touch devices, or when using the keyboard in the GUI control
                if (TOUCH_DEVICE || (settings.sourceType !== 'click')) {
                    // insert a shape object into the active cell, show warning messages if needed
                    var cellRectangle = docView.getCellRectangle(selectionEngine.getActiveCell());
                    var rectangle = new Rectangle(cellRectangle.left, cellRectangle.top, 0, 0);
                    return docView.yellOnFailure(self.execInsertShape(presetId, rectangle));
                }

                // special behavior for line/connector shapes
                var twoPointMode = isPresetConnectorId(presetId);
                // custom aspect ratio for special shapes
                var aspectRatio = getPresetAspectRatio(presetId);

                // returns the flipping options for connector/line shapes
                function getFlippingOptions(frameRect) {
                    return twoPointMode ? { flipH: frameRect.reverseX, flipV: frameRect.reverseY } : null;
                }

                // inserts a shape into the passed rectangle (callback for `enterFrameSelectionMode`)
                function implInsertShape(frameRect) {
                    docView.yellOnFailure(self.execInsertShape(presetId, frameRect, getFlippingOptions(frameRect)));
                }

                // renders the preview shape into the frame node (callback for `enterFrameSelectionMode`)
                function renderPreviewShape(frameNode, frameRect) {
                    var options = { transparent: true, ...getFlippingOptions(frameRect) };
                    drawPreviewShape(docApp, frameNode, frameRect, presetId, options);
                }

                // start frame selection mode
                selectionEngine.enterFrameSelectionMode('shape/insert', {
                    applySelected: implInsertShape,
                    renderPreview: renderPreviewShape,
                    statusLabel: gt('Insert a shape'),
                    aspectRatio,
                    twoPointMode
                });
            }
        },

        // Parent item that is enabled if at least one drawing object is selected.
        // The getter returns an array with all selected drawing models.
        'drawing/models': {
            parent: 'document/editable/drawing',
            get(positions) {
                return positions.map(drawingCollection.getModel, drawingCollection).filter(Boolean);
            }
        },

        // returns the type identifiers of the selected drawing objects as flag set
        'drawing/types': {
            parent: 'drawing/models', // returns the selected drawing models
            get(drawingModels) { return dict.fill(drawingModels.map(model => model.drawingType), true); }
        },

        // enabled if the selected drawing objects contain a shape or a connector
        'drawing/type/shape': {
            parent: 'drawing/types', // the type identifiers of the selected drawing models, as flag set
            enable(typeSet) { return ('shape' in typeSet) || ('connector' in typeSet); }
        },

        // returns the title label for the drawing toolbar, according to the selected drawing objects
        'drawing/type/label': {
            parent: 'drawing/types', // the type identifiers of the selected drawing models, as flag set
            get(typeSet) {
                var mixedType = dict.singleKey(typeSet) ? dict.anyKey(typeSet) : null;
                return getDrawingTypeLabel(mixedType);
            }
        },

        // Parent item that is enabled if at least one drawing object is selected, AND the active
        // sheet is unlocked. The getter returns an array with all selected drawing models.
        'drawing/operation': {
            parent: 'drawing/models',
            enable() { return !sheetModel.isLocked(); }
        },

        'drawing/delete': {
            parent: 'drawing/operation',
            set() {
                // delete the selected drawing objects
                var promise = self.execDeleteDrawings(selectionEngine.getSelectedDrawings());
                // show warning messages if needed
                return docView.yellOnFailure(promise);
            }
        },

        'drawing/order': {
            parent: 'drawing/operation',
            set(orderType) {
                // reorder the specified drawing objects
                var promise = self.execReorderDrawings(selectionEngine.getSelectedDrawings(), orderType);
                // show warning messages if needed
                return docView.yellOnFailure(promise);
            }
        },

        'drawing/change/explicit': {
            parent: 'drawing/operation',
            set(drawingDescs) {
                // change the specified drawing objects
                var promise = self.execChangeDrawings(drawingDescs);
                // show warning messages if needed
                return docView.yellOnFailure(promise);
            }
        },

        // flipping and rotation ------------------------------------------

        // Parent item for drawing objects that can be flipped and rotated. The getter returns
        // an array with all drawing models contained in the selection that can be flipped.
        'drawing/operation/transform': {
            parent: 'drawing/operation',
            enable() { return this.value.length > 0; },
            get(drawingModels) { return drawingModels.filter(isRotatableModel); }
        },

        // (stateless command) flips the selected drawing objects horizontally
        'drawing/transform/fliph': {
            parent: 'drawing/operation/transform',
            set() {
                return implChangeDrawings(this.parentValue, function (drawingModel) {
                    return { drawing: { flipH: !drawingModel.isFlippedH(), rotation: -drawingModel.getRotationDeg() } };
                });
            }
        },

        // (stateless command) flips the selected drawing objects vertically
        'drawing/transform/flipv': {
            parent: 'drawing/operation/transform',
            set() {
                return implChangeDrawings(this.parentValue, function (drawingModel) {
                    return { drawing: { flipV: !drawingModel.isFlippedV(), rotation: -drawingModel.getRotationDeg() } };
                });
            }
        },

        // (stateless command) rotates the selected drawing objects by the passed angle
        'drawing/transform/rotate': {
            parent: 'drawing/operation/transform',
            enable() { return !odf; }, // bug 52930: rotation disabled due to bugs in LO when displaying rotated shapes
            set(angle) {
                return implChangeDrawings(this.parentValue, function (drawingModel) {
                    return { drawing: { rotation: drawingModel.getRotationDeg() + angle } };
                });
            }
        },

        // getter returns the current rotation angle of all rotatable drawings;
        // setter rotates the selected drawing objects to a specific rotation angle
        'drawing/transform/angle': {
            parent: 'drawing/operation/transform',
            enable() { return !odf; }, // bug 52930: rotation disabled due to bugs in LO when displaying rotated shapes
            get(drawingModels) { return getMixedAttributeValue(drawingModels, 'drawing', 'rotation'); },
            set(angle) { return implChangeDrawings(this.parentValue, { drawing: { rotation: angle } }); }
        },

        // line attributes ------------------------------------------------

        // Parent item for drawing objects whose line attributes can be modified. The getter returns an
        // array with all drawing models contained in the selection that support line attributes, either
        // selected directly, or the children in selected group objects.
        'drawing/operation/line': {
            parent: 'drawing/operation',
            enable() { return this.value.length > 0; },
            get(drawingModels) { return resolveModelsDeeply(drawingModels, isLineFormatModel); }
        },

        // preset border style of all selected drawings
        'drawing/border/style': {
            parent: 'drawing/operation/line',
            get: getMixedPresetBorder,
            set(preset) {
                // create the line attributes for the passed preset border, clear color when hiding the line
                var defLineAttrs = resolvePresetBorder(preset);
                if (defLineAttrs.type === 'none') { defLineAttrs.color = null; }
                // generate and apply the operations for all selected drawing objects
                return implChangeDrawings(this.parentValue, function (_drawingModel, mergedAttrSet) {
                    var attrSet = { line: { ...defLineAttrs } };
                    // replace automatic color with black
                    if ((defLineAttrs.type !== 'none') && (mergedAttrSet.line.color.type === 'auto')) {
                        attrSet.line.color = Color.BLACK;
                    }
                    return attrSet;
                });
            }
        },

        // border color of all selected drawings
        'drawing/border/color': {
            parent: 'drawing/operation/line',
            get: getMixedBorderColor,
            set(color) {
                return implChangeDrawings(this.parentValue, function (_drawingModel, mergedAttrSet) {
                    var attrSet = { line: { color } };
                    // change invisible borders to solid borders
                    if (!hasVisibleBorder(mergedAttrSet.line)) { attrSet.line.type = 'solid'; }
                    return attrSet;
                });
            }
        },

        // fill attributes ------------------------------------------------

        // Parent item for drawing objects whose fill attributes can be modified. The getter returns an
        // array with all drawing models contained in the selection that support fill attributes, either
        // selected directly, or the children in selected group objects.
        'drawing/operation/fill': {
            parent: 'drawing/operation',
            enable() { return this.value.length > 0; },
            get(drawingModels) { return resolveModelsDeeply(drawingModels, isFillFormatModel); }
        },

        // fill color of all selected drawings
        'drawing/fill/color': {
            parent: 'drawing/operation/fill',
            get: getMixedFillColor,
            set(color) {
                var type = (color.type === 'auto') ? 'none' : 'solid';
                return implChangeDrawings(this.parentValue, { fill: { type, color } });
            }
        },

        // connector attributes -------------------------------------------

        // Parent item for connector line objects. The getter returns an array with all connector models
        // contained in the selection, either selected directly, or the children in selected group objects.
        'drawing/operation/connector': {
            parent: 'drawing/operation',
            enable() { return this.value.length > 0; },
            get(drawingModels) { return resolveModelsDeeply(drawingModels, isConnectorModel); }
        },

        // arrow types (head and tail) of all selected connector lines
        'drawing/connector/arrows': {
            parent: 'drawing/operation/connector',
            get(drawingModels) {
                var arrowHead = getMixedAttributeValue(drawingModels, 'line', 'headEndType');
                var arrowTail = getMixedAttributeValue(drawingModels, 'line', 'tailEndType');
                return (arrowHead && arrowTail) ? (arrowHead + ':' + arrowTail) : null;
            },
            set(presetArrows) {
                var parts = presetArrows.split(':');
                return implChangeDrawings(this.parentValue, { line: { headEndType: parts[0], tailEndType: parts[1] } });
            }
        },

        // text content attributes ----------------------------------------

        // Parent item for drawing objects with text contents. The getter returns an array with all
        // drawing models contained in the selection that suuport text contents, either selected
        // directly, or the children in selected group objects.
        'drawing/operation/text': {
            parent: 'drawing/operation',
            enable() { return this.value.length > 0; },
            get(drawingModels) { return resolveModelsDeeply(drawingModels, isTextFrameModel); }
        },

        // fixed item key, as expected by the text framework
        'drawing/verticalalignment': {
            parent: 'drawing/operation/text',
            get(drawingModels) { return getMixedAttributeValue(drawingModels, 'shape', 'anchor'); },
            set(anchorMode) { return implChangeDrawings(this.parentValue, { shape: { anchor: anchorMode } }); }
        }
    });

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
        drawingCollection = docView.drawingCollection;
    });
}
