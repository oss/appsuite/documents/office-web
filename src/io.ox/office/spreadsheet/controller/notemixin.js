/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import { itr } from '@/io.ox/office/tk/algorithms';
import { RangeArray } from '@/io.ox/office/spreadsheet/utils/rangearray';

// mix-in class NoteMixin =====================================================

/**
 * Implementations of all controller items for manipulating cell notes in
 * the active sheet, intended to be mixed into a document controller
 * instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the current cell
 *  selection.
 */
export default function NoteMixin(docView) {

    const { selectionEngine } = docView;

    // the document model
    var docModel = docView.docModel;

    // the model and collections of the active sheet
    var sheetModel = null;
    var noteCollection = null;

    // the model of a cell note made visible temporarily by "search" action
    var tempShowNoteModel = null;

    // different labels for notes depending on file format
    var ooxml = docView.docApp.isOOXML();

    // private methods ----------------------------------------------------

    /**
     * Leaves text edit mode before modifying notes, and checks that the
     * notes in the active sheet are not locked.
     *
     * @param {string} [errorCode]
     *  An error code to be returned if the notes are locked.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil if the notes can be modified; or reject,
     *  if the notes are locked.
     */
    function ensureUnlockedNotes(errorCode) {
        return docView.leaveTextEditMode().then(function () {
            return docView.ensureUnlockedNotes(errorCode ? { errorCode } : null);
        });
    }

    /**
     * Generates document operations to show or hide all cell notes in the
     * active sheet
     *
     * @param {boolean} visible
     *  Whether to show all hidden notes (`true`), or to hide all visible
     *  notes (`false`).
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when all operations have been generated.
     */
    function toggleAllNotes(visible) {

        // leave edit mode (bug 56937: notes CAN be toggled in locked sheets)
        var promise = docView.leaveTextEditMode();

        // generate and apply the operations
        promise = promise.then(function () {
            return sheetModel.createAndApplyOperations(function (generator) {
                return noteCollection.generateToggleAllNotesOperations(generator, visible);
            });
        });

        // show warning alerts if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Moves the active cell to the next or preceding cell note.
     */
    function moveToNote(reverse) {

        // the remaining ranges in the sheet to be visited, starting at the active cell
        var ranges = RangeArray.splitRanges(docModel.addressFactory.getSheetRange(), {
            reverse,
            startAddress: selectionEngine.getActiveCell(),
            skipStart: true
        });

        // show a message if there are no more notes available
        const noteModel = itr.shift(noteCollection.yieldModels({ ranges, reverse, visibleCells: true }));
        if (!noteModel) {
            docView.yellMessage('note:search:finished', true);
            return;
        }

        // select and scroll to the anchor cell of the note
        var anchorAddress = noteModel.getAnchor();
        selectionEngine.selectCell(anchorAddress);
        docView.scrollToCell(anchorAddress);

        // make the note visible temporarily
        docView.tempShowDrawingFrame(noteModel);
        tempShowNoteModel = noteModel;
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        'note/model': {
            parent: 'sheet/operation/cell',
            enable() { return !!this.value; },
            get() { return noteCollection.getByAddress(selectionEngine.getActiveCell()); }
        },

        'note/delete': {
            parent: 'note/model',
            set() {

                // the active cell note model
                var noteModel = this.parentValue;
                // check that the notes in the active sheet are not locked
                var promise = ensureUnlockedNotes('note:delete:locked');

                // generate and apply the operations
                promise = promise.then(function () {
                    return sheetModel.createAndApplyOperations(function (generator) {
                        noteModel.generateDeleteOperations(generator);
                    }, { storeSelection: true });
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            }
        },

        // toggle item for visibility of the note in the active cell
        'note/visible': {
            parent: 'note/model',
            get(noteModel) {
                return !!noteModel && noteModel.isVisible();
            },
            set(visible) {

                // the active cell note model
                var noteModel = this.parentValue;
                // leave edit mode (bug 56937: notes CAN be toggled in locked sheets)
                var promise = docView.leaveTextEditMode();

                // generate and apply the operations
                promise = promise.then(function () {
                    return sheetModel.createAndApplyOperations(function (generator) {
                        var drawingAttrs = { hidden: !visible };
                        noteModel.generateChangeOperations(generator, { drawing: drawingAttrs });
                    }, { storeSelection: true });
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            }
        },

        // parent item that collects data for all notes in the active sheet once (performance)
        'note/available/info': {
            parent: 'app/valid',
            get() { return noteCollection.getNotesInfo(); }
        },

        // parent item that is enabled if the active sheet contains any notes
        'note/available/any': {
            parent: 'note/available/info',
            enable(notesInfo) { return notesInfo.hasNotes; }
        },

        // parent item that is enabled if the active sheet contains notes in visible columns/rows
        'note/available/visiblecell': {
            parent: 'note/available/info',
            enable(notesInfo) { return notesInfo.hasNotesInVisibleCells; }
        },

        // dummy item for "Notes" dropdown menu
        'view/notes/menu': {
            parent: 'note/available/any'
        },

        'note/show/all': {
            parent: ['note/available/info', 'sheet/operation/cell'],
            enable(notesInfo) { return notesInfo.hasHiddenNotesInVisibleCells; },
            set() { return toggleAllNotes(true); }
        },

        'note/hide/all': {
            parent: ['note/available/info', 'sheet/operation/cell'],
            enable(notesInfo) { return notesInfo.hasVisibleNotesInVisibleCells; },
            set() { return toggleAllNotes(false); }
        },

        'note/delete/all': {
            parent: ['sheet/operation/cell', 'note/available/any'],
            set() {

                // check that the notes in the active sheet are not locked
                var promise = ensureUnlockedNotes('note:delete:locked');

                // show a safety query
                promise = promise.then(function () {
                    var title = ooxml ? gt('Delete notes') : gt('Delete comments');
                    var message = ooxml ? gt('All notes on this sheet will be removed. Do you want to continue?') : gt('All comments on this sheet will be removed. Do you want to continue?');
                    return docView.showQueryDialog(title, message, {
                        width: 650,
                        // close automatically, if all notes will be deleted by another client
                        autoCloseKey: 'note/delete/all',
                        // close automatically, if active sheet will be deleted or hidden by another client
                        autoCloseScope: 'activeSheet'
                    });
                });

                // generate and apply the operations
                promise = promise.then(function () {
                    return sheetModel.buildOperations(function (sheetCache) {
                        return noteCollection.generateDeleteAllNotesOperations(sheetCache);
                    });
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            }
        },

        'note/goto/next': {
            parent: 'note/available/visiblecell',
            set() { moveToNote(false); }
        },

        'note/goto/prev': {
            parent: 'note/available/visiblecell',
            set() { moveToNote(true); }
        }
    });

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
        noteCollection = docView.noteCollection;
    });

    // forget cached note model made visible temporarily
    this.listenTo(docView, 'delete:note', function (event) {
        if (event.noteModel === tempShowNoteModel) {
            tempShowNoteModel = null;
        }
    });

    // hide notes that have been made visible temporarily
    this.listenTo(docView, 'change:selection:prepare', function () {
        if (tempShowNoteModel) {
            docView.tempHideDrawingFrame(tempShowNoteModel);
            tempShowNoteModel = null;
        }
    });
}
