/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import { is, jpromise } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getChartTypeStyle } from '@/io.ox/office/drawinglayer/view/drawinglabels';
import { CHANGE_CHART_AXIS, CHANGE_CHART_GRID, CHANGE_CHART_LEGEND, CHANGE_CHART_TITLE,
    CHANGE_DRAWING } from '@/io.ox/office/spreadsheet/utils/operations';
import { createChart, getHeadChar, getNoneShape, getStandardShape,
    updateSeries } from '@/io.ox/office/spreadsheet/view/chartcreator';

// mix-in class ChartMixin ====================================================

/**
 * Implementations of all controller items for manipulating chart objects
 * in the active sheet, intended to be mixed into a document controller
 * instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the current drawing
 *  selection.
 */
export default function ChartMixin(docView) {

    const { selectionEngine } = docView;

    // the application instance containing this controller
    var docApp = docView.docApp;
    // the document model
    var docModel = docView.docModel;

    // private methods ----------------------------------------------------

    /**
     * Shows a warning alert box for the 'exchange direction' function for
     * chart series.
     */
    function yellChartDirection(type) {

        switch (type) {
            case 'sheets':
                docView.yell({
                    type: 'warning',
                    //#. Warning text: A chart object in a spreadsheet contains complex data source (cells from multiple sheets).
                    message: gt('Data references are located on different sheets.')
                });
                break;

            case 'directions':
                docView.yell({
                    type: 'warning',
                    //#. Warning text: A chart object in a spreadsheet contains complex data source (not a simple cell range).
                    message: gt('Data references are too complex for this operation.')
                });
                break;

            default:
                globalLogger.warn('$badge{ChartMixin} yellChartDirection: unknown warning type: "' + type + '"');
        }

        return jpromise.reject();
    }

    /**
     * Returns the position of the selected drawing object, if exactly one
     * drawing object is currently selected.
     *
     * @returns {Opt<Position>}
     *  The position of the selected drawing object in the active sheet
     *  (without leading sheet index), if available; otherwise `undefined`.
     */
    function getDrawingPosition() {
        var drawingPositions = selectionEngine.getSelectedDrawings();
        return (drawingPositions.length === 1) ? drawingPositions[0] : undefined;
    }

    /**
     * @returns {Opt<DrawingModel>}
     *  The drawing model currently selected.
     */
    function getDrawingModel(type) {
        var drawingPos = getDrawingPosition();
        return drawingPos && docView.drawingCollection.getModel(drawingPos, { type });
    }

    /**
     * Generates and applies a document operation for a chart object with correct undo operation.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the drawing operations have
     *  been applied successfully, or that will be rejected on any error.
     */
    function createAndApplyChartOperation(chartModel, opName, operProps) {
        return docView.sheetModel.createAndApplyOperations(function (generator) {

            var position = getDrawingPosition();
            var undoProps = {};

            switch (opName) {
                case CHANGE_CHART_LEGEND: {
                    var legendModel = chartModel.getLegendModel(operProps.axis);
                    undoProps.attrs = legendModel.getUndoAttributeSet(operProps.attrs);
                    break;
                }

                case CHANGE_CHART_TITLE: {
                    var axisId = is.number(operProps.axis) ? operProps.axis : null;
                    var titleModel = chartModel.getTitleModel(axisId);
                    if (axisId !== null) { undoProps.axis = axisId; }
                    undoProps.attrs = titleModel.getUndoAttributeSet(operProps.attrs);
                    break;
                }

                case CHANGE_CHART_AXIS:
                    var axisModel = chartModel.getAxisModel(operProps.axis);
                    undoProps = { axis: operProps.axis, axPos: operProps.axPos, crossAx: operProps.crossAx };
                    undoProps.attrs = axisModel.getUndoAttributeSet(operProps.attrs);
                    break;

                case CHANGE_CHART_GRID:
                    var gridModel = chartModel.getAxisModel(operProps.axis).getGrid();
                    undoProps.axis = operProps.axis;
                    undoProps.attrs = gridModel.getUndoAttributeSet(operProps.attrs);
                    break;

                case CHANGE_DRAWING:
                    undoProps.attrs = chartModel.getUndoAttributeSet(operProps.attrs);
                    break;

                default:
                    globalLogger.error('$badge{ChartMixin} generateChartOperation: unknown operation "' + opName + '"');
                    return;
            }

            generator.generateDrawingOperation(opName, position, undoProps, { undo: true });
            generator.generateDrawingOperation(opName, position, operProps);
        }, { storeSelection: true });
    }

    /**
     * Returns the title text from the passed attribute set.
     *
     * @param {Object} [attrSet]
     *  The merged attribute set for a chart title. If omitted, returns an
     *  empty string.
     *
     * @returns {String}
     *  The title text from the passed attribute set.
     */
    function getChartTitle(attrSet) {
        return (attrSet && is.array(attrSet.text.link)) ? (attrSet.text.link[0] || '').trim() : '';
    }

    /**
     * Changes the attributes of the specified title in the selected chart
     * object.
     *
     * @param {ChartModel} chartModel
     *  The chart model to be manipulated.
     *
     * @param {number|null} axisId
     *  The axis identifier of an axis title, or `null` for the main title.
     *
     * @param {Dict} attrSet
     *  The attribute set to be set for the title.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all operations have been generated.
     */
    function changeChartTitle(chartModel, axisId, attrSet) {
        var operProps = { attrs: attrSet };
        if (is.number(axisId)) { operProps.axis = axisId; }
        return createAndApplyChartOperation(chartModel, CHANGE_CHART_TITLE, operProps);
    }

    /**
     * Changes the text of the specified title in the selected chart
     * object.
     *
     * @param {ChartModel} chartModel
     *  The chart model to be manipulated.
     *
     * @param {number|null} axisId
     *  The axis identifier of an axis title, or `null` for the main title.
     *
     * @param {string} title
     *  The new text contents of the title. An empty string will remove the
     *  title from the chart.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when all operations have been generated.
     */
    function setChartTitle(chartModel, axisId, title) {
        var attrSet = { text: { link: [title] }, character: getHeadChar(docModel) };
        return changeChartTitle(chartModel, axisId, attrSet);
    }

    function selectNewChartSource(selection) {

        var range = selection.ranges[0];
        var sheet = docView.activeSheet;

        var chart = getDrawingModel('chart');

        var possSources = chart.getDataSourceInfo();
        if (possSources.warn) {
            return updateSeries({ app: docApp, sourceRange: range, sourceSheet: sheet, chartModel: chart });
        }
        var forceNames;
        if (chart.isXYType()) { forceNames = chart.isNamesLabel(); }

        return updateSeries({ app: docApp, sourceRange: range, sourceSheet: sheet, chartModel: chart, axis: possSources.axis, forceNames });
    }

    /**
     * changeChartType maps the assigned id to chart-data
     * all ids are mapped in DrawingLabels.CHART_TYPE_STYLES
     *
     * There is a special behavior for bubble-chart, change to bubble or from bubble.
     * all series-data will be removed an initialized complete new by the ChartCreator
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the operation has been
     *  applied successfully, or that will be rejected on any error.
     */
    function changeChartType(id) {

        var chartModel = getDrawingModel('chart');
        var data = getChartTypeStyle(id);

        // fix for Bug 46917 & Bug 49738
        var possSources = chartModel.getDataSourceInfo();
        if (possSources.warn) {
            return yellChartDirection(possSources.warn);
        }
        var forceNames;
        if (chartModel.isXYType()) { forceNames = chartModel.isNamesLabel(); }
        return updateSeries({ app: docApp, sourceRange: possSources.range, sourceSheet: possSources.sheet, chartModel, axis: possSources.axis, chartData: data, forceNames/*, typeChanged: true*/ });
    }

    function isFirstRowColEnabled(chartModel, rowCol) {
        if (!chartModel) { return false; }

        var possSources = chartModel.getDataSourceInfo();
        if (possSources.warn) { return false; }
        if (possSources.axis) { rowCol = Math.abs(rowCol - 1); }
        if (rowCol === 1) {
            return isTitleLabelEnabled(chartModel);
        } else {
            return chartModel.isNamesLabel() || (chartModel.getSeriesCount() > 1);
        }
    }

    function isFirstRowCol(chartModel, rowCol) {
        if (!chartModel) { return false; }

        var possSources = chartModel.getDataSourceInfo();
        if (possSources.warn) { return false; }
        if (possSources.axis) { rowCol = Math.abs(rowCol - 1); }
        if (rowCol === 1) {
            return chartModel.isTitleLabel();
        } else {
            return chartModel.isNamesLabel();
        }
    }

    function setFirstRowCol(chartModel, rowCol, value) {
        if (!chartModel) { return null; }
        var possSources = chartModel.getDataSourceInfo();
        if (possSources.warn) { return null; }

        if (possSources.axis) { rowCol = Math.abs(rowCol - 1); }

        var forceTitle;
        var forceNames;
        if (rowCol === 1) {
            forceNames = chartModel.isNamesLabel();
            forceTitle = value;
        } else {
            forceNames = value;
            forceTitle = chartModel.isTitleLabel();
        }
        return updateSeries({ app: docApp, sourceRange: possSources.range, sourceSheet: possSources.sheet, chartModel, axis: possSources.axis, forceTitle, forceNames });
    }

    function isTitleLabelEnabled(chartModel) {
        if (chartModel.isTitleLabel()) {
            return true;
        }
        var min = 1;
        if (chartModel.isAreaOrLine()) { min = 2; }
        return chartModel.getFirstPointsCount() > min;
    }

    function exchangeEnabled(chartModel) {
        var min = 0;
        if (chartModel.isAreaOrLine()) { min = 1; }
        return chartModel.getSeriesCount() > min;
    }

    function switchRowAndColumn(chartModel) {
        var possSources = chartModel.getDataSourceInfo();
        if (possSources.warn) {
            return yellChartDirection(possSources.warn);
        }
        var forceTitle = chartModel.isNamesLabel();
        var forceNames = chartModel.isTitleLabel();
        return updateSeries({ app: docApp, sourceRange: possSources.range, sourceSheet: possSources.sheet, chartModel, axis: 1 - possSources.axis, forceTitle, forceNames, switchRowColumn: true });
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        'chart/insert': {
            parent: 'drawing/insert/enable',
            enable() {
                var ranges = selectionEngine.getSelectedRanges();
                return (ranges.length === 1) && !docView.sheetModel.isSingleCellInRange(ranges[0]);
            },
            set(id) { return createChart(docApp, getChartTypeStyle(id)); }
        },

        'drawing/chart': {
            parent: 'drawing/operation',
            enable() { return !!this.value; },
            get() { return getDrawingModel('chart') || null; }
        },

        'drawing/chart/valid': {
            parent: 'drawing/chart',
            enable(chartModel) { return chartModel.isRestorable(); }
        },

        'drawing/chartlabels': {
            parent: 'drawing/chart/valid',
            get() { return docView.chartLabelsMenu.isVisible(); },
            set(state) { docView.chartLabelsMenu.toggle(state); }
        },

        'drawing/chartexchange': {
            parent: 'drawing/chart/valid',
            enable: exchangeEnabled,
            set() { return switchRowAndColumn(this.parentValue); }
        },

        'drawing/chartfirstcol': {
            parent: 'drawing/chart/valid',
            enable(chartModel) { return isFirstRowColEnabled(chartModel, 0); },
            get(chartModel) { return isFirstRowCol(chartModel, 0); },
            set(state) { return setFirstRowCol(this.parentValue, 0, state); }
        },

        'drawing/chartfirstrow': {
            parent: 'drawing/chart/valid',
            enable(chartModel) { return isFirstRowColEnabled(chartModel, 1); },
            get(chartModel) { return isFirstRowCol(chartModel, 1); },
            set(state) { return setFirstRowCol(this.parentValue, 1, state); }
        },

        'drawing/charttype': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? chartModel.getChartTypeForGui() : null; },
            set: changeChartType
        },

        // parent item providing access to the attributes of a chart model
        'drawing/chart/attributes': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? chartModel.getMergedAttributeSet(true) : null; }
        },

        'drawing/chartvarycolor': {
            parent: ['drawing/chart/valid', 'drawing/chart/attributes'],
            enable(chartModel) { return chartModel.isVaryColorEnabled(); },
            get(chartModel) { return chartModel ? chartModel.isVaryColor() : null; },
            set(state) { return this.parentValue.changeVaryColors(state); }
        },

        'drawing/chartdatalabel': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? (!is.empty(chartModel.getDataLabel())) : null; },
            set(state) { return this.parentValue.setDataLabel(state); }
        },

        'drawing/chartcolorset': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? chartModel.getColorSet() : null; },
            set(colorset) { return this.parentValue.changeColorSet(colorset); }
        },

        'drawing/chartstyleset': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? chartModel.getStyleSet() : null; },
            set(colorset) { return this.parentValue.changeStyleSet(colorset); }
        },

        'drawing/chartdatasource': {
            parent: 'drawing/chart/valid'
        },

        'drawing/chartsource': {
            parent: 'drawing/chart/valid',
            enable() { return !TOUCH_DEVICE; }, // disable for touch devices. remove this if the story DOCS-1036 user can edit chart data reference on touch devices is ready
            get() {
                return selectionEngine.isCustomSelectionMode('drawing/chartsource');
            },
            set() {
                selectionEngine.enterCustomSelectionMode('drawing/chartsource', {
                    applySelected: selectNewChartSource,
                    //#. change source data for a chart object in a spreadsheet
                    statusLabel: gt('Select source data')
                });
            }
        },

        'drawing/chartlegend/pos': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? chartModel.getLegendModel().getMergedAttributeSet(true).legend.pos : null; },
            set(pos) { return createAndApplyChartOperation(this.parentValue, CHANGE_CHART_LEGEND, { attrs: { legend: { pos } } }); }
        },

        'drawing/chart/axes/enabled': {
            parent: 'drawing/chart/valid',
            enable(chartModel) { return chartModel.isAxesEnabled(); }
        },

        // parent item providing access to the main title model of a chart
        'drawing/chart/title/model': {
            parent: 'drawing/chart/valid',
            get(chartModel) { return chartModel ? chartModel.getTitleModel(null) : null; }
        },

        // parent item providing access to the attributes of the main title model of a chart
        'drawing/chart/title/attributes': {
            parent: ['drawing/chart/title/model', 'drawing/chart/valid'],
            get(titleModel) { return titleModel ? titleModel.getMergedAttributeSet(true) : null; },
            set(attributes) { return changeChartTitle(this.parentValues[1], null, attributes); }
        },

        // return or modify the text contents of the main title of a chart
        'drawing/chart/title/text': {
            parent: ['drawing/chart/title/attributes', 'drawing/chart/valid'],
            get(attributes) { return getChartTitle(attributes); },
            set(title) { return setChartTitle(this.parentValues[1], null, title); }
        },

        // remove the main title from a chart (disabled if title does not exist)
        'drawing/chart/title/delete': {
            parent: ['drawing/chart/title/text', 'drawing/chart/valid'],
            enable() { return this.value.length > 0; },
            set() { return setChartTitle(this.parentValues[1], null, ''); }
        }
    });

    ['x', 'y'].forEach(function (axisType) {

        // the base path of axis items
        var keyPath = 'drawing/chart/axis/' + axisType + '/';
        // item definitions map with dynamic keys
        var definitions = {};

        function getCorrectAxis(chartModel) {
            return chartModel ? chartModel.getAxisForType(axisType) : null;
        }

        function getCorrectAxisId(chartModel) {
            return chartModel ? chartModel.getAxisIdForType(axisType) : null;
        }

        function getStandardLineAttributes(visible) {
            return { line: visible ? getStandardShape(docModel) : getNoneShape() };
        }

        function setAxisAttributes(chartModel, attrs) {
            var axis = getCorrectAxis(chartModel);
            return createAndApplyChartOperation(chartModel, CHANGE_CHART_AXIS, { axis: axis.axisId, axPos: axis.axPos, crossAx: axis.crossAx, attrs });
        }

        function setGridLineAttributes(chartModel, attrs) {
            return createAndApplyChartOperation(chartModel, CHANGE_CHART_GRID, { axis: getCorrectAxisId(chartModel), attrs });
        }

        // *** axis items ***

        // parent item providing access to a chart axis model
        definitions[keyPath + 'model'] = {
            parent: ['drawing/chart/valid', 'drawing/chart/axes/enabled'],
            get(chartModel) { return chartModel ? chartModel.getAxisModel(getCorrectAxisId(chartModel)) : null; }
        };

        // parent item providing access to the attributes of a chart axis model
        definitions[keyPath + 'attributes'] = {
            parent: [keyPath + 'model', 'drawing/chart/valid'],
            get(axisModel) { return axisModel ? axisModel.getMergedAttributeSet(true) : null; },
            set(attributes) { return setAxisAttributes(this.parentValues[1], attributes); }
        };

        // return or modify the visibility of the axis caption labels
        definitions[keyPath + 'labels/visible'] = {
            parent: [keyPath + 'attributes', 'drawing/chart/valid'],
            get(attributes) { return this.parentsEnabled && !!attributes && (attributes.axis.label === true); },
            set(visible) { return setAxisAttributes(this.parentValues[1], { axis: { label: visible } }); }
        };

        // return or modify the visibility of the axis line
        definitions[keyPath + 'line/visible'] = {
            parent: [keyPath + 'attributes', 'drawing/chart/valid'],
            get(attributes) { return !!attributes && (attributes.line.type !== 'none'); },
            set(visible) { return setAxisAttributes(this.parentValues[1], getStandardLineAttributes(visible)); }
        };

        // *** grid line items ***

        // parent item providing access to a chart axis grid model
        definitions[keyPath + 'grid/model'] = {
            parent: keyPath + 'model',
            get(axisModel) { return axisModel ? axisModel.getGrid() : null; }
        };

        // parent item providing access to the attributes of a chart axis grid model
        definitions[keyPath + 'grid/attributes'] = {
            parent: [keyPath + 'grid/model', 'drawing/chart/valid'],
            get(gridModel) { return gridModel ? gridModel.getMergedAttributeSet(true) : null; },
            set(attributes) { return setGridLineAttributes(this.parentValues[1], attributes); }
        };

        // return or modify the visibility of the axis grid lines
        definitions[keyPath + 'grid/visible'] = {
            parent: [keyPath + 'grid/attributes', 'drawing/chart/valid'],
            get(attributes) { return !!attributes && (attributes.line.type !== 'none'); },
            set(visible) { return setGridLineAttributes(this.parentValues[1], getStandardLineAttributes(visible)); }
        };

        // *** title items ***

        // parent item providing access to a chart axis title model
        definitions[keyPath + 'title/model'] = {
            parent: keyPath + 'model',
            get(axisModel) { return axisModel ? axisModel.getTitle() : null; }
        };

        // parent item providing access to the attributes of a chart axis title model
        definitions[keyPath + 'title/attributes'] = {
            parent: [keyPath + 'title/model', 'drawing/chart/valid'],
            get(titleModel) { return titleModel ? titleModel.getMergedAttributeSet(true) : null; },
            set(attributes) {
                var chartModel = this.parentValues[1];
                return changeChartTitle(chartModel, getCorrectAxisId(chartModel), attributes);
            }
        };

        // return or modify the text contents of a chart axis title
        definitions[keyPath + 'title/text'] = {
            parent: [keyPath + 'title/attributes', 'drawing/chart/valid'],
            get(attributes) { return getChartTitle(attributes); },
            set(title) {
                var chartModel = this.parentValues[1];
                return setChartTitle(chartModel, getCorrectAxisId(chartModel), title);
            }
        };

        // remove the main title from a chart (disabled if title does not exist)
        definitions[keyPath + 'title/delete'] = {
            parent: [keyPath + 'title/text', 'drawing/chart/valid'],
            enable() { return this.value.length > 0; },
            set() {
                var chartModel = this.parentValues[1];
                return setChartTitle(chartModel, getCorrectAxisId(chartModel), '');
            }
        };

        this.registerItemsOnSuccess(definitions);
    }, this);
}
