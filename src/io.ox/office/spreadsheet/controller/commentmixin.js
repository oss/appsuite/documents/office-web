/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import { SMALL_DEVICE } from '@/io.ox/office/tk/dom';
import { itr } from '@/io.ox/office/tk/algorithms';
import { RangeArray } from '@/io.ox/office/spreadsheet/utils/rangearray';

// mix-in class CommentMixin ==================================================

/**
 * Implementations of all controller items for manipulating cell comments
 * in the active sheet, intended to be mixed into a document controller
 * instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the current cell
 *  selection.
 */
export default function CommentMixin(docView) {

    const { selectionEngine } = docView;

    // the document model
    var docModel = docView.docModel;

    // the model and collections of the active sheet
    var sheetModel = null;
    var noteCollection = null;
    var commentCollection = null;

    // private methods ----------------------------------------------------

    /**
     * Moves the active cell to the next or preceding cell with a comment
     * thread.
     */
    function moveToComment(reverse) {

        // the current active cell in the sheet
        var activeCell = selectionEngine.getActiveCell();

        // the remaining ranges in the sheet to be visited, starting at the active cell
        var ranges = RangeArray.splitRanges(docModel.addressFactory.getSheetRange(), {
            reverse,
            startAddress: activeCell,
            skipStart: true,
            wrap: true
        });

        // resolve the next available comment model
        const threadModel = itr.shift(commentCollection.threadModels({ ranges, reverse, visibleCells: true }));
        if (!threadModel) { return; }

        // show a message if iterator has wrapped at the sheet borders
        var anchorAddress = threadModel.model.getAnchor();
        var compResult = activeCell.compareTo(anchorAddress);
        if (reverse ? (compResult <= 0) : (compResult >= 0)) {
            docView.yellMessage('comment:search:docbeginning', true);
        }

        // activate the comments side pane
        if (!SMALL_DEVICE) {
            docView.commentsPane.show();
        }

        // select and scroll to the anchor cell of the comment
        selectionEngine.selectCell(anchorAddress);
        docView.scrollToCell(anchorAddress);
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        // parent item that is enabled if the active cell contains a comment thread
        'threadedcomment/hasthread': {
            parent: 'app/valid',
            enable() { return commentCollection.hasComment(selectionEngine.getActiveCell()); }
        },

        // parent item that is enabled if the active cell does not contain a comment thread
        'threadedcomment/hasnothread': {
            parent: 'app/valid',
            enable() { return !commentCollection.hasComment(selectionEngine.getActiveCell()); }
        },

        // parent item that is enabled if the active sheet contains at least one comment thread
        'threadedcomment/available': {
            parent: 'app/valid',
            enable() { return commentCollection.hasComments(); }
        },

        // parent item that is enabled if the active sheet contains comments in visible columns/rows
        'threadedcomment/available/visible': {
            parent: 'app/valid',
            enable() { return commentCollection.hasComments({ visibleCells: true }); }
        },

        // starts a new comment thread in the active cell, or starts a reply for an existing thread
        'threadedcomment/insert': {
            parent: ['sheet/operation/cell', 'document/ooxml'],
            // cannot insert a comment into a cell with an existing (old-style) note
            enable() { return !noteCollection.getByAddress(selectionEngine.getActiveCell()); },
            set() {

                // leave text edit mode, select unsaved comment instead of starting another unsaved comment
                var promise = docView.leaveTextEditMode().then(function () {
                    return docView.processUnsavedComment();
                });

                // create a reply or a new comment
                promise = promise.then(function () {
                    var anchorAddress = selectionEngine.getActiveCell();
                    var commentModel = commentCollection.getStartCommentByAddress(anchorAddress);
                    if (commentModel) {
                        docModel.trigger('start:threadedcomment:reply', commentModel);
                    } else {
                        return commentCollection.startNewThread(anchorAddress);
                    }
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            },
            // bug 68348: force focus into comment text area (explicit focus during controller busy mode fails)
            focusTarget() { return docView.commentsPane.getEditFocusTarget(); }
        },

        // deletes the passed comment reply (or the entire thread if passed comment is a root)
        'threadedcomment/delete/reply': {
            parent: 'document/editable',
            set(commentModel) {

                // leave edit mode before deleting comments
                var promise = docView.leaveTextEditMode();

                // generate and apply the operations
                promise = promise.then(function () {
                    return sheetModel.createAndApplyOperations(function (generator) {
                        return commentCollection.generateDeleteCommentOperations(generator, commentModel);
                    }, { storeSelection: true });
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            }
        },

        // deletes the selected comment thread
        'threadedcomment/delete/thread': {
            parent: ['sheet/operation/cell', 'threadedcomment/hasthread'],
            set() {

                // leave edit mode before deleting comments
                var promise = docView.leaveTextEditMode();

                // generate and apply the operations
                promise = promise.then(function () {
                    return sheetModel.createAndApplyOperations(function (generator) {
                        return commentCollection.generateDeleteThreadOperations(generator, selectionEngine.getActiveCell());
                    }, { storeSelection: true });
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            }
        },

        // deletes all comment threads in the active sheet
        'threadedcomment/delete/all': {
            parent: ['sheet/operation/cell', 'threadedcomment/available'],
            set() {

                // leave edit mode before deleting comments
                var promise = docView.leaveTextEditMode();

                // show a safety query
                promise = promise.then(function () {
                    return docView.showQueryDialog(gt('Delete comments'), gt('All comments on this sheet will be removed. Do you want to continue?'), {
                        width: 650,
                        // close automatically, if all comments will be deleted by another client
                        autoCloseKey: 'threadedcomment/delete/all',
                        // close automatically, if active sheet will be deleted or hidden by another client
                        autoCloseScope: 'activeSheet'
                    });
                });

                // generate and apply the operations
                promise = promise.then(function () {
                    return sheetModel.createAndApplyOperations(function (generator) {
                        return commentCollection.generateDeleteAllCommentsOperations(generator);
                    });
                });

                // show warning alerts if necessary
                return docView.yellOnFailure(promise);
            }
        },

        'comment/goto/next': {
            parent: 'threadedcomment/available/visible',
            set() { moveToComment(false); }
        },

        'comment/goto/prev': {
            parent: 'threadedcomment/available/visible',
            set() { moveToComment(true); }
        }
    });

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function () {
        sheetModel = docView.sheetModel;
        noteCollection = docView.noteCollection;
        commentCollection = docView.commentCollection;
    });

    // select a comment thread in the side pane after changing the sheet selection
    this.listenTo(docView, 'change:selection', function (selection) {
        var commentModel = commentCollection.getStartCommentByAddress(selection.address);
        if (commentModel) {
            docModel.trigger('select:threadedcomment', commentModel);
        }
    });

    // focus back to application pane after editing a comment thread
    this.listenTo(docView, "commenteditor:closed", () => this.grabFocus());
}
