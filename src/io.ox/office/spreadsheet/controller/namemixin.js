/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { Address } from '@/io.ox/office/spreadsheet/utils/sheetutils';
import { TokenArray } from '@/io.ox/office/spreadsheet/model/formula/tokenarray';
import { DefinedNameDialog } from '@/io.ox/office/spreadsheet/view/dialogs';

// mix-in class NameMixin =====================================================

/**
 * Implementations of all controller items for manipulating defined names
 * in the document, or in the active sheet, intended to be mixed into a
 * document controller instance.
 *
 * @param {SpreadsheetView} docView
 *  The document view providing view settings such as the current cell
 *  selection.
 */
export default function NameMixin(docView) {

    const { selectionEngine } = docView;

    // the document model
    var docModel = docView.docModel;

    // index of the active sheet
    var activeSheet = -1;

    // private methods ----------------------------------------------------

    /**
     * Inserts a new defined name into the document.
     *
     * @param {string} label
     *  The label of the new defined name.
     *
     * @param {string} formula
     *  The formula expression to be bound to the new defined name.
     *
     * @param {OptSheet} sheet
     *  The index of the sheet the new defined name shall be inserted into,
     *  or `null` to create a globally defined name.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the defined name has been created
     *  successfully, or that will reject on any error.
     */
    function insertName(label, formula, sheet) {

        // the parent model for the new defined name (sheet or document)
        var parentModel = (sheet === null) ? docModel : docModel.getSheetModel(sheet);
        // the configuration for the formula expression
        var fmlaSpec = {
            grammarType: "ui",
            formula,
            refSheet: activeSheet,
            refAddress: selectionEngine.getActiveCell()
        };

        // create and apply the operations to create the defined name
        var promise = docModel.buildOperations(function (builder) {
            return parentModel.nameCollection.generateInsertNameOperations(builder, label, fmlaSpec);
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows a dialog that allows to create a new defined name for the
     * document. The dialog will be kept open until a valid label and
     * formula expression has been entered for the defined name.
     *
     * @returns {JPromise<void>}
     *  A promise representing the dialog. Will fulfil after the defined
     *  name has been created successfully; or will reject, if the dialog
     *  has been canceled, or if the document has switched to read-only
     *  mode.
     */
    function showInsertNameDialog() {

        // leave text edit mode before creating a name
        return docView.leaveTextEditMode().then(function () {

            // create a formula from the current selection (bug 40477: use start address of single merged range)
            var tokenArray = new TokenArray(docModel, "name");
            if (selectionEngine.isSingleCellSelection()) {
                tokenArray.appendAddress(selectionEngine.getActiveCell(), { sheet: activeSheet, abs: true });
            } else {
                tokenArray.appendRangeList(selectionEngine.getSelectedRanges(), { sheet: activeSheet, abs: true });
            }

            // create the dialog, initialize with the formula expression
            var formula = tokenArray.getFormula("ui", Address.A1, Address.A1);
            var dialog = new DefinedNameDialog(docView, insertName, { formula });

            // show the dialog, inserting the defined name will be done by the action callback
            return dialog.show();
        });
    }

    /**
     * Changes the label or formula definition of an existing defined name.
     *
     * @param {NameModel} nameModel
     *  The model of the defined name to be changed.
     *
     * @param {string|null} newLabel
     *  The new label for the defined name. If set to `null`, the label of
     *  the defined name will not be changed.
     *
     * @param {string|null} newFormula
     *  The new formula expression to be bound to the defined name. If set
     *  to `null`, the formula expression of the defined name will not be
     *  changed (an invalid formula expression in the defined name will be
     *  retained).
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the defined name has been changed
     *  successfully, or that will reject on any error.
     */
    function changeName(nameModel, newLabel, newFormula) {

        // the descriptor for the new formula expression
        var fmlaSpec = (typeof newFormula === 'string') ? {
            grammarType: "ui",
            formula: newFormula,
            refSheet: activeSheet,
            refAddress: selectionEngine.getActiveCell()
        } : null;

        // create and apply the operations to change the defined name
        var promise = docModel.buildOperations(function (builder) {
            return nameModel.generateChangeOperations(builder, newLabel, fmlaSpec);
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    /**
     * Shows a dialog that allows to change an existing defined name in the
     * document. The dialog will be kept open until a valid label and
     * formula expression has been entered.
     *
     * @param {NameModel} nameModel
     *  The model of the defined name to be changed.
     *
     * @returns {JPromise<void>}
     *  A promise representing the dialog. Will fulfil after the defined
     *  name has been changed successfully; or will reject, if the dialog
     *  has been canceled, or if the document has switched to read-only
     *  mode.
     */
    function showChangeNameDialog(nameModel) {

        // leave text edit mode before changing a defined name
        return docView.leaveTextEditMode().then(function () {

            // create the dialog, initialize with settings of the defined name
            var dialog = new DefinedNameDialog(docView, changeName.bind(null, nameModel), { nameModel });

            // show the dialog, changing the defined name will be done by the callback passed to the dialog
            return dialog.show();
        });
    }

    /**
     * Deletes a defined name from the document.
     *
     * @param {NameModel} nameModel
     *  The model of the defined name to be deleted.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil when the defined name has been deleted
     *  successfully, or that will reject on any error.
     */
    function deleteName(nameModel) {

        // create and apply the operations to delete the defined name
        var promise = docModel.buildOperations(function (builder) {
            return nameModel.generateDeleteOperations(builder);
        });

        // show warning alert if necessary
        return docView.yellOnFailure(promise);
    }

    // item registration --------------------------------------------------

    // register all controller items
    this.registerItemsOnSuccess({

        // toggles the visibility of the 'Defined Names' floating menu
        'view/namesmenu/toggle': {
            parent: 'document/editable/cell',
            get() { return docView.namesMenu.isVisible(); },
            set(state) { docView.namesMenu.toggle(state); }
        },

        'name/insert/dialog': {
            parent: 'sheet/operation/unlocked/cell',
            set: showInsertNameDialog
        },

        'name/edit/dialog': {
            parent: 'sheet/operation/unlocked/cell',
            set: showChangeNameDialog
        },

        'name/delete': {
            parent: 'sheet/operation/unlocked/cell',
            set: deleteName
        }
    });

    // initialization -----------------------------------------------------

    // initialize sheet-dependent class members according to the active sheet
    this.listenTo(docView, 'change:activesheet', function (sheet) {
        activeSheet = sheet;
    });
}
