/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ArrayCallbackFn, ArrayPredicateFn } from "@/io.ox/office/tk/algorithms";

import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { type ArraySource, consumeArraySource, someInArraySource, ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";
import type { IntervalSource } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import type { RangeSource } from "@/io.ox/office/spreadsheet/utils/rangearray";

const { min, max } = Math;

// types ======================================================================

/**
 * Data type for values providing cell range addresses to be consumed by
 * methods of the class `Range3DArray`. Can be an array or an iterator with
 * cell range addresses, a single cell range address, or nullish.
 */
export type Range3DSource = ArraySource<Range3D>;

// class Range3DArray =========================================================

/**
 * Represents a list of cell range addresses with sheet indexes.
 */
export class Range3DArray extends ArrayBase<Range3D> {

    // static functions -------------------------------------------------------

    /**
     * If the passed data source is a `RangeArray`, it will be returned
     * directly (no shallow clone). Otherwise, a new array instance will be
     * created and filled with the elements from the data source.
     *
     * @param source
     *  The data source to be converted to an instance of `RangeArray`.
     *
     * @returns
     *  An instance of this class.
     */
    static cast(source: Range3DSource): Range3DArray {
        return (source instanceof Range3DArray) ? source : new Range3DArray(source);
    }

    /**
     * Creates a `Range3DArray` from the passed data source with the cell
     * ranges passing a truth test.
     *
     * @param source
     *  The data source to be converted to a `Range3DArray`.
     *
     * @param fn
     *  The predicate callback function invoked on the elements of the data
     *  source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The cell range addresses passing the truth test.
     */
    static filter(source: Range3DSource, fn: ArrayPredicateFn<Range3D>, context?: object): Range3DArray {
        return new Range3DArray().appendFiltered(source, fn, context);
    }

    /**
     * Creates a `Range3DArray` from the return values of a callback function
     * that will be invoked on the elements of the passed data source.
     *
     * @param source
     *  The data source to be converted to a `Range3DArray`.
     *
     * @param fn
     *  The callback function invoked on the elements of the data source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The cell range addresses returned from the callback function.
     */
    static map<U>(source: ArraySource<U>, fn: ArrayCallbackFn<Range3DSource, U>, context?: object): Range3DArray {
        return new Range3DArray().appendMapped(source, fn, context);
    }

    /**
     * Creates a list of cell range address with sheet indexes from the passed
     * simple cell range addresses, and the passed sheet indexes. The sheet
     * indexes in the resulting cell range address will be ordered
     * automatically. The start and end address of the original cell range
     * addresses will be deeply cloned when creating the new range addresses.
     *
     * @param ranges
     *  An array of simple cell range addresses (without sheet indexes), or a
     *  simple cell range address (without sheet indexes).
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the created cell range
     *  addresses.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the created cell range
     *  addresses. If omitted, the new ranges will refer to a single sheet.
     *
     * @returns
     *  The cell range addresses created from the passed parameters.
     */
    static fromRanges(ranges: RangeSource, sheet1: number, sheet2?: number): Range3DArray {
        return Range3DArray.map(ranges, range => Range3D.fromRange3D(range, sheet1, sheet2));
    }

    /**
     * Creates a list of cell range addresses with sheet indexes from the
     * passed column and row intervals, by combining each column interval with
     * each row interval. The sheet indexes in the resulting cell range address
     * will be ordered automatically.
     *
     * @param colIntervals
     *  The column intervals of the new cell range addresses. This method also
     *  accepts a single index interval as parameter.
     *
     * @param rowIntervals
     *  The row intervals of the new cell range addresses. This method also
     *  accepts a single index interval as parameter.
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the created cell range
     *  addresses.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the created cell range
     *  addresses. If omitted, the new ranges will refer to a single sheet.
     *
     * @returns
     *  The cell range addresses created from the passed intervals.
     */
    static fromIntervals(colIntervals: IntervalSource, rowIntervals: IntervalSource, sheet1: number, sheet2?: number): Range3DArray {
        const resultRanges = new Range3DArray();
        consumeArraySource(rowIntervals, rowInterval => {
            consumeArraySource(colIntervals, colInterval => {
                resultRanges.push(Range3D.fromIntervals3D(colInterval, rowInterval, sheet1, sheet2));
            });
        });
        return resultRanges;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the number of cells covered by all cell range addresses.
     *
     * @returns
     *  The number of cells covered by all cell range addresses.
     */
    cells(): number {
        return this.reduce((count, range) => count + range.cells(), 0);
    }

    /**
     * Returns whether all cell range addresses in this list refer to the same
     * single sheet.
     *
     * @returns
     *  Whether all cell range addresses in this array refer to the same single
     *  sheet.
     */
    singleSheet(): boolean {
        if (this.empty()) { return false; }
        const sheet = this.first()!.sheet1;
        return this.every(range => range.isSheet(sheet));
    }

    /**
     * Returns whether any cell range address in this list overlaps with any
     * cell range address in the passed data source.
     *
     * @param ranges
     *  The other cell range addresses to be checked. This method also accepts
     *  a single cell range address as parameter.
     *
     * @returns
     *  Whether any cell range address in this list overlaps with any cell
     *  range address in the passed data source.
     */
    overlaps(ranges: Range3DSource): boolean {
        return this.some(range1 => someInArraySource(ranges, range2 => range1.overlaps(range2)));
    }

    /**
     * Returns the address of the bounding cell range of all range addresses in
     * this list (the smallest range that contains all ranges in the list).
     *
     * @returns
     *  The address of the bounding cell range containing all range addresses;
     *  or `undefined`, if this list is empty.
     */
    boundary(): Opt<Range3D> {

        const l = this.length;
        if (l === 0) { return undefined; }

        // this method must not return original array elements
        const boundary = this[0].clone();
        for (let i = 1; i < l; i += 1) {
            const range = this[i];
            boundary.sheet1 = min(boundary.sheet1, range.sheet1);
            boundary.sheet2 = max(boundary.sheet2, range.sheet2);
            boundary.a1.r = min(boundary.a1.r, range.a1.r);
            boundary.a1.c = min(boundary.a1.c, range.a1.c);
            boundary.a1.r = min(boundary.a1.r, range.a1.r);
            boundary.a2.c = max(boundary.a2.c, range.a2.c);
            boundary.a2.r = max(boundary.a2.r, range.a2.r);
        }
        return boundary;
    }

    /**
     * Returns the cell range addresses covered by this and the passed cell
     * range list. More precisely, returns the existing intersection ranges
     * from all pairs of the cross product of the cell range lists.
     *
     * @param ranges
     *  The other cell range addresses to be intersected with the cell range
     *  addresses in this list. This method also accepts a single cell range
     *  address as parameter.
     *
     * @returns
     *  The cell range addresses covered by this array and the passed array.
     */
    intersect(ranges: Range3DSource): Range3DArray {
        const resultRanges = new Range3DArray();
        this.forEach(range1 => consumeArraySource(ranges, range2 => {
            resultRanges.append(range1.intersect(range2));
        }));
        return resultRanges;
    }
}
