/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ReverseOptions, type Relation, is, math, itr } from "@/io.ox/office/tk/algorithms";

import { type IntervalIteratorOptions, Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { COL_PATTERN, ROW_PATTERN, addressKey, parseCol, parseRow, Address } from "@/io.ox/office/spreadsheet/utils/address";

const { min, max, floor } = Math;

// types ======================================================================

/**
 * Options for a cell address iterator created by a `Range` instance.
 */
export interface RangeIteratorOptions extends ReverseOptions {

    /**
     * If set to true, the addresses in this range will be generated in
     * vertical order. Default value is `false`.
     */
    columns?: boolean;
}

// constants ==================================================================

/**
 * Pattern for a single cell address in a range.
 */
const CELL_PATTERN = `(${COL_PATTERN})(${ROW_PATTERN})`;

/**
 * Regular expression object matching a range name.
 */
const RE_RANGE_NAME = new RegExp(`^${CELL_PATTERN}:${CELL_PATTERN}$`, "i");

// public functions ===========================================================

/**
 * Returns a unique string key for the specified indexes of a cell range that
 * can be used for example as key in an associative map.
 *
 * @param col1
 *  The column index of the left border in the cell range.
 *
 * @param row1
 *  The row index of the top border in the cell range.
 *
 * @param col2
 *  The column index of the right border in the cell range.
 *
 * @param row2
 *  The row index of the bottom border in the cell range.
 *
 * @returns
 *  A unique string key for the cell range address.
 */
export function rangeKeyFromIndexes(col1: number, row1: number, col2: number, row2: number): string {
    return `${addressKey(col1, row1)}:${addressKey(col2, row2)}`;
}

/**
 * Returns a unique string key for the specified start and end addresses of a
 * cell range that can be used for example as key in an associative map.
 *
 * @param start
 *  The start address of a cell range.
 *
 * @param end
 *  The end address of a cell range.
 *
 * @returns
 *  A unique string key for the cell range address.
 */
export function rangeKey(start: Address, end: Address): string {
    return `${start.key}:${end.key}`;
}

// class Range ================================================================

/**
 * Represents the address of a rectangular cell range in a sheet of a
 * spreadsheet document.
 *
 * @param start
 *  The address of the top-left cell in the range. The new range instance takes
 *  ownership of this address.
 *
 * @param [end]
 *  The address of the bottom-right cell in the range. If omitted, constructs a
 *  cell range address covering a single cell by cloning the passed start
 *  address.
 */
export class Range implements Keyable, Equality<Range>, Cloneable<Range> {

    // static functions -------------------------------------------------------

    /**
     * Creates a cell range address from the passed column and row indexes. The
     * columns and rows in the resulting cell range address will be ordered
     * automatically.
     *
     * @param col1
     *  The column index of one of the borders in the cell range.
     *
     * @param row1
     *  The row index of one of the borders in the cell range.
     *
     * @param col2
     *  The column index of the other border in the cell range.
     *
     * @param row2
     *  The row index of the other border in the cell range.
     *
     * @returns
     *  The cell range address with adjusted column and row indexes.
     */
    static fromIndexes(col1: number, row1: number, col2: number, row2: number): Range {
        const a1 = new Address(min(col1, col2), min(row1, row2));
        const a2 = new Address(max(col1, col2), max(row1, row2));
        return new Range(a1, a2);
    }

    /**
     * Creates a cell range address from the passed column and row interval.
     *
     * @param colInterval
     *  The column interval of the new cell range address.
     *
     * @param rowInterval
     *  The row interval of the new cell range address.
     *
     * @returns
     *  The cell range address created from the passed index intervals.
     */
    static fromIntervals(colInterval: Interval, rowInterval: Interval): Range {
        return Range.fromIndexes(colInterval.first, rowInterval.first, colInterval.last, rowInterval.last);
    }

    /**
     * Creates a cell range address from the passed column interval and row
     * indexes.
     *
     * @param colInterval
     *  The column interval of the new cell range address.
     *
     * @param firstRow
     *  The index of the first row in the new cell range address.
     *
     * @param [lastRow]
     *  The index of the last row in the new cell range address. If omitted,
     *  the range will cover a single row only.
     *
     * @returns
     *  The cell range address created from the passed column interval and row
     *  indexes.
     */
    static fromColInterval(colInterval: Interval, firstRow: number, lastRow?: number): Range {
        if (!is.number(lastRow)) { lastRow = firstRow; }
        return Range.fromIndexes(colInterval.first, firstRow, colInterval.last, lastRow);
    }

    /**
     * Creates a cell range address from the passed row interval and column
     * indexes.
     *
     * @param rowInterval
     *  The row interval of the new cell range address. Note that this function
     *  is one of the rare cases where a row parameter precedes column
     *  parameters, in order to provide an optional column parameter.
     *
     * @param firstCol
     *  The index of the first column in the new cell range address.
     *
     * @param [lastCol]
     *  The index of the last column in the new cell range address. If omitted,
     *  the range will cover a single column only.
     *
     * @returns
     *  The cell range address created from the passed row interval and column
     *  indexes.
     */
    static fromRowInterval(rowInterval: Interval, firstCol: number, lastCol?: number): Range {
        if (!is.number(lastCol)) { lastCol = firstCol; }
        return Range.fromIndexes(firstCol, rowInterval.first, lastCol, rowInterval.last);
    }

    /**
     * Creates a cell range address from the passed cell addresses. The columns
     * and rows in the resulting cell range address will be ordered
     * automatically. The passed cell addresses will be deeply cloned when
     * creating the new range address.
     *
     * @param address1
     *  The first cell address used to create the new cell range address.
     *
     * @param [address2]
     *  The second cell address used to create the new cell range address. If
     *  omitted, the range will represent a single cell address.
     *
     * @returns
     *  The cell range address created from the passed cell addresses.
     */
    static fromAddresses(address1: Address, address2?: Address | null): Range {
        return address2 ? Range.fromIndexes(address1.c, address1.r, address2.c, address2.r) : new Range(address1.clone());
    }

    /**
     * Creates a cell range address from the passed cell address, and number of
     * columns and rows.
     *
     * @param address
     *  The start cell address used to create the new cell range address. The
     *  cell address will be cloned when creating the new range address (the
     *  range does NOT take ownership).
     *
     * @param cols
     *  The number of columns contained in the resulting cell range address.
     *
     * @param rows
     *  The number of rows contained in the resulting cell range address.
     *
     * @returns
     *  The cell range address created from the passed cell address and size.
     */
    static fromAddressAndSize(address: Address, cols: number, rows: number): Range {
        return Range.fromIndexes(address.c, address.r, address.c + cols - 1, address.r + rows - 1);
    }

    /**
     * Returns the cell range address for the passed string representation.
     *
     * @param text
     *  The string representation of a cell range address, in A1 notation, with
     *  a colon as separator between start and end address.
     *
     * @returns
     *  The cell range address (with adjusted column and row indexes), if the
     *  passed string is valid, otherwise null.
     */
    static parse(text: string): Range | null {

        // range notation (e.g. "A1:B2")
        const matches = RE_RANGE_NAME.exec(text);
        if (matches) {
            const col1 = parseCol(matches[1]);
            const row1 = parseRow(matches[2]);
            const col2 = parseCol(matches[3]);
            const row2 = parseRow(matches[4]);
            return (min(col1, row1, col2, row2) >= 0) ? Range.fromIndexes(col1, row1, col2, row2) : null;
        }

        // address notation (e.g. "A1")
        const address = Address.parse(text);
        return address ? new Range(address) : null;
    }

    /**
     * Compares the passed cell range addresses. Defines a natural order for
     * sorting with the following behavior (let A and B be cell range
     * addresses): If the start address of A is less than the start address of
     * B (using the natural ordering defined by class Address); or if both
     * start addresses are equal, and the end address of A is less than the end
     * address of B, then A is considered less than B.
     *
     * @param range1
     *  The first cell range address to be compared to the second cell range
     *  address.
     *
     * @param range2
     *  The second cell range address to be compared to the first cell range
     *  address.
     *
     * @returns
     *  The relation of the passed cell range addresses.
     */
    static compare(range1: Range, range2: Range): Relation {
        return math.comparePairs(range1.a1, range1.a2, range2.a1, range2.a2, Address.compare);
    }

    // properties -------------------------------------------------------------

    /**
     * The address of the top-left cell in the range.
     */
    a1: Address;

    /**
     * The address of the bottom-right cell in the range.
     */
    a2: Address;

    // constructor ------------------------------------------------------------

    constructor(a1: Address, a2?: Address) {
        this.a1 = a1;
        this.a2 = a2 ?? a1.clone();
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns a unique string key for this cell range address that can be used
     * for example as key in an associative map. This method is faster than the
     * method `toString()`.
     *
     * @returns
     *  A unique string key for this cell range address.
     */
    get key(): string {
        return rangeKey(this.a1, this.a2);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a deep clone of this cell range address.
     *
     * @returns
     *  A deep clone of this range address.
     */
    clone(): Range {
        return new Range(this.a1.clone(), this.a2.clone());
    }

    /**
     * Returns whether the passed cell range address is equal to this cell
     * range address.
     *
     * @param range
     *  The other cell range address to be compared to this cell range address.
     *
     * @returns
     *  Whether both cell range addresses contain the same start and end cell
     *  addresses.
     */
    equals(range: Range): boolean {
        return this.a1.equals(range.a1) && this.a2.equals(range.a2);
    }

    /**
     * Returns whether the passed cell range address differs from this cell
     * range address.
     *
     * @param range
     *  The other cell range address to be compared to this cell range address.
     *
     * @returns
     *  Whether the passed cell range address differs from this cell range
     *  address.
     */
    differs(range: Range): boolean {
        return this.a1.differs(range.a1) || this.a2.differs(range.a2);
    }

    /**
     * Compares this cell range address with the passed cell range address. See
     * static method Range.compare() for details.
     *
     * @param range
     *  The other cell range address to be compared to this cell range address.
     *
     * @returns
     *  The relation of this range address and the passed range address.
     */
    compareTo(range: Range): Relation {
        return Range.compare(this, range);
    }

    /**
     * Returns the number of columns covered by this cell range address.
     *
     * @returns
     *  The number of columns covered by this cell range address.
     */
    cols(): number {
        return this.a2.c - this.a1.c + 1;
    }

    /**
     * Returns the number of rows covered by this cell range address.
     *
     * @returns
     *  The number of rows covered by this cell range address.
     */
    rows(): number {
        return this.a2.r - this.a1.r + 1;
    }

    /**
     * Returns the number of columns or rows covered by this cell range
     * address.
     *
     * @param columns
     *  Whether to return the number of columns (`true`), or the number of rows
     *  (`false`) in this cell range address.
     *
     * @returns
     *  The number of columns or rows in this cell range address.
     */
    size(columns: boolean): number {
        return columns ? this.cols() : this.rows();
    }

    /**
     * Returns the number of cells covered by this cell range address.
     *
     * @returns
     *  The number of cells covered by this cell range address.
     */
    cells(): number {
        return this.cols() * this.rows();
    }

    /**
     * Returns whether this cell range address covers a single column (column
     * indexes in start address and end address are equal).
     *
     * @returns
     *  Whether this cell range address covers a single column.
     */
    singleCol(): boolean {
        return this.a1.c === this.a2.c;
    }

    /**
     * Returns whether this cell range address covers a single row (row indexes
     * in start address and end address are equal).
     *
     * @returns
     *  Whether this cell range address covers a single row.
     */
    singleRow(): boolean {
        return this.a1.r === this.a2.r;
    }

    /**
     * Returns whether this cell range address covers a single column or row.
     *
     * @param columns
     *  Whether to check this cell range address for a single column (`true`),
     *  or for a single row (`false`).
     *
     * @returns
     *  Whether this cell range address covers a single column or row.
     */
    singleLine(columns: boolean): boolean {
        return columns ? this.singleCol() : this.singleRow();
    }

    /**
     * Returns whether this cell range address covers a single cell (start
     * address and end address are equal).
     *
     * @returns
     *  Whether this cell range address covers a single cell.
     */
    single(): boolean {
        return this.a1.equals(this.a2);
    }

    /**
     * Returns whether the size (width AND height) of this range address is
     * equal to the size of the passed range address.
     *
     * @param range
     *  The range address to be checked.
     *
     * @returns
     *  Whether this range address has the same size as the passed range
     *  address.
     */
    equalSize(range: Range): boolean {
        return (this.cols() === range.cols()) && (this.rows() === range.rows());
    }

    /**
     * Returns whether the start AND end column indexes of this range address
     * are equal to the column indexes of the passed range address.
     *
     * @param range
     *  The range address to be checked.
     *
     * @returns
     *  Whether this range address has the same column indexes as the passed
     *  range address.
     */
    equalCols(range: Range): boolean {
        return (this.a1.c === range.a1.c) && (this.a2.c === range.a2.c);
    }

    /**
     * Returns whether the start AND end row indexes of this range address are
     * equal to the row indexes of the passed range address.
     *
     * @param range
     *  The range address to be checked.
     *
     * @returns
     *  Whether this range address has the same row indexes as the passed range
     *  address.
     */
    equalRows(range: Range): boolean {
        return (this.a1.r === range.a1.r) && (this.a2.r === range.a2.r);
    }

    /**
     * Return whether this cell range address fits completely into the passed
     * cell range address, either once or repeatedly in either direction.
     *
     * @param range
     *  The other cell range address to be checked.
     *
     * @returns
     *  Whether this cell range address fits completely into the passed cell
     *  range address, either once or repeatedly in either direction.
     */
    sizeFitsInto(range: Range): boolean {
        return (range.cols() % this.cols() === 0) && (range.rows() % this.rows() === 0);
    }

    /**
     * Returns whether this cell range address starts at the specified cell
     * address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether this cell range address starts at the specified cell address.
     */
    startsAt(address: Address): boolean {
        return this.a1.equals(address);
    }

    /**
     * Returns whether this cell range address ends at the specified cell
     * address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether this cell range address ends at the specified cell address.
     */
    endsAt(address: Address): boolean {
        return this.a2.equals(address);
    }

    /**
     * Returns the linear row-wise index of the passed address inside this
     * range. The top-left cell has index 0, the next cell in the first row has
     * index 1, and so on.
     *
     * @param address
     *  The address whose linear index will be returned. MUST be located inside
     *  this range.
     *
     * @returns
     *  The zero-based linear index of the passed address. The result is
     *  undefined, if the address is not located inside this range.
     */
    indexAt(address: Address): number {
        return (address.r - this.a1.r) * this.cols() + (address.c - this.a1.c);
    }

    /**
     * Returns the address representing the passed linear row-wise index inside
     * this range. The top-left cell has index 0, the next cell in the first
     * row has index 1, and so on.
     *
     * @param index
     *  The zero-based linear index. The result is undefined, if the index is
     *  not located inside this range.
     *
     * @returns
     *  The address representing the passed linear index.
     */
    addressAt(index: number): Address {
        const cols = this.cols();
        return new Address(this.a1.c + floor(index % cols), this.a1.r + floor(index / cols));
    }

    /**
     * Returns whether this cell range address contains the specified column
     * index.
     *
     * @param col
     *  The zero-based column index to be checked.
     *
     * @returns
     *  Whether this cell range address contains the specified column index.
     */
    containsCol(col: number): boolean {
        return (this.a1.c <= col) && (col <= this.a2.c);
    }

    /**
     * Returns whether this cell range address contains the specified row
     * index.
     *
     * @param row
     *  The zero-based row index to be checked.
     *
     * @returns
     *  Whether this cell range address contains the specified row index.
     */
    containsRow(row: number): boolean {
        return (this.a1.r <= row) && (row <= this.a2.r);
    }

    /**
     * Returns whether this cell range address contains the specified column or
     * row index.
     *
     * @param index
     *  The zero-based column or row index to be checked.
     *
     * @param columns
     *  Whether to check the column index (`true`), or the row index (`false`).
     *
     * @returns
     *  Whether this cell range address contains the specified column or row
     *  index.
     */
    containsIndex(index: number, columns: boolean): boolean {
        return columns ? this.containsCol(index) : this.containsRow(index);
    }

    /**
     * Returns whether this cell range address contains the specified cell
     * address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether this cell range address contains the specified cell address.
     */
    containsAddress(address: Address): boolean {
        return this.containsCol(address.c) && this.containsRow(address.r);
    }

    /**
     * Returns whether the specified cell is a border cell of this cell range
     * address (a cell located in the leading or trailing column, or in the top
     * or bottom row).
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether the specified cell is a border cell of this cell range address.
     */
    isBorderAddress(address: Address): boolean {
        return this.containsAddress(address) &&
            ((this.a1.c === address.c) || (this.a2.c === address.c) || (this.a1.r === address.r) || (this.a2.r === address.r));
    }

    /**
     * Returns whether this cell range address contains the passed cell range
     * address completely.
     *
     * @param range
     *  The other cell range address to be checked.
     *
     * @returns
     *  Whether this cell range address contains the passed cell range address.
     */
    contains(range: Range): boolean {
        return this.containsAddress(range.a1) && this.containsAddress(range.a2);
    }

    /**
     * Returns whether this cell range address overlaps with the passed cell
     * range address by at least one cell.
     *
     * @param range
     *  The other cell range address to be checked.
     *
     * @returns
     *  Whether this cell range address overlaps with the passed cell range
     *  address by at least one cell.
     */
    overlaps(range: Range): boolean {
        return (this.a1.c <= range.a2.c) && (range.a1.c <= this.a2.c) && (this.a1.r <= range.a2.r) && (range.a1.r <= this.a2.r);
    }

    /**
     * Returns the column interval covered by this cell range address.
     *
     * @returns
     *  The column interval covered by this cell range address.
     */
    colInterval(): Interval {
        return new Interval(this.a1.c, this.a2.c);
    }

    /**
     * Returns the row interval covered by this cell range address.
     *
     * @returns
     *  The row interval covered by this cell range address.
     */
    rowInterval(): Interval {
        return new Interval(this.a1.r, this.a2.r);
    }

    /**
     * Returns the column or row interval covered by this cell range address.
     *
     * @param columns
     *  Whether to return the column interval (`true`), or the row interval
     *  (`false`) of this cell range address.
     *
     * @returns
     *  The column or row interval covered by this cell range address.
     */
    interval(columns: boolean): Interval {
        return columns ? this.colInterval() : this.rowInterval();
    }

    /**
     * Returns the address of the specified columns in this cell range address.
     *
     * @param index
     *  The relative column index (the value zero will return the address of
     *  the leading column in this range).
     *
     * @param [count=1]
     *  The number of columns covered by the resulting range.
     *
     * @returns
     *  The address of the specified columns in this cell range address.
     */
    colRange(index: number, count = 1): Range {
        const col = this.a1.c + index;
        return Range.fromIndexes(col, this.a1.r, col + count - 1, this.a2.r);
    }

    /**
     * Returns the address of the specified rows in this cell range address.
     *
     * @param index
     *  The relative row index (the value zero will return the address of the
     *  top row in this range).
     *
     * @param [count=1]
     *  The number of rows covered by the resulting range.
     *
     * @returns
     *  The address of the specified rows in this cell range address.
     */
    rowRange(index: number, count = 1): Range {
        const row = this.a1.r + index;
        return Range.fromIndexes(this.a1.c, row, this.a2.c, row + (count || 1) - 1);
    }

    /**
     * Returns the address of the specified columns or rows in this cell range
     * address.
     *
     * @param columns
     *  Whether to return a column range (`true`), or a row range (`false`) of
     *  this cell range address.
     *
     * @param index
     *  The relative column/row index (the value zero will return the address
     *  of the leading column or top row in this range).
     *
     * @param [count=1]
     *  The number of columns or rows covered by the resulting range.
     *
     * @returns
     *  The address of the specified columns/rows in this cell range address.
     */
    lineRange(columns: boolean, index: number, count = 1): Range {
        return columns ? this.colRange(index, count) : this.rowRange(index, count);
    }

    /**
     * Returns the address of the leading column in this cell range address.
     *
     * @returns
     *  The address of the leading column in this cell range address.
     */
    leadingCol(): Range {
        return Range.fromIndexes(this.a1.c, this.a1.r, this.a1.c, this.a2.r);
    }

    /**
     * Returns the address of the trailing column in this cell range address.
     *
     * @returns
     *  The address of the trailing column in this cell range address.
     */
    trailingCol(): Range {
        return Range.fromIndexes(this.a2.c, this.a1.r, this.a2.c, this.a2.r);
    }

    /**
     * Returns the address of the top row in this cell range address.
     *
     * @returns
     *  The address of the top row in this cell range address.
     */
    headerRow(): Range {
        return Range.fromIndexes(this.a1.c, this.a1.r, this.a2.c, this.a1.r);
    }

    /**
     * Returns the address of the bottom row in this cell range address.
     *
     * @returns
     *  The address of the bottom row in this cell range address.
     */
    footerRow(): Range {
        return Range.fromIndexes(this.a1.c, this.a2.r, this.a2.c, this.a2.r);
    }

    /**
     * Returns a copy of this cell range address with switched column and row
     * indexes.
     *
     * @returns
     *  A copy of this cell range address with switched column and row indexes.
     */
    mirror(): Range {
        return Range.fromIndexes(this.a1.r, this.a1.c, this.a2.r, this.a2.c);
    }

    /**
     * Returns the address of the bounding cell range of this cell range, and
     * the passed cell address (the smallest range that contains this range,
     * and the cell address).
     *
     * @param address
     *  The cell address to create the bounding range from.
     *
     * @returns
     *  The address of the bounding cell range of this cell range, and the
     *  passed cell address.
     */
    expand(address: Address): Range {
        return Range.fromIndexes(
            min(this.a1.c, address.c),
            min(this.a1.r, address.r),
            max(this.a2.c, address.c),
            max(this.a2.r, address.r)
        );
    }

    /**
     * Returns the address of the bounding cell range of this cell range, and
     * the passed cell range address (the smallest range that contains both
     * ranges).
     *
     * @param range
     *  The other cell range address to create the bounding range from.
     *
     * @returns
     *  The address of the bounding cell range of this cell range, and the
     *  passed cell range address.
     */
    boundary(range: Range): Range {
        return Range.fromIndexes(
            min(this.a1.c, range.a1.c),
            min(this.a1.r, range.a1.r),
            max(this.a2.c, range.a2.c),
            max(this.a2.r, range.a2.r)
        );
    }

    /**
     * Returns the intersecting cell range address between this cell range
     * address, and the passed cell range address.
     *
     * @param range
     *  The other cell range address.
     *
     * @returns
     *  The address of the cell range covered by both cell range addresses, if
     *  existing; otherwise null.
     */
    intersect(range: Range): Range | null {
        return this.overlaps(range) ? Range.fromIndexes(
            max(this.a1.c, range.a1.c),
            max(this.a1.r, range.a1.r),
            min(this.a2.c, range.a2.c),
            min(this.a2.r, range.a2.r)
        ) : null;
    }

    /**
     * Returns the column or row index of the start cell of this cell range
     * address.
     *
     * @param columns
     *  Whether to return the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  The column or row index of the start cell of this cell range address.
     */
    getStart(columns: boolean): number {
        return this.a1.get(columns);
    }

    /**
     * Returns the column or row index of the end cell of this cell range
     * address.
     *
     * @param columns
     *  Whether to return the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  The column or row index of the end cell of this cell range address.
     */
    getEnd(columns: boolean): number {
        return this.a2.get(columns);
    }

    /**
     * Changes the column or row index of the start cell of this cell range
     * address.
     *
     * @param index
     *  The new column or row index for the start cell.
     *
     * @param columns
     *  Whether to change the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  A reference to this instance.
     */
    setStart(index: number, columns: boolean): this {
        this.a1.set(index, columns);
        return this;
    }

    /**
     * Changes the column or row index of the end cell of this cell range
     * address.
     *
     * @param index
     *  The new column or row index for the end cell.
     *
     * @param columns
     *  Whether to change the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  A reference to this instance.
     */
    setEnd(index: number, columns: boolean): this {
        this.a2.set(index, columns);
        return this;
    }

    /**
     * Changes the column or row index of the start cell, AND the end cell of
     * this cell range address.
     *
     * @param index
     *  The new column or row index for the start cell, and the end cell.
     *
     * @param columns
     *  Whether to change the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  A reference to this instance.
     */
    setBoth(index: number, columns: boolean): this {
        this.a1.set(index, columns);
        this.a2.set(index, columns);
        return this;
    }

    /**
     * Modifies the column or row index of the start cell, AND the end cell of
     * this cell range address.
     *
     * @param distance
     *  The distance to move the column or row indexes of this cell range.
     *  Negative values will decrease the indexes.
     *
     * @param columns
     *  Whether to change the column indexes (`true`), or the row indexes
     *  (`false`).
     *
     * @returns
     *  A reference to this instance.
     */
    moveBoth(distance: number, columns: boolean): this {
        this.a1.move(distance, columns);
        this.a2.move(distance, columns);
        return this;
    }

    /**
     * Modifies the column and row indexes of both cell range addresses.
     *
     * @param cols
     *  The distance to move the column indexes of this cell range. Negative
     *  values will decrease the indexes.
     *
     * @param rows
     *  The distance to move the row indexes of this cell range. Negative
     *  values will decrease the indexes.
     *
     * @returns
     *  A reference to this instance.
     */
    move(cols: number, rows: number): this {
        this.moveBoth(cols, true);
        this.moveBoth(rows, false);
        return this;
    }

    /**
     * Creates an iterator that visits the column indexes covered by this cell
     * range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new iterator.
     */
    colIndexes(options?: IntervalIteratorOptions): IterableIterator<number> {
        return itr.interval(this.a1.c, this.a2.c, options?.reverse);
    }

    /**
     * Creates an iterator that visits the row indexes covered by this cell
     * range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new iterator.
     */
    rowIndexes(options?: IntervalIteratorOptions): IterableIterator<number> {
        return itr.interval(this.a1.r, this.a2.r, options?.reverse);
    }

    /**
     * Creates an iterator that visits the column or row indexes covered by
     * this cell range address.
     *
     * @param columns
     *  Whether to iterate the column indexes (`true`), or the row indexes
     *  (`false`).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new iterator.
     */
    indexes(columns: boolean, options?: IntervalIteratorOptions): IterableIterator<number> {
        return columns ? this.colIndexes(options) : this.rowIndexes(options);
    }

    /**
     * Creates an iterator that visits the cell addresses in this cell range
     * row-by-row, or column-by-column.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The cell addresses.
     */
    *addresses(options?: RangeIteratorOptions): IterableIterator<Address> {
        const columns = !!options?.columns;
        for (const index1 of this.indexes(columns, options)) {
            for (const index2 of this.indexes(!columns, options)) {
                yield columns ? new Address(index1, index2) : new Address(index2, index1);
            }
        }
    }

    /**
     * Returns the A1 notation of this cell range address as used in document
     * operations.
     *
     * @returns
     *  The A1 notation of this cell range address as used in document
     *  operations.
     */
    toOpStr(): string {
        const a1str = this.a1.toOpStr();
        return this.single() ? a1str : `${a1str}:${this.a2.toOpStr()}`;
    }

    /**
     * Returns the string representation of this cell range address, in
     * upper-case A1 notation.
     *
     * @returns
     *  The string representation of this cell range address, in upper-case A1
     *  notation.
     */
    toString(): string {
        return `${this.a1}:${this.a2}`;
    }
}
