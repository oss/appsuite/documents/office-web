/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * A callback function that converts the value of a source iterator to a
 * numeric synchronization offset. The parallel iterator will collect values of
 * different data sources with the same synchronization offset.
 *
 * @param value
 *  The current value of the source iterator.
 *
 * @param index
 *  The array index of the source iterator providing the passed value.
 *
 * @returns
 *  A finite number representing the synchronization offset of the value. The
 *  offsets returned from this function MUST grow strictly per source iterator
 *  for all its results.
 */
export type ParallelIteratorOffsetFn<VT> = (value: VT, index: number) => number;

/**
 * The value type of a parallel iterator.
 */
export interface ParallelIteratorValue<VT> {

    /**
     * The current values collected from the source iterators.
     *
     * Array elements will be set to `undefined` for source iterators that did
     * not provide a result object for the current synchronization offset
     * contained in the property `offset`. At least one element in this array
     * will be an existing value from a source iterator (i.e., synchronization
     * offsets not provided by any source iterator will be skipped).
     */
    values: Array<Opt<VT>>;

    /**
     * The current synchronization offset matching all source values in
     * `values` that are not `undefined`.
     */
    offset: number;

    /**
     * Whether all data sources have provided a value for the current
     * synchronization offset (i.e. whether all elements in `values` have been
     * filled from the source iterators and are not `undefined`).
     */
    complete: boolean;

    /**
     * The first element in `values` that is not `undefined`.
     */
    first: VT;
}

// public functions ===========================================================

/**
 * An iterator that visits the results provided by the passed source iterators
 * simultaneously, according to some offset or sorting index for the results of
 * all iterators.
 *
 * The parallel iterator will visit the result objects of the source iterators
 * simultaneously. The iterator will provide all results of the source
 * iterators that have an equal sorting index, and will pass the value `null`
 * as result for all iterators that have skipped that sorting index.
 *
 * @param sources
 *  An arbitrary number of source iterators to be visited.
 *
 * @param offsetFn
 *  A callback function that converts the results of the source iterators to a
 *  numeric offset or sorting index.
 *
 * @yields
 *  The entries of the source iterators.
 */
export function *iterateParallelSynchronized<VT>(sources: Array<Iterable<VT>>, offsetFn: ParallelIteratorOffsetFn<VT>): IterableIterator<ParallelIteratorValue<VT>> {

    // a descriptor for every data source, with iterator and current value/offset
    class IteratorState {
        readonly iterator: Iterator<VT>;
        readonly index: number;
        value: Opt<VT>;
        offset = 0;
        constructor(iterator: Iterator<VT>, index: number) {
            this.iterator = iterator;
            this.index = index;
            this.next();
        }
        next(): void {
            const result = this.iterator.next();
            this.value = result.done ? undefined : result.value;
            this.offset = result.done ? Infinity : offsetFn(this.value!, this.index);
        }
    }

    // initialize the iterators for all data sources
    const states = sources.map((source, index) => new IteratorState(source[Symbol.iterator](), index));

    // next offset to be visited
    let offset = states.reduce((min, state) => Math.min(min, state.offset), Infinity);

    // process until all iterators are done
    while (Number.isFinite(offset)) {

        // the properties to be inserted into the next iterator value object
        const values: Array<Opt<VT>> = [];
        let complete = true;
        let first = Infinity;

        // offset to be visited in next iteration step (minimum of all offsets greater than `currOffset`)
        let nextOffset = Infinity;

        // collect the results into an array, and step ahead all affected iterators
        for (const state of states) {

            // skip current iterator, if it is done, or if offset is greater than the current offset
            if ((state.value === undefined) || (offset < state.offset)) {
                values.push(undefined);
                complete = false;
            } else {
                // add the value of the current iterator to the state
                values.push(state.value);
                first = Math.min(first, state.index);
                // step the iterator ahead
                state.next();
            }

            // update next offset to be visited
            nextOffset = Math.min(nextOffset, state.offset);
        }

        // yield the result object
        yield { values, offset, complete, first: values[first]! };

        // store new offset for next iteration
        offset = nextOffset;
    }
}
