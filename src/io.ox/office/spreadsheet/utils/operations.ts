/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import type { OTSortVector } from "@/io.ox/office/editframework/utils/otutils";
import type { Position, Operation, OptAttrsOperation } from "@/io.ox/office/textframework/utils/operations";
import type { SheetType, Direction, MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

// re-exports =================================================================

export * from "@/io.ox/office/textframework/utils/operations";

// types ======================================================================

/**
 * The operation "insertNumberFormat" to register a new number format code.
 */
export interface InsertNumberFormatOperation extends Operation {
    name: typeof INSERT_NUMBER_FORMAT;
    id: number;
    code: string;
}

/**
 * The operation "deleteNumberFormat" to delete an existing number format code.
 */
export interface DeleteNumberFormatOperation extends Operation {
    name: typeof DELETE_NUMBER_FORMAT;
    id: number;
}

/**
 * Base interface for all operations containing a `sheet` property with a sheet
 * index.
 */
export interface SheetOperation extends Operation {
    sheet: number;
}

/**
 * The operation "insertSheet" to insert a new sheet into the document.
 */
export interface InsertSheetOperation extends SheetOperation, OptAttrsOperation {
    name: typeof INSERT_SHEET;
    type?: SheetType;
    sheetName: string;
}

/**
 * The operation "deleteSheet" to delete an existing sheet from the document.
 */
export interface DeleteSheetOperation extends SheetOperation {
    name: typeof DELETE_SHEET;
}

/**
 * A sheet operation with a sheet index property `to`.
 */
export interface TargetSheetOperation extends SheetOperation {
    to: number;
}

/**
 * The operation "moveSheet" to move an existing sheet to a new position.
 */
export interface MoveSheetOperation extends TargetSheetOperation {
    name: typeof MOVE_SHEET;
}

/**
 * The operation "moveSheets" to move multiple sheets to new positions.
 */
export interface MoveSheetsOperation extends Operation {
    name: typeof MOVE_SHEETS;
    sheets: OTSortVector;
}

/**
 * The operation "copySheet" to create a copy of an existing sheet.
 */
export interface CopySheetOperation extends TargetSheetOperation {
    name: typeof COPY_SHEET;
    sheetName: string;
    tableNames?: Dict<string>;
}

/**
 * The operation "changeSheet" to change the name or the attributes of a sheet.
 */
export interface ChangeSheetOperation extends SheetOperation, OptAttrsOperation {
    name: typeof CHANGE_SHEET;
    sheetName?: string;
}

/**
 * The type of a cell value in "changeCells" operations, except error codes.
 */
export type CellDataValueType = number | string | boolean | null;

/**
 * The contents descriptor for a single cell in a "changeCells" operation.
 */
export interface CellData {

    /**
     * If set to `true`, an existing cell must be undefined (i.e., physically
     * removed) first, before applying any other property of this object.
     */
    u?: true;

    /**
     * The scalar cell value (or formula result) of the cell. The value `null`
     * represents a blank cell. Formula cells must return something, and
     * therefore cannot be blank. MUST NOT be used together with the property
     * `e` containing an error code.
     */
    v?: CellDataValueType;

    /**
     * The error code value (or formula result) of the cell (error code name
     * with leading hash character, e.g. `"#VALUE!"`). MUST NOT be used
     * together with the property `v` containing a regular scalar value.
     */
    e?: string;

    /**
     * The formula expression of a formula cell, without leading equality sign,
     * in the native formula syntax of the current file format (NOT translated
     * to the UI language). The value `null` removes an existing formula from
     * the cell.
     */
    f?: string | null;

    /**
     * The index of a shared formula that will be reused in multiple cells
     * (with relocated cell references). If the index appears the first time,
     * the property `f` MUST contain the shared formula expression. The value
     * `null` removes a shared formula index from the cell.
     */
    si?: number | null;

    /**
     * The position of the bounding range containing all cells of a shared
     * formula, as range address in A1 style. MUST be specified when the shared
     * formula will be defined the first time, using the properties `f` and
     * `si`. The value `null` removes a bounding range from the cell.
     */
    sr?: string | null;

    /**
     * The position of the matrix formula starting at this cell, as range
     * address in A1 style. MUST be specified for the top-left cell of the
     * matrix formula only. The property `f` contains the formula expression of
     * the matrix formula. The value `null` removes a matrix range from the
     * cell.
     */
    mr?: string | null;

    /**
     * The dynamic state of the matrix formula starting at this cell. Has no
     * effect, if no matrix formula start sin the cell.
     */
    md?: boolean;

    /**
     * The style identifier of the cell auto-style containing the formatting
     * attributes for the cell. The empty string can be used for the default
     * auto-style of the document.
     */
    s?: string;
}

export type CellDataOrValue = CellData | CellDataValueType;

export type CellDataDict = Dict<CellDataOrValue>;

/**
 * The operation "changeCells" to change cell contents or formatting.
 */
export interface ChangeCellsOperation extends SheetOperation {
    name: typeof CHANGE_CELLS;
    contents: CellDataDict;
}

/**
 * Base interface for spreadsheet operations with an interval list.
 */
export interface IntervalListOperation extends SheetOperation {
    intervals: string;
}

/**
 * Formatting properties for spreadsheet operations changing an interval list
 * (multiple columns or rows).
 */
export interface ChangeIntervalsProps {
    attrs?: Dict;
    s?: string;
}

/**
 * Base interface for spreadsheet operations with an interval list, an optional
 * attribute set, and an optional cell auto-style identifier.
 */
export interface ChangeIntervalsOperation extends IntervalListOperation, ChangeIntervalsProps { }

/**
 * The operation "insertColumns" to insert new empty columns.
 */
export interface InsertColumnsOperation extends ChangeIntervalsOperation {
    name: typeof INSERT_COLUMNS;
}

/**
 * The operation "deleteColumns" to delete existing columns.
 */
export interface DeleteColumnsOperation extends IntervalListOperation {
    name: typeof DELETE_COLUMNS;
}

/**
 * The operation "changeColumns" to change the formatting of columns.
 */
export interface ChangeColumnsOperation extends ChangeIntervalsOperation {
    name: typeof CHANGE_COLUMNS;
}

/**
 * The operation "insertRows" to insert new empty rows.
 */
export interface InsertRowsOperation extends ChangeIntervalsOperation {
    name: typeof INSERT_ROWS;
}

/**
 * The operation "deleteRows" to delete existing rows.
 */
export interface DeleteRowsOperation extends IntervalListOperation {
    name: typeof DELETE_ROWS;
}

/**
 * The operation "changeRows" to change the formatting of rows.
 */
export interface ChangeRowsOperation extends ChangeIntervalsOperation {
    name: typeof CHANGE_ROWS;
}

/**
 * Base interface for spreadsheet operations with a range list.
 */
export interface RangeListOperation extends SheetOperation {
    ranges: string;
}

/**
 * The operation "moveCells" to move a cell range.
 */
export interface MoveCellsOperation extends SheetOperation {
    name: typeof MOVE_CELLS;
    range: string;
    dir: Direction;
}

/**
 * The operation "mergeCells" to merge/unmerge cell ranges.
 */
export interface MergeCellsOperation extends RangeListOperation {
    name: typeof MERGE_CELLS;
    type?: MergeMode;
}

/**
 * The operation "insertHyperlink" to insert a hyperlink into cell ranges.
 */
export interface InsertHlinkOperation extends RangeListOperation {
    name: typeof INSERT_HYPERLINK;
    url: string;
}

/**
 * The operation "deleteHyperlink" to delete a hyperlink from cell ranges.
 */
export interface DeleteHlinkOperation extends RangeListOperation {
    name: typeof DELETE_HYPERLINK;
}

/**
 * Base interface for operations for a named formula expression.
 */
export interface NameOperation extends Operation {
    sheet?: number;
    label: string;
}

/**
 * Shared properties for the formula expression of defined name operations.
 */
export interface NameOperationFormulaProps {
    formula: string;
    ref?: string;
    isExpr?: boolean;
}

/**
 * The operation "insertName" to insert a named formula expression.
 */
export interface InsertNameOperation extends NameOperation, NameOperationFormulaProps {
    name: typeof INSERT_NAME;
    hidden?: boolean;
}

/**
 * The operation "deleteName" to delete a named formula expression.
 */
export interface DeleteNameOperation extends NameOperation {
    name: typeof DELETE_NAME;
}

/**
 * The operation "changeName" to change a named formula expression.
 */
export interface ChangeNameOperation extends NameOperation, Partial<NameOperationFormulaProps> {
    name: typeof CHANGE_NAME;
    newLabel?: string;
}

/**
 * Base interface for operations for table ranges.
 */
export interface TableOperation extends SheetOperation {
    table?: string;
}

/**
 * The operation "insertTable" to insert a new table range.
 */
export interface InsertTableOperation extends TableOperation, OptAttrsOperation {
    name: typeof INSERT_TABLE;
    range: string;
    headers?: string[];
}

/**
 * The operation "deleteTable" to delete a table range.
 */
export interface DeleteTableOperation extends TableOperation {
    name: typeof DELETE_TABLE;
}

/**
 * The operation "changeTable" to change the location or attributes of a table
 * range.
 */
export interface ChangeTableOperation extends TableOperation, OptAttrsOperation {
    name: typeof CHANGE_TABLE;
    newName?: string;
    range?: string;
    headers?: string[];
}

/**
 * The operation "changeTableColumn" to change the attributes of a column in a
 * table range.
 */
export interface ChangeTableColumnOperation extends TableOperation, OptAttrsOperation {
    name: typeof CHANGE_TABLE_COLUMN;
    col: number;
    sort?: boolean;
}

/**
 * Base interface for operations for data validation rules.
 */
export interface DVRuleOperation extends SheetOperation {
    index?: number; // used in OOXML to address the rule
    ranges?: string; // used in ODF to address the rule
}

/**
 * The configuration attributes of a data validation rule.
 */
export interface DVRuleAttributes {
    type?: string;
    compare?: string;
    value1?: string;
    value2?: string;
    showInfo?: boolean;
    infoTitle?: string;
    infoText?: string;
    showError?: boolean;
    errorTitle?: string;
    errorText?: string;
    errorType?: string;
    showDropDown?: boolean;
    ignoreEmpty?: boolean;
}

/**
 * The operation "insertDVRule" that inserts a new data validation rule.
 */
export interface InsertDVRuleOperation extends DVRuleOperation, DVRuleAttributes {
    name: typeof INSERT_DVRULE;
    ranges: string; // ranges required for inserting a rule (also OOXML)
    ref?: string;
}

/**
 * The operation "deleteDVRule" that deletes an existing data validation rule.
 */
export interface DeleteDVRuleOperation extends DVRuleOperation {
    name: typeof DELETE_DVRULE;
}

/**
 * The operation "changeDVRule" that changes an existing data validation rule.
 */
export interface ChangeDVRuleOperation extends DVRuleOperation, DVRuleAttributes {
    name: typeof CHANGE_DVRULE;
    ref?: string;
}

/**
 * Base interface for operations for conditional formatting rules.
 */
export interface CFRuleOperation extends SheetOperation {
    id: string;
}

/**
 * The configuration attributes of a conditional formatting rule.
 */
export interface CFRuleAttributes {
    type?: string;
    value1?: string;
    value2?: string;
    colorScale?: Dict[];
    dataBar?: Dict;
    iconSet?: Dict;
    priority?: number;
    stop?: boolean;
    attrs?: Dict;
}

/**
 * The operation "insertCFRule" that inserts a new conditional formatting rule.
 */
export interface InsertCFRuleOperation extends CFRuleOperation, CFRuleAttributes {
    name: typeof INSERT_CFRULE;
    ranges: string;
    ref?: string;
}

/**
 * The operation "deleteCFRule" that deletes an existing conditional formatting
 * rule.
 */
export interface DeleteCFRuleOperation extends CFRuleOperation {
    name: typeof DELETE_CFRULE;
}

/**
 * The operation "changeCFRule" that changes an existing conditional formatting
 * rule.
 */
export interface ChangeCFRuleOperation extends CFRuleOperation, CFRuleAttributes {
    name: typeof CHANGE_CFRULE;
    ranges?: string;
    ref?: string;
}

/**
 * Base interface for all operations containing an `anchor` property with a
 * cell address for a cell note or comment anchored to that cell.
 */
export interface CellAnchorOperation extends SheetOperation {

    /**
     * The anchor address of the cell note or comment.
     */
    anchor: string;
}

/**
 * Base interface for all operations containing `from` and `to` properties with
 * cell address lists for moving cell notes or comments.
 */
export interface MoveAnchorsOperation extends SheetOperation {
    from: string;
    to: string;
}

/**
 * The operation "insertNote" that inserts a new note shape anchored to a cell.
 */
export interface InsertNoteOperation extends CellAnchorOperation, OptAttrsOperation {
    name: typeof INSERT_NOTE;
    text?: string;
    date?: string;
    author?: string;
}

/**
 * The operation "deleteNote" that deletes a note shape anchored to a cell.
 */
export interface DeleteNoteOperation extends CellAnchorOperation {
    name: typeof DELETE_NOTE;
}

/**
 * The operation "changeNote" that changes the text or formatting of a note
 * shape anchored to a cell.
 */
export interface ChangeNoteOperation extends CellAnchorOperation, OptAttrsOperation {
    name: typeof CHANGE_NOTE;
    text?: string;
}

/**
 * The operation "moveNotes" that moves multiple cell note shapes to new anchor
 * cells.
 */
export interface MoveNotesOperation extends MoveAnchorsOperation {
    name: typeof MOVE_NOTES;
}

/**
 * Base interface for all cell comment operations containing an `anchor`
 * property with a cell address of a comment thread, and an integral index for
 * a single comment in the thread.
 */
export interface CommentOperation extends CellAnchorOperation {
    index: number;
}

/**
 * The operation "insertComment" that creates a new comment anchored to a cell,
 * or adds a reply to a comment thread.
 */
export interface InsertCommentOperation extends CommentOperation, OptAttrsOperation {
    name: typeof INSERT_COMMENT;
    text?: string;
    mentions?: unknown[];
    done?: boolean;
}

/**
 * The operation "deleteComment" that deletes a reply from a comment thread, or
 * an entire comment thread from a cell.
 */
export interface DeleteCommentOperation extends CommentOperation {
    name: typeof DELETE_COMMENT;
}

/**
 * The operation "changeComment" that changes the settings of a single comment
 * or reply.
 */
export interface ChangeCommentOperation extends CommentOperation, OptAttrsOperation {
    name: typeof CHANGE_COMMENT;
    text?: string;
    mentions?: unknown[];
    done?: boolean;
}

/**
 * The operation "moveComments" that moves multiple cell comment threads to new
 * anchor cells.
 */
export interface MoveCommentsOperation extends MoveAnchorsOperation {
    name: typeof MOVE_COMMENTS;
}

/**
 * Synthetic operation used to transform the selection state of a sheet.
 */
export interface SheetSelectionOperation extends RangeListOperation {
    name: typeof SHEET_SELECTION;
    index: number;
    active: string;
    origin?: string;
    drawings?: Position[];
}

// constants ==================================================================

export const INSERT_NUMBER_FORMAT = "insertNumberFormat";
export const DELETE_NUMBER_FORMAT = "deleteNumberFormat";

export const INSERT_SHEET = "insertSheet";
export const DELETE_SHEET = "deleteSheet";
export const CHANGE_SHEET = "changeSheet";
export const MOVE_SHEET = "moveSheet";
export const MOVE_SHEETS = "moveSheets";
export const COPY_SHEET = "copySheet";

export const CHANGE_CELLS = "changeCells";
export const MOVE_CELLS = "moveCells";
export const MERGE_CELLS = "mergeCells";

export const INSERT_HYPERLINK = "insertHyperlink";
export const DELETE_HYPERLINK = "deleteHyperlink";

export const INSERT_COLUMNS = "insertColumns";
export const DELETE_COLUMNS = "deleteColumns";
export const CHANGE_COLUMNS = "changeColumns";

export const INSERT_ROWS = "insertRows";
export const DELETE_ROWS = "deleteRows";
export const CHANGE_ROWS = "changeRows";

export const INSERT_NAME = "insertName";
export const CHANGE_NAME = "changeName";
export const DELETE_NAME = "deleteName";

export const INSERT_TABLE = "insertTable";
export const CHANGE_TABLE = "changeTable";
export const DELETE_TABLE = "deleteTable";
export const CHANGE_TABLE_COLUMN = "changeTableColumn";

export const INSERT_DVRULE = "insertDVRule";
export const CHANGE_DVRULE = "changeDVRule";
export const DELETE_DVRULE = "deleteDVRule";

export const INSERT_CFRULE = "insertCFRule";
export const CHANGE_CFRULE = "changeCFRule";
export const DELETE_CFRULE = "deleteCFRule";

export const INSERT_NOTE = "insertNote";
export const DELETE_NOTE = "deleteNote";
export const CHANGE_NOTE = "changeNote";
export const MOVE_NOTES = "moveNotes";

export const INSERT_COMMENT = "insertComment";
export const CHANGE_COMMENT = "changeComment";
export const DELETE_COMMENT = "deleteComment";
export const MOVE_COMMENTS = "moveComments";

export const SHEET_SELECTION = "sheetSelection";

// private functions ==========================================================

interface ToOpStr<AT extends unknown[]> {
    toOpStr(...args: AT): string;
}

function toOpStr<AT extends unknown[]>(obj: string | ToOpStr<AT>, ...args: AT): string {
    return is.string(obj) ? obj : obj.toOpStr(...args);
}

// public functions ===========================================================

/**
 * Returns whether the passed entry of a `contents` dictionary from a
 * "changeCells" operation is a `CellData` object (and not a plain scalar).
 *
 * @param data
 *  The dictionary entry to be checked.
 *
 * @returns
 *  Whether the passed entry is a `CellData` object (and not a plain scalar).
 */
export const isCellData = is.dict as ((data: CellDataOrValue) => data is CellData);

/**
 * Merges the passed `CellData` objects or scalar values.
 *
 * @param cellData1
 *  The `CellData` object to be extended in-place, or a scalar value to start
 *  with.
 *
 * @param cellData2
 *  The `CellData` object, or a scalar value, to be used to extend `data1`.
 *
 * @returns
 *  The extended `CellData` object.
 */
export function mergeCellData(cellData1: CellDataOrValue, cellData2: CellDataOrValue): CellData {
    if (!isCellData(cellData1)) { cellData1 = { v: cellData1 }; }
    if (isCellData(cellData2)) { Object.assign(cellData1, cellData2); } else { cellData1.v = cellData2; }
    return cellData1;
}

/**
 * Creates a "deleteSheet" JSON operation.
 */
export function deleteSheet(sheet: number): DeleteSheetOperation {
    return { name: DELETE_SHEET, sheet };
}

/**
 * Creates a "mergeCells" JSON operation.
 */
export function mergeCells(sheet: number, ranges: string | RangeArray, mergeMode: MergeMode): MergeCellsOperation {
    return { name: MERGE_CELLS, sheet, ranges: toOpStr(ranges), type: mergeMode };
}

/**
 * Creates a "deleteTable" JSON operation.
 */
export function deleteTable(sheet: number, table?: string): DeleteTableOperation {
    const op: DeleteTableOperation = { name: DELETE_TABLE, sheet };
    if (table) { op.table = table; }
    return op;
}

/**
 * Creates a "deleteDVRule" JSON operation.
 */
export function deleteDVRule(sheet: number, target: number | string): DeleteDVRuleOperation {
    const op: DeleteDVRuleOperation = { name: DELETE_DVRULE, sheet };
    if (is.number(target)) { op.index = target; } else { op.ranges = target; }
    return op;
}

/**
 * Creates a "deleteCFRule" JSON operation.
 */
export function deleteCFRule(sheet: number, id: string): DeleteCFRuleOperation {
    return { name: DELETE_CFRULE, sheet, id };
}

/**
 * Creates a "deleteNote" JSON operation.
 */
export function deleteNote(sheet: number, anchor: string | Address): DeleteNoteOperation {
    return { name: DELETE_NOTE, sheet, anchor: toOpStr(anchor) };
}

/**
 * Creates a "deleteComment" JSON operation.
 */
export function deleteComment(sheet: number, anchor: string | Address, index?: number): DeleteCommentOperation {
    return { name: DELETE_COMMENT, sheet, anchor: toOpStr(anchor), index: index ?? 0 };
}
