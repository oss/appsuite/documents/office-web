/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, dict } from "@/io.ox/office/tk/algorithms";
import type { LocaleEqualStringOptions, LocaleCompareStringOptions } from "@/io.ox/office/tk/locale";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

// types ======================================================================

/**
 * The keys of all native (built-in) error codes, as enumeration type.
 */
export type ErrorKey = keyof typeof NATIVE_ERROR_DEFINITIONS;

/**
 * All data types used for scalar values used in cells and formulas, except
 * error code literals. Scalar values are numbers, strings, boolean values, and
 * the special value `null` that represents a blank cell, or an empty function
 * parameter.
 */
export type ScalarTypeNE = number | string | boolean | null;

/**
 * All data types used for scalar values used in cells and formulas. Scalar
 * values are numbers, strings, boolean values, error code literals, and the
 * special value `null` that represents a blank cell, or an empty function
 * parameter.
 */
export type ScalarType = ScalarTypeNE | ErrorLiteral;

/**
 * Enumeration of data type identifiers of scalar values used in cells and
 * formulas.
 *
 * The values of the type identifiers represent a natural order for different
 * data types, as used in spreadsheet formulas or for sorting (for example,
 * strings are always considered to be greater than numbers).
 */
export enum ScalarTypeId {

    /**
     * Type specifier for the special value `null` representing a blank cell or
     * an empty function parameter. Used in situations when `null` values will
     * be considered to be less than any other scalar value.
     */
    NULL = 1,

    /**
     * Type specifier for floating-point numbers (including infinite numbers,
     * and NaN).
     */
    NUMBER = 2,

    /**
     * Type specifier for strings.
     */
    STRING = 3,

    /**
     * Type specifier for boolean values.
     */
    BOOLEAN = 4,

    /**
     * Type specifier for error code literals.
     */
    ERROR = 5,

    /**
     * Type specifier for the special value `null` representing a blank cell or
     * an empty function parameter. Used in situations when `null` values will
     * be considered to be greater than any other scalar value.
     */
    NULL_MAX = 6
}

/**
 * Specifies the comparison behavior of a scalar value with a `null` value.
 */
export enum CompareNullMode {

    /**
     * The `null` value will be replaced with the neutral value of the data
     * type of the other scalar value (zero, empty string, or `false`), and
     * these values will be compared. If the other value is an error code, the
     * `null` value will not be converted, but will be considered less than the
     * error code.
     */
    CONVERT = 0,

    /**
     * The `null` value is less than all other scalar values.
     */
    NULL_MIN = 1,

    /**
     * The `null` value is greater than all other scalar values.
     */
    NULL_MAX = 2
}

/**
 * Settings for comparison of scalar values.
 */
export interface CompareScalarOptions extends LocaleCompareStringOptions {

    /**
     * Specifies the comparison behavior of a scalar value with a `null` value.
     * The default value is `ScalarNullMode.CONVERT`.
     */
    nullMode?: CompareNullMode;
}

/**
 * Enumeration for the relation of two values for comparison.
 */
export enum CompareResult {

    /**
     * The first value is less than the second value.
     */
    LESS = -1,

    /**
     * The values are equal.
     */
    EQUAL = 0,

    /**
     * The first value is greater than the second value.
     */
    GREATER = 1,

    /**
     * The values could not be compared (e.g. infinite numbers).
     */
    // the enum value `NA` is set to NaN intentionally to make it not match in any comparison operation
    // eslint-disable-next-line @typescript-eslint/prefer-literal-enum-member
    NA = Number.NaN
}

// constants ==================================================================

/**
 * An enumeration with all supported built-in error codes, mapped by key, with
 * integers used e.g. for comparison in filters and sorting.
 */
const NATIVE_ERROR_DEFINITIONS = {
    NULL: 1,
    DIV0: 2,
    VALUE: 3,
    REF: 4,
    NAME: 5,
    NUM: 6,
    NA: 7,
    DATA: 8
};

// private functions ==========================================================

const { NULL, NUMBER, STRING, BOOLEAN, ERROR, NULL_MAX } = ScalarTypeId;
const { LESS, EQUAL, GREATER, NA } = CompareResult;

/**
 * Returns the neutral value for the passed scalar value.
 *
 * @param value
 *  Any scalar value used in spreadsheet formulas.
 *
 * @returns
 *  The neutral value for the passed scalar value, if available.
 *  - The number zero for any number.
 *  - The empty string for any string.
 *  - The value false for a boolean value.
 *
 * Otherwise, null will be returned (especially for error codes which do not
 * have a neutral value).
 */
function getNeutralValue(value: ScalarType): 0 | "" | false | null {
    switch (typeof value) {
        case "number":  return 0;
        case "string":  return "";
        case "boolean": return false;
        default:        return null; // null for error codes
    }
}

/**
 * Rounds the passed number to 15 significant digits.
 *
 * @param n
 *  The number to be rounded. MUST be finite.
 *
 * @returns
 *  The rounded number.
 */
function round15(n: number): number {
    const parts = math.splitNumber(n);
    return Math.round(parts.mant * 1e14) * (10 ** (parts.expn - 14));
}

/**
 * Returns whether the passed numbers are considered to be equal, allowing a
 * slight difference for equally signed numbers.
 *
 * @param n1
 *  The first number for comparison. MUST be finite.
 *
 * @param n2
 *  The second number for comparison. MUST be finite.
 *
 * @returns
 *  Whether the passed numbers are considered to be equal.
 */
function equalNumbersFinite(n1: number, n2: number): boolean {

    // shortcut: true, if the numbers are exactly equal
    if (n1 === n2) { return true; }

    // check whether at least one number is zero or denormalized
    const zero1 = math.isZero(n1);
    const zero2 = math.isZero(n2);
    if (zero1 || zero2) { return zero1 === zero2; }

    // false, if the numbers have different signs (regardless of distance)
    if ((n1 < 0) !== (n2 < 0)) { return false; }

    // performance: false, if the numbers have a decent difference
    const quot = Math.abs((n1 / n2) - 1);
    if (!Number.isFinite(quot) || (quot > 1e-14)) { return false; }

    // round the numbers to 15 significant digits for comparison
    return round15(n1) === round15(n2);
}

// class ErrorLiteral =========================================================

/**
 * Instances of this class represent scalar spreadsheet error codes, as used in
 * cells, in formula expressions, and as the result of formulas.
 *
 * Implemented as named class to be able to use the `instanceof` operator to
 * distinguish error codes from other scalar cell values.
 */
export class ErrorLiteral<KeyT extends string = string> {

    /**
     * A unique resource key for the error code (needed e.g. to find the
     * correct translation).
     */
    readonly key: KeyT;

    /**
     * The internal numeric index of built-in error codes, used for example
     * when comparing error codes in filters and filtering sheet functions. The
     * value `NaN` represents error codes that are not built-in.
     */
    readonly num: number;

    // constructor ------------------------------------------------------------

    /**
     * @param key
     *  A unique resource key for the error code (needed e.g. to find the
     *  correct translation).
     *
     * @param num
     *  The internal numeric index of built-in error codes, used for example
     *  when comparing error codes in filters and filtering sheet functions.
     *  Set to `NaN` for all error codes that are not built-in.
     */
    constructor(key: KeyT, num = Number.NaN) {
        this.key = key;
        this.num = num;
    }

    // public methods ---------------------------------------------------------

    /**
     * Throws this error code as exception. Used in the formula engine to stop
     * interpreting a formula when an error occurs somewhere inside.
     */
    throw(): never {
        throw this as unknown;
    }

    /**
     * Returns the JSON representation of this error code (a string with a
     * leading hash character).
     *
     * @returns
     *  The JSON string representation of this error code.
     */
    toJSON(): string {
        return `#${this.key}`;
    }

    /**
     * Returns the string representation of this error code for debug logging.
     *
     * _Attention:_ DO NOT USE this method for GUI related code where correct
     * locale dependent representation of the error code is required.
     *
     * @returns
     *  The string representation of this error code for debug logging.
     */
    toString(): string {
        return `#${this.key}`;
    }
}

// enum ErrorCode =============================================================

/**
 * All built-in error codes with native names, as used in operations. Intended
 * to be used like an enumeration type.
 */
export const ErrorCode = dict.mapRecord(NATIVE_ERROR_DEFINITIONS, (num, key) => new ErrorLiteral(key, num));

// public functions ===========================================================

/**
 * Returns whether the passed value is an error code.
 *
 * @param data
 *  The value to be tested.
 *
 * @returns
 *  Whether the passed value is an error code.
 */
export function isErrorCode(data: unknown): data is ErrorLiteral {
    return data instanceof ErrorLiteral;
}

/**
 * Throws the passed value, if it is an error code literal. Otherwise, the
 * passed value will be returned.
 *
 * @param value
 *  The value to be tested.
 *
 * @returns
 *  The passed value, if it is not an error code literal.
 *
 * @throws
 *  The passed error code literal.
 */
export function throwErrorCode<T>(value: T | ErrorLiteral): T {
    return isErrorCode(value) ? value.throw() : value;
}

/**
 * Returns whether the passed data is a valid scalar value used in cells.
 *
 * @param data
 *  The data to be checked.
 *
 * @returns
 *  Whether the passed data is a valid scalar value used in cells.
 */
export function isScalar(data: unknown): data is ScalarType {
    return (data === null) || is.number(data) || is.string(data) || is.boolean(data) || isErrorCode(data);
}

/**
 * Returns a numeric type identifier for the passed scalar value. The type
 * identifier represents a natural order of different data types, as used in
 * spreadsheet formulas (for example, strings are always considered to be
 * greater than numbers).
 *
 * @param value
 *  The scalar value to be checked.
 *
 * @param [nullMax=false]
 *  Whether to consider the `null` value to be greater than any other value
 *  (i.e. whether to return the type identifier `ScalarTypeId.NULL_MAX` for the
 *  `null` value). By default, the `null` value is less than any other value
 *  (i.e. the type identifier `ScalarTypeId.NULL` will be returned for `null`).
 *
 * @returns
 *  A numeric type identifier for the passed scalar value.
 */
export function getScalarType(value: ScalarType, nullMax?: boolean): ScalarTypeId {
    switch (typeof value) {
        case "number":  return NUMBER;
        case "string":  return STRING;
        case "boolean": return BOOLEAN;
        default:        return isErrorCode(value) ? ERROR : nullMax ? NULL_MAX : NULL;
    }
}

/**
 * Returns the effective result of a cell formula. If the formula evaluates to
 * the empty value (null, e.g. reference to a blank cell), the number zero will
 * be returned instead.
 *
 * @param value
 *  The scalar value to be checked.
 *
 * @returns
 *  The number zero, if the passed value is null; otherwise the passed value
 *  itself.
 */
export function getCellValue(value: ScalarType): ScalarType {
    return value ?? 0;
}

// comparison -----------------------------------------------------------------

/**
 * Returns whether the passed numbers are considered to be equal, allowing a
 * slight difference for equally signed numbers. This function may be faster
 * than the function `compareNumbers()` in some situations.
 *
 * @param n1
 *  The first number for comparison.
 *
 * @param n2
 *  The second number for comparison.
 *
 * @returns
 *  Whether the passed numbers are both finite, and considered to be equal.
 */
export function equalNumbers(n1: number, n2: number): boolean {
    // false, if one of the numbers is not finite
    return Number.isFinite(n1) && Number.isFinite(n2) && equalNumbersFinite(n1, n2);
}

/**
 * Returns whether the passed strings are considered to be equal.
 *
 * @param s1
 *  The first string for comparison.
 *
 * @param s2
 *  The second string for comparison.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether the passed strings are considered to be equal.
 */
export function equalStrings(s1: string, s2: string, options?: LocaleEqualStringOptions): boolean {
    return LOCALE_DATA.getCollator(options).equals(s1, s2);
}

/**
 * Returns whether the passed scalar values are considered to be equal. This
 * function may be faster than the function `compareScalars()` in some
 * situations.
 *
 * @param v1
 *  The first scalar value for comparison.
 *
 * @param v2
 *  The second scalar value for comparison.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether the passed scalar values are considered to be equal, according to
 *  the passed options.
 */
export function equalScalars(v1: ScalarType, v2: ScalarType, options?: CompareScalarOptions): boolean {

    // whether to convert a null value to the neutral value of the other parameter
    const convertNull = !options || !("nullMode" in options) || (options.nullMode === CompareNullMode.CONVERT);

    // convert null values if specified
    if (convertNull) {
        if (v1 === null) { v1 = getNeutralValue(v2); }
        if (v2 === null) { v2 = getNeutralValue(v1); }
    }

    // shortcut: simple check for strict equality
    if (v1 === v2) { return true; }

    // get type identifiers (values of different types are never equal)
    const type1 = getScalarType(v1);
    const type2 = getScalarType(v2);
    if (type1 !== type2) { return false; }

    // compare numbers and strings using the helper methods
    switch (type1) {
        case NUMBER: return equalNumbers(v1 as number, v2 as number);
        case STRING: return LOCALE_DATA.getCollator(options).equals(v1 as string, v2 as string);
        // equal booleans, error codes, and null values would have been found above already
        default: return false;
    }
}

/**
 * Compares the passed numbers.
 *
 * @param n1
 *  The first number for comparison.
 *
 * @param n2
 *  The second number for comparison.
 *
 * @returns
 *  An indicator for the relation of the passed numbers. If the difference of
 *  the numbers is less than `MIN_NORM_NUMBER` (e.g., due to rounding errors),
 *  the numbers are also considered to be equal. If any of the numbers is `NaN`
 *  or infinite, the result will be the special value `CompareResult.NA`.
 */
export function compareNumbers(n1: number, n2: number): CompareResult {

    // check for invalid numbers
    if (!Number.isFinite(n1) || !Number.isFinite(n2)) { return NA; }

    // return comparison specifier for the (finite) numbers
    return equalNumbersFinite(n1, n2) ? EQUAL : (n1 < n2) ? LESS : GREATER;
}

/**
 * Compares the passed strings lexicographically.
 *
 * @param s1
 *  The first string for comparison.
 *
 * @param s2
 *  The second string for comparison.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  An indicator for the relation of the passed strings.
 */
export function compareStrings(s1: string, s2: string, options?: LocaleCompareStringOptions): CompareResult {
    return LOCALE_DATA.getCollator(options).compare(s1, s2);
}

/**
 * Compares the passed boolean values.
 *
 * @param b1
 *  The first boolean value for comparison.
 *
 * @param b2
 *  The second boolean value for comparison.
 *
 * @returns
 *  An indicator for the relation of the passed boolean values (`false` is less
 *  than `true`).
 */
export function compareBooleans(b1: boolean, b2: boolean): CompareResult {
    return (b1 === b2) ? EQUAL : b1 ? GREATER : LESS;
}

/**
 * Compares the internal indexes of built-in error codes.
 *
 * @param e1
 *  The first error code for comparison.
 *
 * @param e2
 *  The second error code for comparison.
 *
 * @returns
 *  The special value `CompareResult.NA`, if any of the passed error codes does
 *  not represent a built-in error (the property `num` of the error code is not
 *  finite); otherwise the relation of the properties `num` of the error codes.
 */
export function compareErrorCodes(e1: ErrorLiteral, e2: ErrorLiteral): CompareResult {
    return (Number.isFinite(e1.num) && Number.isFinite(e2.num)) ? math.compare(e1.num, e2.num) : NA;
}

/**
 * Compares the passed scalar values.
 *
 * @param v1
 *  The first scalar value for comparison.
 *
 * @param v2
 *  The second scalar value for comparison.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  An indicator for the relation of the passed scalar values. Numbers are
 *  always less than strings, strings are always less than boolean values, and
 *  boolean values are always less than error codes. The value `null` will be
 *  treated according to the option `nullMode`. Values of the same type will be
 *  compared as described for the public functions `compareNumbers()`,
 *  `compareStrings()`, `compareBooleans()`, and `compareErrorCodes()`.
 */
export function compareScalars(v1: ScalarType, v2: ScalarType, options?: CompareScalarOptions): CompareResult {

    // convert null values if specified
    if (!options?.nullMode) {
        if (v1 === null) { v1 = getNeutralValue(v2); }
        if (v2 === null) { v2 = getNeutralValue(v1); }
    }

    // whether the null value is considered greater than any other value (not for 'convert' mode)
    const nullMax = options?.nullMode === CompareNullMode.NULL_MAX;
    // get type identifiers, compare different types (ignore the actual values)
    const type1 = getScalarType(v1, nullMax);
    const type2 = getScalarType(v2, nullMax);
    if (type1 !== type2) { return math.compare(type1, type2); }

    // compare values of equal types
    switch (type1) {
        case NUMBER:  return compareNumbers(v1 as number, v2 as number);
        case STRING:  return compareStrings(v1 as string, v2 as string, options);
        case BOOLEAN: return compareBooleans(v1 as boolean, v2 as boolean);
        case ERROR:   return compareErrorCodes(v1 as ErrorLiteral, v2 as ErrorLiteral);
        default:      return EQUAL; // both values are null
    }
}

/**
 * Returns the string representation of the passed scalar value for debug
 * logging.
 *
 * _Attention:_ DO NOT USE this function for GUI related code where correct
 * locale dependent representation of the passed value is required.
 *
 * @param v
 *  The scalar value to be converted to the string representation.
 *
 * @returns
 *  The string representation of the passed value.
 */
export function scalarToString(v: unknown): string {
    return (typeof v === "string") ? `"${v.replace(/"/g, '""')}"` :
        (v instanceof Date) ? v.toISOString() :
        (v === null) ? "null" :
        (v === true) ? "TRUE" :
        (v === false) ? "FALSE" :
        // eslint-disable-next-line @typescript-eslint/no-base-to-string
        String(v);
}
