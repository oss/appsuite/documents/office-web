/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import type { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { rangeKey, Range } from "@/io.ox/office/spreadsheet/utils/range";

const { min, max } = Math;

// class Range3D ==============================================================

/**
 * A cell range address with two sheet index properties, that represents a
 * cell range in a specific single sheet, or in a range of consecutive
 * sheets of a spreadsheet document.
 *
 * @param sheet1
 *  The zero-based index of the first sheet of this cell range address.
 *
 * @param sheet2
 *  The zero-based index of the last sheet of this cell range address.
 */
export class Range3D extends Range implements Equality<Range3D>, Cloneable<Range3D> {

    // static functions -------------------------------------------------------

    /**
     * Creates a cell range address from the passed column, row, and sheet
     * indexes. The column, row, and sheet indexes in the resulting cell range
     * address will be ordered automatically.
     *
     * @param col1
     *  The column index of one of the borders in the cell range.
     *
     * @param row1
     *  The row index of one of the borders in the cell range.
     *
     * @param col2
     *  The column index of the other border in the cell range.
     *
     * @param row2
     *  The row index of the other border in the cell range.
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the new cell range address.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the new cell range address.
     *  If omitted, the new range will refer to a single sheet.
     *
     * @returns
     *  The cell range address with adjusted column and row indexes.
     */
    static fromIndexes3D(col1: number, row1: number, col2: number, row2: number, sheet1: number, sheet2?: number): Range3D {
        const a1 = new Address(min(col1, col2), min(row1, row2));
        const a2 = new Address(max(col1, col2), max(row1, row2));
        if (!is.number(sheet2)) { sheet2 = sheet1; }
        return new Range3D(min(sheet1, sheet2), max(sheet1, sheet2), a1, a2);
    }

    /**
     * Creates a cell range address with sheet indexes from the passed simple
     * cell address. The sheet indexes in the resulting cell range address will
     * be ordered automatically.
     *
     * @param address
     *  A simple cell address without sheet indexes.
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the new cell range address.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the new cell range address.
     *  If omitted, the new range will refer to a single sheet.
     *
     * @returns
     *  The cell range address created from the passed parameters.
     */
    static fromAddress3D(address: Address, sheet1: number, sheet2?: number): Range3D {
        return Range3D.fromIndexes3D(address.c, address.r, address.c, address.r, sheet1, sheet2);
    }

    /**
     * Creates a cell range address with sheet indexes from the passed cell
     * addresses. The columns, rows, and sheets in the resulting cell range
     * address will be ordered automatically. The passed cell addresses will be
     * deeply cloned when creating the new range address.
     *
     * @param address1
     *  The first cell address used to create the new cell range address.
     *
     * @param address2
     *  The second cell address used to create the new cell range address.
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the new cell range address.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the new cell range address.
     *  If omitted, the new range will refer to a single sheet.
     *
     * @returns
     *  The cell range address created from the passed cell addresses.
     */
    static fromAddresses3D(address1: Address, address2: Address, sheet1: number, sheet2?: number): Range3D {
        return Range3D.fromIndexes3D(address1.c, address1.r, address2.c, address2.r, sheet1, sheet2);
    }

    /**
     * Creates a cell range address with sheet indexes from the passed simple
     * cell range address. The sheet indexes in the resulting cell range
     * address will be ordered automatically. The start and end address of the
     * original cell range address will be deeply cloned when creating the new
     * range address.
     *
     * @param range
     *  A simple cell range address without sheet indexes.
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the new cell range address.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the new cell range address.
     *  If omitted, the new range will refer to a single sheet.
     *
     * @returns
     *  The cell range address created from the passed parameters.
     */
    static fromRange3D(range: Range, sheet1: number, sheet2?: number): Range3D {
        return Range3D.fromIndexes3D(range.a1.c, range.a1.r, range.a2.c, range.a2.r, sheet1, sheet2);
    }

    /**
     * Creates a cell range address with sheet indexes from the passed column
     * and row interval. The sheet indexes in the resulting cell range address
     * will be ordered automatically.
     *
     * @param colInterval
     *  The column interval of the new cell range address.
     *
     * @param rowInterval
     *  The row interval of the new cell range address.
     *
     * @param sheet1
     *  The zero-based index of the first sheet of the new cell range address.
     *
     * @param [sheet2]
     *  The zero-based index of the second sheet of the new cell range address.
     *  If omitted, the new range will refer to a single sheet.
     *
     * @returns
     *  The cell range address created from the passed index intervals.
     */
    static fromIntervals3D(colInterval: Interval, rowInterval: Interval, sheet1: number, sheet2?: number): Range3D {
        return Range3D.fromIndexes3D(colInterval.first, rowInterval.first, colInterval.last, rowInterval.last, sheet1, sheet2);
    }

    // properties -------------------------------------------------------------

    /**
     * The zero-based index of the first sheet of this cell range address.
     */
    sheet1: number;

    /**
     * The zero-based index of the last sheet of this cell range address.
     */
    sheet2: number;

    // constructor ------------------------------------------------------------

    constructor(sheet1: number, sheet2: number, a1: Address, a2?: Address) {
        super(a1, a2);
        this.sheet1 = sheet1;
        this.sheet2 = sheet2;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a unique string key for this cell range address that can be used
     * for example as key in an associative map. This method is faster than the
     * method `toString()`.
     *
     * @returns
     *  A unique string key for this cell range address.
     */
    override get key(): string {
        return `${this.sheet1}:${this.sheet2}!${rangeKey(this.a1, this.a2)}`;
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a deep clone of this cell range address.
     *
     * @returns
     *  A deep clone of this range address.
     */
    override clone(): Range3D {
        return new Range3D(this.sheet1, this.sheet2, this.a1.clone(), this.a2.clone());
    }

    /**
     * Returns whether the passed cell range address is equal to this cell
     * range address.
     *
     * @param range
     *  The other cell range address to be compared to this cell range address.
     *
     * @returns
     *  Whether both cell range addresses contain the same start and end cell
     *  addresses.
     */
    override equals(range: Range3D): boolean {
        return (this.sheet1 === range.sheet1) && (this.sheet2 === range.sheet2) && super.equals(range);
    }

    /**
     * Returns whether the passed cell range address differs from this cell
     * range address.
     *
     * @param range
     *  The other cell range address to be compared to this cell range address.
     *
     * @returns
     *  Whether the passed cell range address differs from this cell range
     *  address.
     */
    override differs(range: Range3D): boolean {
        return (this.sheet1 !== range.sheet1) || (this.sheet2 !== range.sheet2) || super.differs(range);
    }

    /**
     * Returns the number of sheets covered by this cell range address.
     *
     * @returns
     *  The number of sheets covered by this cell range address.
     */
    sheets(): number {
        return this.sheet2 - this.sheet1 + 1;
    }

    /**
     * Returns the number of cells covered by this cell range address.
     *
     * @returns
     *  The number of cells covered by this cell range address.
     */
    override cells(): number {
        return this.sheets() * super.cells();
    }

    /**
     * Returns whether this cell range address covers a single sheet.
     *
     * @returns
     *  Whether this cell range address covers a single sheet.
     */
    singleSheet(): boolean {
        return this.sheet1 === this.sheet2;
    }

    /**
     * Returns whether this cell range address refers exactly to the specified
     * sheet index (with both sheet indexes).
     *
     * @param sheet
     *  The zero-based sheet index to be checked.
     *
     * @returns
     *  Whether this cell range address refers to the specified sheet index.
     */
    isSheet(sheet: number): boolean {
        return (this.sheet1 === sheet) && (this.sheet2 === sheet);
    }

    /**
     * Returns whether this cell range address contains the specified sheet
     * index.
     *
     * @param sheet
     *  The zero-based sheet index to be checked.
     *
     * @returns
     *  Whether this cell range address contains the specified sheet index.
     */
    containsSheet(sheet: number): boolean {
        return (this.sheet1 <= sheet) && (sheet <= this.sheet2);
    }

    /**
     * Returns whether this cell range address contains the passed cell range
     * address completely, including their sheet ranges.
     *
     * @param range
     *  The other cell range address to be checked.
     *
     * @returns
     *  Whether this cell range address contains the passed cell range address.
     */
    override contains(range: Range3D): boolean {
        return (this.sheet1 <= range.sheet1) && (range.sheet2 <= this.sheet2) && super.contains(range);
    }

    /**
     * Returns whether this cell range address overlaps with the passed cell
     * range address by at least one cell, including their sheet ranges.
     *
     * @param range
     *  The other cell range address to be checked.
     *
     * @returns
     *  Whether this cell range address overlaps with the passed cell range
     *  address by at least one cell.
     */
    override overlaps(range: Range3D): boolean {
        return (this.sheet1 <= range.sheet2) && (range.sheet1 <= this.sheet2) && super.overlaps(range);
    }

    /**
     * Returns the address of the specified columns in this cell range address.
     *
     * @param index
     *  The relative column index (the value zero will return the address of
     *  the leading column in this range).
     *
     * @param [count=1]
     *  The number of columns covered by the resulting range.
     *
     * @returns
     *  The address of the specified columns in this cell range address.
     */
    override colRange(index: number, count = 1): Range3D {
        return Range3D.fromRange3D(super.colRange(index, count), this.sheet1, this.sheet2);
    }

    /**
     * Returns the address of the specified rows in this cell range address.
     *
     * @param index
     *  The relative row index (the value zero will return the address of the
     *  top row in this range).
     *
     * @param [count=1]
     *  The number of rows covered by the resulting range.
     *
     * @returns
     *  The address of the specified rows in this cell range address.
     */
    override rowRange(index: number, count = 1): Range3D {
        return Range3D.fromRange3D(super.rowRange(index, count), this.sheet1, this.sheet2);
    }

    /**
     * Returns the address of the specified columns or rows in this cell range
     * address.
     *
     * @param columns
     *  Whether to return a column range (`true`), or a row range (`false`) of
     *  this cell range address.
     *
     * @param index
     *  The relative column/row index (the value zero will return the address
     *  of the leading column or top row in this range).
     *
     * @param [count=1]
     *  The number of columns or rows covered by the resulting range.
     *
     * @returns
     *  The address of the specified columns/rows in this cell range address.
     */
    override lineRange(columns: boolean, index: number, count = 1): Range3D {
        return columns ? this.colRange(index, count) : this.rowRange(index, count);
    }

    /**
     * Returns the address of the bounding cell range of this cell range, and
     * the passed cell range address (the smallest range that contains both
     * ranges).
     *
     * @param range
     *  The other cell range address to create the bounding range from.
     *
     * @returns
     *  The address of the bounding cell range of this cell range, and the
     *  passed cell range address
     */
    override boundary(range: Range3D): Range3D {
        return Range3D.fromIndexes3D(
            min(this.a1.c, range.a1.c),
            min(this.a1.r, range.a1.r),
            max(this.a2.c, range.a2.c),
            max(this.a2.r, range.a2.r),
            min(this.sheet1, range.sheet1),
            max(this.sheet2, range.sheet2)
        );
    }

    /**
     * Returns the intersecting cell range address between this cell range
     * address, and the passed cell range address.
     *
     * @param range
     *  The other cell range address.
     *
     * @returns
     *  The address of the cell range covered by both cell range addresses, if
     *  existing; otherwise null.
     */
    override intersect(range: Range3D): Range3D | null {
        return this.overlaps(range) ? Range3D.fromIndexes3D(
            max(this.a1.c, range.a1.c),
            max(this.a1.r, range.a1.r),
            min(this.a2.c, range.a2.c),
            min(this.a2.r, range.a2.r),
            max(this.sheet1, range.sheet1),
            min(this.sheet2, range.sheet2)
        ) : null;
    }

    /**
     * Converts this cell range address to an instance of the class `Range`
     * without sheet indexes.
     *
     * @returns
     *  An instance of the class `Range` containing start and end cell address
     *  of this range, but no sheet indexes.
     */
    toRange(): Range {
        return new Range(this.a1, this.a2);
    }

    /**
     * Returns the string representation of this cell range address, in
     * upper-case A1 notation. The sheet indexes will be inserted as numbers.
     *
     * @returns
     *  The string representation of this cell range address, in upper-case A1
     *  notation.
     */
    override toString(): string {
        return `${this.sheet1}:${this.sheet2}!${super.toString()}`;
    }
}
