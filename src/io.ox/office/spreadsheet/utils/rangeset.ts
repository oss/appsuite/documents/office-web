/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { itr, ary } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

// types ======================================================================

/**
 * Specifies which overlapping ranges will be searched for in a range set.
 */
export enum FindMatchType {

    /**
     * Searches for all ranges in the set that overlap somehow with the search
     * range (partially or completely).
     */
    OVERLAP = 0,

    /**
     * Searches for all ranges in the set that are completely covered by
     * (contained in) the search range.
     */
    CONTAIN = 1,

    /**
     * Searches for all ranges in the set that completely cover the search
     * range.
     */
    COVER = 2,

    /**
     * Searches for all ranges in the set that are equal to the search range.
     */
    EXACT = 3,

    /**
     * Searches for all ranges in the set that are covered partially (but not
     * contained in) the search range.
     */
    PARTIAL = 4,

    /**
     * Searches for all ranges in the set whose start address is contained in
     * the search range.
     */
    REFERENCE = 5
}

// class CountedInterval ======================================================

class CountedInterval extends Interval {
    count = 1;
    constructor(interval: Interval) {
        super(interval.first, interval.last);
    }
}

// constants ==================================================================

const FIND_MATCHERS: Record<FindMatchType, FuncType<boolean, Pair<Range>>> = {
    [FindMatchType.OVERLAP]:   (range, element) => range.overlaps(element),
    [FindMatchType.CONTAIN]:   (range, element) => range.contains(element),
    [FindMatchType.COVER]:     (range, element) => element.contains(range),
    [FindMatchType.EXACT]:     (range, element) => range.equals(element),
    [FindMatchType.PARTIAL]:   (range, element) => range.overlaps(element) && !range.contains(element),
    [FindMatchType.REFERENCE]: (range, element) => range.containsAddress(element.a1)
};

// private functions ==========================================================

function increase(array: CountedInterval[], interval: Interval, last: boolean): void {
    const compare = last ? Interval.compareLast : Interval.compare;
    const ai = ary.fastFindFirstIndex(array, entry => compare(interval, entry) <= 0);
    if (array[ai]?.equals(interval)) {
        array[ai].count += 1;
    } else {
        ary.insertAt(array, ai, new CountedInterval(interval));
    }
}

function decrease(array: CountedInterval[], interval: Interval, last: boolean): void {
    const compare = last ? Interval.compareLast : Interval.compare;
    const ai = ary.fastFindFirstIndex(array, entry => compare(interval, entry) <= 0);
    if (array[ai]?.equals(interval) && !(array[ai].count -= 1)) {
        ary.deleteAt(array, ai);
    }
}

// class RangeSet =============================================================

/**
 * A unique set of cell range addresses. This class provides optimized methods
 * for insertion, deletion, and access of cell range addresses, while keeping
 * the set unique (two different range addresses in this set will never have
 * the same start and end address).
 */
export class RangeSet<RangeT extends Range = Range> extends KeySet<RangeT> {

    // static functions -------------------------------------------------------

    static from<RangeT extends Range>(source: Iterable<RangeT>): RangeSet<RangeT>;
    static from<RangeT extends Range, ST>(source: Iterable<ST>, fn: (value: ST) => Opt<RangeT>): RangeSet<RangeT>;
    // implementation
    static from<RangeT extends Range>(source: Iterable<unknown>, fn?: (value: unknown) => Opt<RangeT>): RangeSet<RangeT> {
        return new RangeSet(fn ? itr.map(source, fn) : (source as Iterable<RangeT>));
    }

    // properties -------------------------------------------------------------

    // counts of all column intervals of the cell ranges in this set, sorted by first index
    readonly #cols1: CountedInterval[] = [];
    // counts of all column intervals of the cell ranges in this set, sorted by last index
    readonly #cols2: CountedInterval[] = [];
    // counts of all row intervals of the cell ranges in this set, sorted by first index
    readonly #rows1: CountedInterval[] = [];
    // counts of all row intervals of the cell ranges in this set, sorted by last index
    readonly #rows2: CountedInterval[] = [];

    // constructor ------------------------------------------------------------

    constructor(source?: Iterable<RangeT>) {
        super();
        if (source) {
            for (const range of source) {
                this.add(range);
            }
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts a new cell range into this set. Another cell range with the same
     * key will be overwritten.
     *
     * @param range
     *  The cell range to be inserted into this set.
     *
     * @returns
     *  A reference to this instance.
     */
    override add(range: RangeT): this {

        // do not modify the counter intervals, if another range exists in this set
        if (!this.has(range)) {
            const cols = range.colInterval();
            increase(this.#cols1, cols, false);
            increase(this.#cols2, cols, true);
            const rows = range.rowInterval();
            increase(this.#rows1, rows, false);
            increase(this.#rows2, rows, true);
        }

        // insert the range into the base storage (will replace an old element in the set)
        return super.add(range);
    }

    /**
     * Removes a cell range from this set.
     *
     * @param range
     *  The cell range used to resolve the key of the cell range to be removed.
     *
     * @returns
     *  Whether a cell range has been removed from the set.
     */
    override delete(range: Range): boolean {
        const element = this.at(range);
        if (element) {
            const cols = element.colInterval();
            decrease(this.#cols1, cols, false);
            decrease(this.#cols2, cols, true);
            const rows = element.rowInterval();
            decrease(this.#rows1, rows, false);
            decrease(this.#rows2, rows, true);
        }
        return super.delete(range);
    }

    /**
     * Removes all cell ranges from this set.
     */
    override clear(): void {
        this.#cols1.length = this.#cols2.length = this.#rows1.length = this.#rows2.length = 0;
        super.clear();
    }

    /**
     * Returns the bounding range (the smallest cell range that contains all
     * cell range addresses) of this range set.
     *
     * @returns
     *  The bounding range of this cell range set; or `undefined`, if this set
     *  is empty.
     */
    boundary(): Opt<Range> {
        return this.size ? Range.fromIndexes(this.#cols1[0].first, this.#rows1[0].first, ary.at(this.#cols2, -1)!.last, ary.at(this.#rows2, -1)!.last) : undefined;
    }

    /**
     * Returns whether the passed cell range overlaps with the bounding range
     * of this set. This does not imply that the cell range overlaps with any
     * elements of this set!
     *
     * @param range
     *  The cell range to be checked.
     *
     * @returns
     *  Whether the passed cell range overlaps with the bounding range of this
     *  set.
     */
    overlapsBoundary(range: Range): boolean {
        return !!this.boundary()?.overlaps(range);
    }

    /**
     * Returns an iterator that will visit all cell ranges in this set that
     * overlap with the passed cell range address.
     *
     * @param range
     *  The cell range address used to look for the cell ranges in this set.
     *
     * @param [matchType=OVERLAP]
     *  Specifies which overlapping cell ranges will be visited.
     *
     * @yields
     *  The cell ranges in this set overlapping with the passed cell range
     *  address, according to the specified match type, in no specific order.
     */
    *yieldMatching(range: Range, matchType = FindMatchType.OVERLAP): IterableIterator<RangeT> {

        // quick check if the passed cell range is out of the own boundary
        if (!this.overlapsBoundary(range)) { return; }

        // fast access for match type `EXACT` (only one range in the set can match)
        if (matchType === FindMatchType.EXACT) {
            const found = this.at(range);
            if (found) { yield found; }
            return;
        }

        // create an iterator that visits the matching cell ranges
        const matcher = FIND_MATCHERS[matchType];
        for (const element of this) {
            if (matcher(range, element)) {
                yield element;
            }
        }
    }

    /**
     * Creates an iterator for all cell ranges in this set that cover a cell
     * address.
     *
     * @param address
     *  The cell address used to look for the cell ranges in this set.
     *
     * @yields
     *  The cell ranges in this set that cover the passed cell address.
     */
    *yieldByAddress(address: Address): IterableIterator<RangeT> {

        // quick check if the passed address is out of the own boundary
        if (!this.overlapsBoundary(new Range(address))) { return; }

        // yield all matching cell ranges
        for (const element of this) {
            if (element.containsAddress(address)) {
                yield element;
            }
        }
    }

    /**
     * Returns whether this set contains a cell range covering the passed cell
     * address.
     *
     * @param address
     *  The cell address used to look for a cell range in this set.
     *
     * @returns
     *  Whether this set contains a cell range covering the passed address.
     */
    containsAddress(address: Address): boolean {
        return !!this.findOneByAddress(address);
    }

    /**
     * Returns an arbitrary cell range in this set that covers the passed cell
     * address.
     *
     * @param address
     *  The cell address used to look for a cell range in this set.
     *
     * @returns
     *  An arbitrary cell range address in this set that covers the passed cell
     *  address, or `undefined`, if no cell range has been found.
     */
    findOneByAddress(address: Address): Opt<RangeT> {

        // quick check if the passed address is out of the own boundary
        const range = new Range(address);
        if (!this.overlapsBoundary(range)) { return undefined; }

        // fast lookup for single range
        const eqRange = this.at(range);
        if (eqRange) { return eqRange; }

        // search through the elements
        return itr.find(this, element => element.containsAddress(address));
    }

    /**
     * Returns an arbitrary range that overlaps with the passed cell range.
     *
     * @param range
     *  The cell range address used to look for overlapping ranges in this set.
     *
     * @param [matchType=OVERLAP]
     *  Specifies which overlapping cell ranges from this set will match.
     *
     * @returns
     *  An arbitrary cell range address overlapping with the passed cell range.
     */
    findOne(range: Range, matchType = FindMatchType.OVERLAP): Opt<RangeT> {

        // quick check if the passed range is out of the own boundary
        if (!this.overlapsBoundary(range)) { return undefined; }

        // fast lookup for equal interval if not matching partially
        const eqRange = (matchType !== FindMatchType.PARTIAL) ? this.at(range) : undefined;
        if (eqRange || (matchType === FindMatchType.EXACT)) { return eqRange; }

        // search through the elements
        const matcher = FIND_MATCHERS[matchType];
        return itr.find(this, element => matcher(range, element));
    }

    /**
     * Removes all ranges from this set that overlap with the passed cell range
     * address.
     *
     * @param range
     *  The cell range used to look for the ranges in this set.
     *
     * @param [matchType]
     *  Specifies which overlapping cell ranges from this set will match.
     *
     * @returns
     *  An array with all ranges removed from this set overlapping with the
     *  passed cell range, in no specific order.
     */
    deleteMatching(range: Range, matchType = FindMatchType.OVERLAP): RangeArray {
        // put all ranges into the array *before* modifying the set (`yieldMatching` not safe against deletion)
        const ranges = RangeArray.from(this.yieldMatching(range, matchType)) as unknown as RangeArray;
        for (const element of ranges) { this.delete(element); }
        return ranges;
    }
}
