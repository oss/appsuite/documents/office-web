/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { to, str } from "@/io.ox/office/tk/algorithms";
import type { Rectangle } from "@/io.ox/office/tk/dom";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { EntryOffsetDescriptor } from "@/io.ox/office/spreadsheet/model/colrowcollection";

const { floor, max } = Math;

// enum AnchorMode ============================================================

/**
 * Enumeration for different anchor modes of drawing objects.
 */
export enum AnchorMode {

    /**
     * The top-left corner of the drawing object is positioned absolutely in
     * the sheet, and its size is fixed. Column and row operations do not
     * influence the (absolute) location of the drawing object.
     */
    ABSOLUTE = 0,

    /**
     * The top-left corner of the drawing object is anchored to a spreadsheet
     * cell, and its size is fixed. Column and row operations in front of the
     * drawing object will cause to move it accordingly.
     */
    ONE_CELL = 1,

    /**
     * The top-left corner and the bottom-right corner of the drawing object
     * are anchored to spreadsheet cells. Column and row operations in front
     * of, or inside the drawing object will cause to move or resize it
     * accordingly.
     */
    TWO_CELL = 2
}

// enum CellAnchorType ========================================================

/**
 * Enumeration for anchor types as used in the formatting attribute "anchor".
 *
 * Specifies how a drawing object is anchored in the sheet area, and how the
 * drawing object behaves when its effective position and size will be
 * recalculated while modifying the sheet structure (insert/delete columns and
 * rows, change their size, hide/show them).
 */
export enum CellAnchorType {

    /**
     * The base attributes `top`, `left`, `width`, and `height` describe the
     * absolute position in the sheet area.
     *
     * Runtime: The position and size of the drawing is fixed, and does not
     * change while modifying the sheet structure.
     */
    ABSOLUTE = "a",

    /**
     * The top-left corner of the drawing object is bound to the anchor cell
     * from the `anchor` attribute. Its size is obtained from the attributes
     * `width` and `height`.
     *
     * Runtime: The drawing will be moved when modifying the sheet structure.
     * The size of the drawing is fixed.
     */
    ONE_CELL = "1",

    /**
     * The top-left and bottom-right corners of the drawing object are bound to
     * their current anchor cells from the `anchor` attribute.
     *
     * Runtime: The drawing will be moved and resized when modifying the sheet
     * structure.
     */
    TWO_CELL = "2",

    /**
     * Attributes as `TWO_CELL`, runtime behavior as `ABSOLUTE`.
     */
    TWO_CELL_AS_ABSOLUTE = "A",

    /**
     * Attributes as `TWO_CELL`, runtime behavior as `ONE_CELL`.
     */
    TWO_CELL_AS_ONE_CELL = "O"
}

// private functions ==========================================================

function getInternalAnchorMode(anchorType: CellAnchorType): AnchorMode {
    switch (anchorType) {
        case CellAnchorType.ABSOLUTE:
            return AnchorMode.ABSOLUTE;
        case CellAnchorType.ONE_CELL:
            return AnchorMode.ONE_CELL;
        default:
            return AnchorMode.TWO_CELL;
    }
}

function getDisplayAnchorMode(anchorType: CellAnchorType): AnchorMode {
    switch (anchorType) {
        case CellAnchorType.ABSOLUTE:
        case CellAnchorType.TWO_CELL_AS_ABSOLUTE:
            return AnchorMode.ABSOLUTE;
        case CellAnchorType.ONE_CELL:
        case CellAnchorType.TWO_CELL_AS_ONE_CELL:
            return AnchorMode.ONE_CELL;
        default:
            return AnchorMode.TWO_CELL;
    }
}

// class CellAnchorPoint ======================================================

/**
 * Represents the position of a single corner of a drawing object that is
 * anchored to a specific spreadsheet cell.
 *
 * @param a
 *  The address of the anchor cell containing the corner of the drawing object.
 *
 * @param x
 *  The relative offset inside the cell's column, in 1/100 mm.
 *
 * @param y
 *  The relative offset inside the cell's row, in 1/100 mm.
 */
export class CellAnchorPoint implements Cloneable<CellAnchorPoint> {

    // static functions -------------------------------------------------------

    /**
     * Creates a cell anchor from the passed column/row descriptors.
     */
    static createFromColRow(colEntry: EntryOffsetDescriptor<"column">, rowEntry: EntryOffsetDescriptor<"row">): CellAnchorPoint {
        return new CellAnchorPoint(
            new Address(colEntry.index, rowEntry.index),
            colEntry.relOffsetHmm,
            rowEntry.relOffsetHmm
        );
    }

    /**
     * Parses the string tokens of an "anchor" attribute value to an instance
     * of class `CellAnchorPoint`.
     */
    static parseTokens(tokens: string[], offset = 0): CellAnchorPoint {
        const a = Address.parse(tokens[offset] || "") ?? Address.A1;
        const x = max(0, parseInt(tokens[offset + 1] || "", 10));
        const y = max(0, parseInt(tokens[offset + 2] || "", 10));
        return new CellAnchorPoint(a, x, y);
    }

    // properties -------------------------------------------------------------

    /**
     * The address of the cell containing the corner of the drawing object.
     */
    a: Address;

    /**
     * The relative offset inside the cell's column, in 1/100 mm.
     */
    x: number;

    /**
     * The relative offset inside the cell's row, in 1/100 mm.
     */
    y: number;

    // constructor ------------------------------------------------------------

    constructor(a: Address);
    constructor(a: Address, x: number, y: number);
    // implementation
    constructor(a: Address, x?: number, y?: number) {
        this.a = a.clone();
        this.x = x || 0;
        this.y = y || 0;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a deep clone of this instance.
     */
    clone(): CellAnchorPoint {
        return new CellAnchorPoint(this.a, this.x, this.y);
    }

    /**
     * Sets the address component and offset in a specific direction.
     *
     * @param index
     *  The column or row index of the cell to be set.
     *
     * @param offset
     *  The relative offset inside the cell to be set.
     *
     * @param columns
     *  Whether to set column index and offset (`true`), or row index and
     *  offset (`false`).
     */
    set(index: number, offset: number, columns: boolean): void {
        this.a.set(index, columns);
        this[columns ? "x" : "y"] = offset;
    }

    /**
     * Returns the string representation used as attribute value.
     */
    toOpStr(): string {
        return `${this.a.toOpStr()} ${floor(this.x)} ${floor(this.y)}`;
    }

    /**
     * Returns the string representation for debug logging.
     */
    toString(): string {
        return `a=${this.a} x=${this.x} y=${this.y}`;
    }
}

// class CellAnchor ===========================================================

/**
 * Represents the position of a drawing object that is anchored to specific
 * spreadsheet cells.
 */
export class CellAnchor implements Cloneable<CellAnchor> {

    // static functions -------------------------------------------------------

    /**
     * Default string value for attributes used in document operations.
     */
    static readonly DEFAULT_VALUE = new CellAnchor(CellAnchorType.TWO_CELL, new CellAnchorPoint(Address.A1), new CellAnchorPoint(Address.A1)).toOpStr();

    /**
     * Parses the string tokens of an "anchor" attribute value to an instance
     * of class `CellAnchor`.
     */
    static parseTokens(tokens: string[], offset = 0): CellAnchor {
        const type = to.enum(CellAnchorType, tokens[offset], CellAnchorType.TWO_CELL);
        const point1 = CellAnchorPoint.parseTokens(tokens, offset + 1);
        const point2 = CellAnchorPoint.parseTokens(tokens, offset + 4);
        return new CellAnchor(type, point1, point2);
    }

    /**
     * Parses an "anchor" attribute value to an instance of class `CellAnchor`.
     */
    static parseOpStr(parseStr: string): CellAnchor {
        return CellAnchor.parseTokens(str.splitTokens(parseStr, 7));
    }

    // properties -------------------------------------------------------------

    readonly type: CellAnchorType;
    readonly point1: CellAnchorPoint;
    readonly point2: CellAnchorPoint;

    // constructor ------------------------------------------------------------

    constructor(type: CellAnchorType, point1: CellAnchorPoint, point2: CellAnchorPoint) {
        this.type = type;
        this.point1 = point1;
        this.point2 = point2;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a deep clone of this instance.
     */
    clone(): CellAnchor {
        return new CellAnchor(this.type, this.point1.clone(), this.point2.clone());
    }

    /**
     * Returns the effective anchor mode of this cell anchor.
     *
     * @param displayMode
     *  If set to `false`, returns the internal anchor mode used for
     *  determining the active anchor attributes. If set to `true`, returns
     *  the anchor mode used for runtime behavior (how the drawing object
     *  behaves when manipulating the columns or rows in the sheet).
     *
     * @returns
     *  The effective anchor mode of this cell anchor.
     */
    getAnchorMode(displayMode: boolean): AnchorMode {
        return displayMode ? getDisplayAnchorMode(this.type) : getInternalAnchorMode(this.type);
    }

    /**
     * Returns the address of the cell range covered by this cell anchor.
     *
     * @returns
     *  The address of the cell range covered by this cell anchor. If one of
     *  the offsets of the end position is zero, the respective end column/row
     *  index of the range will be decreased by one.
     */
    getRange(): Range {

        // build the cell range address from the anchor addresses (with auto-adjust)
        const range = Range.fromAddresses(this.point1.a, this.point2.a);

        // shorten by one column/row, if the end offset is zero
        if ((this.point2.x === 0) && !range.singleCol()) { range.a2.c -= 1; }
        if ((this.point2.y === 0) && !range.singleRow()) { range.a2.r -= 1; }

        return range;
    }

    /**
     * Returns the string representation used as attribute value.
     */
    toOpStr(): string {
        return `${this.type} ${this.point1.toOpStr()} ${this.point2.toOpStr()}`;
    }

    /**
     * Returns the string representation for debug logging.
     */
    toString(): string {
        return `type=${this.type} p1=(${this.point1}) p2=(${this.point2})`;
    }
}

// class SheetDrawingAnchor ===================================================

/**
 * Position information for drawing anchor settings in a spreadsheet.
 *
 * @param anchorMode
 *  The anchor mode specifying the active anchor settings.
 *
 * @param rectHmm
 *  The absolute location of the drawing object in the sheet, in 1/100 of
 *  millimeters, independent from the current sheet zoom factor.
 *
 * @param rectPx
 *  The absolute location of the drawing object in the sheet, in pixels,
 *  according to the current sheet zoom factor.
 *
 * @param cellAnchor
 *  The complete cell anchor specification, regardless of the anchor mode.
 */
export class SheetDrawingAnchor {

    // static functions -------------------------------------------------------

    /**
     * Returns the complete anchor attributes to be applied at a drawing object
     * to update its anchor settings.
     *
     * @param cellAnchor
     *  The complete cell anchor specification.
     *
     * @param rectHmm
     *  The absolute location of the drawing object in the sheet, in 1/100 of
     *  millimeters, independent from the current sheet zoom factor.
     *
     * @returns
     *  The complete anchor attributes to be applied at a drawing object.
     */
    static createAttributes(cellAnchor: CellAnchor, rectHmm: Rectangle): Dict {
        const anchorAttrs = rectHmm.toJSON() as unknown as Dict;
        anchorAttrs.anchor = cellAnchor.toOpStr();
        return anchorAttrs;
    }

    // properties -------------------------------------------------------------

    readonly anchorMode: AnchorMode;
    readonly rectHmm: Rectangle;
    readonly rectPx: Rectangle;
    readonly cellAnchor: CellAnchor;

    // constructor ------------------------------------------------------------

    constructor(anchorMode: AnchorMode, rectHmm: Rectangle, rectPx: Rectangle, cellAnchor: CellAnchor) {
        this.anchorMode = anchorMode;
        this.rectHmm = rectHmm;
        this.rectPx = rectPx;
        this.cellAnchor = cellAnchor;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the complete anchor attributes to be applied at a drawing object
     * to update its anchor settings.
     *
     * @returns
     *  The complete anchor attributes to be applied at a drawing object.
     */
    createAttributes(): Dict {
        return SheetDrawingAnchor.createAttributes(this.cellAnchor, this.rectHmm);
    }
}
