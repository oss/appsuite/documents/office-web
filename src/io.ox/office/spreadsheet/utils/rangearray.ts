/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ReverseOptions, ArrayCallbackFn, ArrayPredicateFn } from "@/io.ox/office/tk/algorithms";
import { math, ary } from "@/io.ox/office/tk/algorithms";

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { type RangeIteratorOptions, Range } from "@/io.ox/office/spreadsheet/utils/range";
import type { ArraySource, SortComparatorFn } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { consumeArraySource, everyInArraySource, someInArraySource, ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { type IntervalSource, IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";

// types ======================================================================

/**
 * Data type for values providing cell range addresses to be consumed by
 * methods of the class `RangeArray`. Can be an array or an iterator with cell
 * range addresses, a single cell range address, or nullish.
 */
export type RangeSource = ArraySource<Range>;

/**
 * An index interval extended with additional properties, as returned from the
 * methods `RangeArray#getColBands` or `RangeArray#getRowBands`.
 */
export interface BandInterval extends Interval {

    /**
     * The addresses of the original cell ranges covering this band interval,
     * sorted by start column/row index.
     */
    ranges: RangeArray | null;

    /**
     * The merged index intervals of the cell ranges covered along this band
     * interval.
     */
    intervals: IntervalArray | null;
}

/**
 * Specifies which properties will be included into generated band intervals
 * (instances of `BandInterval`).
 */
export interface BandIntervalOptions {

    /**
     * If set to `true`, the band intervals will contain the property `ranges`
     * with the addresses of the original cell ranges covering the respective
     * band interval, sorted by start column/row index. Default value is
     * `false`.
     */
    ranges?: boolean;

    /**
     * If set to `true`, the band intervals will contain the property
     * `intervals` containing the merged column/row intervals of the ranges
     * covered by the respective band interval. Default value is `false`.
     */
    intervals?: boolean;
}

/**
 * A cell range address extended with additional properties, as returned from
 * the method `RangeArray#partition`.
 */
export interface PartitionRange extends Range {

    /**
     * A cell range list containing the original cell range addresses of a cell
     * range list covering this particular result cell range of the partitioned
     * result.
     */
    coveredBy: RangeArray;
}

/**
 * Optional parameters for the method `RangeArray#split`.
 *
 * If the option "reverse" is set to `true`, the ranges will be ordered for
 * iteration in reversed direction. If a start position inside the ranges has
 * been specified, the parts before this start position will be included in the
 * result. Without reverse mode, the parts after the start position will be
 * returned.
 */
export interface SplitRangesOptions extends ReverseOptions {

    /**
     * If specified, the array index of the cell range in the source range list
     * that will be the first range in the result. MUST BE a valid array index.
     * MUST remain zero, if a single range has been passed to the function.
     * Default value is `0`.
     */
    startIndex?: number;

    /**
     * If specified, the address of a cell inside the first cell range (see
     * option `startIndex`). The cell address MUST BE located inside the start
     * range. The start range will be split and shortened to the trailing parts
     * (or leading parts in reverse mode) according to this start address.
     */
    startAddress?: Address;

    /**
     * If set to `true`, the specified start address will not be included in
     * the result ranges. This option has no effect without the option
     * `startAddress`. Default value is `false`.
     */
    skipStart?: boolean;

    /**
     * If set to `true`, the leading parts of the ranges (in reverse mode: the
     * trailing parts of the ranges), according to the start position, will be
     * appended to the resulting cell ranges. This option has no effect, if no
     * custom start range or start address has been set (options `startIndex`
     * and `startAddress`). Default value is `false`.
     */
    wrap?: boolean;
}

/**
 * A cell range address extended with additional properties, as returned from
 * the method `RangeArray#split`.
 */
export interface SplitRange extends Range {

    /**
     * The original cell range address. May be larger than this cell range, if
     * using the option `startAddress` from `SplitRangesOptions` which leads to
     * split the start range into multiple parts.
     */
    orig: Range;

    /**
     * The array index of the range address in the property `orig` in the
     * original range list (zero, if the data source was a single range).
     */
    index: number;
}

// private functions ==========================================================

/**
 * Creates a new band interval instance.
 */
function createBandInterval(first: number, last: number, ranges?: RangeSource): BandInterval {
    const bandInterval = new Interval(first, last) as BandInterval;
    bandInterval.ranges = ranges ? new RangeArray(ranges) : null;
    bandInterval.intervals = null;
    return bandInterval;
}

/**
 * Divides the cell range addresses into a sorted array of column/row intervals
 * representing the boundaries of all cell ranges.
 */
function createBandIntervals(ranges: RangeArray, columns: boolean, options?: BandIntervalOptions): BandInterval[] {

    // whether to create additional range addresses
    const needsRanges = options && (options.ranges || options.intervals);
    // the resulting interval bands
    const bandIntervals: BandInterval[] = [];
    // the start array index in `bandIntervals` used to search for matching intervals
    let startIndex = 0;

    // work on a clone sorted by start column/row index
    ranges = ranges.clone().sortBy(range => range.getStart(columns));

    // build the array of row bands
    ranges.forEach(range => {

        // the start and end band index of the range
        let first = range.getStart(columns);
        const last = range.getEnd(columns);

        // update the row bands containing the range
        for (let bandIndex = startIndex; bandIndex < bandIntervals.length; bandIndex += 1) {

            // the current band interval
            const bandInterval = bandIntervals[bandIndex];

            // row band is above the range, ignore it in the next iterations
            if (bandInterval.last < first) {
                startIndex = bandIndex + 1;
                continue;
            }

            // band interval needs to be split (range starts inside the band)
            if (bandInterval.first < first) {
                ary.insertAt(bandIntervals, bandIndex, createBandInterval(bandInterval.first, first - 1, needsRanges ? bandInterval.ranges : null));
                bandInterval.first = first;
                startIndex = bandIndex + 1;
                // next iteration of the for loop will process the current row band again
                continue;
            }

            // band interval needs to be split (range ends inside the band)
            if (last < bandInterval.last) {
                ary.insertAt(bandIntervals, bandIndex + 1, createBandInterval(last + 1, bandInterval.last, needsRanges ? bandInterval.ranges : null));
                bandInterval.last = last;
            }

            // now, current band interval contains the range completely
            if (needsRanges) { bandInterval.ranges!.push(range); }

            // continue with next range, if end of range found
            if (last === bandInterval.last) { return; }

            // adjust start index in case a new band interval needs to be appended
            first = bandInterval.last + 1;
        }

        // no band interval found, create and append a new band interval
        bandIntervals.push(createBandInterval(first, last, needsRanges ? range : null));
    });

    // sort the ranges in each band (option `ranges`), create the merged intervals (option `intervals`)
    bandIntervals.forEach(bandInterval => {

        // create the merged intervals from the original ranges
        if (options?.intervals) {
            bandInterval.intervals = bandInterval.ranges!.intervals(!columns).merge();
        }

        // sort the original ranges in the band interval, or remove them if not needed outside
        if (options?.ranges) {
            bandInterval.ranges!.sortBy(range => range.getStart(!columns));
        } else {
            bandInterval.ranges = null;
        }
    });

    // further merge adjoining band intervals with equal merged inner intervals originating from
    // different ranges (only required, if these ranges will not be returned)
    if (options?.intervals && !options.ranges) {
        ary.forEach(bandIntervals, (bandInterval, index) => {
            const nextInterval = bandIntervals[index + 1];
            if (nextInterval && (bandInterval.last + 1 === nextInterval.first) && bandInterval.intervals!.equals(nextInterval.intervals!)) {
                bandInterval.last = nextInterval.last;
                ary.deleteAt(bandIntervals, index + 1);
            }
        }, { reverse: true });
    }

    return bandIntervals;
}

/**
 * Returns whether the passed range precedes the specified column interval, or
 * whether it starts at the interval, but ends at another column.
 */
function isPrecedingUnextendableRange(range: Range, interval: Interval): boolean {
    return (range.a1.c < interval.first) ||
        ((range.a1.c === interval.first) && (range.a2.c !== interval.last)) ||
        ((interval.first < range.a1.c) && (range.a1.c <= interval.last));
}

/**
 * Converts the passed array of row bands to an array cell range addresses,
 * as returned by the instance method `RangeArray.merge()`.
 *
 * @param rowBands
 *  The row bands, as returned for example by `createBandIntervals()`. The band
 *  intervals MUST have initialized the property `intervals` with the column
 *  intervals of the row band.
 *
 * @returns
 *  The cell range list resulting from the passed row bands.
 */
function mergeRangesFromRowBands(rowBands: BandInterval[]): RangeArray {

    // resulting array of cell range addresses
    const resultRanges = new RangeArray();
    // current ranges whose last row ends in the current row band
    let bandRanges: Range[] = [];

    // build ranges from the row bands and their column intervals
    rowBands.forEach((rowBand, bandIndex) => {

        // the preceding row band
        const prevRowBand = rowBands[bandIndex - 1];
        // array index into the bandRanges array
        let bandRangeIndex = 0;

        // try to extend ranges from the previous row band with ranges from the current row band
        if (prevRowBand && (prevRowBand.last + 1 === rowBand.first)) {

            // process all intervals of the current row band, extend ranges or move them to the result array
            rowBand.intervals?.forEach(colInterval => {

                // move all ranges from bandRanges to resultRanges that cannot be merged with the current interval anymore
                // (taking advantage of the fact that all ranges in bandRanges are distinct and ordered by column)
                while ((bandRangeIndex < bandRanges.length) && isPrecedingUnextendableRange(bandRanges[bandRangeIndex], colInterval)) {
                    resultRanges.push(ary.deleteAt(bandRanges, bandRangeIndex)!);
                }

                // extend the current band range to this row band, if column interval matches
                const bandRange = bandRanges[bandRangeIndex];
                if (bandRange && (bandRange.a1.c === colInterval.first) && (bandRange.a2.c === colInterval.last)) {
                    bandRange.a2.r = rowBand.last;
                } else {
                    ary.insertAt(bandRanges, bandRangeIndex, Range.fromIntervals(colInterval, rowBand));
                }
                bandRangeIndex += 1;
            });

            // move all remaining unextended ranges from bandRanges to resultRanges
            resultRanges.append(bandRanges.splice(bandRangeIndex));

        } else {

            // store all old band ranges in the result array, create new ranges for this row band
            resultRanges.append(bandRanges);
            bandRanges = RangeArray.fromIntervals(rowBand.intervals, rowBand);
        }
    });

    // append remaining band ranges to the result
    return resultRanges.append(bandRanges);
}

// class RangeArray ===========================================================

/**
 * Represents a list of cell range addresses.
 */
export class RangeArray extends ArrayBase<Range> {

    // static functions -------------------------------------------------------

    /**
     * If the passed data source is a `RangeArray`, it will be returned
     * directly (no shallow clone). Otherwise, a new array instance will be
     * created and filled with the elements from the data source.
     *
     * @param source
     *  The data source to be converted to an instance of `RangeArray`.
     *
     * @returns
     *  An instance of this class.
     */
    static cast(source: RangeSource): RangeArray {
        return (source instanceof RangeArray) ? source : new RangeArray(source);
    }

    /**
     * Creates a `RangeArray` from the passed data source with the cell ranges
     * passing a truth test.
     *
     * @param source
     *  The data source to be converted to a `RangeArray`.
     *
     * @param fn
     *  The predicate callback function invoked on the elements of the data
     *  source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The cell range addresses passing the truth test.
     */
    static filter(source: RangeSource, fn: ArrayPredicateFn<Range>, context?: object): RangeArray {
        return new RangeArray().appendFiltered(source, fn, context);
    }

    /**
     * Creates a `RangeArray` from the return values of a callback function
     * that will be invoked on the elements of the passed data source.
     *
     * @param source
     *  The data source to be converted to a `RangeArray`.
     *
     * @param fn
     *  The callback function invoked on the elements of the data source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The cell range addresses returned from the callback function.
     */
    static map<U>(source: ArraySource<U>, fn: ArrayCallbackFn<RangeSource, U>, context?: object): RangeArray {
        return new RangeArray().appendMapped(source, fn, context);
    }

    /**
     * Creates a cell range list from the passed column and row intervals, by
     * combining each column interval with each row interval.
     *
     * @param colIntervals
     *  The column intervals for the new cell range list. This method also
     *  accepts a single index interval as parameter.
     *
     * @param rowIntervals
     *  The row intervals of the new cell range list. This method also accepts
     *  a single index interval as parameter.
     *
     * @returns
     *  The cell range addresses created from the passed intervals.
     */
    static fromIntervals(colIntervals: IntervalSource, rowIntervals: IntervalSource): RangeArray {
        const resultRanges = new RangeArray();
        consumeArraySource(rowIntervals, rowInterval => {
            consumeArraySource(colIntervals, colInterval => {
                resultRanges.push(Range.fromIntervals(colInterval, rowInterval));
            });
        });
        return resultRanges;
    }

    /**
     * Creates a cell range list from the passed column intervals and row
     * indexes.
     *
     * @param colIntervals
     *  The column intervals of the new cell range addresses. This method also
     *  accepts a single index interval as parameter.
     *
     * @param firstRow
     *  The index of the first row in all new cell range addresses.
     *
     * @param [lastRow]
     *  The index of the last row in all new cell range addresses. If omitted,
     *  all ranges will cover a single row only.
     *
     * @returns
     *  The cell range addresses created from the passed column intervals and
     *  row indexes.
     */
    static fromColIntervals(colIntervals: IntervalSource, firstRow: number, lastRow?: number): RangeArray {
        return RangeArray.fromIntervals(colIntervals, new Interval(firstRow, lastRow));
    }

    /**
     * Creates a cell range list from the passed row intervals and column
     * indexes.
     *
     * @param rowIntervals
     *  The row intervals of the new cell range addresses. This method also
     *  accepts a single index interval as parameter. Note that this method is
     *  one of the rare cases where row parameters precede column parameters,
     *  in order to provide an optional column parameter.
     *
     * @param firstCol
     *  The index of the first column in all new cell range addresses.
     *
     * @param [lastCol]
     *  The index of the last column in all new cell range addresses. If
     *  omitted, all ranges will cover a single column only.
     *
     * @returns
     *  The array of cell range addresses created from the passed row intervals
     *  and column indexes.
     */
    static fromRowIntervals(rowIntervals: IntervalSource, firstCol: number, lastCol?: number): RangeArray {
        return RangeArray.fromIntervals(new Interval(firstCol, lastCol), rowIntervals);
    }

    /**
     * Creates a cell range list from the passed cell address source. The cell
     * ranges in the result will be merged, i.e. no cell range address overlaps
     * with or adjoins exactly to another cell range address.
     *
     * @param source
     *  An unordered data source of cell addresses to be merged to cell range
     *  addresses.
     *
     * @param [fn]
     *  A mapper function that converts the values of the data source to cell
     *  addresses.
     *
     * @returns
     *  A merged cell range list from the passed cell addresses.
     */
    static mergeAddresses(source: Iterable<Address>): RangeArray;
    static mergeAddresses<VT>(source: Iterable<VT>, fn: (value: VT) => Address): RangeArray;
    // implementation
    static mergeAddresses(source: Iterable<unknown>, fn?: (value: unknown) => Address): RangeArray {

        // convert data source to local array
        const addresses = (fn ? AddressArray.from(source, fn) : AddressArray.from(source as Iterable<Address>)) as AddressArray;

        // simplifications for small arrays
        switch (addresses.length) {
            case 0:
                return new RangeArray();
            case 1:
                return new RangeArray(new Range(addresses[0].clone()));
        }

        // the row bands containing column intervals for all existing rows
        const rowBands: BandInterval[] = [];

        // sort and unify the cell addresses, create row bands with all rows that contains any cells
        for (const address of addresses.unify().sort()) {

            // the last row band in the array of all row bands
            let lastRowBand = ary.last(rowBands);

            // create a new row band on demand
            if (!lastRowBand || (lastRowBand.first !== address.r)) {
                lastRowBand = new Interval(address.r) as BandInterval;
                lastRowBand.ranges = null;
                lastRowBand.intervals = new IntervalArray();
                rowBands.push(lastRowBand);
            }

            // get the last interval, extend it or create a new interval
            const lastInterval = lastRowBand.intervals!.last();
            if (lastInterval && (lastInterval.last + 1 === address.c)) {
                lastInterval.last += 1;
            } else {
                lastRowBand.intervals!.push(new Interval(address.c));
            }
        }

        // convert the row bands to an array of cell ranges
        return mergeRangesFromRowBands(rowBands);
    }

    /**
     * Merges the cell range addresses in the passed data source so that no
     * cell range address in the result overlaps with or adjoins exactly to
     * another cell range address.
     *
     * @param source
     *  An unordered data source of cell ranges to be merged.
     *
     * @param [fn]
     *  A mapper function that converts the values of the data source to cell
     *  range addresses.
     *
     * @returns
     *  The merged cell range addresses that do not overlap, and that exactly
     *  cover the cell range addresses in the passed data source.
     */
    static mergeRanges(source: Iterable<Range>): RangeArray;
    static mergeRanges<VT>(source: Iterable<VT>, fn: (value: VT) => Range): RangeArray;
    // implementation
    static mergeRanges(source: Iterable<unknown>, fn?: (value: unknown) => Range): RangeArray {
        return ((fn ? RangeArray.from(source, fn) : RangeArray.from(source)) as RangeArray).merge();
    }

    /**
     * Splits and orders the passed cell range list according to specific
     * settings.
     *
     * @param ranges
     *  The cell range list to be split. This function also accepts a single cell
     *  range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The resulting cell range addresses.
     */
    static splitRanges(ranges: RangeSource, options?: SplitRangesOptions): SplitRange[] {
        return RangeArray.cast(ranges).split(options);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the number of cells covered by all cell range addresses.
     *
     * @returns
     *  The number of cells covered by all cell range addresses.
     */
    cells(): number {
        return this.reduce((count, range) => count + range.cells(), 0);
    }

    /**
     * Returns whether a cell range address in this list contains the passed
     * column index.
     *
     * @param col
     *  The zero-based column index to be checked.
     *
     * @returns
     *  Whether a cell range address in this list contains the passed column
     *  index.
     */
    containsCol(col: number): boolean {
        return this.some(range => range.containsCol(col));
    }

    /**
     * Returns whether a cell range address in this list contains the passed
     * row index.
     *
     * @param row
     *  The zero-based row index to be checked.
     *
     * @returns
     *  Whether a cell range address in this list contains the passed row
     *  index.
     */
    containsRow(row: number): boolean {
        return this.some(range => range.containsRow(row));
    }

    /**
     * Returns whether a cell range address in this list contains the passed
     * column or row index.
     *
     * @param index
     *  The column or row index to be checked.
     *
     * @param columns
     *  Whether to check for a column index (`true`), or a row index (`false`).
     *
     * @returns
     *  Whether a cell range address in this list contains the passed column or
     *  row index.
     */
    containsIndex(index: number, columns: boolean): boolean {
        return columns ? this.containsCol(index) : this.containsRow(index);
    }

    /**
     * Returns whether a cell range address in this list contains the passed
     * cell address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether a cell range address in this list contains the passed cell
     *  address.
     */
    containsAddress(address: Address): boolean {
        return this.some(range => range.containsAddress(address));
    }

    /**
     * Returns whether all cell range addresses in the passed data source are
     * contained completely in any of the cell range addresses in this list.
     *
     * @param ranges
     *  The other cell range addresses to be checked. This method also accepts
     *  a single cell range address as parameter.
     *
     * @returns
     *  Whether all cell range addresses in the passed data source are
     *  contained completely in any of the cell range addresses in this list.
     */
    contains(ranges: RangeSource): boolean {
        return everyInArraySource(ranges, range2 => this.some(range1 => range1.contains(range2)));
    }

    /**
     * Returns whether any cell range address in this list overlaps with any
     * cell range address in the passed data source.
     *
     * @param ranges
     *  The other cell range addresses to be checked. This method also accepts
     *  a single cell range address as parameter.
     *
     * @returns
     *  Whether any cell range address in this list overlaps with any cell
     *  range address in the passed data source.
     */
    overlaps(ranges: RangeSource): boolean {
        return this.some(range1 => someInArraySource(ranges, range2 => range1.overlaps(range2)));
    }

    /**
     * Returns whether all cell ranges in this list are distinct to each other
     * (none of the cell range addresses overlaps with any of the others).
     *
     * @returns
     *  Whether all cell ranges are distinct.
     */
    distinct(): boolean {

        // simplifications for small arrays
        switch (this.length) {
            case 0:
            case 1:
                return true;
            case 2:
                return !this[0].overlaps(this[1]);
            case 3:
                return !this[0].overlaps(this[1]) && !this[0].overlaps(this[2]) && !this[1].overlaps(this[2]);
        }

        // work on sorted clones for performance (check adjacent array elements)
        const ranges = this.clone().sortBy(range => range.a1.r);
        const { length } = ranges;
        return ranges.every((range1, index1) => {
            for (let index2 = index1 + 1; index2 < length; index2 += 1) {
                const range2 = ranges[index2];
                // a range starting below the current range, and all following ranges cannot overlap anymore
                if (range1.a2.r < range2.a1.r) { return true; }
                if (range1.overlaps(range2)) { return false; }
            }
            return true;
        });
    }

    /**
     * Returns the first cell range address in this list that contains the
     * passed column index.
     *
     * @param col
     *  The zero-based column index to be checked.
     *
     * @returns
     *  The first cell range address in this list that contains the passed
     *  column index; or null, if no range has been found.
     */
    findByCol(col: number): Range | null {
        return this.find(range => range.containsCol(col)) ?? null;
    }

    /**
     * Returns the first cell range address in this list that contains the
     * passed row index.
     *
     * @param row
     *  The zero-based row index to be checked.
     *
     * @returns
     *  The first cell range address in this list that contains the passed row
     *  index; or null, if no range has been found.
     */
    findByRow(row: number): Range | null {
        return this.find(range => range.containsRow(row)) ?? null;
    }

    /**
     * Returns the first cell range address in this list that contains the
     * passed column or row index.
     *
     * @param index
     *  The zero-based column or row index to be checked.
     *
     * @param columns
     *  Whether to search for a column index (`true`), or a row index
     *  (`false`).
     *
     * @returns
     *  The first cell range address in this list that contains the passed
     *  column or row index; or null, if no range has been found.
     */
    findByIndex(index: number, columns: boolean): Range | null {
        return columns ? this.findByCol(index) : this.findByRow(index);
    }

    /**
     * Returns the first cell range address in this list that contains the
     * passed cell address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  The first cell range address in this list that contains the passed cell
     *  address; or null, if no range has been found.
     */
    findByAddress(address: Address): Range | null {
        return this.find(range => range.containsAddress(address)) ?? null;
    }

    /**
     * Returns the column intervals of all cell range addresses in this list.
     *
     * @returns
     *  The column intervals of all cell range addresses in this list.
     */
    colIntervals(): IntervalArray {
        return IntervalArray.map(this, range => range.colInterval());
    }

    /**
     * Returns the row intervals of all cell range addresses in this list.
     *
     * @returns
     *  The row intervals of all cell range addresses in this list.
     */
    rowIntervals(): IntervalArray {
        return IntervalArray.map(this, range => range.rowInterval());
    }

    /**
     * Returns the column or row intervals of all cell range addresses in this
     * list.
     *
     * @param columns
     *  Whether to return the column intervals (`true`), or the row intervals
     *  (`false`) of the cell range addresses.
     *
     * @returns
     *  The column or row intervals of all cell range addresses in this list.
     */
    intervals(columns: boolean): IntervalArray {
        return columns ? this.colIntervals() : this.rowIntervals();
    }

    /**
     * Returns the address of the bounding cell range of all range addresses in
     * this list (the smallest range that contains all ranges in the list).
     *
     * @returns
     *  The address of the bounding cell range containing all range addresses;
     *  or `undefined`, if this cell range list is empty.
     */
    boundary(): Opt<Range> {

        const l = this.length;
        if (l === 0) { return undefined; }

        let { a1: { c: c1, r: r1 }, a2: { c: c2, r: r2 } } = this[0];
        for (let i = 1; i < l; i += 1) {
            const { a1, a2 } = this[i];
            c1 = Math.min(c1, a1.c);
            r1 = Math.min(r1, a1.r);
            c2 = Math.max(c2, a2.c);
            r2 = Math.max(r2, a2.r);
        }
        return Range.fromIndexes(c1, r1, c2, r2);
    }

    /**
     * Returns the top-left address of the bounding cell range of all range
     * addresses in this list (the smallest range that contains all ranges in
     * the list).
     *
     * @returns
     *  The top-left address of the bounding cell range containing all range
     *  addresses; or `undefined`, if this cell range list is empty.
     */
    refAddress(): Opt<Address> {

        const l = this.length;
        if (l === 0) { return undefined; }

        let { c, r } = this[0].a1;
        for (let i = 1; i < l; i += 1) {
            const { a1 } = this[i];
            c = Math.min(c, a1.c);
            r = Math.min(r, a1.r);
        }
        return new Address(c, r);
    }

    /**
     * Returns a new cell range list with only the cell range addresses in this
     * list that are not completely covered by other cell ranges in this list.
     *
     * @returns
     *  A new cell range list with only the cell range addresses in this list
     *  that are not completely covered by other cell ranges in this list. The
     *  list will contain the same ranges as contained in this list (no deep
     *  clones of the ranges).
     */
    filterCovered(): this {

        // nothing to do for empty array, or single range; but return a cloned array
        if (this.length <= 1) { return this.clone(); }

        // `unify()` is fast and may help to reduce the following O(n^2) loop
        const resultRanges = this.unify();

        // remove all ranges from the result that are covered by any other range in the array
        // (duplicate ranges have been removed above, therefore they cannot remove each other)
        return resultRanges.reject((range1, index1) =>
            // remove range1 from the result, if it is covered by another range in the array
            resultRanges.some((range2, index2) => (index1 !== index2) && range2.contains(range1))
        );
    }

    /**
     * Divides the cell range addresses into a sorted array of column intervals
     * representing the left and right boundaries of all cell ranges.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A list of column intervals representing all cell range addresses.
     */
    getColBands(options?: BandIntervalOptions): BandInterval[] {
        return createBandIntervals(this, true, options);
    }

    /**
     * Divides the cell range addresses into a sorted array of row intervals
     * representing the upper and lower boundaries of all cell ranges.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A list of row intervals representing all cell range addresses.
     */
    getRowBands(options?: BandIntervalOptions): BandInterval[] {
        return createBandIntervals(this, false, options);
    }

    /**
     * Merges the cell range addresses in this list so that no cell range
     * address in the result overlaps with or adjoins exactly to another cell
     * range address.
     *
     * Example: The cell range list `[A1:D4, C3:F6]` will be merged to the
     * ranges `A1:D2` (upper part of first range), `A3:F4` (lower part of first
     * range, and upper part of second range), and `C5:F6` (lower part of
     * second range).
     *
     * @returns
     *  The merged cell range addresses that do not overlap, and that exactly
     *  cover the cell range addresses in this list.
     */
    merge(): RangeArray {

        // at most one range: return directly instead of running the complex row band code
        if (this.length <= 1) { return this.deepClone(); }

        // calculate the row bands of the own ranges, add the merged column intervals
        const rowBands = this.getRowBands({ intervals: true });

        // return the merged ranges calculated from the row bands
        return mergeRangesFromRowBands(rowBands);
    }

    /**
     * Works similar to merging the cell range list (see method `merge()`) but
     * divides the ranges into smaller parts, so that the resulting ranges do
     * not cover the boundaries of any of the original ranges.
     *
     * Example: The cell range list `[A1:D4, C3:F6]` will be partitioned to the
     * ranges `A1:D2` (upper part of first range above the upper boundary of
     * the second range), `A3:B4` (lower-left part of first range that does not
     * cover the second range), `C3:D4` (common parts of both ranges), `E3:F4`
     * (upper-right part of second range), and C5:F6 (lower part of second
     * range).
     *
     * @returns
     *  An array with cell range addresses that do not overlap, that exactly
     *  cover the cell range addresses in this list, and that are partitioned
     *  in a way that the resulting ranges do not cover any boundary of an
     *  original range. Each cell range in the resulting array will contain an
     *  additional property `coveredBy` that is a `RangeArray` containing the
     *  original ranges of this instance covering that particular result range.
     */
    partition(): PartitionRange[] {

        // partition the first range according to the second range
        function addPartitionRanges(ranges: Range[], range1: Range, range2: Range): void {

            // ranges do not overlap: nothing to partition
            const intersectRange = range1.intersect(range2);
            if (!intersectRange) {
                ranges.push(range1.clone());
                return;
            }

            // shortcuts to the start/end addresses
            const start1 = range1.a1, end1 = range1.a2;
            const start2 = range2.a1, end2 = range2.a2;

            // row indexes of the inner rows covered by both ranges
            const row1 = Math.max(start1.r, start2.r);
            const row2 = Math.min(end1.r, end2.r);

            // put the upper part of the first range, if it starts above the second range
            if (start1.r < start2.r) {
                ranges.push(Range.fromIndexes(start1.c, start1.r, end1.c, start2.r - 1));
            }

            // put the left part of the first range, if it starts left of the second range
            if (start1.c < start2.c) {
                ranges.push(Range.fromIndexes(start1.c, row1, start2.c - 1, row2));
            }

            // put the common part of both ranges
            ranges.push(intersectRange);

            // put the right part of the first range, if it ends right of the second range
            if (end1.c > end2.c) {
                ranges.push(Range.fromIndexes(end2.c + 1, row1, end1.c, row2));
            }

            // put the lower part of the first range, if it ends below the second range
            if (end1.r > end2.r) {
                ranges.push(Range.fromIndexes(start1.c, end2.r + 1, end1.c, end1.r));
            }
        }

        // process each range in this array, split the ranges that already exist in the result
        let resultRanges: Range[] = [];
        for (const newRange of this) {

            // partition all collected result ranges, without adding the new range yet
            const nextRanges: Range[] = [];
            for (const resultRange of resultRanges) {
                addPartitionRanges(nextRanges, resultRange, newRange);
            }

            // add the uncovered parts of the new range to the result
            ary.append(nextRanges, new RangeArray(newRange).difference(nextRanges));
            resultRanges = nextRanges;
        }

        // add the original ranges as property `coveredBy` to each partition range
        for (const resultRange of resultRanges) {
            (resultRange as PartitionRange).coveredBy = this.filter(origRange => origRange.contains(resultRange));
        }

        // change array type after adding all properties
        return resultRanges as PartitionRange[];
    }

    /**
     * Splits and orders the cell ranges according to specific settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The resulting cell range addresses.
     */
    split(options?: SplitRangesOptions): SplitRange[] {

        // whether to generate the ranges for a reverse iterator
        const reverse = options?.reverse;
        // a specific start array index
        const startIndex = Math.floor(options?.startIndex ?? 0);
        // a specific start address
        const startAddress = options?.startAddress;
        // whether to skip the start address in the beginning
        const skipStart = options?.skipStart;
        // whether to wrap at the end of the range array
        const wrap = options?.wrap;

        // insert original range addresses, and array indexes, to the ranges
        const cloneRanges = RangeArray.from(this, (range, index) => {
            const cloneRange = range.clone() as SplitRange;
            cloneRange.orig = range;
            cloneRange.index = index;
            return cloneRange;
        });

        // no further processing required without custom start position
        if (!cloneRanges.length || (!startAddress && !startIndex)) {
            return cloneRanges;
        }

        // the start range according to the passed options
        const startRange = cloneRanges[math.clamp(startIndex, 0, cloneRanges.length - 1)];
        // the effective ranges, in the correct order according to the passed options
        const resultRanges: SplitRange[] = [];

        // appends a new range (part of the start range) to the array of effective iterator ranges
        function appendRange(col1: number, row1: number, col2: number, row2: number): void {
            const range = Range.fromIndexes(col1, row1, col2, row2) as SplitRange;
            range.orig = startRange.orig;
            range.index = startIndex;
            resultRanges.push(range);
        }

        // add the parts of the range array following the start address (in forward mode, or in wrapping mode)
        if (!reverse || wrap) {
            // add the trailing parts of the start range
            if (startAddress) {
                const startCol = startAddress.c + ((reverse !== skipStart) ? 1 : 0);
                if (startCol === startRange.a1.c) {
                    appendRange(startRange.a1.c, startAddress.r, startRange.a2.c, startRange.a2.r);
                } else {
                    if (startCol <= startRange.a2.c) {
                        appendRange(startCol, startAddress.r, startRange.a2.c, startAddress.r);
                    }
                    if (startAddress.r < startRange.a2.r) {
                        appendRange(startRange.a1.c, startAddress.r + 1, startRange.a2.c, startRange.a2.r);
                    }
                }
            } else if (!reverse) {
                // no start address: add the start range in forward mode (in backward mode, it must be the last array element)
                resultRanges.push(startRange);
            }
            // add the ranges following the start range
            ary.append(resultRanges, cloneRanges.slice(startIndex + 1));
        }

        // add the parts of the range array preceding the start address (in backward mode, or in wrapping mode)
        if (reverse || wrap) {
            // add the ranges preceding the start range
            ary.append(resultRanges, cloneRanges.slice(0, startIndex));
            // add the leading parts of the start range
            if (startAddress) {
                const endCol = startAddress.c - ((reverse === skipStart) ? 1 : 0);
                if (endCol === startRange.a2.c) {
                    appendRange(startRange.a1.c, startRange.a1.r, startRange.a2.c, startAddress.r);
                } else {
                    if (startRange.a1.r < startAddress.r) {
                        appendRange(startRange.a1.c, startRange.a1.r, startRange.a2.c, startAddress.r - 1);
                    }
                    if (startRange.a1.c <= endCol) {
                        appendRange(startRange.a1.c, startAddress.r, endCol, startAddress.r);
                    }
                }
            } else if (reverse) {
                // no start address: add the start range in backward mode (in forward mode, it must be the first array element)
                resultRanges.push(startRange);
            }
        }

        return resultRanges;
    }

    /**
     * Returns the cell range addresses covered by this and the passed cell
     * range list. More precisely, returns the existing intersection ranges
     * from all pairs of the cross product of the cell range lists.
     *
     * @param ranges
     *  The other cell range addresses to be intersected with the cell range
     *  addresses in this list. This method also accepts a single cell range
     *  address as parameter.
     *
     * @returns
     *  The cell range addresses covered by this list and the passed list.
     */
    intersect(ranges: RangeSource): RangeArray {
        const resultRanges = new RangeArray();
        this.forEach(range1 => consumeArraySource(ranges, range2 => {
            resultRanges.append(range1.intersect(range2));
        }));
        return resultRanges;
    }

    /**
     * Returns the difference of the cell range addresses in this list, and
     * the passed cell range addresses (all cell range addresses contained in
     * this list, that are not contained in the passed list).
     *
     * @param ranges
     *  The cell range addresses to be removed from this list. This method also
     *  accepts a single cell range address as parameter.
     *
     * @returns
     *  All cell range addresses that are contained in this list, but not in
     *  the passed list. May be an empty array, if the passed list completely
     *  covers all cell range addresses in this list.
     */
    difference(ranges: RangeSource): RangeArray {

        // the resulting ranges
        let resultRanges = this as RangeArray;

        // reduce ranges in `resultRanges` with each range from the passed array
        someInArraySource(ranges, range2 => {

            // initialize the arrays for this iteration
            const sourceRanges = resultRanges;
            resultRanges = new RangeArray();

            // process each range in the source range list
            sourceRanges.forEach(range1 => {

                // check if the ranges cover each other at all
                if (!range1.overlaps(range2)) {
                    resultRanges.push(range1.clone());
                    return;
                }

                // do nothing if range2 covers range1 completely (delete range1 from the result)
                if (range2.contains(range1)) {
                    return;
                }

                // if range1 starts above range2, extract the upper part of range1
                if (range1.a1.r < range2.a1.r) {
                    resultRanges.push(Range.fromIndexes(range1.a1.c, range1.a1.r, range1.a2.c, range2.a1.r - 1));
                }

                // if range1 starts left of range2, extract the left part of range1
                if (range1.a1.c < range2.a1.c) {
                    resultRanges.push(Range.fromIndexes(range1.a1.c, Math.max(range1.a1.r, range2.a1.r), range2.a1.c - 1, Math.min(range1.a2.r, range2.a2.r)));
                }

                // if range1 ends right of range2, extract the right part of range1
                if (range1.a2.c > range2.a2.c) {
                    resultRanges.push(Range.fromIndexes(range2.a2.c + 1, Math.max(range1.a1.r, range2.a1.r), range1.a2.c, Math.min(range1.a2.r, range2.a2.r)));
                }

                // if range1 ends below range2, extract the lower part of range1
                if (range1.a2.r > range2.a2.r) {
                    resultRanges.push(Range.fromIndexes(range1.a1.c, range2.a2.r + 1, range1.a2.c, range1.a2.r));
                }
            });

            // early exit the loop, if all source ranges have been deleted already
            return resultRanges.length === 0;
        });

        // ensure to return a deep clone
        return (resultRanges === this) ? this.deepClone() : resultRanges;
    }

    /**
     * Creates an iterator that visits the single cell addresses of all cell
     * ranges in this list.
     *
     * @param [options]
     *  Optional parameters. If the option `reverse` is set, the single cell
     *  addresses in all cell ranges will be visited in reversed order too.
     *
     * @yields
     *  The cell addresses.
     */
    *addresses(options?: RangeIteratorOptions): IterableIterator<Address> {
        for (const range of this.values(options)) {
            yield* range.addresses(options);
        }
    }

    /**
     * Sorts the cell range addresses in this list in-place.
     *
     * @param [compare=Range.compare]
     *  The comparator callback function that receives two cell range addresses
     *  from this list, and returns their relation as signed number.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this array.
     */
    override sort(compare?: SortComparatorFn<Range>, context?: object): this {
        return super.sort(compare ?? Range.compare, context);
    }

    /**
     * Returns the A1 notation of this cell range list as used in document
     * operations.
     *
     * @returns
     *  The A1 notation of this cell range list as used in document operations.
     */
    toOpStr(): string {
        return this.map(range => range.toOpStr()).join(" ");
    }
}
