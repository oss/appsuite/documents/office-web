/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ArrayCallbackFn, ArrayPredicateFn  } from "@/io.ox/office/tk/algorithms";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type ArraySource, type SortComparatorFn, ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";

const { min, max } = Math;

// types ======================================================================

/**
 * Data type for values providing cell addresses to be consumed by methods of
 * the class `AddressArray`. Can be an array or an iterator with cell range
 * addresses, a single cell address, or nullish.
 */
export type AddressSource = ArraySource<Address>;

// class AddressArray =========================================================

/**
 * Represents an unordered list of cell addresses.
 */
export class AddressArray extends ArrayBase<Address> {

    // static methods ---------------------------------------------------------

    /**
     * If the passed data source is an `AddressArray`, it will be returned
     * directly (no shallow clone). Otherwise, a new array instance will be
     * created and filled with the elements from the data source.
     *
     * @param source
     *  The data source to be converted to an instance of `AddressArray`.
     *
     * @returns
     *  An instance of this class.
     */
    static cast(source: AddressSource): AddressArray {
        return (source instanceof AddressArray) ? source : new AddressArray(source);
    }

    /**
     * Creates an `AddressArray` from the passed data source with the cell
     * addresses passing a truth test.
     *
     * @param source
     *  The data source to be converted to a `AddressArray`.
     *
     * @param fn
     *  The predicate callback function invoked on the elements of the data
     *  source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The cell addresses passing the truth test.
     */
    static filter(source: AddressSource, fn: ArrayPredicateFn<Address>, context?: object): AddressArray {
        return new AddressArray().appendFiltered(source, fn, context);
    }

    /**
     * Creates an `AddressArray` from the return values of a callback function
     * that will be invoked on the elements of the passed data source.
     *
     * @param source
     *  The data source to be converted to an `AddressArray`.
     *
     * @param fn
     *  The callback function invoked on the elements of the data source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The cell addresses returned from the callback function.
     */
    static map<U>(source: ArraySource<U>, fn: ArrayCallbackFn<AddressSource, U>, context?: object): AddressArray {
        return new AddressArray().appendMapped(source, fn, context);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether a cell address in this list is equal to the passed cell
     * address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether this list contains the passed cell address.
     */
    contains(address: Address): boolean {
        return this.some(element => element.equals(address));
    }

    /**
     * Returns the address of the bounding cell range of all cell addresses in
     * this list (the smallest range that contains all addresses in the list).
     *
     * @returns
     *  The address of the bounding cell range containing all cell addresses;
     *  or `undefined`, if this cell address list is empty.
     */
    boundary(): Opt<Range> {

        const l = this.length;
        if (l === 0) { return undefined; }

        // this method must not return original array elements
        const boundary = new Range(this[0].clone());
        for (let i = 1; i < l; i += 1) {
            const address = this[i];
            boundary.a1.c = min(boundary.a1.c, address.c);
            boundary.a1.r = min(boundary.a1.r, address.r);
            boundary.a2.c = max(boundary.a2.c, address.c);
            boundary.a2.r = max(boundary.a2.r, address.r);
        }
        return boundary;
    }

    /**
     * Sorts the cell addresses in this list in-place.
     *
     * @param [compare=Address.compare]
     *  The comparator callback function that receives two cell addresses from
     *  this list, and returns their relation as signed number.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this array.
     */
    override sort(compare?: SortComparatorFn<Address>, context?: object): this {
        return super.sort(compare ?? Address.compare, context);
    }

    /**
     * Returns the A1 notation of this cell address list as used in document
     * operations (space-separated).
     *
     * @returns
     *  The space-separated A1 notation of this cell address list as used in
     *  document operations.
     */
    toOpStr(): string {
        return this.map(address => address.toOpStr()).join(" ");
    }
}
