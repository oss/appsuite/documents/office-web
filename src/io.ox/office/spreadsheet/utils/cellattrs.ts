/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type OpColor, Color } from "@/io.ox/office/editframework/utils/color";
import { type OpBorder, NO_BORDER } from "@/io.ox/office/editframework/utils/border";
import type { PtAttrSet, StyledAttributeSet, CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import { MINOR_FONT_KEY } from "@/io.ox/office/editframework/utils/attributeutils";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";

// types ======================================================================

/**
 * Formatting attributes for the family "character".
 */
export type { CharacterAttributes };

/**
 * Formatting attributes for the family "cell".
 */
export interface CellAttributes {

    /**
     * Horizontal alignment of the text contents in the cell. Bound to the
     * apply flag "align".
     */
    alignHor: string;

    /**
     * Vertical alignment of the text contents in the cell. Bound to the apply
     * flag "align".
     */
    alignVert: string;

    /**
     * Specifies whether to break long text automatically into multiple text
     * lines. Has no effect on non-text cell results. Bound to the apply flag
     * "align".
     */
    wrapText: boolean;

    /**
     * The identifier of a number format code (used in OOXML together with the
     * operation "insertNumberFormat"). Bound to the apply flag "number".
     */
    formatId: number;

    /**
     * An explicit number format code, used in ODF, and in contexts where the
     * number format has not been created with the operation
     * "insertNumberFormat" (e.g. in conditional formatting rules). Bound to
     * the apply flag "number".
     */
    formatCode: string;

    /**
     * The fill type ("solid", "pattern", "gradient", or "inherit"). Bound to
     * the apply flag "fill".
     */
    fillType: string;

    /**
     * The fill color for the cell area, the background color of a pattern, or
     * the start color of a gradient. Bound to the apply flag "fill".
     */
    fillColor: OpColor;

    /**
     * The foreground color of a pattern, or the end color of a gradient. Bound
     * to the apply flag "fill".
     */
    foreColor: OpColor;

    /**
     * The identifier of a fill pattern. Bound to the apply flag "fill".
     */
    pattern: string;

    /**
     * The style of the left border of the cell. Bound to the apply flag
     * "border".
     */
    borderLeft: OpBorder;

    /**
     * The style of the right border of the cell. Bound to the apply flag
     * "border".
     */
    borderRight: OpBorder;

    /**
     * The style of the top border of the cell. Bound to the apply flag
     * "border".
     */
    borderTop: OpBorder;

    /**
     * The style of the bottom border of the cell. Bound to the apply flag
     * "border".
     */
    borderBottom: OpBorder;

    /**
     * The style of the diagonal border of the cell from top-left corner to
     * bottom-right corner. Bound to the apply flag "border".
     */
    borderDown: OpBorder;

    /**
     * The style of the diagonal border of the cell from bottom-left corner to
     * top-right corner. Bound to the apply flag "border".
     */
    borderUp: OpBorder;

    /**
     * Specifies whether the cell is unlocked. Locked cells in locked sheets
     * cannot be modified. Has no effect in unlocked sheets. Bound to the apply
     * flag "protect".
     */
    unlocked: boolean;

    /**
     * Specifies whether the formula expression of a formula cell will be
     * hidden in the user interface of a locked sheet. Has no effect in
     * unlocked sheets. Bound to the apply flag "protect".
     */
    hidden: boolean;
}

/**
 * Formatting attributes for the family "apply".
 */
export interface ApplyAttributes {

    /**
     * Specifies whether the font attributes of the style sheet will reset
     * explicit attributes of the target cell; or whether font attributes of a
     * cell have been changed manually.
     */
    font: boolean;

    /**
     * Specifies whether the fill attributes of the style sheet will reset
     * explicit attributes of the target cell; or whether fill attributes of a
     * cell have been changed manually.
     */
    fill: boolean;

    /**
     * Specifies whether the border attributes of the style sheet will reset
     * explicit attributes of the target cell; or whether border attributes of
     * a cell have been changed manually.
     */
    border: boolean;

    /**
     * Specifies whether the alignment attributes of the style sheet will reset
     * explicit attributes of the target cell; or whether alignment attributes
     * of a cell have been changed manually.
     */
    align: boolean;

    /**
     * Specifies whether the number format of the style sheet will reset
     * explicit attributes of the target cell; or whether the number format of
     * a cell has been changed manually.
     */
    number: boolean;

    /**
     * Specifies whether the protection flags of the style sheet will reset
     * explicit attributes of the target cell; or whether protection flags of a
     * cell have been changed manually.
     */
    protect: boolean;
}

/**
 * A merged attribute set for cells.
 */
export interface CellAttributeSet extends StyledAttributeSet {
    character: CharacterAttributes;
    cell: CellAttributes;
    apply: ApplyAttributes;
}

export type PtCellAttributeSet = PtAttrSet<CellAttributeSet>;

/**
 * Extended configuration options for character and cell attributes.
 */
export interface ExtCellAttrConfig {
    apply: KeysOf<ApplyAttributes>;
}

// constants ==================================================================

export const CHARACTER_ATTRIBUTES: AttributeConfigBag<CharacterAttributes, ExtCellAttrConfig> = {
    fontName:      { def: MINOR_FONT_KEY,  apply: "font" },
    fontSize:      { def: 11,              apply: "font" },
    bold:          { def: false,           apply: "font" },
    italic:        { def: false,           apply: "font" },
    underline:     { def: false,           apply: "font" },
    strike:        { def: "none",          apply: "font" },
    color:         { def: Color.AUTO,      apply: "font" },
    field:         { def: {},              apply: "font" },
    autoDateField: { def: false,           apply: "font" },
    url:           { def: "",              apply: "font" },
    language:      { def: "",              apply: "font" }
};

export const CELL_ATTRIBUTES: AttributeConfigBag<CellAttributes, ExtCellAttrConfig> = {
    alignHor:       { def: "auto",      apply: "align" },
    alignVert:      { def: "bottom",    apply: "align" },
    wrapText:       { def: false,       apply: "align" },
    formatId:       { def: 0,           apply: "number" },
    formatCode:     { def: "General",   apply: "number" },
    fillType:       { def: "solid",     apply: "fill",  merge: mergeFillType },
    fillColor:      { def: Color.AUTO,  apply: "fill" },
    foreColor:      { def: Color.AUTO,  apply: "fill" },
    pattern:        { def: "solid",     apply: "fill" },
    borderLeft:     { def: NO_BORDER,   apply: "border" },
    borderRight:    { def: NO_BORDER,   apply: "border" },
    borderTop:      { def: NO_BORDER,   apply: "border" },
    borderBottom:   { def: NO_BORDER,   apply: "border" },
    borderDown:     { def: NO_BORDER,   apply: "border" },
    borderUp:       { def: NO_BORDER,   apply: "border" },
    unlocked:       { def: false,       apply: "protect" },
    hidden:         { def: false,       apply: "protect" }
};

// definitions for "apply" group flags (used in OOXML only!)
export const APPLY_ATTRIBUTES: AttributeConfigBag<ApplyAttributes> = {
    font:    { def: true },
    fill:    { def: true },
    border:  { def: true },
    align:   { def: true },
    number:  { def: true },
    protect: { def: true }
};

// private functions ==========================================================

// fill type "inherit" is used in conditional formatting to switch from "gradient" to "pattern", but inherit all other fill types
function mergeFillType(type1: string, type2: string): string {
    return (type2 !== "inherit") ? type2 : (type1 === "gradient") ? "pattern" : type1;
}

// public functions ===========================================================

/**
 * Returns whether the passed merged attribute set will force to split the
 * text contents of a cell into multiple lines, either by the cell
 * attribute "wrapText" set to true, or by horizontally or vertically
 * justified alignment.
 *
 * @param attrSet
 *  A merged (complete) cell attribute set.
 *
 * @returns
 *  Whether the attribute set will force to split the text contents of a
 *  cell into multiple lines.
 */
export function hasWrappingAttributes(attrSet: CellAttributeSet): boolean {
    const cellAttrs = attrSet.cell;
    return !!cellAttrs.wrapText || (cellAttrs.alignHor === "justify") || (cellAttrs.alignVert === "justify");
}
