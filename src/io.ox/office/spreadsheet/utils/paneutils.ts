/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { AnyKeyEvent, ModifierKeys } from "@/io.ox/office/tk/dom";
import { COMPACT_DEVICE, matchKeyCode } from "@/io.ox/office/tk/dom";
import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";

// types ======================================================================

/**
 * Enumeration with identifiers of all column panes.
 */
export enum ColPaneSide {
    L = "left",
    R = "right"
}

/**
 * Enumeration with identifiers of all row panes.
 */
export enum RowPaneSide {
    T = "top",
    B = "bottom"
}

/**
 * Enumeration with identifiers of all (column and row) header panes.
 */
export const PaneSide = { ...ColPaneSide, ...RowPaneSide };
// eslint-disable-next-line @typescript-eslint/no-redeclare
export type PaneSide = ColPaneSide | RowPaneSide;

/**
 * Enumeration with identifiers of all grid panes.
 */
export enum PanePos {
    TL = "topLeft",
    TR = "topRight",
    BL = "bottomLeft",
    BR = "bottomRight"
}

/**
 * Different selection modes according to keyboard modifier keys.
 */
export enum SelectionMode {

    /**
     * Standard selection without modifier keys.
     */
    SELECT = 0,

    /**
     * Append new range to current selection (`CTRL/META` key).
     *
     */
    APPEND = 1,

    /**
     * Extend current active range (`SHIFT` key).
     */
    EXTEND = 2
}

// private types --------------------------------------------------------------

interface PaneSideData {
    nextSide: PaneSide;
    panePos1: PanePos;
    panePos2: PanePos;
    columns: boolean;
    leading: boolean;
    anchorName: string;
}

interface PanePosData {
    colSide: ColPaneSide;
    rowSide: RowPaneSide;
    nextColPos: PanePos;
    nextRowPos: PanePos;
}

// private constants ==========================================================

// configuration for pane sides (header panes)
const PANE_SIDE_DATA: RoRecord<PaneSide, PaneSideData> = {
    [PaneSide.L]: { nextSide: PaneSide.R, panePos1: PanePos.TL, panePos2: PanePos.BL, columns: true,  leading: true,  anchorName: "anchorLeft" },
    [PaneSide.R]: { nextSide: PaneSide.L, panePos1: PanePos.TR, panePos2: PanePos.BR, columns: true,  leading: false, anchorName: "anchorRight" },
    [PaneSide.T]: { nextSide: PaneSide.B, panePos1: PanePos.TL, panePos2: PanePos.TR, columns: false, leading: true,  anchorName: "anchorTop" },
    [PaneSide.B]: { nextSide: PaneSide.T, panePos1: PanePos.BL, panePos2: PanePos.BR, columns: false, leading: false, anchorName: "anchorBottom" }
};

// configuration for pane positions (grid panes)
const PANE_POS_DATA: RoRecord<PanePos, PanePosData> = {
    [PanePos.TL]: { colSide: ColPaneSide.L, rowSide: RowPaneSide.T, nextColPos: PanePos.TR, nextRowPos: PanePos.BL },
    [PanePos.TR]: { colSide: ColPaneSide.R, rowSide: RowPaneSide.T, nextColPos: PanePos.TL, nextRowPos: PanePos.BR },
    [PanePos.BL]: { colSide: ColPaneSide.L, rowSide: RowPaneSide.B, nextColPos: PanePos.BR, nextRowPos: PanePos.TL },
    [PanePos.BR]: { colSide: ColPaneSide.R, rowSide: RowPaneSide.B, nextColPos: PanePos.BL, nextRowPos: PanePos.TR }
};

// used to map pane side identifiers to pane position identifiers
const PANE_SIDE_TO_PANE_POS: RoRecord<ColPaneSide, RoRecord<RowPaneSide, PanePos>> = {
    [ColPaneSide.L]: { [RowPaneSide.T]: PanePos.TL, [RowPaneSide.B]: PanePos.BL },
    [ColPaneSide.R]: { [RowPaneSide.T]: PanePos.TR, [RowPaneSide.B]: PanePos.BR }
};

// public constants ===========================================================

/**
 * All available pane side identifiers, as array of strings.
 */
export const ALL_PANE_SIDES = Object.keys(PANE_SIDE_DATA) as readonly PaneSide[];

/**
 * All available pane position identifiers, as array of strings.
 */
export const ALL_PANE_POSITIONS = Object.keys(PANE_POS_DATA) as readonly PanePos[];

/**
 * The minimum size of grid/header panes, in pixels.
 */
export const MIN_PANE_SIZE = 10;

/**
 * The ratio of the header/grid pane size to be rendered additionally before
 * and after the visible area.
 */
export const ADDITIONAL_SIZE_RATIO = COMPACT_DEVICE ? 0.15 : 0.2;

// public functions ===========================================================

/**
 * Returns whether the passed pane side represents a column header pane.
 *
 * @param paneSide
 *  The pane side identifier.
 *
 * @returns
 *  Value `true` for the pane side identifiers `L` and `R`; `false` otherwise.
 */
export function isColumnSide(paneSide: PaneSide): boolean {
    return PANE_SIDE_DATA[paneSide].columns;
}

/**
 * Returns whether the passed pane side represents a leading header pane.
 *
 * @param paneSide
 *  The pane side identifier.
 *
 * @returns
 *  Value `true` for the pane side identifiers `L` and `T`; `false` otherwise.
 */
export function isLeadingSide(paneSide: PaneSide): boolean {
    return PANE_SIDE_DATA[paneSide].leading;
}

/**
 * Returns the name of the scroll anchor view property for a pane side.
 *
 * @param paneSide
 *  The pane side identifier.
 *
 * @returns
 *  The name of the scroll anchor sheet view property.
 */
export function getScrollAnchorName(paneSide: PaneSide): string {
    return PANE_SIDE_DATA[paneSide].anchorName;
}

/**
 * Returns the column pane side identifier for the passed grid pane position.
 *
 * @param panePos
 *  The pane position.
 *
 * @returns
 *  The pane side `L` for the pane positions `TL` and `BL`, or `R` for the pane
 *  positions `TR` and `BR`.
 */
export function getColPaneSide(panePos: PanePos): ColPaneSide {
    return PANE_POS_DATA[panePos].colSide;
}

/**
 * Returns the row pane side identifier for the passed grid pane position.
 *
 * @param panePos
 *  The pane position.
 *
 * @returns
 *  The pane side `T` for the pane positions `TL` and `TR`, or `B` for the pane
 *  positions `BL` and `BR`.
 */
export function getRowPaneSide(panePos: PanePos): RowPaneSide {
    return PANE_POS_DATA[panePos].rowSide;
}

/**
 * Returns the pane position of the horizontal neighbor of the passed grid pane
 * position.
 *
 * @param panePos
 *  The pane position.
 *
 * @returns
 *  The pane position of the horizontal neighbor (for example, returns `BL` for
 *  the pane position `BR`).
 */
export function getNextColPanePos(panePos: PanePos): PanePos {
    return PANE_POS_DATA[panePos].nextColPos;
}

/**
 * Returns the pane position of the vertical neighbor of the passed grid pane
 * position.
 *
 * @param panePos
 *  The pane position.
 *
 * @returns
 *  The pane position of the vertical neighbor (for example, returns `TR` for
 *  the pane position `BR`).
 */
export function getNextRowPanePos(panePos: PanePos): PanePos {
    return PANE_POS_DATA[panePos].nextRowPos;
}

/**
 * Returns the pane position identifier according to the passed pane side
 * identifiers.
 *
 * @param colPaneSide
 *  The column pane side.
 *
 * @param rowPaneSide
 *  The row pane side.
 *
 * @returns
 *  The pane position for the passed pane side identifiers.
 */
export function getPanePos(colPaneSide: ColPaneSide, rowPaneSide: RowPaneSide): PanePos {
    return PANE_SIDE_TO_PANE_POS[colPaneSide][rowPaneSide];
}

/**
 * Returns the pane position nearest to the specified source pane position, but
 * matching the specified target pane side.
 *
 * @param panePos
 *  The source pane position.
 *
 * @param paneSide
 *  The target pane side.
 *
 * @returns
 *  The grid pane position of the nearest neighbor, matching the specified pane
 *  side (for example, returns `TR` for the source pane position `BR` and the
 *  target pane side `T`).
 */
export function getNextPanePos(panePos: PanePos, paneSide: PaneSide): PanePos {
    const panePosData = PANE_POS_DATA[panePos];
    const paneSideData = PANE_SIDE_DATA[paneSide];
    return ((paneSide === panePosData.colSide) || (paneSide === panePosData.rowSide)) ? panePos :
        (paneSideData.columns ? panePosData.nextColPos : panePosData.nextRowPos);
}

// events ---------------------------------------------------------------------

/**
 * Returns the GUI selection mode according to the modifier keys in the
 * passed GUI event.
 *
 * @param modifiers
 *  The object to be checked. Can be any DOM event with modifier flags (e.g.,
 *  classes `KeyboardEvent`, `MouseEvent`, or `TouchEvent`), a `JQuery.Event`,
 *  or the property "modifiers" of a `TrackingRecord`.
 *
 * @returns
 *  The selection mode according to the keyboard modifier keys.
 */
export function getSelectionMode(modifiers: Partial<ModifierKeys>): SelectionMode {
    const ctrlMeta = modifiers.ctrlKey || modifiers.metaKey;
    if (!modifiers.shiftKey && ctrlMeta) { return SelectionMode.APPEND; }
    if (modifiers.shiftKey && !ctrlMeta) { return SelectionMode.EXTEND; }
    return SelectionMode.SELECT;
}

/**
 * Returns the move direction for the active cell according to the passed
 * GUI keyboard event.
 *
 * @param event
 *  The `keydown` event object.
 *
 * @returns
 *  The value `undefined`, if the passed event would not move the active cell;
 *  otherwise, the move direction for the active cell:
 *  - `LEFT` for `SHIFT+TAB` key,
 *  - `RIGHT` for `TAB` key,
 *  - `UP` for `SHIFT+ENTER` key,
 *  - `DOWN` for `ENTER` key.
 */
export function getCellMoveDirection(event: AnyKeyEvent): Opt<Direction> {
    if (matchKeyCode(event, "ENTER", { shift: null })) {
        return event.shiftKey ? Direction.UP : Direction.DOWN;
    }
    if (matchKeyCode(event, "TAB", { shift: null })) {
        return event.shiftKey ? Direction.LEFT : Direction.RIGHT;
    }
    return undefined;
}
