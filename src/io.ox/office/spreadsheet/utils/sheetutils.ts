/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { is, str, jpromise } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE, isRightToLeft } from "@/io.ox/office/tk/dom";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import type { OTSortVector } from "@/io.ox/office/editframework/utils/otutils";
import type { OpBorder, LineWidthPreset } from "@/io.ox/office/editframework/utils/border";
import { getWidthForPreset, getPresetForWidth } from "@/io.ox/office/editframework/utils/border";
import type { MixedBorder } from "@/io.ox/office/editframework/utils/mixedborder";
import type { DisplayText } from "@/io.ox/office/editframework/model/formatter/baseformatter";

import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";
import type { Address } from "@/io.ox/office/spreadsheet/utils/address";
import type { Range } from "@/io.ox/office/spreadsheet/utils/range";

const { ceil } = Math;

// re-exports =================================================================

export * from "@/io.ox/office/spreadsheet/utils/scalar";
export * from "@/io.ox/office/spreadsheet/utils/interval";
export * from "@/io.ox/office/spreadsheet/utils/address";
export * from "@/io.ox/office/spreadsheet/utils/range";
export * from "@/io.ox/office/spreadsheet/utils/range3d";
export * from "@/io.ox/office/spreadsheet/utils/intervalarray";
export * from "@/io.ox/office/spreadsheet/utils/addressarray";
export * from "@/io.ox/office/spreadsheet/utils/rangearray";
export * from "@/io.ox/office/spreadsheet/utils/range3darray";
export * from "@/io.ox/office/spreadsheet/utils/addressset";
export * from "@/io.ox/office/spreadsheet/utils/rangeset";
export * from "@/io.ox/office/spreadsheet/utils/cellanchor";

export type { DisplayText };

// types ======================================================================

/**
 * An optional index of a sheet in a spreadsheet document. Omitting a sheet may
 * have different reasons, e.g. to specify globally defined names, or to denote
 * a missing reference sheet when parsing or evaluating a formula (unqualified
 * cell references as in `=A1`).
 */
export type OptSheet = number | null;

/**
 * An index vector that maps the old sheet indexes (array index) to their new
 * position (array elements) after inserting, deleting, or moving sheets in the
 * spreadsheet document.
 *
 * In case of a deleted sheet, the array element will have the value
 * `undefined` at the respective position. The index of an inserted sheet will
 * not appear in the vector.
 *
 * Note that this is the *reversed* logic of `OTSortVevtor` which contains the
 * old sheet indexes as array *values* at new sheet position (array *index*).
 */
export type TransformSheetVector = ArrayLike<OptSheet>;

/**
 * Enumeration for the types of sheets that can occur in a spreadsheet
 * document. The enumeration string values correspond to the values used in the
 * document operation `insertSheet`.
 */
export enum SheetType {

    /**
     * A regular sheet with value cells, formula cells, and drawing objects.
     */
    WORKSHEET = "worksheet",

    /**
     * A sheet consisting of a single big chart object.
     */
    CHARTSHEET = "chartsheet",

    /**
     * A sheet consisting of formula cells with specific macro formulas. Used
     * for compatibility with MS Excel 4.0 only.
     */
    MACROSHEET = "macrosheet",

    /**
     * A sheet consisting of a single dialog box. Used for compatibility with
     * MS Excel 5.0 only.
     */
    DIALOGSHEET = "dialogsheet"
}

/**
 * Enumeration for the split modes for a sheet. The enumeration string values
 * correspond to the values used in the sheet formatting attribute `splitMode`.
 */
export enum SplitMode {

    /**
     * Specifies a dynamic split with resizable view panes.
     */
    SPLIT = "split",

    /**
     * Specifies a frozen split with fixed view panes.
     */
    FROZEN = "frozen",

    /**
     * Specifies a frozen split similar to FROZEN, but thawing the frozen split
     * will change split mode to SPLIT instead of removing the split
     * completely.
     */
    FROZEN_SPLIT = "frozenSplit"
}

/**
 * Enumeration for the direction to move the cells in cell ranges to. The
 * enumeration string values correspond to the values used in the document
 * operation `moveCells`.
 */
export enum Direction {

    /**
     * A cell range will be deleted, the following cells right of the cell
     * range will be moved to the left.
     */
    LEFT = "left",

    /**
     * A cell range will be made blank by moving the existing cells to the
     * right. Cells moved outside the sheet will be deleted.
     */
    RIGHT = "right",

    /**
     * A cell range will be deleted, the following cells below the cell range
     * will be moved up.
     */
    UP = "up",

    /**
     * A cell range will be made blank by moving the existing cells down. Cells
     * moved outside the sheet will be deleted.
     */
    DOWN = "down"
}

/**
 * Enumeration for the merge modes for cell ranges. The enumeration string
 * values corresponds to the values used in the document operation
 * `mergeCells`.
 */
export enum MergeMode {

    /**
     * Merges an entire cell range (multiple columns and rows).
     */
    MERGE = "merge",

    /**
     * Merges the individual rows in a cell range.
     */
    HORIZONTAL = "horizontal",

    /**
     * Merges the individual columns in a cell range.
     */
    VERTICAL = "vertical",

    /**
     * Converts the affected merged ranges to single cells.
     */
    UNMERGE = "unmerge"
}

/**
 * A sorting rule for a cell range or table range.
 */
export interface SortRule {
    index: number;
    descending: boolean;
}

/**
 * The single-character keys of all supported outer cell borders.
 *
 * - "l" = Left border line.
 * - "r" = Right border line.
 * - "t" = Top border line.
 * - "b" = Bottom border line.
 */
export type OuterBorderKey = (typeof OUTER_BORDER_KEYS)[number];

/**
 * The single-character keys of all supported inner cell borders.
 *
 * - "v": Vertical inner border line.
 * - "h": Horizontal inner border line.
 */
export type InnerBorderKey = (typeof INNER_BORDER_KEYS)[number];

/**
 * The single-character keys of all supported diagonal cell borders.
 *
 * - "d": Top-left to bottom-right border line ("down").
 * - "u": Bottom-left to top-right border line ("up").
 */
export type DiagonalBorderKey = (typeof DIAGONAL_BORDER_KEYS)[number];

/**
 * The single-character keys of all cell borders that can occur in attribute
 * sets in document operations (all but inner borders which are only used at
 * runtime to generate different attribute sets for the affected cells).
 */
export type AttrBorderKey = OuterBorderKey | DiagonalBorderKey;

/**
 * The single-character keys of all supported cell borders.
 */
export type BorderKey = AttrBorderKey | InnerBorderKey;

/**
 * The attribute names of all supported outer cell borders.
 */
export type OuterBorderName = "borderLeft" | "borderRight" | "borderTop" | "borderBottom";

/**
 * The attribute names of all supported inner cell borders.
 */
export type InnerBorderName = "borderInsideVert" | "borderInsideHor";

/**
 * The attribute names of all supported diagonal cell borders.
 */
export type DiagonalBorderName = "borderDown" | "borderUp";

/**
 * The attribute names of all cell border that can occur in attribute sets in
 * document operations (all but inner borders which are only used at runtime to
 * generate different attribute sets for the affected cells).
 */
export type AttrBorderName = OuterBorderName | DiagonalBorderName;

/**
 * The attribute names of all supported cell borders.
 */
export type BorderName = AttrBorderName | InnerBorderName;

/**
 * The short keys of all corners in a border frame.
 */
export type BorderCornerKey = (typeof BORDER_CORNER_KEYS)[number];

/**
 * Values of the CSS attribute `text-align` used in the Spreadsheet API.
 */
export type TextAlignment = "left" | "center" | "right" | "justify";

/**
 * Text orientation settings according to specific cell formatting attributes.
 */
export interface TextOrientation {

    /**
     * The effective writing direction in the single text lines of the display
     * text.
     */
    rtl: boolean;

    /**
     * The effective writing direction in the single text lines of the display
     * text, as value supported by the HTML element attribute `dir`.
     */
    domTextDir: "ltr" | "rtl";

    /**
     * The effective horizontal CSS text alignment, as value supported by the
     * CSS property `text-align`. For automatic cell alignment, the resulting
     * text alignment depends on the data type of the cell value. Numbers will
     * be aligned to the right, boolean values and error literals will be
     * centered, and strings will be aligned according to their own writing
     * direction (NOT the display text of the cell value which may differ from
     * the string, e.g. by using a specific number format). Will be one of the
     * following value:
     * - `left`: The text lines are aligned to the left in their bounding box.
     * - `center`: The text lines are centered in their bounding box.
     * - `right`: The text lines are aligned to the right in their bounding
     *   box.
     * - `justify`: The text lines will be expanded to the width of the
     *   bounding box. The alignment of a single text line, or of the last text
     *   line in multi-line cells is dependent on the default writing direction
     *   of the text.
     */
    cssTextAlign: TextAlignment;

    /**
     * The resolved horizontal alignment of the single text lines inside their
     * bounding box. Will be equal to the property `cssTextAlign` except for
     * justified alignment (property `cssTextAlign` contains the value
     * `justify`) which will be resolved to `left` or `right`, depending on the
     * default writing direction of the text value (NOT the display string).
     */
    baseBoxAlign: TextAlignment;
}

/**
 * The components of a formula in text edit mode (e.g. "=1+1" or "+1+1").
 */
export interface EditFormulaParts {

    /**
     * The prefix character that is not part of the formula expression. Will be
     * the equals sign (e.g. for the edit text "=1+1"), or an empty string
     * (e.g. for the edit text "+1+1").
     */
    prefix: string;

    /**
     * The formula expression without prefix character (e.g. "1+1" for the edit
     * text "=1+1", or the complete edit text "+1+1").
     */
    expr: string;
}

/**
 * Options for cell or range position in either pixels or 1/100 of millimeters.
 */
export interface SheetPixelOptions {

    /**
     * If set to `true`, the input parameter will be interpreted as, or the
     * return value will be converted to pixels, according to the current zoom
     * factor of the sheet. By default, length values will be interpreted as
     * 1/100 of millimeters (independent from sheet zoom factor).
     */
    pixel?: boolean;
}

// constants ==================================================================

/**
 * Maximum length of a defined name.
 */
export const MAX_NAME_LENGTH = 255;

/**
 * The maximum number of cells to be filled with one range operation, or
 * auto-fill operation.
 */
export const MAX_FILL_CELL_COUNT = 20000;

/**
 * The maximum number of entire columns/rows to be filled with one
 * auto-fill operation.
 */
export const MAX_AUTOFILL_COL_ROW_COUNT = 100;

/**
 * Maximum number of merged ranges to be created with a single operation.
 */
export const MAX_MERGED_RANGES_COUNT = 1000;

/**
 * The maximum number of cells to be filled when unmerging a range.
 */
export const MAX_UNMERGE_CELL_COUNT = 5000;

/**
 * The maximum number of columns to be changed with one row operation.
 */
export const MAX_CHANGE_COLS_COUNT = 20000;

/**
 * The maximum number of rows to be changed with one row operation.
 */
export const MAX_CHANGE_ROWS_COUNT = 5000;

/**
 * The maximum number of columns/rows that can be sorted.
 */
export const MAX_SORT_LINES_COUNT = 5000;

/**
 * The maximum length of the columns/rows to be sorted.
 */
export const MAX_SORT_LINE_LENGTH = 100;

/**
 * Maximum number of characters for the absolute part of a number formatted
 * with the standard number format (in cells).
 */
export const MAX_LENGTH_STANDARD_CELL = 11;

/**
 * Maximum number of characters for the absolute part of a number formatted
 * with the standard number format (in cell edit mode).
 */
export const MAX_LENGTH_STANDARD_EDIT = 21;

/**
 * Maximum number of characters for the absolute part of a number formatted
 * with the standard number format (in formula interpretation, e.g. when
 * converting numbers to strings for functions expecting strings).
 */
export const MAX_LENGTH_STANDARD_FORMULA = 20;

/**
 * Minimum effective size of a cell, in CSS pixels.
 */
export const MIN_CELL_SIZE = 5;

/**
 * Maximum width of cell border lines (in CSS pixels), to prevent that the
 * left/right borders or the top/bottom borders of a cell overlay each other.
 */
export const MAX_BORDER_WIDTH = MIN_CELL_SIZE;

/**
 * Whether multi-selection (multiple ranges at the same time) is supported.
 */
export const MULTI_SELECTION = !TOUCH_DEVICE;

/**
 * Short keys for all outer border attributes in a cell.
 */
export const OUTER_BORDER_KEYS = ["t", "b", "l", "r"] as const;

/**
 * Short keys for all outer border attributes in a cell.
 */
export const INNER_BORDER_KEYS = ["h", "v"] as const;

/**
 * Short keys for all diagonal border attributes in a cell.
 */
export const DIAGONAL_BORDER_KEYS = ["d", "u"] as const;

/**
 * Short keys for all border attributes in a cell.
 */
export const ATTR_BORDER_KEYS = [...OUTER_BORDER_KEYS, ...DIAGONAL_BORDER_KEYS] as const;

/**
 * Short keys for all corners of a border frame.
 */
export const BORDER_CORNER_KEYS = ["tl", "tr", "bl", "br"] as const;

// private constants ----------------------------------------------------------

// maps reverse directions for fast look-up
const REVERSE_DIRECTIONS = {
    [Direction.LEFT]: Direction.RIGHT,
    [Direction.RIGHT]: Direction.LEFT,
    [Direction.UP]: Direction.DOWN,
    [Direction.DOWN]: Direction.UP
};

// maps single-character border keys to the attribute names of cell borders
const BORDER_ATTRIBUTE_NAMES: Record<BorderKey, BorderName> = {
    l: "borderLeft",
    r: "borderRight",
    t: "borderTop",
    b: "borderBottom",
    d: "borderDown",
    u: "borderUp",
    v: "borderInsideVert",
    h: "borderInsideHor"
};

// maps horizontal alignment values used in operations to CSS text alignment values
const CSS_TEXT_ALIGN_VALUES: Dict<TextAlignment> = {
    left: "left",
    center: "center",
    right: "right",
    justify: "justify",
    distribute: "justify",
    fillAcross: "left",
    centerAcross: "center"
};

// singletons =================================================================

/**
 * Logger for messages related to the various document models, bound to the
 * debug configuration flag `spreadsheet:log-models`.
 */
export const modelLogger = new Logger("spreadsheet:log-models", { tag: "MODEL", tagColor: 0x00FF00 });

// private functions ==========================================================

// converts the passed text direction to left/right alignment
function getAlignForDir(rtl: boolean): TextAlignment {
    return rtl ? "right" : "left";
}

// public functions ===========================================================

/**
 * Convenience shortcut that returns a rejected promise with a specific message
 * code as object with `cause` property.
 *
 * @param msgCode
 *  The message code the returned promise will be rejected with.
 *
 * @returns
 *  A promise that has already been rejected with an object containing a string
 *  property `cause` set to the passed message code.
 */
export function makeRejected<FT = void>(msgCode: string): JPromise<FT> {
    return jpromise.reject({ cause: msgCode });
}

/**
 * Returns a unique map key for the passed name of a model object.
 *
 * @param name
 *  The original name of a model object.
 *
 * @returns
 *  A unique map key for the passed name.
 */
export function mapKey(name: string): string {
    return name.toUpperCase();
}

/**
 * Inverses the passed permutation vector, so that applying it to an array will
 * restore its old order.
 *
 * @param sortVector
 *  The permutation vector to be inversed.
 *
 * @returns
 *  The inversed permutation vector.
 */
export function inverseSortVector(sortVector: OTSortVector): OTSortVector {
    const inverseVector: OTSortVector = new Array<number>(sortVector.length);
    sortVector.forEach((oldIndex, newIndex) => (inverseVector[oldIndex] = newIndex));
    return inverseVector;
}

/**
 * Returns whether the passed sheet type is supported by the spreadsheet
 * application. Unsupported sheets will always be hidden in the user interface.
 *
 * @param sheetType
 *  The sheet type to be tested.
 *
 * @returns
 *  Whether the passed sheet type is supported.
 */
export function isSupportedSheetType(sheetType: SheetType): boolean {
    return (sheetType === SheetType.WORKSHEET) || (sheetType === SheetType.CHARTSHEET);
}

/**
 * Returns whether sheets of the passed type contain cells (either a worksheet,
 * or a legacy macro sheet).
 *
 * @param sheetType
 *  The sheet type to be tested.
 *
 * @returns
 *  Whether this sheet contains cells.
 */
export function isCellSheetType(sheetType: SheetType): boolean {
    return (sheetType === SheetType.WORKSHEET) || (sheetType === SheetType.MACROSHEET);
}

/**
 * Returns the localized sheet name with the specified index, ready to be used
 * in the document model (without any I18N marker characters used for UI
 * debugging).
 *
 * @param index
 *  The one-based (!) sheet index to be inserted into the sheet name.
 *
 * @returns
 *  The generated sheet name (e.g. `Sheet1`, `Sheet2`, etc.).
 */
export function generateSheetName(index: number): string {
    //#. Default sheet names in spreadsheet documents. Should be equal to the sheet
    //#. names used in existing spreadsheet applications (Excel, OpenOffice Calc, ...).
    //#. %1$d is the numeric index inside the sheet name
    //#. Resulting names e.g. "Sheet1", "Sheet2", etc.
    //#. No white-space between the "Sheet" label and sheet index!
    return _.noI18n.fix(gt("Sheet%1$d", index));
}

/**
 * Returns the translated word for "Table" to be used for new table ranges,
 * ready to be used in the document model (without any I18N marker characters
 * used for UI debugging).
 *
 * @returns
 *  The translated word for "Table".
 */
export function getTableName(): string {
    //#. Default name for table ranges (cell ranges in a sheet with filter settings) in spreadsheet
    //#. documents. Will be used to create serially numbered tables, e.g. "Table1", "Table2", etc.
    return _.noI18n.fix(gt.pgettext("filter", "Table"));
}

/**
 * Returns the translated word for "Column", ready to be used in the document
 * model (without any I18N marker characters used for UI debugging).
 *
 * @returns
 *  The translated word for "Column".
 */
export function getTableColName(): string {
    //#. Default name for columns in filtered ranges in spreadsheet documents. Will be
    //#. used to create serially numbered columns, e.g. "Column1", "Column2", etc.
    return _.noI18n.fix(gt.pgettext("filter", "Column"));
}

/**
 * Returns the effective text orientation settings for the passed cell value,
 * formatted display string, and horizontal alignment.
 *
 * @param value
 *  A scalar cell value. Influences the behavior of the `auto` alignment. The
 *  value `null` represents a blank cell.
 *
 * @param align
 *  The horizontal alignment, as used in the cell attribute `alignHor`. The
 *  value `auto` will cause special behavior based on the passed cell value and
 *  display string.
 *
 * @param [display=""]
 *  The formatted display string for the passed cell value. Can be omitted, or
 *  set to `null`, to return the text orientation based on the cell value only.
 *
 * @param [preferText=false]
 *  If set to `true`, numbers will be left-aligned like text (but booleans and
 *  error codes remain centered).
 *
 * @returns
 *  A descriptor containing the text orientation settings.
 */
export function getTextOrientation(value: ScalarType, align: string, display?: DisplayText, preferText?: boolean): TextOrientation {

    // the effective writing direction of the display text
    const rtl = is.string(display) ? isRightToLeft(display) : LOCALE_DATA.rtl;
    const domTextDir = rtl ? "rtl" : "ltr";

    // the effective CSS text alignment
    let cssTextAlign = CSS_TEXT_ALIGN_VALUES[align];
    if (!cssTextAlign) {
        if (value === null) {
            // blank cells depend on system writing direction
            cssTextAlign = getAlignForDir(LOCALE_DATA.rtl);
        } else if (is.number(value)) {
            // numbers always right aligned, independent from display string (DOCS-2550: except for TEXT number format)
            cssTextAlign = preferText ? "left" : "right";
        } else if (is.string(value)) {
            // strings depend on writing direction of the text result (not display string!)
            cssTextAlign = getAlignForDir((value === display) ? rtl : isRightToLeft(value));
        } else {
            // booleans and error codes always centered
            cssTextAlign = "center";
        }
    }

    // base box alignment in justified cells depends on writing direction of display string
    const baseBoxAlign = (cssTextAlign === "justify") ? getAlignForDir(rtl) : cssTextAlign;

    // return the orientation descriptor object
    return { rtl, domTextDir, cssTextAlign, baseBoxAlign };
}

/**
 * Returns a single-character key for an outer cell border.
 *
 * @param columns
 *  Whether to return the key of a vertical border (`true`), or of a horizontal
 *  border (`false`).
 *
 * @param leading
 *  Whether to return the key of a leading border (`true`), or of a trailing
 *  border (`false`).
 *
 * @returns
 *  The border key.
 */
export function getOuterBorderKey(columns: boolean, leading: boolean): OuterBorderKey {
    return columns ? (leading ? "l" : "r") : (leading ? "t" : "b");
}

/**
 * Returns a single-character key for an inner cell border.
 *
 * @param columns
 *  Whether to return the key of the vertical inner border (`true`), or of the
 *  horizontal inner border (`false`).
 *
 * @returns
 *  The border key.
 */
export function getInnerBorderKey(columns: boolean): InnerBorderKey {
    return columns ? "v" : "h";
}

/**
 * Returns the attribute name of a cell border.
 *
 * @param key
 *  The single-character key of a cell border.
 *
 * @returns
 *  The name of the specified border attribute.
 */
export function getBorderName(key: OuterBorderKey): OuterBorderName;
export function getBorderName(key: InnerBorderKey): InnerBorderName;
export function getBorderName(key: DiagonalBorderKey): DiagonalBorderName;
export function getBorderName(key: AttrBorderKey): AttrBorderName;
export function getBorderName(key: BorderKey): BorderName;
// implementation
export function getBorderName(key: BorderKey): BorderName {
    return BORDER_ATTRIBUTE_NAMES[key];
}

/**
 * Returns the attribute name of an outer cell border.
 *
 * @param columns
 *  Whether to return the name of a vertical border attribute (`true`), or of a
 *  horizontal border attribute (`false`).
 *
 * @param leading
 *  Whether to return the name of a leading border attribute (`true`), or of a
 *  trailing border attribute (`false`).
 *
 * @returns
 *  One of the border attribute names (`borderTop`, `borderBottom`,
 *  `borderLeft`, or `borderRight`).
 */
export function getOuterBorderName(columns: boolean, leading: boolean): OuterBorderName {
    return getBorderName(getOuterBorderKey(columns, leading));
}

/**
 * Returns the attribute name of an inner cell border.
 *
 * @param columns
 *  Whether to return the name of the vertical inner border attribute (`true`),
 *  or of the horizontal inner border attribute (`false`).
 *
 * @returns
 *  One of the border attribute names `borderInsideHor` or `borderInsideVert`.
 */
export function getInnerBorderName(columns: boolean): InnerBorderName {
    return getBorderName(getInnerBorderKey(columns));
}

/**
 * Returns the identifier of a predefined border style for the passed mixed
 * border object.
 *
 * @param mixedBorder
 *  The mixed border to be converted to a predefined border style.
 *
 * @returns
 *  The identifier of a predefined border style; either the string "none", or
 *  two string tokens separated by a colon character. The first token
 *  represents the line style as used in all border attribute values (except
 *  "none", see above), the second token represents the line width (as key of
 *  `LineWidthPreset`). If the passed mixed border could not be resolved to a
 *  border style, `null` will be returned instead.
 */
export function getPresetStyleForBorder(mixedBorder: MixedBorder): string | null {

    // special identifier 'none' regardless of line width (even not existing)
    if (mixedBorder.style === "none") {
        return "none";
    }

    // ambiguous line style or width: return null
    if (!is.string(mixedBorder.style) || !is.number(mixedBorder.width) || (mixedBorder.width === 0)) {
        return null;
    }

    // concatenate the line style and the line width into a single identifier
    return `${mixedBorder.style}:${getPresetForWidth(mixedBorder.width)}`;
}

/**
 * Returns an incomplete border attribute value for the passed predefined
 * border style.
 *
 * @param presetStyle
 *  The identifier of a predefined border style, as returned by the function
 * `getPresetStyleForBorder()`.
 *
 * @returns
 *  An incomplete border attribute value, containing the properties `style`
 *  (line style), and `width` (line width in 1/100 of millimeters). The
 *  property `color` will not be inserted into the border value.
 */
export function getBorderForPresetStyle(presetStyle: string): OpBorder {

    // the tokens in the passed preset style
    const tokens = presetStyle.split(":");
    // resulting border value
    const border: OpBorder = { style: "none", width: 0 };

    if ((tokens.length === 2) && (tokens[0].length > 0) && (tokens[1].length > 0)) {
        border.style = tokens[0];
        border.width = getWidthForPreset(tokens[1] as LineWidthPreset);
    } else if (presetStyle !== "none") {
        modelLogger.warn(`getBorderForPresetStyle: invalid preset style "${presetStyle}"`);
    }

    return border;
}

/**
 * Returns the effective horizontal padding between cell grid lines and the
 * text contents of the cell, according to the width of the digits of a
 * specific font.
 *
 * @param digitWidth
 *  The width of the digits of a specific font, in pixels.
 *
 * @returns
 *  The effective horizontal cell content padding, in pixels.
 */
export function getTextPadding(digitWidth: number): number {
    // text padding is 1/4 of the digit width
    return ceil(digitWidth / 4);
}

/**
 * Returns the total size of all horizontal padding occupied in a cell that
 * cannot be used for the cell contents, according to the width of the digits
 * of a specific font. This value includes the text padding (twice, for left
 * and right border), and additional space needed for the trailing grid line.
 *
 * @param digitWidth
 *  The width of the digits of a specific font, in pixels.
 *
 * @returns
 *  The total size of the horizontal cell content padding, in pixels.
 */
export function getTotalCellPadding(digitWidth: number): number {
    // padding at left and right cell border, and 1 pixel for the grid line
    return 2 * getTextPadding(digitWidth) + 1;
}

/**
 * Returns whether the passed direction value is orientated vertically.
 *
 * @param direction
 *  The direction to be checked.
 *
 * @returns
 *  Whether the direction value is orientated vertically (`true` for the
 *  directions `UP` and `DOWN`, `false` for the directions `LEFT` and `RIGHT`).
 */
export function isVerticalDir(direction: Direction): boolean {
    return (direction === Direction.UP) || (direction === Direction.DOWN);
}

/**
 * Returns whether the passed direction value is orientated to the leading
 * border of a bounding box.
 *
 * @param direction
 *  The direction to be checked.
 *
 * @returns
 *  Whether the direction value is orientated to the leading border (`true` for
 *  the directions `LEFT` and `UP`, `false` for the directions `RIGHT` and
 *  `DOWN`).
 */
export function isLeadingDir(direction: Direction): boolean {
    return (direction === Direction.LEFT) || (direction === Direction.UP);
}

/**
 * Returns the reverse direction of the passed direction.
 *
 * @param direction
 *  The direction to be reversed.
 *
 * @returns
 *  The reverse direction.
 */
export function getReverseDirection(direction: Direction): Direction {
    return REVERSE_DIRECTIONS[direction];
}

/**
 * Returns the address of a cell range adjacent to the passed cell range
 * address.
 *
 * @param range
 *  The original range address.
 *
 * @param direction
 *  The direction how to move to the adjacent range.
 *
 * @param size
 *  The size of the adjacent range.
 *
 * @returns
 *  The address of the adjacent range.
 */
export function getAdjacentRange(range: Range, direction: Direction, size: number): Range {

    // whether to modify column or row indexes
    const columns = !isVerticalDir(direction);
    // the resulting range
    const targetRange = range.clone();

    if (isLeadingDir(direction)) {
        targetRange.a1.move(-size, columns);
        targetRange.a2.set(range.a1.get(columns) - 1, columns);
    } else {
        targetRange.a1.set(range.a2.get(columns) + 1, columns);
        targetRange.a2.move(size, columns);
    }

    return targetRange;
}

/**
 * Returns whether the specified address is an origin cell in the merged range
 * according to the passed merge mode.
 *
 * @param address
 *  The cell address to be checked.
 *
 * @param mergedRange
 *  The address of the cell range to be merged.
 *
 * @param mergeMode
 *  The merge mode.
 *
 * @returns
 *  Whether the specified address is an origin cell in the merged range.
 */
export function isOriginCell(address: Address, mergedRange: Range, mergeMode: MergeMode): boolean {
    switch (mergeMode) {
        case MergeMode.MERGE:      return mergedRange.startsAt(address);
        case MergeMode.HORIZONTAL: return mergedRange.a1.c === address.c;
        case MergeMode.VERTICAL:   return mergedRange.a1.r === address.r;
        case MergeMode.UNMERGE:    return false;
    }
}

/**
 * Splits the passed cell display text into separate text lines, and cleans the
 * text lines from non-printable characters.
 *
 * @param text
 *  The cell display text to be split into separate text lines.
 *
 * @returns
 *  The separate text lines.
 */
export function splitDisplayString(text: string): string[] {
    // remove TAB characters (DOCS-4748); replace ASCII control characters with placeholders
    return text.split(/\n/).map(paragraph => str.cleanNPC(paragraph.replace(/\t+/g, ""), str.RECTANGLE_CHAR));
}

/**
 * Cleans the passed text from non-printable characters. Removes all newlines,
 * and replaces other non-printable characters with a placeholder character.
 *
 * @param text
 *  The cell display text to be cleaned for rendering.
 *
 * @returns
 *  The clean display text.
 */
export function cleanDisplayString(text: string): string {
    // remove line breaks from text (but leading/trailing whitespace remains significant, e.g. for the
    // optimal column width); remove TAB characters (DOCS-4748); replace ASCII control characters with placeholders
    return str.cleanNPC(text.replace(/[\n\t]+/g, ""), str.RECTANGLE_CHAR);
}

/**
 * Checks whether the passed text starts with a specific character used for
 * formula expressions (an equals sign, a plus, or a minus), and splits the
 * string into the prefix and the actual formula expression.
 *
 * - The equals sign will be moved to the `prefix` property.
 * - Everything else will be moved to the `expr` property (also a leading plus
 *   or minus sign).
 *
 * @param editText
 *  The edit text to be plit.
 *
 * @returns
 *  The prefix and formula expression of the passed text; or `undefined`, if
 *  the text cannot be evaluated as formula.
 */
export function splitFormulaParts(editText: string): Opt<EditFormulaParts> {
    if (/^[-+=]/.test(editText)) {
        const expr = editText.replace(/^=/, "");
        const prefix = editText.slice(0, editText.length - expr.length);
        return { prefix, expr };
    }
    return undefined;
}
