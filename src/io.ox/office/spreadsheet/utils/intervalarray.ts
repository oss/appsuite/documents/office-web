/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ArrayCallbackFn, type ArrayPredicateFn, math, ary } from "@/io.ox/office/tk/algorithms";

import { type IntervalIteratorOptions, Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import type { ArraySource, SortComparatorFn } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { yieldArraySource, someInArraySource, everyInArraySource, ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";

const { min, max } = Math;

// types ======================================================================

/**
 * Data type for values providing index intervals to be consumed by methods of
 * the class `IntervalArray`. Can be an array or an iterator with index
 * intervals, a single index interval, or nullish.
 */
export type IntervalSource = ArraySource<Interval>;

/**
 * An interval extended with additional properties, as returned from the method
 * `IntervalArray#partition`.
 */
export interface PartitionInterval extends Interval {

    /**
     * An interval array containing the original intervals of an interval array
     * covering this particular result interval of the partitioned result.
     */
    coveredBy: IntervalArray;
}

// class IntervalArray ========================================================

/**
 * Represents a list of column/row index intervals.
 */
export class IntervalArray extends ArrayBase<Interval> {

    // static methods ---------------------------------------------------------

    /**
     * If the passed data source is an `IntervalArray`, it will be returned
     * directly (no shallow clone). Otherwise, a new array instance will be
     * created and filled with the elements from the data source.
     *
     * @param source
     *  The data source to be converted to an instance of `IntervalArray`.
     *
     * @returns
     *  An instance of this class.
     */
    static cast(source: IntervalSource): IntervalArray {
        return (source instanceof IntervalArray) ? source : new IntervalArray(source);
    }

    /**
     * Creates an `IntervalArray` from the passed data source with the index
     * intervals passing a truth test.
     *
     * @param source
     *  The data source to be converted to a `IntervalArray`.
     *
     * @param fn
     *  The predicate callback function invoked on the elements of the data
     *  source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The index intervals passing the truth test.
     */
    static filter(source: IntervalSource, fn: ArrayPredicateFn<Interval>, context?: object): IntervalArray {
        return new IntervalArray().appendFiltered(source, fn, context);
    }

    /**
     * Creates an `IntervalArray` from the return values of a callback function
     * that will be invoked on the elements of the passed data source.
     *
     * @param source
     *  The data source to be converted to an `IntervalArray`.
     *
     * @param fn
     *  The callback function invoked on the elements of the data source.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The index intervals returned from the callback function.
     */
    static map<U>(source: ArraySource<U>, fn: ArrayCallbackFn<IntervalSource, U>, context?: object): IntervalArray {
        return new IntervalArray().appendMapped(source, fn, context);
    }

    /**
     * Merges the index intervals in the passed data source so that no index
     * interval in the result overlaps with or adjoins to another index
     * interval. The resulting index intervals will be sorted in ascending
     * order.
     *
     * @param intervals
     *  The index intervals to be merged. This function also accepts a single
     *  index interval as parameter.
     *
     * @returns
     *  A sorted array of merged index intervals exactly covering the index
     *  intervals in this array.
     */
    static merge(intervals: IntervalSource): IntervalArray {
        return IntervalArray.cast(intervals).merge();
    }

    /**
     * Merges the index intervals in the passed data source so that no index
     * interval in the result overlaps with or adjoins exactly to another
     * interval. The resulting index intervals will be sorted in ascending
     * order.
     *
     * @param source
     *  An unordered data source of cell index intervals to be merged.
     *
     * @param [fn]
     *  A mapper function that converts the values of the data source to index
     *  intervals.
     *
     * @returns
     *  The merged index intervals that do not overlap, and that exactly cover
     *  the index intervals in the passed data source.
     */
    static mergeIntervals(source: Iterable<Interval>): IntervalArray;
    static mergeIntervals<VT>(source: Iterable<VT>, fn: (value: VT) => Interval): IntervalArray;
    // implementation
    static mergeIntervals(source: Iterable<unknown>, fn?: (value: unknown) => Interval): IntervalArray {
        return ((fn ? IntervalArray.from(source, fn) : IntervalArray.from(source)) as IntervalArray).merge();
    }

    /**
     * Creates an index interval array from the passed array of single column
     * or row indexes. The index intervals in the resulting array will be
     * merged, i.e. no index interval overlaps with or adjoins to another index
     * interval. The index intervals will be sorted in ascending order.
     *
     * @param source
     *  An unordered data source of column/row indexes to be merged to an array
     *  of index intervals.
     *
     * @param [fn]
     *  A mapper function that converts the values of the data source to
     *  indexes.
     *
     * @returns
     *  A sorted array of unified index intervals exactly covering the passed
     *  column/row indexes.
     */
    static mergeIndexes(source: Iterable<number>): IntervalArray;
    static mergeIndexes<VT>(source: Iterable<VT>, fn: (value: VT) => number): IntervalArray;
    // implementation
    static mergeIndexes(source: Iterable<unknown>, fn?: (value: unknown) => number): IntervalArray {

        // resolve to array, needed for sorting
        const indexes = fn ? Array.from(source, fn) : Array.from(source as Iterable<number>);

        // simplifications for small arrays
        switch (indexes.length) {
            case 0: return new IntervalArray();
            case 1: return new IntervalArray(new Interval(indexes[0]));
        }

        // sort all indexes (can be done inplace, original array has been cloned above)
        indexes.sort(math.compare);

        // the resulting intervals
        const intervals = new IntervalArray();

        // create the resulting intervals (expand last interval, or create a new interval)
        let trailing: Opt<Interval>;
        for (const index of indexes) {
            if (trailing && (index <= trailing.last + 1)) {
                trailing.last = index;
            } else {
                intervals.push(trailing = new Interval(index));
            }
        }

        return intervals;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the number of column/row indexes covered by all index intervals
     * in this array.
     *
     * @returns
     *  The number of column/row indexes covered by all index intervals.
     */
    size(): number {
        return this.reduce((count, interval) => count + interval.size(), 0);
    }

    /**
     * Returns whether an index interval in this array contains the passed
     * column or row index.
     *
     * @param index
     *  The column or row index to be checked.
     *
     * @returns
     *  Whether an index interval in this array contains the passed index.
     */
    containsIndex(index: number): boolean {
        return this.some(interval => interval.containsIndex(index));
    }

    /**
     * Returns whether all index intervals in the passed array are contained
     * completely in any of the index intervals in this array.
     *
     * @param intervals
     *  The other index intervals to be checked. This method also accepts a
     *  single index interval as parameter.
     *
     * @returns
     *  Whether all index intervals in the passed array are contained
     *  completely in any of the index intervals in this array.
     */
    contains(intervals: IntervalSource): boolean {
        return everyInArraySource(intervals, interval2 => this.some(interval1 => interval1.contains(interval2)));
    }

    /**
     * Returns whether any index interval in this array overlaps with any index
     * interval in the passed array.
     *
     * @param intervals
     *  The other index intervals to be checked. This method also accepts a
     *  single index interval as parameter.
     *
     * @returns
     *  Whether any index interval in this array overlaps with any index
     *  interval in the passed array.
     */
    overlaps(intervals: IntervalSource): boolean {
        return this.some(interval1 => someInArraySource(intervals, interval2 => interval1.overlaps(interval2)));
    }

    /**
     * Returns whether all index intervals in this list are distinct to each
     * other (none of the index intervals overlaps with any of the others).
     *
     * @returns
     *  Whether all index intervals are distinct.
     */
    distinct(): boolean {

        // simplifications for small arrays
        switch (this.length) {
            case 0:
            case 1:
                return true;
            case 2:
                return !this[0].overlaps(this[1]);
            case 3:
                return !this[0].overlaps(this[1]) && !this[0].overlaps(this[2]) && !this[1].overlaps(this[2]);
        }

        // work on a sorted clone for performance (check adjacent array elements)
        return this.clone().sort().every((interval, index, clone) => (index === 0) || !clone[index - 1].overlaps(interval));
    }

    /**
     * Returns the bounding interval of the index intervals in this array (the
     * smallest index interval that contains all intervals in the array).
     *
     * @returns
     *  The bounding interval containing all index intervals; or `undefined`,
     *  if this interval array is empty.
     */
    boundary(): Opt<Interval> {

        const l = this.length;
        if (l === 0) { return undefined; }

        // this method must not return original array elements
        const boundary = this[0].clone();
        for (let i = 1; i < l; i += 1) {
            const interval = this[i];
            boundary.first = min(boundary.first, interval.first);
            boundary.last = max(boundary.last, interval.last);
        }
        return boundary;
    }

    /**
     * Merges the index intervals in this array so that no index interval in
     * the result overlaps with or adjoins to another index interval. The
     * resulting index intervals will be sorted in ascending order.
     *
     * @returns
     *  A sorted array of merged index intervals exactly covering the index
     *  intervals in this array.
     */
    merge(): this {

        // make deep copy, and sort the intervals by start index
        const resultIntervals = this.deepClone().sort();

        // merge overlapping intervals in-place
        resultIntervals.forEach((interval1, index) => {
            // extend current interval, as long as adjacent or overlapping with next interval
            let interval2 = resultIntervals[index + 1];
            while (interval2 && (interval1.last + 1 >= interval2.first)) {
                interval1.last = max(interval1.last, interval2.last);
                resultIntervals.deleteAt(index + 1);
                interval2 = resultIntervals[index + 1];
            }
        });

        return resultIntervals;
    }

    /**
     * Works similar to merging the index interval array (see method `merge()`)
     * but divides the intervals that overlap each other into smaller adjoining
     * parts, so that the resulting intervals do not cover the boundaries of
     * any of the original intervals.
     *
     * Example: The interval array `[A:D,C:F]` will be partitioned to the
     * intervals `A:B` (leading part of first interval), `C:D` (common parts of
     * both intervals), and `E:F` (trailing part of second interval).
     *
     * @returns
     *  A sorted array of index intervals that do not overlap, that exactly
     *  cover the intervals in this array, and that are partitioned in a way
     *  that the resulting intervals do not cover any boundary of an original
     *  interval. Each interval object in the resulting array will contain an
     *  additional property `coveredBy` that is an `IntervalArray` containing
     *  the original intervals of this instance covering that particular result
     *  interval.
     */
    partition(): PartitionInterval[] {

        // sort the intervals in this array by start index
        const sourceIntervals = this.clone().sort();
        // the source intervals to be processed for the next partial interval (updated during iteration)
        const coveringIntervals = new IntervalArray();
        // the resulting partition
        const resultIntervals: PartitionInterval[] = [];

        // creates a new partial interval and inserts it into the result array
        // (this function must be defined outside the while-loop)
        function createPartialInterval(): void {

            // the start index of the new partial interval to be created
            let first = sourceIntervals.empty() ? Infinity : sourceIntervals[0].first;
            if (!coveringIntervals.empty()) { first = ary.at(resultIntervals, -1)!.last + 1; }

            // remove all original intervals already processed in the preceding iteration that cannot be
            // part of the partition anymore, and find the minimum end index of all remaining source intervals
            let last = Infinity;
            coveringIntervals.forEachReverse((leadingInterval, index) => {
                if (first <= leadingInterval.last) {
                    last = min(last, leadingInterval.last);
                } else {
                    coveringIntervals.deleteAt(index);
                }
            });

            // add all source intervals with the current start position to the intermediate array
            while (!sourceIntervals.empty() && (first === sourceIntervals.first()!.first)) {
                const sourceInterval = sourceIntervals.shift()!;
                last = min(last, sourceInterval.last);
                coveringIntervals.push(sourceInterval);
            }

            // restrict the new partial interval to the start of the next source interval not processed here
            if (!sourceIntervals.empty()) {
                last = min(last, sourceIntervals.first()!.first - 1);
            }

            // add the resulting partial interval to the result
            if (!coveringIntervals.empty()) {
                const partInterval = new Interval(first, last) as PartitionInterval;
                partInterval.coveredBy = coveringIntervals.clone();
                resultIntervals.push(partInterval);
            }
        }

        // process the source intervals
        while (!sourceIntervals.empty() || !coveringIntervals.empty()) {
            createPartialInterval();
        }

        return resultIntervals;
    }

    /**
     * Returns the index intervals covered by this array, and the passed array
     * of index intervals. More precisely, returns the existing intersection
     * intervals from all interval pairs of the cross product of the interval
     * arrays.
     *
     * @param intervals
     *  The other index intervals to be intersected with this index intervals.
     *  This method also accepts a single index interval as parameter.
     *
     * @returns
     *  The index intervals covered by this array, and the passed array of
     *  index intervals.
     */
    intersect(intervals: IntervalSource): IntervalArray {
        const resultIntervals = new IntervalArray();
        for (const interval1 of this.values()) {
            for (const interval2 of yieldArraySource(intervals)) {
                const interval = interval1.intersect(interval2);
                if (interval) { resultIntervals.push(interval); }
            }
        }
        return resultIntervals;
    }

    /**
     * Returns the difference of the index intervals in this array, and the
     * passed index intervals (all index intervals contained in this array,
     * that are not contained in the passed array).
     *
     * @param intervals
     *  The index intervals to be removed from this array. This method also
     *  accepts a single index interval as parameter.
     *
     * @returns
     *  All index intervals that are contained in this array, but not in the
     *  passed array. May be an empty array, if the passed index intervals
     *  completely cover all index intervals in this array.
     */
    difference(intervals: IntervalSource): IntervalArray {

        // the resulting intervals
        let resultIntervals = this as IntervalArray;

        // reduce intervals in 'result' with each interval from the passed array
        for (const interval2 of yieldArraySource(intervals)) {

            // initialize the arrays for this iteration
            const source = resultIntervals;
            resultIntervals = new IntervalArray();

            // process each interval in the source interval list
            for (const interval1 of source) {

                // check if the intervals cover each other at all
                if (!interval1.overlaps(interval2)) {
                    resultIntervals.push(interval1.clone());
                    continue;
                }

                // do nothing if interval2 covers interval1 completely (delete interval1 from the result)
                if (interval2.contains(interval1)) {
                    continue;
                }

                // if interval1 starts before interval2, extract the leading part of interval1
                if (interval1.first < interval2.first) {
                    resultIntervals.push(new Interval(interval1.first, interval2.first - 1));
                }

                // if interval1 ends after interval2, extract the trailing part of interval1
                if (interval1.last > interval2.last) {
                    resultIntervals.push(new Interval(interval2.last + 1, interval1.last));
                }
            }

            // early exit the loop, if all source intervals have been deleted already
            if (!resultIntervals.length) { break; }
        }

        // ensure to return a clone
        return (resultIntervals === this) ? this.deepClone() : resultIntervals;
    }

    /**
     * Modifies the indexes of all index intervals in this array relatively.
     *
     * @param distance
     *  The distance to move the indexes of all index intervals in this array.
     *  Negative values will decrease the indexes.
     *
     * @returns
     *  A reference to this instance.
     */
    move(distance: number): this {
        this.forEach(interval => interval.move(distance));
        return this;
    }

    /**
     * Creates an iterator that visits the single indexes of the intervals in
     * this array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The single indexes of the index intervals.
     */
    *indexes(options?: IntervalIteratorOptions): IterableIterator<number> {
        for (const interval of this.values(options)) {
            yield* interval.indexes(options);
        }
    }

    /**
     * Sorts the index intervals in this list in-place.
     *
     * @param [compare=Interval.compare]
     *  The comparator callback function that receives two index intervals from
     *  this list, and returns their relation as signed number.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this array.
     */
    override sort(compare?: SortComparatorFn<Interval>, context?: object): this {
        return super.sort(compare ?? Interval.compare, context);
    }

    /**
     * Returns the A1 notation of this column interval list as used in document
     * operations (e.g. `"A:C E"`).
     *
     * @returns
     *  The A1 notation of this column interval list as used in document
     *  operations.
     */
    toOpStrCols(): string {
        return this.map(interval => interval.toOpStrCols()).join(" ");
    }

    /**
     * Returns the A1 notation of this row interval list as used in document
     * operations (e.g. `"1:3 5"`).
     *
     * @returns
     *  The A1 notation of this row interval list as used in document
     *  operations.
     */
    toOpStrRows(): string {
        return this.map(interval => interval.toOpStrRows()).join(" ");
    }

    /**
     * Returns the A1 notation of this interval list as used in document
     * operations (e.g. `"A:C E"` or `"1:3 5"`).
     *
     * @param columns
     *  Whether to return the column representation (`true`), or the row
     *  representation (`false`) of the index intervals.
     *
     * @returns
     *  The A1 notation of this interval list as used in document operations.
     */
    toOpStr(columns: boolean): string {
        return this.map(interval => interval.toOpStr(columns)).join(" ");
    }
}
