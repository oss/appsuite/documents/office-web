/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import type { ScalarType } from "@/io.ox/office/spreadsheet/utils/scalar";

const { min, max } = Math;

// class SubtotalResult =======================================================

/**
 * The collected subtotal results of a sheet selection.
 */
export class SubtotalResult {

    // properties -------------------------------------------------------------

    /**
     * The total count of non-blank cells (numbers, strings, booleans, and
     * error codes).
     */
    cells = 0;

    /**
     * The total count of number cells.
     */
    numbers = 0;

    /**
     * The sum of all numbers.
     */
    sum = 0;

    /**
     * The minimum of all numbers (`Infinity` if no numbers are available).
     */
    min = Infinity;

    /**
     * The maximum of all numbers (`-Infinity` if no numbers are available).
     */
    max = -Infinity;

    /**
     * The arithmetic mean of all numbers (`NaN` if no numbers are available).
     */
    average = Number.NaN;

    // public methods ---------------------------------------------------------

    /**
     * Adds a new scalar value to this subtotal result.
     *
     * @param value
     *  The value to be added to this subtotal result. All non-null values will
     *  be counted in the property `cells`. All numbers will be counted and
     *  aggregated in the remaining properties `numbers`, `sum`, `min`, `max`,
     *  and `average`.
     */
    addValue(value: ScalarType): void {

        if (value !== null) {
            this.cells += 1;
        }

        if (is.number(value)) {
            this.numbers += 1;
            this.sum += value;
            this.min = min(this.min, value);
            this.max = max(this.max, value);
            this.average = this.sum / this.numbers;
        }
    }

    /**
     * Adds the settings of the passed subtotal result object.
     *
     * @param result
     *  The subtotals to be added to this result.
     */
    addResult(result: SubtotalResult): void {
        this.cells += result.cells;
        this.numbers += result.numbers;
        this.sum += result.sum;
        this.min = min(this.min, result.min);
        this.max = max(this.max, result.max);
        this.average = this.sum / this.numbers;
    }

    /**
     * Adds the settings of the passed scalar value, or subtotal result object.
     *
     * @param result
     *  A scalar value, or another instance of this class, to be added to this
     *  result.
     */
    add(result: ScalarType | SubtotalResult): void {
        if (result instanceof SubtotalResult) {
            this.addResult(result);
        } else {
            this.addValue(result);
        }
    }
}
