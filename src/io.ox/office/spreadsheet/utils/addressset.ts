/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ReverseOptions, ary } from "@/io.ox/office/tk/algorithms";
import { KeySet } from "@/io.ox/office/tk/containers";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";

// types ======================================================================

/**
 * Optional parameters for an ordered address iterator returned by the method
 * `AddressSet#ordered`.
 */
export interface AddressSetIteratorOptions extends ReverseOptions {

    /**
     * The address of the cell range to restrict the iterator to.
     */
    range?: Range;

    /**
     * If set to `false`, the addresses will be visited from first to last row,
     * and in each row from left to right.
     *
     * If set to `true`, the addresses will be visited from first to last
     * column, and in each column from top to bottom.
     *
     * Default value is `false`.
     */
    vertical?: boolean;
}

// class AddressSet ===========================================================

/**
 * A unique set of cell addresses. This class provides optimized methods for
 * insertion, deletion, and access of cell addresses, while keeping the set
 * unique (two different addresses in this set will never have the same column
 * and row index).
 */
export class AddressSet<AddrT extends Address = Address> extends KeySet<AddrT> {

    // properties -------------------------------------------------------------

    // the addresses in this set sorted horizontally (by rows)
    readonly #rows: AddrT[] = [];
    // the addresses in this set sorted vertically (by columns)
    readonly #cols: AddrT[] = [];

    // constructor ------------------------------------------------------------

    constructor(source?: Iterable<AddrT>) {
        super();
        if (source) {
            for (const address of source) {
                this.add(address);
            }
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds an address to this set.
     *
     * @param address
     *  The address to be inserted into this set.
     *
     * @returns
     *  A reference to this instance.
     */
    override add(address: AddrT): this {
        ary.insertSorted(this.#rows, address, Address.compare, { unique: true });
        ary.insertSorted(this.#cols, address, Address.compareVert, { unique: true });
        return super.add(address);
    }

    /**
     * Removes an address from this set.
     *
     * @param address
     *  The address to be removed from this set.
     *
     * @returns
     *  Whether this set has contained the passed address, or an equal address.
     */
    override delete(address: Address): boolean {
        // manually lower the array types (TS infers `AddrT` as value type for `ary.deleteSorted`)
        ary.deleteSorted(this.#rows as Address[], address, Address.compare);
        ary.deleteSorted(this.#cols as Address[], address, Address.compareVert);
        return super.delete(address);
    }

    /**
     * Removes all cell addresses from this set.
     */
    override clear(): void {
        this.#rows.length = this.#cols.length = 0;
        super.clear();
    }

    /**
     * Returns the smallest address in this set, using horizontal or vertical
     * sorting order.
     *
     * @param [vertical=false]
     *  If set to `true`, uses vertical sorting order.
     *
     * @returns
     *  The smallest address in this set, using horizontal or vertical sorting
     *  order; or `undefined`, if this set is empty.
     */
    first(vertical?: boolean): Opt<AddrT> {
        return vertical ? this.#cols[0] : this.#rows[0];
    }

    /**
     * Returns the address of the bounding range (the smallest cell range that
     * contains all addresses) of this address set.
     *
     * @returns
     *  The address of the bounding range of this address set; or `undefined`,
     *  if this set is empty.
     */
    boundary(): Opt<Range> {
        return this.size ? Range.fromIndexes(this.#cols[0].c, this.#rows[0].r, ary.at(this.#cols, -1)!.c, ary.at(this.#rows, -1)!.r) : undefined;
    }

    /**
     * Creates an iterator that visits all existing cell addresses in this set.
     * The addresses will be visited in order according to `Address#compare`
     * (row by row). Optionally, the iterator can be reversed, addresses can be
     * visited column-by-column, and the iterator can be restricted to a
     * subrange.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The cell addresses.
     */
    *ordered(options?: AddressSetIteratorOptions): IterableIterator<AddrT> {

        // nothing to do, if the target range does not cover the set
        const targetRange = options?.range;
        const boundRange = this.boundary();
        if (!boundRange || (targetRange && !boundRange.overlaps(targetRange))) {
            return;
        }

        // the address vector to be used for iteration
        const vector = options?.vertical ? this.#cols : this.#rows;
        const compare = options?.vertical ? Address.compareVert : Address.compare;
        // whether to iterate backwards
        const reverse = options?.reverse;

        // no range filter, if the target range covers the entire set
        if (!targetRange || targetRange.contains(boundRange)) {
            yield* ary.values(vector, { reverse });
            return;
        }

        // visit the addresses between start and end address of the passed range
        const lower = ary.fastFindFirstIndex(vector, address => compare(targetRange.a1, address) <= 0);
        const upper = ary.fastFindLastIndex(vector, address => compare(address, targetRange.a2) <= 0);
        for (const address of ary.interval(vector, lower, upper, reverse)) {
            // filter addresses that are inside the array interval, but outside the range
            // TODO: a more advanced implementation would try to skip the gaps
            // between the covered row/column intervals with binary search too
            if (targetRange.containsAddress(address)) {
                yield address;
            }
        }
    }
}
