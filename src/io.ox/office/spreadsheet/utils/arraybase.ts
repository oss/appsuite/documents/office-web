/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ArrayLoopOptions, ArrayCallbackFn, ArrayPredicateFn } from "@/io.ox/office/tk/algorithms";
import { is, math, ary } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * A generic data source for a sequence of values. Can be a read-only array of
 * values, or a single value, or nullish (which will be ignored).
 */
export type ArraySource<T> = readonly T[] | Nullable<T>;

/**
 * Base type for index intervals, cell addresses, and cell range addresses.
 * Defines a common API of all classes used in their array container classes.
 */
export interface TArrayElementType extends Keyable, Equality<TArrayElementType>, CloneableThis { }

/**
 * A callback function that will be invoked for each element of an array.
 *
 * @template RT
 *  The return type of the callback function.
 *
 * @template AT
 *  The type of the entire array.
 *
 * @param value
 *  The value of the current element.
 *
 * @param index
 *  The index of the current element.
 *
 * @param array
 *  A reference to the entire array.
 */
export type TArrayCallbackFn<RT, VT extends TArrayElementType, AT extends ArrayBase<VT>> = (value: VT, index: number, array: AT) => RT;

/**
 * A predicate callback function that will be invoked for each element of an
 * array.
 *
 * @template AT
 *  The type of the entire array.
 *
 * @param value
 *  The value of the array element to be tested.
 *
 * @param index
 *  The array index of the element.
 *
 * @param array
 *  A reference to the entire array.
 *
 * @returns
 *  Whether the array element passes the truth test.
 */
export type TArrayPredicateFn<VT extends TArrayElementType, AT extends ArrayBase<VT>> = TArrayCallbackFn<boolean, VT, AT>;

/**
 * A comparator callback function for sorting arrays.
 *
 * @param value1
 *  The first array element to be compared.
 *
 * @param value2
 *  The second array elements to be compared.
 *
 * @returns
 *  A negative number, if `value1` is less than `value2`; or a positive number,
 *  if `value1` is greater than `value2`; otherwise zero.
 */
export type SortComparatorFn<VT> = (value1: VT, value2: VT) => number;

/**
 * An indexer callback function for sorting arrays.
 *
 * @param value
 *  The array element to be indexed.
 *
 * @returns
 *  A number specifying the sort index of the array element relative to the
 *  other array elements.
 */
export type SortIndexerFn<T> = (value: T) => unknown;

// public functions ===========================================================

/**
 * Returns an iterator that visits all elements in the passed array source.
 *
 * @param source
 *  The array value source. Can be an array, nullish, or a single value (of
 *  type `VT`).
 *
 * @yields
 *  All values of the passed array source.
 */
export function *yieldArraySource<VT>(source: ArraySource<VT>): IterableIterator<VT> {
    if (is.readonlyArray(source)) {
        yield* source;
    } else if (!is.nullish(source)) {
        yield source;
    }
}

/**
 * Visits all elements in the passed array source.
 *
 * @param source
 *  The array value source. Can be an array, nullish, or a single value (of
 *  type `VT`). In the latter case, the callback function will be invoked once
 *  for that value (as if it was a one-element array) but without creating a
 *  temporary array internally.
 *
 * @param fn
 *  The callback function invoked for each value in the array source.
 *
 * @param [context]
 *  The calling context for the callback function.
 */
export function consumeArraySource<VT>(source: ArraySource<VT>, fn: ArrayCallbackFn<void, VT>, context?: object): void {
    if (is.readonlyArray(source)) {
        source.forEach((v, i) => fn.call(context, v, i));
    } else if (!is.nullish(source)) {
        fn.call(context, source, 0);
    }
}

/**
 * Returns whether at least one element in the passed array source passes a
 * truth test. Works similar to the method `Array#some`.
 *
 * @param source
 *  The array value source. Can be an array, nullish, or a single value (of
 *  type `T`). In the latter case, the callback function will be invoked once
 *  for that value (as if it was a one-element array).
 *
 * @param fn
 *  The predicate callback function invoked for each value in the array source.
 *  Returns whether the element passes the test.
 *
 * @param [context]
 *  The calling context for the callback function.
 *
 * @returns
 *  Whether at least one value in the passed data source passes the truth test.
 */
export function someInArraySource<VT>(source: ArraySource<VT>, fn: ArrayPredicateFn<VT>, context?: object): boolean {
    return is.readonlyArray(source) ? source.some((v, i) => fn.call(context, v, i)) : (!is.nullish(source) && fn.call(context, source, 0));
}

/**
 * Returns whether all elements in the passed array source pass a truth test.
 * Works similar to the method `Array#every`.
 *
 * @param source
 *  The array value source. Can be an array, nullish, or a single value (of
 *  type `T`). In the latter case, the callback function will be invoked once
 *  for that value (as if it was a one-element array).
 *
 * @param fn
 *  The predicate callback function invoked for each value in the array source.
 *  Returns whether the element passes the test.
 *
 * @param [context]
 *  The calling context for the callback function.
 *
 * @returns
 *  Whether all values in the passed data source pass the truth test.
 */
export function everyInArraySource<VT>(source: ArraySource<VT>, fn: ArrayPredicateFn<VT>, context?: object): boolean {
    return is.readonlyArray(source) ? source.every((v, i) => fn.call(context, v, i)) : (is.nullish(source) || fn.call(context, source, 0));
}

// class ArrayBase ============================================================

/**
 * An array of keyable and cloneable values.
 *
 * Intended to be used as internal helper base class for index interval arrays,
 * cell address arrays, and cell range arrays.
 */
export abstract class ArrayBase<T extends TArrayElementType> extends Array<T> implements Keyable, Equality<ArrayLike<T>> {

    // constructor ------------------------------------------------------------

    constructor(...args: Array<ArraySource<T>>) {
        super();
        // constructor will be called with single number from internal implementations
        // of array methods creating new subclass arrays, e.g. `Array#slice`, `Array#splice`
        if ((args.length !== 1) || !is.number(args[0])) { this.append(...args); }
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns a unique string key for this list that can be used for example
     * as key in an associative map. This method is faster than the method
     * `toString()`.
     *
     * @returns
     *  A unique string key for this instance.
     */
    get key(): string {
        return this.map(element => element.key).join(" ");
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the array is empty.
     *
     * @returns
     *  Whether the array is empty.
     */
    empty(): boolean {
        return this.length === 0;
    }

    /**
     * Returns the first element of this array.
     *
     * @returns
     *  The first element of this array; or `null`, if the array is empty.
     */
    first(): T | null {
        return this.empty() ? null : this[0];
    }

    /**
     * Returns the last element of this array.
     *
     * @returns
     *  The last element of this array; or `null`, if the array is empty.
     */
    last(): T | null {
        return this.empty() ? null : this[this.length - 1];
    }

    /**
     * Removes all elements from this array.
     *
     * @returns
     *  A reference to this array.
     */
    clear(): this {
        this.length = 0;
        return this;
    }

    /**
     * Returns a shallow or deep clone of this array.
     *
     * @param [cloner]
     *  A generator callback function that can be used to deeply clone the
     *  elements of this array. The return value of this function will be
     *  inserted as new array element into the clone. The function may return
     *  `undefined` or `null` to filter specific elements of this array.
     *
     * @param [context]
     *  The calling context for the generator callback function.
     *
     * @returns
     *  The clone of this array. If called from a subclass instance, creates a
     *  new instance of that subclass automatically.
     */
    clone(cloner?: TArrayCallbackFn<Nullable<T>, T, this>, context?: object): this {

        const other = this.#new();

        if (cloner) {
            this.forEach((value, index) => {
                const clone = cloner.call(context, value, index, this);
                if (!is.nullish(clone)) { other.push(clone); }
            });
        } else {
            other.append(this);
        }

        return other;
    }

    /**
     * Returns a deep clone of this list by calling the method `clone()` of all
     * contained elements.
     *
     * @returns
     *  A deep clone of this list.
     */
    deepClone(): this {
        return this.clone(element => element.clone());
    }

    /**
     * Returns whether this array is equal to the specified array. Compares the
     * array elements by reference (uses the strict equality operator `===` for
     * the array elements).
     *
     * @param other
     *  The array to be compared with this array.
     *
     * @returns
     *  Whether this array is equal to the specified array.
     */
    equals(other: ArrayLike<T>): boolean {
        return (this.length === other.length) && this.every((value, index) => value === other[index]);
    }

    /**
     * Returns whether the elements in this array are equal to the elements in
     * the specified array. Compares the array elements deeply. The elements in
     * the passed array MUST provide an `equals()` instance method taking an
     * argument of type `T` to compare against.
     *
     * @param other
     *  The array to be compared with this array.
     *
     * @returns
     *  Whether this array is deeply equal to the specified array.
     */
    deepEquals(other: ArrayLike<Equality<T>>): boolean {
        return (this.length === other.length) && this.every((value, index) => other[index].equals(value));
    }

    /**
     * Appends all passed parameters to this array in-place, similar to the
     * method `Array.push()`.
     *
     * @param args
     *  All values to be appended to this array. Either parameter can be an
     *  array, a single value, or nullish (which will be skipped).
     *
     * @returns
     *  A reference to this array.
     */
    append(...args: Array<ArraySource<T>>): this {
        for (const source of args) {
            consumeArraySource(source, value => this.push(value));
        }
        return this;
    }

    /**
     * Appends the values of the passed data source that pass a truth test.
     * Works similar to the method `Array.filter()`.
     *
     * @param source
     *  All values to be appended to this array conditionally. Either parameter
     *  can be an array, a single value, or nullish (which will be skipped).
     *
     * @param fn
     *  The predicate callback function to be invoked for each value in the
     *  array source. If it returns `true`, the value will be appended to this
     *  array.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this array.
     */
    appendFiltered(source: ArraySource<T>, fn: ArrayPredicateFn<T>, context?: object): this {
        consumeArraySource(source, (value, index) => fn.call(context, value, index) && this.push(value));
        return this;
    }

    /**
     * Appends the return values of a callback function invoked on the values
     * of the passed data source. Works similar to the method `Array.map()`.
     *
     * @param source
     *  All values to be processed. Either parameter can be an array, a single
     *  value, or nullish (which will be skipped).
     *
     * @param fn
     *  The callback function invoked for each value in the data source. The
     *  data source returned by the function will be appended to this array.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this array.
     */
    appendMapped<U>(source: ArraySource<U>, fn: ArrayCallbackFn<ArraySource<T>, U>, context?: object): this {
        consumeArraySource(source, (value, index) => this.append(fn.call(context, value, index)));
        return this;
    }

    /**
     * Appends all values of the passed iterable object.
     *
     * @param source
     *  The iterable object to be consumed.
     *
     * @returns
     *  A reference to this array.
     */
    appendIterable(source: Iterable<ArraySource<T>>): this {
        for (const value of source) { this.append(value); }
        return this;
    }

    /**
     * Assigns all passed parameters to this array in-place (clears all old
     * contents of this array before).
     *
     * @param args
     *  All values to be assigned to this array. Either parameter can be an
     *  array, a single value, or nullish (which will be skipped).
     *
     * @returns
     *  A reference to this array.
     */
    assign(...args: Array<ArraySource<T>>): this {
        this.clear();
        return this.append(...args);
    }

    /**
     * Creates a shallow clone of this array, and appends the passed parameters
     * to it. Overwrites the method `Array.concat()` to be able to return an
     * instance of this class.
     *
     * @param args
     *  All values to be appended to the clone of this array. Either parameter
     *  can be an array, a single value, or nullish (which will be skipped).
     *
     * @returns
     *  The new array containing the elements of this array, and the passed
     *  parameters. If called from a subclass instance, creates a new instance
     *  of that subclass automatically.
     */
    // type `ConcatArray<T>` needed to overwrite base method and prevent follow-up type errors
    override concat(...args: Array<ConcatArray<T> | ArraySource<T>>): this;
    override concat(...args: readonly T[]): this; // needed to remedy type errors in TS3.6+
    // implementation
    override concat(...args: Array<ArraySource<T>>): this {
        return this.clone().append(...args);
    }

    /**
     * Returns a subset of the elements of this array. Overwrites the method
     * `Array#slice` to be able to return an instance of this class.
     *
     * @param [begin]
     *  The array index of the first element to be returned. Behaviour is equal
     *  to the standard method `Array.slice()`.
     *
     * @param [end]
     *  The array index of the first element not to be returned anymore.
     *  Behaviour is equal to the standard method `Array.slice()`.
     *
     * @returns
     *  The new array containing the specified elements of this array. If
     *  called from a subclass instance, creates a new instance of that
     *  subclass automatically.
     */
    override slice(begin?: number, end?: number): this;
    // implementation
    override slice(...args: number[]): this {
        // do not pass `undefined` values explicitly to base method
        return this.#make(super.slice(...args));
    }

    /**
     * Overwrites the method `splice()` of the base class to be able to
     * return an instance of this class.
     *
     * @param index
     *  The array index of the first array element to be removed.
     *
     * @param [count]
     *  The number of array elements to be removed.
     *
     * @param values
     *  New elements to be inserted into this array.
     *
     * @returns
     *  The elements removed from this array. If called from a subclass
     *  instance, creates a new instance of that subclass automatically.
     */
    override splice(index: number, count?: number): this;
    override splice(index: number, count: number, ...values: T[]): this;
    // implementation
    override splice(index: number, count?: number, ...values: T[]): this {
        // do not pass `undefined` values explicitly to base method
        const removed = is.number(count) ? super.splice(index, count, ...values) : super.splice(index);
        return this.#make(removed);
    }

    /**
     * Inserts the specified element into this array _in-place_.
     *
     * @param index
     *  The new array index of the element to be inserted.
     *
     * @param value
     *  The value to be inserted into this array.
     *
     * @returns
     *  The passed value.
     */
    insertAt<U extends T>(index: number, value: U): U {
        return ary.insertAt(this, index, value);
    }

    /**
     * Removes the specified element from this array _in-place_.
     *
     * @param index
     *  The array index of the element to be removed.
     *
     * @returns
     *  The value of the element removed from this array.
     */
    deleteAt(index: number): Opt<T> {
        return ary.deleteAt(this, index);
    }

    /**
     * Creates an array iterator to visit the element entries of this array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The array iterator that will visit the entries of this array.
     */
    override entries(options?: ArrayLoopOptions): IndexedIterator<T> {
        return options ? ary.entries(this, options) : super.entries();
    }

    /**
     * Creates an array iterator to visit the element values of this array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The array iterator that will visit the element values of this array.
     */
    override values(options?: ArrayLoopOptions): IterableIterator<T> {
        return options ? ary.values(this, options) : super.values();
    }

    /**
     * Creates an array value iterator for the passed closed index interval.
     *
     * @param first
     *  The array index of the leading element in the array. All array elements
     *  with an index equal to or greater than this value will be visited by
     *  the returned iterator.
     *
     * @param last
     *  The array index of the trailing element in this array. All array
     *  elements with an index equal to or less than this value will be visited
     *  by the returned iterator (closed interval).
     *
     * @param [reverse=false]
     *  Whether to visit the array elements in reversed order. The element at
     *  the array index specified in `last` will be visited first.
     *
     * @returns
     *  The new array iterator.
     */
    interval(first: number, last: number, reverse?: boolean): IterableIterator<T> {
        return ary.interval(this, first, last, reverse);
    }

    /**
     * Visits all elements in this array in reversed order. Works similar to
     * the method `Array.forEach()`.
     *
     * @param callbackFn
     *  The callback function invoked for each array element.
     *
     * @param [context]
     *  The calling context for the callback function.
     */
    forEachReverse(callbackFn: TArrayCallbackFn<void, T, this>, context?: object): void {
        for (let idx = this.length - 1; idx >= 0; idx -= 1) {
            callbackFn.call(context, this[idx], idx, this);
        }
    }

    /**
     * Returns whether at least one element in this array passes a truth test.
     * Works similar to the method `Array.some()`, but visits the array
     * elements in reversed order.
     *
     * @param predicateFn
     *  The predicate callback function invoked for each array element. Returns
     *  a truthy value if the element passes the test.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  Whether at least one element in this array passes the truth test.
     */
    someReverse(predicateFn: TArrayPredicateFn<T, this>, context?: object): boolean {
        for (let idx = this.length - 1; idx >= 0; idx -= 1) {
            if (predicateFn.call(context, this[idx], idx, this)) { return true; }
        }
        return false;
    }

    /**
     * Returns whether all elements in this array pass a truth test. Works
     * similar to the method `Array.every()`, but visits the array elements in
     * reversed order.
     *
     * @param predicateFn
     *  The predicate callback function invoked for each array element. Returns
     *  a truthy value if the element passes the test.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  Whether all elements in this array pass the truth test.
     */
    everyReverse(predicateFn: TArrayPredicateFn<T, this>, context?: object): boolean {
        for (let idx = this.length - 1; idx >= 0; idx -= 1) {
            if (!predicateFn.call(context, this[idx], idx, this)) { return false; }
        }
        return true;
    }

    /**
     * Returns a new array with only the elements of this array that pass a
     * truth test. Overwrites the method `Array.filter()` to be able to return
     * an instance of this class.
     *
     * @param predicateFn
     *  The predicate callback function invoked for each array element. Returns
     *  a truthy value if the element passes the test and has to be inserted
     *  into the resulting array.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The new array with all elements that pass the truth test. If called
     *  from a subclass instance, creates a new instance of that subclass
     *  automatically.
     */
    override filter(predicateFn: TArrayPredicateFn<T, this>, context?: object): this {
        return this.#new().appendFiltered(this, (value, index) => predicateFn.call(context, value, index, this));
    }

    /**
     * Returns a new array with only the elements of this array that do not
     * pass a truth test. Works similar to the method `TArray.filter()` but
     * reverses the predicate.
     *
     * @param predicateFn
     *  The predicate callback function invoked for each array element. Returns
     *  a truthy value if the element does not pass the test and will not be
     *  inserted into the resulting array.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The new array with all elements that do not pass the truth test. If
     *  called from a subclass instance, creates a new instance of that
     *  subclass automatically.
     */
    reject(predicateFn: TArrayPredicateFn<T, this>, context?: object): this {
        return this.#new().appendFiltered(this, (value, index) => !predicateFn.call(context, value, index, this));
    }

    /**
     * Returns a new array with the results of a callback function invoked for
     * every element in this array. Overwrites the method `Array.map()` to be
     * able to return an instance of this class.
     *
     * @param generatorFn
     *  The callback function invoked for each array element. The return value
     *  (which can be any array source, i.e. an array, or a single value) will
     *  be inserted into the resulting array.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The new array with all return values of the callback function.
     */
    override map<U>(generatorFn: TArrayCallbackFn<ArraySource<U>, T, this>, context?: object): U[] {
        const result: U[] = [];
        this.forEach((value, index) => {
            const mapped = generatorFn.call(context, value, index, this);
            consumeArraySource(mapped, v => result.push(v));
        });
        return result;
    }

    /**
     * Returns the index of the last element in this array that passes a truth
     * test.
     *
     * @param predicateFn
     *  The predicate callback function invoked for each array element. Returns
     *  a truthy value if the element passes the test and has to be returned.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The index of the last element in this array that passes the truth test;
     *  or `-1`, if no such element exists.
     */
    findLastIndex(predicateFn: TArrayPredicateFn<T, this>, context?: object): number {
        for (let index = this.length - 1; index >= 0; index -= 1) {
            if (predicateFn.call(context, this[index], index, this)) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Returns the last element of this array that passes a truth test.
     *
     * @param predicateFn
     *  The predicate callback function invoked for each array element. Returns
     *  a truthy value if the element passes the test and has to be returned.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  The last element from this array that passes the truth test; or
     *  `undefined`, if no such element exists.
     */
    findLast(predicateFn: TArrayPredicateFn<T, this>, context?: object): Opt<T> {
        const index = this.findLastIndex(predicateFn, context);
        return (index >= 0) ? this[index] : undefined;
    }

    /**
     * Overwrites the native method, and implements look-up of the passed value
     * by using the element's method `T.equals()`.
     *
     * @param value
     *  The value to look up in this array.
     *
     * @returns
     *  The index of the first array element that equals with the passed value
     *  according to their `equals()` method.
     */
    override indexOf(value: T): number {
        return this.findIndex(elem => elem.equals(value));
    }

    /**
     * Overwrites the method of the array base class, and implements look-up of
     * the passed value by using the element's method `T.equals()`.
     *
     * @param value
     *  The value to look up in this array.
     *
     * @returns
     *  The index of the last array element that equals with the passed value
     *  according to their `equals()` method.
     */
    override lastIndexOf(value: T): number {
        return this.findLastIndex(elem => elem.equals(value));
    }

    /**
     * Sorts the elements of this array in-place.
     *
     * @param compareFn
     *  The comparator callback function that receives two elements from this
     *  array, and returns their relation as signed number.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @returns
     *  A reference to this array.
     */
    override sort(compareFn: SortComparatorFn<T>, context?: object): this {
        return super.sort(compareFn.bind(context));
    }

    /**
     * Sorts the elements of this array in-place. The elements will be sorted
     * according to the return values of the callback function, or to the
     * property value of the elements specified as string parameter.
     *
     * @param indexerFn
     *  The callback function that will be invoked for each element in this
     *  array to receive a sort index relative to the other elements.
     *
     * @param [context]
     *  The calling context for the callback function.
     *
     * @param prop
     *  The name of a property of each object element that will be used for
     *  sorting.
     *
     * @returns
     *  A reference to this array.
     */
    sortBy(indexerFn: SortIndexerFn<T>, context?: object): this;
    sortBy(prop: KeysOf<T>): this;
    // implementation
    sortBy(arg: SortIndexerFn<T> | KeysOf<T>, context?: object): this {
        const indexer = is.string(arg) ? ((value: T) => value[arg]) : arg.bind(context);
        return super.sort((value1, value2) => math.compare(indexer(value1), indexer(value2)));
    }

    /**
     * Returns a shallow copy of this list that does not contain any duplicate
     * elements (comparing by the element keys).
     *
     * @returns
     *  A shallow copy of this list without any duplicates.
     */
    unify(): this {

        // nothing to do for empty array, or single element; but return a cloned array
        if (this.length <= 1) { return this.clone(); }

        // a set with the unique keys of all elements (prevents to search for duplicates in the result)
        const keys = new Set<string>();

        // remove all elements from the result that are already contained in the map
        return this.filter(element => {
            const { key } = element;
            if (keys.has(key)) { return false; }
            keys.add(key);
            return true;
        });
    }

    /**
     * Converts this array to a JSON array with JSON elements.
     *
     * @returns
     *  A JSON array with JSON elements.
     */
    toJSON(): unknown[] {
        const jsonArray: unknown[] = [];
        this.forEach(value => jsonArray.push((is.object(value) && is.function(value.toJSON)) ? value.toJSON() : value));
        return jsonArray;
    }

    /**
     * Stringifies all array elements, and joins them with the passed separator
     * text.
     *
     * @param [separator]
     *  The separator text to be inserted between the string representation of
     *  the array elements.
     *
     * @returns
     *  The string representation of this array.
     */
    override toString(separator = ","): string {
        return this.join(separator);
    }

    // private methods --------------------------------------------------------

    /**
     * Hides the TypeScript workaround to create a correctly typed new instance
     * of a subclass. Can be overwritten by subclasses to create new instances
     * with passing some parameters to the constructor.
     */
    #new(): this {
        return new (this.constructor as CtorType<this>)();
    }

    /**
     * Hides the TypeScript workaround to create a correctly typed new instance
     * of a subclass with the passed elements.
     */
    #make(values: T[]): this {
        const result = this.#new();
        result.length = values.length;
        values.forEach((v, ai) => { result[ai] = v; });
        return result;
    }
}
