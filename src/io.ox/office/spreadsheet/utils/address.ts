/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type Relation, math, fmt } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * A function that returns one specific index of a cell address.
 */
export type AddressIndexFn = (address: Address) => number;

// constants ==================================================================

/**
 * The maximum number of columns that will ever be allowed in a sheet (column A
 * to column ZZZZ).
 */
const MAX_COL_COUNT = 475_254;

/**
 * The maximum number of rows that will ever be allowed in a sheet (row 1 to
 * row 99,999,999).
 */
const MAX_ROW_COUNT = 99_999_999;

/**
 * A regular expression pattern matching a valid column name (A to ZZZZ).
 */
export const COL_PATTERN = "[A-Z]{1,4}";

/**
 * A regular expression pattern matching a row name (1 to 99,999,999).
 */
export const ROW_PATTERN = "0*[1-9][0-9]{0,7}";

/**
 * A regular expression object matching a row name only.
 */
const RE_ROW_NAME = new RegExp(`^${ROW_PATTERN}$`);

/**
 * A regular expression object matching a cell name.
 */
const RE_CELL_NAME = new RegExp(`^(${COL_PATTERN})(${ROW_PATTERN})$`, "i");

// public functions ===========================================================

/**
 * Returns a unique string key for the specified cell address components that
 * can be used for example as key in an associative map.
 *
 * @param col
 *  The zero-based column index.
 *
 * @param row
 *  The zero-based row index.
 *
 * @returns
 *  A unique string key for the cell address.
 */
export function addressKey(col: number, row: number): string {
    return `${col},${row}`;
}

/**
 * Returns the string representation of the passed column index.
 *
 * @param col
 *  The zero-based column index.
 *
 * @returns
 *  The upper-case string representation of the column index.
 */
export function stringifyCol(col: number): string {
    return fmt.formatAlphabetic(col + 1);
}

/**
 * Returns the string representation of the passed row index.
 *
 * @param row
 *  The zero-based row index.
 *
 * @returns
 *  The one-based string representation of the row index.
 */
export function stringifyRow(row: number): string {
    return (row + 1).toString();
}

/**
 * Returns the string representation of the passed column or row index.
 *
 * @param index
 *  The zero-based column or row index.
 *
 * @param columns
 *  Whether the passed index is a column index (`true`), or a row index
 *  (`false`).
 *
 * @returns
 *  The string representation of the column or row index.
 */
export function stringifyIndex(index: number, columns: boolean): string {
    return columns ? stringifyCol(index) : stringifyRow(index);
}

/**
 * Returns the column index of the passed column string representation.
 *
 * @param text
 *  The string representation of a column (case-insensitive).
 *
 * @returns
 *  The zero-based column index, if the passed string is valid, otherwise the
 *  value -1.
 */
export function parseCol(text: string): number {
    const col = (fmt.parseAlphabetic(text) || 0) - 1;
    return (col < MAX_COL_COUNT) ? col : -1;
}

/**
 * Returns the row index of the passed row string representation.
 *
 * @param text
 *  The string representation of a row (one-based).
 *
 * @returns
 *  The zero-based row index, if the passed string is valid, otherwise the
 *  value -1.
 */
export function parseRow(text: string): number {
    const row = RE_ROW_NAME.test(text) ? (parseInt(text, 10) - 1) : -1;
    return (row < MAX_ROW_COUNT) ? row : -1;
}

// class Address ==============================================================

/**
 * Represents the address of a single cell in a sheet of a spreadsheet
 * document.
 *
 * @param col
 *  The zero-based index of the column containing the cell.
 *
 * @param row
 *  The zero-based index of the row containing the cell.
 */
export class Address implements Keyable, Equality<Address>, Cloneable<Address> {

    // static properties ------------------------------------------------------

    /**
     * Convenience shortcut for the address A1 (zero for column and row index).
     */
    static A1: Address = new Address(0, 0);

    // static functions -------------------------------------------------------

    /**
     * Creates a cell address from direction-dependent indexes.
     *
     * @param columns
     *  Whether the first index is a column index (`true`), or a row index
     *  (`false`).
     *
     * @param index1
     *  The first index (column or row, dependent on parameter "columns").
     *
     * @param index2
     *  The second index (column or row, dependent on parameter "columns").
     *
     * @returns
     *  The new cell address.
     */
    static fromDir(columns: boolean, index1: number, index2: number): Address {
        return columns ? new Address(index1, index2) : new Address(index2, index1);
    }

    /**
     * Returns the cell address for the passed string representation.
     *
     * @param text
     *  The string representation of a cell address, in A1 notation.
     *
     * @returns
     *  The cell address, if the passed string is valid, otherwise null.
     */
    static parse(text: string): Address | null {
        const matches = RE_CELL_NAME.exec(text);
        if (!matches) { return null; }
        const col = parseCol(matches[1]);
        const row = parseRow(matches[2]);
        return ((col >= 0) && (row >= 0)) ? new Address(col, row) : null;
    }

    /**
     * Compares the passed cell addresses. Defines a natural order for sorting
     * with the following behavior (let A and B be cell addresses): If the row
     * index of A is less than the row index of B; or if both row indexes are
     * equal, and the column index of A is less than the column index of B,
     * then A is considered less than B (horizontally ordered cell addresses).
     *
     * @param address1
     *  The first cell address to be compared to the second cell address.
     *
     * @param address2
     *  The second cell address to be compared to the first cell address.
     *
     * @returns
     *  The relation of the addresses.
     */
    static compare(address1: Address, address2: Address): Relation {
        return math.comparePairs(address1.r, address1.c, address2.r, address2.c);
    }

    /**
     * Compares the passed cell addresses. Defines a natural order for sorting
     * with the following behavior (let A and B be cell addresses): If the
     * column index of A is less than the column index of B; or if both column
     * indexes are equal, and the row index of A is less than the row index of
     * B, then A is considered less than B (vertically ordered cell addresses).
     *
     * @param address1
     *  The first cell address to be compared to the second cell address.
     *
     * @param address2
     *  The second cell address to be compared to the first cell address.
     *
     * @returns
     *  The relation of the addresses.
     */
    static compareVert(address1: Address, address2: Address): Relation {
        return math.comparePairs(address1.c, address1.r, address2.c, address2.r);
    }

    /**
     * Compares the column indexes of the passed cell addresses.
     *
     * @param address1
     *  The first cell address to be compared to the second cell address.
     *
     * @param address2
     *  The second cell address to be compared to the first cell address.
     *
     * @returns
     *  The relation of the column indexes in the addresses.
     */
    static compareCol(address1: Address, address2: Address): Relation {
        return math.compare(address1.c, address2.c);
    }

    /**
     * Compares the row indexes of the passed cell addresses.
     *
     * @param address1
     *  The first cell address to be compared to the second cell address.
     *
     * @param address2
     *  The second cell address to be compared to the first cell address.
     *
     * @returns
     *  The relation of the row indexes in the addresses.
     */
    static compareRow(address1: Address, address2: Address): Relation {
        return math.compare(address1.r, address2.r);
    }

    /**
     * Returns a function that converts a cell address to one of its indexes.
     * Used especially for working with sorted arrays of addresses or related
     * objects.
     *
     * @param columns
     *  Whether to create a function that returns the column index (`true`), or
     *  the row index (`false`),
     *
     * @returns
     *  A function that converts a cell address to one of its indexes.
     */
    static indexFn(columns: boolean): AddressIndexFn {
        return columns ? (address => address.c) : (address => address.r);
    }

    // properties -------------------------------------------------------------

    /**
     * The zero-based index of the column containing the cell.
     */
    c: number;

    /**
     * The zero-based index of the row containing the cell.
     */
    r: number;

    // constructor ------------------------------------------------------------

    constructor(col: number, row: number) {
        this.c = col;
        this.r = row;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns a unique string key for this cell address that can be used for
     * example as key in an associative map. This method is faster than the
     * method `toString()`.
     *
     * @returns
     *  A unique string key for this cell address.
     */
    get key(): string {
        return addressKey(this.c, this.r);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a clone of this cell address.
     *
     * @returns
     *  A clone of this address.
     */
    clone(): Address {
        return new Address(this.c, this.r);
    }

    /**
     * Returns whether the passed cell address is equal to this cell address.
     *
     * @param address
     *  The other cell address to be compared to this cell address.
     *
     * @returns
     *  Whether both cell addresses contain the same column and row indexes.
     */
    equals(address: Address): boolean {
        return (this.c === address.c) && (this.r === address.r);
    }

    /**
     * Returns whether the passed cell address differs from this cell address.
     *
     * @param address
     *  The other cell address to be compared to this cell address.
     *
     * @returns
     *  Whether the passed cell address differs from this cell address.
     */
    differs(address: Address): boolean {
        return (this.c !== address.c) || (this.r !== address.r);
    }

    /**
     * Compares this cell address with the passed cell address. See static
     * function `Address.compare()` for details.
     *
     * @param address
     *  The other cell address to be compared to this cell address.
     *
     * @returns
     *  The relation of this address and the passed address.
     */
    compareTo(address: Address): Relation {
        return Address.compare(this, address);
    }

    /**
     * Compares this cell address with the passed cell address. See static
     * function `Address.compareVert()` for details.
     *
     * @param address
     *  The other cell address to be compared to this cell address.
     *
     * @returns
     *  The relation of this address and the passed address.
     */
    compareVertTo(address: Address): Relation {
        return Address.compareVert(this, address);
    }

    /**
     * Returns the column or row index of this cell address.
     *
     * @param columns
     *  Whether to return the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  The column or row index of this cell address.
     */
    get(columns: boolean): number {
        return columns ? this.c : this.r;
    }

    /**
     * Returns a copy of this cell address with switched column and row index.
     *
     * @returns
     *  A copy of this cell address with switched column and row index.
     */
    mirror(): Address {
        return new Address(this.r, this.c);
    }

    /**
     * Sets a new column or row index for this cell address.
     *
     * @param index
     *  The new column or row index for this cell address.
     *
     * @param columns
     *  Whether to change the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  A reference to this instance.
     */
    set(index: number, columns: boolean): this {
        if (columns) { this.c = index; } else { this.r = index; }
        return this;
    }

    /**
     * Sets the contents of this cell address to the contents of the passed
     * cell address.
     *
     * @param address
     *  The cell address to be copied into this cell address.
     *
     * @returns
     *  A reference to this instance.
     */
    assign(address: Address): this {
        this.c = address.c;
        this.r = address.r;
        return this;
    }

    /**
     * Modifies the column or row index of this cell address relatively.
     *
     * @param distance
     *  The distance to move the column or row index of this cell address.
     *  Negative values will decrease the index.
     *
     * @param columns
     *  Whether to change the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  A reference to this instance.
     */
    move(distance: number, columns: boolean): this {
        if (columns) { this.c += distance; } else { this.r += distance; }
        return this;
    }

    /**
     * Returns the A1 notation of this cell address as used in document
     * operations.
     *
     * @returns
     *  The A1 notation of this cell address as used in document operations.
     */
    toOpStr(): string {
        return stringifyCol(this.c) + stringifyRow(this.r);
    }

    /**
     * Returns the string representation of this cell address, in upper-case A1
     * notation.
     *
     * @returns
     *  The string representation of this cell address, in upper-case A1
     *  notation.
     */
    toString(): string {
        return this.toOpStr();
    }
}
