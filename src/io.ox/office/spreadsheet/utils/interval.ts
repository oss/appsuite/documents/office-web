/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ReverseOptions, type Relation, math, itr } from "@/io.ox/office/tk/algorithms";

import { COL_PATTERN, ROW_PATTERN, stringifyCol, stringifyRow, parseCol, parseRow } from "@/io.ox/office/spreadsheet/utils/address";

const { min, max } = Math;

// types ======================================================================

/**
 * Optional parameters for the function `IndexIterator#interval`.
 */
export interface IntervalIteratorOptions extends ReverseOptions { }

// constants ==================================================================

/**
 * Regular expression object matching a column interval.
 */
const RE_COL_INTERVAL_NAME = new RegExp(`^(${COL_PATTERN}):(${COL_PATTERN})$`, "i");

/**
 * Regular expression object matching a row interval.
 */
const RE_ROW_INTERVAL_NAME = new RegExp(`^(${ROW_PATTERN}):(${ROW_PATTERN})$`);

// public functions ===========================================================

/**
 * Returns a unique string key for the specified indexes of an interval that
 * can be used for example as key in an associative map.
 *
 * @param first
 *  The first index of an index interval.
 *
 * @param last
 *  The last index of an index interval.
 *
 * @returns
 *  A unique string key for the index interval.
 */
export function intervalKey(first: number, last: number): string {
    return `${first}:${last}`;
}

// class Interval =============================================================

/**
 * Represents the position of an interval of columns or rows in a sheet of a
 * spreadsheet document (an "index interval").
 */
export class Interval implements Keyable, Equality<Interval>, Cloneable<Interval> {

    // static functions -------------------------------------------------------

    /**
     * Creates an index interval from the passed column/row indexes. The first
     * and last index in the resulting interval will be ordered automatically.
     *
     * @param index1
     *  The index of one of the borders in the index interval.
     *
     * @param index2
     *  The index of the other border in the index interval.
     *
     * @returns
     *  The index interval with adjusted indexes.
     */
    static create(index1: number, index2: number): Interval {
        return new Interval(min(index1, index2), max(index1, index2));
    }

    /**
     * Returns the index interval for the passed string representation of a
     * column interval. The first and last index in the resulting interval will
     * be ordered automatically.
     *
     * @param text
     *  The string representation of a column interval, e.g. `"A:D"`.
     *
     * @returns
     *  The index interval, if the passed string is valid, otherwise `null`.
     */
    static parseAsCols(text: string): Interval | null {

        // range notation (e.g. "A:C")
        const matches = RE_COL_INTERVAL_NAME.exec(text);
        if (matches) {
            const col1 = parseCol(matches[1]);
            const col2 = parseCol(matches[2]);
            return (min(col1, col2) >= 0) ? Interval.create(col1, col2) : null;
        }

        // index notation (e.g. "A")
        const col = parseCol(text);
        return (col >= 0) ? new Interval(col) : null;
    }

    /**
     * Returns the index interval for the passed string representation of a row
     * interval. The first and last index in the resulting interval will be
     * ordered automatically.
     *
     * @param text
     *  The string representation of a row interval, e.g. `"1:4"`.
     *
     * @returns
     *  The index interval, if the passed string is valid, otherwise `null`.
     */
    static parseAsRows(text: string): Interval | null {

        // range notation (e.g. "1:3")
        const matches = RE_ROW_INTERVAL_NAME.exec(text);
        if (matches) {
            const row1 = parseRow(matches[1]);
            const row2 = parseRow(matches[2]);
            return (min(row1, row2) >= 0) ? Interval.create(row1, row2) : null;
        }

        // index notation (e.g. "1")
        const row = parseRow(text);
        return (row >= 0) ? new Interval(row) : null;
    }

    /**
     * Returns the index interval for the passed string representation of a
     * column or row interval. The first and last index in the resulting
     * interval will be ordered automatically.
     *
     * @param text
     *  The string representation of a column or row interval.
     *
     * @param columns
     *  Whether the passed string represents a column interval (`true`), or a
     *  row interval (`false`).
     *
     * @returns
     *  The index interval, if the passed string is valid, otherwise `null`.
     */
    static parseAs(text: string, columns: boolean): Interval | null {
        return columns ? Interval.parseAsCols(text) : Interval.parseAsRows(text);
    }

    /**
     * Compares the passed index intervals. Defines a natural order for sorting
     * with the following behavior (let A and B be index intervals): If the
     * first index of A is less than the first index of B; or if both first
     * indexes are equal, and the last index of A is less than the last index
     * of B, then A is considered less than B.
     *
     * @param interval1
     *  The first index interval to be compared to the second index interval.
     *
     * @param interval2
     *  The second index interval to be compared to the first index interval.
     *
     * @returns
     *  The relation of the passed index intervals.
     */
    static compare(interval1: Interval, interval2: Interval): Relation {
        return math.comparePairs(interval1.first, interval1.last, interval2.first, interval2.last);
    }

    /**
     * Compares the passed index intervals. Defines a natural order for sorting
     * with the following behavior (let A and B be index intervals): If the
     * last index of A is less than the last index of B; or if both last
     * indexes are equal, and the first index of A is less than the first index
     * of B, then A is considered less than B.
     *
     * @param interval1
     *  The first index interval to be compared to the second index interval.
     *
     * @param interval2
     *  The second index interval to be compared to the first index interval.
     *
     * @returns
     *  The relation of the passed index intervals.
     */
    static compareLast(interval1: Interval, interval2: Interval): Relation {
        return math.comparePairs(interval1.last, interval1.first, interval2.last, interval2.first);
    }

    // properties -------------------------------------------------------------

    /**
     * The zero-based index of the first column/row in the interval.
     */
    first: number;

    /**
     * The zero-based index of the last column/row in the interval.
     */
    last: number;

    // constructor ------------------------------------------------------------

    /**
     * @param first
     *  The zero-based index of the first column/row in the interval.
     *
     * @param [last]
     *  The zero-based index of the last column/row in the interval. Can be
     *  omitted to construct an index interval covering a single column or row.
     */
    constructor(first: number, last = first) {
        this.first = first;
        this.last = last;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns a unique string key for this index interval that can be used for
     * example as key in an associative map. This method is faster than the
     * method Interval.toString().
     *
     * @returns
     *  A unique string key for this index interval.
     */
    get key(): string {
        return intervalKey(this.first, this.last);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a clone of this index interval.
     *
     * @returns
     *  A clone of this index interval.
     */
    clone(): Interval {
        return new Interval(this.first, this.last);
    }

    /**
     * Returns whether the passed index intervals is equal to this interval.
     *
     * @param interval
     *  The other index interval to be compared to this interval.
     *
     * @returns
     *  Whether both intervals contain the same column/row indexes.
     */
    equals(interval: Interval): boolean {
        return (this.first === interval.first) && (this.last === interval.last);
    }

    /**
     * Returns whether the passed index intervals differs from this interval.
     *
     * @param interval
     *  The other index interval to be compared to this interval.
     *
     * @returns
     *  Whether the passed index intervals differs from this interval.
     */
    differs(interval: Interval): boolean {
        return (this.first !== interval.first) || (this.last !== interval.last);
    }

    /**
     * Compares this index interval with the passed index interval. See static
     * function `Interval.compare()` for details.
     *
     * @param interval
     *  The other index interval to be compared to this index interval.
     *
     * @returns
     *  The relation of the this interval and the passed interval.
     */
    compareTo(interval: Interval): Relation {
        return Interval.compare(this, interval);
    }

    /**
     * Returns the number of columns/rows covered by this index interval.
     *
     * @returns
     *  The number of columns/rows covered by this index interval.
     */
    size(): number {
        return this.last - this.first + 1;
    }

    /**
     * Returns whether this index interval covers a single index (first and
     * last index are equal).
     *
     * @returns
     *  Whether this index interval covers a single index.
     */
    single(): boolean {
        return this.first === this.last;
    }

    /**
     * Returns whether this index interval contains the passed column/row
     * index.
     *
     * @param index
     *  The zero-based column/row index to be checked.
     *
     * @returns
     *  Whether this index interval contains the passed column/row index.
     */
    containsIndex(index: number): boolean {
        return (this.first <= index) && (index <= this.last);
    }

    /**
     * Returns whether this index interval completely contains the passed index
     * interval.
     *
     * @param interval
     *  The index interval to be checked.
     *
     * @returns
     *  Whether this index interval completely contains the passed index
     *  interval.
     */
    contains(interval: Interval): boolean {
        return (this.first <= interval.first) && (interval.last <= this.last);
    }

    /**
     * Returns whether this index interval overlaps with the passed index
     * interval by at least one column/row.
     *
     * @param interval
     *  The index interval to be checked.
     *
     * @returns
     *  Whether this index interval overlaps with the passed index interval.
     */
    overlaps(interval: Interval): boolean {
        return (this.first <= interval.last) && (interval.first <= this.last);
    }

    /**
     * Returns the bounding interval of this index interval, and the passed
     * index interval (the smallest index interval that contains both
     * intervals).
     *
     * @param interval
     *  The other index interval to create the bounding interval from.
     *
     * @returns
     *  The bounding interval containing this and the passed index interval.
     */
    boundary(interval: Interval): Interval {
        return new Interval(min(this.first, interval.first), max(this.last, interval.last));
    }

    /**
     * Returns the index interval covered by this index interval, and the
     * passed index interval.
     *
     * @param interval
     *  The other index interval to be intersected with this index interval.
     *
     * @returns
     *  The index interval covered by this and the passed index interval, if
     *  existing; otherwise null.
     */
    intersect(interval: Interval): Interval | null {
        const first = max(this.first, interval.first);
        const last = min(this.last, interval.last);
        return (first <= last) ? new Interval(first, last) : null;
    }

    /**
     * Sets the contents of this index interval to the contents of the passed
     * index interval.
     *
     * @param interval
     *  The index interval to be copied into this index interval.
     *
     * @returns
     *  A reference to this instance.
     */
    assign(interval: Interval): this {
        this.first = interval.first;
        this.last = interval.last;
        return this;
    }

    /**
     * Modifies both indexes of this index interval relatively.
     *
     * @param distance
     *  The distance to move the first and last index of this index interval.
     *  Negative values will decrease the indexes.
     *
     * @returns
     *  A reference to this instance.
     */
    move(distance: number): this {
        this.first += distance;
        this.last += distance;
        return this;
    }

    /**
     * Creates an iterator that visits the single indexes of this interval.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new iterator.
     */
    indexes(options?: IntervalIteratorOptions): IterableIterator<number> {
        return itr.interval(this.first, this.last, options?.reverse);
    }

    /**
     * Returns the A1 notation of this column interval as used in document
     * operations (e.g. `"A:C"` or `"B"`).
     *
     * @returns
     *  The A1 notation of this column interval as used in document operations.
     */
    toOpStrCols(): string {
        const first = stringifyCol(this.first);
        return this.single() ? first : `${first}:${stringifyCol(this.last)}`;
    }

    /**
     * Returns the A1 notation of this row interval as used in document
     * operations (e.g. `"1:3"` or `"2"`).
     *
     * @returns
     *  The A1 notation of this row interval as used in document operations.
     */
    toOpStrRows(): string {
        const first = stringifyRow(this.first);
        return this.single() ? first : `${first}:${stringifyRow(this.last)}`;
    }

    /**
     * Returns the A1 notation of this index interval as used in document
     * operations (e.g. `"A:C"`, `"1:3"`, `"B"`, or `"2"`).
     *
     * @param columns
     *  Whether to return the column representation (`true`), or the row
     *  representation (`false`) of the index interval.
     *
     * @returns
     *  The A1 notation of this index interval as used in document operations.
     */
    toOpStr(columns: boolean): string {
        return columns ? this.toOpStrCols() : this.toOpStrRows();
    }

    /**
     * Returns the string representation of this index interval.
     *
     * @returns
     *  The string representation of this index interval.
     */
    toString(columns?: boolean): string {
        const stringify = columns ? stringifyCol : stringifyRow;
        return `${stringify(this.first)}:${stringify(this.last)}`;
    }
}
