/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary, json } from "@/io.ox/office/tk/algorithms";

import { type Position, clonePositions } from "@/io.ox/office/editframework/utils/operations";

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { type RangeSource, RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

// types =====================================================================

/**
 * The selection state of a spreadsheet doument.
 */
export interface SelectionState {

    /**
     * The zero-based index of the active sheet.
     */
    sheet: number;

    /**
     * The selection (cell ranges and drawing objects) in the active sheet.
     */
    selection: SheetSelection;
}

// private functions ==========================================================

/**
 * Returns whether the passed addresses are equal. Either of the parameters can
 * be `null`.
 */
function equalOptAddresses(address1: Address | null, address2: Address | null): boolean {
    return (!address1 && !address2) || Boolean(address1 && address2 && address1.equals(address2));
}

// class SheetSelection =======================================================

/**
 * Represents a complete sheet selection (cell ranges and drawing objects).
 *
 * @param [ranges]
 *  The addresses of all selected cell ranges, or a single range address. If
 *  omitted, the new object will be constructed with an empty range address
 *  list.
 *
 * @param [active]
 *  Element index of the active range in the property `ranges`.
 *
 * @param [address]
 *  The address of the active cell in the active range. If omitted, the
 *  top-left cell of the active range will be used.
 *
 * @param [drawings]
 *  The positions of all selected drawing frames in the sheet (without leading
 *  sheet index). If omitted, an empty array will be created.
 */
export class SheetSelection implements Equality<SheetSelection>, Cloneable<SheetSelection> {

    // static functions -------------------------------------------------------

    /**
     * Creates a selection instance from a single cell address. The selection
     * will contain a single cell range covering the address, and the active
     * cell will be set to the address.
     *
     * @param address
     *  The cell address to create the sheet selection instance from.
     *
     * @returns
     *  The new sheet selection instance.
     */
    static createFromAddress(address: Address, drawings?: Position[]): SheetSelection {
        return new SheetSelection(new Range(address.clone()), 0, address.clone(), drawings);
    }

    // properties -------------------------------------------------------------

    /**
     * The addresses of all selected cell ranges.
     */
    ranges: RangeArray;

    /**
     * Element index of the active range in the property `ranges`.
     */
    active: number;

    /**
     * The address of the active cell in the active range.
     */
    address: Address;

    /**
     * The exact position of the active cell. Used in case a merged range has
     * been set as active cell using the keyboard, to keep track of the exact
     * cell position when moving the active cell again.
     */
    origin: Address | null;

    /**
     * The positions of all selected drawing frames in the sheet (without
     * leading sheet index).
     */
    drawings: Position[];

    // constructor ------------------------------------------------------------

    constructor(ranges?: RangeSource, active?: number, address?: Address, drawings?: Position[]) {

        // initialize array of cell range addresses (default to empty array)
        this.ranges = RangeArray.cast(ranges);

        // initialize array index of active range (restrict to existing ranges, default to first range)
        this.active = Math.min(is.number(active) ? Math.max(active, 0) : 0, this.ranges.length - 1);

        // initialize address (fall-back to start cell of active range)
        this.address = address ?? (this.ranges.empty() ? Address.A1.clone() : this.activeRange()!.a1.clone());

        // support an additional address property, used to store the exact position in merged ranges
        this.origin = null;

        // initialize drawing selection (fall-back to empty array)
        this.drawings = is.array(drawings) ? drawings : [];
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a deep clone of this selection instance.
     *
     * @returns
     *  A deep clone of this selection instance.
     */
    clone(): SheetSelection {
        const selection = new SheetSelection(this.ranges.deepClone(), this.active, this.address.clone(), clonePositions(this.drawings));
        if (this.origin) { selection.origin = this.origin.clone(); }
        return selection;
    }

    /**
     * Returns whether the passed sheet selection is equal to this sheet
     * selection.
     *
     * @param selection
     *  The other sheet selection to be compared to this instance.
     *
     * @returns
     *  Whether both sheet selections are equal.
     */
    equals(selection: SheetSelection): boolean {
        return (this.active === selection.active) && // most simple comparison first
            this.address.equals(selection.address) &&
            equalOptAddresses(this.origin, selection.origin) &&
            this.ranges.deepEquals(selection.ranges) &&
            (this.drawings.length === selection.drawings.length) &&
            ary.equals(this.drawings, selection.drawings, ary.equals);
    }

    /**
     * Returns the address of the active range in this selection.
     *
     * @returns
     *  The address of the active cell range; or `null`, if this selection is
     *  empty.
     */
    activeRange(): Range | null {
        return this.ranges[this.active] || null;
    }

    /**
     * Returns whether this selection contains the passed cell address.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether a cell range address in this selection contains the passed cell
     *  address.
     */
    containsAddress(address: Address): boolean {
        return this.ranges.containsAddress(address);
    }

    /**
     * Returns whether the passed cell address is the active cell of this
     * selection.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether the passed cell address is the active cell of this selection.
     */
    isActive(address: Address): boolean {
        return Boolean(this.address) && this.address.equals(address);
    }

    /**
     * Adds the passed drawing position to this selection.
     *
     * @param position
     *  The position of a drawing object in the sheet (without sheet index).
     *
     * @returns
     *  Whether the passed position was not part of this selection before (i.e.
     *  whether it has been added to the selection).
     */
    selectDrawing(position: Position): boolean {

        // try to find an existing drawing position
        const ai = this.drawings.findIndex(drawingPos => ary.equals(position, drawingPos));

        // nothing to do if the drawing position exists already
        if (ai < this.drawings.length) { return false; }

        // select the drawing object
        this.drawings.push(position);
        return true;
    }

    /**
     * Removes the passed drawing position from this selection.
     *
     * @param position
     *  The position of a drawing object in the sheet (without sheet index).
     *
     * @returns
     *  Whether the passed position was part of this selection (i.e. whether it
     *  has been removed from the selection).
     */
    deselectDrawing(position: Position): boolean {
        return ary.deleteAllMatching(this.drawings, drawingPos => ary.equals(position, drawingPos)) > 0;
    }

    /**
     * Toggles the passed drawing position in this selection.
     *
     * @param position
     *  The position of a drawing object in the sheet (without sheet index).
     *
     * @returns
     *  Whether the passed position is now part of this selection.
     */
    toggleDrawing(position: Position): boolean {

        // try to deselect the drawing object
        if (this.deselectDrawing(position)) { return false; }

        // select the drawing object
        this.drawings.push(position);
        return true;
    }

    /**
     * Returns the string representation of this selection for debug logging.
     *
     * @returns
     *  The string representation of this selection for debug logging.
     */
    toString(): string {
        return `ranges=${this.ranges} active=${this.active}=${this.activeRange()} address=${this.address} origin=${this.origin} drawings=${json.safeStringify(this.drawings)}`;
    }
}
