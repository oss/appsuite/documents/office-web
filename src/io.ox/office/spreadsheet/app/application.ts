/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { pick, json } from "@/io.ox/office/tk/algorithms";

import { FlushReason } from "@/io.ox/office/baseframework/utils/apputils";
import type ErrorCode from "@/io.ox/office/baseframework/utils/errorcode";
import { SPREADSHEET } from "@/io.ox/office/baseframework/utils/errorcontext";
import type { CoreApp } from "@/io.ox/office/baseframework/app/appfactory";
import { handleMinifiedObject } from "@/io.ox/office/editframework/utils/operationutils";
import { type EmergencyLeaveData, EditApplication } from "@/io.ox/office/editframework/app/editapplication";
import type { TextBaseApplicationEventMap, TextBaseApplication } from "@/io.ox/office/textframework/app/application";

import { generateSheetName } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import type { SpreadsheetResourceManager } from "@/io.ox/office/spreadsheet/resource/resourcemanager";
import { SheetOperationGenerator } from "@/io.ox/office/spreadsheet/model/operation/generator";
import type { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";
import type { RecalcFormulasOptions } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencymanager";
import type { SpreadsheetOTManager } from "@/io.ox/office/spreadsheet/model/operation/otmanager";
import type { SpreadsheetView } from "@/io.ox/office/spreadsheet/view/spreadsheetview";
import type { SpreadsheetController } from "@/io.ox/office/spreadsheet/controller/spreadsheetcontroller";
import mvcFactory from "@/io.ox/office/spreadsheet/app/mvcfactory";

// types ======================================================================

/**
 * Type mapping for the events emitted by `SpreadsheetApplication` instances.
 */
export interface SpreadsheetApplicationEventMap extends TextBaseApplicationEventMap {

    /**
     * Will be emitted during the import process when the active sheet has been
     * loaded, and import of all other sheets starts.
     */
    "docs:preview:activesheet": [];
}

// class SpreadsheetApp =======================================================

interface SpreadsheetApp extends TextBaseApplication<SpreadsheetApplicationEventMap> { }

/**
 * The OX Spreadsheet application.
 */
class SpreadsheetApp extends EditApplication<SpreadsheetApplicationEventMap> {

    declare readonly resourceManager: SpreadsheetResourceManager;
    declare readonly docModel: SpreadsheetModel;
    declare readonly docView: SpreadsheetView;
    declare readonly docController: SpreadsheetController;
    declare readonly otManager: SpreadsheetOTManager;

    // constructor ------------------------------------------------------------

    /**
     * @param coreApp
     *  The core application instance.
     */
    constructor(coreApp: CoreApp) {

        super(coreApp, mvcFactory, {
            otConfigKey: "spreadsheet/concurrentEditing",
            realTimeDelay: 10,
            supportsOnlineSync: true,
        });

        const hasUnsavedChangesOffice = this.hasUnsavedChangesOffice.bind(this);

        /**
         * Returns whether this application caches any contents that have not
         * been sent successfully to the server. In addition to unsaved JSON
         * document operations checked by the base class `EditApplication`,
         * this method checks for running cell text edit mode.
         *
         * @returns
         *  Whether the application currently caches unsaved contents.
         */
        this.hasUnsavedChangesOffice = (): boolean => {

            // check for unsaved document actions
            if (hasUnsavedChangesOffice()) { return true; }

            // check for running cell text edit mode with unsaved changes
            return !!this.docView.getActiveTextEditor("cell")?.hasUnsavedChanges();
        };
    }

    // protected methods ------------------------------------------------------

    /**
     * Returns additional parameters to be inserted into the server request for
     * a new empty spreadsheet document.
     */
    protected override implPrepareNewDoc(): Dict {
        return {
            initial_sheetname: generateSheetName(1),
            initial_formats: json.safeStringify(this.docModel.numberFormatter.getInitialFormatCodes()),
        };
    }

    /**
     * Post-processing of the document, after all import operations have been
     * applied successfully.
     *
     * @returns
     *  A promise that will fulfil when the document has been post-processed
     *  successfully, or reject when an error has occurred.
     */
    protected override async implPostProcessImport(): Promise<void> {
        await this.docModel.postProcessImport();
    }

    /**
     * Shows an early preview of the document. Activates the sheet that is
     * specified in the passed preview data, and starts querying cell contents
     * for the active sheet.
     *
     * @param previewData
     *  The preview data containing the index of the active sheet.
     *
     * @returns
     *  Whether a sheet has been activated successfully.
     */
    protected override implPreviewDocument(previewData: Dict): boolean {
        const activeSheet = Math.floor(pick.number(previewData, "activeSheet", 0));
        const success = this.docController.activatePreviewSheet(activeSheet);
        // notify toolpanes, e.g. repaint the sheet tabs once during import
        if (success) { this.trigger("docs:preview:activesheet"); }
        return success;
    }

    /**
     * Will be called by base class if importing the document failed.
     *
     * @param error
     *  The parsed error response data.
     */
    protected override implImportFailed(error: ErrorCode): void {

        switch (error.getCodeAsConstant()) {
            case "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR":
            case "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR":
                this.setInternalError(error, SPREADSHEET, undefined, { showMessage: false });
                break;
        }
    }

    /**
     * Preprocessing before the document will be flushed for downloading,
     * printing, sending as mail, or closing.
     *
     * @param reason
     *  The origin of the flush request.
     *
     * @returns
     *  A promise that will fulfil when all pending update tasks have been
     *  finished (e.g. recalculating all dirty cell formulas).
     */
    protected override async implPrepareFlushDocument(reason: FlushReason): Promise<void> {
        const timeout = (reason === FlushReason.QUIT) ? 5000 : 15000;
        await this.#finalizePendingDocumentChanges({ timeout });
    }

    /**
     * Preparations before the edit mode will be switched off.
     *
     * @returns
     *  A promise that will fulfil when all pending update tasks have been
     *  finished (e.g. recalculating all dirty cell formulas).
     */
    protected override async implPrepareLoseEditRights(): Promise<void> {
        await this.#finalizePendingDocumentChanges();
    }

    /**
     * Preparations before the request to rename the edited document will be
     * sent to the backend.
     *
     * @returns
     *  A promise that will fulfil when all pending update tasks have been
     *  finished (e.g. recalculating all dirty cell formulas).
     */
    protected override async implPrepareRenameDocument(): Promise<void> {
        await this.#finalizePendingDocumentChanges({ timeout: 15000 });
    }

    /**
     * Returns launch options to be sent to the new document after it has been
     * reloaded.
     *
     * @returns
     *  The launch options needed to be passed to the reloaded document.
     */
    protected override implPrepareReloadDocument(): Dict {
        return this.docModel.createReloadSettings();
    }

    /**
     * Returns the data to be sent with a realtime emergency leave before the
     * browser window unloads.
     *
     * @returns
     *  The changed view attributes of this document if any, embedded in a
     *  leave-data structure as expected by the realtime connection.
     */
    protected override implPrepareLeaveData(): Opt<EmergencyLeaveData> {

        // add operations for pending changes (bug 65579: only if allowed)
        if (!this.#hasUnsavedPendingDocumentChanges()) { return; }

        // generate the operations to update the view settings synchronously (!)
        const generator = new SheetOperationGenerator(this.docModel);
        this.docModel.generatePendingChangesOperations(generator);
        const operations = generator.getOperations();
        if (operations.length === 0) { return; }

        // payload to be passed to the RT connection
        const leaveData: EmergencyLeaveData = {};

        // DOCS-2718: server needs the local OSN for OT
        if (this.isOTEnabled()) {
            const serverOSN = leaveData.serverOSN = this.getServerOSN();
            for (const operation of operations) {
                operation.osn = serverOSN;
            }
        }

        // minify the property names in the document operations (as done
        // by the edit application when sending operations regularly)
        for (const operation of operations) {
            handleMinifiedObject(operation);
        }
        leaveData.operations = operations;

        return leaveData;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether the document contains local pending changes that need to
     * be sent back to the server via specific document operations.
     *
     * Changed view attributes will only be sent when user has edit rights, and
     * has modified the document locally, or if "important" sheet view settings
     * (e.g. split settings) have been changed. This is needed to prevent
     * creating new versions of the document after selecting cells or scrolling
     * around, but without editing anything.
     *
     * @returns
     *  Whether the document contains pending changes that need to be sent back
     *  to the server.
     */
    #hasUnsavedPendingDocumentChanges(): boolean {

        // do not send anything without edit rights
        if (!this.isEditable()) { return false; }

        // if the document is modified locally, anything can be sent back
        if (this.isLocallyModified()) { return true; }

        // return if the document model contains pending changes
        return this.docModel.hasUnsavedPendingChanges();
    }

    /**
     * Generic handler that finalizes the document in preparation of any global
     * actions, such as downloading or closing the document, or losing the edit
     * rights.
     *
     * @param [options]
     *  Optional parameters. The option "timeout" is the maximum time allowed
     *  to recalculate all pending cell formulas, in milliseconds. If the time
     *  elapses before all formulas have been calculated, the recalculation
     *  cycle will be aborted, and the "calcOnLoad" document flag will be set.
     *  If omitted, no timeout will be set.
     *
     * @returns
     *  A promise that will fulfil when all pending update tasks have been
     *  finished (e.g. recalculating all dirty cell formulas).
     */
    async #finalizePendingDocumentChanges(options?: RecalcFormulasOptions): Promise<void> {

        // commit current edit text of cell edit mode (without validation)
        await this.docView.leaveTextEditMode({ force: true });

        // wait for the dependency manager recalculating all dirty formulas
        try {
            await this.docModel.dependencyManager.recalcFormulas({ ...options, volatile: false });
        } catch (cause) {
            // do not forward the rejected state of the dependency manager after a timeout
            if (cause === "destroy") { throw cause; }
        }

        // send all changed view settings (after recalculating the formulas which may
        // initially set the "document modified" flag)

        // Only send changed view attributes when user has edit rights, and has modified
        // the document locally, or if "important" sheet view settings have been changed.
        // This is needed to prevent creating new versions of the document after selecting
        // cells or scrolling around, but without editing anything.
        // Bug 53641: do not generate operations in long running processes (recursive call).
        if (!this.docModel.isProcessingActions() && this.#hasUnsavedPendingDocumentChanges()) {

            // generate and apply the operations for the document, and all affected sheets
            await this.docModel.buildOperations(builder => {
                this.docModel.generatePendingChangesOperations(builder.headGenerator);
            }, { undoMode: "skip" }); // do not create an undo action for view settings
        }
    }
}

export default SpreadsheetApp;
