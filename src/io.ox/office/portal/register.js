/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import registerStage from "$/io.ox/core/extPatterns/stage";

// static initialization ======================================================

// add the Portal applications to the main launcher menu of AppSuite
registerStage('io.ox/core/stages', {
    id: 'office_apps',
    after: 'app_register',
    async run() {

        const t0 = Date.now();

        // dynamically import source modules to include them in performance measurement
        const { globalLogger } = await import("@/io.ox/office/tk/utils/logger");
        const { registerDocumentsApps } = await import("@/io.ox/office/portal/app/appregistry");

        // register all portal and editor apps
        globalLogger.info("$badge{launch} starting application module registration ...");
        await registerDocumentsApps();
        globalLogger.info(`$badge{launch} ... application module registration done in $duration{${Date.now() - t0}}`);
    }
});
