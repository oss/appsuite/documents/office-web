/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import ext from '$/io.ox/core/extensions';
import apps from '$/io.ox/core/api/apps';
import mobile from '$/io.ox/backbone/views/actions/mobile';

import '@/io.ox/office/portal/action/portalactions'; // loading actions

import PageController from '$/io.ox/core/page-controller';
import Bars from '$/io.ox/core/toolbars-mobile';

import { TEXT_DOCUMENT_NAME_ENCRYPTED_NEW, TEXT_DOCUMENT_NAME_NEW,
    SPREADSHEET_DOCUMENT_NAME_ENCRYPTED_NEW, SPREADSHEET_DOCUMENT_NAME_NEW,
    PRESENTATION_DOCUMENT_NAME_ENCRYPTED_NEW, PRESENTATION_DOCUMENT_NAME_NEW,
    OPEN_DOCUMENT_LABEL, ADD_TEMPLATE_LABEL
} from '@/io.ox/office/baseframework/view/baselabels';

// constants ==================================================================

const APPTYPE_BUTTON_TEXT = {
    text: TEXT_DOCUMENT_NAME_NEW,
    spreadsheet: SPREADSHEET_DOCUMENT_NAME_NEW,
    presentation: PRESENTATION_DOCUMENT_NAME_NEW
};

const APPTYPE_BUTTON_TEXT_ENCRYPTED = {
    text: TEXT_DOCUMENT_NAME_ENCRYPTED_NEW,
    spreadsheet: SPREADSHEET_DOCUMENT_NAME_ENCRYPTED_NEW,
    presentation: PRESENTATION_DOCUMENT_NAME_ENCRYPTED_NEW
};

/**
 * Preparing the bottom toolbar in the Portal apps that is used
 * on small devices.
 *
 * @param {PortalApplication} app
 *  The Portal application.
 *
 * @param {String} appType
 *  The appType of the app for that the portal app is generated. This
 *  can be 'text', 'spreadsheet' or 'presentation'.
 *
 * @param {JQuery} toolbarParentNode
 *  The jQueryfied parent node for the bottom toolbar.
 */
export function prepareMobileToolbar(app, appType, toolbarParentNode) {

    app.navbar = $('#io-ox-appcontrol');
    app.toolbar = app.toolbar || $('<div class="mobile-toolbar flex">');
    app.pages = new PageController({ appname: app.options.name, navbar: app.navbar, toolbar: app.toolbar, container: toolbarParentNode });
    const baton = ext.Baton({ app });

    app.pages.addPage({
        name: 'main',
        startPage: true,
        navbar: new Bars.NavbarView({ baton }),
        showSearch: true,
        toolbar: new Bars.MobileToolbarView({
            baton,
            page: 'main',
            extension: `io.ox/${appType}/mobile/toolbar`
        })
    });

    app.pages.getNavbar('main')
        .setRight(app.toolbar);

    apps.trigger('show:appcontrol', app);
}

/**
 * Registering the content of the bottom toolbar in the Portal apps
 * that is used on small devices.
 *
 * @param {String} appType
 *  The appType of the app for that the portal app is generated. This
 *  can be 'text', 'spreadsheet' or 'presentation'.
 */
export function registerMobilePortalActions(appType) {

    // define links for each page
    const meta = {
        menu: {
            prio: 'hi',
            mobile: 'hi',
            icon: "bi/list.svg",
            dropdown: `io.ox/office/portal/${appType}/toolbar/menu`,
            drawDisabled: true,
            caret: false
        }
    };

    const points = {
        listView: `io.ox/${appType}/mobile/toolbar/main`,
        listViewMultiSelect: `io.ox/${appType}/mobile/toolbar/main/multiselect`
    };

    mobile.addAction(points.listView, meta, ['menu']);

    ext.point(`io.ox/office/portal/${appType}/toolbar/menu`).extend(
        {
            index: 100,
            id: `new-${appType}-document`,
            title: APPTYPE_BUTTON_TEXT[appType],
            ref: `io.ox/office/portal/${appType}/actions/new/${appType}`,
            section: 'new'
        },
        {
            index: 300,
            id: `new-${appType}-document-encrypted`,
            title: APPTYPE_BUTTON_TEXT_ENCRYPTED[appType],
            ref: `io.ox/office/portal/${appType}/actions/newencrypted/${appType}`,
            section: 'new'
        },
        {
            index: 500,
            id: `open-${appType}-document`,
            title: OPEN_DOCUMENT_LABEL,
            ref: `io.ox/office/portal/open${appType}`,
            section: 'open'
        },
        {
            index: 700,
            id: 'add-template',
            title: ADD_TEMPLATE_LABEL,
            ref: `io.ox/office/portal/add/template/${appType}`,
            section: 'open'
        }
    );
}
