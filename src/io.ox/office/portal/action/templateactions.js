/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import ext from '$/io.ox/core/extensions';
import Collection from '$/io.ox/core/collection';
import FilesApi from '$/io.ox/files/api';
import { Action as createAction, invoke } from '$/io.ox/backbone/views/actions/util';

import { IOS_SAFARI_DEVICE, createIcon } from "@/io.ox/office/tk/dom";
import { FILTER_MODULE_NAME } from '@/io.ox/office/tk/utils/io';
import { canOpenDocsTab, openEditorChildTab } from '@/io.ox/office/tk/utils/tabutils';
import { getFile, getFileUrl, getStandardDocumentsFolderId, getSanitizedFileName } from '@/io.ox/office/tk/utils/driveutils';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { GUARD_AVAILABLE } from '@/io.ox/office/baseframework/utils/baseconfig';
import { AppType, trackActionEvent } from '@/io.ox/office/baseframework/utils/apputils';
import { getAppType } from '@/io.ox/office/baseframework/app/extensionregistry';
import { launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';

import { DEFAULT_FILE_BASE_NAME } from '@/io.ox/office/editframework/view/editlabels';

import { refreshRecentFileAndTemplateList } from '@/io.ox/office/portal/action/portalsearchaction';
import { canShowInDrive, createDocumentIcon,
    getNewEncryptedLaunchOptions, getNewLaunchOptions, iosContextEventHandler, keydownHandler,
    removeFileExtension, showInDrive } from '@/io.ox/office/portal/portalutils';
import { PortalContextMenu } from '@/io.ox/office/portal/view/contextmenu';

// constants ==================================================================

// extension point id
const templateExtension = 'io.ox/portal/office/templates';

// private functions ==========================================================

/**
 * @param {JQuery} image node
 *
 * @param {String} src
 *
 */
var loadImageInQueue = (function () {
    var queue = [];
    var loadedURLs = {};
    var max = 2;

    function err(/*error*/) {
        var img = $(this);
        //globalLogger.error(new Date().toLocaleTimeString(), 'error loading preview for "' + img.attr('name') + '"', error);
        img.data('state', 'done');
        img.off('load', loaded);
        img.off('error', err);
        loadedURLs[img.attr('src')] = true;
        checkQueue();
    }

    function loaded() {
        var img = $(this);
        img.off('load', loaded);
        img.off('error', err);
        img.data('src', null);
        img.data('state', 'done');
        loadedURLs[img.attr('src')] = true;
        checkQueue();
    }

    function checkQueue() {
        var currentLoading = max;
        var toRemove = [];
        _.each(queue, function inLoop(entry) {
            var state = entry.data('state');
            if (state === 'loading') {
                currentLoading--;
            } else if (state === 'done') {
                toRemove.push(entry);
            }
        });
        while (toRemove.length > 0) {
            var index = queue.indexOf(toRemove.shift());
            queue.splice(index, 1);
        }
        if (currentLoading > 0) {
            for (var i = 0; i < queue.length; i++) {
                var entry = queue[i];
                if (entry.data('state') === 'inQueue') {
                    currentLoading--;

                    bindListener(entry);

                    if (currentLoading <= 0) {
                        break;
                    }
                }
            }
        }
    }

    function bindListener(img) {
        img.on('load', loaded);
        img.on('error', err);
        img.attr('src', img.data('src'));
        img.data('state', 'loading');
    }

    return function loadImageInQueueInner(img, src) {
        if (loadedURLs[src]) {
            img.attr('src', src);
        } else {
            img.data('src', src);
            img.data('state', 'inQueue');
            queue.push(img);

            checkQueue();
        }
    };
}());

function TemplateExtensions() {

    const contextMenu = new PortalContextMenu();

    /**
     * Returns the URL to a template file using the passed file descriptor.
     *
     * @param {Object} data
     *  The file descriptor object.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.global=false]
     *      If set to true, the URL for a global template file will be
     *      created, otherwise for a user template.
     *
     * @returns {String}
     *  The URL for the template.
     */
    var createTemplateURL = function (data, options) {

        // the constructed url
        const url = options?.global ? `${ox.abs}${ox.apiRoot}/${FILTER_MODULE_NAME}?action=templatepreview&id=${data.id}&folder=${data.folder_id}&format=preview_image&delivery=view` : getFileUrl(data, 'preview');

        // Thumbnails have a different size for presentation and text/spreadsheet.
        // In presentation the standard is 16:9 (180px : 100px). When the ratio is higher, the thumbnail
        // width is 180px but the hight is smaller. When the ratio is smaller, the thumbnail
        // height is 100px, but the width is rendered smaller. In text/spreadsheet we just a have a width of
        // 120px (legacy behaviour).
        const thumbSize = (data.type === 'presentation') ? 'height=100&width=180' : 'width=120';

        return `${url}&${thumbSize}&scaleType=contain&dummy=${data.last_modified}`;
    };

    /**
     * Draws the item to create a new document from a global or a user
     * template file.
     *
     * @param {Object} baton
     *  The file descriptor.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.global=false]
     *      If set to true, the URL for a global template file will be
     *      created, otherwise for a user template.
     */
    function drawItem(baton, options) {

        // This block sets the data to get a different layout
        // in the presentation and text/spreadsheet portal.

        // application type for the currently used portal
        var appType = baton.data.type;
        var fullFileName = getSanitizedFileName(baton.data);
        // the template container
        var templateItem = $('<a class="template-item" tabindex="0">').addClass(appType).attr({
            'data-toggle': 'tooltip',
            'data-animation': false,
            'data-delay': '{ "show": 1400 }',
            title: _.noI18n(fullFileName)
        });
        var previewBox = $('<div class="preview-box">');
        var displayName = _.noI18n(removeFileExtension(fullFileName));
        var nameBox = $('<p class="template-name">').text(displayName);
        var previewImg = $('<img>').attr('name', displayName);
        var iconNode = createDocumentIcon(fullFileName, 16);

        // show the item in "blank document" style while loading the image
        previewBox.addClass('new-blank').append(iconNode);
        previewImg.css('visibility', 'hidden');
        // shown delayed to prevent flickering icons when the images are loaded very fast (e.g. from cache)
        iconNode.hide().delay(200).fadeIn(100);

        previewImg.on('load', function () {
            previewBox.removeClass('new-blank');
            previewImg.css('visibility', 'visible');
            iconNode.remove();
        });
        previewImg.on('error', function () {
            previewImg.remove();
        });

        loadImageInQueue(previewImg, createTemplateURL(baton.data, options));

        // attach the preview img to the template
        previewBox.append(previewImg);

        // add new preview to the template container
        templateItem.append(previewBox, nameBox);

        // Get file descriptor of the newly created file as result, and open it with an office app.
        templateItem.click(function () {

            // special case for templates, which has 'template:local' as folder_id:
            // we must create a collection for the bation,
            // otherwise a request will be made by invoke to create one with
            // 'template:local' as id, which creates an error
            baton.collection = new Collection(baton.array());
            invoke(templateExtension + '/action/open', baton);
        });

        templateItem.on('keydown', { grid: true }, keydownHandler);

        // handle context menu
        templateItem.on('contextmenu', function (e) {
            contextMenuHandler(e, baton, templateItem);
        });

        // prevent focus change on click due to mousedown
        templateItem.on('mousedown', function (e) {
            e.preventDefault();
        });

        // iOS: check if there is a long tap to invoke a 'contextmenu' event
        if (IOS_SAFARI_DEVICE) {
            templateItem.on('touchstart', function (event) {
                iosContextEventHandler(event, templateItem);
            });
        }

        this.append(templateItem);
    }

    function drawGlobalItem(baton) {
        return drawItem.call(this, baton, { global: true });
    }

    function drawLocalItem(baton) {
        return drawItem.call(this, baton);
    }

    /**
     * A Handler function to generate the context menu for templates in the
     * text/spreadsheet portal.
     *
     * @param {Event} event
     *  The event which triggered the context menu.
     *  The event contains the position were the context menu is opened.
     *
     * @param {Object} baton
     *  The baton for the element the context menu is opened for.
     *
     * @param {JQuery} templateItem
     *  The selected template item element.
     *
     * @param {String} blankInvokeId
     *  The invokeId for a new blank document.
     *
     * @param {String} blankTitle
     *  The context menu entry title for a blank document.
     *
     * @param {String} blankInvokeIdEncrypted
     *  The invokeId for a new blank encrypted document.
     *
     * @param {String} blankTitleEncrypted
     *  The context menu entry title for a blank encrypted document.
     *
     */
    function contextMenuHandler(event, baton, templateItem, blankInvokeId, blankTitle, blankInvokeIdEncrypted, blankTitleEncrypted) {

        event.stopPropagation();
        event.preventDefault();

        // mark the selected element
        templateItem.addClass("context-open");

        // clear old entries in the context menu
        contextMenu.clearOptions();

        function createStandardButtons(id, title) {
            contextMenu.addSection(id, title);
            contextMenu.addAction('newfromtemplate', gt('New from template'), function () {
                invoke(templateExtension + '/action/open', baton.data);
            });
            if (GUARD_AVAILABLE) {
                contextMenu.addAction('newfromtemplateencrypted', gt('New from template (encrypted)'), function () {
                    invoke(templateExtension + '/action/open-encrypted', baton.data);
                });
            }
        }

        function createExtendedButtons() {
            let batonPromise = Promise.resolve();
            if (!baton.collection) {
                ox.busy();
                baton.collection = new Collection(baton.data);
                batonPromise = baton.collection.getProperties();
            }

            return batonPromise.then(() => {
                if (baton.collection.has('modify')) {
                    contextMenu.addAction('edittemplate', gt('Edit template'), function () {
                        var appType = getAppType(baton.data.filename);
                        invoke(appType + '-edit-template', baton.data);
                    });
                }
                if (baton.collection.has('delete')) {
                    contextMenu.addAction('deletetemplate', gt('Delete template'), function () {
                        invoke(templateExtension + '/delete/template', baton.data).then(function () {
                            var appType = getAppType(baton.data.filename);
                            refreshRecentFileAndTemplateList(appType); // updating the search list
                        });
                    });
                }
                if (canShowInDrive()) {
                    contextMenu.addAction('showindrive', gt('Show in Drive'), function (evt) {
                        evt.preventDefault(); // ?? TODO
                        showInDrive(baton.data, true);
                    });
                }
                ox.idle();
            });
        }

        let contextMenuPromise = Promise.resolve();
        switch (baton.data.source) {

            // system template (note: it's called global in the baton)
            case 'global':
                createStandardButtons(baton.data.source, gt('System template'));
                break;

            // global template set by the admin
            case 'admin':
                createStandardButtons(baton.data.source, gt('Global template'));
                contextMenuPromise = createExtendedButtons();
                break;

            // init the GUI - case: user template
            case 'user':
                createStandardButtons(baton.data.source, gt('User template'));
                contextMenuPromise = createExtendedButtons();
                break;
        }

        if (blankInvokeId) {
            contextMenu.addAction('newblank', blankTitle, function () {
                invoke(blankInvokeId, baton);
            });
        }

        if (blankInvokeIdEncrypted && GUARD_AVAILABLE) {
            contextMenu.addAction('newblankencrypted', blankTitleEncrypted, function () {
                invoke(blankInvokeIdEncrypted, baton);
            });
        }

        // remove the highlight
        contextMenu.one('popup:hide', () => templateItem.removeClass("context-open"));

        // set the click position as anchor
        contextMenu.setAnchor({ left: event.pageX, top: event.pageY });

        // show the context menu
        contextMenuPromise.then(() => {
            contextMenu.show();
        });
    }

    /**
     * Draws a list item for a blank template.
     * The templates are displayed differently in presentation and text/spreadsheet portal.
     * In presentation the newBlank template is in 16:9 and for text/spreadsheet it's 4:3.
     */
    function drawBlankTemplate($parent, baton, appType, title, actionTitle, actionTitleEncrypted) {

        // This block sets the data to get a different layout
        // in the presentation and text/spreadsheet portal.

        // the template container (class "blank-doc" expected and used by Portal Welcome Tour!)
        var templateItem = $('<a class="template-item blank-doc">').addClass(baton.appType).attr({
            'data-toggle': 'tooltip',
            'data-animation': 'false',
            'data-delay': JSON.stringify({ show: 1400 }),
            title: _.noI18n(title),
            tabindex: 0
        });

        var previewBox = $('<div class="preview-box new-blank">').append(createIcon('svg:add', 32));
        var nameBox = $('<p class="template-name">').text(_.noI18n(title));

        const invokeId = `${templateExtension}/action/new/${appType}`;
        const invokeIdEncrypted = `${templateExtension}/action/newencrypted/${appType}`;

        // add new blank template to the template container
        templateItem.append(previewBox, nameBox);

        // provide feedback to the user which element will be opened
        templateItem.click(function () {
            invoke(invokeId, baton);
        });

        templateItem.on('keydown', { grid: true }, keydownHandler);

        // prevent native context menu for the getBlank template and the focus change on click due to mousedown
        templateItem.on('contextmenu', function (e) {
            contextMenuHandler(e, baton, templateItem, invokeId, actionTitle, invokeIdEncrypted, actionTitleEncrypted);
        });

        // prevent focus change on click due to mousedown
        templateItem.on('mousedown', function (e) {
            e.preventDefault();
        });

        // iOS: check if there is a long tap to invoke a 'contextmenu' event
        // it's also needed here to get the same interaction behaviour on touchend
        // for the newBlank template
        if (IOS_SAFARI_DEVICE) {
            templateItem.on('touchstart', function (event) {
                iosContextEventHandler(event, templateItem);
            });
        }

        $parent.append(templateItem);
    }

    // create extension points
    this.text = ext.point(templateExtension + '/text');
    this.textuser = ext.point(templateExtension + '/text/templateuser');
    this.textglobal = ext.point(templateExtension + '/text/templateglobal');
    this.spreadsheet = ext.point(templateExtension + '/spreadsheet');
    this.spreadsheetuser = ext.point(templateExtension + '/spreadsheet/templateuser');
    this.spreadsheetglobal = ext.point(templateExtension + '/spreadsheet/templateglobal');
    this.presentation = ext.point(templateExtension + '/presentation');
    this.presentationuser = ext.point(templateExtension + '/presentation/templateuser');
    this.presentationglobal = ext.point(templateExtension + '/presentation/templateglobal');

    // actions to create a blank document
    ['text', 'spreadsheet', 'presentation'].forEach(function (appType) {
        createAction(templateExtension + '/action/new/' + appType, {
            action() {
                if (canOpenDocsTab()) {
                    var params = {
                        target_folder_id: getStandardDocumentsFolderId(),
                        target_filename: DEFAULT_FILE_BASE_NAME,
                        new: true
                    };
                    openEditorChildTab(appType, params);
                    return;
                }

                trackActionEvent(appType, "templates", "newblank");

                return launchDocsApp(appType, getNewLaunchOptions());
            }
        });
    });

    // action to create from an existing template
    createAction(templateExtension + '/action/open', {
        action(baton) {

            var fileDesc = baton.first();
            var appType = getAppType(fileDesc.filename);
            var standardDocFolderId = getStandardDocumentsFolderId();

            if (canOpenDocsTab()) {
                var params = {
                    folder: fileDesc.folder_id,
                    id: fileDesc.id,
                    destfolderid: standardDocFolderId,
                    destfilename: DEFAULT_FILE_BASE_NAME,
                    convert: true
                };
                openEditorChildTab(appType, params);
                return;
            }

            let launchPromise = fileDesc.loadDeferred;
            if (launchPromise && launchPromise.state() === 'pending') {
                globalLogger.warn('cant run convert document, because old call is still pending');
                return;
            }

            trackActionEvent(appType, "templates", "newfromtemplate");

            launchPromise = launchDocsApp(appType, {
                action: 'convert',
                template: true,
                target_folder_id: standardDocFolderId,
                templateFile: fileDesc,
                target_filename: DEFAULT_FILE_BASE_NAME
            });

            fileDesc.loadDeferred = launchPromise;
            return launchPromise;
        }
    });

    // action to create from an existing template encrypted
    createAction(templateExtension + '/action/open-encrypted', {
        action(baton) {

            var fileDesc = baton.first();

            let launchPromise = fileDesc.loadDeferred;
            if (launchPromise && launchPromise.state() === 'pending') {
                globalLogger.warn('cant run convert document, because old call is still pending');
                return;
            }

            launchPromise = getNewEncryptedLaunchOptions();

            // set authorization data to the model
            fileDesc.loadDeferred = launchPromise.then(function (launchOptions) {

                const appType = getAppType(fileDesc.filename);
                trackActionEvent(appType, "templates", "newfromtemplate");

                _.extend(launchOptions, { action: 'convert', template: true, templateFile: fileDesc, target_filename: DEFAULT_FILE_BASE_NAME });
                return launchDocsApp(appType, launchOptions);
            });
            return launchPromise;
        }
    });

    createAction(templateExtension + '/delete/template', {
        collection: 'one && delete',
        matches(baton) {
            return baton.first().filename && baton.collection.has('one', 'delete');
        },
        quick(baton) {
            return baton.first().filename;
        },
        action(baton) {
            getFile(baton.first()).then(function (file) {
                invoke('io.ox/files/actions/delete', file);
            });
        }
    });

    FilesApi.on('remove:file', function () {
        ox.trigger('refresh-portal');
    });

    // define extension for creating text document with a blank template
    this.text.extend({
        id: 'text_template_blank',
        draw(baton) {
            drawBlankTemplate(this, baton, AppType.TEXT, gt('Blank text document'), gt('New text document'), gt('New text document (encrypted)'));
        }
    });

    // define extension for creating text documents with global templates
    this.textglobal.extend({
        id: 'text_global_template',
        draw: drawGlobalItem
    });

    // define extension for creating text documents with user templates
    this.textuser.extend({
        id: 'text_user_template',
        draw: drawLocalItem
    });

    // define extension for creating spreadsheet with a blank template
    this.spreadsheet.extend({
        id: 'spreadsheet_template_blank',
        draw(baton) {
            drawBlankTemplate(this, baton, AppType.SPREADSHEET, gt('Blank spreadsheet'), gt('New spreadsheet'), gt('New spreadsheet (encrypted)'));
        }
    });

    // define extension for creating spreadsheet with global templates
    this.spreadsheetglobal.extend({
        id: 'spreadsheet_global_template',
        draw: drawGlobalItem
    });

    // define extension for creating spreadsheet with user templates
    this.spreadsheetuser.extend({
        id: 'spreadsheet_user_template',
        draw: drawLocalItem
    });

    // define extension for creating presentation with a blank template
    this.presentation.extend({
        id: 'presentation_template_blank',
        draw(baton) {
            drawBlankTemplate(this, baton, AppType.PRESENTATION, gt('Blank presentation'), gt('New presentation'), gt('New presentation (encrypted)'));
        }
    });

    // define extension for creating presentation with global templates
    this.presentationglobal.extend({
        id: 'presentation_global_template',
        draw: drawGlobalItem
    });

    // define extension for creating presentation with user templates
    this.presentationuser.extend({
        id: 'presentation_user_template',
        draw: drawLocalItem
    });

    if (GUARD_AVAILABLE) {

        // action to create a blank text document encrypted
        createAction(templateExtension + '/action/newencrypted/text', {
            action() {
                var promise = getNewEncryptedLaunchOptions();

                // set authorization data to the model
                return promise.then(launchOptions => {
                    trackActionEvent(AppType.TEXT, "templates", "newblank");
                    return launchDocsApp(AppType.TEXT, launchOptions);
                });
            }
        });

        // action to create a blank spreadsheet document
        createAction(templateExtension + '/action/newencrypted/spreadsheet', {
            action() {
                var promise = getNewEncryptedLaunchOptions();

                // set authorization data to the model
                return promise.then(launchOptions => {
                    trackActionEvent(AppType.SPREADSHEET, "templates", "newblank");
                    return launchDocsApp(AppType.SPREADSHEET, launchOptions);
                });
            }
        });

        createAction(templateExtension + '/action/newencrypted/presentation', {
            action() {
                var promise = getNewEncryptedLaunchOptions();

                // set authorization data to the model
                return promise.then(launchOptions => {
                    trackActionEvent(AppType.PRESENTATION, "templates", "newblank");
                    return launchDocsApp(AppType.PRESENTATION, launchOptions);
                });
            }
        });
    }
}

export default new TemplateExtensions();
