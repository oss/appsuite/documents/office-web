/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import ext from '$/io.ox/core/extensions';
import { Action as createAction, invoke } from '$/io.ox/backbone/views/actions/util';
import Collection from '$/io.ox/core/collection';

import '$/io.ox/files/toolbar'; // important for action links (download in contextmenu)
import '$/io.ox/mail/detail/links'; // this line is important for deep links usage (works with jq delegation)

import { COMPACT_DEVICE, IOS_SAFARI_DEVICE, createIcon } from '@/io.ox/office/tk/dom';
import { FILTER_MODULE_NAME, sendRequest } from '@/io.ox/office/tk/utils/io';
import { canOpenDocsTab, openEditorChildTab } from '@/io.ox/office/tk/utils/tabutils';
import { getSanitizedFileName, hasGuardExt } from '@/io.ox/office/tk/utils/driveutils';

import { trackActionEvent } from '@/io.ox/office/baseframework/utils/apputils';
import { DOCUMENTS_COLLABORA, PRESENTER_AVAILABLE } from '@/io.ox/office/baseframework/utils/baseconfig';
import { getAppType } from '@/io.ox/office/baseframework/app/extensionregistry';
import { launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';

import { canShowInDrive, createDocumentIcon, escapeHTML, formatDate,
    iosContextEventHandler, keydownHandler, showInDrive, showInPresenter } from '@/io.ox/office/portal/portalutils';
import { PortalContextMenu } from '@/io.ox/office/portal/view/contextmenu';
import { refreshRecentFileAndTemplateList } from '@/io.ox/office/portal/action/portalsearchaction';

// constants ==================================================================

const recentDocumentExtension = 'io.ox/portal/office/recents/document';

// class RecentsExtensions ====================================================

class RecentsExtensions {

    constructor() {

        this.documentPoint = ext.point(recentDocumentExtension);

        const contextMenu = new PortalContextMenu();

        createAction(recentDocumentExtension + '/open', {

            action(baton) {

                var fileDesc = baton.first();
                const appType = getAppType(fileDesc.filename);
                if (!appType) { return; } // TODO: error message?

                trackActionEvent(appType, "recentlist", "edit");

                launchDocsApp(appType, { action: 'load', file: fileDesc });

                //FIXME: TODO: combine me with normal file-actions
            }
        });

        this.documentPoint.extend({

            id: 'recent_document',

            draw(baton) {

                const fullFileName = getSanitizedFileName(baton.data);
                const appType = getAppType(fullFileName);

                // the bare folder names, as array
                const folderNames = baton.data.path.map(folder => folder.filename);

                // build the breadcrump file path
                const divPath = $('<div class="document-path">');
                folderNames.forEach((folderName, index) => {
                    if (index > 0) { divPath.append(createIcon("bi:chevron-right", "divider")); }
                    divPath.append($("<span>").text(_.noI18n(folderName)));
                });

                // build the path tooltip (iPad title problem: long tap on element -> ios popup appears -> HTML markup is visible)
                let tooltipMarkup = escapeHTML(_.noI18n(fullFileName));
                if (!COMPACT_DEVICE) {
                    tooltipMarkup += '<div class="tooltip-filepath">' + escapeHTML(_.noI18n(folderNames.join(" > "))) + '</div>';
                }

                // build the HTML output. Important: the tooltip displays HTML,
                // so all used values must be escaped
                var link = $('<a>').addClass('document-link row')
                    .attr({
                        'data-toggle': 'tooltip',
                        'data-html': 'true',
                        'data-animation': 'false',
                        'data-delay': '{ "show": 1400 }',
                        'data-viewport': '{ "selector": "body", "padding": 43}',
                        title: tooltipMarkup,
                        tabindex: 0
                    }),
                    divMetadata = $('<div class="document-metadata">'),
                    divName = $('<div class="document-name col-xs-9">'),
                    divFilename = $('<div class="document-filename">').text(_.noI18n(fullFileName)),
                    divDate = $('<div class="document-date col-xs-3">').text(_.noI18n(formatDate(baton.data.last_modified || baton.data.last_opened)));

                divName.append(createDocumentIcon(fullFileName), divFilename, divPath);

                // check if the document is encrypted add add a overlay icon if true
                if (hasGuardExt(fullFileName)) {
                    divDate.append(createIcon('bi:lock-fill', 'encryption-overlay ' + appType));
                }

                link.append(divMetadata.append(divName, divDate));

                link.click(function () {

                    if (canOpenDocsTab()) {
                        var params = {
                            folder: baton.data.id
                        };
                        if (baton.data.folder_id !== undefined) {
                            _.extend(params, {
                                folder: baton.data.folder_id,
                                id: baton.data.id
                            });
                        }
                        openEditorChildTab(appType, params);
                        return;
                    }

                    invoke(recentDocumentExtension + '/open', baton);
                });

                link.on('keydown', keydownHandler);

                // handle context menu
                link.on('contextmenu', function (e) {
                    contextMenuHandler(e, link, baton);
                });

                // prevent focus change on click due to mousedown
                link.on('mousedown', function (e) {
                    e.preventDefault();
                });

                // iOS: check if there is a long tap to invoke a 'contextmenu' event
                if (IOS_SAFARI_DEVICE) {
                    link.on('touchstart', function (event) {
                        iosContextEventHandler(event, link);
                    });
                }

                this.append(link);
            }
        });

        /*
            * A Handler function to generate the context menu for recent list
            * items in the text/spreadsheet portal.
            *
            * @param {Event} event
            *  The event which triggered the context menu. The event contains the
            *  position were the context menu is opened.
            *
            * @param {JQuery} link
            *  The DOM element the context menu is opened for.
            *
            * @param {Object} baton
            *  The baton for the element the context menu is opened for.
            */
        function contextMenuHandler(event, link, baton) {

            event.stopPropagation();
            event.preventDefault();

            link.addClass("context-open");

            // clear old entries in the context menu
            contextMenu.clearOptions();

            // current app name
            const appType = getAppType(baton.data.filename);

            var encrypted = hasGuardExt(baton.data.filename);

            let batonPromise = Promise.resolve();
            if (!baton.collection) {
                ox.busy();
                baton.collection = new Collection(baton.data);
                batonPromise = baton.collection.getProperties();
            }

            // create buttons and sections for the context menu
            batonPromise.then(() => {
                if (baton.collection.has('modify')) {
                    contextMenu.addAction('edit', gt('Edit'), function () {
                        contextMenu.hide(); //hide the context menu after a button was clicked
                        invoke(recentDocumentExtension + '/open', baton);
                    });
                }
                if (!encrypted) {
                    contextMenu.addAction('editasnew', gt('Edit as new'), function () {
                        invoke(appType + '-edit-asnew', baton); //action from filesaction.js
                    });
                }

                // if the 'DownloadButton' should be visible: don't show the download button in iOS, because a download is not possible in iOS
                if (!IOS_SAFARI_DEVICE && !encrypted) {
                    contextMenu.addAction('download', gt('Download'), function () {
                        invoke('io.ox/files/actions/download', baton);
                    });
                }

                if (canShowInDrive()) {
                    contextMenu.addAction('showindrive', gt('Show in Drive'), function () {
                        showInDrive(baton.data, true);
                    });
                }

                // only show the 'Present' button in the Presentation portal and when the Presenter is available
                if (!encrypted && (appType === 'presentation') && PRESENTER_AVAILABLE && !DOCUMENTS_COLLABORA) {
                    contextMenu.addSection('presentation');
                    contextMenu.addAction('present', /*#. context: present the item in the Presenter */ gt('Present'), function () {
                        showInPresenter(baton, true);
                    });
                }

                contextMenu.addSection('recentlist'); // a new section

                contextMenu.addAction('removefromrecentlist', /*#. context: remove the selected item from the 'recent documents' list in app portal */ gt('Remove from list'), function () {
                    sendRequest(FILTER_MODULE_NAME, { action: 'deletefilefromrecentlist', type: appType, remove_file_id: baton.data.id }).done(function () {
                        // Remove the item from the backbone collection which represents the recent list.
                        // This will fire an event ('update') which will re-render the recent list.
                        baton.model.collection.remove(baton.model);
                        // updating the search list
                        refreshRecentFileAndTemplateList(appType);
                    });
                });

                contextMenu.addAction('clearrecentlist', /*#. context: clear all items from the 'recent documents' list in app portal */ gt('Clear list'), function () {
                    sendRequest(FILTER_MODULE_NAME, { action: 'clearrecentlist', type: appType }).done(function () {
                        // Remove all items from the backbone collection which represents the recent list.
                        // This will fire an event ('reset') which will re-render the recent list.
                        baton.model.collection.reset(null);
                        // updating the search list
                        refreshRecentFileAndTemplateList(appType);
                    });
                });

                // remove the highlight
                contextMenu.one('popup:hide', () => link.removeClass("context-open"));

                // set the click position as anchor
                contextMenu.setAnchor({ left: event.pageX, top: event.pageY });

                // show the context menu
                contextMenu.show();
                ox.idle();
            });
        }
    }
}

export default new RecentsExtensions();
