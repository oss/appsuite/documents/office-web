/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ext from '$/io.ox/core/extensions';

import { GUARD_AVAILABLE } from '@/io.ox/office/baseframework/utils/baseconfig';
import { TEXT_DOCUMENT_NAME_ENCRYPTED_NEW, TEXT_DOCUMENT_NAME_NEW,
    SPREADSHEET_DOCUMENT_NAME_ENCRYPTED_NEW, SPREADSHEET_DOCUMENT_NAME_NEW,
    PRESENTATION_DOCUMENT_NAME_ENCRYPTED_NEW, PRESENTATION_DOCUMENT_NAME_NEW,
    OPEN_DOCUMENT_LABEL, ADD_TEMPLATE_LABEL
} from '@/io.ox/office/baseframework/view/baselabels';

import '@/io.ox/office/portal/action/portalactions'; // loading actions

// constants ==================================================================

const APPTYPE_BUTTON_TEXT = {
    text: TEXT_DOCUMENT_NAME_NEW,
    spreadsheet: SPREADSHEET_DOCUMENT_NAME_NEW,
    presentation: PRESENTATION_DOCUMENT_NAME_NEW
};

const APPTYPE_BUTTON_TEXT_ENCRYPTED = {
    text: TEXT_DOCUMENT_NAME_ENCRYPTED_NEW,
    spreadsheet: SPREADSHEET_DOCUMENT_NAME_ENCRYPTED_NEW,
    presentation: PRESENTATION_DOCUMENT_NAME_ENCRYPTED_NEW
};

const DOCUMENTS_APPS = {
    text: 1,
    spreadsheet: 1,
    presentation: 1
};

const SWITCH_SECONDARY_IDS = ['newContact', 'newTask'];

/**
 * Updating whether secondary extension point shall be shown (enable) or not (disable).
 * When the documents portal is shown, some extension points shall not be shown. When
 * the portal is no longer active, these extensions points must be enabled again.
 *
 * @param {string} type
 *  When the documents portal is shown, the type parameter must be "show". When the
 * documents portal is no longer active, the type parameter must be "hide".
 */
export function updateSecondaryExtensionPoints(type) {

    const secondaryExtPoint = ext.point('io.ox/secondary');

    if (type === 'show') {
        // when the portal is shown, not adding the extension points for contacts and
        // tasks to the primary button (DOCS-3675)
        SWITCH_SECONDARY_IDS.forEach(function (id) { secondaryExtPoint.disable(id); });
    } else if (type === 'hide') {
        // but the extension points must be enabled again, when the portal is no longer shown
        SWITCH_SECONDARY_IDS.forEach(function (id) { secondaryExtPoint.enable(id); });
    }
}

/**
 * Creates all extensions points and bind actions for the portal primary button.
 */
export function registerSecondaryPortalActions() {

    /**
     * Generating the secondary extension points are used by the primary button (dropdown).
     *
     * Important: Every action has to be registered for every application. Which
     *            secondary button is shown, is then only dependent from the 'if'
     *            statement in the render-function of the extension point.
     *
     * @returns {Object[]}
     *  An array with extension points, attached to the primary button.
     */
    function getSecondaryExtensions() {

        return [

            // open document
            {
                id: `open-text-document`,
                index: 150,
                prio: 'hi',
                render(baton) {
                    if (baton.app.attributes.appType !== 'text') { return; }
                    this.action(`io.ox/office/portal/opentext`, OPEN_DOCUMENT_LABEL, baton);
                }
            },
            {
                id: `open-spreadsheet-document`,
                index: 150,
                prio: 'hi',
                render(baton) {
                    if (baton.app.attributes.appType !== 'spreadsheet') { return; }
                    this.action(`io.ox/office/portal/openspreadsheet`, OPEN_DOCUMENT_LABEL, baton);
                }
            },
            {
                id: `open-presentation-document`,
                index: 150,
                prio: 'hi',
                render(baton) {
                    if (baton.app.attributes.appType !== 'presentation') { return; }
                    this.action(`io.ox/office/portal/openpresentation`, OPEN_DOCUMENT_LABEL, baton);
                }
            },

            // add template
            {
                id: `add-template-text`,
                index: 200,
                prio: 'hi',
                render(baton) {
                    if (baton.app.attributes.appType !== 'text') { return; }
                    this.action(`io.ox/office/portal/add/template/text`, ADD_TEMPLATE_LABEL, baton);
                    this.divider();
                }
            },
            {
                id: `add-template-spreadsheet`,
                index: 200,
                prio: 'hi',
                render(baton) {
                    if (baton.app.attributes.appType !== 'spreadsheet') { return; }
                    this.action(`io.ox/office/portal/add/template/spreadsheet`, ADD_TEMPLATE_LABEL, baton);
                    this.divider();
                }
            },
            {
                id: `add-template-presentation`,
                index: 200,
                prio: 'hi',
                render(baton) {
                    if (baton.app.attributes.appType !== 'presentation') { return; }
                    this.action(`io.ox/office/portal/add/template/presentation`, ADD_TEMPLATE_LABEL, baton);
                    this.divider();
                }
            },

            // new text
            {
                id: `new-text-in-portal`,
                index: 250,
                prio: 'hi',
                render(baton) {
                    if (!DOCUMENTS_APPS[baton.app.attributes.appType]) { return; }
                    this.action(`io.ox/office/portal/text/actions/new/text`, APPTYPE_BUTTON_TEXT.text, baton);
                }
            },

            // new text encrypted
            {
                id: `new-text-encrypted-in-portal`,
                index: 300,
                prio: 'hi',
                render(baton) {
                    if (!DOCUMENTS_APPS[baton.app.attributes.appType] || !GUARD_AVAILABLE) { return; }
                    this.action(`io.ox/office/portal/text/actions/newencrypted/text`, APPTYPE_BUTTON_TEXT_ENCRYPTED.text, baton);
                }
            },

            // new spreadsheet
            {
                id: `new-spreadsheet-in-portal`,
                index: 350,
                prio: 'hi',
                render(baton) {
                    if (!DOCUMENTS_APPS[baton.app.attributes.appType]) { return; }
                    this.action(`io.ox/office/portal/spreadsheet/actions/new/spreadsheet`, APPTYPE_BUTTON_TEXT.spreadsheet, baton);
                }
            },

            // new spreadsheet encrypted
            {
                id: `new-spreadsheet-encrypted-in-portal`,
                index: 400,
                prio: 'hi',
                render(baton) {
                    if (!DOCUMENTS_APPS[baton.app.attributes.appType] || !GUARD_AVAILABLE) { return; }
                    this.action(`io.ox/office/portal/spreadsheet/actions/newencrypted/spreadsheet`, APPTYPE_BUTTON_TEXT_ENCRYPTED.spreadsheet, baton);
                }
            },

            // new presentation
            {
                id: `new-presentation-in-portal`,
                index: 450,
                prio: 'hi',
                render(baton) {
                    if (!DOCUMENTS_APPS[baton.app.attributes.appType]) { return; }
                    this.action(`io.ox/office/portal/presentation/actions/new/presentation`, APPTYPE_BUTTON_TEXT.presentation, baton);
                    if (!GUARD_AVAILABLE) { this.divider(); }
                }
            },

            // new presentation encrypted
            {
                id: `new-presentation-encrypted-in-portal`,
                index: 500,
                prio: 'hi',
                render(baton) {
                    if (!DOCUMENTS_APPS[baton.app.attributes.appType] || !GUARD_AVAILABLE) { return; }
                    this.action(`io.ox/office/portal/presentation/actions/newencrypted/presentation`, APPTYPE_BUTTON_TEXT_ENCRYPTED.presentation, baton);
                    this.divider();
                }
            }
        ];
    }

    // appending all extensions to the primary button in the side panel -> they appear in the dropdown
    const secondaryExtPoint = ext.point('io.ox/secondary');

    secondaryExtPoint.extend(getSecondaryExtensions());

    // not showing all registered secondary extension points
    updateSecondaryExtensionPoints('show');
}
