/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import $ from '$/jquery';
import ox from '$/ox';

import ext from '$/io.ox/core/extensions';
import FolderApi from '$/io.ox/core/folder/api';
import { Action as createAction, invoke } from '$/io.ox/backbone/views/actions/util';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getStandardTemplateFolderId } from '@/io.ox/office/tk/utils/driveutils';

import { trackActionEvent } from '@/io.ox/office/baseframework/utils/apputils';
import { launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';

import { filterTemplateFiles, getNewEncryptedLaunchOptions, getNewLaunchOptions } from '@/io.ox/office/portal/portalutils';
import { getTemplateExtensions } from '@/io.ox/office/baseframework/app/extensionregistry';

// prevent multiple async calls of new/load document
let pendingLaunchNewDocPromise = null;

/**
 * Launches a Documents editor application instance for a new document.
 *
 * @param {String} localAppType
 *  The appType of the app for that the portal app is generated. This
 *  can be 'text', 'spreadsheet' or 'presentation'.
 *
 * @param {() => MaybeAsync<LaunchOptions>} lauchOptionsResolver
 *  A callback function that returns or fulfils with the launch options
 *  for the new document.
 *
 * @returns {JPromise}
 *  A promise that fulfils when the new Document editor application has
 *  been launched.
 */
function launchNewDocument(localAppType, lauchOptionsResolver) {

    if (pendingLaunchNewDocPromise) {
        globalLogger.warn('cannot create multiple new documents at the same time');
        return $.Deferred().reject();
    }

    pendingLaunchNewDocPromise = jpromise.invoke(lauchOptionsResolver).then(function (launchOptions) {
        trackActionEvent(localAppType, "toolbar", "newblank");
        return launchDocsApp(localAppType, launchOptions);
    });

    return pendingLaunchNewDocPromise.always(function () {
        pendingLaunchNewDocPromise = null;
    });
}

/**
 * Helper function that is triggered, when the user adds a new template
 * in the Portal apps.
 */
function addTemplateAction(event) {

    FolderApi.get(getStandardTemplateFolderId()).then(function (data) {
        var baton = ext.Baton(data);
        baton.folder_id = baton.id;
        baton.e = event;
        baton.app = ox.ui.App.getCurrentApp();
        baton.filter = filterTemplateFiles;
        baton.acceptExt = getTemplateExtensions(ox.ui.App.getCurrentApp().get('appType'));
        invoke('io.ox/files/actions/upload', baton);
    });
}

// creating all required actions

createAction('io.ox/office/portal/text/actions/new/text', {
    capabilities: 'text',
    action() { launchNewDocument('text', getNewLaunchOptions); }
});

createAction('io.ox/office/portal/text/actions/newencrypted/text', {
    capabilities: 'guard guard-docs text',
    action() { launchNewDocument('text', getNewEncryptedLaunchOptions); }
});

createAction('io.ox/office/portal/spreadsheet/actions/new/spreadsheet', {
    capabilities: 'spreadsheet',
    action() { launchNewDocument('spreadsheet', getNewLaunchOptions); }
});

createAction('io.ox/office/portal/spreadsheet/actions/newencrypted/spreadsheet', {
    capabilities: 'guard guard-docs spreadsheet',
    action() { launchNewDocument('spreadsheet', getNewEncryptedLaunchOptions); }
});

createAction('io.ox/office/portal/presentation/actions/new/presentation', {
    capabilities: 'presentation',
    action() { launchNewDocument('presentation', getNewLaunchOptions); }
});

createAction('io.ox/office/portal/presentation/actions/newencrypted/presentation', {
    capabilities: 'guard guard-docs presentation',
    action() { launchNewDocument('presentation', getNewEncryptedLaunchOptions); }
});

createAction(`io.ox/office/portal/add/template/text`, {
    capabilities: 'text',
    action: addTemplateAction
});

createAction(`io.ox/office/portal/add/template/spreadsheet`, {
    capabilities: 'spreadsheet',
    action: addTemplateAction
});

createAction(`io.ox/office/portal/add/template/presentation`, {
    capabilities: 'presentation',
    action: addTemplateAction
});
