/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import $ from '$/jquery';
import gt from 'gettext';
import _ from '$/underscore';

import { invoke } from '$/io.ox/backbone/views/actions/util';

import ext from '$/io.ox/core/extensions';
import SearchView, { appendSearchView } from '$/io.ox/backbone/views/search';

import { createDocumentIcon } from '@/io.ox/office/portal/portalutils';
import { createIcon } from '@/io.ox/office/tk/dom';
import { hasGuardExt } from '@/io.ox/office/tk/utils/driveutils';
import { FILTER_MODULE_NAME, sendRequest } from '@/io.ox/office/tk/utils/io';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import "@/io.ox/office/portal/action/portalsearchaction.less";

// constants ==================================================================

// const SEARCH_PLACEHOLDER_TEXTS = {
//     text: gt('Search text documents'),
//     spreadsheet: gt('Search spreadsheets'),
//     presentation: gt('Search presentations')
// };

const SEARCH_PLACEHOLDER_TEXT = gt('Search recent documents');

const recentFileLists = {};
const templateFileLists = {};
const nodeCollector = {};

// private global functions ===================================================

/**
 * Clearing all suggestions inside the search list.
 */
function clearList(appType) {
    nodeCollector[appType]?.autocompleteList.empty();
}

/**
 * Creating the extension point for the portal search.
 *
 * @param {String} appType
 *  The portal application type. Supported values are "text", "spreadsheet" and "presentation".
 *
 * @returns {Boolean}
 *  Whether the search toolbar was integrated into the top bar of the portal. Typically this must always happen,
 *  because "initOXDocuments" in the Portal factory must only be called once for each documents application.
 *  So if this function returns false (together with a console warning) "initOXDocuments" was called a second time.
 */
export function registerPortalSearch(appType) {

    let searchNode = null;
    let autocompleteList = $();
    let inputField = $();
    let cancelButton = $();

    /**
     * The handler for opening a file from the suggestion list.
     *
     * @param {Object} file
     *  The file object
     *
     * @param {Boolean} isTemplate
     *  Whether the file object describes a file or a template
     */
    function openFileHandler(file, isTemplate) {

        clearList(appType);
        inputField.val('');
        cancelButton.toggle(false);

        // using the same actions that are already specified for the recents list
        // -> no special handling for encrypted documents required here

        const action = isTemplate ? 'io.ox/portal/office/templates/action/open' : 'io.ox/portal/office/recents/document/open';

        invoke(action, file);
    }

    /**
     * Creating one list item in the suggestion list for the user.
     *
     * @param {Object} file
     *  The file object
     *
     * @param {Boolean} isTemplate
     *  Whether the file object describes a file or a template
     *
     * @returns {jQuery}
     *  The jQueryfied link in the suggestion list.
     */
    function createOneListItem(file, isTemplate) {

        const templateIconName = 'svg:new-from-template';
        const fileName = file.filename;

        // build the HTML output. Important: the tooltip displays HTML,
        // so all used values must be escaped

        const listItem = $('<li>').addClass('list-item selectable');

        const link = $('<a>').addClass('document-link'),
            divMetadata = $('<div class="document-metadata">'),
            divName = $('<div class="document-name">'),
            divFilename = $('<div class="document-filename">').text(_.noI18n(fileName));

        const iconNode = isTemplate ? $(createIcon(templateIconName)).addClass('document-icon ' + appType) : createDocumentIcon(fileName);

        divName.append(iconNode, divFilename);

        // check if the document is encrypted add add a overlay icon if true
        if (hasGuardExt(fileName)) { divMetadata.append(createIcon('bi:lock-fill', 'encryption-overlay ' + appType)); }

        link.append(divMetadata.append(divName));

        link.click(function () { openFileHandler(file, isTemplate); });

        // saving data at list item (required when user presses "Enter")
        listItem.data('isTemplate', isTemplate);
        listItem.data('file', file);

        return listItem.append(link);
    }

    /**
     * The keydown handler for the search node.
     */
    function searchKeydownHandler(e) {
        let index, children;
        switch (e.which) {
            // enter
            case 13: {
                e.preventDefault(); // avoid reload of form element, parent of input field (task DOCS-4628, tests DOCS-3500C, DOCS-3500D)
                const selected = autocompleteList.children('.selected');
                if (selected.length) {
                    const file = selected.data('file');
                    const isTemplate = selected.data('isTemplate');
                    openFileHandler(file, isTemplate);
                }
                break;
            }
            // cursor up
            case 38:
                children = autocompleteList.children();
                index = children.index(children.filter('.selected'));
                if (index <= 0) { return; }
                children.filter('.selected').removeClass('selected');
                children.eq(index - 1).addClass('selected').intoView(autocompleteList);
                break;
            // cursor down
            case 40:
                children = autocompleteList.children();
                index = children.index(children.filter('.selected'));
                if (index >= children.length - 1) { return; }
                children.filter('.selected').removeClass('selected');
                children.eq(index + 1).addClass('selected').intoView(autocompleteList);
                break;
            // no default
        }

    }

    /**
     * The click handler for the cancel button in the search field. The cancel button has
     * to be hidden, after it was clicked.
     */
    function cancelButtonClickHandler() {
        cancelButton.toggle(false);
    }

    /**
     * Rendering the file list for a specified search string.
     *
     * @param {String} searchString
     *  The searchString to find the corresponding files.
     */
    function renderList(searchString) {

        let isTemplate = false;

        if (recentFileLists[appType]?.fileList) {
            recentFileLists[appType].fileList.forEach(function (file) {
                if (file.filename && file.filename.toLowerCase().startsWith(searchString.toLowerCase())) {
                    autocompleteList.append(createOneListItem(file, isTemplate));
                }
            });
        }

        isTemplate = true;

        if (templateFileLists[appType]?.templateList) {
            templateFileLists[appType].templateList.forEach(function (template) {
                if (template.filename && template.filename.toLowerCase().startsWith(searchString.toLowerCase())) {
                    autocompleteList.append(createOneListItem(template, isTemplate));
                }
            });
        }
    }

    /**
     * The handler for the 'input' event of the search input field.
     */
    function inputHandler() {
        const searchString = inputField.val();
        searchString.trim();
        // stopping the current rendering of the list. This is required for asynchronous rendering of search list
        clearList(appType);
        cancelButton.toggle(false);
        if (searchString.length === 0) { return; }
        renderList(searchString);
        cancelButton.toggle(true);
    }

    if ($(`#io-ox-documentsbar > .search-container[data-app-type="${appType}"]`).length) {
        // this should never be the case, because "initOXDocuments" in the Portal appfactory
        // must only be called once for each documents application
        globalLogger.warn(`search container for ${appType} application exists already`);
        return false;
    }

    // search bar
    ext.point(`io.ox/office/portal/${appType}/mediator`).extend({
        id: `top-search-portal-${appType}`,
        capabilities: appType,
        index: 100,
        setup(app) {

            const view = new SearchView({
                app,
                placeholder: SEARCH_PLACEHOLDER_TEXT,
                autocomplete: false
            });

            // different parent node in multitab mode
            const docsParentNode = document.getElementById("io-ox-documentsbar");
            if (docsParentNode) {
                docsParentNode.append(view.render().el);
            } else {
                appendSearchView({ app, view });
            }

            // saving the app type at the search node. This is important for activating the correct search node when closing
            // a document in multi tab mode (see tabtoolbar.js)
            view.$el.addClass('io-ox-office-portal-search').attr('data-app-type', appType);

            searchNode = view.$el;
            searchNode.off('input keydown'); // removing the existing 'input' handler because of its backbone model
            autocompleteList = searchNode.children('.autocomplete');
            cancelButton = searchNode.children('.cancel-button');
            inputField = searchNode.find('input.search-field'); // DOCS-4618: Input field is no direct child
            inputField.off('keydown'); // removing the existing 'keydown' handler because of its backbone model
            inputField.off('blur'); // DOCS-4414, not using generic blur handler from core to remove the list items

            inputField.on('input', inputHandler);
            searchNode.on('keydown', searchKeydownHandler);
            cancelButton.on('click', cancelButtonClickHandler);

            // collecting all application specific nodes
            nodeCollector[appType] = { searchNode, autocompleteList, cancelButton, inputField };

            // showing the new created searchview, required after OXUIB-2110 (DOCS-4643)
            searchNode.show();
        }
    });

    return true;
}

/**
 * Setting the list of the recent files.
 *
 * @param {Object[]} recentFileList
 *  The list of recent files that is used for the search.
 *
 * @param {Object[]} templateFileList
 *  The list of template files that is used for the search.
 *
 * @param {String} appType
 *  The portal application type. Supported values are "text", "spreadsheet" and "presentation".
 */
export function setRecentFileAndTemplateList(recentFileList, templateFileList, appType) {
    recentFileLists[appType] = recentFileLists[appType] || {};
    recentFileLists[appType].fileList = recentFileList;
    templateFileLists[appType] = templateFileLists[appType] || {};
    templateFileLists[appType].templateList = templateFileList;
}

/**
 * Refreshing the list of recent files and templates. This is required after removing
 * a file from the recent list or clearing the complete list.
 *
 * @param {String} appType
 *  The portal application type. Supported values are "text", "spreadsheet" and "presentation".
 */
export function refreshRecentFileAndTemplateList(appType) {

    // try to get recent files and templates
    sendRequest(FILTER_MODULE_NAME, { action: 'gettemplateandrecentlist', type: appType })
    .done(function (data) {
        setRecentFileAndTemplateList(data.recents || [], data.templates || [], appType);
    });
}

/**
 * Clearing the search toolbar.
 *
 * @param {String} appType
 *  The portal application type. Supported values are "text", "spreadsheet" and "presentation".
 */
export function emptySearchToolbar(appType) {
    clearList(appType);
    nodeCollector[appType]?.inputField.val('');
    nodeCollector[appType]?.cancelButton.toggle(false);
}
