/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";
import type TextApp from "@/io.ox/office/text/app/application";
import { type PortalTourConfig, PortalTour } from "@/io.ox/office/portal/app/portaltour";

// class TextFullTour =========================================================

/**
 * The guided tour for the Text Portal application.
 */
export default class TextFullTour extends PortalTour<TextApp> {

    public constructor(config?: PortalTourConfig) {

        // base constructor
        super(AppType.TEXT, config);

        // the initial welcome message
        this.welcomeStep({
            title: gt.pgettext("tour", "The Text app"),
            content: gt.pgettext("tour", "Welcome to your new Text app. This Guided Tour will introduce you to the most important functions of creating and editing text documents online.")
        });

        // highlight the header of the template area
        this.templatesAreaStep({
            title: gt.pgettext("tour", "Templates"),
            content: gt.pgettext("tour", "Clicking on the Text app in the navigation bar displays an overview of all the templates available to you in Text.")
        });

        // describe the "Blank document" button
        this.blankDocumentStep({
            title: gt.pgettext("tour", "Blank text document"),
            content: [
                gt.pgettext("tour", "You can choose from a variety of templates to help you create professional documents."),
                gt.pgettext("tour", "Let's start with a blank document.")
            ]
        });

        // launch the editor application with a new empty document
        this.launchDocumentStep();

        // steps for internal editor applications
        if (!this.isWopiTour()) {

            // insert document contents, show the "Saving changes" label
            this.autoSaveStep(() => this.docModel.insertText("Lorem ipsum dolor sit amet.", [0, 0]));

            // describe the "File" toolbar
            this.fileTabStep();
            this.saveAsStep();
            this.sendAsMailStep("dropdown");
        }

        // describe the "Quit" button
        if (!this.isShortTour()) {
            this.quitButtonStep();
        }

        // final tour step describing how to restart the tour (only when auto-started)
        this.restartTourStep();
    }
}
