/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import ui from "$/io.ox/core/desktop";

import { settings as docsSettings } from "@/io.ox/office/settings";
import { createIcon } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { BROWSER_TAB_SUPPORT, createDocsURL } from "@/io.ox/office/tk/utils/tabutils";

import { AppType, GLOBAL_APP_CONFIGS, getPortalModulePath, getPortalCapabilities, getEditorModulePath, getEditorCapabilities } from "@/io.ox/office/baseframework/utils/apputils";
import { registerAppModule } from "@/io.ox/office/baseframework/appregistry";

// public functions ===========================================================

export async function registerDocumentsApps() {

    // whether something has been changed in the settings (settings need to be saved afterwards)
    let settingsChanged = false;

    // create a stub for each Documents Portal application
    for (const { appType, appIcon, appTitle } of GLOBAL_APP_CONFIGS) {

        // the module identifier and requirements of the portal application
        const moduleName = getPortalModulePath(appType);
        const moduleCaps = getPortalCapabilities(appType);

        // the factory loader function
        const factoryLoader = async () => {
            const { PortalAppFactory } = await import("@/io.ox/office/portal/app/appfactory");
            // create a subclass with parameter-less constructor
            class Factory extends PortalAppFactory {
                constructor() { super({ appType, appTitle }); }
            }
            // return a module-like object with a "default export"
            return { default: Factory };
        };

        // register the application module (returns an application loader)
        const appLoader = registerAppModule(moduleName, factoryLoader);

        // create the portal application stub used for the launcher control, and for auto launch/restore
        ui.createApp({
            id: moduleName,
            name: moduleName,
            title: appTitle,
            icon: $(createIcon(appIcon)),
            requires: moduleCaps,
            settings: false,
            refreshable: true,
            openInTab: BROWSER_TAB_SUPPORT,
            tabUrl: createDocsURL(appType, { app: moduleName }),
            load: appLoader
        });

        // Bug 62755, DOCS-3611, ...
        //
        // When creating a new document, or opening an existing document, backend wants to update the settings
        // property "portal/<apptype>/recents" containing the "Recent Documents" list. If this property is not
        // present at all, the entire "portal/<apptype>" subtree will be re-created, effectively deleting all
        // other existing properties there, for example "portal/<apptype>/fulltour/shown" which causes to show
        // the portal tours twice.
        //
        // Workaround (hacky!): Create the settings subtree with this property before starting the portal
        // application.
        //
        // DOCS-3698: This must be done very early (!) in order to not conflict with portal welcome tours
        // starting quickly, e.g. in automated test scenarios.
        //
        const configKey = `portal/${appType}/recents`;
        if (docsSettings.get(configKey) === undefined) {
            docsSettings.set(configKey, "[]", { silent: true });
            settingsChanged = true;
        }
    }

    // configuration of all Documents editor applications
    const EDITOR_APP_CONFIGS = [
        { appType: AppType.TEXT,         factoryLoader() { return import("@/io.ox/office/text/app/appfactory"); } },
        { appType: AppType.SPREADSHEET,  factoryLoader() { return import("@/io.ox/office/spreadsheet/app/appfactory"); } },
        { appType: AppType.PRESENTATION, factoryLoader() { return import("@/io.ox/office/presentation/app/appfactory"); } },
        { appType: AppType.PRESENTER,    factoryLoader() { return import("@/io.ox/office/presenter/app/appfactory"); } }
    ];

    // create a stub for each Documents editor application
    for (const { appType, factoryLoader } of EDITOR_APP_CONFIGS) {

        // the module identifier and requirements of the application
        const moduleName = getEditorModulePath(appType);
        const moduleCaps = getEditorCapabilities(appType);

        // register the application module (returns an application loader)
        const appLoader = registerAppModule(moduleName, factoryLoader);

        // create the application stub used for auto launch/restore
        ui.createApp({
            id: moduleName,
            name: moduleName,
            requires: moduleCaps,
            settings: false,
            refreshable: true,
            load: appLoader
        });
    }

    // save settings if required (await to prevent any possible race condition with other settings accesses)
    if (settingsChanged) {
        globalLogger.info("$badge{launch} saving updated configuration");
        await docsSettings.save(undefined, { force: true });
    }
}
