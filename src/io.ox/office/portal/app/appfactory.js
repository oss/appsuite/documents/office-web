/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';
import Backbone from '$/backbone';

import ui from '$/io.ox/core/desktop';
import Commons from '$/io.ox/core/commons';
import yell from '$/io.ox/core/yell';
import FilesApi from '$/io.ox/files/api';
import { settings } from '$/io.ox/files/settings';
import FilesUpload from '$/io.ox/files/upload/main';
import TabApi from '$/io.ox/core/api/tab';
import DropZone from '$/io.ox/core/dropzone';
import FolderView from '$/io.ox/core/folder/view';
import TreeView from '$/io.ox/core/folder/tree';
import { invoke } from '$/io.ox/backbone/views/actions/util';

import { SMALL_DEVICE } from '@/io.ox/office/tk/dom';
import { globalEvents } from '@/io.ox/office/tk/events';
import { FILTER_MODULE_NAME, sendRequest } from '@/io.ox/office/tk/utils/io';
import { BROWSER_TAB_SUPPORT } from '@/io.ox/office/tk/utils/tabutils';
import { getStandardTemplateFolderId, isGuest } from '@/io.ox/office/tk/utils/driveutils';

import { getPortalModulePath, setAppFavIcon } from '@/io.ox/office/baseframework/utils/apputils';
import { prefetchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';
import { AbstractAppFactory } from '@/io.ox/office/baseframework/appregistry';
import { DOCUMENTS_COLLABORA } from '@/io.ox/office/baseframework/utils/baseconfig';
import { ADD_TEMPLATE_LABEL, PRESENTATION_DOCUMENT_NAME_NEW, SPREADSHEET_DOCUMENT_NAME_NEW,
    TEXT_DOCUMENT_NAME_NEW } from '@/io.ox/office/baseframework/view/baselabels';

import { registerSecondaryPortalActions, updateSecondaryExtensionPoints } from '@/io.ox/office/portal/action/portal-desktop-actions';
import { prepareMobileToolbar, registerMobilePortalActions } from '@/io.ox/office/portal/action/mobile-toolbar-actions';
import { emptySearchToolbar, refreshRecentFileAndTemplateList, registerPortalSearch,
    setRecentFileAndTemplateList } from '@/io.ox/office/portal/action/portalsearchaction';
import { filterTemplateFiles } from '@/io.ox/office/portal/portalutils';
import { PortalTour } from '@/io.ox/office/portal/app/portaltour';
import DocumentCollection from '@/io.ox/office/portal/collection/documentcollection';
import TemplateCollection from '@/io.ox/office/portal/collection/templatecollection';
import DocumentView from '@/io.ox/office/portal/view/documentview';
import TemplateView from '@/io.ox/office/portal/view/templateview';
import { PortalContextMenu } from '@/io.ox/office/portal/view/contextmenu';

import '@/io.ox/office/portal/style.less';

// class PortalAppFactory =====================================================

export class PortalAppFactory extends AbstractAppFactory {

    /*public*/ constructor(factoryConfig) {

        // base constructor
        super({
            moduleName: getPortalModulePath(factoryConfig.appType),
            singletonApp: true
        });

        // private properties
        this._appType = factoryConfig.appType;
        this._appTitle = factoryConfig.appTitle;
    }

    /*protected*/ implLoadAppClass() {
        return Promise.resolve();
    }

    /*protected*/ implCreateApp() {

        const appType = this._appType;
        const appTitle = this._appTitle;

        // the application instance
        var app = ui.createApp({
            name: this.moduleName,
            title: appTitle,
            help: { base: 'help-documents', target: 'index.html' },
            trackingId: `io.ox/${appType}-portal`,
            appType,
            load() { return Promise.resolve(true); } // no need to load the main module again
        });

        // the document collection for the recent documents list
        var documentCollection = new DocumentCollection();
        // the template collection for the listed templates
        var templateCollection = new TemplateCollection();

        const contextMenu = new PortalContextMenu();

        // update app favicon for tabbed mode (e.g. open a text document in spreadsheet portal, close Doc and go back to portal)
        if (BROWSER_TAB_SUPPORT) {
            setAppFavIcon(appType);
        }

        document.documentElement.classList.add('office-tab');

        app.setLauncher(function (options) {

            // create main window object
            var portalWindow = ui.createWindow({
                name: app.getName(),
                title: appTitle,
                chromeless: true
            });

            // generic CSS marker class for all kinds of Portal applications
            portalWindow.addClass("io-ox-office-portal-window");

            var recentsNode = $('<div>').addClass('office-portal-recents f6-target').append($('<h5>').text(gt('Recent Documents'))),
                recentsList = $('<div>').addClass('office-portal-recents-list'),
                templateHeader = $('<h2>').text(gt('New from Template')),
                templatesNode = $('<div>').addClass('office-portal-templates translucent-low').append(templateHeader),
                templateTypeWrapper = $('<div>').addClass('template-type-wrapper col-xs-12'),
                progressToolbar = $('<div class="toolbar generic-toolbar visual-focus bottom" role="toolbar">');

            const $winRootNode = portalWindow.nodes.outer;
            const $winSideBar = portalWindow.nodes.sidebar;
            const $winSidePanel = portalWindow.nodes.sidepanel;
            const $winContentNode = portalWindow.nodes.main;
            const $winBodyNode = portalWindow.nodes.body;

            // Dropzone to upload template files
            var templateDropZone = new DropZone.Inplace({
                caption: gt('Drop template files here for upload')
            });

            templateDropZone.on({
                show() {
                    templateHeader.hide();
                    templateTypeWrapper.hide();
                },
                hide() {
                    templateHeader.fadeIn('fast');
                    templateTypeWrapper.fadeIn('fast');
                },
                drop(files) {
                    FilesUpload.setWindowNode(app.getWindowNode());
                    files = filterTemplateFiles(files);
                    if (files.length) {
                        FilesUpload.offer(files, { folder: getStandardTemplateFolderId() });
                    }
                }
            });

            templatesNode.append(
                templateDropZone.render().$el.addClass('abs')
            );

            // handle context menu
            templatesNode.on('contextmenu', function (e) {
                contextMenuHandler(e, appType);
            });

            // the bootstrap of the OX Documents app
            function initOXDocuments() {

                const PRIMARY_BUTTON_OPTIONS = {
                    text: { point: 'io.ox/office/text/portal', label: TEXT_DOCUMENT_NAME_NEW, action: 'io.ox/office/portal/text/actions/new/text' },
                    spreadsheet: { point: 'io.ox/office/spreadsheet/portal', label: SPREADSHEET_DOCUMENT_NAME_NEW, action: 'io.ox/office/portal/spreadsheet/actions/new/spreadsheet' },
                    presentation: { point: 'io.ox/office/presentation/portal', label: PRESENTATION_DOCUMENT_NAME_NEW, action: 'io.ox/office/portal/presentation/actions/new/presentation' }
                };

                if (SMALL_DEVICE) {
                    registerMobilePortalActions(appType);
                } else {
                    // creating and registering the actions required for the primary button in the side panel
                    registerSecondaryPortalActions();
                }

                // creating the extension point for the search in the top bar
                const searchToolbarCreated = registerPortalSearch(appType);

                // TODO: refactor later: there are two 'on' listener for portalWindow in this class, merge them
                portalWindow.setTitle(appTitle);
                portalWindow.on('show', function () {
                    portalWindow.setTitle(appTitle);
                });

                // prefetch application source code
                prefetchDocsApp(appType);

                // create application views
                /* eslint-disable no-new */
                new DocumentView({ collection: documentCollection, el: recentsList, model: app });
                new TemplateView({ collection: templateCollection, el: templateTypeWrapper, model: app });
                /* eslint-enable no-new */

                // const root = app.settings.get('folder/root');
                if (!app.settings) { app.settings = settings; }

                // default properties
                app.props = new Backbone.Model();

                app.treeView = new TreeView({ minWidth: 360, app, icons: true, module: getPortalModulePath(appType), contextmenu: true, flat: true, indent: false });
                FolderView.initialize({ app, tree: app.treeView });

                if (SMALL_DEVICE) {
                    prepareMobileToolbar(app, appType, $winBodyNode);
                    $winContentNode.empty().append(recentsNode.append(recentsList), templatesNode.append(templateTypeWrapper), progressToolbar);
                    $winBodyNode.empty().append($winContentNode, app.toolbar);
                } else {

                    // creating the primary button in the side panel and the side panel itself
                    app.addPrimaryAction(PRIMARY_BUTTON_OPTIONS[appType]);

                    // appending the recent list to the sidepanel
                    $winSidePanel.append(recentsNode.append(recentsList));

                    // append the template node to the window content node
                    $winContentNode.empty().append(templatesNode.append(templateTypeWrapper), progressToolbar);
                }

                // adding the search pane
                if (searchToolbarCreated) { app.mediate(); }

                // adapting the side panel for the document portals
                [100, 1000].forEach(function (delay) {
                    window.setTimeout(function () {
                        // forcing that side panel with the recents list is always visible ...
                        if (!SMALL_DEVICE) {
                            $winSidePanel.css('min-width', '320px').show();
                        }
                        // ... and hiding the smaller 'sidebar' in all document portal apps
                        $winSideBar.hide();
                    }, delay);
                });

                // get data for the portal the first time and show it
                updatePortal();
            }

            function updatePortal() {

                if (isGuest()) {
                    // guests & anonymous users
                    documentCollection.setData([]);
                    templateCollection.setData([]);
                } else {

                    // try to get recent files and templates
                    sendRequest(FILTER_MODULE_NAME, { action: 'gettemplateandrecentlist', type: appType })
                    .done(function (data) {

                        // must be reset, because the order from the list is important and
                        // when no element is removed/added, but the order is, there would be no repaint
                        // (minor issue, reset also repaints, so we repaint two times (reset, setData) for the recent list)
                        documentCollection.reset();

                        // set data for the collection, what cause a 'update' that renders also the view
                        documentCollection.setData(data.recents || []);

                        // setting the recent file list for the search
                        setRecentFileAndTemplateList(data.recents || [], data.templates || [], appType);

                        // register a "close" handler for the search, if the user clicks next to the searchNode (later this should include the top bar, too)
                        $winRootNode.on("click", function () { emptySearchToolbar(appType); });

                        // Bug 47782: The template view must be rendered at least one time to make the 'create new document'
                        // template visible. When the documentCollection is empty and also data.templates is empty, there is no change
                        // and therefore setData() doesn't trigger an event to render the template view. To render the template
                        // view in this case, the collection is reseted. That invokes a event that renders the template view.
                        // It should not reset by default before setData(), because this would introduce visible
                        // flicker due to unnecessary rendering at every portal refresh.
                        if (data.templates.length === 0) {
                            templateCollection.reset();
                        } else {
                            // set data for the collection, what cause a 'update' that renders also the view
                            templateCollection.setData(data.templates || []);
                        }

                        // start the welcome tour
                        PortalTour.runPortalTour(appType);

                    }).fail(appFail);
                }

                // hide entries in dropdown menus if the standard tour package is not installed
                PortalTour.updateDropDownMenus(appType);
            }

            // yells if something went wrong...
            function appFail(result) {
                yell(result);
            }

            // refreshes Documents app only on visibility
            function refresh() {

                //TODO for later - maybe we should refresh all portals not only the visible, but check if 'documentsToShow' in 'showOptimalCount' is called without an NAN error on global refresh
                if (!portalWindow.state.visible) { return; }
                updatePortal();
            }

            function contextMenuHandler(event, appType) {

                event.stopPropagation();
                event.preventDefault();

                // clear old entries in the context menu
                contextMenu.clearOptions();

                contextMenu.addOption('addtemplate', { section: 'heading', label: ADD_TEMPLATE_LABEL }).on('click', async event => {
                    contextMenu.hide();  //hide the context menu after a button was clicked
                    const done = await invoke('io.ox/office/portal/add/template/' + appType, event);
                    if (done) { refreshRecentFileAndTemplateList(appType); } // updating the search list
                });

                // set the click position as anchor
                contextMenu.setAnchor({ left: event.pageX, top: event.pageY });

                // show the context menu
                contextMenu.show();
            }

            // set it as the application window
            app.setWindow(portalWindow);

            // #46: no online help for Collabora (must be registered before "show()" is called)
            if (DOCUMENTS_COLLABORA && !BROWSER_TAB_SUPPORT) {
                portalWindow.on('show', function () { document.documentElement.classList.add('documents-collabora-singletab'); });
                portalWindow.on('hide', function () { document.documentElement.classList.remove('documents-collabora-singletab'); });
            }

            // show portal window
            portalWindow.show();

            // Reinit app on portal window show. This is needed for this use case:
            // User opens a document from the recents list -> loses app focus,
            // and close the document -> gained focus to this app without an app 'resume' event).
            portalWindow.on('show', function () {
                document.documentElement.classList.add('office-tab');
                updatePortal();
                // selecting, which secondary extension points shall be shown
                updateSecondaryExtensionPoints('show');

                if (SMALL_DEVICE) {
                    app.toolbar.toggle(true);
                    $winBodyNode.addClass('mobile-toolbar-visible');
                }
            });

            portalWindow.on('hide', function () {
                emptySearchToolbar(appType);
                // making disabled secondary extension points visible again
                updateSecondaryExtensionPoints('hide');

                if (SMALL_DEVICE) {
                    app.toolbar.toggle(false);
                    $winBodyNode.removeClass('mobile-toolbar-visible');
                }
            });

            // refresh Documents portal on OX global refresh event
            ox.on('refresh^ refresh-portal', refresh);

            FilesApi.on('stop:upload', function (requests) {
                if (BROWSER_TAB_SUPPORT && requests && _.isArray(requests)) {
                    requests.forEach(function (request) {
                        request.then(function (file) {
                            TabApi.propagate('upload-file', { folder_id: file.folder_id });
                        });
                    });
                }
                refresh();
            });

            if (BROWSER_TAB_SUPPORT) {
                // Update Calendar on main tab if new appointment was created
                ox.on('appointment:create', function () {
                    TabApi.propagate('appointment-create');
                });
                // Update Mail on main tab if new Email was sent
                ox.on('mail:send:stop', function () {
                    TabApi.propagate('email-send');
                });
                // refresh portal when page becomes visible again to simulate the non-tab appsuite updatePortal behavior with the 'show' event
                globalEvents.on("show:tab", refresh);
            }

            // after the tooltip is inserted in the DOM, set the max-width for the tooltip, according to the rootNote width
            $winRootNode.on('inserted.bs.tooltip', function () {
                // '-90' to give some margin
                $winRootNode.find('.tooltip-inner').css('maxWidth', $winRootNode.outerWidth() - 90 + 'px');
            });

            // init OX Documents in folder support addition success.
            Commons.addFolderSupport(app, null, 'infostore', options.folder).always(initOXDocuments).fail(appFail);
        });

        // register as application tour (will create an entry in the settings menu to start the tour)
        PortalTour.registerPortalTour(appType);

        return app;
    }
}
