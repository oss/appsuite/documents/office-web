/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";
import gt from "gettext";

import { manifestManager } from "$/io.ox/core/manifests";

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE, createDiv } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { purgeFile } from "@/io.ox/office/tk/utils/driveutils";
import { BROWSER_TAB_SUPPORT } from "@/io.ox/office/tk/utils/tabutils";

import { MAIL_AVAILABLE, DOCUMENTS_COLLABORA, AUTOTEST, getDebugFlag, getFlag, setValue, registerDebugCommand } from "@/io.ox/office/baseframework/utils/baseconfig";
import { AppType, QuitReason, getPortalModulePath, getEditorModulePath } from "@/io.ox/office/baseframework/utils/apputils";
import { type BaseLaunchConfig, type CoreApp, launchPortalApp, launchDocsApp } from "@/io.ox/office/baseframework/app/appfactory";
import { type GuidedTourConfig, type GuidedStepConfig, tourLogger, GuidedTour } from "@/io.ox/office/baseframework/app/guidedtour";

import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

import { getNewLaunchOptions } from "@/io.ox/office/portal/portalutils";

import "@/io.ox/office/portal/app/portaltour.less";

// types ======================================================================

export type PortalTourConfig = GuidedTourConfig;

// constants ==================================================================

// the "shown" configuration key for any of the portal tours
const ANY_PORTAL_TOUR_SHOWN_KEY = "portal/alltours/shown";

// selector for any tool pane without hidden contents
const TOOLPANE_SELECTOR = ".tool-pane:not(.hidden-contents)";

// class PortalTour ===========================================================

/**
 * Base class for all guided tours for the Portal applications.
 *
 * @param appType
 *  The type identifier of the application represented by this tour.
 */
export class PortalTour<DocAppT extends EditApplication> extends GuidedTour<DocAppT> {

    // static properties ------------------------------------------------------

    /**
     * Timestamp when the last portal tour has stopped. Needed to prevent
     * running the same tour immediately again in debug mode.
     */
    static #lastStopTime = 0;

    // static functions -------------------------------------------------------

    /**
     * Returns whether the Portal tours are globally supported.
     *
     * @returns
     *  Whether the Portal tours are globally supported.
     */
    static isSupported(): boolean {

        // override all settings with debug flag
        if (getDebugFlag("office:show-tours")) { return true; }

        // do not show welcome tours during automated tests
        if (AUTOTEST) { return false; }

        // bug 51700: do not show welcome tours if the standard tour package is not installed
        const plugins = manifestManager.pluginPoints?.["io.ox/core/main"];
        return !!plugins?.find(plugin => plugin.package === "open-xchange-guidedtours");
    }

    /**
     * Returns whether any of the welcome tours for a Documents portal
     * application has already been shown.
     *
     * @returns
     *  Whether any of the welcome tours for a Documents portal application has
     *  already been shown.
     */
    static isAnyPortalTourShown(): boolean {
        return getFlag(ANY_PORTAL_TOUR_SHOWN_KEY);
    }

    /**
     * Registers a welcome tour for the specified Documents portal application.
     *
     * @param appType
     *  The type identifier of the portal application.
     */
    static registerPortalTour(appType: AppType): void {
        if (!SMALL_DEVICE) {
            const moduleName = getPortalModulePath(appType);
            const factoryFn = async (config?: PortalTourConfig): Promise<PortalTour<EditApplication>> => {
                let modulePromise;
                switch (appType) {
                    case AppType.TEXT:         modulePromise = import("@/io.ox/office/portal/text/fulltour");         break;
                    case AppType.SPREADSHEET:  modulePromise = import("@/io.ox/office/portal/spreadsheet/fulltour");  break;
                    case AppType.PRESENTATION: modulePromise = import("@/io.ox/office/portal/presentation/fulltour"); break;
                    default:                   throw new Error(`invalid application type "${appType}"`);
                }
                const { default: TourClass } = await modulePromise;
                return new TourClass(config);
            };
            const shownKey = `portal/${appType}/fulltour/shown`;
            GuidedTour.registerAppTour(moduleName, factoryFn, shownKey);
        }
    }

    /**
     * Starts the welcome tour for the specified Documents portal application
     * once.
     *
     * @param appType
     *  The type identifier of the portal application.
     *
     * @returns
     *  Whether the tour has actually been started. The tour may reject to
     * start e.g. when it already has been started earlier.
     */
    static runPortalTour(appType: AppType): boolean {

        // bug 51700: do not show any tours if the standard tour package is not installed
        tourLogger.info(`$badge{PortalTour} runPortalTour: running ${appType} portal tour`);
        if (!PortalTour.isSupported()) {
            tourLogger.warn("$badge{PortalTour} runPortalTour: aborted (tours not supported)");
            return false;
        }

        // start the welcome tour for the application once
        const moduleName = getPortalModulePath(appType);
        return GuidedTour.runAppTour(moduleName, { once: true, auto: true });
    }

    /**
     * Hides the menu entry "Show tour for this app" in the global dropdown
     * menus for the specified Documents portal application.
     *
     * @param appType
     *  The type identifier of the portal application.
     */
    static updateDropDownMenus(appType: AppType): void {
        // workaround for bug 51700: hide entry in settings dropdown if the standard tour package is not installed
        // DOCS-4154: hide menu entry in all dropdown menus (has been moved from "Settings" to "Help" for example)
        if (!PortalTour.isSupported()) {
            const moduleName = getPortalModulePath(appType);
            $(`ul>li[data-id="default/${moduleName}"]`).remove();
        }
    }

    // properties -------------------------------------------------------------

    /** Selector for the root window node of the editor application. */
    readonly EDITOR_WINDOW_SELECTOR: string;

    /** Selector for the top pane of the editor application. */
    readonly EDITOR_TOPPANE_SELECTOR: string;

    /** Selector for the main tool pane of the editor application. */
    readonly EDITOR_MAINPANE_SELECTOR: string;

    /** Selector for the application pane of the editor application. */
    readonly EDITOR_APPPANE_SELECTOR: string;

    // whether to show the short version of the tour
    readonly #shortTour: boolean;
    // whether to show the WOPI version of the tour
    readonly #wopiTour: boolean;

    // the editor application (may exist but be inactive, e.g. when using Back button)
    #coreApp: Opt<CoreApp>;
    // the launch configuration used to show the existing editor application
    #appLaunchConfig: Opt<BaseLaunchConfig>;

    // constructor ------------------------------------------------------------

    protected constructor(appType: AppType, config?: PortalTourConfig) {

        // base constructor
        super(appType, config);

        // properties
        this.#shortTour = !!this.config.auto && !getDebugFlag("office:full-tours") && PortalTour.isAnyPortalTourShown();
        this.#wopiTour = DOCUMENTS_COLLABORA;

        // constants
        this.EDITOR_WINDOW_SELECTOR = `.window-container[data-app-name="${getEditorModulePath(appType)}"][data-doc-loaded="true"]:visible`;
        this.EDITOR_TOPPANE_SELECTOR = `${this.EDITOR_WINDOW_SELECTOR} ${TOOLPANE_SELECTOR}.top-pane`;
        this.EDITOR_MAINPANE_SELECTOR = `${this.EDITOR_WINDOW_SELECTOR} ${TOOLPANE_SELECTOR}.main-pane`;
        this.EDITOR_APPPANE_SELECTOR = `${this.EDITOR_WINDOW_SELECTOR} .app-pane`;

        // set the "shown" configuration flag for any of the portal welcome tours,
        // needed to distinguish between full tours and shortened tours
        this.on("start", () => {
            tourLogger.info(`$badge{PortalTour} running ${this.#shortTour ? "short" : "long"} version of the tour`);
            jpromise.floating(setValue(ANY_PORTAL_TOUR_SHOWN_KEY, true));
        });

        // delete the temporary document in Drive when closing the tour
        this.on("stop", jpromise.wrapFloating(async () => {

            // store timestamp to prevent immediate repetition
            PortalTour.#lastStopTime = Date.now();

            // launch the portal application (the tour may have been canceled elsewhere)
            const launchPromise = this.launchPortalApp();

            // close the editor application, and delete the document from Drive
            if (this.#coreApp) {
                try {
                    const fileDesc = this.#coreApp.get("docsFileDesc");
                    await this.#coreApp.quit(false, { reason: QuitReason.AUTO });
                    await purgeFile(fileDesc);
                } catch (err) {
                    tourLogger.exception(err, "Error cleaning up edited document");
                }
            }

            // wait for the portal launcher if still running, handle errors
            try {
                await launchPromise;
            } catch (err) {
                tourLogger.exception(err, "Error launching portal after guided tour");
            }
        }));
    }

    // public methods ---------------------------------------------------------

    /**
     * Starts this tour, except if it has been stopped immediately before.
     */
    override start(config?: PortalTourConfig): this {

        // The debug configuration "office:show-tours" allows to run the same
        // tour everytime when launching or activating the Portal application.
        // Prevent auto-restarting this tour immediately after running it and
        // returning to the Portal when stopping it.
        if (config?.auto && (Date.now() - PortalTour.#lastStopTime <= 2000)) {
            tourLogger.trace("$badge{PortalTour} start: aborted (fast rerun after last tour)");
        } else {
            super.start(config);
        }

        return this;
    }

    /**
     * Returns whether to show the short version of this tour.
     *
     * @returns
     *  Whether to show the short version of this tour.
     */
    isShortTour(): boolean {
        return this.#shortTour;
    }

    /**
     * Returns whether to show the WOPI version of this tour.
     *
     * @returns
     *  Whether to show the WOPI version of this tour.
     */
    isWopiTour(): boolean {
        return this.#wopiTour;
    }

    // specific portal tour steps ---------------------------------------------

    /**
     * Immediately launches the Portal application of this tour.
     *
     * @returns
     *  A promise that will be resolved when the Portal application has been
     *  launched.
     */
    async launchPortalApp(): Promise<void> {
        tourLogger.trace(`$badge{PortalTour} launchPortalApp: launching ${this.appType} portal`);
        await launchPortalApp(this.appType);
    }

    /**
     * Immediately launches an existing editor application, if a blank document
     * has been created already.
     *
     * @returns
     *  A promise that will fulfil when the editor application has been
     *  launched.
     */
    async launchEditorApp(): Promise<void> {
        if (this.#coreApp) {
            tourLogger.trace(`$badge{PortalTour} launchEditorApp: launching ${this.appType} editor`);
            await launchDocsApp(this.appType, this.#appLaunchConfig);
        } else {
            tourLogger.error(`$badge{PortalTour} launchEditorApp: missing ${this.appType} editor`);
            throw new Error("missing application instance");
        }
    }

    /**
     * Defines the leading welcome step of the portal tour.
     */
    welcomeStep(config: GuidedStepConfig): void {
        this.step(config);
    }

    /**
     * Defines the tour step referring to the templates area in the portal
     * application window.
     */
    templatesAreaStep(config: GuidedStepConfig): void {
        const step = this.step(config);
        step.hotspot(".office-portal-templates h2");
        step.referTo(".office-portal-templates .template-item:first-child", { position: "bottom" });
    }

    /**
     * Defines the tour step referring to the "Blank document" button of the
     * portal application.
     */
    blankDocumentStep(config: GuidedStepConfig): void {
        const step = this.step(config);
        // wait for the element in case this step is shown via Back button from next step
        step.waitForAndReferTo(".office-portal-templates .template-item.blank-doc", { position: "bottom", hotspot: ".preview-box" });
    }

    /**
     * Defines the tour step that creates a new blank document, activates the
     * editor application, waits for launching, and refers to the main toolpane
     * of the application window. The launch step will already contain a title
     * and a description text.
     */
    launchDocumentStep(): void {

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "Toolbar"),
            content: gt.pgettext("tour", "At the top of the screen you will find the toolbar which shows all the common functions.")
        });

        // register the application launcher, create a new blank document
        step.navigateToEditor(this.appType, getNewLaunchOptions());

        // once the document has been created, change this step so that it navigates to the existing document,
        // instead of creating more new documents when navigating backwards through the tour
        step.once("navigate", () => {
            const coreApp = this.#coreApp = this.coreApp;
            // wait for the file descriptor
            coreApp.on("change:docsFileDesc", () => {
                this.#appLaunchConfig = { action: "load", file: coreApp.get("docsFileDesc") };
                // overwrite the step configuration that has been initialized above
                step.navigateToEditor(this.appType, this.#appLaunchConfig);
            });
            // create additional helper nodes for step container positioning and highlighting for WOPI apps
            if (this.#wopiTour) {
                coreApp.getWindowNode().append(
                    createDiv({ id: "io-ox-office-portal-tour-toolbar-overlay" }),
                    createDiv({ id: "io-ox-office-portal-tour-closebutton-overlay" }),
                );
            }
        });

        // wait for the application window and highlight the toolbars
        if (this.#wopiTour) {
            step.waitForAndReferTo("#io-ox-office-portal-tour-toolbar-overlay", { timeout: 20, position: "bottom", hotspot: true });
        } else {
            // bug 44195: use first embedded toolbar element to prevent positioning problems with full-width pane
            step.waitForAndReferTo(`${this.EDITOR_MAINPANE_SELECTOR} .toolbar:not(.hidden):eq(0)`, { timeout: 20, position: "bottom", hotspot: true });
        }

        // launch the source application when reaching this step backwards
        step.on("back", () => jpromise.floating(this.launchPortalApp()));
    }

    /**
     * Defines the tour step that inserts some contents in the new document
     * created by the launch step, and refers to the application status label
     * shown in the top pane. The auto-save step will already contain a title
     * and a description text.
     *
     * @param fn
     *  A callback function that must insert some contents into the new
     *  document, in order to show the "Saving changes" label. Will only be
     *  called once when reaching this step the first time.
     */
    autoSaveStep(fn: FuncType<MaybeAsync<unknown>>): void {

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "Autosaving changes"),
            content: gt.pgettext("tour", "Changes are saved automatically whenever you work on your document.")
        });

        // invoke the callback function that will insert some contents into the document
        step.once("before:show", jpromise.wrapFloating(fn));

        // wait for the "All changes saved" label
        step.waitFor(`${this.EDITOR_TOPPANE_SELECTOR} .app-status-label [data-icon-id]`);

        // highlight the "All changes saved" status label
        step.referTo(`${this.EDITOR_TOPPANE_SELECTOR} .app-status-label`, { position: "bottom", hotspot: ".caption" });
    }

    /**
     * Defines the tour step that refers to the tab button of the "File"
     * toolpane. The step will already contain a title and a description text.
     */
    fileTabStep(): void {

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "File tab"),
            content: gt.pgettext("tour", "The file tab offers you various ways to share or save your document.")
        });

        // open and highlight the "File" toolpane
        step.on("before:show", () => step.triggerOptionButton(this.EDITOR_TOPPANE_SELECTOR, "view/toolbars/tab", "file"));
        step.highlightOptionButton(this.EDITOR_TOPPANE_SELECTOR, "view/toolbars/tab", "file");

        // do not cover the main toolpane with the popup box
        step.referTo(this.EDITOR_MAINPANE_SELECTOR, { position: "bottom" });
    }

    /**
     * Defines the tour step that opens and refers to the "Save In Drive"
     * dropdown menu. The save-as step will already contain a title and a
     * description text.
     */
    saveAsStep(): void {

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "Saving options"),
            content: [
                gt.pgettext("tour", 'If you would like to store your document in a specific folder, just click on "Save as".'),
                gt.pgettext("tour", 'You can also click on "Save as template" to create your own templates.')
            ]
        });

        // open and highlight the "Save As" dropdown menu
        step.showDropDownMenu(this.EDITOR_MAINPANE_SELECTOR, "view/saveas/menu");
        step.waitForAndReferToPopup({ position: "right", hotspot: true });
    }

    /**
     * Defines the tour step that refers to the "Send As Mail" button. The mail
     * step will already contain a title and a description text.
     *
     * @param type
     *  type type of the mail button (simple push button, or dropdown menu
     *  button).
     */
    sendAsMailStep(type: "button" | "dropdown"): void {

        // Mail app may not be available
        if (!MAIL_AVAILABLE) { return; }

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "Sending your document"),
            content: gt.pgettext("tour", "Sending your document by email is easy. Just click on the mail icon and you are ready to go!")
        });

        // launch editor in case this step is reached by using the Back button in a short tour
        step.on("before:show", () => jpromise.floating(this.launchEditorApp()));

        // highlight the mail button
        const itemKey = (type === "dropdown") ? "document/sendmail/menu" : "document/sendmail";
        step.referToControl(this.EDITOR_MAINPANE_SELECTOR, itemKey, { position: "bottom", hotspot: ".caption" });
    }

    /**
     * Defines the tour step that refers to the "Quit" button in the toolpane,
     * without actually closing the document. The step will already contain a
     * title and a description text.
     */
    quitButtonStep(): void {

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "Closing document"),
            content: gt.pgettext("tour", "The document can be closed now. As all changes are stored automatically, you do not need to save documents manually.")
        });

        // return to editor application when step is reached with the "Back" button
        step.on("before:show", () => jpromise.floating(this.launchEditorApp()));

        // DOCS-4149: document will be closed with top-level Portal button in multi-tab mode
        // wait for the element in case this step is shown via Back button from next step
        if (BROWSER_TAB_SUPPORT) {
            step.waitForAndReferTo('#io-ox-documentsbar .btn[data-action="app/quit"]', { position: "bottom", hotspot: "[data-icon-id]" });
        } else if (this.#wopiTour) {
            step.waitForAndReferTo("#io-ox-office-portal-tour-closebutton-overlay", { position: "bottom", hotspot: true });
        } else {
            step.waitForAndReferToControl(this.EDITOR_TOPPANE_SELECTOR, "app/quit", { position: "bottom", hotspot: ".caption" });
        }
    }

    /**
     * Defines the tour step that opens the Help dropdown menu, and refers to
     * the "Restart This Tour" menu entry. The step will already contain a
     * title and a description text.
     */
    restartTourStep(): void {

        // only show this step when tour has been started automatically
        if (!this.config.auto) { return; }

        // create a new tour step
        const step = this.step({
            title: gt.pgettext("tour", "Restart Guided Tour"),
            content: gt.pgettext("tour", "Hint: you can start guided tours, any time you need them, from the system menu.")
        });

        // show the Portal application again
        step.navigateToPortal(this.appType);

        // open the global "Help" dropdown menu
        step.on("navigate", jpromise.wrapFloating(async () => {
            await this.launchPortalApp();
            $("#io-ox-topbar-help-dropdown-icon>.btn").dropdown("toggle");
        }));
        // close the global "Help" dropdown menu
        step.on("before:hide", () => {
            $("#io-ox-topbar-help-dropdown-icon>.btn").dropdown("toggle");
        });
        step.hotspot("#io-ox-topbar-help-dropdown-icon svg");

        // wait for and highlight the "Restart tour" menu entry
        step.waitForAndReferTo('#topbar-help-dropdown li:visible [data-action="guided-tour"]', { position: "left", hotspot: true });
    }
}

// static initialization ======================================================

// register handler for the debug command "office.debug.resetAllTours"
registerDebugCommand("resetAllTours", () => {
    globalLogger.log('$badge{PortalTour} Resetting "any shown" flag for portal apps.');
    jpromise.floating(setValue(ANY_PORTAL_TOUR_SHOWN_KEY, null));
});
