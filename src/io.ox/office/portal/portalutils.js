/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import yell from '$/io.ox/core/yell';

import { createIcon, setFocus, matchKeyCode } from '@/io.ox/office/tk/dom';
import { ACTIVATE_TAB_SUPPORT, BROWSER_TAB_SUPPORT, openDriveParentTab } from '@/io.ox/office/tk/utils/tabutils';
import { authorizeGuard, getFileModelFromDescriptor, getStandardDocumentsFolderId } from '@/io.ox/office/tk/utils/driveutils';

import { getGlobalAppConfig } from "@/io.ox/office/baseframework/utils/apputils";
import { getAppType, getTemplateExtensions } from '@/io.ox/office/baseframework/app/extensionregistry';
import { format, getDateFormat, getNoYearFormat, getTimeFormat } from '@/io.ox/office/editframework/utils/dateutils';
import { launchPresenter } from '@/io.ox/office/presenter/utils/presenterutils';

// re-exports =================================================================

export * from '@/io.ox/office/tk/utils';

// public functions ===========================================================

/**
 * Detects file type and creates an corresponding icon node.
 *
 * @param {String} fileName
 *  The filename.
 *
 * @param {Number} [size]
 *  An optional parameter to set the size (in pixel) of the document icon.
 *
 * @returns {jQuery}
 *  the icon node as jQuery object
 */
export function createDocumentIcon(fileName, size) {
    const appType = getAppType(fileName);
    const iconId = getGlobalAppConfig(appType)?.appIcon ?? "bi:file";
    const options = { classes: `document-icon ${appType}` };
    if (_.isNumber(size)) { options.size = size; }
    return $(createIcon(iconId, options));
}

/**
 * Formats the timestamp representation for the recents document list:
 * - show only time, if the document is created today
 * - show year only, if the document is not modified in this year.
 *
 * @param {Number} timestamp
 * - milliseconds since Jan 1, 1970
 *
 * @returns {String}
 *  the formatted date as string
 */
export function formatDate(timestamp) {

    if (!_.isNumber(timestamp)) { return gt('unknown'); }

    var now = new Date();
    var date = new Date(timestamp);

    function getTimeString() {
        return format(date, getTimeFormat());
    }

    function getDateString() {
        // get locale country default date format
        var formatString = getDateFormat();
        // hide year if the document is modified in this year
        if (date.getYear() === now.getYear()) {
            formatString = getNoYearFormat();
        }
        return format(date, formatString);
    }

    function isToday(date) {
        return date.getDate() === now.getDate() &&
            date.getMonth() === now.getMonth() &&
            date.getYear() === now.getYear();
    }

    // show only time if the document is last modifed today
    return isToday(date) ? getTimeString() : getDateString();

}

/**
 * Returns the given file name without the file extension.
 *
 * @param {String} filename
 *  Filename as string
 *
 * @returns {String}
 *  Specified file name as string without file extension.
 */
export function removeFileExtension(filename) {

    var shortFileName = filename,
        pos = filename.lastIndexOf('.');

    if (pos > -1) {
        shortFileName = shortFileName.substr(0, pos);
    }

    return shortFileName;
}

/**
 * Move focus to from given node to its next or previous sibling. (keyboard accessibility)
 *
 * @param {jQuery} startNode
 *  Starting node.
 *
 * @param {String = 'right'} direction
 *  Available direction strings: 'right','left', 'up', 'down'.
 */
export function moveFocus(startNode, direction) {

    if (!startNode) { return; }

    var nodeToFocus = startNode;
    direction = direction || 'right';

    /**
     * Tries to find a vertical neighbor according to cursor direction (up/down)
     *
     * @returns {jQuery|Null}
     *  the vertical neighbor object or null no vertical neighbours found.
     */
    function findVerticalSibling() {
        var startNodePosition = startNode.position(),
            startNodeHeight = startNode.outerHeight(true),
            targetNodes = startNode.siblings();
        return _.find(targetNodes, function (node) {
            if (!$(node).attr('tabindex')) { return false; }
            var nodePosition = $(node).position();
            if (nodePosition.left !== startNodePosition.left) { return false; }
            switch (direction) {
                case 'down':
                    return nodePosition.top === (startNodePosition.top + startNodeHeight);
                case 'up':
                    return nodePosition.top === (startNodePosition.top - startNodeHeight);
                default:
                    return false;
            }
        });
    }

    switch (direction) {
        case 'right':
            nodeToFocus = startNode.next();
            // temp workaround: jump over inserted Bootstrap tooltip element if there is any.
            if (!nodeToFocus.attr('tabindex')) { nodeToFocus = nodeToFocus.next(); }
            nodeToFocus = nodeToFocus[0];
            break;
        case 'left':
            nodeToFocus = startNode.prev()[0];
            break;
        case 'down':
            nodeToFocus = findVerticalSibling('down');
            break;
        case 'up':
            nodeToFocus = findVerticalSibling('up');
            break;
    }

    setFocus(nodeToFocus);
}

/**
 * Keyboard accessibility handler for recent documents and templates list.
 *
 * @param {jQuery.Event} event
 *  {Boolean} event.data.grid - Whether grid navigation (up,down,left,right) is required.
 */
export function keydownHandler(event) {

    if (!event || !event.target) { return; }

    var startNode = $(event.target),
        isGrid = event.data ? event.data.grid : false;

    if (matchKeyCode(event, 'ENTER') || matchKeyCode(event, 'SPACE')) { startNode.trigger('click'); }
    if (matchKeyCode(event, 'LEFT_ARROW')) { moveFocus(startNode, 'left'); }
    if (matchKeyCode(event, 'RIGHT_ARROW')) { moveFocus(startNode, 'right'); }
    if (matchKeyCode(event, 'DOWN_ARROW')) { moveFocus(startNode, isGrid ? 'down' : 'right'); }
    if (matchKeyCode(event, 'UP_ARROW')) { moveFocus(startNode, isGrid ? 'up' : 'left'); }
}

/**
 * A handler to check if a long tap was made on iOS. It fires a 'contextmenu'
 * event on long tap (after 750 ms) and a 'click' on a normal tap (0-400 ms).
 *
 * @param {Event} event
 *  A touchstart event which started the tap.
 *
 * @param {jQuery} link
 *  A reference to the element on which the handler was called.
 */
export function iosContextEventHandler(event, link) {

    var // a flag if the long tap was canceled (touchend event before hold() is called)
        inCancelFlag = true,
        // a flag if the a long tap is/was invoked
        inHoldFlag = true,
        // a flag if the touch position is moved while holding
        movedWhileHolding = false,
        // time from touchstart
        timeStart = event.timeStamp,
        // position at touchstart
        xTouchstart = event.originalEvent.pageX,
        yTouchstart = event.originalEvent.pageY,
        // tracked position from touchmove
        yMoved,
        // ignore user movment from from n pixels
        ignoreMovmentInPixel = 10;

    function trackPos(event) {
        var touch = event.originalEvent.touches[0];
        yMoved = touch.pageY;
    }

    function cancel(event) {

        // to prevent the ghost click
        event.preventDefault();

        var // position at touchend
            yTouchend = event.originalEvent.pageY,
            // don't click when touchstart has moved compared to touchend ->??? scroll??
            movedOnTouchend = (Math.abs(yTouchstart - yTouchend) > ignoreMovmentInPixel),
            // prevent click when touchend event is greater than 400ms
            preventClickOnTouchHold = (Math.abs(timeStart - event.timeStamp) < 400);

        inCancelFlag = false;

        // trigger a click when there was no scrolling and the touch was released quick
        // do not trigger when a longtap was started in hold()
        if (inHoldFlag && !movedOnTouchend && preventClickOnTouchHold) {
            link.trigger('click');
        }

        // event listeners are destroyed on touchend
        removeListener();
    }

    function hold() {

        inHoldFlag = false;

        // check if moved (compare touchstart with the current touchmove pos) -> scroll (down&move) would fire otherwise
        movedWhileHolding = (Math.abs(yTouchstart - yMoved) > ignoreMovmentInPixel);

        // trigger context menu when there was no touchend before and no movement
        if (inCancelFlag && !movedWhileHolding) {

            // Create a new jQuery.Event object with specified event properties.
            var e = new $.Event('contextmenu', { pageX: xTouchstart, pageY: yTouchstart });

            // trigger an artificial keydown event with keyCode 64
            link.trigger(e);
        }
    }

    // remove all listener used in iosContextEventHandler()
    function removeListener() {
        $(document).off('touchmove', trackPos);
        link.off('touchend', cancel);
    }

    // track the position when the finger touches the touchscreen
    $(document).on('touchmove', trackPos);

    // when the finger leaves the touchscreen
    link.on('touchend', cancel);

    // check if a long tap should be triggered after 750 ms
    window.setTimeout(hold, 750);
}

/**
 * Launch options to create a new document in the default 'Documents'
 * folder in Drive.
 *
 * @returns {Object}
 *  Necessary parameters for creating a new document in the default folder.
 */
export function getNewLaunchOptions() {
    return { action: 'new', target_folder_id: getStandardDocumentsFolderId() };
}

/**
 * Launch options to create a new encrypted document in the default 'Documents'
 * folder in Drive.
 *
 * @returns {Promise}
 *  A promise which returns the necessary parameters for creating a new encrypted document in the default folder.
 */
export function getNewEncryptedLaunchOptions() {
    // set authorization data to the model
    return authorizeGuard().then(function (auth_code) {
        return _.extend(getNewLaunchOptions(), { auth_code, cryptoAction: 'Encrypt' });
    });
}

/**
 * Returns whether the action "Show file in Drive app" is supported. In
 * browser tab mode, the browser must allow to activate another browser tab
 * from a script.
 *
 * @returns {boolean}
 *  Whether the action "Show file in Drive app" is supported.
 */
export function canShowInDrive() {
    return !BROWSER_TAB_SUPPORT || ACTIVATE_TAB_SUPPORT;
}

/**
 * runs ox-Drive with assigned folder id
 * then it calls "selectFile" in the Drive app with assigned file-descriptor
 *
 * @param {Object} file is a file-descriptor
 *
 * @param {Boolean} [busy=false]
 *
 * if assigned current ox app is set to busy mode
 * and back to idle after drive is loaded
 *
 */
export function showInDrive(file, busy) {

    if (BROWSER_TAB_SUPPORT) {
        openDriveParentTab(file.folder_id, file.id);
        return;
    }

    var win = null;
    if (busy) {
        win = ox.ui.App.getCurrentApp().getWindow();
        win.busy();
    }

    // folder from the file in drive
    ox.launch(() => import('$/io.ox/files/main'), { folder: file.folder_id }).then(driveApp => {
        // set proper folder and select file
        driveApp.selectFile(file);
        driveApp.listView.trigger('listview:shown');
        if (win) { win.idle(); }
    });
}

/**
 * Shows the document in the Presenter.
 *
 * @param {Object} baton
 *  A baton object which has a backbone cid.
 *  Note: Unfortunately there are cases where the cid is the file-descriptor (e.g. in Drive),
 *  so better take a look at the baton before using it here.
 *
 * @param {Boolean} [busy=false]
 *  If assigned current ox app is set to busy mode
 *  and back to idle after the Presenter is loaded.
 *
 */
export function showInPresenter(baton, busy) {
    var win = null;
    if (busy) {
        win = ox.ui.App.getCurrentApp().getWindow();
        win?.busy();
    }
    // There are two different kinds of batons, one with a backbone cid and
    // one which has a file descriptor as a cid (e.g. in Drive). This baton
    // is expected to have a backbone cid. To launch the Presenter, a baton
    // with a file descriptor as a cid is needed. Therefore 'getFileModelFromDescriptor'
    // is used to create it.
    getFileModelFromDescriptor(baton.data).then(launchPresenter).then(() => win?.idle());
}

export function filterTemplateFiles(files) {

    const appType = ox.ui.App.getCurrentApp().get('appType');
    const fileExt = getTemplateExtensions(appType);

    // filter templates by file extension
    var templateFiles = _(files).filter(function (file) {
        var parts = file.name.split('.');
        var extension = parts.length === 1 ? '' : parts.pop().toLowerCase();
        return fileExt.indexOf(extension) >= 0;
    });

    // yell if files with unsupported file extension exists
    if (files.length !== templateFiles.length) {
        yell('error',  (files.length === 1) ? gt('The file cannot be uploaded as this is not a supported template.') : gt('Some files could not be uploaded as they are not supported templates.'));
    }

    return templateFiles;
}
