/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import Backbone from '$/backbone';

import DocumentModel from '@/io.ox/office/portal/model/documentmodel';

/**
 * Filters the path property of a recent file entry for specific
 * infostore folders.
 *
 * @param {Array} recents
 *  an array with recent file data objects provided by the REST call
 *  'getrecentfilesandtemplates'.
 *
 * @returns {Array}
 *  An array of recent file data objects where the path property was
 *  filtered (removing specific folders, e.g. root & drive)
 */
function filterRecentsPath(recents) {
    _.each(recents, function (recentFile) {
        recentFile.path = _.filter(recentFile.path || [], function (folder) {
            return ((folder.id !== '1') && (folder.id !== '9'));
        });
    });
    return recents;
}

var documentCollection = Backbone.Collection.extend({

    model: DocumentModel,

    setData(recentsArray) {

        var self = this;

        recentsArray = filterRecentsPath(recentsArray);

        // populate the collection with recent documents and
        // invokes an 'update' event that re-renders the view
        // TODO refactor: to risky to remove it before release, but
        // merge=true should not be needed here, it's the default for 'set'
        self.set(recentsArray, { merge: true });

    }

});

export default documentCollection;
