/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import Backbone from '$/backbone';
import TemplateModel from '@/io.ox/office/portal/model/templatemodel';

var templateCollection = Backbone.Collection.extend({

    model: TemplateModel,

    /**
     * Fills this collection with passed array of template objects.
     *
     * @param {Array} templateList
     *  An array of template objects (infostore file descriptors)
     */
    setData(templateList) {

        var self = this;

        // populate the collection with template objects and
        // invokes an 'update' event that re-renders the view
        self.set(templateList);

    },

    /**
     * Get templates according to passed template type.
     *
     * @param {AppType} appType
     *  The type identifier of the current application.
     *
     * @param {String = 'all'} source
     *  valid types: 'all', 'user', 'admin' or 'global'. Defaults to 'all' if omitted.
     *
     * @returns {Array}
     *  An array of template models.
     */
    getTemplates(appType, source) {

        var models = this.models;

        source = source || 'all';

        function templateFilter(templateModel) {
            if (source === 'all') { return templateModel.get('type') === appType; }
            return templateModel.get('source') === source && templateModel.get('type') === appType;
        }

        return _.filter(models, templateFilter);

    },

    /**
     * Comparator function for sorting templates according to their filenames alphabetically.
     *
     * @param {Backbone.Model} model
     *  a template model.
     *
     * @returns {String}
     *  returns the values to be sorted.
     */
    comparator(model) {
        return model.get('filename').toLowerCase();
    }

});

export default templateCollection;
