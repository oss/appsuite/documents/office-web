/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { jpromise } from "@/io.ox/office/tk/algorithms";

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";
import { PRESENTER_AVAILABLE } from "@/io.ox/office/baseframework/utils/baseconfig";
import type PresentationApp from "@/io.ox/office/presentation/app/application";
import { type PortalTourConfig, PortalTour } from "@/io.ox/office/portal/app/portaltour";

// class PresentationFullTour =================================================

/**
 * The guided tour for the Presentation Portal application.
 */
export default class PresentationFullTour extends PortalTour<PresentationApp> {

    public constructor(config?: PortalTourConfig) {

        // base constructor
        super(AppType.PRESENTATION, config);

        // the initial welcome message
        this.welcomeStep({
            title: gt.pgettext("tour", "The Presentation app"),
            content: gt.pgettext("tour", "Welcome to your new Presentation app. This Guided Tour will introduce you to the most important functions of creating and editing presentations online.")
        });

        // do not show the following steps in the short version of the tour
        if (!this.isShortTour()) {

            // highlight the header of the template area
            this.templatesAreaStep({
                title: gt.pgettext("tour", "Templates"),
                content: gt.pgettext("tour", "Clicking on the Presentation app in the navigation bar displays an overview of all the templates available to you in Presentation.")
            });

            // describe the "Blank document" button
            this.blankDocumentStep({
                title: gt.pgettext("tour", "Blank presentation"),
                content: [
                    gt.pgettext("tour", "You can choose from a variety of templates to help you create professional presentations."),
                    gt.pgettext("tour", "Let's start with a blank presentation.")
                ]
            });
        }

        // launch the editor application with a new empty document
        this.launchDocumentStep();

        // steps for internal editor applications
        if (!this.isWopiTour()) {

            // describe the Slide and Layout buttons
            const newSlideStep = this.step({
                title: gt.pgettext("tour", "New slides"),
                content: gt.pgettext("tour", 'With a click on "Slide" you can add new slides to your presentation. Choose "Layout" to change the design of the selected slide. Depending on your template, many different layouts and slide types are available to you.')
            });
            // open and highlight the "Save As" drop-down menu
            newSlideStep.showDropDownMenu(this.EDITOR_MAINPANE_SELECTOR, "layoutslidepicker/insertslide");
            newSlideStep.waitForAndReferToPopup({ position: "right", hotspot: true });

            // do not show the following steps in the short version of the tour
            if (!this.isShortTour()) {

                // insert document contents, show the "Saving changes" label
                this.autoSaveStep(() => this.docModel.insertText("Lorem ipsum dolor sit amet.", [0, 0, 0, 0]));
            }

            // describe the "Edit master" button
            const editMasterStep = this.step({
                //#. "master" means a master slide in a presentation
                title: gt.pgettext("tour", "Edit master"),
                content: gt.pgettext("tour", 'You can create and edit the layout of your slides by clicking on "Edit master". While editing your master slides, you are able to change back to edit your normal slides by clicking on "Edit normal".')
            });
            editMasterStep.on("before:show", () => editMasterStep.triggerOptionButton(this.EDITOR_TOPPANE_SELECTOR, "view/toolbars/tab", "slide"));
            editMasterStep.waitForAndReferToControl(this.EDITOR_MAINPANE_SELECTOR, "slide/masternormalslide", { position: "bottom", hotspot: ".caption" });

            // do not show the following steps in the short version of the tour
            if (!this.isShortTour()) {

                // describe the "File" toolbar
                this.fileTabStep();
                this.saveAsStep();
                this.sendAsMailStep("button");
            }

            // describe the "Present" button starting a presentation
            if (PRESENTER_AVAILABLE) {
                const presentStep = this.step({
                    title: gt.pgettext("tour", "Present slides"),
                    content: gt.pgettext("tour", 'With a click on "Present" you can start a presentation of your slides right away.')
                });
                // launch editor in case this step is reached by using the Back button in a short tour
                presentStep.on("before:show", () => jpromise.floating(this.launchEditorApp()));
                presentStep.waitFor(this.EDITOR_TOPPANE_SELECTOR);
                presentStep.referToControl(this.EDITOR_TOPPANE_SELECTOR, "present/startpresentation", { position: "bottom", hotspot: ".caption" });
            }
        }

        // describe the "Quit" button
        if (!this.isShortTour()) {
            this.quitButtonStep();
        }

        // final tour step describing how to restart the tour (only when auto-started)
        this.restartTourStep();
    }
}
