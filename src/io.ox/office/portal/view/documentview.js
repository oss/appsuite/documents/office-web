/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import Backbone from '$/backbone';

import ext from '$/io.ox/core/extensions';

import { getAppType } from '@/io.ox/office/baseframework/app/extensionregistry';
import { COMPACT_DEVICE, getBooleanOption } from '@/io.ox/office/portal/portalutils';
import RecentsActions from '@/io.ox/office/portal/action/recentsactions';

// class DocumentView =====================================================

var DocumentView =  Backbone.View.extend({

    tagName: 'div',

    className: 'office-portal-document',

    initialize() {

        var // the document view object
            self = this,
            // the handler for Window resize events
            resizeHandler = _.debounce(function () { self.showOptimalCount(self, { isResize: true }); }, 250);

        this.listenTo(this.collection, 'destroy', this.remove);

        // Registering resize handler for the recent document list
        $(window).on('resize', function () { resizeHandler(); });

        this.model.getWindow().on('hide', function () {
            // Deregistering resize handler for the recent document list, if the app window is hidden
            $(window).off('resize', resizeHandler);
        });

        // observer for the collection
        this.listenTo(this.collection, 'update', function () {
            // re-render the recent list for the current app
            self.render(self.model.get('appType'));
        });

        // observer for the collection
        this.listenTo(this.collection, 'reset', function () {
            // re-render the recent list for the current app
            self.render(self.model.get('appType'));
        });

    },

    /**
     * Renders recent documents section of supported applications
     *
     * @param {AppType} appType
     *  The type identifier of the current application.
     *
     * @returns {this}
     */
    render(appType) {

        var self = this,
            documentCollection = self.collection,
            emptyNotificationNode = $('<p>').addClass('office-portal-notification bg-info').text(gt('You have no recent documents.'));

        // empty the recent list
        self.$el.empty();

        if (documentCollection.length === 0) {
            self.$el.append(emptyNotificationNode);
            self.$el.removeClass('f6-target');
            return this;
        }

        _.each(documentCollection.models, function (model) {
            var fileName = model.get('com.openexchange.file.sanitizedFilename') || model.get('filename');
            // the application handling files with the specified name
            // fix for bug 47431: check if a file name can be resolved, before getting the fileAppType
            var fileAppType = fileName ? getAppType(fileName) : null;
            // the extension of files
            var errorSuffix = '_ox';
            // quit if the current visible application is not the file application
            // or if the file name ends with the error extension '_ox'
            if ((appType !== fileAppType) || fileName.endsWith(errorSuffix)) { return; }
            // draw recent document items and bind actions through extension invoke
            RecentsActions.documentPoint.invoke('draw', self.$el, ext.Baton({ data: model.attributes, model }));
        });

        self.showOptimalCount(self);

        // if no recent file item is drawn to the DOM after app type filter, show empty notification,
        // remove f6-target so that the recent file list is skipped while iterating with keyboard.
        if (self.$el.find('.document-link').length === 0) {
            self.$el.append(emptyNotificationNode);
            self.$el.removeClass('f6-target');
        }

        // to init the bootstrap tooltips in portal
        self.$el.find('[data-toggle="tooltip"]').tooltip();

        return this;
    },

    // show as many recent documents, that they all fit in the current window height.
    // count is set fixed at 10 for small and medium displays.
    showOptimalCount(self, options) {

        var // whether this list creation is triggered by a resize event
            isResize = getBooleanOption(options, 'isResize', false),
            // the node element with class 'office-portal-recents-list'
            list = self.$el,
            // the side panel node
            sidePanel = list.closest('.window-sidepanel'),
            // the primary button
            primaryButton = sidePanel.children('.primary-action'),
            // the height in pixel of the complete recent list
            listPaneHeight = sidePanel.outerHeight() - primaryButton.height(),
            // the existing links to the recent documents
            documents = list.children('a.document-link'),
            // the height in pixel of one link
            documentHeight = documents.outerHeight(),
            // the length of the collection of recent documents
            collectionLength = documents.length,
            // whether the view is displayed in mode for small devices (width < 1024px)
            isSmallDevice = COMPACT_DEVICE,
            // the height in pixel of the header line
            headlineHeight = list.parent().find('h5').outerHeight(), // TODO: Should not be dependent from "h5"
            // calculate optimal recent documents to show
            // TODO refactor: documentsToShow can be NAN, when the portal is updated, but not visible, which cause a unnecessary 'rerender' later
            documentsToShow = isSmallDevice ? collectionLength : Math.floor((listPaneHeight - headlineHeight) / documentHeight) - 1,
            // the current number of displayed documents
            oldLength = 0,
            // whether it is necessary to rerender the recent document list
            rerender = true;

        // set a fixed document count for small and medium displays
        if ((documentsToShow > 10) && isSmallDevice) { documentsToShow = 10; }

        // checking, if rerendering the list of recent files is neccessary
        if (isResize) {
            oldLength = _.filter(documents, function (oneLink) {
                return $(oneLink).css('display') !== 'none';
            }).length;

            // no rerendering required, if list length is not modified, or
            // if already all documents are shown and the size is increased
            if ((documentsToShow === oldLength) ||
                ((documentsToShow > oldLength) && (oldLength >= collectionLength))) { rerender = false; }
        }

        if (rerender) {
            // show optimal count of documents
            documentsToShow = (documentsToShow > collectionLength) ? collectionLength : documentsToShow;

            _.each(documents, function (oneLink, index) {
                if (index + 1 <= documentsToShow) {
                    $(oneLink).css('display', 'block');
                } else {
                    $(oneLink).css('display', 'none');
                }
            });
        }

    }

});

export default DocumentView;
