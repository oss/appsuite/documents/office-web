/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { TOUCH_DEVICE } from '@/io.ox/office/tk/dom';
import { ListMenu } from '@/io.ox/office/tk/popup/listmenu';

// class PortalContextMenu ====================================================

export class PortalContextMenu extends ListMenu {

    constructor() {

        // base constructor
        super({
            displayType: "blocking", // catch clicks next to visible context menu to just hide itself
            anchorBorder: TOUCH_DEVICE ? "top" : "right",
            coverAnchor: true,
            detachedAnchor: true,
            _kind: "singleton"
        });

        // After closing an OX app there could still be an invisible text selection active.
        // Due to this, iOS detects an open softkeyboard and therefore would reduce the boundingBox.
        // This results in a self closing context menu, when opened at the bottom part from the screen,
        // because the popup is self closed in the refreshNode loop.
        window.getSelection().removeAllRanges();

        // add marker class for additional CSS formatting
        this.$el.addClass('portal-context-menu');

        // close the context menu on orientationchange
        this.listenTo(window, 'orientationchange', () => this.hide());
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds a new button to this context menu.
     *
     * @param {string} id
     *  A unique identifier for the action button.
     *
     * @param {string} label
     *  The label text.
     *
     * @param {Function} handler
     *  The click event handler function.
     */
    addAction(id, label, handler) {
        this.addOption(id, { label }).on("click", event => {
            this.hide();
            return handler.call(this, event);
        });
    }
}
