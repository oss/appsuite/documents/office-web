/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';
import Backbone from '$/backbone';

import { CHROME_ON_ANDROID } from '@/io.ox/office/portal/portalutils';
import ext from '$/io.ox/core/extensions';
import TemplateActions from '@/io.ox/office/portal/action/templateactions';


var templateView = Backbone.View.extend({

    tagName: 'div',

    className: 'office-portal-template',

    initialize() {

        var // the document view object
            self = this;

        // observer for the collection (when the collection is updated)
        this.listenTo(this.collection, 'update', function () {
            // re-render the recent list for the current app
            self.render(self.model.get('appType'));
        });

        // observer for the collection (when only a model in the collection has been changed)
        this.listenTo(this.collection, 'change', function () {
            // re-render the recent list for the current app
            self.render(self.model.get('appType'));
        });

        // observer for the collection
        this.listenTo(this.collection, 'reset', function () {
            // re-render the recent list for the current app
            self.render(self.model.get('appType'));
        });
    },

    /**
     * Renders templates section of supported applications and 'My Templates' section
     *
     * @param {AppType} appType
     *  The type identifier of the current application.
     *
     * @returns {this}
     */
    render(appType) {

        // render nothing if we are on Text app on Android with non-supported Chrome or other browser
        if (_.browser.Android && (appType === 'text') && !CHROME_ON_ANDROID) {
            return this;
        }

        var self = this,
            templateWrapper = $('<div>').addClass('template-list f6-target');

        // empty the template list
        self.$el.empty();

        self.$el.append(templateWrapper);

        // draw create with blank template
        TemplateActions[appType].invoke('draw', templateWrapper, ext.Baton({ app: self.model, appType }));

        // get templates to render from template collection for all sources (user, admin, global)
        // and render the templates for the active app type
        _.each(['user', 'admin', 'global'], function (templateSource) {

            // 'user' and 'admin' templates are used alike while rendering templates,
            // therefore is only differentiated between 'user' and 'global' here
            var type = ((templateSource === 'global') ? 'global' : 'user');

            _.each(this.collection.getTemplates(appType, templateSource), function (template) {
                // render
                // Important: 'template.attributes' is cloned here, because it's changed when a document is opened otherwise (passed by reference).
                // That would introduce a 'version' attribute in the baton that prevent displaying the most recent template thumbnail.
                // (e.g. modify a template -> thumbnail is not updated)
                TemplateActions[appType + type].invoke('draw', templateWrapper, ext.Baton({ data: _.clone(template.attributes) }));
            });
        }, this);

        return this;
    }

});

export default templateView;
