/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { jpromise } from "@/io.ox/office/tk/algorithms";

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";
import type SpreadsheetApp from "@/io.ox/office/spreadsheet/app/application";
import { type PortalTourConfig, PortalTour } from "@/io.ox/office/portal/app/portaltour";

// class SpreadsheetFullTour ==================================================

/**
 * The guided tour for the Spreadsheet Portal application.
 */
export default class SpreadsheetFullTour extends PortalTour<SpreadsheetApp> {

    constructor(config?: PortalTourConfig) {

        // base constructor
        super(AppType.SPREADSHEET, config);

        // the initial welcome message
        this.welcomeStep({
            title: gt.pgettext("tour", "The Spreadsheet app"),
            content: gt.pgettext("tour", "Welcome to your new Spreadsheet app. This Guided Tour will introduce you to the most important functions of creating and editing spreadsheets online.")
        });

        // do not show the following steps in the short version of the tour
        if (!this.isShortTour()) {

            // highlight the header of the template area
            this.templatesAreaStep({
                title: gt.pgettext("tour", "Templates"),
                content: gt.pgettext("tour", "Clicking on the Spreadsheet app in the navigation bar displays an overview of all the templates available to you in Spreadsheet.")
            });

            // describe the "Blank document" button
            this.blankDocumentStep({
                title: gt.pgettext("tour", "Blank spreadsheet"),
                content: [
                    gt.pgettext("tour", "You can choose from a variety of templates to help you create professional documents."),
                    gt.pgettext("tour", "Let's start with a blank document.")
                ]
            });
        }

        // launch the editor application with a new empty document
        this.launchDocumentStep();

        // steps for internal editor applications
        if (!this.isWopiTour()) {

            // do not show the following steps in the short version of the tour
            if (!this.isShortTour()) {

                // insert document contents, show the "Saving changes" label
                this.autoSaveStep(() => this.docController.executeItem("cell/active/parse", "Lorem ipsum dolor sit amet."));

                // describe the "File" toolbar
                this.fileTabStep();
                this.saveAsStep();
                this.sendAsMailStep("button");
            }

            // start to type a cell formula
            const formulaStep = this.step({
                title: gt.pgettext("tour", "Formulas"),
                content: gt.pgettext("tour", "You can use all the regular spreadsheet formulas by typing them into a cell.")
            });
            formulaStep.on("before:show", jpromise.wrapFloating(async () => {
                this.docView.cancelTextEditMode();
                const formula = this.#getLocalizedFormula("'=$SUM");
                await this.docController.executeItem("cell/active/parse", formula);
            }));
            formulaStep.waitForAndReferTo(this.EDITOR_APPPANE_SELECTOR + " .selection-layer .active-cell", { position: "bottom", hotspot: true });

            // show auto-completion dropdown menu for functions
            const autoCompleteStep = this.step({
                title: gt.pgettext("tour", "Autocompletion"),
                content: gt.pgettext("tour", "As soon as you start typing a formula, autocompletion offers you a list of matching functions to choose from.")
            });
            autoCompleteStep.on("before:show", jpromise.wrapFloating(async () => {
                this.docView.cancelTextEditMode();
                const formula = this.#getLocalizedFormula("=$SUM");
                await this.docView.enterTextEditMode("cell", { text: formula, stickyList: true });
            }));
            autoCompleteStep.waitForAndReferToPopup({ position: "bottom", hotspot: true });

            // describe the formula label in the status pane
            const showFormulaStep = this.step({
                title: gt.pgettext("tour", "Displaying hidden formulas"),
                content: gt.pgettext("tour", "If a result is displayed in a cell, the calculating formula will be displayed in the formula bar.")
            });
            showFormulaStep.on("before:show", jpromise.wrapFloating(async () => {
                // need to launch explicitly, in case this step is reached by using the Back button in a short tour
                await this.launchEditorApp();
                this.docView.cancelTextEditMode();
                const formula = this.#getLocalizedFormula("=$SUM(1$SEP 2$SEP 3)");
                await this.docController.executeItem("cell/active/parse", formula);
            }));
            showFormulaStep.waitForAndReferTo(this.EDITOR_WINDOW_SELECTOR + " .formula-pane .textarea-container", { position: "bottom", hotspot: true });
        }

        // describe the "Quit" button
        if (!this.isShortTour()) {
            this.quitButtonStep();
        }

        // final tour step describing how to restart the tour (only when auto-started)
        this.restartTourStep();
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the localized version of the passed cell formula. Replaces the
     * string "$SEP" with the local function parameter separator (e.g. comma in
     * English UI, semicolon in German UI), and all other strings starting with
     * "$" followed by uppercase characters with the respective translated
     * function name (e.g. "$SUM" to English "SUM" and German "SUMME").
     */
    #getLocalizedFormula(formula: string): string {
        const formulaGrammar = this.docModel.formulaGrammarUI;
        return formula
            .replace(/\$SEP/g, formulaGrammar.SEP)
            .replace(/\$([A-Z]+)/g, (_match, token: string) => formulaGrammar.getFunctionName(token) || token);
    }
}
