/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

// class CommentLayer =========================================================

/**
 * An instance of this class represents the model for all comments in the
 * edited OX Presentation document.
 *
 * @param {PresentationModel} docModel
 *  The presentation model instance.
 */
class CommentLayer extends ModelObject {

    // a list of all comments (jQuery)
    #comments = [];
    // the undo manager
    #undoManager = null;

    constructor(docModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {

            // setting the global undo manager
            this.#undoManager = this.docModel.getUndoManager();

            // registering handler for preparations after successful document loading
            this.waitForImportSuccess(this.#importSuccessHandler);

            // updating the model after the document was reloaded (after cancelling long running actions)
            this.listenTo(this.docModel, 'document:reloaded', this.#handleDocumentReload);
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Whether the document contains comments in the document.
     *
     * @returns {Boolean}
     *  Whether the document contains at least one comment.
     */
    isEmpty() {
        return this.#comments.length === 0;
    }

    /**
     * Provides the array with all comments.
     *
     * @returns {jQuery[]}
     *  The list with all comments.
     */
    getComments() {
        return this.#comments;
    }

    /**
     * This function generates the operations for inserting a comment.
     * It does not modify the DOM. This function is only executed in the client with edit
     * privileges, not in remote clients.
     */
    insertComment() {}

    /**
     * Handler for insertComment operations. Inserts a comment node into the DOM.
     *
     * @param {Number[]} _start
     *  The logical start position for the new comment.
     *
     * @param {String} _id
     *  The unique id of the comment.
     *
     * @param {String} _author
     *  The author of the comment.
     *
     * @param {String} _userId
     *  The unique id for the author of the comment.
     *
     * @param {String} _date
     *  The date of the comment.
     *
     * @returns {Boolean}
     *  Whether the comment has been inserted successfully.
     */
    insertCommentHandler(_start, _id, _author, _userId, _date, _target) {
        // Handler function for insertComment operations in OX Presentation
        return true;
    }

    // private methods ----------------------------------------------------

    /**
     * Comment specific handling after reloading the document (for example after
     * canceling a long running action).
     */
    #handleDocumentReload() {}

    /**
     * Function that is executed after the 'import success' event of the application.
     */
    #importSuccessHandler() {
        if (this.#undoManager) {} // only for using the undoManager
    }

}

// export -----------------------------------------------------------------

export default CommentLayer;
