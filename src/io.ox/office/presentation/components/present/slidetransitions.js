/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { TOUCH_DEVICE } from '@/io.ox/office/tk/dom';

// constants ==================================================================

// the collection of all slide transitions
export const SLIDE_TRANSITIONS = [
    {
        id: 'animation_1',
        name: 'Slow horizontal transparency',
        tooltip: 'Horizontal slide animation running 2 seconds',
        duration: 1500,
        forward: {
            inClass: 'animate-left-in',
            outClass: 'animate-left-out'
        },
        backward: {
            inClass: 'animate-right-in',
            outClass: 'animate-right-out'
        }
    },
    {
        id: 'animation_2',
        name: 'Fast horizontal transparency',
        tooltip: 'Horizontal slide animation running 1 second',
        duration: 800,
        forward: {
            inClass: 'animate-left-in',
            outClass: 'animate-left-out'
        },
        backward: {
            inClass: 'animate-right-in',
            outClass: 'animate-right-out'
        }
    },
    {
        id: 'FAST_SWIPER',
        name: 'Horizontal swiper',
        tooltip: 'Swiper effect',
        restriction: 'TOUCHDEVICE',
        duration: 400,
        forward: {
            inClass: 'animate-left-in',
            outClass: 'animate-left-out'
        },
        backward: {
            inClass: 'animate-right-in',
            outClass: 'animate-right-out'
        }
    },
    {
        id: 'animation_3',
        name: 'Horizontal',
        tooltip: 'Shrinks and expands horizontally',
        duration: 1500,
        forward: {
            inClass: 'animate-expand-left',
            outClass: 'animate-shrink-right'
        },
        backward: {
            inClass: 'animate-expand-right',
            outClass: 'animate-shrink-left'
        }
    },
    {
        id: 'animation_4',
        name: 'Vertical',
        tooltip: 'Shrinks and expands vertically',
        duration: 1500,
        forward: {
            inClass: 'animate-expand-top',
            outClass: 'animate-shrink-bottom'
        },
        backward: {
            inClass: 'animate-expand-bottom',
            outClass: 'animate-shrink-top'
        }
    },
    {
        id: 'animation_5',
        name: 'Upper left corner',
        tooltip: 'Shrinks and expands in upperleft corner',
        duration: 1500,
        forward: {
            inClass: 'animate-expand-upperleft',
            outClass: 'animate-shrink-upperleft'
        },
        backward: {
            inClass: 'animate-expand-upperleft',
            outClass: 'animate-shrink-upperleft'
        }
    },
    {
        id: 'animation_6',
        name: 'Two corners',
        tooltip: 'Shrinks and expands in two corners',
        duration: 1500,
        forward: {
            inClass: 'animate-expand-upperleft',
            outClass: 'animate-shrink-lowerright'
        },
        backward: {
            inClass: 'animate-expand-lowerright',
            outClass: 'animate-shrink-upperleft'
        }
    },
    {
        id: 'animation_7',
        name: 'Center',
        tooltip: 'Shrinks and expands in center',
        duration: 1500,
        forward: {
            inClass: 'animate-expand-center',
            outClass: 'animate-shrink-center'
        },
        backward: {
            inClass: 'animate-expand-center',
            outClass: 'animate-shrink-center'
        }
    },
    {
        id: 'animation_8',
        name: 'Center rotation',
        tooltip: 'Shrinks and expands with rotation in center',
        duration: 1500,
        forward: {
            inClass: 'animate-expand-center-rotate-left',
            outClass: 'animate-shrink-center-rotate-left'
        },
        backward: {
            inClass: 'animate-expand-center-rotate-right',
            outClass: 'animate-shrink-center-rotate-right'
        }
    },
    {
        id: 'animation_9',
        name: 'Transparency',
        tooltip: 'Hide and show with changing transparency',
        duration: 1500,
        forward: {
            inClass: 'animate-show-transparency',
            outClass: 'animate-hide-transparency'
        },
        backward: {
            inClass: 'animate-show-transparency',
            outClass: 'animate-hide-transparency'
        }
    },
    {
        id: 'animation_10',
        name: '3d rotation',
        tooltip: 'Hide and show within a 3d rotation',
        duration: 1500,
        forward: {
            inClass: 'animate-in-rotatey-left',
            outClass: 'animate-out-rotatey-left'
        },
        backward: {
            inClass: 'animate-in-rotatey-right',
            outClass: 'animate-out-rotatey-right'
        }
    }
];

// the default slide transition
const DEFAULT_SLIDE_TRANSITION = TOUCH_DEVICE ? 'FAST_SWIPER' : null;

// private functions ==========================================================

/**
 * Finding a slide transition in the container of all slide transitions by its ID.
 *
 * @param {string|null} id
 *  The ID of the searched slide transition.
 *
 * @returns {Object|null}
 *  The slide transition, or null, if it could not be found.
 */
function getSlideTransitionById(id) {
    return (id && SLIDE_TRANSITIONS.find(slideTransition => slideTransition.id === id)) || null;
}

// class SlideTransitions =====================================================

/**
 * An instance of this class represents the container for the slide transition
 * objects.
 *
 * @param {PresentationApplication} app
 *  The application instance.
 */
export class SlideTransitions {

    constructor() {

        // the ID of the active slide transition
        this._activeTransition = null;
    }

    // public methods ---------------------------------------------------------

    /**
     * Setting the active slide transition. If the specified ID does not
     * exist, it is not accepted as new ID.
     *
     * @param {String|Null} [id]
     *  Setting an active slide transition. If null or omitted, the slide
     *  transition is removed.
     */
    setActiveSlideTransition(id) {
        this._activeTransition = getSlideTransitionById(id) ? id : null;
    }

    /**
     * Getting the ID of the active slide transition.
     *
     * @returns {String|Null}
     *  The ID of the active slide transition. Or null, if no ID is specified.
     */
    getActiveSlideTransitionId() {
        return this._activeTransition;
    }

    /**
     * Getting the definition of the active slide transition.
     *
     * @returns {Object|Null}
     *  The definition of the active slide transition. Or null, if no ID is specified.
     */
    getActiveSlideTransition() {
        return getSlideTransitionById(this._activeTransition);
    }

    /**
     * Check, whether there is an active slide transition.
     *
     * @returns {Boolean}
     *  Whether there is an active slide transition.
     */
    isActiveSlideTransition() {
        return this._activeTransition !== null;
    }

    /**
     * Setting a default slide transition.
     */
    activateDefaultSlideTransition() {
        this.setActiveSlideTransition(DEFAULT_SLIDE_TRANSITION);
    }
}
