/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import { locationHash } from '$/url';

import { jpromise } from '@/io.ox/office/tk/algorithms';

import { TOUCH_DEVICE, IOS_SAFARI_DEVICE, setFocus, hideFocus, KeyCode, hasKeyCode, isEscapeKey, createIcon,
    isFullscreenEnabled, isFullscreenActive, toggleFullscreen, setElementAttribute, addChangeFullscreenHandler, removeChangeFullscreenHandler
} from '@/io.ox/office/tk/dom';

import { EObject, debounceMethod } from '@/io.ox/office/tk/objects';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { QuitReason } from '@/io.ox/office/baseframework/utils/apputils';

import { getPageContentNode, isCursorKey } from '@/io.ox/office/textframework/utils/dom';
import { increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { CHROME_ON_ANDROID, getBooleanOption, getNumberOption, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import { SlideTransitions } from '@/io.ox/office/presentation/components/present/slidetransitions';

// constants ==================================================================

// The maximum time difference between touchstart and touchend event on
// a touch device, that leads to a change of slide.
const MAX_SWIPER_TIME = 300;
// the minimum number of pixel for a horizontal swiper movement on
// a touch device, that leads to a change of slide.
const MIN_SWIPER_DISTANCE = 20;
// the minimum number of pixel to show the cursor and the navigation panel
const MIN_MOVE_DISTANCE = 20;
// the duration for that the big fullscreen button shall be shown
const FULLSCREEN_BUTTON_TIME_START = 1000;
// the duration for that the big fullscreen button shall be shown
const FULLSCREEN_BUTTON_TIME_END = 10000;
// the radius of the laser pointer in px
const LP_RADIUS = 5;
// the minimum distance to a border in px
const MIN_DISTANCE = 6;
// the name of the class to mark several DOM nodes in presentation mode
const PRESENTATION_MODE_CLASS = 'presentationmode';
// the screen mode of the presentation: "full" or "window"
const SCREEN_MODE_ATTRIBUTE = 'data-screenmode';
// the class name for the 'previous' button
const PREV_CLASS_NAME = 'prev';
// the class name for the 'next' button
const NEXT_CLASS_NAME = 'next';
// the class name for the 'laser' button
const LASER_CLASS_NAME = 'laser';
// the class name for the 'fullscreen' button
const FULLSCREEN_CLASS_NAME = 'fullscreen';
// the class name for the 'leaving fullscreen' button
const LEAVE_FULLSCREEN_CLASS_NAME = 'leavefullscreen';
// the class name for the 'exit' button
const EXIT_CLASS_NAME = 'exit';
// the class name for every navigation button
const NAVIGATION_BUTTON_CLASS_NAME = 'navigationbutton';
// the time for hiding cursor and navigation panel if mouse is not moved
const MOUSE_MOVE_TIMER = 3000;

// class PresentationModeManager ===============================================

/**
 * An instance of this class represents the presentation mode manager. This manager
 * handles the presentation mode in the presentation app.
 *
 * @param {PresentationApplication} app
 *  The application instance.
 */
class PresentationModeManager extends EObject {

    // a reference to the application
    #app;
    // a reference to the model
    #model;
    // whether the presentation mode is active
    #presentationMode;
    // the node, that blocks the slides in the presentation mode
    #presentationBlockerNode;
    // the navigation panel node that always needs an expansion because of hover
    #navigationPanelNode;
    // the info text displayed for leaving presentation mode
    #lastSlideInfoNode;
    // the panel with buttons for mouse control
    #navigationPanel;
    // the laser pointer node
    #laserPointer;
    // the big button for activating full screen mode
    #bigFullscreenButton;
    // the parent node of the artificial slide borders (DOCS-2802, DOCS-2841)
    #borderParentNode;
    // the page node in the DOM
    #pagediv;
    // the content root node
    #contentRootNode;
    // the height of the topbar node
    #topbarHeight;
    // the ID of that slide, that was active before switiching into the presentation mode
    #lastActiveSlideID;
    // the ID of the first slide shown in the presentation mode
    #startSlideID;
    // whether full screen shall be used
    #useFullscreen;
    // marker for the black screen after the final slide in presentation mode
    #isFinalScreen;
    // whether the presentation is completely empty
    #isEmptyPresentation;
    // whether slide effects are possible (not for the first or the last slide)
    #slideEffectsPossible;
    // whether the laser pointer is active
    #isActiveLaserPointer;
    // the laser button in the navigation panel
    #laserButtonNode;
    // the full screen button in the navigation panel
    #fullscreenButtonNode;
    // the leave full screen button in the navigation panel
    #leaveFullscreenButtonNode;
    // a collector for mouse move events
    #lastMouseMoveEvent;
    // whether the app was started directly in the presentation mode
    #startInPresentationMode;
    // whether leaving the fullscreen mode is currently active
    #leaveFullscreenActive;
    // the time of the touchstart event
    #touchStartTime;
    // the horizontal position of the touchstart event
    #horizontalTouchStartValue = -1;
    // the vertical position of the touchstart event
    #verticalTouchStartValue;
    // whether the contenteditable value for the 'page' node was modified
    #pageNodeModified;
    // whether the contenteditable value for the 'app-content' node was modified
    #appContentNodeModified;
    // whether the contenteditable value for the clipboard node was modified
    #clipboardNodeModified;
    // whether a keyboard listener is registered at the document
    // -> this is required for the final screen in Chrome and Edge browser
    #documentListenerRegistered;
    // whether the mousedown event happened on the navigation panel
    #mouseDownOnPanel;
    // the collector for the slide transitions
    #slideTransitions;
    // the timer that reacts to mouse movements
    #mouseMoveTimer;
    // whether the cursor is hidden
    #isHiddenCursor;
    // the position of the cursor, when the cursor was hidden
    #cursorHidePosition;
    // whether all slides of the presentation are hidden, this is handled like no slide is hidden
    #showHiddenSlides;
    // whether there is an active change from or to the full screen mode
    #activeFullscreenSwitch;
    // a timer for fast updating the borders around the slide
    #updateBorderTimer;
    // a timer for slower update of the borders around the slide
    #updateBorderTimerMedium;
    // a timer for long update of the borders around the slide
    #updateBorderTimerLong;

    constructor(app) {

        // base constructor
        super();

        this.#app = app;
        this.#presentationMode = false;
        this.#topbarHeight = 0;
        this.#useFullscreen = true;
        this.#isFinalScreen = false;
        this.#isEmptyPresentation = false;
        this.#slideEffectsPossible = false;
        this.#isActiveLaserPointer = false;
        this.#startInPresentationMode = false;
        this.#leaveFullscreenActive = false;
        this.#touchStartTime = 0;
        this.#horizontalTouchStartValue = -1;
        this.#verticalTouchStartValue = -1;
        this.#pageNodeModified = false;
        this.#appContentNodeModified = false;
        this.#clipboardNodeModified = false;
        this.#documentListenerRegistered = false;
        this.#mouseDownOnPanel = false;
        this.#isHiddenCursor = false;
        this.#showHiddenSlides = false;
        this.#activeFullscreenSwitch = false;

        // initialization -----------------------------------------------------

        this.#app.onInit(() => {
            // the application model
            this.#model = this.#app.getModel();
            // setting the height of the top bar node
            this.#topbarHeight = $('#io-ox-appcontrol').height();
            // initializing the collector for the slide transitions
            this.#slideTransitions = new SlideTransitions();
            // setting the default slide transition
            this.#slideTransitions.activateDefaultSlideTransition();
            // registering the handler for drawing artificial borders around the slide
            this.listenTo(this.#app.getView(), 'refresh:layout', this.#updateSlideBordersDeferred);
            // clean up tasks when the document is closed (in presentation mode)
            this.listenTo(this.#app, 'docs:beforequit', this.#documentQuitHandler);
        }, this);
    }

    // methods ------------------------------------------------------------

    /**
     * Check, whether the presentation mode is active
     *
     * @returns {Boolean}
     *  Whether the presentation mode is active.
     */
    isPresentationMode() {
        return this.#presentationMode;
    }

    /**
     * Whether the presentation shall be started in full screen mode.
     *
     * @returns {Boolean}
     *  Whether the presentation shall be started in full screen mode.
     */
    useFullscreenPresentation() {
        return this.#useFullscreen;
    }

    /**
     * Setting whether the presentation shall be started in full screen mode.
     *
     * @param {Boolean} value
     *  Setting whether the presentation shall be started in full screen mode.
     */
    setFullscreenPresentation(value) {
        this.#useFullscreen = value;
    }

    /**
     * Getter for the instance of the slideTransitions class.
     *
     * @returns {Object}
     *  Returns the instance of the slideTransitions class.
     */
    getSlideTransitions() {
        return this.#slideTransitions;
    }

    /**
     * Whether the presentation shall use slide effects.
     *
     * @returns {Boolean}
     *  Whether the presentation shall use slide effects.
     */
    useSlideEffects() {
        return this.#slideTransitions.isActiveSlideTransition();
    }

    /**
     * Getting the ID of the active slide transition.
     *
     * @returns {String|Null}
     *  The ID of the active slide transition. Or null, if no ID is specified.
     */
    getActiveSlideEffectId() {
        return this.#slideTransitions.getActiveSlideTransitionId();
    }

    /**
     * Setting the ID of the slide effect in presentation mode.
     *
     * @param {String} id
     *  Setting the ID of the slide effect in presentation mode.
     */
    setSlideEffect(id) {
        this.#slideTransitions.setActiveSlideTransition(id);
    }

    /**
     * Handler function for the key down event that needs to be handled in the presentation mode.
     *
     * @param {jQuery.Event} event
     *  A jQuery keyboard event object.
     */
    handleKeydownInPresentationMode(event) {
        this.#handleSlideChangeEvent(event);
    }

    /**
     * Whether the animation in a slide change shall be shown.
     *
     * @returns {Boolean}
     *  Whether the animation in a slide change shall be shown.
     */
    showSlideAnimation() {
        return this.#presentationMode && this.#slideEffectsPossible && this.useSlideEffects();
    }

    /**
     * Getting an object that specfies the animation classes and animation
     * duration for the slide change.
     *
     * @returns {Object}
     *  An object describing a slide transition. It contains the properties
     * 'forward' and 'backward' that contains information for a slide change
     * in forward direction (slide 3 to slide 4) and backward direction
     * (slide 4 to slide 3).
     */
    getActiveSlideAnimation() {
        return this.#slideTransitions.getActiveSlideTransition();
    }

    /**
     * Starting the presentation mode.
     *
     * @param {Object|Boolean} [options]
     *  This object is specified, if the application was started with launch options
     *  with the key 'presentationMode'. If this option is set, OX Presentation starts in
     *  presentation mode. If the value is set to true, the default values for the
     *  options are used. If the value is an object, it is possible to specify the
     *  options for the presentation mode.
     *  Optional parameters:
     *  @param {Boolean} [options.fullscreen=true]
     *   Whether the presentation mode shall be started in full screen mode.  This is
     *   only evaluated, if "startInPresentationMode" is true.
     *  @param {Number} [options.startSlide=0]
     *   The zero-base position of the slide with that the presentation starts. This is
     *   only evaluated, if "startInPresentationMode" is true.
     *  @param {Boolean} [options.startInPresentationMode=false]
     *   Whether the presentation was started externally (from portal or drive)
     *   or internally from OX Presentation. Default is 'false' meaning the presentation
     *   was started from OX Presentation.
     *  @param {String} [options.startMode=null]
     *   The mode to specify with which slide the presentation starts. Allowed values
     *   are 'FIRST' and 'CURRENT'.
     *
     * @returns {Promise}
     *  A promise that will be resolved when the presentation mode starts in full screen mode
     *  and the Fullscreen API resolves the promise. If the presentation does not start in
     *  full screen mode, a resolved promise is returned.
     */
    startPresentation(options) {

        // an optional start slide specified in the launch options (as number value)
        let launchOptionsStartSlide = 0;
        // the application view
        const docView = this.#app.docView;
        // whether the presentation shall be started at the first slide
        let useFirstSlide = true;
        // optionally a start mode for the first or current slide can be specified
        const startMode = getStringOption(options, 'startMode', null);
        // the promise for activating the full screen mode
        let startPresentationPromise = null;

        // whether the application was started externally from drive or portal
        this.#startInPresentationMode = getBooleanOption(options, 'startInPresentationMode', false);

        // check, whether all slides are hidden. In this case, all slides are shown as if no slide is hidden
        this.#showHiddenSlides = this.#model.allSlidesHidden();
        // check whether there is at least one slide available
        this.#isEmptyPresentation = this.#model.isEmptyPresentation();

        this.#setPresentationMode(true); // setting marker for active presentation mode

        if (_.isString(startMode)) { useFirstSlide = (startMode === 'FIRST'); }

        this.#pagediv = this.#model.getNode();
        this.#contentRootNode = docView.getContentRootNode();

        // registering the handler for changes of the full screen mode at the content root node
        addChangeFullscreenHandler(this.#contentRootNode[0], () => this.#fullscreenChangeHandler());

        this.#lastActiveSlideID = this.#model.getActiveSlideId(); // saving slide ID to restore it later

        // evaluating the start slide, if presentation is started with document launch options
        if (this.#startInPresentationMode) {
            launchOptionsStartSlide = _.isObject(options) ? getNumberOption(options, 'startSlide', 0) : 0;
            this.#startSlideID = this.#model.getSlideIdByPosition(launchOptionsStartSlide);
            if (!this.#startSlideID) { this.#startSlideID = this.#model.getFirstStandardSlideId(); }
        } else {
            this.#startSlideID = useFirstSlide ? this.#model.getFirstStandardSlideId() : this.#lastActiveSlideID; // Switch: Using current or first slide
        }

        if (this.#model.isHiddenSlide(this.#startSlideID) && !this.#showHiddenSlides) { this.#startSlideID = this.#model.findVisibleNeighbourSlide(this.#startSlideID); } // there must be at least one visible slide

        if (this.#lastActiveSlideID !== this.#startSlideID) { this.#model.setActiveSlideId(this.#startSlideID); } // activating the slide, if required

        this.#updateURL(); // improving reload handling

        this.#slideEffectsPossible = true; // after setting the first slide, it is possible to use slide effects

        this.#model.getSelection().setSlideSelection(); // cleaning existing selections

        docView.setVirtualFocusSlidepane(false); // avoiding, that the slide pane gets the key events for cursor keys

        // it seems as if all browsers cannot be started externally in full screen mode
        if (this.#startInPresentationMode || IOS_SAFARI_DEVICE) { this.setFullscreenPresentation(false); }
        // but on Android devices the full screen mode can be started externally -> forcing full screen
        if (this.#startInPresentationMode && CHROME_ON_ANDROID) { this.setFullscreenPresentation(true); }

        this.#prepareDomNodesForPresentationMode(); // modifying DOM nodes, registering handlers, creating navigation panel

        this.#prepareInternalPresentationScreen(); // increasing app-pane node in size and z-index, in fullscreen and in non-fullscreen mode

        if (this.#useFullscreen && isFullscreenEnabled()) {
            startPresentationPromise = this.#changeFullscreenMode(true);
        } else {
            startPresentationPromise = Promise.resolve();
        }

        startPresentationPromise.then(() => {
            // scrolling to top position, so that presentation can start with non-first slide, too
            this.#contentRootNode.scrollTop(0);
            // optimizing the size of the slide in presentation mode
            docView.changeZoom('slide');
            // drawing artificial borders around the slide
            this.#updateSlideBordersDeferred();
            // in some browsers the focus is set into the body, so that keyboard events are not handled correctly (Chrome and Edge)
            this.#setFocusIntoPageIfRequired();
            // updating the visibility of the full-screen buttons in the navigation panel
            this.#updateFullscreenButtons();
            // starting the presentation without cursor and navigation panel
            this.#hideCursorAndNavigationPanel();
            // showing for some seconds the button to activate the full screen mode
            if (this.#startInPresentationMode && !isFullscreenActive() && !TOUCH_DEVICE) { this.#activateBigFullscreenButton(); }
        });

        return startPresentationPromise;
    }

    // private methods ----------------------------------------------------

    /**
     * Setting the presentation mode
     *
     * @param {Boolean} value
     *  Setting the presentation mode active.
     */
    #setPresentationMode(value) {
        this.#presentationMode = value;
        this.#updateURL();
        document.documentElement.classList.toggle("io-ox-office-presentation-mode", value);
    }

    /**
     * Improving the reload handling by updating the URL, when presentationMode status is
     * changed or the visible slide has changed.
     *
     * Updating the presentation mode in the URL to improve the reload and to avoid that
     * after closing the presentation mode, this mode is still active in the presentation
     * portal, so that all files are opened in presentation mode.
     */
    #updateURL() {
        if (this.#presentationMode) {
            locationHash('presentationMode', true);
            locationHash('startSlide', this.#model.getActiveSlideIndex());
        } else {
            locationHash('presentationMode', null);
            locationHash('startSlide', null);
        }
    }

    /**
     * The success handler after toggle the full screen mode using the Fullscreen API.
     */
    #toggleFullscreenSuccessHandler() {
        this.#activeFullscreenSwitch = false;
    }

    /**
     * The error handler after toggle the full screen mode using the Fullscreen API.
     *
     * @param {boolean} state
     *  Whether the full screen state was requested by the Fullscreen API.
     *
     * @param {String} err
     *  The reason sent by the Fullscreen API.
     */
    #toggleFullscreenFailureHandler(state, err) {
        if (state) {
            globalLogger.warn('PresentationModeManager: Failed to switch to full screen mode: ', err);
            this.#toggleFullscreenInPresentationMode(false); // -> switching to non full screen mode
        } else {
            globalLogger.warn('PresentationModeManager: Failed to switch to non full screen mode: ', err);
        }
        this.#activeFullscreenSwitch = false;
    }

    /**
     * Toggle the fullscreen mode using the Fullscreen API.
     *
     * Info:
     * This function can only be called from a user-initiated event (click, key, or touch event),
     * otherwise the browser will deny the request.
     *
     * FULLSCREEN_TODO: Handle asynchronous errors within Fullscreen API promise (see testrunner test 36).
     *
     * @param {Boolean|undefined} state
     *  Starting, leaving or toggling full screen mode. A state of 'true' starts full screen
     *  mode, 'false' exits the full screen mode and 'undefined' toggles the full screen state.
     *
     * @returns {Promise}
     *  A promise that will be resolved when the Fullscreen API successfully successfully switched
     *  the full screen mode. If this fails, the promise is rejected. If there is nothing to do, a
     *  resolved promise is returned.
     */
    async #changeFullscreenMode(state) {

        if (this.#activeFullscreenSwitch) { return; }

        if (state === undefined) { state = !isFullscreenActive(); }

        // using the fullscreen API it can be checked, whether the full screen mode is active or whether it can be enabled
        if ((state && isFullscreenActive()) || (!state && !isFullscreenActive()) || (state && !isFullscreenEnabled())) { return; }

        if (_.device('!iOS') && !this.#activeFullscreenSwitch) {
            this.#activeFullscreenSwitch = true;
            try {
                await toggleFullscreen(this.#contentRootNode[0], state);
                this.#toggleFullscreenSuccessHandler();
            } catch (err) {
                this.#toggleFullscreenFailureHandler(state, err);
            }
        }
    }

    /**
     * Checking whether the specified event is the keyboard event for toggling the
     * full screen mode in the presentation mode.
     *
     * @returns {Boolean}
     *  Whether the specified event is the keyboard event for toggling the full screen
     *  mode in the presentation mode.
     */
    #isToggleFullScreenEvent(event) {
        return event.ctrlKey && event.shiftKey && event.keyCode === 70; // ctrl-shift-F
    }

    /**
     * Helper function to restore the modified screen nodes after leaving the presentation mode.
     *
     * @returns {Promise}
     *  A promise that will be resolved when the Fullscreen API successfully exited the
     *  full screen mode. If this fails, the promise is rejected. If the fullscreen mode
     *  is not active, a resolved promise is returned.
     */
    async #leavePresentationScreenMode() {
        if (isFullscreenActive()) { await this.#changeFullscreenMode(false); }
        this.#app.getWindowNode().removeClass(PRESENTATION_MODE_CLASS);
    }

    /**
     * Helper function to prepare the DOM for the presentation mode. The app-pane node fills the
     * complete area inside the app and gets a very high z-index so that all other panes are
     * below. For smarter and faster changes between fullscreen and none-fullscreen mode, this
     * css styling is also used in full screen mode.
     */
    #prepareInternalPresentationScreen() {
        this.#app.getWindowNode().addClass(PRESENTATION_MODE_CLASS); // increasing app-pane node in size and z-index.
    }

    /**
     * Handler for the attribute "data-screenmode" that is attached to the app content root node.
     * This attribute can be used for example in tests, to check the current screen mode.
     *
     * @param {boolean} isActivePresentation
     *  Whether the presentation mode is active.
     */
    #updateScreenModeAttribute(isActivePresentation) {
        const value = isActivePresentation ? (this.#useFullscreen ? "full" : "window") : null;
        setElementAttribute(this.#contentRootNode[0], SCREEN_MODE_ATTRIBUTE, value);
    }

    /**
     * Setting or removing a marker class to specified DOM nodes and changing the visibility
     * of DOM nodes.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.presentationMode=true]
     *   Whether the classes shall be added to or removed from the specific DOM nodes.
     */
    #handleDomMarkerClassAndVisibility(options) {

        const startPresentationMode = getBooleanOption(options, 'presentationMode', true);

        this.#pagediv.toggleClass(PRESENTATION_MODE_CLASS, startPresentationMode);
        this.#contentRootNode.toggleClass(PRESENTATION_MODE_CLASS, startPresentationMode);
        getPageContentNode(this.#pagediv).toggleClass(PRESENTATION_MODE_CLASS, startPresentationMode);
        // 'overflow: hidden' at the slide (no content outside the slide in presentation mode) and the scrollable app-content-root node
        // (no scroll bars in presentation mode) leads to the problem that the text in drawings is often rendered behind the canvas node.
        // This problem is (currently) fixed by adding the negative z-index to the canvas elements inside drawings.

        // updating the attribute that specifies the screen mode at the content root node
        this.#updateScreenModeAttribute(startPresentationMode);

        // hiding additional nodes, not only in non-fullscreen mode
        this.#pagediv.children('.collaborative-overlay').toggle(!startPresentationMode); // -> no remote selection in presentation mode
    }

    /**
     * Helper function to generate the complete button panel for navigation in the
     * presentation mode with the mouse.
     *
     * @returns {jQuery}
     *  The button panel with all handlers registered, but not added to the DOM.
     */
    #createNavigationPanel() {

        // the hidden or visible content node that contains the buttons
        const panelContent = $('<div class="navigationcontent">');
        // the buttons for previous slide, next slide and leaving presentation
        const prevButtonNode = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${PREV_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:caret-left-fill'));
        const nextButtonNode = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${NEXT_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:caret-right-fill'));
        const exitButtonNode = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${EXIT_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:stop-fill'));
        this.#laserButtonNode = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${LASER_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:sun-fill'));
        this.#fullscreenButtonNode = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${FULLSCREEN_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:arrows-angle-expand'));
        this.#leaveFullscreenButtonNode = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${LEAVE_FULLSCREEN_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:arrows-angle-contract'));

        // adding tooltips to buttons (or not)
        // Forms.setToolTip(prevButtonNode, gt('Change to previous slide'));
        // Forms.setToolTip(nextButtonNode, gt('Change to next slide'));
        // Forms.setToolTip(exitButtonNode, gt('Exit presentation'));

        // preparing the node for the buttons
        panelContent.append(prevButtonNode);
        panelContent.append(nextButtonNode);
        panelContent.append(this.#laserButtonNode);
        panelContent.append(this.#fullscreenButtonNode);
        panelContent.append(this.#leaveFullscreenButtonNode);
        panelContent.append(exitButtonNode);

        // the panel node that always needs an expansion because of hover
        this.#navigationPanelNode = $('<div class="blockernavigation">');

        this.#navigationPanelNode.append(panelContent);

        // adding visibility handler for the navigation panel
        this.#navigationPanelNode.on('mouseenter', () => {
            this.#presentationBlockerNode.addClass('forcecursor');
            this.#laserPointer.addClass('hidelaserpointer');
        });
        this.#navigationPanelNode.on('mouseleave', () => {
            this.#presentationBlockerNode.removeClass('forcecursor');
            this.#laserPointer.removeClass('hidelaserpointer');
        });

        // adding handler for the buttons
        panelContent.on('mousedown mouseup touchstart touchend', evt => this.#navigationPanelClickHandler(evt)); // registering the handler for the complete panel

        return this.#navigationPanelNode;
    }

    /**
     * Helper function to generate the big button for activating the
     * full screen mode.
     *
     * @returns {jQuery}
     *  The button DOM node, not added to the DOM.
     */
    #createBigFullscreenButton() {
        const _bigFullscreenButton = $('<div>').addClass('bigfullscreennode');
        const bigFullscreenButtonLink = $(`<a class="${NAVIGATION_BUTTON_CLASS_NAME} ${FULLSCREEN_CLASS_NAME}" tabindex="0">`).append(createIcon('bi:arrows-angle-expand'));
        _bigFullscreenButton.append(bigFullscreenButtonLink);

        // adding handler for the buttons
        _bigFullscreenButton.on('mousedown mouseup touchstart touchend', evt => this.#navigationPanelClickHandler(evt)); // registering the handler for the button

        // button is not visible
        _bigFullscreenButton.toggle(false);

        return _bigFullscreenButton;
    }

    /**
     * Helper function to toggle between usage of laser pointer and usage of cursor.
     */
    #toggleLaserPointer() {
        if (this.#isActiveLaserPointer) {
            this.#isActiveLaserPointer = false;
            this.#presentationBlockerNode.removeClass('hidecursor');
            this.#laserPointer.removeClass('showlaserpointer');
            this.#navigationPanelNode.removeClass('lasermode');
            this.#laserButtonNode.removeClass('selected');
        } else {
            this.#isActiveLaserPointer = true;
            this.#presentationBlockerNode.addClass('hidecursor');
            this.#laserPointer.addClass('showlaserpointer');
            this.#navigationPanelNode.addClass('lasermode'); // some more space to activate the navigation panel
            this.#laserButtonNode.addClass('selected');
        }
    }

    /**
     * Toggling between the fullscreen mode and the non-fullscreen mode while the
     * presentation mode is active.
     *
     * This is especially required for Safari, because it does not allow to start
     * a presentation in full scree mode if it is activated externally with
     * launch options, for example as mail or calendar attachment.
     *
     * @param {Boolean|undefined} newState
     *  The new state for the full screen in presentation mode. A state of 'true'
     *  starts the full screen mode, 'false' exits the full screen mode.
     *
     * @returns {Promise | void} // TODO: check-void-return
     *  A promise that will be resolved when the Fullscreen API successfully toggled
     *  the full screen mode. If this fails, the promise is rejected. If no toggling
     *  is required, a resolved promise is returned.
     */
    #toggleFullscreenInPresentationMode(newState) {

        if (!this.#presentationMode) { return; }

        let togglePromise = null;

        if (newState && !isFullscreenActive() && isFullscreenEnabled()) {
            this.setFullscreenPresentation(true);
            this.#bigFullscreenButton.toggle(false);
            togglePromise = this.#changeFullscreenMode(true);
        } else if (!newState && isFullscreenActive()) {
            this.#leaveFullscreenActive = true; // avoiding leaving the presentation mode in the following 'fullscreenChangeHandler'
            this.setFullscreenPresentation(false);
            if (this.#documentListenerRegistered) { this.#removeDocumentListener(); }
            this.setTimeout(() => { this.#leaveFullscreenActive = false; }, 2000);
            togglePromise = this.#changeFullscreenMode(false);
        }

        if (togglePromise) {
            togglePromise.then(() => {
                this.#setFocusIntoPageIfRequired();
                this.#updateScreenModeAttribute(true); // updating the screen mode attribute at the app content root node
                this.#updateFullscreenButtons(); // updating the buttons in the navigation panel
                this.#updateSlideBordersDeferred(); // updating the slide borders
            });
        }

        return togglePromise ? togglePromise : Promise.resolve();
    }

    /**
     * Toggling the current full screen mode in the presentation mode.
     */
    #toggleCurrentFullScreenMode() {
        if (isFullscreenActive()) {
            this.#toggleFullscreenInPresentationMode(false);
        } else {
            this.#toggleFullscreenInPresentationMode(true);
        }
    }

    /**
     * Helper function for "updateFullscreenButtons".
     */
    #doUpdateButtons() {
        const isFullscreenMode = isFullscreenActive();
        this.#fullscreenButtonNode.toggle(!isFullscreenMode);
        this.#leaveFullscreenButtonNode.toggle(isFullscreenMode);
    }

    /**
     * Updating the visibility of the two buttons that switch to full screen mode
     * or leave the full screen mode, while the presentation is running.
     */
    #updateFullscreenButtons() {
        if (_.browser.Safari) { // DOCS-4692, mus be async in Safari browser
            this.setTimeout(this.#doUpdateButtons, 0);
        } else {
            this.#doUpdateButtons();
        }
    }

    /**
     * Helper function to generate the point of the laser pointer.
     *
     * @returns {jQuery}
     *  The laser pointer DOM node, not added to the DOM.
     */
    #createLaserPointer() {
        const pointer = $('<div>').addClass('laserpointer');
        return pointer;
    }

    /**
     * Helper function to register and deregister the event handlers
     * at the presentation blocker node.
     *
     * @param {jQuery} node
     *  The jQueryfied node for the handler functions.
     *
     * @param {Boolean} add
     *  Whether the handler functions shall be added (true) to or
     *  removed (false) from the node
     */
    #addOrRemoveHandler(node, add) {

        const funcName = add ? 'on' : 'off';

        // registering or deregistering the handler at the specified node
        node[funcName]({ 'mousedown touchstart': evt => this.#mouseDownHandler(evt) });
        node[funcName]({ 'mouseup touchend': evt => this.#mouseUpHandler(evt) });
        node[funcName]({ mousemove: evt => this.#saveMouseMoveEvent(evt) });
    }

    /**
     * Aborting the interval timers for the update of the borders around the slide.
     */
    #abortBorderTimers() {
        if (this.#updateBorderTimer) { this.#updateBorderTimer.abort(); }
        if (this.#updateBorderTimerMedium) { this.#updateBorderTimerMedium.abort(); }
        if (this.#updateBorderTimerLong) { this.#updateBorderTimerLong.abort(); }
    }

    /**
     * Drawing 4 artificial borders around the slide to avoid 'overflow: hidden' at the slide.
     * See description at function 'updateSlideBorders'.
     *
     * The toggling from and to the full screen mode might take some time. Therefore it is
     * important to update the black borders even if this toggling takes some seconds. Because
     * it is not clear when the browser rendered the slides correctly, a border update needs
     * to be triggered after some seconds.
     *
     * This function is called:
     * - when the presentation starts
     * - when the fullscreen mode is toggled in presentation mode
     * - when the browser size changes (event 'refresh:layout')
     */
    #updateSlideBordersDeferred() {

        this.#abortBorderTimers(); // stopping current border timers

        if (!this.isPresentationMode()) { return; }

        // one timer for fast border update in the first second
        this.#updateBorderTimer = this.setInterval(this.#updateSlideBorders, { delay: 0, interval: 100, count: 10 });
        // one timer for medium update after the first second
        this.#updateBorderTimerMedium = this.setInterval(this.#updateSlideBorders, { delay: 1000, interval: 500, count: 10 });
        // one timer for long updates
        this.#updateBorderTimerLong = this.setInterval(this.#updateSlideBorders, { delay: 7000, interval: 2000, count: 10 });
    }

    /**
     * Clean up tasks, when the document is closed (in presentation mode).
     */
    #documentQuitHandler(reason) {
        // removing marker for active presentation mode and update the URL (DOCS-4703)
        if (this.#presentationMode && reason !== QuitReason.RELOAD) { this.#setPresentationMode(false); }
    }

    /**
     * Chrome, Edge and Safari sometimes render the text behind the canvas in presentation mode (also
     * DOCS-2802, DOCS-2841).
     *
     * An overflow hidden at the slide would be the best solution to cut all content around the slides.
     * But unfortunately this has a problem on all Chromium browsers. There seems to be a problem with
     * two 'overflow: hidden' (at content-root-node and at the slide). Sometimes the text vanishes
     * behind their canvas elements or other drawings (DOCS-2802, DOCS-2841).
     *
     * Therefore the current solution is an artificial border around the slide. But this should be
     * removed, when the Chromium problem no longer occurs (Chrome, Edge and Safari are affected).
     */
    #updateSlideBorders() {

        if (this.#borderParentNode) { this.#borderParentNode.children().remove(); }

        // attaching 4 borders to the borderParentNode around the slide -> simulating 'overflow: hidden'
        // at the slide.
        if (this.isPresentationMode()) {

            this.#contentRootNode.css('padding-top', '');

            const leftBorderNode = $('<div>').addClass('verticalborder slideborder upperleft');
            const rightBorderNode = $('<div>').addClass('verticalborder slideborder bottomright');
            const topBorderNode = $('<div>').addClass('horizontalborder slideborder upperleft');
            const bottomBorderNode = $('<div>').addClass('horizontalborder slideborder bottomright');

            const zoomFactor = this.#app.docView.getZoomFactor();

            const contentRootPos = this.#contentRootNode.offset();
            const slideNode = this.#model.getNode();
            const pagePos = slideNode.offset();

            const leftThickness = Math.abs(pagePos.left - contentRootPos.left);
            let topThickness = Math.abs(pagePos.top - contentRootPos.top);
            const rightThickness = Math.abs(this.#contentRootNode.width() - slideNode.width() * zoomFactor  - leftThickness);
            let bottomThickness = Math.abs(this.#contentRootNode.height() - slideNode.height() * zoomFactor - topThickness);

            // taking care of vertical centering
            if (bottomThickness > (2 * MIN_DISTANCE) && leftThickness < MIN_DISTANCE && rightThickness < MIN_DISTANCE && topThickness < MIN_DISTANCE) {
                bottomThickness /= 2;
                topThickness = bottomThickness;
                this.#contentRootNode.css('padding-top', topThickness);
            }

            leftBorderNode.width(leftThickness);
            rightBorderNode.width(rightThickness);
            topBorderNode.height(topThickness);
            bottomBorderNode.height(bottomThickness);

            this.#borderParentNode.append(leftBorderNode);
            this.#borderParentNode.append(rightBorderNode);
            this.#borderParentNode.append(topBorderNode);
            this.#borderParentNode.append(bottomBorderNode);
        }
    }

    /**
     * Helper function to insert the blocker node, to prepare the contentRootNode
     * and to add listeners when starting the presentation mode.
     */
    #prepareDomNodesForPresentationMode() {

        // the clipboard node
        let clipboardNode = null;

        this.#presentationBlockerNode = $('<div>').addClass('presentationblocker');

        // adding child node with text to leave presentation
        this.#lastSlideInfoNode = $('<div>').addClass('blockerinfonode').text(gt('Click to leave presentation ...'));
        this.#presentationBlockerNode.append(this.#lastSlideInfoNode);

        // also appending the buttons for mouse navigation
        this.#navigationPanel = this.#createNavigationPanel();
        this.#presentationBlockerNode.append(this.#navigationPanel);

        this.#laserPointer = this.#createLaserPointer();
        this.#presentationBlockerNode.append(this.#laserPointer);

        this.#bigFullscreenButton = this.#createBigFullscreenButton();
        this.#presentationBlockerNode.append(this.#bigFullscreenButton);

        this.#borderParentNode = $('<div>').addClass('borderparent');
        this.#presentationBlockerNode.append(this.#borderParentNode);

        // setting blocker node
        this.#contentRootNode.prepend(this.#presentationBlockerNode);

        // adding the handler functions
        this.#addOrRemoveHandler(this.#presentationBlockerNode, true);

        this.#handleDomMarkerClassAndVisibility({ presentationMode: true });

        // avoiding contenteditable on touch devices -> keyboard will be opened
        if (TOUCH_DEVICE) {
            this.#pageNodeModified = false;
            this.#appContentNodeModified = false;
            if (this.#model.getNode().attr('contenteditable') === 'true') {
                this.#model.getNode().attr('contenteditable', 'false');
                this.#pageNodeModified = true;
            }
            if (this.#contentRootNode.children('app-content').attr('contenteditable') === 'true') {
                this.#contentRootNode.children('app-content').attr('contenteditable', 'false');
                this.#appContentNodeModified = true;
            }
            clipboardNode = this.#model.getSelection().getClipboardNode();
            if (clipboardNode.attr('contenteditable') === 'true') {
                clipboardNode.attr('contenteditable', 'false');
                this.#clipboardNodeModified = true;
            }
        }
    }

    /**
     * Helper function to restore the DOM nodes when leaving the presentation mode.
     */
    #restoreDomNodesAfterPresentationMode() {

        if (this.#presentationBlockerNode) {
            this.#addOrRemoveHandler(this.#presentationBlockerNode, false);
            this.#presentationBlockerNode.remove();
        }
        this.#presentationBlockerNode = null;

        this.#handleDomMarkerClassAndVisibility({ presentationMode: false });

        this.#contentRootNode.css('padding-top', '');

        this.#pagediv.toggle(true); // making page visible again (it was hidden, when the final screen was visible before)
    }

    /**
     * Setting the focus and the contenteditable values after the presentation mode. This needs to be asynchronous to
     * avoid that the keyboard pops up.
     */
    #restoreContentEditableOnTouch() {
        hideFocus(); // hiding the focus on touch devices
        if (this.#clipboardNodeModified) { this.#model.getSelection().getClipboardNode().attr('contenteditable', 'true'); }
        if (this.#pageNodeModified) { this.#model.getNode().attr('contenteditable', 'true'); }
        if (this.#appContentNodeModified) { this.#contentRootNode.children('app-content').attr('contenteditable', 'true'); }
        this.#clipboardNodeModified = false;
        this.#pageNodeModified = false;
        this.#appContentNodeModified = false;
    }

    /**
     * The global handler function that handles all events on the navigation panel.
     *
     * @param {jQuery.Event} event
     *  The mousedown/touchstart event.
     */
    #navigationPanelClickHandler(event) {

        if (event.type === 'mousedown' || event.type === 'touchstart') {

            // the click target
            let target = $(event.target);

            // checking for class 'NAVIGATION_BUTTON_CLASS_NAME'
            if (!target.hasClass(NAVIGATION_BUTTON_CLASS_NAME)) {
                target = target.closest(`.${NAVIGATION_BUTTON_CLASS_NAME}`);
            }

            if (target.hasClass(LASER_CLASS_NAME)) {
                this.#toggleLaserPointer();
            } else if (target.hasClass(FULLSCREEN_CLASS_NAME) && isFullscreenEnabled()) {
                this.#toggleFullscreenInPresentationMode(true);
                if (_.browser.Chrome || _.browser.Edge) { this.#mouseDownOnPanel = true; } // after switching to full screen, the mouseup is still triggered -> needs to be ignored
            } else if (target.hasClass(LEAVE_FULLSCREEN_CLASS_NAME)) {
                this.#toggleFullscreenInPresentationMode(false);
            } else if (target.hasClass(PREV_CLASS_NAME)) {
                this.#handleSlideChangeEvent({ keyCode: KeyCode.LEFT_ARROW });
            } else if (target.hasClass(NEXT_CLASS_NAME)) {
                this.#handleSlideChangeEvent({ keyCode: KeyCode.RIGHT_ARROW });
            } else if (target.hasClass(EXIT_CLASS_NAME)) {
                this.#handleSlideChangeEvent({ keyCode: KeyCode.ESCAPE });
            }
        }

        event.preventDefault();
        event.stopPropagation(); // avoid handling by the mousedown/touchstart handler at the blocker node
    }

    /**
     * Hiding the cursor and the navigation panel. This happens, if the presentation starts
     * and if the user did not move the mouse for more than a specified time (3 seconds).
     *
     * The existence checks are required, because this function might be called asynchronously
     * and the nodes might be removed in the meantime (the abort() function of the mouse move
     * promise returns another promise and does not call this function synchronously after the
     * presentation has ended).
     */
    #hideCursorAndNavigationPanel() {
        if (this.#presentationBlockerNode) { this.#presentationBlockerNode.addClass('hidecursor'); }
        if (this.#navigationPanelNode) { this.#navigationPanelNode.removeClass('shownavigation'); }
        this.#isHiddenCursor = true;
    }

    /**
     * Showing the cursor and the navigation panel. This happens when both are hidden and the
     * user moves the mouse for more than a specified distance (20 pixel).
     */
    #showCursorAndNavigationPanel() {
        if (this.#presentationBlockerNode) { this.#presentationBlockerNode.removeClass('hidecursor'); }
        if (this.#navigationPanelNode) { this.#navigationPanelNode.addClass('shownavigation'); }
        this.#isHiddenCursor = false;
        this.#cursorHidePosition = null;
    }

    /**
     * Aborting an optionally running mousemove timer, that checks if the user did not move the
     * mouse for specified time.
     */
    #abortMouseMoveTimer() {
        if (this.#mouseMoveTimer) {
            this.#mouseMoveTimer.abort();
            this.#mouseMoveTimer = null;
        }
    }

    /**
     * The mousedown/touchstart handler for events on the slide in presentation mode.
     * If the event was already handled by the navigation panel, it will not be handled
     * within this function.
     *
     * Info: The slide is only changed in mouseup/touchend event.
     *
     * @param {jQuery.Event} event
     *  The mousedown/touchstart event.
     */
    #mouseDownHandler(event) {

        if (TOUCH_DEVICE) {
            // saving the horizontal position to simulate a swiper and saving the time
            this.#touchStartTime = _.now();
            const touches = event.touches;
            const changed = event.changedTouches;
            const validEvent = touches.length === 1 && changed.length === 1;
            this.#horizontalTouchStartValue = validEvent ? changed[0].pageX : -1;
            this.#verticalTouchStartValue = validEvent ? changed[0].pageY : -1;
        }

        event.preventDefault();
        event.stopPropagation();
    }

    /**
     * The handler function to handle mouse move events.
     * Debounced mouse move handler.
     */
    @debounceMethod({ delay: 10, maxDelay: 50 })
    _mouseMoveHandler() {

        let topOffset = 0;

        this.#abortMouseMoveTimer();

        if (this.#isActiveLaserPointer) {
            if (!this.#isHiddenCursor) { this.#hideCursorAndNavigationPanel(); }
            topOffset = isFullscreenActive() ? 0 : this.#topbarHeight;
            this.#laserPointer.css({ left: (this.#lastMouseMoveEvent.pageX - LP_RADIUS), top: (this.#lastMouseMoveEvent.pageY - topOffset) });
        } else {

            if (!this.#isHiddenCursor) {
                // check, if the user was inactive for more than 3 seconds -> hide cursor and navigation panel
                this.#mouseMoveTimer = this.setTimeout(this.#hideCursorAndNavigationPanel, MOUSE_MOVE_TIMER);
            }

            if (this.#cursorHidePosition) {
                if (Math.abs(this.#lastMouseMoveEvent.pageX - this.#cursorHidePosition.x) > MIN_MOVE_DISTANCE || Math.abs(this.#lastMouseMoveEvent.pageY - this.#cursorHidePosition.y) > MIN_MOVE_DISTANCE) {
                    // check, if the user moved more than 20px after the cursor was hidden -> show cursor and navigation pane
                    this.#showCursorAndNavigationPanel();
                }
            } else {
                this.#cursorHidePosition = { x: this.#lastMouseMoveEvent.pageX, y: this.#lastMouseMoveEvent?.pageY };
            }
        }
    }

    /**
     * The direct callback of the debounced mouse move handler.
     *
     * @param {jQuery.Event} event
     *  A jQuery mouse move event.
     */
    #saveMouseMoveEvent(event) {
        this.#lastMouseMoveEvent = event;
        this._mouseMoveHandler(); // calling debounced method
    }

    /**
     * Handler for mouseup and touchend events. In the case of touch
     * devices, this function includes a simple swiper functionality.
     *
     * Info: The slide is only changed in mouseup/touchend event, not
     *       in mousedown/touchstart handler.
     */
    #mouseUpHandler(event) {

        if (this.#mouseDownOnPanel) {
            this.#mouseDownOnPanel = false;
            return; // nothing to do, if mousedown happened on panel (especially after switching to full screen mode)
        }

        if (TOUCH_DEVICE) {
            if (this.#touchStartTime > 0) {
                // saving the horizontal position to simulate a swiper
                const touchendTime = _.now();
                if (touchendTime - this.#touchStartTime < MAX_SWIPER_TIME) { // reacting only to short finger moves
                    const touches = event.touches;
                    const changed = event.changedTouches;
                    const validEvent = touches.length === 0 && changed.length === 1;
                    const horizontalTouchEndValue = validEvent ? changed[0].pageX : -1;
                    const verticalTouchEndValue = validEvent ? changed[0].pageY : -1;
                    const horizontalDistance = Math.abs(horizontalTouchEndValue - this.#horizontalTouchStartValue);
                    const verticalDistance = Math.abs(verticalTouchEndValue - this.#verticalTouchStartValue);

                    if (verticalDistance > horizontalDistance) {
                        if (verticalTouchEndValue - this.#verticalTouchStartValue > MIN_SWIPER_DISTANCE) {
                            this.#handleSlideChangeEvent({ keyCode: KeyCode.ESCAPE }); // a vertical swipe can be used to leave the presentation
                        }
                    } else {
                        if (this.#horizontalTouchStartValue >= 0 && horizontalTouchEndValue >= 0 && (horizontalTouchEndValue - this.#horizontalTouchStartValue > MIN_SWIPER_DISTANCE)) {
                            this.#handleSlideChangeEvent({ keyCode: KeyCode.LEFT_ARROW }); // swipe to the previous slide
                        } else {
                            this.#handleSlideChangeEvent({ keyCode: KeyCode.RIGHT_ARROW }); // defaulting to next slide
                        }
                    }
                }
                this.#touchStartTime = 0;
                this.#horizontalTouchStartValue = -1;
            }
        } else {
            // handle mouse down like event on right arrow key
            this.#handleSlideChangeEvent({ keyCode: KeyCode.RIGHT_ARROW });
        }

        event.preventDefault();
        event.stopPropagation();
    }

    /**
     * Often the focus is set to the body, especially in full screen mode on Chrome and Edge. And often it is not
     * possible to set the focus into the page (or the clipboard node).
     *
     * In this case it is necessary to listen to keydown events at the document.
     */
    #handleDocumentListener() {
        this.setTimeout(() => {
            const isActiveBody = $(document.activeElement).is('body');
            if (!this.#documentListenerRegistered && isActiveBody) {
                this.listenTo(document, 'keydown', this.#handleSlideChangeEvent); // registering a key handler at the document
                this.#documentListenerRegistered = true;
            } else if (this.#documentListenerRegistered && !isActiveBody) {
                this.#removeDocumentListener();
            }
        }, 100);
    }

    /**
     * If the final screen is no longer visible, because a previous slide is selected or the presentation mode is left,
     * it can be possible to remove a keyboard listener from the document.
     */
    #removeDocumentListener() {
        this.stopListeningTo(document, 'keydown', this.#handleSlideChangeEvent); // removing a key handler from the document
        this.#documentListenerRegistered = false;
    }

    /**
     * After starting a presentation or after leaving the final screen (to a previous slide), it might be necessary to
     * set the focus into the page again, because the focus was in the body before. This happens in Chrome and Edge
     * browser in the full screen mode.
     *
     * If the focus is in the body node, the keyboard events do no longer work correctly, because the 'default' handler
     * function is assigned to the page node.
     *
     * If the final screen is active, this function is not successful, because the page got 'display' set to 'none'.
     * Therefore in this special case a keydown listener is assigned to the document. This happens in the function
     * 'handleDocumentListener'.
     *
     * But after starting a presentation or leaving the final screen to the last slide, it is possible to set the
     * focus into the page node, so that keydown events are handled correctly again.
     */
    #setFocusIntoPageIfRequired() {
        this.setTimeout(() => {
            const isActiveBody = $(document.activeElement).is('body');
            if (!this.#documentListenerRegistered && isActiveBody) {
                setFocus(this.#model.getNode()); // trying to set the focus into the page
                this.#handleDocumentListener(); // check, if focus change was possible and set document listener
            } else if (this.#documentListenerRegistered && !isActiveBody) {
                this.#removeDocumentListener();
            }
        }, 400);
    }

    /**
     * Forcing an update of the top bar by selecting the first drawing and switching back
     * to the slide selection.
     *
     * This process is only required on Android tablets after presentations in non fullscreen
     * mode. Android small devices do not show the top pane and IOS does not show this behavior.
     *
     * TODO: This might be improved in the future.
     */
    #forceTopPaneRepaintBySelectionChange() {
        const selection = this.#model.getSelection();
        selection.setSlideSelection();
        const startPosition = selection.getStartPosition();
        selection.setTextSelection(_.copy(startPosition), increaseLastIndex(startPosition));
        // if the delay for the following 'setSlideSelection' is to low, the top pane is not rerendered
        this.setTimeout(()  => { selection.setSlideSelection(); }, 500);
    }

    /**
     * Handler function for the events that need to be handled in the presentation mode. This
     * is the central function, in which all slide changes, the final screen or the exit of
     * the presentation needs to be handled.
     *
     * @param {jQuery.Event|Object} event
     *  A jQuery keyboard event object. Or an object that at least contains the property 'keyCode'.
     *  Such an object can be generated by the calling function, so that a mouseclick can be
     *  handled like an event with specific keyCode.
     */
    #handleSlideChangeEvent(event) {

        if (!this.#presentationMode) { return; } // only handle the slide change in presentation mode

        if (this.showSlideAnimation() && this.#app.getView().isSlideChangeAnimationRunning && this.#app.getView().isSlideChangeAnimationRunning()) { return; } // avoiding conflicts in animations

        if (isEscapeKey(event) || this.#isEmptyPresentation) {
            this.#leavePresentationMode({ userAbort: true }); // the user aborted the presentation -> activating the current slide in edit mode
            this.#pagediv.toggle(true); // making page visible again
            this.#isFinalScreen = false;
        } else if (this.#isToggleFullScreenEvent(event)) {
            this.#toggleCurrentFullScreenMode();
        } else if (this.#model.isLastSlideActive({ onlyVisible: !this.#showHiddenSlides }) && hasKeyCode(event, 'RIGHT_ARROW', 'DOWN_ARROW', 'PAGE_DOWN')) {
            if (this.#isFinalScreen) {
                this.#leavePresentationMode();
                this.#pagediv.toggle(true); // making page visible again
                this.#isFinalScreen = false;
            } else {
                this.#pagediv.toggle(false); // hiding the last slide
                this.#lastSlideInfoNode.toggle(true); // displaying info text
                this.#isFinalScreen = true; // setting marker for final black screen
                this.#setFocusIntoPageIfRequired(); // in some browsers the focus is set into the body
            }
        } else if (isCursorKey(event.keyCode)) {
            if (this.#isFinalScreen) {
                this.#pagediv.toggle(true); // making page visible again
                this.#lastSlideInfoNode.toggle(false); // hiding info text
                this.#isFinalScreen = false; // removing marker for final black screen
            } else {
                this.#model.setActiveSlideByCursorEvent(event, { onlyVisible: !this.#showHiddenSlides });
                this.#updateURL();
            }
        }
    }

    /**
     * Checking, if the top pane was rendered correctly after leaving the presentation mode (DOCS-2768).
     */
    #checkTopPaneVisibility() {
        const topPaneWidth = this.#app.docView.topPane.$el.children('.leading').width();
        if ((topPaneWidth === 0) && !this.#app.docController.destroyed) {
            if (TOUCH_DEVICE) {
                this.#forceTopPaneRepaintBySelectionChange();
            } else {
                this.#app.docController.update({ full: true });
            }
        }
    }

    /**
     * Showing the button to activate the full screen mode. This is required, when the presentation was not
     * started from OX Presentation, but from Drive or the Portal. In this case the presentation cannot be
     * started in full screen mode. Therefore a big button shall simplify the switch into full screen.
     */
    #activateBigFullscreenButton() {

        this.setTimeout(() => {
            this.#bigFullscreenButton.toggle(true);
            this.#bigFullscreenButton.addClass('button-in');
            this.#bigFullscreenButton.children('a').addClass('color-pulse bigbutton');
        }, FULLSCREEN_BUTTON_TIME_START);

        this.setTimeout(() => {
            this.#bigFullscreenButton.addClass('button-out');
            this.#bigFullscreenButton.on('animationend', () => { this.#bigFullscreenButton.toggle(false); });
        }, FULLSCREEN_BUTTON_TIME_END);

    }

    /**
     * Leaving the presentation mode.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.userAbort=false]
     *   Whether the user aborted the presentation mode. The can happen with the 'Escape' key or using the
     *   button in the navigation panel. In both cases the current slide shall be activated.
     */
    #leavePresentationMode(options) {

        // the application view
        const docView = this.#app.getView();

        // restore the modified screen nodes when leaving the presentation mode
        jpromise.floating(this.#leavePresentationScreenMode()); // ignore promise

        this.#abortMouseMoveTimer(); // aborting the mouse move timer
        if (this.#isActiveLaserPointer) { this.#toggleLaserPointer(); } // clear cleanup, avoiding problems in next start of presentation mode

        this.#slideEffectsPossible = false; // not using slide effects, when the edit state is restored

        this.#abortBorderTimers(); // aborting border updates

        // registering the handler for changes of the full screen mode at the content root node
        removeChangeFullscreenHandler(this.#contentRootNode[0], () => this.#fullscreenChangeHandler());

        // activating the slide that was active before the presentation
        // -> before setting contenteditable, so that focus on Android is not set
        this.#model.setActiveSlideId(getBooleanOption(options, 'userAbort', false) ? this.#model.getActiveSlideId() : this.#lastActiveSlideID);

        this.#updateURL(); // improving reload handling

        // DOCS-4777: When presentation was started in presentation mode, the collaborators list must be updated now
        if (this.#startInPresentationMode && this.#presentationMode) { docView.executeControllerItem("document/users"); }

        this.#setPresentationMode(false); // removing marker for active presentation mode

        this.#restoreDomNodesAfterPresentationMode(); // restoring the DOM nodes when leaving the presentation mode

        docView.changeZoom('slide'); // this also handles vertical scroll bar

        // closing the app, if it was opened in the presentation mode
        if (this.#startInPresentationMode) { this.#model.leavePresentationModeHelper(); } // finalizing the initialization

        // removing an existing keyboard listener on the document (required, if the focus is in the body node)
        if (this.#documentListenerRegistered) { this.#removeDocumentListener(); }

        // the user might have resized the browser in presentation mode
        docView.refreshPaneLayout();

        // setting the focus immediately, but asynchronous
        if (TOUCH_DEVICE) { this.setTimeout(this.#restoreContentEditableOnTouch, 0); }

        // Sometimes the top pane is not rendered correctly (DOCS-2768) -> controller update
        this.setTimeout(this.#checkTopPaneVisibility, 500);
    }

    /**
     * Handler for full screen mode change events. This is especially important, if the user
     * presses the "Escape" key in full screen mode.
     */
    #fullscreenChangeHandler() {
        if (!isFullscreenActive()) { // -> the full screen is no longer active
            // TODO: PowerPoint closes the presentation after pressing 'Escape', but OX Presentation says that 'Escape' leaves the full screen mode.
            if (this.#leaveFullscreenActive) { return; } // nothing to do, only switching away from full screen mode while presentation mode is still active
            // -> PowerPoint handling, leaving the presentation mode, when user pressed "Escape"
            // -> Info: When the user pressed the "stop presentation" button, leavePresentationMode is called twice. Then the sidepane looks better after
            //          leaving full screen mode
            this.#leavePresentationMode({ userAbort: true });
        }
    }
} // class PresentationModeManager

// export =================================================================

export default PresentationModeManager;
