/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { LOCALE_DATA } from '@/io.ox/office/tk/locale';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { EMPTYTEXTFRAME_CLASS, getContentNode, getDrawingNode } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { getBooleanOption, getNumberOption, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import { DELETE, FIELD_INSERT, FIELD_UPDATE, INSERT_DRAWING, PARA_INSERT, TEXT_INSERT,
    SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { FIELD_NODE_SELECTOR, LAYOUTSLIDELAYER_SELECTOR, MASTERSLIDELAYER_SELECTOR, SLIDE_SELECTOR, SLIDECONTAINER_SELECTOR,
    createFieldNode, ensureExistingTextNode, getChildContainerNode, getFieldDateTimeFormat, getFieldInstruction, getFieldPageFormat,
    getPageContentNode, getSlideFieldId, getTargetContainerId, isChangeTrackNode, isFieldNode, isFixedSimpleField as isFixedSimpleFieldDOM,
    isInsideHeaderFooterTemplateNode, isMarginalNode, isSpecialField, splitTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getOxoPosition, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import SimpleField from '@/io.ox/office/textframework/components/field/simplefield';

import { getODFFooterRepresentation, getPlaceHolderDrawingType, isPlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';
import { InsertFooterDialog, InsertDateFieldDialog } from '@/io.ox/office/presentation/view/dialogs';

// class SlideFieldManager ====================================================

/**
 * An instance of this class represents the handler for all fields in the
 * edited Presentation document.
 *
 * @param {PresentationApplication} app
 *  The application instance.
 */
class SlideFieldManager extends ModelObject {

    // the selection object
    #selection;
    // simple field instance
    #simpleField;
    // counter for simple field Id
    #simpleFieldID = 0;
    // number formatter instance
    #numberFormatter;
    // flag to determine if cursor position is inside field
    #isCursorHighlighted = false;
    // for simple fields we store node instance of highlighted field
    #currentlyHighlightedNode;
    // field format popup promise
    #fieldFormatPopupTimer;
    // holds value of type of highlighted field
    #highlightedFieldType;
    // collection of all fields in document
    #allFields = [];
    // collection of all fields in normal slide view
    #allNormalSlideFields = [];
    // collection of all date fields in document
    #allDateFields = [];
    // collection of all footer fields in document
    #allFooterFields = [];
    // collection of all page number fields in document
    #allSlideNumFields = [];
    // collection of all page number fields only on master and layout slides in non-placeholder drawings
    #allMasterNumFields = [];
    // collection of all date/time fields only on master and layout slides in non-placeholder drawings
    #allMasterDateFields = [];
    // collection of all footer fields only on master slides (ODP only)
    #allMasterFooterFields = [];
    // temporary collection of special complex field nodes, used during changetracking
    #tempSpecFieldCollection = [];
    // holds value of format instruction of highlighted field
    #highlightedFieldInstruction;
    // list for locale date and time formats
    #localFormatList;

    constructor(docModel) {

        // base constructor
        super(docModel);

        this.#simpleField = this.member(new SimpleField(this.docModel));

        this.#localFormatList = this.docApp.isODF() ? {
            date: [],
            time: [],
            creator: [],
            'page-number': [
                { option: 'default', value: /*#. dropdown list option: original string value, no formatting applied */ gt('(no format)') },
                { option: 'i', value: 'i, ii, iii, ...' },
                { option: 'I', value: 'I, II, III, ...' },
                { option: 'a', value: 'a, b, c, ...' },
                { option: 'A', value: 'A, B, C, ...' }
            ]
        } : {
            date: [],
            time: []
        };

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {
            this.#selection = this.docModel.getSelection();
            this.#numberFormatter = this.docModel.numberFormatter;

            this.waitForImportSuccess(() => {
                if (this.docModel.isLocalStorageImport() || this.docModel.isFastLoadImport()) {
                    this.refreshSlideFields();
                }

                // category codes loaded from local resource
                const categoryCodesDate = this.#numberFormatter.getCategoryCodes('date');
                // fill in date codes
                if (categoryCodesDate.length) {
                    categoryCodesDate.forEach(formatCode => {
                        this.#localFormatList.date.push({ option: formatCode, value: this.#numberFormatter.formatNow(formatCode) });
                    });
                } else {
                    this.#localFormatList.date.push(
                        { option: LOCALE_DATA.shortDate, value: this.#numberFormatter.formatNow(LOCALE_DATA.shortDate) },
                        { option: LOCALE_DATA.longDate, value: this.#numberFormatter.formatNow(LOCALE_DATA.longDate) }
                    );
                }

                // refresh and update datetime and slide number fields on document successfully imported.
                this.updateDateFieldsOnLoad();
                // listen to slide order change to update slide number fields
                this.listenTo(this.docModel, 'moved:slide inserted:slide removed:slide', this.updateSlideNumFields);
                // listen to change of slide or layout slide to update fields on master slide
                this.listenTo(this.docModel, 'change:slide change:layoutslide', this.updateMasterSlideFields);
            });

            this.listenTo(this.#selection, 'update', this.#checkHighlighting);

            this.listenTo(this.docModel, 'document:reloaded', this.refreshSlideFields);

            this.listenTo(this.docApp, 'docs:editmode:leave', this.destroyFieldPopup);

            this.listenTo(this.docApp.getView(), 'fielddatepopup:change', this.#updateDateFromPopup);
            this.listenTo(this.docApp.getView(), 'fielddatepopup:autoupdate', this.#setDateFieldToAutoUpdate);
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Inserts a simple text field component into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new text field.
     *
     * @param {String} type
     *  A property describing the field type.
     *
     * @param {String} representation
     *  A fallback value, if the placeholder cannot be substituted with a
     *  reasonable value.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new text field, as map of attribute
     *  maps (name/value pairs), keyed by attribute family.
     *
     * @param {string[]} [target]
     *
     * @returns {Boolean}
     *  Whether the text field has been inserted successfully.
     */
    implInsertField(start, type, representation, attrs, target) {

        // text span that will precede the field
        const span = this.docModel.prepareTextSpanForInsertion(start, {}, target);
        // whether this is an ODF application
        const isODF = this.docApp.isODF();
        // new text span for the field node
        let fieldSpan = null;
        // the field node
        let fieldNode = null;
        // format of the field
        let format;
        // the ID of the slide
        const slideID = target || this.docModel.getSlideIdByPosition(start);
        // the field type for slide number
        const slideNum = isODF ? 'page-number' : 'slidenum';

        if (!span) { return false; }

        // expanding attrs to disable change tracking, if not explicitely set
        attrs = this.docModel.checkChangesMode(attrs);

        if (!target && type === slideNum && representation) { // don't update slidenum field on slides with target (layout&master slides)
            representation = this.docModel.getSlideNumByID(slideID); // ensure to insert up-to-date slide number
        }

        // on ODF master slides, the fields representation must be visible immediately in footer fields
        if (isODF && target && !representation && _.contains(SlideFieldManager.ODF_FOOTER_FIELD_TYPES, type)) {
            representation = getODFFooterRepresentation(SlideFieldManager.ODF_FIELD_PLACEHOLDER_CONVERTER[type]);
        }

        // split the text span again to get initial character formatting
        // for the field, and insert the field representation text

        // Fix for 29265: Removing empty text node in span with '.contents().remove().end()'.
        // Otherwise there are two text nodes in span after '.text(representation)' in IE.
        fieldSpan = splitTextSpan(span, 0).contents().remove().end().text(representation);
        if (!representation) { ensureExistingTextNode(fieldSpan); }

        // insert a new text field before the addressed text node, move
        // the field span element into the field node
        fieldNode = createFieldNode();
        fieldNode.append(fieldSpan).insertAfter(span);
        fieldNode.data('type', type);

        // odf - field types needed to be updated in frontend (but do not update the page number)
        if (type === 'page-number') {
            fieldNode.addClass('field-' + type);
            if (attrs?.field?.pageNumFormat) { format = attrs.field.pageNumFormat; }
            const doUpdate = !(this.docModel.useSlideMode() && this.docModel.isMasterView()); // not updating field on odp master slides
            // update the field, but use the correct number saved in "representation" -> no recalculation of slide number (DOCS-4916)
            if (doUpdate) { this.docModel.getPageLayout().updatePageNumberField(fieldNode, format, representation); }
        }

        // adding inherited character attributes
        this.docModel.inheritInlineCharacterAttributes(fieldNode);

        // it might be necessary to transfer the hard character attributes to a previous empty text span (DOCS-3244)
        if (attrs && attrs.character) { this.docModel.checkTransferOfInlineCharacterAttributes(fieldNode, attrs.character); }

        if (!representation) { fieldNode.addClass('empty-field'); }

        this.addSimpleFieldToCollection(fieldNode, slideID);

        // validate paragraph, store new cursor position
        this.docModel.implParagraphChanged(span.parentNode);
        this.docModel.setLastOperationEnd(increaseLastIndex(start));
        return true;
    }

    /**
     * Updating field with given instruction.
     * Depending of passed instruction, different type of field is updated.
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String} instruction
     *  Complex field instruction, containing type,
     *  formating and optionally style, separated by \ .
     *
     */
    updateByInstruction(node, instruction) {

        if (instruction) {
            this.#simpleField.updateByInstruction(node, instruction);
        } else {
            globalLogger.warn('SlideFieldManager.updateByInstruction(): missing instruction!');
        }
    }

    /**
     * Gets simple field from collection by passed id.
     *
     * @param {String} id
     *  Id of queried field.
     *
     * @returns {jQuery|null}
     *  The simple field with the specified id, or null, if no such start marker exists.
     *
     */
    getSimpleField(id) {
        const fieldObj = _.findWhere(this.#allFields, { fid: id });
        return (fieldObj && fieldObj.node) || null;
    }

    /**
     * Check if there are simple fields in document.
     *
     * @returns {Boolean}
     */
    isSimpleFieldEmpty() {
        return this.isEmpty();
    }

    /**
     * Check if there are any type of fields in document.
     *
     * @returns {Boolean}
     */
    fieldsAreInDocument() {
        return !this.isEmpty();
    }

    /**
     * Warning: This accessor is used only for unit testing! Please use DOM's public method!
     *
     * @param {Node|jQuery|Null} field
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @param {Boolean} isODF
     *  If document format is odf, or ooxml.
     *
     * @returns {Boolean}
     *  If the field has fixed date property or not
     */
    isFixedSimpleField(field, isODF) {
        return isFixedSimpleFieldDOM(field, isODF);
    }

    /**
     * Getting the collection with all date fields on master/layout slides, optionally filtered by a master or layout slide ID.
     *
     * @param {String} [filterSlideID=undefined]
     *  An ID of a master or layout slide to get only the date fields located on this master or layout slide.
     *
     * @returns {Object[]}
     *  The collection of all date/time fields on master and layout slides.
     */
    getAllMasterDateFields(filterSlideID) {
        return filterSlideID ? _.filter(this.#allMasterDateFields, dateField => { return dateField.slideID === filterSlideID; }) : this.#allMasterDateFields;
    }

    /**
     * Getting the collection with all slide number fields on master/layout slides, optionally filtered by a master or layout slide ID.
     *
     * @param {String} [filterSlideID=undefined]
     *  An ID of a master or layout slide to get only the slide number fields located on this master or layout slide.
     *
     * @returns {Object[]}
     *  The collection of all slide number fields on master and layout slides.
     */
    getAllMasterNumFields(filterSlideID) {
        return filterSlideID ? _.filter(this.#allMasterNumFields, dateField => { return dateField.slideID === filterSlideID; }) : this.#allMasterNumFields;
    }

    /**
     * Getting the collection with all footer fields on master/layout slides, optionally filtered by a master or layout slide ID.
     * This footer fields exist only in ODF applications.
     *
     * @param {String} [filterSlideID=undefined]
     *  An ID of a master or layout slide to get only the footer fields located on this master or layout slide.
     *
     * @returns {Object[]}
     *  The collection of all footer fields on master and layout slides.
     */
    getAllMasterFooterFields(filterSlideID) {
        return filterSlideID ? _.filter(this.#allMasterFooterFields, dateField => { return dateField.slideID === filterSlideID; }) : this.#allMasterFooterFields;
    }

    /**
     * Create current date(time) value string with passed format type.
     *
     * @param {String} formatCode
     *  The format type.
     *
     * @returns {String}
     *  The current date(time) value string.
     */
    getDateTimeRepresentation(formatCode) {
        return this.#numberFormatter.formatNow(formatCode);
    }

    /**
     * Creates highlight range on enter cursor in complex field,
     * with keyboard key pressed or mouse click.
     *
     * @param {jQuery} field
     *  Field to be highlighted.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.simpleField=false]
     *      If it's simple field or not.
     *  @param {Boolean} [options.mouseTouchEvent=false]
     *      If event is triggered by mouse or touch event.
     *  @param {Boolean} [options.rightClick=false]
     *      If its right click or not.
     */
    createEnterHighlight(field, options) {

        let fieldConfig = null;

        if (options?.simpleField) {
            if (this.#currentlyHighlightedNode) {
                this.removeEnterHighlight();
            }
            this.#currentlyHighlightedNode = $(field);
            this.#currentlyHighlightedNode.addClass('sf-highlight');

            const fieldType = getFieldInstruction(field);

            if (fieldType && this.docApp.isODF() && (this.#simpleField.isCurrentDate(fieldType) || this.#simpleField.isCurrentTime(fieldType))) {
                fieldConfig = { type: fieldType, instruction: getFieldDateTimeFormat(field) || 'default' };
            } else if (fieldType && this.docApp.isODF() && this.#simpleField.isPageNumber(fieldType)) {
                fieldConfig = { type: fieldType, instruction: getFieldPageFormat(field) || 'default' };
            } else {
                fieldConfig = this.#simpleField.cleanUpAndExtractType(fieldType);
            }

            fieldConfig.autoDate = !isFixedSimpleFieldDOM(field, this.docApp.isODF());
        }

        if (fieldConfig?.type) {
            fieldConfig.formats = this.#localFormatList[fieldConfig.type.toLowerCase()];
        }
        this.#highlightedFieldType = fieldConfig?.type || null;
        this.#highlightedFieldInstruction = fieldConfig?.instruction || null;

        this.setCursorHighlightState(true);

        this.destroyFieldPopup();
        const isDateTimeField = (this.#highlightedFieldType && (/^DATE|^TIME/i).test(this.#highlightedFieldType)) || false;
        const isSupportedFormat = fieldConfig?.formats?.length || isDateTimeField;
        const isChangeTracked = isChangeTrackNode(field) || isChangeTrackNode($(field).parent());

        if (this.#highlightedFieldType && isSupportedFormat && options?.mouseTouchEvent && !options?.rightClick && !isChangeTracked) {
            this.#fieldFormatPopupTimer = this.setTimeout(() => {
                this.docApp.docView.showFieldFormatPopup(fieldConfig, field);
            }, 500);
        }

        // for debugging ->
        // if (!this.#highlightedFieldType) {
        //     globalLogger.error('no highlightedFieldType');
        // }
    }

    /**
     * Removes highlight range on enter cursor in complex field, or in simple field,
     * with keyboard key pressed or mouse click.
     */
    removeEnterHighlight() {
        if (this.#currentlyHighlightedNode) {
            // simple field
            this.#currentlyHighlightedNode.removeClass('sf-highlight');
            this.#currentlyHighlightedNode = null;
        }
        this.setCursorHighlightState(false);
        this.#highlightedFieldType = null;
        this.#highlightedFieldInstruction = null;

        this.destroyFieldPopup();
    }

    /**
     * Set highlighting state of field during cursor traversal.
     *
     * @param {Boolean} state
     *  If cursor is inside field or not.
     */
    setCursorHighlightState(state) {
        this.#isCursorHighlighted = state === true;
    }

    /**
     * If field highlight state is active, or not.
     *
     * @returns {Boolean}
     *  Whether the field highlight state is active, or not.
     */
    isHighlightState() {
        return this.#isCursorHighlighted;
    }

    /**
     * Forces update of highlighted simple/complex field content.
     */
    updateHighlightedField() {
        let node;
        let instruction;
        let atEndOfSpan;
        let domPos;

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
            instruction = getFieldInstruction(node);
            this.updateByInstruction(node, instruction);

            // remove highlight from deleted node
            this.#currentlyHighlightedNode = null;
            this.setCursorHighlightState(false);

            // add highlight to newly created node
            domPos = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
            if (domPos && domPos.node) {
                node = domPos.node;
                if (node.nodeType === 3) {
                    atEndOfSpan = domPos.offset === node.textContent.length;
                    node = node.parentNode;
                }
                if (atEndOfSpan && isFieldNode(node.nextSibling)) {
                    this.createEnterHighlight(node.nextSibling, { simpleField: true });
                }
            }
        }
    }

    /**
     * Forces update of complex fields inside selection range.
     */
    updateHighlightedFieldSelection() {
        let instruction = null;
        if (this.isEmpty()) { return; }

        this.#selection.iterateNodes(node => {
            if (isFieldNode(node)) {
                instruction = getFieldInstruction(node);
                this.updateByInstruction(node, instruction);
            }
        });
    }

    /**
     * Update all fields in document.
     *
     */
    updateAllFields() {
        // the generate operations deferred
        let generateDef = null;

        // helper iterator function for handling field update
        const handleFieldUpdate = (entry, fieldId) => {
            let field;
            let instruction;

            if (fieldId && fieldId.indexOf('s') > -1) { // simple field
                field = this.getSimpleField(fieldId);
                if (!isMarginalNode(field) || !isInsideHeaderFooterTemplateNode(this.docModel.getNode(), field)) {
                    instruction = getFieldInstruction(entry);
                    if (!isSpecialField(field)) {
                        this.updateByInstruction(field, instruction);
                    }
                }
            }
        };

        if (this.fieldsAreInDocument()) {
            if (this.docModel.isHeaderFooterEditState()) { // #42671
                this.docModel.getPageLayout().leaveHeaderFooterEditMode();
                this.#selection.setNewRootNode(this.docModel.getNode());
            }
            // show a message with cancel button
            this.docApp.getView().enterBusy({
                cancelHandler: () => {
                    if (generateDef && generateDef.abort) {
                        generateDef.abort();
                    }
                },
                warningLabel: gt('Sorry, updating of all fields in document will take some time.')
            });

            // iterate objects
            // TODO: getAllFields() returns an array! Simulate old behaviour by converting array indexes to strings
            // generateDef = self.iterateObjectSliced(this.getAllFields(), handleFieldUpdate)
            generateDef = this.iterateArraySliced(this.#allFields, (field, index) => handleFieldUpdate(field, String(index)))
            .always(() => {
                // leave busy state
                this.docApp.docView.leaveBusy();
                this.docApp.docView.grabFocus();
            });

            // add progress handling
            generateDef.progress(partialProgress => {
                const progress = 0.2 + (partialProgress * 0.8);
                this.docApp.getView().updateBusyProgress(progress);
            });
        }
    }

    /**
     * This method dispatch parent node to simple field, for removing it.
     *
     * @param {jQuery|Node} parentNode
     *  Element that's being searched for simple fields.
     */
    removeAllFieldsInNode(parentNode) {

        // checking, if the specified node contains additional simple field nodes, that need to be removed, too.
        // this.#simpleField.removeSimpleFieldsInNode(parentNode);
        // all simple field nodes inside the parentNode
        const slideFieldNodes = $(parentNode).find(FIELD_NODE_SELECTOR);

        // update the marker nodes in the collection objects
        _.each(slideFieldNodes, field => {
            this.removeSimpleFieldFromCollection(field);
        });
    }

    /**
     * Dispatch function to simpleField class's method removeSimpleFieldFromCollection.
     *
     * @param {HTMLElement|jQuery} node
     *  One simple field node.
     */
    removeSimpleFieldFromCollection(node) {
        const id = getSlideFieldId(node);

        const removeFieldInCollection = collection => { return _.filter(collection, obj => { return obj && obj.fid !== id; }); };

        this.#allFields               = removeFieldInCollection(this.#allFields);
        this.#allNormalSlideFields    = removeFieldInCollection(this.#allNormalSlideFields);
        this.#allSlideNumFields       = removeFieldInCollection(this.#allSlideNumFields);
        this.#allDateFields           = removeFieldInCollection(this.#allDateFields);
        this.#allFooterFields         = removeFieldInCollection(this.#allFooterFields);
        this.#allMasterNumFields      = removeFieldInCollection(this.#allMasterNumFields);
        this.#allMasterDateFields     = removeFieldInCollection(this.#allMasterDateFields);
        this.#allMasterFooterFields   = removeFieldInCollection(this.#allMasterFooterFields);
    }

    /**
     * Filling collections with presentation fields.
     *
     * @param {HTMLElement|jQuery} fieldNode
     *  One simple field node.
     *
     * @param {String} slideID
     *  ID of the slide, where the simple field is located.
     */
    addSimpleFieldToCollection(fieldNode, slideID) {
        const $fieldNode = $(fieldNode);
        const type = $fieldNode.data().type;
        const id = 'sf' + this.#simpleFieldID;
        const isODF = this.docApp.isODF();
        const fieldObj = { slideID, node: $fieldNode, fid: id };
        const slideNum = isODF ? 'page-number' : 'slidenum';
        const dateField = isODF ? 'date' : 'datetime';
        const isDateFieldType = type && type.indexOf(dateField) > -1;

        if (!slideID) {
            globalLogger.error('SlideFieldManager.addSimpleFieldToCollection(): no slide id provided!');
            return;
        }

        this.#allFields.push(fieldObj);

        if (this.docModel.isStandardSlideId(slideID)) {
            this.#allNormalSlideFields.push(fieldObj);
            if (type === slideNum) {
                this.#allSlideNumFields.push(fieldObj);
            }
        } else if (type === slideNum && (isODF || !isPlaceHolderDrawing(getDrawingNode(fieldNode)))) {
            this.#allMasterNumFields.push(fieldObj);
        } else if (isDateFieldType && (isODF || !isPlaceHolderDrawing(getDrawingNode(fieldNode)))) {
            this.#allMasterDateFields.push(fieldObj);
        } else if (type === 'footer' && isODF) {
            this.#allMasterFooterFields.push(fieldObj); // ODP only
        }

        if (isDateFieldType) {
            if (isODF) { fieldObj.node.removeClass('empty-field'); } // no need to handle 'empty-field' in ODP files
            this.#allDateFields.push(fieldObj);
        } else if (type && type === 'footer') {
            fieldObj.node.removeClass('empty-field'); // no need to handle 'empty-field' in ODP files
            this.#allFooterFields.push(fieldObj);
        }

        $fieldNode.attr('data-fid', id);
        this.#simpleFieldID += 1;
    }

    /**
     * Refresh collection of fields after fastload and local storage.
     */
    refreshSlideFields() {
        // page node
        const pageNode = this.docModel.getNode();
        // the page content node's children slides
        const pageContentNodeSlides = getPageContentNode(pageNode).children(SLIDE_SELECTOR);
        // master slide layer node's children slide containers
        const masterLayerSlideContainers = pageNode.children(MASTERSLIDELAYER_SELECTOR).children(SLIDECONTAINER_SELECTOR);
        // layout slide layer node's children slide containers
        const layoutLayerSlideContainers = pageNode.children(LAYOUTSLIDELAYER_SELECTOR).children(SLIDECONTAINER_SELECTOR);

        // reset collections
        this.#allFields = [];
        this.#allNormalSlideFields = [];
        this.#allDateFields = [];
        this.#allSlideNumFields = [];
        this.#allFooterFields = [];
        this.#allMasterNumFields = [];
        this.#allMasterDateFields = [];
        this.#allMasterFooterFields = [];

        _.each(pageContentNodeSlides.add(masterLayerSlideContainers).add(layoutLayerSlideContainers), slide => {
            const slideID = getTargetContainerId(slide);
            _.each($(slide).find(FIELD_NODE_SELECTOR), fieldNode => {
                this.addSimpleFieldToCollection(fieldNode, slideID);
            });
        });
    }

    /**
     * After splitting a paragraph, it is necessary, that all complex field nodes in the cloned
     * 'new' paragraph are updated in the collectors.
     * This method dispatches to complex field class.
     */
    updateSimpleFieldCollector() {
        // this.#simpleField.updateSimpleFieldCollector(paragraph);
    }

    /**
     * Dispatches to handler for updateField operations.
     *
     * @param {Number[]} start
     *  The logical start position for the new simple field.
     *
     * @param {String} type
     *  Type of the simple field.
     *
     * @param {String} representation
     *  Content of the field that's updated.
     *
     * @param {object} attrs
     *
     * @param {String} target
     *  The target corresponding to the specified logical start position.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the simple field has been inserted successfully.
     */
    updateSimpleFieldHandler(start, type, representation, attrs, target, external) {
        return this.#simpleField.updateSimpleFieldHandler(start, type, representation, attrs, target, external);
    }

    /**
     * Dispatch update of date time fields on download to field classes.
     */
    prepareDateTimeFieldsDownload() {

        // helper iterator function for handling field update
        const handleFieldUpdate = (entry, fieldId) => {
            let field;
            let instruction;

            this.destroyFieldPopup();

            if (fieldId && fieldId.indexOf('s') > -1) { // simple field
                field = this.getSimpleField(fieldId);
                if (field) {
                    instruction = getFieldInstruction(entry);
                    this.#simpleField.updateDateTimeField(field, instruction);
                }
            }
        };

        _.each(this.getAllFields(), handleFieldUpdate);
    }

    /**
     * Method to update all slide number fields, triggered after slides are moved, inserted, or deleted.
     */
    updateSlideNumFields() {

        const isODF         = this.docApp.isODF();
        const isMasterView  = this.docModel.isMasterView();

        const data = [];
        const targetChain = this.docModel.getTargetChain(this.docModel.getActiveSlideId());

        _.each(this.#allSlideNumFields, slideNumObj => {
            const node = slideNumObj.node;
            let fieldHolder = null;
            const representation = this.docModel.getSlideNumByID(slideNumObj.slideID);

            if (node.length && representation) {
                fieldHolder = node.children();
                if (fieldHolder.text() !== String(representation)) {
                    fieldHolder.text(representation);
                    data.push({ slideID: slideNumObj.slideID, fieldID: node.attr('data-fid'), value: representation });
                }
            }
        });

        _.each(this.#allMasterNumFields, slideNumObj => {
            const node = slideNumObj.node;
            const representation = isMasterView ? '<#>' : this.docModel.getSlideNumByID(this.docModel.getActiveSlideId());
            let fieldHolder;

            if (node.length && representation) {
                if (_.contains(targetChain, slideNumObj.slideID)) {
                    fieldHolder = node.children();
                    if (fieldHolder.text() !== String(representation)) {
                        fieldHolder.text(representation);
                    }
                }
            }

            _.each(this.docModel.getAllSlideIDsWithSpecificTargetAncestor(slideNumObj.slideID), oneId => {
                if (!isODF || !isMasterView || !this.docModel.isStandardSlideId(oneId)) {

                    const tempVal = isMasterView ? '<#>' : this.docModel.getSlideNumByID(oneId);
                    if (tempVal) {
                        data.push({
                            slideID: oneId,
                            fieldID: node.attr('data-fid'),
                            value: tempVal,
                            masterViewField: true
                        });
                    }
                }
            });
        });

        if (data.length) {
            this.docModel.trigger('slideNumFields:update', data);
        }
    }

    /**
     * Method to update slide number fields on master and layout slides, when slide view is changed.
     * In ppt they are in non-placeholder drawings only, in odf, in placeholder also.
     */
    updateMasterSlideFields() {

        // the ID of the active slide
        const activeSlideId = this.docModel.getActiveSlideId();
        // the complete target chain for the active slide
        const targetChain = this.docModel.getTargetChain(activeSlideId);
        // the 'slide' family attributes of the active slide
        let slideFamilyAttrs = null;

        _.each(this.#allMasterNumFields, slideNumObj => {
            const node = slideNumObj.node;
            let fieldHolder;
            let representation;

            if (_.contains(targetChain, slideNumObj.slideID)) {
                representation = this.docModel.isMasterView() ? '<#>' : this.docModel.getSlideNumByID(activeSlideId);
                if (node.length && representation) {
                    fieldHolder = node.children();
                    if (fieldHolder.text() !== String(representation)) {
                        fieldHolder.text(representation);
                    }
                }
            }
        });

        // in ODF format also the fields of type 'footer' and 'date' need to be updated on the master slides.

        if (this.docApp.isODF()) {

            // getting the attributes of family 'slide' of the document slide, so that the footer fields can be updated
            slideFamilyAttrs = (this.docModel.isStandardSlideId(activeSlideId) ? this.docModel.getSlideAttributesByFamily(activeSlideId, 'slide') : {}) || {};

            _.each(this.#allFooterFields, footerObj => {

                const node = footerObj.node;
                let fieldHolder;
                let representation;

                if (_.contains(targetChain, footerObj.slideID)) {
                    representation = this.docModel.isMasterView() ? getODFFooterRepresentation('ftr') : (slideFamilyAttrs.footerText || '');
                    if (node.length) {
                        fieldHolder = node.children();
                        if (fieldHolder.text() !== String(representation)) {
                            fieldHolder.text(representation);
                        }
                    }
                }

            });

            // handling of date fields inside footer placeholder drawing on master slide
            const masterDateFieldsInFooter = _.filter(this.#allMasterDateFields, fieldObj => {
                const node = fieldObj.node;
                const drawing = getDrawingNode(node);
                const drawingType = getPlaceHolderDrawingType(drawing);

                return _.contains(this.docModel.getPresentationFooterTypes(), drawingType);
            });
            _.each(masterDateFieldsInFooter, footerObj => {

                const node = footerObj.node;
                let fieldHolder;
                let representation;

                if (this.docModel.isMasterView()) {
                    representation = getODFFooterRepresentation('dt');
                } else {
                    representation = (slideFamilyAttrs.dateField ? this.#numberFormatter.formatNow(slideFamilyAttrs.dateField) : slideFamilyAttrs.dateText) || '';
                }

                if (node.length) {
                    fieldHolder = node.children();
                    if (fieldHolder.text() !== String(representation)) {
                        fieldHolder.text(representation);
                    }
                }

            });

            // TODO: Updating the slide pane to display content correctly
            // -> But not calling slide pane update on every slide change (only at start and if slide attributes have changed)!

        }

    }

    /**
     * Depending of selected value, insert slide number field, or open date field dialog.
     *
     * @param {String} value
     *  Value of dropdown menu for insert field. Can be 'slidenum' or 'datetime' (or 'date-time' in ODF).
     *
     * @returns {InsertDateFieldDialog | void} // TODO: check-void-return
     */
    dispatchInsertField(value) {
        if (value === 'slidenum') {
            this.insertField(value);
        } else {
            return new InsertDateFieldDialog(this.docApp.getView()).show(); // open dialog with date formats
        }
    }

    /**
     * Opens insert footer dialog.
     *
     * @returns {InsertFooterDialog}
     */
    openFooterDialog() {
        return new InsertFooterDialog(this.docApp.getView(), (this.docApp.isODF() ? this.#getFieldOptionsOnSlideODF() : this.#getFieldOptionsOnSlide())).show(); // open dialog with date formats
    }

    /**
     * Insert simple fields.
     *
     * @param {String} fieldType
     *  Can be slide number or date.
     * @param {Object} [options]
     *  @param {String} [options.fieldFormat]
     *      Optional parameter used for formatting date fields.
     *  @param {Boolean} [options.automatic]
     *      Optional parameter used to decide if date field is automatically updated, or fixed value.
     *  @param {Number} [options.formatIndex]
     *      Optional parameter used to construct datetime type.
     */
    insertField(fieldType, options) {
        let fieldFormat = getStringOption(options, 'fieldFormat', '');
        const automatic = getBooleanOption(options, 'automatic', false);
        const formatIndex = getNumberOption(options, 'formatIndex', 1);

        return this.docModel.getUndoManager().enterUndoGroup(() => {

            const doInsertField = () => {

                let start = this.#selection.getStartPosition();
                const slideID = this.docModel.getActiveSlideId();
                const generator = this.docModel.createOperationGenerator();
                let operation = {};
                let representation = null;
                let repLength = 1;
                const isSelectionInTextFrame = this.#selection.isAdditionalTextframeSelection();
                const dateTimeType = this.docApp.isODF() ? 'date' : 'datetime';

                if (fieldType) {
                    if (!isSelectionInTextFrame) {
                        this.docModel.insertTextFrame();
                        start = this.#selection.getStartPosition();
                    } else {
                        this.docModel.doCheckImplicitParagraph(start);
                    }
                    if (fieldType === dateTimeType && !automatic) {
                        fieldFormat = fieldFormat || LOCALE_DATA.shortDate;
                        representation = this.#numberFormatter.formatNow(fieldFormat);
                        operation = { start:  _.copy(start), text: representation };
                        generator.generateOperation(TEXT_INSERT, operation);
                        repLength = representation.length;
                    } else {
                        operation = { start:  _.copy(start), type: fieldType };
                        if (fieldType === dateTimeType) {
                            fieldFormat = fieldFormat || LOCALE_DATA.shortDate;
                            representation = this.#numberFormatter.formatNow(fieldFormat);
                            operation.type = fieldType + (this.docApp.isODF() ? '' : formatIndex);
                            if (this.docApp.isODF()) {
                                operation.attrs = { character: { field: { dateFormat: fieldFormat } } };
                            }
                        } else if (fieldType === 'slidenum') {
                            representation = this.docModel.getSlideNumByID(slideID) || '<#>'; // hash tag for master and layout slides
                            representation = String(representation); // convert to string if number
                            operation.type = this.docApp.isODF() ? 'page-number' : 'slidenum';
                        }
                        operation.representation = representation;
                        generator.generateOperation(FIELD_INSERT, operation);
                    }
                    this.docModel.applyOperations(generator);
                    this.#selection.setTextSelection(increaseLastIndex(start, repLength));
                }
            };

            if (this.#selection.hasRange()) {
                return this.docModel.deleteSelected()
                .done(() => {
                    doInsertField();
                });
            }

            doInsertField();
            return $.when();
        });
    }

    /**
     * Clicking on remove button in popup, removes highlighted simple or complex field.
     */
    removeField() {
        let node;
        const rootNode = this.#selection.getRootNode();
        let startPos;

        // hide popup
        this.destroyFieldPopup();

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
            startPos = getOxoPosition(rootNode, node);

            if (startPos) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.docModel.deleteRange(startPos);
                });
            }
        }
    }

    /**
     * Helper function used to make diff between starting state of checkboxes and state on clicking apply button.
     *
     * @param {Object} startOptions
     *  Options on the start state, before clicking apply button.
     * @param {Object} endOptions
     *  Options on the end state, on clicking apply button.
     * @returns {Array}
     */
    diffStateCheckboxes(startOptions, endOptions) {
        const diff = [];
        _.each(startOptions, startOpt => {
            if (!_.findWhere(endOptions, { type: startOpt.type })) {
                diff.push(startOpt);
            }
        });

        return diff;
    }

    /**
     * Helper function used to make diff between starting state of checkboxes and state on clicking apply button.
     *
     * @param {Object} startOptions
     *  Options on the start state, before clicking apply button.
     * @param {Object} endOptions
     *  Options on the end state, on clicking apply button.
     * @returns {Array}
     */
    diffInputContent(startOptions, endOptions) {
        const computedOptions = [];
        _.each(endOptions, endOpt => {
            if (_.isString(endOpt.ftrText) && (_.findWhere(startOptions, { ftrInherited: true }) || !_.findWhere(startOptions, { ftrText: endOpt.ftrText }))) {
                computedOptions.push(endOpt);
            } else if (endOpt.representation && endOpt.automatic === true && !_.findWhere(startOptions, { representation: endOpt.representation, automatic: true })) {
                computedOptions.push(endOpt);
            } else if (_.isString(endOpt.representation) && endOpt.automatic === false && !_.findWhere(startOptions, { dtText: endOpt.representation, automatic: false })) {
                computedOptions.push(endOpt);
            } else if (endOpt.type === 'sldNum' && !_.findWhere(startOptions, { type: 'sldNum' })) {
                computedOptions.push(endOpt);
            }
        });

        return computedOptions;
    }

    /**
     * Insert footer with fields like date, slide number or footer text (ODF only). These values are
     * always assigned to document slides, never to master slides (but the content is inserted into
     * fields that exist only on master slides).
     *
     * @param {Array} checkedOptions
     *  List of options, which placeholder fields in footer to insert, and proprietary content.
     *
     * @param {Array} allSlides
     */
    insertFooterODF(checkedOptions, allSlides) {

        // a container for all affected slide IDs
        let slides = null;
        // the current slide pane selection
        let slidePaneSelection = null;
        // the operations generator
        const generator = this.docModel.createOperationGenerator();

        // generating a list of affected slides:
        // - all document slides (user checked 'Apply to all')
        // - the active slide if this is not the master view
        // - the last active document slide, if this is the master view
        // - the first document slide, if no other slide can be determined (should never occur)
        if (allSlides) {
            slides = this.docModel.getStandardSlideOrder(); // user checked 'Apply to all'
        } else {
            if (this.docModel.isStandardSlideId(this.docModel.getActiveSlideId())) {
                slidePaneSelection = this.docModel.getSlidePaneSelection();
                if (slidePaneSelection && slidePaneSelection.length > 1) {
                    slides = [];
                    slidePaneSelection.sort((a, b) => { return a - b; }); // more than one selected slide in document view
                    _.each(slidePaneSelection, slideIndex => {
                        slides.push(this.docModel.getIdOfSlideOfActiveViewAtIndex(slideIndex));
                    });
                } else {
                    slides = [this.docModel.getActiveSlideId()];  // active slide in document view
                }
            } else if (this.docModel.getLastActiveStandardSlideId()) {
                slides = [this.docModel.getLastActiveStandardSlideId()]; // in master view, the last active slide is used
            } else {
                slides = [this.docModel.getStandardSlideOrder()[0]];  // using the first standard slide (should never happen)
            }
        }

        // iterating over the affected standard slides and generate setAttributes operations.
        // No delete or insertDrawing operation needs to be created in ODF format.
        if (slides) {

            _.each(slides, slideId => {

                // the logical position of the slide
                const pos = this.docModel.getSlidePositionById(slideId);
                // the attribute object for all slide footer attributes (with all values resetted)
                const slideFooterAttrs = { isDate: false, isFooter: false, isSlideNum: false, footerText: null, dateText: null, dateField: null };
                // the operation object for the setAttributes operation
                let operation = null;

                _.each(checkedOptions, oneTypeObj => {

                    // the footer type. Supported values: 'dt', 'sldNum', 'ftr'
                    const type = oneTypeObj.type;

                    switch (type) {
                        case 'dt':
                            slideFooterAttrs.isDate = true;
                            if (oneTypeObj.automatic) {
                                if (oneTypeObj.representation) { slideFooterAttrs.dateField = oneTypeObj.representation; }
                            } else {
                                if (oneTypeObj.representation) { slideFooterAttrs.dateText = oneTypeObj.representation; }
                            }
                            break;
                        case 'sldNum':
                            slideFooterAttrs.isSlideNum = true;
                            break;
                        case 'ftr':
                            slideFooterAttrs.isFooter = true;
                            if (oneTypeObj.ftrText) { slideFooterAttrs.footerText = oneTypeObj.ftrText; }
                            break;
                    }

                });

                operation = { start:  _.copy(pos), attrs: { slide: slideFooterAttrs } };
                if (this.docModel.isMasterView()) { operation.target = this.docModel.getOperationTargetBlocker(); } // avoid setting of target for document slide, if master view is active
                generator.generateOperation(SET_ATTRIBUTES, operation);
            });

            this.docModel.applyOperations(generator);

            // updating the currently active document slide immediately
            this.updateMasterSlideFields();
        }
    }

    /**
     * Insert footer with fields like date, slide number or footer text (not ODF).
     *
     * @param {Array} options
     *  List of options, which placeholder fields in footer to insert, and proprietary content.
     *
     * @param {Array} deleteOptions
     *  List of options, which placeholder fields in footer to delete.
     */
    insertFooter(options, deleteOptions) {
        const { drawingStyles } = this.docModel;
        const activeSlideId = this.docModel.getActiveSlideId();
        const generator = this.docModel.createOperationGenerator();
        let operation = {};
        let isDate = false;
        let isSlideNum = false;
        let isFooter = false;
        let listOfSlideIds = [];
        let operationsDef;
        // a snapshot object
        let snapshot = null;

        const generateOpsFromOptions = currentSlideId => {
            let start = null;
            let nextAvailablePosition = null;

            _.each(options, opt => {
                let fieldPos = null;
                const fieldType = opt.type;
                const layoutId = this.docModel.getLayoutSlideId(currentSlideId);
                const layoutDrawing = layoutId && drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId, fieldType);
                const elementAttrs = layoutDrawing && layoutDrawing.length && getExplicitAttributeSet(layoutDrawing); // getElementAttributes is not necessary
                const slideDrawing = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(currentSlideId, fieldType);
                let slideDrawingContent = null;
                let len = null;

                operation = {};
                if (slideDrawing.length) {
                    if (fieldType === 'sldNum' && this.docModel.isLayoutOrMasterId(currentSlideId)) {
                        return; // not needed for slide number fields on layout and master slides
                    }

                    start = getOxoPosition(this.docModel.getNode(), slideDrawing);
                    if (_.isArray(start)) {
                        slideDrawingContent = getChildContainerNode(slideDrawing);
                        len = slideDrawingContent.children().length;
                        while (len > 0) {
                            operation = { start: start.concat([len - 1]) };
                            this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                            generator.generateOperation(DELETE, operation);
                            len -= 1;
                        }
                    }
                } else {
                    if (nextAvailablePosition) {
                        start = increaseLastIndex(nextAvailablePosition);
                        nextAvailablePosition = start;
                    } else {
                        nextAvailablePosition = this.docModel.getNextAvailablePositionInSlide(currentSlideId);
                        start = nextAvailablePosition;
                    }

                    // clear unnecessary properties
                    if (elementAttrs) {
                        // text frames are drawings of type shape
                        operation = { attrs: elementAttrs, start:  _.copy(start), type: 'shape' };
                        this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                        generator.generateOperation(INSERT_DRAWING, operation);
                    } else {
                        return;
                    }
                }

                // add a paragraph into the shape
                const paraPos = _.clone(start);
                paraPos.push(0);
                fieldPos = _.clone(paraPos);
                operation = { start:  _.copy(paraPos) };
                this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                generator.generateOperation(PARA_INSERT, operation);

                if (fieldType) {
                    fieldPos.push(0);
                    operation = { start:  _.copy(fieldPos) };
                    if (fieldType === 'dt') {
                        if (opt.automatic) {
                            operation.type = 'datetime' + opt.formatIndex;
                            operation.representation = opt.representation;
                            this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                            generator.generateOperation(FIELD_INSERT, operation);
                        } else {
                            if (_.isString(opt.representation) && opt.representation.length > 0) {
                                operation.text = opt.representation;
                                this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                                generator.generateOperation(TEXT_INSERT, operation);
                            }
                        }
                    } else if (fieldType === 'sldNum') {
                        operation.type = this.docApp.isODF() ? 'page-number' : 'slidenum';
                        operation.representation = String(this.docModel.getSlideNumByID(currentSlideId)); // convert to string if number
                        this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                        generator.generateOperation(FIELD_INSERT, operation);
                    } else if (fieldType === 'ftr') {
                        if (_.isString(opt.ftrText) && opt.ftrText.length > 0) {
                            operation.text = opt.ftrText;
                            this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                            generator.generateOperation(TEXT_INSERT, operation);
                        }
                    }
                }
            });

            // at the end, delete unchecked fields
            if (!this.docModel.isLayoutOrMasterId(currentSlideId)) {
                _.each(this.#getDelOpsFromOptions(drawingStyles, deleteOptions, currentSlideId), operation => {
                    generator.generateOperation(DELETE, operation);
                });
            }
        };

        if (this.docModel.isLayoutOrMasterId(activeSlideId)) {
            _.each(options, opt => {
                if (opt.type === 'dt') { isDate = true; }
                if (opt.type === 'sldNum') { isSlideNum = true; }
                if (opt.type === 'ftr') { isFooter = true; }
            });
            operation = { start: [0], target: activeSlideId, attrs: { slide: { isDate, isFooter, isHeader: false, isSlideNum } } };
            generator.generateOperation(SET_ATTRIBUTES, operation);

            if (this.docModel.isLayoutSlideId(activeSlideId)) {
                listOfSlideIds =  this.docModel.getAllSlideIDsWithSpecificTargetParent(activeSlideId);
            } else {
                listOfSlideIds = this.docModel.getAllSlideIDsWithSpecificTargetAncestor(activeSlideId);
            }
        }
        generateOpsFromOptions(activeSlideId);
        _.each(listOfSlideIds, currId => {
            generateOpsFromOptions(currId);
        });

        if (this.docModel.isLayoutOrMasterId(activeSlideId)) {
            // blocking keyboard input during applying of operations
            this.docModel.setBlockKeyboardEvent(true);

            // creating snapshot so that the document is restored after cancelling action
            snapshot = new Snapshot(this.docModel);

            this.docApp.getView().enterBusy({
                cancelHandler: () => {
                    if (operationsDef && operationsDef.abort) {
                        snapshot.apply();  // restoring the old state
                        this.docApp.enterBlockOperationsMode(() => { operationsDef.abort(); }); // block sending of operations
                    }
                },
                immediate: true,
                warningLabel: /*#. shown while applying selected properties from dialog to all slides in document */ gt('Applying footer fields to all slides, please wait...')
            });

            // fire apply operations asynchronously
            operationsDef = this.docModel.applyOperationsAsync(generator);

            // handle the result of footer fields operations
            operationsDef
                .progress(progress => {
                    // update the progress bar according to progress of the operations promise
                    this.docApp.getView().updateBusyProgress(progress);
                })
                .always(() => {
                    this.docApp.getView().leaveBusy();
                    // allowing keyboard events again
                    this.docModel.setBlockKeyboardEvent(false);
                    if (snapshot) { snapshot.destroy(); }
                });
        } else {
            this.docModel.applyOperations(generator);
        }
    }

    /**
     * Insert footers with fields like date, slide num or text, but in contrary to insertFooter function,
     * this applys to all standard slides in document.
     *
     *
     * @param {Array} endOptions
     *  List of footer options which user has chosen for all slides.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the insertion of all footers
     *  ended. It is rejected, if the dialog has been canceled.
     */
    insertAllFooters(endOptions) {
        const { drawingStyles } = this.docModel;
        const masterIds = _.keys(this.docModel.getAllGroupSlides('master'));
        const layoutIds = _.keys(this.docModel.getAllGroupSlides('layout'));
        const standardIds = _.keys(this.docModel.getAllGroupSlides('standard'));
        const generator = this.docModel.createOperationGenerator();
        let operation = {};
        let isDate = false;
        let isFooter = false;
        let isSlideNum = false;
        let operationsDef = null;
        // a snapshot object
        let snapshot = null;

        const handleOneSlide = currentSlideId => {
            let start = null;
            let nextAvailablePosition = null;
            const startOptions = this.#getFieldOptionsOnSlide(currentSlideId);
            const diffContentOptions = this.diffInputContent(startOptions, endOptions);
            // if some checkbox states are dechecked, this fields needs to be deleted
            const diffStateOptions = this.diffStateCheckboxes(startOptions, endOptions);

            _.each(diffContentOptions, opt => {
                let fieldPos = null;
                const fieldType = opt.type;
                const slideLayoutId = this.docModel.getLayoutSlideId(currentSlideId);
                const layoutDrawing = slideLayoutId && drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(slideLayoutId, fieldType);
                let elementAttrs = layoutDrawing && layoutDrawing.length && getExplicitAttributeSet(layoutDrawing);
                const slideDrawing = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(currentSlideId, fieldType);
                let slideDrawingContent = null;
                let len = null;

                if (this.docModel.isLayoutOrMasterId(currentSlideId) && !elementAttrs) {
                    elementAttrs = slideDrawing && slideDrawing.length && getExplicitAttributeSet(slideDrawing);
                }

                // appending the slide into the DOM, if necessary
                this.docModel.appendSlideToDom(currentSlideId);

                if (slideDrawing.length) {
                    start = getOxoPosition(this.docModel.getNode(), slideDrawing);
                    if (_.isArray(start)) {
                        slideDrawingContent = getChildContainerNode(slideDrawing);
                        len = slideDrawingContent.children().length;
                        while (len > 0) {
                            operation = { start: start.concat([len - 1]) };
                            this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                            generator.generateOperation(DELETE, operation);
                            len -= 1;
                        }
                    }
                } else {
                    if (nextAvailablePosition) {
                        start = increaseLastIndex(nextAvailablePosition);
                        nextAvailablePosition = start;
                    } else {
                        nextAvailablePosition = this.docModel.getNextAvailablePositionInSlide(currentSlideId);
                        start = nextAvailablePosition;
                    }

                    // clear unnecessary properties
                    if (elementAttrs) {
                        // text frames are drawings of type shape
                        operation = { attrs: elementAttrs, start:  _.copy(start), type: 'shape' };
                        this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                        generator.generateOperation(INSERT_DRAWING, operation);
                    } else {
                        return;
                    }
                }

                if (!start) { // this should never happen
                    globalLogger.error('SlideFieldManager.insertAllFooters(): failed to find valid start position!');
                    return;
                }

                // add a paragraph into the shape
                const paraPos = _.clone(start);
                paraPos.push(0);
                fieldPos = _.clone(paraPos);
                operation = { start:  _.copy(paraPos) };
                this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                generator.generateOperation(PARA_INSERT, operation);

                if (fieldType) {
                    fieldPos.push(0);
                    operation = { start:  _.copy(fieldPos) };
                    if (fieldType === 'dt') {
                        if (opt.automatic) {
                            operation.type = 'datetime' + opt.formatIndex;
                            operation.representation = opt.representation;
                            this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                            generator.generateOperation(FIELD_INSERT, operation);
                        } else {
                            if (_.isString(opt.representation) && opt.representation.length > 0) {
                                operation.text = opt.representation;
                                this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                                generator.generateOperation(TEXT_INSERT, operation);
                            }
                        }
                    } else if (fieldType === 'sldNum') {
                        operation.type = this.docApp.isODF() ? 'page-number' : 'slidenum';
                        operation.representation = this.docModel.isLayoutOrMasterId(currentSlideId) ? '<#>' : String(this.docModel.getSlideNumByID(currentSlideId)); // convert to string if number
                        this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                        generator.generateOperation(FIELD_INSERT, operation);
                    } else if (fieldType === 'ftr') {
                        if (_.isString(opt.ftrText) && opt.ftrText.length > 0) {
                            operation.text = opt.ftrText;
                            this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                            generator.generateOperation(TEXT_INSERT, operation);
                        }
                    }
                }
            });

            // at the end, delete unchecked fields
            if (!this.docModel.isLayoutOrMasterId(currentSlideId)) {
                _.each(this.#getDelOpsFromOptions(drawingStyles, diffStateOptions, currentSlideId), operation => {
                    generator.generateOperation(DELETE, operation);
                });
            }
        };

        _.each(endOptions, opt => {
            if (opt.type === 'dt') { isDate = true; }
            if (opt.type === 'sldNum') { isSlideNum = true; }
            if (opt.type === 'ftr') { isFooter = true; }
        });

        _.each(masterIds, masterId => {
            operation = { start: [0], target: masterId, attrs: { slide: { isDate, isFooter, isHeader: false, isSlideNum } } };
            generator.generateOperation(SET_ATTRIBUTES, operation);
            handleOneSlide(masterId);
        });

        _.each(layoutIds, layoutId => {
            operation = { start: [0], target: layoutId, attrs: { slide: { isDate, isFooter, isHeader: false, isSlideNum } } };
            generator.generateOperation(SET_ATTRIBUTES, operation);
            handleOneSlide(layoutId);
        });

        _.each(standardIds, standardId => {
            handleOneSlide(standardId);
        });

        // blocking keyboard input during applying of operations
        this.docModel.setBlockKeyboardEvent(true);

        // creating snapshot so that the document is restored after cancelling action
        snapshot = new Snapshot(this.docModel);

        // fire apply operations asynchronously
        operationsDef = this.docModel.applyOperationsAsync(generator);

        this.docApp.getView().enterBusy({
            cancelHandler: () => {
                if (operationsDef && operationsDef.abort) {
                    snapshot.apply();  // restoring the old state
                    this.docApp.enterBlockOperationsMode(() => { operationsDef.abort(); }); // block sending of operations
                }
            },
            immediate: true,
            warningLabel: /*#. shown while applying selected properties from dialog to all slides in document */ gt('Applying footer fields to current layout and all following standard slides, please wait...')
        });

        // handle the result of footer fields operations
        return operationsDef
            .progress(progress => {
                // update the progress bar according to progress of the operations promise
                this.docApp.getView().updateBusyProgress(progress);
            })
            .always(() => {
                this.docApp.getView().leaveBusy();
                // allowing keyboard events again
                this.docModel.setBlockKeyboardEvent(false);
                if (snapshot) { snapshot.destroy(); }
            });
    }

    /**
     * Clicking on field format from popup will update field with new formatting.
     *
     * @param {String} format
     */
    updateFieldFormatting(format) {
        let node;
        const rootNode = this.#selection.getRootNode();
        const fieldType = this.#highlightedFieldType; // store values before closing popup and deleting field
        const highlightedNode = this.#currentlyHighlightedNode;
        let isFixedDate;

        // hide popup
        this.destroyFieldPopup();

        if (!fieldType) {
            globalLogger.warn('SlideFieldManager.updateFieldFormatting(): missing field type!');
            return;
        }

        if (highlightedNode) { // simple field
            node = highlightedNode;
            const startPos = getOxoPosition(rootNode, node);
            isFixedDate = isFixedSimpleFieldDOM(node, this.docApp.isODF());

            if (startPos) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.docModel.deleteRange(startPos);
                    this.#simpleField.insertField(fieldType, format, { isFixed: isFixedDate });
                });
            }
        }
    }

    /**
     * Returns highlighted field format, or default one.
     *
     * @returns {String}
     *  Highlighted field format.
     */
    getSelectedFieldFormat() {
        return this.#highlightedFieldInstruction || 'default';
    }

    /**
     * Gets the anchor (start) node of the given field.
     *
     * @returns {jQuery | null}
     *  The anchor node of the given field, otherwise null if its not found.
     */
    getSelectedFieldNode() {
        return this.#currentlyHighlightedNode || null;
    }

    /**
     * On loosing edit right, close possible open field popup, and abort promise.
     *
     */
    destroyFieldPopup() {
        this.docApp.docView.hideFieldFormatPopup();
        this.#fieldFormatPopupTimer?.abort();
        this.#fieldFormatPopupTimer = null;
    }

    /**
     * Add special field that is changetracked to temporary collection.
     *
     * @param {jQuery} trackingNode
     */
    addToTempSpecCollection(trackingNode) {
        this.#tempSpecFieldCollection.push(trackingNode);
    }

    /**
     * Return array of collected special field nodes.
     */
    getSpecFieldsFromTempCollection() {
        return this.#tempSpecFieldCollection;
    }

    /**
     * Empty temporary collection after change track resolve is finished.
     */
    emptyTempSpecCollection() {
        this.#tempSpecFieldCollection = [];
    }

    /**
     * Converts number for page and numPages fields into proper format code.
     *
     * @param {Number} pageCount
     *  Number to be inserted into field.
     *
     * @param {String} format
     *  Format of the number inserted into field.
     *
     * @param {Boolean} [isNumPages]
     *  NumPages fields have some formats different from PageNum.
     *
     * @returns {String}
     *  Formatted number.
     */
    formatFieldNumber(pageCount, format, isNumPages) {
        return this.#simpleField.formatPageFieldInstruction(pageCount, format, isNumPages);
    }

    /**
     * Warning: Public accessor used only for unit tests! Not meant to be called directly.
     *
     */
    setDateFieldToAutoUpdate(state) {
        this.#setDateFieldToAutoUpdate(state);
    }

    /**
     * Public method to refresh collection of simple fields.
     */
    refreshSimpleFieldsCollection() {
        this.refreshSlideFields();
    }

    /**
     * Method for stopping operation distribution to server, and updating document's date fields.
     */
    updateDateFieldsOnLoad() {
        this.#updateDateTimeFields();
        if (this.docApp.isODF()) { this.updateMasterSlideFields(); } // updating the fields on the active slide
        this.updateSlideNumFields();
        if (this.docApp.isODF()) { this.docModel.createMasterSlideFieldsEvent(); } // informing the side pane about the master field state of every slide
    }

    /**
     * Returns whether there are fields in presentation document.
     *
     * @returns {Boolean}
     */
    isEmpty() {
        return this.#allFields.length === 0;
    }

    /**
     * Public getter for collection of all fields in the document.
     *
     * @returns {Array} allFields - array of objects containing slideID and DOM node of field.
     */
    getAllFields() {
        return this.#allFields;
    }

    /**
     * Public getter for collection of all standard slide fields in the document.
     *
     * @returns {Array} allNormalSlideFields - array of objects containing slideID and DOM node of field.
     */
    getAllNormalSlideFields() {
        return this.#allNormalSlideFields;
    }

    /**
     * Public getter that returns which footer fields and placeholders are present on slide.
     *
     * @param {String} [slideId]
     *  If ommited, active slide id is used.
     * @returns {Array}
     */
    getFieldOptionsOnSlide(slideId) {
        return this.#getFieldOptionsOnSlide(slideId);
    }

    /**
     * Returns current date in localized short format.
     *
     * @returns {String}
     */
    getCurrentShortLocalDate() {
        return this.#numberFormatter.formatNow(LOCALE_DATA.shortDate);
    }

    /**
     * Providing empty implementation for field manager function 'updateComplexFieldCollector'
     */
    updateComplexFieldCollector() {}

    handlePasteOperationIDs() {}

    checkRestoringSpecialFields() {}

    checkIfSpecialFieldsSelected() { return false; }

    // private methods ----------------------------------------------------

    /**
     * Private function called after importing document.
     * It loops all datetime fields in collection and:
     *  - updates content to current date WITHOUT sending the operations to server.
     */
    #updateDateTimeFields() {

        const serial = this.#numberFormatter.convertNowToNumber();
        const data = []; // collection of modified field contents, to send to slidepane
        const isODF = this.docApp.isODF();
        if (this.#allDateFields.length) {
            _.each(this.#allDateFields, dateFieldObj => {
                if (!dateFieldObj.node.length) { return; }

                const node = dateFieldObj.node;
                const dateType = node.data('type');
                let fieldHolder = null;
                const formattedDate = isODF ? this.#createDateFromFormat(node) : this.#createDateFromType(dateType, serial); // ooxml uses format such as "datetime2", odf classical "DD.MM.YYYY" variations
                const isNotFixedDate = isODF ? !isFixedSimpleFieldDOM(node, isODF) : true; // ooxml has only auto-update fields, odf can have fixed and auto-update

                if (formattedDate && isNotFixedDate) { // if date field is not fixed, it's content is updated with new date string
                    fieldHolder = node.children();
                    if (fieldHolder.text() !== formattedDate) {
                        fieldHolder.text(formattedDate);
                        data.push({ slideID: dateFieldObj.slideID, fieldID: node.attr('data-fid'), value: formattedDate });
                    }
                }
            });
            if (data.length) { // inform slidepane about new field values
                this.docModel.trigger('slideDateFields:update', data);
            }
        }
    }

    /**
     * Private helper function to create localized current date from given field type (ex.: 'datetime2')
     *
     * @param {String} fieldType
     *
     * @param {Date} [date]
     *  Optional parameter, if not sent, date object will be created.
     *
     * @returns {String} created localized current date
     */
    #createDateFromType(fieldType, date) {

        const formatIndexStr = (fieldType && fieldType.match(/\d+/) && fieldType.match(/\d+/)[0]) || null;
        const formatIndex = (formatIndexStr && parseInt(formatIndexStr, 10)) || null;
        const localFormat = formatIndex  && this.#localFormatList.date[formatIndex - 1] && this.#localFormatList.date[formatIndex - 1].option;
        const formatCode = localFormat ? localFormat : LOCALE_DATA.shortDate;

        return date ? this.#numberFormatter.formatValue(formatCode, date).text : this.#numberFormatter.formatNow(formatCode);
    }

    /**
     * Convinience function that fetches date format from node,
     * and uses it to convert and format current date to string.
     *
     * @param {jQuery} node
     *
     * @returns {String} created localized current date
     */
    #createDateFromFormat(node) {
        const nodeAttrs = node.data('attributes');
        const dateFormat = (nodeAttrs && nodeAttrs.character && nodeAttrs.character.field && nodeAttrs.character.field.dateFormat) || LOCALE_DATA.shortDate;

        return this.#numberFormatter.formatNow(dateFormat);
    }

    /**
     * Handler for highlighting of fields, after selection has changed.
     *
     * @param {Function} options
     *  Passed selection options.
     *
     */
    #checkHighlighting(options) {
        const isSimpleTextSelection = getBooleanOption(options, 'simpleTextSelection', false);
        const isSlideSelection = getBooleanOption(options, 'slideSelection', false);
        let domPos = null;
        let node = null;
        let atEndOfSpan = null;
        const relevantEvents = ['mousedown', 'mouseup', 'touchstart', 'touchend'];
        const browserEvent = (options && options.event) ? options.event : null;
        const mouseTouchEvent = _.isObject(browserEvent) && _.contains(relevantEvents, browserEvent.type);
        const rightClick = _.isObject(browserEvent) && (browserEvent.type === 'mousedown') && (browserEvent.button === 2);

        if (this.isEmpty() || (isSimpleTextSelection && !isSlideSelection) || this.#selection.hasRange()) {

        } else {
            domPos = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
            if (domPos && domPos.node) {
                node = domPos.node;
                if (node.nodeType === 3) {
                    atEndOfSpan = domPos.offset === node.textContent.length;
                    node = node.parentNode;
                }

                if (atEndOfSpan && isFieldNode(node.nextSibling)) {
                    this.createEnterHighlight(node.nextSibling, { simpleField: true, mouseTouchEvent, rightClick });
                } else if (this.isHighlightState()) {
                    this.removeEnterHighlight();
                }
            }
        }
    }

    /**
     * Callback function for triggering event fielddatepopup:change, when value of datepicker of input field is changed.
     *
     * @param {Object} [options]
     *   @param {String} [options.value]
     *       Changed value for the field.
     *   @param {String} [options.format]
     *       Format for the field.
     *   @param {String} [options.standard]
     *       Odf needs this standard date for fixed fields
     */
    #updateDateFromPopup(options) {

        let node;
        let atEndOfSpan;
        let fieldValue = (options && options.value) || null;
        const fieldFormat = (options && options.format) || null;
        const standardizedDate = (options && options.standard) || null;

        if (!fieldValue) {
            if (!fieldFormat) { return; } // no data to update, exit
            fieldValue = this.#numberFormatter.formatNow(fieldFormat);
        }

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
            this.#simpleField.updateDateFromPopupValue(node, fieldValue, standardizedDate);

            // remove highlight from deleted node
            this.#currentlyHighlightedNode = null;
            this.setCursorHighlightState(false);

            // add highlight to newly created node
            const domPos = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
            if (domPos && domPos.node) {
                node = domPos.node;
                if (node.nodeType === 3) {
                    atEndOfSpan = domPos.offset === node.textContent.length;
                    node = node.parentNode;
                }
                if (atEndOfSpan && isFieldNode(node.nextSibling)) {
                    this.createEnterHighlight(node.nextSibling, { simpleField: true });
                }
            }
        }
    }

    /**
     * Sets state of date field to be updated automatically or to be fixed value.
     * It is callback from field popup checkbox toggle.
     *
     * @param {Boolean} state
     *  State of the field, fixed or updated automatically
     */
    #setDateFieldToAutoUpdate(state) {
        let node;
        const generator = this.docModel.createOperationGenerator();
        let operation = {};
        // target for operation - if exists, it's for ex. header or footer
        const target = this.docModel.getActiveTarget();
        let representation;
        let attrs = {};
        let type;

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
            representation = $(node).text();
        }
        if (!node) {
            globalLogger.warn('SlideFieldManager.setDateFieldToAutoUpdate(): node not fetched!');
            return;
        }
        const pos = getOxoPosition(this.#selection.getRootNode(), node);

        if (pos) {
            return this.docModel.getUndoManager().enterUndoGroup(() => {
                if (this.docApp.isODF()) {
                    type = 'date';
                    attrs = { field: { fixed: JSON.stringify(!state), dateFormat: getFieldDateTimeFormat(node) } };
                } else {
                    type = getFieldInstruction(node);
                    attrs = { character: { autoDateField: JSON.stringify(state) } };
                }
                operation = { start:  _.copy(pos), type, representation, attrs };
                this.docModel.extendPropertiesWithTarget(operation, target);
                generator.generateOperation(FIELD_UPDATE, operation);

                this.docModel.applyOperations(generator);
            });
        } else {
            globalLogger.warn('SlideFieldManager.setDateFieldToAutoUpdate(): invalid position for field node!');
        }
    }

    /**
     * Helper function to collect text content from (footer) drawings from layout or master slide.
     *
     * @param {String} slideId
     *  The slide id of the (document) slide.
     *
     * @param {String} drawingType
     *  The type of the drawing.
     *
     * @returns {string}
     *  The text content of the drawing in the layout or master slide for the specified drawing type.
     *  If the drawing contains a field, only the text representation is returned (like in PP).
     */
    #collectTextContentOnParentSlides(slideId, drawingType) {
        const slideIdSet = this.docModel.getSlideIdSet(slideId); // return { id: slideId, layoutId: layoutId, masterId: masterId };
        let oneDrawing = null;
        let textContent = '';

        if (slideIdSet.layoutId) {
            oneDrawing = this.docModel.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(slideIdSet.layoutId, drawingType);
            if (oneDrawing && !getContentNode(oneDrawing).hasClass(EMPTYTEXTFRAME_CLASS)) { textContent = oneDrawing.text(); } // only collecting simple text
        }

        if (!textContent && slideIdSet.masterId) {
            oneDrawing = this.docModel.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(slideIdSet.masterId, drawingType);
            if (oneDrawing && !getContentNode(oneDrawing).hasClass(EMPTYTEXTFRAME_CLASS)) { textContent = oneDrawing.text(); } // only collecting simple text
        }

        return textContent;
    }

    /**
     * Helper function that returns the required values for the footer dialog in ODF format.
     *
     * @returns {Array}
     *  An array, that contains objects for the footer place holder drawings of type 'dt', 'ftr'
     *  or 'sldNum'. One object for every footer place holder drawing, that is displayed on the
     *  affected document slide. If the drawing is not displayed, the array does not contain an
     *  object for this place holder type.
     */
    #getFieldOptionsOnSlideODF() {

        // the ID of that slide, whose content will be displayed in the footer dialog
        let slideId = null;
        // the attributes of 'slide' family
        let slideFamilyAttrs = null;
        // a collector cointaining the footer place holder attributes of a document slide
        const options = [];

        // getting the affected slide, whose content will be shown in the dialog:
        // - the active slide if this is not the master view
        // - the last active document slide, if this is the master view
        // - the first document slide, if no other slide can be determined (should never occur)
        if (this.docModel.isStandardSlideId(this.docModel.getActiveSlideId())) {
            slideId = this.docModel.getActiveSlideId();  // active slide in document view
        } else if (this.docModel.getLastActiveStandardSlideId()) {
            slideId = this.docModel.getLastActiveStandardSlideId(); // in master view, the last active slide is used
        } else {
            slideId = this.docModel.getStandardSlideOrder()[0]; // using the first standard slide (should never happen)
        }

        // getting the attributes of family 'slide' for the specfied slide
        slideFamilyAttrs = this.docModel.getSlideAttributesByFamily(slideId, 'slide') || {};

        // collecting the information in the 'options' collector
        if (slideFamilyAttrs.isSlideNum) { options.push({ type: 'sldNum' }); }

        if (slideFamilyAttrs.isFooter) { options.push({ type: 'ftr', ftrText: (slideFamilyAttrs.footerText ? slideFamilyAttrs.footerText : '') }); }

        if (slideFamilyAttrs.isDate) {
            const dateObject = { type: 'dt' };

            if (slideFamilyAttrs.dateField) {
                dateObject.automatic = true;
                dateObject.dtType = slideFamilyAttrs.dateField;
                dateObject.representation = slideFamilyAttrs.dateField;
            } else {
                dateObject.automatic = false;
                dateObject.dtText = slideFamilyAttrs.dateText;
            }

            options.push(dateObject);
        }

        return options;
    }

    /**
     * Helper function that returns which fields are present on slide with given id, or active slide.
     *
     * @param {String} [slideId]
     *  If ommited, active slide id is used.
     * @returns {Array}
     */
    #getFieldOptionsOnSlide(slideId) {

        const options = [];
        let tempObj = {};
        const { drawingStyles } = this.docModel;
        const activeSlideId = slideId || this.docModel.getActiveSlideId();
        let ftr;
        let sldNum;
        let dt;

        if (this.docModel.isLayoutOrMasterId(activeSlideId)) {
            let slideAttrs = this.docModel.getAllFamilySlideAttributes(activeSlideId);
            slideAttrs = slideAttrs && slideAttrs.slide;
            if (slideAttrs) {
                if (slideAttrs.isFooter) {
                    ftr = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, 'ftr');
                }
                if (slideAttrs.isSlideNum) {
                    sldNum = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, 'sldNum');
                }
                if (slideAttrs.isDate) {
                    dt = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, 'dt');
                }
            }
        } else {
            ftr = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, 'ftr');
            sldNum = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, 'sldNum');
            dt = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, 'dt');
        }

        if (sldNum && sldNum.length) { options.push({ type: 'sldNum' }); }
        if (ftr && ftr.length) {
            tempObj.type = 'ftr';
            tempObj.ftrText = ftr.text();
            options.push(tempObj);
            tempObj = {};
        } else {
            const footerText = this.#collectTextContentOnParentSlides(activeSlideId, 'ftr');
            if (footerText) {
                tempObj.type = 'ftr';
                tempObj.ftrInherited = true;
                tempObj.ftrText = footerText; // checking for text in the layout or master slide
                options.push(tempObj);
                tempObj = {};
            }
        }
        if (dt && dt.length) {
            tempObj.type = 'dt';
            const dtFields = dt.find('.field');
            if (dtFields.length) {
                _.each(dtFields, field => {
                    const type = getFieldInstruction(field);
                    if (type && type.indexOf('date') > -1) {
                        tempObj.dtType = type;
                        tempObj.automatic = true;
                        tempObj.representation = $(field).text();
                    }
                });
            } else {
                tempObj.dtText = dt.text();
                tempObj.automatic = false;
            }
            options.push(tempObj);
        }

        return options;
    }

    /**
     * Helper to generate delete operation from passed options. Used in insertFooter and insertAllFooters
     *
     * @param {Object} drawingStyles
     *  instance of the DrawingStyles class.
     * @param {Array} deleteOptions
     *  list of delete options to be transformed to operations
     * @param {String} currentSlideId
     *  Id of the slide.
     * @returns {Array}
     */
    #getDelOpsFromOptions(drawingStyles, deleteOptions, currentSlideId) {
        let deleteOperations = [];
        let operation = null;
        _.each(deleteOptions, option => {
            const slideDrawing = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(currentSlideId, option.type);

            if (slideDrawing.length) {
                operation = { start: getOxoPosition(this.docModel.getNode(), slideDrawing) };
                this.docModel.extendPropertiesWithTarget(operation, this.docModel.isLayoutOrMasterId(currentSlideId) ? currentSlideId : null);
                deleteOperations.push(operation);
            }
        });
        if (deleteOperations.length) {
            deleteOperations = _.sortBy(deleteOperations, 'start');
            deleteOperations.reverse();
        }
        return deleteOperations;
    }

}

// constants --------------------------------------------------------------

/**
 * A list of those field types used in ODF footers.
 */
SlideFieldManager.ODF_FOOTER_FIELD_TYPES = ['date-time', 'footer', 'page-number'];

/**
 * A converter from ODF field types to place holder types.
 */
SlideFieldManager.ODF_FIELD_PLACEHOLDER_CONVERTER = {
    'date-time': 'dt',
    footer: 'ftr',
    'page-number': 'sldNum'
};

// export =================================================================

export default SlideFieldManager;
