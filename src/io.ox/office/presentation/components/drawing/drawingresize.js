/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { CHROME_ON_ANDROID, MIN_MOVE_DURATION, getBooleanOption } from '@/io.ox/office/tk/utils';
import { math, coord } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, convertHmmToLength, convertLengthToHmm, Rectangle } from '@/io.ox/office/tk/dom';
import { TrackingDispatcher } from '@/io.ox/office/tk/tracking';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { generateRecalcConnectorOptions, getCssTransform, getRotatedDrawingPoints, getUniqueConnections, pointCloseToSnapPoint, pointInsideRect } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { calculateBitmapSettings } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { END_LINKED_CLASS, FLIPPED_HORIZONTALLY_CLASSNAME, FLIPPED_VERTICALLY_CLASSNAME, FRAME_WITH_TEMP_BORDER, FRAME_WITH_TEMP_BORDER2, GROUPCONTENT_SELECTOR,
    ROTATE_ANGLE_HINT_SELECTOR, ROTATING_STATE_CLASSNAME, START_LINKED_CLASS, TEXTFRAME_NODE_SELECTOR, addRotationHint, clearAdjPoints, clearSelection, cloneDiv,
    drawSelection, getDrawingNode, getDrawingRotationAngle, getDrawingType, getGeometryValue, getNormalizedMoveCoordinates, getNormalizedResizeDeltas, getPointPixelPosition,
    getPxPosFromGeo, getResizerHandleType, getTextFrameNode, isActiveCropping, isAdjustmentHandle, isGroupContentNode, isAutoResizeHeightDrawingFrame, isConnectorDrawingFrame,
    isFlippedHorz, isFlippedVert, isGroupDrawingFrame, isMinHeightDrawingFrame, isNoWordWrapDrawingFrame, isOddFlipHVCount, isRotateHandle, isTableDrawingFrame,
    normalizeMoveOffset, normalizeResizeOffset, previewOnResizeDrawingsInGroup, recalcConnectorPoints, removeRotateAngleHint, scaleAdjustmentValue, toggleTracking,
    updateCssTransform, updateResizersMousePointers, updateShapeFormatting } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getCropHandleType, getDrawingMoveCropOp, getDrawingResizeCropOp, createCropMoveCallbacks, createCropResizeCallbacks, refreshCropFrame } from '@/io.ox/office/textframework/components/drawing/imagecropframe';
import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { APPCONTENT_NODE_SELECTOR, getDrawingPlaceHolderNode, getTableRows, isDrawingPlaceHolderNode, isInsideTableCellNode, isPlaceholderTemplateButton } from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, getOxoPosition } from '@/io.ox/office/textframework/utils/position';

import { checkEventPosition, extendPlaceholderProp } from '@/io.ox/office/presentation/utils/presentationutils';
import { isEmptyPlaceHolderDrawing, isPlaceHolderAttributeSet, isPlaceHolderDrawing, requiresUserTransformedProperty } from '@/io.ox/office/presentation/utils/placeholderutils';

// constants ==============================================================

// class name used when dragging is active
var ACTIVE_DRAGGING = 'activedragging';
// class name used when resizing is active
var ACTIVE_RESIZING = 'activeresizing';
// class name used to define indirect modification of connector linked to active shape
var INDIRECT_CONNECTOR_CHANGE_CLASS = 'indirectconnectorchange';
// whether the app runs on an iPadAir (special behavior for 68414)
var IPAD_AIR = _.device('macos && safari && touch');

// static methods ---------------------------------------------------------

/**
 * Draws a selection box for the specified drawing node and registers
 * mouse handlers for moving and resizing.
 *
 * @param {PresentationApplication} app
 *  The application instance containing the drawing.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node to be selected, as DOM node or jQuery object.
 */
export function drawDrawingSelection(app, drawingNode) {

    var // the model object
        model = app.getModel(),
        // the selection object
        selection = model.getSelection(),
        // the current root node
        rootNode = model.getCurrentRootNode(),
        // the node containing the scroll bar
        scrollNode = app.getView().getContentRootNode(),
        // if it's text frame with table inside it
        containsTable = isTableDrawingFrame(drawingNode),
        // object containing information about movable and resizable
        options = { movable: true, resizable: true, rotatable: !containsTable, adjustable: true },  // -> parameter?
        // the container element used to visualize the selection
        selectionBox = null,
        //zoom factor in floating point notation
        zoomFactor,
        // target ID of currently active root node, if existing
        target = model.getActiveTarget(),
        // an object collecting the attributes for an operation
        operationProperties = {},
        // whether this is a multi drawing selection
        isMultiSelection = false,
        // a collector for all move boxes and drawing informations
        allDrawings = null,
        allConnectors = null,
        // the empty drawing node that contains the selection node
        selectionDrawing = null,
        allConnectorsOnSlide = null,
        // whether this is a text selection
        isTextSelection = selection.isAdditionalTextframeSelection();

    var updateTextFrameShapesDebounced = model.debounce(updateTextFrameShapes, { delay: "animationframe" });
    var updateAdjustmentShapeDebounced = model.debounce(updateAdjustmentShape, { delay: "animationframe" });
    var updateConnectorsDebounced = model.debounce(updateConnectorsPreview, { delay: "animationframe" });

    // key code to turn off snap lines
    function isSnapIgnoreKey(record) { return  _.browser.MacOS ? record.modifiers.metaKey : record.modifiers.altKey; }

    // debounced callback for updating text frames in allDrawings
    function updateTextFrameShapes() {
        _.each(allDrawings, function (oneDrawing) {
            if (!isTableDrawingFrame(oneDrawing.drawingNode)) {
                updateTextFrameInShape(oneDrawing.drawingNode, oneDrawing.moveBox, { isResizeActive: true });
            }
        });
    }

    function updateAdjustmentShape() {
        _.each(allDrawings, function (oneDrawing) {
            updateTextFrameInShape(oneDrawing.drawingNode, oneDrawing.moveBox, { isResizeActive: true, modAttrs: oneDrawing.moveBox.data('modAttrs') });
        });
    }

    function updateConnectorsPreview() {
        _.each(allConnectors, function (conn) {
            updateTextFrameInShape($(conn.node), conn.moveBox, { isResizeActive: true });
        });
    }

    function isDrawingTypeConnector(drawingNode) {
        return getDrawingType(drawingNode) === 'connector';
    }

    // helper function to collect all move box information in multiple drawing selections
    function collectAndActivateAllMoveBoxes(options) {
        var tempCollection = [];

        if (selection.isMultiSelectionSupported() && selection.isMultiSelection()) {
            tempCollection = selection.getMultiSelection();
        } else {
            var drawingPos = getOxoPosition(rootNode, drawingNode, 0);
            tempCollection.push({ drawing: drawingNode, startPosition: drawingPos });
        }

        _.each(tempCollection, function (drawingSelection) {
            var attrs = getExplicitAttributeSet(drawingSelection.drawing);
            if (isPlaceHolderAttributeSet(attrs)) {
                attrs = model.drawingStyles.getElementAttributes(drawingSelection.drawing);
            }
            if (!selection.isMultiSelection() || (!isDrawingTypeConnector(drawingSelection.drawing) || !attrs.connector || !(attrs.connector.startId || attrs.connector.endId))) {
                activateMoveBox(drawingSelection.drawing, attrs, _.clone(drawingSelection.startPosition), options);
            }
        });
    }

    function activateMoveBox(drawingNode, attrs, drawingPos, options) {
        var isResize = getBooleanOption(options, 'resize', false);
        var hasNoFillNoLine = attrs && attrs.fill && attrs.fill.type === 'none' && attrs.line && attrs.line.type === 'none';
        var isNotEmptyPlaceholderDrawing = isPlaceHolderDrawing(drawingNode) && !isEmptyPlaceHolderDrawing(drawingNode);
        var isEmptyNoFillLineDrawing = hasNoFillNoLine && drawingNode.text().length === 0;
        var lockResizeAspectRatio = false;
        var id = attrs && attrs.drawing && attrs.drawing.id;
        var drawingSelectionNode = drawingNode.data('selection');

        drawingNode.addClass(ACTIVE_DRAGGING);
        if (drawingSelectionNode) { drawingSelectionNode.addClass(ACTIVE_DRAGGING); }

        if (isResize) { // don't evaluate lock ratio on move, just for resize
            drawingNode.addClass(ACTIVE_RESIZING);
            if (drawingSelectionNode) { drawingSelectionNode.addClass(ACTIVE_RESIZING); }
            lockResizeAspectRatio = (attrs && attrs.drawing && attrs.drawing.aspectLocked) || (SMALL_DEVICE && !isDrawingTypeConnector(drawingNode));
        }

        var oneMoveBox = cloneDiv(model, drawingNode, isResize);
        if ((isResize && hasNoFillNoLine) || isEmptyNoFillLineDrawing || isNotEmptyPlaceholderDrawing) {
            oneMoveBox.addClass(FRAME_WITH_TEMP_BORDER);
        }
        allDrawings.push({ drawingNode, id, attrs, moveBox: oneMoveBox, width: drawingNode.width(), height: drawingNode.height(), start: drawingPos, lockResizeAspectRatio });
        activateLinkedConnectors(allConnectorsOnSlide, id);
    }

    function clearDrawing(drawing) {
        drawing.removeClass(ACTIVE_DRAGGING + ' ' + ACTIVE_RESIZING);
        drawing.children('.copy').remove();
        var drawingSelectionNode = drawing.data('selection');
        if (drawingSelectionNode) {
            drawingSelectionNode.removeClass(ACTIVE_DRAGGING + ' ' + ACTIVE_RESIZING);
            if (isDrawingTypeConnector(drawing)) {
                drawingSelectionNode.toggleClass(START_LINKED_CLASS, drawing.hasClass(START_LINKED_CLASS));
                drawingSelectionNode.toggleClass(END_LINKED_CLASS, drawing.hasClass(END_LINKED_CLASS));
            }
        }
    }

    /**
     * Local method to update text frame box during resize of the shape,
     * so that text can reflow even before drawing resize is finalized.
     *
     * @param {jQuery} drawingNode
     *
     * @param {jQuery} moveBox
     *
     * @param {Object} [options]
     *  @param {Boolean} [options.isResizeActive=false]
     *     If this method is called during resizing of the shape.
     */
    function updateTextFrameInShape(drawingNode, moveBox, options) {
        var mergedAttributes = app.docModel.drawingStyles.getElementAttributes(drawingNode);

        if (isGroupDrawingFrame(drawingNode)) {
            moveBox.find('.textframecontent').children('img').remove();
            previewOnResizeDrawingsInGroup(app, drawingNode, mergedAttributes, options);
        } else {
            moveBox.children('img').remove();
            updateShapeFormatting(app, drawingNode, mergedAttributes, options);
        }
    }

    /**
     * Helper function to collect and prepare all drawings for rotation in multiple drawing selections
     *
     * @param {Boolean} isMultiSelection
     *
     * @param {jQuery} drawingNode
     *
     * @param {Number} startAngle
     */
    function collectAndPrepareDrawingsForRotation(isMultiSelection, drawingNode, startAngle) {
        var tempCollection = [];
        if (isMultiSelection) {
            tempCollection = selection.getMultiSelection();
        } else {
            var drawingPos = getOxoPosition(rootNode, drawingNode, 0);
            tempCollection.push({ drawing: drawingNode, startPosition: drawingPos, rotation: startAngle });

        }
        _.each(tempCollection, function (drawingSelection) {
            var drawingNode = drawingSelection.drawing;
            var drawingPos, startRotation;

            if (!isTableDrawingFrame(drawingNode)) {
                var attrs = getExplicitAttributeSet(drawingNode);
                if (isPlaceHolderAttributeSet(attrs)) {
                    attrs = model.drawingStyles.getElementAttributes(drawingNode);
                }
                if (!selection.isMultiSelection() || (!isDrawingTypeConnector(drawingNode) || !attrs.connector || !(attrs.connector.startId || attrs.connector.endId))) {
                    var hasNoFillNoLine = attrs && (!attrs.fill || attrs.fill.type === 'none') && attrs.line && attrs.line.type === 'none';
                    var id = attrs && attrs.drawing && attrs.drawing.id;

                    drawingPos = _.clone(drawingSelection.startPosition);
                    startRotation = !_.isUndefined(drawingSelection.rotation) ? drawingSelection.rotation : getDrawingRotationAngle(model, drawingNode);
                    drawingNode.addClass(ROTATING_STATE_CLASSNAME);
                    drawingNode.data('selection').addClass(ROTATING_STATE_CLASSNAME);
                    if (hasNoFillNoLine) { drawingNode.addClass(FRAME_WITH_TEMP_BORDER2); }
                    allDrawings.push({ drawingNode, id, attrs, start: drawingPos, rotation: startRotation });
                    activateLinkedConnectors(allConnectorsOnSlide, id);
                }
            }
        });
    }

    /**
     * Prepares and activates all connectors linked with given id drawing that is beeing modified currently.
     *
     * @param {Array} allConnectorsOnSlide
     *  Collection of all connectors on active slide, with properties
     * @param {String} id
     *  Currently modified drawing node's id used to search for linked connectors
     */
    function activateLinkedConnectors(allConnectorsOnSlide, id) {
        var connections = _.filter(allConnectorsOnSlide, function (c) {
            return c.attrs.drawing.id !== id &&
                (c.startId === id || c.endId === id || c.parentStartId === id || c.parentEndId === id);
        });

        _.each(connections, function (connector) {
            var oneMoveBox = $(connector.node).children('.copy');
            oneMoveBox = oneMoveBox.length ? oneMoveBox : cloneDiv(model, $(connector.node), true);
            var connectorSelectionNode = $(connector.node).data('selection');

            $(connector.node).addClass(ACTIVE_DRAGGING + ' ' + ACTIVE_RESIZING);
            if (connectorSelectionNode) { connectorSelectionNode.addClass(ACTIVE_DRAGGING); }

            allConnectors.push({
                node: $(connector.node),
                moveBox: oneMoveBox,
                attrs: connector.attrs,
                startIndex: connector.startIndex,
                endIndex: connector.endIndex,
                startId: connector.startId,
                endId: connector.endId,
                parentStartId: connector.parentStartId,
                parentEndId: connector.parentEndId
            });
        });
    }

    /**
     * Does the cleanup after finalize move/resize/rotate.
     *
     * @param {Array} allConnectors - array of modified connector objects
     * @returns {Array} empty array
     */
    function clearModifiedConnectors(allConnectors) {
        _.each(allConnectors, function (conn) {
            clearDrawing(conn.node);
            conn.node.removeClass(INDIRECT_CONNECTOR_CHANGE_CLASS);
        });
        return [];
    }

    function extendAndGenerateOp(generator, drawingNode, operationProperties, target) {
        extendPlaceholderProp(app, drawingNode, operationProperties);
        model.extendPropertiesWithTarget(operationProperties, target);
        generator.generateOperation(SET_ATTRIBUTES, operationProperties);

        model.updateDynFontSizeDebounced(drawingNode);
    }

    /**
     * This function will return object containing coordinates for every snap line on the active slide.
     * Coordinates are coresponding to slide edges and center, and also to other, static, drawings on slide.
     * The structure is: current selection/multiselection correlated to slide will have 'multiselection' key,
     * drawings are saved by their ids. Each entry has x and y properties, each containing data about snaplines.
     * Some entries will have dx and dy properties, for horizontal and vertical distance help snaplines.
     * Value is stored relative to the current position/size of the active drawing;
     * if left or top from the start position of the active drawing with minus sign.
     *
     * @param {Object} allSlideDrawings
     * @param {Object} allDrawings
     * @param {Number} zoomFactor
     * @param {Object} [options]
     *  @param {jQuery} [options.resizePoint] Currently used resize point (not available for move operation)
     *  @param {Object} [options.slideOffset] Top and left offset values of slide on the screen.
     *
     * @returns {Object}
     */
    function getStartSnapLinesCoords(allSlideDrawings, allDrawings, zoomFactor, options) {
        var lines = {};
        var staticShapesRects = {};
        var activeSlide = model.getSlideById(model.getActiveSlideId());
        var slideWidth = activeSlide.width();
        var slideHeight = activeSlide.height();
        var resizePoint = options && options.resizePoint; // optional property used only in resize mode
        var slideOffset = options && options.slideOffset;

        if (!app.getView().areSnaplinesEnabled()) { return {}; }

        _.each(allSlideDrawings, function (slideDrawing) {
            if (!_.findWhere(allDrawings, { id: slideDrawing.id })) {
                var dAttrs = slideDrawing.attrs.drawing;
                var normLeft = Math.round(convertHmmToLength(dAttrs.left, 'px'));
                var normTop = Math.round(convertHmmToLength(dAttrs.top, 'px'));
                var pxWidth = Math.round(convertHmmToLength(dAttrs.width, 'px'));
                var pxHeight = Math.round(convertHmmToLength(dAttrs.height, 'px')) || $(slideDrawing.node).height();
                var normRight = normLeft + pxWidth;
                var normBottom = normTop + pxHeight;
                if (dAttrs.rotation) {
                    var rPoints = getRotatedDrawingPoints({ left: normLeft, top: normTop, width: pxWidth, height: pxHeight }, dAttrs.rotation);
                    staticShapesRects[slideDrawing.id] = {
                        left: Math.round(rPoints.left),
                        top: Math.round(rPoints.top),
                        right: Math.round(rPoints.right),
                        bottom: Math.round(rPoints.bottom),
                        width: rPoints.right - rPoints.left,
                        height: rPoints.bottom - rPoints.top,
                        cx: (rPoints.left + rPoints.right) / 2,
                        cy: (rPoints.top + rPoints.bottom) / 2 };
                } else {
                    staticShapesRects[slideDrawing.id] = {
                        left: normLeft,
                        top: normTop,
                        right: normRight,
                        bottom: normBottom,
                        width: normRight - normLeft,
                        height: normBottom - normTop,
                        cx: (normRight + normLeft) / 2,
                        cy: (normBottom + normTop) / 2 };
                }
            }
        });

        var boundRect = { id: [], left: Infinity, top: Infinity, right: -Infinity, bottom: -Infinity };
        var boundCoords = {}; // in case of rotated shape, we narrow rect to the point for easier calc

        _.each(allDrawings, function (drawing) {
            var dAttrs = drawing.attrs.drawing;
            var normLeft = Math.round(convertHmmToLength(dAttrs.left, 'px'));
            var normTop = Math.round(convertHmmToLength(dAttrs.top, 'px'));
            var pxWidth = Math.round(convertHmmToLength(dAttrs.width, 'px'));
            var pxHeight = Math.round(convertHmmToLength(dAttrs.height, 'px')) || $(drawing.drawingNode).height();
            var normRight = normLeft + pxWidth;
            var normBottom = normTop + pxHeight;
            if (dAttrs.rotation) {
                var rPoints = getRotatedDrawingPoints({ left: normLeft, top: normTop, width: pxWidth, height: pxHeight }, dAttrs.rotation);
                normLeft = Math.round(rPoints.left);
                normTop = Math.round(rPoints.top);
                normRight = Math.round(rPoints.right);
                normBottom = Math.round(rPoints.bottom);
            }
            boundRect.left = Math.min(boundRect.left, normLeft);
            boundRect.right = Math.max(boundRect.right, normRight);
            boundRect.top = Math.min(boundRect.top, normTop);
            boundRect.bottom = Math.max(boundRect.bottom, normBottom);
            boundRect.centerX = (boundRect.left + boundRect.right) / 2;
            boundRect.centerY = (boundRect.top + boundRect.bottom) / 2;
            boundRect.id.push(drawing.id);
            boundCoords = { left: boundRect.left, top: boundRect.top, right: boundRect.right, bottom: boundRect.bottom, centerX: boundRect.centerX, centerY: boundRect.centerY };

            if (resizePoint) {
                var rPointOffset = resizePoint.offset();
                var rPointPos = { top: (rPointOffset.top - slideOffset.top) / zoomFactor, left: (rPointOffset.left - slideOffset.left) / zoomFactor };

                if (rPointPos.top > boundRect.centerY) {
                    boundCoords.top = boundRect.bottom;
                } else {
                    boundCoords.bottom = boundRect.top;
                }
                if (rPointPos.left > boundRect.centerX) {
                    boundCoords.left = boundRect.right;
                } else {
                    boundCoords.right = boundRect.left;
                }
                // recalculate points again
                boundCoords.centerX = (boundRect.left + boundRect.right) / 2;
                boundCoords.centerY = (boundRect.top + boundRect.bottom) / 2;
            }

        });

        var zoomSlideWidth = slideWidth * zoomFactor;
        var zoomSlideHeight = slideHeight * zoomFactor;
        var propsObj = { staticRect: { top: 0, left: 0, bottom: slideHeight, right: slideWidth }, activeRect: boundRect }; // in this case, static rect is slide
        // v: value relative to active bounding rect,
        // norm: value relative to 0,0,
        // props: utility values,
        // id: useful for debugging, s- slide, key - uuid of the static shape
        lines.multiselection = {
            x: [
                { v: -boundCoords.right,                          norm: 0,                            props: propsObj, id: 'sx1' },
                { v: -boundCoords.left,                           norm: 0,                            props: propsObj, id: 'sx2' },
                { v: slideWidth / 2 - boundCoords.right,          norm: zoomSlideWidth / 2,           props: propsObj, id: 'sx3' },
                { v: slideWidth / 2 - boundCoords.left,           norm: zoomSlideWidth / 2,           props: propsObj, id: 'sx4' },
                { v: slideWidth - boundCoords.left,               norm: zoomSlideWidth,               props: propsObj, id: 'sx5' },
                { v: slideWidth - boundCoords.right,              norm: zoomSlideWidth,               props: propsObj, id: 'sx6' }
            ],
            y: [
                { v: -boundCoords.bottom,                         norm: 0,                            props: propsObj, id: 'sy1' },
                { v: -boundCoords.top,                            norm: 0,                            props: propsObj, id: 'sy2' },
                { v: slideHeight / 2 - boundCoords.bottom,        norm: zoomSlideHeight / 2,          props: propsObj, id: 'sy3' },
                { v: slideHeight / 2 - boundCoords.top,           norm: zoomSlideHeight / 2,          props: propsObj, id: 'sy4' },
                { v: slideHeight - boundCoords.top,               norm: zoomSlideHeight,              props: propsObj, id: 'sy5' },
                { v: slideHeight - boundCoords.bottom,            norm: zoomSlideHeight,              props: propsObj, id: 'sy6' }
            ]
        };
        // center values are not evaluated for resize operation, only for move
        if (!resizePoint) {
            lines.multiselection.x.push(
                { v: -boundCoords.centerX,                  norm: 0,                   props: propsObj, id: 'sx7' },
                { v: slideWidth - boundCoords.centerX,      norm: zoomSlideWidth,      props: propsObj, id: 'sx8' },
                { v: slideWidth / 2 - boundCoords.centerX,  norm: zoomSlideWidth / 2,  props: propsObj, id: 'sx9' }
            );
            lines.multiselection.y.push(
                { v: -boundCoords.centerY,                  norm: 0,                   props: propsObj, id: 'sy7' },
                { v: slideHeight - boundCoords.centerY,     norm: zoomSlideHeight,     props: propsObj, id: 'sy8' },
                { v: slideHeight / 2 - boundCoords.centerY, norm: zoomSlideHeight / 2, props: propsObj, id: 'sx9' }
            );
        }

        _.each(_.keys(staticShapesRects), function (key) {
            var staticShapeRect = staticShapesRects[key];
            var propsObj = { staticRect: staticShapeRect, activeRect: boundRect };

            lines[key] = {
                x: [
                    { v: staticShapeRect.left - boundCoords.left,       norm: staticShapeRect.left * zoomFactor,   props: propsObj, id: key + '_x1' },
                    { v: staticShapeRect.left - boundCoords.right,      norm: staticShapeRect.left * zoomFactor,   props: propsObj, id: key + '_x2' },
                    { v: staticShapeRect.right - boundCoords.left,      norm: staticShapeRect.right * zoomFactor,  props: propsObj, id: key + '_x3' },
                    { v: staticShapeRect.right - boundCoords.right,     norm: staticShapeRect.right * zoomFactor,  props: propsObj, id: key + '_x4' },
                    { v: staticShapeRect.cx - boundCoords.left,         norm: staticShapeRect.cx * zoomFactor,     props: propsObj, id: key + '_x5' },
                    { v: staticShapeRect.cx - boundCoords.right,        norm: staticShapeRect.cx * zoomFactor,     props: propsObj, id: key + '_x6' }
                ],
                y: [
                    { v: staticShapeRect.top - boundCoords.top,         norm: staticShapeRect.top * zoomFactor,    props: propsObj, id: key + '_y1' },
                    { v: staticShapeRect.top - boundCoords.bottom,      norm: staticShapeRect.top * zoomFactor,    props: propsObj, id: key + '_y2' },
                    { v: staticShapeRect.bottom - boundCoords.top,      norm: staticShapeRect.bottom * zoomFactor, props: propsObj, id: key + '_y3' },
                    { v: staticShapeRect.bottom - boundCoords.bottom,   norm: staticShapeRect.bottom * zoomFactor, props: propsObj, id: key + '_y4' },
                    { v: staticShapeRect.cy - boundCoords.top,          norm: staticShapeRect.cy * zoomFactor,     props: propsObj, id: key + '_y5' },
                    { v: staticShapeRect.cy - boundCoords.bottom,       norm: staticShapeRect.cy * zoomFactor,     props: propsObj, id: key + '_y6' }
                ],
                dx: [
                    { v: slideWidth - staticShapeRect.left - boundCoords.right, norm: (slideWidth - staticShapeRect.left) * zoomFactor,  slideLength: slideWidth, props: propsObj, id: key + '_dx1' },
                    { v: slideWidth - staticShapeRect.right - boundCoords.left, norm: (slideWidth - staticShapeRect.right) * zoomFactor, slideLength: slideWidth, props: propsObj, id: key + '_dx2' }
                ],
                dy: [
                    { v: slideHeight - staticShapeRect.top - boundCoords.bottom, norm: (slideHeight - staticShapeRect.top) * zoomFactor,    slideLength: slideHeight, props: propsObj, id: key + '_dy1' },
                    { v: slideHeight - staticShapeRect.bottom - boundCoords.top, norm: (slideHeight - staticShapeRect.bottom) * zoomFactor, slideLength: slideHeight, props: propsObj, id: key + '_dy2' }
                ]
            };
            if (resizePoint) { // lines that painted only on resize
                lines[key].w = [
                    { v: boundRect.left + staticShapeRect.width - boundRect.right,    norm: boundCoords,   props: propsObj, id: key + '_w1' },
                    { v: -(boundRect.left + staticShapeRect.width - boundRect.right), norm: boundCoords,   props: propsObj, id: key + '_w2' }
                ];
                lines[key].h = [
                    { v: boundRect.top + staticShapeRect.height - boundRect.bottom,    norm: boundCoords,   props: propsObj, id: key + '_h1' },
                    { v: -(boundRect.top + staticShapeRect.height - boundRect.bottom), norm: boundCoords,   props: propsObj, id: key + '_h2' }
                ];
            } else { // center values are evaluated only for move
                lines[key].x.push(
                    { v: staticShapeRect.left - boundCoords.centerX,  norm: staticShapeRect.left * zoomFactor,  props: propsObj, id: key + '_x7' },
                    { v: staticShapeRect.right - boundCoords.centerX, norm: staticShapeRect.right * zoomFactor, props: propsObj, id: key + '_x8' },
                    { v: staticShapeRect.cx - boundCoords.centerX,    norm: staticShapeRect.cx * zoomFactor,    props: propsObj, id: key + '_x9' }
                );
                lines[key].y.push(
                    { v: staticShapeRect.bottom - boundCoords.centerY, norm: staticShapeRect.bottom * zoomFactor, props: propsObj, id: key + '_y7' },
                    { v: staticShapeRect.top - boundCoords.centerY,    norm: staticShapeRect.top * zoomFactor,    props: propsObj, id: key + '_y8' },
                    { v: staticShapeRect.cy - boundCoords.centerY,     norm: staticShapeRect.cy * zoomFactor,     props: propsObj, id: key + '_y9' }
                );
            }
        });

        return lines;
    }

    /**
     * Creates guidelines if not existing, and returning them as the collection object.
     *
     * @param {JQuery} contentRootNode
     * @returns {Object}
     */
    function initGuidelines(contentRootNode) {
        if (!app.getView().areSnaplinesEnabled()) { return {}; }

        var appContentNode = contentRootNode.children(APPCONTENT_NODE_SELECTOR);
        var contentHeight = appContentNode.outerHeight() + 100;
        var guidelinesContainer = appContentNode.find('.guidelines-overlay');
        var existingLines = guidelinesContainer.find('.guide-line');
        var vertStartLine, horzStartLine,
            distanceXStaticLine, distanceXActiveLine,
            distanceYStaticLine, distanceYActiveLine,
            distanceXmiddleLine, distanceYmiddleLine,
            wStaticLine, wActiveLine, hStaticLine, hActiveLine/*, vertEndLine, horzEndLine*/;

        if (!guidelinesContainer.length) {
            guidelinesContainer = $('<div class="guidelines-overlay"></div>');
            appContentNode.append(guidelinesContainer);
        }

        if (existingLines.length) {
            vertStartLine = existingLines.eq(0);
            horzStartLine = existingLines.eq(1);
            distanceXStaticLine = existingLines.eq(2);
            distanceXActiveLine = existingLines.eq(3);
            distanceYStaticLine = existingLines.eq(4);
            distanceYActiveLine = existingLines.eq(5);
            distanceXmiddleLine = existingLines.eq(6);
            distanceYmiddleLine = existingLines.eq(7);
            wStaticLine = existingLines.eq(8);
            wActiveLine = existingLines.eq(9);
            hStaticLine = existingLines.eq(10);
            hActiveLine = existingLines.eq(11);
            // vertEndLine = existingLines.eq(12);
            // horzEndLine = existingLines.eq(13);
        } else {
            vertStartLine = $('<div class="guide-line dashed hidden v"></div>').height(contentHeight);
            horzStartLine = $('<div class="guide-line dashed hidden h"></div>');
            distanceXStaticLine = $('<div class="guide-line dashed hidden dxs"></div>');
            distanceXActiveLine = $('<div class="guide-line dashed hidden dxa"></div>');
            distanceYStaticLine = $('<div class="guide-line dashed hidden dys"></div>');
            distanceYActiveLine = $('<div class="guide-line dashed hidden dya"></div>');
            distanceXmiddleLine = $('<div class="guide-line dashed hidden dxmiddle"></div>');
            distanceYmiddleLine = $('<div class="guide-line dashed hidden dymiddle"></div>');
            wStaticLine = $('<div class="guide-line dashed hidden ws"></div>');
            wActiveLine = $('<div class="guide-line dashed hidden wa"></div>');
            hStaticLine = $('<div class="guide-line dashed hidden hs"></div>');
            hActiveLine = $('<div class="guide-line dashed hidden ha"></div>');
            // vertEndLine = $('<div class="guide-line dashed hidden v"></div>').height(contentHeight);
            // horzEndLine = $('<div class="guide-line dashed hidden h"></div>');
            guidelinesContainer.append(vertStartLine, horzStartLine,
                distanceXStaticLine, distanceXActiveLine,
                distanceYStaticLine, distanceYActiveLine,
                distanceXmiddleLine, distanceYmiddleLine,
                wStaticLine, wActiveLine,
                hStaticLine, hActiveLine/*, vertEndLine, horzEndLine*/);
        }

        return {
            xs: horzStartLine, ys: vertStartLine,
            dxs: distanceXStaticLine, dxa: distanceXActiveLine,
            dys: distanceYStaticLine, dya: distanceYActiveLine,
            dxmiddle: distanceXmiddleLine, dymiddle: distanceYmiddleLine,
            ws: wStaticLine, wa: wActiveLine,
            hs: hStaticLine, ha: hActiveLine /*, xe: horzEndLine, xe: vertEndLine*/
        };
    }

    /**
     * Helper function, that checks, if the move shall be stopped because of the target node of this event.
     * Special handling for the iPadAir, where the timer is used -> taking care of the timer (see pagehandermixin.js)
     *
     * @param {Event} event
     *  The mousedown or touchstart event.
     *
     * @returns {Boolean}
     *  Whether it is required to stop the move immediately.
     */
    function isStopMoveRequired(event) {
        // whether the move needs to be stopped
        let stopMove = false;
        // no move, if the event happens on a placeholder template button or inside a table cell node (tracking is also canceled) (DOCS-3915)
        if (isPlaceholderTemplateButton(event.target) || isInsideTableCellNode(event.target)) {
            stopMove = true;
        } else if ($(event.target).closest(TEXTFRAME_NODE_SELECTOR).length > 0) {
            // no move, if the target is inside a textframe in a drawing node -> this is typical for text selection
            stopMove = true;
            if (IPAD_AIR && !drawingNode.hasClass('active-edit')) { // handling for iPadAir (68414)
                const timeDiff = Date.now() - model.getTouchEventTime(); // more than 1 sec for two following 'start' (touchstart) events? -> move instead of text selection
                if ((timeDiff > model.getIOSDoubleTouchTime()) && drawingNode.get(0) === model.getTouchEventDrawing()) { stopMove = false; }
            }
        }
        return stopMove;
    }

    /**
     * Creates callback handlers for tracking notifications while moving a drawing.
     */
    function createDrawingMoveCallbacks() {

        var // the current position change (in px)
            shiftX = 0, shiftY = 0,
            // the current scroll position
            scrollX = 0, scrollY = 0,
            // the start scroll position
            startScrollX = 0, startScrollY = 0,
            // whether the move box was at least once painted
            moveBoxPainted = false,
            activeSlide = model.getSlideById(model.getActiveSlideId()),
            slideOffset,
            allSlideDrawings = null,
            // the start time of the tracking. A move operation is only generated
            // if the tracking duration is larger than Utils.MIN_MOVE_DURATION
            snapLinesContainer,
            guideLinesObj,
            vertGuidelineOffset = 0,
            contentRootNode,
            // the OSN when moving starts
            startOSN = 0;

        // whether the user activated drag to copy with Ctrl-Key or Alt-Key (Mac)
        function isDragToCopyEnabled(record) {
            return _.browser.MacOS ? record.modifiers.altKey : record.modifiers.ctrlKey;
        }

        function prepareMoveDrawing(event) {

            // no move, when some specified nodes got the mousedown/touchstart event
            if (isStopMoveRequired(event)) { return false; }

            // avoid moving, if the event happens for example next to a line.
            var checkDrawingNode = isGroupDrawingFrame(drawingNode) ? getDrawingNode(event.target) : drawingNode;
            return checkEventPosition(app, event, checkDrawingNode);
        }

        // initialing the moving of the drawing according to the passed tracking event
        function startMoveDrawing() {

            zoomFactor = app.getView().getZoomFactor();
            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();
            moveBoxPainted = false;
            isMultiSelection = selection.isMultiSelectionSupported() && selection.isMultiSelection();
            allDrawings = []; // 'start' event is triggered twice consecutively, so we clean collection to prevent doubling
            allConnectors = [];
            allConnectorsOnSlide = model.getAllConnectorsOnSlide(activeSlide);
            allSlideDrawings = model.getAllDrawingsOnSlide(activeSlide);
            slideOffset = activeSlide.offset();
            startOSN = model.getOperationStateNumber();

            collectAndActivateAllMoveBoxes();
            contentRootNode = app.getView().getContentRootNode();
            snapLinesContainer = getStartSnapLinesCoords(allSlideDrawings, allDrawings, zoomFactor);
            guideLinesObj = initGuidelines(contentRootNode);
            vertGuidelineOffset = parseInt(contentRootNode.find('.page').css('margin-top'), 10);
        }

        // updating the drawing position according to the passed tracking event
        function updateMove(record) {

            // if ctrl key (on MacOS the alt key) is enabled and cropping is not active, drawing is copied
            const dragToCopy = isDragToCopyEnabled(record);

            record.cursor(dragToCopy ? 'copy' : 'move');

            // removing 'active-edit' from drawing, should be done only once
            if (drawingNode.hasClass('active-edit')) {
                // but not removing 'active-edit' too fast after touch start (only Samsung (67724))
                if (CHROME_ON_ANDROID && SMALL_DEVICE && (record.duration < (0.5 * MIN_MOVE_DURATION))) {
                    selection.setSelectionIntoTextframe(); // forcing the cursor into the selected drawing (last position)
                    return;
                }
                drawingNode.attr('contenteditable', 'false').removeClass('active-edit');
                if (drawingNode.data('selection')) { drawingNode.data('selection').removeClass('active-edit'); }
            }

            // make move box visible when tracking position has moved
            toggleTracking(drawingNode.data('selection'), true);

            // reading scrollPosition again. Maybe it was not updated or not updated completely.
            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;

            shiftX = (record.offset.x + scrollX) / zoomFactor;
            shiftY = (record.offset.y + scrollY) / zoomFactor;

            // only move the moveBox, if the mouse was really moved
            if (_.isNumber(shiftX) && _.isNumber(shiftY) && ((shiftX !== 0) || (shiftY !== 0))) {
                var updateMoveBox = function (oneDrawing) {
                    var oneDrawingNode = oneDrawing.drawingNode;
                    var flipH = isFlippedHorz(oneDrawingNode);
                    var flipV = isFlippedVert(oneDrawingNode);
                    var rotation = getDrawingRotationAngle(model, oneDrawingNode);

                    if (app.getView().areSnaplinesEnabled() && !isSnapIgnoreKey(record)) {
                        var newShift = model.checkSnapLines(shiftX, shiftY, snapLinesContainer, guideLinesObj, vertGuidelineOffset, zoomFactor, activeSlide);
                        shiftX = newShift.x;
                        shiftY = newShift.y;
                    }
                    var normalizedCoords = getNormalizedMoveCoordinates(shiftX, shiftY, rotation, flipH, flipV);

                    oneDrawing.moveBox.css({ left: normalizedCoords.x, top: normalizedCoords.y, width: oneDrawing.width, height: oneDrawing.height });

                    var connections = getUniqueConnections(allConnectors, oneDrawing.id);
                    if (!record.modifiers.ctrlKey) {
                        _.each(connections, function (connObj) {
                            var normalizedOffset = normalizeMoveOffset(model, oneDrawing.drawingNode, oneDrawing.moveBox);
                            // setting the new attributes of the drawing
                            var opDrawingAttrs = {
                                left: convertLengthToHmm((normalizedOffset.left - slideOffset.left) / zoomFactor, 'px'),
                                top: convertLengthToHmm((normalizedOffset.top - slideOffset.top) / zoomFactor, 'px')
                            };
                            var oneDrawingAttrs = oneDrawing.attrs.drawing;
                            var optionFlipH = isFlippedHorz(oneDrawing.drawingNode);
                            var optionFlipV = isFlippedVert(oneDrawing.drawingNode);
                            var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, opDrawingAttrs, 'move', optionFlipH, optionFlipV, oneDrawing.drawingNode, oneDrawingAttrs.rotation || 0, null, zoomFactor);
                            var newConnProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                            if (newConnProp) {
                                var newDrawingAttrs = newConnProp.attrs.drawing;
                                var calcFlipH = !_.isUndefined(newDrawingAttrs.flipH) ? newDrawingAttrs.flipH : connObj.attrs.drawing.flipH;
                                var calcFlipV = !_.isUndefined(newDrawingAttrs.flipV) ? newDrawingAttrs.flipV : connObj.attrs.drawing.flipV;
                                var transformProp = getCssTransform(0, calcFlipH, calcFlipV);

                                connObj.node.addClass(INDIRECT_CONNECTOR_CHANGE_CLASS);
                                connObj.moveBox.css({ left: convertHmmToLength(newDrawingAttrs.left, 'px'),
                                    top: convertHmmToLength(newDrawingAttrs.top, 'px'),
                                    width: convertHmmToLength((newDrawingAttrs.width || connObj.attrs.drawing.width), 'px'),
                                    height: convertHmmToLength((newDrawingAttrs.height || connObj.attrs.drawing.height), 'px'),
                                    transform: transformProp
                                });
                            }
                        });
                    }
                };

                if (record.modifiers.shiftKey) { // restrict move to x or y axis only
                    if (Math.abs(shiftX) > Math.abs(shiftY)) {
                        shiftY = 0;
                    } else {
                        shiftX = 0;
                    }
                }
                moveBoxPainted = true;

                _.each(allDrawings, updateMoveBox);
                updateConnectorsDebounced();
            }
        }

        // updates scroll position according to the passed tracking event
        function updateMoveScroll(record) {

            // update scrollPosition with suggestion from event
            if (record.scroll.x) {
                scrollNode.scrollLeft(scrollNode.scrollLeft() + record.scroll.x);
            }

            if (record.scroll.y) {
                scrollNode.scrollTop(scrollNode.scrollTop() + record.scroll.y);
            }

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        // handling the drawing position, when moving is stopped according to the passed tracking event
        function stopMoveDrawing(record) {

            var // the operations generator
                generator = model.createOperationGenerator(),
                // whether an update of the saved logical position is required
                positionUpdateRequired = app.isOTEnabled() && startOSN !== model.getOperationStateNumber(), // OT is enabled and external operation received since start resizing
                //
                availablePos = null,
                //
                newPoses = null,
                // if image crop mode is active
                isActiveCropMode = isActiveCropping(drawingNode),
                // if ctrl key is enabled and cropping is not active, drawing is copied
                dragToCopy = isDragToCopyEnabled(record) && !isActiveCropMode,
                // whether at least one drawing is moved above the slide
                isMovedAboveSlide = false,
                // the minimum time for a valid move operation (not checked in unit tests)
                minMoveTime = model.ignoreMinMoveTime() ? -1 : MIN_MOVE_DURATION,
                // whether this is a wanted move operation or just triggered by selecting the drawing
                validOperation = minMoveTime <= record.duration,
                // finalizing the move of one specified drawing
                finalizeDrawing = function (oneDrawing) {
                    var start = positionUpdateRequired ? getOxoPosition(rootNode, oneDrawing.drawingNode, 0) : oneDrawing.start;

                    if (dragToCopy) {
                        if (!availablePos) {
                            availablePos = model.getNextAvailablePositionInActiveSlide();
                            newPoses = [];
                        } else {
                            availablePos[1]++;
                        }
                        start = _.clone(availablePos);
                        newPoses.push(start);

                        var node = oneDrawing.drawingNode;
                        if (isDrawingPlaceHolderNode(node)) { node = getDrawingPlaceHolderNode(node); }

                        // for place holder drawings all attributes need to be included into the operation
                        var allAttributes = isPlaceHolderDrawing(node);

                        generator.generateDrawingOperations(node, start, { ignoreFamily: 'changes presentation', target, getNewDrawingId: true, allAttributes });
                    }

                    var normalizedOffset = normalizeMoveOffset(model, oneDrawing.drawingNode, oneDrawing.moveBox);

                    // setting the new attributes of the drawing
                    operationProperties = {
                        start: _.clone(start),
                        attrs: { drawing: {
                            left: convertLengthToHmm((normalizedOffset.left - rootNode.offset().left) / zoomFactor, 'px'),
                            top: convertLengthToHmm((normalizedOffset.top - rootNode.offset().top) / zoomFactor, 'px') }
                        }
                    };

                    if (isActiveCropMode) {
                        var cropOps = getDrawingMoveCropOp(oneDrawing.drawingNode, oneDrawing.attrs.drawing, operationProperties.attrs.drawing);
                        if (cropOps && !_.isEmpty(cropOps)) {
                            operationProperties.attrs.image = cropOps;
                        }
                    }

                    // generate attributes for disconnection of connector and shape if necessary
                    if (isConnectorDrawingFrame(oneDrawing.drawingNode) && oneDrawing.attrs && oneDrawing.attrs.connector) {
                        var startLinkId = oneDrawing.attrs.connector.startId;
                        var endLinkId = oneDrawing.attrs.connector.endId;

                        // if the linked start or end id drawing is not in the selection, disconnect
                        if (startLinkId && !_.findWhere(allDrawings, { id: startLinkId })) {
                            operationProperties.attrs.connector = { startId: null, startIndex: null };
                        }
                        if (endLinkId && !_.findWhere(allDrawings, { id: endLinkId })) {
                            operationProperties.attrs.connector = operationProperties.attrs.connector || {};
                            operationProperties.attrs.connector.endId = null;
                            operationProperties.attrs.connector.endIndex = null;
                        }
                    }

                    // marking place holder drawings in ODF as modified by the user
                    if (app.isODF() && requiresUserTransformedProperty(drawingNode)) { operationProperties.attrs.presentation = { userTransformed: true }; }

                    extendAndGenerateOp(generator, oneDrawing.drawingNode, operationProperties, target);

                    // adding operations for all drawings, that are dependent from this moved drawing
                    if (app.isODF()) { model.checkODFDependentDrawings(generator, drawingNode, operationProperties, 'move'); }

                    if (!isConnectorDrawingFrame(oneDrawing.drawingNode) && !record.modifiers.ctrlKey) {
                        var connections = getUniqueConnections(allConnectors, oneDrawing.id);
                        if (connections.length) {
                            var opDrawingAttrs = operationProperties.attrs.drawing;
                            var oneDrawingAttrs = oneDrawing.attrs.drawing;
                            var optionFlipH = isFlippedHorz(oneDrawing.drawingNode);
                            var optionFlipV = isFlippedVert(oneDrawing.drawingNode);
                            var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, opDrawingAttrs, 'move', optionFlipH, optionFlipV, oneDrawing.drawingNode, oneDrawingAttrs.rotation || 0, null, zoomFactor);
                            _.each(connections, function (connObj) {
                                var connProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                                if (connProp) {
                                    connProp.start = getOxoPosition(rootNode, connObj.node, 0);
                                    extendAndGenerateOp(generator, connObj.node, connProp, target);
                                }
                            });
                        }
                    }

                    // check whether a drawing is moved above the slide
                    if (operationProperties.attrs.drawing.top < 0) { isMovedAboveSlide = true; }
                };

            if (validOperation && moveBoxPainted && !($(drawingNode).parent().is('.notselectable'))) { // only move drawing, if it is selectable

                _.each(allDrawings, finalizeDrawing);

                // apply the operations (undo group is created automatically)
                model.applyOperations(generator);

                if (dragToCopy) { model.getSelection().setMultiDrawingSelectionByPosition(newPoses); }

                if (isActiveCropMode) { refreshCropFrame(drawingNode); }

                // update of vertical scrollbar is required, if a drawing is moved above the slide
                if (isMovedAboveSlide) { model.trigger('update:verticalscrollbar', { keepScrollPosition: true, pos: model.getActiveSlideIndex() }); }
            }
        }

        // finalizes the move tracking
        function finalizeMoveDrawing() {

            // Resetting variables for new mouseup events without mousemove
            shiftX = shiftY = scrollX = scrollY = 0;

            isMultiSelection = false;
            _.each(allDrawings, function (oneDrawing) {
                oneDrawing.moveBox.css({ left: 0, top: 0, width: 0, height: 0 }).toggle(false);
                updateSelectionDrawingSize(oneDrawing.drawingNode);
                clearDrawing(oneDrawing.drawingNode);
            });
            allConnectors = clearModifiedConnectors(allConnectors);
            allDrawings = [];
            if (guideLinesObj) {
                _.each(guideLinesObj, function (lineObj) { lineObj.addClass('hidden'); }); // as an alternative, lines can be removed from dom
            }

            // switching from text to drawing selection
            if (isTextSelection) { selection.switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareMoveDrawing,
            start: startMoveDrawing,
            move: updateMove,
            scroll: updateMoveScroll,
            end: stopMoveDrawing,
            finally: finalizeMoveDrawing
        };
    }

    /**
     * Creates the callbacks for tracking notifications while resizing a drawing.
     */
    function createDrawingResizeCallbacks() {

        var // the size of the resized drawing (in px)
            finalWidth = 0, finalHeight = 0,
            // the current scroll position
            scrollX = 0, scrollY = 0,
            // the initial scroll position
            startScrollX = 0, startScrollY = 0,
            // whether resizing is available in horizontal/vertical direction
            useX = false, useY = false,
            // whether resizing is available in left or top direction
            useLeft = false, useTop = false,
            // correction factor for resizing to the left/top
            scaleX = 0, scaleY = 0,
            // a collector for the rows of a table drawing
            allRows = null,
            // a counter for the update resize event (to check, whether the drawing was really resized, 50395)
            updateResizeCounter = 0,
            // collection of drawing rectangle properties for all drawings on active slide
            rectanglesCollection = [],
            // stored node of currently hovered shape (on connector resize over)
            hoveredShape = $(),
            // stored node of snap point in shape (on connector resize)
            storedSnapNode = null,
            // type of the drawing
            drawingType = getDrawingType(drawingNode),
            // current active slide
            activeSlide = model.getSlideById(model.getActiveSlideId()),
            // offset value of the current slide
            slideOffset,
            // start offset of the resize handle
            posStartOff,
            // collection of all connectors on slide
            allSlideDrawings = null,
            snapLinesContainer,
            guideLinesObj,
            vertGuidelineOffset = 0,
            contentRootNode,
            // the OSN when resizing starts
            startOSN = 0;

        function prepareResizeDrawing(event) {
            const isResize = !!getResizerHandleType(event.target) && !getCropHandleType(event.target);
            if (isResize) { event.stopPropagation(); }
            return isResize;
        }

        // initializes resizing the drawing according to the passed tracking event
        function startResizeDrawing(record) {

            var // evaluating which resize element is active
                pos = $(record.target).attr('data-pos');

            // helper function to set position properties at move box(es)
            function setPositionAtMoveBox(oneMoveBox) {

                if (useLeft) {
                    scaleX = -1;
                    oneMoveBox.css({ left: 'auto', right: 0 });
                } else {
                    scaleX = 1;
                    oneMoveBox.css({ left: 0, right: 'auto' });
                }
                if (isFlippedHorz(drawingNode)) { scaleX *= -1; }

                if (useTop) {
                    scaleY = -1;
                    oneMoveBox.css({ top: 'auto', bottom: 0 });
                } else {
                    scaleY = 1;
                    oneMoveBox.css({ top: 0, bottom: 'auto' });
                }
                if (isFlippedVert(drawingNode)) { scaleY *= -1; }

            }

            updateResizeCounter = 0; // resetting the counter
            zoomFactor = app.getView().getZoomFactor();
            allConnectorsOnSlide = model.getAllConnectorsOnSlide(activeSlide);
            allSlideDrawings = model.getAllDrawingsOnSlide(activeSlide);
            slideOffset = activeSlide.offset();
            startOSN = model.getOperationStateNumber();

            if (drawingType === 'connector') {
                posStartOff = record.target.getBoundingClientRect();
                posStartOff = { x: posStartOff.left + (posStartOff.width / 2), y: posStartOff.top + (posStartOff.height / 2) };
                rectanglesCollection = model.getAllDrawingRectsOnSlide(activeSlide, slideOffset, zoomFactor);
            }
            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();

            // collecting information about the handle node
            useX = /[lr]/.test(pos);
            useY = /[tb]/.test(pos);

            useLeft = /[l]/.test(pos);
            useTop = /[t]/.test(pos);

            isMultiSelection = selection.isMultiSelectionSupported() && selection.isMultiSelection();
            allDrawings = [];
            allConnectors = [];
            collectAndActivateAllMoveBoxes({ resize: true });

            _.each(allDrawings, function (oneDrawing) {
                setPositionAtMoveBox(oneDrawing.moveBox);
                oneDrawing.drawingNode.addClass(ACTIVE_DRAGGING + ' ' + ACTIVE_RESIZING);
                var selectionDrawingNode = oneDrawing.drawingNode.data('selection');
                if (selectionDrawingNode) { selectionDrawingNode.addClass(ACTIVE_DRAGGING + ' ' + ACTIVE_RESIZING); }
            });

            contentRootNode = app.getView().getContentRootNode();
            snapLinesContainer = getStartSnapLinesCoords(allSlideDrawings, allDrawings, zoomFactor, { resizePoint: $(record.target), slideOffset });
            guideLinesObj = initGuidelines(contentRootNode);
            vertGuidelineOffset = parseInt(contentRootNode.find('.page').css('margin-top'), 10);
        }

        // updates scroll position according to the passed tracking event
        function updateResizeScroll(record) {

            // update scrollPosition with suggestion from event
            scrollNode
                .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
                .scrollTop(scrollNode.scrollTop() + record.scroll.y);

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        // updates resizing the drawing according to the passed tracking event
        function updateResize(record) {

            var // the horizontal shift
                deltaX = record.offset.x / zoomFactor + scrollX,
                // the vertical shift
                deltaY = record.offset.y / zoomFactor + scrollY,
                // the scaling factor for the width
                scaleWidth = 1,
                // the scaling factor for the height
                scaleHeight = 1,
                // normalize and scale deltas of the dimensions
                normalizedDeltas;

            // helper function to set the height of rows and cells in table drawing nodes
            function updateVerticalTableHeight(drawingNode, moveBox, oldHeight) {

                var firstRun = !allRows;
                allRows = allRows || moveBox.find('tr');
                if (firstRun) {
                    _.each(allRows, function (row) {
                        var rowHeight = _.browser.IE ? row.offsetHeight : $(row).height(); // #57882 - jquery v3 height() doesn't return correct value in IE/Edge
                        $(row).data('startrowheight', rowHeight); // saving row height at start of resize
                    });
                }

                oldHeight = oldHeight || drawingNode.height;
                var currentRatio = moveBox.height() / oldHeight; // ratio of drawing height
                _.each(allRows, function (row) {
                    var newRowHeight = $(row).data('startrowheight') * currentRatio;
                    if (_.browser.WebKit || _.browser.IE) {
                        $(row).children('td').css('height', newRowHeight);
                    } else {
                        $(row).css('height', newRowHeight);
                    }
                });
            }

            function clearHoveredShape() {
                if (hoveredShape) {
                    hoveredShape.removeClass('connector-hover');
                    hoveredShape = null; // reset previously stored values
                }
            }

            // helper function to set the current width and height of each selected drawing
            function updateDrawingDimension(drawingNode, moveBox, oldWidth, oldHeight, lockAspectRatio, oneDrawingId, oneDrawingAttrs) {
                var canvasExpansion = 0; // an optional expansion of the canvas, for example for arrows
                var sumWidth = oldWidth + normalizedDeltas.x;
                var sumHeight = oldHeight + normalizedDeltas.y;
                var signWidth = sumWidth > 0 ? 1 : -1;
                var signHeight = sumHeight > 0 ? 1 : -1;
                var flipH = sumWidth < 0;
                var flipV = sumHeight < 0;
                var isTableDrawingFrameLocal = isTableDrawingFrame(drawingNode);

                if (isTableDrawingFrameLocal && (flipH || flipV)) {
                    return false;
                }

                var moveBoxTextframe = moveBox.find('.textframe');
                var transformProp = 'scaleX(' + (flipH ? -1 : 1) + ') scaleY(' + (flipV ? -1 : 1) + ')';
                var topPos, bottomPos, leftPos, rightPos;

                // use the same scaling factor for vertical and horizontal resizing, if both are enabled
                // -> the larger number wins
                if (useX && useY && (lockAspectRatio || record.modifiers.shiftKey) && !isActiveCropping(drawingNode)) {
                    scaleWidth = Math.abs(sumWidth / oldWidth); // scale shouldn't be negative
                    scaleHeight = Math.abs(sumHeight / oldHeight);

                    if (scaleWidth > scaleHeight) {
                        sumHeight = scaleWidth * oldHeight * signHeight;  // return sign of current width/height after scale
                    } else {
                        sumWidth = scaleHeight * oldWidth * signWidth;
                    }
                }

                topPos = useTop ? 'auto' : Math.min(0, sumHeight);
                bottomPos = useTop ? Math.min(0, sumHeight) : 'auto';
                leftPos = useLeft ? 'auto' : Math.min(0, sumWidth);
                rightPos = useLeft ? Math.min(0, sumWidth) : 'auto';

                var isActiveFlipOfRotatedShape = oneDrawingAttrs.drawing.rotation && (flipH || flipV); // disable snap guidlines for this case - not working reliably
                if (app.getView().areSnaplinesEnabled() && !isSnapIgnoreKey(record) && !isActiveFlipOfRotatedShape) { // adjust to snap guidelines
                    var shiftX = sumWidth - oldWidth;
                    var shiftY = sumHeight - oldHeight;

                    if (useLeft) { shiftX *= -1; }
                    if (oneDrawingAttrs.drawing.flipH) { shiftX *= -1; }
                    if (useTop) { shiftY *= -1; }
                    if (oneDrawingAttrs.drawing.flipV) { shiftY *= -1; }

                    if (oneDrawingAttrs.drawing.rotation) {
                        var normShift = getNormalizedMoveCoordinates(shiftX, shiftY, -oneDrawingAttrs.drawing.rotation, flipH, flipV);
                        shiftX = normShift.x;
                        shiftY = normShift.y;
                    }

                    var newShift = model.checkSnapLines(shiftX, shiftY, snapLinesContainer, guideLinesObj, vertGuidelineOffset, zoomFactor, activeSlide);
                    if (oneDrawingAttrs.drawing.rotation) {
                        newShift = getNormalizedMoveCoordinates(newShift.x, newShift.y, oneDrawingAttrs.drawing.rotation, flipH, flipV);
                    }
                    var isShiftDiffX = newShift.x !== shiftX;
                    var isShiftDiffY = newShift.y !== shiftY;

                    if (isShiftDiffX) {
                        if (useLeft) { newShift.x *= -1; }
                        if (oneDrawingAttrs.drawing.flipH) { newShift.x *= -1; }
                        sumWidth = oldWidth + newShift.x;
                    }
                    if (isShiftDiffY) {
                        if (useTop) { newShift.y *= -1; }
                        if (oneDrawingAttrs.drawing.flipV) { newShift.y *= -1; }
                        sumHeight = oldHeight + newShift.y;
                    }
                }

                // update drawing size
                finalWidth = Math.max(1, Math.abs(sumWidth));
                finalHeight = Math.max(1, Math.abs(sumHeight));

                moveBox.css({ width: finalWidth, height: finalHeight, top: topPos, left: leftPos, bottom: bottomPos, right: rightPos, transform: transformProp });
                moveBox.toggleClass(FLIPPED_HORIZONTALLY_CLASSNAME, flipH);
                moveBox.toggleClass(FLIPPED_VERTICALLY_CLASSNAME, flipV);
                moveBox.data({ flipH, flipV });
                moveBoxTextframe.toggleClass('flipH', isOddFlipHVCount(moveBoxTextframe, '.slide'));

                // make move box visible when tracking position has moved
                toggleTracking(drawingNode.data('selection'), true);

                // simulating height changes of tables (makes only sense for single drawing selections because of performance)
                if (useY && !isMultiSelection && isTableDrawingFrameLocal) { updateVerticalTableHeight(drawingNode, moveBox, oldHeight); }

                var connections = getUniqueConnections(allConnectors, oneDrawingId);
                _.each(connections, function (connObj) {
                    var normalizedOffset = normalizeResizeOffset(model, drawingNode, moveBox, useTop, useLeft);
                    // setting the new attributes of the drawing
                    var opDrawingAttrs = {
                        left: convertLengthToHmm((normalizedOffset.left - slideOffset.left) / zoomFactor, 'px'),
                        top: convertLengthToHmm((normalizedOffset.top - slideOffset.top) / zoomFactor, 'px'),
                        width: convertLengthToHmm(finalWidth, 'px'),
                        height: convertLengthToHmm(finalHeight, 'px')
                    };

                    var optionFlipH = flipH ? !isFlippedHorz(drawingNode) : isFlippedHorz(drawingNode);
                    var optionFlipV = flipV ? !isFlippedVert(drawingNode) : isFlippedVert(drawingNode);
                    var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs.drawing, opDrawingAttrs, 'resize', optionFlipH, optionFlipV, drawingNode, oneDrawingAttrs.drawing.rotation || 0, null, zoomFactor);
                    var newConnProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                    if (newConnProp) {
                        var newDrawingAttrs = newConnProp.attrs.drawing;
                        var calcFlipH = !_.isUndefined(newDrawingAttrs.flipH) ? newDrawingAttrs.flipH : connObj.attrs.drawing.flipH;
                        var calcFlipV = !_.isUndefined(newDrawingAttrs.flipV) ? newDrawingAttrs.flipV : connObj.attrs.drawing.flipV;
                        var transformProp = getCssTransform(0, calcFlipH, calcFlipV);

                        connObj.node.addClass(INDIRECT_CONNECTOR_CHANGE_CLASS);
                        connObj.moveBox.css({ left: convertHmmToLength(newDrawingAttrs.left, 'px'),
                            top: convertHmmToLength(newDrawingAttrs.top, 'px'),
                            width: convertHmmToLength((newDrawingAttrs.width || connObj.attrs.drawing.width), 'px'),
                            height: convertHmmToLength((newDrawingAttrs.height || connObj.attrs.drawing.height), 'px'),
                            transform: transformProp
                        });
                    }
                });

                if (!isGroupContentNode(moveBox)) {
                    canvasExpansion = drawingNode.data('canvasexpansion') || 0; // canvas expansion was saved at the drawing node
                    if (_.isObject(canvasExpansion)) { return; } // avoid flickering during resize
                    canvasExpansion *= 2;
                    moveBox.find('canvas').css({ width: finalWidth + canvasExpansion + 2, height: finalHeight + canvasExpansion + 2 }); // make smooth transition, until canvas is updated
                    // cropped images need to have calculated crop settings
                    var cropFrameImg = moveBox.find('.cropping-frame img');
                    if (cropFrameImg.length) {
                        var imageAttrs = oneDrawingAttrs.image;
                        var cropLeft = (imageAttrs && imageAttrs.cropLeft) || 0;
                        var cropRight = (imageAttrs && imageAttrs.cropRight) || 0;
                        var cropTop = (imageAttrs && imageAttrs.cropTop) || 0;
                        var cropBottom = (imageAttrs && imageAttrs.cropBottom) || 0;
                        var horzSettings = calculateBitmapSettings(app, convertLengthToHmm(finalWidth, 'px'), cropLeft, cropRight);
                        var vertSettings = calculateBitmapSettings(app, convertLengthToHmm(finalHeight, 'px'), cropTop, cropBottom);
                        cropFrameImg.css({ width: horzSettings.size, height: vertSettings.size, left: horzSettings.offset, top: vertSettings.offset });
                    }
                }
            }

            if (drawingType === 'connector') {
                var currentPoint = coord.point(record.point.x - slideOffset.left, record.point.y - slideOffset.top);
                storedSnapNode = null; // reset previously stored values
                clearHoveredShape();
                _.each(rectanglesCollection, function (rectObj) {
                    var isInside = pointInsideRect(currentPoint, rectObj.rect);
                    var snapPointsContainer = $(rectObj.node).children('.snap-points-container');
                    snapPointsContainer.toggleClass('connector-hover', isInside);
                    if (isInside) {
                        clearHoveredShape();
                        hoveredShape = snapPointsContainer;
                        _.each(snapPointsContainer.children(), function (snapPointNode) {
                            var snapPointRect = snapPointNode.getBoundingClientRect();
                            var snapPoint = coord.point(snapPointRect.left + snapPointRect.width / 2, snapPointRect.top + snapPointRect.height / 2);
                            // scale points according to zoom
                            $(snapPointNode).css('transform', 'scale(' + (1 / zoomFactor) + ')');

                            if (pointCloseToSnapPoint(record.point, snapPoint)) {
                                // re-initialize deltas with new values of snap points
                                deltaX = (snapPoint.x - posStartOff.x) / zoomFactor + scrollX;
                                deltaY = (snapPoint.y - posStartOff.y) / zoomFactor + scrollY;
                                // store value
                                storedSnapNode = snapPointNode;
                            }
                        });
                    }
                });
            }

            var angle = getDrawingRotationAngle(model, drawingNode);
            normalizedDeltas = getNormalizedResizeDeltas(deltaX, deltaY, useX, useY, scaleX, scaleY, angle);

            _.each(allDrawings, function (oneDrawing) {
                updateDrawingDimension(oneDrawing.drawingNode, oneDrawing.moveBox, oneDrawing.width, oneDrawing.height, oneDrawing.lockResizeAspectRatio, oneDrawing.id, oneDrawing.attrs);
            });

            // debounce updating of text frames in allDrawings on slower browsers & devices
            updateTextFrameShapesDebounced();
            updateConnectorsDebounced();

            updateResizeCounter++; // the drawing is really resized
        }

        // stop resizing of drawing
        function stopResizeDrawing() {

            var // the operations generator
                generator = model.createOperationGenerator(),
                // whether event was triggered while in image crop mode
                isActiveCropMode = isActiveCropping(drawingNode),
                // whether a operation was generated
                operationGenerated = false,
                // whether an update of the saved logical position is required
                positionUpdateRequired = app.isOTEnabled() && startOSN !== model.getOperationStateNumber(); // OT is enabled and external operation received since start resizing

            // helper function to generate the operation for resizing one drawing
            function generateResizeOperation(oneDrawing, drawingNode, moveBox, start) {
                var normalizedOffset = normalizeResizeOffset(model, drawingNode, moveBox, useTop, useLeft);
                var rootNodeOffset = rootNode.offset();

                if (moveBox.css('display') !== 'none' && moveBox.width() && moveBox.height()) { // generate operations only if moveBox has real w/h value, measured without outer
                    // setting the new attributes of the drawing
                    operationProperties = {
                        start: positionUpdateRequired ? getOxoPosition(rootNode, drawingNode, 0) : _.copy(start),
                        attrs: { drawing: {
                            width: convertLengthToHmm(moveBox.outerWidth(true), 'px'), // 'outer' is required to avoid shrinking in unused dimension
                            height: convertLengthToHmm(moveBox.outerHeight(true), 'px') }
                        }
                    };

                    // setting a minimum height at frames (ODP only), 55236
                    if (app.isODF() && isMinHeightDrawingFrame(drawingNode)) {
                        operationProperties.attrs.drawing.minFrameHeight = convertLengthToHmm(moveBox.outerHeight(true), 'px');
                    }

                    if (moveBox.data('flipH')) {
                        operationProperties.attrs.drawing.flipH = !isFlippedHorz(drawingNode);
                        operationProperties.attrs.drawing.left = convertLengthToHmm((normalizedOffset.left - rootNodeOffset.left) / zoomFactor, 'px');
                    }
                    if (moveBox.data('flipV')) {
                        if (app.isODF()) {
                            operationProperties.attrs.drawing.rotation = math.modulo((oneDrawing.attrs.drawing.rotation || 0) + 180, 360);
                            operationProperties.attrs.drawing.flipH = !_.isUndefined(operationProperties.attrs.drawing.flipH) ? !operationProperties.attrs.drawing.flipH : !isFlippedHorz(drawingNode);
                        } else {
                            operationProperties.attrs.drawing.flipV = !isFlippedVert(drawingNode);
                        }
                        operationProperties.attrs.drawing.top = convertLengthToHmm((normalizedOffset.top - rootNodeOffset.top) / zoomFactor, 'px');
                    }

                    // marking place holder drawings in ODF as modified by the user
                    if (app.isODF() && requiresUserTransformedProperty(drawingNode)) { operationProperties.attrs.presentation = { userTransformed: true }; }

                    // evaluating useTop and useLeft to avoid sending of superfluous drawing information
                    if (useTop || normalizedOffset.sendMoveOperation) { operationProperties.attrs.drawing.top = convertLengthToHmm((normalizedOffset.top - rootNodeOffset.top) / zoomFactor, 'px'); }
                    if (useLeft || normalizedOffset.sendMoveOperation) { operationProperties.attrs.drawing.left = convertLengthToHmm((normalizedOffset.left - rootNodeOffset.left) / zoomFactor, 'px'); }

                    if (useX && isNoWordWrapDrawingFrame(drawingNode) && isAutoResizeHeightDrawingFrame(drawingNode)) {
                        operationProperties.attrs.shape = { wordWrap: true };
                    }

                    if (drawingType === 'connector') {
                        var ind = useLeft ? 'startIndex' : 'endIndex';
                        var id = useLeft ? 'startId' : 'endId';
                        operationProperties.attrs.connector = {};
                        if (storedSnapNode) {
                            var startInd = parseInt($(storedSnapNode).data('cxnInd'), 10);
                            var drawingAttrs = model.getDrawingAttrsByNode(getDrawingNode(storedSnapNode));
                            var startId = (drawingAttrs && drawingAttrs.id) || null;
                            if (_.isNumber(startInd)) { operationProperties.attrs.connector[ind] = startInd; }
                            if (startId) {
                                operationProperties.attrs.connector[id] = startId;
                            } else {
                                globalLogger.error('stopResizeDrawing(): missing drawing id for drawing: ', getDrawingNode(storedSnapNode));
                            }
                        } else { // link is disconnected
                            operationProperties.attrs.connector[ind] = null;
                            operationProperties.attrs.connector[id] = null;
                        }
                    }

                    if (isActiveCropMode) {
                        var cropOps = getDrawingResizeCropOp(oneDrawing.drawingNode, oneDrawing.attrs.drawing, operationProperties.attrs.drawing, useLeft, useTop);
                        if (cropOps && !_.isEmpty(cropOps)) {
                            operationProperties.attrs.image = cropOps;
                        }
                    }

                    extendAndGenerateOp(generator, drawingNode, operationProperties, target);

                    // adding operations for all drawings, that are dependent from this moved drawing
                    if (app.isODF()) { model.checkODFDependentDrawings(generator, drawingNode, operationProperties, 'resize'); }

                    if (!isConnectorDrawingFrame(drawingNode)) {
                        var connections = getUniqueConnections(allConnectors, oneDrawing.id);
                        if (connections.length) {
                            var opDrawingAttrs = operationProperties.attrs.drawing;
                            var oneDrawingAttrs = oneDrawing.attrs.drawing;
                            var optionFlipH = _.isUndefined(opDrawingAttrs.flipH) ? isFlippedHorz(drawingNode) : !isFlippedHorz(drawingNode);
                            var optionFlipV = _.isUndefined(opDrawingAttrs.flipV) ? isFlippedVert(drawingNode) : !isFlippedVert(drawingNode);
                            var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, opDrawingAttrs, 'resize', optionFlipH, optionFlipV, drawingNode, oneDrawingAttrs.rotation || 0, null, zoomFactor);

                            _.each(connections, function (connObj) {
                                var connProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                                if (connProp) {
                                    connProp.start = getOxoPosition(rootNode, connObj.node, 0);
                                    extendAndGenerateOp(generator, connObj.node, connProp, target);
                                }
                            });
                        }
                    }

                    operationGenerated = true; // setting marker for generated operation
                }
            }

            // helper function to generate the operation for resizing one drawing of type table
            function generateTableDrawingResizeOperations(oneDrawing, drawingNode, moveBox, start, height) {

                // an update of the start position might be required in OT case
                start = positionUpdateRequired ? getOxoPosition(rootNode, drawingNode, 0) : start;

                if (useX || useTop) {
                    // generating the drawing operation for the width or the movement upwards
                    generateResizeOperation(oneDrawing, drawingNode, moveBox, start);
                }

                if (useY) {
                    // increase the height of all table rows, if the drawing height has changed
                    // -> height must not be set explicitely for table drawings, because this is
                    //    determined by the height of all rows.
                    var tableNode = drawingNode.children('.content:not(.copy)').find('table');  // the table node, that is NOT inside the copy content
                    var oldHeight = height || tableNode.outerHeight(true);
                    var newHeight = moveBox.outerHeight(true);
                    var factor = newHeight / oldHeight;
                    var allRows = getTableRows(tableNode);  // using specific table node, otherwise there might be too many rows because of 'copy' node
                    var newRowHeight = 0;
                    var rowCounter = 0;

                    _.each(allRows, function (row) {
                        var rowHeight = _.browser.IE ? row.offsetHeight : $(row).outerHeight(true); // #57882 - jquery v3 height() doesn't return correct value in IE/Edge
                        newRowHeight = Math.round(convertLengthToHmm(rowHeight, 'px') * factor);
                        // -> not using the saved height in the explicit attributes, because this value might be lower
                        //    than the 'real' height, if the text content forces the row to be higher.

                        operationProperties = {
                            start: appendNewIndex(start, rowCounter),
                            attrs: { row: { height: newRowHeight } }
                        };

                        extendAndGenerateOp(generator, drawingNode, operationProperties, target);

                        rowCounter++;
                    });

                    if (app.isODF() && model.useSlideMode()) { // table height must be send to the filter via operation
                        model.trigger('drawingHeight:update', drawingNode);
                    }

                    operationGenerated = true; // setting marker for generated operation
                }
            }

            if (updateResizeCounter === 0) { return; } // no resize -> no operation

            _.each(allDrawings, function (oneDrawing) {

                operationGenerated = false;

                if (isTableDrawingFrame(oneDrawing.drawingNode)) {
                    generateTableDrawingResizeOperations(oneDrawing, oneDrawing.drawingNode, oneDrawing.moveBox, oneDrawing.start, oneDrawing.height);
                } else {
                    generateResizeOperation(oneDrawing, oneDrawing.drawingNode, oneDrawing.moveBox, oneDrawing.start);
                }

                if (operationGenerated) { // <- do not use canvas copy, if no operation could be generated (for example after setting width to 0)
                    // prevent flickering on finalize resizing by replacing original canvas with repainted one
                    var copyCanvas = oneDrawing.moveBox.children('canvas');
                    oneDrawing.drawingNode.children('.textframecontent').not('.copy').children('canvas').replaceWith(copyCanvas);
                }
            });

            // Applying the operations (undo group is created automatically)
            model.applyOperations(generator);
            // repaint adjustment points of shape after resize, if not multi selection
            if (!isMultiSelection && drawingNode && drawingNode.length && !isActiveCropMode) {
                clearSelection(drawingNode);
                selection.drawDrawingSelection(drawingNode);
            }

            if (isActiveCropMode) { refreshCropFrame(drawingNode); }
        }

        // finalizes the resize tracking
        function finalizeResizeDrawing() {

            isMultiSelection = false;
            _.each(allDrawings, function (oneDrawing) {
                oneDrawing.moveBox.css({ left: 0, top: 0, width: 0, height: 0 }).toggle(false);
                updateSelectionDrawingSize(oneDrawing.drawingNode);
                // oneDrawing.drawingNode.data('selection').css({ transform: oneDrawing.drawingNode.css('transform'), left: oneDrawing.drawingNode.css('left'), top: oneDrawing.drawingNode.css('top'), width: oneDrawing.drawingNode.width(), height: oneDrawing.drawingNode.height() });
                clearDrawing(oneDrawing.drawingNode);
            });
            allConnectors = clearModifiedConnectors(allConnectors);
            allRows = null;
            rectanglesCollection = [];
            if (hoveredShape && hoveredShape.length) { hoveredShape.removeClass('connector-hover'); }
            if (drawingType === 'connector') { activeSlide.find('.snap-points-container').remove(); }
            if (guideLinesObj) {
                _.each(guideLinesObj, function (lineObj) { lineObj.addClass('hidden'); }); // as an alternative, lines can be removed from dom
            }

            // switching from text to drawing selection
            if (isTextSelection) { selection.switchAdditionalDrawingToDrawingSelection(); }

            _.each(allDrawings, function (oneDrawing) {
                model.trigger('drawingHeight:update', oneDrawing.drawingNode); // additional update required after blocker was removed (DOCS-3420)
            });

            allDrawings = [];
        }

        return {
            prepare: prepareResizeDrawing,
            start: startResizeDrawing,
            move: updateResize,
            scroll: updateResizeScroll,
            end: stopResizeDrawing,
            finally: finalizeResizeDrawing
        };
    }

    function createDrawingRotateCallbacks() {
        // dimensions of the drawing
        var drawingWidth = 0, drawingHeight = 0;
        // position of the center rotation of the drawingWidth
        var centerX, centerY;
        // the current scroll position
        var scrollX = 0, scrollY = 0;
        // the initial scroll position
        var startScrollX = 0, startScrollY = 0;
        // starting angle before rotation
        var startAngle = 0;
        // final angle on finish of the rotation
        var rotationAngle = null;
        // flag for marking that the operation is generated
        var generatedOp = false;
        var activeSlide = null;
        var allSlideDrawings = null;
        // the OSN when rotating starts
        var startOSN = 0;

        function prepareRotateDrawing(event) {
            const isRotate = isRotateHandle(event.target);
            if (isRotate) { event.stopPropagation(); }
            return isRotate;
        }

        function startRotateDrawing() {

            var drawingOffset;

            activeSlide = model.getSlideById(model.getActiveSlideId());
            allConnectorsOnSlide = model.getAllConnectorsOnSlide(activeSlide);
            allSlideDrawings = model.getAllDrawingsOnSlide(activeSlide);
            zoomFactor = app.getView().getZoomFactor();
            startAngle = getDrawingRotationAngle(model, drawingNode);
            startOSN = model.getOperationStateNumber();

            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();

            if (_.isNumber(startAngle) && (startAngle !== 0)) {
                $(drawingNode).css('transform', ''); // reset to def to get values
                drawingOffset = drawingNode.offset();
                updateCssTransform(drawingNode, startAngle);
            } else {
                startAngle = 0;
                drawingOffset = drawingNode.offset();
            }
            if (selection.isAdditionalTextframeSelection()) {
                getTextFrameNode(drawingNode, { deep: true }).attr('contenteditable', false);
            }

            drawingHeight = drawingNode.height() * zoomFactor;
            drawingWidth = drawingNode.width() * zoomFactor;
            centerX = drawingOffset.left + (drawingWidth / 2);
            centerY = drawingOffset.top + (drawingHeight / 2);

            isMultiSelection = selection.isMultiSelectionSupported() && selection.isMultiSelection();
            allDrawings = []; // 'start' event is triggered twice consecutively, so we clean collection to prevent doubling
            allConnectors = [];
            collectAndPrepareDrawingsForRotation(isMultiSelection, drawingNode, startAngle);
        }

        function updateRotate(record) {
            var radians = Math.atan2(record.point.x + scrollX - centerX, record.point.y + scrollY - centerY);
            rotationAngle = Math.round(180 - radians * 180 / Math.PI);

            if (record.modifiers.shiftKey) { rotationAngle = math.roundp(rotationAngle, 15); } // make 15 deg step with shift key
            if (isFlippedVert(drawingNode)) { rotationAngle += 180; }

            _.each(allDrawings, function (obj) {
                var objAngle = math.modulo(rotationAngle - startAngle + obj.rotation, 360);
                var flipH = isFlippedHorz(obj.drawingNode);
                var flipV = isFlippedVert(obj.drawingNode);
                var scaleX = flipH ? 'scaleX(-1)' : 'scaleX(1)';
                var scaleY = flipV ? ' scaleY(-1)' : ' scaleY(1)';
                var selectionNode = obj.drawingNode.data('selection');
                var oneDrawingId = obj.id;

                updateCssTransform(obj.drawingNode, objAngle);
                selectionNode.css('transform', 'rotate(' + objAngle + 'deg) ' + scaleX + scaleY);
                addRotationHint(selectionNode.find(ROTATE_ANGLE_HINT_SELECTOR), objAngle, flipH, flipV);

                var connections = getUniqueConnections(allConnectors, oneDrawingId);
                var oneDrawingAttrs = obj.attrs.drawing;
                var optionFlipH = isFlippedHorz(obj.drawingNode);
                var optionFlipV = isFlippedVert(obj.drawingNode);
                var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, oneDrawingAttrs, 'rotate', optionFlipH, optionFlipV, obj.drawingNode, objAngle || 0, obj.rotation || 0, zoomFactor);
                _.each(connections, function (connObj) {
                    var newConnProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                    if (newConnProp) {
                        var newDrawingAttrs = newConnProp.attrs.drawing;
                        var calcFlipH = !_.isUndefined(newDrawingAttrs.flipH) ? newDrawingAttrs.flipH : connObj.attrs.drawing.flipH;
                        var calcFlipV = !_.isUndefined(newDrawingAttrs.flipV) ? newDrawingAttrs.flipV : connObj.attrs.drawing.flipV;
                        var transformProp = getCssTransform(0, calcFlipH, calcFlipV);

                        connObj.node.addClass(INDIRECT_CONNECTOR_CHANGE_CLASS);
                        connObj.moveBox.css({ left: convertHmmToLength(newDrawingAttrs.left, 'px'),
                            top: convertHmmToLength(newDrawingAttrs.top, 'px'),
                            width: convertHmmToLength((newDrawingAttrs.width || connObj.attrs.drawing.width), 'px'),
                            height: convertHmmToLength((newDrawingAttrs.height || connObj.attrs.drawing.height), 'px'),
                            transform: transformProp
                        });
                    }
                });
            });

            updateConnectorsDebounced();
        }

        function updateRotateScroll(record) {
            // update scrollPosition with suggestion from event
            scrollNode
                .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
                .scrollTop(scrollNode.scrollTop() + record.scroll.y);

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        function stopRotateDrawing() {
            var generator = model.createOperationGenerator();
            var operationProperties;
            // whether an update of the saved logical position is required
            var positionUpdateRequired = app.isOTEnabled() && startOSN !== model.getOperationStateNumber(); // OT is enabled and external operation received since start resizing

            if (_.isNumber(rotationAngle)) {
                _.each(allDrawings, function (obj) {
                    // calculate angle value for each drawing and restrict between 0 and 359 degree
                    var oneDrawingAngle = math.modulo(rotationAngle - startAngle + obj.rotation, 360);
                    operationProperties = { start: positionUpdateRequired ? getOxoPosition(rootNode, obj.drawingNode, 0) : _.clone(obj.start), attrs: { drawing: { rotation: oneDrawingAngle } } };
                    // marking place holder drawings in ODF as modified by the user
                    if (app.isODF() && requiresUserTransformedProperty(drawingNode)) { operationProperties.attrs.presentation = { userTransformed: true }; }

                    if (!isConnectorDrawingFrame(obj.drawingNode)) {
                        var connections = getUniqueConnections(allConnectors, obj.id);
                        if (connections.length) {
                            var opDrawingAttrs = operationProperties.attrs.drawing;
                            var oneDrawingAttrs = obj.attrs.drawing;
                            var optionFlipH = isFlippedHorz(obj.drawingNode);
                            var optionFlipV = isFlippedVert(obj.drawingNode);
                            var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, oneDrawingAttrs, 'rotate', optionFlipH, optionFlipV, obj.drawingNode, opDrawingAttrs.rotation || 0, obj.rotation || 0, zoomFactor);

                            _.each(connections, function (connObj) {
                                var connProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                                if (connProp) {
                                    connProp.start = getOxoPosition(rootNode, connObj.node, 0);
                                    extendAndGenerateOp(generator, connObj.node, connProp, target);
                                }
                            });
                        }
                    }

                    extendAndGenerateOp(generator, obj.drawingNode, operationProperties, target);
                });

                // Applying the operations (undo group is created automatically)
                model.applyOperations(generator);
                generatedOp = true;
            }
        }

        function finalizeRotateDrawing() {
            _.each(allDrawings, function (obj) {
                obj.drawingNode.removeClass(ROTATING_STATE_CLASSNAME);
                removeRotateAngleHint(obj.drawingNode);
                if (!generatedOp) { // reset to start angle if tracking was canceled
                    updateCssTransform(obj.drawingNode, obj.rotation || 0);
                } else {
                    updateResizersMousePointers(obj.drawingNode, getDrawingRotationAngle(model, obj.drawingNode));
                }
                obj.drawingNode.removeClass(FRAME_WITH_TEMP_BORDER2);
                var drawingSelNode = obj.drawingNode.data('selection');
                if (drawingSelNode) { drawingSelNode.removeClass(ROTATING_STATE_CLASSNAME).css({ transform: obj.drawingNode.css('transform') }); }
            });

            allConnectors = clearModifiedConnectors(allConnectors);

            if (selection.isAdditionalTextframeSelection()) {
                getTextFrameNode(drawingNode, { deep: true }).attr({ contenteditable: true, 'data-gramm': false });
                selection.restoreBrowserSelection();
            }

            if (isMultiSelection) {
                isMultiSelection = false;
                allDrawings = [];
            }
            generatedOp = false;

            // switching from text to drawing selection
            if (isTextSelection) { selection.switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareRotateDrawing,
            start: startRotateDrawing,
            move: updateRotate,
            scroll: updateRotateScroll,
            end: stopRotateDrawing,
            finally: finalizeRotateDrawing
        };
    }

    /**
     * Creates the callbacks for moving adjustment handle of the shape.
     */
    function createDrawingAdjustCallbacks() {
        var scrollX = 0, scrollY = 0;
        var deltaX, deltaY, normDeltas;
        // the start scroll position
        var startScrollX = 0, startScrollY = 0;
        var adjHandlerNode, adjPos;
        var cxnObject, ahList, avList, gdList;
        var moveX = false, moveY = false, isPolar = false;
        var xDiffpx, yDiffpx, minMaxRdiff;
        var drawingRotation, scaleX, scaleY, snapRect;
        var minXValue, maxXValue, minYValue, maxYValue;
        var minXpos, maxXpos, minYpos, maxYpos, minAng, maxAng;
        var minR, maxR, minRPolar, maxRPolar, minRposPx, maxRposPx;
        var absMinMaxSumX, absMinMaxSumY, absMinMaxSumR;
        var actXpos, actYpos, currentPolar, currentAng, currentRadius;
        var activeSlide = model.getSlideById(model.getActiveSlideId());
        var allSlideDrawings = null;

        function prepareAdjust(event) {
            const isAdjust = isAdjustmentHandle(event.target);
            if (isAdjust) { event.stopPropagation(); }
            return isAdjust;
        }

        function startAdjust(record) {
            adjHandlerNode = $(record.target);
            zoomFactor = app.getView().getZoomFactor();
            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();
            drawingRotation = getDrawingRotationAngle(model, drawingNode);
            scaleX = isFlippedHorz(drawingNode) ? -1 : 1;
            scaleY = isFlippedVert(drawingNode) ? -1 : 1;
            cxnObject = adjHandlerNode.data('cxnObj');
            avList = cxnObject.avList;
            gdList = cxnObject.gdList;
            ahList = cxnObject.ah;
            isPolar = ahList.type === 'polar';
            moveX = !_.isUndefined(ahList.minX);
            moveY = !_.isUndefined(ahList.minY);
            snapRect = new Rectangle(0, 0, drawingNode.width(), drawingNode.height());
            adjPos = getPointPixelPosition(ahList, snapRect, avList, gdList);
            allDrawings = [];
            allConnectors = [];
            allConnectorsOnSlide = model.getAllConnectorsOnSlide(activeSlide);
            allSlideDrawings = model.getAllDrawingsOnSlide(activeSlide);
            collectAndActivateAllMoveBoxes({ resize: true });

            if (isPolar) {
                currentPolar = snapRect.pointToPolar(adjPos);
                if (!_.isUndefined(ahList.minAng) || !_.isUndefined(ahList.maxAng)) {
                    minAng = getGeometryValue(ahList.minAng, snapRect, avList, gdList) / 60000;
                    maxAng = getGeometryValue(ahList.maxAng, snapRect, avList, gdList) / 60000;
                    currentAng = Math.round(currentPolar.a * 180 / Math.PI);
                    if (currentAng < 0) { currentAng += 360; }
                    currentAng = math.clamp(currentAng, minAng, maxAng);
                }
                if (!_.isUndefined(ahList.minR) || !_.isUndefined(ahList.maxR)) {
                    currentRadius = currentPolar.r;
                    minR = getGeometryValue(ahList.minR, snapRect, avList, gdList);
                    minRposPx = getPxPosFromGeo(minR, snapRect, avList, ahList, gdList, ahList.gdRefR);
                    minRPolar = snapRect.pointToPolar(minRposPx).r;

                    maxR = getGeometryValue(ahList.maxR, snapRect, avList, gdList);
                    maxRposPx = getPxPosFromGeo(maxR, snapRect, avList, ahList, gdList, ahList.gdRefR);
                    maxRPolar = snapRect.pointToPolar(maxRposPx).r;

                    absMinMaxSumR = Math.abs(maxR - minR);
                    minMaxRdiff = Math.abs(maxRPolar - minRPolar);
                }
            }

            if (moveX) {
                minXValue = getGeometryValue(ahList.minX, snapRect, avList, gdList);
                minXpos = getPxPosFromGeo(minXValue, snapRect, avList, ahList, gdList, ahList.gdRefX);

                maxXValue = getGeometryValue(ahList.maxX, snapRect, avList, gdList);
                maxXpos = getPxPosFromGeo(maxXValue, snapRect, avList, ahList, gdList, ahList.gdRefX);

                xDiffpx = Math.abs(maxXpos.x - minXpos.x);
                absMinMaxSumX = Math.abs(maxXValue - minXValue);
            }
            if (moveY) {
                minYValue = getGeometryValue(ahList.minY, snapRect, avList, gdList);
                minYpos = getPxPosFromGeo(minYValue, snapRect, avList, ahList, gdList, ahList.gdRefY);

                maxYValue = getGeometryValue(ahList.maxY, snapRect, avList, gdList);
                maxYpos = getPxPosFromGeo(maxYValue, snapRect, avList, ahList, gdList, ahList.gdRefY);

                yDiffpx = Math.abs(maxYpos.y - minYpos.y);
                absMinMaxSumY = Math.abs(maxYValue - minYValue);
            }
        }

        function updateAdjust(record) {
            var allowX = isPolar || moveX;
            var allowY = isPolar || moveY;
            var modAttrs = { avList: _.copy(avList) };

            deltaX = record.offset.x / zoomFactor + scrollX;
            deltaY = record.offset.y / zoomFactor + scrollY;
            normDeltas = getNormalizedResizeDeltas(deltaX, deltaY, allowX, allowY, scaleX, scaleY, drawingRotation);
            actXpos = normDeltas.x + adjPos.x;
            actYpos = normDeltas.y + adjPos.y;
            if (moveX) {
                actXpos = math.clamp(actXpos, Math.min(maxXpos.x, minXpos.x), Math.max(maxXpos.x, minXpos.x));
                if (_.isFinite(actXpos)) { modAttrs.avList[ahList.gdRefX] = scaleAdjustmentValue(actXpos, minXpos.x, maxXpos.x, minXValue, xDiffpx, absMinMaxSumX); }
            }
            if (moveY) {
                actYpos = math.clamp(actYpos, Math.min(maxYpos.y, minYpos.y), Math.max(maxYpos.y, minYpos.y));
                if (_.isFinite(actYpos)) { modAttrs.avList[ahList.gdRefY] = scaleAdjustmentValue(actYpos, minYpos.y, maxYpos.y, minYValue, yDiffpx, absMinMaxSumY); }

            }
            if (isPolar) {
                currentPolar = snapRect.pointToPolar({ x: actXpos, y: actYpos });
                if (!_.isUndefined(minAng) || !_.isUndefined(maxAng)) {
                    currentAng = Math.round(currentPolar.a * 180 / Math.PI);
                    if (currentAng < 0) { currentAng += 360; }
                    currentAng = math.clamp(currentAng, minAng, maxAng);
                }
                if (!_.isUndefined(minR) || !_.isUndefined(maxR)) {
                    currentRadius = currentPolar.r;
                    currentRadius = math.clamp(currentRadius, Math.min(minRPolar, maxRPolar), Math.max(minRPolar, maxRPolar));
                }
                if (!_.isUndefined(ahList.gdRefAng)) {
                    modAttrs.avList[ahList.gdRefAng] = currentAng * 60000;
                }
                if (!_.isUndefined(ahList.gdRefR)) {
                    modAttrs.avList[ahList.gdRefR] = scaleAdjustmentValue(currentRadius, minRPolar, maxRPolar, minR, minMaxRdiff, absMinMaxSumR, true);
                }
            }

            adjHandlerNode.css({ left: actXpos - 5, top: actYpos - 5 });
            _.each(allDrawings, function (oneDrawing) {
                oneDrawing.moveBox.data('modAttrs', modAttrs);

                var connections = getUniqueConnections(allConnectors, oneDrawing.id);
                var oneDrawingAttrs = oneDrawing.attrs.drawing;
                var optionFlipH = isFlippedHorz(oneDrawing.drawingNode);
                var optionFlipV = isFlippedVert(oneDrawing.drawingNode);
                var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, oneDrawingAttrs, 'adjust', optionFlipH, optionFlipV, oneDrawing.drawingNode, oneDrawingAttrs.rotation || 0, null, zoomFactor);
                optionObj.avList = modAttrs.avList;
                _.each(connections, function (connObj) {
                    var newConnProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                    if (newConnProp) {
                        var newDrawingAttrs = newConnProp.attrs.drawing;
                        var calcFlipH = !_.isUndefined(newDrawingAttrs.flipH) ? newDrawingAttrs.flipH : connObj.attrs.drawing.flipH;
                        var calcFlipV = !_.isUndefined(newDrawingAttrs.flipV) ? newDrawingAttrs.flipV : connObj.attrs.drawing.flipV;
                        var transformProp = getCssTransform(0, calcFlipH, calcFlipV);

                        connObj.node.addClass(INDIRECT_CONNECTOR_CHANGE_CLASS);
                        connObj.moveBox.css({ left: convertHmmToLength(newDrawingAttrs.left, 'px'),
                            top: convertHmmToLength(newDrawingAttrs.top, 'px'),
                            width: convertHmmToLength((newDrawingAttrs.width || connObj.attrs.drawing.width), 'px'),
                            height: convertHmmToLength((newDrawingAttrs.height || connObj.attrs.drawing.height), 'px'),
                            transform: transformProp
                        });
                    }
                });
            });
            updateAdjustmentShapeDebounced();
            updateConnectorsDebounced();
        }

        function updateAdjustScroll(record) {
            scrollNode
                .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
                .scrollTop(scrollNode.scrollTop() + record.scroll.y);

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        function stopAdjust() {
            var generator = model.createOperationGenerator();
            var operationProperties = { attrs: { geometry: { avList: _.copy(avList) } }, start: getOxoPosition(rootNode, drawingNode, 0) };
            var opCreated = false;
            var scaledValue = null;

            if (moveX && _.isFinite(actXpos)) {
                scaledValue = scaleAdjustmentValue(actXpos, minXpos.x, maxXpos.x, minXValue, xDiffpx, absMinMaxSumX);
                if (_.isFinite(scaledValue)) {
                    operationProperties.attrs.geometry.avList[ahList.gdRefX] = scaledValue;
                    opCreated = true;
                }
            }
            if (moveY && _.isFinite(actYpos)) {
                scaledValue = scaleAdjustmentValue(actYpos, minYpos.y, maxYpos.y, minYValue, yDiffpx, absMinMaxSumY);
                if (_.isFinite(scaledValue)) {
                    operationProperties.attrs.geometry.avList[ahList.gdRefY] = scaledValue;
                    opCreated = true;
                }
            }
            if (isPolar) {
                if (!_.isUndefined(ahList.gdRefAng) && _.isFinite(currentAng)) {
                    operationProperties.attrs.geometry.avList[ahList.gdRefAng] = currentAng * 60000;
                    opCreated = true;
                }
                if (!_.isUndefined(ahList.gdRefR)) {
                    scaledValue = scaleAdjustmentValue(currentRadius, minRPolar, maxRPolar, minR, minMaxRdiff, absMinMaxSumR, true);
                    if (_.isFinite(scaledValue)) {
                        operationProperties.attrs.geometry.avList[ahList.gdRefR] = scaledValue;
                        opCreated = true;
                    }
                }
            }
            if (opCreated) {
                extendAndGenerateOp(generator, drawingNode, operationProperties, target);

                _.each(allDrawings, function (obj) {
                    if (!isConnectorDrawingFrame(obj.drawingNode)) {
                        var connections = getUniqueConnections(allConnectors, obj.id);
                        if (connections.length) {
                            var oneDrawingAttrs = obj.attrs.drawing;
                            var optionFlipH = isFlippedHorz(obj.drawingNode);
                            var optionFlipV = isFlippedVert(obj.drawingNode);
                            var optionObj = generateRecalcConnectorOptions(oneDrawingAttrs, oneDrawingAttrs, 'adjust', optionFlipH, optionFlipV, obj.drawingNode, oneDrawingAttrs.rotation || 0, null, zoomFactor);
                            optionObj.avList = operationProperties.attrs.geometry.avList;

                            _.each(connections, function (connObj) {
                                var connProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                                if (connProp) {
                                    connProp.start = getOxoPosition(rootNode, connObj.node, 0);
                                    extendAndGenerateOp(generator, connObj.node, connProp, target);
                                }
                            });
                        }
                    }
                });

                model.applyOperations(generator);
                if (drawingNode && drawingNode.length) {
                    if (isAutoResizeHeightDrawingFrame(drawingNode)) {
                        // drawing selection must be set async for drawings with autoresize height (DOCS-3809)
                        selection.setTimeout(function () {
                            clearSelection(drawingNode);
                            selection.drawDrawingSelection(drawingNode);
                        }, 0);
                    } else {
                        clearSelection(drawingNode);
                        selection.drawDrawingSelection(drawingNode);
                    }
                }
            }
        }

        function finalizeAdjust() {

            // Resetting variables for new mouseup events without mousemove
            scrollX = scrollY = 0;

            _.each(allDrawings, function (oneDrawing) {
                clearDrawing(oneDrawing.drawingNode);
            });
            allDrawings = [];
            allConnectors = clearModifiedConnectors(allConnectors);

            // switching from text to drawing selection
            if (isTextSelection) { selection.switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareAdjust,
            start: startAdjust,
            move: updateAdjust,
            scroll: updateAdjustScroll,
            end: stopAdjust,
            finally: finalizeAdjust
        };
    }

    // helper function to set the size, position and rotation of a selection drawing node
    function updateSelectionDrawingSize(drawing) {
        if (drawing.data('selection')) {
            drawing.data('selection').css({ transform: drawing.css('transform'), left: drawing.css('left'), top: drawing.css('top'), width: drawing.width(), height: drawing.height() });
        }
    }

    // helper function to remove adjustment points from drawing selection node, if more than one drawing is selected
    function removeAdjPointsInMultiSelection() {
        _.each(selection.getSelectedDrawings(), function (oneDrawing) {
            var selDrawing = $(oneDrawing).data('selection');
            if (selDrawing) { clearAdjPoints(selDrawing); }
        });
    }

    // shifting the selection into an overlay node
    function shiftSelectionToOverlayNode() {

        var // the overlay node that contains the selection drawings
            overlayNode = model.getNode().children('.drawingselection-overlay'),
            // the empty drawing node that contains the selection
            selDrawing =  $('<div>').addClass('drawing selectiondrawing').css({ position: 'absolute' }),
            // a helper string for class names
            classesString = drawingNode.get(0).className;

        if (classesString) { selDrawing.addClass(classesString); }  // transferring drawing class names to the selection drawing node
        drawingNode.data('selection', selDrawing); // saving the drawing selection node at the drawing
        selectionBox.css('position', 'static');
        updateSelectionDrawingSize(drawingNode);
        selDrawing.append(selectionBox); // shifting the selection to the new empty drawing node
        overlayNode.append(selDrawing);

        return selDrawing;
    }

    // starting code of static method drawDrawingSelection()
    drawingNode = $(drawingNode);

    if (drawingNode.length !== 1) {
        globalLogger.error('drawDrawingSelection(): single drawing node expected');
        drawingNode = drawingNode.first();
    }

    if (drawingNode.hasClass('horizontal-line')) {
        options.movable = false;
    } else if (drawingNode.hasClass('grouped')) {
        // options.movable = options.resizable = false;
        drawingNode = drawingNode.closest(GROUPCONTENT_SELECTOR).parent();
    }

    if (!app.isEditable()) {
        options.movable = options.resizable = options.rotatable = options.adjustable = false;
    }

    var currentZoomFactor    = app.getView().getZoomFactor();
    options.zoomValue        = 100 * currentZoomFactor;
    options.scaleHandles     = 1 / currentZoomFactor;
    options.rotation         = getDrawingRotationAngle(model, drawingNode) || 0;
    options.isMultiSelection = selection.isMultiSelection();

    if (selection.isMultiSelection()) { removeAdjPointsInMultiSelection(); }

    // removing an existing selection (possible for example in additional text frame selections)
    if (drawingNode.data('selection')) { clearSelection(drawingNode); }

    var isCropMode = selection.isCropMode();
    if (isCropMode) { model.exitCropMode(); }

    // the container element used to visualize the selection
    selectionBox = drawSelection(drawingNode, options);

    if (isCropMode) { model.handleDrawingCrop(true); }

    // shifting the selection into an overlay node
    selectionDrawing = shiftSelectionToOverlayNode();

    if (!drawingNode.data("dispatcher") && (options.movable || options.resizeable || options.rotatable || options.adjustable)) {

        // create a tracking dispatcher for the selection elements
        const dispatcher = new TrackingDispatcher({
            before() {
                // setting a tracking marker at the selection
                model.getSelection().setActiveTracking(true);
                // blocking external operations, to avoid conflicts with operations inside the drawing like insertText
                model.setDrawingChangeOTHandling(true);
            },
            after() {
                // remove tracking marker at the selection
                model.getSelection().setActiveTracking(false);
                // no longer blocking external operations
                model.setDrawingChangeOTHandling(false);
                // always remove the tracking overlay box
                toggleTracking(drawingNode, false);
                toggleTracking(selectionDrawing, false);
            }
        });

        // resize drawing object
        if (options.resizable) {
            dispatcher.register("crop-resize", createCropResizeCallbacks(app, drawingNode));
            dispatcher.register("drawing-resize", createDrawingResizeCallbacks());
        }

        // rotate drawing object
        if (options.rotatable) {
            dispatcher.register("drawing-rotate", createDrawingRotateCallbacks());
        }

        // adjustment handles of shapes
        if (options.adjustable && !options.isMultiSelection) {
            dispatcher.register("shape-adjust", createDrawingAdjustCallbacks());
        }

        // move drawing object (fallback if no other receiver matches)
        if (options.movable) {
            dispatcher.register("crop-move", createCropMoveCallbacks(app, drawingNode));
            dispatcher.register("drawing-move", createDrawingMoveCallbacks());
        }

        // standard configuration options for all oberved elements
        const TRACKING_CONFIG = {
            stopPropagation: false,
            trackModifiers: true,
            scrollDirection: "all",
            scrollBoundary: app.docView.$contentRootNode[0],
            scrollMinSpeed: 10,
            scrollMaxSpeed: 250,
            scrollAccelBand: [-30, 30]
        };

        // observe the drawing node, and its overlay clone
        dispatcher.observe(drawingNode[0], TRACKING_CONFIG);
        dispatcher.observe(selectionDrawing[0], TRACKING_CONFIG);

        // this hack is needed to be able to access the dispatcher in external generic code
        // ("clearSelection" in drawinglayer/view/drawingframe.js)
        drawingNode.data("dispatcher", dispatcher);
    }
}
