/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, ary, json } from '@/io.ox/office/tk/algorithms';

import { equalPositions } from "@/io.ox/office/editframework/utils/operations";
import { isOperationRemoved, setOperationRemoved, transformIndexMoveMove } from '@/io.ox/office/editframework/utils/otutils';
import { OT_DELETE_TARGET_ALIAS, OT_IGNORE_TARGET_ALIAS, OT_INSERT_CHAR_ALIAS, OT_INSERT_COMP_ALIAS,
    isGapInArray, getResultObject, TextBaseOTManager } from '@/io.ox/office/textframework/model/otmanager';

import * as Op from '@/io.ox/office/presentation/utils/operations';

// constants ==============================================================

/**
 * The names of all additional text operations that delete a target object
 * (an object addressed with the `target` property). Will be aliased to the
 * synthetic operation name `OT_DELETE_TARGET_ALIAS`.
 */
const DELETE_TARGET_OPS = [
    Op.DELETE_TARGET_SLIDE
];

/**
 * The names of all additional operations that need to ignore a specified
 * target object during OT processing. Will be aliased to the synthetic
 * operation name `OT_IGNORE_TARGET_ALIAS`.
 */
const IGNORE_TARGET_OPS = [
    Op.SLIDE_INSERT,
    Op.LAYOUT_SLIDE_INSERT,
    Op.MASTER_SLIDE_INSERT,
    Op.LAYOUT_SLIDE_MOVE,
    Op.LAYOUT_CHANGE,
    Op.MASTER_CHANGE
];

/**
 * The names of all additional presentation operations that insert content
 * into a paragraph. Will be aliased to the synthetic operation name
 * `OT_INSERT_CHAR_ALIAS`.
 */
const INSERT_CHAR_OPS = [
    Op.INSERT_DRAWING
];

/**
 * The names of all additional presentation operations that insert a
 * first-class component into the document (a slide, in addition to
 * paragraphs and tables). Will be aliased to the synthetic operation name
 * `OT_INSERT_COMP_ALIAS`.
 */
const INSERT_COMP_OPS = [
    Op.SLIDE_INSERT
];

// class PresentationOTManager ============================================

export default class PresentationOTManager extends TextBaseOTManager {

    constructor() {

        // base constructor
        super();

        // initialization
        this.registerAliasName(OT_DELETE_TARGET_ALIAS, DELETE_TARGET_OPS);
        this.registerAliasName(OT_INSERT_CHAR_ALIAS, INSERT_CHAR_OPS);
        this.registerAliasName(OT_INSERT_COMP_ALIAS, INSERT_COMP_OPS);
        this.registerAliasName(OT_IGNORE_TARGET_ALIAS, IGNORE_TARGET_OPS);

        // Operations.LAYOUT_SLIDE_INSERT conflicts with 'delete' (of master and layout slide),
        // with another Operations.LAYOUT_SLIDE_INSERT and with Operations.LAYOUT_SLIDE_MOVE.
        // Operations.LAYOUT_SLIDE_INSERT does not conflict with any other operation.

        // Layout operations:
        // - insertLayoutSlide
        // - insertMasterSlide
        // - changeLayout
        // - changeMaster (only ODF)
        // - moveLayoutSlide
        // - delete
        // - deleteTargetSlide
        //
        // Layout operations are also influenced by moveSlide and insertSlide

        this.registerTransformations({
            changeComment: {
                changeComment: this.handleChangeCommentChangeComment,
                changeLayout: null,
                changeMaster: null,
                delete: this.handleCommentDelete,
                deleteComment: this.handleChangeCommentDeleteComment,
                deleteColumns: null,
                deleteTargetSlide: null,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: this.handleChangeCommentInsertComment,
                insertComp: this.handleInsertSlideGeneric,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            changeLayout: {
                changeComment: null,
                changeLayout: this.handleChangeLayoutChangeLayout,
                changeMaster: null,
                delete: null,
                deleteColumns: null,
                deleteComment: null,
                deleteTargetSlide: this.handleChangeLayoutDeleteTargetSlide,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: null,
                insertComp: this.handleInsertSlideGeneric,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            changeMaster: {
                changeComment: null,
                changeLayout: null,
                changeMaster: this.handleChangeLayoutChangeLayout,
                delete: null,
                deleteColumns: null,
                deleteComment: null,
                deleteTargetSlide: this.handleChangeLayoutDeleteTargetSlide,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: null,
                insertComp: this.handleInsertSlideGeneric,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            delete: {
                changeComment: this.handleCommentDelete,
                changeLayout: null,
                changeMaster: null,
                deleteComment: this.handleCommentDelete,
                deleteTargetSlide: null,
                group: this.handleGroupDelete,
                insertComment: this.handleCommentDelete,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveDelete,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideDelete,
                ungroup: this.handleUngroupDelete
            },
            deleteColumns: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGeneric,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupGeneric
            },
            deleteComment: {
                changeComment: this.handleChangeCommentDeleteComment,
                changeLayout: null,
                changeMaster: null,
                delete: this.handleCommentDelete,
                deleteComment: this.handleDeleteCommentDeleteComment,
                deleteColumns: null,
                deleteTargetSlide: null,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: this.handleDeleteCommentInsertComment,
                insertComp: this.handleInsertSlideGeneric,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            deleteTargetSlide: { // handled by preprocessor for targets (but for the ignore target operations an OT handler must be specified)
                changeComment: null,
                changeLayout: this.handleChangeLayoutDeleteTargetSlide,
                changeMaster: this.handleChangeLayoutDeleteTargetSlide,
                delete: null,
                deleteColumns: null,
                deleteComment: null,
                deleteTargetSlide: null, // handled in global deleteTarget handler
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: null,
                insertComp: this.handleInsertSlideDeleteTargetSlide,
                insertLayoutSlide: this.handleInsertLayoutSlideDeleteTargetSlide,
                insertMasterSlide: this.handleInsertMasterSlideDeleteTargetSlide,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: this.handleMoveLayoutSlideDeleteTargetSlide,
                moveSlide: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            group: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                delete: this.handleGroupDelete,
                deleteColumns: this.handleGroupGeneric,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGroup,
                insertCells: this.handleGroupGeneric,
                insertChar: this.handleGroupInsertChar,
                insertColumn: this.handleGroupGeneric,
                insertComment: null,
                insertComp: this.handleGroupInsertComp,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: this.handleGroupGeneric,
                mergeComp: this.handleGroupGeneric,
                move: this.handleGroupMove,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: this.handleGroupSetAttrs,
                setAttributes: this.handleGroupSetAttrs,
                splitComp: this.handleGroupGeneric,
                ungroup: this.handleGroupUngroup,
                updateField: this.handleGroupSetAttrs
            },
            insertCells: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGeneric,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupGeneric
            },
            insertChar: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupInsertChar,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveInsertChar,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupInsertChar
            },
            insertColumn: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGeneric,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupGeneric
            },
            insertComment: {
                changeComment: this.handleChangeCommentInsertComment,
                changeLayout: null,
                changeMaster: null,
                delete: this.handleCommentDelete,
                deleteColumns: null,
                deleteComment: this.handleDeleteCommentInsertComment,
                deleteTargetSlide: null,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: this.handleInsertCommentInsertComment,
                insertComp: this.handleInsertSlideGeneric,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            insertComp: {
                changeComment: this.handleInsertSlideGeneric,
                changeLayout: this.handleInsertSlideGeneric,
                changeMaster: this.handleInsertSlideGeneric,
                deleteComment: this.handleInsertSlideGeneric,
                deleteTargetSlide: this.handleInsertSlideDeleteTargetSlide,
                group: this.handleGroupInsertComp,
                insertComment: this.handleInsertSlideGeneric,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveInsertComp,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideInsertComp,
                ungroup: this.handleUngroupInsertComp
            },
            insertLayoutSlide: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                delete: null,
                deleteColumns: null,
                deleteComment: null,
                deleteTargetSlide: this.handleInsertLayoutSlideDeleteTargetSlide,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: null,
                insertComp: null,
                insertLayoutSlide: this.handleInsertLayoutSlideInsertLayoutSlide,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: this.handleInsertLayoutSlideMoveLayoutSlide,
                moveSlide: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            insertMasterSlide: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                delete: null,
                deleteColumns: null,
                deleteComment: null,
                deleteTargetSlide: this.handleInsertMasterSlideDeleteTargetSlide,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: null,
                insertComp: null,
                insertLayoutSlide: null,
                insertMasterSlide: this.handleInsertMasterSlideInsertMasterSlide,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: null,
                moveSlide: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            insertRows: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGeneric,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupGeneric
            },
            mergeComp: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGeneric,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupGeneric
            },
            move: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                delete: this.handleMoveDelete,
                deleteColumns: this.handleMoveGeneric,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupMove,
                insertCells: this.handleMoveGeneric,
                insertChar: this.handleMoveInsertChar,
                insertColumn: this.handleMoveGeneric,
                insertComment: null,
                insertComp: this.handleMoveInsertComp,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: this.handleMoveGeneric,
                mergeComp: this.handleMoveGeneric,
                move: this.handleMoveMove,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: this.handleMoveSetAttrs,
                setAttributes: this.handleMoveSetAttrs,
                splitComp: this.handleMoveGeneric,
                ungroup: this.handleUngroupMove,
                updateField: this.handleMoveSetAttrs
            },
            moveLayoutSlide: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                delete: null,
                deleteColumns: null,
                deleteComment: null,
                deleteTargetSlide: this.handleMoveLayoutSlideDeleteTargetSlide,
                group: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComment: null,
                insertComp: null,
                insertLayoutSlide: this.handleInsertLayoutSlideMoveLayoutSlide,
                insertMasterSlide: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                moveLayoutSlide: this.handleMoveLayoutSlideMoveLayoutSlide,
                moveSlide: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                ungroup: null,
                updateField: null
            },
            moveSlide: {
                changeComment: this.handleMoveSlideGeneric,
                changeLayout: this.handleMoveSlideGeneric,
                changeMaster: this.handleMoveSlideGeneric,
                delete: this.handleMoveSlideDelete,
                deleteColumns: this.handleMoveSlideGeneric,
                deleteComment: this.handleMoveSlideGeneric,
                deleteTargetSlide: null,
                group: this.handleMoveSlideGeneric,
                insertCells: this.handleMoveSlideGeneric,
                insertChar: this.handleMoveSlideGeneric,
                insertColumn: this.handleMoveSlideGeneric,
                insertComment: this.handleMoveSlideGeneric,
                insertComp: this.handleMoveSlideInsertComp,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: this.handleMoveSlideGeneric,
                mergeComp: this.handleMoveSlideGeneric,
                move: this.handleMoveSlideGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideMoveSlide,
                position: this.handleMoveSlideGeneric,
                setAttributes: this.handleMoveSlideGeneric,
                splitComp: this.handleMoveSlideGeneric,
                ungroup: this.handleMoveSlideGeneric,
                updateField: this.handleMoveSlideGeneric
            },
            setAttributes: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupSetAttrs,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveSetAttrs,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupSetAttrs
            },
            splitComp: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupGeneric,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveGeneric,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupGeneric
            },
            ungroup: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                delete: this.handleUngroupDelete,
                deleteColumns: this.handleUngroupGeneric,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupUngroup,
                insertCells: this.handleUngroupGeneric,
                insertChar: this.handleUngroupInsertChar,
                insertColumn: this.handleUngroupGeneric,
                insertComment: null,
                insertComp: this.handleUngroupInsertComp,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                insertRows: this.handleUngroupGeneric,
                mergeComp: this.handleUngroupGeneric,
                move: this.handleUngroupMove,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                position: this.handleUngroupSetAttrs,
                setAttributes: this.handleUngroupSetAttrs,
                splitComp: this.handleUngroupGeneric,
                ungroup: this.handleUngroupUngroup,
                updateField: this.handleUngroupSetAttrs

            },
            updateField: {
                changeComment: null,
                changeLayout: null,
                changeMaster: null,
                deleteComment: null,
                deleteTargetSlide: null,
                group: this.handleGroupSetAttrs,
                insertComment: null,
                insertLayoutSlide: null,
                insertMasterSlide: null,
                move: this.handleMoveSetAttrs,
                moveLayoutSlide: null,
                moveSlide: this.handleMoveSlideGeneric,
                ungroup: this.handleUngroupSetAttrs
            }
        });
    }

    // handler functions for two operations -------------------------------

    /**
     * Handling of two insertMasterSlide operations.
     *
     * In odp this affects the 'layout' slides.
     * In pptx there is a start number to sort the master slides (but this is not important for the user).
     * In pptx the operation "insertMasterSlide" is only generated in undo.
     *
     * odp:  insertMasterSlide { id: "master_id", attrs: { .... } }
     * pptx: insertMasterSlide { start: 2, id: "master_id", attrs: { ... } }
     */
    handleInsertMasterSlideInsertMasterSlide(localOp, extOp) {

        let localStart = 0;
        let extStart = 0;

        // two clients inserted master slides with the same IDs (this cannot be resolved because of all following operations)
        this.ensure(localOp.id !== extOp.id, 'ERROR: OT conflict, two master slides with same ID inserted. Reload forced by client OT handler: handleInsertMasterSlideInsertMasterSlide.'); // Reload required

        // taking care of the order in pptx case
        if (is.number(localOp.start) && is.number(extOp.start)) {
            // reading the integer start positions
            localStart = localOp.start || 0;
            extStart = extOp.start || 0;

            if (localStart <= extStart) { // this must be "<" on server side
                extOp.start++;
            } else {
                localOp.start++;
            }
        }
    }

    /**
     * Handling of two insertLayoutSlide operations.
     *
     * This is only possible in pptx case.
     *
     * pptx: insertLayoutSlide: { start: 3, target: "master_id", id: "layoutid", attrs: { slide: { type: "vertTitleAndTx" } } }
     */
    handleInsertLayoutSlideInsertLayoutSlide(localOp, extOp) {

        this.ensure(localOp.id !== extOp.id, 'ERROR: OT conflict, two layout slides with same ID inserted. Reload forced by client OT handler: handleInsertLayoutSlideInsertLayoutSlide.'); // Reload required

        // Both operations influence its position, if they have the same master slide.
        if (localOp.target !== extOp.target) { return; } // this will be handled earlier

        // reading the integer start positions
        const localStart = localOp.start || 0;
        const extStart = extOp.start || 0;

        if (localStart <= extStart) { // this must be "<" on server side
            extOp.start++;
        } else {
            localOp.start++;
        }
    }

    /**
     * Handling of two changeLayoutSlide (pptx) operations or two changeMaster (odp) operations.
     *
     * This is only possible in pptx case.
     * Only document slides can change their layout.
     *
     * pptx: changeLayout: { start: [3], target: "new_layout_id" }
     * odp : changeMaster: { start: [3], target: "new_master_id" }
     */
    handleChangeLayoutChangeLayout(localOp, extOp) {

        // whether the same slide got a new layout slide assigned
        const sameDocumentSlide = localOp.start[0] === extOp.start[0];
        // whether the same layout slide was assigned to the document slide
        let sameLayoutSlide = false;

        if (sameDocumentSlide) {
            sameLayoutSlide = localOp.target === extOp.target;

            this.ensure(sameLayoutSlide, 'ERROR: OT conflict, different layout/master ID for the same document slide. Reload forced by client OT handler: handleChangeLayoutChangeLayout.'); // Reload required

            // identical operations, both can be ignored
            setOperationRemoved(localOp);
            setOperationRemoved(extOp);
        }
    }

    /**
     * Handling of two moveLayoutSlide operations.
     *
     * This is only possible in pptx case.
     *
     * moveLayoutSlide: { id: "2147483660", start: 0, target: "2147483648", oldindex: 1, oldtarget: "2147483648" }
     */
    handleMoveLayoutSlideMoveLayoutSlide(localOp, extOp) {

        // ignore both, if both are identical
        // do nothing, if all targets of both operations are different

        const localFromTarget = localOp.oldtarget;
        const extFromTarget = extOp.oldtarget;

        const localToTarget = localOp.target;
        const extToTarget = extOp.target;

        const localFromPos = localOp.oldindex;
        const extFromPos = extOp.oldindex;

        const localToPos = localOp.start;
        const extToPos = extOp.start;

        const localSameMasterMove = localFromTarget === localToTarget;
        const extSameMasterMove = extFromTarget === extToTarget;

        // let sameSlideMoved = localOp.id === extOp.id;
        const sameSlideMoved = localFromTarget === extFromTarget && localFromPos === extFromPos;

        let transformResult = null;
        let localResult = null;
        let extResult = null;

        if (localSameMasterMove && extSameMasterMove && localFromTarget !== extFromTarget) { return; } // nothing to do

        if (sameSlideMoved && localToTarget === extToTarget && localToPos === extToPos) {
            // identical move operations -> both can be ignored
            setOperationRemoved(localOp);
            setOperationRemoved(extOp);
        } else if (sameSlideMoved) {
            // different destination for the same layout slide -> setting the new source to local operation that will be sent to the server
            setOperationRemoved(extOp); // setting marker at external operation
            localOp.oldindex = extOp.start;
            localOp.oldtarget = extOp.target;
        } else if (localSameMasterMove && extSameMasterMove && localFromTarget === extFromTarget) {
            // different layout slides moved: both move operations below the same master
            transformResult =  transformIndexMoveMove(localFromPos, localToPos, extFromPos, extToPos);
            localResult = transformResult.lclRes;
            extResult = transformResult.extRes;

            if (localResult) {
                localOp.oldindex = localResult.fromIdx;
                localOp.start = localResult.toIdx;
            } else {
                setOperationRemoved(localOp);
            }

            if (extResult) {
                extOp.oldindex = extResult.fromIdx;
                extOp.start = extResult.toIdx;
            } else {
                setOperationRemoved(extOp);
            }

        } else if (localSameMasterMove && (localFromTarget === extFromTarget || localFromTarget === extToTarget)) {
            // different layout slides moved: local move below one master, external move source or destination below this master
            if (localFromTarget === extFromTarget) { // the external operation moves the slide away from the local move master -> extToPos can be ignored

                if (localFromPos > extFromPos && localToPos <= extFromPos) {
                    extOp.oldindex++;
                } else if (localFromPos < extFromPos && localToPos >= extFromPos) {
                    extOp.oldindex--;
                }

                if (extFromPos < localFromPos) { localOp.oldindex--; }
                if (extFromPos < localToPos) { localOp.start--; }
                if (extFromPos === localToPos && localFromPos < localToPos) { localOp.start--; }

            } else { // localFromTarget === extToTarget, the external operation moves the slide to the local move master -> extFromPos can be ignored

                if (localFromPos > extToPos && localToPos < extToPos) {
                    extOp.start++;
                } else if (localFromPos < extToPos && localToPos > extToPos) {
                    extOp.start--;
                }

                if (extToPos <= localFromPos) { localOp.oldindex++; }
                if (extToPos <= localToPos) { localOp.start++; }
            }

            if (localOp.start === localOp.oldindex) { setOperationRemoved(localOp); }

        } else if (extSameMasterMove && (extFromTarget === localFromTarget || extFromTarget === localToTarget)) {
            // different layout slides moved: external move below one master, local move source or destination below this master
            if (extFromTarget === localFromTarget) { // the local operation moves the slide away from the external move master -> localToPos can be ignored

                if (extFromPos > localFromPos && extToPos <= localFromPos) {
                    localOp.oldindex++;
                } else if (extFromPos < localFromPos && extToPos >= localFromPos) {
                    localOp.oldindex--;
                }

                if (localFromPos < extFromPos) { extOp.oldindex--; }
                if (localFromPos < extToPos) { extOp.start--; }
                if (localFromPos === extToPos && extFromPos < extToPos) { extOp.start--; }

            } else { // extFromTarget === localToTarget, the local operation moves the slide to the external move master -> localFromPos can be ignored

                if (extFromPos > localToPos && extToPos < localToPos) {
                    localOp.start++;
                } else if (extFromPos < localToPos && extToPos > localToPos) {
                    localOp.start--;
                }

                if (localToPos <= extFromPos) { extOp.oldindex++; }
                if (localToPos <= extToPos) { extOp.start++; }
            }

            if (extOp.start === extOp.oldindex) { setOperationRemoved(extOp); }

        } else if (localFromTarget === extFromTarget || localFromTarget === extToTarget || localToTarget === extFromTarget || localToTarget === extToTarget) {
            // different layout slides moved: both move operations change the master slide
            if (localFromTarget === extFromTarget) { if (localFromPos < extFromPos) { extOp.oldindex--; } else { localOp.oldindex--; } }
            if (localFromTarget === extToTarget) { if (localFromPos < extToPos) { extOp.start--; } else { localOp.oldindex++; } }
            if (localToTarget === extFromTarget) { if (localToPos <= extFromPos) { extOp.oldindex++; } else { localOp.start--; } }
            if (localToTarget === extToTarget) { if (localToPos <= extToPos) { extOp.start++; } else { localOp.start++; } }
        }
    }

    /**
     * Handling an insertLayoutSlide operation with a moveLayoutSlide operation.
     * This is relevant, if a layout slide or a master slide is removed.
     *
     * insertLayoutSlide: { start: 1, target: "2147483648", id: "2147483660", attrs: { slide: { type: "titleOnly" }}}
     * moveLayoutSlide: { id: "2147483660", target: "2147483648", oldtarget: "2147483648", oldindex: 1, start: 0 }
     * -> even the target (the masterSlide) might change
     */
    handleInsertLayoutSlideMoveLayoutSlide(localOp, extOp) {

        // Both operations influence its position, if they have the same master slide.

        // the delete target slide comment operation
        const moveOp = localOp.name === Op.LAYOUT_SLIDE_MOVE ? localOp : extOp;
        // the insert slide operation
        const insertOp = (localOp === moveOp) ? extOp : localOp;
        // whether the layout slide was moved without changing the master
        const sameMasterMove = (moveOp.target === moveOp.oldtarget);
        // the slide position of the insert operation
        const insertCompSlidePos = insertOp.start;
        // the slide start position of the move operation
        const movePosStart = moveOp.oldindex;
        // the slide end position of the move operation
        const movePosTo = moveOp.start;

        if (insertOp.target !== moveOp.target && insertOp.target !== moveOp.oldtarget) { return; } // nothing to do

        if (sameMasterMove) {

            // Changing the move operation (insert moves both positions, even if positions are equal)
            if (insertCompSlidePos <= movePosStart) { moveOp.oldindex++; }
            if (insertCompSlidePos <= movePosTo) { moveOp.start++; }

            // Changing also the insertSlide operation
            if (movePosStart >= insertCompSlidePos && movePosTo < insertCompSlidePos) {
                insertOp.start++;
            } else if (movePosStart < insertCompSlidePos && movePosTo >= insertCompSlidePos) {
                insertOp.start--;
            }

        } else { // changing the master slide for the layout slide during move

            if (insertOp.target === moveOp.oldtarget) {

                if (insertCompSlidePos <= movePosStart) { moveOp.oldindex++; }
                if (movePosStart < insertCompSlidePos) { insertOp.start--; }

            } else if  (insertOp.target === moveOp.target) {

                if (insertCompSlidePos <= movePosTo) { moveOp.start++; }
                if (movePosTo < insertCompSlidePos) { insertOp.start++; }
            }
        }
    }

    /**
     * Handling an insertSlide and a deleteTargetSlide operation.
     *
     * This might require a document reload, if a new slide was inserted, whose layout slide
     * was removed in the meantime.
     *
     * Problems that require a reload:
     *
     * 1. The target slide cannot be restored.
     *
     * 2. After the insertSlide operation follow several further operations like insertDrawing
     *    and insertParagraph. These operations do not know anything about the layout slide (or
     *    its ID). Therefore these operations cannot be marked as removed, like this is the case
     *    for the insertSlide operation.
     *
     * deleteTargetSlide: { id: "2147483672" }
     * insertSlide: { start: [1], target: "2147483672" }
     */
    handleInsertSlideDeleteTargetSlide(localOp, extOp) {

        // the delete target slide comment operation
        const deleteOp = localOp.name === Op.DELETE_TARGET_SLIDE ? localOp : extOp;
        // the insert slide operation
        const insertOp = (localOp === deleteOp) ? extOp : localOp;

        if (insertOp.start.length > 1) { return; } // only handle insertSlide operations

        this.ensure(deleteOp.id !== insertOp.target, 'ERROR: OT conflict, layout slide for inserted document slide removed. Reload forced by client OT handler: handleInsertSlideDeleteTargetSlide.');
        // this conflict cannot be resolved, because all following operations for drawings and paragraphs have no
        // information about the removed target.
    }

    /**
     * Handling an changeLayout (pptx) or changeMaster (odp) operation and a deleteTargetSlide operation.
     *
     * This might require a document reload, if a document slide gets a new layout or master assigned,
     * that was removed in the meantime.
     *
     * Problems that require a reload:
     *
     * 1. The target slide cannot be restored.
     *
     * 2. After the insertSlide operation follow several further operations like insertDrawing
     *    and insertParagraph. These operations do not know anything about the layout slide (or
     *    its ID). Therefore these operations cannot be marked as removed, like this is the case
     *    for the changeLayout/changeMaster operation.
     *
     * deleteTargetSlide: { id: "2147483672" }
     * pptx: changeLayout: { start: [1], target: "new_layout_id" }
     * odp:  changeMaster: { start: [1], target: "new_master_id" }
     */
    handleChangeLayoutDeleteTargetSlide(localOp, extOp) {

        // the delete target slide comment operation
        const deleteOp = localOp.name === Op.DELETE_TARGET_SLIDE ? localOp : extOp;
        // the insert slide operation
        const changeOp = (localOp === deleteOp) ? extOp : localOp;

        this.ensure(deleteOp.id !== changeOp.target, 'ERROR: OT conflict, new layout/master slide for changed document slide removed. Reload forced by client OT handler: handleChangeLayoutDeleteTargetSlide.');
        // this conflict cannot be resolved, because all following operations for drawings and paragraphs have no
        // information about the removed target.
    }

    /**
     * Handling an insertLayoutSlide operation with a deleteTargetSlide operation.
     * This is relevant, if a layout slide or a master slide is removed.
     *
     * insertLayoutSlide collides with deletion of layoutSlide. This is relevant for the start position value
     * below the master slide
     *
     * insertLayoutSlide collides with deletion of masterSlide. This is relevant, if the master slide for the
     * inserted layout slide was removed. In this case a reload is required.
     *
     * deleteTargetSlide: { id: "2147483650", index: 2, parenttarget: "2147483756" }
     * insertLayoutSlide: { id: "2147483672", target: "2147483648", attrs: { slide: { type: "cust" } }, start: 3 }
     */
    handleInsertLayoutSlideDeleteTargetSlide(localOp, extOp) {

        // the delete target slide comment operation
        const deleteOp = localOp.name === Op.DELETE_TARGET_SLIDE ? localOp : extOp;
        // the insert slide operation
        const insertOp = (localOp === deleteOp) ? extOp : localOp;

        //  avoiding that insertLayoutSlide collides with deletion of masterSlide
        this.ensure(insertOp.target !== deleteOp.id, 'ERROR: OT conflict, master slide for new inserted layout slide removed. Reload forced by client OT handler: handleInsertLayoutSlideDeleteTargetSlide.'); // Reload required
        // If the master was removed, the insertLayoutSlide can be ignored completely.
        // But also all following operations after insertLayoutSlide must be ignored -> RELOAD REQUIRED

        if (insertOp.target === deleteOp.parenttarget) {

            // Adapting the index of both operations, because another layout slide of the same master slide was removed.
            if (deleteOp.index < insertOp.start) {
                insertOp.start--;
            } else {
                deleteOp.index++;
            }
        }
    }

    /**
     * Handling an insertMasterSlide operation with a deleteTargetSlide operation.
     *
     * In pptx the insertMasterSlide operation is triggered by an undo of a removal of a master slide.
     * In odp the insertMasterSlide operation is triggered by duplicating a master (layout) slide.
     *
     * pptx:
     * deleteTargetSlide: { id: "2147483650", index: 2, parenttarget: "2147483756" }
     * insertMasterSlide: { id: "2147483672", start: 2, attrs: { slide: { type: "cust" } } }
     *
     * odp:
     * deleteTargetSlide: { id: "2147483650" }
     * insertMasterSlide: { id: "2147483672", attrs: { slide: { type: "cust" } } }
     *
     * In odp there is an arbitrary order of master (layout) slides. It is not important, if the order
     * is different on different clients.
     */
    handleInsertMasterSlideDeleteTargetSlide(localOp, extOp) {

        // the delete target slide comment operation
        const deleteOp = localOp.name === Op.DELETE_TARGET_SLIDE ? localOp : extOp;
        // the insert slide operation
        const insertOp = (localOp === deleteOp) ? extOp : localOp;
        // whether a master slide was removed
        const isMasterSlideRemoved = !deleteOp.parenttarget;

        // only in pptx case, the position might be adapted, if a master slide is inserted and another one deleted.
        if (isMasterSlideRemoved && is.number(insertOp.start) && is.number(deleteOp.index)) {
            if (deleteOp.index < insertOp.start) {
                insertOp.start--;
            } else {
                deleteOp.index++;
            }
        }
    }

    /**
     * Handling a moveLayoutSlide operation with a deleteTargetSlide operation.
     *
     * moveLayoutSlide is only generated in PPTX files, not in ODP files. In the latter the master
     * (layout) slides are not sorted.
     *
     * The layout slide can be moved within one master slide or from one master slide to another
     * master slide. It must be checked, if the source master slide or the destination master slide
     * was removed.
     *
     * It can also happen, that the moved layout slide is removed by another client.
     *
     * deleteTargetSlide: { id: "2147483650", index: 2, parenttarget: "2147483756" }
     * moveLayoutSlide: { id: "2147483734", target: "2147483732", oldtarget: "2147483732", oldindex: 1, start: 0 }
     */
    handleMoveLayoutSlideDeleteTargetSlide(localOp, extOp) {

        // the delete target slide comment operation
        const deleteOp = localOp.name === Op.DELETE_TARGET_SLIDE ? localOp : extOp;
        // the insert slide operation
        const moveOp = (localOp === deleteOp) ? extOp : localOp;
        // the position of the deleted layout slide below its master slide
        let deletePos = 0;
        // the start position of the moved layout slide below its (old) master slide
        let moveStartPos = 0;
        // the end position of the moved layout slide below its (new) master slide
        let moveToPos = 0;
        // whether a master slide was removed
        const isMasterSlideRemoved = !deleteOp.parenttarget;
        // whether the layout slide was moved without changing the master
        const sameMasterMove = (moveOp.target === moveOp.oldtarget);
        // whether the move source target slide was deleted
        const moveSourceDeleted = moveOp.oldtarget === deleteOp.id;
        // whether the move destination target slide was deleted
        const moveDestinationDeleted = moveOp.target === deleteOp.id;
        // an optionally required delete operation
        let newDeleteOperation = null;
        // an optionally required deleteTargetSlide operation
        let newDeleteTargetSlideOperation = null;
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        if (isMasterSlideRemoved) {

            if (moveSourceDeleted && moveDestinationDeleted) {
                // the master slide was removed
                setOperationRemoved(moveOp);
                // no need to change the delete operation
            } else if (moveSourceDeleted || moveDestinationDeleted) {

                // the source or the destination master slide was removed
                setOperationRemoved(moveOp);

                // expanding the delete operation for the deleted layout slide
                newDeleteOperation = json.deepClone(deleteOp);
                newDeleteOperation.name = Op.DELETE;
                newDeleteOperation.start = [0]; // valid for all target slides
                newDeleteOperation.target = moveOp.id;
                delete newDeleteOperation.id;
                delete newDeleteOperation.index;

                newDeleteTargetSlideOperation = json.deepClone(deleteOp);
                // adapting index and parenttarget
                newDeleteTargetSlideOperation.id = moveOp.id;
                newDeleteTargetSlideOperation.index = moveSourceDeleted ? moveOp.start : moveOp.oldindex;
                newDeleteTargetSlideOperation.parenttarget = moveSourceDeleted ? moveOp.target : moveOp.oldtarget;

                if ((moveOp === extOp && moveSourceDeleted) || (moveOp === localOp && moveDestinationDeleted)) {
                    localOpsAfter = [newDeleteOperation, newDeleteTargetSlideOperation]; // inserting delete operations that will be sent to the server (and ignore move operation)
                } else if ((moveOp === localOp && moveSourceDeleted) || (moveOp === extOp && moveDestinationDeleted)) {
                    externalOpsAfter = [newDeleteOperation, newDeleteTargetSlideOperation]; // inserting delete operations that need to be executed locally (and ignore move operation)
                }
            }
        } else {

            // a layout slide was removed

            if (moveOp.id === deleteOp.id) { // the moved layout slide itself was removed

                setOperationRemoved(moveOp); // move operation no longer required

                // the new position of the moved layout slide
                deleteOp.index = moveOp.start;
                deleteOp.parenttarget = moveOp.target;

            } else {

                // the positions might influence each other
                deletePos = deleteOp.index;
                moveStartPos = moveOp.oldindex;
                moveToPos = moveOp.start;

                if (sameMasterMove && deleteOp.parenttarget === moveOp.target) {

                    if (deletePos < moveStartPos) { moveOp.oldindex--; }
                    if (deletePos < moveToPos) { moveOp.start--; }

                    if (moveStartPos > deletePos && moveToPos <= deletePos) {
                        deleteOp.index++;
                    } else if (moveStartPos < deletePos && moveToPos > deletePos) {
                        deleteOp.index--;
                    }
                } else if (!sameMasterMove) {

                    if (deleteOp.parenttarget === moveOp.target) {
                        // a layoutslide below the destination master slide was removed
                        if (deletePos < moveToPos) { moveOp.start--; }
                        if (moveToPos <= deletePos) { deleteOp.index++; }

                    } else if (deleteOp.parenttarget === moveOp.oldtarget) {
                        // a layoutslide below the source master slide was removed
                        if (deletePos < moveStartPos) { moveOp.oldindex--; }
                        if (moveStartPos < deletePos) { deleteOp.index--; }
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and an insertChar operation.
     * A move 'start' position and a move 'to' position must be on the same slide.
     * Additionally all positions of the move operation are top level positions on the slide.
     * In a multi selection, one move operation is generated for every drawing.
     * The move operation influences the insertChar operation and the insertChar influences
     * the move operation, if it is a drawing insertion on the slide.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    handleMoveInsertChar(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the slide position of the insert operation
        const insertSlidePos = insertOp.start[0];
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];

        if (insertSlidePos !== moveSlidePos) { return; }

        // the drawing position of the insert operation
        const insertPos = insertOp.start[1];
        // the drawing 'start' position of the move operation
        const movePosStart = moveOp.start[1];
        // the drawing 'to' position of the move operation
        const movePosTo = moveOp.to[1];

        if (insertOp.start.length === moveOp.start.length) {

            if (insertPos < movePosStart) {
                moveOp.start[1]++;
                if (moveOp.end) { moveOp.end[1]++; }
            }
            if (insertPos < movePosTo) {
                moveOp.to[1]++;
            }

            if (movePosStart > insertPos && movePosTo <= insertPos) {
                insertOp.start[1]++;
            } else if (movePosStart < insertPos && movePosTo > insertPos) {
                insertOp.start[1]--;
            }

        } else {
            if (movePosStart === insertPos) {
                insertOp.start[1] = movePosTo; // the drawing with the inserted text was moved
            } else if (movePosStart > insertPos && movePosTo <= insertPos) {
                insertOp.start[1]++;
            } else if (movePosStart < insertPos && movePosTo > insertPos) {
                insertOp.start[1]--;
            }
        }
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and an insertComp operation (insertParagraph and insertSlide (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, only the move operation is affected.
     * In the case of an insertParagraph operation, the move operation affects the insertPargraph
     * operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    handleMoveInsertComp(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = (moveOp === extOp) ? localOp : extOp;
        // the slide position of the insert operation
        const insertSlidePos = insertOp.start[0];
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];

        if (insertOp.start.length === 1) { // handling an insertSlide operation

            if (insertSlidePos <= moveSlidePos) {
                moveOp.start[0]++;
                if (moveOp.end) { moveOp.end[0]++; }
                moveOp.to[0]++;
            }

        } else {

            if (insertSlidePos !== moveSlidePos) { return; }

            // the drawing position of the insert operation
            const insertPos = insertOp.start[1];
            // the drawing 'start' position of the move operation
            const movePosStart = moveOp.start[1];
            // the drawing 'to' position of the move operation
            const movePosTo = moveOp.to[1];

            // inserting component and moving of drawing happens on the same slide
            if (movePosStart === insertPos) {
                insertOp.start[1] = movePosTo; // the drawing with the inserted text was moved
            } else if (movePosStart > insertPos && movePosTo <= insertPos) {
                insertOp.start[1]++;
            } else if (movePosStart < insertPos && movePosTo > insertPos) {
                insertOp.start[1]--;
            }
        }
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and a setAttributes operation.
     *
     * The move operation influnces the setAttributes position only, if the attribute position
     * is on the same slide and is a selected drawing or inside a drawing.
     * The setAttributes position never influences the move operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    handleMoveSetAttrs(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the attributes operation
        const attrsOp = (moveOp === extOp) ? localOp : extOp;
        // the slide position of the setAttributes operation
        const attrsSlidePos = attrsOp.start[0];
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];

        if (attrsSlidePos !== moveSlidePos || attrsOp.start.length < moveOp.start.length) { return; }

        // the drawing position of the setAttributes operation
        const drawingPos = attrsOp.start[1];
        // the drawing 'start' position of the move operation
        const movePosStart = moveOp.start[1];
        // the drawing 'to' position of the move operation
        const movePosTo = moveOp.to[1];

        // inserting component and moving of drawing happens on the same slide
        if (movePosStart === drawingPos) {
            attrsOp.start[1] = movePosTo; // the drawing was moved
            if (attrsOp.end) { attrsOp.end[1] = movePosTo; }
        } else if (movePosStart > drawingPos && movePosTo <= drawingPos) {
            attrsOp.start[1]++;
            if (attrsOp.end) { attrsOp.end[1]++; }
        } else if (movePosStart < drawingPos && movePosTo >= drawingPos) {
            attrsOp.start[1]--;
            if (attrsOp.end) { attrsOp.end[1]--; }
        }
    }

    /**
     * Handling two move operations (bringing drawings to front or sending them backwards).
     *
     * The move operations influence each other, if they are on the same slide.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    handleMoveMove(localOp, extOp) {

        // the slide position of the local operation
        const localSlidePos = localOp.start[0];
        // the slide position of the external operation
        const extSlidePos = extOp.start[0];

        if (localSlidePos !== extSlidePos) { return; }

        const transformResult =  transformIndexMoveMove(localOp.start[1], localOp.to[1], extOp.start[1], extOp.to[1]);
        const localResult = transformResult.lclRes;
        const extResult = transformResult.extRes;

        if (localResult) {
            localOp.start[1] = localResult.fromIdx;
            if (localOp.end) { localOp.end[1] = localResult.fromIdx; }
            localOp.to[1] = localResult.toIdx;
        } else {
            setOperationRemoved(localOp);
        }

        if (extResult) {
            extOp.start[1] = extResult.fromIdx;
            if (extOp.end) { extOp.end[1] = extResult.fromIdx; }
            extOp.to[1] = extResult.toIdx;
        } else {
            setOperationRemoved(extOp);
        }
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and any operation that can only happen inside a drawing (insertRows, deleteRow, ...).
     * A move 'start' position and a move 'to' position must be on the same slide.
     * Additionally all positions of the move operation are top level positions on the slide.
     * In a multi selection, one move operation is generated for every drawing.
     * The move operation influences the position of the second operation, but not vice versa.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    handleMoveGeneric(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const secondOp = (moveOp === extOp) ? localOp : extOp;
        // the slide position of the second operation
        const secondOpSlidePos = secondOp.start[0];
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];

        if (secondOpSlidePos !== moveSlidePos) { return; }

        // the drawing position of the second operation
        const secondOpPos = secondOp.start[1];
        // the drawing 'start' position of the move operation
        const movePosStart = moveOp.start[1];
        // the drawing 'to' position of the move operation
        const movePosTo = moveOp.to[1];

        if (movePosStart === secondOpPos) {
            secondOp.start[1] = movePosTo; // the drawing with the change of the second operation
        } else if (movePosStart > secondOpPos && movePosTo <= secondOpPos) {
            secondOp.start[1]++;
        } else if (movePosStart < secondOpPos && movePosTo > secondOpPos) {
            secondOp.start[1]--;
        }
    }

    /**
     * Handling of move operation with a delete operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [2, 3] }
     */
    handleMoveDelete(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the delete operation
        const deleteOp = (moveOp === extOp) ? localOp : extOp;
        // the start position of the delete operations
        const deleteStartPos = deleteOp.start;
        // the slide position of the delete start position
        const deleteSlidePos = deleteStartPos[0];
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];
        // the drawing position of the delete start position
        let deleteDrawingPos = null;
        // the drawing position of the move 'start' position
        let moveDrawingStartPos = null;
        // the drawing position of the move 'to' position
        let moveDrawingToPos = null;

        if (deleteStartPos.length === 1) {
            // a slide was deleted
            if (deleteSlidePos === moveSlidePos) { // exactly the slide of the move operation was removed
                setOperationRemoved(moveOp); // setting marker at move operation!
            } else if (deleteSlidePos < moveSlidePos) {
                moveOp.start[0]--;
                if (moveOp.end) { moveOp.end[0]--; }
                moveOp.to[0]--;
            }
        } else if (deleteStartPos.length === moveOp.start.length) {

            if (deleteSlidePos !== moveSlidePos) { return; }

            // a drawing on the same slide as the move operation was removed

            deleteDrawingPos = deleteStartPos[1];
            moveDrawingStartPos = moveOp.start[1];
            moveDrawingToPos = moveOp.to[1];

            if (deleteDrawingPos === moveDrawingStartPos) {
                // the moved drawing was deleted
                setOperationRemoved(moveOp); // moveOp cannot be executed anymore because the drawing is already deleted

                deleteOp.start = json.deepClone(moveOp.to); // but the drawing was deleted at a modified position
                deleteOp.end = json.deepClone(moveOp.to);

            } else {
                // move of drawing and delete of drawing influence each other
                if (deleteDrawingPos < moveDrawingStartPos) {
                    moveOp.start[1]--;
                    if (moveOp.end) { moveOp.end[1]--; }
                }

                if (deleteDrawingPos < moveDrawingToPos) {
                    moveOp.to[1]--;
                }

                if (moveDrawingStartPos > deleteDrawingPos && moveDrawingToPos <= deleteDrawingPos) {
                    deleteOp.start[1]++;
                    if (deleteOp.end) { deleteOp.end[1]++; }
                } else if (moveDrawingStartPos < deleteDrawingPos && moveDrawingToPos > deleteDrawingPos) { // TODO: Check, if >= is better (see moveSlideDelete handler)
                    deleteOp.start[1]--;
                    if (deleteOp.end) { deleteOp.end[1]--; }
                }
            }

        } else {
            // the position of the delete operation is longer than the position of the move operation
            // -> the move operation influences the delete operation, not vice versa.

            if (deleteSlidePos !== moveSlidePos) { return; }

            deleteDrawingPos = deleteStartPos[1];
            moveDrawingStartPos = moveOp.start[1];
            moveDrawingToPos = moveOp.to[1];

            if (moveDrawingStartPos === deleteDrawingPos) {
                deleteOp.start[1] = moveDrawingToPos; // the drawing was moved
                if (deleteOp.end) { deleteOp.end[1] = moveDrawingToPos; }
            } else if (moveDrawingStartPos > deleteDrawingPos && moveDrawingToPos <= deleteDrawingPos) {
                deleteOp.start[1]++;
                if (deleteOp.end) { deleteOp.end[1]++; }
            } else if (moveDrawingStartPos < deleteDrawingPos && moveDrawingToPos > deleteDrawingPos) {
                deleteOp.start[1]--;
                if (deleteOp.end) { deleteOp.end[1]--; }
            }

        }
    }

    /**
     * Handling two group operations.
     *
     * Info: Table drawings cannot be grouped.
     * Info: The new position of the grouped drawing is the position of the first
     *       drawing that will be grouped.
     *
     * All positions of the group operation are top level positions on the slide.
     *
     * The group operations influence each other, if they are on the same slide.
     * In this case only separated ranges of grouped drawings can be resolved. In
     * all other cases, one group operation must be reverted.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleGroupGroup(localOp, extOp) {

        // the slide position of the local operation
        const localSlidePos = localOp.start[0];
        // the slide position of the external operation
        const extSlidePos = extOp.start[0];

        if (localSlidePos !== extSlidePos) { return; }

        // the drawing start position of the local operation
        const localDrawingStartPos = localOp.start[1];
        // the drawing start position of the external operation
        const extDrawingStartPos = extOp.start[1];
        // the local drawings that will be grouped
        const localDrawingsPos = localOp.drawings;
        // the external drawings that will be grouped
        const extDrawingsPos = extOp.drawings;
        // the last local drawing that will be grouped
        const localDrawingLastPos = localDrawingsPos[localDrawingsPos.length - 1];
        // the last external drawing that will be grouped
        const extDrawingLastPos = extDrawingsPos[extDrawingsPos.length - 1];
        // the number of grouped drawings
        let groupCount = 0;
        // whether the group areas overlap each other
        const separatedRange = localDrawingStartPos > extDrawingLastPos || extDrawingStartPos > localDrawingLastPos;

        if ((localDrawingStartPos === extDrawingStartPos) && equalPositions(localDrawingsPos, extDrawingsPos)) {
            // if both operations are identical, both can be ignored
            setOperationRemoved(extOp);
            setOperationRemoved(localOp);
            return;
        }

        this.ensure(separatedRange, 'ERROR: OT conflict, group operations in overlapping ranges. Reload forced by client OT handler: handleGroupGroup.'); // Reload required

        // no overlap -> calculating new positions
        if (localDrawingStartPos > extDrawingLastPos) {
            groupCount = extDrawingsPos.length - 1; // reducing by 1 because of grouped drawing itself
            // reducing all positions of local operation
            localDrawingsPos.forEach((value, index) => { localDrawingsPos[index] = value - groupCount; });
            localOp.start[1] -= groupCount;
        } else {
            groupCount = localDrawingsPos.length - 1;
            // reducing all positions of external operation
            extDrawingsPos.forEach((value, index) => { extDrawingsPos[index] = value - groupCount; });
            extOp.start[1] -= groupCount;
        }
    }

    /**
     * Handling two ungroup operations.
     *
     * All positions of the ungroup operation are top level positions on the slide.
     *
     * The ungroup operations influence each other, if they are on the same slide.
     * In this case only separated ranges of ungrouped drawings can be resolved. In
     * all other cases, one ungroup operation must be reverted.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleUngroupUngroup(localOp, extOp) {

        // the slide position of the local operation
        const localSlidePos = localOp.start[0];
        // the slide position of the external operation
        const extSlidePos = extOp.start[0];

        if (localSlidePos !== extSlidePos) { return; }

        // the drawing start position of the local operation
        const localDrawingStartPos = localOp.start[1];
        // the drawing start position of the external operation
        const extDrawingStartPos = extOp.start[1];
        // the local drawings that will be grouped
        const localDrawingsPos = localOp.drawings;
        // the external drawings that will be grouped
        const extDrawingsPos = extOp.drawings;
        // the number of grouped drawings
        let groupCount = 0;

        // handling different drawings, if the first drawing does not have a gap
        if (localDrawingStartPos < extDrawingStartPos && !isGapInArray(localDrawingsPos)) {
            groupCount = localDrawingsPos.length - 1; // reducing by 1 because of ungrouped drawing itself
            // increasing all positions of external operation
            extDrawingsPos.forEach((value, index) => { extDrawingsPos[index] = value + groupCount; });
            extOp.start[1] += groupCount;

        } else if (extDrawingStartPos < localDrawingStartPos && !isGapInArray(extDrawingsPos)) {
            groupCount = extDrawingsPos.length - 1; // reducing by 1 because of grouped drawing itself
            // increasing all positions of local operation
            localDrawingsPos.forEach((value, index) => { localDrawingsPos[index] = value + groupCount; });
            localOp.start[1] += groupCount;

        } else if (localDrawingStartPos === extDrawingStartPos && equalPositions(localDrawingsPos, extDrawingsPos)) {
            // if both operations are identical, both can be ignored
            setOperationRemoved(extOp);
            setOperationRemoved(localOp);

        } else {
            this.ensure(false, 'ERROR: OT conflict, ungroup operations generate overlapping ranges. Reload forced by client OT handler: handleUngroupUngroup.'); // Reload required
        }

    }

    /**
     * Handling a group and an ungroup operations.
     *
     * Info: Table drawings cannot be grouped.
     * Info: The new position of the grouped drawing is the position of the first
     *       drawing that will be grouped.
     *
     * All positions of the group and ungroup operations are top level positions on the slide.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     * { name: ungroup, start: [2, 5], drawings:[4, 5, 6, 7] }
     */
    handleGroupUngroup(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the ungroup operation
        const ungroupOp = (extOp === groupOp) ? localOp : extOp;
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];

        if (ungroupSlidePos !== groupSlidePos) { return; } // must be on the same slide

        // the group start position
        const groupDrawingStartPos = groupOp.start[1];
        // the ungroup start position
        const ungroupDrawingStartPos = ungroupOp.start[1];
        // the drawings that will be grouped
        const groupDrawingsPos = groupOp.drawings;
        // the drawings that will be ungrouped
        const ungroupDrawingsPos = ungroupOp.drawings;
        // the last local drawing that will be grouped
        const groupDrawingLastPos = groupDrawingsPos[groupDrawingsPos.length - 1];
        // the number of grouped/ungrouped drawings
        let groupCount = 0;

        if (ungroupDrawingStartPos > groupDrawingLastPos) {
            // no overlap -> calculating new positions
            groupCount = groupDrawingsPos.length - 1; // reducing by 1 because of grouped drawing itself
            // reducing all positions of local operation
            ungroupDrawingsPos.forEach((value, index) => { ungroupDrawingsPos[index] = value - groupCount; });
            ungroupOp.start[1] -= groupCount;

        } else if (ungroupDrawingStartPos < groupDrawingStartPos && !isGapInArray(ungroupDrawingsPos)) {
            groupCount = ungroupDrawingsPos.length - 1; // reducing by 1 because of ungrouped drawing itself
            // increasing all positions of external operation
            groupDrawingsPos.forEach((value, index) => { groupDrawingsPos[index] = value + groupCount; });
            groupOp.start[1] += groupCount;

        } else {
            this.ensure(false, 'ERROR: OT conflict, ungroup and group operation generate overlapping ranges. Reload forced by client OT handler: handleGroupUngroup.'); // Reload required
        }

    }

    /**
     * Handling a group operation and an insertChar operation.
     *
     * Info: insertChar operation includes the 'insertDrawing'. All other insertChar
     *       operations are inside a drawing.
     * Info: Table drawings cannot be grouped.
     * Info: The new position of the grouped drawing is the position of the first
     *       drawing the will be grouped.
     *
     * All positions of the group operation are top level positions on the slide.
     *
     * The group operation influences the insertChar operation and the insertChar influences
     * the group operation, if it is a drawing insertion on the slide.
     *
     * Example:
     * { name: group, start: [2, 3], drawings: [3, 4, 7, 8] }
     */
    handleGroupInsertChar(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the insert operation
        const insertOp = (extOp === groupOp) ? localOp : extOp;
        // the slide position of the insert operation
        const insertSlidePos = insertOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];

        if (insertSlidePos !== groupSlidePos) { return; } // must be on the same slide

        // the drawing position of the insert operation
        const insertPos = insertOp.start[1];
        // the drawing 'start' position of the group operation
        const groupPos = groupOp.start[1];
        // the array with indices of grouped drawings
        const groupedDrawings = groupOp.drawings;
        // a counter for the grouped drawings
        let counter = 0;
        // the index of the drawing inside the array of grouped drawings
        let drawingIndex = 0;

        if (insertOp.start.length === groupOp.start.length) {
            // handling an insertDrawing operation
            if (insertPos <= groupPos) {
                groupOp.start[1]++;
                groupedDrawings.forEach((_value, index) => { groupOp.drawings[index]++; });
            } else {
                // the position needs to be decreased -> counting grouped drawings before insert-drawing
                groupedDrawings.forEach((value, index) => {
                    if (value < insertPos) {
                        counter++;
                    } else {
                        groupOp.drawings[index]++;
                    }
                });
                counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
                if (counter > 0) { insertOp.start[1] -= counter; }
            }
        } else {
            if (insertPos < groupPos) { return; } // fast exit, insert is not influenced by grouping

            drawingIndex = groupedDrawings.indexOf(insertPos);
            // is the inserted char inside a grouped drawing?
            if (drawingIndex > -1) {
                // calculating the new position inside the group
                insertOp.start[1] = groupPos;
                insertOp.start.splice(2, 0, drawingIndex); // inserting the counter position behind the group position
            } else {
                // not grouped, but the position needs to be decreased -> counting grouped drawings before insert-drawing
                groupedDrawings.forEach(value => { if (value < insertPos) { counter++; } });
                counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
                if (counter > 0) { insertOp.start[1] -= counter; }
            }
        }
    }

    /**
     * Handling a group operation and any operation that can only happen inside a drawing
     * (insertRows, deleteRow, ...).
     * All positions of the group operation are top level positions on the slide.
     * The group operation influences the position of the second operation, but not vice versa.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleGroupGeneric(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the second operation
        const secondOp = (extOp === groupOp) ? localOp : extOp;
        // the slide position of the second operation
        const secondOpSlidePos = secondOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];

        if (secondOpSlidePos !== groupSlidePos) { return; } // must be on the same slide

        // the drawing position of the second operation
        const secondOpPos = secondOp.start[1];
        // the drawing 'start' position of the group operation
        const groupPos = groupOp.start[1];
        // the array with indices of grouped drawings
        const groupedDrawings = groupOp.drawings;
        // a counter for the grouped drawings
        let counter = 0;
        // the index of the drawing inside the array of grouped drawings
        let drawingIndex = 0;

        if (secondOpPos < groupPos) { return; } // fast exit, second operation is not influenced by grouping

        drawingIndex = groupedDrawings.indexOf(secondOpPos);

        // is the second operation inside a grouped drawing?
        if (drawingIndex > -1) {
            secondOp.start[1] = groupPos;
            secondOp.start.splice(2, 0, drawingIndex); // inserting the counter position behind the group position
        } else {
            // not grouped, but the position needs to be decreased -> counting grouped drawings before insert-drawing
            groupedDrawings.forEach(value => { if (value < secondOpPos) { counter++; } });
            counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
            if (counter > 0) { secondOp.start[1] -= counter; }
        }
    }

    /**
     * Handling a group operation and a setAttributes operation.
     *
     * The group operation influnces the setAttributes position only, if the attribute position
     * is on the same slide.
     * The setAttributes position never influences the group operation.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleGroupSetAttrs(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the attributes operation
        const attrsOp = (groupOp === extOp) ? localOp : extOp;
        // the slide position of the setAttributes operation
        const attrsSlidePos = attrsOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];

        if (attrsSlidePos !== groupSlidePos || attrsOp.start.length < groupOp.start.length) { return; }

        // the drawing position of the setAttributes operation
        const drawingPos = attrsOp.start[1];
        // the drawing 'start' position of the group operation
        const groupPos = groupOp.start[1];
        // the array with indices of grouped drawings
        const groupedDrawings = groupOp.drawings;
        // a counter for the grouped drawings
        let counter = 0;
        // the index of the drawing inside the array of grouped drawings
        let drawingIndex = 0;
        // whether the setAttributes operation is really a 'setAttributes'
        const isSetAttrsOp = attrsOp.name === Op.SET_ATTRIBUTES;

        if (drawingPos < groupPos) { return; } // fast exit, setAttributes is not influenced by grouping

        drawingIndex = groupedDrawings.indexOf(drawingPos);

        // is the setAttributes for a drawing or inside a drawing that will be grouped?
        if (drawingIndex > -1) {

            this.ensure(!isSetAttrsOp || attrsOp.start.length > 2, 'ERROR: OT conflict, setting attributes to drawing that will be grouped. Reload forced by client OT handler: handleGroupSetAttrs.'); // Reload required

            if (attrsOp.start.length > 2 || (!isSetAttrsOp && attrsOp.start.length === 2)) { // content inside a grouped drawing is attributed

                attrsOp.start[1] = groupPos;
                attrsOp.start.splice(2, 0, drawingIndex); // inserting the counter position behind the group position
                if (attrsOp.end) { // end position must be for or inside the same drawing as the start position
                    attrsOp.end[1] = groupPos;
                    attrsOp.end.splice(2, 0, drawingIndex);
                }
            }

        } else {
            // not grouped, but the position needs to be decreased -> counting grouped drawings before setAttributes-drawing
            groupedDrawings.forEach(value => { if (value < drawingPos) { counter++; } });
            counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
            if (counter > 0) {
                attrsOp.start[1] -= counter;
                if (attrsOp.end) { attrsOp.end[1] -= counter; }
            }
        }

    }

    /**
     * Handling a group operation and an insertComp operation (insertParagraph and insertSlide
     * (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, only the group operation is affected.
     * In the case of an insertParagraph operation, the group operation affects the insertPargraph
     * operation.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleGroupInsertComp(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the insert operation
        const insertOp = (groupOp === extOp) ? localOp : extOp;
        // the slide position of the insert operation
        const insertSlidePos = insertOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];
        // the drawing position of the insert operation
        let drawingPos = null;
        // the drawing 'start' position of the group operation
        let groupPos = null;
        // the array with indices of grouped drawings
        let groupedDrawings = null;
        // a counter for the grouped drawings
        let counter = 0;
        // the index of the drawing inside the array of grouped drawings
        let drawingIndex = 0;

        if (insertOp.start.length === 1) { // handling an insertSlide operation
            if (insertSlidePos <= groupSlidePos) { groupOp.start[0]++; }
        } else {
            if (insertSlidePos !== groupSlidePos) { return; } // fast exit, different slides

            drawingPos = insertOp.start[1];
            groupPos = groupOp.start[1];
            groupedDrawings = groupOp.drawings;

            if (drawingPos < groupPos) { return; } // fast exit, insertParagraph is not influenced by grouping

            drawingIndex = groupedDrawings.indexOf(drawingPos);

            // is the insertParagraph for a drawing or inside a drawing that will be grouped?
            if (drawingIndex > -1) {
                insertOp.start[1] = groupPos;
                insertOp.start.splice(2, 0, drawingIndex); // inserting the counter position behind the group position
            } else {
                // not grouped, but the position needs to be decreased -> counting grouped drawings before insertParagraph-drawing
                groupedDrawings.forEach(value => { if (value < drawingPos) { counter++; } });
                counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
                if (counter > 0) { insertOp.start[1] -= counter; }
            }
        }
    }

    /**
     * Handling a group operation and a delete operation.
     *
     * If a slide is deleted, the group operation can be affected.
     * If content inside a drawing is deleted, the group operation influences the delete operation.
     * If a drawing is deleted, both operations influence each other.
     *
     * Important: If a drawing is deleted, there is always one delete operation for each drawing.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleGroupDelete(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the delete operation
        const deleteOp = (groupOp === extOp) ? localOp : extOp;
        // the slide position of the delete operation
        const deleteSlidePos = deleteOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];
        // the drawing position of the delete operation
        let drawingPos = null;
        // the drawing 'start' position of the group operation
        let groupPos = null;
        // the array with indices of grouped drawings
        let groupedDrawings = null;
        // a counter for the grouped drawings
        let counter = 0;
        // whether a complete slide is removed
        const isSlideDeletion = deleteOp.start.length === 1;
        // whether the content inside a drawing is removed (grouping is only possible for toplevel drawings)
        const isDrawingContentDeletion = deleteOp.start.length > groupOp.start.length; // groupOp.start.length should always be 2
        // an optionally required new array of grouped drawings
        let newGroupedDrawings = null;
        // the index of the drawing inside the array of grouped drawings
        let drawingIndex = 0;

        if (isSlideDeletion) { // handling removal of a complete slide
            if (deleteSlidePos < groupSlidePos) {
                groupOp.start[0]--;
            } else if (deleteSlidePos === groupSlidePos) {
                setOperationRemoved(groupOp); // group operation can be ignored, because the slide is already removed
            }
        } else if (isDrawingContentDeletion) {
            // only the delete operation needs to be modified
            if (deleteSlidePos !== groupSlidePos) { return; } // fast exit, different slides

            drawingPos = deleteOp.start[1];
            groupPos = groupOp.start[1];
            groupedDrawings = groupOp.drawings;

            drawingIndex = groupedDrawings.indexOf(drawingPos);
            // is the drawing with deleted content one of the grouped drawings?
            if (drawingIndex > -1) {
                // calculating the new position inside the group
                deleteOp.start[1] = groupPos;
                deleteOp.start.splice(2, 0, drawingIndex); // inserting the counter position for the drawing inside its group behind the group position
                if (deleteOp.end) {
                    deleteOp.end[1] = groupPos;
                    deleteOp.end.splice(2, 0, drawingIndex);
                }
            } else {
                // not grouped, but the position needs to be decreased -> counting grouped drawings before insertParagraph-drawing
                groupedDrawings.forEach(value => { if (value < drawingPos) { counter++; } });
                counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
                if (counter > 0) {
                    deleteOp.start[1] -= counter;
                    if (deleteOp.end) { deleteOp.end[1] -= counter; }
                }
            }
        } else {
            // a drawing was deleted
            if (deleteSlidePos !== groupSlidePos) { return; } // fast exit, different slides

            drawingPos = deleteOp.start[1];
            groupPos = groupOp.start[1];
            groupedDrawings = groupOp.drawings;

            this.ensure(groupedDrawings.indexOf(drawingPos) === -1, 'ERROR: OT conflict, deleted drawing that will be grouped. Reload forced by client OT handler: handleGroupDelete.'); // Reload required

            // not grouped, but the positions need to be adapted
            newGroupedDrawings = [];
            groupedDrawings.forEach(value => {
                if (value < drawingPos) {
                    counter++;
                } else {
                    value--; // modifying the group operation
                }
                newGroupedDrawings.push(value);
            });
            if (groupPos !== newGroupedDrawings[0]) {
                groupOp.start[1] = newGroupedDrawings[0]; // adapting the start position might be required
            }
            groupOp.drawings = newGroupedDrawings; // modifying the group operation
            counter--; // decreasing by 1 again, because of the position of the grouped drawing itself
            if (counter > 0) { // modifying the delete operation
                deleteOp.start[1] -= counter;
                if (deleteOp.end) { deleteOp.end[1] -= counter; }
            }
        }

    }

    /**
     * Handling a group operation and a move operation.
     *
     * If a drawing is moved, the group operation can be affected.
     *
     * Important: If a drawing is moved, there is always one move operation for each drawing.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     * { name: move, start: [1, 1], to: [1, 4] }  // -> bringing drawing to front
     */
    handleGroupMove(localOp, extOp) {

        // the group operation
        const groupOp = localOp.name === Op.GROUP ? localOp : extOp;
        // the move operation
        const moveOp = (groupOp === extOp) ? localOp : extOp;
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];
        // the slide position of the group operation
        const groupSlidePos = groupOp.start[0];
        // the drawing start position of the move operation
        const drawingStartPos = moveOp.start[1];
        // the drawing 'to' position of the move operation
        const drawingToPos =  moveOp.to[1];
        // the drawing 'start' position of the group operation
        const groupStartPos = groupOp.start[1];
        // the array with indices of grouped drawings
        const groupedDrawings = groupOp.drawings;
        // the index of the last drawing that will be grouped
        const lastGroupedDrawing = groupedDrawings[groupedDrawings.length - 1];
        // the number of all grouped drawings with positions before the moved drawing
        let prevDrawingsCount = 0;
        // // an optionally required new move operation
        let newMoveOperation = null;
        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        // helper function for difficult scenarios -> undo the move operation before group
        const undoMoveOperation = () => {
            newMoveOperation = json.deepClone(moveOp);
            // -> the move operation must be marked as 'removed'
            setOperationRemoved(moveOp); // move operation can be ignored, because the moved slide is already removed

            newMoveOperation.start = json.deepClone(moveOp.to);
            newMoveOperation.to = json.deepClone(moveOp.start);
            if (newMoveOperation.end) { newMoveOperation.end = json.deepClone(newMoveOperation.start); }

            if (groupOp === extOp) {
                externalOpsBefore = [newMoveOperation];
            } else {
                localOpsBefore = [newMoveOperation];
            }
        };

        if (moveSlidePos !== groupSlidePos) { return; } // fast exit, different slides

        if (drawingStartPos < groupStartPos) {

            if (drawingToPos < groupStartPos) {
                return; // nothing to do, move drawing before grouped drawings
            } else if (drawingToPos > lastGroupedDrawing) {
                moveOp.to[1] -= (groupedDrawings.length - 1); // moving drawing behind the last grouped drawing
                groupOp.start[1]--; // adapting also the start position of the group operation
                groupedDrawings.forEach((_value, index) => { groupedDrawings[index]--; });
            } else {
                prevDrawingsCount = ary.count(groupedDrawings, val => val < drawingToPos);
                if (drawingToPos === groupStartPos) { prevDrawingsCount = 1; } // moving to the group position itself
                moveOp.to[1] -= (prevDrawingsCount - 1);
                groupOp.start[1]--;
                groupedDrawings.forEach((value, index) => { if (value <= drawingToPos) { groupedDrawings[index]--; } }); // ('<=' because of the old position!)
            }

        } else if (drawingStartPos > lastGroupedDrawing) {

            if (drawingToPos <= groupStartPos) {
                moveOp.start[1] -= (groupedDrawings.length - 1); // moving drawing before the first grouped drawing
                groupOp.start[1]++; // adapting also the start position of the group operation
                groupedDrawings.forEach((_value, index) => { groupedDrawings[index]++; });
            } else if (drawingToPos > lastGroupedDrawing) {
                moveOp.start[1] -= (groupedDrawings.length - 1); // moving drawing behind the last grouped drawing
                moveOp.to[1] -= (groupedDrawings.length - 1); // moving drawing behind the last grouped drawing
            } else {
                prevDrawingsCount = ary.count(groupedDrawings, val => val < drawingToPos);
                moveOp.start[1] -= (groupedDrawings.length - 1);
                moveOp.to[1] -= (prevDrawingsCount - 1);
                groupedDrawings.forEach((value, index) => { if (value >= drawingToPos) { groupedDrawings[index]++; } });
            }
        } else {
            // the drawing start position is in the range of the grouped drawings (but it is not sure, whether it is grouped)
            if (groupedDrawings.indexOf(drawingStartPos) > -1) {
                undoMoveOperation(); // the moved drawing is one of the grouped drawings
            } else {
                prevDrawingsCount = ary.count(groupedDrawings, val => val < drawingStartPos);
                // the moved drawing is in the range of the grouped drawings, but it is not grouped
                if (drawingToPos < groupStartPos) {
                    moveOp.start[1] -= (prevDrawingsCount - 1);
                    groupOp.start[1]++; // adapting also the start position of the group operation
                    groupedDrawings.forEach((value, index) => { if (value < drawingStartPos) { groupedDrawings[index]++; } });
                } else if (drawingToPos > lastGroupedDrawing) {
                    moveOp.start[1] -= (prevDrawingsCount - 1);
                    moveOp.to[1] -= (groupedDrawings.length - 1);
                    groupedDrawings.forEach((value, index) => { if (value > drawingStartPos) { groupedDrawings[index]--; } });
                } else {
                    undoMoveOperation(); // both move positions ('start' and 'to') in the range of the grouped drawings
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling an ungroup operation and an insertChar operation.
     *
     * Info: insertChar operation includes the 'insertDrawing'. All other insertChar
     *       operations are inside a drawing.
     * Info: Table drawings cannot be ungrouped.
     * Info: The new position of the ungrouped drawing is the position of the first
     *       drawing the will be ungrouped.
     *
     * All positions of the ungroup operation are top level positions on the slide.
     *
     * The ungroup operation influences the insertChar operation and the insertChar influences
     * the ungroup operation, if it is a drawing insertion on the slide.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleUngroupInsertChar(localOp, extOp) {

        // the ungroup operation
        const ungroupOp = localOp.name === Op.UNGROUP ? localOp : extOp;
        // the insert operation
        const insertOp = (extOp === ungroupOp) ? localOp : extOp;
        // the slide position of the insert operation
        const insertSlidePos = insertOp.start[0];
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];

        if (insertSlidePos !== ungroupSlidePos) { return; } // must be on the same slide to influence each other

        // the drawing position of the insert operation
        const insertPos = insertOp.start[1];
        // the drawing 'start' position of the ungroup operation
        const ungroupPos = ungroupOp.start[1];
        // the array with indices of ungrouped drawings
        const ungroupedDrawings = ungroupOp.drawings;
        // the drawing position inside its drawing group
        let drawingPosInGroup = 0;
        // the new toplevel position of the drawing on the slide
        let newDrawingTopLevelPos = 0;
        // whether a gap for the inserted drawing or the drawing that got a char inserted was found
        let foundGap = false;
        // the size of the gap between two ungrouped drawings
        let gap = 0;
        // the number of drawings that need to be filled into gaps between ungrouped drawings
        let fillDrawings = 0;
        // the position of the previous ungrouped drawing
        let previousUngroup = -1;

        if (insertOp.start.length === ungroupOp.start.length) {
            // handling an insertDrawing operation
            if (insertPos <= ungroupPos) {
                ungroupOp.start[1]++;
                ungroupedDrawings.forEach((_value, index) => { ungroupOp.drawings[index]++; });
            } else {
                // check for a gap for the new inserted drawing in the ungrouped drawings -> modifying inserted drawing and following ungrouped drawings
                fillDrawings = insertPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                ungroupedDrawings.forEach((value, index) => {
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                insertOp.start[1] = previousUngroup + fillDrawings;
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                    if (foundGap) { ungroupOp.drawings[index]++; } // increasing the index for all following ungrouped drawings
                });
                if (!foundGap) { insertOp.start[1] = previousUngroup + fillDrawings; } // shifting drawing with inserted char behind the last ungrouped drawing
            }
        } else {
            if (insertPos < ungroupPos) { return; } // fast exit, insert is not influenced by grouping

            // is the inserted char inside a grouped drawing?
            if (insertPos === ungroupPos) {
                drawingPosInGroup = insertOp.start[2]; // the new top level position is specified by the (old) position of the insertChar operation
                newDrawingTopLevelPos = ungroupedDrawings[drawingPosInGroup];
                insertOp.start[1] = newDrawingTopLevelPos;
                insertOp.start.splice(2, 1); // index at [2] is no longer required
            } else if (ungroupPos < insertPos) {
                // not ungrouped, but the position for text insertion needs to be increased -> counting gaps in ungroup
                fillDrawings = insertPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                ungroupedDrawings.forEach(value => {
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                insertOp.start[1] = previousUngroup + fillDrawings;
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                });
                if (!foundGap) { insertOp.start[1] = previousUngroup + fillDrawings; } // shifting drawing with inserted char behind the last ungrouped drawing
                // gaps exist only in undo scenario
                // Test: insertText [1, 4, 0, 0] and ungroup [1, 1] with drawings [1, 2, 3, 5, 6, 7] (shifting [1, 4] to [1, 9]) -> not sufficient gaps
                // Test: insertText [1, 4, 0, 0] and ungroup [1, 1] with drawings [1, 2, 3, 4, 5, 6] (shifting [1, 4] to [1, 9]) -> no gaps
                // Test: insertText [1, 4, 0, 0] and ungroup [1, 1] with drawings [1, 2, 3, 7, 8 ,9] (shifting [1, 4] to [1, 6]) -> sufficient gaps
            }
        }
    }

    /**
     * Handling an ungroup operation and an insertComp operation (insertParagraph and insertSlide
     * (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, only the ungroup operation is affected.
     * In the case of an insertParagraph operation, the ungroup operation affects the insertPargraph
     * operation.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleUngroupInsertComp(localOp, extOp) {

        // the ungroup operation
        const ungroupOp = localOp.name === Op.UNGROUP ? localOp : extOp;
        // the insert operation
        const insertOp = (ungroupOp === extOp) ? localOp : extOp;
        // the slide position of the insert operation
        const insertSlidePos = insertOp.start[0];
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];
        // the drawing position of the insert operation
        let drawingPos = null;
        // the drawing 'start' position of the ungroup operation
        let ungroupPos = null;
        // the array with indices of ungrouped drawings
        let ungroupedDrawings = null;
        // the drawing position inside its drawing group
        let drawingPosInGroup = 0;
        // the new toplevel position of the drawing on the slide
        let newDrawingTopLevelPos = 0;
        // whether a gap for the inserted drawing or the drawing that got a char inserted was found
        let foundGap = false;
        // the size of the gap between two ungrouped drawings
        let gap = 0;
        // the number of drawings that need to be filled into gaps between ungrouped drawings
        let fillDrawings = 0;
        // the position of the previous ungrouped drawing
        let previousUngroup = -1;

        if (insertOp.start.length === 1) { // handling an insertSlide operation
            if (insertSlidePos <= ungroupSlidePos) { ungroupOp.start[0]++; }
        } else {
            if (insertSlidePos !== ungroupSlidePos) { return; } // fast exit, different slides

            drawingPos = insertOp.start[1];
            ungroupPos = ungroupOp.start[1];
            ungroupedDrawings = ungroupOp.drawings;

            if (drawingPos < ungroupPos) { return; } // fast exit, insertParagraph is not influenced by ungrouping

            // is the insertParagraph for a drawing or inside a drawing that will be ungrouped?
            if (drawingPos === ungroupPos) {
                drawingPosInGroup = insertOp.start[2]; // the new top level position is specified by the (old) position of the insertChar operation
                newDrawingTopLevelPos = ungroupedDrawings[drawingPosInGroup];
                insertOp.start[1] = newDrawingTopLevelPos;
                insertOp.start.splice(2, 1); // index at [2] is no longer required
            } else {
                // not ungrouped, but the position for paragraph insertion needs to be increased -> counting gaps in ungroup
                fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                ungroupedDrawings.forEach(value => {
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                insertOp.start[1] = previousUngroup + fillDrawings;
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                });
                if (!foundGap) { insertOp.start[1] = previousUngroup + fillDrawings; } // shifting drawing with inserted paragraph behind the last ungrouped drawing
            }
        }
    }

    /**
     * Handling a ungroup operation and any operation that can only happen inside a drawing
     * (insertRows, deleteRow, ...).
     * All positions of the ungroup operation are top level positions on the slide.
     * The ungroup operation influences the position of the second operation, but not vice versa.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleUngroupGeneric(localOp, extOp) {

        // the ungroup operation
        const ungroupOp = localOp.name === Op.UNGROUP ? localOp : extOp;
        // the second operation
        const secondOp = (extOp === ungroupOp) ? localOp : extOp;
        // the slide position of the second operation
        const secondOpSlidePos = secondOp.start[0];
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];

        if (secondOpSlidePos !== ungroupSlidePos) { return; } // must be on the same slide

        // the drawing position of the second operation
        const drawingPos = secondOp.start[1];
        // the drawing 'start' position of the ungroup operation
        const ungroupPos = ungroupOp.start[1];
        // the array with indices of ungrouped drawings
        const ungroupedDrawings = ungroupOp.drawings;
        // the drawing position inside its drawing group
        let drawingPosInGroup = 0;
        // the new toplevel position of the drawing on the slide
        let newDrawingTopLevelPos = 0;
        // whether a gap for the inserted drawing or the drawing that got a char inserted was found
        let foundGap = false;
        // the size of the gap between two ungrouped drawings
        let gap = 0;
        // the number of drawings that need to be filled into gaps between ungrouped drawings
        let fillDrawings = 0;
        // the position of the previous ungrouped drawing
        let previousUngroup = -1;

        if (drawingPos < ungroupPos) { return; } // fast exit, second operation is not influenced by ungrouping

        // is the second operation inside a ungrouped drawing?
        if (drawingPos === ungroupPos) {
            drawingPosInGroup = secondOp.start[2]; // the new top level position is specified by the (old) position of the insertChar operation
            newDrawingTopLevelPos = ungroupedDrawings[drawingPosInGroup];
            secondOp.start[1] = newDrawingTopLevelPos;
            secondOp.start.splice(2, 1); // index at [2] is no longer required
        } else {
            // not ungrouped, but the position needs to be increased -> counting ungrouped drawings before insert-drawing
            fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
            ungroupedDrawings.forEach(value => {
                if (previousUngroup >= 0 && !foundGap) {
                    gap = value - previousUngroup - 1;
                    if (gap > 0) {
                        if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                            secondOp.start[1] = previousUngroup + fillDrawings;
                            foundGap = true;
                        } else {
                            fillDrawings -= gap;
                        }
                    }
                }
                previousUngroup = value;
            });
            if (!foundGap) { secondOp.start[1] = previousUngroup + fillDrawings; } // shifting drawing with second operation behind the last ungrouped drawing
        }
    }

    /**
     * Handling an ungroup operation and a setAttributes operation.
     *
     * The ungroup operation influnces the setAttributes position only, if the attribute position
     * is on the same slide.
     * The setAttributes position never influences the ungroup operation.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleUngroupSetAttrs(localOp, extOp) {

        // the ungroup operation
        const ungroupOp = localOp.name === Op.UNGROUP ? localOp : extOp;
        // the attributes operation
        const attrsOp = (ungroupOp === extOp) ? localOp : extOp;
        // the slide position of the setAttributes operation
        const attrsSlidePos = attrsOp.start[0];
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];

        if (attrsSlidePos !== ungroupSlidePos || attrsOp.start.length < ungroupOp.start.length) { return; }

        // the drawing position of the setAttributes operation
        const drawingPos = attrsOp.start[1];
        // the drawing 'start' position of the ungroup operation
        const ungroupPos = ungroupOp.start[1];
        // the array with indices of ungrouped drawings
        const ungroupedDrawings = ungroupOp.drawings;
        // the drawing position inside its drawing group
        let drawingPosInGroup = 0;
        // the new toplevel position of the drawing on the slide
        let newDrawingTopLevelPos = 0;
        // whether a gap for the inserted drawing or the drawing that got a char inserted was found
        let foundGap = false;
        // the size of the gap between two ungrouped drawings
        let gap = 0;
        // the number of drawings that need to be filled into gaps between ungrouped drawings
        let fillDrawings = 0;
        // the position of the previous ungrouped drawing
        let previousUngroup = -1;
        // the new drawing position
        let newDrawingPos = 0;
        // whether the setAttributes operation is really a 'setAttributes'
        const isSetAttrsOp = attrsOp.name === Op.SET_ATTRIBUTES;

        if (drawingPos < ungroupPos) { return; } // fast exit, setAttributes is not influenced by ungrouping

        // is the setAttributes for a drawing or inside a drawing that will be ungrouped?
        if (drawingPos === ungroupPos) {

            this.ensure(!isSetAttrsOp || attrsOp.start.length > 2, 'ERROR: OT conflict, setting attributes to drawing group that will be ungrouped. Reload forced by client OT handler: handleUngroupSetAttrs.'); // Reload required

            if (attrsOp.start.length > 2 || (!isSetAttrsOp && attrsOp.start.length === 2)) { // setAttributes inside a drawing
                drawingPosInGroup = attrsOp.start[2]; // the new top level position is specified by the (old) position of the insertChar operation
                newDrawingTopLevelPos = ungroupedDrawings[drawingPosInGroup];
                attrsOp.start[1] = newDrawingTopLevelPos;
                attrsOp.start.splice(2, 1); // index at [2] is no longer required
                if (attrsOp.end) {
                    attrsOp.end[1] = newDrawingTopLevelPos;
                    attrsOp.end.splice(2, 1); // index at [2] is no longer required
                }
            }
        } else {
            // not ungrouped, but the position needs to be increased -> counting ungrouped drawings before insert-drawing
            fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
            ungroupedDrawings.forEach(value => {
                if (previousUngroup >= 0 && !foundGap) {
                    gap = value - previousUngroup - 1;
                    if (gap > 0) {
                        if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                            newDrawingPos = previousUngroup + fillDrawings;
                            attrsOp.start[1] = newDrawingPos;
                            if (attrsOp.end) { attrsOp.end[1] = newDrawingPos; }
                            foundGap = true;
                        } else {
                            fillDrawings -= gap;
                        }
                    }
                }
                previousUngroup = value;
            });
            if (!foundGap) {
                newDrawingPos = previousUngroup + fillDrawings;
                attrsOp.start[1] = newDrawingPos; // shifting drawing with setAttributes operation behind the last ungrouped drawing
                if (attrsOp.end) { attrsOp.end[1] = newDrawingPos; }
            }
        }
    }

    /**
     * Handling an ungroup operation and a delete operation.
     *
     * If a slide is deleted, the ungroup operation can be affected.
     * If content inside a drawing is deleted, the ungroup operation influences the delete operation.
     * If a drawing is deleted, both operations influence each other.
     *
     * Important: If a drawing is deleted, there is always one delete operation for each drawing.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    handleUngroupDelete(localOp, extOp) {

        // the ungroup operation
        const ungroupOp = localOp.name === Op.UNGROUP ? localOp : extOp;
        // the delete operation
        const deleteOp = (ungroupOp === extOp) ? localOp : extOp;
        // the slide position of the delete operation
        const deleteSlidePos = deleteOp.start[0];
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];
        // the drawing position of the delete operation
        let drawingPos = null;
        // the drawing 'start' position of the ungroup operation
        let ungroupPos = null;
        // the array with indices of ungrouped drawings
        let ungroupedDrawings = null;
        // the drawing position inside its drawing group
        let drawingPosInGroup = 0;
        // the new toplevel position of the drawing on the slide
        let newDrawingTopLevelPos = 0;
        // whether a complete slide is removed
        const isSlideDeletion = deleteOp.start.length === 1;
        // whether the content inside a drawing is removed (ungrouping is only possible for toplevel drawings)
        const isDrawingContentDeletion = deleteOp.start.length > ungroupOp.start.length; // ungroupOp.start.length should always be 2
        // an optionally required new array of ungrouped drawings
        let newUngroupedDrawings = null;
        // whether a gap for the inserted drawing or the drawing that got a char inserted was found
        let foundGap = false;
        // the size of the gap between two ungrouped drawings
        let gap = 0;
        // the shift of the position in the delete operation
        let shift = 0;
        // the number of drawings that need to be filled into gaps between ungrouped drawings
        let fillDrawings = 0;
        // the position of the previous ungrouped drawing
        let previousUngroup = -1;

        if (isSlideDeletion) { // handling removal of a complete slide
            if (deleteSlidePos < ungroupSlidePos) {
                ungroupOp.start[0]--;
            } else if (deleteSlidePos === ungroupSlidePos) {
                setOperationRemoved(ungroupOp); // ungroup operation can be ignored, because the slide is already removed
            }
        } else if (isDrawingContentDeletion) {
            // only the delete operation needs to be modified
            if (deleteSlidePos !== ungroupSlidePos) { return; } // fast exit, different slides

            drawingPos = deleteOp.start[1];
            ungroupPos = ungroupOp.start[1];
            ungroupedDrawings = ungroupOp.drawings;

            if (drawingPos < ungroupPos) { return; } // fast exit, content inside a previous drawing removed

            // is the delete operation inside an ungrouped drawing?
            if (drawingPos === ungroupPos) {
                drawingPosInGroup = deleteOp.start[2]; // the new top level position is specified by the (old) position of the delete operation
                newDrawingTopLevelPos = ungroupedDrawings[drawingPosInGroup];
                deleteOp.start[1] = newDrawingTopLevelPos;
                deleteOp.start.splice(2, 1); // index at [2] is no longer required
                if (deleteOp.end) {
                    deleteOp.end[1] = newDrawingTopLevelPos;
                    deleteOp.end.splice(2, 1); // index at [2] is no longer required
                }
            } else {
                // not ungrouped, but the position needs to be increased -> counting ungrouped drawings before insert-drawing
                fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                ungroupedDrawings.forEach(value => {
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                shift = previousUngroup + fillDrawings;
                                deleteOp.start[1] = shift;
                                if (deleteOp.end) { deleteOp.end[1] = shift; }
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                });
                if (!foundGap) {
                    shift = previousUngroup + fillDrawings;
                    deleteOp.start[1] = shift; // shifting drawing with second operation behind the last ungrouped drawing
                    if (deleteOp.end) { deleteOp.end[1] = shift; }
                }
            }
        } else {
            // a drawing was deleted
            if (deleteSlidePos !== ungroupSlidePos) { return; } // fast exit, different slides

            drawingPos = deleteOp.start[1];
            ungroupPos = ungroupOp.start[1];

            this.ensure(drawingPos !== ungroupPos, 'ERROR: OT conflict, deleted drawing that will be ungrouped. Reload forced by client OT handler: handleUngroupDelete.'); // Reload required

            if (drawingPos < ungroupPos) { // the deleted drawing is before the ungrouped drawing

                newUngroupedDrawings = [];
                ungroupedDrawings = ungroupOp.drawings;
                ungroupedDrawings.forEach(value => { newUngroupedDrawings.push(--value); });
                ungroupOp.drawings = newUngroupedDrawings; // modifying the group operation
                ungroupOp.start[1]--;

            } else {

                // both operations influence each other
                ungroupedDrawings = ungroupOp.drawings;

                // check for a gap for the new inserted drawing in the ungrouped drawings -> modifying inserted drawing and following ungrouped drawings
                fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                ungroupedDrawings.forEach((value, index) => {
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                shift =  previousUngroup + fillDrawings;
                                deleteOp.start[1] = shift;
                                if (deleteOp.end) { deleteOp.end[1] = shift; }
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                    if (value > drawingPos) { ungroupOp.drawings[index]--; } // decreasing the index for all following ungrouped drawings
                });
                if (!foundGap) {
                    shift =  previousUngroup + fillDrawings; // shifting the deleted drawing behind the last ungrouped drawing
                    deleteOp.start[1] = shift;
                    if (deleteOp.end) { deleteOp.end[1] = shift; }
                }
            }
        }

    }

    /**
     * Handling an ungroup operation and a move operation.
     *
     * If a drawing is moved, the ungroup operation can be affected.
     * Also the ungrouping can influence the positions of the move operation.
     *
     * Important: If a drawing is moved, there is always one move operation for each drawing.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     * { name: move, start: [2, 1], to: [2, 4] }  // -> bringing drawing to front
     */
    handleUngroupMove(localOp, extOp) {

        // the ungroup operation
        const ungroupOp = localOp.name === Op.UNGROUP ? localOp : extOp;
        // the move operation
        const moveOp = (ungroupOp === extOp) ? localOp : extOp;
        // the slide position of the move operation
        const moveSlidePos = moveOp.start[0];
        // the slide position of the ungroup operation
        const ungroupSlidePos = ungroupOp.start[0];
        // the drawing start position of the move operation
        const drawingStartPos = moveOp.start[1];
        // the drawing 'to' position of the move operation
        const drawingToPos =  moveOp.to[1];
        // the drawing 'start' position of the ungroup operation
        const ungroupStartPos = ungroupOp.start[1];
        // the array with indices of ungrouped drawings
        const ungroupedDrawings = ungroupOp.drawings;
        // whether there is a gap in the array of ungrouped drawings (only in undo)
        let gapInArray = false;
        // whether a gap for the inserted drawing or the drawing that got a char inserted was found
        let foundGap = false;
        // the size of the gap between two ungrouped drawings
        let gap = 0;
        // the number of drawings that need to be filled into gaps between ungrouped drawings
        let fillDrawings = 0;
        // the position of the previous ungrouped drawing
        let previousUngroup = -1;
        // the last position of the ungrouped drawings
        let lastUngroupPos = 0;
        // whether the move operation does not influence the ungroup
        let simpleMove = false;
        // an optionally required new move operation
        let newMoveOperation = null;
        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        // helper function for difficult scenarios -> undo the move operation before ungroup
        const undoMoveOperation = () => {
            newMoveOperation = json.deepClone(moveOp);
            // -> the move operation must be marked as 'removed'
            setOperationRemoved(moveOp); // move operation can be ignored, because the moved slide is already removed

            newMoveOperation.start = json.deepClone(moveOp.to);
            newMoveOperation.to = json.deepClone(moveOp.start);
            if (newMoveOperation.end) { newMoveOperation.end = json.deepClone(newMoveOperation.start); }

            if (ungroupOp === extOp) {
                externalOpsBefore = [newMoveOperation];
            } else {
                localOpsBefore = [newMoveOperation];
            }
        };

        if (moveSlidePos !== ungroupSlidePos) { return; } // fast exit, different slides

        gapInArray = isGapInArray(ungroupedDrawings);

        if (drawingStartPos < ungroupStartPos) {

            if (drawingToPos >= ungroupStartPos) {
                if (gapInArray) {
                    fillDrawings = drawingToPos - ungroupStartPos; // the gaps will be filled with the preceeding drawings
                    ungroupedDrawings.forEach((value, index) => {
                        if (previousUngroup >= 0 && !foundGap) {
                            gap = value - previousUngroup - 1;
                            if (gap > 0) {
                                if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                    moveOp.to[1] = previousUngroup + fillDrawings;
                                    foundGap = true;
                                } else {
                                    fillDrawings -= gap;
                                }
                            }
                        }
                        previousUngroup = value;
                        if (!foundGap) { ungroupedDrawings[index]--; } // decreasing the index for all ungrouped drawings, as long as no gap for the moved drawing is found
                    });
                    if (!foundGap) { moveOp.to[1] = previousUngroup + fillDrawings; } // shifting drawing behind the last ungrouped drawing
                    ungroupOp.start[1]--; // adapting also the start position of the ungroup operation
                } else {
                    moveOp.to[1] += (ungroupedDrawings.length - 1); // shifting drawing behind the last ungrouped drawing
                    ungroupOp.start[1]--; // adapting also the start position of the ungroup operation
                    ungroupedDrawings.forEach((_value, index) => { ungroupedDrawings[index]--; });
                }
            }

        } else if (drawingStartPos > ungroupStartPos) {

            if (drawingToPos <= ungroupStartPos) {
                if (gapInArray) {
                    fillDrawings = drawingStartPos - ungroupStartPos; // the gaps will be filled with the preceeding drawings
                    ungroupedDrawings.forEach((value, index) => {
                        if (previousUngroup >= 0 && !foundGap) {
                            gap = value - previousUngroup - 1;
                            if (gap > 0) {
                                if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                    moveOp.start[1] = previousUngroup + fillDrawings;
                                    foundGap = true;
                                } else {
                                    fillDrawings -= gap;
                                }
                            }
                        }
                        previousUngroup = value;
                        if (!foundGap) { ungroupedDrawings[index]++; } // increasing the index for all ungrouped drawings, as long as the new position is not found
                    });
                    if (!foundGap) { moveOp.start[1] = previousUngroup + fillDrawings; } // shifting drawing start position behind the last ungrouped drawing
                    ungroupOp.start[1]++; // adapting also the start position of the ungroup operation
                } else {
                    moveOp.start[1] += (ungroupedDrawings.length - 1); // shifting drawing behind the last ungrouped drawing
                    ungroupOp.start[1]++; // adapting also the start position of the ungroup operation
                    ungroupedDrawings.forEach((_value, index) => { ungroupedDrawings[index]++; });
                }
            } else {
                lastUngroupPos = ungroupedDrawings[ungroupedDrawings.length - 1];
                simpleMove = moveOp.start[1] > lastUngroupPos && moveOp.to[1] > lastUngroupPos; // move happened completely behind ungroup

                if (gapInArray && !simpleMove) {
                    // reverting the move operation (this might be improved in the future)
                    // ungroup influences move, but move also ungroup (move [3, 5] to [3, 7] and ungroup [3,4] with positions [4, 5, 7])
                    undoMoveOperation();
                } else {
                    moveOp.start[1] += (ungroupedDrawings.length - 1);
                    moveOp.to[1] += (ungroupedDrawings.length - 1);
                }
            }

        } else {
            // the same drawing was moved and ungrouped -> undo the move operation
            undoMoveOperation();
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling a moveSlide operation and any operation that can only happen inside
     * a slide.
     * A moveSlide operation can only influence the first number in the position array
     * of the second operation. On the other hand, the operation on the slide, cannot
     * influence the moveSlide operation.
     *
     * This handler also handles setAttributes operations, that affect the slide, for
     * example slide background settings at position start: [2].
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    handleMoveSlideGeneric(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.SLIDE_MOVE ? localOp : extOp;
        // the second operation
        const secondOp = (moveOp === extOp) ? localOp : extOp;
        // the slide position of the second operation
        const secondPosStart = secondOp.start[0];
        // the slide start position of the move operation
        const movePosStart = moveOp.start[0];
        // the slide end position of the move operation
        const movePosTo = moveOp.end[0];

        if (movePosStart === secondPosStart) {
            secondOp.start[0] = movePosTo; // the slide with the change was moved
            if (secondOp.end) { secondOp.end[0] = movePosTo; }
            if (secondOp.to) { secondOp.to[0] = movePosTo; }
        } else if (movePosStart > secondPosStart && movePosTo <= secondPosStart) {
            secondOp.start[0]++;
            if (secondOp.end) { secondOp.end[0]++; }
            if (secondOp.to) { secondOp.to[0]++; }
        } else if (movePosStart < secondPosStart && movePosTo >= secondPosStart) {
            secondOp.start[0]--;
            if (secondOp.end) { secondOp.end[0]--; }
            if (secondOp.to) { secondOp.to[0]--; }
        }
    }

    /**
     * Handling a moveSlide operation and an insertComp operation (insertParagraph and insertSlide
     * (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, both operations influence each other.
     * In the case of an insertParagraph operation, the moveSlide operation affects the insertPargraph
     * operation, but not vice versa.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    handleMoveSlideInsertComp(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.SLIDE_MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = (moveOp === extOp) ? localOp : extOp;
        // the slide position of the insert operation
        const insertCompSlidePos = insertOp.start[0];
        // the slide start position of the move operation
        const movePosStart = moveOp.start[0];
        // the slide end position of the move operation
        const movePosTo = moveOp.end[0];

        if (insertOp.start.length === 1) { // handling an insertSlide operation -> both operations influence each other

            // Changing the move operation (insert moves both positions, even if positions are equal)
            if (insertCompSlidePos <= movePosStart) { moveOp.start[0]++; }
            if (insertCompSlidePos <= movePosTo) { moveOp.end[0]++; }

            // Changing also the insertSlide operation
            if (movePosStart >= insertCompSlidePos && movePosTo < insertCompSlidePos) {
                insertOp.start[0]++;
            } else if (movePosStart < insertCompSlidePos && movePosTo >= insertCompSlidePos) {
                insertOp.start[0]--;
            }

        } else {
            if (movePosStart === insertCompSlidePos) {
                insertOp.start[0] = movePosTo; // the slide with the change was moved
            } else if (movePosStart > insertCompSlidePos && movePosTo <= insertCompSlidePos) {
                insertOp.start[0]++;
            } else if (movePosStart < insertCompSlidePos && movePosTo >= insertCompSlidePos) {
                insertOp.start[0]--;
            }
        }
    }

    /**
     * Handling a moveSlide operation and a delete operation. The delete operation also
     * handles deleting slides. In this case the delete operation influences the moveSlide
     * operation. In all other cases (the delete happens on the slide) the moveSlide
     * operation influences the delete operation.
     *
     * Important for the delete operation is, that there is no range. Every slide is removed
     * with its own delete operation.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    handleMoveSlideDelete(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.SLIDE_MOVE ? localOp : extOp;
        // the delete operation
        const deleteOp = (moveOp === extOp) ? localOp : extOp;
        // the slide position of the second operation
        const deletePosStart = deleteOp.start[0];
        // the slide start position of the move operation
        const movePosStart = moveOp.start[0];
        // the slide end position of the move operation
        const movePosTo = moveOp.end[0];
        // whether this is a deletion of a slide
        const isSlideDeletion = deleteOp.start.length === 1;

        if (isSlideDeletion) {
            // -> a slide was deleted
            if (deletePosStart === movePosStart) { // the moved slide was deleted
                // -> the move operation must be marked as 'removed'
                setOperationRemoved(moveOp); // move operation can be ignored, because the moved slide is already removed
                // -> the delete operation gets as target the destination of the move operation
                deleteOp.start = json.deepClone(moveOp.end);
            } else {
                if (deletePosStart < movePosStart) { moveOp.start[0]--; }
                if (deletePosStart < movePosTo) { moveOp.end[0]--; }
                // move also influences delete
                if (movePosStart > deletePosStart && movePosTo <= deletePosStart) {
                    deleteOp.start[0]++;
                    if (deleteOp.end) { deleteOp.end[0]++; }
                } else if (movePosStart < deletePosStart && movePosTo >= deletePosStart) {
                    deleteOp.start[0]--;
                    if (deleteOp.end) { deleteOp.end[0]--; }
                }
            }
        } else {
            // -> deleting content on a slide -> the move operation influences the delete operation
            if (movePosStart === deletePosStart) {
                deleteOp.start[0] = movePosTo; // the slide with the deleted content was moved
                if (deleteOp.end) { deleteOp.end[0] = movePosTo; }
            } else if (movePosStart > deletePosStart && movePosTo <= deletePosStart) {
                deleteOp.start[0]++;
                if (deleteOp.end) { deleteOp.end[0]++; }
            } else if (movePosStart < deletePosStart && movePosTo >= deletePosStart) {
                deleteOp.start[0]--;
                if (deleteOp.end) { deleteOp.end[0]--; }
            }
        }
    }

    /**
     * Handling two moveSlide operations.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    handleMoveSlideMoveSlide(localOp, extOp) {

        const transformResult =  transformIndexMoveMove(localOp.start[0], localOp.end[0], extOp.start[0], extOp.end[0]);
        const localResult = transformResult.lclRes;
        const extResult = transformResult.extRes;

        if (localResult) {
            localOp.start[0] = localResult.fromIdx;
            localOp.end[0] = localResult.toIdx;
        } else {
            setOperationRemoved(localOp);
        }

        if (extResult) {
            extOp.start[0] = extResult.fromIdx;
            extOp.end[0] = extResult.toIdx;
        } else {
            setOperationRemoved(extOp);
        }
    }

    /**
     * Handling any comment operation with a delete operation.
     *
     * These operatons influence each other, if the delete operation deletes a complete slide.
     */
    handleCommentDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the comment operation
        const commentOp = (localOp === deleteOp) ? extOp : localOp;

        if (deleteOp.start.length > 1) { return; } // handle only delete operations for slides

        if (deleteOp.start[0] < commentOp.start[0]) {
            commentOp.start[0]--;
        } else if ((deleteOp.start[0] === commentOp.start[0])) {
            setOperationRemoved(commentOp); // the slide is already removed
        }
    }

    /**
     * Handling an insertSlide operation and a second operation, that does not influence the
     * insertSlide operation.
     *
     * Info: This handler is also called for insertParagraph operation, but it can be ignored in this case.
     */
    handleInsertSlideGeneric(localOp, extOp) {

        // the insertSlide/insertParagraph operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the second operation
        const secondOp = (localOp === insertOp) ? extOp : localOp;

        if (insertOp.start.length > 1) { return; } // handle only insertSlide operations

        if (insertOp.start[0] <= secondOp.start[0]) { secondOp.start[0]++; }
    }

    /**
     * Handling two insertComment operations.
     *
     * These operatons influence each other, if they happen on the same slide.
     *
     * If there are two identical comment positions and one comment is a child of a previous comment and
     * the other comment is not, then the position of the comment, that is not a child, must be increased.
     */
    handleInsertCommentInsertComment(localOp, extOp) {

        let localParent = null;
        let extParent = null;

        if (localOp.start[0] === extOp.start[0]) {

            if (localOp.start[1] < extOp.start[1]) { // INFO: On server side this is implemented with '<='
                extOp.start[1]++;
            } else {
                localParent = localOp.parent;
                extParent = extOp.parent;
                if (is.number(localParent) && !is.number(extParent)) {
                    extOp.start[1]++; // increasing external number, if local comment is a child comment
                } else if (!is.number(localParent) && is.number(extParent)) {
                    localOp.start[1]++;
                } else {
                    localOp.start[1]++; // increasing local number, if both positions are equal
                }
            }
        }
    }

    /**
     * Handling two deleteComment operations.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    handleDeleteCommentDeleteComment(localOp, extOp) {

        if (localOp.start[0] === extOp.start[0]) {
            if (localOp.start[1] < extOp.start[1]) {
                extOp.start[1]--;
            } else if  (localOp.start[1] > extOp.start[1]) {
                localOp.start[1]--;
            } else {
                // ignoring both operations -> both removed the same comment
                setOperationRemoved(extOp);
                setOperationRemoved(localOp);
            }
        }
    }

    /**
     * Handling two changeComment operations.
     *
     * These operatons influence each other, if they happen in the same comment.
     */
    handleChangeCommentChangeComment(localOp, extOp) {

        if (localOp.start[0] === extOp.start[0] && localOp.start[1] === extOp.start[1]) {
            // ignore the external operation -> local operation will be sent to the server and overwrite comment again
            setOperationRemoved(extOp);
        }
    }

    /**
     * Handling an insertComment and a deleteComment operation.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    handleDeleteCommentInsertComment(localOp, extOp) {

        // the delete comment operation
        const deleteOp = localOp.name === Op.DELETE_COMMENT ? localOp : extOp;
        // the insert comment operation
        const insertOp = (localOp === deleteOp) ? extOp : localOp;

        if (deleteOp.start[0] === insertOp.start[0]) {
            if (deleteOp.start[1] < insertOp.start[1]) { // check '<=' on server side?
                insertOp.start[1]--;
            } else if (deleteOp.start[1] >= insertOp.start[1]) {
                deleteOp.start[1]++;
            }
        }
    }

    /**
     * Handling an insertComment and a changeComment operation.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    handleChangeCommentInsertComment(localOp, extOp) {

        // the insert comment operation
        const insertOp = localOp.name === Op.INSERT_COMMENT ? localOp : extOp;
        // the change comment operation
        const changeOp = (localOp === insertOp) ? extOp : localOp;

        if (insertOp.start[0] === changeOp.start[0] && insertOp.start[1] <= changeOp.start[1]) { changeOp.start[1]++; }
    }

    /**
     * Handling a deleteComment and a changeComment operation.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    handleChangeCommentDeleteComment(localOp, extOp) {

        // the delete comment operation
        const deleteOp = localOp.name === Op.DELETE_COMMENT ? localOp : extOp;
        // the change comment operation
        const changeOp = (localOp === deleteOp) ? extOp : localOp;

        if (deleteOp.start[0] === changeOp.start[0]) {
            if (deleteOp.start[1] < changeOp.start[1]) {
                changeOp.start[1]--;
            } else if (deleteOp.start[1] === changeOp.start[1]) {
                setOperationRemoved(changeOp); // the comment is already removed
            }
        }
    }

    /**
     * Handling an insertCell and a deleteColumn operation.
     *
     * This function overwrites the function in the base class, because in OX Presentation
     * is a reload required, when a cell cannot be inserted (DOCS-4422).
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    handleInsertCellsDeleteColumns(localOp, extOp) {

        super.handleInsertCellsDeleteColumns(localOp, extOp);

        // the insertCells operation
        const insertOp = localOp.name === Op.CELLS_INSERT ? localOp : extOp;

        // in OX Presentation missing cells in tables are not allowed -> reload (DOCS-4422)
        const reloadRequired = insertOp.name === Op.CELLS_INSERT && isOperationRemoved(insertOp);
        this.ensure(!reloadRequired, 'ERROR: OT conflict, cells cannot be inserted after deleteColumns operation.', { forceReload: true });
    }

    /**
     * Handling an insertCell/insertRows and a delete operation.
     *
     * This function overwrites the function in the base class, because in OX Presentation
     * is a reload required, when a cell cannot be inserted (DOCS-4422).
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    handleInsertRowsDelete(localOp, extOp) {

        super.handleInsertRowsDelete(localOp, extOp);

        // the insertCells operation
        const insertOp = (localOp.name === Op.CELLS_INSERT || localOp.name === Op.ROWS_INSERT) ? localOp : extOp;

        // in OX Presentation missing cells in tables are not allowed -> reload (DOCS-4422)
        const reloadRequired = insertOp.name === Op.CELLS_INSERT && isOperationRemoved(insertOp);
        this.ensure(!reloadRequired, 'ERROR: OT conflict, cells cannot be inserted after delete operation.', { forceReload: true });
    }

}
