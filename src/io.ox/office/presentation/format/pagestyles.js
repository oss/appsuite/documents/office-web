/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { convertHmmToLength } from '@/io.ox/office/tk/dom';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';

// constants ==================================================================

// definitions for page attributes
const PAGE_ATTRIBUTES = {
    width: { def: 28000 },
    height: { def: 21000 },
    marginLeft: { def: 0 },
    marginRight: { def: 0 },
    marginTop: { def: 0 },
    marginBottom: { def: 0 },
    marginHeader: { def: 0 },
    marginFooter: { def: 0 },
    firstPage: { def: false },
    evenOddPages: { def: false }
};

// class PageStyles ===========================================================

/**
 * Contains the style sheets for page formatting attributes. The CSS
 * formatting will be read from and written to the page container elements.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 */
export default class PageStyles extends StyleCollection {

    constructor(docModel) {

        // base constructor
        super(docModel, 'page');

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('page', PAGE_ATTRIBUTES);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updatePageFormatting);
    }

    /**
     * Will be called for every page whose attributes have been changed.
     *
     * @param {jQuery} page
     *  The page container element whose character attributes have been
     *  changed, as jQuery object.
     *
     * @param {Object} mergedAttrs
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     */
    #updatePageFormatting(page, mergedAttrs) {

        // the page attributes of the passed attribute map
        const pageAttrs = mergedAttrs.page;
        // effective page width (at least 2 cm)
        const pageWidth = Math.max(pageAttrs.width, 2000);
        // effective page height (at least 2 cm)
        const pageHeight = Math.max(pageAttrs.height, 2000);

        // Set CSS attributes.
        page.css({
            width: convertHmmToLength(pageWidth, 'px', 1),
            minHeight: convertHmmToLength(pageHeight, 'px', 1)
        });
    }

}
