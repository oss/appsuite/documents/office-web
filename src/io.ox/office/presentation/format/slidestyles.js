/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { Canvas } from '@/io.ox/office/tk/canvas';
import { convertHmmToLength, getBooleanOption } from '@/io.ox/office/tk/utils';

import { isFillThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import Gradient from '@/io.ox/office/editframework/utils/gradient';
import { createPatternDataURL } from '@/io.ox/office/editframework/utils/pattern';
import { getTextureFill } from '@/io.ox/office/editframework/utils/texture';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';
import { LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS, LIVE_PREVIEW_SLIDE_BACKGROUND_SELECTOR } from '@/io.ox/office/textframework/utils/dom';

// constants ==============================================================

// definitions for slide attributes
const DEFINITIONS = {

    /**
     * Whether the drawings from layout or master slide shall be visible.
     */
    followMasterShapes: { def: true },

    /**
     * Whether the date place holder will be inherited.
     */
    isDate: { def: false },

    /**
     * Whether the header place holder will be inherited.
     */
    isFooter: { def: false },

    /**
     * Whether the footer place holder will be inherited.
     */
    isHeader: { def: false },
    /**
     * Whether the slide number place holder will be inherited.
     */
    isSlideNum: { def: false },

    /**
     * Whether the drawings from layout or master slide shall be visible.
     */
    hidden: { def: false },

    /**
     * The type of a layout slide
     */
    type: { def: '' },

    /**
     * The fixed text in ODF placeholder drawings of type 'ftr'
     */
    footerText: { def: '' },

    /**
     * The fixed text in ODF placeholder drawings of type 'dt'
     */
    dateText: { def: '' },

    /**
     * The field text in ODF placeholder drawings of type 'dt'
     */
    dateField: { def: '' }
};

// Info for the properties 'isDate', 'isFooter', 'isHeader' and 'isSildeNum'.
// In OOXML file there is an 'hf' element. If this does not exist, all values are set
// to false. This is the default that the client uses. But if there is at least one child
// (for example 'isFooter') set explicitely to 'false', all other (not defined) children
// are set automatically to true (the default changes). This needs to be handled by the
// filter for the loading operations of the document. If the client changes one of these
// values in the UI, all four values need to be set.

// class SlideStyles =======================================================

/**
 * Contains the style sheets for page formatting attributes. The CSS
 * formatting will be read from and written to the slide container elements.
 *
 * @param {PresentationModel} docModel
 *  The presentation document model containing this instance.
 */
class SlideStyles extends StyleCollection {

    #backgroundImageFailureSlideIds;

    constructor(docModel) {

        // base constructor
        super(docModel, 'slide', { families: ['fill'] });

        // properties ---------------------------------------------------------

        // SlideIds with background image failures, with a boolean flag which indicates if a notification for this slide was displayed
        this.#backgroundImageFailureSlideIds = {};

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('slide', DEFINITIONS);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updateSlideFormatting);
    }

    // public methods -----------------------------------------------------

    /**
     * Public accesor for updateBackground. See function updateBackground for more details.
     *
     * @param {jQuery} $slide
     *  The slide element whose attributes have been changed, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     *
     * @param {String} slideId
     *  The ID of the slide whose background will be updated.
     *
     * @param {String} themeSlideIdStr
     *  The ID of the slide that determines the theme.
     *
     * @param {Object} [options]
     *  Optional parameters
     *
     * @returns {jQuery.Promise}
     */
    updateBackground($slide, mergedAttributes, slideId, themeSlideIdStr, options) {
        return this.#updateBackground($slide, mergedAttributes, slideId, themeSlideIdStr, options);
    }

    /**
     * Forcing an update of slide background with a specified theme (48471).
     *
     * @param {jQuery} slide
     *  The slide element whose attributes have been changed, as jQuery object.
     *
     * @param {Object} attributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     *
     * @param {String} slideId
     *  The ID of the slide whose background will be updated.
     *
     * @param {String} themeId
     *  The ID of the slide that determines the theme.
     *
     * @returns {jQuery.Promise}
     */
    forceSlideBackgroundUpdateWithSpecificId(slide, attributes, slideId, themeId) {
        return this.#updateBackground(slide, attributes, slideId, themeId);
    }

    // private functions --------------------------------------------------

    /**
     * Updating the slide background node with the specified attributes of the family 'fill'.
     *
     * @param {jQuery} $slide
     *  The slide element whose attributes have been changed, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     *
     * @param {String} slideId
     *  The ID of the slide whose background will be updated.
     *
     * @param {String} [themeSlideIdStr]
     *  The ID of the slide that determines the theme.
     *
     * @param {Object} [options]
     *  Optional object containing paramters.
     *  @param {Boolean} [options.imageLoad=false]
     *         Optional paramter indicating that updateBackground was triggered by image load in preview dialog.
     *  @param {Boolean} [options.preview=false]
     *         Optional parameter indicating that updateBackground was triggered from preview dialog.
     *  @param {Boolean} [options.onDialogOpen=false]
     *         Optional parameter indicating that updateBackground was triggered from preview dialog on initial dialog openning.
     *
     * @returns {jQuery.Promise}
     */
    #updateBackground($slide, mergedAttributes, slideId, themeSlideIdStr, options) {

        // the fill attributes of the slide
        let fillAttrs = mergedAttributes.fill;
        // the fill attributes of the slide
        let inheritedFillAttrs = null;
        // the CSS properties for the slide
        const cssProps = {};
        // the slide ID that is used for the theme color
        const themeSlideId = themeSlideIdStr || slideId;
        // the target chain needed to resolve the current theme
        const targets = this.docModel.getTargetChain(themeSlideId);
        // if updateBackground was triggered by image load in preview dialog
        const isImageLoad = getBooleanOption(options, 'imageLoad', false);
        // if updateBackground is triggered in preview dialog
        const isPreview = getBooleanOption(options, 'preview', false);
        // if updateBackground is called on dialogOpen
        const calledOnDialogOpen = getBooleanOption(options, 'onDialogOpen', false);
        // slide node for which the background is updated
        let livePreviewNode = null;
        // the slide of the specified element
        let elmSlide = null;
        // the gradient object
        let gradient = null;
        // the data url for the gradient
        let dataUrl = null;
        // the result promise
        let promise = null;

        // Helper function to create a background for a slide, that has its own theme, but no slide background specified.
        // If an underlaying layout or master slide has a specified background that also uses a theme, it it necessary, to
        // update this background, with the theme of the current slide.
        // -> this problem should be solved by reformatting the background of the underlying slide, but such a process
        //    is currently not supported by the slide pane. In this case, there is more than one 'state' for the underlying
        //    slide dependent from the theme of the visible slide.
        // -> in the current solution, the visible slide gets a slide background with correct theming, although no slide
        //    background is specified for this slide.
        const checkInheritedFillAttrs = () => {
            if (!this.docModel.isMasterSlideId(slideId) && this.docModel.hasDefaultTheme(slideId)) {
                const backgroundSlideId = this.docModel.getMostTopBackgroundSlide(_.rest(targets));
                if (backgroundSlideId) {
                    inheritedFillAttrs = this.docModel.getSlideAttributesByFamily(backgroundSlideId, 'fill');
                    if (isFillThemed(inheritedFillAttrs)) {
                        fillAttrs = _.copy(inheritedFillAttrs, true);
                    }
                }
            }
        };

        // special handling for task 48471 -> setting new fill attrs, if the slide has no background, but a default theme registered.
        if (fillAttrs.type === 'none') { checkInheritedFillAttrs(); }

        // calculate the CSS fill attributes
        switch (fillAttrs.type) {
            case 'none':
            case null:
                // clear everything: color and bitmaps
                cssProps.background = '';
                break;

            case 'solid':
                cssProps.background = this.docModel.getCssColor(fillAttrs.color, 'fill', targets);
                if (isPreview) { cssProps.opacity = ''; } // removing an optional opacity set from an image background
                break;

            case 'gradient':
                elmSlide = $slide[0];
                gradient = Gradient.create(Gradient.getDescriptorFromAttributes(fillAttrs));
                dataUrl = Gradient.parseImageDataUrl(gradient, this.docModel, { width: elmSlide.offsetWidth, height: elmSlide.offsetHeight }, targets);

                cssProps.background = 'url("' + dataUrl + '")';
                break;

            case 'pattern':
                cssProps.background = 'url("' + createPatternDataURL(fillAttrs.pattern, fillAttrs.color2, fillAttrs.color, this.docModel.getThemeModel(targets)) + '")';
                break;

            case 'bitmap':
                if (!fillAttrs.bitmap) { return $.Deferred().reject(); }

                if (isPreview && fillAttrs.bitmap.transparency && _.isNumber(fillAttrs.bitmap.transparency)) {
                    cssProps.opacity = 1 - fillAttrs.bitmap.transparency;
                }
                if (!isPreview || isImageLoad) {
                    if (calledOnDialogOpen) {
                        livePreviewNode = $slide.parent().children(LIVE_PREVIEW_SLIDE_BACKGROUND_SELECTOR).removeClass(LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS);
                    }
                    if (!this.docModel.getApp().isODF() && fillAttrs.bitmap.tiling && (fillAttrs.bitmap.tiling.stretchX === 0 || fillAttrs.bitmap.tiling.stretchY === 0)) {
                        promise = $.when(); // #55969 - when stretching is 0%, MS doesn't display image, ODP displays it as 100%
                    } else {
                        promise = this.#generateImageURLfromCanvas(fillAttrs, isPreview, slideId).then(canvasUrl => {
                            cssProps.background = 'url("' + canvasUrl + '")';
                            $slide.css(cssProps);
                            if (calledOnDialogOpen) {
                                livePreviewNode.addClass(LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS);
                            }
                            if (!calledOnDialogOpen && !isPreview) {
                                this.docModel.forceTriggerOfSlideStateUpdateEvent(slideId); // do not trigger, if slide background dialog is open (DOCS-4084)
                            }
                        });
                    }
                }
                break;

            default:
                globalLogger.warn('SlideStyles.setSlideBackground(): unknown fill type "' + fillAttrs.type + '"');
        }

        // taking care of helper node for background preview during dialog open
        if (!isPreview) {
            this.docModel.removeHelpBackgroundNode($slide);
        }
        // apply the fill attributes
        $slide.css(cssProps);

        return promise || $.when();
    }

    /**
     * Helper method for loading image in canvas, make texture tiling, and returning edited image as base64 png image url.
     *
     * @param  {Object} fillAttrs
     *   fill attributes
     * @returns {jQuery.Promise|String}
     *   String base64 png image url, if the promise is resolved.
     */
    #generateImageURLfromCanvas(fillAttrs, isPreview, slideId) {
        const slideSize = this.docModel.getSlideDocumentSize();
        const slideW = convertHmmToLength(slideSize.width, 'px', 1);
        const slideH = convertHmmToLength(slideSize.height, 'px', 1);
        const tempCanvas = new Canvas({ location: { width: slideW, height: slideH } });

        const promise = tempCanvas.render(tempCtx => {
            const promise = getTextureFill(this.docModel, tempCtx, fillAttrs, slideW, slideH, 0, { isSlide: true, noAlpha: isPreview });
            promise.fail(() => {
                if (!_.isEmpty(slideId) && !(slideId in this.#backgroundImageFailureSlideIds)) {
                    this.#backgroundImageFailureSlideIds[slideId] = true;
                    this._showBackgroundImageFailureNotification();
                }
            });
            return promise.then(textureFill => {
                tempCtx.setFillStyle(textureFill).drawRect(0, 0, slideW, slideH, 'fill');
                return tempCanvas.getDataURL();
            });
        });

        return promise.done(() => {
            tempCanvas.destroy();
        });
    }

    /**
     * Show a notification for all slides with background image failures.
     */
    @debounceMethod({ delay: 1000 })
    _showBackgroundImageFailureNotification() {

        if (this.docModel.getSlideFormatManager().allSlidesFormatted()) {
            let idList = null;
            let firstId = true;
            _.each(this.#backgroundImageFailureSlideIds, (show, slideId) => {
                // check if a notifaction was displayed for this slide
                if (show) {
                    let id = '';
                    if (this.docModel.isLayoutOrMasterId(slideId)) {
                        id = this.docModel.getSlideFamilyAttributeForSlide(slideId, 'slide', 'name');
                        if (_.isEmpty(id)) {
                            id = null;
                        }
                    } else {
                        id = this.docModel.getSortedSlideIndexById(slideId);
                        if (id >= 0) {
                            id++;
                        } else {
                            id = null;
                        }
                    }
                    if (id !== null) {
                        if (firstId) {
                            idList = String(id);
                        } else {
                            idList += ', ' + String(id);
                        }
                        firstId = false;
                    }
                    this.#backgroundImageFailureSlideIds[slideId] = false;
                }
            });
            if (idList) {
                this.docModel.getApp().getView().yell({ type: 'info', message: gt('This document contains slide background images which can\'t be displayed: %s', idList) });
            }
        } else {
            this._showBackgroundImageFailureNotification();
        }
    }

    /**
     * Will be called for every page whose attributes have been changed.
     *
     * @param {jQuery} slide
     *  The slide element whose attributes have been changed, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     *
     * @returns {jQuery.Promise}
     *  A promise that is resolved, after the slide background is set.
     */
    #updateSlideFormatting(slide, mergedAttributes) {

        // the slide id
        const slideId = this.docModel.getSlideId(slide);
        // getting the old 'hidden' state (the model is already updated)
        const oldHiddenState = (slide.attr('isHidden') === 'true');
        // whether the hidden state of a slide was changed
        const hiddenStateChanged = (mergedAttributes.slide.hidden !== oldHiddenState);
        // a promise for handling the slide background
        let backgroundPromise = null;

        // updating the slide background
        backgroundPromise = this.#updateBackground(slide, mergedAttributes, slideId);

        // setting a marker at the slide, so that changes can be recognized
        if (hiddenStateChanged) { slide.attr('isHidden', mergedAttributes.slide.hidden); }

        // inform the view, that the merged slide attributes (including 'fill') need to be updated
        backgroundPromise.done(() => {
            this.docModel.trigger('change:slideAttributes', { slideId, hiddenStateChanged, isHidden: mergedAttributes.slide.hidden });
        });

        return backgroundPromise;
    }
}

// exports ================================================================

export default SlideStyles;
