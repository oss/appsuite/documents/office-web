/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { json, math } from '@/io.ox/office/tk/algorithms';
import { createIcon } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { convertHmmToLength, getBooleanOption, getDomNode, iterateSelectedDescendantNodes } from '@/io.ox/office/tk/utils';
import { setToolTip } from '@/io.ox/office/tk/forms';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { IMAGE_TEMPLATE_BUTTON_CLASS, PARAGRAPH_NODE_SELECTOR, PLACEHOLDER_TEMPLATE_BUTTON_CLASS,
    TABLE_TEMPLATE_BUTTON_CLASS, getTargetContainerId, isBorderAttributeSet, isEmptyTextframe } from '@/io.ox/office/textframework/utils/dom';
import DrawingStyleCollection from '@/io.ox/office/drawinglayer/model/drawingstylecollection';
import { EMPTY_THICKBORDER_CLASS, EMPTYTEXTFRAME_CLASS, EMPTYTEXTFRAMEBORDER_CLASS, NO_GROUP_CLASS, ROTATED_DRAWING_CLASSNAME,
    getGroupNode, getTextFrameNode, isAutoResizeHeightDrawingFrameWithHorizontalText, isGroupContentNode, isGroupedDrawingFrame,
    isShapeDrawingFrame, isTableDrawingFrame, isTextFrameShapeDrawingFrame, prepareDrawingFrameForTextInsertion, updateCssTransform,
    updateFormatting } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { CONTENTBODY, DEFAULT_PLACEHOLDER_INDEX, DEFAULT_PLACEHOLDER_TYPE, NO_TEXT_PLACEHOLDER_TYPES,
    getDrawingModelDescriptor, getPlaceHolderDrawingIndex, getPlaceHolderDrawingType,
    getPlaceHolderTemplateButtonsList, getValidTypeIndexSet, isContentBodyPlaceHolderAttributeSet,
    isPlaceHolderAttributeSet, isPlaceHolderDrawing, isPlaceHolderWithoutTextDrawing,
    isPlaceHolderWithoutTextAttributeSet, isEmptyPlaceHolderDrawing,
    isTitleOrSubtitlePlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';

// class PresentationDrawingStyles ========================================

/**
 * Contains the style sheets for drawing formatting attributes. The CSS
 * formatting will be read from and written to drawing elements of any type.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 */
export default class PresentationDrawingStyles extends DrawingStyleCollection {

    // a marker for ODF documents
    #odf;
    // collector for the place holder attributes in layout or master slides
    #placeHolderCollector;

    constructor(docModel) {

        // base constructor
        super(docModel, {
            families: ["presentation", "table", "changes"],
            baseAttributesResolver: (...args) => this.#resolveLayoutAttributes(...args)
        });

        // properties ---------------------------------------------------------

        this.#placeHolderCollector = {};
        this.#odf = this.docModel.getApp().isODF();

        // initialization -----------------------------------------------------

        // register the formatting handler for DOM elements (will be called during document import too)
        this.registerFormatHandler(this.#updateDrawingFormatting, { duringImport: false });

        // register handler for removing content for place holder collector
        this.listenTo(this.docModel, 'removed:slide', this.#removeFromPlaceholderCollector);
    }

    // public methods -----------------------------------------------------

    /**
     * Saving the drawing attributes in a container, if the drawing is a place holder
     * element.
     *
     * @param {String} target
     *  The id of the layout or master slide, that defines the attributes for
     *  the presentation place holder type.
     *
     * @param {Object} attrs
     *  The attributes object, that contains the attributes for the specified
     *  target and place holder type. This are the explicit attributes set at
     *  the drawing.
     *
     * @param {jQuery|Node} drawing
     *  The drawing node, that gets the specified attributes assigned.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.saveOldAttrs=false]
     *      Whether the old attributes shall be saved and returned in the return object.
     *
     * @returns {Object|Null}
     *  If the attributes are registered, an object with the complete registration
     *  information is returned. The object's keys are: 'target', 'placeHolderType',
     *  'placeHolderIndex', 'attrs' and 'oldAttrs'. If the attributes were not registered,
     *  because target is not set (no master or layout slide) or the attributes
     *  contain no place holder information (no place holder drawing), null is
     *  returned.
     */
    savePlaceHolderAttributes(target, attrs, drawing, options) {

        if (!target || !isPlaceHolderDrawing(drawing)) { return null; }

        const keySet = getValidTypeIndexSet(getExplicitAttributeSet(drawing, true));

        // target must be defined and the attributes must have phType or phIndex specified
        // -> this is a placeholder drawing in master or layout slide -> the attributes are saved in the model.
        return this.#savePlaceHolderAttributes(target, keySet.type, keySet.index, attrs, options);
    }

    /**
     * If a place holder drawing is deleted on a master or layout slide, it is necessary
     * to remove its place holder attributes from the model.
     *
     * @param {jQuery|Node} drawing
     *  The drawing node, that gets the specified attributes assigned.
     */
    deletePlaceHolderAttributes(drawing) {

        // the target of the slide that contains the drawing
        let target = '';
        // the explicit drawing attributes
        let drawingAttrs = null;
        // the type of the place holder drawing
        let type = '';
        // the index of the place holder drawing
        let index = 0;

        if (!isPlaceHolderDrawing(drawing)) { return null; }

        // checking, if the drawing is located on a master or layout slide
        target = getTargetContainerId($(drawing).parent()); // place holder drawings are direct children of slide

        if (this.docModel.isLayoutOrMasterId(target)) {

            // getting type and index from the drawing
            drawingAttrs = getExplicitAttributeSet(drawing);

            if (this.#placeHolderCollector[target]) {
                type = getPlaceHolderDrawingType(drawing, drawingAttrs) || DEFAULT_PLACEHOLDER_TYPE;
                if (this.#placeHolderCollector[target][type]) {
                    index = getPlaceHolderDrawingIndex(drawing, drawingAttrs) || DEFAULT_PLACEHOLDER_INDEX;
                    if (this.#placeHolderCollector[target][type][index]) {
                        delete this.#placeHolderCollector[target][type][index];
                        if (_.isEmpty(this.#placeHolderCollector[target][type])) {
                            delete this.#placeHolderCollector[target][type];
                            if (_.isEmpty(this.#placeHolderCollector[target])) {
                                delete this.#placeHolderCollector[target];
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Receiving the attributes for a drawing with place holder attribute. In this case the attributes from the
     * parent layers can be used. If the drawing is inside a 'standard' slide, the attributes might be defined
     * in the layout or the master slide. If the drawing is inside a layout slide, the missing attributes might
     * be defined in the master slide.
     *
     * Info: For master slides the index needs to be ignored. This can easily be seen for the place holder types
     *       'ftr', 'dt' and 'sldNum'. In the default master slide they have not the same indices as used in the
     *       layout slides.
     *
     * @param {String} target
     *  ID of target container node.
     *
     * @param {String} type
     *  The place holder type.
     *
     * @param {Number} index
     *  The place holder index.
     *
     * @returns {Object|Null}
     *  The attributes defined for the specified target and place holder type.
     */
    getPlaceHolderAttributes(target, type, index) {

        // the attributes assigned to target and type
        let attributes = null;
        // the available indices for this type of place holder
        let indices = null;

        // switching type, if not supported on master slide (or on layout slide in ODF)
        if ((this.docModel.isMasterSlideId(target) || (this.#odf && this.docModel.isLayoutSlideId(target))) && PresentationDrawingStyles.MASTER_PLACEHOLDER_TYPE_SWITCHES[type]) { type = PresentationDrawingStyles.MASTER_PLACEHOLDER_TYPE_SWITCHES[type]; }

        if (this.#placeHolderCollector && this.#placeHolderCollector[target] && this.#placeHolderCollector[target][type]) {
            attributes = this.#placeHolderCollector[target][type];

            if (attributes[index]) {
                attributes = attributes[index];
            } else {
                if (this.docModel.isMasterSlideId(target)) {
                    // if this is a master slide, the index can be ignored
                    indices = _.keys(attributes);
                    if (indices && indices.length > 0) {
                        attributes = attributes[indices[0]]; // using the first key
                    }
                }
            }
        }

        return attributes ? json.deepClone(attributes) : null;
    }

    /**
     * Receiving all place holder attributes that are stored for a specific master or layout node.
     *
     * @param {String} target
     *  ID of target container node.
     *
     * @returns {Object|Null}
     *  An object containing all place holder attributes defined for the specified layout/master
     *  slide. Or null, if this slide contains no place holder attributes.
     */
    getAllPlaceHolderAttributesForId(target) {
        return this.#placeHolderCollector[target] || null;
    }

    /**
     * If there are no placeholder attributes for "title" and "body" (that cannot be deleted in LibreOffice)
     * this set of fallback-attributes is used (DOCS-4913).
     *
     * @returns {Object|Null}
     *  An object containing all default place holder attributes for the types "title" and "body" on ODF
     *  master slides.
     */
    getODFDefaultPlaceHolderAttributes() {

        // taking care of current slide size and the slide size for the default values. This slide size is
        // specified in PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_SLIDE_SIZE
        const slideSize = this.docModel.getSlideDocumentSize();
        const widthRatio = math.roundp(slideSize.width / PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_SLIDE_SIZE.width, 0.0001);
        const heightRatio = math.roundp(slideSize.height / PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_SLIDE_SIZE.height, 0.0001);

        // adapting the values for "left", "top", "width" and "height" in the "drawing" family to the current slide size
        const placeholderAttrs = json.deepClone(PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_ATTRIBUTES);

        // iterating over type ("title", "body", ...) and over "index"
        for (const type in placeholderAttrs) {
            for (const index in placeholderAttrs[type]) {
                const drawingAttrs = placeholderAttrs[type][index].drawing;
                if (drawingAttrs?.left) { drawingAttrs.left *= widthRatio; }
                if (drawingAttrs?.top) { drawingAttrs.top *= heightRatio; }
                if (drawingAttrs?.width) { drawingAttrs.width *= widthRatio; }
                if (drawingAttrs?.height) { drawingAttrs.height *= heightRatio; }
            }
        }

        return placeholderAttrs;
    }

    /**
     * Return highest placeholder index used for a specific master or layout node.
     *
     * @param {String} target
     *  ID of target container node.
     *
     * @returns {Number}
     *  Highest placeholder index used on given slide, or 0 if there are no placeholder indexes used.
     */
    getHighestPlaceholderIndexForId(target) {
        const placeholderAttrs = this.#placeHolderCollector[target];
        const placeholderKeys = [];
        _.each(placeholderAttrs, plHolder => {
            _.each(_.keys(plHolder), oneKey => {
                if (oneKey && _.isString(oneKey)) {
                    oneKey = parseInt(oneKey, 10);
                    placeholderKeys.push(oneKey);
                }
            });
        });
        placeholderKeys.sort((a, b) => { return a - b; });

        return _.last(placeholderKeys) || 0;
    }

    /**
     * Receiving a place holder index that is not used for a specified slide ID
     *
     * @param {String} id
     *  ID of a layout or master slide. If this is a document slide, the id of the corresponding
     *  layout slide is used.
     *
     * @returns {Number}
     *  A free index for a place holder drawing
     */
    getFreePlaceHolderIndex(id) {

        // the index number
        let index = 0;
        // all used indices
        let allUsedIndices = [];
        // the id of the layout or master slide
        const layoutId = this.docModel.isLayoutOrMasterId(id) ? id : this.docModel.getLayoutSlideId(id);
        // all layout drawings attribute sets
        const allAttributes = this.getAllPlaceHolderAttributesForId(layoutId);
        // the maximum of the used values
        let max = 0;

        // iterating over all types
        _.each(allAttributes, type => {
            allUsedIndices = allUsedIndices.concat(_.keys(allAttributes[type]));
        });

        max = Math.max(...allUsedIndices);

        if (_.isNumber(max)) { index = max + 1; }

        return index;
    }

    /**
     * Receiving the complete list style object. This is necessary, if a snap shot is created, because these
     * list styles are not saved in the data object of the drawing. The other attributes are all saved in the
     * data-object, so that they can be restored later.
     *
     * Info: This function shall only be used for snap shot creation.
     *
     * @returns {Object}
     *  The collector object for the list style attributes assigned to place holder drawings.
     */
    getAllDrawingListStyleAttributes() {

        // the object containing the drawing list styles
        let allDrawingListStyles = null;

        // iterating over target, type and index
        _.each(_.keys(this.#placeHolderCollector), target => {
            _.each(_.keys(this.#placeHolderCollector[target]), type => {
                _.each(_.keys(this.#placeHolderCollector[target][type]), index => {

                    const drawingAttrs = this.#placeHolderCollector[target][type][index];

                    if (drawingAttrs.listStyle) {
                        allDrawingListStyles = allDrawingListStyles || {};
                        allDrawingListStyles[target] = allDrawingListStyles[target] || {};
                        allDrawingListStyles[target][type] = allDrawingListStyles[target][type] || {};
                        allDrawingListStyles[target][type][index] = allDrawingListStyles[target][type][index] || {};

                        allDrawingListStyles[target][type][index].listStyle = json.deepClone(drawingAttrs.listStyle);
                    }

                });
            });
        });

        return allDrawingListStyles;
    }

    /**
     * Setting the full object for the drawing list styles. This is necessary after applying a snapshot.
     *
     * Info: This function shall only be used for applying snapshot.
     *
     * @param {Object} listStyles
     *  The object containing the full set of drawing list styles. This was typically saved before using
     *  the function 'getAllDrawingListStyleAttributes'.
     */
    setAllDrawingListStyleAttributes(listStyles) {

        // iterating over target, type and index
        _.each(_.keys(listStyles), target => {
            _.each(_.keys(listStyles[target]), type => {
                _.each(_.keys(listStyles[target][type]), index => {

                    const drawingAttrs = listStyles[target][type][index];

                    if (drawingAttrs.listStyle) {
                        this.#placeHolderCollector = this.#placeHolderCollector || {};
                        this.#placeHolderCollector[target] = this.#placeHolderCollector[target] || {};
                        this.#placeHolderCollector[target][type] = this.#placeHolderCollector[target][type] || {};
                        this.#placeHolderCollector[target][type][index] = this.#placeHolderCollector[target][type][index] || {};

                        this.#placeHolderCollector[target][type][index].listStyle = json.deepClone(drawingAttrs.listStyle);
                    }
                });
            });
        });
    }

    /**
     * Emptying the complete place holder model. This is necessary, if a new snapshot is applied.
     */
    emptyPlaceHolderModel() {
        this.#emptyPlaceHolderModel();
    }

    /**
     * Getting the list styles for a specified drawing that are saved in the model. Or null, if the
     * specified drawing is no place holder drawing or if no list styles exist for this drawing.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node to be checked.
     *
     * @returns {Object|Null}
     *  The list styles for the specified drawing that are saved in the model. Or null, if the
     *  specified drawing is no place holder drawing or if no list styles exist for this drawing.
     */
    getAllListStylesAtPlaceHolderDrawing(drawing) {

        // an object containing target, type and index for the specified drawing
        let descriptor = null;
        // the drawing list style object
        let drawingListStyles = null;

        // getting target, type and index
        descriptor = getDrawingModelDescriptor(drawing);

        if (!descriptor) { return drawingListStyles; }  // this might not be a place holder drawing

        // the descriptor object must contain the keys 'target', 'type' and 'index' for the specified drawing

        if (this.#placeHolderCollector[descriptor.target] &&
            this.#placeHolderCollector[descriptor.target][descriptor.type] &&
            this.#placeHolderCollector[descriptor.target][descriptor.type][descriptor.index] &&
            this.#placeHolderCollector[descriptor.target][descriptor.type][descriptor.index].listStyle) {
            drawingListStyles = { listStyle: json.deepClone(this.#placeHolderCollector[descriptor.target][descriptor.type][descriptor.index].listStyle) };
        }

        return drawingListStyles;
    }

    /**
     * Getting the list styles for a specified drawing that are saved in the model (for place
     * holder drawings) or directly at the data attribute of the drawing (for non-place holder
     * drawings). If no list styles exist for this drawing, null is returned.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node to be checked.
     *
     * @returns {Object|Null}
     *  The list styles for the specified drawing that are saved in the model or in the data
     *  object of the drawing. If no list styles exist for this drawing, null is returned.
     */
    getAllListStylesAtDrawing(drawing) {

        // the drawing list style object
        let drawingListStyles = this.getAllListStylesAtPlaceHolderDrawing(drawing);

        if (!drawingListStyles) {
            // there might also be list styles attributes set at the drawing data object
            // for non placeholder drawings -> see function 'setDrawingListStyleAttributes'
            if ($(drawing).data('listStyle')) {
                drawingListStyles = { listStyle: json.deepClone($(drawing).data('listStyle')) };
            }
        }

        return drawingListStyles;
    }

    /**
     * Checking, whether a specified place holder type is allowed on a slide with a specified ID.
     * This function is especially used to find, if place holders of type 'dt', 'ftr' or 'sldNum'
     * are to be inserted on a slide or not. For all other place holder types, true is returned.
     *
     * @param {String} id
     *  The slide ID.
     *
     * @param {String} phType
     *  The place holder type.
     *
     * @param {Object} [attrs]
     *  The attributes of family 'slide'. If these attributes are not specified, the specified id
     *  is used to determine this attributes. For performance reasons, this attributes can be given
     *  into this function, if they are already available.
     *
     * @returns {Boolean}
     *  Whether a specified place holder type is allowed on a slide with a specified ID. This value
     *  can only be false for place holders of type 'dt', 'ftr' or 'sldNum'.
     */
    isSupportedPlaceHolderForSlideId(id, phType, attrs) {

        // whether the slide with the specified ID allows inserting of place holder drawings of the specified type
        let isSupported = true;
        // the merged slide attributes, to determine, which place holder drawings will be inserted
        const mergedSlideAttributes = attrs || this.docModel.getMergedSlideAttributesByFamily(id, 'slide');

        // The place holder drawings of type 'dt', 'ftr' and 'sldNum' are not always created
        // on the new slide. This is specified at the layout and/or master slide.
        if ((phType === 'dt' && !mergedSlideAttributes.isDate) ||
            (phType === 'ftr' && !mergedSlideAttributes.isFooter) ||
            (phType === 'sldNum' && !mergedSlideAttributes.isSlideNum)) {
            isSupported = false;
        }

        return isSupported;
    }

    /**
     * Helper function to find different and identical place holder drawings in two
     * specified layout slides.
     *
     * The return object contains three properties, that have an array as value. The
     * property names are 'onlyFirst', 'onlySecond' and 'both'.
     *
     * The values are arrays, that contain objects, that describe a place holder drawing
     * by its type and its index. Therefore these objects have the properties 'type'
     * and 'index'.
     *
     * So the properties 'onlyFirst', 'onlySecond' and 'both' describe the place holder
     * drawings that exist only on the first slide, only on the second slide or in both
     * slides.
     *
     * @param {String} slideId
     *  The ID of the slide, that gets a new layout assigned.
     *
     * @param {String} firstId
     *  The ID of the first layout slide.
     *
     * @param {String} secondId
     *  The ID of the second layout slide.
     *
     * @param {Object} firstSlideAttrs
     *  The attributes of 'slide' family for the first slide
     *
     * @param {Object} secondSlideAttrs
     *  The attributes of 'slide' family for the second slide
     *
     * @returns {Object}
     *  A comparison object with the properties 'onlyFirst', 'onlySecond' and 'both' that
     *  describe the place holder drawings that exist only on the first slide, only on the
     *  second slide or in both slides.
     */
    comparePlaceHolderSet(slideId, firstId, secondId, firstSlideAttrs, secondSlideAttrs) {

        // the comparison object with the properties 'onlyFirst', 'onlySecond' and 'both'
        const comparison = { onlyFirst: [], onlySecond: [], both: [] };
        // the saved place holder attributes for the first specified layout ID
        let first = json.deepClone(this.getAllPlaceHolderAttributesForId(firstId));
        // the saved place holder attributes for the second specified layout ID
        let second = json.deepClone(this.getAllPlaceHolderAttributesForId(secondId));

        // handling differentitation between text body and content body place holders
        first = this.#handleDifferentBodySet(first);
        second = this.#handleDifferentBodySet(second);

        // Checking, if the place holder drawings still do exist
        if (first) { this.#removeNonExistentPlaceHolderDrawings(slideId, first); }

        // Content body place holders can be converted to text body placeholders, if they already contain text (52805)
        if (first && second) { this.#checkContentBodyPlaceHolderDrawings(slideId, first, second); }

        _.each(_.keys(first), type => {
            if (this.isSupportedPlaceHolderForSlideId(firstId, type, firstSlideAttrs)) {
                _.each(_.keys(first[type]), index => {
                    if (second && second[type] && second[type][index] && this.isSupportedPlaceHolderForSlideId(secondId, type, secondSlideAttrs)) {
                        comparison.both.push({ type, index: parseInt(index, 10) });
                    } else {
                        comparison.onlyFirst.push({ type, index: parseInt(index, 10) });
                    }
                });
            }
        });

        _.each(_.keys(second), type => {
            if (this.isSupportedPlaceHolderForSlideId(secondId, type, secondSlideAttrs)) {
                _.each(_.keys(second[type]), index => {
                    if (!first || !first[type] || !first[type][index]) {
                        comparison.onlySecond.push({ type, index: parseInt(index, 10) });
                    }
                });
            }
        });

        // some magic conversion:
        // title <-> ctrTitle
        // body <-> subTitle

        return comparison;
    }

    /**
     * After a change of the slide layout it is possible that the new
     * layout contains some other types for the place holder drawings
     * than the old layout. Some types are converted into each other.
     * For example a place holder drawing of type 'title' can be
     * interpreted as 'ctrTitle' after changing the layout slide.
     *
     * The allowed conversions are listed in the constant:
     * PresentationDrawingStyles.ALLOWED_DRAWING_TYPE_SWITCHES.
     *
     * The parameter 'comparePlaceHolder' is an object created by the
     * function 'PresentationDrawingStyles.comparePlaceHolderSet'.
     * It has to contain the properties 'onlyFirst', 'onlySecond'
     * and 'both'.
     *
     * This parameter object is modified within this function.
     *
     * @param {Object} comparePlaceHolder
     *  The object that describe the differences in the place holder
     *  drawings of two layout slides. This object is generated by the
     *  function 'PresentationDrawingStyles.comparePlaceHolderSet' and
     *  contains the properties 'onlyFirst', 'onlySecond' and 'both'.
     *  This object is modified within this function.
     *
     * @returns {Object[]}
     *  An array of objects. The objects inside this array describe
     *  the allowed switches of drawing place holder. Every object
     *  contains the properties 'from' and 'to'. The values are
     *  the place holder drawing info objects, that contain the
     *  type and index for a place holder drawing.
     */
    analyzeComparePlaceHolder(comparePlaceHolder) {

        // an array of supported switches for the place holder drawings
        const switches = [];

        // helper function to find the specified type in the collector
        const foundType = (type, allPlaceHolders) => {
            return _.find(allPlaceHolders, placeHolder => {
                return placeHolder.type === type;
            });
        };

        if (comparePlaceHolder && comparePlaceHolder.onlyFirst.length > 0 && comparePlaceHolder.onlySecond.length > 0) {

            _.each(PresentationDrawingStyles.ALLOWED_DRAWING_TYPE_SWITCHES, (value, key) => {

                if (!this.#odf) {
                    if (value === 'body') { value = CONTENTBODY; }
                    if (key === 'body') { key = CONTENTBODY; }
                }

                const first = foundType(key, comparePlaceHolder.onlyFirst);
                const second = foundType(value, comparePlaceHolder.onlySecond);

                // searching the key in onlyFirst and the value in onlySecond
                if (first && second) {

                    // adding to switches object
                    switches.push({ from: first, to: second });

                    // removing items from collectors
                    comparePlaceHolder.onlyFirst = _.without(comparePlaceHolder.onlyFirst, first);
                    comparePlaceHolder.onlySecond = _.without(comparePlaceHolder.onlySecond, second);
                }
            });
        }

        return switches;
    }

    /**
     * Collecting all place holder drawings on the specified slide, that have no definition in the
     * layout slide. This can happen after switching the layout slide. It is possible, that the new
     * layout slide has less place holder drawing definitions.
     *
     * @param {String} id
     *  The ID of the specified slide.
     *
     * @returns {jQuery}
     *  A jQuery collection of all place holder drawings, that have no definition in the layout slide.
     *  This can be drawings of any type, not only of type 'body'.
     */
    getAllUnassignedPhDrawings(id) {
        return this.getAllPlaceHolderDrawingsOnSlideByIndex(id, this.getMsDefaultPlaceHolderIndex());
    }

    /**
     * Helper function for updating all place holder drawings on a
     * specified slide.
     *
     * @param {Node|jQuery} slide
     *  The (jQueryfied) slide node.
     */
    updatePlaceHolderDrawings(slide) {

        // simply update all drawings that are place holder types
        _.each($(slide).children('div[data-placeholdertype]'), drawing => {

            // updating the drawing itself
            this.updateElementFormatting($(drawing));

            // ... and updating the paragraphs
            _.each($(drawing).find(PARAGRAPH_NODE_SELECTOR), para => {
                // updating all paragraphs, that have a modified attribute set
                this.docModel.paragraphStyles.updateElementFormatting($(para));
                this.docModel.updateListsDebounced($(para));
            });

        });

    }

    /**
     * Helper function for updating all drawings on a specified slide.
     *
     * @param {Node|jQuery} slide
     *  The (jQueryfied) slide node.
     */
    updateAllDrawingsOnSlide(slide) {

        // simply update all drawings that are place holder types
        _.each($(slide).children('div.drawing'), drawing => {

            // updating the drawing itself
            this.updateElementFormatting($(drawing));

            // ... and updating the paragraphs
            _.each($(drawing).find(PARAGRAPH_NODE_SELECTOR), para => {
                // updating all paragraphs, that have a modified attribute set
                this.docModel.paragraphStyles.updateElementFormatting($(para));
                this.docModel.updateListsDebounced($(para));
            });

        });

    }

    /**
     * Updating the template text in place holder drawing that cannot contain text content. This
     * are the place holder drawings of type 'tbl', 'pic', ... . The text of these place holder
     * drawings cannot be modified in a document slide.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node.
     *
     * @param {String} type
     *  The type of the place holder drawing.
     *
     * @param {Number|Null} index
     *  The index of the place holder drawing.
     *
     * @param {String} slideId
     *  The ID of the slide that contains the drawing.
     */
    updateTemplateReadOnlyText(drawing, type, index, slideId) {
        this.#addTemplateReadOnlyText(drawing, type, index, slideId, { update: true });
    }

    /**
     * Handling the visualization of template buttons for inserting image or table.
     * This template buttons are visible in empty place holder drawings of type
     * 'body'.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains.
     *
     * @param {Boolean} addImages
     *  Whether the template images shall be inserted or removed.
     *
     * @param {String} [slideId]
     *  The ID of the slide containing the drawing. If not specified, it is
     *  determined within this function.
     */
    handleImageTemplateContainer(drawing, addImages, slideId) {

        // the container node for the template images
        let imageContainerNode = null;
        // the explicit attributes of the drawing
        const expDrawingAttrs = getExplicitAttributeSet(drawing);
        // the place holder type
        let placeHolderType = null;
        // the place holder index
        let placeHolderIndex = null;

        // helper function to get the image container node in a drawing
        const setImageContainerNode = () => {
            imageContainerNode = $(drawing).children(PresentationDrawingStyles.TEMPLATE_DRAWING_CONTAINER_SELECTOR);
        };

        // handling place holder drawings
        if (isPlaceHolderAttributeSet(expDrawingAttrs)) {

            placeHolderType = expDrawingAttrs.presentation.phType || '';

            if (!slideId) { slideId = this.docModel.getSlideId($(drawing).parent()); }

            if (this.docModel.isLayoutOrMasterId(slideId)) {

                if (this.docModel.isLayoutSlideId(slideId)) { // not inserting for master slides

                    setImageContainerNode(); // in the master view the icons are always visible (and have no effect)

                    if (imageContainerNode.length === 0) {
                        this.#insertImageTemplateContainer(drawing, placeHolderType, placeHolderIndex, slideId);
                    } else if (imageContainerNode.length === 1) {
                        // maybe a positioning update is required (after changing the layout)
                        this.#updateImageContainerNode(drawing, imageContainerNode);
                    }
                }

            } else {
                if (addImages) {

                    setImageContainerNode();

                    if (imageContainerNode.length === 0) {
                        placeHolderIndex = _.isNumber(expDrawingAttrs.presentation.phIndex) ? expDrawingAttrs.presentation.phIndex : null;
                        this.#insertImageTemplateContainer(drawing, placeHolderType, placeHolderIndex, slideId);
                    } else if (imageContainerNode.length === 1) {
                        // maybe a positioning update is required (after changing the layout)
                        this.#updateImageContainerNode(drawing, imageContainerNode);
                    }
                } else {
                    this.#removeImageTemplateContainer(drawing);
                }
            }
        }
    }

    /**
     * Handling the visualization of empty text frames for a complete slide.
     *
     * @param {Node|jQuery} slide
     *  The slide node for that the visualization is done for all empty
     *  text frames.
     *
     * @param {Object} [options]
     *  Some additional options:
     *  @param {Boolean} [options.ignoreTemplateText=false]
     *      This option is handled within DOM.isEmptyTextFrame to define, that text frames
     *      that contain template text are also empty.
     */
    handleAllEmptyPlaceHolderDrawingsOnSlide(slide, options) {
        _.each($(slide).children('div.drawing'), drawing => {
            this.handleEmptyPlaceHolderDrawing(drawing, null, options);
        });
    }

    /**
     * Handling the visualization of empty text frames. In the standard view it is
     * necessary to assign a dotted line to these empty text frames and to insert
     * a template text.
     * In the layout or master view, such a dotted line is always assigned to the place
     * holder drawings.
     * In PowerPoint only place holder drawings get the dotted border (even if
     * they have a background color). Empty text frames do not get a border.
     *
     * @param {Node|jQuery} drawing
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains.
     *
     * @param {Object} [attributes]
     *  The attributes of the drawing node.
     *
     * @param {Object} [options]
     *  Some additional options:
     *  @param {Boolean} [options.ignoreTemplateText=false]
     *      This option is handled within DOM.isEmptyTextFrame to define, that text frames
     *      that contain template text are also empty.
     *  @param {Boolean} [options.remoteUpdate=false]
     *      This option is set, if this function call is triggered by an external operation.
     *
     * @returns {Boolean}
     *  Whether text was inserted into the specified drawing.
     */
    handleEmptyPlaceHolderDrawing(drawing, attributes, options) {

        // whether the default template text was inserted into the drawing
        let addedText = false;
        // the id of the slide containing the specified drawing
        let slideId = null;
        // whether the slide is a master or layout slide
        let isLayoutOrMasterSlide = false;
        // whether the drawing is an empty place holder in which no text can be inserted (table, image, ...)
        let isEmptyNoTextPlaceHolder = false;
        // the selection object
        let selection = null;

        if (isPlaceHolderDrawing(drawing)) {
            if (this.#odf) { this.#handleMissingParagraphInPlaceHolderDrawing(drawing); }
            slideId = this.docModel.getSlideId($(drawing).parent());
            isLayoutOrMasterSlide = this.docModel.isLayoutOrMasterId(slideId);
            isEmptyNoTextPlaceHolder = !isLayoutOrMasterSlide && isPlaceHolderWithoutTextDrawing(drawing) && isEmptyPlaceHolderDrawing(drawing);
            this.#handleBorderForEmptyTextFrames(drawing, isEmptyNoTextPlaceHolder, isLayoutOrMasterSlide, attributes);
            if (!isEmptyNoTextPlaceHolder) { addedText = this.#handleTemplateText(drawing, options ? _.extend(options, { id: slideId }) : { id: slideId }); }
            this.handleImageTemplateContainer(drawing, (isLayoutOrMasterSlide ? true : (isEmptyNoTextPlaceHolder || isEmptyTextframe(drawing, { ignoreTemplateText: true }))), slideId); // handling the template buttons for inserting image or table

            if (addedText && isLayoutOrMasterSlide) {
                if (slideId && !this.docModel.getSlideFormatManager().isUnformattedSlide(slideId)) {
                    this.docModel.forceTriggerOfSlideStateUpdateEvent(slideId);
                }
                if (isPlaceHolderWithoutTextDrawing(drawing)) { // trigger an event, if text was added inside drawings that do not contain text (picture, chart, ...)
                    this.docModel.forceTriggerOfTemplateTextUpdatedEvent(slideId, drawing);
                }
            }

            // setting selection again, if it is currently a text selection inside the empty place holder drawing
            if (addedText && !getBooleanOption(options, 'remoteUpdate', false)) {
                selection = this.docModel.getSelection();
                if (selection.isAdditionalTextframeSelection() && getDomNode(drawing) === getDomNode(selection.getSelectedTextFrameDrawing())) {
                    selection.setTextSelection(selection.getStartPosition(), selection.getEndPosition());
                }
            }

        }

        return addedText;
    }

    /**
     * Collecting from a specified slide all place holder drawings with the specified
     * type and index. If the index is not specified, all drawings with the specified
     * type will be returned.
     *
     * @param {String} id
     *  The ID of the slide.
     *
     * @param {String|String[]} type
     *  The type(s) of the drawing. It is possible to determine several types. This is a
     *  convenience function, so that subtitle can be changed, too, if a body was modified.
     *  In this case the parameter 'index' will be ignored.
     *
     * @param {String|Number} [index]
     *  The index of the drawing. If not specified, all drawings with the specified type
     *  with any index will be collected.
     *
     * @returns {jQuery}
     *  A collection of all place holder drawings, that are direct children of
     *  the slide.
     */
    getAllPlaceHolderDrawingsOnSlideBySelector(id, type, index) {

        // the slide node
        const slide = this.docModel.getSlideById(id);
        // the selector specific to the place holder type and index. This is used to find the drawing
        // that need an update of formatting (the model is already updated).
        let selector = '';
        // whether the index will be ignored
        let ignoreIndexValue = false;

        if (!slide) { return $(); }

        if (type === CONTENTBODY) { type = 'body'; }

        if (_.isArray(type)) {
            _.each(type, oneType => {
                if (oneType === CONTENTBODY) { oneType = 'body'; }
                if (selector) { selector += ', '; }
                selector += 'div[data-placeholdertype=' + oneType + ']';
                if (oneType === 'body') { selector += '[data-placeholderindex], div[data-placeholderindex]:not([data-placeholdertype])'; }
            });
        } else {

            ignoreIndexValue = (!_.isNumber(index) && !_.isString(index));  // check if the index is specified
            selector = 'div[data-placeholdertype=' + type + ']';

            // if type is body, the 'placeholderindex' must be correct, but 'placeholdertype' might not be set
            if (type === 'body') {
                if (ignoreIndexValue) {
                    selector += '[data-placeholderindex], div[data-placeholderindex]:not([data-placeholdertype])';
                } else {
                    selector += '[data-placeholderindex=' + index + '], div[data-placeholderindex=' + index + ']:not([data-placeholdertype])';
                }
            } else if (!ignoreIndexValue) {
                selector += '[data-placeholderindex=' + index + ']';
            }
        }

        return slide.children(selector);
    }

    /**
     * Collecting from a specified slide all place holder drawing of specified
     * type and index.
     *
     * @param {String} id
     *  The ID of the slide.
     *
     * @param {String|Number} index
     *  The index of the drawing.
     *
     * @returns {jQuery}
     *  A collection of the place holder drawings with the specified index. All those
     *  drawings are direct children of the slide. This function can be used to find
     *  all unassigned place holder drawings with index
     *  'PresentationDrawingStyles.MS_DEFAULT_PLACEHOLDER_INDEX'.
     *  This drawings can be drawings of any type, not only of type 'body'.
     */
    getAllPlaceHolderDrawingsOnSlideByIndex(id, index) {

        // the slide node
        const slide = this.docModel.getSlideById(id);
        // the selector specific to the place holder index.
        const selector = 'div[data-placeholderindex=' + index + ']';

        return slide.children(selector);
    }

    /**
     * Collecting from a specified slide all place holder drawings.
     *
     * @param {String} id
     *  The ID of the slide.
     *
     * @returns {jQuery}
     *  A collection of the place holder drawings.
     */
    getAllPlaceHolderDrawingsOnSlide(id) {

        // the slide node
        const slide = this.docModel.getSlideById(id);
        // the selector specific to the place holder index.
        const selector = 'div[data-placeholderindex]';

        return slide.children(selector);
    }

    /**
     * Getter for the list of allowed drawing type switches during change of layout.
     *
     * @returns {Object}
     *  The object that contains all allowed drawing type switches during a change
     *  of the layout of a slide.
     */
    getListOfAllowedDrwawingTypeSwitches() {
        return PresentationDrawingStyles.ALLOWED_DRAWING_TYPE_SWITCHES;
    }

    /**
     * Getter for the constant value of 'PresentationDrawingStyles.MS_DEFAULT_PLACEHOLDER_INDEX'.
     *
     * @returns {Number}
     *  The default place holder index that is used in PP for place holder drawings
     *  that cannot be assigned to place holder drawing of current layout slide.
     */
    getMsDefaultPlaceHolderIndex() {
        return PresentationDrawingStyles.MS_DEFAULT_PLACEHOLDER_INDEX;
    }

    /**
     * Getter for the master slide type converter.
     *
     * @returns {Object}
     *  The object describing the master slide type conversions.
     */
    getMasterSlideTypeConverter() {
        return json.flatClone(PresentationDrawingStyles.MASTER_PLACEHOLDER_TYPE_SWITCHES);
    }

    /**
     * Setting list style attributes at the drawing node for non-place holder drawings (45279).
     * For place holder drawings there is a model, but it is also possible, that the
     * list styles are directly defined at a non-placeholder drawing node. In this case,
     * it is necessary to save this data directly at the node.
     *
     * Info: Using this data object must be an exception. It is only valid for non
     *       place holder drawings with directly set list styles. For all other drawings
     *       the model in the object 'placeHolderCollector' must be used.
     *
     * @param {jQuery|Node} drawingNode
     *  The drawing node.
     *
     * @param {Object} attrs
     *  The attributes object.
     */
    setDrawingListStyleAttributes(drawingNode, attrs) {
        if (attrs && attrs.listStyle && !isPlaceHolderDrawing(drawingNode)) {
            $(drawingNode).data('listStyle', attrs.listStyle);
        }
    }

    /**
     * Getting the list style attributes directly from the drawing node for non-place holder
     * drawings (45279). For place holder drawings there is a model, but it is also possible,
     * that the list styles are directly defined at a non placeholder drawing node. In this case,
     * it is necessary to read this data directly from the node.
     *
     * @param {jQuery|Node} drawingNode
     *  The drawing node.
     */
    getDrawingListStyleAttributes(drawingNode) {
        return $(drawingNode).data('listStyle');
    }

    /**
     * Whether this drawing cannot be successfully formatted without being visible in the DOM.
     * This is especially true for drawings with the shape attributes autoResizeHeight set to true
     * and non horizontal text alignment (DOCS-1751). Maybe this list must be expanded in the future.
     *
     * @param {Object} attributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family, containing the
     *  effective attribute values merged from style sheets and explicit attributes.
     */
    requiresFormatVisibility(attributes) {
        return attributes.shape && attributes.shape.autoResizeHeight && attributes.shape.vert !== 'horz';
    }

    /**
     * Public function for the private drawing formatting function 'updateDrawingFormatting'.
     *
     * @param {jQuery} drawing
     *  The drawing node whose attributes have been changed, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family, containing the
     *  effective attribute values merged from style sheets and explicit attributes.
     *
     * @param {Object} [options]
     *  Optional parameters that are evaluated by 'DrawingFrame.updateFormatting'.
     */
    updateDrawingFormatting(drawing, mergedAttributes, options) {
        return this.#updateDrawingFormatting(drawing, mergedAttributes, options);
    }

    // private methods ----------------------------------------------------

    /**
     * Setting the width of the drawing.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} attributes
     *  A map with the merged drawing attributes, as name/value pairs.
     */
    #setDrawingWidth(drawing, attributes) {

        if (!isGroupedDrawingFrame(drawing)) {
            const width = attributes.drawing.width;

            // bugfix :: BUG#45141 :: https://bugs.open-xchange.com/show_bug.cgi?id=45141 :: Vertical drawing object is shown diagonally
            if (_.isString(width)) {
                const value = parseInt(width, 10);
                const tuple = width.split(value);
                const unit = (tuple.length === 2) ? $.trim(tuple[1]) : '';

                if (unit === '%') {
                    drawing.width([value, unit].join(''));
                }/* else if (unit !== '') {
                    drawing.width(Utils.convertLength(value, unit, 'px', 1));
                    }*/
            } else {
                drawing.width(convertHmmToLength(width, 'px', 1));
            }
        }
    }

    /**
     * Setting the height of the drawing. This needs to be done, because it might be
     * dependent from other attribute values.
     *
     * Info: Height must be set to drawing with autoResizeHeight set to true, if the
     *       text is vertically oriented inside the drawing (55562)
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} attributes
     *  A map with the merged drawing attributes, as name/value pairs.
     */
    #setDrawingHeight(drawing, attributes) {

        let drawingHeight = 0;

        if (!(isGroupedDrawingFrame(drawing) || isTableDrawingFrame(drawing) || isAutoResizeHeightDrawingFrameWithHorizontalText(drawing, attributes.shape))) {
            drawingHeight = Math.max(convertHmmToLength(attributes.drawing.height, 'px', 1), 1);  // at least 1px, even for lines and connectors
            drawing.height(drawingHeight);
        } else if (isTableDrawingFrame(drawing) || isAutoResizeHeightDrawingFrameWithHorizontalText(drawing, attributes.shape)) {
            // -> no setting of height for grouped drawings, tables and for shapes with property 'autoResizeHeight'
            drawing.height(''); // if 'autoResizeHeight' is set, an explicit height must be removed
        }
    }

    /**
     * Helper function to update attributes of children of drawings. This is only used for text frames yet.
     *
     * @param {jQuery|Node} drawing
     *  The drawing node as jQuery object, whose children need an update of element formatting.
     *
     * @param {Object} mergedAttributes
     *  A map with the merged drawing attributes, as name/value pairs.
     */
    #handleChildAttributes(drawing, mergedAttributes) {

        // the explicit drawing attributes
        const explicitDrawingAttrs = getExplicitAttributeSet(drawing);
        // the container for the paragraphs and tables inside the drawing
        let textFrameNode = null;

        // check, if the empty text frame needs an artificial border and a default text
        this.handleEmptyPlaceHolderDrawing(drawing, mergedAttributes);

        // checking the fill color (task 36385)
        if (explicitDrawingAttrs && (explicitDrawingAttrs.fill || explicitDrawingAttrs.character || explicitDrawingAttrs.paragraph)) {
            textFrameNode = getTextFrameNode(drawing);

            if (textFrameNode.length > 0) {
                iterateSelectedDescendantNodes(textFrameNode, PARAGRAPH_NODE_SELECTOR, paragraph => {
                    this.docModel.paragraphStyles.updateElementFormatting(paragraph);
                }, undefined, { children: true });
            }
        }
    }

    /**
     * Update the formatting of the table node inside the specified drawing.
     *
     * @param {jQuery|Node} drawing
     *  The drawing node as jQuery object, whose table needs an update of formatting.
     */
    #handleTableAttributes(drawing) {

        const tableNode = $(drawing).children('.content:not(.copy)').find('table');

        if (tableNode.length > 0) {
            this.docModel.tableStyles.updateElementFormatting(tableNode);
        }
    }

    /**
     * Checking the existence of place holder drawings on a specified slide.
     *
     * @param {String} slideId
     *  The ID of the slide, on that the drawing nodes will be searched.
     *
     * @param {Object} placeHolderSet
     *  An object containing the saved place holder attributes for the specified slide.
     *  This object might be modified within this function.
     */
    #removeNonExistentPlaceHolderDrawings(slideId, placeHolderSet) {

        const ignoreList = ['dt', 'ftr', 'sldNum']; // drawing types to be ignored

        _.each(_.keys(placeHolderSet), type => {
            if (!_.contains(ignoreList, type)) {
                _.each(_.keys(placeHolderSet[type]), index => {
                    const drawings = this.getAllPlaceHolderDrawingsOnSlideBySelector(slideId, type, index);
                    if (drawings.length === 0) {
                        delete placeHolderSet[type][index];
                        if (_.keys(placeHolderSet[type]).length === 0) { delete placeHolderSet[type]; }
                    }
                });
            }
        });
    }

    /**
     * If the layout of a slide is changed, it is allowed, that a content place holder drawing (that
     * allows input of text, table, picture, ...) that already contains text, is replaced by a text
     * body place holder (52805).
     *
     * @param {String} slideId
     *  The ID of the slide, on that the drawing nodes will be searched.
     *
     * @param {Object} set1
     *  An object containing the saved place holder attributes for the specified slide.
     *  This object might be modified within this function.
     *
     * @param {Object} set2
     *  An object containing the place holder attributes for the new assigned slide layout.
     */
    #checkContentBodyPlaceHolderDrawings(slideId, set1, set2) {

        // a container with all keys of set1.contentbody
        let contentbodyKeys = null;
        // the length of the contentbodyKeys object
        let numberOfContentBodies = 0;
        // the number of 'body' place holder drawings in set2
        let numberOfBodies = 0;
        // the maximum possible number of conversions in this function
        let conversionMax = 0;
        // the number of already converted drawings
        let convertedDrawings = 0;
        // the number of already checked drawings
        let checkedDrawings = 0;

        // modifying the 'set1' from 'contentbody' to 'body' if possible
        const checkBodyDrawing = index => {

            // converting only content bodies, that are filled with text
            const oneDrawing = this.getAllPlaceHolderDrawingsOnSlideBySelector(slideId, 'body', index);

            if (oneDrawing.length === 1) {
                // check, if the drawing contains text and is not empty and is no table
                if (isTextFrameShapeDrawingFrame(oneDrawing) && !isEmptyTextframe(oneDrawing, { ignoreTemplateText: true }) && !isTableDrawingFrame(oneDrawing)) {
                    set1.body = set1.body || {};
                    set1.body[index] = set1.contentbody[index];
                    delete set1.contentbody[index];
                    if (_.isEmpty(set1.contentbody)) { delete set1.contentbody; }
                    convertedDrawings++;
                }
            }
        };

        if (set1.contentbody && !set1.body && !set2.contentbody && set2.body) {
            // 'contentbody' can be handled like (text) 'body', if it already contains text content
            contentbodyKeys = _.keys(set1.contentbody);
            numberOfContentBodies = contentbodyKeys.length;
            numberOfBodies = _.keys(set2.body).length;
            conversionMax = Math.min(numberOfContentBodies, numberOfBodies);

            while ((checkedDrawings < numberOfContentBodies) && (convertedDrawings < conversionMax)) {
                _.each(contentbodyKeys, checkBodyDrawing);
                checkedDrawings++;
            }
        }

    }

    /**
     * Marks drawing objects that are placeholders with specific element
     * attributes.
     *
     * @param {jQuery} drawing
     *  The drawing node, as jQuery object.
     */
    #assignPlaceholderAttributes(drawing) {

        // the explicit attributes at the specified element
        // for place holder type and place holder index only the explicit attributes
        // must be evaluated. Otherwise every drawing will become a place holder drawing,
        // because the merge attributes contain the default values.
        // -> marking presentation placeholder drawings
        const elemAttrs = getExplicitAttributeSet(drawing, true);

        const keySet = isPlaceHolderAttributeSet(elemAttrs) ? getValidTypeIndexSet(elemAttrs) : null;

        drawing.attr({
            'data-placeholdertype': keySet ? keySet.type : null,
            'data-placeholderindex': keySet ? keySet.index : null
        });
    }

    /**
     * Handling drawing that are absolute positioned. This are all drawings that are not in-line
     * with text. This is especially important, if the drawing anchor was modified. In this case
     * some clean up at the node is required.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map with the merged drawing attributes, as name/value pairs.
     */
    #assignDrawingAttributes(drawing, mergedAttributes) {

        // the drawing attributes of the passed attribute map
        const drawingAttrs = mergedAttributes.drawing;

        // drawing is absolutely positioned
        drawing.addClass('absolute');

        // for place holder type and place holder index only the explicit attributes
        // must be evaluated. Otherwise every drawing will become a place holder drawing,
        // because the merge attributes contain the default values.
        // -> marking presentation placeholder drawings
        this.#assignPlaceholderAttributes(drawing);

        // apply CSS formatting to drawing node
        drawing.css({
            left: convertHmmToLength(drawingAttrs.left, 'px', 1),
            top: convertHmmToLength(drawingAttrs.top, 'px', 1)
        });

        // special behavior for special hidden drawings in PP (55595, 55741)
        // -> not toggling 'display' directly, because 'display: block' at the drawing node might overwrite layout and master slide visibility
        if (!this.#odf) { drawing.toggleClass('forcehiddenfile', drawingAttrs.left === 0 && drawingAttrs.top === 0 && drawingAttrs.width === 0 && drawingAttrs.height === 0); }
    }

    /**
     * Adding attributes of master and layout slide into the merge attributes. This has to happen
     * before the explicit attributes are set into the specified attribute set.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     */
    #resolveLayoutAttributes(drawing) {

        // the attribute set of the place holder elements in layout and/or master slide
        let placeHolderAttributes = null;
        // the explicit attributes at the specified element
        const elemAttrs = getExplicitAttributeSet(drawing);
        // the IDs of document slide, layout slide and master slide
        let id = null;
        let layoutId = null;
        let masterId = null;
        // an object containing the place holder type and index as property
        let keySet = null;

        if (isPlaceHolderAttributeSet(elemAttrs)) {

            id = getTargetContainerId(drawing.parent()); // checking only direct children of slide
            keySet = getValidTypeIndexSet(elemAttrs);

            if (keySet) {
                if (this.docModel.isStandardSlideId(id)) {
                    layoutId = this.docModel.getLayoutSlideId(id);
                    masterId = this.docModel.getMasterSlideId(layoutId);
                    placeHolderAttributes = this.#getMergedPlaceHolderAttributes(layoutId, masterId, keySet.type, keySet.index);

                } else if (this.docModel.isLayoutSlideId(id)) {
                    masterId = this.docModel.getMasterSlideId(id);
                    placeHolderAttributes = this.#getMergedPlaceHolderAttributes(null, masterId, keySet.type, keySet.index);
                }
            }
        }

        return placeHolderAttributes;
    }

    /**
     * Will be called for every drawing node whose attributes have been changed. Repositions
     * and reformats the drawing according to the passed attributes.
     *
     * @param {jQuery} drawing
     *  The drawing node whose attributes have been changed, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family, containing the
     *  effective attribute values merged from style sheets and explicit attributes.
     *
     * @param {Object} [options]
     *  Optional parameters that are evaluated by 'DrawingFrame.updateFormatting'.
     */
    #updateDrawingFormatting(drawing, mergedAttributes, options) {

        // No formatting is done during loading the document. But in Presentation, invisible slides might be
        // formatted much later (or not at all). But some minimum formatting might be necessary, to work with
        // unformatted slides. This is for example the 'type' and the 'index' of place holder drawings. Only
        // if this is set correctly, the selectors that are used to find drawings on slides, work reliable.
        if (!this.isImportFinished) {
            this.#assignPlaceholderAttributes(drawing);
            return;
        }

        // the attributes of the generic 'drawing' family
        const drawingAttrs = mergedAttributes.drawing;

        // update CSS class for drawings that are not groupable
        drawing.toggleClass(NO_GROUP_CLASS, drawingAttrs.noGroup);

        // rotation
        const degrees = drawingAttrs.rotation || 0;
        updateCssTransform(drawing, degrees);
        drawing.toggleClass(ROTATED_DRAWING_CLASSNAME, degrees !== 0);

        // nothing to do, if paragraph is a group content object
        // -> in this case the group is responsible for the correct attributes
        if (isGroupContentNode(drawing.parent())) {
            drawing.addClass('absolute grouped');  // absolute positioned drawing inside its group!
            // do NOT set generic formatting to the drawing frame -> this is handled by the drawing group node!
            // -> avoid to call: DrawingFrame.updateFormatting(this.docModel.getApp(), drawing, mergedAttributes); (Performance of groups!)
            // The slideFormatManager takes care of order of grouped drawings (-> the group node is formatted as last node)
            // So if this formatting is not triggered by slideFormatManager, it is necessary that the group is formatted after the grouped node (55849)
            // -> but this should happen only once for all grouped drawings inside a group -> debouncing the call
            if (!drawing.hasClass('slideformat')) { this.docModel.updateDrawingsDebounced(getGroupNode(drawing)); }

            // Grouped images have to be formatted completely, at least once after loading document (with operations)
            // or after pasting a group that contains image drawings or duplicating a slide (56742).
            // In drawingFrame.js 'updateImageAttributesInGroup' is called instead of 'updateImageFormatting' for
            // grouped drawings and this 'short' version does not handle all image properties.
            // The formatting of all other drawings has to be started by their group node and not here for performance
            // reasons.
            // Grouped tables (ODP only) also need to be formatted once completely (58537).
            if (!drawing.hasClass('formatrequired')) {
                return; // Performance: This should be the default (only grouped images must not return here)
            }
        }

        // some drawings can only be formatted successfully, if they are visible in the DOM
        let changedVisibility = false;
        const forceVisibility = this.requiresFormatVisibility(mergedAttributes);
        if (forceVisibility) { changedVisibility = this.docModel.handleContainerVisibility(drawing, { makeVisible: true }); }

        // setting the drawing height
        this.#setDrawingHeight(drawing, mergedAttributes);

        // setting the drawing width
        this.#setDrawingWidth(drawing, mergedAttributes);

        // setting the drawing attributes except width and height (those are already set)
        this.#assignDrawingAttributes(drawing, mergedAttributes);

        // set generic formatting to the drawing frame
        updateFormatting(this.docModel.getApp(), drawing, mergedAttributes, options);

        // removed marker class for required formatting after formatting is done
        drawing.removeClass('formatrequired');

        // update formatting of all paragraphs inside the text frame content node (36385)
        if (isTableDrawingFrame(drawing)) {
            this.#handleTableAttributes(drawing);
        } else if (isTextFrameShapeDrawingFrame(drawing) || (isPlaceHolderWithoutTextDrawing(drawing) && isEmptyPlaceHolderDrawing(drawing))) {
            // update formatting of all paragraphs inside the text frame content node (36385)
            this.#handleChildAttributes(drawing, mergedAttributes);
        } else if (this.#odf && isShapeDrawingFrame(drawing) && !isPlaceHolderWithoutTextAttributeSet(mergedAttributes)) {
            this.#handleChildAttributes(drawing, mergedAttributes);
        }

        if (changedVisibility) { this.docModel.handleContainerVisibility(drawing, { makeVisible: false }); }
    }

    /**
     * Saving the specified drawing attributes in the model container.
     *
     * This function can be called directly from insertDrawing operation. In that
     * case the attributes already contain the full set of properties from the
     * families 'drawing' and 'presentation'.
     *
     * @param {String} target
     *  The id of the layout or master slide, that defines the attributes for
     *  the presentation place holder type.
     *
     * @param {String} type
     *  The place holder type.
     *
     * @param {Number} index
     *  The place holder index.
     *
     * @param {Object} attrs
     *  The attributes object, that contains the attributes for the specified
     *  target and place holder type. This are the explicit attributes set at
     *  the drawing.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.saveOldAttrs=false]
     *      Whether the old attributes shall be saved and returned in the return object.
     *
     * @returns {Object|Null}
     *  If the attributes are registered, an object with the complete registration
     *  information is returned. The object's keys are: 'target', 'placeHolderType',
     *  'placeHolderIndex', 'attrs' and 'oldAttrs'. If the attributes were not registered,
     *  because target, type or index are not defined, null is returned.
     */
    #savePlaceHolderAttributes(target, type, index, attrs, options) {

        // whether list style handling is required
        const listStyle = attrs && attrs.listStyle;
        // the level specified in the list styles
        let level = null;
        // a helper object for merging list style attributes
        let drawingAttrs = null;
        // the old place holder attributes
        let oldAttrs = null;
        // whether an update of dependent slide shall be triggered immediately
        const saveOldAttrs = getBooleanOption(options, 'saveOldAttrs', false);

        if (!(target && type && _.isNumber(index))) { return null; }

        this.#placeHolderCollector[target] = this.#placeHolderCollector[target] || {};
        this.#placeHolderCollector[target][type] = this.#placeHolderCollector[target][type] || {};
        this.#placeHolderCollector[target][type][index] = this.#placeHolderCollector[target][type][index] || {};

        // optionally saving a deep copy of the old value in the return object for later usage
        if (saveOldAttrs) { oldAttrs = json.deepClone(this.#placeHolderCollector[target][type][index]); }

        // merging the attributes, that are not list styles ('autoclear' is required for undo, so that attributes can be removed)
        this.extendAttrSet(this.#placeHolderCollector[target][type][index], attrs, { autoClear: true });

        // special handling for list style attribute at drawings
        if (listStyle) {
            for (level in listStyle) {
                drawingAttrs = this.#placeHolderCollector[target][type][index];
                drawingAttrs.listStyle = drawingAttrs.listStyle || {};
                drawingAttrs.listStyle[level] = drawingAttrs.listStyle[level] || {};
                this.extendAttrSet(drawingAttrs.listStyle[level], listStyle[level], { autoClear: true });
            }
        }

        // returning an object with the complete registration information
        return { target, placeHolderType: type, placeHolderIndex: index, attrs, oldAttrs };
    }

    /**
     * Getting the merged place holder attributes from master slide and layout slide.
     *
     * @param {String} [layoutId]
     *  The id of the layout slide.
     *
     * @param {String} [masterId]
     *  The id of the master slide.
     *
     * @param {String} placeHolderType
     *  The drawing place holder type.
     *
     * @param {Number} placeHolderIndex
     *  The drawing place holder index.
     *
     * @returns {Object|Null}
     *  An object containing the attributes from the layout slide and the master slide. If attributes are defined
     *  at both slide, the layout slide overwrites the values from the master slide.
     */
    #getMergedPlaceHolderAttributes(layoutId, masterId, placeHolderType, placeHolderIndex) {

        // the layout slide attributes
        const layoutAttrs = layoutId ? this.getPlaceHolderAttributes(layoutId, placeHolderType, placeHolderIndex) : null;
        // the master slide attributes
        const masterAttrs = masterId ? this.getPlaceHolderAttributes(masterId, placeHolderType, placeHolderIndex) : null;
        // the attributes of layout and master slide
        let attributes = null;

        // merging the collected attributes from master and layout slides and drawings (never overwrite attribute set)
        if (masterAttrs) { attributes = this.extendAttrSet({}, masterAttrs); }
        if (layoutAttrs) { attributes = this.extendAttrSet(attributes || {}, layoutAttrs); }

        return attributes;
    }

    /**
     * Handler function for the event 'removed:slide'. This event is triggered from the model
     * after a slide is removed. Within this function the place holder information specified
     * for the master or layout slides will be removed, if a master or layout slide was
     * removed.
     *
     * @param {Object} eventOptions
     *  The information sent, if a 'removed:slide' event is triggered by the model. This object
     *  contains the ID of the removed slide, whether the master view is active and whether the
     *  removed slide is a master or layout slide.
     */
    #removeFromPlaceholderCollector(eventOptions) {

        // the ID of a master or layout slide (or null, if a document slide was removed)
        const target = eventOptions.isMasterOrLayoutSlide ? eventOptions.id : null;

        if (target && this.#placeHolderCollector[target]) {
            delete this.#placeHolderCollector[target]; // removing all place holder info from model
        }
    }

    /**
     * Handling the visualization of empty text frames without explicit border. In the
     * standard view it is necessary to assign a dotted line to these empty text frames.
     * In the layout or master view, such a dotted line is always assigned to the place
     * holder drawings.
     *
     * @param {Node|jQuery} drawing
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains.
     *
     * @param {Boolean} isEmptyPlaceHolderWithoutText
     *  Whether the drawing is an empty place holder in which no text can be inserted (table, image, ...)
     *
     * @param {Boolean} isLayoutOrMasterSlide
     *  Whether the drawing is located on a layout or master slide
     *
     * @param {Object} [drawingAttrs]
     *  The attributes of the drawing node.
     */
    #handleBorderForEmptyTextFrames(drawing, isEmptyPlaceHolderWithoutText, isLayoutOrMasterSlide, drawingAttrs) {

        // whether the text frame is empty, ignoring any existing template text -> this is independent from an existing border
        const setEmptyTextFrameClass = isEmptyPlaceHolderWithoutText || isEmptyTextframe(drawing, { ignoreTemplateText: true });
        // receiving the full stack of attributes, but only if there is no text content or this is a master/layout slide
        if ((setEmptyTextFrameClass || isLayoutOrMasterSlide) && !drawingAttrs) { drawingAttrs = this.getElementAttributes(drawing); }
        // setting an additional marker for displaying a dotted line around an empty text frame
        const setEmptyTextFrameBorderClass = (setEmptyTextFrameClass || isLayoutOrMasterSlide) && !isBorderAttributeSet(drawingAttrs);
        // the content node inside the drawing
        const contentNode = $(drawing).children('.content');

        // set class DrawingFrame.EMPTYTEXTFRAME_CLASS at empty text frames
        contentNode.toggleClass(EMPTYTEXTFRAME_CLASS, setEmptyTextFrameClass);
        // set class DrawingFrame.EMPTYTEXTFRAMEBORDER_CLASS at empty text frames or master/layout slide text frames without border
        contentNode.toggleClass(EMPTYTEXTFRAMEBORDER_CLASS, setEmptyTextFrameBorderClass);
        $(drawing).toggleClass(EMPTYTEXTFRAMEBORDER_CLASS, setEmptyTextFrameBorderClass);
        // if the dotted line is shown, it must be shown thicker in FF browser (DOCS-1645, DOCS-2165)
        if (_.browser.Firefox) {
            contentNode.toggleClass(EMPTY_THICKBORDER_CLASS, setEmptyTextFrameBorderClass);
            $(drawing).toggleClass(EMPTY_THICKBORDER_CLASS, setEmptyTextFrameBorderClass);
        }
    }

    /**
     * Handling a default text that is inserted into empty place holder drawings.
     *
     * @param {Node|jQuery} drawing
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains.
     *
     * @param {Object} [options]
     *  Some additional options that are supported by the model function 'getDefaultTextForTextFrame'.
     *  Additionally supported:
     *  @param {Boolean} [options.ignoreTemplateText=false]
     *      This option is handled within DOM.isEmptyTextFrame to define, that text frames
     *      that contain template text are also empty.
     *
     * @returns {Boolean}
     *  Whether the default template text was inserted into the drawing.
     */
    #handleTemplateText(drawing, options) {
        return this.docModel.addTemplateTextIntoTextSpan(drawing, options);
    }

    /**
     * ODP only.
     * Adding a paragraph into empty place holder drawings. After loading they do not contain
     * a paragraph, if they contain no content.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node.
     */
    #handleMissingParagraphInPlaceHolderDrawing(drawing) {

        // the implicit paragraph node
        let implicitParagraph = null;
        // the textframe node
        let textFrameNode = null;
        // whether an update of drawing is required
        let drawingUpdateRequired = false;

        if ($(drawing).find('div.p').length === 0) {

            textFrameNode = getTextFrameNode(drawing); // during loading the text frame already exists (and was already formatted, so it must be reused)

            if (textFrameNode.length === 0) { // create new text frame node required -> updateDrawingFormatting is also required (should happen only when loading document with operations)
                textFrameNode = prepareDrawingFrameForTextInsertion(drawing);
                drawingUpdateRequired = true;
            }

            if (textFrameNode && textFrameNode.length > 0) {
                implicitParagraph = this.docModel.getValidImplicitParagraphNode();
                // no bullets allowed for implicit paragraphs title place holder drawings
                if (this.#odf && isTitleOrSubtitlePlaceHolderDrawing(drawing)) {
                    // adding paragraph attributes without operation (because it is an implicit paragraph) -> no inherited bullets in titles
                    this.docModel.paragraphStyles.setElementAttributes(implicitParagraph, { paragraph: { bullet: { type: 'none' }, indentLeft: 0, indentFirstLine: 0 } });
                }
                // inserting an implicit paragraph into the drawing
                textFrameNode.append(implicitParagraph);
                this.docModel.paragraphStyles.updateElementFormatting(implicitParagraph);
                // taking care of implicit paragraphs
                if (this.#odf) { this.docModel.handleTriggeringListUpdate(implicitParagraph); }
                // starting drawing formatting if required (should only be necessary after load with operations)
                if (drawingUpdateRequired) { this.docModel.drawingStyles.updateElementFormatting(drawing); }
            }
        }
    }

    /**
     * Helper function, that adds the place holder text from layout drawings for place holder
     * types, that cannot contain text. This are tables, images, ... .
     * There is no paragraph in the document slide, that can contain the template text. Therefore
     * the complete content from the drawing node in the corresponding layout slide is cloned.
     * During loading the document it happens, that the document slide is formatted before its
     * layout slide (which directly follows the document slide). Therefore it might be necessary
     * to try to get the template text later again.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node.
     *
     * @param {String} type
     *  The type of the place holder drawing.
     *
     * @param {Number|Null} index
     *  The index of the place holder drawing.
     *
     * @param {String} slideId
     *  The ID of the slide that contains the drawing.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.update=false]
     *      Whether an existing old text shall be removed. If this option is set to true and
     *      there is no old content, this function returns without any modification. This
     *      guarantees, that only empty place holder drawings get the template text.
     */
    #addTemplateReadOnlyText(drawing, type, index, slideId, options) {

        // the container for the read-only inserted text span for tables, images, ...
        let templateReadOnlyText = null;
        // the id of the layout slide
        const layoutId = this.docModel.getLayoutSlideId(slideId);
        // the corresponding drawing on the layout slide
        const layoutDrawing = this.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId, type, index);
        // whether an existing text shall be replaced
        const  update = getBooleanOption(options, 'update', false);
        // an already existing template text
        const oldTemplateText = update ? $(drawing).children(PresentationDrawingStyles.TEMPLATE_READONLY_TEXT_CONTAINER_SELECTOR) : null;

        const registerLaterCall = () => {
            this.docModel.executeDelayed(() => { // try later again, because the layout slide is formatted now
                this.#addTemplateReadOnlyText(drawing, type, index, slideId);
            }, 200);
        };

        if (update && oldTemplateText.length === 0) { return; } // do nothing, if update is true, but there is no old text

        // getting the text from the layout slide
        if (layoutDrawing.length > 0) {
            templateReadOnlyText = $('<div>', { contenteditable: false }).addClass(PresentationDrawingStyles.TEMPLATE_READONLY_TEXT_CONTAINER_CLASS);
            templateReadOnlyText.append(layoutDrawing.find('div.p').clone(true));
            if (update) { oldTemplateText.remove(); } // remove existing old text
            $(drawing).append(templateReadOnlyText);
        } else if (this.docModel.getSlideFormatManager().isUnformattedSlide(layoutId)) {  // the layout slide formatting has not finished yet
            registerLaterCall();
        }
    }

    /**
     * Inserting the container for the template images into a specified drawing.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node.
     *
     * @param {AllPlaceHolderTypes} type
     *  The type of the place holder drawing (from the explicit drawing attributes).
     *
     * @param {Number|Null} index
     *  The index of the place holder drawing (from the explicit drawing attributes).
     *
     * @param {String} slideId
     *  The ID of the slide that contains the drawing.
     */
    #insertImageTemplateContainer(drawing, type, index, slideId) {

        // the container node for the template images
        let imageContainerNode = null;
        // the list with the required buttons for the specified type
        const buttonList = getPlaceHolderTemplateButtonsList(type, this.#odf);
        // whether some text also needs to be added. This is the case for place holder drawings
        // without text input. In this case the text from the layout slide needs to be used.
        const addText = NO_TEXT_PLACEHOLDER_TYPES.has(type);

        if (buttonList) {

            imageContainerNode = $('<div>').addClass(PresentationDrawingStyles.TEMPLATE_DRAWING_CONTAINER_CLASS);

            _.each(buttonList, buttonString => {

                let buttonNode = null;

                // calculate the CSS fill attributes
                switch (buttonString) {
                    case 'table':
                        buttonNode = $('<a tabindex="0">').append(createIcon('bi:table'));
                        buttonNode.addClass(TABLE_TEMPLATE_BUTTON_CLASS);
                        setToolTip(buttonNode, gt('Insert table'));
                        break;
                    case 'picture':
                        buttonNode = $('<a tabindex="0">').append(createIcon('bi:image'));
                        buttonNode.addClass(IMAGE_TEMPLATE_BUTTON_CLASS);
                        setToolTip(buttonNode, gt('Insert image'));
                        break;
                    case 'media':
                        buttonNode = $('<a tabindex="0">').append(createIcon('bi:film'));
                        setToolTip(buttonNode, gt('Insert media clip'));
                        break;
                    case 'chart':
                        buttonNode = $('<a tabindex="0">').append(createIcon('bi:bar-chart'));
                        setToolTip(buttonNode, gt('Insert chart'));
                        break;
                    case 'clipart':
                        buttonNode = $('<a tabindex="0">').append(createIcon('bi:globe'));
                        setToolTip(buttonNode, gt('Insert online image'));
                        break;
                    case 'smartart':
                        buttonNode = $('<a tabindex="0">').append(createIcon('bi:layout-text-window-reverse'));
                        setToolTip(buttonNode, gt('Insert smart art graphic'));
                        break;
                    default:
                        globalLogger.warn('DrawingStyles.insertImageTemplateContainer(): unknown button type: ' + buttonString);
                }

                if (buttonNode) {
                    buttonNode.addClass(PLACEHOLDER_TEMPLATE_BUTTON_CLASS);
                    imageContainerNode.append(buttonNode);
                }
            });

            $(drawing).append(imageContainerNode);

            if (addText && !this.docModel.isLayoutOrMasterId(slideId)) { this.#addTemplateReadOnlyText(drawing, type, index, slideId); }

            // centering the image container node in the drawing
            imageContainerNode.css('left', (0.5 * ($(drawing).width() - imageContainerNode.width())) + 'px');
            imageContainerNode.css('top', (0.5 * ($(drawing).height() - imageContainerNode.height())) + 'px');
        }
    }

    /**
     * This function modifies the return object from the function 'this.getAllPlaceHolderAttributesForId(firstId)'
     * in that way, that it can be differentiated between body place holders that are text body place holders (type
     * 'body' is explicitely set) and content body place holders ('type' not set).
     *
     * @param {Object} set
     *  The object from function 'this.getAllPlaceHolderAttributesForId(firstId)', in which the key 'body' collects
     *  all place holders that can contain text. This are text place holders and content place holders.
     *
     * @returns {Object}
     *  An object, in which text content place holders and content body place holders are separated. Content body
     *  place holder are stored under the key 'contentbody' (CONTENTBODY).
     */
    #handleDifferentBodySet(set) {

        const newSet = _.isObject(set) ? {} : null;
        let allIndizes = null;

        _.each(_.keys(set), type => {
            if (type === 'body') {
                allIndizes = _.keys(set[type]);
                _.each(allIndizes, index => {
                    const attrs = set[type][index];
                    if (isContentBodyPlaceHolderAttributeSet(attrs)) {
                        newSet[CONTENTBODY] = newSet[CONTENTBODY] || {};
                        newSet[CONTENTBODY][index] = attrs;
                    } else {
                        newSet.body = newSet.body || {};
                        newSet.body[index] = attrs;
                    }
                });
            } else {
                newSet[type] = set[type];
            }
        });

        return newSet;
    }

    /**
     * Updating the position of an already existing image container node. This might
     * be necessary after a change of layout.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node.
     *
     * @param {jQuery} imageContainerNode
     *  The image container node.
     */
    #updateImageContainerNode(drawing, imageContainerNode) {
        // centering the image container node in the drawing
        imageContainerNode.css('left', (0.5 * ($(drawing).width() - imageContainerNode.width())) + 'px');
        imageContainerNode.css('top', (0.5 * ($(drawing).height() - imageContainerNode.height())) + 'px');
    }

    /**
     * Removing the container for the template images from a specified drawing.
     *
     * @param {Node|jQuery} drawing
     *  The drawing node.
     */
    #removeImageTemplateContainer(drawing) {
        // Removing the container node(s) for the template images and the (read-only) template text
        $(drawing).children(PresentationDrawingStyles.TEMPLATE_DRAWING_CONTAINER_SELECTOR + ', ' + PresentationDrawingStyles.TEMPLATE_READONLY_TEXT_CONTAINER_SELECTOR).remove();
    }

    /**
     * Emptying the complete place holder model. This is necessary, if a new snapshot is applied.
     */
    #emptyPlaceHolderModel() {
        this.#placeHolderCollector = {};
    }

}

// constants --------------------------------------------------------------

/**
 * Helper object with allowed switches of drawing types during
 * a change of a layout slide.
 */
PresentationDrawingStyles.ALLOWED_DRAWING_TYPE_SWITCHES = {
    title: 'ctrTitle',
    ctrTitle: 'title',
    body: 'subTitle',
    subTitle: 'body'
};

/**
 * Helper object with forced switches for receiving the place holder
 * attributes from a master slide. Example: A master never has a place
 * holder of type 'subTitle'. Instead the 'body' must be used.
 *
 * Info: Please also check 'SlideAttributesMixin.LIST_TYPE_CONVERTER'.
 */
PresentationDrawingStyles.MASTER_PLACEHOLDER_TYPE_SWITCHES = {
    subTitle: 'body',
    ctrTitle: 'title'
};

/**
 * The CSS class used to mark the template image container nodes inside
 * a drawing node.
 */
PresentationDrawingStyles.TEMPLATE_DRAWING_CONTAINER_CLASS = 'templatedrawingcontainer';

/**
 * A jQuery selector that matches nodes representing a text frame node that
 * are 'classical' text frames in odf format.
 */
PresentationDrawingStyles.TEMPLATE_DRAWING_CONTAINER_SELECTOR = '.' + PresentationDrawingStyles.TEMPLATE_DRAWING_CONTAINER_CLASS;

/**
 * The CSS class used to mark the template text in empty place holder nodes that cannot contain text content.
 * This are the place holder templates defined in 'NO_TEXT_PLACEHOLDER_TYPES'.
 */
PresentationDrawingStyles.TEMPLATE_READONLY_TEXT_CONTAINER_CLASS = 'templatereadonlytext';

/**
 * A jQuery selector that matches nodes representing read-only text information inside empty place holder
 * nodes that cannot contain text content. This are the place holder templates defined in
 * 'NO_TEXT_PLACEHOLDER_TYPES'.
 */
PresentationDrawingStyles.TEMPLATE_READONLY_TEXT_CONTAINER_SELECTOR = '.' + PresentationDrawingStyles.TEMPLATE_READONLY_TEXT_CONTAINER_CLASS;

/**
 * The default place holder index for drawings of type 'body'.
 */
PresentationDrawingStyles.MS_DEFAULT_PLACEHOLDER_INDEX = 4294967295;

/**
 * The default place holder attributes for ODF (DOCS-4913)
 */
PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_ATTRIBUTES = {
    title: {
        0: {
            line: { type: "none" },
            fill: { type: "none" },
            paragraph: { alignment: "center" },
            presentation: { phType: "title" },
            shape: { wordWrap: true, anchor: "centered" },
            character: { color: { type: "auto" }, underline: false, strike: "none", bold: false, italic: false, caps: "none", fillColor: { type: "auto" }, fontName: "Liberation Sans", fontSize: 44, fontNameComplex: "Lucida Sans" },
            drawing: { top: 628, left: 1400, width: 25199, height: 2629 }
        }
    },
    body: {
        0: {
            line: { type: "none" },
            fill: { type: "none" },
            paragraph: { spacingBefore: { value: 500, type: "fixed" }, spacingAfter: { value: 0, type: "fixed" } },
            presentation: { phType: "body" },
            shape: { autoResizeHeight: false, wordWrap: true },
            character: { color: { type: "auto" }, underline: false, strike: "none", bold: false, italic: false, caps: "none", fillColor: { type: "auto" }, fontName: "Liberation Sans", fontSize: 32, fontNameComplex: "Lucida Sans" },
            drawing: { top: 3685, left: 1400, width: 25199, height: 9134 }
        }
    },
    dt: {
        0: {
            line: { type: "none" },
            fill: { color: { value: "ffffff", type: "rgb" }, type: "none" },
            presentation: { phType: "dt" },
            shape: { autoResizeHeight: false, wordWrap: true },
            character: { fontSize: 14 },
            drawing: { top: 14300, left: 1400, width: 6523, height: 1085, minFrameHeight: 1086 }
        }
    },
    ftr: {
        0: {
            line: { type: "none" },
            fill: { color: { value: "ffffff", type: "rgb" }, type: "none" },
            paragraph: { alignment: "center" },
            presentation: { phType: "ftr" },
            shape: { autoResizeHeight: false, wordWrap: true },
            character: { fontSize: 14 }, drawing: { top: 14300, left: 9500, width: 8875, height: 1085, minFrameHeight: 1086 }
        }
    },
    sldNum: {
        0: {
            line: { type: "none" },
            fill: { color: { value: "ffffff", type: "rgb" }, type: "none" },
            paragraph: { alignment: "right" },
            presentation: { phType: "sldNum" },
            shape: { autoResizeHeight: false, wordWrap: true },
            character: { fontSize: 14 }, drawing: { top: 14300, left: 20000, width: 6523, height: 1085, minFrameHeight: 1086 }
        }
    }
};

/**
 * The slide size for the default place holder attributes for ODF (DOCS-4913)
 * specified in PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_ATTRIBUTES
 */
PresentationDrawingStyles.DEFAULT_ODF_PLACEHOLDER_SLIDE_SIZE = {
    width: 28000,
    height: 15750
};
