/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import PresentationModel, { DocAttributePoolMap } from "@/io.ox/office/presentation/model/docmodel";

// types ======================================================================

export interface PageAttributes {

    /**
     * Total width of a single page, in 1/100 of millimeters.
     */
    width: number;

    /**
     * Total height of a single page, in 1/100 of millimeters.
     */
    height: number;

    /**
     * Margin between left page border and editing area, in 1/100 of
     * millimeters.
     */
    marginLeft: number;

    /**
     * Margin between right page border and editing area, in 1/100 of
     * millimeters.
     */
    marginRight: number;

    /**
     * Margin between top page border and editing area, in 1/100 of
     * millimeters.
     */
    marginTop: number;

    /**
     * Margin between bottom page border and editing area, in 1/100 of
     * millimeters.
     */
    marginBottom: number;

    /**
     * Margin between top page border and top header border, in 1/100 of
     * millimeters.
     */
    marginHeader: number;

    /**
     * Margin between bottom page border and bottom footer border, in 1/100 of
     * millimeters.
     */
    marginFooter: number;

    /**
     * Whether to use separate header/footer settings for the first page.
     *
     */
    firstPage: boolean;

    /**
     * Whether to use separate header/footer settings for even and odd pages,
     * and mirror margin settings accordingly.
     */
    evenOddPages: boolean;
}

export interface PageAttributeSet {
    page: PageAttributes;
}

// class PageStyleCollection ==================================================

export default class PageStyleCollection extends StyleCollection<PresentationModel, DocAttributePoolMap, PageAttributeSet> {
    public constructor(docModel: PresentationModel);
}
