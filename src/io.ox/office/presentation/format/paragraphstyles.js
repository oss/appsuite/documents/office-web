/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { SMALL_DEVICE, getBooleanOption, convertHmmToLength, convertLength, convertLengthToHmm, iterateDescendantNodes } from '@/io.ox/office/tk/utils';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributes, getExplicitAttributeSet, isCharacterFontThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import { getTargetContainerId, isCellContentNode, isComplexFieldNode, isHardBreakNode, iterateTextSpans } from '@/io.ox/office/textframework/utils/dom';

import ParagraphStyles from '@/io.ox/office/textframework/format/paragraphstyles';
import { NODE_SELECTOR, isAutoResizeHeightDrawingFrame, isNoWordWrapDrawingFrame, isTableDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { DEFAULT_PLACEHOLDER_INDEX, DEFAULT_PLACEHOLDER_TYPE, isPlaceHolderAttributeSet } from '@/io.ox/office/presentation/utils/placeholderutils';

// definitions for paragraph attributes
const DEFINITIONS = {

    // list level
    level: { def: 0 },

    listLabelHidden: { def: false },

    listStartValue: { def: -1 },

    outlineLevel: { def: 9 },

    bullet: { def: {} },    // TODO: check if should use "def: { type: 'none' }" instead; according to the operation definition: https://intranet.open-xchange.com/wiki/documents-team:operations:presentation#bullet

    bulletFont: { def: { followText: true } },

    bulletColor: { def: { followText: true } },

    bulletSize: { def: { type: 'followText', size: 0 } },

    defaultTabSize: { def: 2540 }, // default in presentation app: one inch (47964)

    spacingBefore: { def: 0 },

    spacingAfter: { def: 0 }
};

// parent families with parent element resolver functions
const PARENT_RESOLVERS = {
    cell: paragraph => { return isCellContentNode(paragraph.parent()) ? paragraph.closest('td') : null; }
};

// class PresentationParagraphStyles ======================================

/**
 * Contains the style sheets for paragraph formatting attributes. The CSS
 * formatting will be read from and written to DOM paragraph elements.
 *
 * @param {PresentationModel} docModel
 *  The presentation document model containing this instance.
 */
export default class PresentationParagraphStyles extends ParagraphStyles {

    constructor(docModel) {

        // base constructor
        super(docModel, {
            families: ["character", "changes"],
            parentResolvers: PARENT_RESOLVERS,
            baseAttributesResolver: (...args) => this.#resolveLayoutAttributes(...args)
        });

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('paragraph', DEFINITIONS);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updateParagraphFormatting);
    }

    // public methods -----------------------------------------------------

    /**
     * Getting the level of a specified paragraph. If the paragraph has no
     * level specified in the attributes, 0 is returned.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph node whose level shall be determined.
     *
     * @returns {Number}
     *  The paragraph level. If it cannot be determined, 0 is returned.
     */
    getParagraphLevel(paragraph) {
        // the explicit attributes at a paragraph
        const paraAttrs = getExplicitAttributeSet(paragraph, true);
        return (paraAttrs && paraAttrs.paragraph && paraAttrs.paragraph.level) || 0;
    }

    /**
     * Getting all available list level attributes specified for a given drawing. This list styles can be
     * defined at the master or layout slide or at the drawing on the master or layout slide.
     * id, place holder type and place holder index.
     *
     * @param {jQuery} drawing
     *  The drawing, for that the list styles shall be determined.
     *
     * @param {Object} [options]
     *  Supports all options specified at the private function 'getAllAvailableListStyleAttributes'.
     *
     * @returns {Object|Null}
     *  An object containing the list level attributes that are available inside a specified drawing. If no
     *  list styles are defined for this drawing, null is returned.
     *  The returned object has the top level keys 'l1', 'l2', ...
     */
    getAllAvailableListStyleAttributes(drawing, options) {

        // the ids of active slide, master slide and layout slide
        let id = null;
        let layoutId = null;
        let masterId = null;
        // the place holder type and index
        let placeHolderType = null;
        let placeHolderIndex = null;
        // the explicit drawing attributes
        let drawingAttrs = null;
        // the list style set
        let allListStyles = null;

        // check, if the drawing, containing the paragraph, is a 'placeholder' drawing
        if (drawing && drawing.length > 0) { drawingAttrs = getExplicitAttributeSet(drawing, true); }

        if (isPlaceHolderAttributeSet(drawingAttrs) && !isTableDrawingFrame(drawing)) {

            id = getTargetContainerId(drawing.parent()); // checking only direct children of slide
            placeHolderType = drawingAttrs.presentation.phType;
            placeHolderIndex = drawingAttrs.presentation.phIndex;

            if (placeHolderType && !placeHolderIndex) { placeHolderIndex = DEFAULT_PLACEHOLDER_INDEX; }
            if (placeHolderIndex && !placeHolderType) { placeHolderType = DEFAULT_PLACEHOLDER_TYPE; }

            if (this.docModel.isStandardSlideId(id)) {
                layoutId = this.docModel.getLayoutSlideId(id);
                masterId = this.docModel.getMasterSlideId(layoutId);
                allListStyles = this.#getAllAvailableListStyleAttributes(layoutId, masterId, placeHolderType, placeHolderIndex, options);
            } else if (this.docModel.isLayoutSlideId(id)) {
                masterId = this.docModel.getMasterSlideId(id);
                allListStyles = this.#getAllAvailableListStyleAttributes(id, masterId, placeHolderType, placeHolderIndex, options);
            } else if (this.docModel.isMasterSlideId(id)) {
                allListStyles = this.#getAllAvailableListStyleAttributes(null, id, placeHolderType, placeHolderIndex, options);
            }
        } else {

            // using the default text list styles, that are saved in the list handler mixin
            // -> these values will be used for shapes or text frames
            allListStyles = this.docModel.getDefaultTextListStyles();
        }

        return allListStyles;
    }

    // private methods ----------------------------------------------------

    /**
     * Will be called for every paragraph whose character attributes have
     * been changed.
     *
     * @param {jQuery} paragraph
     *  The paragraph node whose attributes have been changed, as jQuery
     *  object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    #updateParagraphFormatting(paragraph, mergedAttributes) {

        // the paragraph attributes of the passed attribute map
        const paragraphAttributes = mergedAttributes.paragraph;

        let leftMargin = 0;
        let rightMargin = 0;

        const nextParagraph = paragraph.next();
        const collectedAttrs =  {};
        let textIndent = 0;
        // target for targetChain for themes
        let target;
        // explicitParaAttributes = null;

        const calcSpacingHmm = lineHeight => {
            // fix for Bug 48442
            if (!lineHeight.value) { return 0; }

            let height = 0;
            let fontHeight = convertLength(mergedAttributes.character.fontSize, 'pt', 'px');
            let firstSpan;
            let explicitAttrs;

            if (lineHeight.type === 'fixed') {
                height = convertHmmToLength(lineHeight.value, 'px');
            } else if (lineHeight.type === 'percent') {
                // 61284: If percentage is specified, it must be token care of the font height of a span inside the
                // paragraph (simplified in this case: using the first span inside the paragraph)
                // -> the space before the paragraph changes in PP, if the font height inside the paragraph is modified
                // -> checking if the first text span has an implicit height (example task 47960)
                firstSpan = paragraph.children('span').first();
                if (firstSpan.length) {
                    explicitAttrs = getExplicitAttributes(firstSpan, 'character');
                    if (explicitAttrs.fontSize) { fontHeight = convertLength(explicitAttrs.fontSize, 'pt', 'px'); }
                }
                height = fontHeight * lineHeight.value / 100;
            } else {
                globalLogger.warn('ParagraphStyles.calcSpacingHmm(): invalid line height type', lineHeight.type);
                return 0;
            }

            if (!target && this.docModel.useSlideMode() && isCharacterFontThemed(mergedAttributes.character)) {
                target = this.docModel.getTargetChainForNode(paragraph);
            }

            return convertLengthToHmm(height, 'px');
        };

        // Bug 49446, Bug 52808 & Bug 52475
        const drawingNode = paragraph.closest(NODE_SELECTOR);
        if (this.docModel.getApp().isOOXML() && isNoWordWrapDrawingFrame(drawingNode) && !isAutoResizeHeightDrawingFrame(drawingNode)) {
            collectedAttrs.alignSelf = (function () {
                switch (paragraphAttributes.alignment) {
                    case 'center':  return 'center';
                    case 'left':    return 'flex-start';
                    case 'right':   return 'flex-end';
                    case 'justify': return 'stretch';
                    default:        return '';
                }
            }());
        }

        // Always update character formatting of all child nodes which may
        // depend on paragraph settings, e.g. automatic text color which
        // depends on the paragraph fill color. Also visit all helper nodes
        // containing text spans, e.g. numbering labels.
        const { characterStyles } = this.docModel;
        iterateDescendantNodes(paragraph, node => {

            // visiting the span inside a hard break node
            // -> this is necessary for change tracking attributes
            if (isHardBreakNode(node)) {
                characterStyles.updateElementFormatting(node.firstChild);
            }
            if (isComplexFieldNode(node)) {
                characterStyles.updateElementFormatting(node);
            }

            iterateTextSpans(node, span => {
                characterStyles.updateElementFormatting(span, { baseAttributes: mergedAttributes });
            });
        }, undefined, { children: true });

        // update borders
        let leftPadding = 0;
        let topMargin = 0;
        let bottomMargin = 0;

        // special ODF handling for paragraphs with default attributes and without bullet (56062)
        if (this.docModel.getApp().isODF() && mergedAttributes.paragraph.bullet.type === 'none') {
            mergedAttributes.paragraph.indentLeft = 0;
            mergedAttributes.paragraph.indentFirstLine = 0;
        }

        //calculate list indents
        //            let listLabel = $(paragraph).children(DOM.LIST_LABEL_NODE_SELECTOR);
        //            let listStyleId = paragraphAttributes.listStyleId;
        //            if (listStyleId.length) {
        //                let listLevel = paragraphAttributes.listLevel,
        //                    lists = this.docModel.getListCollection();
        //                if (listLevel < 0) {
        //                    // is a numbering level assigned to the current paragraph style?
        //                    listLevel = lists.findIlvl(listStyleId, mergedAttributes.styleId);
        //                }
        //                if (listLevel !== -1 && listLevel < 10) {
        //                    let listItemCounter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        //                    let listObject = lists.formatNumber(paragraphAttributes.listStyleId, listLevel, listItemCounter, 1);
        //
        //                    if (listObject.indent > 0) {
        //                        leftPadding += listObject.indent - listObject.firstLine;
        //                        leftMargin += listObject.firstLine;
        //                    }
        //                    if (listLabel.length) {
        //                        let listSpan = listLabel.children('span');
        //                        if (listObject.fontSize) {
        //                            listSpan.css('font-size', listObject.fontSize + 'pt');
        //                        }
        //                        if (listObject.color) {
        //                            listSpan.css('color', this.docModel.getCssTextColor(listObject.color, [paragraphAttributes.fillColor, listObject.fillColor]));
        //                        }
        //                    }
        //                } else {
        //                    //fix for Bug 37594 some list-helper dont disappear
        //                    listLabel.detach();
        //                }
        //            } else {
        //                //fix for Bug 37594 some list-helper dont disappear
        //                listLabel.detach();
        //            }

        // paragraph margin attributes - also applying to paragraphs in a list, if they are defined as explicit attribute
        // -> handle both cases correctly: 40792 and 41118
        // if (listStyleId === '') {
        leftMargin += paragraphAttributes.indentLeft ? paragraphAttributes.indentLeft : 0;
        rightMargin += paragraphAttributes.indentRight ? paragraphAttributes.indentRight : 0;
        textIndent = paragraphAttributes.indentFirstLine ? paragraphAttributes.indentFirstLine : 0;
        collectedAttrs.textIndent = textIndent / 100 + 'mm';
        //            } else {
        //                // but explicit attributes need to be handled (40792)
        //                explicitParaAttributes = getExplicitAttributeSet(paragraph);
        //                explicitParaAttributes = (explicitParaAttributes && explicitParaAttributes.paragraph) || {};
        //                if (explicitParaAttributes.indentLeft) { leftMargin += explicitParaAttributes.indentLeft; }
        //                if (explicitParaAttributes.indentRight) { rightMargin += explicitParaAttributes.indentRight; }
        //                if (explicitParaAttributes.indentFirstLine) {
        //                    textIndent += explicitParaAttributes.indentRight;
        //                    collectedAttrs.textIndent = textIndent / 100 + 'mm';
        //                }
        //            }

        if (textIndent < 0) {
            leftPadding -= textIndent;
            leftMargin += textIndent;
        }

        _.extend(collectedAttrs, {
            paddingLeft: (leftPadding / 100) + 'mm',
            marginLeft: (leftMargin / 100) + 'mm',
            marginRight: (rightMargin / 100) + 'mm',
            textIndent: (textIndent / 100) + 'mm'
        });

        // Overwrite of margin left for lists in draft mode (conversion from fixed unit mm to %)
        if (SMALL_DEVICE && paragraph.data('draftRatio')) {
            collectedAttrs.marginLeft = (parseInt($(paragraph).css('margin-left'), 10) * paragraph.data('draftRatio')) + '%';
        }

        if (paragraphAttributes.spacingBefore) {
            topMargin += calcSpacingHmm(paragraphAttributes.spacingBefore);
        }
        if (paragraphAttributes.spacingAfter && nextParagraph.length) {
            bottomMargin += calcSpacingHmm(paragraphAttributes.spacingAfter);
        }

        _.extend(collectedAttrs, {
            marginTop: (topMargin / 100) + 'mm',
            marginBottom: (bottomMargin / 100) + 'mm'
        });

        if (!_.browser.IE) {
            collectedAttrs.width = null;
        }

        // apply collected attributes at the end
        paragraph.css(collectedAttrs);

        // update the size of all tab stops in this paragraph
        this.implUpdateTabStops(paragraph, mergedAttributes);
    }

    /**
     * Getting the merged place holder attributes from master slide and layout slide.
     *
     * @param {String} [layoutId]
     *  The id of the layout slide.
     *
     * @param {String} [masterId]
     *  The id of the master slide.
     *
     * @param {String} placeHolderType
     *  The drawing place holder type.
     *
     * @param {Number} placeHolderIndex
     *  The drawing place holder index.
     *
     * @param {String} paraLevel
     *  The paragraph level.
     *
     * @returns {Object|Null}
     *  An object containing the attributes from the layout slide and the master slide. If attributes are defined
     *  at both slide, the layout slide overwrites the values from the master slide.
     */
    #getMergedPlaceHolderAttributes(layoutId, masterId, placeHolderType, placeHolderIndex, paraLevel) {

        // the layout drawing attributes
        let layoutDrawingAttrs = layoutId ? this.docModel.drawingStyles.getPlaceHolderAttributes(layoutId, placeHolderType, placeHolderIndex) : null;
        // the layout slide attributes
        let layoutSlideAttrs = layoutId ? this.docModel.getListStylesAttributesOfSlide(layoutId, placeHolderType) : null;
        // the master drawing attributes
        let masterDrawingAttrs = masterId ? this.docModel.drawingStyles.getPlaceHolderAttributes(masterId, placeHolderType, placeHolderIndex) : null;
        // the master slide attributes
        let masterSlideAttrs = masterId ? this.docModel.getListStylesAttributesOfSlide(masterId, placeHolderType) : null;
        // the attributes of layout and master slide
        let attributes = null;

        // using only the 'listStyle' property from the drawing attribute set
        layoutDrawingAttrs = layoutDrawingAttrs && layoutDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] ? layoutDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] : null;
        masterDrawingAttrs = masterDrawingAttrs && masterDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] ? masterDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] : null;

        // using only selected list level from drawing 'listStyle' attribute
        layoutDrawingAttrs = layoutDrawingAttrs && layoutDrawingAttrs[paraLevel] ? layoutDrawingAttrs[paraLevel] : null;
        masterDrawingAttrs = masterDrawingAttrs && masterDrawingAttrs[paraLevel] ? masterDrawingAttrs[paraLevel] : null;

        // using only selected list level from slide attributes
        layoutSlideAttrs = layoutSlideAttrs && layoutSlideAttrs[paraLevel] ? layoutSlideAttrs[paraLevel] : null;
        masterSlideAttrs = masterSlideAttrs && masterSlideAttrs[paraLevel] ? masterSlideAttrs[paraLevel] : null;

        // merging the collected attributes from master and layout slides and drawings (never overwrite attribute set)
        if (masterSlideAttrs) { attributes = this.extendAttrSet({}, masterSlideAttrs); }
        if (masterDrawingAttrs) { attributes = this.extendAttrSet(attributes || {}, masterDrawingAttrs); }
        if (layoutSlideAttrs) { attributes = this.extendAttrSet(attributes || {}, layoutSlideAttrs); }
        if (layoutDrawingAttrs) { attributes = this.extendAttrSet(attributes || {}, layoutDrawingAttrs); }

        return attributes;
    }

    /**
     * Getting the merged place holder attributes for non place holder drawings.
     *
     * @param {jQuery|Node} drawing
     *  The drawing containing the paragraph with the specified level.
     *
     * @param {String} paraLevel
     *  The paragraph level.
     *
     * @returns {Object}
     *  An object containing the merged attributes for a specified paragraph level. Merged are the
     *  default list attributes and the list attributes that are assigned to the drawing node.
     */
    #getMergedNonPlaceHolderAttributes(drawing, paraLevel) {

        // the default attributes for a specified level
        let defaultAttrs = this.docModel.getDefaultTextListStylesForLevel(paraLevel);
        // the hard list style attributes at the drawing (45279)
        const explicitDrawingAttrs = this.docModel.drawingStyles.getDrawingListStyleAttributes(drawing);
        // the explicit character attributes at the drawing
        let explicitCharAttributes = null;

        defaultAttrs = this.extendAttrSet({}, defaultAttrs);

        if (explicitDrawingAttrs && explicitDrawingAttrs[paraLevel]) {
            defaultAttrs = this.extendAttrSet(defaultAttrs, explicitDrawingAttrs[paraLevel]);
        }

        // also checking explicit character attributes at the drawing
        explicitCharAttributes = getExplicitAttributes(drawing, 'character');

        if (explicitCharAttributes && !_.isEmpty(explicitCharAttributes)) {
            defaultAttrs = this.extendAttrSet(defaultAttrs, { character: explicitCharAttributes });
        }

        return defaultAttrs;
    }

    /**
     * Getting all available list level attributes specified for a given set of layout slide id, master slide
     * id, place holder type and place holder index.
     *
     * @param {String} [layoutId]
     *  The id of the layout slide.
     *
     * @param {String} [masterId]
     *  The id of the master slide.
     *
     * @param {String} placeHolderType
     *  The drawing place holder type.
     *
     * @param {Number} placeHolderIndex
     *  The drawing place holder index.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.nameOnly=false]
     *      If set to true, only the names of the list styles are returned, not the complete definitions.
     *
     * @returns {Object|Null}
     *  An object containing the list level attributes that are available inside a drawing, that is specified
     *  by layout id, master id, place holder type and place holder index. If no list styles are defined for
     *  this drawing, null is returned.
     *  The returned object has the top level keys 'l1', 'l2', ...
     */
    #getAllAvailableListStyleAttributes(layoutId, masterId, placeHolderType, placeHolderIndex, options) {

        // the layout drawing attributes
        let layoutDrawingAttrs = layoutId ? this.docModel.drawingStyles.getPlaceHolderAttributes(layoutId, placeHolderType, placeHolderIndex) : null;
        // the layout slide attributes
        const layoutSlideAttrs = layoutId ? this.docModel.getListStylesAttributesOfSlide(layoutId, placeHolderType) : null;
        // the master drawing attributes
        let masterDrawingAttrs = masterId ? this.docModel.drawingStyles.getPlaceHolderAttributes(masterId, placeHolderType, placeHolderIndex) : null;
        // the master slide attributes
        const masterSlideAttrs = masterId ? this.docModel.getListStylesAttributesOfSlide(masterId, placeHolderType) : null;
        // the attributes of layout or master slide
        let attributes = null;
        // whether only the names shall be returned, not the definition of the list level styles
        const nameOnly = getBooleanOption(options, 'nameOnly', false);
        // a collector for the list level keys
        let allKeys = null;

        // using only the 'listStyle' property from the drawing attribute set
        layoutDrawingAttrs = layoutDrawingAttrs && layoutDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] ? layoutDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] : null;
        masterDrawingAttrs = masterDrawingAttrs && masterDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] ? masterDrawingAttrs[PresentationParagraphStyles.LISTSTYLE_NAME] : null;

        if (nameOnly) {

            allKeys = [];

            if (layoutDrawingAttrs) { allKeys = allKeys.concat(_.keys(layoutDrawingAttrs)); }
            if (layoutSlideAttrs) { allKeys = allKeys.concat(_.keys(layoutSlideAttrs)); }
            if (masterDrawingAttrs) { allKeys = allKeys.concat(_.keys(masterDrawingAttrs)); }
            if (masterSlideAttrs) { allKeys = allKeys.concat(_.keys(masterSlideAttrs)); }

            if (allKeys.length > 0) {
                attributes = {};
                _.each(allKeys, key => {
                    attributes[key] = 1;
                });
            }

        } else {

            // Merging all attributes for all list level -> this should be used with care because of performance reasons.
            // -> merging the collected attributes from master and layout slides and drawings (never overwrite attribute set)
            if (masterSlideAttrs) {
                _.each(_.keys(masterSlideAttrs), key => {
                    attributes = attributes || {};
                    if (!attributes[key]) { attributes[key] = {}; }
                    attributes[key] = this.extendAttrSet(attributes[key], masterSlideAttrs[key]);
                });
            }

            if (masterDrawingAttrs) {
                _.each(_.keys(masterDrawingAttrs), key => {
                    attributes = attributes || {};
                    if (!attributes[key]) { attributes[key] = {}; }
                    attributes[key] = this.extendAttrSet(attributes[key], masterDrawingAttrs[key]);
                });
            }

            if (layoutSlideAttrs) {
                _.each(_.keys(layoutSlideAttrs), key => {
                    attributes = attributes || {};
                    if (!attributes[key]) { attributes[key] = {}; }
                    attributes[key] = this.extendAttrSet(attributes[key], layoutSlideAttrs[key]);
                });
            }

            if (layoutDrawingAttrs) {
                _.each(_.keys(layoutDrawingAttrs), key => {
                    attributes = attributes || {};
                    if (!attributes[key]) { attributes[key] = {}; }
                    attributes[key] = this.extendAttrSet(attributes[key], layoutDrawingAttrs[key]);
                });
            }
        }

        return attributes;
    }

    /**
     * A helper function to set the valid paragraph and character
     * attributes to paragraphs inside tables.
     *
     * @param {Object} baseAttributeSet
     *  The attribute set specified for the given paragraph. For paragraphs
     *  inside tables this already includes the table specific attributes.
     *
     * @param {Object} placeHolderAttributeSet
     *  The collection of attributes that are collected in the function
     *  'resolveLayoutAttributes'. For paragraphs inside tables this are
     *  the 'other' attributes specified on the master slide for place
     *  holder drawings.
     */
    #handleParagraphInTableAttributes(baseAttributeSet, placeHolderAttributeSet) {

        // Some attributes are inherited from the table style and must not be overwritten, for
        // example font color, that is dependent from the position inside the table.
        // But other attributes are not inherited and also the default value should not be used.
        // This is the case for the font size, where the default of 11pt is too small. But the
        // default font color must not overwrite the value specified by the table style.
        // Unfortunately it is not clear at this point, which attributes are inherited from the
        // table and which are the default values.
        //
        // -> Removing those values from the place holder attributes, for that the given attributes
        //    are NOT the default values

        const defaultAttributeSet = this.attrPool.getDefaultValueSet(Object.keys(placeHolderAttributeSet));

        _.each(defaultAttributeSet, (defaultValues, family) => {

            const placeHolderAttrs = placeHolderAttributeSet[family];

            if (baseAttributeSet[family]) {
                // iterating over all place holder attributes and check if the given attribute set uses the default value
                _.each(placeHolderAttrs, (value, name) => {
                    if (!_.isEqual(baseAttributeSet[family][name], defaultValues[name])) {
                        delete placeHolderAttrs[name]; // -> not overwriting specified non-default value
                    }
                });
            }
        });
    }

    /**
     * Very special handling for paragraph attibutes that are assigned to the (subTitle) placeholder drawing.
     * This is a required process for ODP presentations, because place holder drawings must not contain an
     * empty paragraph, if they contain no content. It is necessary to remove the last paragraph together
     * with the last character. Therefore it is not possible, to save the paragraph attributes at the
     * paragraph node.
     *
     * @param {Object} placeHolderAttributes
     *  The full set of attributes of the place holder drawing.
     *
     * @param {Object} paraAttrs
     *  The paragraph attributes saved at the drawing node.
     */
    #handleODFParagraphAttibutes(placeHolderAttributes, paraAttrs, paraLevel) {
        let localParaAttrs = paraAttrs;
        if (placeHolderAttributes && placeHolderAttributes.paragraph) {
            if ((_.isNumber(paraLevel) && paraLevel > 0) || (placeHolderAttributes.paragraph.bullet && placeHolderAttributes.paragraph.bullet.type !== 'none')) { // check for ODP list
                localParaAttrs = _.copy(paraAttrs, true); // not overwriting "indentFirstLine" and "indentLeft" in lists with paragraph attributes at drawing (DOCS-3831)
                if (_.isNumber(placeHolderAttributes.paragraph.indentLeft) && _.isNumber(localParaAttrs.indentLeft)) { delete localParaAttrs.indentLeft; }
                if (_.isNumber(placeHolderAttributes.paragraph.indentFirstLine) && _.isNumber(localParaAttrs.indentFirstLine)) { delete localParaAttrs.indentFirstLine; }
            }
        }
        this.extendAttrSet(placeHolderAttributes, { paragraph: localParaAttrs });
    }

    /**
     * Very special handling for paragraph and character attributes that are assigned to the drawing that
     * contains the paragraph (ODF only, 51533).
     *
     * @param {Object} placeHolderAttributes
     *  The attribute set of the place holder elements in layout and/or master slide.
     *
     * @param {Object} expDrawingAttrs
     *  The explicit attributes assigned to the surrounding drawing.
     *
     * @param {String} family
     *  The attribute family.
     *
     * @param {Number} paraLevel
     *  The paragraph level.
     *
     * @returns {Object}
     *  The new or modified set of attributes.
     */
    #handleODFExplicitDrawingAttributes(placeHolderAttributes, expDrawingAttrs, family, paraLevel) {

        // an object with the explicit paragraph attributes
        const attrs = {};

        attrs[family] = _.copy(expDrawingAttrs, true);

        // Not simply modifying all character and paragraph attributes for all levels by using the drawing attributes (maybe further attributes need to be added here) 56607
        if (placeHolderAttributes) {
            if ((_.isNumber(paraLevel) && paraLevel > 0) || (placeHolderAttributes.paragraph && placeHolderAttributes.paragraph.bullet && placeHolderAttributes.paragraph.bullet.type !== 'none')) { // check for ODP list
                if (family === 'character' && placeHolderAttributes.character && placeHolderAttributes.character.fontSize && attrs.character.fontSize) {
                    delete attrs.character.fontSize;
                }
                if (family === 'paragraph' && placeHolderAttributes.paragraph) {
                    if (_.isNumber(placeHolderAttributes.paragraph.indentLeft) && _.isNumber(attrs.paragraph.indentLeft)) { delete attrs.paragraph.indentLeft; }
                    if (_.isNumber(placeHolderAttributes.paragraph.indentFirstLine) && _.isNumber(attrs.paragraph.indentFirstLine)) { delete attrs.paragraph.indentFirstLine; }
                }
            }
        }

        if (placeHolderAttributes) {
            this.extendAttrSet(placeHolderAttributes, attrs);
        } else {
            placeHolderAttributes = attrs;
        }

        return placeHolderAttributes;
    }

    /**
     * Returns the attributes of master and layout slide for the passed
     * base attribute set of a paragraph.
     *
     * @param {jQuery} paragraph
     *  The paragraph node, as jQuery object.
     *
     * @param {Object} baseAttributeSet
     *  The base attribute set already collected for the passed paragraph
     *  element, e.g. from a parent table cell.
     *
     * @param {Object} [newAttributes]
     *  An optional object containing the new attributes that will be assigned
     *  to the paragraph. This is especially important for resolving the
     *  attributes from layout or master slide, if the paragraph level has
     *  changed (resolveLayoutAttributes is called before the new attributes
     *  are assigned to the paragraph).
     */
    #resolveLayoutAttributes(paragraph, baseAttributeSet, newAttributes) {

        // the attribute set of the place holder elements in layout and/or master slide
        let placeHolderAttributes = null;
        // the IDs of document slide, layout slide and master slide
        let id = null;
        let layoutId = null;
        let masterId = null;
        // the place holder type and index
        let placeHolderType = null;
        let placeHolderIndex = null;
        // the closest drawing node upwards in the dom
        const drawing = paragraph.closest('div.drawing'); // TODO: Group handling
        // the explicit drawing attributes
        let drawingAttrs = null;
        // the paragraph level attribute (and a save for its value)
        let paraLevel = 0;
        let paraLevelSav = 0;
        // whether the paragraph is inside a table (attributes can be defined at table, never overwrite)
        const insideTable = isCellContentNode(paragraph.parent());

        // check, if the drawing, containing the paragraph, is a 'placeholder' drawing
        if (drawing.length > 0) { drawingAttrs = getExplicitAttributeSet(drawing, true); }

        paraLevel = (newAttributes && newAttributes.paragraph && _.isNumber(newAttributes.paragraph.level)) ? newAttributes.paragraph.level : this.getParagraphLevel(paragraph);
        paraLevelSav = paraLevel;
        paraLevel = this.docModel.generateListKeyFromLevel(paraLevel);

        if (isPlaceHolderAttributeSet(drawingAttrs) && !insideTable) { // place holder, but not in table

            id = getTargetContainerId(drawing.parent()); // checking only direct children of slide
            placeHolderType = drawingAttrs.presentation.phType;
            placeHolderIndex = drawingAttrs.presentation.phIndex;

            if (placeHolderType && !placeHolderIndex) { placeHolderIndex = DEFAULT_PLACEHOLDER_INDEX; }
            if (placeHolderIndex && !placeHolderType) { placeHolderType = DEFAULT_PLACEHOLDER_TYPE; }

            if (this.docModel.isStandardSlideId(id)) {
                layoutId = this.docModel.getLayoutSlideId(id);
                masterId = this.docModel.getMasterSlideId(layoutId);
                placeHolderAttributes = this.#getMergedPlaceHolderAttributes(layoutId, masterId, placeHolderType, placeHolderIndex, paraLevel);
                if (this.docModel.getApp().isODF() && _.isObject(drawingAttrs.paragraph)) { this.#handleODFParagraphAttibutes(placeHolderAttributes, drawingAttrs.paragraph, paraLevelSav); }
            } else if (this.docModel.isLayoutSlideId(id)) {
                masterId = this.docModel.getMasterSlideId(id);
                placeHolderAttributes = this.#getMergedPlaceHolderAttributes(id, masterId, placeHolderType, placeHolderIndex, paraLevel);
            } else if (this.docModel.isMasterSlideId(id)) {
                placeHolderAttributes = this.#getMergedPlaceHolderAttributes(null, id, placeHolderType, placeHolderIndex, paraLevel);
            }
        } else if (insideTable) {

            id = getTargetContainerId(drawing.parent()); // checking only direct children of slide
            masterId = this.docModel.getCorrespondingMasterSlideId(id);
            // using the 'other' attributes from master slide
            placeHolderAttributes = this.docModel.getListStylesAttributesOfSlide(masterId, this.docModel.getDefaultPlaceHolderListStyleType());

            if (placeHolderAttributes && placeHolderAttributes[paraLevel]) {
                placeHolderAttributes = placeHolderAttributes[paraLevel];
            } else {
                placeHolderAttributes = this.#getMergedNonPlaceHolderAttributes(drawing, paraLevel); // fallback to non-placeholder default attributes
            }

            // merge handling for paragraphs inside tables
            if (placeHolderAttributes) {
                this.#handleParagraphInTableAttributes(baseAttributeSet, placeHolderAttributes);
            }

        } else {

            // using the default text list styles
            // -> these values will be used for shapes or text frames
            // -> additionally there can be list styles hard defined at the drawing (45279)
            placeHolderAttributes = this.#getMergedNonPlaceHolderAttributes(drawing, paraLevel);
        }

        // adding the paragraph and character attributes, that are explicitely assigned to the drawing node
        if (this.docModel.getApp().isODF() && drawingAttrs) {
            if (drawingAttrs.paragraph) { placeHolderAttributes = this.#handleODFExplicitDrawingAttributes(placeHolderAttributes, drawingAttrs.paragraph, 'paragraph', paraLevelSav); }
            if (drawingAttrs.character) { placeHolderAttributes = this.#handleODFExplicitDrawingAttributes(placeHolderAttributes, drawingAttrs.character, 'character', paraLevelSav); }
        }

        return placeHolderAttributes;
    }
}

// constants --------------------------------------------------------------

/**
 * Default solid border line for paragraphs and tables.
 * 'auto' is not supported in Presentation app.
 * The 'fallbackValue' is specified for ODF.
 */
PresentationParagraphStyles.SINGLE_BORDER = { style: 'single', width: 17, space: 140, color: { ...Color.TEXT1, fallbackValue: '000000' } };

/**
 * The name of the property in the drawing attributes, that contains the list style definitions.
 */
PresentationParagraphStyles.LISTSTYLE_NAME = 'listStyle';
