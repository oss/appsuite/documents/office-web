/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { Rectangle } from '@/io.ox/office/tk/dom';
import { convertCssLength, convertHmmToLength, convertLengthToHmm } from '@/io.ox/office/tk/utils';
import { pointInsideRect, rotatePointWithAngle } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { getCanvasNode, getGroupNode, getFarthestGroupNode, getTextFrameNode, isGroupDrawingFrame, isGroupedDrawingFrame, isShapeDrawingFrame,
    isTableDrawingFrame, isTextFrameShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { isParagraphNode, isTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { getLastTextPositionInTableCell, getLastTextPositionInTextFrame, getPositionInsideParagraph } from '@/io.ox/office/textframework/utils/position';
import { isPlaceHolderAttributeSet, isPlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';

// constants ==============================================================

export const PLACEHOLDER_DRAWING_IMPORTANT_ATTRS = ["left", "top", "width", "height", "rotation", "flipH", "flipV"];

// methods ----------------------------------------------------------------

/**
 * This function converts the properties of a rectangle object ('top', 'left',
 * 'width' and 'height'). Specified is a rectangle that has values in pixel
 * relative to the app-content-root node. This is for example created by the
 * selectionBox. All properties are converted into 1/100 mm relative to the
 * slide. Therefore the returned values can directly be used for the creation
 * of operations. The zoom level is also taken into account.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {Object} box
 *  The rectangle with the properties 'top', 'left', 'width' and 'height'.
 *  The values are given in pixel relative to the app-content-root node.
 *
 * @param {Boolean} [notScaleBoxSize=false]
 *  Optional flag to prevent the scaling from the box size. The default is false,
 *  so when it's not provided the box is not scaled. This is useful when a box is
 *  not based on the px size from the selection box (created with the mouse), but
 *  with fixed values. These fixed values are not depended on the scaling and
 *  therefore must not be scaled.
 *
 *
 * @returns {Object}
 *  The rectangle with the properties 'top', 'left', 'width' and 'height'.
 *  The values are given in 1/100 mm relative to the slide.
 */
export function convertAppContentBoxToOperationBox(app, box, notScaleBoxSize) {

    var // the current zoom factor
        zoomFactor = app.getView().getZoomFactor(),
        // the active slide node
        activeSlide = app.getModel().getSlideById(app.getModel().getActiveSlideId()),
        // the offset of the active slide inside the content root node
        slideOffset = activeSlide.offset(), // the slide is positioned absolutely
        // the content root node, the base for the position calculation
        contentRootNode = app.getView().getContentRootNode(),
        // the position of the content root node
        pos = contentRootNode.offset(),
        // the horizontal scroll shift
        scrollLeft = contentRootNode.scrollLeft(),
        // the vertical scroll shift
        scrollTop = contentRootNode.scrollTop(),
        // the new box object
        operationBox = {},
        // flag if the width/height from the box should be scaled by the zoomFactor or not
        notScaleBoxSizeFlag = _.isBoolean(notScaleBoxSize) ? notScaleBoxSize : false;

    // the left position of the user defined box relative to the slide (can be negative)
    operationBox.left = (box.left - (slideOffset.left - pos.left) - scrollLeft) / zoomFactor;
    // the top position of the user defined box relative to the slide (can be negative)
    operationBox.top = (box.top - (slideOffset.top - pos.top) - scrollTop) / zoomFactor;
    // the width of the box
    operationBox.width = notScaleBoxSizeFlag ? box.width : Math.round(box.width / zoomFactor);
    // the height of the box
    operationBox.height = notScaleBoxSizeFlag ? box.height : Math.round(box.height / zoomFactor);

    // converting to 1/100 mm
    operationBox.left = convertLengthToHmm(operationBox.left, 'px');
    operationBox.top = convertLengthToHmm(operationBox.top, 'px');
    operationBox.width = convertLengthToHmm(operationBox.width, 'px');
    operationBox.height = convertLengthToHmm(operationBox.height, 'px');

    //workaround for Bug 52977, TODO: opBox is broken when zoomed out!!!
    if (operationBox.left < 0) { operationBox.left = 0; }

    return operationBox;
}

/**
 * Helper function, for extending operation with the drawing-attributes listed
 * in PLACEHOLDER_DRAWING_IMPORTANT_ATTRS.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node to be selected, as DOM node or jQuery object.
 *
 * @param {Object} operationProperties
 *  the operation properties to generate operations. 'attrs.drawing' must be set
 */
export function extendPlaceholderProp(app, drawingNode, operationProperties) {
    if (operationProperties && operationProperties.attrs && operationProperties.attrs.drawing) {
        var expAttrs = getExplicitAttributeSet(drawingNode);
        if (isPlaceHolderAttributeSet(expAttrs)) {
            var mergedDrawing = app.docModel.drawingStyles.getElementAttributes(drawingNode).drawing;
            _.each(mergedDrawing, function (mAttr, mKey) {

                if (!(mKey in operationProperties.attrs.drawing) &&
                    _.contains(PLACEHOLDER_DRAWING_IMPORTANT_ATTRS, mKey) &&
                    !(mKey in expAttrs.drawing)) {
                    operationProperties.attrs.drawing[mKey] = mAttr;
                }

            });
        }
    }
}

/**
 * Helper function that checks, if the specified drawing can handle the specified event.
 * This is the case, if the click happens for example into a text frame or into a range
 * that is filled by a canvas. But this is not the case, if the click happened next to
 * the filled range inside a canvas, for example next to a line.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {Event|jQuery.Event} event
 *  A browser event, expected to contain the numeric properties 'pageX'
 *  and 'pageY'.
 *
 * @param {jQuery} drawing
 *  The drawing node that is checked, if it has to handle the event.
 *
 * @param {Object} [drawingAttributes]
 *  The attributes of the family 'drawing' of the specified drawing node. For performance
 *  reasons this attributes can set as parameter, if they are already available in the
 *  calling function.
 *
 * @returns {Boolean}
 *  Whether the specified drawing has to handle the specified event.
 */
export function checkEventPosition(app, event, drawing, drawingAttributes) {

    // whether the specified drawing can handle this event
    var handleEvent = true;
    // the tolerance, for example the distance of the click event to a line
    var radius = 6;
    // the model object
    var model = app.getModel();
    // the width of the range that is checked in the canvas node
    var rangeX = null;
    // the height of the range that is checked in the canvas node
    var rangeY = null;

    // several scenarios for fast exit
    // TODO: All nodes inside a text frame, not only text spans and paragraphs
    if (isTextSpan(event.target) || isParagraphNode(event.target)) { return handleEvent; }

    // allow moving of drawings without filling and without border, for example text frames
    if (event.target && $(event.target.parentNode).is('.borders')) { return handleEvent; }

    // place holders drawings always handle the event
    if (isPlaceHolderDrawing(drawing)) { return handleEvent; }

    // TODO: handle grouped drawings
    // if (isGroupedDrawingFrame(drawing)) { return handleEvent; }

    if (isGroupDrawingFrame(drawing)) { return !handleEvent; }

    // check only shapes, connectors and groups. All other drawings must be handled (this includes 'groups' (<- TODO))
    if (!isShapeDrawingFrame(drawing)) { return handleEvent; }

    // the canvas node inside the drawing
    var canvasNode = getCanvasNode(drawing);
    if (!canvasNode || canvasNode.length === 0) { return handleEvent; } // fast exit, drawings without canvas are always handled

    // getting explicit or merged attributes (TODO)
    var drawingAttrs = drawingAttributes || getExplicitAttributes(drawing, 'drawing');
    // the offset of the slide relative to the window (offset() can be used, because slide is never rotated)
    var slideOffset = model.getActiveSlide().offset();
    // horizontal event position relative to slide
    var eventX = 0;
    // vertical event position relative to slide
    var eventY = 0;
    // the left offset of the drawing (in px)
    var drawingLeft = 0;
    // the top offset of the drawing (in px)
    var drawingTop = 0;
    // the width of the canvas node
    var drawingWidth = 0;
    // the height of the canvas node
    var drawingHeight = 0;
    // whether the drawing is flipped horizontally
    var flipH = drawingAttrs.flipH;
    // whether the drawing is flipped vertically
    var flipV = drawingAttrs.flipV;
    // generating drawing and event position normalizing grouping and rotation
    var drawingObj = normalizeRotationAndGrouping(app, drawing, event, slideOffset);
    // a rectangle object required for rotation calculation (containing position and size of the canvas node)
    var rect = drawingObj.rect;

    // TODO: On small devices the point cannot be determined sometimes -> drawing must handle the event
    if (!drawingObj.point) { return handleEvent; }

    // the calculated drawing values including grouping and rotation
    drawingLeft = rect.left;
    drawingTop = rect.top;
    drawingWidth = rect.width;
    drawingHeight = rect.height;

    // the calculated event position including rotation
    eventX = drawingObj.point.left;
    eventY = drawingObj.point.top;

    // event position relative to drawing
    eventX -= drawingLeft;
    eventY -= drawingTop;

    if (flipH) { eventX = drawingWidth - eventX; }
    if (flipV) { eventY = drawingHeight - eventY; }

    rangeX = Math.max(eventX - radius, 0);
    rangeY = Math.max(eventY - radius, 0);

    // the context of the canvas node
    var context = canvasNode[0].getContext('2d', { willReadFrequently: true });
    // the image data inside the specified range
    var imageData = context.getImageData(rangeX, rangeY, 2 * radius, 2 * radius);

    // do not handle the event, if all values inside imageData are 0
    handleEvent = !_.every(imageData.data, function (num) { return num === 0; });

    // Debug code to visualize the handled range
    //        if (event.type === 'mousedown') {
    //            var length = imageData.data.length;
    //            var j = 0;
    //            for (j = 0; j < length; j += 1) { imageData.data[j] = 128; } // show range
    //            context.putImageData(imageData, rangeX, rangeY);
    //        }

    return handleEvent;
}

/**
 * Helper function that tries to find a drawing node that can handle the specified event
 * and that is located behind a specified drawing node. The specified drawing node is for
 * example a line drawing node, that catched the event, but cannot handle it, because the
 * click was next to the line. Therefore all underlying drawings are investigated, if they
 * can handle the event. This is the case, if the event position is inside a text frame or
 * inside a canvas node with filled content.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {Event|jQuery.Event} event
 *  A browser event, expected to contain the numeric properties 'pageX'
 *  and 'pageY'.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node for that is checked, if any of its underlying drawing nodes have to
 *  handle the event.
 *
 * @returns {Object|Null}
 *  An object containing the keys 'drawing' and 'textFrameNode'. If a drawing is found, that
 *  handles the event, the drawing node is available at the key 'drawing'. If a textframe
 *  inside this drawing can handle this event, the text frame node is saved with the key
 *  'textFrameNode'. If no drawing is found, null is returned.
 */
export function findValidDrawingNodeBehindDrawing(app, event, drawingNode) {

    // the counter for the drawings
    var counter = 0;
    // whether a drawing was found, that can handle the event
    var foundDrawing = false;
    // whether the drawing node is a grouped drawing
    var isGroupedDrawingNode = isGroupedDrawingFrame(drawingNode);
    // the drawing node, whose parent is the slide
    var topLevelDrawing = isGroupedDrawingNode ? getFarthestGroupNode(app.docModel.getCurrentRootNode(), drawingNode) : drawingNode;
    // all underlying drawings are the previous drawings in the DOM
    var allDrawingsBehind = topLevelDrawing.prevAll('.drawing');
    // the number of underlying drawings
    var maxCounter = allDrawingsBehind.length;
    // the drawing object as returned from 'checkEventPositionInDrawing'
    var drawingObj = null;

    // iterating over all underlying drawings
    while (counter < maxCounter && !foundDrawing) {

        // one drawing behind the specified drawing
        var oneDrawing = $(allDrawingsBehind[counter]);
        counter++; // increasing the counter for next drawing

        drawingObj = checkEventPositionInDrawing(app, event.pageX, event.pageY, oneDrawing);

        if (drawingObj) { foundDrawing = true; }
    }

    return drawingObj;
}

/**
 * Helper function that checks, if an event, that happened at position eventX and eventY (in px relative
 * to the slide) must be handled by the specified drawing.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {Number} eventPageX
 *  The horizontal event position in pixel like event.pageX.
 *
 * @param {Number} eventPageY
 *  The vertical event position in pixel like event.pageY.
 *
 * @param {jQuery} oneDrawing
 *  The drawing node for that is checked, if it has to handle the event.
 *
 * @returns {Object|Null}
 *  An object containing the keys 'drawing' and 'textFrameNode' and 'info'. If the specified drawing (or one
 *  of its child drawings) handles this event, the drawing node is available at the key 'drawing'. If a textframe
 *  inside this drawing can handle this event, the text frame node is saved with the key 'textFrameNode'. If
 *  'textFrameNode' is specified, an additional 'info' object is specified, that contains the calculated data
 *  for following more precise calculations (for example to calculate a precise text position).
 *  If no drawing is found, null is returned.
 */
export function checkEventPositionInDrawing(app, eventPageX, eventPageY, oneDrawing) {

    // the model object
    var model = app.getModel();
    // the slide offset relative to the browser window (offset() can be used, because slide is not rotated)
    var slideOffset = model.getActiveSlide().offset();
    // getting explicit or merged attributes
    var drawingAttrs = getExplicitAttributes(oneDrawing, 'drawing');
    // getting the rectangle for the drawing
    var drawingObj = normalizeRotationAndGrouping(app, oneDrawing, { pageX: eventPageX, pageY: eventPageY }, slideOffset);
    // a rectangle object of the drawing (or the expanded canvas)
    var rect = drawingObj.rect;
    // a rectangle object of the drawing
    var drawingRect = drawingObj.drawingRect;
    // the horizontal event position relative to slide
    var eventX = drawingObj.point.left;
    // the vertical event position relative to slide
    var eventY = drawingObj.point.top;
    // the current zoom factor
    var zoomFactor = app.getView().getZoomFactor();
    // a text frame node inside the drawing
    var textFrameNode = null;
    // the drawing node, that can handle the specified event
    var validDrawing = null;
    // whether a drawing was found, that can handle the event
    var foundDrawing = false;
    // an info object containing position of cell or text frame and event position
    var info = null;

    // helper function to get a rectangle for the specified text frame
    function getTextFrameRectangle(textFrameNode, drawingRect, drawingAttrs) {

        // the expansion of the canvas to the left out of the drawing in px
        var textFrameLeft = Math.round(convertCssLength(textFrameNode.css('left'), 'px'));
        // the expansion of the canvas to the top out of the drawing in px
        var textFrameTop = Math.round(convertCssLength(textFrameNode.css('top'), 'px'));
        // the width of the text frame node
        var textFrameWidth = textFrameNode.width();
        // the height of the text frame node
        var textFrameHeight = textFrameNode.height();
        // whether the drawing is flipped horizontally
        var flipH = drawingAttrs.flipH;
        // whether the drawing is flipped vertically
        var flipV = drawingAttrs.flipV;

        if (flipH) { textFrameLeft = (drawingRect.width - textFrameWidth - textFrameLeft); }
        if (flipV) { textFrameTop = (drawingRect.height - textFrameHeight - textFrameTop); }

        textFrameLeft += drawingRect.left;
        textFrameTop += drawingRect.top;

        return new Rectangle(textFrameLeft, textFrameTop, textFrameWidth, textFrameHeight);
    }

    // Helper function to handle drawings inside groups
    function handleOneDrawingInGroup(oneGroupedDrawing) {

        // skipping over all internal group drawings
        if (isGroupDrawingFrame(oneGroupedDrawing)) { return; }

        oneGroupedDrawing = $(oneGroupedDrawing);

        // a rectangle object required for rotation calculation
        var drawingObj = normalizeRotationAndGrouping(app, oneGroupedDrawing, { pageX: eventPageX, pageY: eventPageY }, slideOffset);
        // the position and size of the drawing node (including the expanded canvas)
        var canvasRect = drawingObj.rect;
        // the position and size of the drawing node (not the expanded canvas)
        var drawingRect = drawingObj.drawingRect;

        // the calculated event position including rotation
        eventX = drawingObj.point.left;
        eventY = drawingObj.point.top;

        // check, if the event happened inside the drawing node
        // getting explicit or merged attributes
        var groupedDrawingAttrs = getExplicitAttributes(oneGroupedDrawing, 'drawing');

        // check, if the event position is located inside the drawing rectangle
        if (pointInsideRect({ x: eventX, y: eventY }, canvasRect, 6)) {
            checkEventPositionInsideDrawing(oneGroupedDrawing, groupedDrawingAttrs, drawingRect);
        }
    }

    // Helper function for tables -> no grouping, no rotation
    // But the valid table cell must be determined
    function checkEventPositionInsideTable(oneDrawing) {

        // a rectangle object with the data for the table cell
        var cellRect = null;
        // the cell node, that contains the event position
        var cell = _.find(oneDrawing.find('div.cell'), function (oneCell) {

            oneCell = $(oneCell);

            var cellOffset = oneCell.offset();
            var cellLeft = (cellOffset.left - slideOffset.left) / zoomFactor;
            var cellTop = (cellOffset.top - slideOffset.top) / zoomFactor;
            cellRect = new Rectangle(cellLeft, cellTop, oneCell.width(), oneCell.height());

            return pointInsideRect({ x: eventX, y: eventY }, cellRect, 1);
        });

        if (cell) {
            validDrawing = oneDrawing;
            textFrameNode = cell; // using cell as textFrameNode -> will be used for logical position and cursor type
            foundDrawing = true;
            info = { rect: cellRect, posX: eventX, posY: eventY }; // can be used for more precise calculations later
        }

    }

    // Helper function to check, if a click (or another event) happened inside a specified drawing.
    // The event might be inside a text frame or inside the canvas content. But if the event
    // position is outside of the canvas content (and inside the drawing), the value for
    // foundDrawing will be false.
    function checkEventPositionInsideDrawing(oneDrawing, drawingAttrs, oneRect) {

        // check, if the click happened inside a text frame in the drawing
        // -> for this the function 'checkEventPosition' does not need to
        //    return true, because the drawing might have no filling
        // This test is needed for two reasons:
        // 1. 'checkEventPosition' returns false, because the drawing has no
        //    filling. But the click might have happened into an existing
        //    textFrame, so that this drawing needs to handle this event.
        // 2. 'checkEventPosition' returns true. Then it is the question, if
        //    the drawing shall be selected or the text inside the drawing.
        // In both cases the drawing must handle the event.
        if (isTextFrameShapeDrawingFrame(oneDrawing)) {

            // does the drawing contain a text frame node -> was the click inside this text frame node?
            textFrameNode = getTextFrameNode(oneDrawing);

            if (textFrameNode) {

                // getting a rectangle object for the text frame
                // -> using the smaller drawingRect (not rect!), so that drawing position is used and not position of expanded canvas
                var textFrameRect = getTextFrameRectangle(textFrameNode, oneRect, drawingAttrs);

                if (pointInsideRect({ x: eventX, y: eventY }, textFrameRect, 1)) {
                    validDrawing = oneDrawing;
                    foundDrawing = true;
                    info = { rect: textFrameRect, posX: eventX, posY: eventY }; // can be used for more precise calculations later
                } else {
                    textFrameNode = null;
                }
            }
        }

        // if the click did not happen inside a text frame node, the canvas content need to be checked
        if (!foundDrawing) {
            // this might be a valid drawing (but maybe this is also a line and the click was next to the shape).
            if (checkEventPosition(app, { pageX: eventPageX, pageY: eventPageY, target: null }, oneDrawing, drawingAttrs)) {
                validDrawing = oneDrawing;
                foundDrawing = true;
            }
        }

    }

    // check, if the event position is located inside the drawing rectangle (no further rotation required)
    if (pointInsideRect({ x: eventX, y: eventY }, rect, 6)) {
        // handling groups
        if (isGroupDrawingFrame(oneDrawing)) {
            // check all children of the group, if they can handle the event
            _.each(oneDrawing.find('.drawing'), handleOneDrawingInGroup); // collecting all drawings in all levels
        } else if (isTableDrawingFrame(oneDrawing)) {
            checkEventPositionInsideTable(oneDrawing, drawingAttrs, rect);
        } else {
            checkEventPositionInsideDrawing(oneDrawing, drawingAttrs, drawingRect);
        }
    }

    return foundDrawing ? { drawing: validDrawing, textFrameNode, info } : null;
}

/**
 * A helper function that can be used to calculate the event position and the position of a
 * drawing normalized corresponding to grouping and rotation. This is typically used to
 * determine, whether the position of a specified event is located inside the specified
 * drawing.
 *
 * Info: This function should work independent from the application (tested yet only for
 *       Presentation app).
 * Info: Using jQuery offset() does not work for rotated drawings. It returns the left and
 *       top position of an unrotated rectangle generated around the rotated drawing.
 *
 * Using the properties of the returning object of this function, it is easy to check,
 * whether the point specified by 'pageX' and 'pageY' in the event object is located
 * inside the specified drawing. The function DrawingUtils.pointInsideRect() can be used
 * without handling any further rotation, grouping or expanded canvas node.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {jQuery} drawing
 *  The drawing node whose position and size is calculated and for that the event position
 *  is modified caused by rotation.
 *
 * @param {Event|jQuery.Event} [event]
 *  Optinally a browser event or another object that is expected to contain the numeric
 *  properties 'pageX' and 'pageY'. The coordinates of this point are adapted corresponding
 *  to the rotation of the drawing and all its parent group drawins.
 *
 * @param {Object} [offset]
 *  An optional offset for the positions of drawing and event. This offset object has to
 *  contain the properties 'left' and 'top' that contain the offset value in pixel. This
 *  can be used for example to calculate the positions relative to the slide in the
 *  presentation app.
 *
 * @returns {Object}
 *  An object containing the properties 'rect', 'drawingRect' and 'point'.
 *  The value of 'rect' is a rectangle object, that describes the position and the size of
 *  the drawing (for example relative to the slide), or, if it exists, the position and size
 *  of the expanded canvas node inside the drawing.
 *  The value of 'drawingRect' is a rectangle object, that describes the position and the size
 *  of the drawing (for example relative to the slide) and not of an optionally existing
 *  expanded canvas node inside the drawing. Therefore the rectangle described by
 *  'drawingRect' might be smaller than the rectangle described by 'rect'.
 *  The value of 'point' is an object containing the properties 'left' and 'top' that
 *  contain the normalized position of the event (in pixel) relative to the specified
 *  offset (for example the slide). If no event is specified, the values for 'left' and
 *  'top' are set to 0. If the point cannot be determined, null is returned for this property.
 */
export function normalizeRotationAndGrouping(app, drawing, event, offset) {

    // the zoom factor
    var zoomFactor = app.getView().getZoomFactor();
    // whether the drawing frame is inside a group
    var isGroupedDrawing = isGroupedDrawingFrame(drawing);
    // whether a event object is specified
    var handleEventPosition = !!event;
    // the offset that is used for the event position, for example the slide offset
    var localOffset = offset ? offset : { left: 0, top: 0 };
    // the vertical event position
    var eventX = 0;
    // the horizontal event position
    var eventY = 0;
    // the left position of the group(s) in pixel
    var groupLeft = 0;
    // the top position of the group(s) in pixel
    var groupTop = 0;
    // a helper point for the event after rotation is assigned
    var point = 0;
    // whether the event contains all required information
    var isEventProblem = false;
    // getting explicit or merged attributes (TODO)
    var drawingAttrs = getExplicitAttributes(drawing, 'drawing');
    // an optional rotation angle of the drawing
    var rotationAngle = drawingAttrs.rotation;
    // whether the drawing has a canvas expansion node
    var hasCanvasExpansion = drawing.children('.canvasexpansion').length > 0;
    // a rectangle object for the drawing node
    var drawingRect = null;
    // a rectangle object for the drawing node, or, if available the expanded canvas node
    var rect = null;

    // helper function to get a rectangle for a specified drawing
    function getDrawingRectangle(oneDrawing, drawingAttrs) {

        // the left offset of the drawing (in px)
        var drawingLeft = 0;
        // the top offset of the drawing (in px)
        var drawingTop = 0;
        // whether this is a grouped drawing
        var isGroupedDrawing = isGroupedDrawingFrame(oneDrawing);

        if (!_.isNumber(drawingAttrs.left) || !_.isNumber(drawingAttrs.top)) {  // TODO: better specification
            drawingAttrs = app.docModel.drawingStyles.getElementAttributes(oneDrawing).drawing;
        }

        if (isGroupedDrawing) {
            // the values in the attributes cannot be used for grouped drawings
            drawingLeft = Math.round(convertCssLength(oneDrawing.css('left'), 'px'));
            drawingTop = Math.round(convertCssLength(oneDrawing.css('top'), 'px'));
        } else {
            drawingLeft = convertHmmToLength(drawingAttrs.left, 'px', 1);
            drawingTop = convertHmmToLength(drawingAttrs.top, 'px', 1);
        }

        drawingLeft += groupLeft;
        drawingTop += groupTop;

        // the width of the drawing, respectively the expanded canvas
        var drawingWidth = oneDrawing.width();
        // the height of the drawing, respectively the expanded canvas
        var drawingHeight = oneDrawing.height();

        return new Rectangle(drawingLeft, drawingTop, drawingWidth, drawingHeight);
    }

    // helper function for handling an expanded canvas inside the drawing.
    function handleExpandedCanvas(rect, drawingAttrs, canvasNode) {

        // the width of the canvas node in the drawing
        var canvasWidth = canvasNode.width();
        // the height of the canvas node in the drawing
        var canvasHeight = canvasNode.height();
        // whether the drawing is flipped horizontally
        var flipH = drawingAttrs.flipH;
        // whether the drawing is flipped vertically
        var flipV = drawingAttrs.flipV;
        // the expansion of the canvas to the left in the drawing
        var leftExpansion = 0;
        // the expansion of the canvas to the top in the drawing
        var topExpansion = 0;

        if (flipH) {
            leftExpansion = -(canvasWidth - rect.width + Math.round(convertCssLength(canvasNode.css('left'), 'px'))); // calculating right expansion
        } else {
            leftExpansion = Math.round(convertCssLength(canvasNode.css('left'), 'px')); // simply using left expansion
        }

        if (flipV) {
            topExpansion = -(canvasHeight - rect.height + Math.round(convertCssLength(canvasNode.css('top'), 'px'))); // calculating bottom expansion
        } else {
            topExpansion = Math.round(convertCssLength(canvasNode.css('top'), 'px')); // simply using top expansion
        }

        // saving position and size of the drawing (will be returned by this function)
        drawingRect = _.copy(rect);

        rect.left += leftExpansion; // handling the expansion to the left
        rect.top += topExpansion; // handling the expansion to the top
        rect.width = canvasWidth; // using canvas size instead of drawing size
        rect.height = canvasHeight; // using canvas size instead of drawing size
    }

    // helper function to resolve all groups and rotations in groups
    function handleGroupingAndRotation(drawing) {

        // a collector for all group nodes
        var groupNodeCollector = [];

        groupNodeCollector.push(getGroupNode(drawing));

        // collecting all group drawings
        while (isGroupedDrawingFrame(_.last(groupNodeCollector))) {
            groupNodeCollector.push(getGroupNode(_.last(groupNodeCollector)));
        }

        groupNodeCollector = groupNodeCollector.reverse(); // reverting the order, starting with the top level group node

        // reverting rotation from outer drawing to inner drawing
        _.each(groupNodeCollector, function (groupNode) {

            var groupAttrs = getExplicitAttributes(groupNode, 'drawing');
            var groupRect = null;

            if (handleEventPosition && _.isNumber(groupAttrs.rotation)) {
                groupRect = getDrawingRectangle(groupNode, groupAttrs);
                point = rotatePointWithAngle(groupRect.centerX(), groupRect.centerY(), eventX, eventY, groupAttrs.rotation); // point relative to unrotated group
                eventX = point.left; // modifying the event position with every group
                eventY = point.top; // modifying the event position with every group
            }

            // adding all top and left values of groups (for grouped drawings the attributes cannot be used)
            groupLeft += Math.round(convertCssLength(groupNode.css('left'), 'px'));
            groupTop += Math.round(convertCssLength(groupNode.css('top'), 'px'));
        });
    }

    // calculating the event position relative to the specified offset
    if (handleEventPosition) {
        if (!_.isNumber(event.pageX) || !_.isNumber(event.pageY)) {
            isEventProblem = true; // TODO: Happens on small devices
        } else {
            eventX = (event.pageX - localOffset.left) / zoomFactor;
            eventY = (event.pageY - localOffset.top) / zoomFactor;
        }
    }

    // handling grouped drawings
    if (isGroupedDrawing) { handleGroupingAndRotation(drawing); }

    rect = getDrawingRectangle(drawing, drawingAttrs);

    // handling an optional rotation
    if (handleEventPosition && _.isNumber(rotationAngle) && rotationAngle !== 0) {
        // using the drawing node as rectangle to calculate rotation, not the canvas node, even if canvas expansion exists
        point = rotatePointWithAngle(rect.centerX(), rect.centerY(), eventX, eventY, rotationAngle);
        eventX = point.left;
        eventY = point.top;
    }

    // using the data of the canvas expansion to calculate position and size, if drawing is not large enough
    if (hasCanvasExpansion) { handleExpandedCanvas(rect, drawingAttrs, getCanvasNode(drawing)); }

    return { rect, drawingRect: drawingRect ? drawingRect : rect, point: (isEventProblem ? null : { left: eventX, top: eventY }) };
}

/**
 * A helper function that tries to calculate the best logical text position for an event
 * position, that is inside a specified table cell or text frame.
 *
 * @param {Application} app
 *  The application containing this instance.
 *
 * @param {jQuery} drawing
 *  The drawing node that contains the specified text frame or table cell node.
 *
 * @param {HtmlElement|jQuery} textFrameNode
 *  The text frame node or table cell, inside which the text position shall be calculated.
 *
 * @param {Object} [infoObject]
 *  The info object as returned by the function 'checkEventPositionInDrawing'.
 *  It contains the rectangle data for the cell or the text frame node and the calculated
 *  event position.
 *  info = { rect: textFrameRect, posX: eventX, posY: eventY };
 *
 * @returns {Number[]|Null}
 *  The calculated logical position or null, if it could not be determined.
 */
export function getBackgroundTextPosition(app, drawing, textFrameNode, infoObject) {

    // the calculated logical text position
    var pos = null;
    // the current root node
    var activeRootNode = app.getModel().getCurrentRootNode();

    textFrameNode = $(textFrameNode);

    if ((textFrameNode).parent().is('.emptytextframe')) {
        pos = getLastTextPositionInTextFrame(activeRootNode, drawing);
    } else {
        var allParagraphs = textFrameNode.find('div.p');
        var nodeOffset = textFrameNode.offset();

        _.each(allParagraphs, function (oneParagraph) {
            if (pos) { return; }
            var paraOffset = $(oneParagraph).offset();
            var paraX = (paraOffset.left - nodeOffset.left) / app.getView().getZoomFactor();
            var paraY = (paraOffset.top - nodeOffset.top) / app.getView().getZoomFactor();
            var posX = infoObject.posX - infoObject.rect.left - paraX;
            var posY = infoObject.posY - infoObject.rect.top - paraY;
            var point = getPositionInsideParagraph(activeRootNode, oneParagraph, posX, posY, app.getView().getZoomFactor() * 100);
            if (point && point.pos) { pos = point.pos; }
        });
    }

    // default is last position in text frame or table cell
    pos = pos || (isTableDrawingFrame(drawing) ? getLastTextPositionInTableCell(activeRootNode, textFrameNode) : getLastTextPositionInTextFrame(activeRootNode, drawing));

    return pos;
}
