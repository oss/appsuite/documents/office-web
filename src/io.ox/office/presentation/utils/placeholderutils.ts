/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import type { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import type { PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";

import { is } from "@/io.ox/office/tk/algorithms";
import { getExplicitAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { getTargetContainerId, isEmptyTextframe } from "@/io.ox/office/textframework/utils/dom";
import gt from "gettext";

// types ==================================================================

export type NoTextPlaceHolderTypes = typeof NO_TEXT_PLACEHOLDER_TYPE_COLLECTION[number];

export type AllPlaceHolderTypes = "title" | "ctrTitle" | "subTitle" | "body" | "sldNum" | "ftr" | "dt" | NoTextPlaceHolderTypes;

export type PlaceHolderTypesStringType = Record<AllPlaceHolderTypes, string>;

export type NoTextPlaceHolderFamilyType = Record<NoTextPlaceHolderTypes, "table" | "image" | null>;

export type PlaceHolderButtonValueType = Array<"picture" | "table" | "media" | "clipart" | "chart" | "smartart"> | null;

export type AllPlaceHolderButtonTypes = "contentBody" | AllPlaceHolderTypes;

export type PlaceHolderButtonTypesType = Record<AllPlaceHolderButtonTypes, PlaceHolderButtonValueType>;

interface PlaceHolderDescription {
    target?: string;
    type?: AllPlaceHolderTypes;
    index?: number;
}

/**
 * Type definition of attribute maps for the "presentation" attribute family.
 */
export interface PresentationAttributes {
    phType: AllPlaceHolderTypes;
    phIndex: number;
    customPrompt: boolean;
    userTransformed: boolean;
}

/**
 * Type definition of a presentation attribute set.
 */
export interface PresentationAttributesSet {
    presentation: PresentationAttributes;
}

/**
 * Partial presentation attribute set (all families and attributes are optional).
 */
export type PtPresentationAttributeSet = PtAttrSet<PresentationAttributesSet>;

// constants ==============================================================

/**
 * The default type for place holders in presentation attributes.
 */
export const DEFAULT_PLACEHOLDER_TYPE = "body";

/**
 * The table place holder type.
 */
export const TABLE_PLACEHOLDER_TYPE = "tbl";

/**
 * The image place holder type.
 */
export const IMAGE_PLACEHOLDER_TYPE = "pic";

/**
 * The collection of all placeholder types without text input.
 */
export const NO_TEXT_PLACEHOLDER_TYPE_COLLECTION = ["tbl", "pic", "media", "clipArt", "chart", "dgm"] as const;

/**
 * The default index for place holders in presentation attributes.
 */
export const DEFAULT_PLACEHOLDER_INDEX = 0;

/**
 * A name used to differentiate between the two place holder types "body".
 * There is a content body, if "body" is not set a explicit attribute and
 * a text body, if the phType is explicitely set to "body". This marker
 * can be used to mark content body place holders.
 */
export const CONTENTBODY = "contentbody";

/**
 * The title types for place holders title drawings.
 */
export const TITLE_PLACEHOLDER_TYPES = new Set<AllPlaceHolderTypes>(["title", "ctrTitle"]);

/**
 * The title types for place holders title and subtitle drawings.
 */
export const TITLE_OR_SUBTITLE_PLACEHOLDER_TYPES = new Set<AllPlaceHolderTypes>(["title", "ctrTitle", "subTitle"]);

/**
 * The place holder types, whose content cannot be modified or that cannot be
 * deleted on master slides in ODF.
 */
export const ODF_MASTER_READONLY_TYPES = new Set<AllPlaceHolderTypes>(["title", "body"]);

/**
 * The place holder drawing types, in that no text content can be inserted.
 */
export const NO_TEXT_PLACEHOLDER_TYPES = new Set<AllPlaceHolderTypes>(NO_TEXT_PLACEHOLDER_TYPE_COLLECTION);

/**
 * The types for place holders footer drawings.
 */
export const FOOTER_PLACEHOLDER_TYPES = new Set<AllPlaceHolderTypes>(["dt", "ftr", "sldNum"]);

/**
 * Defining the name of that attributes family that must be defined as explicit
 * attribute to a place holder drawing that cannot contain text.
 * If a place holder drawing of type "tbl" contains the "table" family in its
 * explicit attributes, it cannot be empty (in this case the template images for
 * inserting content must not be inserted). The same is true for a place holder
 * drawing of type "pic", if the explicit attributes contain the image family.
 */
export const NO_TEXT_PLACEHOLDER_CONTENT_PROPERTY: NoTextPlaceHolderFamilyType = {
    media: null,
    clipArt: "image",
    chart: null,
    dgm: null,
    tbl: "table",
    pic: "image"
};

/**
 * The default texts that are inserted, if the place holder drawing is empty.
 */
export const PLACEHOLDER_TEMPLATE_TEXTS: PlaceHolderTypesStringType = {
    body: gt("Click to add text"),
    title: gt("Click to add title"),
    ctrTitle: gt("Click to add title"),
    subTitle: gt("Click to add subtitle"),
    media: gt("Media clip"),
    clipArt: gt("Online image"),
    chart: gt("Chart"),
    dgm: gt("Smart Art"),
    tbl: gt("Click to add table"),
    pic: gt("Click to add image"),
    sldNum: gt("Slide Number"),
    ftr: gt("Footer"),
    dt: gt("Date")
};

/**
 * The default texts that are inserted, if the place holder drawing is empty (ODF format).
 */
export const PLACEHOLDER_TEMPLATE_TEXTS_ODF: PlaceHolderTypesStringType = {
    body: gt("Click to add text"),
    title: gt("Click to add title"),
    ctrTitle: gt("Click to add title"),
    subTitle: gt("Click to add text"),
    media: gt("Media clip"),
    clipArt: gt("Online image"),
    chart: gt("Chart"),
    dgm: gt("Smart Art"),
    tbl: gt("Click to add table"),
    pic: gt("Click to add image"),
    sldNum: gt("Slide Number"),
    ftr: gt("Footer"),
    dt: gt("Date")
};

/**
 * The default texts that are inserted, if the place holder drawing with click button is empty.
 * Info: These strings need to be used by the filter.
 */
export const PLACEHOLDER_TEMPLATE_TEXTS_CLICK: Partial<PlaceHolderTypesStringType> = {
    pic: gt("Click icon to add image"),
    tbl: gt("Click icon to add table"),
    chart: gt("Click icon to add chart"),
    dgm: gt("Click icon to add smart art graphic"),
    clipArt: gt("Click icon to add clip art"),
    media: gt("Click icon to add media")
};

/**
 * The supported buttons inside a place holder drawing.
 */
export const PLACEHOLDER_TEMPLATE_BUTTONS: PlaceHolderButtonTypesType = {
    contentBody: ["picture", "table"],
    body: null,
    title: null,
    ctrTitle: null,
    subTitle: null,
    media: ["media"],
    clipArt: ["clipart"],
    chart: ["chart"],
    dgm: ["smartart"],
    tbl: ["table"],
    pic: ["picture"],
    sldNum: null,
    ftr: null,
    dt: null
};

/**
 * The supported buttons inside a place holder drawing in ODF format.
 */
export const PLACEHOLDER_TEMPLATE_BUTTONS_ODF: PlaceHolderButtonTypesType = {
    contentBody: ["picture", "table"],
    body: ["picture", "table"],
    title: null,
    ctrTitle: null,
    subTitle: null,
    media: ["media"],
    clipArt: ["clipart"],
    chart: ["chart"],
    dgm: ["smartart"],
    tbl: ["table"],
    pic: ["picture"],
    sldNum: null,
    ftr: null,
    dt: null
};

/**
 * Supported names for inserting custom drawing placeholders.
 */
export const PLACEHOLDER_DRAWING_NAMES = {
    content:    gt("Content placeholder"),
    contentv:   gt("Vertical content placeholder"),
    text:       gt("Text placeholder"),
    textv:      gt("Vertical text placeholder"),
    picture:    gt("Image placeholder"),
    table:      gt("Table placeholder")
};

/**
 * The template text inserted into supported placeholder drawings.
 */
export const PLACEHOLDER_TEXT_CONTENT = [
    gt("Edit Master text styles"),
    gt("Second level"),
    gt("Third level"),
    gt("Fourth level"),
    gt("Fifth level")
];

/**
 * The template text inserted into title placeholder drawing.
 */
export const PLACEHOLDER_TITLE_CONTENT = gt("Click to edit Master title style");

// methods ----------------------------------------------------------------

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes. This means, that the phType is set, or that the phIndex is specified. It is possible,
 * that only the phIndex is specified. In this case the phType is defaulted to "body".
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes.
 */
export function isPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation?.phType || is.number(attrs?.presentation?.phIndex));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a title place holder drawing. This means, that the phType is set to "title" or
 * "ctrTitle".
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of "title" drawing.
 */
export function isTitlePlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation?.phType && TITLE_PLACEHOLDER_TYPES.has(attrs.presentation.phType));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a title or subtitle place holder drawing. This means, that the phType is set to
 * "title", "ctrTitle" or "subTitle".
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of "title" or "subtitle" drawing.
 */
export function isTitleOrSubtitlePlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation?.phType && TITLE_OR_SUBTITLE_PLACEHOLDER_TYPES.has(attrs.presentation.phType));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a body place holder drawing. This means, that the phType is set to "body" or
 * that it is not defined.
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of "body" drawing.
 */
export function isBodyPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation && is.number(attrs.presentation.phIndex) && (attrs.presentation.phType === DEFAULT_PLACEHOLDER_TYPE || !attrs.presentation.phType));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a body place holder drawing for inserting text only. This means, that the
 * phType is set explicitely to "body".
 * Info: In ODF file format this is quite different, because there is no place holder of type body, that
 *       accepts only text input.
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @param [isODF=false]
 *  Whether this is an ODF application. If not specified, the value is set to "false".
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of "body" drawing
 *  that allows only text insertion.
 */
export function isTextBodyPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet, isODF?: boolean): boolean {
    const odf = isODF || false;
    return !!(!odf && attrs?.presentation && is.number(attrs.presentation.phIndex) && (attrs.presentation.phType === DEFAULT_PLACEHOLDER_TYPE));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a body place holder drawing that allows any content (text, table, picture, ...).
 * This means, that the phType is explicitely undefined and the phIndex is explicitely defined.
 * Info: In ODF file format this is quite different. The phType is set to "body" and it is not necessary,
 *       that a phIndex is specified.
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @param [isODF=false]
 *  Whether this is an ODF application. If not specified, the value is set to "false".
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of "body" drawing
 *  that allows input of any content (text, table, picture, ...).
 */
export function isContentBodyPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet, isODF?: boolean): boolean {
    const odf = isODF || false;
    return !!(attrs?.presentation && (is.number(attrs.presentation.phIndex) || odf) && (!attrs.presentation.phType || (odf && attrs.presentation.phType === DEFAULT_PLACEHOLDER_TYPE)));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a footer place holder drawing. This means, that the phType is set to "dt", "ftr"
 * or "sldNum".
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of a footer drawing.
 */
export function isFooterPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation?.phType && FOOTER_PLACEHOLDER_TYPES.has(attrs.presentation.phType));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) describes a place holder drawing
 * in which no text can be inserted.
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of a place holder drawing, in
 *  that no text can be inserted.
 */
export function isPlaceHolderWithoutTextAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs.presentation?.phType && NO_TEXT_PLACEHOLDER_TYPES.has(attrs.presentation.phType));
}

/**
 * Check, whether the specified attribute set describes a place holder drawing with user defined template text.
 *
 * @param attrs
 *  The attribute object. This might be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of a place holder drawing with
 *  user defined template text.
 */
export function isCustomPromptPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation?.customPrompt);
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a place holder drawing in that in ODF in the master view the content cannot be
 * modified.
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of a drawing that cannot
 *  be deleted or whose content cannot be modified in ODF masterview.
 */
export function isODFReadOnlyPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation?.phType && ODF_MASTER_READONLY_TYPES.has(attrs.presentation.phType));
}

/**
 * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
 * place holder attributes of a body place holder drawing without specified index. This means, that the phType
 * is set to "body" and the phIndex is not specified (used for ODF).
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set contains presentation place holder attributes of "body" drawing without
 *  specified index.
 */
export function isBodyPlaceHolderAttributeSetWithoutIndex(attrs: PtPresentationAttributeSet): boolean {
    return !!(attrs?.presentation && !is.number(attrs.presentation.phIndex) && (attrs.presentation.phType === DEFAULT_PLACEHOLDER_TYPE));
}

/**
 * Checking, whether the specified attribute set (explicit attributes from a drawing) specifies
 * a place holder drawing whose size and/or position was modified by the user.
 * This check is important for inheritance from master slide in ODP documents.
 *
 * @param attrs
 *  The attribute object. This should be the explicit attributes from a drawing.
 *
 * @returns
 *  Whether the specified attribute set specifies a place holder drawing whose size
 *  and/or position was modified by the user..
 */
export function isUserModifiedPlaceHolderAttributeSet(attrs: PtPresentationAttributeSet): boolean {
    return !!((attrs?.presentation?.userTransformed) || false);
}

/**
 * Check, whether the specified drawing is a drawing that contains place holder attributes. This means, that
 * the phType is set, or that the phIndex is specified. It is possible, that only the phIndex is specified. In
 * this case the phType is defaulted to "body".
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing contains presentation place holder attributes.
 */
export function isPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isPlaceHolderAttributeSet(getExplicitAttributeSet(drawing) as PtPresentationAttributeSet);
}

/**
 * Helper function to get the place holder type from a specified drawing. If it cannot be determined,
 * null is returned.
 *
 * @param drawing
 *  The drawing node.
 *
 * @param [drawingAttrs]
 *  The explicit drawing attributes. They can be specified for performance reasons.
 *
 * @returns
 *  The place holder type or null if it cannot be determined.
 */
export function getPlaceHolderDrawingType(drawing: NodeOrJQuery, drawingAttrs?: PtPresentationAttributeSet): AllPlaceHolderTypes | null {

    // the explicit drawing attributes
    const attrs = drawingAttrs ?? getExplicitAttributeSet(drawing) as PtPresentationAttributeSet;

    return (attrs?.presentation?.phType) ? attrs.presentation.phType : null;
}

/**
 * Helper function to get the place holder index from a specified drawing. If it cannot be determined,
 * null is returned.
 *
 * @param drawing
 *  The drawing node.
 *
 * @param [drawingAttrs]
 *  The explicit drawing attributes. They can be specified for performance reasons.
 *
 * @returns
 *  The place holder index or null if it cannot be determined.
 */
export function getPlaceHolderDrawingIndex(drawing: NodeOrJQuery, drawingAttrs?: PtPresentationAttributeSet): number | null {

    // the explicit drawing attributes
    const attrs = drawingAttrs ?? getExplicitAttributeSet(drawing) as PtPresentationAttributeSet;

    return (attrs?.presentation && is.number(attrs?.presentation?.phIndex)) ? attrs.presentation.phIndex : null;
}

/**
 * Check, whether the specified drawing is a title drawing that contains place holder attributes. This means, that
 * the phType is set to "title" or "ctrTitle".
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing contains presentation place holder attributes of type "title".
 */
export function isTitlePlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isTitlePlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a title or a subtitle drawing that contains place holder attributes.
 * This means, that the phType is set to "title", "ctrTitle" or "subTitle".
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing contains presentation place holder attributes of type "title" or "subTitle".
 */
export function isTitleOrSubtitlePlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isTitleOrSubtitlePlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a body drawing that contains place holder attributes. This means, that
 * the phType is set to "body or is not specified and the phIndex is specified.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing contains presentation place holder attributes of type "body".
 */
export function isBodyPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isBodyPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a body drawing that contains place holder attributes. This means, that
 * the phType is set explicitely to "body". In this case, only text content can be inserted into the
 * place holder drawing.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing contains presentation place holder attributes of type "body" that allows
 *  only text insertion.
 */
export function isTextBodyPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isTextBodyPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a body drawing that contains place holder attributes. This means, that
 * the phType is explicitely undefined. In this case, any content can be inserted into the place holder drawing
 * (text, table, picture, ...).
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing contains presentation place holder attributes of type "body" that allow input
 *  of any content into the "body".
 */
export function isContentBodyPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isContentBodyPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Checking, whether the specified drawing is a footer place holder drawing. This
 * includes the drawings for "Date and time", "Footer" and "Slide number".
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is a footer place holder drawing.
 */
export function isFooterPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isFooterPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a place holder drawing, in that no text can be inserted. In this
 * case it contains no paragraphs (in master/layout view a paragraph is inserted, but not in the document
 * view). Instead a button is available that can be used to insert a table or an image.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is a place holder drawing, that does not support text input.
 */
export function isPlaceHolderWithoutTextDrawing(drawing: NodeOrJQuery): boolean {
    return isPlaceHolderWithoutTextAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is an empty place holder drawing. For all types of place
 * holders it is checked, if the place holder contains the possible content.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is an empty place holder drawing.
 */
export function isEmptyPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {

    // the explicit attributes of the drawing
    const attrs = getExplicitAttributeSet(drawing) as PtPresentationAttributeSet;
    // whether this is a place holder that does not contain text and that contains no content
    let isEmpty = false;

    // First check: Has the place holder a valid type
    if (isPlaceHolderWithoutTextAttributeSet(attrs)) {
        const phType =  attrs.presentation!.phType as NoTextPlaceHolderTypes;
        if ($(drawing).attr("data-type") !== "undefined") {
            // type: "undefined" : handling place holder drawings with unknown content -> they are never empty
            // -> otherwise checking the explicit attributes to find out, if the drawing contains content like a table or an image
            const attributesProperty = NO_TEXT_PLACEHOLDER_CONTENT_PROPERTY[phType];

            // ... and is the place holder empty (or contains only the template text)
            if (!attrs || !attributesProperty || !(attrs as Dict)[attributesProperty]) { // check "table" family or "image" family at attributes
                isEmpty = true;
            }

        }
    } else if (isPlaceHolderAttributeSet(attrs)) {
        // handling of those place holders, that allow text input (text body place holders and content body place holders)
        // but also title, ctrTitle, ...
        isEmpty = isEmptyTextframe(drawing, { ignoreTemplateText: true });
    }

    return isEmpty;
}

/**
 * Check, whether the specified drawing is a place holder drawing that contains a user
 * specified template text.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is a place holder drawing with user defined template text.
 */
export function isCustomPromptPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isCustomPromptPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a place holder drawing that cannot be deleted or whose content
 * cannot be modified in master view in ODF files.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is a place holder drawing that cannot be deleted or whose content
 *  cannot be modified in master view in ODF files.
 */
export function isODFReadOnlyPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isODFReadOnlyPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Check, whether the specified drawing is a place holder drawing with explicitely set phType to "body" and
 * undefined "phIndex" (used for ODF).
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is a place holder drawing with explicitely set phType to "body" and
 *  undefined "phIndex".
 */
export function isBodyPlaceHolderWithoutIndexDrawing(drawing: NodeOrJQuery): boolean {
    return isBodyPlaceHolderAttributeSetWithoutIndex(getExplicitAttributeSet(drawing));
}

/**
 * Checking, whether the specified drawing is a place holder drawing whose size
 * and/or position was modified by the user.
 * This check is important for inheritance from master slide in ODP documents.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing is a place holder drawing whose size
 *  and/or position was modified by the user.
 */
export function isUserModifiedPlaceHolderDrawing(drawing: NodeOrJQuery): boolean {
    return isUserModifiedPlaceHolderAttributeSet(getExplicitAttributeSet(drawing));
}

/**
 * Checking, whether the specified drawing requires the property "userTransformed".
 * This is the case of place holder drawings in ODF, that are moved or resized
 * by the user.
 * This check is important for inheritance from master slide in ODP documents.
 * This check can be used after a drawing was moved or resized by the user, so
 * that a valid operation can be generated.
 *
 * @param drawing
 *  The drawing node.
 *
 * @returns
 *  Whether the specified drawing requires the property "userTransformed". This is
 *  the case, if it is a place holder drawing whose size and/or position was modified
 *  by the user.
 */
export function requiresUserTransformedProperty(drawing: NodeOrJQuery): boolean {
    const attrs = getExplicitAttributeSet(drawing);
    return isPlaceHolderAttributeSet(attrs) && !isUserModifiedPlaceHolderAttributeSet(attrs);
}

/**
 * Getting an object containing the keys "target", "type" and "index" for a specified drawing. This
 * object can be used to get attributes and list styles from the model for the drawing
 *
 * Info: Place holder drawings cannot be grouped, they are always direct children of a slide.
 *
 * @param drawing
 *  The drawing node.
 *
 * @param [options]
 *  Optional parameters:
 *  @param [options.useDefaultValues=true]
 *      If set to true, the default values for drawing place holder type and index are used.
 *      Otherwise the explicit attributes for type and index are inserted into the return object,
 *      but this might not be defined.
 *
 * @returns
 *  An object describing "target" (the ID of the slide), "type" (the type of the drawing) and
 *  "index" of a specified drawing. Or undefined, if drawing is no placeholder.
 */
export function getDrawingModelDescriptor(drawing: NodeOrJQuery, options: { useDefaultValues?: true }): Opt<PlaceHolderDescription> {

    // the explicit attributes set at the drawing
    const drawingAttrs = getExplicitAttributeSet(drawing) as PtPresentationAttributeSet;
    // whether the default values for type and index shall be returned
    const useDefaultValues = options?.useDefaultValues ?? true;
    // the return object containing keys "target", "type" and "index"
    let drawingObj: Opt<PlaceHolderDescription>;

    if (drawingAttrs.presentation && isPlaceHolderAttributeSet(drawingAttrs)) {

        drawingObj = {
            target: getTargetContainerId($(drawing).parent()), // place holder drawings are direct children of slide
            type: drawingAttrs.presentation.phType,
            index: drawingAttrs.presentation.phIndex
        };

        if (useDefaultValues) {
            if (drawingObj.type && !is.number(drawingObj.index)) { drawingObj.index = DEFAULT_PLACEHOLDER_INDEX; }
            if (!drawingObj.type && is.number(drawingObj.index)) { drawingObj.type = DEFAULT_PLACEHOLDER_TYPE; }
        }
    }

    return drawingObj;
}

/**
 * Receiving a valid set for the properties "type" and "index", so that the model can be accessed.
 * Typically the specified attributes parameter is the set of explicit drawing attributes.
 *
 * @param attrs
 *  The attributes object. This are typically the explicit attributes set at a drawing.
 *
 * @returns
 *  An object containing the properties "type" and "index". "type" is a string, that describes
 *  the place holder type, "index" is a number describing the index specified for the given
 *  place holder type.
 *  If the specified attribute does not contain a "presentation" property or inside this
 *  "presentation" property neither "phType" nor "phIndex" are defined, null is returned.
 */
export function getValidTypeIndexSet(attrs: PtPresentationAttributeSet): { type: AllPlaceHolderTypes; index: number } | null {

    // it is necessary that at least one of phType or phIndex is defined in the attributes
    if (!attrs?.presentation || !("phType" in attrs.presentation || "phIndex" in attrs.presentation)) {
        return null;
    }

    return {
        type: attrs?.presentation?.phType ?? DEFAULT_PLACEHOLDER_TYPE,
        index: attrs?.presentation?.phIndex ?? DEFAULT_PLACEHOLDER_INDEX
    };
}

/**
 * Getting the default text for a place holder drawing. This text is dependent from the place holder type.
 *
 * @param drawing
 *  The drawing node.
 *
 * @param [isODF=false]
 *  Whether this is an ODF application. If not specified, the value is set to "false".
 *
 * @returns
 *  The default text for a place holder drawing. Or null, if it cannot be determined.
 */
export function getPlaceHolderTemplateText(drawing: NodeOrJQuery, isODF?: boolean): string | null {

    // the place holder type
    let type = getPlaceHolderDrawingType(drawing);
    // whether this is an ODF application.
    const odf = isODF || false;

    if (!type && is.number(getPlaceHolderDrawingIndex(drawing))) {
        type = DEFAULT_PLACEHOLDER_TYPE;
    }

    return type ? (odf ? PLACEHOLDER_TEMPLATE_TEXTS_ODF[type] : PLACEHOLDER_TEMPLATE_TEXTS[type]) : null;
}

/**
 * Getting a list of strings, that describe the required buttons for a specified place holder type.
 * If the type is the empty string, it is defaulted to the place holder content body.
 *
 * @param type
 *  The string describing the place holder type. The content body type must use the empty string.
 *
 * @param [isODF=false]
 *  Whether this is an ODF application. If not specified, the value is set to "false".
 *
 * @returns
 *  An array containing strings for the required buttons for the specified place holder type.
 */
export function getPlaceHolderTemplateButtonsList(type: AllPlaceHolderTypes, isODF?: boolean): string[] | null {
    const buttonMap = isODF ? PLACEHOLDER_TEMPLATE_BUTTONS_ODF : PLACEHOLDER_TEMPLATE_BUTTONS;
    return buttonMap[type || "contentBody"]; // using special name for content body place holders
}

/**
 * Getting the default representation string for fields in footer place holder types in ODF.
 *
 * @param type
 *  The string describing the place holder type.
 *
 * @returns
 *  The default representation string for fields in footer place holder types in ODF. Or an
 *  empty string, if it cannot be determined.
 */
export function getODFFooterRepresentation(type: AllPlaceHolderTypes): string {
    const representationContent = PLACEHOLDER_TEMPLATE_TEXTS_ODF[type];
    return representationContent ? (`<${representationContent}>`) : "";
}
