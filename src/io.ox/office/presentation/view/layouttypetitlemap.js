/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
// module LayoutTypeTitleMap ==============================================

/**
 * Provides title/caption texts for any layout slide type
 * bundled into a single map for convenience.
 */
const LayoutTypeTitleMap = {
    // - https://intranet.open-xchange.com/wiki/documents-team:operations:presentation?&#slideattributes
    //
    //   internal ox presentation slide attributes copy text mapping:
    //
    // - when available, specific title texts derived from office 2016    // https://www.microsoft.com/Language/en-US/Search.aspx
    //   (,2013) powerpoint are used (and flagged as "pp-xxxx").          //
    //  ox attribute key:       //                                                                    //

    master:                     'Master',

    //blank:                      'Blank',                                                          // pp-2016  // en: 'Blank' - de: 'Leer'
    blank:                      'Blank',                                                          //

    chart:                      'Title and chart',
    chartAndTx:                 'Title, chart on left and text on right',
    clipArtAndTx:               'Title, clipart on left, text on right',
    clipArtAndVertTx:           'Title, clip art on left, vertical text on right',

    //cust:                       'Custom layout defined by user',                                  // pp-2016  // en: 'Custom' - de: 'Benutzerdefiniert'
    cust:                       'Custom',                                                         //

    dgm:                        'Title and diagram',
    fourObj:                    'Title and four objects',
    mediaAndTx:                 'Title, media on left, text on right',

    //obj:                        'Title and object',                                               // pp-2016  // en: 'Title and Content' - de: 'Titel und Inhalt'
    obj:                        'Title and Content',                                              //

    objAndTwoObj:               'Title, one object on left, two objects on right',
    objAndTx:                   'Title, object on left, text on right',
    objOnly:                    'Object only',
    objOverTx:                  'Title, object on top, text on bottom',

    //objTx:                      'Title, object and caption text',                                 // pp-2016  // en: 'Content with Caption' - de: 'Inhalt mit Beschriftung'
    objTx:                      'Content with Caption',                                           //

    //picTx:                      'Title, image, and caption text',                               // pp-2016  // en: 'Image with Caption' - de: 'Bild mit Beschriftung'
    picTx:                      'Image with Caption',                                           //

    //secHead:                    'Section header title and subtitle text',                         // pp-2016  // en: 'Section Header' - de: 'Abschnittsueberschrift'
    secHead:                    'Section Header',                                                 //

    tbl:                        'Title and table',

    //title:                      'Title layout with centered title and subtitle placeholders',     // pp-2016  // en: 'Title Slide' - de: 'Titelfolie'
    title:                      'Title Slide',                                                    //

    //titleOnly:                  'Title only',                                                     // pp-2016  // en: 'Title Only' - de: 'Nur Titel'
    titleOnly:                  'Title Only',                                                     //

    twoColTx:                   'Title, text on left, text on right',

    //twoObj:                     'Title, object on left, object on right',                         // pp-2016  // en: 'Two Content' - de: 'Zwei Inhalte'
    twoObj:                     'Two Content',                                                    //

    twoObjAndObj:               'Title, two objects on left, one object on right',
    twoObjAndTx:                'Title, two objects on left, text on right',
    twoObjOverTx:               'Title, two objects on top, text on bottom',

    //twoTxTwoObj:                'Title, two objects each with text',                              // pp-2016  // en: 'Title, 2 Content and Text' - de: 'Titel, zwei Inhalte und Text'
    twoTxTwoObj:                'Title, 2 Content and Text',                                      //

    tx:                         'Title and text',
    txAndChart:                 'Title, text on left and chart on right',
    txAndClipArt:               'Title, text on left, clip art on right',
    txAndMedia:                 'Title, text on left, media on right',
    txAndObj:                   'Title, text on left, object on right',
    txAndTwoObj:                'Title, text on left, two objects on right',
    txOverObj:                  'Title, text on top, object on bottom',

    //vertTitleAndTx:             'Vertical title on right, vertical text on left',                 // pp-2016  // en: 'Vertical Title and Text' - de: 'Vertikaler Titel und Text'
    vertTitleAndTx:             'Vertical Title and Text',                                        //

    vertTitleAndTxOverChart:    'Vertical title on right, vertical text on top, chart on bottom',

    //vertTx:                     'Title and vertical text body'                                    // pp-2016  // en: 'Title and Vertical Text' - de: 'Titel und vertikaler Text'
    vertTx:                     'Title and Vertical Text'                                         //
};

//                              https://www.microsoft.com/Language/en-US/Search.aspx              //  Order
//                                                                                                //
//
//  master:
//                              en: 'Master'
//                              de: 'Master':
//
//  cust:
//                              en: 'Custom'
//                              de: 'Benutzerdefiniert':
//
//  title:                                                                                            (01)
//                              en: 'Title Slide'
//                              de: 'Titelfolie'
//
//  obj:                                                                                              (02)
//                              en: 'Title and Content'
//                              de: 'Titel und Inhalt'
//
//  secHead:                                                                                          (03)
//                              en: 'Section Header'
//                              de: 'Abschnittsueberschrift'
//
//  twoObj:                                                                                           (04)
//                              en: 'Two Content'
//                              de: 'Zwei Inhalte'
//
//  twoTxTwoObj:                                                                                      (05)
//                              en: 'Title, 2 Content and Text'
//                              de: 'Titel, zwei Inhalte und Text'
//
//  titleOnly:                                                                                        (06)
//                              en: 'Title Only'
//                              de: 'Nur Titel'
//
//  blank:                                                                                            (07)
//                              en: 'Blank'
//                              de: 'Leer'
//
//  objTx:                                                                                            (08)
//                              en: 'Content with Caption'
//                              de: 'Inhalt mit Beschriftung'
//
//  picTx:                                                                                            (09)
//                              en: 'Image with Caption'
//                              de: 'Bild mit Beschriftung'
//
//  vertTx:                                                                                           (10)
//                              en: 'Title and Vertical Text'
//                              de: 'Titel und vertikaler Text'
//
//  vertTitleAndTx:                                                                                   (11)
//                              en: 'Vertical Title and Text'
//                              de: 'Vertikaler Titel und Text'
//

// exports ================================================================

export default LayoutTypeTitleMap;
