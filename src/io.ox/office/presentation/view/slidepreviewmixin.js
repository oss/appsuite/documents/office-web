/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { fun } from '@/io.ox/office/tk/algorithms';
import { BREAK, convertHmmToLength, getBooleanOption } from '@/io.ox/office/tk/utils';
import { APP_TOOLTIP_CLASS, SELECTED_CLASS } from '@/io.ox/office/tk/forms';

import '@/io.ox/office/presentation/view/style.less';

// mixin class SlidePreviewMixin ==========================================

/**
 * Additional preview specific behavior for the presentation view.
 *
 * Does provide a set of specific functionality that is just for rendering
 * the preview of any presentation slide that most recently has been changed.
 *
 * @mixin
 *  for class `PresentationView`
 */
export default function SlidePreviewMixin() {

    var
        self = this,                    // self reference :: PresentationView

        docModel,                       // {TextModel} docModel - The document model created by the passed application.

        slidepane,                      // the `slide pane` reference.

        $pageNode,

        //$thumbRenderBox,

        imgNodeBlueprint,

        $masterPreviewBlueprint,
        $standardPreviewBlueprint,

        //array_unique = _.unique,

        isMasterView   = false,
        isStandardView = false,

        thumb_scale_value,
        css_value__thumb_scale,

        THUMBNAIL_HEIGHT = 50,

        SELECTOR__PREVENT_PREVIEW           = '[data-prevent-preview]',

        SELECTOR__MASTER_SLIDE              = '.masterslidelayer',
        SELECTOR__LAYOUT_SLIDE              = '.layoutslidelayer',
        SELECTOR__PAGE_CONTENT              = '.pagecontent',
        SELECTOR__SLIDE                     = '.slide',

        SELECTOR__SLIDE_CONTAINER           = '.slidecontainer',
        SELECTOR__SLIDEPANE_CONTAINER       = '.slide-pane-container',

        SELECTOR__DRAWING                   = '.drawing',
        SELECTOR__COLLABORATIVE_OVERLAY     = '.collaborative-overlay',
        SELECTOR__DRAWINGSELECTION_OVERLAY  = '.drawingselection-overlay',

        CLASS_NAME__FORCEPLACEHOLDERVISIBILITY  = 'forceplaceholdervisibility',

        CLASS_NAME__UNFORCE_BACKGROUND      = 'unforce-background',
        CLASS_NAME__INVISIBLE_BACKGROUND    = 'invisiblebackground',
        CLASS_NAME__INVISIBLE_DRAWINGS      = 'invisibledrawings',
        CLASS_NAME__INVISIBLE_SLIDE         = 'invisibleslide',
        CLASS_NAME__LEFTSHIFT_SLIDE         = 'leftshift',
        CLASS_NAME__NOT_SELECTABLE          = 'notselectable',
        CLASS_NAME__FIELD_HIGHLIGHT         = 'sf-highlight',

        CLASS_NAME__EDIT_MODE               = 'edit-mode',

        //CLASS_NAME__THUMB_MODE              = 'thumb-mode',
        CLASS_NAME__PREVIEW_MODE            = 'preview-mode',

        // boolean value constant that indicates whether the workerd document features ODF or not.
        IS_ODF = false,

        // does refer to available preview-master layer-blueprints each as already sanitized DOM fragment.
        previewMasterMap    = {},

        // stores all available previews each as always up to date DOM fragment.
        previewStore        = {},
        // keeps track of any preview's attribution state.
        attributionMap      = {},

        // keeps track of render state of all available thumbs and, if necessary, stores them too.
        thumbRegistry       = {},

        // caches/stores slide specific field data for the time a preview does not yet exist
        // due to referring to a still not completely formatted slide (incomplete attribution).
        fieldDataStore      = {},

        // does cache all data objects that have been received by listening to
        // '...Fields:update' events before all internal states were initialized.
        fieldUpdateCache    = [],

        // does cache all slide id's that have been notified to be finally formatted
        // via the model before all internal states were initialized.
        formattedSlideCache = [];

    // modify initial `initHandler` with mixin specific `init` functionality via basic function composition.
    this.config.initHandler = _.wrap(this.config.initHandler, (baseInitHandler, options) => {
        baseInitHandler.call(this, options);
        initHandler(); // the `SlidePreviewMixin`s specific `initHandler` functionality.
    });

    // local / private methods --------------------------------------------

    function putPreview(slideId, $preview) {
        previewStore[slideId] = $preview;
    }

    function getPreview(slideId) {
        var
            $preview        = previewStore[slideId],
            attributionItem = attributionMap[slideId];

        //return (attributionItem && !attributionItem.isMustFormat && $preview) ? $preview.clone() : $('<div>');

        //if (attributionItem && $preview) {  // quickfix (but feasible) regarding following bugs:
        //
        // - change from slidepane's filling state into empty one   - https://bugs.open-xchange.com/show_bug.cgi?id=49451
        // - empty slidepane triggered by drag&drop                 - https://bugs.open-xchange.com/show_bug.cgi?id=49270
        // - empty slides in master-slave edit-mode for slave view  - https://bugs.open-xchange.com/show_bug.cgi?id=49358
        // - undisplayed slide background changes in slidepane      - https://bugs.open-xchange.com/show_bug.cgi?id=49059
        //

        if (attributionItem && !attributionItem.isMustFormat && $preview) {
            // - restored to default again (that uses a lookup table)
            //   after applying a fix to the model's attribution and
            //   formatting process that now informs correctly via
            //   'slidestate:formatted' when a slide has reached its
            //   final formatting state.
            // - the above quickfix nevertheless remains (though
            //   commented now) in order to directly document the
            //   formerly targeted bug ids.
            //
            // for each preview that is taken from store readjust
            // each of its containing slides' background rendering.
            //
            renderPreviewBackgroundById($preview, slideId);

            $preview = $preview.clone();
        } else {
            $preview = $('<div>');
        }
        return $preview;
    }/*
    function getPreviewList() {
        return Object.keys(previewStore).reduce(function (collector, slideId) {

            collector.push(getPreview(slideId));
            return collector;

        }, []);
    }*/

    function unflagAttributionItemAsMustFormat(slideId) {
        attributionMap[slideId].isMustFormat = false;
    }
    function flagAttributionItemAsMustFormat(slideId) {
        attributionMap[slideId].isMustFormat = true;
    }/*

    function deleteAttributionItem(slideId) {
        return (delete attributionMap[slideId]);
    }*/
    function createAttributionItem(slideId) {
        attributionMap[slideId] = {};
        flagAttributionItemAsMustFormat(slideId);
    }

    function unflagThumbItemAsMustRender(slideId) {
        thumbRegistry[slideId].isMustRender = false;
    }
    function flagThumbItemAsMustRender(slideId) {
        thumbRegistry[slideId].isMustRender = true;
    }/*

    function deleteThumbItem(slideId) {
        return (delete thumbRegistry[slideId]);
    }*/
    function createThumbItem(slideId/*, idx, list*/) {
        thumbRegistry[slideId] = {
            thumb: null
        };
        flagThumbItemAsMustRender(slideId);
    }

    var getThumbDeferred = (function () {
        var
            firstLevelRequestQueue  = [],   // queue of pending thumb request/render promises of high priority (thumbs already have been rendered).
            secondLevelRequestQueue = [],   // queue of pending thumb request/render promises of low priority (thumbs still need to be rendered).
            requestTimer            = null; // background loop processing all pending thumb request/render promises.

        // direct callback: called every time when `getThumbDeferred` has been called.
        function registerRequestPromise(deferredRequest, slideId) {
            var
                renderStateItem   = thumbRegistry[slideId],
                requestDescriptor = {
                    deferred: deferredRequest,
                    slideId
                };
            if (renderStateItem.isMustRender) {
                secondLevelRequestQueue.push(requestDescriptor);
            } else {
                firstLevelRequestQueue.push(requestDescriptor);
            }
        }

        // deferred callback: called once after the specified timeout.
        function getThumbDeferredFromPrioritizedQueues() {

            // check if the background loop is already running
            if (requestTimer) { return; }

            // create a new background loop that processes all pending tables
            requestTimer = self.repeatSliced(function () {

                var requestDescriptor = firstLevelRequestQueue.shift() || secondLevelRequestQueue.shift();
                if (!requestDescriptor) {

                    return BREAK;
                }
                var
                    deferredRequest = requestDescriptor.deferred,
                    slideId         = requestDescriptor.slideId,

                    renderStateItem = thumbRegistry[slideId];

                if (renderStateItem.isMustRender) {
                    renderStateItem.thumb = createLayoutThumb(slideId);
                }
                deferredRequest.resolve({ thumb: renderStateItem.thumb });
            });

            // forget timer reference as soon as every request/render promise is out of 'pending' state.
            requestTimer.always(function () { requestTimer = null; });
        }

        // create and return the debounced and queued `getThumbDeferred` method with a delay of 10ms per thumb request/render promise.
        return self.debounce(registerRequestPromise, getThumbDeferredFromPrioritizedQueues, { delay: 20 });
    }());

    // function getThumbDeferred(deferredRequest, slideId) {
    //     var renderStateItem = thumbRegistry[slideId];
    //
    //     if (renderStateItem.isMustRender) {
    //         renderStateItem.thumb = createLayoutThumb(slideId);
    //     }
    //     deferredRequest.resolve({ thumb: renderStateItem.thumb });
    // }

    function requestThumb(slideId) {
        var thumbRequest = self.createDeferred();

        if (slideId in thumbRegistry) {
            getThumbDeferred(thumbRequest, slideId);
        } else {
            thumbRequest.reject(new ReferenceError('Slide ID does not match.'));
        }
        // return the `deferred` 's `promise` object
        return thumbRequest.promise();
    }

    function createLayoutThumb(slideId) {
        //globalLogger.info('+++ createLayoutThumb :: slideId', slideId);
        var
            $previewThumb = getPreview(slideId)/*.addClass(CLASS_NAME__THUMB_MODE)*/,
            $layoutThumb = $previewThumb.clone();

        applyThumbScaling($layoutThumb);

        $layoutThumb.addClass('thumb');

        // check back with local function `renderLayoutThumb` as of 'io.ox/office/presentation/view/control/layoutslidepicker'
        $layoutThumb.data({ width: Math.round($layoutThumb.width() * thumb_scale_value)/*, height: Math.round($layoutThumb.height() * thumb_scale_value)*/ });

        unflagThumbItemAsMustRender(slideId);

        return $layoutThumb;
    }

    function computeInitialThumbScaling() {
        thumb_scale_value       = (Math.round((THUMBNAIL_HEIGHT / convertHmmToLength(docModel.getSlideDocumentSize().height, 'px', 1)) * 100000) / 100000);
        css_value__thumb_scale  = 'scale(' + thumb_scale_value + ')';
    }

    function applyThumbScaling($fragment) {
        $fragment.css('transform-origin', '0 0'); // class name bound LESS/CSS rule.
        $fragment.css('transform', css_value__thumb_scale);
    }

    /**
     * Helper that creates a base64-encoded image from a provided canvas element whilst preserving size measurements.
     */
    function createImageFromCanvas(canvasNode) {
        var
            imgNode = imgNodeBlueprint.cloneNode();

        imgNode.src           = canvasNode.toDataURL();

        imgNode.width         = canvasNode.width;         // canvasNode.offsetWidth
        imgNode.height        = canvasNode.height;        // canvasNode.offsetHeight

        imgNode.className     = canvasNode.className;
        imgNode.style.cssText = canvasNode.style.cssText;

        return imgNode;
    }

    /**
     * Sanitizing helper that will preserve any canvas content (in this case,
     * any shapes appearance) by stepwise taking a base64-encoded image-snapshot
     * of each canvas source element and replacing the corresponding canvas target
     * element with this very snapshot.
     */
    function preserveShapes($sourceNode, $targetNode) {

        if (!$sourceNode || !$targetNode) { return; }

        var
            sourceList = $sourceNode.find('canvas').toArray(),
            targetList = $targetNode.find('canvas').toArray();

        sourceList.forEach(function (sourceNode, idx/*, list*/) {
            $(targetList[idx]).replaceWith(createImageFromCanvas(sourceNode));
        });
    }

    /**
     * Removes the specified CSS class from the passed node and all its
     * siblings.
     *
     * @param {HTMLElement|jQuery} node
     *  The DOM element to be manipulated.
     *
     * @param {String} className
     *  The CSS class to be removed from the passed node.
     */
    function removeClass(node, className) {
        $(node).find('.' + className).addBack().removeClass(className);
    }

    /**
     * Sanitizing helper that assures any preview fragment
     * of being in a visible state.
     */
    function preventInvisibleSlide(node) {
        removeClass(node, CLASS_NAME__INVISIBLE_SLIDE);
    }

    /**
     * Sanitizing helper that assures any preview fragment
     * of being not shifted to the left.
     */
    function preventLeftShiftSlide(node) {
        removeClass(node, CLASS_NAME__LEFTSHIFT_SLIDE);
    }

    /**
     * Sanitizing helper that assures any preview fragment
     * of not being a F6 target.
     */
    function preventF6Target(node) {
        removeClass(node, 'f6-target');
    }

    /**
     * Sanitizing helper that prevents any preview fragment
     * from being in a selectable state.
     */
    function preventNotSelectable(node) {
        removeClass(node, CLASS_NAME__NOT_SELECTABLE);
    }

    /**
     * Sanitizing helper that prevents any `slide` classified preview
     * fragment from being in a selectable state.
     */
    function assureNotSelectableSlide(node) {
        $(node).find(SELECTOR__SLIDE).addBack(SELECTOR__SLIDE).addClass(CLASS_NAME__NOT_SELECTABLE);
    }

    /**
     * Sanitizing helper that prevents every data field
     * being displayed in its selected/highlighted state.
     */
    function preventHighlightedFields(node) {
        removeClass(node, CLASS_NAME__FIELD_HIGHLIGHT);
    }

    /**
     * Sanitizing helper that prevents any preview fragment
     * from being in a selected state.
     */
    function preventSelectedElement(node) {
        removeClass(node, SELECTED_CLASS);
    }

    /**
     * Sanitizing helper that prevents any preview fragment
     * from being in an editable state.
     */
    function preventContendEditable(node) {
        $(node).find('[contenteditable]').addBack().removeAttr('contenteditable');
    }

    /**
     * Sanitizing helper that prevents any preview fragment
     * from triggering the tooltip display on hover.
     */
    function preventApplicationTooltip(node) {
        removeClass(node, APP_TOOLTIP_CLASS);
    }

    /**
     *
     *  ODP specific visibility handling
     */
    function handleOdfVisibilityOfPlaceholderFooterDrawings(slideId, $node) {
        if (IS_ODF) {
            var
                drawingItemList = $node.find(SELECTOR__DRAWING).toArray().map(function (elmNode) {
                    var
                        $node = $(elmNode);

                    return {
                        type: $node.attr('data-placeholdertype'),
                        $node
                    };
                }).filter(function (drawingItem) {

                    return docModel.isPlaceHolderFooterDrawingType(drawingItem.type);
                });

            drawingItemList.forEach(function (drawingItem) {
                if (docModel.isVisiblePlaceHolderFooterDrawingType(slideId, drawingItem.type)) {

                    drawingItem.$node.addClass(CLASS_NAME__FORCEPLACEHOLDERVISIBILITY);
                } else {
                    drawingItem.$node.removeClass(CLASS_NAME__FORCEPLACEHOLDERVISIBILITY);
                }
            });
        }
    }

    //  BEGIN - Field Data Support
    //
    //  - code block that implements support of
    //    how to properly deal with field data
    //
    //    * be it updating already existing previews
    //    * or handling field data pre storing/caching
    //      with deferred field data updates
    //    * and regardless of being word- or ODF-formatted.
    //
    //

    function getSlideIdFromFieldData(data) {
        return data.slideID;
    }
    function getFieldIdFromFieldData(data) {
        return data.fieldID;
    }

    function putFieldData(data) {
        //window.console.log('+++ putFieldData - slide ' + getSlideIdFromFieldData(data) + ' still needs to be formatted.');
        var
            slideId   = getSlideIdFromFieldData(data),
            fieldId   = getFieldIdFromFieldData(data),

            groupedDataItem = fieldDataStore[slideId];

        if (!groupedDataItem) {
            groupedDataItem = fieldDataStore[slideId] = {}; // - no array in order to prevent pushing
        }                                                   //   data-objects multiple times that do
        groupedDataItem[fieldId] = data;                    //   target the same `slideId.fieldId` combination.
    }

    function getFieldDataBySlideId(slideId) {
        return fieldDataStore[slideId];
    }
    function deleteFieldDataBySlideId(slideId) {
        return (delete fieldDataStore[slideId]);
    }

    function renderPrestoredFieldData(slideId) {
        var
            groupedDataItem = getFieldDataBySlideId(slideId);

        if (groupedDataItem) {
            Object.keys(groupedDataItem).forEach(function (fieldId) {

                updateField(groupedDataItem[fieldId]);
            });
            deleteFieldDataBySlideId(slideId);
        }
    }

    function updateField(data) {
        var
            isVisibleField,

            slideId   = getSlideIdFromFieldData(data),
            fieldId   = getFieldIdFromFieldData(data),

            $preview  = previewStore[slideId],
            $field    = ($preview && $preview.find('[data-fid="' + fieldId + '"] > span')),

            elmField  = ($field && $field[0]);

        // if (IS_ODF && isMasterView && docModel.isStandardSlideId(slideId)) {
        //     return;
        // }
        // @Note: this guard is implemented into 'io.ox/office/presentation/components/field/slidefieldmanager' around line 1126

        if (elmField) { // preview already does exist, field also can bee accessed ... proceed with updating all slide specific field data.

            $field.eq(0).text(data.value);
            $field.parent().removeClass(CLASS_NAME__FIELD_HIGHLIGHT);

            if (data.masterViewField === true) {

                $field    = $field.closest('[data-placeholdertype="' + data.fieldType + '"]');
                elmField  = $field[0];
            }
            if (elmField && _.isBoolean(isVisibleField = data.visible)) {
                var
                    fieldStyle = elmField.style;

                if (isVisibleField) {
                    fieldStyle.display = 'initial';   // - directly restore an elements style value from e.g. 'none' like from an applied css-rule to its default / initial render value.
                    fieldStyle.visibility = 'visible';
                } else {
                    fieldStyle.visibility = 'hidden';

                    fieldStyle.display = '';          // - directly reset an elements style value,
                }                                     //   allowing e.g. css-rules taking place again.
            }
        }
    }

    function handleUpdateField(data/*, idx, list*/) {
        var
            slideId = getSlideIdFromFieldData(data),
            attributionItem = attributionMap[slideId];

        //  if (IS_ODF && isMasterView && docModel.isStandardSlideId(slideId)) { return; }
        if (!IS_ODF || !isMasterView || !docModel.isStandardSlideId(slideId)) {

            // - does apply for either non ODF documents only, or,
            //   if featuring ODF, then for standards slides only.

            if (!attributionItem || attributionItem.isMustFormat) { // - preview does not yet exist (or not even the registration in the attributionMap),
                // because attribution has not yet been taken place;
                // - do cache slide specific field data.
                putFieldData(data);

            } else {
                // - preview might exist already;
                // - therefore proceed with updating all slide specific field data.
                updateField(data);
            }
        }
    }

    function handleFieldUpdate(fieldDataList) {
        //window.console.log('+++ handleFieldUpdate +++ fieldDataList : ', fieldDataList);

        fieldDataList.forEach(handleUpdateField);

        var
            filterByCurrentView   = isStandardView ? docModel.isStandardSlideId : docModel.isLayoutOrMasterId,
            fieldDataSlideIdList  = fieldDataList.map(getSlideIdFromFieldData),
            slideIdPreviewList    = fieldDataSlideIdList.filter(filterByCurrentView),
            slideIdLayoutThumbList;

        self.trigger('previewupdate:after', slideIdPreviewList);

        if (isStandardView) {
            slideIdLayoutThumbList = fieldDataSlideIdList.filter(docModel.isLayoutOrMasterId);

            if (slideIdLayoutThumbList.length > 0) {
                self.trigger('layoutthumb:change', slideIdLayoutThumbList.filter(docModel.isLayoutSlideId));
            }
        }
    }

    // please keep this commented block for debugging.
    //
    // function handleMasterFieldUpdate(fieldDataList) {
    //   //window.console.log('+++ handleMasterFieldUpdate +++ fieldDataList : ', fieldDataList);
    //
    //     handleFieldUpdate(fieldDataList);
    // }
    // function handleMasterFieldUpdateBeforeCompiledView(fieldDataList) {
    //   //window.console.log('+++ handleMasterFieldUpdateBeforeCompiledView +++ fieldDataList : ', fieldDataList);
    //
    //     handleFieldUpdateBeforeCompiledView(evt, fieldDataList);
    // }

    var handleFieldUpdateBeforeCompiledView = function (fieldDataList) {
        //window.console.log('+++ handleFieldUpdateBeforeCompiledView +++ fieldDataList : ', fieldDataList);

        fieldUpdateCache = fieldUpdateCache.concat(fieldDataList);
    };

    //
    //
    //  END - Field Data Support

    var handleSlideStateFormattedBeforeCompiledView = function (slideIdList) {
        formattedSlideCache = formattedSlideCache.concat(slideIdList);
    };

    /**
     * Generic sanitizer for both blueprint compiler methods
     * `compileMasterPreviewBlueprint` and `compileMasterPreviewBlueprint`.
     * This method cleans up any given jQuery-fied DOM fragment
     * in order to make it the render base of any preview.
     *
     * @param {jQuery.ElementNode} $node
     *  Any `pageNode` classified jQuery-fied element node.
     *
     * @returns {jQuery.ElementNode}
     *  Either a jQuery-fied blueprint for standard or for master/layout previews.
     */
    function getSanitizedPreviewBlueprint($node) {

        $node.removeClass(CLASS_NAME__EDIT_MODE);

        $node.find(SELECTOR__PREVENT_PREVIEW).remove();
        $node.find(SELECTOR__MASTER_SLIDE).empty();
        $node.find(SELECTOR__LAYOUT_SLIDE).empty();
        $node.find(SELECTOR__PAGE_CONTENT).empty();

        $node.addClass(CLASS_NAME__PREVIEW_MODE);

        preventContendEditable($node);
        preventF6Target($node);

        $node.css('margin', 0); // TODO to be refactored / to be applied within every sanitize process.

        return $node;
    }

    /**
     * Function that initially clones all master/layout layers
     * as render source for a continuous updating process of
     * each of a slide's related/connected master/layout preview.
     *
     * This blueprint does become the render base for any process
     * that does update, scale, render, clone and replace the
     * master/layout preview of the most recently changes/updated
     * master/layout slide(s).
     */
    function compileMasterPreviewBlueprint() {
        //var time = (new Date());

        $masterPreviewBlueprint = getSanitizedPreviewBlueprint($pageNode.clone());

        //$masterPreviewBlueprint.find(SELECTOR__COLLABORATIVE_OVERLAY).remove();
        $masterPreviewBlueprint.find(SELECTOR__COLLABORATIVE_OVERLAY).empty();

        $masterPreviewBlueprint.find(SELECTOR__DRAWINGSELECTION_OVERLAY).remove();

        //globalLogger.info('+++ compileMasterPreviewBlueprint +++');
        //globalLogger.info('+++ compileMasterPreviewBlueprint :: $masterPreviewBlueprint ', $masterPreviewBlueprint);

        //globalLogger.info('+++ compileMasterPreviewBlueprint :: time : ', (new Date() - time));
    }

    /**
     * Function that initially clones all presentation layers
     * as render source for a continuous updating process of
     * each of a slide's related/connected standard preview.
     *
     * This blueprint does become the render base for any process
     * that does update, scale, render, clone and replace the
     * standard preview of the most recently changes/updated
     * standard slide(s).
     */
    function compileStandardPreviewBlueprint() {
        //var time = (new Date());

        $standardPreviewBlueprint = getSanitizedPreviewBlueprint($pageNode.clone());

        $standardPreviewBlueprint.find(SELECTOR__COLLABORATIVE_OVERLAY).remove();
        //$standardPreviewBlueprint.find(SELECTOR__COLLABORATIVE_OVERLAY).empty();

        $standardPreviewBlueprint.find(SELECTOR__DRAWINGSELECTION_OVERLAY).remove();

        //globalLogger.info('+++ compileStandardPreviewBlueprint +++');
        //globalLogger.info('+++ compileStandardPreviewBlueprint :: $standardPreviewBlueprint ', $standardPreviewBlueprint);

        //globalLogger.info('+++ compileStandardPreviewBlueprint :: time : ', (new Date() - time));
    }

    /**
     * This method does set or update the visibility states of depended preview layers.
     *
     * @param {jQuery.ElementNode} $preview
     *  The jQuery element node all DOM operations do act upon.
     *
     * Note:
     *      A similar method can be found in 'io.ox/office/presentation/view/view'.
     *      There it is called `handleLayoutMasterVisibility`. The following description
     *      has been copied partly from there ...
     *
     * Helper function to handle the visibility of the layout and the master slide. This is
     * required, if the slide attribute 'followMasterShapes' is modified. It is important, that
     * not the complete slide gets 'display: none' but only the drawing children. Otherwise
     * the background would also vanish. But that would not be the correct behavior.
     *
     * @param {String} [layoutId]
     *  The id of the layout slide.
     *
     * @param {String} [masterId]
     *  The id of the master slide.
     *
     * @param {Boolean} showLayoutSlide
     *  Whether the specified layout slide shall be made visible or not.
     *
     * @param {Boolean} showMasterSlide
     *  Whether the specified master slide shall be made visible or not.
     */
    function setLayoutMasterVisibility($preview, layoutId, masterId, showLayoutSlide, showMasterSlide) {
        var
            $layout = $preview
                    .find(SELECTOR__LAYOUT_SLIDE).eq(0)
                    .children('[data-container-id="' + layoutId + '"]').eq(0)
                    .find(SELECTOR__SLIDE).eq(0),

            $master = $preview
                .find(SELECTOR__MASTER_SLIDE).eq(0)
                .children('[data-container-id="' + masterId + '"]').eq(0)
                .find(SELECTOR__SLIDE).eq(0);

        if (layoutId) { $layout.toggleClass(CLASS_NAME__INVISIBLE_DRAWINGS, !showLayoutSlide); }
        if (masterId) { $master.toggleClass(CLASS_NAME__INVISIBLE_DRAWINGS, !showMasterSlide); }
    }

    /**
     * @param {JQuery} $preview
     * @param {string} masterId
     * @returns {*|{name, minParams, maxParams, type, signature, resolve}}
     */
    function getMasterSlideContainerFromPreview($preview, masterId) {
        return $preview
            .find(SELECTOR__MASTER_SLIDE).eq(0)
            .children('[data-container-id="' + masterId + '"]').eq(0);
    }

    /**
     * @param {JQuery} $preview
     * @param {string} layoutId
     * @returns {*|{name, minParams, maxParams, type, signature, resolve}}
     */
    function getLayoutSlideContainerFromPreview($preview, layoutId) {
        return $preview
            .find(SELECTOR__LAYOUT_SLIDE).eq(0)
            .children('[data-container-id="' + layoutId + '"]').eq(0);
    }

    /**
     * @param {JQuery} $preview
     * @param {string} slideId
     * @returns {*|{name, minParams, maxParams, type, signature, resolve}}
     */
    function getStandardSlideContainerFromPreview($preview, slideId) {
        return $preview
            .find(SELECTOR__PAGE_CONTENT).eq(0)
            .children('[data-container-id="' + slideId + '"]').eq(0);
        //.find(SELECTOR__SLIDE).eq(0);
    }

    function renderMasterPreviewBackgroundProperly(data) {
        var
            layoutData        = data.layout,
            masterData        = data.master,

            isLayoutData      = !!(layoutData && layoutData.$container),

            hasOwnBackground  = (isLayoutData && docModel.isSlideWithOwnBackground(layoutData.slideId));

        if (isLayoutData) {
            layoutData.$container.children(SELECTOR__SLIDE).removeClass(CLASS_NAME__INVISIBLE_BACKGROUND);
            layoutData.$container.addClass(CLASS_NAME__UNFORCE_BACKGROUND);
        }
        masterData.$container.children(SELECTOR__SLIDE).removeClass(CLASS_NAME__INVISIBLE_BACKGROUND);
        masterData.$container.addClass(CLASS_NAME__UNFORCE_BACKGROUND);

        if (hasOwnBackground) {
            layoutData.$container.removeClass(CLASS_NAME__UNFORCE_BACKGROUND);

        } else {
            hasOwnBackground = docModel.isSlideWithOwnBackground(masterData.slideId);

            if (hasOwnBackground) {
                masterData.$container.removeClass(CLASS_NAME__UNFORCE_BACKGROUND);
            }
        }
    }

    function renderStandardPreviewBackgroundProperly(data) {
        var
            contentData       = data.content,
            layoutData        = data.layout,
            masterData        = data.master,

            hasOwnBackground  = docModel.isSlideWithOwnBackground(contentData.slideId);

        //contentData.$container.removeClass(CLASS_NAME__INVISIBLE_BACKGROUND);
        layoutData.$container.children(SELECTOR__SLIDE).removeClass(CLASS_NAME__INVISIBLE_BACKGROUND);
        masterData.$container.children(SELECTOR__SLIDE).removeClass(CLASS_NAME__INVISIBLE_BACKGROUND);

        contentData.$container.addClass(CLASS_NAME__UNFORCE_BACKGROUND);
        layoutData.$container.addClass(CLASS_NAME__UNFORCE_BACKGROUND);
        masterData.$container.addClass(CLASS_NAME__UNFORCE_BACKGROUND);

        if (hasOwnBackground) {
            contentData.$container.removeClass(CLASS_NAME__UNFORCE_BACKGROUND);

        } else {
            hasOwnBackground = docModel.isSlideWithOwnBackground(layoutData.slideId);

            if (hasOwnBackground) {
                layoutData.$container.removeClass(CLASS_NAME__UNFORCE_BACKGROUND);

            } else {
                hasOwnBackground = docModel.isSlideWithOwnBackground(masterData.slideId);

                if (hasOwnBackground) {
                    masterData.$container.removeClass(CLASS_NAME__UNFORCE_BACKGROUND);
                }
            }
        }
    }

    function renderPreviewBackgroundById($preview, slideId) {
        var
            layoutId,
            masterId;

        if (docModel.isStandardSlideId(slideId)) {

            layoutId = docModel.getLayoutSlideId(slideId);
            masterId = docModel.getMasterSlideId(layoutId);

            renderStandardPreviewBackgroundProperly({
                master: {
                    slideId:    masterId,
                    $container: getMasterSlideContainerFromPreview($preview, masterId)
                },
                layout: {
                    slideId:    layoutId,
                    $container: getLayoutSlideContainerFromPreview($preview, layoutId)
                },
                content: {
                    slideId,
                    $container: getStandardSlideContainerFromPreview($preview, slideId)
                }
            });
        } else if (docModel.isLayoutSlideId(slideId)) {

            layoutId = slideId;
            masterId = docModel.getMasterSlideId(layoutId);

            renderMasterPreviewBackgroundProperly({
                master: {
                    slideId:    masterId,
                    $container: getMasterSlideContainerFromPreview($preview, masterId)
                },
                layout: {
                    slideId:    layoutId,
                    $container: getLayoutSlideContainerFromPreview($preview, layoutId)
                }
            });
        } else { // if (docModel.isMasterSlideId(slideId)) {

            renderMasterPreviewBackgroundProperly({
                master: {
                    slideId,
                    $container: getMasterSlideContainerFromPreview($preview, slideId)
                }
            });
        }
    }

    /**
     * Master/Layout DOM Fragment Factory
     *
     * This method is a helper factory that creates jQuery-fied DOM
     * fragments according to the provided master/layout slide model ID.
     *
     * @param {String} slideId
     *  Any master/layout slide model ID.
     *
     * @returns {jQuery.ElementNode}
     *  Either a jQuery-fied `$master` or a `$layout` layer DOM fragment.
     */
    function createLayoutFragment(slideId) {
        var
            $fragmentSource = (docModel.getSlideById(slideId) || $()).parent(SELECTOR__SLIDE_CONTAINER),
            //$fragmentSource = docModel.getSlideById(slideId).parent(SELECTOR__SLIDE_CONTAINER),
            $fragment       = $fragmentSource.clone();

        preserveShapes($fragmentSource, $fragment);

        preventContendEditable($fragment);
        preventSelectedElement($fragment);

        preventApplicationTooltip($fragment);
        preventHighlightedFields($fragment);

        preventNotSelectable($fragment);
        preventInvisibleSlide($fragment);
        preventLeftShiftSlide($fragment);

        return $fragment;
    }

    /**
     * Initial Renderer of any Standard Preview
     *
     * This method reduces a list of master/layout slide model ID's.
     * Therefore it makes use of a `collector` parameter.
     * For every master/layout slide model ID it accordingly renders
     * a master/layout preview into the DOM.
     *
     * @param {String} slideId
     *  Any master/layout slide model ID.
     */
    function assignMasterPreviewInitially(slideId) {

        createAttributionItem(slideId);
        createThumbItem(slideId);

        var
            $preview = $masterPreviewBlueprint.clone(),

            $master,
            $layout,

            masterId, showMasterSlide,
            layoutId, showLayoutSlide;

        //globalLogger.info('+++ assignMasterPreviewInitially +++');
        //globalLogger.info('+++ assignMasterPreviewInitially - slideId : ', slideId);

        //globalLogger.info('+++ assignMasterPreviewInitially - ' + SELECTOR__MASTER_SLIDE + ' : ', $preview.find(SELECTOR__MASTER_SLIDE));
        //globalLogger.info('+++ assignMasterPreviewInitially - ' + SELECTOR__LAYOUT_SLIDE + ' : ', $preview.find(SELECTOR__LAYOUT_SLIDE));

        if (docModel.isMasterSlideId(slideId)) {

            $master = createLayoutFragment(slideId);

            previewMasterMap[slideId] = $master.clone(); // storing away a clean preview master element fragment for good.

            showMasterSlide = true;
            masterId        = slideId;
        } else {
            layoutId        = slideId;
            masterId        = docModel.getMasterSlideId(layoutId);

            showLayoutSlide = true;
            showMasterSlide = docModel.isFollowMasterShapeSlide(layoutId);

            $master = (previewMasterMap[masterId] && previewMasterMap[masterId].clone()) || $('<div/>'); // fail silently.
            $layout = createLayoutFragment(slideId);

            assureNotSelectableSlide($master);

            $layout.prependTo($preview.find(SELECTOR__LAYOUT_SLIDE));
        }
        $master.prependTo($preview.find(SELECTOR__MASTER_SLIDE));

        setLayoutMasterVisibility($preview, layoutId, masterId, showLayoutSlide, showMasterSlide);

        putPreview(slideId, $preview);
    }

    /**
     * Rerender Process of an already existing Master/Layout Preview
     *
     * This method iterates a list of master/layout slide model ID's.
     *
     * The whole iteration process gets provided an additional
     * `followMasterSlideMap` context. Thus this method easily
     * can access additional information of how to recompute the
     * visibility states of all preview layers.
     *
     * This method takes any master/layout slide model ID. It then
     * accordingly creates either a new `$master` layer only or both
     * a pair of `$master` / `$layout` layers in dependency of either
     * the provided ID is a master or a layout ID only. The newly
     * created layer(s) does/do replace the former one(s). Right after,
     * all visibility states get recomputed.
     *
     * @param {String} slideId
     *  Any master/layout slide model ID.
     *
     * @this {Object} followMasterSlideMap
     *  An object used as registry/map that as key contains every `slideId`
     *  that has master/layout dependencies and therefore will trigger the
     *  recalculation of all visibility states of this targeted layer set.
     */
    function reassignMasterPreview(slideId) {
        var
            followMasterSlideMap = this,

            $preview        = getPreview(slideId),
            //elmPreview      = $preview[0],

            $master,
            $layout,

            masterId, showMasterSlide,
            layoutId, showLayoutSlide;

        // @TODO - consider refactoring this block beneath - it right now does act as guard - is there a better solution to it?
        //
        if (!$preview) {
            masterId        = docModel.getMasterSlideId(slideId);

            assignMasterPreviewInitially(slideId);

            followMasterSlideMap = {};
            if (docModel.isFollowMasterShapeSlide(slideId)) {

                followMasterSlideMap[slideId] = true;
            }
            reassignMasterPreview.call(followMasterSlideMap, slideId);

            return;
            //
            // @TODO - consider refactoring this block above - it right now does act as guard - is there a better solution to it?

        }/* else if ( // necessary after DOM performance optimization
                    // that deals with 'replacementslide' classified
                    // and empty surrogate slides, that's content
                    // is not considered to become ever part of the
                    // preview store

            ($preview.html() === '')
            && (
                (('outerHTML' in elmPreview) && (elmPreview.outerHTML === '<div></div>'))
                || (
                    (elmPreview.tagName.toLowerCase() === 'div') && (elmPreview.className === '')
                )
            )
        ) {
            return;
        }*/

        if (docModel.isMasterSlideId(slideId)) {

            $master         = createLayoutFragment(slideId);

            showMasterSlide = true;
            masterId        = slideId;
        } else {
            layoutId        = slideId;
            masterId        = docModel.getMasterSlideId(layoutId);

            showLayoutSlide = true;
            showMasterSlide = docModel.isFollowMasterShapeSlide(layoutId);

            $master         = createLayoutFragment(masterId);
            $layout         = createLayoutFragment(layoutId);

            assureNotSelectableSlide($master);

            $layout.prependTo($preview.find(SELECTOR__LAYOUT_SLIDE).empty());
        }
        $master.prependTo($preview.find(SELECTOR__MASTER_SLIDE).empty());

        setLayoutMasterVisibility($preview, layoutId, masterId, showLayoutSlide, showMasterSlide);

        layoutId = docModel.getLayoutSlideId(slideId);
        masterId = docModel.getMasterSlideId(layoutId);

        if (slideId in followMasterSlideMap) {

            showLayoutSlide = docModel.isFollowMasterShapeSlide(slideId);
            showMasterSlide = showLayoutSlide ? docModel.isFollowMasterShapeSlide(layoutId) : false;

            setLayoutMasterVisibility($preview, layoutId, masterId, showLayoutSlide, showMasterSlide);
        }
        putPreview(slideId, $preview);

        flagThumbItemAsMustRender(slideId);
    }

    /**
     * Standard Preview DOM Fragment Factory
     *
     * This method takes any standard slide model ID. It then accordingly creates
     * a complete set of jQuery-fied layers (`$master`, `$layout`, `$content`), in
     * order to return a jQuery-fied standard preview representative that already
     * features all current visibility states.
     *
     * @param {String} slideId
     *  Any standard slide model ID.
     *
     * @returns {jQuery.ElementNode}
     *  A complete set of jQuery-fied layers (`$master`, `$layout`, `$content`)
     *  that represent a standard preview DOM fragment, including all of its
     *  current visibility states.
     */
    function createStandardPreview(slideId) {
        var
            slideIdSet      = docModel.getSlideIdSet(slideId),

            masterId        = slideIdSet.masterId,
            layoutId        = slideIdSet.layoutId,

            $masterSource   = masterId ? docModel.getSlideById(masterId).parent(SELECTOR__SLIDE_CONTAINER) : null,
            $layoutSource   = docModel.getSlideById(layoutId).parent(SELECTOR__SLIDE_CONTAINER),
            $contentSource  = docModel.getSlideById(slideId),

            $master         = masterId ? $masterSource.clone() : null,
            $layout         = $layoutSource.clone(),
            $content        = $contentSource.clone(),

            $preview        = $standardPreviewBlueprint.clone(),

            showLayoutSlide = docModel.isFollowMasterShapeSlide(slideId),
            showMasterSlide = showLayoutSlide ? docModel.isFollowMasterShapeSlide(layoutId) : false;

        if ($masterSource) { preserveShapes($masterSource, $master); }
        preserveShapes($layoutSource, $layout);
        preserveShapes($contentSource, $content);

        if ($master) { preventContendEditable($master); }
        preventContendEditable($layout);
        preventContendEditable($content);

        if ($master) { preventSelectedElement($master); }
        preventSelectedElement($layout);
        preventSelectedElement($content);

        if ($master) { preventApplicationTooltip($master); }
        preventApplicationTooltip($layout);
        preventApplicationTooltip($content);

        if ($master) { preventHighlightedFields($master); }
        preventHighlightedFields($layout);

        if ($master) { assureNotSelectableSlide($master); }
        assureNotSelectableSlide($layout);

        if ($master) { preventInvisibleSlide($master); }
        preventInvisibleSlide($layout);

        if ($master) { preventLeftShiftSlide($master); }
        preventLeftShiftSlide($layout);

        handleOdfVisibilityOfPlaceholderFooterDrawings(slideId, $layout);

        $content.removeClass(CLASS_NAME__INVISIBLE_SLIDE);  // this line acknowledges remote clients.

        if ($master) { $master.prependTo($preview.find(SELECTOR__MASTER_SLIDE)); }
        $layout.prependTo($preview.find(SELECTOR__LAYOUT_SLIDE));
        $content.prependTo($preview.find(SELECTOR__PAGE_CONTENT));

        setLayoutMasterVisibility($preview, layoutId, masterId, showLayoutSlide, showMasterSlide);

        return $preview;
    }

    /**
     * Rerender Process of an already existing Standard Preview
     *
     * This method iterates a list of standard slide model ID's.
     *
     * The whole iteration process gets provided an additional
     * `followMasterSlideMap` context. Thus this method easily
     * can access additional information of how to recompute the
     * visibility states of all preview layers.
     *
     * This method takes any standard slide model ID. It then accordingly
     * creates a minimum preview subset only. A new jQuery-fied `$content`
     * layer gets created. It does replace the former one. Right after,
     * all visibility states get recomputed.
     *
     * @param {String} slideId
     *  Any standard slide model ID.
     *
     * @this {Object} followMasterSlideMap
     *  An object used as registry/map that as key contains every `slideId`
     *  that has master/layout dependencies and therefore will trigger the
     *  recalculation of all visibility states of this targeted layer set.
     */
    function reassignStandardPreview(slideId) {
        var
            followMasterSlideMap = this,

            $preview        = getPreview(slideId),
            $container      = $preview ? $preview.find(SELECTOR__PAGE_CONTENT) : $(),

            //elmPreview      = $preview[0],

            $contentSource  = docModel.getSlideById(slideId),
            $content        = $contentSource ? $contentSource.clone() : $(),

            layoutId = docModel.getLayoutSlideId(slideId),
            masterId = docModel.getMasterSlideId(layoutId),

            showLayoutSlide, showMasterSlide;

        // if (  // necessary after DOM performance optimization
        //      // that deals with 'replacementslide' classified
        //      // and empty surrogate slides, that's content
        //      // is not considered to become ever part of the
        //      // preview store
        //
        //    ($preview.html() === '')
        //    && (
        //        (('outerHTML' in elmPreview) && (elmPreview.outerHTML === '<div></div>'))
        //        || (
        //            (elmPreview.tagName.toLowerCase() === 'div') && (elmPreview.className === '')
        //        )
        //    )
        //) {
        //    return;
        //}

        preserveShapes($contentSource, $content);

        preventContendEditable($content);
        preventSelectedElement($content);

        preventApplicationTooltip($content);
        preventHighlightedFields($content);

        $content.removeClass(CLASS_NAME__INVISIBLE_SLIDE);  // this line acknowledges remote clients.

        $content.prependTo($container.empty());

        if (slideId in followMasterSlideMap) {

            showLayoutSlide = docModel.isFollowMasterShapeSlide(slideId);
            showMasterSlide = showLayoutSlide ? docModel.isFollowMasterShapeSlide(layoutId) : false;

            setLayoutMasterVisibility($preview, layoutId, masterId, showLayoutSlide, showMasterSlide);
        }
        putPreview(slideId, $preview);

        flagThumbItemAsMustRender(slideId); // right now there is no need for standard-slide thumb-previews.
    }

    /**
     * Initial Renderer of any Standard Preview
     *
     * This method iterates a list of standard slide model ID's.
     * For every standard slide model ID it accordingly renders
     * a standard preview into the DOM.
     *
     * @param {String} slideId
     *  Any standard slide model ID.
     */
    function assignStandardPreviewInitially(slideId/*, idx, list*/) {

        createAttributionItem(slideId);
        createThumbItem(slideId);       // right now there is no need for standard-slide thumb-previews.

        var
            $preview = createStandardPreview(slideId);

        putPreview(slideId, $preview);
    }

    function assignEveryMasterPreviewItemInitially() {
        //var time = (new Date());

        docModel.getMasterSlideOrder().forEach(assignMasterPreviewInitially);

        //globalLogger.info('+++ assignEveryMasterPreviewItemInitially :: time : ', (new Date() - time));
    }

    function assignEveryStandardPreviewItemInitially() {
        //var time = (new Date());

        docModel.getStandardSlideOrder().forEach(assignStandardPreviewInitially);

        //globalLogger.info('+++ assignEveryStandardPreviewItemInitially :: time : ', (new Date() - time));
    }

    function triggerPreviewFormatting(slideIdList) {
        slideIdList = slideIdList.filter(function (slideId/*, idx, list*/) {

            // Performance: During loading process it can happen, that the first slides are ordered
            // by the slide pane, before the attributeMap is filled. This is the case, after the
            // event 'slidepane:init' was replaced by 'slideModel:init:thumbnail' (docs-891).
            // It the attributeMap is still empty, the corresponding slide must be formatted.

            return !attributionMap[slideId] || attributionMap[slideId].isMustFormat;
        });
        docModel.trigger('request:formatting', slideIdList);
    }

    const forceLayoutMasterSlideFormatting = fun.once(() => {
        const masterLayoutSlideIds = docModel.getMasterSlideOrder();
        triggerPreviewFormatting([
            ...masterLayoutSlideIds.filter(slideId => docModel.isMasterSlideId(slideId)),
            ...masterLayoutSlideIds.filter(slideId => docModel.isLayoutSlideId(slideId))
        ]);
    });

    function compilePreviewStatesInitially() {
        //var time = (new Date());

        attributionMap  = {};

        previewStore    = {};
        thumbRegistry   = {};

        assignEveryStandardPreviewItemInitially();
        assignEveryMasterPreviewItemInitially();

        //globalLogger.info('+++ compilePreviewStatesInitially :: time : ', (new Date() - time));
        //globalLogger.info('+++ compilePreviewStatesInitially :: attributionMap : ', attributionMap);
        //globalLogger.info('+++ compilePreviewStatesInitially :: previewStore : ', previewStore);
        //globalLogger.info('+++ compilePreviewStatesInitially :: thumbRegistry : ', thumbRegistry);
    }

    function compileAllDisplayStates() {

        isMasterView   = docModel.isMasterView();
        isStandardView = !isMasterView;

        //globalLogger.info('+++ compileAllDisplayStates :: isMasterView, isStandardView : ', isMasterView, isStandardView);
    }

    function handlePreviewCreation(slideIdList) {
        //globalLogger.info('+++ ENTER handlePreviewCreation +++');
        //
        //compileAllDisplayStates();

        var
            followMasterSlideMap  = {},

            standardSlideIdList   = [],
            layoutOrMasterIdList  = [];

        slideIdList.forEach(function (slideId) {
            if (docModel.isFollowMasterShapeSlide(slideId)) {

                followMasterSlideMap[slideId] = true;
            }
            if (docModel.isStandardSlideId(slideId)) {

                assignStandardPreviewInitially(slideId);
                unflagAttributionItemAsMustFormat(slideId);

                reassignStandardPreview.call(followMasterSlideMap, slideId);

                standardSlideIdList.push(slideId);

            } else {

                assignMasterPreviewInitially(slideId);
                unflagAttributionItemAsMustFormat(slideId);

                reassignMasterPreview.call(followMasterSlideMap, slideId);

                layoutOrMasterIdList.push(slideId);
            }
            renderPrestoredFieldData(slideId);
        });

        if (isStandardView) {
            if (standardSlideIdList.length > 0) {
                self.trigger('previewupdate:after', standardSlideIdList);
            }
            layoutOrMasterIdList = layoutOrMasterIdList.filter(docModel.isLayoutSlideId);

            if (layoutOrMasterIdList.length > 0) {
                self.trigger('layoutthumb:change', layoutOrMasterIdList);
            }
        } else if (layoutOrMasterIdList.length > 0) {
            self.trigger('previewupdate:after', layoutOrMasterIdList);
        }
    }

    /**
     * Handler for any of a document's 'slidestate:update' events.
     *
     * This handler is aware of a document's view modeindirectSlideIdList
     * and accordingly triggers the preview update process.
     */
    function handlePreviewUpdate(slideIdLists, followMasterSlideMap) {
        //globalLogger.info('+++ ENTER handlePreviewUpdate +++');
        //
        //compileAllDisplayStates();

        followMasterSlideMap = followMasterSlideMap || {};

        slideIdLists.nonStandardDependend.forEach(function (slideId) {
            assignMasterPreviewInitially(slideId);

            // Important! - do not unflag - depending slides are not necessarily fully atrributed/formated yet.
            //unflagAttributionItemAsMustFormat(slideId);

            // - empty slides in master-slave edit-mode for slave view  - https://bugs.open-xchange.com/show_bug.cgi?id=49358
            // - undisplayed slide background changes in slidepane      - https://bugs.open-xchange.com/show_bug.cgi?id=49059
            //
            // - restored to default again (that uses a lookup table)
            //   after applying a fix to the model's attribution and
            //   formatting process that now informs correctly via
            //   'slidestate:formatted' when a slide has reached its
            //   final formatting state.
            // - the above quickfix nevertheless remains (though
            //   commented now) in order to directly document the
            //   formerly targeted bug ids.
            //
            unflagAttributionItemAsMustFormat(slideId);

            reassignMasterPreview.call(followMasterSlideMap, slideId);
        });
        slideIdLists.nonStandardIndependend.forEach(function (slideId) {
            unflagAttributionItemAsMustFormat(slideId);
            reassignMasterPreview.call(followMasterSlideMap, slideId);
        });
        //slideIdLists.nonStandardIndependend.forEach(reassignMasterPreview, followMasterSlideMap);

        slideIdLists.standardDependend.forEach(function (slideId) {
            assignStandardPreviewInitially(slideId);

            // - empty slides in master-slave edit-mode for slave view  - https://bugs.open-xchange.com/show_bug.cgi?id=49358
            // - undisplayed slide background changes in slidepane      - https://bugs.open-xchange.com/show_bug.cgi?id=49059
            //
            // - restored to default again (that uses a lookup table)
            //   after applying a fix to the model's attribution and
            //   formatting process that now informs correctly via
            //   'slidestate:formatted' when a slide has reached its
            //   final formatting state.
            // - the above quickfix nevertheless remains (though
            //   commented now) in order to directly document the
            //   formerly targeted bug ids.
            //
            unflagAttributionItemAsMustFormat(slideId);

            reassignStandardPreview.call(followMasterSlideMap, slideId);
        });
        slideIdLists.standardIndependend.forEach(function (slideId) {
            unflagAttributionItemAsMustFormat(slideId);
            reassignStandardPreview.call(followMasterSlideMap, slideId);
        });
        //slideIdLists.standardIndependend.forEach(reassignStandardPreview, followMasterSlideMap);

        var
            standardSlideIdList   = [].concat(slideIdLists.standardIndependend, slideIdLists.standardDependend),
            layoutOrMasterIdList  = [].concat(slideIdLists.nonStandardIndependend, slideIdLists.nonStandardDependend);

        if (isStandardView) {
            if (standardSlideIdList.length > 0) {
                self.trigger('previewupdate:after', standardSlideIdList);
            }
            layoutOrMasterIdList = layoutOrMasterIdList.filter(docModel.isLayoutSlideId);

            if (layoutOrMasterIdList.length > 0) {
                self.trigger('layoutthumb:change', layoutOrMasterIdList);
            }
        } else if (layoutOrMasterIdList.length > 0) {
            self.trigger('previewupdate:after', layoutOrMasterIdList);
        }
    }

    function handleInsertSlide(data) {
        //data :: { id: id, isMasterView: isMasterView, documentReloaded: documentReloaded }
        var
            slideId = data.id;

        // do nothing, if the document was reloaded with a snapshot
        if (getBooleanOption(data, 'documentReloaded', false)) { return; }

        if (docModel.isStandardSlideId(slideId)) {

            assignStandardPreviewInitially(slideId);

            if (isStandardView) {
                self.trigger('previewupdate:after', [slideId]);
            }
        } else {
            assignMasterPreviewInitially(slideId);

            if (isMasterView) {
                self.trigger('previewupdate:after', [slideId]);
            }
        }
        unflagAttributionItemAsMustFormat(slideId);
    }

    function handleSlideSizeChange() {

        compileStandardPreviewBlueprint();
        compileMasterPreviewBlueprint();

        compilePreviewStatesInitially();
        docModel.getMasterSlideOrder().concat(docModel.getStandardSlideOrder()).forEach(unflagAttributionItemAsMustFormat);

        computeInitialThumbScaling();
    }

    function compileView() {
        //globalLogger.info('+++ ENTER compileView +++');
        //
        //var time = (new Date());

        if (!imgNodeBlueprint) { imgNodeBlueprint = $('<img/>')[0]; }

        compileStandardPreviewBlueprint();
        compileMasterPreviewBlueprint();

        compileAllDisplayStates();
        compilePreviewStatesInitially();

        computeInitialThumbScaling();

        self.getPreview       = getPreview;
        //self.getPreviewList   = getPreviewList;

        self.requestThumb     = requestThumb;
        //self.requestThumbList = requestThumbList;

        self.forceLayoutMasterSlideFormatting = forceLayoutMasterSlideFormatting;

        self.stopListeningTo(docModel, 'slidestate:formatted', handleSlideStateFormattedBeforeCompiledView);

        handlePreviewCreation(formattedSlideCache);
        handleSlideStateFormattedBeforeCompiledView = null;
        formattedSlideCache = [];

        self.listenTo(docModel, 'slidestate:formatted', handlePreviewCreation);
        self.listenTo(docModel, 'slidestate:update', handlePreviewUpdate);

        self.stopListeningTo(docModel, 'slideNumFields:update', handleFieldUpdateBeforeCompiledView);
        self.stopListeningTo(docModel, 'slideDateFields:update', handleFieldUpdateBeforeCompiledView);

        self.stopListeningTo(docModel, 'masterfield:update', handleFieldUpdateBeforeCompiledView);
        //self.stopListeningTo(docModel, 'masterfield:update', handleMasterFieldUpdateBeforeCompiledView); // please keep this commented line for debugging.

        //window.console.log('+++ fieldUpdateCache : ', fieldUpdateCache);
        handleFieldUpdate(fieldUpdateCache);
        handleFieldUpdateBeforeCompiledView = null;
        fieldUpdateCache = [];

        self.listenTo(docModel, 'slideNumFields:update', handleFieldUpdate);
        self.listenTo(docModel, 'slideDateFields:update', handleFieldUpdate);

        self.listenTo(docModel, 'masterfield:update', handleFieldUpdate);
        //self.listenTo(docModel, 'masterfield:update', handleMasterFieldUpdate); // please keep this commented line for debugging.

        self.listenTo(docModel, 'inserted:slide', handleInsertSlide);

        //globalLogger.info('+++ compileView :: time : ', (new Date() - time));

        self.trigger('slidepreview:init:after', {});
    }

    //
    // --------------------------------------------------------------------

    // public methods -----------------------------------------------------
    //

    this.getPreview       = $.noop;
    //this.getPreviewList   = $.noop;

    this.requestThumb     = $.noop;
    //this.requestThumbList = $.noop;

    this.forceLayoutMasterSlideFormatting = $.noop;

    //
    // --------------------------------------------------------------------

    // initialization -----------------------------------------------------
    //

    /**
     * lazy initialization after composition.
     */
    function initHandler() {

        IS_ODF    = self.docApp.isODF();

        docModel  = self.docModel;
        $pageNode = docModel.getNode();

        slidepane = self.getSlidePane();
        if (slidepane) {
            var $paneNode = slidepane.$el.find(SELECTOR__SLIDEPANE_CONTAINER).first();

            preventContendEditable($paneNode);

            self.listenTo(slidepane, 'visiblesliderange:changed', function (slideRangeState) {
            //window.console.log('+++ slidepreview - visiblesliderange:changed +++ [currentSlideRange, visibleSlideIdList] : ', slideRangeState.slideRanges.current, slideRangeState.slideIdLists.visible);

                triggerPreviewFormatting(slideRangeState.slideIdLists.visible);
            });
        }
        //globalLogger.info('+++ initHandler :: $pageNode : ', $pageNode);

        self.listenTo(docModel, 'change:activeView:after', compileAllDisplayStates);

        self.listenTo(docModel, 'slideModel:init:thumbnail', compileView);
        //self.listenTo(docModel, 'formatmanager:init', compileView);                                   // - too late.

        //self.listenTo(docModel, 'formatmanager:init', registerSlidestateHandlingBeforeCompiledView);  // - too complicated.
        self.listenTo(docModel, 'slidestate:formatted', handleSlideStateFormattedBeforeCompiledView);

        self.listenTo(docModel, 'slideNumFields:update', handleFieldUpdateBeforeCompiledView);
        self.listenTo(docModel, 'slideDateFields:update', handleFieldUpdateBeforeCompiledView);

        self.listenTo(docModel, 'masterfield:update', handleFieldUpdateBeforeCompiledView);
        //self.listenTo(docModel, 'masterfield:update', handleMasterFieldUpdateBeforeCompiledView); // please keep this commented line for debugging.

        // slide size has changed - e.g. orientation change.
        self.listenTo(docModel, 'slidesize:change', handleSlideSizeChange);
    }
}
