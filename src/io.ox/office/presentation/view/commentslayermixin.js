/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { is } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, convertHmmToLength, createIcon } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { CommentsPane } from '@/io.ox/office/editframework/view/pane/commentspane';
import { CommentPopup } from '@/io.ox/office/editframework/view/popup/commentpopup';

import { PRESENTATION_COMMENTBUBBLENODE_CLASS } from '@/io.ox/office/textframework/utils/dom';

import '@/io.ox/office/presentation/view/style.less';

// constants ==============================================================

var CLASS_NAME__COMMENT_BUBBLE          = PRESENTATION_COMMENTBUBBLENODE_CLASS;
var CLASS_NAME__COMMENT_BUBBLE_LAYER    = CLASS_NAME__COMMENT_BUBBLE + '-layer';
var CLASS_NAME__COMMENT_BUBBLE_HOVER    = 'hover';
var CLASS_NAME__COMMENT_BUBBLE_SELECTED = 'selected';

// mixin class CommentsLayerMixin =========================================

/**
 * Additional preview specific behavior for the presentation view.
 *
 * Does provide a set of specific functionality that is just for rendering
 * the preview of any presentation slide that most recently has been changed.
 *
 * @mixin
 *  for class `PresentationView`
 */
function CommentsLayerMixin() {
    var
        docView = this,     // self reference :: PresentationView

        docApp,
        docModel,           // {PresentationModel} docModel ... the document model created by the passed application.
        docController,      // {PresentationController} docController ... the document controller created by the passed application.

        slidepane,          // the {SlidePane} reference ... it is available via `docView.getSlidePane()` immediately after `docView` got initialized.

        $pageNode,
        $bubbleLayer,

        $bubbleBlueprint,
        $bubblesBlueprint,

        isEverySlidePaneThreadCountInitiallyRendered  = false,
        isSlidePaneContentAccessible                  = false,

        isBubbleLayerInitialized        = false,

        zoomFactor = 1,
        isPositionChanged = false;

    var commentPopup = this.member(new CommentPopup());

    // modify initial `initHandler` with mixin specific `init` functionality via basic function composition.
    this.config.initHandler = _.wrap(this.config.initHandler, (baseInitHandler, options) => {
        baseInitHandler.call(this, options);
        initHandler(); // the `CommentsLayerMixin`s specific `initHandler` functionality.
    });

    // local / private methods --------------------------------------------

    function initializeBubbleLayer() {
        if (isBubbleLayerInitialized) {

            $bubbleLayer.empty();
        } else {
            // $pageNode.append($bubbleLayer);

            $pageNode[0].appendChild($bubbleLayer[0]);

            isBubbleLayerInitialized = true;
        }
    }

    function showCommentsPane() {
        if (docView.commentsPane && !docView.commentsPane.isVisible()) {
            docView.commentsPane.show();
        }
    }

    function getCommentBubbleRelatedThreadedCommentModel(commentBubble) {

        var commentCollection = docView.getCommentCollection();
        var commentId         = $(commentBubble).attr('data-comment-id');

        return commentCollection.getCommentById(commentId);
    }

    function getThreadedCommentModelRelatedCommentBubble(commentModel) { // a `ThreadedCommentModel` instance/reference.
        var $commentBubble;

        commentModel = (commentModel && (commentModel.getParent() || commentModel));
        if (commentModel) {
            $commentBubble = getCommentBubbleById(commentModel.getId());
        }

        return $commentBubble; // undefined || {jQuery}
    }

    function getCommentBubbleById(commentId) {
        var $commentBubble = $bubbleLayer.find('[data-comment-id="' + commentId + '"]').eq(0);
        return ($commentBubble[0] && $commentBubble); // undefined || {jQuery}
    }

    function deselectAnyCommentBubble() {
        $bubbleLayer.find('[data-comment-id]').removeClass(CLASS_NAME__COMMENT_BUBBLE_SELECTED);
        commentPopup.hide();
    }

    function selectCommentBubble(commentModel) {

        var $commentBubble  = getThreadedCommentModelRelatedCommentBubble(commentModel);

        if ($commentBubble) {
            $commentBubble.addClass(CLASS_NAME__COMMENT_BUBBLE_SELECTED);
            if (SMALL_DEVICE) {
                var commentFrame = docView.commentsPane.detachOrCreateCommentFrame(commentModel);
                if (commentFrame) {
                    commentPopup.setCommentFrame(commentFrame);
                    commentPopup.setAnchor($commentBubble);
                    commentPopup.show();
                }
            }
            return true;
        }

        return false;
    }

    /**
     *  ... all the recent changes are due to fixing #Bug DOCS-1579 ...
     *
     *  (IE 11) - Insert a comment - close-/re-edit: Comment is not shown
     *  [https://jira.open-xchange.com/browse/DOCS-1579]
     *
     *  fix for special issue raised by Maike Henschel at 2019-12-05 08:18 ...
     *  ... And clicking on the bubble throws a type error ...
     *  ... => Die Eigenschaft "getBoundingClientRect" eines undefinierten oder Nullverweises kann nicht abgerufen werden.
     *
     *  This is due to IE11 and below that do neither recognize `parentNode` nor
     *  `parentElement` for SVG root nodes that ARE child nodes of an outer HTML-DOM.
     *
     *  The straightforward solution of binding all necessary data to an event handler
     *  bypasses the problem of a backward search traversal in order to find a parent
     *  HTML-Node. It also is considered to be the more elegant approach.
     */
    function handleCommentBubbleMousedown(evt, commentModel, commentNode) {
        evt.stopPropagation();

        if (evt.button === 0) { // moving bubble only via left mouse
            drag(commentModel, commentNode, evt, { constrainingEl: document.querySelector('.app-content-root'), coordEl: document.querySelector('.app-content') });
        } else {

            docModel.getSelection().setSlideSelection({ triggerChange: false });

            deselectAnyCommentBubble();

            if (selectCommentBubble(commentModel)) {
                docView.getCommentCollection().selectComment(commentModel);
            } else {
                docView.trigger('thread:deselect:all');
            }
        }
    }

    function handleCommentBubbleClick(evt, commentModel) {
        evt.stopPropagation();
        evt.preventDefault();

        if (isPositionChanged) {
            return (isPositionChanged = false);
        }

        /**
         *  - resetting an existing selection to avoid further text input.
         *  - also prevent triggering of the main 'change' event of almost any slide selection activity.
         */
        docModel.getSelection().setSlideSelection({ triggerChange: false });

        deselectAnyCommentBubble();

        if (selectCommentBubble(commentModel)) {
            docView.getCommentCollection().selectComment(commentModel);
        } else {
            docView.trigger('thread:deselect:all');
        }
        showCommentsPane();

        // inform about a selected (parent) comment ... e.g. threaded-comments-pane listens to this event.
        docModel.trigger('select:threadedcomment', commentModel);

        docController.update();
    }

    function handleCommentBubbleMouseenter(evt, commentModel) {
        docModel.trigger('commentbubble:mouseenter', { originalEvent: evt, commentModel });
    }

    function handleCommentBubbleMouseleave(evt, commentModel) {
        docModel.trigger('commentbubble:mouseleave', { originalEvent: evt, commentModel });
    }

    function handleCommentThreadMouseenter(payload) { // payload:{ originalEvent: {DOMEvent}, commentModel: {ThreadedCommentModel} }

        var commentModel    = payload.commentModel; // - a `ThreadedCommentModel` instance/reference.
        var $commentBubble  = getThreadedCommentModelRelatedCommentBubble(commentModel);

        if ($commentBubble) {
            $commentBubble.addClass(CLASS_NAME__COMMENT_BUBBLE_HOVER);
        }
    }

    function handleCommentThreadMouseleave(payload) { // payload:{ originalEvent: {DOMEvent}, commentModel: {ThreadedCommentModel} }

        var commentModel    = payload.commentModel; // - a `ThreadedCommentModel` instance/reference.
        var $commentBubble  = getThreadedCommentModelRelatedCommentBubble(commentModel);

        if ($commentBubble) {
            $commentBubble.removeClass(CLASS_NAME__COMMENT_BUBBLE_HOVER);
        }
    }

    function handleCommentBubbleContextMenu(payload) {
        if (!docView.commentsPane.isEditMode()) {

            var commentCollection = docView.getCommentCollection();
            var commentModel = getCommentBubbleRelatedThreadedCommentModel(payload.node);

            if (commentModel) {
                commentCollection.selectComment(commentModel);
            }
        }
    }

    function handleCommentThreadDeselectAll() {
        deselectAnyCommentBubble();
        docView.trigger('thread:deselect:all');
    }

    function handleCommentThreadSelection(payload) { // payload:{ commentParent: {ThreadedCommentModel}, commentChild: {ThreadedCommentModel|null} }

        var commentParent = payload.commentParent; // - a `ThreadedCommentModel` instance/reference.

        deselectAnyCommentBubble();
        selectCommentBubble(commentParent);

        // inform about a selected (parent) comment ... e.g. threaded-comments-pane listens to this event.
        docModel.trigger('select:threadedcomment', commentParent);
    }

    function handleSlideSelectionChange() {
        deselectAnyCommentBubble();
        docModel.trigger('select:threadedcomment');
    }

    function rerenderCommentBubbleAtFirstReply(commentModel) {
        var $commentBubble = getThreadedCommentModelRelatedCommentBubble(commentModel);
        if ($commentBubble) {
            $commentBubble.empty().append(createIcon('svg:comment-reply-o', 25));
        }
    }
    function rerenderCommentBubbleAtNoReply(commentModel) {
        var $commentBubble = getThreadedCommentModelRelatedCommentBubble(commentModel);
        if ($commentBubble) {
            $commentBubble.empty().append(createIcon('svg:comment-o', 25));
        }
    }

    function removeCommentBubble(commentModel) {
        var $commentBubble = getThreadedCommentModelRelatedCommentBubble(commentModel);
        if ($commentBubble) {
            $commentBubble.off('mousedown click mouseenter mouseleave');
            $commentBubble.remove();
        }
    }

    function setCommentBubblePosition($commentBubble, commentModel) {
        var positionList = commentModel.getPositionList();
        $commentBubble.css('left', convertHmmToLength(positionList[0], 'px', 1));
        $commentBubble.css('top', convertHmmToLength(positionList[1], 'px', 1));
    }

    function changeCommentBubblePosition(commentModel) {
        var $commentBubble = getCommentBubbleById(commentModel.getId());
        if ($commentBubble) {
            setCommentBubblePosition($commentBubble, commentModel);
        }
    }

    function renderCommentBubble(commentCollection, commentModel) {
        // GUARD
        if (commentModel.isReply() || getCommentBubbleById(commentModel.getId()) || (docModel.getActiveSlideId() !== commentModel.getSlideId())) {
            return;
        }
        zoomFactor = docView.getZoomFactor();

        var childCommentList = commentCollection.getChildComments(commentModel);
        var doesThreadExist = (childCommentList !== null) && (childCommentList.length >= 1);
        var $commentBubble = (doesThreadExist ? $bubblesBlueprint : $bubbleBlueprint).clone();

        setCommentBubblePosition($commentBubble, commentModel);

        $commentBubble.attr('data-comment-id', commentModel.getId()); // comment position ... index (or count) of a comment in relation to a certain slide.

        const authorColorIndex = docApp.getAuthorColorIndex(commentModel.getAuthor());
        const commentAuthorClass = is.number(authorColorIndex) ? `comment-author-${authorColorIndex}` : '';
        if (commentAuthorClass) { $commentBubble.addClass(commentAuthorClass); }

        $commentBubble.on('mousedown', function (event) { handleCommentBubbleMousedown(event, commentModel, $commentBubble[0]); });
        $commentBubble.on('click', function (event) { handleCommentBubbleClick(event, commentModel); });
        $commentBubble.on('mouseenter', function (event) { handleCommentBubbleMouseenter(event, commentModel); });
        $commentBubble.on('mouseleave', function (event) { handleCommentBubbleMouseleave(event, commentModel); });

        $bubbleLayer.append($commentBubble);
    }

    function handleRenderCommentBubble(commentModel) {
        var commentCollection = docModel.getCommentCollection(commentModel.getSlideId());
        renderCommentBubble(commentCollection, commentModel);
    }

    function renderSlidePaneThreadCount(slideId, threadCount) {
        var $slideContainer = slidepane.getSlideContainerById(slideId); // {jQuery}
        $slideContainer.find('.comment-thread-count').attr("data-thread-count", threadCount);
    }

    function renderEverySlidePaneThreadCountInitially() {
        docModel.getStandardSlideOrder().forEach(slideId => {
            var theadCount = docModel.getSlideSpecificThreadCount(slideId);
            renderSlidePaneThreadCount(slideId, theadCount);
        });
    }

    function handleSlidePaneThreadCountChange(payload) {
        // payload: { slideId: {slideId}, current: {threadCountCurrent}, recent: {threadCountRecent} }
        if (isSlidePaneContentAccessible) {
            renderSlidePaneThreadCount(payload.slideId, payload.current);
        }
    }

    function handleSlidePaneThreadCountAccessOnce(payload, leaveMasterView) {

        // GUARD
        if (leaveMasterView) { isEverySlidePaneThreadCountInitiallyRendered = false; } // DOCS-2181, full update required
        if (isEverySlidePaneThreadCountInitiallyRendered) {
            return;
        }
        globalLogger.info('$badge{CommentsLayerMixin} handleSlidePaneThreadCountAccessOnce: payload', payload);

        renderEverySlidePaneThreadCountInitially();

        isEverySlidePaneThreadCountInitiallyRendered = true;
        isSlidePaneContentAccessible = true;
    }

    /**
     * Function that for drag&drop of the comments bubbles.
     * It contains a couple of additional options like
     * limiting container, which limits the movement of the comment bubble;
     * coordinate system container, which serves as a starting point
     */
    function drag(threadedCommentModel, elementToDrag, event, options) {
        zoomFactor = docView.getZoomFactor();

        var scroll = getScrollOffsets();
        var scrollW = getScrollbarWidth();
        var startX = event.clientX + scroll.x; // The original mouse position (in document coordinates) of the element
        var startY = event.clientY + scroll.y;
        var elBoundingClientRect = elementToDrag.getBoundingClientRect();
        var origX = elBoundingClientRect.left;
        var origY = elBoundingClientRect.top;

        var elementToDragW = elBoundingClientRect.width;
        var elementToDragH = elBoundingClientRect.height;

        // the container, which is the coordinate system starting point for the comments
        var coordStartEl = event.currentTarget.parentNode; // TODO: loop through the parents to check which has a value different then static.
        var coordStartElBoundingClientRect = coordStartEl.getBoundingClientRect();
        var coordStartX = coordStartElBoundingClientRect.left;
        var coordStartY = coordStartElBoundingClientRect.top;

        var constrainingEl = options && options.constrainingEl; // the area, over which the comment bubble can be dragged
        var constrainingElBoundingClientRect = null;
        var constrainXLeft = 0;
        var constrainXRight = 0;
        var constrainYTop = 0;
        var constrainYBottom = 0;

        // Compute the distance between the mouse down event and the upper-left
        // corner of the element. The distanse will be maintained this distance as the mouse moves.
        var deltaX = startX - origX;
        var deltaY = startY - origY;

        var bubbleX = 0;
        var bubbleY = 0;

        if (constrainingEl) {
            constrainingElBoundingClientRect = constrainingEl.getBoundingClientRect();
            constrainXLeft = constrainingElBoundingClientRect.left; // left offset of the constraining element
            // right offset of the constraining element, limiting the movement
            // of the comment bubble on the right side of the constraining container
            constrainXRight = constrainingElBoundingClientRect.right - scrollW - elementToDragW;
            constrainYTop = constrainingElBoundingClientRect.top; // top offset of the constraining element
            constrainYBottom = constrainingElBoundingClientRect.bottom - elementToDragH; // bottom offset of the constraining element
        }

        document.addEventListener('mousemove', moveHandler, true);
        document.addEventListener('mouseup', upHandler, true);

        event.stopPropagation();
        event.preventDefault();

        function getScrollbarWidth() {
            var outer = document.createElement('div');
            outer.style.visibility = 'hidden';
            outer.style.overflow = 'scroll';
            outer.style.msOverflowStyle = 'scrollbar';
            document.body.appendChild(outer);

            var inner = document.createElement('div');
            outer.appendChild(inner);

            var scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);
            outer.parentNode.removeChild(outer);

            return scrollbarWidth + 1;
        }

        function getScrollOffsets() {

            if (window.pageXOffset !== null) {
                return { x: window.pageXOffset, y: window.pageYOffset };
            }

            return { x: document.body.scrollLeft, y: document.body.scrollTop };
        }

        /**
         * This is the handler that captures mousemove events when an element
         * is being dragged. It is responsible for moving the element
         */
        function moveHandler(e) {
            // Move the element to the current mouse position, adjusted by the
            // position of the scrollbars and the offset of the initial click.
            scroll = getScrollOffsets();

            elementToDrag.style.left = (function () {
                var left = (function () {
                    var coordLeft = e.clientX + scroll.x - deltaX;

                    if (constrainingEl) {
                        if (coordLeft < constrainXLeft) {
                            coordLeft = constrainXLeft;
                        } else if (coordLeft > constrainXRight) {
                            coordLeft = constrainXRight;
                        }
                    }

                    return coordLeft;
                }());

                bubbleX = (left - coordStartX) / zoomFactor;

                return bubbleX + 'px';
            }());
            elementToDrag.style.top = (function () {
                var top = (function () {
                    var coordTop = e.clientY + scroll.y - deltaY;

                    if (constrainingEl) {
                        if (coordTop < constrainYTop) {
                            coordTop = constrainYTop;
                        } else if (coordTop > constrainYBottom) {
                            coordTop = constrainYBottom;
                        }
                    }

                    return coordTop;
                }());

                bubbleY = (top - coordStartY) / zoomFactor;

                return bubbleY + 'px';
            }());

            e.stopPropagation();
        }

        /**
         * This is the handler that captures the final mouseup event that
         * occurs at the end of a drag.
         */
        function upHandler(e) {
            var currentX = e.clientX + scroll.x - deltaX;
            var currentY = e.clientY + scroll.y - deltaY;

            e.stopPropagation();

            document.removeEventListener('mouseup', upHandler, true);
            document.removeEventListener('mousemove', moveHandler, true);

            if (currentX !== origX || currentY !== origY) {
                isPositionChanged = true;
                docView.trigger('bubble:position:change', { model: threadedCommentModel, node: elementToDrag, position: { x: bubbleX, y: bubbleY } });
            }
        }
    }

    function handleSlideChange(slideId) {

        initializeBubbleLayer();

        var commentCollection = docModel.getCommentCollection(slideId);
        if (commentCollection) {
            commentCollection.getParentComments().forEach(function (commentModel) {
                renderCommentBubble(commentCollection, commentModel);
            });
        }

        docView.commentsPane.repaintAllComments();
    }

    // public methods -----------------------------------------------------

    /**
     * Returns the (threaded) comment collection of the currently active slide.
     *
     * @returns {Opt<CommentCollection>}
     *  The cell comment collection instance of the active sheet.
     */
    docView.getCommentCollection = function () {
        // Fix: DOCS-2180
        return docModel?.getCommentCollection(docModel.getActiveSlideId());
    };

    /**
     * Activates the slide that contains a comment with unsaved changes.
     *
     * This methods overwrites the dummy method defined in `EditView` to
     * implement the specific behaviour for presentations.
     */
    docView.activateUnsavedCommentCollection = function () {

        var slideId = docModel.getFirstBestSlideIdWithUnsavedComment();
        if (is.string(slideId)) {
            docModel.setActiveSlideId(slideId);
        }
    };

    // initialization -----------------------------------------------------

    /**
     * lazy initialization after composition.
     */
    function initHandler() {

        // no support for comment threads in ODF
        docApp = docView.docApp;
        if (docApp.isODF()) { return; }

        docModel      = docApp.docModel;
        docController = docApp.docController;

        slidepane = docView.getSlidePane();
        $pageNode = docModel.getNode();

        $bubbleLayer          = $('<div class="' + CLASS_NAME__COMMENT_BUBBLE_LAYER + '" data-prevent-preview />');

        $bubbleBlueprint      = $('<a class="' + CLASS_NAME__COMMENT_BUBBLE + '">').append(createIcon('svg:comment-o', 25));
        $bubblesBlueprint     = $('<a class="' + CLASS_NAME__COMMENT_BUBBLE + '">').append(createIcon('svg:comment-reply-o', 25));

        // add the CommentsPane
        const commentsPane = docView.commentsPane = docView.addPane(new CommentsPane(docView, gt('There are no comments on this slide.')));

        if (slidepane) {
            docView.listenTo(slidepane, 'visiblesliderange:changed', handleSlidePaneThreadCountAccessOnce);
            docView.listenTo(docModel, 'threadedcommentcollection:threadcount:change', handleSlidePaneThreadCountChange);
        }

        docView.listenTo(commentsPane, 'thread:mouseenter', handleCommentThreadMouseenter);
        docView.listenTo(commentsPane, 'thread:mouseleave', handleCommentThreadMouseleave);
        docView.listenTo(commentsPane, 'thread:deselect:all', handleCommentThreadDeselectAll);

        docView.listenTo(docModel, 'change:threadedcomment:firstreply', rerenderCommentBubbleAtFirstReply);
        docView.listenTo(docModel, 'change:threadedcomment:noreply', rerenderCommentBubbleAtNoReply);
        docView.listenTo(docModel, 'delete:threadedcomment:thread', removeCommentBubble);

        docView.listenTo(docModel, 'change:threadedcomment', changeCommentBubblePosition);
        docView.listenTo(docModel, 'new:threadedcomment:thread insert:threadedcomment', handleRenderCommentBubble);

        docView.listenTo(docView, 'contextmenu:init:threadedcomment', handleCommentBubbleContextMenu);

        docView.listenTo(docModel, 'change:activeslide:done', handleSlideChange);
        docView.listenTo(docModel, 'select:threadedcomment:collection', handleCommentThreadSelection);

        // listen to the main 'change' event of almost any slide selection activity.
        docView.listenTo(docModel.getSelection(), 'change', handleSlideSelectionChange);

        // jump to a comment thread specified in launch options
        docView.waitForImportSuccess(function () {

            var commentId = docApp.getLaunchOption('comment_id');
            var slideId = commentId ? docModel.getSlideIdFromComment(commentId) : undefined;
            var commentCollection = slideId ? docModel.getCommentCollection(slideId) : undefined;
            if (!commentCollection) { return; }

            var timestamp = parseInt(commentId, 10);
            var targetModel = _.find(commentCollection.getCommentModels(), function (threadModel) {
                return new Date(threadModel.getDate()).getTime() === timestamp;
            });

            if (targetModel) {
                commentsPane.selectThread(targetModel);
            }
        });
    }
}

// exports ================================================================

export default CommentsLayerMixin;
