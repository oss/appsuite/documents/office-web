/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import TextBaseView from "@/io.ox/office/textframework/view/view";
import CommentCollection from "@/io.ox/office/presentation/model/threadedcommentcollection";
import PresentationModel from "@/io.ox/office/presentation/model/docmodel";
import PresentationApp from "@/io.ox/office/presentation/app/application";

// class PresentationView =====================================================

export default class PresentationView extends TextBaseView {

    declare readonly docApp: PresentationApp;
    declare readonly docModel: PresentationModel;

    public constructor(docApp: PresentationApp, docModel: PresentationModel);

    getCommentCollection(): CommentCollection | null;
}
