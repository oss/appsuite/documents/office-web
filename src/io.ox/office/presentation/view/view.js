/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { json } from '@/io.ox/office/tk/algorithms';
import {
    SMALL_DEVICE, COMPACT_DEVICE, TOUCH_DEVICE, IOS_SAFARI_DEVICE, SCROLLBAR_WIDTH,
    containsNode, addDeviceMarkers, parseCssLength,
    getFocus, setFocus, hideFocus
} from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getBooleanOption, getChildNodePositionInNode,
    getNumberOption, getStringOption, isContextEventTriggeredByKeyboard, isElementNode, isSoftKeyboardOpen, scrollToChildNode,
    scrollToPageRectangle } from '@/io.ox/office/tk/utils';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { isModifyingDrawingActive } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { COMBINED_TOOL_PANES, MAX_TABLE_COLUMNS, MAX_TABLE_ROWS, SPELLING_ENABLED } from '@/io.ox/office/textframework/utils/config';
import { getPixelPositionToRootNodeOffset } from '@/io.ox/office/textframework/utils/position';
import { COMMENTMARGIN_CLASS, isPresentationCommentBubbleTarget } from '@/io.ox/office/textframework/utils/dom';
import TextBaseView from '@/io.ox/office/textframework/view/view';
import { CELL_BORDERS_LABEL, DELETE_DRAWING_BUTTON_OPTIONS, FONT_HEADER_LABEL, FOOTER_BUTTON_OPTIONS, FORMAT_HEADER_LABEL,
    GROUP_DRAWING_TOOLTIP, HYPERLINK_BUTTON_OPTIONS, INSERT_COMMENT_OPTIONS, INSERT_HEADER_LABEL, OPTIONS_LABEL,
    PARAGRAPH_LABEL, PRESENT_HEADER_LABEL, REVIEW_HEADER_LABEL, SHOW_COLLABORATORS_CHECKBOX_OPTIONS, SHOW_COMMENTSPANE_CHECKBOX_OPTIONS,
    SHOW_SLIDEPANE_CHECKBOX_OPTIONS, SHOW_SNAPLINES_CHECKBOX_OPTIONS, SHOW_TOOLBARS_CHECKBOX_OPTIONS, SLIDE_HEADER_LABEL,
    TABLE_HEADER_LABEL, UNGROUP_DRAWING_TOOLTIP, ZOOM_SCREEN_WIDTH_LABEL } from '@/io.ox/office/textframework/view/labels';
import { checkEventPosition, findValidDrawingNodeBehindDrawing } from '@/io.ox/office/presentation/utils/presentationutils';
import ObjectOperationMixin from '@/io.ox/office/presentation/model/objectoperationmixin';
import { PresentationContextMenu } from '@/io.ox/office/presentation/view/popup/presentationcontextmenu';
import { BorderFlagsPicker, BorderWidthPicker, Button, CheckBox, ColorPicker, CompoundButton,
    DrawingAlignmentPicker, DrawingArrangementPicker, DrawingLineStylePicker, DrawingArrowPresetPicker, DrawingLineColorPicker, DrawingFillColorPicker,
    ImageCropPosition, ImagePicker, InsertTextFrameButton,
    LanguagePicker, PlaceholderTypePicker, InsertFieldPicker, RadioGroup, ShapeTypePicker, SwitchSlideEditModeButton,
    TableAlignmentPicker, TableSizePicker, TableStylePicker, StartPresentationPicker } from '@/io.ox/office/presentation/view/controls';
import { BoxAlignmentToolBar, CommentsToolBar, FontColorToolBar, FontFamilyToolBar, FontStyleToolBar, FormatPainterToolBar,
    ListStyleToolBar, SlideToolBar } from '@/io.ox/office/presentation/view/toolbars';
import { DrawingFormatDialog, PageSettingsDialog, SlideBackgroundDialog } from '@/io.ox/office/presentation/view/dialogs';
import CommentsLayerMixin from '@/io.ox/office/presentation/view/commentslayermixin';
import SlidePreviewMixin from '@/io.ox/office/presentation/view/slidepreviewmixin';
import { SlidePane } from '@/io.ox/office/presentation/view/pane/slidepane';
import { StatusPane } from '@/io.ox/office/presentation/view/pane/statuspane';

import '@/io.ox/office/presentation/view/style.less';

// constants ==================================================================

// predefined zoom factors
const ZOOM_STEPS = [0.35, 0.5, 0.75, 1, 1.5, 2, 3, 4, 6, 8];

// class PresentationView =====================================================

/**
 * The presentation view.
 *
 * Triggers the events supported by the base class EditView, and the
 * following additional events:
 * - 'change:zoom': When the zoom level for the displayed document was changed.
 *
 * @param {TextApplication} app
 *  The application containing this view instance.
 *
 * @param {TextModel} docModel
 *  The document model created by the passed application.
 */
export default class PresentationView extends TextBaseView {

    constructor(app, docModel) {

        // base constructor
        super(app, docModel, {
            zoomSteps: ZOOM_STEPS,
            initHandler,
            grabFocusHandler,
            contentScrollable: true,
            contentMargin: (SMALL_DEVICE ? 0 : 30),
            enablePageSettings: true,
            showOwnCollaboratorColor: true
        });

        // self reference
        const self = this;

        // the page node
        let pageNode = null;

        // the 'slide pane' reference
        let slidepane = null;

        // scroll position of the application pane
        let scrollPosition = { left: 0, top: 0 };

        // the vertical position of the application pane
        let verticalScrollPosition = 0;

        // outer width of page node, used for zoom calculations
        let pageNodeWidth = 0;

        // outer height of page node, used for zoom calculations
        let pageNodeHeight = 0;

        // the generic content menu for all types of contents
        let contextMenu = null;

        // whether the vertical scrolling happens with pressed mouse button (on the scroll bar)
        let isMouseButtonPressedScrolling = false;

        // whether the slide changed during scrolling with pressed mouse button. If yes, the
        // slide position needs to be adapted when mouse button is released.
        let slideChanged = false;

        // whether the vertical scroll position was really modified, when mouse button was pressed.
        // This marker is required to distinguish this scrolling from moving of drawings.
        let verticalScrollChanged = false;

        // the handler for the vertical scroll bar events
        let refreshVerticalScrollBar = $.noop;

        // whether there is a scroll event triggered by scrolling into selection
        let scrollingToPageRectangle = false;

        // storage for last know touch position
        // (to open context menu on the correct position)
        let touchX = null;
        let touchY = null;

        // A virtual focus to indicate whether the slidepane or the document
        // has the focus. The browser focus can't be used for this because of
        // clashing with the clipboard.This is needed for keyboard handling and shortcuts
        let slidepaneHasFocus = false;

        // whether snap guide lines for aligning shapes on slide are enabled or not
        let snapGuideLinesEnabled = true;

        // the drawing background dialog
        let drawingFormatDialog = null;

        // a vertical scroll blocking that is required in Chrome and Edge browser to avoid repeated
        // slide changes, after a slide change happened (flickering). This seems to be caused by
        // modified behavior of Chrome browser and might be removed in the future.
        let scrollBlockIsActive = false;

        // a vertical scroll blocking that is required in Chrome and Edge browser to avoid 'backjumps'
        // to a previous selected slide, after a slide change happened. This seems to be caused by
        // modified behavior of Chrome browser and might be removed in the future.
        let isChangeToFollowingSlide = false;
        let isChangeToPreviousSlide = false;

        // debounced function that checks incoming vertical scroll events (DOCS-3084)
        let showActiveSlideDebounced = $.noop;

        // whether a slide change animation is running -> blocking following slide changes
        let isSlideChangeAnimationRunning = false;

        // mixins -------------------------------------------------------------

        ObjectOperationMixin.call(this, app);

        // private methods ----------------------------------------------------

        /**
         * Handler for slide changes.
         *
         * @param {boolean} before
         *  Whether this event was emitted before the slide changes (event
         *  "change:slide:before").
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.newSlideId='']
         *      The id of the standard slide that will be activated.
         *  @param {String} [options.newLayoutId='']
         *      The id of the layout slide that will be activated.
         *  @param {String} [options.newMasterId='']
         *      The id of the master slide that will be activated.
         *  @param {String} [options.oldSlideId='']
         *      The id of the standard slide that will be deactivated.
         *  @param {String} [options.oldLayoutId='']
         *      The id of the layout slide that will be deactivated.
         *  @param {String} [options.oldMasterId='']
         *      The id of the master slide that will be deactivated.
         *  @param {Boolean} [options.isMasterChange=false]
         *      Whether only the master slide for a layout slide was changed. In this case the
         *      'options.newLayoutId' is not set, because this is also possible for inactive
         *      slides.
         *  @param {Number} [options.delayedVisibility]
         *      If specified, the new slides will be made visible with a specified delay in ms.
         *      This option is not supported in presentation mode.
         */
        function handleSlideChange(before, options) {

            // the id of the standard slide that will be activated
            const slideId = getStringOption(options, 'newSlideId', '');
            // the id of the layout slide that will be activated
            const layoutId = getStringOption(options, 'newLayoutId', '');
            // the id of the master slide that will be activated
            const masterId = getStringOption(options, 'newMasterId', '');
            // the id of the standard slide that will be deactivated
            const oldSlideId = getStringOption(options, 'oldSlideId', '');
            // the id of the layout slide that will be deactivated
            const oldLayoutId = getStringOption(options, 'oldLayoutId', '');
            // the id of the master slide that will be deactivated
            const oldMasterId = getStringOption(options, 'oldMasterId', '');
            // whether drawings layout slide shall be visible
            const showLayoutSlide = slideId ? docModel.isFollowMasterShapeSlide(slideId) : true;
            // whether drawings of master slide shall be visible (can only be visible, if showLayoutSlide is true)
            const showMasterSlide = showLayoutSlide ? (layoutId ? docModel.isFollowMasterShapeSlide(layoutId) : true) : false;
            // whether a (not active) layout slide got a new master slide assigned (multi selection in master view side pane)
            // -> in this case the layout ID is not defined, but the master must stay not selectable.
            const isMasterChange = getBooleanOption(options, 'isMasterChange', false);
            // whether this is a slide change in forward or backward direction
            let isBackward = false;
            // the info object for the animation
            let animation = null;
            // the animation class for the leaving slide
            let animationOutClass = null;
            // the animation class for the incoming slide
            let animationInClass = null;
            // whether the animation for the slide change shall be shown
            const showSlideAnimation = docModel.getPresentationModeManager().showSlideAnimation();
            // the node listening to event 'animationend' for the slide-out-animation
            let animationOutTarget = null;
            // the node listening to event 'animationend' for the slide-in-animation
            let animationInTarget = null;
            // the duration of an optional animation in ms
            let animationDuration = null;
            // the duration of an optional animation as string in seconds ('1.5s')
            let animationDurationString = null;
            // an emergency cleanup promise, if 'animationend' was not corretly triggered
            let animationPromise = null;
            // whether the new slide shall be shown delayed (to avoid flickering)
            let showNewSlideDelayed = false;
            // whether the blocked active slide was made visible
            let blockedSlideIsVisible = false;

            // helper function to finalize the change of the active slide
            const finalizeSlideChange = () => {

                // making layout and master slide non-selectable, if required
                if (layoutId) { docModel.getSlideById(layoutId).toggleClass('notselectable', slideId !== ''); }
                if (masterId) { docModel.getSlideById(masterId).toggleClass('notselectable', layoutId !== '' || isMasterChange); }

                // Handling the visibility of empty place holder drawings, before a slide is made visible. If
                // it was inserted before, it might be possible that the class 'emptyplaceholdercheck' is still
                // set. This functionality just before showing the slide is especially important for remote clients,
                // where it is otherwise not possible to detect, when this handling of empty place holder needs
                // to be done.
                const id = slideId || layoutId || masterId;
                if (before && id) {
                    const slide = docModel.getSlideById(id);
                    if (slide.hasClass('emptyplaceholdercheck')) {
                        docModel.drawingStyles.handleAllEmptyPlaceHolderDrawingsOnSlide(slide);
                        slide.removeClass('emptyplaceholdercheck');
                    }
                }

                // handling the visibility of the drawings of the layout and the master slide
                // -> taking care, that a layout slide (slideId is empty), does not hide itself.
                handleLayoutMasterVisibility(slideId, layoutId, masterId, showLayoutSlide, showMasterSlide);

                // handling the visibility of the slide backgrounds of slide, layout and master slide
                handleSlideBackgroundVisibility(slideId, layoutId, masterId);
            };

            // helper function to unhide the active slide delayed (DOCS-3084)
            const unhideBlockedVisibleSlide = () => {
                if (!blockedSlideIsVisible) {
                    docModel.getNode().removeClass('hiddenpage'); // also hiding the page to avoid flickering
                    if (slideId) { docModel.getSlideById(slideId).removeClass('invisibleslide'); }
                    if (layoutId) { docModel.getSlideById(layoutId).removeClass('invisibleslide'); }
                    if (masterId) { docModel.getSlideById(masterId).removeClass('invisibleslide'); }
                    finalizeSlideChange();
                    blockedSlideIsVisible = true;
                    self.off('activeslide:unhide', unhideBlockedVisibleSlide);
                    // avoiding that the last slide is positioned at the bottom
                    if (docModel.isLastSlideActive()) { docModel.trigger('update:verticalscrollbar', { pos: docModel.getActiveSlideIndex() }); }
                }
            };

            if (showSlideAnimation && slideId && oldSlideId) {

                isSlideChangeAnimationRunning = true;

                isBackward = slideId && oldSlideId && docModel.getDocumentSlideIndexById(slideId) < docModel.getDocumentSlideIndexById(oldSlideId);

                animation = docModel.getPresentationModeManager().getActiveSlideAnimation();

                animationOutClass = isBackward ? animation.backward.outClass : animation.forward.outClass;
                animationInClass = isBackward ? animation.backward.inClass : animation.forward.inClass;

                animationDuration = animation.duration;
                if (_.isNumber(animationDuration)) {
                    animationDurationString = Math.round(animationDuration * 10 / 2000) / 10 + 's';
                    if (oldSlideId) { docModel.getSlideById(oldSlideId).css('animation-duration', animationDurationString); }
                    if (oldLayoutId) { docModel.getSlideById(oldLayoutId).css('animation-duration', animationDurationString); }
                    if (oldMasterId) { docModel.getSlideById(oldMasterId).css('animation-duration', animationDurationString); }
                    if (slideId) { docModel.getSlideById(slideId).css('animation-duration', animationDurationString); }
                    if (layoutId) { docModel.getSlideById(layoutId).css('animation-duration', animationDurationString); }
                    if (masterId) { docModel.getSlideById(masterId).css('animation-duration', animationDurationString); }
                }

                // setting the animation to remove the old slide
                if (oldSlideId) { docModel.getSlideById(oldSlideId).addClass(animationOutClass); }
                if (oldLayoutId) { docModel.getSlideById(oldLayoutId).addClass(animationOutClass); }
                if (oldMasterId) { docModel.getSlideById(oldMasterId).addClass(animationOutClass); }

                docModel.getNode().addClass('hide-background no-overflow');

                animationOutTarget = docModel.getSlideById(oldSlideId);
                animationInTarget = docModel.getSlideById(slideId);

                // avoiding endless block by 'isSlideChangeAnimationRunning', if 'animationend' failed for some reason
                animationPromise = docModel.executeDelayed(function () { isSlideChangeAnimationRunning = false; }, _.isNumber(animationDuration) ? 2 * animationDuration : 5000);

                // listening to the end of the out-animation, so that the in-animation can be started
                animationOutTarget.on('animationend', () => {

                    // making the old slides invisible, before the animation stops
                    if (oldSlideId) { docModel.getSlideById(oldSlideId).addClass('invisibleslide'); }
                    if (oldLayoutId) { docModel.getSlideById(oldLayoutId).addClass('invisibleslide'); }
                    if (oldMasterId) { docModel.getSlideById(oldMasterId).addClass('invisibleslide'); }

                    // making the new slides visible
                    if (slideId) { docModel.getSlideById(slideId).removeClass('invisibleslide'); }
                    if (layoutId) { docModel.getSlideById(layoutId).removeClass('invisibleslide'); }
                    if (masterId) { docModel.getSlideById(masterId).removeClass('invisibleslide'); }

                    finalizeSlideChange();

                    // setting the animation to make the new slides visible
                    if (slideId) { docModel.getSlideById(slideId).addClass(animationInClass); }
                    if (layoutId) { docModel.getSlideById(layoutId).addClass(animationInClass); }
                    if (masterId) { docModel.getSlideById(masterId).addClass(animationInClass); }

                    animationOutTarget.off('animationend');
                });

                // listening to the end of the in-animation, to remove all animation classes
                animationInTarget.on('animationend', () => {

                    docModel.getNode().removeClass('hide-background no-overflow');

                    // final step: removing the animation classes
                    if (oldSlideId) { docModel.getSlideById(oldSlideId).removeClass(animationOutClass); }
                    if (oldLayoutId) { docModel.getSlideById(oldLayoutId).removeClass(animationOutClass); }
                    if (oldMasterId) { docModel.getSlideById(oldMasterId).removeClass(animationOutClass); }

                    if (slideId) { docModel.getSlideById(slideId).removeClass(animationInClass); }
                    if (layoutId) { docModel.getSlideById(layoutId).removeClass(animationInClass); }
                    if (masterId) { docModel.getSlideById(masterId).removeClass(animationInClass); }

                    if (_.isNumber(animationDuration)) {
                        if (oldSlideId) { docModel.getSlideById(oldSlideId).css('animation-duration', ''); }
                        if (oldLayoutId) { docModel.getSlideById(oldLayoutId).css('animation-duration', ''); }
                        if (oldMasterId) { docModel.getSlideById(oldMasterId).css('animation-duration', ''); }
                        if (slideId) { docModel.getSlideById(slideId).css('animation-duration', ''); }
                        if (layoutId) { docModel.getSlideById(layoutId).css('animation-duration', ''); }
                        if (masterId) { docModel.getSlideById(masterId).css('animation-duration', ''); }
                    }

                    isSlideChangeAnimationRunning = false;
                    animationPromise.abort(); // helper process is no longer required

                    animationInTarget.off('animationend');
                });

            } else {

                showNewSlideDelayed = getNumberOption(options, 'delayedVisibility', 0);

                // deactivating the old slide (together with master and layout slide)
                if (oldSlideId) { docModel.getSlideById(oldSlideId).addClass('invisibleslide'); }
                if (oldLayoutId) { docModel.getSlideById(oldLayoutId).addClass('invisibleslide'); }
                if (oldMasterId) { docModel.getSlideById(oldMasterId).addClass('invisibleslide'); }

                if (showNewSlideDelayed) {
                    // this process is required to avoid flickering in Chrome and Edge browser (DOCS-3084)
                    docModel.getNode().addClass('hiddenpage'); // also hiding the page to avoid flickering
                    // check, if the hidden slide can be made visible (no more scroll events -> avoiding flickering)
                    self.on('activeslide:unhide', unhideBlockedVisibleSlide);
                    // ... but making the slide visible not later than the specified time (no event 'activeslide:unhide')
                    docModel.executeDelayed(unhideBlockedVisibleSlide, showNewSlideDelayed);
                } else {
                    // activating one slide again (together with master and layout slide)
                    if (slideId) { docModel.getSlideById(slideId).removeClass('invisibleslide'); }
                    if (layoutId) { docModel.getSlideById(layoutId).removeClass('invisibleslide'); }
                    if (masterId) { docModel.getSlideById(masterId).removeClass('invisibleslide'); }
                    finalizeSlideChange();
                }
            }
        }

        /**
         * Handler for the slide background visibility - background union from master, layout and standard slides
         *
         * @param  {String} slideId
         * @param  {String} layoutId
         * @param  {String} masterId
         * @param  {String} [previewFillAttrs] optional
         */
        function handleSlideBackgroundVisibility(slideId, layoutId, masterId, previewFillAttrs) {

            let hideBkg;

            if (docModel.isActiveSlideId(slideId)) {
                hideBkg = previewFillAttrs ? ((previewFillAttrs.type && (previewFillAttrs.type !== 'none')) || false) : docModel.isSlideWithExplicitBackground(slideId);

                if (layoutId) {
                    docModel.getSlideById(layoutId).toggleClass('invisiblebackground', hideBkg);

                    if (!hideBkg) { // if standard slide has no bg set, check if layout has bg, to hide master
                        hideBkg = docModel.isSlideWithExplicitBackground(layoutId);
                    }
                }
                if (masterId) {
                    docModel.getSlideById(masterId).toggleClass('invisiblebackground', hideBkg);
                }
            } else if (docModel.isActiveSlideId(layoutId)) {
                hideBkg = previewFillAttrs ? ((previewFillAttrs.type && (previewFillAttrs.type !== 'none')) || false) : docModel.isSlideWithExplicitBackground(layoutId);

                docModel.getSlideById(layoutId).removeClass('invisiblebackground');
                if (masterId) { docModel.getSlideById(masterId).toggleClass('invisiblebackground', hideBkg); }
            } else if (docModel.isActiveSlideId(masterId)) {
                docModel.getSlideById(masterId).removeClass('invisiblebackground');
            }
        }

        /**
         * Handler for all slide attribute changes.
         */
        function handleSlideAttributeChange(dataObj) {

            // build an inheritance chain of ids, to check if changed slide is related to any of them
            const checkSlideBackgoundVisibility = () => {

                const activeSlideId = docModel.getActiveSlideId();
                let slideId;
                let layoutId;
                let masterId;

                if (docModel.isMasterSlideId(activeSlideId)) {
                    slideId = '';
                    layoutId = '';
                    masterId = activeSlideId;
                } else if (docModel.isLayoutSlideId(activeSlideId)) {
                    slideId = '';
                    layoutId = activeSlideId;
                    masterId = docModel.getMasterSlideId(layoutId);
                } else {
                    slideId = activeSlideId;
                    layoutId = docModel.getLayoutSlideId(slideId);
                    masterId = docModel.getMasterSlideId(layoutId);
                }

                const idChain = [slideId, layoutId, masterId];

                if (_.isString(dataObj.slideId) && idChain.indexOf(dataObj.slideId) > -1) {
                    self.handleSlideBackgroundVisibility(activeSlideId);
                }
            };

            // updating a possible change of attribute 'followMasterPage', and possible slide background attribute
            if (docModel.isImportFinished()) {
                updateFollowMasterPageChange();
                checkSlideBackgoundVisibility();
            }
        }

        /**
         * After changes of slide attributes, it is required to react to changes
         * of 'followMasterShapes' attributes of slides.
         */
        function updateFollowMasterPageChange() {

            // the id of the active slide
            const slideId = docModel.getActiveSlideId();
            // the id of the layout slide
            let layoutId = null;
            // the id of the master slide
            let masterId = null;
            // whether drawings of layout slide shall be visible
            let showLayoutSlide = true;
            // whether drawings of master slide shall be visible
            let showMasterSlide = true;

            if (!slideId) { return; } // this can happen during loading

            if (docModel.isStandardSlideId(slideId)) {
                layoutId = docModel.getLayoutSlideId(slideId);
                showLayoutSlide = docModel.isFollowMasterShapeSlide(slideId);
                if (layoutId) {
                    masterId = docModel.getMasterSlideId(layoutId);
                    // master slide can only be visible, if layout slide is visible, too
                    showMasterSlide = showLayoutSlide ? docModel.isFollowMasterShapeSlide(layoutId) : false;
                }
            } else if (docModel.isLayoutSlideId(slideId)) {
                masterId = docModel.getMasterSlideId(slideId);
                showMasterSlide = docModel.isFollowMasterShapeSlide(slideId);
            }

            // handling the visibility of the drawings of the layout and the master slide
            handleLayoutMasterVisibility(slideId, layoutId, masterId, showLayoutSlide, showMasterSlide);
        }

        /**
         * Handler for a removal of the active slide. In this it might be necessary, to hide also
         * the underlying master and layout slides.
         *
         * Info: The options object supports the properties 'id', 'layoutId' and 'masterId'. This
         *       is generated by the public model class function 'getSlideIdSet'.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.id='']
         *      The id of the standard slide that is removed.
         *  @param {String} [options.layoutId='']
         *      The id of the layout slide that will be removed or needs to be made invisible.
         *  @param {String} [options.masterId='']
         *      The id of the layout slide that will be removed or needs to be made invisible.
         */
        function hideActiveSlideCompletely(options) {

            // the id of the standard slide that will be activated
            const slideId = getStringOption(options, 'id', '');
            // the id of the layout slide that will be activated
            const layoutId = getStringOption(options, 'layoutId', '');
            // the id of the master slide that will be activated
            const masterId = getStringOption(options, 'masterId', '');

            // deactivating the old slide (together with master and layout slide)
            if (slideId && docModel.getSlideById(slideId)) { docModel.getSlideById(slideId).addClass('invisibleslide'); }
            if (layoutId && docModel.getSlideById(layoutId)) { docModel.getSlideById(layoutId).addClass('invisibleslide'); }
            if (masterId && docModel.getSlideById(masterId)) { docModel.getSlideById(masterId).addClass('invisibleslide'); }

            handleLayoutMasterVisibility(slideId, layoutId, masterId, false, false);
        }

        /**
         * Helper function to handle the visibility of the layout and the master slide. This is
         * required, if the slide attribute 'followMasterShapes' is modified. It is important, that
         * not the complete slide gets 'display: none' but only the drawing children. Otherwise
         * the background would also vanish. But that would not be the correct behavior.
         *
         * @param {String} slideId
         *
         * @param {String} [layoutId]
         *  The id of the layout slide.
         *
         * @param {String} [masterId]
         *  The id of the master slide.
         *
         * @param {Boolean} showLayoutSlide
         *  Whether the specified layout slide shall be made visible or not.
         *
         * @param {Boolean} showMasterSlide
         *  Whether the specified master slide shall be made visible or not.
         */
        function handleLayoutMasterVisibility(slideId, layoutId, masterId, showLayoutSlide, showMasterSlide) {

            // the layout slide
            let layoutSlide = null;

            if (layoutId) { docModel.getSlideById(layoutId).toggleClass('invisibledrawings', !showLayoutSlide); }
            if (masterId) { docModel.getSlideById(masterId).toggleClass('invisibledrawings', !showMasterSlide); }

            // in ODP format the visibility of the footer slides can be set
            // -> the footer place holder drawings of the layout slide must become visible on document slides,
            //    although they are place holder drawings. Therefore the class 'forceplaceholdervisibility' to
            //    overwrite all existing rules for hiding place holder drawings.
            if (app.isODF()) {
                if (docModel.isMasterView() && layoutId) {
                    layoutSlide = docModel.getSlideById(layoutId);
                    layoutSlide.children('[data-placeholdertype=sldNum]').toggleClass('forceplaceholdervisibility', false);
                    layoutSlide.children('[data-placeholdertype=ftr]').toggleClass('forceplaceholdervisibility', false);
                    layoutSlide.children('[data-placeholdertype=dt]').toggleClass('forceplaceholdervisibility', false);
                } else if (!docModel.isMasterView() && layoutId && slideId) {
                    layoutSlide = docModel.getSlideById(layoutId);
                    layoutSlide.children('[data-placeholdertype=sldNum]').toggleClass('forceplaceholdervisibility', docModel.isSlideNumberDrawingSlide(slideId));
                    layoutSlide.children('[data-placeholdertype=ftr]').toggleClass('forceplaceholdervisibility', docModel.isFooterDrawingSlide(slideId));
                    layoutSlide.children('[data-placeholdertype=dt]').toggleClass('forceplaceholdervisibility', docModel.isDateDrawingSlide(slideId));
                }
            }
        }

        /**
         * Getting the larger of the two values 'content root node height' and the 'height of a slide' (in pixel).
         *
         * @returns {Number}
         *  The larger of the two values 'content root node height' and the 'height of a slide' (in pixel).
         */
        function getScrollStepHeight() {
            return Math.max(Math.round(pageNode.outerHeight() * self.getZoomFactor()), self.$contentRootNode.height());
        }

        /**
         * Handling the change of the active view. It is necessary to set the height
         * of the app content node, so that the vertical scrollbar can be adapted.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Number} [options.sliceCount=1]
         *      The number of slides in the activated view.
         */
        function handleActiveViewChange(options) {

            // the number of slides in the activated view
            const slideCount = getNumberOption(options, 'slideCount', 1);

            // setting the height of the app content node, so that the vertical scroll bar is adapted.
            self.$appContentNode.height(Math.round(getScrollStepHeight() * slideCount));
        }

        /**
         * Hander for the event 'update:verticalscrollbar'. This is triggered
         * after a change of the active slide.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Number} [options.pos=0]
         *      The index of the slide inside its container.
         *  @param {Boolean} [options.keepScrollPosition=false]
         *      Whether the current scroll position shall not be modified, although the vertical scroll bar
         *      was modified. This is the case if a drawing is moved above a slide.
         */
        function handleVerticalScrollbarUpdate(options) {

            if (docModel.isPresentationMode()) { return; } // no scrolling in presentation mode

            // the index of the slide in its container
            const index = getNumberOption(options, 'pos', 0);
            // the old margin-top caused by the slide position
            const oldMarginTop = pageNode.data('scrollTopMargin') || 0;
            // the new margin-top caused by the slide position
            let newMarginTop = parseCssLength(pageNode, 'marginTop') - oldMarginTop + (getScrollStepHeight() * index);
            // whether the scroll position shall not be modified (this is the case if a drawing is moved above a slide)
            const keepScrollPosition = getBooleanOption(options, 'keepScrollPosition', false);
            // a collector for all visible drawings on the slide.
            // The top margin must be as large as the distance to the upmost drawing on the slide (47692)
            // -> this can also be a drawing on a layout or master slide!
            const allDrawings = docModel.getAllVisibleDrawingsOnSlide();
            // the maximum top distance of a drawing above the slide to the slide
            let maxTopDrawingDistance = 0;

            if (isDrawingAboveSlide(allDrawings)) { // check, if at least one drawing is above the slide
                maxTopDrawingDistance = getDrawingDistanceAboveSlide(allDrawings);
                // increasing the top margin because of the drawing above the slide
                if (maxTopDrawingDistance > 0) { newMarginTop += maxTopDrawingDistance; }
            }

            pageNode.css('margin-top', newMarginTop + 'px');
            pageNode.data('scrollTopMargin', newMarginTop);

            if (!keepScrollPosition && !isMouseButtonPressedScrolling) {
                self.$contentRootNode.scrollTop(newMarginTop); // setting the vertical scroll bar position
                verticalScrollPosition = Math.round(newMarginTop);
            }

            // saving that a slide change happened when scrolling with pressed mouse button
            if (isMouseButtonPressedScrolling) { slideChanged = true; }
        }

        /**
         * Moves the browser focus focus to the editor root node.
         */
        function grabFocusHandler(options) {
            const rootNode = docModel.getNode();
            if (TOUCH_DEVICE) {
                const sourceType = options?.sourceEvent?.type ?? 'click';
                if ((sourceType === 'tap') && !isSoftKeyboardOpen()) {
                    hideFocus();
                } else {
                    setFocus(rootNode, { forceEvent: true });
                }
            } else {
                if (options?.restoreBrowserSelection) { docModel.getSelection().restoreBrowserSelection(); } // (DOCS-1541)
                setFocus(rootNode);
            }
        }

        /**
         * Caches the current scroll position of the application pane.
         *
         * @returns {Object}
         *  An object with the properties 'top' and 'left' of the current scroll position.
         */
        function saveScrollPosition() {
            scrollPosition = { top: self.$contentRootNode.scrollTop(), left: self.$contentRootNode.scrollLeft() };
            return scrollPosition;
        }

        /**
         * Restores the scroll position of the application pane according to
         * the cached scroll position.
         *
         * The optional parameter can be used to specify a scrollPosition. If it
         * is not specified, the local available scroll position is used.
         *
         * @param {Object} [scrollPos]
         *  An optional object with the properties 'top' and 'left' specifying
         *  the vertical and the horizontal scroll position.
         */
        function restoreScrollPosition(scrollPos) {
            const localScrollPos = scrollPos ? scrollPos : scrollPosition;
            self.$contentRootNode.scrollTop(localScrollPos.top).scrollLeft(localScrollPos.left);
        }

        /**
         * Scrolls the application pane to the focus position of the selection.
         * If this is a slide selection, there is no need to scroll.
         */
        function scrollToSelection() {

            // the selection object
            const selection = docModel.getSelection();
            // the boundaries of the text cursor
            const boundRect = selection.isSlideSelection() ? null : selection.getFocusPositionBoundRect();

            // make cursor position visible in application pane
            if (boundRect) { self.scrollToPageRectangle(boundRect); }
        }

        /**
         * Helper function that checks, if there is at least one drawing, that is above the active slide. This
         * function is used for the vertical scroll bar calculations.
         *
         * @param {jQuery} [allVisibleDrawings]
         *  A container with all visible drawings on the slide. If it is not specified, it is calculated within
         *  this function.
         *
         * @returns {Boolean}
         *  Whether there is at least one visible drawing, whose upper border is above the active slide.
         */
        function isDrawingAboveSlide(allVisibleDrawings) {

            // the drawings on the slide
            const allDrawings = allVisibleDrawings || docModel.getAllVisibleDrawingsOnSlide();
            // the top offset of the page node
            const pageNodeTopOffset = pageNode.offset().top;
            // a drawing that is positioned above the slide
            const drawingAboveSlide = _.find(allDrawings, function (drawing) { return (drawing.getBoundingClientRect().top < pageNodeTopOffset); });

            return !_.isUndefined(drawingAboveSlide);
        }

        /**
         * Helper function that checks, if there is at least one drawing, that is below the active slide. This
         * function is used for scrolling of the last slide. If there is one drawing at least 1mm below the last
         * slide, it must not be scrolled upwards to the upper border of the slide (47514).
         *
         * @param {Number} pageHeight
         *  The effective height of the visible page in px. This includes already the zoom level.
         *
         * @param {jQuery} [allVisibleDrawings]
         *  A container with all visible drawings on the slide. If it is not specified, it is calculated within
         *  this function.
         *
         * @returns {Boolean}
         *  Whether there is at least one visible drawing, whose upper border is above the active slide.
         */
        function isDrawingBelowSlide(pageHeight, allVisibleDrawings) {

            // the visible drawings on the slide
            const allDrawings = allVisibleDrawings || docModel.getAllVisibleDrawingsOnSlide();
            // the top offset of the page node
            const pageNodeTopOffset = pageNode.offset().top;
            // a drawing that is not yet visible in scroll direction,
            // checking the bottom positions of drawings on the slide
            const drawingBelowSlide = _.find(allDrawings, drawing => {
                return (drawing.getBoundingClientRect().bottom > (pageNodeTopOffset + pageHeight));
            });

            return !_.isUndefined(drawingBelowSlide);
        }

        /**
         * Calculating the distance of the most top drawing on the active slide to the slide itself for all
         * drawings, whose upper border is above the slide. If no drawing is above the slide, 0 is returned.
         *
         * @param {jQuery} [allVisibleDrawings]
         *  A container with all visible drawings on the slide. If it is not specified, it is calculated within
         *  this function.
         *
         * @returns {Number}
         *  The distance in px of the most top drawing above the slide to the slide itself. If no drawing is
         *  located above the slide, 0 is returned.
         */
        function getDrawingDistanceAboveSlide(allVisibleDrawings) {

            // the drawings on the slide
            const allDrawings = allVisibleDrawings || docModel.getAllVisibleDrawingsOnSlide();
            // the top offset of the page node
            const pageNodeTopOffset = pageNode.offset().top;
            // the maximum distance of a visible drawing above the slide
            let maxTopDistance = 0;

            // iterating over all visible drawings on the slide
            _.each(allDrawings, drawing => {
                const distance = pageNodeTopOffset - drawing.getBoundingClientRect().top;
                if (distance > maxTopDistance) { maxTopDistance = distance; }
            });

            return maxTopDistance;
        }

        /**
         * Handler for scroll events at the content root node.
         */
        function scrollInContentRootNode() {

            // the current scrollPosition
            const scrollY = self.$contentRootNode.scrollTop();
            // the scroll direction
            const downwards = scrollY > verticalScrollPosition;
            // the additional top margin that is required because of the scroll bar
            let marginTop = 0;
            // the height of the visible page
            let realPageHeight = 0;
            // whether the top border of the page is visible
            let topBorderVisible = false;
            // whether the top border of the page is visible
            let bottomBorderVisible = false;
            // the height of the content root node
            let contentRootNodeHeight = 0;
            // the top offset of the content root node
            let contentRootNodeTopOffset = 0;
            // the top offset of the page node
            let pageNodeTopOffset = 0;
            // if drawing is currently selected
            let selectedDrawing = null;
            // whether the slide is completely visible (top and bottom border)
            let fullSlideVisible = false;
            // the selection object
            const selection = docModel.getSelection();
            // a collector for all visible drawings on the slide (should be filled as late as possible (performance))
            let allVisibleDrawings = null;
            // allow visibility of the border before switchting to other slide (only if slide is not completely visible)
            const offset = 50;
            // in Chrome and Edge browser the vertical scroll position needs to be repaired after a slide change to avoid flickering
            // in Firefox and Safari browser there is also a timeout between slide changes required (DOCS-4898)
            const useScrollBlock = true; // -> this is required for all browsers after DOCS-4898
            // a visibility delay time for the new slide to avoid flickering on Chrome and Edge (DOCS-3084)
            const flickerDelayTime = (useScrollBlock && !isMouseButtonPressedScrolling) ? 400 : 0;
            // a 'backjump' delay time that is only used, if useScrollBlock is set (DOCS-3084)
            const blockOppositeDirectionTime = 1000;

            /**
             * helper function that checks, if there is at least one drawing, that is only partly visible
             * in the scroll direction. In this case, it must not be switched to the neighboring slide.
             */
            const allDrawingsVisible = allDrawings => {

                // a drawing that is not yet visible in scroll direction
                let partlyVisibleDrawing;

                // Info: Using an optional offset, that is necessary to access for example rotation selector, if drawing is outside the slide.
                // -> but trying to avoid usage of offset, because slides must not flicker, if they are completely visible.

                if (downwards) {
                    // checking the bottom positions of drawings on the slide
                    partlyVisibleDrawing = _.find(allDrawings, drawing => {
                        const drawingBottomPos = drawing.getBoundingClientRect().bottom;
                        const useOffset = (drawingBottomPos > (pageNodeTopOffset + realPageHeight)); // drawing is outside the slide
                        return ((drawingBottomPos + (useOffset ? offset : 0)) > (contentRootNodeTopOffset + contentRootNodeHeight));
                    });
                } else {
                    // checking the top positions of drawings on the slide
                    partlyVisibleDrawing = _.find(allDrawings, drawing => {
                        const drawingTopPos = drawing.getBoundingClientRect().top;
                        const useOffset = (drawingTopPos < pageNodeTopOffset); // drawing is outside the slide
                        return ((drawingTopPos - (useOffset ? (2 * offset) : 0)) < contentRootNodeTopOffset);  // twice the offset to enable rotation
                    });
                }

                return _.isUndefined(partlyVisibleDrawing);
            };

            // no slide change handled by scrolling in presentation mode
            if (docModel.isPresentationMode()) { return; }

            // do nothing in Safari, if a popup-container is open (61423)
            if (_.browser.Safari && self.$appContentNode.children('.popup-container').length > 0) { return; }

            // in Chrome and Edge browser (and Firefox and Safari since DOCS-4898) the vertical scroll position needs to be repaired after a slide change
            // -> otherwise the slide will be changed very often and flickers therefore.
            // -> this seems to be a problem in Chrome browser and will be repaired maybe in the future
            // After switching for example to a follwing slide and setting scrollTop to 400, the browser sends
            // further scroll events with lower values of scrollTop (and displays those values before this function
            // is called).
            if (useScrollBlock && !isMouseButtonPressedScrolling) {
                // only blocking the event, if it would lead to the previously selected slide (especially important on Edge browser)
                if (scrollBlockIsActive || (isChangeToFollowingSlide && scrollY < verticalScrollPosition) || (isChangeToPreviousSlide && scrollY > verticalScrollPosition)) {
                    self.$contentRootNode.scrollTop(verticalScrollPosition); // repairing the vertical slide position and leaving this handler
                    // making slide visible, if there is no new scroll event for 100 ms
                    if (scrollBlockIsActive) { showActiveSlideDebounced(); }
                    return;
                }
            }

            // no change of slide, if vertical scroll position did not change or did change only one pixel or less.
            // -> allowing 1px difference increases the resilience of the vertical scrolling, because this difference
            //    might be caused by rounding errors. This can lead for example to an upward slide change directly
            //    following a downward slide change making a slide change via scroll wheel impossible (doc 44961).
            if (Math.abs(scrollY - verticalScrollPosition) <= 1) { return; }

            verticalScrollPosition = scrollY; // saving the value for the next scroll event. This must be done before any of the following 'return' statements!

            // scroll event triggered by selection of a drawing -> never change slide
            if (scrollingToPageRectangle) {
                scrollingToPageRectangle = false;
                return;
            }

            // setting a marker, if the vertical scrollbar was really modified with pressed mouse button
            if (isMouseButtonPressedScrolling) {
                // never handle selection box or drawing moves within this function
                if (docModel.getSelectionBox() && docModel.getSelectionBox().isSelectionBoxActive()) { return; }
                verticalScrollChanged = true;
            }

            // don't scroll if modifying of drawing or group of drawings is currently active - early exit on move, resize, rotate
            if (selection.isMultiSelection()) {
                selectedDrawing = selection.getFirstSelectedDrawingNode();
            } else if (selection.isAnyDrawingSelection()) {
                selectedDrawing = selection.getAnyDrawingSelection();
            }

            if (selectedDrawing && isModifyingDrawingActive(selectedDrawing)) { return; }

            // calculating change of vertical scroll bar
            realPageHeight = Math.round(pageNode.outerHeight() * self.getZoomFactor());
            pageNodeTopOffset = pageNode.offset().top;
            contentRootNodeHeight = self.$contentRootNode.height();
            contentRootNodeTopOffset = self.$contentRootNode.offset().top;

            topBorderVisible = pageNodeTopOffset > contentRootNodeTopOffset;
            bottomBorderVisible = (pageNodeTopOffset + realPageHeight) < (contentRootNodeTopOffset + contentRootNodeHeight);

            fullSlideVisible = topBorderVisible && bottomBorderVisible; // not using offset, slides must not move up or downwards

            // switching to the next or previous slide
            if (downwards) {
                if (bottomBorderVisible) {

                    // avoid using the offset to make the bottom border visible, if the slide is completely visible
                    if (!fullSlideVisible && ((contentRootNodeTopOffset + contentRootNodeHeight - (pageNodeTopOffset + realPageHeight)) < offset)) { return; }

                    allVisibleDrawings = docModel.getAllVisibleDrawingsOnSlide(); // filling the container with visible drawings
                    if (!allDrawingsVisible(allVisibleDrawings)) { return; }

                    if (!docModel.changeToNeighbourSlide({ delayedVisibility: flickerDelayTime })) {
                        // restoring the previous position (scroll down on last slide)
                        if ((contentRootNodeHeight > realPageHeight) && !isDrawingBelowSlide(realPageHeight, allVisibleDrawings)) {
                            // only jump upwards, if top and bottom border is visible and no drawing is below the last slide (47514)
                            marginTop = pageNode.data('scrollTopMargin') || 0;
                            self.$contentRootNode.scrollTop(marginTop); // setting the vertical scroll bar position
                            verticalScrollPosition = Math.round(marginTop);
                        } else {
                            self.$contentRootNode.scrollTop(verticalScrollPosition);
                        }
                    } else {
                        if (useScrollBlock) {
                            scrollBlockIsActive = true;
                            isChangeToFollowingSlide = true;
                            docModel.executeDelayed(() => { scrollBlockIsActive = false; }, flickerDelayTime);
                            docModel.executeDelayed(() => { isChangeToFollowingSlide = false; }, blockOppositeDirectionTime);
                        }
                    }
                } else {
                    // saving vertical scroll position (required for the last slide)
                    verticalScrollPosition = self.$contentRootNode.scrollTop();
                }
            } else if (!downwards && topBorderVisible) {
                allVisibleDrawings = docModel.getAllVisibleDrawingsOnSlide();
                if (!allDrawingsVisible(allVisibleDrawings)) { return; }
                docModel.changeToNeighbourSlide({ next: false, delayedVisibility: flickerDelayTime });
                if (useScrollBlock) {
                    scrollBlockIsActive = true;
                    isChangeToPreviousSlide = true;
                    docModel.executeDelayed(() => { scrollBlockIsActive = false; }, flickerDelayTime);
                    docModel.executeDelayed(() => { isChangeToPreviousSlide = false; }, blockOppositeDirectionTime);
                }
            }

        }

        /**
         * Performance: Register in the selection, whether the current operation is an insertText
         * operation. In this case the debounced 'scrollToSelection' function can use the saved
         * DOM node instead of using Position.getDOMPosition in 'getPointForTextPosition'. This
         * makes the calculation of scrolling requirements faster.
         */
        function registerInsertText(options) {
            docModel.getSelection().registerInsertText(options);

            //fixing the problem with an upcoming soft-keyboard
            if (TOUCH_DEVICE) { scrollToSelection(); }
        }

        /**
         * Handles the 'contextmenu' event for the complete browser.
         */
        function globalContextMenuHandler(event) {

            // was the event triggered by keyboard?
            const isGlobalTrigger = isContextEventTriggeredByKeyboard(event);
            // the slide pane container as a jQuery object
            const slidePaneContainer = self.getSlidePane().getSlidePaneContainer();
            // the selection object
            const selection = docModel.getSelection();

            // no context menu in the presentation mode
            if (docModel.isPresentationMode()) { return false; }

            if (contextMenu && (event.type === 'contextmenu') && isPresentationCommentBubbleTarget(event.target)) {

                contextMenu.hide();

                // remove all existing controls from context menu
                contextMenu.destroyAllControls();
                contextMenu.setToolTip('');
            }

            // set virtual focus to the document when the context menu was not invoked in the slide pane and not by keyboard (e.g. right mouse click in the slide)
            if (!isGlobalTrigger && !containsNode(slidePaneContainer, event.target, { allowSelf: true })) { self.setVirtualFocusSlidepane(false); }

            /* context menu detection for the slide pane */

            // when the focus is on the slidepane, trigger the contextmenu on the slide pane, when the following cases are NOT fulfilled:
            if (self.hasSlidepaneVirtualFocus()) {

                // On COMPACT_DEVICES a different method in the slidepane.js is used to trigger the context menu.
                // That means taphold is not used on the slide pane to trigger the context menu, because this interaction is used for drag and drop on COMPACT_DEVICES.
                if (COMPACT_DEVICE) {
                    return false;
                }

                // // when event.target is NOT inside the slide pane and when it's also NOT triggered by keyboard, don't open it // TODO check comment and if
                // if (containsNode(slidePaneContainer, event.target, { allowSelf: true }) === false && !isGlobalTrigger) {
                //     return false;
                // }

                // trigger an event on the slide pane. that invokes a context menu on the activeSlide. when it's triggered by keyboard open the menu at the side
                PresentationContextMenu.triggerShowEvent(slidePaneContainer, event, { stickToSlide: isGlobalTrigger });
                // we have a detected a context menu for the slide pane, therefore return false here
                return false;
            }

            /* context menu detection for the windowNode */

            // prevent context menu on key to open again
            if (isElementNode(getFocus(), '.popup-content')) {
                return false;
            }

            // check if context menu shall be opened on a popup menu
            const menuClicked = $(event.target).closest(".io-ox-office-main.popup-container.popup-menu").length > 0;
            // check if the event target is inside of one of the allowed containers in application window, or inside a popup menu
            const windowClicked = containsNode(app.getWindowNode(), event.target) && ($(event.target).closest(".page,.textdrawinglayer").length > 0);

            // get out here, if no allowed target was clicked, or the global trigger was fired (via keyboard)
            if (!menuClicked && !windowClicked && !isGlobalTrigger) { return false; }

            // if a drawing is selected
            let triggerNode;
            if (docModel.isDrawingSelected()) {
                if ($(event.target).is('.tracker')) { return false; }
                // get the (first) selected drawing as trigger node
                triggerNode = (selection.isMultiSelectionSupported() && selection.isMultiSelection()) ? selection.getFirstSelectedDrawingNode() : $(selection.getSelectedDrawing()[0]);
            } else if (selection.isSlideSelection()) {
                triggerNode = docModel.getNode();
            } else {
                // get the current focus node to trigger on it
                if (menuClicked) {
                    triggerNode = $(event.target);
                }
                const windowSelection = window.getSelection();
                if (!triggerNode && windowSelection.type !== 'None') {
                    triggerNode = $(windowSelection.focusNode.parentNode);
                }
            }

            if (!triggerNode) {
                globalLogger.warn("$badge{PresentationView} globalContextMenuHandler: no target node found, context menu will not open");
                return;
            }

            // if we have a last known touch position, use it
            if (touchX && touchY) {
                event.pageX = touchX;
                event.pageY = touchY;

            // in case of global trigger (via keyboard), or x/y are both "0"
            // locate a meaningful position
            } else if (isGlobalTrigger || (!event.pageX && !event.pageY)) {

                // find the position of the trigger node
                const anchorPoint = getPixelPositionToRootNodeOffset($('html'), triggerNode);
                event.pageX = anchorPoint.x;
                event.pageY = anchorPoint.y;

                if (isGlobalTrigger && selection.isSlideSelection()) {
                    event.target = triggerNode.parent();
                } else {
                    // move anchor point to the middle of the trigger node
                    event.pageX += Math.round(triggerNode.width() / 2);
                    event.pageY += Math.round(triggerNode.height() / 2);
                }
            }

            // trigger our own contextmennu event on the found trigger node
            PresentationContextMenu.triggerShowEvent(triggerNode, event);

            return false;
        }

        /**
         * Helper function to set the correct cursor in overlapping drawings. If the top level
         * drawing is not responsible for the mouse move event at the event position (for example
         * a line or a circle without filling), it might be possible, that an underlying drawing
         * has text input at the event position, so that the cursor must be the text cursor. If
         * no underlying drawing is responsible, the pointer for the slide selection must be
         * activated (docs-977).
         */
        function getDebouncedMouseMoveHandler() {

            // the last event node
            let lastEvent = null;
            // the last modified drawing node
            let lastDrawingNode = null;

            // saving the last mouse move event in direct callback
            function saveMouseMoveEvent(event) {
                lastEvent = event;
            }

            // deferred callback to determine the cursor type
            function checkCursorAtPosition() {

                // clean up old forced cursor types
                if (lastDrawingNode) {
                    lastDrawingNode.removeClass('forcepointercursor forcetextcursor forcecopycursor');
                    lastDrawingNode = null;
                }

                if (lastEvent && app.isEditable()) {

                    const targetNode = $(lastEvent.target);

                    if (targetNode.is('.content') || targetNode.is('.canvasexpansion')) {

                        const drawingNode = targetNode.parent();

                        if (drawingNode.length > 0) {
                            if (!checkEventPosition(app, lastEvent, drawingNode)) {
                                // trying to find a valid drawing node behind the drawing that is target of the mouse move event
                                const drawingNodeObj = findValidDrawingNodeBehindDrawing(app, lastEvent, drawingNode);

                                if (drawingNodeObj && drawingNodeObj.textFrameNode) {
                                    // drawing with text frame found to handle event
                                    lastDrawingNode = drawingNode;
                                    drawingNode.addClass('forcetextcursor');
                                } else if (drawingNodeObj && drawingNodeObj.drawing) {
                                    // drawing found to handle this event
                                    // -> typically do nothing, because move cursor is already active,
                                    //    but if Ctrl-Key is pressed, the copy cursor needs to be activated (for an underlying drawing)
                                    if (docModel.getNode().hasClass('drag-to-copy')) { // ctrl-key is pressed
                                        lastDrawingNode = drawingNode;
                                        drawingNode.addClass('forcecopycursor');
                                    }
                                } else {
                                    // no drawing found to handle event
                                    lastDrawingNode = drawingNode;
                                    drawingNode.addClass('forcepointercursor');
                                }
                            } else if (docModel.getNode().hasClass('drag-to-copy')) { // ctrl-key is pressed
                                lastDrawingNode = drawingNode;
                                drawingNode.addClass('forcecopycursor');
                            }
                        }
                    }
                }

                lastEvent = null;
            }

            // create and return the deferred method
            return self.debounce(saveMouseMoveEvent, checkCursorAtPosition, { delay: 50 });
        }

        /**
         * Initialization after construction.
         */
        function initHandler() {

            // deferred and debounced scroll handler, registering insertText operations immediately
            const scrollToSelectionDebounced = self.debounce(registerInsertText, scrollToSelection, { delay: 50, maxDelay: 500 });

            // register the global event handler for 'contextmenu' event
            if (COMPACT_DEVICE) {
                //this.listenToDOMWhenVisible($(document), 'taphold', globalContextMenuHandler);
                this.listenToDOMWhenVisible($(document.body), 'contextmenu', evt => {
                    if (/^(INPUT|TEXTAREA)/.test(evt.target.tagName)) { return true; }
                    evt.preventDefault();
                    return false;
                });
            } else {
                this.listenToDOMWhenVisible($(document.body), 'contextmenu', globalContextMenuHandler);
            }

            if (IOS_SAFARI_DEVICE) {
                let touchStartTimer;
                this.listenToDOMWhenVisible($(document.body), 'touchstart', evt => {
                    touchStartTimer = setTimeout(() => {
                        if ((docModel.getSelection().isSlideSelection() || docModel.getSelection().isTextSelection()) && this.docApp.isEditable()) {
                            globalContextMenuHandler(evt);
                        }
                    }, 400);
                });
                this.listenToDOMWhenVisible($(document.body), 'touchend', () => {
                    clearTimeout(touchStartTimer);
                });
            }

            // to save the position from 'touchstart', we have to register some global eventhandler
            // we need this position to open the contextmenu on the correct location
            if (IOS_SAFARI_DEVICE || _.browser.Android) {
                this.listenToDOMWhenVisible($(document.body), 'touchstart', function (event) {
                    if (event.originalEvent.changedTouches) {
                        touchX = event.originalEvent.changedTouches[0].pageX;
                        touchY = event.originalEvent.changedTouches[0].pageY;
                    }
                });
                this.listenToDOMWhenVisible($(document.body), 'touchend', function () {
                    touchX = touchY = null;
                });
            }

            // add the SlidePane
            slidepane = self.addPane(new SlidePane(self));

            // Add slidepane keyhandler to the clipboard too. This is needed,
            // because the browser focus must be partly in the clipboard for copy&paste.
            // Without this handler shortcuts for the slidepane would not work.
            docModel.getSelection().getClipboardNode().on({
                keydown: slidepane.getSlidepaneKeyDownHandler()
            });

            // insert the editor root node into the application pane
            self.insertContents(docModel.getNode());

            // Page node, used for zooming
            pageNode = docModel.getNode();

            // handle scroll position when hiding/showing the entire application
            self.listenTo(app.getWindow(), "beforehide", function () {
                saveScrollPosition();
                slidepane.saveSlidePaneScrollPosition();
                self.$appPaneNode.css('opacity', 0);
            });
            self.listenTo(app.getWindow(), "show", function () {
                restoreScrollPosition();
                slidepane.restoreSlidePaneScrollPosition();
                // restore visibility deferred, needed to restore IE scroll position first
                self.executeDelayed(function () { self.$appPaneNode.css('opacity', 1); });
            });

            // handle selection change events
            self.listenTo(docModel, 'selection', function (selection, options) {
                // scroll to text cursor (but not, if only a remote selection was updated (39904))
                if (!getBooleanOption(options, 'updatingRemoteSelection', false)) { scrollToSelectionDebounced(options); }
                const slideID = docModel.getActiveSlideId();
                const layoutOrMaster = docModel.isLayoutOrMasterId(slideID);
                const userData = {
                    selections: selection.getRemoteSelectionsObject(),
                    target: layoutOrMaster ? slideID : undefined
                };
                if (docModel.isHeaderFooterEditState()) {
                    userData.targetIndex = docModel.getPageLayout().getTargetsIndexInDocument(docModel.getCurrentRootNode(), docModel.getActiveTarget());
                }
                // send user data to server
                app.updateUserData(userData);
                // exit from crop image mode, if currently active
                if (selection.isCropMode()) { docModel.exitCropMode(); }
            });

            // slide size has changed - e.g. orientation change.
            self.listenTo(docModel, 'slidesize:change', () => self.changeZoom('slide'));

            // disable drag&drop for the content root node to prevent dropping images which are loaded by the browser
            self.$contentRootNode.on('drop dragstart dragenter dragexit dragover dragleave', false);

            // Handle switch of slide inside standard view or master/layout view
            self.listenTo(docModel, 'change:slide:before', options => handleSlideChange(true, options));
            // Handle the removal of the active slide
            self.listenTo(docModel, 'removed:activeslide', hideActiveSlideCompletely);
            // Handle the change of a layout slide for a document slide
            self.listenTo(docModel, 'change:layoutslide', options => handleSlideChange(false, options));
            // Handle switch of followMasterShapes attribute
            self.listenTo(docModel, 'change:slideAttributes', handleSlideAttributeChange);

            // Registering a debounced hander function for setting correct cursor in underlying drawings
            self.$contentRootNode.on('mousemove', getDebouncedMouseMoveHandler());

            // store the values of page width and height with paddings, after loading is finished
            pageNodeWidth = pageNode.outerWidth();
            pageNodeHeight = pageNode.outerHeight();
        }

        /**
         * Debounced handler function for the 'refresh:layout' event.
         */
        this._refreshLayoutHandler = this.debounce(function () {

            // the remote selection object
            const remoteSelection = docModel.getRemoteSelection();
            // temporary page node width value
            const tempPageNodeWidth = pageNode.outerWidth();

            // fix for #33829 - if padding is reduced, dependent variables have to be updated
            pageNodeWidth = tempPageNodeWidth;
            pageNodeHeight = pageNode.outerHeight();

            // update dynamic zoom factor
            this.setTimeout(() => this.zoomState.refresh(), 0);

            // it might be necessary to update the slide position at the vertical scroll bar
            refreshVerticalScrollBar({ keepScrollPosition: true });

            // clean and redraw remote selections
            remoteSelection.renderCollaborativeSelections();

            // taking care of comment selection on small devices (DOCS-3963)
            this.trigger("refresh:layout:after");

        }, { delay: 100 }); // Note: The delay should be higher than resizeWindowHandlerDebounced() in slidepane.js to prevent flickering while resizing with 'fit to width'.

        /**
         * calculate the optimal document scale for the slide.
         * It considers width and hight so that the whole slide fits to the visible screen.
         */
        function calculateOptimalZoomFactor() {

            // the slide in presentation
            const documentWidth = pageNode.outerWidth();
            const documentHeight = pageNode.outerHeight();

            const parentWidth = docModel.isFullscreenPresentationMode() ? self.$contentRootNode.width() : self.$appPaneNode.width();
            const parentHeight = docModel.isFullscreenPresentationMode() ? self.$contentRootNode.height() : self.$appPaneNode.height();

            // the visible area, more or less...
            const appContent = self.$appPaneNode.find('.app-content');
            let scaledWidth = null;
            let scaledHeight = null;

            // "3" because of the rounding issues
            const outerDocumentMarginX = docModel.isPresentationMode() ? 0 : (3 + parseCssLength(appContent, 'marginLeft') + parseCssLength(appContent, 'marginRight'));
            const outerDocumentMarginY = docModel.isPresentationMode() ? 0 : (3 + parseCssLength(appContent, 'marginTop') + parseCssLength(appContent, 'marginBottom'));

            const scrollBarWidth = docModel.isPresentationMode() ? 0 : SCROLLBAR_WIDTH;
            const roundingHelper = docModel.isPresentationMode() ? 0 : 4;

            // the scale ratio
            let ratio;

            // during loading phase the height might not be determined correctly
            if (documentHeight === 0) { return 1; }

            // landscape
            if (documentWidth > documentHeight) {

                ratio = ((parentWidth * parentWidth) / (documentWidth * (parentWidth + scrollBarWidth + roundingHelper + outerDocumentMarginX)));
                scaledHeight = (documentHeight * ratio);

                // when image is still not fitting stage - then we need to resize it by smaller dimension
                if (scaledHeight > (parentHeight - outerDocumentMarginY)) {
                    ratio = ((parentHeight * parentHeight) / (documentHeight * (parentHeight + outerDocumentMarginY)));
                }
            // portrait
            } else {

                ratio = ((parentHeight * parentHeight) / (documentHeight * (parentHeight + outerDocumentMarginY)));
                scaledWidth = (documentWidth * ratio);

                // when image is still not fitting stage - then we need to resize it by smaller dimension
                if (scaledWidth > (parentWidth - scrollBarWidth - roundingHelper - outerDocumentMarginX)) {
                    ratio = ((parentWidth * parentWidth) / (documentWidth * (parentWidth + scrollBarWidth + roundingHelper + outerDocumentMarginX)));
                }
            }

            return ratio;
        }

        /**
         * Applies the effective zoom factor, and updates the view.
         *
         * @param {ZoomEvent} event
         *  The event data with new zoom type and factor to be used.
         */
        function zoomChangedHandler(event) {

            pageNode.css({ transform: `scale(${event.factor})`, transformOrigin: 'top' });
            self.recalculateDocumentMargin();

            if (event.factor < 1) {
                // there needs to be a padding, so that comments will still be visible
                if (!docModel.getCommentLayer().isEmpty()) {
                    const commentLayerWidth = docModel.getCommentLayer().getAppContentPaddingWidth();
                    self.$appContentNode.css({ paddingLeft: commentLayerWidth, paddingRight: commentLayerWidth });
                }
            } else {
                self.$appContentNode.css({ paddingLeft: '', paddingRight: '' });
            }

            // Use zoom as soon as all browsers support it.
            // pageNode.css({ zoom: zoomFactor });

            scrollToSelection();
            self.trigger('change:zoom');
        }

        function setAdditiveMargin() {
            if (docModel.getSlideTouchMode()) {
                //extra special code for softkeyboard behavior, we need more space to scroll to the end of the textfile
                const dispHeight = Math.max(window.screen.height, window.screen.width);
                pageNode.css('margin-bottom', (parseCssLength(pageNode, 'marginBottom') + dispHeight / 3) + 'px');
            }
        }

        /**
         * Handler function for the event 'revealApp' that is triggered after an reload,
         * when the 'new' application becomes visible.
         */
        function updateScrollAfterReload() {
            refreshVerticalScrollBar();
            // restoring the precise saved scroll position
            // -> this is only required for large zoom level, so that there is
            //    a scrolling inside the slide. All other scenarios are already
            //    handled by 'refreshVerticalScrollBar'.
            const scrollPosition = app.getLaunchOption('scrollPosition');
            if (scrollPosition) {
                restoreScrollPosition(scrollPosition);
                if (_.isNumber(scrollPosition.slidePaneTop)) { slidepane.restoreSlidePaneScrollPosition(scrollPosition.slidePaneTop); }
            }
        }

        // public methods -----------------------------------------------------

        /**
         * Returns the position of the passed node in the entire scroll area of
         * the application pane.
         *
         * @param {HTMLElement|jQuery} node
         *  The DOM element whose dimensions will be calculated. Must be
         *  contained somewhere in the application pane. If this object is a
         *  jQuery collection, uses the first node it contains.
         *
         * @returns {Object}
         *  An object with numeric attributes representing the position and
         *  size of the passed node relative to the entire scroll area of the
         *  application pane. See method Utils.getChildNodePositionInNode() for
         *  details.
         */
        this.getChildNodePositionInNode = function (node) {
            return getChildNodePositionInNode(this.$contentRootNode, node);
        };

        /**
         * Scrolls the application pane to make the passed node visible.
         *
         * @param {HTMLElement|jQuery} node
         *  The DOM element that will be made visible by scrolling the
         *  application pane. Must be contained somewhere in the application
         *  pane. If this object is a jQuery collection, uses the first node it
         *  contains.
         *
         * @param {Object} options
         *  @param {Boolean} [options.forceToTop=false]
         *   Whether the specified node shall be at the top border of the
         *   visible area of the scrolling node.
         *
         * @returns {TextView}
         *  A reference to this instance.
         */
        this.scrollToChildNode = function (node, options) {
            if (getBooleanOption(options, 'regardSoftkeyboard', false)) {
                //fix for Bug 39979 on Ipad text is sometimes under the keyboard
                if (!IOS_SAFARI_DEVICE || !isSoftKeyboardOpen()) { delete options.regardSoftkeyboard; }
            }
            scrollingToPageRectangle = true;
            scrollToChildNode(this.$contentRootNode, node, _.extend(options || {}, { padding: 15 }));
            return this;
        };

        /**
         * Scrolls the application pane to make the passed document page
         * rectangle visible.
         *
         * @param {Object} pageRect
         *  The page rectangle that will be made visible by scrolling the
         *  application pane. Must provide the properties 'left', 'top',
         *  'width', and 'height' in pixels. The properties 'left' and 'top'
         *  are interpreted relatively to the entire document page.
         *
         * @returns {TextView}
         *  A reference to this instance.
         */
        this.scrollToPageRectangle = function (pageRect) {
            const options = { padding: 15 };

            scrollingToPageRectangle = true;
            scrollToPageRectangle(this.$contentRootNode, pageRect, options);

            return this;
        };

        /**
         * Returns the slide pane.
         *
         * @returns {SlidePane}
         *  The slidepane pane of this application.
         */
        this.getSlidePane = function () {
            return slidepane;
        };

        /**
         * Creates necessary margin values when using transform scale css property
         *
         */
        this.recalculateDocumentMargin = function (options) {

            // must be refreshed before 'recalculatedMargin' is calculated,
            // otherwise the margin is wrong at the document loading
            const zoomFactor = this.getZoomFactor();
            if (zoomFactor !== 1) {
                // #34735 page width and height values have to be updated
                pageNodeWidth = pageNode.outerWidth();
                pageNodeHeight = pageNode.outerHeight();
            }

            const marginAddition = (zoomFactor < 0.99) ? 3 : 0;
            const recalculatedMargin = (pageNodeWidth * (zoomFactor - 1)) / 2 + marginAddition;
            // whether the (vertical) scroll position needs to be restored
            const keepScrollPosition = getBooleanOption(options, 'keepScrollPosition', false);
            // an additional right margin used for comments that might be set at the page node
            const commentMargin = pageNode.hasClass(COMMENTMARGIN_CLASS) ? pageNode.data(COMMENTMARGIN_CLASS) : 0;

            if (_.browser.Chrome && (zoomFactor > 1)) {
                pageNode.css({
                    margin: `${marginAddition}px ${recalculatedMargin + commentMargin}px ${30 / zoomFactor}px ${recalculatedMargin}px`
                });
            } else {
                pageNode.css({
                    margin: `${marginAddition}px ${recalculatedMargin + commentMargin}px ${pageNodeHeight * (zoomFactor - 1) + marginAddition}px ${recalculatedMargin}px`
                });
            }

            setAdditiveMargin();

            // in the end scroll to cursor position/selection (but not after event 'refresh:layout', that uses the option 'keepScrollPosition')
            if (!keepScrollPosition) { scrollToSelection(); }
        };

        /**
         * Rejects an attempt to edit the text document (e.g. due to reaching
         * the table size limits).
         *
         * @param {String} cause
         *  The cause of the rejection.
         *
         * @returns {TextView}
         *  A reference to this instance.
         */
        this.rejectEditTextAttempt = function (cause) {

            switch (cause) {
                case 'tablesizerow':
                    this.yell({ type: 'info', message: gt('The table reached its maximum size. You cannot insert further rows.') });
                    break;

                case 'tablesizecolumn':
                    this.yell({ type: 'info', message: gt('The table reached its maximum size. You cannot insert further columns.') });
                    break;
            }

            return this;
        };

        this.showPageSettingsDialog = function () {
            return new PageSettingsDialog(this).show();
        };

        /**
         * Opens slide background dialog.
         *
         * @returns {SlideBackgroundDialog}
         */
        this.openSlideBackgroundDialog = function () {
            return new SlideBackgroundDialog(self, json.deepClone(docModel.getSlideAttributesByFamily(docModel.getActiveSlideId(), 'fill'))).show(); // open slide background dialog
        };

        /**
         * Public method for handling slide background visibility.
         *
         * @param {String} activeSlideId
         *  id of the active slide
         *
         * @param {Object} [previewFillAttrs]
         *  optional set of attributes on the slide for preview
         */
        this.handleSlideBackgroundVisibility = function (activeSlideId, previewFillAttrs) {
            let slideId;
            let layoutId;
            let masterId;

            if (docModel.isMasterSlideId(activeSlideId)) {
                slideId = null;
                layoutId = null;
                masterId = activeSlideId;
            } else if (docModel.isLayoutSlideId(activeSlideId)) {
                slideId = null;
                layoutId = activeSlideId;
                masterId = docModel.getMasterSlideId(layoutId);
            } else {
                slideId = activeSlideId;
                layoutId = docModel.getLayoutSlideId(slideId);
                masterId = docModel.getMasterSlideId(layoutId);
            }
            handleSlideBackgroundVisibility(slideId, layoutId, masterId, previewFillAttrs);
        };

        /**
         * Opens drawing background dialog.
         */
        this.showDrawingFormatDialog = function (attrSet) {
            drawingFormatDialog = new DrawingFormatDialog(self, attrSet);
            return drawingFormatDialog.show();
        };

        /**
         * Public method for handling follow master page change.
         * Info: this method is only used as preview, while slide background dialog is open.
         *
         * @param {Object} options
         *     @param {Boolean} options.followMasterShapes = false
         */
        this.previewFollowMasterPageChange = function (options) {

            // the id of the active slide
            const slideId = docModel.getActiveSlideId();
            // the id of the layout slide
            let layoutId = null;
            // the id of the master slide
            let masterId = null;
            // whether drawings of layout slide shall be visible
            const followMasterShapes = getBooleanOption(options, 'followMasterShapes', false);
            // whether drawings of layout slide shall be visible
            let showLayoutSlide = true;
            // whether drawings of master slide shall be visible
            let showMasterSlide = true;

            if (docModel.isStandardSlideId(slideId)) {
                layoutId = docModel.getLayoutSlideId(slideId);
                showLayoutSlide = followMasterShapes;
                if (layoutId) {
                    masterId = docModel.getMasterSlideId(layoutId);
                    // master slide can only be visible, if layout slide is visible, too
                    showMasterSlide = showLayoutSlide ? docModel.isFollowMasterShapeSlide(layoutId) : false;
                }
            } else if (docModel.isLayoutSlideId(slideId)) {
                masterId = docModel.getMasterSlideId(slideId);
                showMasterSlide = followMasterShapes;
            }

            // handling the visibility of the drawings of the layout and the master slide
            handleLayoutMasterVisibility(slideId, layoutId, masterId, showLayoutSlide, showMasterSlide);
        };

        /**
         * After successful import the scroll handler for the vertical scrolling can be registered.
         */
        this.enableVerticalScrollBar = function () {

            /**
             * After a change of the layout, the height of the content node need
             * to be updated and also the margin-top of the page node.
             */
            refreshVerticalScrollBar = function (options) {

                // do nothing, if the document was reloaded with a snapshot
                if (getBooleanOption(options, 'documentReloaded', false)) { return; }

                // coming back later, if a selection box is currently active (creating a selection, insering a shape, ...)
                if (docModel.getSelectionBox() && docModel.getSelectionBox().isSelectionBoxActive() && getBooleanOption(options, 'hasScrollEffect', false)) {
                    docModel.executeDelayed(function () { refreshVerticalScrollBar(options); }, 200);
                    return;
                }

                // setting the height of the app content node, so that the vertical scroll bar is adapted.
                handleActiveViewChange({ slideCount: docModel.getActiveViewCount() });

                // also updating the top margin at the page for the active slide
                handleVerticalScrollbarUpdate({ pos: docModel.getActiveSlideIndex(), keepScrollPosition: getBooleanOption(options, 'keepScrollPosition', false) });
            };

            /**
             * After releasing the mouse button when scrolling on the vertical scroll bar,
             * it might be necessary to adapt the slide position and to reset some global markers.
             */
            const stopScrollBarScrolling = () => {

                isMouseButtonPressedScrolling = false; // must be set to false before final call of 'handleVerticalScrollbarUpdate'

                //  updating the top margin at the page for the active slide, if a slide change happened
                if (slideChanged && verticalScrollChanged) { handleVerticalScrollbarUpdate({ pos: docModel.getActiveSlideIndex() }); }
                // resetting global markers
                slideChanged = false;
                verticalScrollChanged = false;
            };

            // Registering vertical scroll handler
            self.$contentRootNode.on('scroll', scrollInContentRootNode);
            // Handle an update of a zoom change
            self.on('change:zoom', function () {
                pageNode.removeData('scrollTopMargin'); // the saved value cannot be reused
                refreshVerticalScrollBar();
            });
            // Handle switch of active view
            self.listenTo(docModel, 'change:activeView', handleActiveViewChange);
            // Handle an update of the vertical scroll bar
            self.listenTo(docModel, 'update:verticalscrollbar', handleVerticalScrollbarUpdate);
            // inserting or deleting a slide requires also an update of the vertical scroll bar
            self.listenTo(docModel, 'inserted:slide removed:slide moved:slide', refreshVerticalScrollBar);
            // updating the vertical scroll bar and scroll position after a reload of the document (DOCS-1891)
            self.listenTo(app, 'revealApp', updateScrollAfterReload);

            // avoiding scrolling together with mouse down (and taking care of the event target (DOCS-3239))
            self.$contentRootNode.on('mousedown touchstart', function (event) {
                if ($(event.target).is('.app-content-root')) { isMouseButtonPressedScrolling = true; }
            });
            self.$contentRootNode.on('mouseup touchend', stopScrollBarScrolling);

            // refreshing once the vertical scroll bar after successful loading
            refreshVerticalScrollBar();
        };

        /**
         * Set the virtual focus to the slidepane or the document.
         *
         * @param {Boolean} focusToSlidepane
         *  Set focus to slidepane when 'true' or set it to the document when 'false'.
         */
        this.setVirtualFocusSlidepane = function (focusToSlidepane) {
            slidepaneHasFocus = !!focusToSlidepane;
        };

        /**
         * Returns whether the slidepane has the virtual focus.
         */
        this.hasSlidepaneVirtualFocus = function () {
            return slidepaneHasFocus;
        };

        this.getActiveTableFlags = function () {
            const flags = {
                    firstRow:   true,
                    lastRow:    true,
                    firstCol:   true,
                    lastCol:    true,
                    bandsHor:   true,
                    bandsVert:  true
                },
                drawing = docModel.getSelection().getAnyDrawingSelection(),
                tableAttrs =  drawing ? getExplicitAttributes(drawing, 'table') : null;

            if (tableAttrs && tableAttrs.exclude && _.isArray(tableAttrs.exclude) && tableAttrs.exclude.length > 0) {
                tableAttrs.exclude.forEach(function (ex) {
                    flags[ex] = false;
                });
            }

            return flags;
        };

        /**
         * Public getter for checking whether snap guide lines are turned on, or not.
         *
         * @returns {Boolean}
         */
        this.areSnaplinesEnabled = function () {
            return snapGuideLinesEnabled;
        };

        /**
         * Public setter for enabling/disabling snap guide lines durring move or resize shape.
         *
         * @param {Boolean} state
         */
        this.toggleSnapGuideLines = function (state) {
            snapGuideLinesEnabled = !!state;
        };

        /**
         * Wheter a slide change animation is currently running.
         *
         * @returns {Boolean}
         *  Wheter a slide change animation is currently running.
         */
        this.isSlideChangeAnimationRunning = function () {
            return isSlideChangeAnimationRunning;
        };

        /**
         * Public accessor to save and return the current scroll position of the document
         * and the slide pane.
         *
         * @returns {Object}
         *  An object with the properties 'top' and 'left' of the current scroll position.
         *  Additionally the object contains a property 'slidePaneTop' for the vertical
         *  scroll position of the slide pane.
         */
        this.saveScrollPosition = function () {
            const scrollPos = _.copy(saveScrollPosition());
            // also saving the scroll position of the slide pane
            scrollPos.slidePaneTop = slidepane.saveSlidePaneScrollPosition();

            return scrollPos;
        };

        // initialization -----------------------------------------------------

        // create the context menu
        contextMenu = this.member(new PresentationContextMenu(this, { delay: 200 }));

        // initialize zoom handling
        this.zoomState.register("slide", calculateOptimalZoomFactor);
        this.zoomState.set(COMPACT_DEVICE ? "slide" : "fixed", 1, { silent: true });
        this.listenTo(this.zoomState, "change:state", zoomChangedHandler);

        // store the values of page width and height with paddings, after loading is finished
        this.waitForImportSuccess(function () {
            pageNodeWidth = pageNode.outerWidth();
            pageNodeHeight = pageNode.outerHeight();

            if (docModel.getSlideTouchMode()) {
                const appContent = pageNode.parent();
                appContent.attr('contenteditable', true);

                if (IOS_SAFARI_DEVICE) { docModel.getSelection().getClipboardNode().attr('contenteditable', false); }
                const listenerList = docModel.getListenerList();
                pageNode.off(listenerList);
                appContent.on(listenerList);

                //we dont want the focus in the document on the beginning!
                hideFocus();
            }

            // quick fix for bug 40053
            if (_.browser.Safari) { addDeviceMarkers(app.getWindowNode()[0]); }

            // avoiding flickering during slide change for Chrome and Edge browser by hiding the slide during change,
            // but making the slide visible as fast as possible. Therefore this debounced handler checks, if there are
            // no more delayed vertical scroll events from Chrome or Edge. If there is no more event for at least
            // 100 ms and the hidden slide was not already made visible, this function triggers 'activeslide:unhide',
            // so that the hidden slide will immediately be made visible.
            if (_.browser.Chrome || _.browser.Edge) {
                showActiveSlideDebounced = self.debounce(function () { if (scrollBlockIsActive) { self.trigger('activeslide:unhide'); } }, { delay: 100 });
            }
        });

        // presentation view mixins -------------------------------------------

        /**
         * Additional preview specific behavior for the presentation view.
         *
         * `SlidePreviewMixin` provides a set of specific functionality
         *  that is just for rendering the preview of any presentation
         *  slide that most recently has been changed.
         */
        SlidePreviewMixin.call(this);

        /**
         * Additional Comment-Layer specific behavior for the presentation view.
         *
         * `CommentsLayerMixin` provides a set of specific functionality
         *  that is just for rendering the comment indicators of any
         *  presentation slide that features 'threaded comments'.
         */
        CommentsLayerMixin.call(this);
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ async implInitializeGui(options) {

        // base class initialization
        await super.implInitializeGui(options);

        // insert the presentation picker into the topbar (DOCS-2832)
        this.topPane.rightToolBar.addControl('present/startpresentation', new StartPresentationPicker({ label: gt('Present'), tooltip: gt('Present') }), { section: "custom" });

        // create the statuspane
        this.addPane(new StatusPane(this));

        // initialize zoom type passed in options
        if (options?.zoomType) {
            this.changeZoom(options.zoomType, options.zoomFactor);
        }

        // Note: The delay should be higher than resizeWindowHandlerDebounced() in slidepane.js to prevent flickering while resizing with 'fit to width'.
        this.on("refresh:layout", this._refreshLayoutHandler);
    }

    /*protected override*/ implInitToolPanes() {
        super.implInitToolPanes();

        this.registerToolPane("format",     this._initFormatToolPane,    { label: FORMAT_HEADER_LABEL,     visibleKey: "view/toolpane/format/visible",    priority: 2 });
        this.registerToolPane("font",       this._initFontToolPane,      { label: FONT_HEADER_LABEL,       visibleKey: "view/toolpane/font/visible",      priority: 2 });
        this.registerToolPane("paragraph",  this._initParagraphToolPane, { label: PARAGRAPH_LABEL,         visibleKey: "view/toolpane/paragraph/visible", priority: 2 });
        this.registerToolPane("insert",     this._initInsertToolPane,    { label: INSERT_HEADER_LABEL,     visibleKey: "view/toolpane/insert/visible",    priority: 2 });
        this.registerToolPane("slide",      this._initSlideToolPane,     { label: SLIDE_HEADER_LABEL,      visibleKey: "view/toolpane/slide/visible",     priority: 2 });
        this.registerToolPane("present",    this._initPresentToolPane,   { label: PRESENT_HEADER_LABEL,    visibleKey: "view/toolpane/present/visible",   priority: 15, sticky: true });
        this.registerToolPane("review",     this._initReviewToolPane,    { label: REVIEW_HEADER_LABEL,     visibleKey: "view/toolpane/review/visible",    priority: 20, sticky: true });
        this.registerToolPane("table",      this._initTableToolPane,     { label: TABLE_HEADER_LABEL,      visibleKey: "view/toolpane/table/visible",     priority: 1 });
        this.registerToolPane("drawing",    this._initDrawingToolPane,   { labelKey: "drawing/type/label", visibleKey: "view/toolpane/drawing/visible",   priority: 1 });
    }

    /*protected override*/ implInitViewMenu(viewButton) {
        super.implInitViewMenu(viewButton);

        viewButton.addContainer({ section: "zoom" });
        viewButton.addControl("view/zoom/type", new Button({ label: ZOOM_SCREEN_WIDTH_LABEL, value: "slide" }));

        viewButton.addSection("views", gt("Views"));
        viewButton.addControl("slide/activemasterslide", new CheckBox({ label: gt("View master"), tooltip: gt("View master & layout slides") }));

        viewButton.addSection("options", OPTIONS_LABEL);
        // note: we do not set aria role to "menuitemcheckbox" or "button" due to Safari just working correctly with "checkox". CheckBox constructor defaults aria role to "checkbox"
        viewButton.addControl("view/toolpane/show",     new CheckBox(SHOW_TOOLBARS_CHECKBOX_OPTIONS),     { visibleKey: "view/toolpane/show" });
        viewButton.addControl("view/slidepane/show",    new CheckBox(SHOW_SLIDEPANE_CHECKBOX_OPTIONS),    { visibleKey: "view/slidepane/show" });
        viewButton.addControl("view/commentspane/show", new CheckBox(SHOW_COMMENTSPANE_CHECKBOX_OPTIONS), { visibleKey: "document/ooxml" });
        viewButton.addControl("document/users",         new CheckBox(SHOW_COLLABORATORS_CHECKBOX_OPTIONS));
        viewButton.addControl("view/usesnaplines",      new CheckBox(SHOW_SNAPLINES_CHECKBOX_OPTIONS));
    }

    // private methods --------------------------------------------------------

    /*private*/ _initFormatToolPane() {
        this.insertToolBar(new SlideToolBar(this));
        this.insertToolBar(new FontFamilyToolBar(this));
        this.insertToolBar(new FontStyleToolBar(this));
        this.insertToolBar(new FontColorToolBar(this));
        this.insertToolBar(new FormatPainterToolBar(this));
        this.insertToolBar(new BoxAlignmentToolBar(this));
        this.insertToolBar(new ListStyleToolBar(this));
    }

    /*private*/ _initFontToolPane() {
        this.insertToolBar(new SlideToolBar(this));
        this.insertToolBar(new FontFamilyToolBar(this));
        this.insertToolBar(new FontStyleToolBar(this));
        this.insertToolBar(new FontColorToolBar(this));
        this.insertToolBar(new FormatPainterToolBar(this));
    }

    /*private*/ _initParagraphToolPane() {
        this.insertToolBar(new BoxAlignmentToolBar(this));
        this.insertToolBar(new ListStyleToolBar(this));
    }

    /*private*/ _initInsertToolPane() {

        this.insertToolBar(new SlideToolBar(this));

        const insertToolBar = this.createToolBar(COMBINED_TOOL_PANES ? { shrinkToMenu: { label: gt("Insert"), tooltip: gt("Insert Drawing") } } : null);
        insertToolBar.addSection("placeholder");
        insertToolBar.addControl("placeholder/insert", new PlaceholderTypePicker(this));
        insertToolBar.addSection("frames");
        insertToolBar.addControl("table/insert", new TableSizePicker(this, MAX_TABLE_COLUMNS, MAX_TABLE_ROWS), { visibleKey: "table/insert/available" });
        insertToolBar.addControl("image/insert/dialog", new ImagePicker());
        insertToolBar.addControl("textframe/insert", new InsertTextFrameButton({ toggle: !this.docModel.getSlideTouchMode() }));
        insertToolBar.addControl("shape/insert", new ShapeTypePicker(this));
        insertToolBar.addSection("contents");
        insertToolBar.addControl("character/hyperlink/dialog",   new Button(HYPERLINK_BUTTON_OPTIONS));
        insertToolBar.addControl("character/insert/tab",         new Button({ icon: "png:insert-tab", label: gt("Tab stop"), tooltip: gt("Insert tab stop") }));
        insertToolBar.addControl("document/insertfooter/dialog", new Button(FOOTER_BUTTON_OPTIONS));
        insertToolBar.addControl("document/insertfield",         new InsertFieldPicker(this));

        if (!this.docApp.isODF()) {
            insertToolBar.addSection("comments");
            insertToolBar.addControl("threadedcomment/insert", new Button(INSERT_COMMENT_OPTIONS));
        }
    }

    /*private*/ _initSlideToolPane() {

        if (!COMBINED_TOOL_PANES) {
            this.insertToolBar(new SlideToolBar(this));
        }

        const slideToolBar = this.createToolBar();
        slideToolBar.addSection("slide");
        slideToolBar.addControl("slide/duplicateslides", new Button({ icon: "bi:files", label: gt("Duplicate"), tooltip: gt("Duplicate selected slides") }));
        slideToolBar.addControl("slide/deleteslide",     new Button({ icon: "bi:trash", label: gt("Delete"), tooltip: gt("Delete slide") }));
        slideToolBar.addControl("debug/hiddenslide",     new Button({ icon: "bi:eye-slash", toggle: true, label: gt("Hide"), tooltip: gt("Hide slide") }));
        slideToolBar.addSection("style");
        slideToolBar.addControl("slide/setbackground", new Button({ icon: "png:slide-background", label: gt("Background"), tooltip: gt("Set slide background") }));
        slideToolBar.addSection("master");
        slideToolBar.addControl("slide/masternormalslide", new SwitchSlideEditModeButton(this));
    }

    /*private*/ _initPresentToolPane() {

        const presentToolBar = this.createToolBar();
        presentToolBar.addControl("present/startpresentation", new StartPresentationPicker({ label: gt("Start presentation"), tooltip: gt("Start the presentation") }));
        if (!IOS_SAFARI_DEVICE) {
            presentToolBar.addControl("present/fullscreenpresentation", new CheckBox({ boxed: true, label: gt("Full screen"), tooltip: gt("Show the presentation in full screen mode") }));
        }
    }

    /*private*/ _initReviewToolPane() {

        let reviewToolBar;
        if (!COMBINED_TOOL_PANES) {
            reviewToolBar = this.createToolBar({
                visibleKey: "document/spelling/available",
                shrinkToMenu: {
                    icon: "png:online-spelling",
                    itemKey: "document/onlinespelling",
                    splitButton: true,
                    toggle: true,
                    tooltip: gt("Check spelling"),
                    //#. tooltip for a drop-down menu with functions for reviewing the document (spelling, change tracking)
                    caretTooltip: gt("More review actions")
                }
            });
        } else {
            reviewToolBar = this.createToolBar({ visibleKey: "document/spelling/available" });
        }

        // controls
        if (SPELLING_ENABLED) {
            reviewToolBar.addSection("spelling");
            reviewToolBar.addControl("document/onlinespelling", new Button({ icon: "png:online-spelling", tooltip: gt("Check spelling permanently"), toggle: true, dropDownVersion: { label: gt("Check spelling") } }));
            reviewToolBar.addSection("language");
            reviewToolBar.addControl("character/language", new LanguagePicker(this));
        }

        if (this.docApp.isOOXML()) {
            this.insertToolBar(new CommentsToolBar(this));
        }
    }

    /*private*/ _initTableToolPane() {

        const tableToolBar = this.createToolBar();
        tableToolBar.addSection("delete");
        tableToolBar.addControl("drawing/delete", new Button(DELETE_DRAWING_BUTTON_OPTIONS));
        tableToolBar.addSection("colrow");
        tableToolBar.addControl("table/insert/row", new Button({ icon: "png:table-insert-row", tooltip: gt("Insert row") }));
        tableToolBar.addControl("table/delete/row", new Button({ icon: "png:table-delete-row", tooltip: gt("Delete selected rows") }));
        tableToolBar.addContainer();
        tableToolBar.addControl("table/insert/column", new Button({ icon: "png:table-insert-column", tooltip: gt("Insert column") }));
        tableToolBar.addControl("table/delete/column", new Button({ icon: "png:table-delete-column", tooltip: gt("Delete selected columns") }));

        if (!COMBINED_TOOL_PANES) {
            tableToolBar.addSection("alignment");
            tableToolBar.addControl("table/alignment/menu", new TableAlignmentPicker(this));
            tableToolBar.addSection("style");
            tableToolBar.addControl("table/fillcolor", new ColorPicker(this, { icon: "png:table-fill-color", tooltip: gt("Cell fill color"), splitValue: Color.ACCENT2 }));
            tableToolBar.addContainer();
            tableToolBar.addControl("table/cellborder", new BorderFlagsPicker(this, { tooltip: CELL_BORDERS_LABEL, showInnerH: true, showInnerV: true }));

            const tableBorderToolBar = this.createToolBar({ classes: "no-separator" });
            tableBorderToolBar.addControl("table/borderwidth", new BorderWidthPicker(this, { tooltip: gt("Cell border width") }));

            const tableStyleToolBar = this.createToolBar();
            tableStyleToolBar.addControl("table/stylesheet", new TableStylePicker(this, this.getActiveTableFlags.bind(this)));
        }

        const tablePositionToolBar = this.createToolBar();
        tablePositionToolBar.addControl("drawing/order", new DrawingArrangementPicker(this, { rotationControls: true }));
        tablePositionToolBar.addControl("drawing/align", new DrawingAlignmentPicker(this));
    }

    /*private*/ _initDrawingToolPane() {

        const drawingToolBar = this.createToolBar();
        drawingToolBar.addControl("shape/insert",   new ShapeTypePicker(this, { label: null }), { visibleKey: "drawing/type/shape" });
        drawingToolBar.addControl("drawing/delete", new Button(DELETE_DRAWING_BUTTON_OPTIONS),  { visibleKey: "drawing/delete" });

        const drawingLineToolBar = this.createToolBar({ visibleKey: "drawing/shapes/support/line", shrinkToMenu: { label: gt("Borders") } });
        drawingLineToolBar.addControl("drawing/border/style", new DrawingLineStylePicker(this, { line: false }), { visibleKey: "drawing/border/style/support" });
        drawingLineToolBar.addControl("drawing/border/style", new DrawingLineStylePicker(this, { line: true }),  { visibleKey: "drawing/line/style/support" });
        drawingLineToolBar.addControl("drawing/border/color", new DrawingLineColorPicker(this, { line: false }), { visibleKey: "drawing/border/style/support" });
        drawingLineToolBar.addControl("drawing/border/color", new DrawingLineColorPicker(this, { line: true }),  { visibleKey: "drawing/line/style/support" });

        const drawingLineEndsToolBar = this.createToolBar({ visibleKey: "drawing/shapes/support/lineendings" });
        drawingLineEndsToolBar.addControl("drawing/line/endings", new DrawingArrowPresetPicker());

        const drawingFillToolBar = this.createToolBar({ visibleKey: "drawing/shapes/support/fill" });
        drawingFillToolBar.addControl("drawing/fill/color", new DrawingFillColorPicker(this), { visibleKey: "drawing/fill/style/support" });

        const imageCropToolBar = this.createToolBar({ visibleKey: "drawing/cropselection/visible" });
        imageCropToolBar.addControl("drawing/crop", new ImageCropPosition(this));

        this.insertToolBar(new FormatPainterToolBar(this));

        const drawingAnchorToolBar = this.createToolBar({ visibleKey: "document/editable/anydrawing" });
        drawingAnchorToolBar.addControl("drawing/order", new DrawingArrangementPicker(this, { rotationControls: true }));
        drawingAnchorToolBar.addControl("drawing/align", new DrawingAlignmentPicker(this));

        const drawingGroupToolBar = this.createToolBar({ visibleKey: "drawing/notsingledrawing" });
        drawingGroupToolBar.addControl("drawing/group",   new Button({ icon: "svg:group", toggle: true, tooltip: GROUP_DRAWING_TOOLTIP }));
        drawingGroupToolBar.addControl("drawing/ungroup", new Button({ icon: "svg:ungroup", toggle: true, tooltip: UNGROUP_DRAWING_TOOLTIP }));

        const autoFitGroup = new RadioGroup();
        autoFitGroup.addOption("noautofit",      { label: gt("Off"),           tooltip: gt("Turn automatic resizing of text frames off") });
        if (!this.docApp.isODF()) { autoFitGroup.addOption("autotextheight", { label: gt("Shrink text"),   tooltip: gt("Turn automatic shrink text on overflow on") }); }
        autoFitGroup.addOption("autofit",        { label: gt("Resize frame"),  tooltip: gt("Turn automatic height resizing of text frames on") });

        const moreButton = new CompoundButton(this, { label: gt("Options"), tooltip: gt("More options"), dropDownVersion: { visible: false } });
        moreButton.addSection("autofit", gt("Autofit"));
        moreButton.addControl("drawing/textframeautofit", autoFitGroup);
        moreButton.addSection("resize", gt("Resize"));
        moreButton.addControl("drawing/lockratio", new CheckBox({ label: gt("Lock ratio"), boxed: true, tooltip: gt("Lock or unlock the drawing aspect ratio"), tristate: true }));

        const drawingOptionsToolBar = this.createToolBar({ visibleKey: "document/editable/anydrawing" });
        drawingOptionsToolBar.addControl("drawing/options/menu", moreButton);
    }
}
