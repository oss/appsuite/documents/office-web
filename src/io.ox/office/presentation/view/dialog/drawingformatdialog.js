/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math, jpromise } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, IOS_SAFARI_DEVICE, hasKeyCode } from '@/io.ox/office/tk/dom';
import { DISABLED_CLASS, createRadioButtonNode, checkButtonNodes, setButtonKeyHandler } from '@/io.ox/office/tk/forms';
import { BaseDialog } from '@/io.ox/office/tk/dialogs';

import { debounceAfterActionsMethod } from "@/io.ox/office/editframework/app/decorators";
import { Color } from '@/io.ox/office/editframework/utils/color';

import { getOxoPosition } from '@/io.ox/office/textframework/utils/position';
import { PercentField, ImagePicker, DrawingLineStylePicker, DrawingArrowPresetPicker, DrawingLineColorPicker, DrawingFillColorPicker } from '@/io.ox/office/textframework/view/controls';
import { FORMAT_DRAWING_LABEL } from '@/io.ox/office/textframework/view/labels';

import { getPresetArrow, getPresetBorder, resolvePresetBorder } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { updateFormatting } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { showInsertImageDialog } from '@/io.ox/office/drawinglayer/view/drawingdialogs';

import '@/io.ox/office/presentation/view/dialog/presentationdialogs.less';

// constants ==============================================================

// constants for pointer events
const START_POINTER_EVENTS = IOS_SAFARI_DEVICE ? 'touchstart' : 'mousedown touchstart';
const MOVE_POINTER_EVENTS = IOS_SAFARI_DEVICE ? 'touchmove' : 'mousemove touchmove';
const END_POINTER_EVENTS = IOS_SAFARI_DEVICE ? 'touchend' : 'mouseup touchend';

// a marker class for each drawing that is handled by this dialog
export const TEMPORARY_MARKER_CLASS = "temporary";

// class DrawingFormatDialog ============================================

/**
 * The drawing background dialog.
 * Provides different types and options for a drawing backgrounds for Presentation document.
 *
 *
 * The changes will be applied to the current document after clicking the 'Ok' button.
 *
 * The dialog itself is shown by calling this.execute. The caller can react on successful
 * finalization of the dialog within an appropriate done-Handler, chained to the
 * this.execute function call.
 *
 * @param {PresentationView} docView
 *  The document view containing this dialog instance.
 *
 * @param {PtDrawingAttributeSet} attrSet
 *  The mixed attributes of the selected drawing objects.
 */
export class DrawingFormatDialog extends BaseDialog {

    docApp;
    #attrSet;
    #docModel;
    #selection;
    #allDrawings;
    #drawingTypeCount;
    #isFill;
    #origDrawingInfos;
    #previewDrawingInfos;
    #lineColorPicker;
    #lineStylePicker;
    #lineArrowPicker;
    #fillColorPicker;
    #imagePicker;
    #$controlAreas;
    #noBgRadioBtn;
    #colorRadioBtn;
    #imageRadioBtn;
    #activeRadioBtn;
    #previewImageUrl;
    #slidingActive;

    #transpArea;
    #transpSlider;
    #sliderRail;
    #sliderOverlay;
    #transpSpinner;

    constructor(docView, attrSet) {

        // base constructor
        super({
            id: 'io-ox-office-drawing-format-dialog',
            title: FORMAT_DRAWING_LABEL,
            movable: true,
            focus: '[role="radio"][data-checked="true"]',
            blockModelProcessing: true
        });

        this.#attrSet = attrSet;
        this.#docModel = docView.docModel;
        this.docApp = this.#docModel.getApp();
        this.#selection = this.#docModel.getSelection();
        this.#allDrawings = this.#selection.getAllDrawingsInSelection();

        // selected drawings array, consisting of objects with the drawing and its attributes in them
        this.#origDrawingInfos = this.#allDrawings.map(drawing => {

            const type = this.#selection.getDrawingType(drawing);

            // setting a class marker at every affected drawing
            drawing.addClass(TEMPORARY_MARKER_CLASS);

            return {
                drawing,
                type,
                attrs: _.copy(this.#docModel.drawingStyles.getElementAttributes(drawing), true),
                start: getOxoPosition(this.#docModel.getCurrentRootNode(), drawing, 0) // startPosition
            };
        });

        this.#drawingTypeCount = _.countBy(this.#origDrawingInfos, "type");
        this.#isFill = !!(this.#drawingTypeCount.shape || this.#drawingTypeCount.image);
        this.#previewDrawingInfos = this.#origDrawingInfos.map(origDrawingInfo => ({
            drawing: origDrawingInfo.drawing,
            attrs: _.copy(origDrawingInfo.attrs, true),
            start: _.copy(origDrawingInfo.start.slice())
        }));

        this.#lineColorPicker = this.member(new DrawingLineColorPicker(docView, { label: null, title: null, splitValue: null }));
        this.#lineStylePicker = this.member(new DrawingLineStylePicker(docView, { line: true }));
        this.#lineArrowPicker = this.member(new DrawingArrowPresetPicker());
        this.#fillColorPicker = this.member(new DrawingFillColorPicker(docView, { label: null, title: null, splitValue: null }));
        this.#imagePicker = this.member(new ImagePicker({ label: gt('Choose'), splitValue: null, hideUrl: true }));

        this.#slidingActive = false;

        // initialization -----------------------------------------------------

        this.#$controlAreas = $('<div class="control-area">').append($('<h5 class="h5 drawing-format-dialog__area-title">').text(gt('Line')));

        // initialize the body element of the dialog
        this.$bodyNode
            .addClass('drawing-format-dialog')
            .toggleClass('mobile', !!SMALL_DEVICE)
            .append(this.#$controlAreas);

        // create the layout of the dialog
        this.#initControls();

        // handler for the OK button
        this.setOkHandler(() => {

            function getChangedLineAttrs(origAttrSet, previewAttrSet) {

                const origLineAttrs = origAttrSet.line;
                const previewLineAttrs = previewAttrSet.line;
                const changedLineAttrs = {}; // accumulate changed values for the operation

                if (!(_.isEqual(origLineAttrs.color, previewLineAttrs.color))) {
                    changedLineAttrs.color = JSON.parse(JSON.stringify(previewLineAttrs.color));
                }
                if (origLineAttrs.style !== previewLineAttrs.style) {
                    changedLineAttrs.style = previewLineAttrs.style;
                }
                if (origLineAttrs.type !== previewLineAttrs.type) {
                    changedLineAttrs.type = previewLineAttrs.type;
                }
                if (origLineAttrs.width !== previewLineAttrs.width) {
                    changedLineAttrs.width = previewLineAttrs.width;
                }
                if (origLineAttrs.headEndType !== previewLineAttrs.headEndType) {
                    changedLineAttrs.headEndType = previewLineAttrs.headEndType;
                }
                if (origLineAttrs.tailEndType !== previewLineAttrs.tailEndType) {
                    changedLineAttrs.tailEndType = previewLineAttrs.tailEndType;
                }

                return _.isEmpty(changedLineAttrs) ? null : changedLineAttrs;
            }

            function getChangedFillAttrs(origAttrSet, previewAttrSet) {

                const origFillAttrs = origAttrSet.fill;
                const previewFillAttrs = previewAttrSet.fill;
                const changedFillAttrs = {}; // accumulate changed values for the operation

                if (origFillAttrs.bitmap && !(_.isEqual(origFillAttrs.bitmap, previewFillAttrs.bitmap))) {
                    changedFillAttrs.bitmap = previewFillAttrs.bitmap;
                }
                if (!(_.isEqual(origFillAttrs.color, previewFillAttrs.color))) {
                    changedFillAttrs.color = previewFillAttrs.color;
                }
                if (!_.isEmpty(changedFillAttrs) || (origFillAttrs.type !== previewFillAttrs.type)) {
                    changedFillAttrs.type = previewFillAttrs.type;
                }

                return _.isEmpty(changedFillAttrs) ? null : changedFillAttrs;
            }

            // removing the marker from the selected drawings (DOCS-4733)
            this.#allDrawings.forEach(drawing => { drawing.removeClass(TEMPORARY_MARKER_CLASS); });

            const operations = this.#previewDrawingInfos.reduce((accumulator, previewDrawingInfo, index) => {

                const origAttrSet = this.#origDrawingInfos[index].attrs;
                const previewAttrSet = previewDrawingInfo.attrs;
                const lineAttrs = getChangedLineAttrs(origAttrSet, previewAttrSet);
                const fillAttrs = getChangedFillAttrs(origAttrSet, previewAttrSet);

                const attrSet = {};
                if (lineAttrs) { attrSet.line = lineAttrs; }
                if (fillAttrs) { attrSet.fill = fillAttrs; }

                if (!_.isEmpty(attrSet)) {
                    accumulator.push({ start: _.copy(previewDrawingInfo.start), attrs: attrSet });
                }

                return accumulator;
            }, []);

            this.#docModel.operationWithFormatChanges(operations);
        });

        // on cancel restore original values
        this.setCancelHandler(() => {

            // removing the marker from the selected drawings (DOCS-4733)
            this.#allDrawings.forEach(drawing => { drawing.removeClass(TEMPORARY_MARKER_CLASS); });

            this.#origDrawingInfos.forEach(origDrawingInfo => {
                this.#docModel.drawingStyles.updateDrawingFormatting(origDrawingInfo.drawing, origDrawingInfo.attrs);
            });
        });

        // line/border color picker event handling
        this.#lineColorPicker.on('group:commit', data => {
            this.#applyLineColor(data);
            this.#lineColorPicker.menu.hide();
        });

        // line style picker event handling
        this.#lineStylePicker.on('group:commit', data => {
            this.#previewDrawingInfos.forEach(previewDrawingInfo => {
                this.#setLineForPreview(previewDrawingInfo.drawing, previewDrawingInfo.attrs, data);
            });

            this.#lineStylePicker.menu.hide();
        });

        // line arrow picker event handling
        this.#lineArrowPicker.on('group:commit', data => {
            this.#previewDrawingInfos.forEach(previewDrawingInfo => {
                this.#setArrowsForPreview(previewDrawingInfo.drawing, previewDrawingInfo.attrs, data);
            });

            this.#lineArrowPicker.menu.hide();
        });

        if (this.#isFill) {

            // fill color picker event handling
            this.#fillColorPicker.on('group:commit', () => {
                this.#fillColorPicker.menu.hide();
                this.#checkRadioButton(this.#colorRadioBtn, { force: true });
            });

            // background image picker event handling
            this.#imagePicker.on("group:commit", sourceType => {
                jpromise.floating(showInsertImageDialog(docView, sourceType, imageDesc => {
                    if (this.#previewImageUrl !== imageDesc.url) {
                        this.#previewImageUrl = imageDesc.url;
                        this.#checkRadioButton(this.#imageRadioBtn, { force: true });
                    }
                }));
            });

            // automatically check the correct radio button already when opening the dropdown menus
            this.#fillColorPicker.menu.on("popup:show", () => this.#checkRadioButton(this.#colorRadioBtn));
            this.#imagePicker.menu.on("popup:show", () => this.#checkRadioButton(this.#imageRadioBtn));
        }
    }

    // private methods ----------------------------------------------------

    #applyLineColor(jsonColor) {
        this.#previewDrawingInfos.forEach(previewDrawingInfo => {
            previewDrawingInfo.attrs.line.color = jsonColor;
        });
        this.#setAlphaPreview();
    }

    #applyFillColor(jsonColor) {
        this.#previewDrawingInfos.forEach(previewDrawingInfo => {
            let attrs = null;

            if (!jsonColor || jsonColor.type === 'auto') {
                attrs = { fill: { type: 'none' } };
            } else {
                attrs = { fill: { type: 'solid', color: jsonColor } };
                if (this.#attrSet.fill) {
                    if (this.#attrSet.fill.gradient) { attrs.fill.gradient = null; }
                    if (this.#attrSet.fill.bitmap?.imageUrl) { attrs.fill.bitmap = { imageUrl: null }; }
                }
            }

            previewDrawingInfo.attrs.fill = attrs.fill;
        });
        this.#setAlphaPreview();
    }

    /**
     * Helper function for enabling or disabling the specified DOM elements.
     *
     * @param {jQuery} nodes
     *  The DOM elements to be manipulated. If this object is a jQuery
     *  collection, uses all nodes it contains.
     *
     * @param {Boolean} state
     *  Whether to enable (true) or disable (false) the DOM elements.
     */
    #enableNodes(nodes, state) {
        nodes.toggleClass(DISABLED_CLASS, !state);
        if (state) {
            nodes.removeAttr('aria-disabled').prop('tabindex', 0);
        } else {
            nodes.attr('aria-disabled', true).prop('tabindex', -1);
        }
    }

    #checkRadioButton(radioBtn, options) {

        // whether active radio button changes (filter clicks on same radio button)
        const changed = this.#activeRadioBtn !== radioBtn;

        // update radio button selection, and enabled states of dependent controls
        if (changed) {

            if (this.#activeRadioBtn) { checkButtonNodes(this.#activeRadioBtn, false); }
            checkButtonNodes(radioBtn, true);
            this.#activeRadioBtn = radioBtn;

            const isNoFill = radioBtn === this.#noBgRadioBtn;
            this.#transpArea.toggleClass("disabled", isNoFill);
            this.#enableNodes(this.#transpArea.find(".slider-container, input"), !isNoFill);
            this.#transpSpinner.enable(!isNoFill);
        }

        // update live preview in document
        if (!options?.silent && (changed || options?.force)) {
            switch (radioBtn) {
                case this.#noBgRadioBtn:
                    this.#applyFillColor(Color.AUTO);
                    break;
                case this.#colorRadioBtn:
                    this.#applyFillColor(this.#fillColorPicker.getValue());
                    break;
                case this.#imageRadioBtn:
                    this.#renderPreviewImage();
                    break;
            }
        }
    }

    /**
     * Generates and returns HTML markup string for color slider.
     *
     * @returns {JQuery}
     */
    #generateColorSlider() {
        this.#sliderOverlay = $('<div class="slider-overlay">').append('<div class="slider-pointer">');
        this.#sliderRail = $('<div class="slider-rail">').append(this.#sliderOverlay);
        return $('<div class="slider-container" tabindex="0">').append(this.#sliderRail);
    }

    /**
     * Set alpha with corresponding attributes to drawing background as preview.
     */
    #setAlphaPreview() {

        this.#previewDrawingInfos.forEach(previewDrawingInfo => {

            const attrs = previewDrawingInfo.attrs;
            const drawing = previewDrawingInfo.drawing;
            const fill = attrs && attrs.fill;

            if (!fill) { return false; }

            const percent = this.#transpSpinner?.fieldValue || 0;
            if (fill.type === 'solid' || fill.type === 'scheme') {
                fill.color = Color.parseJSON(fill.color).alpha((100 - percent) * 1000).toJSON();
            } else if (fill.type === 'bitmap' && fill.bitmap) {
                fill.bitmap.transparency = percent / 100;
            }

            updateFormatting(this.docApp, drawing, attrs);
        });
    }

    /**
     * Set the alpha value to the drawing background debounced (DOCS-3874).
     */
    @debounceAfterActionsMethod({ delay: 40 })
    _setAlphaPreviewDebounced() {
        this.#setAlphaPreview();
    }

    #setLineForPreview(drawing, attrs, preset) {

        if (!attrs) { return false; }

        const oldLineProps = attrs.line || {};
        const newLineProps = resolvePresetBorder(preset);

        for (const key in _.copy(newLineProps, true)) {
            oldLineProps[key] = newLineProps[key];
        }

        updateFormatting(this.docApp, drawing, attrs);
    }

    #setArrowsForPreview(drawing, attrs, preset) {

        const oldLineProps = attrs.line || {};
        const newLineProps = {};
        const endings = preset.split(':');
        const head = (endings[0] !== 'none') ? endings[0] : null;
        const tail = (endings[1] !== 'none') ? endings[1] : null;

        if (oldLineProps.headEndType !== head) { newLineProps.headEndType = head; }
        if (oldLineProps.tailEndType !== tail) { newLineProps.tailEndType = tail; }

        if (!_.isEmpty(newLineProps)) {
            for (const key in _.copy(newLineProps, true)) {
                oldLineProps[key] = newLineProps[key];
            }

            updateFormatting(this.docApp, drawing, attrs);
        }
    }

    #updateSliderValue(value) {
        this.#sliderOverlay.css('width', value + '%');
    }

    /**
     * Set value to the slider, corresponding percentage spinner, and make
     * preview of transparency.
     *
     * @param {number} percent
     */
    #setTransparency(percent) {

        percent = math.clamp(Math.round(percent), 0, 100);
        this.#sliderOverlay.css('width', percent + '%');
        this.#transpSpinner.setValue(percent);

        // debounced live preview of all selected drawings
        this._setAlphaPreviewDebounced();
    }

    #renderPreviewImage() {

        if (!this.#previewImageUrl) { return; }

        this.#previewDrawingInfos.forEach(previewDrawingInfo => {

            const type = this.#selection.getDrawingType(previewDrawingInfo.drawing);
            if (type === 'connector' || type === 'table') { return; }

            const fillAttrs = previewDrawingInfo.attrs.fill;
            fillAttrs.type = 'bitmap';
            fillAttrs.color = Color.AUTO;
            fillAttrs.bitmap = fillAttrs.bitmap || {};
            fillAttrs.bitmap.imageUrl = this.#previewImageUrl;
        });

        this.#setAlphaPreview();
    }

    /**
     * Initialize controls of the dialog.
     */
    #initControls() {

        const { line, fill } = this.#attrSet;

        const lineColorBlock = '<div class="line-color-radio">' + gt('Color') + '</div>';
        const lineColorAreaRow = $('<div class="row drawing-format-dialog__row"><div class="col-sm-4">' + lineColorBlock + '</div></div>');
        const lineColorRadioArea = $('<div class="col-sm-8">').append(this.#lineColorPicker.$el);
        lineColorAreaRow.append(lineColorRadioArea);
        this.#$controlAreas.append(lineColorAreaRow);

        const lineStyleBlock = '<div class="style-radio">' + gt('Style') + '</div>';
        const lineStyleAreaRow = $('<div class="row drawing-format-dialog__row"><div class="col-sm-4">' + lineStyleBlock + '</div></div>');
        const lineStyleRadioArea = $('<div class="col-sm-8"><div class="style-area"></div></div>');
        lineStyleRadioArea.find('.style-area').append(this.#lineStylePicker.$el);
        lineStyleAreaRow.append(lineStyleRadioArea);
        this.#$controlAreas.append(lineStyleAreaRow);

        if (this.#drawingTypeCount.connector) {
            const lineArrowBlock = '<div class="arrow-radio">' + gt('Arrow') + '</div>';
            const lineArrowAreaRow = $('<div class="row drawing-format-dialog__row"><div class="col-sm-4">' + lineArrowBlock + '</div></div>');
            const lineArrowRadioArea = $('<div class="col-sm-8"><div class="arrow-area"></div></div>');
            lineArrowRadioArea.find('.arrow-area').append(this.#lineArrowPicker.$el);
            lineArrowAreaRow.append(lineArrowRadioArea);
            this.#$controlAreas.append(lineArrowAreaRow);
        }

        if (line?.color) {
            const lineColorToSet = JSON.parse(JSON.stringify(line.color));
            const lineStyleToSet = JSON.parse(JSON.stringify(getPresetBorder(line)));
            const lineArrowToSet = JSON.parse(JSON.stringify(getPresetArrow(line)));
            this.#lineColorPicker.setValue(lineColorToSet);
            this.#lineStylePicker.setValue(lineStyleToSet);
            this.#lineArrowPicker.setValue(lineArrowToSet);
        }

        if (this.#isFill) {

            this.#$controlAreas.append(
                '<hr class="row">',
                $('<h5 class="h5 drawing-format-dialog__area-title">').text(gt('Fill'))
            );

            // "no fill" radio section
            this.#noBgRadioBtn = createRadioButtonNode({ classes: 'no-bg-radio', label: gt('No background') });
            setButtonKeyHandler(this.#noBgRadioBtn);
            const fillNoBgAreaRow = $('<div class="row drawing-format-dialog__row">').append($('<div class="col-xs-12">').append(this.#noBgRadioBtn));

            // "fill color" radio section
            this.#colorRadioBtn = createRadioButtonNode({ classes: 'color-radio', label: gt('Color') });
            setButtonKeyHandler(this.#colorRadioBtn);
            const fillColorAreaRow = $('<div class="row drawing-format-dialog__row">').append(
                $('<div class="col-sm-4">').append(this.#colorRadioBtn),
                $('<div class="col-sm-8">').append($('<div class="color-area">').append(this.#fillColorPicker.$el))
            );

            // "fill bitmap" radio section
            this.#imageRadioBtn = createRadioButtonNode({ classes: 'image-radio', label: gt('Image') });
            setButtonKeyHandler(this.#imageRadioBtn);
            const fillImageAreaRow = $('<div class="row drawing-format-dialog__row">').append(
                $('<div class="col-sm-4">').append(this.#imageRadioBtn),
                $('<div class="col-sm-8">').append($('<div class="image-area"></div>').append(this.#imagePicker.$el))
            );

            // transparency section
            this.#transpSlider = this.#generateColorSlider();
            this.#transpSpinner = new PercentField();
            this.#transpArea = $('<div class="transparency-area row">').append(
                $('<div class="drawing-format-dialog__transparency-label col-sm-4">').text(gt('Transparency')),
                this.#transpSlider,
                this.#transpSpinner.$el
            );

            // append all containers to control area
            this.#$controlAreas.append($('<div class="any-container">').append(fillNoBgAreaRow, fillColorAreaRow, fillImageAreaRow, this.#transpArea));

            // set initial fill color
            if (fill?.color) { this.#fillColorPicker.setValue(fill.color); }

            // store initial bitmap
            const firstDrawingWithImageBg = this.#previewDrawingInfos.find(previewDrawingInfo => previewDrawingInfo.attrs.fill?.type === 'bitmap');
            this.#previewImageUrl = firstDrawingWithImageBg?.attrs.fill.bitmap.imageUrl ?? null;

            // determine the active fill type
            let _activeRadioBtn;
            switch (fill?.type) {
                case 'none':   _activeRadioBtn = this.#noBgRadioBtn;  break;
                case 'solid':  _activeRadioBtn = this.#colorRadioBtn; break;
                case 'scheme': _activeRadioBtn = this.#colorRadioBtn; break;
                case 'bitmap': _activeRadioBtn = this.#imageRadioBtn; break;
            }
            this.#checkRadioButton(_activeRadioBtn, { silent: true });

            // set value to slider/spinner
            // the transparency value array of all the drawings
            let transpValues = this.#origDrawingInfos.map(origDrawingInfo => {
                const fillAttrs = origDrawingInfo?.attrs?.fill;
                switch (fillAttrs?.type) {
                    case 'solid':  return (fillAttrs?.color) ? 1 - Color.parseJSON(fillAttrs.color).resolveAlpha() : 0;
                    case 'bitmap': return fillAttrs?.bitmap?.transparency ?? 0;
                }
                return 0;
            });

            transpValues = _.unique(transpValues);

            const transparencyPercent = (transpValues.length === 1) ? Math.round(transpValues[0] * 100) : 0;

            this.#transpSpinner.setValue(transparencyPercent);
            this.#updateSliderValue(transparencyPercent);

            this.#noBgRadioBtn.on('click', () => this.#checkRadioButton(this.#noBgRadioBtn));
            this.#colorRadioBtn.on('click', () => this.#checkRadioButton(this.#colorRadioBtn));
            this.#imageRadioBtn.on('click', () => this.#checkRadioButton(this.#imageRadioBtn));

            const sliderTracker = event => {
                let offset = event.pageX;
                if (/^touch/.test(event.type)) {
                    const originalEvent = event.originalEvent;
                    const touch = originalEvent && originalEvent.touches && originalEvent.touches[0];
                    if (touch) { offset = touch.pageX; }
                }
                offset -= this.#sliderOverlay.offset().left;
                this.#setTransparency(offset * 100 / this.#sliderRail.width());
            };

            // transparency slider events handling
            this.#transpSlider.on(START_POINTER_EVENTS, event => {
                this.#slidingActive = true;
                sliderTracker(event);
                this.listenTo($(document.body), MOVE_POINTER_EVENTS, sliderTracker);
            });

            this.listenTo($(document.body), END_POINTER_EVENTS, () => {
                if (this.#slidingActive) {
                    this.stopListeningTo($(document.body), MOVE_POINTER_EVENTS);
                    this.#slidingActive = false;
                }
            });

            this.#transpSlider.on('keydown', event => {
                const percent = this.#transpSpinner.fieldValue;
                if (hasKeyCode(event, 'UP_ARROW', 'RIGHT_ARROW')) {
                    this.#setTransparency(percent + 1);
                } else if (hasKeyCode(event, 'DOWN_ARROW', 'LEFT_ARROW')) {
                    this.#setTransparency(percent - 1);
                }
            });

            this.#transpSpinner.on('group:commit', percent => {
                this.#setTransparency(percent);
            });
        }
    }
}
