/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { type SelectListEntry, CheckBox, SelectList } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import type { TextFrameworkDialogConfig } from "@/io.ox/office/textframework/view/labels";

import type PresentationView from "@/io.ox/office/presentation/view/view";

const { min } = Math;

// class InsertDateFieldDialog ================================================

/**
 * The "Insert Date Field" modal dialog. Provides localized date formats
 * available for inserting into a Presentation document.
 */
export class InsertDateFieldDialog extends BaseDialog<void, TextFrameworkDialogConfig> {

    // constructor ------------------------------------------------------------

    public constructor(docView: PresentationView) {

        // base constructor
        super({
            id: "io-ox-office-presentation-date-dialog",
            //#. Dialog title: Insert date/time field into document
            title: gt("Date and time"),
            width: 400,
            blockModelProcessing: true
        });

        // helper function to create an entry for the <select> list
        const { numberFormatter } = docView.docModel;
        const createEntry = (value: string): SelectListEntry<string> => {
            const label = _.noI18n(numberFormatter.formatNow(value) || "");
            return { value, label };
        };

        // generate the entries for the <select> list
        const entries = numberFormatter.getCategoryCodes(FormatCategory.DATE)?.map(createEntry);

        // default entries for the unlikely case that no date format codes are available
        if (!entries.length) {
            entries.push(createEntry(LOCALE_DATA.shortDate), createEntry(LOCALE_DATA.longDate));
        }

        // create a <select> list element
        const selectList = new SelectList(entries, entries[0].value, {
            classes: "date-field-format",
            size: min(13, entries.length)
        });
        //#. 'Insert Date Field' dialog: label for a list box containing date field formats
        this.renderControl(this.bodyNode, selectList, { label: gt.pgettext("field-dialog", "Select format") });

        // create a checkbox for automatic/fixed date mode
        const autoCheckBox = new CheckBox(false, {
            classes: "auto-update-field",
            style: { marginBlockStart: "0.5em" },
            label: gt("Update automatically")
        });
        this.renderControl(this.bodyNode, autoCheckBox);

        // set handler for the OK button
        this.setOkHandler(() => {
            const formatIndex = selectList.index + 1; // DOCS-4073, 1-based index
            const options = { fieldFormat: selectList.value, automatic: autoCheckBox.value, formatIndex };
            const category = docView.docApp.isODF() ? "date" : "datetime";
            // Not "return docView.docModel.getFieldManager().insertField(category, options);" because of problematic typing for getFieldManager()
            return docView.docModel.slideFieldManager.insertField(category, options);
        });
    }
}
