/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math, json, jpromise } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, IOS_SAFARI_DEVICE, hasKeyCode } from '@/io.ox/office/tk/dom';
import { BaseDialog } from '@/io.ox/office/tk/dialogs';
import { DISABLED_CLASS, createCheckboxNode, createRadioButtonNode, checkButtonNodes, isCheckedButtonNode, setButtonKeyHandler } from '@/io.ox/office/tk/forms';

import { Color } from '@/io.ox/office/editframework/utils/color';

import { getTempSlideNodeForSlide } from '@/io.ox/office/textframework/utils/dom';
import { getEmptySlideBackgroundAttributes } from '@/io.ox/office/textframework/utils/textutils';
import { ColorPicker, ImagePicker, PercentField } from '@/io.ox/office/textframework/view/controls';

import { showInsertImageDialog } from '@/io.ox/office/drawinglayer/view/drawingdialogs';

import '@/io.ox/office/presentation/view/dialog/presentationdialogs.less';

// constants ==============================================================

// constants for pointer events
const START_POINTER_EVENTS = IOS_SAFARI_DEVICE ? 'touchstart' : 'mousedown touchstart';
const MOVE_POINTER_EVENTS = IOS_SAFARI_DEVICE ? 'touchmove' : 'mousemove touchmove';
const END_POINTER_EVENTS = IOS_SAFARI_DEVICE ? 'touchend' : 'mouseup touchend';

// class SlideBackgroundDialog ============================================

/**
 * The slide background dialog.
 * Provides different types and options for slide backgrounds for Presentation document.
 *
 *
 * The changes will be applied to the current document after clicking the 'Ok' button.
 *
 * The dialog itself is shown by calling this.execute. The caller can react on successful
 * finalization of the dialog within an appropriate done-Handler, chained to the
 * this.execute function call.
 *
 * @param {PresentationView} docView
 *  The document view containing this dialog instance.
 *
 * @param {Object} [dialogOptions]
 *  Options for filling the dialog
 *  @param {Object} [dialogOptions.color]
 *      If background is already set as type solid color.
 *  @param {String} [dialogOptions.type]
 *      Type of background.
 */
export class SlideBackgroundDialog extends BaseDialog {

    #docView;
    #docModel;
    #dialogOptions;
    #slideId;
    #$slide;
    #colorPicker;
    #imagePicker;

    #transpArea;
    #transpSlider;
    #sliderRail;
    #sliderOverlay;
    #transpSpinner;
    #$tempPreviewSlide;

    #$controlArea;
    #hideBgCheckbox;
    #noBgRadioBtn;
    #colorRadioBtn;
    #imageRadioBtn;
    #activeRadioBtn;

    #previewImageUrl;
    #currentBitmapAttrs;

    constructor(docView, dialogOptions) {

        // base constructor
        super({
            id: 'io-ox-office-presentation-slide-background-dialog',
            title: /*#. Dialog title: Change slide background in a presentation document */ gt('Background'),
            okLabel: gt('Apply'),
            movable: true,
            focus: '[role="radio"][data-checked="true"]',
            blockModelProcessing: true
        });

        this.#docView = docView;
        this.#docModel = this.#docView.docModel;
        this.#dialogOptions = dialogOptions;
        this.#slideId = this.#docModel.getActiveSlideId();
        this.#$slide = this.#docModel.getSlideById(this.#slideId);

        this.#colorPicker = this.member(new ColorPicker(this.#docView, { icon: 'png:color', tooltip: null, title: null }));
        this.#imagePicker = this.member(new ImagePicker({ label: gt('Choose'), splitValue: null, hideUrl: true }));

        let slidingActive = false;

        // initialization -----------------------------------------------------

        // store deep cloned original values in node data
        this.#updatePreviewData({ fill: this.#dialogOptions ? json.deepClone(this.#dialogOptions) : getEmptySlideBackgroundAttributes().fill, slide: { followMasterShapes: this.#docModel.isFollowMasterShapeSlide() } });

        this.#$controlArea = $('<div class="control-area">');
        this.#$tempPreviewSlide = this.#docModel.getHelpBackgroundNode(this.#$slide);

        // initialize the body element of the dialog
        this.$bodyNode
            .toggleClass('mobile', !!SMALL_DEVICE)
            .append(this.#$controlArea);

        // create the layout of the dialog
        this.#initControls();

        if (this.#dialogOptions && this.#dialogOptions.type !== 'none' && this.#dialogOptions.type !== null) {
            this.#docModel.slideStyles.updateBackground(this.#$tempPreviewSlide, { fill: json.deepClone(this.#dialogOptions) || { type: null } }, this.#slideId, null, { preview: true, imageLoad: true, onDialogOpen: true });
        }

        // add additional button
        this.createActionButton('applyall', gt('Apply to all'), { buttonStyle: 'primary' });

        // handler for the OK button
        this.setOkHandler(() => {
            const startOptions = _.extend({ fill: this.#dialogOptions }, { slide: { followMasterShapes: this.#docModel.isFollowMasterShapeSlide() } });
            let endOptions = this.#getPreviewData() || {};
            endOptions = _.extend({ slide: { followMasterShapes: this.#docModel.isFollowMasterShapeSlide() } }, endOptions);
            return _.isEqual(startOptions, endOptions) ?
                this.#docModel.removeHelpBackgroundNode(this.#$slide) :
                this.#docModel.applySlideBackgroundOperations(this.#$slide);
        });

        // handler for the "Apply All" button
        this.setActionHandler('applyall', () => {
            this.#docModel.slideStyles.updateBackground(this.#$tempPreviewSlide, { fill: { type: null } }, this.#slideId, null, { preview: true });
            this.#docModel.removeHelpBackgroundNode(this.#$slide);
            return this.#docModel.applySlideBackgroundOperations(this.#$slide, { allSlides: true });
        });

        // on cancel restore original values
        this.setCancelHandler(() => {
            const attrs = this.#dialogOptions || { type: null };
            this.#docModel.slideStyles.updateBackground(this.#$tempPreviewSlide, { fill: attrs }, this.#slideId, null, { preview: true });
            this.#docView.previewFollowMasterPageChange({ followMasterShapes: this.#docModel.isFollowMasterShapeSlide() });
            this.#docModel.removeHelpBackgroundNode(this.#$slide);
            this.#docView.handleSlideBackgroundVisibility(this.#slideId);
        });

        // listen on checkbox to change icon
        this.#hideBgCheckbox.on('click', event => {
            const node = event.currentTarget;
            checkButtonNodes(node, !isCheckedButtonNode(node));
            this.#updatePreviewData({ slide: { followMasterShapes: !isCheckedButtonNode(node) } });
            this.#docView.previewFollowMasterPageChange({ followMasterShapes: !isCheckedButtonNode(node) });
        });

        this.#noBgRadioBtn.on('click', () => this.#checkRadioButton(this.#noBgRadioBtn));
        this.#colorRadioBtn.on('click', () => this.#checkRadioButton(this.#colorRadioBtn));
        this.#imageRadioBtn.on('click', () => this.#checkRadioButton(this.#imageRadioBtn));

        const sliderTracker = event => {
            let offset = event.pageX;
            if (/^touch/.test(event.type)) {
                const originalEvent = event.originalEvent;
                const touch = originalEvent && originalEvent.touches && originalEvent.touches[0];
                if (touch) { offset = touch.pageX; }
            }
            offset -= this.#sliderOverlay.offset().left;
            this.#setTransparency(offset * 100 / this.#sliderRail.width());
        };

        // transparency slider events handling
        this.#transpSlider.on(START_POINTER_EVENTS, event => {
            slidingActive = true;
            sliderTracker(event);
            this.listenTo($(document.body), MOVE_POINTER_EVENTS, sliderTracker);
        });

        this.listenTo($(document.body), END_POINTER_EVENTS, () => {
            if (slidingActive) {
                this.stopListeningTo($(document.body), MOVE_POINTER_EVENTS);
                slidingActive = false;
            }
        });

        this.#transpSlider.on('keydown', event => {
            const percent = this.#transpSpinner.fieldValue;
            if (hasKeyCode(event, 'UP_ARROW', 'RIGHT_ARROW')) {
                this.#setTransparency(percent + 1);
            } else if (hasKeyCode(event, 'DOWN_ARROW', 'LEFT_ARROW')) {
                this.#setTransparency(percent - 1);
            }
        });

        this.#transpSpinner.on('group:commit', percent => {
            this.#setTransparency(percent);
        });

        // color picker event handling
        this.#colorPicker.on('group:commit', () => {
            this.#colorPicker.menu.hide();
            this.#checkRadioButton(this.#colorRadioBtn, { force: true });
        });

        // background image picker event handling
        this.#imagePicker.on("group:commit", sourceType => {
            jpromise.floating(showInsertImageDialog(this.#docView, sourceType, imageDesc => {
                if (this.#previewImageUrl !== imageDesc.url) {
                    this.#previewImageUrl = imageDesc.url;
                    this.#checkRadioButton(this.#imageRadioBtn, { force: true });
                }
            }));
        });

        // automatically check the correct radio button already when opening the dropdown menus
        this.#colorPicker.menu.on("popup:show", () => this.#checkRadioButton(this.#colorRadioBtn));
        this.#imagePicker.menu.on("popup:show", () => this.#checkRadioButton(this.#imageRadioBtn));

        this.on('close', () => {
            this.#clearPreviewData();
            this.#docModel.removeHelpBackgroundNode(this.#$slide);
        });
    }

    // private methods ----------------------------------------------------

    /**
     * Updating the data object with the currently applied slide
     * background attributes.
     *
     * @param {Object} attrs
     *   Fill and slide attributes to set to slide background as preview.
     */
    #updatePreviewData(attrs) {
        const slideData = this.#$slide.data('previewAttrs') || {};
        this.#$slide.data('previewAttrs', _.extend(slideData, attrs));
    }

    /**
     * Getting the data object with the currently applied slide
     * background attributes.
     *
     * @returns {Object}
     *   Fill and slide attributes to set to slide background as preview.
     */
    #getPreviewData() {
        return this.#$slide.data('previewAttrs');
    }

    /**
     * Removing the data object with the currently applied slide
     * background attributes.
     */
    #clearPreviewData() {
        this.#$slide.removeData('previewAttrs');
    }

    /**
     * Helper function for enabling or disabling the specified DOM elements.
     *
     * @param {jQuery} nodes
     *  The DOM elements to be manipulated. If this object is a jQuery
     *  collection, uses all nodes it contains.
     *
     * @param {Boolean} state
     *  Whether to enable (true) or disable (false) the DOM elements.
     */
    #enableNodes(nodes, state) {
        nodes.toggleClass(DISABLED_CLASS, !state);
        if (state) {
            nodes.removeAttr('aria-disabled').prop('tabindex', 0);
        } else {
            nodes.attr('aria-disabled', true).prop('tabindex', -1);
        }
    }

    #checkRadioButton(radioBtn, options) {

        // whether active radio button changes (filter clicks on same radio button)
        const changed = this.#activeRadioBtn !== radioBtn;

        // update radio button selection, and enabled states of dependent controls
        if (changed) {

            if (this.#activeRadioBtn) { checkButtonNodes(this.#activeRadioBtn, false); }
            checkButtonNodes(radioBtn, true);
            this.#activeRadioBtn = radioBtn;

            const isNoFill = radioBtn === this.#noBgRadioBtn;
            this.#transpArea.toggleClass("disabled", isNoFill);
            this.#enableNodes(this.#transpArea.find(".slider-container, input"), !isNoFill);
            this.#transpSpinner.enable(!isNoFill);
        }

        // update live preview in document
        if (!options?.silent && (changed || options?.force)) {
            switch (radioBtn) {
                case this.#noBgRadioBtn:
                    this.#applyNoFill();
                    break;
                case this.#colorRadioBtn:
                    this.#applyFillColor(this.#colorPicker.getValue());
                    break;
                case this.#imageRadioBtn:
                    this.#renderPreviewImage(this.#previewImageUrl);
                    if (this.#currentBitmapAttrs) {
                        this.#setAlphaPreview({ fill: json.deepClone(this.#currentBitmapAttrs) }, true);
                    }
                    break;
            }
        }
    }

    #applyNoFill() {
        const attrs = getEmptySlideBackgroundAttributes();
        this.#docView.handleSlideBackgroundVisibility(this.#slideId, attrs.fill);
        this.#docModel.slideStyles.updateBackground(this.#$tempPreviewSlide, attrs, this.#slideId, null, { preview: true });
        this.#updatePreviewData(attrs);
    }

    // helper function to apply the fill color
    #applyFillColor(jsonColor) {
        let attrs;
        if (Color.parseJSON(jsonColor).isAuto()) {
            attrs = getEmptySlideBackgroundAttributes();
        } else {
            attrs = { fill: { type: 'solid', color: jsonColor } };
            if (this.#dialogOptions) {
                if (this.#dialogOptions.gradient) { attrs.fill.gradient = null; }
                if (this.#dialogOptions.bitmap?.imageUrl) { attrs.fill.bitmap = { imageUrl: null }; }
            }
        }

        this.#docView.handleSlideBackgroundVisibility(this.#slideId, attrs.fill);
        this.#setAlphaPreview(attrs);
    }

    /**
     * Sets a preview image as slide background (without document
     * operations).
     *
     * @returns {Promise<void> | void} // TODO: check-void-return
     *  A promise that will be resolved or rejected after image has been
     *  inserted into the document.
     */
    #renderPreviewImage() {

        if (!this.#previewImageUrl) { return; }

        const activeSlideId = this.#docModel.getActiveSlideId();
        const  _$slide = this.#docModel.getSlideById(activeSlideId); // TODO: Why not using this.#$slide ?
        const attrs = { type: 'bitmap', bitmap: { imageUrl: this.#previewImageUrl } };
        const oldAttrs = this.#getPreviewData();

        if (oldAttrs && oldAttrs.fill && oldAttrs.fill.gradient) { attrs.gradient = null; }

        return this.#docModel.getImageSize(this.#previewImageUrl).then(size => {
            // the ratio of width and height of the slide
            const slideRatio = this.#docModel.getSlideRatio();
            // the ratio of width and height of the drawing
            const drawingRatio = math.roundp(size.width / size.height, 0.01);
            // stretching ratio
            let stretchingRatio = 1;

            if (slideRatio > drawingRatio) {
            // the drawing needs to be cropped at top and bottom -> height need to be increased
                stretchingRatio = Math.round((1 - slideRatio / drawingRatio) * 50);
                attrs.bitmap.stretching = { top: stretchingRatio, bottom: stretchingRatio, left: 0, right: 0 };
            } else {
                // the drawing needs to be cropped at left and right sides -> width need to be increased
                stretchingRatio = Math.round((1 - drawingRatio / slideRatio) * 50);
                attrs.bitmap.stretching = { top: 0, bottom: 0, left: stretchingRatio, right: stretchingRatio };
            }

            const transp = this.#transpSpinner.getValue();
            if (transp) { attrs.bitmap.transparency = transp / 100; }

            return this.#docModel.slideStyles.updateBackground(getTempSlideNodeForSlide(_$slide), { fill: attrs }, activeSlideId, null, { imageLoad: true, preview: true }).then(() => {
                this.#docView.handleSlideBackgroundVisibility(activeSlideId, attrs);
                // store values for apply
                this.#updatePreviewData({ fill: attrs });
                this.#currentBitmapAttrs = attrs;
            });
        }).fail(() => {
            this.#docView.docApp.rejectEditAttempt('image');
        });
    }

    /**
     * Generates and returns HTML markup string for color slider.
     *
     * @returns {JQuery}
     */
    #generateColorSlider() {
        this.#sliderOverlay = $('<div class="slider-overlay">').append('<div class="slider-pointer">');
        this.#sliderRail = $('<div class="slider-rail">').append(this.#sliderOverlay);
        return $('<div class="slider-container" tabindex="0">').append(this.#sliderRail);
    }

    /**
     * Set alpha with corresponding attributes to slide background as preview.
     *
     * @param {Object} attrs
     *   Fill attributes to set to slide background as preview.
     *
     * @param {boolean} [forceImageLoad=false]
     *   Whether the background image needs to be loaded.
     */
    #setAlphaPreview(attrs, forceImageLoad) {
        let inheritFromParent = false;
        let imageLoad = false;
        const percent = this.#transpSpinner.fieldValue || 0;

        if (attrs && attrs.fill && (attrs.fill.type === null || attrs.fill.type === 'none')) { // inherit fill from parent
            let parentId = this.#docModel.getParentSlideId(this.#slideId);
            let parentFillAttrs = this.#docModel.getSlideAttributesByFamily(parentId, 'fill');
            if (!parentFillAttrs || parentFillAttrs.type === 'none' || parentFillAttrs.type === null) {
                parentId = this.#docModel.getParentSlideId(parentId);
                parentFillAttrs = this.#docModel.getSlideAttributesByFamily(parentId, 'fill');
            }
            attrs = { fill: json.deepClone(parentFillAttrs) };
            if (parentFillAttrs && parentFillAttrs.color && !parentFillAttrs.bitmap) {
                this.#colorPicker.setValue(parentFillAttrs.color);
            }
            inheritFromParent = true;
            imageLoad = true;
        }
        if (attrs && attrs.fill && (attrs.fill.type === 'solid' || attrs.fill.type === 'scheme')) {
            attrs.fill.color = Color.parseJSON(attrs.fill.color).alpha((100 - percent) * 1000).toJSON();
            this.#docModel.slideStyles.updateBackground(this.#$tempPreviewSlide, attrs, this.#slideId, null, { preview: true });
            if (inheritFromParent) {
                this.#docView.handleSlideBackgroundVisibility(this.#slideId, attrs.fill);
            }
            // store values for apply
            this.#updatePreviewData(attrs);
        }
        if (attrs && attrs.fill && attrs.fill.type === 'bitmap' && attrs.fill.bitmap) {
            attrs.fill.bitmap.transparency = percent / 100;

            if (forceImageLoad) { imageLoad = true; }
            this.#docModel.slideStyles.updateBackground(this.#$tempPreviewSlide, attrs, this.#slideId, null, { imageLoad, preview: true });
            if (inheritFromParent) {
                this.#docView.handleSlideBackgroundVisibility(this.#slideId, attrs.fill);
            }
            // store values for apply
            this.#updatePreviewData(attrs);
        }
    }

    /**
     * Set value to the slider, corresponding percentage spinner, and make
     * preview of transparency.
     *
     * @param {number} percent
     */
    #setTransparency(percent) {
        percent = math.clamp(Math.round(percent), 0, 100);
        this.#transpSpinner.setValue(percent);
        this.#updateSliderValue(percent);
        this.#setAlphaPreview(this.#getPreviewData());
    }

    /**
     * Only updating the value of the slider corresponding to the percentage spinner.
     *
     * @param {number} percent
     */
    #updateSliderValue(percent) {
        percent = math.clamp(Math.round(percent), 0, 100);
        this.#sliderOverlay.css('width', percent + '%');
    }
    /**
     * Initialize controls of the dialog.
     */
    #initControls() {

        // "no fill" radio section
        this.#noBgRadioBtn = createRadioButtonNode({ classes: 'no-bg-radio', label: gt('No background') });
        setButtonKeyHandler(this.#noBgRadioBtn);
        const noBgRadioArea = $('<div class="dradio">').append(this.#noBgRadioBtn);

        // "fill color" radio section
        this.#colorRadioBtn = createRadioButtonNode({ classes: 'color-radio', label: gt('Color') });
        setButtonKeyHandler(this.#colorRadioBtn);
        const colorRadioArea = $('<div class="dradio">').append(this.#colorRadioBtn);
        const colorArea = $('<div class="color-area">').append(this.#colorPicker.$el);

        // "fill bitmap" radio section
        this.#imageRadioBtn = createRadioButtonNode({ classes: 'image-radio', label: gt('Image') });
        setButtonKeyHandler(this.#imageRadioBtn);
        const imageRadioArea = $('<div class="dradio">').append(this.#imageRadioBtn);
        const imageArea = $('<div class="image-area">').append(this.#imagePicker.$el);

        // transparency section
        this.#transpSlider = this.#generateColorSlider();
        this.#transpSpinner = this.member(new PercentField());
        this.#transpArea = $('<div class="transparency-area">').append(
            $('<div class="transparency-label">').text(gt('Transparency')),
            this.#transpSlider,
            this.#transpSpinner.$el
        );

        // "Hide background" checkbox
        this.#hideBgCheckbox = createCheckboxNode({ classes: 'hide-bg-btn', label: gt('Hide background graphics') });
        const hideBgContainer = $('<div class="any-container">').append(this.#hideBgCheckbox);
        this.#enableNodes(this.#hideBgCheckbox, !this.#docModel.isMasterSlideId(this.#docModel.getActiveSlideId()));
        checkButtonNodes(this.#hideBgCheckbox, !this.#docModel.isFollowMasterShapeSlide());
        setButtonKeyHandler(this.#hideBgCheckbox);

        // determine the active fill type
        let _activeRadioBtn = this.#noBgRadioBtn;
        switch (this.#dialogOptions?.type) {
            case 'none':   _activeRadioBtn = this.#noBgRadioBtn;  break;
            case 'solid':  _activeRadioBtn = this.#colorRadioBtn; break;
            case 'scheme': _activeRadioBtn = this.#colorRadioBtn; break;
            case 'bitmap': _activeRadioBtn = this.#imageRadioBtn; break;
        }

        // append all containers to control area (don't append transparency area if app is ODF)
        const modifyContainer = $('<div class="any-container">').append(noBgRadioArea, colorRadioArea, colorArea, imageRadioArea, imageArea);
        if (!this.#docModel.docApp.isODF()) { modifyContainer.append(this.#transpArea); }
        this.#$controlArea.append(modifyContainer, hideBgContainer);

        // set initial fill color
        if (this.#dialogOptions?.color) {
            this.#colorPicker.setValue(this.#dialogOptions.color);
        }

        // set value to slider/spinner
        let transpValue = 0;
        if (this.#dialogOptions?.type && (this.#dialogOptions.type !== 'none')) {
            if (_activeRadioBtn === this.#colorRadioBtn) {
                transpValue = Math.round((1 - Color.parseJSON(this.#dialogOptions.color).resolveAlpha()) * 100);
            } else if (_activeRadioBtn === this.#imageRadioBtn) {
                if (this.#dialogOptions.bitmap?.transparency) {
                    transpValue = this.#dialogOptions.bitmap.transparency * 100;
                }
                this.#currentBitmapAttrs = json.deepClone(this.#dialogOptions);
            }
        } else { // check if bitmap is inherited from layout or master slide, and enable proper buttons
            let parentId = this.#docModel.getParentSlideId(this.#slideId);
            let inheritedFill = false;
            let parentFillAttrs = null;

            parentFillAttrs = this.#docModel.getSlideAttributesByFamily(parentId, 'fill');
            if (!parentFillAttrs || parentFillAttrs.type === 'none' || parentFillAttrs.type === null) {
                parentId = this.#docModel.getParentSlideId(parentId);
                parentFillAttrs = this.#docModel.getSlideAttributesByFamily(parentId, 'fill');
            }
            if (parentFillAttrs?.type === 'bitmap') {
                _activeRadioBtn = this.#imageRadioBtn;
                transpValue = (parentFillAttrs.bitmap.transparency || 0) * 100;
                inheritedFill = true;
            } else if (parentFillAttrs && (parentFillAttrs.type === 'solid' || parentFillAttrs.type === 'scheme')) {
                _activeRadioBtn = this.#colorRadioBtn;
                transpValue = Math.round((1 - Color.parseJSON(parentFillAttrs.color).resolveAlpha()) * 100);
                if (parentFillAttrs.color) { this.#colorPicker.setValue(parentFillAttrs.color); }
                inheritedFill = true;
            }
            // an update of preview data is necessary also for inherited fill attributes. Otherwise an "Apply to all"
            // without any changes in this dialog, sets the empty background to the master slide.
            if (inheritedFill) { this.#updatePreviewData({ fill: parentFillAttrs }); }
        }

        this.#checkRadioButton(_activeRadioBtn, { silent: true });
        this.#transpSpinner.setValue(transpValue);
        this.#updateSliderValue(transpValue);
    }
}
