/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { SMALL_DEVICE } from '@/io.ox/office/tk/utils';
import { DISABLED_CLASS, createCheckboxNode, createRadioButtonNode, checkButtonNodes, createInputNode, createSelectListNode,
    isCheckedButtonNode, setButtonKeyHandler } from '@/io.ox/office/tk/forms';
import { OK_ACTION, BaseDialog } from '@/io.ox/office/tk/dialogs';
import { LOCALE_DATA } from '@/io.ox/office/tk/locale';

import '@/io.ox/office/presentation/view/dialog/presentationdialogs.less';

// class InsertFooterDialog ===============================================

/**
 * The insert date field modal dialog.
 * Provides localized date formats available for inserting into Presentation document.
 *
 *
 * The changes will be applied to the current document after clicking the 'Ok' button.
 *
 * The dialog itself is shown by calling this.execute. The caller can react on successful
 * finalization of the dialog within an appropriate done-Handler, chained to the
 * this.execute function call.
 *
 * @param {PresentationView} docView
 *  The view instance containing this dialog.
 *
 * @param {Array} [dialogOptions]
 *  Options for filling the dialog
 *  - {Boolean} [dialogOptions.ftr]
 *      Footer placeholder exists on slide
 *  - {String} [dialogOptions.ftrText]
 *      Text inside footer placeholder
 *  - {Boolean} [dialogOptions.sldNum]
 *      Slide number placeholder exists on slide
 *  - {Boolean} [dialogOptions.dt]
 *      Datetime placeholder exists on slide
 *  - {String} [dialogOptions.dtType]
 *      Datetime placeholder contains date field and has this field type.
 *  - {String} [dialogOptions.dtText]
 *      Datetime placeholder contains fixed date as text.
 */
export class InsertFooterDialog extends BaseDialog {

    constructor(docView, dialogOptions) {

        // base constructor
        super({
            id: 'io-ox-office-presentation-footer-dialog',
            width: 400,
            title: /*#. Dialog title: Insert field into document footer */ gt('Footer'),
            okLabel: gt('Apply'),
            blockModelProcessing: true
        });

        var
            docModel = docView.docModel,
            numberFormatter = docModel.numberFormatter,
            // fetch locale date formats
            categoryCodesDate = numberFormatter.getCategoryCodes('date'),
            // whether this is an ODF application
            isODF = docModel.docApp.isODF(),
            // default fall back date option
            defaultDateOption = 'datetime1',
            $controlArea = this.$bodyNode,
            dateCheckbox = null,
            dateContainer = null,
            footerTxtContainer = null,
            slideNumCheckbox = null,
            footerTxtCheckbox = null,
            autoRadioBtn = null,
            fixedRadioBtn = null;

        // private methods ----------------------------------------------------

        /**
         * Helper function for enabling or disabling the specified DOM elements.
         *
         * @param {jQuery} nodes
         *  The DOM elements to be manipulated. If this object is a jQuery
         *  collection, uses all nodes it contains.
         *
         * @param {Boolean} state
         *  Whether to enable (true) or disable (false) the DOM elements.
         */
        function enableNodes(nodes, state) {
            nodes.toggleClass(DISABLED_CLASS, !state);
            if (state) {
                nodes.removeAttr('aria-disabled');
                nodes.prop('tabindex', 0);
            } else {
                nodes.attr('aria-disabled', true);
                nodes.prop('tabindex', -1);
            }
        }

        /**
         * Generate field values for selected value (date), and insert them into list.
         *
         * returns {JQuery} the select box
         */
        function createFormatsSelectList() {

            var localDateFormatList;
            if (categoryCodesDate.length) {
                localDateFormatList = categoryCodesDate.map(function (formatCode, index) {
                    var value = isODF ? formatCode : 'datetime' + (index + 1);
                    return { value, label: _.noI18n(numberFormatter.formatNow(formatCode)) };
                });
                if (isODF) { defaultDateOption = categoryCodesDate[0]; }
            } else {
                // resolve date formats of current locale (may change at runtime)
                var SHORT_DATE = LOCALE_DATA.shortDate;
                var LONG_DATE = LOCALE_DATA.longDate;
                localDateFormatList = [
                    { value: SHORT_DATE, label: _.noI18n(numberFormatter.formatNow(SHORT_DATE)) },
                    { value: isODF ? LONG_DATE : 'datetime1', label: _.noI18n(numberFormatter.formatNow(LONG_DATE)) }
                ];
                if (isODF) { defaultDateOption = SHORT_DATE; }
            }

            return createSelectListNode(localDateFormatList);
        }

        /**
         * Utility function for extracting object with given type from array of objects.
         *
         * @param {Array} optionsArray
         *  Array of objects.
         * @param {String} type
         *  Type of object which we are searching.
         * @returns {Object|undefined}
         */
        function extractByType(optionsArray, type) {
            return _.findWhere(optionsArray, { type });
        }

        /**
         * Initialize controls of the dialog.
         */
        function initControls() {
            var dateObject = extractByType(dialogOptions, 'dt');
            var isDate = !!dateObject;
            var isAutoDate = dateObject?.automatic || false;
            var slideNumObject = extractByType(dialogOptions, 'sldNum');
            var isSlideNum = !!slideNumObject;
            var footerObject = extractByType(dialogOptions, 'ftr');
            var isFooter = !!footerObject && !footerObject?.ftrInherited;
            var isAutoRadioEnabled = !isDate || isAutoDate;

            // date checkbox
            dateCheckbox = createCheckboxNode({ classes: 'date-time-btn', label: gt('Date and time') });
            checkButtonNodes(dateCheckbox, isDate);
            setButtonKeyHandler(dateCheckbox);

            // auto date radio
            autoRadioBtn = createRadioButtonNode({ classes: 'automatic-radio dradio', label: gt('Update automatically') });
            checkButtonNodes(autoRadioBtn, isAutoRadioEnabled);
            setButtonKeyHandler(autoRadioBtn);

            // fixed date radio
            fixedRadioBtn = createRadioButtonNode({ classes: 'fixed-radio dradio', label: gt('Fixed date') });
            checkButtonNodes(fixedRadioBtn, !isAutoRadioEnabled);
            setButtonKeyHandler(fixedRadioBtn);

            // fixed date text input
            var fixedDateInput = createInputNode({ attributes: { disabled: true } });

            // footer text checkbox
            footerTxtCheckbox = createCheckboxNode({ classes: 'footer-btn', label: gt('Footer') });
            checkButtonNodes(footerTxtCheckbox, isFooter);
            setButtonKeyHandler(footerTxtCheckbox);

            // footer text input field
            var footerTxtInput = createInputNode({ attributes: !isFooter ? { disabled: true } : {} });
            footerTxtInput.val(footerObject?.ftrText);

            // slide number checkbox
            slideNumCheckbox = createCheckboxNode({ classes: 'slide-number-btn', label: gt('Slide number') });
            checkButtonNodes(slideNumCheckbox, isSlideNum);
            setButtonKeyHandler(slideNumCheckbox);

            dateContainer = $('<div class="date-container">').append(autoRadioBtn, createFormatsSelectList(), fixedRadioBtn, fixedDateInput);
            // depending from auto or fixed date, disable select and input fields
            dateContainer.find('select').prop('disabled', !isDate || !isAutoDate).toggleClass('disabled', !isDate || !isAutoDate).val((dateObject && dateObject.dtType) || defaultDateOption);
            const dateVal = (isDate && !isAutoDate) ? dateObject?.dtText : dateContainer.find('option:selected').text();
            dateContainer.find('input').prop('disabled', !isDate || isAutoDate).toggleClass('disabled', !isDate || isAutoDate).val(dateVal);

            // if date checkbox is checked, enable auto and fixed radios
            enableNodes(dateContainer.find('.dradio'), isDate);

            // append all containers to control area
            footerTxtContainer = $('<div class="footertxt-container">').append(footerTxtCheckbox, footerTxtInput);
            var slideNumContainer = $('<div class="slidenum-container">').append(slideNumCheckbox);
            $controlArea.append(dateCheckbox, dateContainer, footerTxtContainer, slideNumContainer);
        }

        function actionHandler(action) {
            var selectedOption;
            var formatIndex;
            var obj = null;
            var checkedOptions = [];
            var diffContent;
            var diffState;
            var odfRepresentation = null;

            if (isCheckedButtonNode(dateCheckbox)) {
                if (isCheckedButtonNode(autoRadioBtn)) {
                    selectedOption = $controlArea.find('option:selected');
                    formatIndex = selectedOption.length ? (selectedOption.index() + 1) : null;
                    if (isODF) { odfRepresentation = categoryCodesDate[selectedOption.index()]; }
                    obj = { type: 'dt', automatic: true, formatIndex, representation: isODF ? (odfRepresentation || '') : selectedOption[0].text };
                } else {
                    obj = { type: 'dt', automatic: false, representation: dateContainer.find('input').val() };
                }
                checkedOptions.push(obj);
            }
            if (isCheckedButtonNode(slideNumCheckbox)) {
                checkedOptions.push({ type: 'sldNum' });
            }
            if (isCheckedButtonNode(footerTxtCheckbox)) {
                checkedOptions.push({ type: 'ftr', ftrText: footerTxtContainer.find('input').val() });
            }

            if (isODF) {
                return docModel.getFieldManager().insertFooterODF(checkedOptions, (action === 'applyall'));
            }

            switch (action) {
                case OK_ACTION:
                    diffContent = docModel.getFieldManager().diffInputContent(dialogOptions, checkedOptions);
                    diffState = docModel.getFieldManager().diffStateCheckboxes(dialogOptions, checkedOptions);
                    return docModel.getFieldManager().insertFooter(diffContent, diffState);
                case 'applyall':
                    return docModel.getFieldManager().insertAllFooters(checkedOptions);
            }
        }

        // initialization -----------------------------------------------------

        // initialize the body element of the dialog
        this.$bodyNode
            .toggleClass('mobile', !!SMALL_DEVICE)
            .append($controlArea);

        // create the layout of the dialog
        initControls();

        // handler for the OK button
        this.setOkHandler(actionHandler);

        // additional button in the dialog footer
        this.createActionButton('applyall', gt('Apply to all'), { buttonStyle: 'primary', handler: actionHandler });

        // listen on checkbox to change icon
        dateCheckbox.on('click', function () {
            checkButtonNodes(this, !isCheckedButtonNode(this));
            // enable/disable date subgroup
            enableNodes(dateContainer.find('.dradio'), isCheckedButtonNode(this));
            dateContainer.find('select, input').prop('disabled', !isCheckedButtonNode(this)).toggleClass('disabled', !isCheckedButtonNode(this));
            if (isCheckedButtonNode(autoRadioBtn)) {
                dateContainer.find('select').prop('disabled', !isCheckedButtonNode(this)).toggleClass('disabled', !isCheckedButtonNode(this));
                dateContainer.find('input').prop('disabled', true).addClass('disabled');
            } else {
                dateContainer.find('input').prop('disabled', !isCheckedButtonNode(this)).toggleClass('disabled', !isCheckedButtonNode(this));
                dateContainer.find('select').prop('disabled', true).addClass('disabled');
            }
        });
        slideNumCheckbox.on('click', function () {
            checkButtonNodes(this, !isCheckedButtonNode(this));
        });
        footerTxtCheckbox.on('click', function () {
            checkButtonNodes(this, !isCheckedButtonNode(this));
            // enable/disable footer textarea
            footerTxtContainer.find('input').prop('disabled', !isCheckedButtonNode(this));

        });

        autoRadioBtn.on('click', function () {
            checkButtonNodes(fixedRadioBtn, false);
            checkButtonNodes(this, true);
            dateContainer.find('select').prop('disabled', false).removeClass('disabled');
            dateContainer.find('input').prop('disabled', true).addClass('disabled');
        });

        fixedRadioBtn.on('click', function () {
            checkButtonNodes(autoRadioBtn, false);
            checkButtonNodes(this, true);
            dateContainer.find('input').prop('disabled', false).removeClass('disabled');
            dateContainer.find('select').prop('disabled', true).addClass('disabled');
        });
    }
}
