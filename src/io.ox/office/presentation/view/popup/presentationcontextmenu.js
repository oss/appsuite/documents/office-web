/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { REPLY_COMMENT_OPTIONS } from "@/io.ox/office/editframework/view/editlabels";
import { UniversalContextMenu } from "@/io.ox/office/textframework/view/popup/universalcontextmenu";
import { Button, ChangeLayoutSlidePicker } from "@/io.ox/office/presentation/view/controls";

// class PresentationContextMenu ==============================================

/**
 * A context menu specific for Presentation app.
 */
export class PresentationContextMenu extends UniversalContextMenu {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            delay: config?.delay ?? 200,
            ...config
        });
    }

    // public methods ---------------------------------------------------------

    prepareSlideMenu(payload) {

        const position = payload?.position;
        const mouseX = position?.mouseX ?? null;
        const mouseY = position?.mouseY ?? null;

        if ((mouseX !== null) && (mouseY !== null)) {
            this.docView.trigger("contextmenu:init:mouseposition", { mouseX, mouseY });
        }

        this.addControl("threadedcomment/insert/fromcontextmenu", new Button({ label: gt("Insert comment"), tooltip: null }));

        this.addControl("layoutslidepicker/changelayout", new ChangeLayoutSlidePicker(this.docView, {
            tooltip: null,
            label: gt("Change layout"),
            anchorBorder: ["right", "bottom", "top", "left"],
            // special handling for odf layout slides
            isOdfLayout: this.docApp.isODF()
        }));
        this.addControl("slide/setbackground", new Button({ label: gt("Set slide background"), tooltip: gt("Format slide background") }));

        this.addSection("start");
        this.addControl("present/startpresentation", new Button({ label: gt("Present"), tooltip: gt("Start the Presenter")  }));
    }

    /**
     * Prepares the context menu for presentation comment bubbles
     */
    preparePresentationCommentBubbleMenu() {
        this.addSection("reply");
        this.addControl("threadedcomment/insert/reply", new Button({ ...REPLY_COMMENT_OPTIONS, icon: null }));
        this.addSection("delete");
        this.addControl("threadedcomment/delete/thread", new Button({ label: gt("Delete"), tooltip: gt("Delete this thread") }));
    }
}
