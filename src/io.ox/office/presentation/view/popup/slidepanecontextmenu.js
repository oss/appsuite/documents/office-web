/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { getNodePositionInPage } from "@/io.ox/office/tk/utils";
import { setFocus } from "@/io.ox/office/tk/dom";
import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";
import { Button, ChangeLayoutSlidePicker } from "@/io.ox/office/presentation/view/controls";

// class SlidePaneContextMenu =================================================

/**
 * A context menu for the slide pane.
 *
 * @param {PresentationView} docView
 *  The presentation view containing this instance.
 */
export class SlidePaneContextMenu extends ContextMenu {

    constructor(docView, sourceNode) {

        // base constructor
        super(docView, sourceNode, {
            repaintAlways: true,
            delay: 200
        });

        // to keep the focus after pressing ESC to close the popup
        this.on("popup:hide", () => setFocus(sourceNode));

        // hide contextmenu if the user start scrolling
        this.listenTo(docView, "change:scrollpos", () => this.hide());
    }

    // protected methods ------------------------------------------------------

    /**
     * Preparations before the context menu will be shown.
     */
    /*protected override*/ implPrepareContext(sourceEvent, options) {

        // calculate the position right to the slide with 5px offset
        if (options?.stickToSlide) {
            var rect = getNodePositionInPage(this.$sourceNode.children(".selected").find(".slidePaneThumbnail"));
            sourceEvent.pageX = rect.left + rect.width + 5;
            sourceEvent.pageY = rect.top;
        }
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // remove all existing controls from context menu
        this.destroyAllControls();

        this.addSection("slide");
        const options = { focusTarget: this.$sourceNode };
        this.addControl("slide/insertslide",       new Button({ label: gt("Insert slide") }), options);
        this.addControl("slide/insertlayoutslide", new Button({ label: gt("Insert layout slide") }), options);
        this.addControl("slide/duplicateslides",   new Button({ label: gt("Duplicate slide") }), options);
        this.addControl("slide/deleteslide",       new Button({ label: gt("Delete slide") }), options);
        this.addControl("slide/hideslide",         new Button({ label: gt("Hide slide") }), options);
        this.addControl("slide/unhideslide",       new Button({ label: gt("Unhide slide") }), options);

        this.addSection("layout");
        this.addControl("layoutslidepicker/changelayout", new ChangeLayoutSlidePicker(this.docView, {
            tooltip: null,
            label: gt("Change layout"),
            anchorBorder: ["right", "bottom", "top", "left"],
            // speicial handling for odf layout slides
            isOdfLayout: this.docApp.isODF()
        }));

        if (this.docApp.isODF()) {
            this.addSection("master");
            this.addControl("layoutslidepicker/changemaster", new ChangeLayoutSlidePicker(this.docView, {
                tooltip: null,
                label: gt("Change master"),
                anchorBorder: ["right", "bottom", "top", "left"],
                // special handling for odf layout slides (but this is change of master slide)
                isOdfLayout: false
            }));
        }
    }
}
