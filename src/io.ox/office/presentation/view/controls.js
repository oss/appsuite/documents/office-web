/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button, CheckBox, CompoundButton } from "@/io.ox/office/textframework/view/controls";
import { LayoutSlidePicker } from "@/io.ox/office/presentation/view/control/layoutslidepicker";

// re-exports =================================================================

export * from "@/io.ox/office/textframework/view/controls";
export * from "@/io.ox/office/presentation/view/control/startpresentationpicker";
export * from "@/io.ox/office/presentation/view/control/slideanimationpicker";

// class InsertLayoutSlidePicker ==============================================

export class InsertLayoutSlidePicker extends LayoutSlidePicker {

    constructor(docView, config) {

        super(docView, {
            label: gt("Slide"),
            tooltip: gt("Insert new slide"),
            icon: "svg:add",
            splitValue: "blank_slide",
            isOdfLayout: docView.docApp.isODF(),
            ...config
        });
    }
}

// class ChangeLayoutSlidePicker ==============================================

export class ChangeLayoutSlidePicker extends LayoutSlidePicker {

    constructor(docView, config) {

        super(docView, {
            label: gt("Layout"),
            tooltip: gt("Change layout"),
            icon: "bi:card-heading",
            updateCaptionMode: "none",
            boundingBox: docView.docApp.getRootNode(),
            isOdfLayout: docView.docApp.isODF(),
            ...config
        });
    }
}

// class SwitchSlideEditModeButton ============================================

//#. start to edit master/layout slides of a presentation
const EDIT_MASTER_LABEL = gt("Edit master");
//#. start to (or return to) edit normal slides of a presentation
const EDIT_NORMAL_LABEL = gt("Edit normal");

export class SwitchSlideEditModeButton extends Button {

    constructor(docView, config) {

        // base constructor
        super({
            label: EDIT_MASTER_LABEL,
            icon: "png:slide-master",
            tooltip: gt("Switch between editing master/layout slides and normal slides"),
            ...config
        });

        // initialization
        this.listenTo(docView.docModel, "change:activeView:after", options => {
            this.setLabel(options.isMasterView ? EDIT_NORMAL_LABEL : EDIT_MASTER_LABEL);
        });
    }
}

// class PlaceholderTypePicker ================================================

/**
 * Control to pick placeholder drawing from dropdown menu
 */
export class PlaceholderTypePicker extends CompoundButton {

    constructor(docView) {

        // base constructor
        super(docView, {
            label: /*#. button to insert placeholder objects */ gt("Placeholder"),
            tooltip: gt("Insert placeholder"),
            icon: "png:placeholder"
        });

        // initialization
        if (docView.docApp.isODF()) {

            this.addControl("placeholder/odfdatetime", new CheckBox({
                label: /*#. check box label: show/hide date/time placeholder object */ gt("Date/Time object"),
                tooltip: /*#. check box tooltip: show/hide date/time placeholder object */ gt("Show or hide date and time placeholder object"),
                boxed: true
            }));

            this.addControl("placeholder/odffooter", new CheckBox({
                label: /*#. check box label: show/hide footer placeholder object */ gt("Footer object"),
                tooltip: /*#. check box tooltip: show/hide footer placeholder object */ gt("Show or hide footer placeholder objects"),
                boxed: true
            }));

            this.addControl("placeholder/odfslidenum", new CheckBox({
                label: /*#. check box label: show/hide slide number placeholder object */ gt("Slide number object"),
                tooltip: /*#. check box tooltip: show/hide slide number placeholder object */ gt("Show or hide slide number object"),
                boxed: true
            }));

        } else {

            this.addControl("placeholder/title", new CheckBox({
                label: /*#. check box label: show/hide the title placeholder object */ gt("Title"),
                tooltip: /*#. check box tooltip: show/hide title placeholder object  */ gt("Show or hide title placeholder object"),
                boxed: true
            }));

            this.addControl("placeholder/body", new CheckBox({
                label: /*#. check box label: show/hide text body placeholder object  */ gt("Text body"),
                tooltip: /*#. check box tooltip: show/hide text body placeholder object */ gt("Show or hide text body placeholder object"),
                boxed: true
            }));

            this.addControl("placeholder/footers", new CheckBox({
                label: /*#. check box label: show/hide footer placeholder objects */ gt("Footer objects"),
                tooltip: /*#. check box tooltip: show/hide footer placeholder objects */ gt("Show or hide footer placeholder objects"),
                boxed: true
            }));

            this.addSection("custom");
            this.addControl("placeholder/insertcustom", new Button({ icon: "png:placeholder-content",          value: "content",  label: /*#. insert content placeholder */ gt("Content") }));
            this.addControl("placeholder/insertcustom", new Button({ icon: "png:placeholder-content-vertical", value: "contentv", label: /*#. insert vertical content placeholder */ gt("Content vertical") }));
            this.addControl("placeholder/insertcustom", new Button({ icon: "png:placeholder-text",             value: "text",     label: /*#. insert text placeholder */ gt("Text") }));
            this.addControl("placeholder/insertcustom", new Button({ icon: "png:placeholder-text-vertical",    value: "textv",    label: /*#. insert vertical text placeholder */ gt("Text vertical") }));
            this.addControl("placeholder/insertcustom", new Button({ icon: "png:placeholder-picture",          value: "picture",  label: /*#. insert picture placeholder */ gt("Image") }));
            this.addControl("placeholder/insertcustom", new Button({ icon: "png:placeholder-table",            value: "table",    label: /*#. insert table placeholder */ gt("Table") }));
        }
    }
}

// class InsertFieldPicker ====================================================

/**
 * Control to pick field from dropdown
 */
export class InsertFieldPicker extends CompoundButton {

    constructor(docView) {

        super(docView, {
            label: /*#. button to insert slide number or date */ gt("Field"),
            tooltip: gt("Insert field"),
            icon: "png:field"
        });

        this.addControl("document/insertfield", new Button({ value: "slidenum", label: /*#. insert slide number */ gt("Slide number") }));
        this.addControl("document/insertfield", new Button({ value: "datetime", label: /*#. insert current date and time */ gt("Date & time") }));
    }
}
