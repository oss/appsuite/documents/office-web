/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { isSoftKeyboardOpen } from "@/io.ox/office/tk/utils";
import { CLOSE_LABEL } from "@/io.ox/office/tk/dom";
import { Button, Label } from "@/io.ox/office/tk/controls";

import { ToolPane } from "@/io.ox/office/baseframework/view/pane/toolpane";
import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';

import "@/io.ox/office/presentation/view/pane/statuspane.less";

// class StatusPane ===========================================================

/**
 * @param {PresentationView} docView
 *  The presentation view containing this instance.
 */
export class StatusPane extends ToolPane {

    constructor(docView) {

        // base constructor
        super(docView, {
            position: "bottom",
            classes: "status-pane"
        });

        const labelToolBar = this.addToolBar(new ToolBar(docView), { targetSection: "center" });
        labelToolBar.addControl(null, new Label({ label: gt("You are in the slide master view."), tooltip: gt("You are in the slide master view."), textAlign: "center", wrapText: true }));

        const buttonToolBar = this.addToolBar(new ToolBar(docView), { targetSection: "trailing" });
        buttonToolBar.addControl("view/slidemasterview/close", new Button({ label: CLOSE_LABEL, tooltip: gt("Leave slide master view") }));

        // ".window-container" is oriented in screen with "top left right bottom" set to zero,
        // when the soft keyboard on Android devices pops up, the status bar stays over the keyboard
        // this makes no sense and looks ugly, so we detect the open keyboard and hide the status pane
        if (_.browser.Android) {
            this.listenToWindowResizeWhenVisible(this._updateVisibility);
        }

        this.listenTo(this.docModel, "change:activeView:after", this._updateVisibility);
        this.listenTo(this.docApp, "docs:editmode", this._updateVisibility);

        // initial visibility
        this._updateVisibility();
    }

    // private methods --------------------------------------------------------

    /*private*/ _updateVisibility() {
        this.toggle(this.docModel.isMasterView() && !isSoftKeyboardOpen());
    }
}
