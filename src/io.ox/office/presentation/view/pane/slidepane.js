/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, COMPACT_DEVICE, IOS_SAFARI_DEVICE, createIcon, convertHmmToLength, setFocus, matchKeyCode,
    isClipboardKeyEvent, isSelectAllKeyEvent } from '@/io.ox/office/tk/dom';
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getBooleanOption, getNumberOption, getPageSize, getScreenWidth, getStringOption, isElementNode, isPortrait,
    scrollToChildNode } from '@/io.ox/office/tk/utils';
import { setToolTip } from '@/io.ox/office/tk/forms';

import { BasePane } from '@/io.ox/office/baseframework/view/pane/basepane';
import { setTargetContainerId } from '@/io.ox/office/textframework/utils/dom';
import { isBrowserShortcutKeyEvent, isF6AcessibilityKeyEvent } from '@/io.ox/office/textframework/utils/keycodeutils';
import LayoutTypeTitleMap from '@/io.ox/office/presentation/view/layouttypetitlemap';
import { SlidePaneContextMenu } from '@/io.ox/office/presentation/view/popup/slidepanecontextmenu';

import '@/io.ox/office/presentation/view/pane/slidepane.less';

// constants ==============================================================

var
    regXNumberOnly            = (/^[0-9]+$/),

    PHRASE__USED_BY_SLIDE     = /*#. pointing to the sole presentation slide that makes use of its master or layout slide     */ gt('used by slide'),
    PHRASE__USED_BY_SLIDES    = /*#. pointing to several presentation slides that make use each of its master or layout slide */ gt('used by slides');

// (local / private) module methods ---------------------------------------

function getCustomSlideTitle(docModel, slideId) {

    return docModel.getSlideFamilyAttributeForSlide(slideId, 'slide', 'name');
}

function collectCoveredStandardSlideRangeAsText(collector, standardSlideId, idx, slideIdList) {
    var
        text = collector.text;

    if (collector.coveringSlideId === collector.getCoveringSlideId(standardSlideId)) {
        if (idx === (collector.previousIdx + 1)) {

            if (!collector.isInRange) {
                collector.isInRange     = true;
                collector.rangeStartIdx = idx;

                collector.text = text + '; ' + (idx + 1);

            } else if (idx === (slideIdList.length - 1)) {

                collector.text = text + '-' + (idx + 1);
            }
        } else {
            if (collector.isInRange && (collector.previousIdx !== collector.rangeStartIdx)) {

                collector.text = text + '-' + (collector.previousIdx + 1);
            }
            collector.isInRange     = false;
            collector.rangeStartIdx = -1;
        }
    } else {
        if (collector.isInRange && (collector.previousIdx !== collector.rangeStartIdx)) {

            collector.text = text + '-' + (collector.previousIdx + 1);
        }
        collector.isInRange     = false;
        collector.rangeStartIdx = -1;
    }
    collector.previousIdx = idx;

    return collector;
}

function getMasterCoveredStandardSlideRangeAsText(docModel, masterId, standardSlideIdList) {
    var
        list,
        text = standardSlideIdList.reduce(collectCoveredStandardSlideRangeAsText, {

            getCoveringSlideId(standardSlideId) {
                return docModel.getMasterSlideId(docModel.getLayoutSlideId(standardSlideId));
            },

            coveringSlideId:    masterId,

            previousIdx:        -1,
            rangeStartIdx:      -1,
            isInRange:          false,
            text:               ''

        }).text;

    list = text.split('; ');
    list.shift();
    text = list.join('; ');

    if (text !== '') {
        text = ' - ' + (regXNumberOnly.test(text) ? PHRASE__USED_BY_SLIDE : PHRASE__USED_BY_SLIDES) + ': ' + text;
    }
    return text;
}
function getLayoutCoveredStandardSlideRangeAsText(docModel, layoutId, standardSlideIdList) {
    var
        list,
        text = standardSlideIdList.reduce(collectCoveredStandardSlideRangeAsText, {

            getCoveringSlideId(standardSlideId) {
                return docModel.getLayoutSlideId(standardSlideId);
            },

            coveringSlideId:    layoutId,

            previousIdx:        -1,
            rangeStartIdx:      -1,
            isInRange:          false,
            text:               ''

        }).text;

    list = text.split('; ');
    list.shift();
    text = list.join('; ');

    if (text !== '') {
        text = ' - ' + (regXNumberOnly.test(text) ? PHRASE__USED_BY_SLIDE : PHRASE__USED_BY_SLIDES) + ': ' + text;
    }
    return text;
}

// class SlidePane ========================================================

/**
 * The side pane in presentation to select slides.
 *
 * @param {PresentationView} docView
 *  The presentation view containing this instance.
 */
export class SlidePane extends BasePane {

    constructor(docView) {

        // base constructor
        super(docView, {
            position: 'left',
            classes: 'slide-pane',
            resizable: !COMPACT_DEVICE,
            minSize: 100,
            maxSize: 400
        });

        // properties ---------------------------------------------------------

        var // self reference
            self = this,
            // the application instance
            app = docView.getApp(),
            // document model
            docModel = app.getModel(),
            // the slide-pane-container node
            slidePaneContainer = $('<div class="slide-pane-container" tabindex="0" > </div>'),
            // drag&drop marker
            slidePlacer = $('<div class="slidePlacer"> </div>'),
            // context menu for the slide pane
            contextMenu = null,
            // slide pane width in percent on tablets, landscape and portrait can have different values
            slidePaneTabletWidth = { landscapeWidth: 14, portraitWidth: 16 },
            // slide pane width in percent on small devices, landscape and portrait can have different values
            slidePaneSmallDevicesWidth = { landscapeWidth: 20, portraitWidth: 25 },

            // the width of the displayed thumbnail (.slideContent) in px at scale 1
            slideContentWidth = null,
            // the height of the displayed thumbnail (.slideContent) in px at scale 1
            slideContentHeight = 100,

            //SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__AAA  = 20,
            SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__AA   = 10,
            SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__A    =  5,
            SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__B    =  1,

            SLIDE_RANGE__BUFFER_SIZE = (_.browser.Chrome
                ? (_.browser.Android
                    ? SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__A
                    : SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__AA
                    //: SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__AAA
                )
                : SLIDE_RANGE__BUFFER_SIZE__PERFORMANCE_LEVEL__B
            ),

            css_value__preview_scale  = 'scale(1)',

            // on touch there is no visible scrollbar, therefore the width is reduced to 6 for a nicer layout
            scrollbarWidth = (!COMPACT_DEVICE) ? 18 : 6,
            // this set the offset after the slidePane thumbnail to the right in px at scale 1
            offsetRight = 10  + scrollbarWidth,
            // current offset before the slidePane thumbnail to the left
            currentOffsetLeft = null,  //TODO
            // depending on the current view the defaultOffSet is used as the currentOffsetLeft - must be equal to the 'margin-left' value from the less
            defaultOffsetLeftForMasterView = 25,
            defaultOffsetLeftForNormalView = 55,
            // total offset in px from the slidePane and slideContentWidth
            offSet = null, // .middle 35 padding left + 10 / offset to right ( 178+35+10 = 223) //TODO comment
            // slidePane width in px at scale 1
            slidePaneWidthAtScale1 = null,
            // get the windows width in which the app suite is shown
            windowWidth = null,
            // name of class that marks a slide as hidden in the DOM
            hiddenClassName = 'hiddenSlide',

            handleVisibleSlideRangeNotificationDebounced = $.noop,
            forceRerenderSlidePaneThumbnailsDebounced = $.noop,
            forceVisibleThumbnailsDebounced = $.noop,

            currentSlideRange = [null, null],
            recentSlideRange  = [0, 0],

            // the slide number were the drag and drop starts
            dragStartSlideNumber,
            // the slide number which is selected while the drag interaction in ongoing (can be from scrolling or mouse/touch movement)
            dragMoveSlideNumber,
            // flag if the context menu should be opened on TOUCH from a second click on the selected slide
            doubleTouchOnSelectedTarget,
            // number of slides in the slide pane
            slideCount,
            // flag if the slide pane context menu is open
            contextIsOpen = false,
            // timer to determine if we have a touch hold
            timer = null,
            // current scale factor for slide thumbnails
            factor,
            // containing IDs from a possible multiselection
            slideSelection = [],
            // scroll position from the slidePane
            slidePaneScrollPosition = 0;

        var i18nLayoutTypeTitleDB = app.resourceManager.getResource('layouttypetitles');

        // private methods ----------------------------------------------------

        function getMasterTypeTitle() {
            var defaultTitle = LayoutTypeTitleMap.master;
            return (i18nLayoutTypeTitleDB[defaultTitle] || defaultTitle);
        }

        function getLayoutTypeTitle(docModel, slideId) {
            var defaultTitle = LayoutTypeTitleMap[docModel.getSlideFamilyAttributeForSlide(slideId, 'slide', 'type')];
            return (i18nLayoutTypeTitleDB[defaultTitle] || defaultTitle);
        }

        /**
         * Returns the screen width on COMPACT_DEVICES or the browser width on other devices.
         * Note: On Android the orientation change event is too early, so that we would get a
         * wrong width when we would use 'Utils.getPageSize().width'.
         *
         * @returns {Number}
         *   The screen width on COMPACT_DEVICES or the browser width in percent.
         */
        function getWindowWidth() {
            if (COMPACT_DEVICE) {
                return getScreenWidth();
            } else {
                return getPageSize().width;
            }
        }

        /**
         * Returns the width in percent which should be used on COMPACT_DEVICES.
         * It considers the device orientation and gives back different value for landscape and portrait.
         *
         * @returns {Number}
         *   The width in percent according to the current orientation.
         */
        function getWidthOnCompactDevices() {
            if (SMALL_DEVICE) {
                return isPortrait() ? slidePaneSmallDevicesWidth.portraitWidth : slidePaneSmallDevicesWidth.landscapeWidth;
            } else {
                return isPortrait() ? slidePaneTabletWidth.portraitWidth : slidePaneTabletWidth.landscapeWidth;
            }
        }

        /**
         * Set the slide pane width to a given percent value.
         *
         * @param {Number} widthInPercent
         *  The width in percent.
         */
        function setSizeSlidePaneInPercent(widthInPercent) {
            self.setSize(Math.round(widthInPercent * windowWidth / 100));
        }

        /**
         * Get the current slide pane width in percent.
         *
         * @returns {Number}
         *   The slide pane width in percent.
         */
        function getWidthSlidePaneInPercent() {
            return self.getSize() / windowWidth * 100;
        }

        /**
         * Calculates values to set dimensions for the slide pane
         * for a given slide ratio.
         *
         * @param {Number} slideRatio
         *  The ratio from the current slide.
         *
         *  @param {Boolean} isMasterView
         *  A flag if the normal or master/slide view is active.
         */
        function calcSlidePaneDimensionsForSlideRatio(slideRatio, isMasterView) {
            slideContentWidth = slideRatio * slideContentHeight;
            //offSet = currentOffsetLeft + offsetRight;
            //slidePaneWidthAtScale1 = slideContentWidth + offSet;
            setNewSlidePaneOffset(isMasterView);
        }

        /**
         * Helper that (re)computes the currently valid scaling multiplier for any preview.
         * Preview thumbnail must fit to the 'slideContentHeight' at scale 1 from the slide pane,
         * so that the big slide out of the page fits in the small thumbnail container in the slide pane.
         */
        function computeInitialPreviewScaling() {
            css_value__preview_scale = `scale(${math.roundp(slideContentHeight / convertHmmToLength(docModel.getSlideDocumentSize().height, 'px', 1), 1e-5)})`;
        }

        /**
         * Helper that applies the currently valid scaling multiplier to any preview.
         */
        function applyPreviewScaling($preview) {
            $preview.css('transform-origin', '0 0');
            $preview.css('transform', css_value__preview_scale);
        }

        /**
         * Sets the visible left offset between thumbnail and border in the slide pane depended on the view (normal/master view).
         * It calculates all values which are depended on the 'currentOffsetLeft' to set the right dimensions for the slide pane.
         *
         *  @param {Boolean} isMasterView
         *  A flag if the normal or master/slide view is active.
         */
        function setNewSlidePaneOffset(isMasterView) {
            currentOffsetLeft = isMasterView ? defaultOffsetLeftForMasterView : defaultOffsetLeftForNormalView;
            offSet = currentOffsetLeft + offsetRight;
            slidePaneWidthAtScale1 = slideContentWidth + offSet;
        }
        // ------ render functions ------

        /**
         * Render the Slidepane.
         *
         * @param {Boolean} isMasterView
         *  A flag if the normal or master/slide view is active.
         */
        function renderSlidePane(isMasterView) {

            // clear the DOM for new rendering
            slidePaneContainer.empty();

            // classify standard view state - somehow connected to BUG-44952 [https://bugs.open-xchange.com/show_bug.cgi?id=44952]
            slidePaneContainer.toggleClass('standard-view', !isMasterView);

            // get slideIdList from the model
            var slideIdList = isMasterView ? docModel.getMasterSlideOrder() : docModel.getStandardSlideOrder();
            // to cache the attached nodes
            var nodes = [];

            // the html template
            var templ = null;

            // render the slideIdList and insert the slide id as an attribute
            slideIdList.forEach(slideId => {

                // get the blue print for one slide
                templ = createSingleSlideMarkup(slideId, isMasterView);

                // attach preview when it already exists in the preview-store
                insertPreviewThumbnailInSlide(slideId, templ);

                // add the slide to the final jQuery object
                nodes.push(templ);
            });
            slidePaneContainer.append(nodes);

        }

        /**
         * Inserting a preview from the preview-store in it's slide container. When it doesn't exist
         * (e.g. isn't finished in the preview-store), nothing is added.
         */
        function insertPreviewThumbnailInSlide(slideId, $slideContainer) {

            // never insert an unformatted slide into the slide pane
            if (docModel.isUnformattedSlide(slideId)) { return; }

            // use preview when it exists, otherwise do nothing
            var $preview = docView.getPreview(slideId);
            if ($preview) {

                // scale the preview to fit to the in it's container at slidepane scale 1
                applyPreviewScaling($preview);

                // in case this method needs to be called by id only without being able to provide a current container reference.
                $slideContainer = $slideContainer || slidePaneContainer.children('[data-container-id="' + slideId + '"]').eq(0);

                $preview.prependTo($slideContainer.find('.slideContent').empty());
            }
        }

        /**
         * Updating all slide previews that has been changed. These slide(s) Ids are delivered in the 'slideIdList'.
         */
        function updatePreviewThumbnails(slideIdList) {
            //console.log('+++ updatePreviewThumbnails +++');
            //console.log('+++ slideIdCastList +++', getSlideIdCastList());
            //console.log('+++ slideIdList +++', slideIdList);

            slideIdList = _.intersection(slideIdList, getSlideIdCastList());

            // anonymous function expression due to `insertPreviewThumbnailInSlide` featuring an arguments api
            // that does not match with the arguments api of array iterator methods.
            slideIdList.forEach(function (slideId/*, idx, list*/) {
                insertPreviewThumbnailInSlide(slideId);
            });
        }

        function getTakeoutIndexListsOfSlidePaneChildCollection(slidePaneChildCount, lowerBufferIndex, upperBufferIndex) {
            var
                idx,

                takeOutIndexListBefore  = [],
                takeOutIndexListAfter   = [];

            if (lowerBufferIndex >= 1) {
                for (idx = 0; idx < lowerBufferIndex; idx++) {
                    takeOutIndexListBefore.push(idx);
                }
            }
            if (upperBufferIndex < (slidePaneChildCount - 1)) {
                for (idx = (upperBufferIndex + 1); idx < slidePaneChildCount; idx++) {
                    takeOutIndexListAfter.push(idx);
                }
            }
            return {
                before: takeOutIndexListBefore,
                after:  takeOutIndexListAfter,
                full:   takeOutIndexListBefore.concat(takeOutIndexListAfter)
            };
        }
        function getPrefetchIndexListsOfSlidePaneChildCollection(rangeBegin, rangeEnd, lowerBufferIndex, upperBufferIndex) {
            var
                idx,

                prefetchIndexListBefore = [],
                prefetchIndexListAfter  = [];

            for (idx = lowerBufferIndex; idx < rangeBegin; idx++) {
                prefetchIndexListBefore.push(idx);
            }
            for (idx = (rangeEnd + 1); idx <= upperBufferIndex; idx++) {
                prefetchIndexListAfter.push(idx);
            }
            return {
                before: prefetchIndexListBefore,
                after:  prefetchIndexListAfter,
                full:   prefetchIndexListBefore.concat(prefetchIndexListAfter)
            };
        }
        function mapSlideIdByPrefetchIndexWithinSlideIdListContext(prefetchIdx/*, idx, list*/) {
            return this[prefetchIdx];
            //return slideIdList[prefetchIdx];
        }

        function handlePreviewPresence(slideRangeState) {
            //window.console.log('--- --- ---');
            //window.console.log('+++ handlePreviewPresence +++ [currentSlideRange, visibleSlideIdList, SLIDE_RANGE__BUFFER_SIZE] : ', slideRangeState.slideRanges.current, slideRangeState.slideIdLists.visible, SLIDE_RANGE__BUFFER_SIZE);

            var                                                               //
                //SLIDE_RANGE__BUFFER_SIZE = 10,                                //  There are different approaches in order to (a) display,
                //                                                              //  (b) clean-up and (c) prefetch/pre-render preview nodes,
                slideRanges   = slideRangeState.slideRanges,                  //  in order to improve DOM performance by reducing operations
                slideIdLists  = slideRangeState.slideIdLists,                 //  acting upon it.

                currentSlideRange = slideRanges.current,                      //  The list variables of a broader but already sufficiently
                recentSlideRange  = slideRanges.recent,                       //  enough approach are marked as (A) and are commented too.

                slideIdList               = slideIdLists.complete,            //
                visibleSlideIdList        = slideIdLists.visible,             //  => (A) was already sufficient enough in order to display the currently visible preview range.
                visiblyAddedSlideIdList   = slideIdLists.added,               //

                $slidePaneChildCollection = slidePaneContainer.children(),    //
                slidePaneChildCount       = $slidePaneChildCollection.length, //

                currentRangeBegin = currentSlideRange[0],                     //
                currentRangeEnd   = currentSlideRange[1],                     //
                recentRangeBegin  = recentSlideRange[0],                      //
                recentRangeEnd    = recentSlideRange[1],                      //

                currentLowerBufferIndex = Math.max(0, (currentRangeBegin - SLIDE_RANGE__BUFFER_SIZE)),
                currentUpperBufferIndex = Math.min((slidePaneChildCount - 1), (currentRangeEnd + SLIDE_RANGE__BUFFER_SIZE)),
                recentLowerBufferIndex  = Math.max(0, (recentRangeBegin - SLIDE_RANGE__BUFFER_SIZE)),
                recentUpperBufferIndex  = Math.min((slidePaneChildCount - 1), (recentRangeEnd + SLIDE_RANGE__BUFFER_SIZE)),

                currentTakeOutIndexLists  = getTakeoutIndexListsOfSlidePaneChildCollection(slidePaneChildCount, currentLowerBufferIndex, currentUpperBufferIndex),
                currentPrefetchIndexLists = getPrefetchIndexListsOfSlidePaneChildCollection(currentRangeBegin, currentRangeEnd, currentLowerBufferIndex, currentUpperBufferIndex),
                recentTakeOutIndexLists   = getTakeoutIndexListsOfSlidePaneChildCollection(slidePaneChildCount, recentLowerBufferIndex, recentUpperBufferIndex),
                recentPrefetchIndexLists  = getPrefetchIndexListsOfSlidePaneChildCollection(recentRangeBegin, recentRangeEnd, recentLowerBufferIndex, recentUpperBufferIndex),

                //currentTakeOutIndexList   = currentTakeOutIndexLists.full,    //  => (A) was already sufficient enough in order to clean-up all not necessarily needed preview nodes.
                //currentPrefetchIndexList  = currentPrefetchIndexLists.full,   //  => (A) was already sufficient enough in order to pre-render all not yet visible preview nodes.
                //recentTakeOutIndexList    = recentTakeOutIndexLists.full,     //
                recentPrefetchIndexList   = recentPrefetchIndexLists.full,    //

                takeouIndexListBefore     = _.difference(currentTakeOutIndexLists.before, recentTakeOutIndexLists.before),
                takeouIndexListAfter      = _.difference(currentTakeOutIndexLists.after, recentTakeOutIndexLists.after),
                takeouIndexList           = takeouIndexListBefore.concat(takeouIndexListAfter),

                prefetchIndexListBefore   = _.difference(currentPrefetchIndexLists.before, recentPrefetchIndexLists.before),
                prefetchIndexListAfter    = _.difference(currentPrefetchIndexLists.after, recentPrefetchIndexLists.after),
                prefetchIndexList         = prefetchIndexListBefore.concat(prefetchIndexListAfter),

                //currentPrefetchSlideIdList  = currentPrefetchIndexList.map(mapSlideIdByPrefetchIndexWithinSlideIdListContext, slideIdList),
                recentPrefetchSlideIdList   = recentPrefetchIndexList.map(mapSlideIdByPrefetchIndexWithinSlideIdListContext, slideIdList),

                visiblyRenderSlideIdList    = _.difference(visiblyAddedSlideIdList, recentPrefetchSlideIdList);

            //   window.console.log('--- --- ---');
            //   window.console.log('+++ handlePreviewPresence +++ [slidePaneChildCount, currentRangeBegin, currentRangeEnd, recentRangeBegin, recentRangeEnd] : ', slidePaneChildCount, currentRangeBegin, currentRangeEnd, recentRangeBegin, recentRangeEnd);
            //   window.console.log('+++ handlePreviewPresence +++ [currentLowerBufferIndex, currentUpperBufferIndex, recentLowerBufferIndex, recentUpperBufferIndex] : ', currentLowerBufferIndex, currentUpperBufferIndex, recentLowerBufferIndex, recentUpperBufferIndex);
            //   window.console.log('--- --- ---');
            //   window.console.log('+++ handlePreviewPresence +++ [visibleSlideIdList, visiblyAddedSlideIdList, recentPrefetchSlideIdList] : ', visibleSlideIdList, visiblyAddedSlideIdList, recentPrefetchSlideIdList);
            // //window.console.log('+++ handlePreviewPresence +++ [visibleSlideIdList, visiblyAddedSlideIdList, recentPrefetchSlideIdList, currentPrefetchSlideIdList] : ', visibleSlideIdList, visiblyAddedSlideIdList, recentPrefetchSlideIdList, currentPrefetchSlideIdList);
            //   window.console.log('--- --- ---');
            //   window.console.log('+++ handlePreviewPresence +++ [visiblyRenderSlideIdList] : ', visiblyRenderSlideIdList);
            //   window.console.log('--- --- ---');
            //   window.console.log('+++ handlePreviewPresence +++ [recentTakeOutIndexListBefore, currentTakeOutIndexListBefore, difference] : ', recentTakeOutIndexLists.before, currentTakeOutIndexLists.before, takeouIndexListBefore);
            //   window.console.log('+++ handlePreviewPresence +++ [recentTakeOutIndexListAfter, currentTakeOutIndexListAfter, difference] : ', recentTakeOutIndexLists.after, currentTakeOutIndexLists.after, takeouIndexListAfter);
            // //window.console.log('+++ handlePreviewPresence +++ [recentTakeOutIndexList, currentTakeOutIndexList] : ', recentTakeOutIndexList, currentTakeOutIndexList);
            //   window.console.log('--- --- ---');
            //   window.console.log('+++ handlePreviewPresence +++ [recentPrefetchIndexListBefore, currentPrefetchIndexListBefore, difference] : ', recentPrefetchIndexLists.before, currentPrefetchIndexLists.before, prefetchIndexListBefore);
            //   window.console.log('+++ handlePreviewPresence +++ [recentPrefetchIndexListAfter, currentPrefetchIndexListAfter, difference] : ', recentPrefetchIndexLists.after, currentPrefetchIndexLists.after, prefetchIndexListAfter);
            //   window.console.log('--- --- ---');
            //   window.console.log('--- --- ---');

            // visibleSlideIdList.forEach(function (slideId, idx/*, list*/) {
            //
            //     insertPreviewThumbnailInSlide(slideId, $slidePaneChildCollection.eq(currentRangeBegin + idx));
            // });
            //
            // currentTakeOutIndexList.forEach(function (takeoutIdx/*, idx, list*/) {
            //
            //     $slidePaneChildCollection.eq(takeoutIdx).find('.slideContent').empty();
            // });
            // currentPrefetchIndexList.forEach(function (prefetchIdx/*, idx, list*/) {
            //
            //     insertPreviewThumbnailInSlide(slideIdList[prefetchIdx], $slidePaneChildCollection.eq(prefetchIdx));
            // });

            takeouIndexList.forEach(function (takeoutIdx/*, idx, list*/) {    // reduces the amount of "empty" actions in comparision to the former solution, the commented code block above.

                $slidePaneChildCollection.eq(takeoutIdx).find('.slideContent').empty();
            });
            visibleSlideIdList.forEach(function (slideId, idx/*, list*/) {    // reduces the amount of "insert" actions in comparision to the former solution, the commented code block above.
                if (visiblyRenderSlideIdList.indexOf(slideId) >= 0) {
                    insertPreviewThumbnailInSlide(slideId, $slidePaneChildCollection.eq(currentRangeBegin + idx));
                }
            });
            prefetchIndexList.forEach(function (prefetchIdx/*, idx, list*/) { // reduces the amount of "insert" actions in comparision to the former solution, the commented code block above.

                insertPreviewThumbnailInSlide(slideIdList[prefetchIdx], $slidePaneChildCollection.eq(prefetchIdx));
            });
        }

        /**
         *
         */
        function updateMasterLayoutTooltips() {
            if (docModel.isMasterView()) {
                var
                    slideContainerList = slidePaneContainer.children().toArray();

                slideContainerList.forEach(function (elmContainer) {
                    var
                        $container  = $(elmContainer),
                        slideId     = $container.attr('data-container-id');

                    applyMasterLayoutTooltip($container, slideId);
                });
            }
        }

        /**
         * @param {JQuery} $container
         * @param {string} slideId
         */
        function applyMasterLayoutTooltip($container, slideId) {

            var slideTitle = getCustomSlideTitle(docModel, slideId);

            if (!slideTitle) {
                if (docModel.isMasterSlideId(slideId) || (app.isODF() && docModel.isLayoutSlideId(slideId))) {
                    slideTitle = getMasterTypeTitle() || ('master / ' + slideId);
                } else {
                    slideTitle = getLayoutTypeTitle(docModel, slideId) || ('layout / ' + slideId);
                }
            }

            var standardSlideIdList = docModel.getStandardSlideOrder();
            if (docModel.isMasterSlideId(slideId)) {
                slideTitle += getMasterCoveredStandardSlideRangeAsText(docModel, slideId, standardSlideIdList);
            } else {
                slideTitle += getLayoutCoveredStandardSlideRangeAsText(docModel, slideId, standardSlideIdList);
            }

            setToolTip($container, _.noI18n(slideTitle));
        }

        /**
         * Returning the markup (without the preview, that happens at a different function) for a single slide.
         * Master and layout and hidden slides have a special class. All slides have their Id attached.
         */
        function createSingleSlideMarkup(slideId, isMasterView) {

            const templ = $('<div class="slide-container">').append(
                $('<div class="left-section">').append(
                    $('<div class="slide-index">'),
                    $('<div class="comment-thread-count" data-thread-count="0">').append(createIcon('svg:comment', 25))
                ),
                $('<div class="main-section">').append(
                    $('<div class="slidePaneThumbnail">'),
                    $('<span class="hidden-slide-icon">').append(createIcon('bi:eye-slash'))
                )
            );

            // create tooltips and set class names according to current master/layout view state.
            if (isMasterView) {
                if (docModel.isMasterSlideId(slideId)) {

                    templ.addClass('master');
                } else {
                    templ.addClass('layout');
                }
                applyMasterLayoutTooltip(templ, slideId);
            }

            // attach the id to the .slide-container
            setTargetContainerId(templ, slideId);

            // if the current slide (see 'slideId') is hidden, attach a hide class
            if (docModel.isHiddenSlide(slideId)) { templ.addClass(hiddenClassName); }

            // append the blue print for the slideContent that is inside a preview slide
            templ.find('.slidePaneThumbnail').eq(0).append('<div class="slideContent">');

            return templ;
        }

        /**
         * Set a slide hidden in the slide pane. It adds/remove a hiddenSlide class according to the state.
         *
         * @param {Object} options
         *  Parameters:
         *
         *  @param {Boolean} [options.slideId]
         *   The ID from the slide for which the attribute was modified.
         *
         *  @param {Boolean} [options.hiddenStateChangede]
         *   A flag if the hidden state from the given slide was changed or not.
         *
         *  @param {Boolean} [options.isHidden]
         *   A flag if the given slide is hidden or not.
         *
         */
        function renderSlideHidden(options) {

            // only change the class when the state was really changed
            if (options.hiddenStateChanged) {

                // add/remove class according to the given state
                if (options.isHidden) {
                    slidePaneContainer.children('div[data-container-id = "' + options.slideId + '"]').addClass(hiddenClassName);
                } else {
                    slidePaneContainer.children('div[data-container-id = "' + options.slideId + '"]').removeClass(hiddenClassName);
                }
            }
        }

        /**
         * Select the slide to a given 'id' in the GUI.
         *
         * @param {String} id
         *  The slide id.
         */
        function guiSelectSlide(id) {

            // unselect all slides
            slidePaneContainer.children('.selected').removeClass('selected');

            // select the given slide
            var selectedSlide = slidePaneContainer.children('div[data-container-id = "' + id + '"]');

            // it might happen, that there are no slides to select in the view so 'id' would be not valid - prevent that
            if (selectedSlide.length > 0) {

                selectedSlide.addClass('selected');

                // scroll to the current selected slide in the slidepane
                scrollToChildNode(slidePaneContainer, selectedSlide);

                // The 'slideSelection' needs to be updated when a different slide is selected/active.
                // - For a single selection (just one slide selected) it happens here. In this case the old
                //   slide must be deleted and the new must be added to reflect this change.
                // - For a multiselection the handling is different and it happens at various handler function for the multiselection.
                if ((!(multiSelectionActive())) && (slideSelection.indexOf(id) === -1)) {
                    slideSelection = [];
                    slideSelection.push(id);
                }

            }

        }

        // ------ handler functions ------

        /**
         * Move a slide node from a given index to a new index position in DOM.
         *
         * @param {Object} movedSlideData
         *  Parameters:
         *  @param {Number} movedSlideData.from
         *     The current index of the moved slide ID.
         *  @param {Number} movedSlideData.to
         *     The new index of the moved slide ID.
         *  @param {Boolean} movedSlideData.isMasterOrLayoutSlide
         *     Whether the moved slide is in the standard or the master/layout container.
         */
        function handleMovedSlide(movedSlideData) {

            // only change the view when the move operation is from the same active view
            // (important when the active view was changed and undo is called after that)
            if (docModel.isMasterView() === movedSlideData.isMasterOrLayoutSlide) {

                // all slide containers
                var slideContainer = slidePaneContainer.children('.slide-container');

                var from = slideContainer.eq(movedSlideData.from)[0];
                var to = null;

                // calculate the correct index
                if (movedSlideData.from > movedSlideData.to) {
                    to = slideContainer.eq(movedSlideData.to)[0];
                } else {
                    to = slideContainer.eq(movedSlideData.to + 1)[0];
                }
                // move the slide in DOM
                from.parentNode.insertBefore(from, to);

                // scroll to the moved slide, which is also the selected one, when it's outside the viewport (important for moving via keyboard arrows)
                var selectedChild = slidePaneContainer.children('.selected');
                if (selectedChild.length > 0) {
                    scrollToChildNode(slidePaneContainer, selectedChild); //TODO maybe debounce
                } else {
                    globalLogger.log('Warning: No selected slide in slide pane!');
                }
            }
        }

        /**
         * Insert a slide node in the slide pane.
         *
         * @param {Object} insertSlideData
         *  Parameters:
         *  @param {String} [insertSlideData.id]
         *      The slide Id from the inserted slide.
         *  @param {Boolean} [insertSlideData.isMasterView]
         *      Whether the event was triggered in master/layout view.
         *  @param {Boolean} [insertSlideData.isMasterOrLayoutSlide]
         *     Whether the inserted slide is a standard or master/layout slide.
         *  @param {Object} [insertSlideData.documentReloaded=false]
         *      Whether the document was reloaded with a snapshot.
         */
        function handleInsertSlide(insertSlideData) {

            // do nothing, if the document was reloaded with a snapshot
            if (getBooleanOption(insertSlideData, 'documentReloaded', false)) { return; }

            var // the position in the slide pane were the slide should be inserted
                insertPos,
                // whether the event was triggered in master/layout view.
                isMasterView = getBooleanOption(insertSlideData, 'isMasterView'),
                // whether the slide is a master/layout slide
                isMasterOrLayoutSlide = getBooleanOption(insertSlideData, 'isMasterOrLayoutSlide'),
                // the created template markup
                templ,
                // the node were the slide template should be inserted
                targetNode;

            // important for remote and undo: only insert a slide in the DOM when the slide type (master&layout/normal) is fitting to the currently active view (master/normal)
            if (isMasterView === isMasterOrLayoutSlide) {

                insertPos = docModel.getSortedSlideIndexById(insertSlideData.id);
                templ = createSingleSlideMarkup(insertSlideData.id, isMasterOrLayoutSlide);
                targetNode = slidePaneContainer.children().eq(insertPos);

                // when no slides exists in the slide pane there is no 'targetNode',
                // then just append to the slidePaneContainer
                if (targetNode.length > 0) {
                    targetNode.before(templ);
                } else {
                    slidePaneContainer.append(templ);
                }

                // restore slide pane thumbnail size after rendering
                setSizeSlidePaneThumbnails(self.getSize()); // TODO maybe debounce

                clearSlidesInMultiSelectionDebounced();

            } else if (isMasterView) {
                // does cover the case of being a slave client in master view mode but having gotten inserted standard slide(s) remotely (thus out of current view).

                updateMasterLayoutTooltips(); // all tooltips are in need of being updated since each master/layout container tooltip displays its standard slide coverage.
            }

            // the range of visible slides might have changed
            handleVisibleSlideRangeNotification({ triggerslidecount: true });
        }

        /**
         * Delete a slide node in the slide pane.
         *
         * @param {Object} deleteSlideData
         *  Parameters:
         *  @param {String} [deleteSlideData.id]
         *      The slide Id from the deleted slide.
         *  @param {Boolean} [deleteSlideData.isMasterView]
         *      Whether the event was triggered in master/layout view.
         *  @param {Boolean} [deleteSlideData.isMasterOrLayoutSlide]
         *     Whether the deleted slide is a standard or master/layout slide.
         */
        function handleDeleteSlide(deleteSlideData) {
            var
                isMasterView          = getBooleanOption(deleteSlideData, 'isMasterView'),
                isMasterOrLayoutSlide = getBooleanOption(deleteSlideData, 'isMasterOrLayoutSlide');

            // important for remote and undo: only delete a slide in the DOM when the slide type (master&layout/normal) is fitting to the currently active view (master/normal)
            if (isMasterView === isMasterOrLayoutSlide) {

                slidePaneContainer.children('div[data-container-id = "' + deleteSlideData.id + '"]').remove();
                clearSlidesInMultiSelectionDebounced();

            } else if (isMasterView) {
                // does cover the case of being a slave client in master view mode but having gotten removed standard slide(s) remotely (thus out of current view).

                updateMasterLayoutTooltips(); // all tooltips are in need of being updated since each master/layout container tooltip displays its standard slide coverage.
            }

            // the range of visible slides might have changed
            handleVisibleSlideRangeNotification({ triggerslidecount: true });
        }

        /**
         * Adapts the slide thumbnail size according to to the slide pane width.
         * Note: The thumbnail size is changed automatically everytime when the slide pane width is changed
         * due to the 'pane:resize' event. 'Normal', 'master' and 'layout' thumbnails have a different layout depending
         * on the activeView.
         *
         * @param {Number} slidePaneWidthinPx
         *  The slide pane width in pixel.
         */
        function setSizeSlidePaneThumbnails(slidePaneWidthinPx) {

            factor = ((slidePaneWidthinPx - offSet) / (slidePaneWidthAtScale1 - offSet));

            var layoutSlideFactor = 0.8;

            // use integer values to prevent pixel rounding errors by the browser (noticed only in win/firefox)
            var currentWidth  = Math.round(factor * slideContentWidth);
            var currentHeight = Math.round(factor * slideContentHeight);

            // for master view
            if (docModel.isMasterView()) {

                var slideLayout = slidePaneContainer.find('.layout').find('.slidePaneThumbnail');
                var slideMaster = slidePaneContainer.find('.master').find('.slidePaneThumbnail');

                var layoutSlideWidth = Math.round(currentWidth * layoutSlideFactor);
                var layoutSlideHeight = Math.round(currentHeight * layoutSlideFactor);
                var offsetToKeepSameHeightAfterLayoutScale = (currentHeight - layoutSlideHeight);
                var offsetLeftForLayoutSlides = (currentWidth - layoutSlideWidth);

                slideMaster.css({ width: currentWidth, height: currentHeight });
                slideMaster.children('.slideContent').css('transform', 'scale(' + factor + ')');

                slideLayout.css({ width: layoutSlideWidth, height: layoutSlideHeight, marginTop: (offsetToKeepSameHeightAfterLayoutScale * 0.5), marginBottom: (offsetToKeepSameHeightAfterLayoutScale * 0.5), marginLeft: offsetLeftForLayoutSlides });
                slideLayout.children('.slideContent').css('transform', 'scale(' + factor * layoutSlideFactor + ')');

                // resize slidePlacer to the current scale //TODO offset
                slidePlacer.css({ width: currentWidth + (currentOffsetLeft - 10) });

            // for normal view
            } else {
                var slideNormal = slidePaneContainer.find('.slidePaneThumbnail');

                slideNormal.css({ width: currentWidth, height: currentHeight });
                slideNormal.children('.slideContent').css('transform', 'scale(' + factor + ')');

                // resize slidePlacer to the current scale //TODO offset
                slidePlacer.css({ width: currentWidth + (currentOffsetLeft - 10) });
            }
        }

        function forceRerenderSlidePaneThumbnails() {
            //globalLogger.log('+++ forceRerenderSlidePaneThumbnails +++');
            var
                //$collection = $('.slidePaneThumbnail > .slideContent');
                $collection = slidePaneContainer.find('.slidePaneThumbnail > .slideContent');

            $collection.toArray().forEach(function (elmNode) {
                $(elmNode).parent().append(elmNode);
            });
        }

        /**
         * After scrolling or resizing the side pane, it might happen in the loading phase, that
         * thumbnail slides are not shown, because they are not formatted yet. This formatting
         * is enforced within this handler function.
         */
        function forceVisibleThumbnails() {

            // the position of the slides in the visible slide pane range
            const slideRange = getVisibleSlideRange();
            // the slides that must be formatted urgently
            let urgentRequiredSlides = null;
            // the position of the first slide
            let index  = slideRange[0];
            // the position of the last slide
            const lastIndex = slideRange[1];

            while (index <= lastIndex) {
                const slideId = docModel.getSlideIdByPosition(index);
                if (slideId && docModel.isUnformattedSlide(slideId)) {
                    urgentRequiredSlides ||= [];
                    urgentRequiredSlides.push(slideId);
                }
                index++;
            }

            if (urgentRequiredSlides) { docModel.forceFormattingOfSlideGroup(urgentRequiredSlides); }
        }

        /**
         * Sets the active slide to the clicked/touched slide in the model via the controller. When the active slide
         * is changed in the model fires and event automatically, which invokes observeActiveslide() to update the GUI.
         *
         * Also: On COMPACT_DEVICES tapping on a selected slide invokes the context menu.
         *
         * @param {Event} event
         *  The click/touch event from the user to select the slide.
         *
         * @param {TrackingInputDevice} device
         *  If the event was triggered by 'mouse' or touch.
         */
        function setActiveSlideforClickedTarget(event, device) {
            // reset it here
            doubleTouchOnSelectedTarget = false;

            // get the 'clicked' slide-container
            var selected = $(event.target).closest('.slide-container');

            // on TOUCH the context menu is invoked by a tap on the selected slide
            if (device === 'touch') {
                // the focus should be on the slidepane when the slide pane is touched
                setFocus(slidePaneContainer);

                // set the virtual focus to the slidepane
                docView.setVirtualFocusSlidepane(true);

                // if the touched target is already selected, invoke the context menu
                if (selected.hasClass('selected')) {
                    doubleTouchOnSelectedTarget = true;
                }
            }

            // set the 'clicked' activeSlide in the model via controller
            if (selected.attr('data-container-id') !== undefined) {  // is undefined when clicking on the margin from the slide-container
                // #49805 - IE 11: Copy and paste of drawing frames doesn't work
                // with "preserveFocus: true" the IE sets the focus into the hidden node instead of the slide pane, which prevents the clipboard events.
                docView.executeControllerItem('slidePaneSetActiveSlide', selected.attr('data-container-id'), { preserveFocus: true });

                // toggle comments pane when clicking comment thread count icon
                if ($(event.target).closest('.comment-thread-count').length) {
                    docView.commentsPane.toggle();
                }
            }
        }

        /**
         * Key handler for the slide pane.
         *
         * @param {event} event
         *  The key handler event.
         */
        function keyHandler(event) {

            // This keyhandler is attached in the slidepane and also in the clipboard node.
            // Therefore this handler must return if the virtual focus was not in the slidepane,
            // these shortcuts should only be used when the slidepane is active.
            if (!docView.hasSlidepaneVirtualFocus()) { return; } // TODO CHECK if this is the right place

            // switching from slide pane to main document via keyboard (DOCS-4430)
            if (isF6AcessibilityKeyEvent(event) || matchKeyCode(event, 'TAB')) { // user can traverse through all elements with "Tab"
                self.setTimeout(() => {
                    docView.setVirtualFocusSlidepane(false);
                    docModel.setFocusToSlideView(); // setting the focus into the main document -> can be clipboard node or a drawing frame
                }, 0);
                return;
            }

            if (isBrowserShortcutKeyEvent(event)) { return; }

            const focusObject = { focusTarget: slidePaneContainer };
            let keepFocusInSlidePane = false;

            if (matchKeyCode(event, 'UP_ARROW'))   { clearSlidesInMultiSelection(); docView.executeControllerItem('slidePaneSlideUp', '', focusObject); }
            if (matchKeyCode(event, 'DOWN_ARROW')) { clearSlidesInMultiSelection(); docView.executeControllerItem('slidePaneSlideDown', '', focusObject); }
            if (matchKeyCode(event, 'PAGE_UP'))   { clearSlidesInMultiSelection(); docView.executeControllerItem('slidePaneSlideUp', '', focusObject); }
            if (matchKeyCode(event, 'PAGE_DOWN')) { clearSlidesInMultiSelection(); docView.executeControllerItem('slidePaneSlideDown', '', focusObject); }

            if (matchKeyCode(event, 'HOME'))       { clearSlidesInMultiSelection(); docView.executeControllerItem('slidePaneShowFirstSlide', '', focusObject); }
            if (matchKeyCode(event, 'END'))        { clearSlidesInMultiSelection(); docView.executeControllerItem('slidePaneShowLastSlide', '', focusObject); }
            if (matchKeyCode(event, 'UP_ARROW', { ctrlOrMeta: true }))   { docView.executeControllerItem('slide/moveslideupwards', getSelection(), focusObject); }
            if (matchKeyCode(event, 'DOWN_ARROW', { ctrlOrMeta: true })) { docView.executeControllerItem('slide/moveslidedownwards', getSelection(), focusObject); }
            if (matchKeyCode(event, 'UP_ARROW', { shift: true }))   { handleSelectionOneStepUpDown(true); keepFocusInSlidePane = true; }
            if (matchKeyCode(event, 'DOWN_ARROW', { shift: true })) { handleSelectionOneStepUpDown(false); keepFocusInSlidePane = true; }
            if (matchKeyCode(event, 'HOME', { shift: true })) { selectAllSlidesUpDownFromActiveSlide(true); keepFocusInSlidePane = true; }
            if (matchKeyCode(event, 'END', { shift: true })) { selectAllSlidesUpDownFromActiveSlide(false); keepFocusInSlidePane = true; }
            if (matchKeyCode(event, 'DELETE')) { docView.executeControllerItem('slide/deleteslide', '', focusObject); }
            if (matchKeyCode(event, 'BACKSPACE')) { docView.executeControllerItem('slide/deleteslide', '', focusObject); }

            if (matchKeyCode(event, 'Z', { ctrlOrMeta: true })) { docView.executeControllerItem('document/undo', '', focusObject); }
            if (matchKeyCode(event, 'Y', { ctrlOrMeta: true })) { docView.executeControllerItem('document/redo', '', focusObject); }

            if (keepFocusInSlidePane) { setFocus(slidePaneContainer); }

            if (isSelectAllKeyEvent(event)) { selectAllSlides(); }

            if (isClipboardKeyEvent(event)) {
                // copy and paste key combos needs to get through
                docModel.getSelection().setFocusIntoClipboardNode();
                docModel.grabClipboardFocus(docModel.getSelection().getClipboardNode());
            } else {
                // important!: needs event.preventDefault() for all keys, otherwise the focus may be lost
                event.preventDefault();
                event.stopPropagation();
            }
        }

        /**
         * Expand the selection for normal or layout slides to the first or last valid slide.
         * The start position it the currently active slide.
         *
         * @param {Boolean} upwards
         *  Whether the selection should be expanded up or downwards.
         */
        function selectAllSlidesUpDownFromActiveSlide(upwards) {
            var indexActiveSlide  = docModel.getSortedSlideIndexById(docModel.getActiveSlideId());
            var slidesToFirstOrLastPosition = upwards ? indexActiveSlide : docModel.getActiveViewCount() - indexActiveSlide - 1;
            _.times(slidesToFirstOrLastPosition, function () {
                handleSelectionOneStepUpDown(upwards, { setSelectionInGui: false });
            });
            // add classes for all selected slides
            setSelectedSlides();
        }
        /**
         * Select all slides in the slide pane. It's only possible in standard view.
         */
        function selectAllSlides() {
            // only in standard view!
            if (!docModel.isMasterView()) {
                self.setSlidePaneSelection(docModel.getStandardSlideOrder());
            }
        }

        /**
         * Expanding the selection for normal or layout slides one step up or downwards. Master slides are not
         * a valid target here, so only consider the next normal or layout slide.
         *
         * It's possible to suppress the final GUI update. This is useful, when this function is called
         * multiple times in a different function and the GUI update should be called there to prevent
         * unnecessary GUI updates.
         *
         *
         * @param {Boolean} upwards
         *  Whether the selection should be expanded up or downwards.
         *
         * @param {Object} [initOptions]
         *  Optional parameters:
         *  @param {Boolean} [initOptions.setSelectionInGui=true]
         *  Whether the GUI update, to make the new selected slides visible, should be called in this function or not.
         */
        function handleSelectionOneStepUpDown(upwards, initOptions) {

            var // the active slide id
                active = docModel.getActiveSlideId(),
                // whether the GUI should be updated in this function or not
                setSelectionInGui = getBooleanOption(initOptions, 'setSelectionInGui', true);

            function selectNextValidSlide(upwards, addToSelection) {

                var nextSlide;

                // remove the currently active slide from selection
                if (!addToSelection) {
                    slideSelection = _.without(slideSelection, active);
                }

                // find next slide that is not a master
                nextSlide = docModel.getNextValidSlideIndex(active, { upwards });

                // when a valid next slide was found set it active
                if (nextSlide !== -1) {
                    docView.executeControllerItem('slidePaneSetActiveSlide', docModel.getIdOfSlideOfActiveViewAtIndex(nextSlide), { preserveFocus: true });

                    if (addToSelection) {
                        // if not in array push it
                        if (slideSelection.indexOf(active) === -1) {
                            slideSelection.push(active);
                        }
                        // push the new slide if not in array
                        // Important: It MUST be used the new, currently active slide,
                        // so don't use the cached 'active' here!
                        if (slideSelection.indexOf(docModel.getActiveSlideId()) === -1) {
                            slideSelection.push(docModel.getActiveSlideId()); // new active
                        }
                    }
                }
            }

            // do nothing if the currently active slide is a master slide
            if (!docModel.isMasterSlideId(active)) {

                // upwards
                if (upwards) {
                    // add to selection when the active slide is above the lowest slide
                    if (Number(_.first(getSelection().sort(function (a, b) { return a - b; }), 1)) >= Number(convertIdsToIndex([active]))) {
                        // add
                        selectNextValidSlide(upwards, true);
                    } else {
                        // remove
                        selectNextValidSlide(upwards, false);
                    }
                } else {
                // downwards
                    // add to selection when the active slide is below the highest slide
                    if (Number(_.first(getSelection().sort(function (b, a) { return a - b; }), 1)) <= Number(convertIdsToIndex([active]))) {
                        // add
                        selectNextValidSlide(upwards, true);
                    } else {
                        // remove
                        selectNextValidSlide(upwards, false);
                    }
                }

                if (setSelectionInGui) {
                    // add classes for all selected slides
                    setSelectedSlides();
                }
            }
        }

        // Note: The delay should be lower than refreshLayoutDebounced() in view.js to prevent flickering while resizing with 'fit to width'.
        var resizeWindowHandlerDebounced = self.debounce(function () {

            if (docModel.isPresentationMode()) { return; }

            // temporary save the slide pane width in percent for the windowWidth before the resize
            var wpercent = getWidthSlidePaneInPercent();

            // must update the page width value here
            windowWidth = getWindowWidth();

            // set it to the same width in percent like before the windows resize
            setSizeSlidePaneInPercent(wpercent);
        }, { delay: 50 });

        /**
         * Observe the activeSlide in the model.
         *
         * @param {Object} viewCollector
         *  Object with information about the currently active and used slides.
         */
        function observeActiveSlide(viewCollector) {

            // update the slidePane GUI with the currently active slide
            guiSelectSlide(viewCollector.activeSlideId);
        }

        /**
         * Render the slidePane again when the activeView is changed.
         *
         * @param {Object} initOptions
         *  Optional parameters:
         *
         *  @param {Boolean} [initOptions.isMasterView=false]
         *   A flag if the normal or master/slide view is active.
         */
        function handleChangeActiveView(initOptions) {
            var
                isMasterView = getBooleanOption(initOptions, 'isMasterView', false);

            // TODO def value for force new
            renderSlidePane(isMasterView);
            // important to change the offset from the thumbnail depending on the activeView
            setNewSlidePaneOffset(isMasterView);
            // restore slide pane thumbnail size after rendering
            setSizeSlidePaneThumbnails(self.getSize()); // TODO
            // important: clear slide selection on change to layoutview/normalview
            slideSelection = [];

            handleVisibleSlideRangeNotification({ leaveMasterView: !isMasterView });
        }

        /**
         * Rebuild the Slide Pane from scratch for the current active view.
         */
        function rebuildSlidepane() {
            handleChangeActiveView({ isMasterView: docModel.isMasterView() });
        }

        // clear the multiselection
        function clearSlidesInMultiSelection() {

            var selectedSlideId = slidePaneContainer.find('.selected').attr('data-container-id');

            // case I: handle a normal multiselection
            if (multiSelectionActive()) {
                slideSelection = [];

                // check if undefined to prevent not valid values in the selection
                if (selectedSlideId) {
                    // add just the active slide to the selection again
                    slideSelection.push(selectedSlideId);
                }
            // case II (bug 50071): when no slide exist, clear the whole selection
            } else if (selectedSlideId === undefined) {
                slideSelection = [];
            }

            slidePaneContainer.find('.slide-container').removeClass('selection');
        }

        // clear the multiselection debounced
        var clearSlidesInMultiSelectionDebounced = self.debounce(clearSlidesInMultiSelection, { delay: 200 });

        // add classes for all selected slides
        function setSelectedSlides() {
            slidePaneContainer.find('.slide-container').removeClass('selection');
            // only add classes if we have a multiselection active
            if (multiSelectionActive()) {
                _.each(slideSelection,  function (id) { slidePaneContainer.children('div[data-container-id = "' + id + '"]').addClass('selection'); });
            }
        }

        // get the slide id from it's index in the slide pane // //TODO use this? getSortedSlideIndexById??
        function getSlideIdFromIndex(index) {
            return slidePaneContainer.find('.slide-container').eq(index).attr('data-container-id');

        }

        // y-position from the mouse/touch in the slide pane
        function calcPos(record) {
            return record.point.y + slidePaneContainer.scrollTop() - slidePaneContainer.offset().top;
        }

        function initTrackingObserver() {

            // if a drag and drop interaction is active at the moment
            let dragInteractionActiveFlag = false;
            // flag that indicates a possible drag&drop that should be checked later
            let pendingDragInteractionFlag = false;
            // flag that indicates clearing the selection should be checked later
            let pendingClearSelectionFlag = false;
            // if the drag and drop was started by mouse or touch
            let clickedByTouch;
            // y-position at drag start
            let dragStartPosY;
            // y-position while dragging
            let dragMovePosY;
            // the slide hight at drag start
            let currentSlideHeightPx = 0;
            // flag if there was a touchend before the touch hold to cancel the hold
            let touchHoldWasCanceled = false;

            function handleStart(record) {
                // set the virtual focus to the slidepane
                docView.setVirtualFocusSlidepane(true);
                // immediately disable further text input when clicking into side pane (57738)
                docModel.setBlockKeyboardEvent(true);

                // flag that indicates clearing the selection should be checked later
                pendingClearSelectionFlag = false;
                // flag that indicates a possible drag&drop that should be checked later
                pendingDragInteractionFlag = false;
                // check at first click if a context menu is open
                contextIsOpen = contextMenu.isVisible();

                // cancel the timer on start so that the last running timer is aborted (e.g. when clicking very fast on the slide)
                if (timer) { timer.abort(); }

                // the target that is clicked
                const selected = $(record.target).closest('.slide-container');
                // check that the tracking does not start on the scrollbar, it would start a unwanted drag&drop when scrolling (mousedown->drag scrollbar)
                const clickedOnSlideContainerElement = selected.is(slidePaneContainer.children('.slide-container'));

                // number of currently existing slides in the slide pane
                slideCount = slidePaneContainer.children('.slide-container').length;
                // reset the slideNumber for moving when starting a new drag interaction
                dragMoveSlideNumber = null;
                // set interaction type 'touch' or 'mouse' for this drag&drop interaction
                clickedByTouch = record.inputDevice === 'touch';
                // save y-position in the slide pane when drag starts
                dragStartPosY = calcPos(record);
                // the first drag move position is the start position, in case there is no movement (touchstart -> touchend)
                dragMovePosY = dragStartPosY;
                // get the current height at drag start for later calculations
                currentSlideHeightPx = slidePaneContainer.children('.slide-container').outerHeight(true); //TODO first?
                // get the slide where the drag starts
                dragStartSlideNumber = calcSlideNumberAtPosY(true, currentSlideHeightPx, dragStartPosY, slideCount);
                // it must be possible to calculate the position with 'calcSlideNumberAtPosY()' after the last slide
                // to insert slides after the last slide, but that would be not valid start position, so cancel a starting drag&drop in this case
                // also clear the multiselection when clicked below the last slide //TODO therefore maybe calc on drop?
                if (dragStartSlideNumber >= slideCount) { docModel.trigger('slidepane:clearselection'); return ''; }

                // [TOUCH]: to start drag&drop on TOUCH hold
                if (clickedByTouch) {

                    // check if drag&drop is forbidden, e.g. cancel drag & drop here when the user has no edit rights and no drag&drop on master slides
                    if (checkDragAndDropForbidden(selected)) { return; }

                    // reset the flag to init value
                    touchHoldWasCanceled = false;

                    // check if a long tap should be triggered after 750 ms
                    timer = self.executeDelayed(function () {

                        //if (debugSlidePane) { globalLogger.log('TOUCH IN HOLD', !touchHoldWasCanceled, dragStartPosY, dragMovePosY, ((Math.abs(dragStartPosY - dragMovePosY)) < 5), ((dragStartSlideNumber === dragMoveSlideNumber) || dragMoveSlideNumber === null)); }

                        //if the touch hold was not canceled before 750ms and the finger stays on the start slide (not moved more than 10 px, not moved outside or not moved at all)
                        if (!touchHoldWasCanceled && ((Math.abs(dragStartPosY - dragMovePosY)) < 5) && ((dragStartSlideNumber === dragMoveSlideNumber) || dragMoveSlideNumber === null)) {

                            dragInteractionActiveFlag = true;
                            setActiveSlideforClickedTarget(record.inputEvent, record.inputDevice); //or clickedBytouch TODO
                            slidePlacer.css({ top: calcSliderPlacerPosForSlide(dragStartSlideNumber, currentSlideHeightPx), visibility: 'visible'  });

                        }
                    }, 750);
                }

                const { shiftKey, ctrlKey, metaKey } = record.modifiers;
                const ctrlOrMeta = _.browser.MacOS ? metaKey : ctrlKey;

                // [MOUSE] + ctrl/cmd for multiselection
                if (!clickedByTouch && !IOS_SAFARI_DEVICE && ctrlOrMeta && clickedOnSlideContainerElement) {

                    // no multiselection if clicked slides is a master or when a master slide is active
                    if (!(isMasterSlideClicked(selected) || isMasterSlidesInSelection())) {
                        handleMultiSelectionOnClickedTarget(selected, dragStartSlideNumber);
                    }
                }

                // [MOUSE] + shift for multiselection
                if (!clickedByTouch && !IOS_SAFARI_DEVICE && shiftKey && clickedOnSlideContainerElement) { // TODO maybe block when meta is true?

                    // no multiselection if clicked slides is a master or when a master slide is active
                    if (!(isMasterSlideClicked(selected) || isMasterSlidesInSelection())) {
                        selectSlideRangeFromActiveSlide(dragStartSlideNumber);
                    }
                }

                // [MOUSE] handler for a normal mousedown in the slidePane  //TODO ios mouse comment and OSX
                if (!clickedByTouch && !IOS_SAFARI_DEVICE && !ctrlOrMeta && !shiftKey && clickedOnSlideContainerElement) {

                    if (multiSelectionActive()) {
                        // case 1) in a multiselection: only clear the selection and change the active slide when the
                        // target is NOT a selected slide or active slide
                        if (!(selected.hasClass('selection') || selected.hasClass('selected'))) {
                            docModel.trigger('slidepane:clearselection');
                            setActiveSlideforClickedTarget(record.inputEvent, record.inputDevice); //or clickedBytouch TODO

                        // case 2) in a multiselection: when we have a active selection and click on a selected slide, the
                        // selection MUST be cleared on "end" (therefore pending), otherwise drag&drop is not possible
                        } else {
                            pendingClearSelectionFlag = true;
                        }

                    // case 3) no multiselection: clicking on the slide should select it
                    } else {
                        setActiveSlideforClickedTarget(record.inputEvent, record.inputDevice); //or clickedBytouch TODO
                    }

                    // check if drag&drop is forbidden, e.g. cancel drag & drop here when the user has no edit rights and no drag&drop on master slides
                    if (checkDragAndDropForbidden(selected)) { return; }

                    // important: we have a pending drag interaction in every three cases from above
                    pendingDragInteractionFlag = true;
                }
            }

            function handleMove(record) {
                // position while moving
                dragMovePosY = calcPos(record);
                // we need it for later cases
                dragMoveSlideNumber = calcSlideNumberAtPosY(clickedByTouch, currentSlideHeightPx, dragMovePosY, slideCount);

                // only process moving when a drag&drop interaction is ongoing or pending
                if (dragInteractionActiveFlag || pendingDragInteractionFlag) {

                    // stops scroll on TOUCH while dragging
                    record.inputEvent.preventDefault();

                    // move the sliderPlacer to the top or bottom of a slide at the current position (for touch only top position)
                    slidePlacer.css({ top: calcSliderPlacerPosForSlide(dragMoveSlideNumber, currentSlideHeightPx) });

                    // MOUSE: make sliderPlacer visible for MOUSE interaction after the cursor has moved 5px
                    if (!clickedByTouch && !IOS_SAFARI_DEVICE && (Math.abs(dragStartPosY - dragMovePosY)) > 5) {
                        slidePlacer.css({ visibility: 'visible' });
                        // dragInteraction is not active until the slide really moved by mouse
                        dragInteractionActiveFlag = true;
                    }
                }
            }

            function handleScroll(record) {
                // only process if a drag&drop interaction is ongoing
                if (dragInteractionActiveFlag) {
                    slidePaneContainer.scrollTop(slidePaneContainer.scrollTop() + record.scroll.y);

                    // scrolling changes the position in the slide pane, therefore we need to update the dragMoveSlideNumber
                    dragMoveSlideNumber = calcSlideNumberAtPosY(clickedByTouch, currentSlideHeightPx, calcPos(record), slideCount);
                    // move the sliderPlacer to the top or bottom of a slide at the current position (for touch only top position)
                    slidePlacer.css({ top: calcSliderPlacerPosForSlide(dragMoveSlideNumber, currentSlideHeightPx) });
                }
            }

            function handleEnd(record) {
                // set flag to cancel the touch hold
                touchHoldWasCanceled = true;
                // there can't be a drag interaction after "end" so set to false again
                //pendingDragInteractionFlag = false;

                // very important to prevent ghost clicks on touch (jquery mobile)
                // it must be prevented on touchend, because on touchstart it would disable scrolling
                record.inputEvent.preventDefault();

                // use the last dragMoveSlideNumber, when there was no move event use the start position
                var dragEndSlideNumber = (dragMoveSlideNumber === null) ? dragStartSlideNumber : dragMoveSlideNumber;

                // TOUCH: when no drag interaction was active, check on touchend if the context menu should be opened
                // (not when moved more than 5px and if the touch start is more than 500ms in the past)

                //console.log('tapp', !dragInteractionActiveFlag, clickedByTouch, ((Math.abs(dragStartPosY - dragMovePosY)) < 5), (Math.abs(event.timeStamp - event.startTime))); //TODO
                if (!dragInteractionActiveFlag && clickedByTouch && ((Math.abs(dragStartPosY - dragMovePosY)) < 5) && (record.duration < 500)) {

                    setActiveSlideforClickedTarget(record.inputEvent, record.inputDevice);

                    if (doubleTouchOnSelectedTarget && (dragStartSlideNumber === dragEndSlideNumber)) {

                        // check if there is a context menu open, when not trigger it
                        if (!contextIsOpen) {
                            // open context menu for the slide pane
                            SlidePaneContextMenu.triggerShowEvent(slidePaneContainer, record.inputEvent, { stickToSlide: true });
                        }
                    }
                }

                // resolves the pending clear selection on mouseup, when clicked on a already selected slide in a multiselection
                if (!dragInteractionActiveFlag && pendingClearSelectionFlag) {
                    docModel.trigger('slidepane:clearselection');
                    setActiveSlideforClickedTarget(record.inputEvent, record.inputDevice); //or clickedBytouch TODO
                }

                // when a drag interaction was active, finalize the drag & drop interaction here
                if (dragInteractionActiveFlag) {
                    slidePlacer.css({ visibility: 'hidden' });
                    docView.executeControllerItem('slide/move', { indexStart: getSelection(), indexEnd: dragEndSlideNumber }, { preserveFocus: true }); // TODO preservefocus here or not?
                }
            }

            function handleCancel() {
                slidePlacer.css({ visibility: 'hidden' });
                // set flag to cancel the touch hold
                touchHoldWasCanceled = true;
            }

            function handleFinally() {
                // drag & drop finished
                dragInteractionActiveFlag = false;
                // enabling text input again (57738)
                docModel.setBlockKeyboardEvent(false);
            }

            // initialize tracking observer for the slide elements
            const observer = self.member(new TrackingObserver({
                start: handleStart,
                move: handleMove,
                scroll: handleScroll,
                end: handleEnd,
                cancel: handleCancel,
                finally: handleFinally
            }));

            // start to observe the slide elements
            observer.observe(slidePaneContainer[0], {
                passiveTouch: false, // code combines native scrolling and drag&drop
                scrollDirection: "vertical",
                scrollMinSpeed: 5,
                scrollMaxSpeed: 300,
                scrollAcceleration: 1.5,
                scrollAccelBand: [-30, 30] // start scrolling before reaching the container borders
            });
        }

        // pos in px for a given slide
        function calcTopPosForSlide(slideNumber, slideHeight) {
            var topPosSlide = ((slideNumber * slideHeight) - slidePaneContainer.scrollTop());

            // MASTER/LAYOUT VIEW: we need to correct the offset between differently scaled master and layout
            // slides to get the centered position between them (e.g. take a look at the slidePlacer position while drag&drop)
            if (docModel.isMasterView()) {

                var slideContainer = slidePaneContainer.children('.slide-container');

                // when we are on a layout slide before a master slide - additionally the first slide MUST not be corrected
                if (slideContainer.eq(slideNumber).hasClass('master') && slideNumber !== 0) {
                    //topPosSlide =  topPosSlide - (5 * factor);
                    topPosSlide -= (5 * factor);
                }
                // when we are on a layout slide slide after a master slide
                if (slideContainer.eq(slideNumber - 1).hasClass('master')) {
                    //topPosSlide =  topPosSlide + (5 * factor);
                    topPosSlide += (5 * factor);
                }
            }

            return topPosSlide;
        }

        /**
         * Check if drag&drop is forbidden. Cases were it is forbidden are:
         * - cancel drag & drop when the user has no edit rights
         * - no drag&drop on master slides
         * - when it's a ODF document completely in the masterview
         */
        function checkDragAndDropForbidden(selected) {
            return !app.isEditable() || isMasterSlideClicked(selected) || (app.isODF() && docModel.isMasterView());
        }

        // Calculate the position for the slidePLacer. It must be offset by -1px
        // for all but the first slide to be centered perfectly between two slides.
        // Otherwise it would be placed exactly at the slide border, which looks wrong.
        function calcSliderPlacerPosForSlide(slideNumber, slideHeight) {
            // return the top position from the given slideNumber, corrected by a offset for all but the first slide
            return calcTopPosForSlide(slideNumber, slideHeight) + (slideNumber <= 0 ? 0 : -1);
        }

        function calcSlideNumberAtPosY(useSlideBorderToCalc, slideHeight, posY, slideCount) {
            // use the top/bottom or the middle from the slide to calculate the slide number
            var slideNumber = useSlideBorderToCalc ? Math.floor(posY / slideHeight) : Math.round(posY / slideHeight);

            // limit to first and last slide number to get valid values
            if (slideNumber < 0) { slideNumber = 0; }
            if (slideNumber > slideCount) {
                slideNumber = slideCount;
            }

            return slideNumber;
        }

        // returns if we currently have a multiselection
        function multiSelectionActive() {
            return (slideSelection.length > 1);
        }

        // returns if a master slide in in the current selection //TODO maybe model functions and not jquery
        function isMasterSlidesInSelection() {
            return slidePaneContainer.find('.selected').hasClass('master');
        }

        // returns if the target is a master slide
        function isMasterSlideClicked(selected) {  //get TODO doppelt? checkSlideAtIndexIsMaster?? maybe model functions and not jquery
            return selected.hasClass('master');
        }

        // handle clicks on a target slide (Selected = dom, dragStartSlideNumber = index) and decides
        // if a selection should be cleared completely, deleted or added for the target slide
        function handleMultiSelectionOnClickedTarget(selected, dragStartSlideNumber) {
            // deselect
            if (selected.hasClass('selected')) {
                if (multiSelectionActive()) {
                    slideSelection = _.without(slideSelection, getSlideIdFromIndex(dragStartSlideNumber));
                    docView.executeControllerItem('slidePaneSetActiveSlide', String(_.last(slideSelection, 1)), { preserveFocus: true });
                }

            } else {
                // if not in array push it
                if (slideSelection.indexOf(getSlideIdFromIndex(dragStartSlideNumber)) === -1) {
                    slideSelection.push(getSlideIdFromIndex(dragStartSlideNumber));
                    docView.executeControllerItem('slidePaneSetActiveSlide', getSlideIdFromIndex(dragStartSlideNumber), { preserveFocus: true });

                // if in object delete it
                } else {
                    slideSelection = _.without(slideSelection, getSlideIdFromIndex(dragStartSlideNumber));
                }
            }
            // add classes for all selected slides
            setSelectedSlides();
        }

        // Selects a range of slides from the active slide to the given index ('dragStartSlideNumber').
        // Master slides are not selected when they are in between the start and end point.
        function selectSlideRangeFromActiveSlide(dragStartSlideNumber) {

            var // all slide-containers in the active view
                containers = slidePaneContainer.children('.slide-container'),
                // slide index were the selection starts
                selectionStart = null,
                // marker for selection end
                selectionMarker = dragStartSlideNumber;

            /* if (false) {
                // give the last element to selectionStart as start as Index
                selectionStart = containers.filter(_.last(slideSelection, 1)).index();
            } else {*/

            // index from the active slide
            selectionStart = containers.filter('.selected').index();

            // upwards
            if (selectionStart > selectionMarker) {
                while (selectionStart >= selectionMarker) {

                    if (slideSelection.indexOf(getSlideIdFromIndex(selectionStart)) !== -1) {
                        slideSelection = _.without(slideSelection, getSlideIdFromIndex(selectionStart));
                    }
                    // don't add it when the index is a master slide
                    if (!checkSlideAtIndexIsMaster(selectionStart)) {
                        slideSelection.push(getSlideIdFromIndex(selectionStart));
                        containers.eq(selectionStart).addClass('selection'); // TODO setSelectedSlides(); ?
                    }
                    selectionStart--;
                }
            // downwards
            } else {
                while (selectionStart <= selectionMarker) {
                    if (slideSelection.indexOf(getSlideIdFromIndex(selectionStart)) !== -1) {
                        slideSelection = _.without(slideSelection, getSlideIdFromIndex(selectionStart));
                    }
                    // don't add it when the index is a master slide
                    if (!checkSlideAtIndexIsMaster(selectionStart)) {
                        slideSelection.push(getSlideIdFromIndex(selectionStart));
                        containers.eq(selectionStart).addClass('selection'); // TODO setSelectedSlides(); ?
                    }
                    selectionStart++;
                }
            }

            docView.executeControllerItem('slidePaneSetActiveSlide', getSlideIdFromIndex(dragStartSlideNumber), { preserveFocus: true });
            // add classes for all selected slides
            setSelectedSlides();
        }

        // check if the slide at the given index is a master // TODO CHECK MODEL is exist if not move to model
        function checkSlideAtIndexIsMaster(index) {
            if (docModel.isMasterSlideId(docModel.getIdOfSlideOfActiveViewAtIndex(index))) {
                return true;
            } else {
                return false;
            }
        }

        // convert an selection array with IDs to index positions and return it
        function convertIdsToIndex(slideSelection) {

            var arrayIndex = [];
            // old   _.each(slideSelection, function (id) { arrayIndex.push(slidePaneContainer.children('div[data-container-id = "' + id + '"]').index()); });
            _.each(slideSelection, function (id) {
                var slideIndex = docModel.getSortedSlideIndexById(id);
                if (slideIndex > -1) { arrayIndex.push(slideIndex); }
            });

            return arrayIndex;
        }

        /**
         * Returning the slide selection in the slide pane.
         *
         * @returns {Number[]}
         *  An Array containing the position as index from all selected slides in the slide pane.
         */
        function getSelection() {
            return convertIdsToIndex(slideSelection);
        }

        // function refreshSlideRangeVisibility(firstVisibleIndex, lastVisibleIndex) {
        //     var
        //         $children = slidePaneContainer.children(),
        //         itemCount = $children.length,
        //         idx       = (firstVisibleIndex - 1);
        //
        //     while (++idx <= lastVisibleIndex) {
        //         $children.eq(idx).css('visibility', '');
        //     }
        //     idx = -1;
        //
        //     while (++idx < firstVisibleIndex) {
        //         $children.eq(idx).css('visibility', 'hidden');
        //     }
        //     idx = lastVisibleIndex;
        //
        //     while (++idx < itemCount) {
        //         $children.eq(idx).css('visibility', 'hidden');
        //     }
        // }

        function getVisibleSlideRange() {
            var
                $elm          = slidePaneContainer,
                elm           = $elm[0],

                boxHeight     = elm.offsetHeight,
                scrollHeight  = elm.scrollHeight,
                scrollTop     = elm.scrollTop,

                $children     = $elm.children(),

                itemCount     = $children.length,
                itemHeight,

                scrollBottom,

                firstIndex,
                lastIndex,

                slideRange    = [];

            if (itemCount) {
                itemHeight    = $children[itemCount - 1].offsetHeight; // last item's height

                scrollBottom  = (scrollHeight - boxHeight - scrollTop);

                firstIndex    = Math.floor(scrollTop / itemHeight);
                lastIndex     = itemCount - 1 - Math.floor(scrollBottom / itemHeight);

                slideRange    = [firstIndex, lastIndex];
            }
            //  if (debugSlidePane) {
            //    globalLogger.log('+++ on scroll +++ getVisibleSlideRange - boxHeight, scrollHeight, scrollTop, scrollBottom, itemCount, itemHeight : ', boxHeight, scrollHeight, scrollTop, scrollBottom, itemCount, itemHeight);
            //  }
            //  if (debugSlidePane) {
            //    globalLogger.log('+++ on scroll +++ getVisibleSlideRange - firstIndex, lastIndex : ', firstIndex, lastIndex);
            //  }
            return slideRange;
        }

        function getSlideIdCastList() {
            var
                castList = [];

            if (currentSlideRange.length === 2) {
                var
                    slideIdList       = docModel.isMasterView() ? docModel.getMasterSlideOrder() : docModel.getStandardSlideOrder(),
                    slideIdCount      = slideIdList.length,

                    rangeStart        = currentSlideRange[0],
                    rangeEnd          = currentSlideRange[1],

                    lowerBufferIndex  = Math.max(0, (rangeStart - SLIDE_RANGE__BUFFER_SIZE)),
                    upperBufferIndex  = Math.min((slideIdCount - 1), (rangeEnd + SLIDE_RANGE__BUFFER_SIZE));

                castList = slideIdList.slice(lowerBufferIndex, (upperBufferIndex + 1));
            }
            return castList;
        }

        function getSlideRangeState() {
            var
                currentRangeStart = currentSlideRange[0],
                currentRangeEnd   = currentSlideRange[1],
                recentRangeStart  = recentSlideRange[0],
                recentRangeEnd    = recentSlideRange[1],

                slideRangeState   = {
                    slideRanges:  {

                        current:  [currentRangeStart, currentRangeEnd],
                        recent:   [recentSlideRange[0], recentSlideRange[1]]
                    },
                    slideIdLists: {

                        complete:   [], // slideIdList.
                        visible:    [], // visibleSlideIdList.
                        added:      []  // visiblyAddedSlideIdList.
                        //takeOut:    [],
                        //takeIn:     []
                    }
                },
                slideIdLists = slideRangeState.slideIdLists;

            if (currentSlideRange.length === 2) {

                slideIdLists.complete = docModel.isMasterView() ? docModel.getMasterSlideOrder() : docModel.getStandardSlideOrder();                    // slideIdList.
                slideIdLists.visible  = slideIdLists.complete.slice(currentRangeStart, (currentRangeEnd + 1));                                    // visibleSlideIdList.
                slideIdLists.added    = _.difference(slideIdLists.visible, slideIdLists.complete.slice(recentRangeStart, (recentRangeEnd + 1)));  // visiblyAddedSlideIdList.
            }
            return slideRangeState;
        }

        /**
         * Triggering an event, if the visible slide range has changed. If a slide was
         * inserted or removed, the event 'visibleslidecount:changed' is triggered. In
         * all other cases 'visiblesliderange:changed'.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.triggerslidecount=false]
         *      If set to true, the event 'visibleslidecount:changed' will be triggered, not
         *      the default event 'visiblesliderange:changed'. This is necessary, because only
         *      the current range needs to be updated (52993), but no further formatting
         *      is required.
         *  @param {Boolean} [options.leaveMasterView=false]
         *      If set to true, the event triggers the information, that the document view is
         *      rendered and therefore a complete rerendering of comment counter nodes is
         *      required, too (DOCS-2181).
         *
         */
        function handleVisibleSlideRangeNotification(options) {
            var
                slideRange  = getVisibleSlideRange(),

                firstIndex  = slideRange[0],
                lastIndex   = slideRange[1],

                currentFirstIndex = currentSlideRange[0],
                currentLastIndex  = currentSlideRange[1],
                // whether the slide count event must be triggered
                triggerSlideCountEvent = getBooleanOption(options, 'triggerslidecount', false),
                // whether the complete slide pane is rendered from scratch
                leaveMasterView = getBooleanOption(options, 'leaveMasterView', false);

            if ((slideRange.length === 2) && ((firstIndex !== currentFirstIndex) || (lastIndex !== currentLastIndex))) {
                recentSlideRange  = [currentFirstIndex, currentLastIndex];
                currentSlideRange = [firstIndex, lastIndex];
                self.trigger((triggerSlideCountEvent ? 'visibleslidecount:changed' : 'visiblesliderange:changed'), getSlideRangeState(), leaveMasterView);
            } else if (leaveMasterView) {
                self.trigger('visiblesliderange:changed', getSlideRangeState(), leaveMasterView);
            }
        }

        function handleInitialPreviewThumbRendering() {
            //if (debugSlidePane) { globalLogger.log('+++ slidepane :: handleInitialPreviewThumbRendering +++ '); }

            handleVisibleSlideRangeNotification();
        }

        function handleSlideSizeChange(data) {
            //if (debugSlidePane) { globalLogger.log('+++ "slidesize:change" +++ [evt, data] : ', evt, data); }

            // init values for the slide pane to a given slide ratio
            calcSlidePaneDimensionsForSlideRatio(data.slideRatio, docModel.isMasterView());

            computeInitialPreviewScaling(); // recalculate any preview's css scaling value `css_value__preview_scale`

            // set initial slide pane width ...
            setSizeSlidePaneThumbnails(self.getSize()); // -> intial width was already set in 'setSidePaneWidthDuringLoad'

            //  /**
            //   *  the next two lines are supposed to be the best solution from a model point of view.
            //   *
            //   *  nevertheless due to performance issues not only a container's size/width measures need to be reassigned
            //   *  but also some of its content css values (that of course are tasks that still belong to and will be continued
            //   *  to be handled by the slidepreview mixin)
            //   */
            //  currentSlideRange = [0, 0];             //  - reset to initial state in order to retrieve properly
            //  handleVisibleSlideRangeNotification();  //    rerendered previews independently from the current
            //                                          //    view and scroll state

            // ... do also apply the new scaling value to each preview within its new width/height context.
            slidePaneContainer.find('.slideContent').children('.page').toArray().forEach(function (elm) {

                $(elm).css(this);             // - apply current page size context provided by invoking `getCurrentPageSize`.
                applyPreviewScaling($(elm));  // - apply the new scaling value.

            }, (function getCurrentPageSize() {
                var
                    $pageNode = docModel.getNode(),
                    elmPage   = $pageNode[0];

                return {
                    width:  (elmPage.offsetWidth  + 'px'),
                    height: (elmPage.offsetHeight + 'px')
                };

            }()));

            forceRerenderSlidePaneThumbnailsDebounced();
        }

        /**
         * Initialization from the SlidePane. The activeSlide and activeView is set directly by the event.
         *
         * Info: The width of the slide pane was already set during 'updateDocumentFormatting' to avoid
         *       jumping of the already visible slide, when the slide pane is inserted.
         *
         * @param {Object} initOptions
         *  Optional parameters:
         *
         *  @param {Boolean} [initOptions.activeSlideId]
         *   The currently active slide, at the moment the SlidePane was invoked by the source event.
         *
         *  @param {Boolean} [initOptions.isMasterView=false]
         *   A flag if the normal or master/slide view is active, at the moment the SlidePane was invoked by the source event.
         *
         */
        function initSlidePane(initOptions) {

            var // the activeSlide id
                activeSlideId = getStringOption(initOptions, 'activeSlideId'),
                // if normal oder master/slide view is active
                isMasterView = getBooleanOption(initOptions, 'isMasterView', false),
                // slide ratio from the loaded presentation
                slideRatio = getNumberOption(initOptions, 'slideRatio');

            // init values for the slide pane to a given slide ratio
            calcSlidePaneDimensionsForSlideRatio(slideRatio, isMasterView);

            computeInitialPreviewScaling();

            // init the windows width
            windowWidth = getWindowWidth();

            //init important listeners
            initListener();

            // render the html
            renderSlidePane(isMasterView, true);

            // set initial slide pane width
            setSizeSlidePaneThumbnails(self.getSize()); // -> intial width was already set in 'setSidePaneWidthDuringLoad'

            // since there is no change:slide' event after the initialization, the selection must be set here initially.
            guiSelectSlide(activeSlideId);

            contextMenu = self.member(new SlidePaneContextMenu(docView, slidePaneContainer));
        }

        /**
         * Initialization from event listener after initialization.
         */
        function initListener() {

            // slide size has changed - e.g. orientation change.
            self.listenTo(docModel, 'slidesize:change', handleSlideSizeChange);

            // slide change
            self.listenTo(docModel, 'change:slide', observeActiveSlide);

            // activeView change (between normal and master/layout)
            self.listenTo(docModel, 'change:activeView', handleChangeActiveView);
            self.listenTo(docModel, 'moved:slide', handleMovedSlide);

            // update the view after insert/remove slide
            self.listenTo(docModel, 'inserted:slide', handleInsertSlide);
            self.listenTo(docModel, 'removed:slide', handleDeleteSlide);

            // when a standard slide gets applied a different layout template
            self.listenTo(docModel, 'change:layoutslide:view-agnostic', updateMasterLayoutTooltips);

            // when a slide is set hidden/unhidden
            self.listenTo(docModel, 'change:slideAttributes', renderSlideHidden);
            self.listenTo(docView, 'previewupdate:after', updatePreviewThumbnails);

            // handle key events on the slidepane (when focused)
            self.listenTo(slidePaneContainer, 'keydown', keyHandler);

            // this is important mostly for Safari, to get cut, copy, paste events on slidepane
            self.listenTo(slidePaneContainer, 'beforecopy beforecut beforepaste', function () {
                docModel.getSelection().setFocusIntoClipboardNode();
                docModel.grabClipboardFocus(docModel.getSelection().getClipboardNode());
            });

            // mousemove have to be preventDefault() or the focus can be lost on some mouse interactions on the pane
            self.listenTo(slidePaneContainer, 'mousemove', function (e) { e.preventDefault(); });

            // when a slide is clicked with the right mouse button set the focus to the slide pane and select the slide TODO check contextmenu is opened in view
            self.listenTo(slidePaneContainer, 'contextmenu', function (e) {
                // not on compact devices
                if (!COMPACT_DEVICE) {

                    if (isElementNode($(e.target), '.slide-pane-container')) {
                        docModel.trigger('slidepane:clearselection');
                        setFocus(slidePaneContainer);

                        // set the virtual focus to the slidepane
                        docView.setVirtualFocusSlidepane(true);

                        docView.executeControllerItem('slidePaneSetLastSlideActive', '', { preserveFocus: true });
                    } else {

                        if (!($(e.target).closest('.slide-container').hasClass('selection'))) {
                            docModel.trigger('slidepane:clearselection');
                            setFocus(slidePaneContainer);

                            // set the virtual focus to the slidepane
                            docView.setVirtualFocusSlidepane(true);

                            setActiveSlideforClickedTarget(e, 'mouse');
                        }
                    }
                }
            });

            self.listenTo(docModel, 'slidepane:clearselection', function () {
                clearSlidesInMultiSelection();
            });

            // To calculate and set the thumbnail size according to the pane width when the pane is resized.
            // When the pane is resized, the thumbnails need to be scaled accordingly.
            self.on('pane:resize', function () {
                setSizeSlidePaneThumbnails(self.getSize());
                handleVisibleSlideRangeNotification();
                forceVisibleThumbnailsDebounced();
                forceRerenderSlidePaneThumbnailsDebounced();
            });

            // save size of slide pane in the document after resizing manually
            self.on("pane:resize:tracking:end", () => {
                docView.executeControllerItem('slidePaneSetSize', getWidthSlidePaneInPercent());
            });

            // scale the slide pane width in relation to the browser size
            if (!COMPACT_DEVICE) {
                self.listenTo($(window), 'resize', resizeWindowHandlerDebounced);
            }

            // slide pane layout according to the orientation
            if (COMPACT_DEVICE) {
                self.listenTo($(window), 'orientationchange', function () {

                    // must update the page width value here
                    windowWidth = getWindowWidth();

                    setSizeSlidePaneInPercent(getWidthOnCompactDevices());
                });
            }

            // handle clicks on the slide in the slidePane
            // Forms.touchAwareListener(slidePaneContainer, setActiveSlideforClickedTarget);
            // self.listenTo(slidePaneContainer, 'touchstart', setActiveSlideforClickedTarget);

            // setting the virtual focus, when the slide pane gets the focus (for example after Strg-F6, DOCS-4430)
            self.listenTo(slidePaneContainer, "focusin", () => { docView.setVirtualFocusSlidepane(true); });

            forceVisibleThumbnailsDebounced = self.debounce(forceVisibleThumbnails, { delay: 400 });
            forceRerenderSlidePaneThumbnailsDebounced = self.debounce(forceRerenderSlidePaneThumbnails, { delay: 500 });
            handleVisibleSlideRangeNotificationDebounced = self.debounce(handleVisibleSlideRangeNotification, { delay: 200 });

            slidePaneContainer[0].addEventListener('scroll', forceVisibleThumbnailsDebounced, false);
            slidePaneContainer[0].addEventListener('scroll', handleVisibleSlideRangeNotificationDebounced, false);

            // initialize tracking observer for dragging slides around
            initTrackingObserver();

            // listener for repaint slidepane after applying Snapshot, eg. when cancelling duplicate slides
            self.listenTo(docModel, 'document:reloaded:after', rebuildSlidepane);

            self.on('visiblesliderange:changed visibleslidecount:changed', handlePreviewPresence);
        }

        /**
         * Getting the slidePaneContainer.
         *
         * @returns {jQuery}
         *   The slidePaneContainer as a jQuery object.
         */
        this.getSlidePaneContainer = function () {
            return slidePaneContainer;
        };

        /**
         *  Getting the `slideId` specific slide-preview-container of the slide-pane.
         *
         * @returns {jQuery}
         *   The `slideId` specific slide-preview-container as a jQuery object.
         */
        this.getSlideContainerById = function (slideId) {
            return slidePaneContainer.children('[data-container-id="' + slideId + '"]').eq(0);
        };

        /**
         * Setting the side pane width in an early state during loading the document.
         * This is necessary during 'updateDocumentFormatting' to avoid jumping of the
         * already visible slide, when the slide pane is inserted.
         *
         * @param {Number} width
         *  The slide pane width from the document, on COMPACT_DEVICES use a fixed width.
         */
        this.setSidePaneWidthDuringLoad = function (width) {
            width = COMPACT_DEVICE ? getWidthOnCompactDevices() : width; //TODO maybe rename to slidePaneWidth
            self.setSize(Math.round(width * getWindowWidth() / 100));
        };

        this.getVisibleSlideRange = function () {
            return getVisibleSlideRange();
        };

        this.getSlideRangeState = function () {
            return getSlideRangeState();
        };

        /**
         * Returning the slide selection in the slide pane.
         *
         * @returns {Number[]}
         *  An Array containing the position as index from all selected slides in the slide pane.
         */
        this.getSlidePaneSelection = function () {
            return getSelection();
        };

        /**
         * Public method to set the slide pane selection to an array of slide ids.
         *
         * @param {Array} slideIdCollection
         *  Collection of slide Ids that set the slides in the slide pane as selected.
         */
        this.setSlidePaneSelection = function (slideIdCollection) {
            slideSelection = slideIdCollection;
            // add classes for all selected slides
            setSelectedSlides();
        };

        /**
         * Public method to save the slide pane scroll position.
         * (e.g. it's needed when the tap was changed from the document tab to the portal tab and back)
         *
         * @returns {Number}
         *  The vertical scroll position of the slide pane.
         */
        this.saveSlidePaneScrollPosition = function () {
            slidePaneScrollPosition = slidePaneContainer.scrollTop();
            return slidePaneScrollPosition;
        };

        /**
         * Public method to restore the slide pane scroll position
         * (e.g. it's needed when the tap was changed from the document tab to the portal tab and back).
         *
         * The optional parameter can be used to specify a scrollPosition. If it
         * is not specified, the local available scroll position is used.
         *
         * @param {Number} [scrollPos]
         *  An optional vertical scroll position. If this is specified, it is used. If not, the saved
         *  slide pane scroll position is used.
         */
        this.restoreSlidePaneScrollPosition = function (scrollPos) {
            slidePaneContainer.scrollTop(_.isNumber(scrollPos) ? scrollPos : slidePaneScrollPosition);
        };

        /**
         * Getting the handler function that is used for processing keydown // TODO
         * events in the slide pane.
         *
         * @returns {Function}
         *  The handler function for the 'keydown' event.
         */
        this.getSlidepaneKeyDownHandler = function () {
            return keyHandler;
        };

        // initialization -----------------------------------------------------

        // add the main HTML container to the slidePane
        this.$el.append(slidePaneContainer, slidePlacer);
        //$('#io-ox-windowmanager-pane').append(slidePlacer);

        // needed for Safari to get beforepaste event
        this.$el.toggleClass('safari-userselect-text', _.browser.Safari && !IOS_SAFARI_DEVICE);

        // init the slidepane
        this.listenTo(docModel, 'slideModel:init', initSlidePane);

        // render preview item into each visible slide item of slidepane
        // when all slide previews are ready
        this.listenTo(docView, 'slidepreview:init:after', handleInitialPreviewThumbRendering);
    }
}
