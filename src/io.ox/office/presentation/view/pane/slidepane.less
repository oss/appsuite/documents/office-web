/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";

/* ========================================================================= */
/* class SlidePane                                                           */
/* ========================================================================= */

.io-ox-office-main.io-ox-office-presentation-main .view-pane.slide-pane {

    .slide-pane-container {
        display: flex;
        flex-direction: column;
        overflow-y: auto;
        height: 100%;
        outline: none;
        -webkit-overflow-scrolling: touch; //momentum-based scrolling ios
        -webkit-user-select: none; // prevent Safari from selecting a text range - see #61474

        // automatic CSS counter for slide indexes
        counter-reset: slide-index;

        // offset bottom to enable the user to insert slides under the last slide with touch
        &::after {
            content: "";
            height: 10px;
        }

        &:focus-visible { // drawing a dotted border around the slide pane after Strg-F6
            &::after {
                .absolute-with-distance(0px, 4px, 0px, 0px);
                border: 2px dotted;
                height: 100%;
                pointer-events: none;
            }
        }

        > .slide-container {
            display: flex;
            position: relative;
            padding: calc(2 * var(--distance-v)) var(--distance-h);
            cursor: pointer;

            > .left-section {
                display: flex;
                flex-direction: column;
                flex: 0 0 auto;
                width: 35px;
                align-self: start;
                text-align: center;

                > .slide-index::before {
                    counter-increment: slide-index;
                    content: counter(slide-index);
                }
            }

            > .main-section {
                display: flex;
                flex-grow: 1;
                align-self: center;
                position: relative;
                margin-left: var(--caption-spacing);

                > .slidePaneThumbnail {
                    box-shadow: 0 0 0 1px var(--border);
                    overflow: hidden;
                    display: inline-block;
                    background-color: white; // place holder to see white slide until we have real slide images
                    pointer-events: none;

                    > .slideContent {
                        transform: scale(1);
                        transform-origin: 0 0;
                        border: none;
                        height: 100%;

                        // DOCS-56: User can see previews in slide sorter
                        .page {
                            -webkit-user-select: none; // prevent Safari from selecting a text range - see #61474
                            box-shadow: none;

                            div.p.slide {
                                width: 100% !important;
                                height: 100% !important;
                            }

                            .drawing {
                                &.absolute {
                                    z-index: 1;
                                }
                                > .selection {
                                    display: none;
                                }
                                > .content > .canvasgeometry {
                                    position: absolute;
                                    top: 0;
                                    right: 0;
                                    bottom: 0;
                                    left: 0;
                                }
                            }
                        }
                    }
                }

                .hidden-slide-icon {
                    display: none;
                }
            }

            // selected (active) slide, or multi-selection of slides
            &.selected, &.selection {
                background: var(--listmenu-selected-fill-color);
                color: var(--listmenu-selected-text-color);

                .slidePaneThumbnail {
                    --thumbnail-border-color: var(--accent);
                    box-shadow: 0 0 0 3px var(--thumbnail-border-color) !important;

                    :root.dark & {
                        --thumbnail-border-color: var(--background);
                    }
                }
            }

            // hidden slide that will be skipped in a presentation
            &.hiddenSlide {

                // hidden slide that will be skipped in a presentation
                .hidden-slide-icon {
                    display: block;
                    position: absolute;
                    top: 1px;
                    left: 1px;
                    padding: 1px 3px;
                    // always black on white, also in dark mode (icon is on top of slides)
                    background: fade(white, 50%);
                    color: black;

                    [data-icon-id] {
                        width: 1rem;
                        height: 1rem;
                    }
                }

                .slideContent {
                    opacity: 0.5;
                }
            }

            // do not show slide indexes and comment count in layout/master view
            &.layout, &.master {
                > .left-section { display: none; }
            }
        }

        // Bug 44952: template text and border hidden in slide pane
        &.standard-view .slidePaneThumbnail > .slideContent .page {

            > div.masterslidelayer,
            > div.layoutslidelayer {
                > div.slidecontainer > div.slide {
                    > div[data-placeholdertype], > div[data-placeholderindex] {
                        > div.content.textframecontent {
                            border: none !important;
                        }
                    }
                }
            }

            > div.pagecontent > div.slide > div.drawing {

                &.emptytextframeborder {
                    &::before {
                        display: none !important;
                    }
                }

                > div.content.emptytextframe {
                    // in standard view mode any empty sidepane preview text frame without specified border
                    // shall suppress them (keeping them invisible) in opposite to its editable appearance.
                    &::before {
                        display: none !important;
                    }
                    .templatetext, .list-label {
                        display: none;
                    }
                }

                // in empty list paragraphs the list-labels are not visible in sidepane. In this case
                // the text frame is not necessarily empty.
                .emptylistparagraph > .list-label:not(.isodf) {
                    display: none;
                }

                > .templatedrawingcontainer, > .templatereadonlytext {
                    display: none;
                }

                /* drawing crop frame should not be visible in sidepane */
                .crop {
                    display: none;
                }

                table td.cellSelectionHighlighting {
                    box-shadow: none !important;
                }
            }
        }
    }

    > .slidePlacer {
        position: absolute;
        top: 0;
        left: 10px;
        width: 100px;
        height: 3px;
        visibility: hidden;
        background: black;
        :root.dark & { background: white; }
    }

    // needed for Safari only, to get beforecopy and beforepaste event
    &.safari-userselect-text {
        -webkit-user-select: text !important;
    }
}
