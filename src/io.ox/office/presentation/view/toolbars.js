/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';
import { INSERT_COMMENT_OPTIONS, SHOW_COMMENTSPANE_CHECKBOX_OPTIONS } from '@/io.ox/office/textframework/view/labels';
import { Button, InsertLayoutSlidePicker, ChangeLayoutSlidePicker } from '@/io.ox/office/presentation/view/controls';

// re-exports =================================================================

export * from '@/io.ox/office/textframework/view/toolbars';

// class SlideToolBar =========================================================

export class SlideToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            shrinkToMenu: {
                icon: 'bi:file-ppt',
                tooltip: gt('Slide')
            }
        });

        // create the controls of this tool bar
        this.addControl('layoutslidepicker/insertslide', new InsertLayoutSlidePicker(docView));
        this.addContainer();
        this.addControl('layoutslidepicker/changelayout', new ChangeLayoutSlidePicker(docView));
    }
}

// class CommentsToolBar ==================================================

/**
 * A tool bar with controls to manipulate comments.
 *
 * @param {PresentationView} docView
 *  The document view instance containing this tool bar.
 */
export class CommentsToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        this.addSection("comment");

        this.addControl('threadedcomment/insert', new Button({ ...INSERT_COMMENT_OPTIONS, label: INSERT_COMMENT_OPTIONS.tooltip }));

        this.addControl('threadedcomment/goto/prev', new Button({
            icon: 'png:comment-back',
            label: /*#. go to preceding comment */ gt.pgettext('comment-actions', 'Previous'),
            tooltip: gt.pgettext('comment-actions', 'Go to previous comment')
        }));

        this.addControl('threadedcomment/goto/next', new Button({
            icon: 'png:comment-next',
            label: /*#. go to next comment */ gt.pgettext('comment-actions', 'Next'),
            tooltip: gt.pgettext('comment-actions', 'Go to next comment')
        }));

        this.addControl('threadedcomment/delete/thread', new Button({
            icon: 'png:comment-remove',
            label: /*#. delete the currently selected comment thread in presentation */ gt.pgettext('comment-actions', 'Delete'),
            tooltip: /*#. delete the currently selected comment thread in presentation */ gt.pgettext('comment-actions', 'Delete selected thread')
        }));

        this.addSection("allcomments");

        this.addControl('threadedcomment/delete/all', new Button({
            icon: 'png:comment-remove',
            label: /*#. delete all comment threads in presentation */ gt.pgettext('comment-actions', 'Delete all'),
            tooltip: /*#. delete all comment threads presentation */ gt.pgettext('comment-actions', 'Delete every comment')
        }));

        if (!_.device('smartphone')) {
            this.addSection("view");
            this.addControl('view/commentspane/show', new Button({
                icon: 'png:comment-show',
                toggle: true,
                ...SHOW_COMMENTSPANE_CHECKBOX_OPTIONS
            }));
        }
    }
}
