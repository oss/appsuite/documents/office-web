/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";
import * as Utils from "@/io.ox/office/textframework/utils/textutils";
import { SLIDE_TRANSITIONS } from "@/io.ox/office/presentation/components/present/slidetransitions";

// class SlideAnimationPicker =================================================

/**
 * A control used to select a slide animation for the Presentation mode.
 */
export class SlideAnimationPicker extends RadioList {

    constructor() {

        // base constructor
        super({
            //#. button label: slide animations in presentation documents
            label: gt("Animations"),
            //#. tooltip for dropdown menu: slide animations in presentation documents
            tooltip: gt("Select a slide animation for the slide changes"),
            updateCaptionMode: "none"
        });

        this.addSection("none");
        this.addOption("NONE", {
            //#. button label: slide animations in presentation documents
            label: gt("No animation"),
            //#. tooltip for dropdown menu entry: slide animations in presentation documents
            tooltip: gt("No slide animation")
        });

        // create the menu entries for the different slide animation scenarios
        this.addSection("transitions");
        SLIDE_TRANSITIONS.forEach(slideTransition => {
            const addTransition = !slideTransition.restriction || Utils[slideTransition.restriction];
            if (addTransition) {
                this.addOption(slideTransition.id, { label: slideTransition.name, tooltip: slideTransition.tooltip });
            }
        });
    }
}
