/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";

import { map } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE } from "@/io.ox/office/tk/dom";
import { Canvas } from "@/io.ox/office/tk/canvas";
import { getCaptionNode } from "@/io.ox/office/tk/forms";
import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import LayoutTypeTitleMap from "@/io.ox/office/presentation/view/layouttypetitlemap";

import "@/io.ox/office/presentation/view/control/layoutslidepicker.less";

// constants ==================================================================

const THUMB_WIDTH = 90;
const THUMB_HEIGHT = 63;

// class LayoutSlidePicker ====================================================

export class LayoutSlidePicker extends AppRadioList {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            updateCaptionMode: "none",
            listLayout: "grid",
            gridColumns: SMALL_DEVICE ? 1 : 5,
            ...config
        });

        this._thumbRegistry = new Map/*<string, JQuery>*/();
        this._thumbPromises = null;
        this._thumbPromiseCount = -1;

        this._isOdfLayout = config?.isOdfLayout ?? false;

        this._resource = this.docApp.resourceManager.getResource("layouttypetitles");

        // a cache for ODF layout slide bitmaps
        this._odfLayoutCache = new Map/*<string, string>*/();

        // CSS class "io-ox-office-presentation-main" needed for content styling of preview slides
        this.menu.$el.addClass("io-ox-office-presentation-main layout-slide-picker");

        this.listenTo(this.menu, "popup:beforeshow", this._createMenuContent);
        this.listenTo(this.menu, "popup:hide", this._clearMenuContent);
        this.listenTo(this.menu, "list:option:add", this._addOptionHandler);
    }

    // private methods --------------------------------------------------------

    /*private*/ _getCustomSlideTitle(docModel, slideId) {
        return docModel.getSlideFamilyAttributeForSlide(slideId, "slide", "name");
    }

    /*private*/ _getMasterTypeTitle() {
        const defaultTitle = LayoutTypeTitleMap.master;
        return this._resource[defaultTitle] || defaultTitle;
    }

    /*private*/ _getLayoutTypeTitle(slideId) {
        const defaultTitle = LayoutTypeTitleMap[this.docModel.getSlideFamilyAttributeForSlide(slideId, "slide", "type")];
        return this._resource[defaultTitle] || defaultTitle;
    }

    /**
     * hook on create option buttons, only called once per type,
     * sets the button text and fill the chain-data for deferred rendering
     */
    /*private*/ _renderLayoutThumb($container, slideId) {

        const thumbPromise = this.docView.requestThumb(slideId);
        thumbPromise.done(data => $container.empty().append(data.thumb));

        if (this._thumbPromises) {
            this._thumbPromises.push(thumbPromise);

            if (this._thumbPromises.length >= this._thumbPromiseCount) {
                $.when(...this._thumbPromises).then(() => {
                    this._thumbPromises = null;
                    this._thumbPromiseCount = -1;
                    this.listenTo(this.docView, "layoutthumb:change", this._handleLayoutThumbUpdate);
                    this._thumbRegistry.forEach(($container, slideId) => this._renderLayoutThumb($container, slideId));
                });
            }
        }
    }

    /**
     * Rendering the content of the thumbs for ODF layout slides.
     *
     * @param {JQuery} $container
     *  The container node for the ODF layout thumb.
     *
     * @param {string} layoutId
     *  The ID of the ODF layout slide.
     */
    /*private*/ _renderODFLayoutThumb($container, layoutId) {

        const dataUrl = map.upsert(this._odfLayoutCache, layoutId, () => {
            const canvas = new Canvas({ location: { width: THUMB_WIDTH, height: THUMB_HEIGHT } });
            const url = canvas.renderToDataURL(context => {
                context.setLineStyle({ style: "#bbb", width: 1 });
                context.setFillStyle("#3c73aa");
                for (const [x, y, w, h, type] of this.docModel.getODFLayoutPreview(layoutId)) {
                    const fill = type === "text";
                    const offset = fill ? 0 : 0.5;
                    const x1 = Math.round(x / 100 * THUMB_WIDTH) + offset;
                    const y1 = Math.round(y / 100 * THUMB_HEIGHT) + offset;
                    const x2 = Math.round((x + w) / 100 * THUMB_WIDTH) + offset;
                    const y2 = Math.round((y + h) / 100 * THUMB_HEIGHT) + offset;
                    context.drawRect(x1, y1, x2 - x1, y2 - y1, fill ? "fill" : "stroke");
                }
            });
            canvas.destroy();
            return url;
        });

        if (dataUrl) {
            $container.append(`<img src="${dataUrl}">`);
        }
    }

    /*private*/ _addOptionHandler($button) {

        const slideId = $button.attr("data-value");
        const $container = $('<div class="thumb-container">');
        $container.css({ width: THUMB_WIDTH, height: THUMB_HEIGHT });
        $button.prepend($container);
        this._thumbRegistry.set(slideId, $container);

        getCaptionNode($button).css("width", THUMB_WIDTH);

        if (this._isOdfLayout) {
            this._renderODFLayoutThumb($container, slideId);
        } else {
            this._renderLayoutThumb($container, slideId);
        }
    }

    /*private*/ _handleLayoutThumbUpdate(slideIds) {
        slideIds.forEach(slideId => {
            map.visit(this._thumbRegistry, slideId, $container => {
                this._renderLayoutThumb($container, slideId);
            });
        });
    }

    /*private*/ _clearMenuContent() {
        this.stopListeningTo(this.docView, "layoutthumb:change");
        this.clearOptions();
        this._thumbRegistry.clear();
    }

    /*private*/ _createMenuContent() {

        this.docView.forceLayoutMasterSlideFormatting();

        const docModel = this.docModel;
        const masterLayoutSlideIdList = this._isOdfLayout ? docModel.getOdfLayoutSlideIDs() : docModel.getMasterSlideOrder();
        let sectionSlideCount;

        if (!this._isOdfLayout) {
            this._thumbPromises = [];
            this._thumbPromiseCount = masterLayoutSlideIdList.filter(docModel.isLayoutSlideId).length;
        }

        if (this.docApp.isODF()) {
            sectionSlideCount = masterLayoutSlideIdList.length;
        } else {
            sectionSlideCount = docModel.getMaxLayoutSlideNumber();
        }

        // use smaller grids in layout slide pickers, if there are less than 5 slides
        if (sectionSlideCount > 5) { sectionSlideCount = undefined; }

        masterLayoutSlideIdList.forEach(slideId => {
            const sectionId = `master_id_${docModel.isMasterSlideId(slideId) ? slideId : docModel.getMasterSlideId(slideId)}`;
            if (docModel.isMasterSlideId(slideId)) {
                const label = this._getCustomSlideTitle(docModel, slideId) || this._getMasterTypeTitle() || _.noI18n(`master / ${slideId}`);
                this.addSection(sectionId, { label, gridColumns: sectionSlideCount });
            } else if (this._isOdfLayout || docModel.isLayoutSlideId(slideId)) {
                const label = this._isOdfLayout ? this.docModel.getOdfLayoutSlideTitle(slideId) : (this._getCustomSlideTitle(docModel, slideId) || this._getLayoutTypeTitle(slideId) || _.noI18n(`layout / ${slideId}`));
                this.addOption(slideId, { label, tooltip: label, section: this._isOdfLayout ? null : sectionId, classes: "mini-caption", gridColumns: sectionSlideCount });
            }
        });
    }
}
