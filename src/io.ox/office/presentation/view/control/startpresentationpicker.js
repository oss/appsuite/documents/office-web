/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";

// class StartPresentationPicker ==============================================

/**
 * A control used to select how to start a presentation.
 */
export class StartPresentationPicker extends RadioList {

    constructor(config) {

        // base constructor
        super({
            icon: "bi:play-circle",
            splitValue: "FIRST",
            ...config
        });

        // initialization
        this.addOption("FIRST",   { label: gt("Start with first slide"), tooltip: gt("Start the presentation with the first slide") });
        this.addOption("CURRENT", { label: gt("Start with current slide"), tooltip: gt("Start the presentation with the current slide") });
    }
}
