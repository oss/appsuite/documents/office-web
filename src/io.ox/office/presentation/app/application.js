/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { FlushReason } from '@/io.ox/office/baseframework/utils/apputils';
import { PRESENTATION } from '@/io.ox/office/baseframework/utils/errorcontext';
import { EditApplication } from '@/io.ox/office/editframework/app/editapplication';
import mvcFactory from '@/io.ox/office/presentation/app/mvcfactory';

// class PresentationApp ======================================================

/**
 * The OX Presentation application.
 */
export default class PresentationApp extends EditApplication {

    /**
     * @param {CoreApp} coreApp
     *  The core application instance.
     */
    constructor(coreApp) {
        super(coreApp, mvcFactory, {
            sendActionsDelay: 1000,
            isMultiSelectionApp: true,
            localStorageApp: true,
            requiredStorageVersion: 3,
            supportedStorageVersion: 3,
            otConfigKey: 'presentation/concurrentEditing'
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Post-processes the document contents and formatting, after all its
     * import operations have been applied.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the document has been post-processed
     *  successfully, or reject when an error has occurred.
     */
    /*protected override*/ implPostProcessImport() {
        return this.docModel.updateDocumentFormatting();
    }

    /**
     * Handler for fast load of empty default documents.
     *
     * @param {string} markup
     *  The HTML markup to be shown as initial document contents.
     *
     * @param {Operations[]} operations
     *  The document operations to be applied to finalize the fast import.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the operations have been applied.
     */
    /*protected override*/ implFastEmptyLoad(markup, operations) {
        return this.docModel.fastEmptyLoadHandler(markup, operations);
    }

    /**
     * Will be called by base class if importing the document failed.
     *
     * @param {ErrorCode} error
     *  The parsed error response data.
     */
    /*protected override*/ implImportFailed(error) {

        // DOCS-733: Improve OOM Error Message in Client
        switch (error.getCodeAsConstant()) {
            case "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR":
                this.setInternalError(error, PRESENTATION, null, { showMessage: false });
                break;
        }
    }

    /**
     * Optimizes the operation actions before sending them to the server.
     *
     * @param {OperationAction[]} actions
     *  An array with operation actions.
     *
     * @returns {OperationAction[]}
     *  The optimized array with operation actions.
     */
    /*protected override*/ implOptimizeOperations(actions) {
        return this.docModel.optimizeActions(actions);
    }

    /**
     * Preprocessing before the document will be flushed for downloading,
     * printing, sending as mail, or closing.
     *
     * Avoids calling "sendChangedViewAttributes" if the flush is triggered by
     * starting a presentation. Otherwise the conversion to PDF is triggered
     * always before starting the presentation, if the document has once been
     * modified before.
     *
     * @param {FlushReason} reason
     *  The origin of the flush request.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the actions (of dynamic font size
     *  calculations) have been applied.
     */
    /*protected override*/ implPrepareFlushDocument(reason) {

        // check, if the current height of autoheight drawing frame must be sent to the filter
        this.docModel.checkTextFrameLeaveHandler();

        // send changed view attributes from the slide pane
        // do not save slide pane width (and optionally trigger a pdf conversion), if the document shall be presented.
        if (reason !== FlushReason.PRESENT) {
            this.docModel.sendChangedViewAttributes();
        }

        return this.docModel.getPreFlushDocumentPromise();
    }

    /**
     * Preparing losing edit rights. Especially for touch devices additional
     * steps are required to disable text input after the edit rights have
     * changed.
     */
    /*protected override*/ implPrepareLoseEditRights() {

        if (this.docModel.getSlideTouchMode()) {
            var selection = this.docModel.getSelection();
            // set to default position in the slide, triggerChange is false to prevent scrolling to 0.0
            selection.setSlideSelection({ triggerChange: false });
            // move focus to close a open soft-keyboard
            selection.setFocusIntoClipboardNode({ specialTouchHandling: true });
            // reset the browser selection
            window.getSelection().removeAllRanges();

            var appPaneNode = this.docView.$appPaneNode;
            // remove all contenteditable
            appPaneNode.find('[contenteditable="true"]').removeAttr('contenteditable');
            // remove the active-edit state
            appPaneNode.find('.active-edit').removeClass('active-edit');
        }
    }

    /**
     * Preparations before the rename requests will be sent to the backend.
     * Must call "sendChangedViewAttributes" to make sure that view settings
     * are not sent while closing the document.
     */
    /*protected override*/ implPrepareRenameDocument() {
        this.docModel.sendChangedViewAttributes();
    }

    /**
     * Returns additional launch options for reloading this document.
     */
    /*protected override*/ implPrepareReloadDocument() {
        return this.docModel.prepareReloadDocument();
    }
}
