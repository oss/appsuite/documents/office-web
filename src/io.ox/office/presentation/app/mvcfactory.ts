/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EditMVCFactory } from "@/io.ox/office/editframework/app/mvcfactory";

import { PresentationResourceManager } from "@/io.ox/office/presentation/resource/resourcemanager";
import PresentationOTManager from "@/io.ox/office/presentation/components/ot/otmanager";
import PresentationModel from "@/io.ox/office/presentation/model/docmodel";
import PresentationView from "@/io.ox/office/presentation/view/view";
import PresentationController from "@/io.ox/office/presentation/controller/controller";
import type PresentationApp from "@/io.ox/office/presentation/app/application";

// class PresentationMVCFactory ===============================================

/**
 * The MVC factory for presentation application instances.
 */
class PresentationMVCFactory extends EditMVCFactory<PresentationApp> {

    public constructor() {
        super();
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates and returns a new resource manager for a presentation document.
     */
    createResourceManager(docApp: PresentationApp): PresentationResourceManager {
        return new PresentationResourceManager(docApp);
    }

    /**
     * Creates and returns a new presentation document model.
     */
    createModel(docApp: PresentationApp): PresentationModel {
        return new PresentationModel(docApp);
    }

    /**
     * Creates and returns a new presentation document view.
     */
    createView(docApp: PresentationApp, docModel: PresentationModel): PresentationView {
        return new PresentationView(docApp, docModel);
    }

    /**
     * Creates and returns a new presentation document controller.
     */
    createController(docApp: PresentationApp, docModel: PresentationModel, docView: PresentationView): PresentationController {
        return new PresentationController(docApp, docModel, docView);
    }

    /**
     * Creates and returns a new OT manager for a presentation document.
     */
    createOTManager(docModel: PresentationModel): PresentationOTManager {
        return new PresentationOTManager(docModel);
    }
}

// exports ====================================================================

export default new PresentationMVCFactory();
