/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { PRESENTATION_APP_CONFIG } from "@/io.ox/office/baseframework/utils/apputils";
import { BaseAppFactory } from "@/io.ox/office/baseframework/app/appfactory";
import type PresentationApp from "@/io.ox/office/presentation/app/application";

// class PresentationAppFactory ===============================================

export default class PresentationAppFactory extends BaseAppFactory<PresentationApp> {

    public constructor() {
        super(PRESENTATION_APP_CONFIG);
    }

    protected async implLoadAppClass(): Promise<typeof PresentationApp> {
        const { default: PresentationApp } = await import("@/io.ox/office/presentation/app/application");
        return PresentationApp;
    }
}
