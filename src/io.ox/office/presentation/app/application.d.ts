/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CoreApp } from "@/io.ox/office/baseframework/app/appfactory";
import { TextBaseApplicationEventMap, TextBaseApplication } from "@/io.ox/office/textframework/app/application";
import { PresentationResourceManager } from "@/io.ox/office/presentation/resource/resourcemanager";
import PresentationModel from "@/io.ox/office/presentation/model/docmodel";
import PresentationView from "@/io.ox/office/presentation/view/view";
import PresentationController from "@/io.ox/office/presentation/controller/controller";

// types ======================================================================

/**
 * Type mapping for the events emitted by `PresentationApplication` instances.
 */
export interface PresentationApplicationEventMap extends TextBaseApplicationEventMap { }

// class PresentationApp ======================================================

export default class PresentationApplication extends TextBaseApplication<PresentationApplicationEventMap> {

    declare readonly resourceManager: PresentationResourceManager;
    declare readonly docModel: PresentationModel;
    declare readonly docView: PresentationView;
    declare readonly docController: PresentationController;

    constructor(coreApp: CoreApp);
}
