/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { math } from '@/io.ox/office/tk/algorithms';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { getOxoPosition } from '@/io.ox/office/textframework/utils/position';

import { DELETE, INSERT_DRAWING, SET_ATTRIBUTES } from '@/io.ox/office/presentation/utils/operations';
import { DEFAULT_PLACEHOLDER_INDEX, DEFAULT_PLACEHOLDER_TYPE, getPlaceHolderDrawingIndex, getPlaceHolderDrawingType,
    isEmptyPlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';

// constants ==================================================================

/**
 * A collection of layout slide descriptions for ODF.
 *
 * The property "preview" is an array of instructions for rendering a preview
 * bitmap. The array elements are array with rectangle coordinates (x, y, w, h)
 * in percent. An optional 5th array element may specify a type identifier for
 * the rectangle.
 */
const ODF_LAYOUT_DESCRIPTIONS = [
    {
        id: "layout_1",
        title: gt('Blank Slide')
    },
    {
        id: "layout_2",
        title: gt('Title Slide'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: { 0: { presentation: { phType: 'subTitle', phIndex: 0 }, shape: { vert: null, anchor: 'centered' }, drawing: { name: 'Subtitle 1' }, fill: { color: Color.WHITE }, paragraph: { alignment: 'center' } } }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 85, 54],
            [13, 14, 73, 8, "text"],
            [22, 52, 55, 6, "text"]
        ]
    },
    {
        id: "layout_3",
        title: gt('Title, Content'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: { 0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } } }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 85, 54],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_4",
        title: gt('Title and 2 Contents'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 40, 54],
            [52, 36, 40, 54],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_5",
        title: gt('Title Only'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } }
        },
        preview: [
            [7, 10, 85, 16],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_6",
        title: gt('Centered Text'),
        drawings: {
            body: { 0: { presentation: { phType: 'subTitle', phIndex: 0 }, shape: { vert: null, anchor: 'centered' }, drawing: { name: 'Subtitle 1' }, fill: { color: Color.WHITE }, paragraph: { alignment: 'center' } } }
        },
        preview: [
            [7, 10, 85, 80],
            [22, 40, 55, 6, "text"],
            [31, 52, 37, 6, "text"]
        ]
    },
    {
        id: "layout_7",
        title: gt('Title, 2 Contents and Content'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                bodyGeometry: 'rightLarge',
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                2: { presentation: { phType: 'body', phIndex: 2 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 40, 24],
            [7, 66, 40, 24],
            [52, 36, 40, 54],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_8",
        title: gt('Title, Content and 2 Contents'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                bodyGeometry: 'leftLarge',
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                2: { presentation: { phType: 'body', phIndex: 2 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 40, 54],
            [52, 36, 40, 24],
            [52, 66, 40, 24],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_9",
        title: gt('Title, 2 Contents over Content'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                bodyGeometry: 'bottomLarge',
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } },
                2: { presentation: { phType: 'body', phIndex: 2 }, shape: { vert: null }, drawing: { name: 'Place holder 3' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 40, 24],
            [52, 36, 40, 24],
            [7, 66, 85, 24],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_10",
        title: gt('Title, Content over Content'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                bodyGeometry: 'vertical',
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 85, 24],
            [7, 66, 85, 24],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_11",
        title: gt('Title, 4 Contents'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } },
                2: { presentation: { phType: 'body', phIndex: 2 }, shape: { vert: null }, drawing: { name: 'Place holder 3' }, fill: { color: Color.WHITE } },
                3: { presentation: { phType: 'body', phIndex: 3 }, shape: { vert: null }, drawing: { name: 'Place holder 4' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 40, 24],
            [52, 36, 40, 24],
            [7, 66, 40, 24],
            [52, 66, 40, 24],
            [13, 14, 73, 8, "text"]
        ]
    },
    {
        id: "layout_12",
        title: gt('Title, 6 Contents'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: {
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Place holder 1' }, fill: { color: Color.WHITE } },
                1: { presentation: { phType: 'body', phIndex: 1 }, shape: { vert: null }, drawing: { name: 'Place holder 2' }, fill: { color: Color.WHITE } },
                2: { presentation: { phType: 'body', phIndex: 2 }, shape: { vert: null }, drawing: { name: 'Place holder 3' }, fill: { color: Color.WHITE } },
                3: { presentation: { phType: 'body', phIndex: 3 }, shape: { vert: null }, drawing: { name: 'Place holder 4' }, fill: { color: Color.WHITE } },
                4: { presentation: { phType: 'body', phIndex: 4 }, shape: { vert: null }, drawing: { name: 'Place holder 5' }, fill: { color: Color.WHITE } },
                5: { presentation: { phType: 'body', phIndex: 5 }, shape: { vert: null }, drawing: { name: 'Place holder 6' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 25, 24],
            [37, 36, 25, 24],
            [67, 36, 25, 24],
            [7, 66, 25, 24],
            [37, 66, 25, 24],
            [67, 66, 25, 24],
            [13, 14, 73, 8, "text"]
        ]
    },
    // TODO Diagram
    // {
    //     id: "layout_13",
    //     title: gt('Vertical Title, Text, Diagram')
    // },
    {
        id: "layout_14",
        title: gt('Vertical Title, Vertical Text'),
        drawings: {
            title: {
                titleGeometry: 'rightTitle',
                0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: 'eaVert' }, drawing: { name: 'Vertical Title 1' } }
            },
            body: {
                bodyGeometry: 'rightTitle',
                0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: 'eaVert' }, drawing: { name: 'Veritical place holder 1' }, fill: { color: Color.WHITE } }
            }
        },
        preview: [
            [7, 10, 63, 80],
            [75, 10, 18, 80],
            [81, 20, 6, 60, "text"]
        ]
    },
    {
        id: "layout_15",
        title: gt('Title, Vertical Text'),
        drawings: {
            title: { 0: { presentation: { phType: 'title', phIndex: 0 }, shape: { vert: null }, drawing: { name: 'Title 1' } } },
            body: { 0: { presentation: { phType: 'body', phIndex: 0 }, shape: { vert: 'eaVert' }, drawing: { name: 'Veritical place holder 1' }, fill: { color: Color.WHITE } } }
        },
        preview: [
            [7, 10, 85, 16],
            [7, 36, 85, 54],
            [13, 14, 73, 8, "text"]
        ]
    }
    // TODO: Clipart
    // {
    //     id: "layout_16",
    //     title: gt('Title, Vertical Text, Clipart')
    // }
];

const ODF_LAYOUT_DESCRIPTIONS_MAP = ODF_LAYOUT_DESCRIPTIONS.reduce((result, entry) => {
    result[entry.id] = entry;
    return result;
}, {});

// mix-in class OdfLayoutMixin ================================================

/**
 * A mix-in class for the document model class PresentationModel providing
 * the layout slide information for ODF applications.
 */
export default function OdfLayoutMixin() {

    // self reference
    var self = this;

    // private methods ----------------------------------------------------

    /**
     * A helper function that calculates the size and position of a rectangle, that contains
     * the title and body drawing of the master slide. Additionally the ratios of the height
     * of title and body place holders in relation to the height of the new calculated rectangle
     * is determined.
     *
     * @param {Object} attrs
     *  The drawing attributes from the master slide.
     *
     * @returns {Object}
     *  A rectangle object containing properties of a common rectangle of title and body
     *  placeholder drawings on the master slide.
     */
    function getBodyTitleRectangle(attrs) {

        var titleAttrs =  attrs.title && attrs.title[0] && attrs.title[0].drawing;
        var bodyAttrs =  attrs.body && attrs.body[0] && attrs.body[0].drawing;
        var bodyTitleRect = {};
        var maxRight = 0;
        var maxBottom = 0;

        bodyTitleRect.left = Math.min(titleAttrs.left, bodyAttrs.left);
        bodyTitleRect.top = Math.min(titleAttrs.top, bodyAttrs.top);

        maxRight = Math.max(titleAttrs.left + titleAttrs.width, bodyAttrs.left + bodyAttrs.width);
        maxBottom = Math.max(titleAttrs.top + titleAttrs.height, bodyAttrs.top + bodyAttrs.height);

        bodyTitleRect.width = maxRight - bodyTitleRect.left;
        bodyTitleRect.height = maxBottom - bodyTitleRect.top;

        bodyTitleRect.titleRatio = math.roundp(titleAttrs.height / bodyTitleRect.height, 0.001);
        bodyTitleRect.bodyRatio = math.roundp(bodyAttrs.height / bodyTitleRect.height, 0.001);

        return bodyTitleRect;
    }

    /**
     * If the layout of a document slide is changed, it is necessary to know the 'real' type
     * of a place holder drawing. In the object 'odfLayoutDescription', the body also contains
     * the type 'subTitle', because the 'body' is used for evaluation, if the master slide is
     * modified. But for the change of an ODF layout it is possible, that a 'body' is changed
     * to a 'subTitle' and vice versa. Therefore it is necessary to modify the sorting of the
     * object returned by the function 'this.getODFPlaceHoldersAttributes'.
     *
     * @param {Object} attrs
     *  The object as it is returned from the function 'this.getODFPlaceHoldersAttributes'.
     *
     * @returns {Object}
     *  The modified object, in which not only 'title' and 'body' can be resolved.
     */
    function resortODFLayoutAttributes(attrs) {

        var // the return object
            resortedAttrs = {};

        _.each(_.keys(attrs), function (type) {
            _.each(_.keys(attrs[type]), function (index) {

                var realType = type;

                if (attrs[type][index].presentation && attrs[type][index].presentation.phType) {
                    realType = attrs[type][index].presentation.phType;
                }

                resortedAttrs[realType] = resortedAttrs[realType] || {};
                resortedAttrs[realType][index] = attrs[type][index];
            });
        });

        // Info: If there is a mixture of different place holder types below one place holder type
        //       in the odfLayoutDescription object, it will be necessary to set the correct index,
        //       too. But this is not the case yet.

        return resortedAttrs;
    }

    /**
     * A helper function to collect an info object for a slide specified by its ID. All place holder
     * drawings on this slide are collected and added into a helper object that contains type and
     * index as keys for the drawings.
     *
     * @param {String} slideId
     *  The ID of the slide.
     *
     * @returns {Object}
     *  An object containing references to all place holder drawings on the specified slide. First
     *  level key is the place holder type, second level key the place holder index.
     */
    function getInfoOfExistingPlaceHoldersOnSlide(slideId) {

        var // the return object with the information about the place holder drawings on the specified slide
            infoObject = {};

        _.each(self.getSlideById(slideId).children('div[data-placeholdertype]'), function (oneDrawing) {

            var // the explicit drawing attributes
                attrs = getExplicitAttributeSet(oneDrawing),
                // the drawing type
                type = getPlaceHolderDrawingType(null, attrs),
                // the place holder drawing index
                index = getPlaceHolderDrawingIndex(null, attrs);

            if (!type) { type = DEFAULT_PLACEHOLDER_TYPE; }
            if (!_.isNumber(index)) { index = DEFAULT_PLACEHOLDER_INDEX; }

            // adding the information to the object
            infoObject[type] = infoObject[type] || {};
            infoObject[type][index] = oneDrawing; // saving the drawing object in the info object
        });

        return infoObject;
    }

    /**
     * A helper function to calculate the position and size of new inserted drawings.
     *
     * @param {String} type
     *  The type of the place holder drawing specified in the 'odfLayoutDescription'.
     *  Currently 'title' and 'body' are supported.
     *
     * @param {Object} phAttrs
     *  The place holder attributes that need to be expanded for position and size
     *  of the drawings.
     *
     * @param {Object} masterAttrs
     *  The drawing attributes from the master slide.
     */
    function setDrawingSizeAndPosition(type, phAttrs, masterAttrs) {

        // the number of drawings of type 'body' in the specified layout
        var bodyCount = 0;
        // the attributes of the drawing of type 'body' on the master slide
        var masterBodyAttrs = null;
        // the attributes of the drawing of type 'title' on the master slide
        var masterTitleAttrs = null;
        // the width of a drawing of type 'body'
        var bodyWidth = 0;
        // the height of a drawing of type 'body'
        var bodyHeight = 0;
        // a specified geometry for the title drawing
        var titleGeometry = null;
        // a specified geometry for the body drawing(s)
        var bodyGeometry = null;
        // a factor for calculating new drawing width and height
        var factor = 1;
        // a factor for calculating the vertical drawing size
        var vertfactor = 1;
        // a helper rectangle for the position and size of combined title and body
        var bodyTitleRect = null;

        // The type 'title' should always be inherited from the master slide, that must contain exactly one title drawing.
        if (type === 'title' && masterAttrs.title && masterAttrs.title[0] && masterAttrs.title[0].drawing) {
            if (phAttrs.title && phAttrs.title[0] && phAttrs.title[0].drawing) {

                titleGeometry = phAttrs.title.titleGeometry || '';
                delete phAttrs.title.titleGeometry; // deleting key, so that bodyCount is correct

                if (titleGeometry === 'rightTitle') {

                    bodyTitleRect = getBodyTitleRectangle(masterAttrs);
                    factor = bodyTitleRect.titleRatio;

                    phAttrs.title[0].drawing.width = Math.round(bodyTitleRect.width * factor);
                    phAttrs.title[0].drawing.height = bodyTitleRect.height;
                    phAttrs.title[0].drawing.left = bodyTitleRect.left + bodyTitleRect.width - phAttrs.title[0].drawing.width;
                    phAttrs.title[0].drawing.top = bodyTitleRect.top;

                } else {

                    masterTitleAttrs = masterAttrs.title[0].drawing;
                    phAttrs.title[0].drawing.left = masterTitleAttrs.left;
                    phAttrs.title[0].drawing.top = masterTitleAttrs.top;
                    phAttrs.title[0].drawing.width = masterTitleAttrs.width;
                    phAttrs.title[0].drawing.height = masterTitleAttrs.height;

                }
            }
        }

        // The type 'body' needs to check the number of drawings on the layout slide.
        // The master slide must contain exactly one body drawing, whose size and position can be used to calculate
        // the size and the position of the new inserted place holder drawings.
        if (type === 'body' && masterAttrs.body && masterAttrs.body[0] && masterAttrs.body[0].drawing) {
            // if there is only one body place holder in the layout slide, it can directly inherit the attributes from
            // the master slide
            if (phAttrs.body) {

                bodyGeometry = phAttrs.body.bodyGeometry || '';
                delete phAttrs.body.bodyGeometry; // deleting key, so that bodyCount is correct

                bodyCount = _.keys(phAttrs.body).length;
                masterBodyAttrs = masterAttrs.body[0].drawing;

                if (bodyCount === 1) {

                    if (bodyGeometry === 'rightTitle') {

                        bodyTitleRect = getBodyTitleRectangle(masterAttrs);
                        factor = bodyTitleRect.bodyRatio;

                        phAttrs.body[0].drawing.left = bodyTitleRect.left;
                        phAttrs.body[0].drawing.top = bodyTitleRect.top;
                        phAttrs.body[0].drawing.width = Math.round(bodyTitleRect.width * factor);
                        phAttrs.body[0].drawing.height = bodyTitleRect.height;

                    } else {

                        phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[0].drawing.width = masterBodyAttrs.width;
                        phAttrs.body[0].drawing.height = masterBodyAttrs.height;

                    }

                } else if (bodyCount === 2) {

                    factor = 0.48;

                    if (bodyGeometry === 'vertical') {

                        bodyWidth = masterBodyAttrs.width;
                        bodyHeight = Math.round(factor * masterBodyAttrs.height);

                        phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[0].drawing.width = bodyWidth;
                        phAttrs.body[0].drawing.height = bodyHeight;

                        phAttrs.body[1].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[1].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                        phAttrs.body[1].drawing.width = bodyWidth;
                        phAttrs.body[1].drawing.height = bodyHeight;

                    } else {

                        bodyWidth = Math.round(factor * masterBodyAttrs.width);
                        bodyHeight = masterBodyAttrs.height;

                        phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[0].drawing.width = bodyWidth;
                        phAttrs.body[0].drawing.height = bodyHeight;

                        phAttrs.body[1].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                        phAttrs.body[1].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[1].drawing.width = bodyWidth;
                        phAttrs.body[1].drawing.height = bodyHeight;

                    }
                } else if (bodyCount === 3) {

                    factor = 0.48;
                    bodyWidth = Math.round(factor * masterBodyAttrs.width);
                    bodyHeight = Math.round(factor * masterBodyAttrs.height);

                    if (bodyGeometry === 'rightLarge') {

                        phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[0].drawing.width = bodyWidth;
                        phAttrs.body[0].drawing.height = bodyHeight;

                        phAttrs.body[1].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[1].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                        phAttrs.body[1].drawing.width = bodyWidth;
                        phAttrs.body[1].drawing.height = bodyHeight;

                        phAttrs.body[2].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                        phAttrs.body[2].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[2].drawing.width = bodyWidth;
                        phAttrs.body[2].drawing.height = masterBodyAttrs.height; // the large drawing

                    } else if (bodyGeometry === 'leftLarge') {

                        phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[0].drawing.width = bodyWidth;
                        phAttrs.body[0].drawing.height = masterBodyAttrs.height; // the large drawing

                        phAttrs.body[1].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                        phAttrs.body[1].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[1].drawing.width = bodyWidth;
                        phAttrs.body[1].drawing.height = bodyHeight;

                        phAttrs.body[2].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                        phAttrs.body[2].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                        phAttrs.body[2].drawing.width = bodyWidth;
                        phAttrs.body[2].drawing.height = bodyHeight;

                    } else if (bodyGeometry === 'bottomLarge') {

                        phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[0].drawing.width = bodyWidth;
                        phAttrs.body[0].drawing.height = bodyHeight;

                        phAttrs.body[1].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                        phAttrs.body[1].drawing.top = masterBodyAttrs.top;
                        phAttrs.body[1].drawing.width = bodyWidth;
                        phAttrs.body[1].drawing.height = bodyHeight;

                        phAttrs.body[2].drawing.left = masterBodyAttrs.left;
                        phAttrs.body[2].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                        phAttrs.body[2].drawing.width = masterBodyAttrs.width; // the large drawing
                        phAttrs.body[2].drawing.height = bodyHeight;
                    }

                } else if (bodyCount === 4) {

                    factor = 0.48;

                    bodyWidth = Math.round(factor * masterBodyAttrs.width);
                    bodyHeight = Math.round(factor * masterBodyAttrs.height);

                    phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                    phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                    phAttrs.body[0].drawing.width = bodyWidth;
                    phAttrs.body[0].drawing.height = bodyHeight;

                    phAttrs.body[1].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                    phAttrs.body[1].drawing.top = masterBodyAttrs.top;
                    phAttrs.body[1].drawing.width = bodyWidth;
                    phAttrs.body[1].drawing.height = bodyHeight;

                    phAttrs.body[2].drawing.left = masterBodyAttrs.left;
                    phAttrs.body[2].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                    phAttrs.body[2].drawing.width = bodyWidth;
                    phAttrs.body[2].drawing.height = bodyHeight;

                    phAttrs.body[3].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                    phAttrs.body[3].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                    phAttrs.body[3].drawing.width = bodyWidth;
                    phAttrs.body[3].drawing.height = bodyHeight;

                } else if (bodyCount === 6) {

                    factor = 0.31;
                    vertfactor = 0.48;

                    bodyWidth = Math.round(factor * masterBodyAttrs.width);
                    bodyHeight = Math.round(vertfactor * masterBodyAttrs.height);

                    phAttrs.body[0].drawing.left = masterBodyAttrs.left;
                    phAttrs.body[0].drawing.top = masterBodyAttrs.top;
                    phAttrs.body[0].drawing.width = bodyWidth;
                    phAttrs.body[0].drawing.height = bodyHeight;

                    phAttrs.body[1].drawing.left = masterBodyAttrs.left + bodyWidth + Math.round(0.5 * (1 - 3 * factor) * masterBodyAttrs.width);
                    phAttrs.body[1].drawing.top = masterBodyAttrs.top;
                    phAttrs.body[1].drawing.width = bodyWidth;
                    phAttrs.body[1].drawing.height = bodyHeight;

                    phAttrs.body[2].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                    phAttrs.body[2].drawing.top = masterBodyAttrs.top;
                    phAttrs.body[2].drawing.width = bodyWidth;
                    phAttrs.body[2].drawing.height = bodyHeight;

                    phAttrs.body[3].drawing.left = masterBodyAttrs.left;
                    phAttrs.body[3].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                    phAttrs.body[3].drawing.width = bodyWidth;
                    phAttrs.body[3].drawing.height = bodyHeight;

                    phAttrs.body[4].drawing.left = masterBodyAttrs.left + bodyWidth + Math.round(0.5 * (1 - 3 * factor) * masterBodyAttrs.width);
                    phAttrs.body[4].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                    phAttrs.body[4].drawing.width = bodyWidth;
                    phAttrs.body[4].drawing.height = bodyHeight;

                    phAttrs.body[5].drawing.left = masterBodyAttrs.left + masterBodyAttrs.width - bodyWidth;
                    phAttrs.body[5].drawing.top = masterBodyAttrs.top + masterBodyAttrs.height - bodyHeight;
                    phAttrs.body[5].drawing.width = bodyWidth;
                    phAttrs.body[5].drawing.height = bodyHeight;
                }
            }
        }

    }

    // public methods -----------------------------------------------------

    /**
     * Getting a list of all available layout slide IDs in ODF application.
     *
     * @returns {String[]}
     *  A list of all available layout slide IDs in ODF application.
     */
    this.getOdfLayoutSlideIDs = function () {
        return ODF_LAYOUT_DESCRIPTIONS.map(entry => entry.id);
    };

    /**
     * Returns, whether the specified ID is the ID of an ODF layout slide.
     *
     * @param {String} id
     *  A slide ID.
     *
     * @returns {Boolean}
     *  Whether the specified ID is the ID of an ODF layout slide.
     */
    this.isOdfLayoutSlideId = function (id) {
        return id in ODF_LAYOUT_DESCRIPTIONS_MAP;
    };

    /**
     * Getting the title of the slide with the specified ID.
     *
     * @param {String} id
     *  The ID of the ODF layout slide.
     *
     * @returns {String|Null}
     *  The title of the slide with the specified ID. Or null, if it cannot be
     *  determined.
     */
    this.getOdfLayoutSlideTitle = function (id) {
        return ODF_LAYOUT_DESCRIPTIONS_MAP[id]?.title ?? null;
    };

    /**
     * Getter for the default ODF layout slide ID, that is used, if not other layout ID
     * can be determined.
     *
     * @returns {String}
     *  The default ODF layout slide ID.
     */
    this.getODFDefaultLayoutSlideId = function () {
        return OdfLayoutMixin.DEFAULT_INSERT_ODF_LAYOUT_ID;
    };

    /**
     * Collecting all place holder attributes for a specified ODF layout slide
     * and the given master slide attributes.
     *
     * @param {String} layoutId
     *  The ID of the layout slide.
     *
     * @param {Object} attrs
     *  The drawing attributes from the master slide.
     *
     * @param  {Object} [options]
     *   @param {Boolean} [options.ignoreFooter=false]
     *      Whether the information about the footer place holders can be
     *      ignored. This is for example the case, if the ODF layout changes.
     *
     * @returns {Object|Null}
     *  An object containing the place holder attributes for the specified layout
     *  ID.
     */
    this.getODFPlaceHoldersAttributes = function (layoutId, attrs, options) {

        var // the return object containing place holder attributes
            allPHAttributes = null;

        if (!options?.ignoreFooter) {
            // transferring the footer place holder attributes
            _.each(self.getPresentationFooterTypes(), function (type) {
                if (attrs[type]) {
                    allPHAttributes = allPHAttributes || {};
                    allPHAttributes[type] = _.copy(attrs[type], true);
                }
            });
        }

        // adding the layout information from the layout slide
        const drawings = ODF_LAYOUT_DESCRIPTIONS_MAP[layoutId]?.drawings;
        if (drawings) {
            allPHAttributes = allPHAttributes || {};
            _.forEach(drawings, (entry, type) => {
                allPHAttributes[type] = _.copy(entry, true);
                // setting hard drawing attributes for position and size
                setDrawingSizeAndPosition(type, allPHAttributes, attrs);
            });
        }

        return allPHAttributes;
    };

    /**
     * Returns instructions for rendering a preview bitmap for a layout slide.
     *
     * @param {string} layoutId
     *  The identifier of a predefined layout slide.
     *
     * @returns {Array}
     *  An array of path instructions.
     */
    this.getODFLayoutPreview = function (layoutId) {
        return ODF_LAYOUT_DESCRIPTIONS_MAP[layoutId]?.preview ?? [];
    };

    /**
     * Change the ODF layout for a specified slide. Changing the layout in
     * ODF format means, that the body and title place holder drawings are
     * adapted in number, size and position. The values for size and position
     * are evaluated from the master slide. The number (and some extras like
     * text alignment) are specified in the ODF layout.
     *
     * 1. Delete empty place holder drawings from old layout.
     * 2. Converting type matching old place holders to new place holders by changing index.
     * 3. 'Magic' conversion from old place holder types to new place holders.
     * 4. Creating place holder drawings that exist only in new layout.
     * 5. Place holder drawings that exist only in the old layout and are not empty
     * 6. Removing hard attributes, so that setting a new layout can be used to reset to the original layout state.
     * 7. Generate delete operations for drawings collected in step 1.
     *
     * @param {String} layoutId
     *  The ID of the layout slide.
     *
     * @param {String} slideId
     *  The ID of the slide, that will get a new layout assigned.
     *
     * @param {Object} [globalGenerator]
     *  The operations generator to collect all affected operations. This is only required
     *  for a multi slide selection. If it is not specified, this function generates its
     *  own operation generator.
     */
    this.changeODFLayout = function (layoutId, slideId, globalGenerator) {

        var // the operations generator
            generator = globalGenerator || self.createOperationGenerator(),
            // the ID of the master slide
            odfMasterId = self.getLayoutSlideId(slideId),
            // the place holder attributes from the master slide (default required for DOCS-4913)
            masterPlaceHoldersAttrs = self.drawingStyles.getAllPlaceHolderAttributesForId(odfMasterId) || self.drawingStyles.getODFDefaultPlaceHolderAttributes(),
            // the place holder attributes for the specified ODF layout
            allNewPHAttrs = self.getODFPlaceHoldersAttributes(layoutId, masterPlaceHoldersAttrs, { ignoreFooter: true }),
            // collector for delete operations
            deletePositions = [],
            // a helper array to find allowed switches of place holder drawings
            switchDrawings = null,
            // the place holder drawings before the new layout is assigned
            allOldPHAttrs = null,
            // the slide node. This must be a document slide
            slideNode = self.getSlideById(slideId),
            // the logical position of the slide
            slidePos = self.getSlidePositionById(slideId),
            // a helper object for comparison of old and new layout
            comparePlaceHolder = null;

        // helper function to calculate the difference of two specified attribute sets
        function comparePlaceHolderSet(first, second) {

            var // the comparison object with the properties 'onlyFirst', 'onlySecond' and 'both'
                comparison = { onlyFirst: [], onlySecond: [], both: [] };

            _.each(_.keys(first), function (type) {
                _.each(_.keys(first[type]), function (index) {
                    if (second && second[type] && second[type][index]) {
                        comparison.both.push({ type, index: parseInt(index, 10) });
                    } else {
                        comparison.onlyFirst.push({ type, index: parseInt(index, 10) });
                    }
                });
            });

            _.each(_.keys(second), function (type) {
                _.each(_.keys(second[type]), function (index) {
                    if (!first || !first[type] || !first[type][index]) {
                        comparison.onlySecond.push({ type, index: parseInt(index, 10) });
                    }
                });
            });

            return comparison;
        }

        // helper function to insert new place holder drawings
        function insertPlaceHolder(placeHolderInfo, allNewAttrs) {

            var // the options for the insertDrawing operation
                operationOptions = {};

            operationOptions.start = self.getNextAvailablePositionInSlide(slideId);
            operationOptions.type = 'shape';
            operationOptions.attrs = { presentation: { phType: placeHolderInfo.type, phIndex: placeHolderInfo.index } };

            if (allNewAttrs[placeHolderInfo.type][placeHolderInfo.index] && allNewAttrs[placeHolderInfo.type][placeHolderInfo.index].drawing) {
                operationOptions.attrs.drawing = _.copy(allNewAttrs[placeHolderInfo.type][placeHolderInfo.index].drawing, true);
            }

            generator.generateOperation(INSERT_DRAWING, operationOptions);
        }

        // helper function to modify existing place holder drawings
        function modifyPlaceHolder(drawingNode, attrs) {

            var // the options for the setAttributes operation
                operationOptions = {},
                // the logical position of the drawing
                drawingPos = slidePos.concat(getOxoPosition(slideNode, drawingNode, true));

            operationOptions = {};
            operationOptions.start = drawingPos;
            operationOptions.attrs = attrs; //  ? _.copy(attrs, true) : {}; // { presentation: { userTransformed: null } };

            generator.generateOperation(SET_ATTRIBUTES, operationOptions);
        }

        // helper function to remove existing place holder drawings
        function removePlaceHolderIfEmpty(placeHolderInfo, allOldAttrs) {

            var // the drawing node
                drawingNode = allOldAttrs[placeHolderInfo.type][placeHolderInfo.index],
                // the logical position of the drawing
                drawingPos = null;

            if (isEmptyPlaceHolderDrawing(drawingNode)) {
                drawingPos = slidePos.concat(getOxoPosition(slideNode, drawingNode, true));
                deletePositions.push(_.copy(drawingPos, true)); // collecting all drawing positions
                comparePlaceHolder.onlyFirst = _.without(comparePlaceHolder.onlyFirst, placeHolderInfo); // removing from collector
            }
        }

        // changing the layout of a document slide
        if (!self.isMasterView()) {

            self.appendSlideToDom(slideId); // the node must be attached to the DOM

            // collecting the existing place holder drawings
            allOldPHAttrs = getInfoOfExistingPlaceHoldersOnSlide(slideId);
            allNewPHAttrs = resortODFLayoutAttributes(allNewPHAttrs);

            comparePlaceHolder = comparePlaceHolderSet(allOldPHAttrs, allNewPHAttrs);

            // Step 1: Delete empty place holder drawings (without any text input) from 'old' layout
            if (comparePlaceHolder.onlyFirst.length > 0) {
                _.each(comparePlaceHolder.onlyFirst, function (placeHolderInfo) {
                    removePlaceHolderIfEmpty(placeHolderInfo, allOldPHAttrs);
                });
            }

            // Step 2: Find the exact match of 'old' place holders and 'new' place holder drawings that have a different phIndex.
            if (comparePlaceHolder.onlyFirst.length > 0 && comparePlaceHolder.onlySecond.length > 0) {

                _.each(comparePlaceHolder.onlyFirst, function (firstPlaceHolderInfo) {

                    var // the type of the drawing
                        type = firstPlaceHolderInfo.type || 'body',
                        // whether the same type was already found (so that only one drawing is replaced)
                        found = false;

                    _.each(comparePlaceHolder.onlySecond, function (secondPlaceHolderInfo) {

                        var // the attributes for the setAttributes operation
                            allAttrs = null,
                            // the drawing node
                            drawingNode = null;

                        if (!found && type === secondPlaceHolderInfo.type) { // handling the same type

                            drawingNode = allOldPHAttrs[firstPlaceHolderInfo.type][firstPlaceHolderInfo.index];

                            if (allNewPHAttrs && allNewPHAttrs[secondPlaceHolderInfo.type][secondPlaceHolderInfo.index]) {
                                allAttrs = allNewPHAttrs[secondPlaceHolderInfo.type][secondPlaceHolderInfo.index];
                                allAttrs.presentation.userTransformed = null; // presentation attribute always exists
                                modifyPlaceHolder(drawingNode, allAttrs);

                                comparePlaceHolder.onlyFirst = _.without(comparePlaceHolder.onlyFirst, firstPlaceHolderInfo); // removing from collector
                                comparePlaceHolder.onlySecond = _.without(comparePlaceHolder.onlySecond, secondPlaceHolderInfo); // removing from collector

                                found = true;
                            }
                        }
                    });
                });
            }

            // Step 3: Trying to reduce mismatch of 'comparePlaceHolder.onlyFirst' and 'comparePlaceHolder.onlySecond'
            //         by some 'magic': title <-> ctrTitle, body <-> subTitle
            //         -> modifying the type of the place holder drawing via setAttribute operation
            switchDrawings = self.drawingStyles.analyzeComparePlaceHolder(comparePlaceHolder);
            if (switchDrawings.length > 0) {

                _.each(switchDrawings, function (oneSwitch) {

                    var // the place holder info of the source
                        from = oneSwitch.from,
                        // the place holder info of the destination
                        to = oneSwitch.to,
                        // the attributes for the setAttributes operation
                        allAttrs = null,
                        // the drawing node
                        drawingNode = allOldPHAttrs[from.type][from.index];

                    if (allNewPHAttrs[to.type][to.index]) {
                        allAttrs = _.copy(allNewPHAttrs[to.type][to.index], true);
                    }

                    allAttrs = allAttrs || {};
                    allAttrs.presentation = allAttrs.presentation || {};

                    allAttrs.presentation.phType = to.type;
                    allAttrs.presentation.phIndex = to.index;
                    allAttrs.presentation.userTransformed = null;

                    modifyPlaceHolder(drawingNode, allAttrs);
                });
            }

            // Step 4: Place holder drawings that exist only in the new layout -> creating new place holder drawings
            if (comparePlaceHolder.onlySecond.length > 0) {
                _.each(comparePlaceHolder.onlySecond, function (placeHolderInfo) {
                    insertPlaceHolder(placeHolderInfo, allNewPHAttrs);
                });
            }

            // Step 5: Place holder drawings that exist only in the old layout (and are not empty (empty drawings are already removed in step 1))
            //         -> special handling required, setting property 'userTransformed' to true.
            if (comparePlaceHolder.onlyFirst.length > 0) {
                _.each(comparePlaceHolder.onlyFirst, function (placeHolderInfo) {
                    var drawingNode = allOldPHAttrs[placeHolderInfo.type][placeHolderInfo.index];
                    modifyPlaceHolder(drawingNode, { presentation: { userTransformed: true } });
                });
            }

            // Step 6: Removing hard attributes, so that setting a new layout can be used to reset to the original layout state
            if (comparePlaceHolder.both.length > 0) {
                _.each(comparePlaceHolder.both, function (placeHolderInfo) {
                    var drawingNode = allOldPHAttrs[placeHolderInfo.type][placeHolderInfo.index];
                    var allAttrs = null;
                    if (allNewPHAttrs && allNewPHAttrs[placeHolderInfo.type][placeHolderInfo.index]) { allAttrs = allNewPHAttrs[placeHolderInfo.type][placeHolderInfo.index]; }
                    allAttrs.presentation.userTransformed = null; // 'allAttrs.presentation' is always defined
                    modifyPlaceHolder(drawingNode, allAttrs);
                });
            }

            // Step 7: Generating the 'delete' operations at the end of all operations
            if (deletePositions.length > 0) {
                // sorting the drawings for logical positions and reverting the order
                deletePositions = _.sortBy(deletePositions, function (pos) { return _.last(pos); }).reverse();
                // ... and generating operations
                _.each(deletePositions, function (pos) {
                    generator.generateOperation(DELETE, { start: _.copy(pos) });
                });
            }

            // ... and finally applying the operations (but only, if no global generator was provided)
            if (!globalGenerator) { self.applyOperations(generator); }
        }

    };
}

// constants --------------------------------------------------------------

/**
 * The attributes that are used for new inserted drawings.
 */
OdfLayoutMixin.DEFAULT_INSERT_ODF_LAYOUT_ID = 'layout_3';
