/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import ox from '$/ox';

import { is, uuid } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getMSFormatIsoDateString } from '@/io.ox/office/editframework/utils/dateutils';
import { calculateUserId, OX_PROVIDER_ID } from '@/io.ox/office/editframework/utils/operationutils';
import { convertLengthToHmm, generateInitialsFromAuthorName, getArrayOption, getNumberOption, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import { CHANGE_COMMENT, DELETE_COMMENT, INSERT_COMMENT } from '@/io.ox/office/presentation/utils/operations';
import ThreadedCommentModel from '@/io.ox/office/presentation/model/threadedcommentmodel';
import ThreadedCommentCollection from '@/io.ox/office/presentation/model/threadedcommentcollection';

// constants ==============================================================

var COMMENT_POSITION__OFFSET_DEFAULT = convertLengthToHmm(20, 'px'); // a comment( bubble)'s default offset spans 20 pixel into any direction.

// mix-in class CommentOperationMixin =====================================

/**
 * A mix-in for the document model class PresentationModel providing
 * the comment operation handling used in a presentation document.
 */
function CommentOperationMixin(app) {
    var
        // {PresentationModel} reference for local functions
        docModel = this,
        docView,

        // keeps track of all available comments.
        commentRegistry         = {},

        // keeps track of all available threaded-comment-collections.
        threadedCommentRegistry = {},

        deleteAsThreadCommentModelList  = [],

        deleteCommentUndoOperationList  = [],
        deleteCommentRedoOperationList  = [];

    // private methods ----------------------------------------------------

    // any local tools and helpers.

    function getSlideIndexBySlideId(slideId) {
        var slidePosition = docModel.getSlidePositionById(slideId);
        return ((slidePosition !== null) ? slidePosition[0] : -1);
    }

    function reIndexCommentList(slideId, commentList, fromIndex) {
        // globalLogger.info('+++ CommentOperationMixin :: reIndexCommentList :: commentList : ', commentList);
        var idx     = fromIndex;
        var comment = commentList[idx];

        while (comment) {

            comment.commentIndex  = idx;
            comment.id            = [comment.slideIndex, idx].join('.');

            comment = commentList[++idx];
        }
        return (idx === commentList.length); // {Boolen} `isSuccess`
    }

    function createComment(slideIndex, commentIndex, slideId, parent, operation, hasInvalidParent, replacedParentId) {

        // not in any case both values will be provided nor will each of it be provided in a way that a backend is willing to consume when sent back.

        var authorId = ((('authorId' in operation) && String(operation.authorId).trim()) || null); // backend needs {Null} value as failure case.
        var authorItem = app.getRegisteredAuthorItemById(authorId); // (un)registered author.

        // authorItem: {
        //   id:         authorId, // {String|Number}
        //   name:       authorName, // non empty {String}
        //   initials:   (getStringOption(authorOptions, 'initials', '').trim() || null), // non empty {String} || {Null}
        //   providerId: (getStringOption(authorOptions, 'providerId', '').trim() || null), // non empty {String} || {Null}
        //   userId:     (getStringOption(authorOptions, 'userId', '').trim() || null) // non empty {String} || {Null}
        // }
        var authorName = ((authorItem && authorItem.name) || getStringOption(operation, 'author', '').trim());

        var userId = ((authorItem && authorItem.userId) || getStringOption(operation, 'userId', '').trim() || null);
        var providerId = ((authorItem && authorItem.providerId) || getStringOption(operation, 'providerId', '').trim() || null);

        // var colorIndex  = operation.colorIndex; //          // do not use the (backend-provided) operation-data ...
        var colorIndex = app.getAuthorColorIndex(authorName); // ... use the color-index related author-list instead.
        var mentions = getArrayOption(operation, 'mentions');

        var commentObject = {
            uuid: uuid.random(), // String  //
            slideId, // String            // `slideId` and `uuid` will form the `anchor` within the `ThreadedCommentModel`.
            slideIndex, // Number
            commentIndex, // Number
            parent, // Object (comment type/model)
            hasInvalidParent,
            replacedParentId,
            attributes: {
                posX: Number(operation.pos[0]), // unit: 'hmm'.
                posY: Number(operation.pos[1]), // unit: 'hmm'.
                authorId,
                author: authorName,
                userId,
                providerId,
                date: operation.date && String(operation.date),
                text: operation.text ? String(operation.text) : '',
                colorIndex
            }
        };

        if (mentions) {
            commentObject.attributes.mentions = mentions;
        }

        // create comment type/model
        return commentObject;
    }

    // any local undo operation functionality.

    function createChangeCommentUndoOperation(operation, comment, external) {
        // operation: {
        //     name:       "changeComment",
        //     start:      [0, 0],
        //     text:       "adfböldbsödlabfjöldsabsdfabsdbsdbssd sdbsdBSdb sDBSDBS db sdB SDb"
        // }
        var isTextChange      = is.string(operation.text);
        var isPositionChange  = (is.array(operation.pos) && (operation.pos.length === 2));
        var undoOperation     = null; // undo operation object, if there is any change in the comment
        var attributes        = null; // attributes object of a current comment (not changed), if there is any text change in the comment
        var currentMentions   = null; // mentions in the current (not changed) comment
        var mentionsCount     = null;

        var isSuccess = (isTextChange || isPositionChange);
        if (isSuccess) {

            var undoManager = docModel.getUndoManager();
            if (undoManager.isUndoEnabled() && !external) {
                undoOperation = {
                    name:   operation.name,
                    start:   _.copy(operation.start)
                };

                if (isTextChange) {
                    attributes = comment.attributes;
                    currentMentions = attributes.mentions;
                    mentionsCount = currentMentions && currentMentions.length;

                    // save text-value for restoring from yet unchanged `comment` model.
                    undoOperation.text = attributes.text;

                    if (mentionsCount) {
                        undoOperation.mentions = currentMentions;
                    }
                }

                if (isPositionChange) {
                    // save position-list for restoring from yet unchanged `comment` model.
                    attributes = comment.attributes;
                    undoOperation.pos = [attributes.posX, attributes.posY]; // all values in 'hmm'.
                }

                if ('parent' in operation) {
                    undoOperation.parent = operation.parent;
                }

                undoManager.addUndo(undoOperation, operation);
            }
        }
        return isSuccess;
    }

    function createDeleteCommentUndoOperation(comment, operationOrGenerator, external) {
        // operation: {
        //     name:  "deleteComment",
        //     start: [0, 0]
        //     // is_delete_as_thread: true
        // }
        var generator   = ((('appendOperations' in operationOrGenerator) && operationOrGenerator) || null);
        var operation   = (((generator === null) && operationOrGenerator) || {});

        var undoManager = docModel.getUndoManager();
        if (undoManager.isUndoEnabled() && !external) {

            var attributes  = comment.attributes;

            var authorId    = attributes.authorId;
            var authorItem  = app.getRegisteredAuthorItemById(authorId); // (un)registered author.

            var authorName  = ((authorItem && authorItem.name) || attributes.author || null);
            var initials    = ((authorItem && authorItem.initials) || (authorName && generateInitialsFromAuthorName(authorName)));

            var providerId  = ((authorItem && authorItem.providerId) || attributes.providerId);
            var userId      = ((authorItem && authorItem.userId) || attributes.userId);

            var startList   = (operation.start || [getSlideIndexBySlideId(comment.slideId), comment.commentIndex]);

            var mentions    = attributes.mentions;

            var undoOperation = {
                name:   INSERT_COMMENT,
                start:   _.copy(startList),
                text:   attributes.text,
                date:   attributes.date,
                pos:    [attributes.posX, attributes.posY] // all values in 'hmm'.
            };
            if ((authorName !== null) && (initials !== null)) {

                undoOperation.author    = authorName;
                undoOperation.initials  = initials;
            }
            if (providerId !== null) {

                undoOperation.providerId = providerId;
            }
            if (userId !== null) {

                undoOperation.userId = userId;
            }
            if (mentions !== null) {
                undoOperation.mentions = mentions;
            }
            if (comment.parent) {

                undoOperation.parent = comment.parent.commentIndex;
            }
            if (generator) {
                // - `generator` takes place only in case a slide is going to be deleted.
                // - `generator` enables grouped undo operations for comments that are close to being deleted.

                generator.appendOperations(undoOperation);

            } else {
                var insertCommentOperationList;
                var deleteCommentOperationList;

                if (operation.is_delete_as_thread === true) {

                    deleteCommentUndoOperationList.unshift(undoOperation); // LIFO *stack* of 'insertComment' operations.
                    deleteCommentRedoOperationList.push(operation); // FIFO *stack* of 'deleteComment' operations.

                    if (!comment.parent) {

                        insertCommentOperationList = Array.from(deleteCommentUndoOperationList);
                        deleteCommentOperationList = Array.from(deleteCommentRedoOperationList);

                        // entirely empty both temporarily used lists.
                        deleteCommentUndoOperationList.length = 0;
                        deleteCommentRedoOperationList.length = 0;

                        undoManager.addUndo(insertCommentOperationList, deleteCommentOperationList);
                    }
                } else if (operation.is_delete_as_plain_list === true) {

                    deleteCommentUndoOperationList.unshift(undoOperation); // LIFO *stack* of 'insertComment' operations.
                    deleteCommentRedoOperationList.push(operation); // FIFO *stack* of 'deleteComment' operations.

                    if (operation.is_last_item === true) {

                        insertCommentOperationList = Array.from(deleteCommentUndoOperationList);
                        deleteCommentOperationList = Array.from(deleteCommentRedoOperationList);

                        // entirely empty both temporarily used lists.
                        deleteCommentUndoOperationList.length = 0;
                        deleteCommentRedoOperationList.length = 0;

                        undoManager.addUndo(insertCommentOperationList, deleteCommentOperationList);
                    }
                } else {
                    undoManager.addUndo(undoOperation, operation);
                }
            }
        }
    }

    function createInsertCommentUndoOperation(operation, startArray, external) {
        var undoManager = docModel.getUndoManager();
        if (undoManager.isUndoEnabled() && !external) {

            var undoOperation = {
                name:   DELETE_COMMENT,
                start:   _.copy(startArray)
            };
            undoManager.addUndo(undoOperation, operation);
        }
    }

    // any local helper for creating and applying insert comment operations.

    function createInsertCommentOperation(slideId, startArray, commentInfo, parentComment) {
        // globalLogger.info('+++ CommentOperationMixin :: createInsertCommentOperation :: startArray, text, parentComment : ', startArray, text, parentComment);

        var commentCollection = docModel.getCommentCollection(slideId); // access `slideId`-specific threaded-comment-collection.

        var positionList = parentComment && parentComment.getPositionList();
        if (!positionList) {
            var position = (commentCollection.getSelectedThread() && commentCollection.getSelectedThread().getPositionList()) || [COMMENT_POSITION__OFFSET_DEFAULT, COMMENT_POSITION__OFFSET_DEFAULT];
            positionList = docModel.getCommentCollection(slideId).createNewCommentPosition(position); // all values in 'hmm'.
        }

        var activeUserId = ox.user_id;
        var activeClient = app.getActiveClients().filter(function (client) { return (client.userId === activeUserId); })[0];
        var isGuest      = (!!activeClient && activeClient.guest);

        /**
         *  fixing #Bug 68174 - "Better 'user name' for external invitees / collaborators"
         *  [https://bugs.open-xchange.com/show_bug.cgi?id=68174]
         *
         *  Though this bug describes spreadsheet behavior it applies to presentation too.
         *
         *  This solutions uses different ways of accessing a user's/client's name.
         *  In order to not end up with a cryptic generated name for an external invitee like 'User 1147', as it
         *  would happen with `getClientOperationName` one uses `getEditClientUserName` for an identified guest.
         */
        var authorName  = ((isGuest && app.getEditClientUserName()) || app.getClientOperationName()).trim();
        var initials    = generateInitialsFromAuthorName(authorName);

        var userId      = String(calculateUserId(activeUserId));
        // var colorIndex = app.getAuthorColorIndex(authorName);
        var operation = {
            start:       _.copy(startArray),
            text:       commentInfo.text,
            date:       getMSFormatIsoDateString(),
            pos:        positionList,
            author:     authorName,
            initials,
            providerId: OX_PROVIDER_ID,
            userId
        };
        if (parentComment) {
            operation.parent = commentCollection.getCommentIndex(parentComment);
            // operation.parent = threadedCommentCollection.getCommentIndex(parentComment); // MERGE
        }

        if (commentInfo.mentions) {
            operation.mentions = commentInfo.mentions;
        }

        return operation;
    }

    function applyInsertCommentOperation(operation) {

        var generator = docModel.createOperationGenerator();
        generator.generateOperation(INSERT_COMMENT, operation);

        docModel.applyOperations(generator);
    }

    // any local function that handles a slide event like "deleting a slide".

    function createSlideSpecificDeleteCommentOperations(operationData) {
        // globalLogger.info('+++ CommentOperationMixin :: createSlideSpecificDeleteCommentOperations :: operationData : ', operationData);

        if (operationData.operation.start.length > 1) { return; } // handle only removal of slides

        if (operationData.external) { return; } // handle only local operations

        var generator   = operationData.generator;
        var slideId     = docModel.getSlideIdByPosition(operationData.operation.start);

        var commentList = commentRegistry[slideId]; // access `slideId`-specific comment list.
        if (commentList) {
            // enable grouped undo operations for comments that are close to being deleted.

            commentList.forEach(function (comment) {

                // save `comment` model for restoring.
                createDeleteCommentUndoOperation(comment, generator, operationData.external);
            });
        }
    }

    function handleSlideSpecificDeregistration(payload) {
        // globalLogger.info('+++ CommentOperationMixin :: handleSlideSpecificDeregistration :: payload : ', payload);

        var slideId = docModel.getSlideId(payload.startNode);

        if (slideId) {
            if (commentRegistry[slideId] || threadedCommentRegistry[slideId]) {
                // delete both the `slideId`-specific comment list and the `slideId`-specific threaded-comment-collection.
                delete commentRegistry[slideId];
                if (threadedCommentRegistry[slideId]) { threadedCommentRegistry[slideId].destroy(); } // DOCS-5059: deleting the collection
                delete threadedCommentRegistry[slideId];
            }

            // check for "no slides" at all in standard view mode.
            var slideIdList = docModel.getStandardSlideOrder(true);
            if ((slideIdList.length === 1) && (slideIdList[0] === slideId)) {
                // `slideIdList` not yet empty but last list item equals `slideId`.
                // sanitize ... just to be on the save side, but anyhow should be already an empty object.
                commentRegistry = {};
                threadedCommentRegistry = {};

                docModel.trigger('threadedcomment:registry:empty');
            }
        }
    }

    /**
     * Cleanup functionality when the document is closed
     */
    function handleAllSlidesDeregistration() {
        for (const slideId in threadedCommentRegistry) { threadedCommentRegistry[slideId].destroy(); } // DOCS-5059: deleting the collection
        commentRegistry = {};
        threadedCommentRegistry = {};
    }

    function handleBubblePositionChange(payload/*{ model: theadedCommentModel, node: elementToDrag, position: { x: bubbleLeft, y: bubbleTop } }*/) {
        // globalLogger.info('+++ CommentOperationMixin :: handleBubblePositionChange :: payload : ', payload);
        docModel.applyChangeComment(payload.model, {}, { x: convertLengthToHmm(payload.position.x, 'px'), y: convertLengthToHmm(payload.position.y, 'px') });
    }

    // public methods -----------------------------------------------------

    // Any public method that does handle an operation. Thus each
    // one also has been registered via `registerPlainOperationHandler`.

    docModel.handleChangeComment = function (operation, external) {

        var isSuccess = false;

        if (operation && (operation.name === CHANGE_COMMENT)) {

            var slideIndex  = Number(operation.start[0]); // slide position ... or slide index.
            var slideId     = String(docModel.getSlideIdByPosition(slideIndex)); // slide id.
            var commentList = commentRegistry[slideId]; // access `slideId`-specific comment list.

            if (commentList) {
                var commentCollection = docModel.getCommentCollection(slideId); // access `slideId`-specific threaded-comment-collection.

                var commentIndex  = Number(operation.start[1]); // comment position ... index (or count) of a comment in relation to a certain slide.
                var comment       = commentList[commentIndex];

                if (comment && (comment.slideId === slideId)) {

                    // save text-value and/or position-list for restoring from yet unchanged `comment` model.
                    isSuccess = createChangeCommentUndoOperation(operation, comment, external);

                    if (isSuccess) {
                        var isTextChange      = is.string(operation.text);
                        var isPositionChange  = (is.array(operation.pos) && (operation.pos.length === 2));

                        // keep everything in sync with the `slideId`-specific comment list.
                        //
                        var threadedCommentModel = commentCollection.getCommentById(comment.uuid);

                        if (isTextChange) {
                            var textValue = operation.text;
                            var mentionsValue = operation.mentions;

                            // assign changed text value to both models.
                            //
                            comment.attributes.text = textValue;
                            comment.attributes.mentions = mentionsValue;
                            threadedCommentModel.setText(textValue);
                            threadedCommentModel.setMentions(mentionsValue);
                        }
                        if (isPositionChange) {
                            var positionList = operation.pos; // all values in 'hmm'.

                            var posX = positionList[0];
                            var posY = positionList[1];

                            // assign changed position value(s) to both models.
                            //
                            comment.attributes.posX = posX;
                            comment.attributes.posY = posY;
                            threadedCommentModel.setPosition(posX, posY);
                        }

                        docModel.trigger('change:threadedcomment', threadedCommentModel);
                    }
                }
            }
        }
        return isSuccess;
    };

    docModel.handleDeleteComment = function (operation, external) {

        var isSuccess = false;

        if (operation && (operation.name === DELETE_COMMENT)) {

            var slideIndex    = Number(operation.start[0]); // slide position ... or slide index.
            var slideId       = String(docModel.getSlideIdByPosition(slideIndex)); // slide id.

            var commentIndex  = Number(operation.start[1]); // comment position ... index (or count) of a comment in relation to a certain slide.
            var commentList   = commentRegistry[slideId]; // access `slideId`-specific comment list.

            if (commentList) {
                var commentCollection = docModel.getCommentCollection(slideId); // access `slideId`-specific threaded-comment-collection.

                // save `comment` model while it gets removed from the internally stored comment list.
                var comment = commentList.splice(commentIndex, 1)[0];

                // save `comment` model for restoring.
                createDeleteCommentUndoOperation(comment, operation, external);

                isSuccess = reIndexCommentList(slideId, commentList, commentIndex);

                if (isSuccess) {

                    // access `ThreadedCommentModel` for passing it with a final 'delete comment' view-event.
                    var commentModel = commentCollection.getByIndex(commentIndex);

                    // if (operation.is_delete_as_thread === true || !threadedCommentModel.isReply()) {
                    if (operation.is_delete_as_thread === true) {

                        // FIFO *stack* of comments that have been(model) / are going to be(view) deleted
                        deleteAsThreadCommentModelList.push(commentModel);

                        if (!comment.parent) {
                            var threadedCommentModelList = Array.from(deleteAsThreadCommentModelList);

                            // entirely empty the temporarily used list.
                            deleteAsThreadCommentModelList.length = 0;

                            // keep everything in sync with the `slideId`-specific comment list.
                            commentCollection.removeThreadFromList(commentModel);

                            deleteUnsavedComment(commentCollection, [commentModel]);
                            // an entire thread has been(model) / is going to be(view) deleted.
                            docModel.trigger('delete:threadedcomment:end', threadedCommentModelList);

                            // destroy the comment model objects
                            threadedCommentModelList.forEach(model => model.destroy());
                        }
                    } else {
                        // keep everything in sync with the `slideId`-specific comment list.
                        commentCollection.removeCommentFromList(commentModel, commentList, external);

                        deleteUnsavedComment(commentCollection, [commentModel]);
                        // just one comment has been(model) / is going to be(view) deleted.
                        //
                        docModel.trigger('delete:threadedcomment:end', [commentModel]);

                        if (!commentModel.isReply()) {

                            // e.g. for removing comments operates with the 'is_delete_as_plain_list' flag ...
                            // ... one wants to trigger e.g. the removal of comment-bubbles
                            //
                            docModel.trigger('delete:threadedcomment:thread', commentModel);
                        }

                        // destroy the comment model object
                        commentModel.destroy();
                    }
                }
            }
        }
        return isSuccess;
    };

    function deleteUnsavedComment(commentCollection, deletedComments) {
        var unsavedComment = commentCollection.getUnsavedComment();
        if (unsavedComment) {
            var unsavedCommentModel = unsavedComment.model;
            var editComment = _.find(deletedComments, function (comment) {
                return (comment.equals(unsavedCommentModel) || (!comment.isReply() && unsavedCommentModel.isReply() && comment.getId() === unsavedCommentModel.getParentId()));
            });
            if (editComment) {
                commentCollection.deleteUnsavedComment();
            }
        }
    }

    /**
     * The handler for the `insertComment` operation.
     *
     * @param {Object} operation
     *  The logical start position for the new comment.
     *
     * @returns {Boolean}
     *  Whether the comment has been inserted successfully.
     */
    docModel.handleInsertComment = function (operation, external) {

        var isSuccess = false;

        if (operation && (operation.name === INSERT_COMMENT)) {

            var slideIndex    = Number(operation.start[0]); // slide position ... or slide index.
            var slideId       = String(docModel.getSlideIdByPosition(slideIndex)); // slide id.

            // create and/or access `slideId`-specific comment list.
            var commentList   = (commentRegistry[slideId] || (commentRegistry[slideId] = []));

            var commentIndex  = Number(operation.start[1]); // comment position ... index (or count) of a comment in relation to a certain slide.
            var parentIndex   = (('parent' in operation) ? Number(operation.parent) : null); // parent comment index (or count) || null value.

            var parent        = (((parentIndex !== null) && commentList[parentIndex]) || null); // null value || parent comment.
            var hasInvalidParent = false;
            var replacedParentId = null;

            if (Number(parentIndex) && (!parent || parent.parent)) { // no parent found or the found parent also has a parent (not allowed)
                hasInvalidParent = true;
                if (commentList.length && !commentList[0].parent) { replacedParentId = commentList[0].uuid; } // using the first thread on the slide
                globalLogger.warn('Invalid insertComment operation! No valid parent specified. Parent: ' + parentIndex + '. Operation:', operation);
            }

            var comment       = createComment(slideIndex, commentIndex, slideId, parent, operation, hasInvalidParent, replacedParentId);

            var isImportFinished = docModel.isImportFinished(); // Performance: DOCS-2377, no DOM manipulation in the loading phase

            commentList.splice(commentIndex, 0, comment);

            // save `comment` positioning for undo operation.
            if (isImportFinished) { createInsertCommentUndoOperation(operation, [slideIndex, commentIndex], external); }

            isSuccess = reIndexCommentList(slideId, commentList, (commentIndex + 1));

            if (isSuccess) {

                // create and/or access `slideId`-specific threaded-comment-collection.
                var commentCollection = docModel.getCommentCollection(slideId);

                var parentModel = comment.parent && commentCollection.getCommentById(comment.parent.uuid);
                var commentModel = new ThreadedCommentModel(docModel, comment, parentModel);

                // keep everything in sync with the `slideId`-specific comment list.
                commentCollection.insertCommentIntoList(commentModel, commentIndex);

                if (isImportFinished) {

                    var isNewComment  = (operation.is_new_comment === true);
                    var isNewThread   = (operation.is_new_thread === true);
                    // show the new comment only, if this is not an external operation or the affected slide is active (DOCS-2703)
                    var showComment = !external || slideId === docModel.getActiveSlideId();

                    if (showComment) {
                        if (isNewThread) {
                            // does e.g. trigger the rendering of a new comment-bubble.
                            //
                            docModel.trigger('new:threadedcomment:thread', commentModel);

                            // does in addition trigger the highlighting of the newly created comment-bubble.
                            commentCollection.selectComment(commentModel);

                        } else if (isNewComment) {
                            docModel.trigger('new:threadedcomment:comment', commentModel);

                        } else {
                            docModel.trigger('insert:threadedcomment', commentModel);
                        }
                    }
                }
            }
        }
        return isSuccess;
    };

    // Any public method that does create an operation. Thus each
    // one also has been triggered via the user interface (view).

    docModel.applyChangeComment = function (commentModel, commentInfo, positionData) {

        var commentCollection = docModel.getCommentCollection(commentModel.getSlideId()); // access `slideId`-specific threaded-comment-collection.   // MERGE

        var isTextChange = (commentInfo && commentInfo.text !== null && commentInfo.text !== undefined);
        var isPositionChange = (positionData && ('x' in positionData) && ('y' in positionData));

        var operation = {
            start:  _.copy(commentCollection.getCommentStart(commentModel))
        };
        if (isTextChange) {
            operation.text = commentInfo.text;
        }
        if (commentInfo.mentions) {
            operation.mentions = commentInfo.mentions;
        }
        if (isPositionChange) {
            operation.pos = [positionData.x, positionData.y]; // all values in 'hmm'.
        }

        if (commentModel.isReply()) {

            operation.parent = commentCollection.getCommentIndex(commentModel.getParent());
        }
        var generator = docModel.createOperationGenerator();
        generator.generateOperation(CHANGE_COMMENT, operation);

        docModel.applyOperations(generator);
    };

    docModel.applyDeleteComment = function (commentModel) {
        if (commentModel.isReply()) {

            var commentCollection = docModel.getCommentCollection(commentModel.getSlideId()); // access `slideId`-specific threaded-comment-collection.

            var operation = {
                start:  _.copy(commentCollection.getCommentStart(commentModel))
            };
            var generator = docModel.createOperationGenerator();
            generator.generateOperation(DELETE_COMMENT, operation);

            docModel.applyOperations(generator);
        }
    };

    docModel.applyDeleteThread = function (threadList) {
        if (is.array(threadList)) {

            var commentModel      = threadList.pop();
            var commentCollection = (commentModel && docModel.getCommentCollection(commentModel.getSlideId())); // access `slideId`-specific threaded-comment-collection.

            var generator = (commentModel && docModel.createOperationGenerator());

            var operation;
            var operationLabel = DELETE_COMMENT;

            while (commentModel) {
                operation = {
                    start:  _.copy(commentCollection.getCommentStart(commentModel)),

                    is_delete_as_thread: true // additional flag targeting the client side handling only, especially generating the undo operation.
                };
                generator.generateOperation(operationLabel, operation);

                commentModel = threadList.pop();
            }
            if (generator) {
                docModel.applyOperations(generator);
            }
        }
    };

    docModel.applyDeleteEveryListedComment = function (threadedCommentList) {
        if (is.array(threadedCommentList)) {

            var commentCollection;
            var commentModel = threadedCommentList.pop();

            var generator = (commentModel && docModel.createOperationGenerator());

            var operation;
            var operationLabel = DELETE_COMMENT;

            while (commentModel) {

                // access `slideId`-specific threaded-comment-collection.
                commentCollection = (commentModel && docModel.getCommentCollection(commentModel.getSlideId()));

                operation = {
                    start:  _.copy(commentCollection.getCommentStart(commentModel)),

                    is_delete_as_plain_list: true // additional flag targeting the client side handling only, especially generating the undo operation.
                };
                commentModel = threadedCommentList.pop();

                if (!commentModel) {
                    operation.is_last_item = true;
                }
                generator.generateOperation(operationLabel, operation);
            }
            if (generator) {
                docModel.applyOperations(generator);
            }
        }
    };

    /**
     * Generate the insert operations to copy the comments from the source slide to the target slide.
     *
     * @param {Number} fromSlideIndex the index of the source slide.
     * @param {Number} toSlideIndex the index of the target slide.
     * @param {Generator} generator the generator to add the insert comment operations.
     */
    docModel.copySlideComments = function (fromSlideIndex, toSlideIndex, generator) {

        function createOperation(start, commentModel, parentComment, parentIndex) {

            var positionList = parentComment ? parentComment.getPositionList() : commentModel.getPositionList();

            var authorName  = commentModel.getAuthor();
            var initials    = generateInitialsFromAuthorName(authorName);

            var operation = {
                start:       _.copy(start),
                text:       commentModel.getText(),
                date:       app.isODF() ? commentModel.getDate() : new Date(new Date(commentModel.getDate()).getTime() + 1).toISOString(),
                pos:        positionList,
                author:     authorName,
                initials,
                providerId: OX_PROVIDER_ID,
                userId:     commentModel.getUserId()
            };

            if (parentComment) {
                operation.parent = parentIndex;
            }

            if (commentModel.getMentions()) {
                operation.mentions = commentModel.getMentions();
            }

            return operation;
        }

        if (fromSlideIndex === toSlideIndex || !generator) {
            return;
        }

        var commentCollection = docModel.getCommentCollection(docModel.getSlideIdByPosition(fromSlideIndex));
        var commentModels = commentCollection ? commentCollection.getCommentModels() : null;

        if (commentModels) {

            var commentIndex = 0;

            var parentComment;
            var parentIndex;

            commentModels.forEach(function (commentModel) {
                var start = [toSlideIndex, commentIndex];
                var operation;
                if (commentModel.isReply()) {
                    operation = createOperation(start, commentModel, parentComment, parentIndex);
                    // operation.is_new_comment = true;
                } else {
                    operation = createOperation(start, commentModel, null, null);
                    parentComment = commentModel;
                    parentIndex = commentIndex;
                    // operation.is_new_thread = true;
                }

                generator.generateOperation(INSERT_COMMENT, operation);
                commentIndex++;
            });
        }

    };

    docModel.applyInsertComment = function (anchor, commentInfo, positionList) {
        var text = commentInfo.text;
        // globalLogger.info('+++ CommentOperationMixin :: applyInsertComment :: anchor, text : ', anchor, text);

        // most probably a newly created comment related to the parent-thread that identifies itself by the passed anchor.
        if (anchor && is.string(text)) {

            if (text) {
                var startArray;
                var operation;

                var slideId     = anchor.valueOf().slideId;
                var slideIndex  = getSlideIndexBySlideId(slideId);

                // access `slideId`-specific threaded-comment-collection.
                var commentCollection = docModel.getCommentCollection(slideId); // {ThreadedCommentCollection|undefined}

                var ignoreCommentsWithInvalidParent = true;
                var threadList    = ((text !== '') && !!commentCollection && commentCollection.getByAddress(anchor, null, ignoreCommentsWithInvalidParent)); // {Array|false}
                var commentCount  = (threadList && (threadList.length)); // {Number|false}

                var parentComment = ((commentCount >= 1) && threadList[0]); // {ThreadedCommentModel|false}
                if (parentComment) {
                    // for sure a newly to be created comment related to the parent-thread that was identified by the passed anchor.

                    var lastComment   = threadList[commentCount - 1];

                    // slideId     = lastComment.getSlideId();
                    // slideIndex  = getSlideIndexBySlideId(slideId);

                    // `commentCount - 1` and `lastCommentIndex` are not equal in their meaning.

                    var lastCommentIndex  = commentCollection.getCommentIndex(lastComment);
                    var newCommentIndex   = (lastCommentIndex + 1);

                    startArray = [slideIndex, newCommentIndex];

                    // globalLogger.info('+++ CommentOperationMixin :: applyInsertComment :: startArray, text, parentComment : ', startArray, text, parentComment);

                    commentInfo.text = text;
                    operation = createInsertCommentOperation(slideId, startArray, commentInfo, parentComment);
                    operation.is_new_comment = true;

                } else {
                    // with no available parent-comment 'insertComment' will be handled as an 'insertThread'.

                    var commentList   = (commentRegistry[slideId] || []); // access `slideId`-specific comment list or an empty list.
                    var commentIndex  = commentList.length;

                    startArray = [slideIndex, commentIndex];

                    // globalLogger.info('+++ CommentOperationMixin :: applyInsertComment :: startArray, text, parentComment : ', startArray, text, parentComment);

                    // the creation of both a new `slideId`-specific comment list and a new `slideId`-specific threaded-comment-collection
                    // is a task that is handled by the 'insertComment' handler .... `docModel.handleInsertComment`.

                    commentInfo.text = text;
                    operation = createInsertCommentOperation(slideId, startArray, commentInfo); // no parent.
                    operation.is_new_thread = true;

                    // only threads that are going to be created support predefined positioning e.g. via the universal-context-menu.
                    if (is.array(positionList)) {

                        // the newly created thread will feature a predefined position.
                        operation.pos = positionList;
                    }
                }

                if (operation.mentions) {
                    var commentId = String(new Date(operation.date).getTime());
                    app.sendCommentNotification(operation.mentions, operation.text, commentId);
                }

                applyInsertCommentOperation(operation);
            }
        }
    };

    docModel.startNewThread = function (slideIndex, payload) {

        slideIndex  = parseInt(slideIndex, 10);
        slideIndex  = ((Number.isFinite(slideIndex) && (slideIndex >= 0)) ? slideIndex : docModel.getActiveSlideIndex());

        var slideId = String(docModel.getSlideIdByPosition(slideIndex));

        var commentList   = (commentRegistry[slideId] || []); // access `slideId`-specific comment list or an empty list.
        var commentIndex  = commentList.length;

        // the creation of both a new `slideId`-specific comment list and a new `slideId`-specific threaded-comment-collection
        // is a task that is handled by the 'insertComment' handler .... `docModel.handleInsertComment`.

        var operation = createInsertCommentOperation(slideId, [slideIndex, commentIndex], { text: '' }); // no text, no parent.

        // only threads that are going to be created support predefined positioning e.g. via the universal-context-menu.
        if (payload && ('position' in payload)) {
            var position  = (payload && payload.position);

            var pageX     = getNumberOption(position, 'x', null);
            var pageY     = getNumberOption(position, 'y', null);

            if ((pageX !== null) && (pageY !== null)) {

                // the newly created thread will feature a predefined position.
                operation.pos = [pageX, pageY];
            }
        }
        // operation.is_new_thread = true;
        //
        // applyInsertCommentOperation(operation);

        // - usually the commented line above ... `applyInsertCommentOperation(operation);` ...
        //   was the terminating statement of this method.
        //
        // - but in order to handle inserting a thread better in terms of user experience,
        //   there is a detour via notifying the `docView` instance.

        // do not directly apply the 'insertComment' operation, but create each a `comment` type/option
        // and a `threadedCommentModel` in order to notify e.g. a `threadedCommentsPane` first.
        //
        var commentOption         = createComment(slideIndex, commentIndex, slideId, null, operation);
        var threadedCommentModel  = (new ThreadedCommentModel(docModel, commentOption, null, true));

        docModel.trigger('start:threadedcomment', { model: threadedCommentModel,
            popupOptions: {
                anchor: { top: 50, left: 50 },
                coverAnchor: true,
                anchorPadding: 2
            }
        });
    };

    // miscellaneous public helper methods.

    function getSlideSpecificThreadCount(slideId) {
        var count = 0;

        var commentCollection = threadedCommentRegistry[slideId];
        if (commentCollection && commentCollection.hasComments()) {

            count = commentCollection.getParentComments().length;
        }
        return count;
    }
    function getOverallThreadCount() {
        return Object.keys(threadedCommentRegistry).reduce(function (count, slideId) {

            return (count + getSlideSpecificThreadCount(slideId));

        }, 0); // `count` gets default assigned Zero.
    }

    function getListOfEveryRegisteredThreadedComment() {
        return Object.keys(threadedCommentRegistry).reduce(function (threadedCommentList, slideId) {

            var commentCollection = threadedCommentRegistry[slideId];
            if (commentCollection && commentCollection.hasComments()) {

                threadedCommentList = threadedCommentList.concat(commentCollection.getCommentModels());
            }
            return threadedCommentList;

        }, []);
    }

    function getFirstBestSlideIdWithUnsavedComment() {
        var firstBestSlideId;

        Object.keys(threadedCommentRegistry).some(function (slideId) {
            var returnValue = false;

            var commentCollection = threadedCommentRegistry[slideId];
            if (commentCollection && commentCollection.hasUnsavedComment()) {

                firstBestSlideId = slideId;
                returnValue = true;
            }
            return returnValue;
        });

        return firstBestSlideId; // {String|undefined}
    }

    docModel.getSlideIdFromComment = function (dateId) {
        dateId = Number(dateId);
        var res = Object.keys(threadedCommentRegistry).reduce(function (result, slideId, resultIndex) {
            globalLogger.log(resultIndex);
            // var commentCollection = threadedCommentRegistry[slideId];
            var commentCollection = docModel.getCommentCollection(slideId);
            var commentModels = commentCollection.getCommentModels();

            if (!result) {
                result = commentModels.reduce(function (commentsSlideId, commentModel, commentSlideIdIndex) {
                    globalLogger.log(commentSlideIdIndex);
                    var commentDate = null;

                    if (!commentsSlideId) {
                        commentDate = new Date(commentModel.getDate()).getTime();

                        if (commentDate === dateId) {
                            commentsSlideId = slideId;
                        }
                    }

                    return commentsSlideId;

                }, null);
            }

            return result;
        }, null);

        return res;
    };

    docModel.getSlideSpecificThreadCount = function (slideId) {
        return getSlideSpecificThreadCount(slideId);
    };
    docModel.getOverallThreadCount = function () {
        return getOverallThreadCount();
    };

    docModel.isAnyThreadedComment = function () {
        return (getOverallThreadCount() >= 1);
    };

    /**
     * Returns whether this document contains a comment thread with unsaved
     * changes on any slide.
     *
     * @returns {boolean}
     *  Whether this document contains any comment thread with unsaved
     *  changes on any slide.
     */
    docModel.hasUnsavedComments = function () {
        return is.string(getFirstBestSlideIdWithUnsavedComment()); // {Boolean}
    };

    /**
     * Delete unsaved changes in all comment threads in this document.
     */
    docModel.deleteUnsavedComments = function () {
        _.forEach(threadedCommentRegistry, function (commentCollection) {
            commentCollection.deleteUnsavedComment();
        });
    };

    docModel.getFirstBestSlideIdWithUnsavedComment = function () {
        return getFirstBestSlideIdWithUnsavedComment(); // {String|undefined}
    };

    docModel.deleteEveryComment = function () {
        docModel.applyDeleteEveryListedComment(getListOfEveryRegisteredThreadedComment());
    };

    /**
     *  This method is accessed frequently via `docView.getCommentCollection` by e.g. a `CommentsPane` instance,
     *  thus it is best to (lazy evaluate and) cache a `ThreadedCommentCollection` instance.
     *
     *  Such a cached collection instance is valid only for the time of the selection of the current slide and only as long as
     *  the threaded-comment model does not get changed during this selection time.
     *
     *  A `ThreadedCommentCollection` instance is crucial for the reuse of the editframework's `CommentsPane`
     *  because the pane consumes/expects a valid API for such a collection (and its collection members/models).
     *
     * @param {String} [slideId]
     *  An optional slide ID.
     *
     * @returns {ThreadedCommentCollection}
     *  The cached or a new created comment collection object
     */
    docModel.getCommentCollection = function (slideId) {
        if (!slideId) { slideId = docModel.getActiveSlideId(); } // TODO: When does this happen? slideId is always defined.
        return threadedCommentRegistry[slideId] || (threadedCommentRegistry[slideId] = new ThreadedCommentCollection(docModel));
    };

    // initialization -----------------------------------------------------

    app.onInit(function () {
        docView = app.getView();
    });

    docModel.waitForImportSuccess(function () {

        docModel.listenTo(docView, 'bubble:position:change', handleBubblePositionChange);

        // update the view after insert/remove slide
        docModel.listenTo(docModel, 'operation:delete:paragraph', createSlideSpecificDeleteCommentOperations);
        docModel.listenTo(docModel, 'delete:paragraph:before', handleSlideSpecificDeregistration);
        docModel.listenTo(docModel, 'delete:allslides:before', handleAllSlidesDeregistration);
    });
}

// exports ================================================================

export default CommentOperationMixin;
