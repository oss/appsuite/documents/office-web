/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE, setFocus, hasKeyCode } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getExplicitAttributeSet, isFillThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import ModelAttributesMixin from '@/io.ox/office/presentation/model/modelattributesmixin';
import SlideAttributesMixin from '@/io.ox/office/presentation/model/slideattributesmixin';
import SlideOperationMixin from '@/io.ox/office/presentation/model/slideoperationmixin';
import ObjectOperationMixin from '@/io.ox/office/presentation/model/objectoperationmixin';
import CommentOperationMixin from '@/io.ox/office/presentation/model/commentoperationmixin';
import PageHandlerMixin from '@/io.ox/office/presentation/model/pagehandlermixin';
import UpdateDocumentMixin from '@/io.ox/office/presentation/model/updatedocumentmixin';
import DynFontSizeMixin from '@/io.ox/office/presentation/model/dynfontsizemixin';
import DragToCopyMixin from '@/io.ox/office/presentation/model/dragtocopymixin';
import OdfLayoutMixin from '@/io.ox/office/presentation/model/odflayoutmixin';
import { drawDrawingSelection } from '@/io.ox/office/presentation/components/drawing/drawingresize';
import ListHandlerMixin from '@/io.ox/office/textframework/model/listhandlermixin';
import UpdateListsMixin from '@/io.ox/office/textframework/model/updatelistsmixin';
import Editor from '@/io.ox/office/textframework/model/editor';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getOxoPosition, getParagraphElement, getParagraphLength, getParagraphNodeLength,
    hasSameParentComponent, increaseLastIndex, isValidElementPosition } from '@/io.ox/office/textframework/utils/position';
import { COMPACT_DEVICE, MIN_MOVE_DURATION, convertLengthToHmm,
    getBooleanOption, getDomNode, getEmptySlideBackgroundAttributes, getNumberOption,
    getObjectOption, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import SelectionBox from '@/io.ox/office/textframework/view/selectionbox';
import * as Op from '@/io.ox/office/presentation/utils/operations';
import { NO_TEXT_PLACEHOLDER_TYPES,
    getODFFooterRepresentation, getPlaceHolderDrawingType, getPlaceHolderTemplateText,
    getValidTypeIndexSet, isCustomPromptPlaceHolderDrawing, isCustomPromptPlaceHolderAttributeSet,
    isPlaceHolderAttributeSet, isPlaceHolderDrawing, isPlaceHolderWithoutTextDrawing,
    isEmptyPlaceHolderDrawing, isODFReadOnlyPlaceHolderDrawing, isTitleOrSubtitlePlaceHolderDrawing,
    isTitlePlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';
import { NO_GROUP_CLASS, NODE_SELECTOR, TEXTFRAME_NODE_SELECTOR, getDrawingType, isGroupedDrawingFrame, isTableDrawingFrame,
    updateSelectionDrawingSize } from '@/io.ox/office/drawinglayer/view/drawingframe';
import CommentLayer from '@/io.ox/office/presentation/components/comment/commentlayer';
import SlideFieldManager from '@/io.ox/office/presentation/components/field/slidefieldmanager';
import { TEMPORARY_MARKER_CLASS } from '@/io.ox/office/presentation/view/dialog/drawingformatdialog';
import PresentationModeManager from '@/io.ox/office/presentation/components/present/presentationmodemanager';
import { NumberFormatter } from '@/io.ox/office/textframework/model/numberformatter';
import SlideFormatManager from '@/io.ox/office/presentation/model/slideformatmanager';
import SlideVisibilityManager from '@/io.ox/office/presentation/model/slidevisibilitymanager';

// class PresentationModel ====================================================

/**
 * Represents the document model of a presentation application.
 *
 * Triggers the events supported by the base class EditModel, and the
 * Triggers the events supported by the base class Editor, and the
 * following additional events:
 * - 'inserted:slide': A new slide was inserted.
 * - 'removed:slide': A slide was removed.
 * - 'removed:activeslide': The active slide was removed.
 * - 'moved:slide': A slide was moved.
 * - 'change:activeslide:done': The active slide has changed. Parameter is the ID of the activated slide.
 * - 'change:activeView:after': The active view was modified, triggering after handler.
 * - 'change:activeView': The active view was modified.
 * - 'change:slide:before': The active slide was modified, triggering before handler.
 * - 'change:slide': The active slide was modified.
 * - 'change:slidelayout': A layout or master slide was modified.
 * - 'slideModel:init': All slide related model values are ready to use (e.g. for initializing the slide pane).
 * - 'slideModel:init:thumbnail': This event is triggered after the toolbar is activated so that thumbnail
 *                                generation can start.
 * - 'slidestate:formatted': At least one slide is completely formatted.  Parameter is an array of all slide IDs,
 *                          that are formatted.
 * - 'masterfield:update': In ODF format it is necessary to inform the slide pane about changes in the fields
 *                          on the master slide. This must be done, because these fields exist often only on
 *                          the master slide and must be evaluated specific to the document slide.
 * - 'slidestate:update': The content of at least one slide was modified so that any preview items within
 *                          the slide pane need to be updated. Parameter is an array of all slide IDs, that
 *                          require an update in the slide pane.
 *                          Furthermore an object, that contains as keys all those slide IDs, for that the
 *                          slide attribute 'followMasterShapes' was modified. The value for each slide ID
 *                          is the Boolean, whether 'followMasterShapes' was set to true or false. Additional
 *                          parameter is an options object, that contains the property 'onlyDocumentSlides'.
 *                          If set to true, no master or layout slides were modified.
 *
 * @mixes ListHandlerMixin
 * @mixes ModelAttributesMixin
 * @mixes SlideAttributesMixin
 * @mixes PageOperationMixin
 * @mixes ObjectOperationMixin
 * @mixes PageHandlerMixin
 * @mixes UpdateDocumentMixin
 * @mixes DynFontSizeMixin
 * @mixes UpdateListsMixin
 *
 * @param {PresentationApplication} app
 *  The application containing this document model.
 */
class PresentationModel extends Editor {

    constructor(app) {

        // base constructor
        super(app, {
            updateSelectionInUndo: true, // undo manager puts selection state into actions
            slideMode: true,
            spellingSupported: true,
            commentLayerClass: CommentLayer
        });

        var // self reference for local functions
            self = this,
            // the root element for the document contents
            pagediv = null,
            // the logical selection, synchronizes with browser DOM selection
            selection = null,
            // a counter for the 'standard' slides, used to generate unique keys for the slides (only used locally)
            mainSlideCounter = 1,
            // the model for all 'standard' slides. Those slides, that are not master or layout slides.
            allStandardSlides = {},
            // the model for the layout slides, an object with target string as key and target node as value
            allLayoutSlides = {},
            // the model for the master slides, an object with target string as key and target node as value
            allMasterSlides = {},
            // the model for standard/layout slides -> the key is the id of the standard slide, the value the key of the layout slide
            layoutConnection = {},
            // the model for layout/master slides -> the key is the id of the layout slide, the value the key of the master slide
            layoutMasterConnection = {},
            // a collector that combines the id of a slide with the type as key value pair
            idTypeConnection = {},
            // container with the slide IDs in the correct order (model)
            slideOrder = [],
            // container with the slide IDs of master and layout slides in the correct order (model)
            masterSlideOrder = [],
            // the id of the currently active slide (set in setActiveSlideId)
            activeSlideId = '',
            // keep ref for target (indirect for theme) of active slide, to better check for changes. is only changed when theme is changed!
            activeTargetChain = [],
            // the id of the active slide that was used before switching to master/layout view
            lastActiveStandardSlideId = '',
            // whether the master and layout slides are visible (true) or the 'standard' slides are visible (model) (set in setActiveView)
            isMasterView = false,
            // whether the active slide is currently formatted
            isUnformattedActiveSlide = false,
            // a collector for the slide IDs that need to be updated in the slide pane
            allModifiedSlideIDs = [],
            // the layer node for all layout slide nodes
            layoutSlideLayerNode = null,
            // the layer node for all master slide nodes
            masterSlideLayerNode = null,
            // a selection range in a master or layout slide
            masterLayoutSelection = null,
            // a collector for all insertTheme operations (needed for undo of slides)
            insertThemeOperations = {},
            // the currently used slidePaneWidth by the user in the browser, it's a temp value independent from the operation.
            localViewSlidePaneWidth = null,
            // debounced function for updating a slide and its content completely. It will be set after successful document load.
            implSlideChanged = $.noop,
            // the handler function that can be used to inform the slide pane about changes (that are not handled in 'operations:after')
            debouncedUpdateSlideStateHandler = $.noop,
            // the selection box object
            selectionBox = null,
            // the paragraph node that contains the selection
            selectedParagraph = $(),
            // a helper to keep track of already formatted slides (performance)
            slideFormatManager = null,
            // a helper to keep track of slides that do not need to be inserted into the DOM (performance)
            slideVisibilityManager = null,
            // a helper to handle the presentation mode
            presentationModeManager = null,
            // collector for modified template text in place holders without text content
            templateTextCollector = null,
            // whether the master slide was modified (ODP only)
            masterModified = false,
            // a node that is used block the view during long running slide formatting
            blockerNode = null,
            // a drawing and optionally its group saved in touchstart handler for later activation of edit mode
            touchStartDrawing = null, touchStartDrawingGroup = null,
            // the 'touchstart' event saved in touchstart handler for later activation of edit mode
            touchStartEvent = null,
            // whether the 'touchstart' event got a preventDefault in 'touchstart' handler (only for IOS)
            touchStartPreventDefault = false,
            // collection of all ids used for drawings in a document
            drawingIdCollection = {},
            // collection of all user IDs used for drawings in a document
            drawingUserIdCollection = new Set/*<string>*/(),
            // a user ID that is used to generated drawings IDs
            userDrawingId = null,
            // a drawing counter that is used to generate unique drawing IDs
            userDrawingCounter = 0;

        // mixins -------------------------------------------------------------

        SlideOperationMixin.call(this, app);
        ObjectOperationMixin.call(this, app);
        CommentOperationMixin.call(this, app);
        ListHandlerMixin.call(this);
        ModelAttributesMixin.call(this);
        SlideAttributesMixin.call(this);
        PageHandlerMixin.call(this, app);
        UpdateDocumentMixin.call(this, app);
        DynFontSizeMixin.call(this, app);
        UpdateListsMixin.call(this, app);
        DragToCopyMixin.call(this, app);
        OdfLayoutMixin.call(this, app);

        // private methods ----------------------------------------------------

        /**
         * Deleting the model of the presentation app completely. This is necessary, if a new
         * snapshot is applied.
         */
        function emptyPresentationModel() {

            allStandardSlides = {};
            allLayoutSlides = {};
            allMasterSlides = {};
            layoutConnection = {};
            layoutMasterConnection = {};
            idTypeConnection = {};
            slideOrder = [];
            masterSlideOrder = [];

            mainSlideCounter = 1;
            activeSlideId = '';
            isMasterView = false;

            // emptying the drawing place holder container
            self.drawingStyles.emptyPlaceHolderModel();

            // emptying the slide attributes model
            self.emptySlideAttributesModel();
        }

        /**
         * Registering one layout or master slide in the slide model.
         *
         * @param {jQuery} slide
         *      The jQuerified slide that will be saved inside the slide collector. The key
         *      inside this collector is the id, the value the jQuerified slide node.
         *
         * @param {String} id
         *      The id of the slide. For master and layout slides, this id is sent from the
         *      server. For 'standard' slides a locally generated id is used.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.type='']
         *      The type of the slide. This is required to distinguish master slides, layout
         *      slide and 'standard' slides. The latter are the default.
         *  @param {String} [options.target='']
         *      The target of the slide that will be registered. This is especially important
         *      for the connection from layout slides to master slides.
         *  @param {Number} [options.index=-1]
         *      The zero-based index describing the position of the slide inside its view. This
         *      can be the container for the standard slides or the container for the master
         *      layout slides.
         *  @param {Object} [options.attrs=null]
         *      The slide attributes.
         *  @param {Object} [options.documentReloaded=false]
         *      Whether the document was reloaded with a snapshot.
         *
         * @returns {Number}
         *  The number of slides in the standard or master/layout model.
         */
        function addIntoSlideModel(slide, id, options) {

            var // the type of the slide
                type = getStringOption(options, 'type', ''),
                // the target of the slide
                target = null,
                // the order index at which the slide shall be inserted
                index = getNumberOption(options, 'index', -1),
                // whether the slide is a standard slide
                isStandardSlide = false,
                // whether the model is modified
                insertedSlide = false,
                // an optional attribute object for the new slide
                attrs = getObjectOption(options, 'attrs', null),
                // the order index at which the slide shall be inserted
                documentReloaded = getBooleanOption(options, 'documentReloaded', false);

            if (type === PresentationModel.TYPE_SLIDE) {

                isStandardSlide = true;
                insertedSlide = true;

                // adding the slide into the model
                allStandardSlides[id] = slide;

                if (index > -1) {
                    slideOrder.splice(index, 0, id);
                } else {
                    slideOrder.push(id); // if index not specified, append slide to the end
                }

                // reading the required options
                target = getStringOption(options, 'target', '');

                // the target must exist, if it is specified. Otherwise the document is broken, but increasing resilience by using the last available layout slide (DOCS-1778)
                if (app.isODF() && target && !self.isLayoutOrMasterId(target)) { target = masterSlideOrder[masterSlideOrder.length - 1]; }

                // saving connection between standard slide and its layout slide
                if (target) { layoutConnection[id] = target; }

                // registering the slide, so that it is not immediately detached from the DOM by the slide visibility manager (48796)
                self.getSlideVisibilityManager().registerOperationSlideId(id);

            } else if (type === PresentationModel.TYPE_MASTER_SLIDE) {

                insertedSlide = true;

                // saving the jQuerified slide in the model
                allMasterSlides[id] = slide;

                // adding the ID of the master slide into the sorted container
                if (index > -1) {
                    masterSlideOrder.splice(index, 0, id);
                } else {
                    masterSlideOrder.push(id); // if index not specified, append slide to the end
                }

            } else if (type === PresentationModel.TYPE_LAYOUT_SLIDE) {

                insertedSlide = true;

                // saving the jQuerified slide in the model
                allLayoutSlides[id] = slide;

                // reading the required options
                target = getStringOption(options, 'target', '');

                // saving connection between layout slide and its master slide
                if (target) { layoutMasterConnection[id] = target; }

                // adding the ID of the layout slide into the sorted container
                if (index > -1) {
                    masterSlideOrder.splice(index, 0, id);
                } else {
                    masterSlideOrder.push(id); // if index not specified, append slide to the end
                }

            } else {
                globalLogger.log('Warning: Unknown slide type: ' + type);
            }

            // registering the type of the slide for every id
            idTypeConnection[id] = type;

            // saving the slide attributes in slide model
            if (attrs) { self.saveSlideAttributes(id, attrs); }

            // saving the target also at the slide (required for generating undo operation and for snapshots)
            if (target) { slide.data('target', target); }  // -> this needs to be updated, if the model changes

            // informing the listeners, that a new slide was inserted (model was updated)
            if (insertedSlide) { self.trigger('inserted:slide', { id, isMasterView, isMasterOrLayoutSlide: !isStandardSlide, documentReloaded, hasScrollEffect: true }); }

            return isStandardSlide ? slideOrder.length : masterSlideOrder.length;
        }

        /**
         * Removing a slide specified by its ID from the model.
         * Before calling this function it must be checked, if the specified slide is the
         * only slide in its view (self.isOnlySlideInView(id)) or if it is a used
         * master or layout slide (self.isUsedMasterOrLayoutSlide(id)).
         *
         * @param {String} id
         *      The id of the slide. For master and layout slides, this id is sent from the
         *      server. For 'standard' slides a locally generated id is used.
         *
         * @returns {Number}
         *  The number of slides in the standard or master/layout model.
         */
        function removeFromSlideModel(id) {

            // Required checks:
            // self.isUsedMasterOrLayoutSlide(id) -> not removing master/layout slides that are referenced
            // self.isOnlySlideInView(id)         -> not removing the last slide in its view

            var // whether the slide is a standard slide
                isStandardSlide = false,
                // whether the model is modified
                removedSlide = false;

            // if this was the active slide, it cannot be active anymore after removal
            if (isValidSlideId(id) && (id === activeSlideId)) {
                activeSlideId = '';
                // handling visibility of master and layout slide
                // -> this needs to be done, before the model is updated
                self.trigger('removed:activeslide', self.getSlideIdSet(id));
            }

            if (self.isStandardSlideId(id)) {

                isStandardSlide = true;
                removedSlide = true;

                // removing the slide from the model
                delete allStandardSlides[id];

                // removing the connection between standard slide and its layout slide
                delete layoutConnection[id];

                // removing the id from the slide order array
                slideOrder = _.without(slideOrder, id);

            } else if (self.isLayoutSlideId(id)) {

                removedSlide = true;

                // removing the slide from the model
                delete allLayoutSlides[id];

                // removing the connection between layout slide and its masterlayout slide
                delete layoutMasterConnection[id];

                // removing the id from the master slide order array
                masterSlideOrder = _.without(masterSlideOrder, id);

            } else if (self.isMasterSlideId(id)) {

                removedSlide = true;

                // removing the slide from the model
                delete allMasterSlides[id];

                // removing the id from the master slide order array
                masterSlideOrder = _.without(masterSlideOrder, id);
            }

            // removing registration of type for the slide
            delete idTypeConnection[id];

            // deleting the attributes in the attributes model
            self.deleteSlideAttributes(id);

            // informing the listeners, that a slide was removed (model was updated)
            if (removedSlide) { self.trigger('removed:slide', { id, isMasterView, isMasterOrLayoutSlide: !isStandardSlide, hasScrollEffect: true }); }

            return isStandardSlide ? slideOrder.length : masterSlideOrder.length;
        }

        /**
         * Changing the ID of a layout slide assigned to a document slide.
         *
         * @param {String} id
         *  The ID of the document slide that will get a new layout slide ID assigned.
         *
         * @param {String} layoutId
         *  The ID of the layout slide.
         *
         * @returns {Boolean}
         *  Whether the new layout ID was assigned to the specified document slide ID.
         */
        function changeLayoutIdInModel(id, layoutId) {

            var // whether the new layout ID was assigned to the document id
                changed = false;

            // update the model
            if (id && layoutId && _.isString(layoutConnection[id]) && self.isLayoutSlideId(layoutId)) {
                // changing connection between document slide and its layout slide
                layoutConnection[id] = layoutId;

                // saving the target also at the slide (required for generating undo operation and for snapshots)
                self.getSlideById(id).data('target', layoutId);

                // Trigger event about change of model (?)

                changed = true;
            }

            return changed;
        }

        /**
         * Changing the ID of a master slide assigned to a layout slide.
         *
         * @param {String} id
         *  The ID of the layout slide that will get a new master slide ID assigned.
         *
         * @param {String} masterId
         *  The ID of the master slide.
         *
         * @returns {Boolean}
         *  Whether the new master ID was assigned to the specified layout slide ID.
         */
        function changeMasterIdInModel(id, masterId) {

            var // whether the new master ID was assigned to the layout id
                changed = false;

            // update the model
            if (id && masterId && _.isString(layoutMasterConnection[id]) && self.isMasterSlideId(masterId)) {
                // changing connection between document slide and its layout slide
                layoutMasterConnection[id] = masterId;

                // saving the target also at the slide (required for generating undo operation and for snapshots)
                self.getSlideById(id).data('target', masterId);

                // Trigger event about change of model (?)

                changed = true;
            }

            return changed;
        }

        /**
         * Moving the ID in the sorted slide container from index 'from' to index 'to'.
         *
         * @param {Number} from
         *  The current index of the moved slide ID.
         *
         * @param {Number} to
         *  The new index of the moved slide ID.
         *
         * @param {Boolean} isStandardSlide
         *  Whether the moved slide is in the standard or the master/layout container.
         *
         * @returns {String|Null}
         *  The ID of the moved slide. Or null, if no slide was moved.
         */
        function moveSlideIdInModel(from, to, isStandardSlide) {

            var // the ID of the moved slide
                id = null,
                // the affected container
                container = isStandardSlide ? slideOrder : masterSlideOrder,
                // the length of the container
                max = container.length;

            if (_.isNumber(from) && _.isNumber(to) && to !== from && from < max && to < max) {

                id = container[from];

                // 1. step remove slide ID
                // 2. step insert slide ID
                container.splice(from, 1); // delete
                container.splice(to, 0, id); // insert

                // informing the listeners, that the slide order was changed (model was updated)
                self.trigger('moved:slide', { id, to, from, isMasterOrLayoutSlide: !isStandardSlide, hasScrollEffect: true });
            }

            return id;
        }

        /**
         * Entering a busy view during formatting of the active slide.
         * This is done by appending a new div element to the application pane and call the
         * busy function on it.
         */
        function enterBusyView() {
            if (!blockerNode) {
                self.setBlockKeyboardEvent(true);
                blockerNode = $('<div>').css({ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' });
                self.getNode().append(blockerNode);
                blockerNode.busy();
            }
        }

        /**
         * Leaving the busy mode for formatting of the active slide, if it is no longer
         * required.
         */
        function leaveBusyView() {
            if (blockerNode) {
                blockerNode.remove();
                blockerNode = null;
                self.setBlockKeyboardEvent(false);
            }
        }

        /**
         * Setting the id of the active slide (model).
         *
         * @param {String} id
         *  The zero-based index of the active slide inside the standard or
         *  master/layout view.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.forceUpdate=false]
         *   Whether a view update is required, even if the index of the slide did not change.
         *  @param {Number} [options.delayedVisibility=0]
         *   Whether the 'new' slide shall be made visible with a delay.
         *   This is sometimes required to avoid a flickering (DOCS-3084).
         *
         * @returns {Boolean}
         *  Whether another slide was activated.
         */
        function setActiveSlideId(id, options) {

            var // whether the slide was modified
                modified = activeSlideId !== id,
                // whether an update is forced
                forceUpdate = getBooleanOption(options, 'forceUpdate', false),
                // whether another view was activated
                activated  = false,
                // the id of the currently active slide
                oldId = activeSlideId,
                // the id set, that shall be activated
                idSet = null,
                // the id set, that shall be deactivated
                oldIdSet = null,
                // collector for all modified slides
                viewCollector = {};

            if (isValidSlideId(id) || (id === '')) {

                // setting all new slide IDs in the set
                idSet = self.getSlideIdSet(id); // an object with keys 'id', 'layoutId' and 'masterId'

                // check, if the new slide (and its dependent slides) are already formatted
                if (!slideFormatManager.allSlidesFormatted() && !slideFormatManager.isSlideSetFormatted(idSet)) {
                    // setting marker that the slide needs to be made visible later
                    isUnformattedActiveSlide = true;
                    // shifting all three slides 10000px to the left, so that user cannot see unformatted slides
                    slideFormatManager.shiftSlideSetOutOfView(idSet);
                    // is the slide formatting already in progress?
                    if (!slideFormatManager.isSlideSetCurrentlyFormatted(idSet)) { slideFormatManager.forceSlideFormatting(idSet); }
                    // blocking the view
                    enterBusyView();
                } else {
                    // leaving the busy mode, if it is no longer required
                    leaveBusyView();
                    // setting marker that the slide is already formatted completely
                    isUnformattedActiveSlide = false;
                    // checking, if some shifted slides can be shifted backward again
                    if (slideFormatManager.shiftedSlideExist()) { slideFormatManager.shiftSlideSetIntoView(); }
                }

                // modifying the model
                activeSlideId = id;

                // appending the slide into the DOM, if necessary
                self.appendSlideToDom(activeSlideId);

                if (modified || forceUpdate) {
                    // setting all IDs that need to be activated
                    if (idSet.id) { viewCollector.newSlideId = idSet.id; }
                    if (idSet.layoutId) { viewCollector.newLayoutId = idSet.layoutId; }
                    if (idSet.masterId) { viewCollector.newMasterId = idSet.masterId; }

                    oldIdSet = self.getSlideIdSet(oldId);

                    // setting all IDs that need to be deactivated
                    if (oldIdSet.id) { viewCollector.oldSlideId = oldIdSet.id; }
                    if (oldIdSet.layoutId) { viewCollector.oldLayoutId = oldIdSet.layoutId; }
                    if (oldIdSet.masterId) { viewCollector.oldMasterId = oldIdSet.masterId; }

                    // add the currently active slide to the viewCollector
                    viewCollector.activeSlideId = activeSlideId;
                    if (getNumberOption(options, 'delayedVisibility', 0)) { viewCollector.delayedVisibility = options.delayedVisibility; }

                    // informing the listeners, that the slide need to be updated (model was updated)
                    self.trigger('change:slide:before', viewCollector);

                    // informing the listeners, that the slide need to be updated (model was updated)
                    self.trigger('change:slide', viewCollector);

                    // informing the listeners, that the scroll bar needs to be updated
                    self.trigger('update:verticalscrollbar', { pos: getActiveSlideIndex() });

                    // clearing all drawing selections (including removing of all handlers at drawing)
                    self.getSelection().clearAllDrawingSelections();

                    // updating the value for the active target (TODO)
                    self.setActiveTarget(self.isLayoutOrMasterId(activeSlideId) ? activeSlideId : '');
                    self.getSelection().setNewRootNode(self.getRootNode(activeSlideId));

                    if (!self.isPresentationMode()) {
                        if (id === '') {
                            self.getSelection().setEmptySelection();
                        } else {
                            self.getSelection().setSlideSelection(); // selecting the slide
                        }
                    }

                    // a new slide was activated
                    activated = true;

                    activeTargetChain = self.getTargetChain(activeSlideId);

                    // informing listeners about the modified target chain (for example the theme might have changed)
                    self.trigger('update:targetchain', activeTargetChain);

                    // informing the listeners, that the slide change is done (inclusive formatting)
                    if (!isUnformattedActiveSlide) {
                        self.trigger('change:activeslide:done', activeSlideId);
                    }
                }
            }

            return activated;
        }

        /**
         * Setting the active view (model) and triggering the events 'change:activeView:after' and
         * 'change:activeView' event to notify observers.
         * The active view can be the view of the normal/standard slides or the view of the master/layout slides.
         *
         * @param {Boolean} isMaster
         *  Whether the view of the master/layout slides shall be activated.
         *
         * @returns {Boolean}
         *  Whether another slide view was activated.
         */
        function setActiveView(isMaster) {

            var // whether the slide view was modified
                modified = (isMaster !== isMasterView),
                // the number of slides in the new active view
                slideCount = 0;

            // updating the model
            if (modified) {

                // changing the model
                isMasterView = isMaster;

                slideCount = self.getActiveViewCount();

                // informing the listeners, that the activeView is changed (model was updated)
                self.trigger('change:activeView', { isMasterView, slideCount });

                // informing the listeners, that the activeView is changed (model was updated)
                self.trigger('change:activeView:after', { isMasterView, slideCount });

                // ODP: Informing the slide pane, that the fields have to be updated, if the user switches from master
                // view to document view. It is possible that the footer placeholders have been changed (for example
                // new text into the place holder drawings, but not the fields), so that the field values are no longer
                // valid. The function createMasterSlideFieldsEvent triggers therefore the event 'masterfield:update'.
                if (app.isODF() && !isMasterView && masterModified) {
                    self.createMasterSlideFieldsEvent();
                    masterModified = false;
                }
            }

            return modified;
        }

        /**
         * Provides a unique ID for the standard slides. This ID is used locally only. The master
         * and layout slides get their IDs from the server. The local ID for the standard slides
         * is used for the model only.
         *
         * @returns {String}
         *  A unique id.
         */
        function getNextSlideID() {
            return 'slide_' + mainSlideCounter++;
        }

        /**
         * Provides a unique ID for the custom layout slides.
         *
         * In the case of an OT collision caused by using the same ID, a reload is required.
         * Generating unique IDs is not possible, because the value is internally evaluated
         * in pptx format.
         *
         * @returns {String}
         *  A unique id.
         */
        function getNextCustomLayoutId() {
            var highestId = _.first(_.clone(masterSlideOrder).sort(function (a, b) { return b - a; }));
            return String(parseInt(highestId, 10) + 1);
        }

        /**
         * Getting the index of the active slide in the current container
         *
         * @returns {Number}
         *  The index of the active slide in the current container. If the
         *  active slide id could not be found in the array, '-1' is returned.
         */
        function getActiveSlideIndex() {

            var // the container that contains the order of the slides
                orderContainer = isMasterView ? masterSlideOrder : slideOrder;

            return _.indexOf(orderContainer, activeSlideId);
        }

        /**
         * Helper function to generate the debounced function for updating a slide
         * and all its (place holder) drawings completely. The generated function
         * should be called carefully, because of performance reasons. This function
         * needs to be created after successful loading.
         * It is necessary to update all drawings (not only place holders) because
         * the theme of the slide might have changed, so that all drawings need to
         * be reformatted.
         *
         * @returns {Function}
         *  The debounced function for updating a slide completely.
         */
        function createDebouncedUpdateSlideHandler() {

            // all slide nodes that need to be updated
            var slides = $();

            // direct callback: called every time when implSlideChanged() has been called
            function registerSlide(slide) {
                // store the new slide in the collection (jQuery keeps the collection unique)
                slides = slides.add(slide);
            }

            // deferred callback: called once, after current script ends
            function updateSlides() {

                _.each(slides, function (slide) {

                    // updating the slide, so that background drawings are handled correctly
                    self.slideStyles.updateElementFormatting(slide);

                    // updating all place holder drawings in the slide
                    self.drawingStyles.updateAllDrawingsOnSlide(slide);
                });

                slides = $(); // reset collector
            }

            // create and return the deferred implTableChanged() method
            return self.debounce(registerSlide, updateSlides);
        }

        /**
         * Helper function to keep the global 'mainSlideCounter' up-to-date. After loading the
         * document with fast load or from local storage, this number needs to be updated.
         * Then a new slide can be inserted from the client with a valid and unique id.
         *
         * @param {String} slideId
         *  A used ID of a slide.
         */
        function updateSlideIdCounter(slideId) {

            var // resulting array of the regular expression
                matches = /(\d+)$/.exec(slideId),
                // the number value at the end of the id
                number = 0;

            if (_.isArray(matches)) {
                number = parseInt(matches[1], 10);

                if (number >= mainSlideCounter) {
                    mainSlideCounter = number + 1;
                }
            }
        }

        /**
         * Adding all slides below a specified node into the presentation slide model.
         *
         * @param {jQuery} node
         *  The jQuerified container node for the slides.
         *
         * @param {String} selector
         *  The css selector to find the slides.
         *
         * @param {String} type
         *  The slide type.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.documentReloaded=false]
         *   Whether the document was reloaded with a new snapshot.
         */
        function addAllSlidesOfLayerIntoSlideModel(node, selector, type, options) {

            var // whether the document was reloaded with a snapshot
                documentReloaded = getBooleanOption(options, 'documentReloaded', false);

            _.each(node.find(selector), function (slideNode) {

                var // the jQuerified slide node
                    slide = $(slideNode),
                    // the slide id
                    slideId = slide.attr('data-container-id'),
                    // the target of the slide (set as attribute during fast load)
                    target = slide.attr('target'),
                    // the slide index
                    index = -1,
                    // the slide attributes (reading from data object)
                    slideAttrs = slide.data('attributes'),
                    // the supported attributes at a slide node
                    supportedAttrs = null;

                // Info: When loading with fast load or local storage, the list style attributes defined at
                //       slides, are assigned to the slide.data('attributes') object. This will not be the
                //       case, if the document is loaded without fast load, because 'listStyles' is not a
                //       supported family for the slide attributes.
                if (!documentReloaded && slideAttrs && slideAttrs.listStyles) {
                    supportedAttrs = _.copy(slideAttrs, true);
                    delete supportedAttrs.listStyles;
                    if (_.isEmpty(supportedAttrs)) {
                        slide.removeData('attributes');
                    } else {
                        slide.data('attributes', supportedAttrs);
                    }
                }

                if (!target) { target = slide.data('target'); } // reload or local storage

                if (type === self.getStandardSlideType()) {
                    updateSlideIdCounter(slideId);
                } else if (type === self.getLayoutSlideType()) {
                    index = self.getLastPositionInModelBehindMaster(target);
                }

                slide.removeAttr('target'); // removing fast load specific attribute that is not updated by the model

                addIntoSlideModel(slide, slideId, { type, target, attrs: slideAttrs, index, documentReloaded });
            });
        }

        function updateSlideContainer(node) {
            _.each(node.find('>' + DOM.SLIDECONTAINER_SELECTOR), function (slideContainer) {
                $(slideContainer).attr('tabindex', '0');
            });
        }

        /**
         * The handler function for changed attribute defaults. Within the object
         * changedAttributes the default text list styles are defined, that will be
         * used in non-placeholder drawings, like text frames or shapes.
         *
         * @param {Object} changedSet
         *  An object with the changed attributes.
         */
        function registerDefaultTextListStyles(changedSet) {

            if (changedSet.defaultTextListStyles) {
                self.setDefaultTextListStyles(changedSet.defaultTextListStyles);
            }
        }

        /**
         * Receiving the layer node for the layout slides. If it does not exist, it is created.
         *
         * @returns {jQuery}
         *  The layout slide layer node
         */
        function getOrCreateLayoutSlideLayerNode() {

            // is there already a layout slide layer node
            if (layoutSlideLayerNode) { return layoutSlideLayerNode; }

            // creating the new node
            layoutSlideLayerNode = $('<div>').addClass(DOM.LAYOUTSLIDELAYER_CLASS);

            // Setting contenteditable 'false' to pagecontent node in IE (46752)
            DOM.handleReadOnly(layoutSlideLayerNode);

            // inserting the new node before the page content node
            layoutSlideLayerNode.insertBefore(DOM.getPageContentNode(pagediv));

            return layoutSlideLayerNode;
        }

        /**
         * Receiving the layer node for the master slides. If it does not exist, it is created.
         *
         * @returns {jQuery}
         *  The master slide layer node
         */
        function getOrCreateMasterSlideLayerNode() {

            // is there already a layout slide layer node
            if (masterSlideLayerNode) { return masterSlideLayerNode; }

            masterSlideLayerNode = $('<div>').addClass(DOM.MASTERSLIDELAYER_CLASS);

            // Setting contenteditable 'false' to pagecontent node in IE (46752)
            DOM.handleReadOnly(masterSlideLayerNode);

            // inserting the new node before the page content node
            masterSlideLayerNode.insertBefore(DOM.getPageContentNode(pagediv));

            return masterSlideLayerNode;
        }

        /**
         * Provides the standard slide node corresponding to the specified id string.
         *
         * @param {String} id
         *  The slide id.
         *
         * @returns {jQuery|null}
         *  The standard slide node. Or null, if no standard slide node with the specified id exists.
         */
        function getStandardSlide(id) {
            return allStandardSlides[id] || null;
        }

        /**
         * Provides the layout slide node in the layout slide layer corresponding to the specified
         * id string.
         *
         * @param {String} id
         *  The slide id.
         *
         * @returns {jQuery|null}
         *  The slide node inside the layout layer node. Or null, if no layout slide node with the
         *  specified id exists.
         */
        function getLayoutSlide(id) {
            return allLayoutSlides[id] || null;
        }

        /**
         * Provides the master slide node in the master slide layer corresponding to the specified
         * id string.
         *
         * @param {String} id
         *  The id string.
         *
         * @returns {jQuery|null}
         *  The slide node inside the master layer node. Or null, if no master slide node with the
         *  specified id exists.
         */
        function getMasterSlide(id) {
            return allMasterSlides[id] || null;
        }

        /**
         * Getting the last top level position inside the slide, that has the current
         * selection.
         * Info: this function is DEPRECATED. Use getNextAvailablePositionInSlide().
         *
         * @returns {Array<Number>}
         *  The next available position at the root of the slide.
         */
        function getNextAvailablePositionInActiveSlide() {

            var // the active slide
                slide = self.getSlideById(activeSlideId),
                // the position of the active slide
                pos = self.getActiveSlidePosition();

            pos.push(slide.children(NODE_SELECTOR).length); // only drawings are relevant children

            return pos;
        }

        /**
         * Getting the last top level position inside any slide (master, layout or standard). If id is passed,
         * gets the position in slide with that id, otherwise in active slide.
         * Behaves as more general version of getNextAvailablePositionInActiveSlide() function.
         *
         * @param {String} [id]
         *  The slide id. If not specified, the active slide is used.
         *
         * @returns {Array<Number>}
         *  The next available position at the root of the slide.
         */
        function getNextAvailablePositionInSlide(id) {

            var // the slide ID
                localId = id || activeSlideId,
                // the slide node
                slide = self.getSlideById(id),
                // the logical position
                pos = self.isLayoutOrMasterId(localId) ? [0] : [_.indexOf(slideOrder, localId)];

            pos.push(slide.children(NODE_SELECTOR).length); // only drawings are relevant children

            return pos;
        }

        /**
         * Provides the content node of the layout slide node or master slide node in the layout slide
         * layer or master slide layer corresponding to the specified target string. This node can be
         * used for calculations of logical positions (not the slide node itself).
         *
         * @param {String} id
         *  The slide id.
         *
         * @returns {jQuery|null}
         *  The slide content node inside the layout or master slide layer node. This content node is
         *  the parent of the slide node. Or null, if no slide with the specified target exists.
         */
        function getLayoutOrMasterSlideContentNode(id) {

            var // the slide node, that is the child of the content node
                slide = self.getLayoutOrMasterSlide(id);

            return slide ? slide.parent() : null;
        }

        /**
         * Checking, whether a specified id is a valid and registered slide id.
         *
         * @param {String} id
         *  The slide id to be checked.
         *
         * @returns {Boolean}
         *  Whether the specified id is a valid value for a slide id.
         */
        function isValidSlideId(id) {
            return _.isString(idTypeConnection[id]);
        }

        /**
         * Checking, whether the specified operation deletes a slide.
         *
         * @param {Object} operation
         *  The operation object to be checked.
         *
         * @returns {Boolean}
         *  Whether the specified operation deletes a slide.
         */
        function isDeleteSlideOperation(operation) {
            return operation.name === Op.DELETE && operation.start.length === 1;
        }

        /**
         * Checking, whether the specified operation inserts, moves or deletes a slide, so
         * that the position of the (document) slides is changed.
         *
         * @param {Object} operation
         *  The operation object to be checked.
         *
         * @returns {Boolean}
         *  Whether the specified operation inserts, moves or deletes a slide.
         */
        function isSlidePositionOperation(operation) {
            return operation.start && operation.start.length === 1 && _.contains(PresentationModel.SLIDE_POSITION_OPERATIONS, operation.name);
        }

        /**
         * Forcing the update of the slides, that have a dependency to a specified target. The
         * target must be defined inside the data object. The event 'change:slidelayout' is
         * triggered to inform all listeners.
         *
         * @param {Object} data
         *  This object is generated inside the function 'savePlaceHolderAttributes' in the
         *  drawing styles. The object's keys are: 'target', 'placeHolderType', 'placeHolderIndex'
         *  and 'attrs'.
         */
        function triggerLayoutChangeEvent(data) {
            self.trigger('change:slidelayout', data);
        }

        /**
         * Check, if the specified paragraph is the default content of a place holder drawing
         * with the specified drawing type.
         * Supported types: ['dt', 'ftr', 'sldNum'];
         *
         * @param {String} drawingType
         *  The drawing type of the place holder drawing.
         *
         * @param {jQuery} paragraph
         *  The jQuerified paragraph of a drawing place holder node.
         *
         * @returns {Boolean}
         *  Whether the specified paragraph is the default content of the drawing place holder node.
         */
        function isDefaultFooterContent(drawingType, paragraph) {

            var // whether the place holder node contains its default content
                isDefaultContent = true;

            // whether this is a paragraph without neighbour
            function isSingleParagraph(para) {
                return para.parent().children().length === 1;
            }

            // whether this is a paragraph that contains only a field
            function isSingleFieldParagraph(para) {
                var allChildren = para.children();
                return (allChildren.length === 3 && DOM.isEmptySpan(allChildren[0]) && DOM.isFieldNode(allChildren[1]) && DOM.isEmptySpan(allChildren[2]));
            }

            if (drawingType === 'dt' || drawingType === 'sldNum') {
                // single paragraph with empty text span and field
                isDefaultContent = isSingleParagraph(paragraph) && isSingleFieldParagraph(paragraph);
            } else if (drawingType === 'ftr') {
                // single empty paragraph (empty text span)
                isDefaultContent = isSingleParagraph(paragraph) && DOM.isEmptyParagraph(paragraph);
            }

            return isDefaultContent;
        }

        /**
         * This function is called, if a node received new attributes. If this node is inside
         * a layout or master slide, it is necessary to update the collector for the attributes
         * of the drawings that are located inside a layout or master slide (the model is the object
         * 'placeHolderCollector'.
         *
         * Because this function is triggered after (!) the drawing received the new attributes,
         * these new attributes are already assigned to the drawing and saved in the data object
         * at the drawing node.
         *
         * Additionally all slides, that have the affected layout or master slide as target,
         * need to be repainted (or invalidated?). Therefore this function itself triggers the
         * event 'change:slidelayout'.
         *
         * @param {HTMLElement|jQuery} element
         *  The element node, that received the new attributes.
         *
         * @param {String} target
         *  The active target for the specified element.
         *
         * @param {String} family
         *  The family of the attribute set.
         *
         * @param {Object} attrs
         *  A map with formatting attribute values, mapped by the attribute
         *  names, and by attribute family names.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.immediateUpdate=true]
         *      Whether slides that are dependent from the layout or master slide change
         *      shall be updated immediately. Default is the immediate update.
         *  @param {String} [options.saveOldAttrs=false]
         *      Whether the old attributes shall be saved and returned in the return object.
         *
         * @returns {Object|Null}
         *  If the attributes are registered, an object with the complete registration
         *  information is returned. The object's keys are: 'target', 'placeHolderType',
         *  'placeHolderIndex', 'attrs' and 'oldAttrs'. If the attributes were not registered,
         *  because target, type or index are not defined, null is returned.
         */
        function registerPlaceHolderAttributes(element, target, family, attrs, options) {

            var // the drawing node containing the specified element
                drawing = null,
                // a paragraph node
                paragraph = null,
                // the explicit attributes at the drawing
                drawingAttrs = null,
                // the type of the place holder drawing
                drawingType = null,
                // the paragraph level
                paraLevel = 0,
                // the key in the list styles corresponding to the paragraph level
                paraKey = null,
                // an object containing the information about the saved values
                registrationObject = null,
                // whether drawing attributes shall be registered
                isDrawing = (family === 'drawing'),
                // whether an update of dependent slide shall be triggered immediately
                immediateUpdate = getBooleanOption(options, 'immediateUpdate', true),
                // whether an update of dependent slide shall be triggered immediately
                saveOldAttrs = getBooleanOption(options, 'saveOldAttrs', false);

            if (target && self.isLayoutOrMasterId(target)) {

                drawing = isDrawing ? $(element) : $(element).closest('div.drawing');

                if (drawing.length > 0 && isPlaceHolderDrawing(drawing)) {

                    if (isDrawing) {
                        drawingAttrs = attrs;
                    } else {

                        drawingType = getPlaceHolderDrawingType(drawing);
                        paragraph = (family === 'paragraph') ? $(element) : $(element).closest(DOM.PARAGRAPH_NODE_SELECTOR);

                        // only registration of character attributes for footer place holder drawings that are not modified by the user
                        // -> but (at least) for drawing type 'ftr' there is an inheritance of character attributes
                        if (_.contains(PresentationModel.FOOTER_TYPES, drawingType) && drawingType !== 'ftr' && !isDefaultFooterContent(drawingType, paragraph)) { return null; }

                        paraLevel = self.paragraphStyles.getParagraphLevel(paragraph);
                        paraKey = self.generateListKeyFromLevel(paraLevel);

                        drawingAttrs = { listStyle: {} };
                        drawingAttrs.listStyle[paraKey] = attrs;
                    }

                    registrationObject = self.drawingStyles.savePlaceHolderAttributes(target, drawingAttrs, drawing, { saveOldAttrs });

                    if (immediateUpdate && registrationObject) {

                        // informing the listeners that place holder drawing styles were modified and that
                        // all affected drawings (that use the modified place holder type and index at a slide
                        // with the specified target) need to be updated.
                        // Therefore the registrationObject is 'complete', with type and index containing at
                        // least the default values.
                        triggerLayoutChangeEvent(registrationObject);
                    }
                }
            }

            return registrationObject;
        }

        /**
         * Getting the index for a master or a layout slide that can be used in operations to set the
         * position of a master or layout slide. For a layout slide this is a 0-based index, that describe
         * the position of a layout slide relative to its master slide.
         * For a master slide this is the 0-based index relative to all other master slides.
         * This index is different from the index in the global container 'masterSlideOrder' that contains
         * the sorted IDs of master and layout slides.
         *
         * @param {String} id
         *  The ID of the master or layout slide.
         *
         * @returns {Number}
         *  The 0-based index of the specified slide. If the index cannot be determined, -1 is returned.
         */
        function getOperationIndexForSlideById(id) {

            var // the index for a master or layout slide that describes its position in an operation
                index = -1;

            if (self.isMasterSlideId(id)) {

                index = self.getMasterSlideStartPosition(id);

            } else if (self.isLayoutSlideId(id)) {

                index = self.getLayoutSlideStartPosition(id);

            }

            return index;
        }

        /**
         * Function that is executed in the start handler of the selection box.
         *
         * @returns {Boolean}
         *  Whether the selection box can continue. If 'false' is returned, the selection box
         *  will be cancelled.
         */
        function selectionBoxStartHandler() {

            if (!isMasterView && self.isEmptySlideView()) {

                app.getView().setVirtualFocusSlidepane(false);

                // Clicking on content root node creates a new slide, no selection box required
                self.insertSlide();

                return false;
            } else {
                self.getSelection().selectionStartHandler();
            }

            return true;
        }

        /**
         * Handler function that is triggered for touch devices (all devices with deactivated
         * selection box) to handle 'touchstart' events on the slide or the slide background.
         *
         * @param {jQuery.Event} event
         *  The 'touchstart' event
         */
        function touchSlideBackgroundHandler(event) {
            var selection = null;
            if ($(event.target).is(self.getMultiSelectionBoxFilterSelector())) {
                selection = self.getSelection();

                if (selection.isAdditionalTextframeSelection()) {
                    // removing the marker for the edit mode
                    selection.getAnyDrawingSelection().removeClass('active-edit');
                }

                if (!selection.isSlideSelection()) {
                    selection.setSlideSelection(); // setting slide selection (but avoiding superfluous jumps to upper left corner by check of slide selection)
                }
            }
        }

        /**
         * Updating the template text of place holder drawings that cannot contain text content. This
         * are place holder drawings of type 'tbl', 'pic', ... . This place holder drawings do not even
         * contain a paragraph in the document slides. Therefore the template text in the drawing in the
         * layout slide must be cloned and added to the corresponding drawing on the document slide.
         *
         * @param {Object} collector
         *  An object that contains information about the place holder drawings whose template text was
         *  modified in the layout slide. This object was filled in the function
         *  'collectPlaceHolderTextUpdates'.
         */
        function updateAllTemplateTextOfTextOnlyDrawings(collector) {

            _.each(_.keys(collector), function (oneKey) {

                var layoutId = collector[oneKey].layoutId;
                var type = collector[oneKey].type;
                var index = collector[oneKey].index;
                var startText = collector[oneKey].text;

                // check, if the text in the layout slide was modified by the user
                var layoutDrawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId, type, index);

                if (layoutDrawing.length > 0) {

                    if (startText !== layoutDrawing.text()) {

                        // getting all document slides that have the specified layout slide as target
                        var allSlideIDs = self.getAllSlideIDsWithSpecificTargetParent(layoutId);

                        _.each(allSlideIDs, function (slideId) {
                            var drawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(slideId, type, index);

                            if (drawing.length > 0) {
                                if (NO_TEXT_PLACEHOLDER_TYPES.has(type)) {
                                    // updating the template text of this place holder drawing without text content
                                    self.drawingStyles.updateTemplateReadOnlyText(drawing, type, index, slideId);
                                } else {
                                    // also an operation must be generated, so that this text is marked as user specified text
                                    if (app.isEditable()) { self.generateCustomPromptOperation(layoutId, type, index, true); }
                                    // set the new template text and ignore an existing template text
                                    self.drawingStyles.handleEmptyPlaceHolderDrawing(drawing, null, { ignoreTemplateText: true });
                                }
                            }
                        });
                    }
                }
            });
        }

        /**
         * Collecting the changes of the template text in layout slides in drawings, that cannot contain text content. This
         * are place holders for table, image, ... . This text must be shown in the document slides, but can only be modified
         * in the layout slide. Therefore an update of this text in the document slide must be done, after it was modified
         * in the layout slide.
         *
         * Info: This function is called after every change of selection.
         *
         * @param {String} [slideId]
         *  The id of the slide containing the optional drawing. If not specified, the active slide is used.
         *
         * @param {jQuery} [oneDrawing]
         *  The jQuerified drawing node. If not specified, the currently selected drawing is used.
         */
        function collectPlaceHolderTextUpdates(slideId, oneDrawing) {

            var // a unique key
                key = null,
                // the explicit attributes of the drawing
                attrs = null,
                // the additionally selected drawing
                drawing = oneDrawing || null,
                // the ID of the active slide
                id = slideId || activeSlideId,
                // a valid key set of type and index for a place holder drawing
                keySet = null;

            if (oneDrawing || (self.isMasterView() && self.isLayoutSlideId(id) && self.getSelection().isAdditionalTextframeSelection() && isPlaceHolderDrawing(self.getSelection().getSelectedTextFrameDrawing()))) {
                // && isPlaceHolderWithoutTextDrawing(self.getSelection().getSelectedTextFrameDrawing())) {
                drawing = drawing || self.getSelection().getSelectedTextFrameDrawing();
                attrs = getExplicitAttributeSet(drawing);
                keySet = getValidTypeIndexSet(attrs);
                if (!keySet) { return; }
                key = id + '_' + keySet.type + '_' + keySet.index;
                templateTextCollector = templateTextCollector || {};
                if (!templateTextCollector[key]) {
                    templateTextCollector[key] = { layoutId: id, type: keySet.type, index: keySet.index, text: oneDrawing ? '' : drawing.text() };
                }
            } else if (templateTextCollector) {
                // updating all affected drawings after changing the drawing
                updateAllTemplateTextOfTextOnlyDrawings(templateTextCollector);
                templateTextCollector = null;
            }

        }

        /**
         * Check, if a change in ODP footer drawings requires an information of the slide pane. This can
         * be the case after moving, resizing, inserting or deleting an ODF footer placeholder drawing.
         * The information for the slide pane is collected inside the parameter 'odfSlideCollector'.
         *
         * @param {String} masterSlideId
         *  The ID of the ODF master slide (internally handled as layout slide).
         *
         * @param {Number[]} pos
         *  The logical position of the affected drawing.
         *
         * @param {Array} odfSlideCollector
         *  The collector for the objects that will be triggered with the event to inform the slide
         *  pane about modified fields on master slide.
         */
        function checkODFFooterUpdate(masterSlideId, pos, odfSlideCollector) {

            var // the drawing node at the specified position
                oneDrawing = getDOMPosition(self.getSlideById(masterSlideId), [_.last(pos)], true),
                // the list of affected document slides
                allSlides = null;

            if (oneDrawing && oneDrawing.node && _.contains(PresentationModel.FOOTER_TYPES, getPlaceHolderDrawingType(oneDrawing.node))) {

                allSlides = self.getAllSlideIDsWithSpecificTargetParent(masterSlideId);

                _.each(allSlides, function (slideID) {
                    var slideAttrs = self.getMergedSlideAttributesByFamily(slideID, 'slide');
                    collectODFMasterSlideUpdates(slideID, slideAttrs, odfSlideCollector);
                });
            }
        }

        /**
         * Collecting the changes in fields on the master slide for document slides with a specified slide ID. If the slide attributes
         * for date/time fields or footer fields or slide number fields are modified (content or visibility), it is required to inform
         * the slidepane about this changes, so that the underlying master slides can be updated there, too.
         * Info: This is only required for ODF applications.
         *
         * @param {String} slideID
         *  The ID of the document slide.
         *
         * @param {Object} slideAttrs
         *  The attributes of the slide family.
         *
         * @param {Array} odfSlideCollector
         *  The collector for the objects that will be triggered with the event to inform the slide pane about modified fields on
         *  master slide.
         */
        function collectODFMasterSlideUpdates(slideID, slideAttrs, odfSlideCollector) {

            var slideAttrsCopy = _.copy(slideAttrs, true);
            var slideAttrsKeys = _.keys(slideAttrsCopy);
            var masterSlideId = self.getLayoutSlideId(slideID);
            var allFields = null;
            var value = null;
            var savedSlideAttributes = null;

            // in the undo-redo process, the slide attributes might not be complete
            if ((slideAttrsCopy.dateText || slideAttrsCopy.dateField) && !_.contains(slideAttrsKeys, 'isDate')) {
                savedSlideAttributes = self.getMergedSlideAttributesByFamily(slideID, 'slide');
                slideAttrsCopy.isDate = savedSlideAttributes.isDate;
                slideAttrsKeys = _.keys(slideAttrsCopy);
            }

            // handling the date fields
            if (_.contains(slideAttrsKeys, 'isDate')) {
                allFields = self.getFieldManager().getAllMasterDateFields(masterSlideId);
                _.each(allFields, function (oneField) {
                    if (slideAttrsCopy.dateText) {
                        value = slideAttrsCopy.dateText;
                    } else if (slideAttrsCopy.dateField) {
                        value = self.getFieldManager().getDateTimeRepresentation(slideAttrsCopy.dateField);
                    } else {
                        value = '';
                    }
                    odfSlideCollector.push({ fieldID: oneField.fid, masterViewField: true, slideID, value, visible: slideAttrsCopy.isDate, fieldType: 'dt' });
                });
            }

            // handling the slide number fields (only required for visibility)
            if (_.contains(slideAttrsKeys, 'isSlideNum')) {
                allFields = self.getFieldManager().getAllMasterNumFields(masterSlideId);

                _.each(allFields, function (oneField) {
                    value = self.getSlideNumByID(slideID);
                    odfSlideCollector.push({ fieldID: oneField.fid, masterViewField: true, slideID, value, visible: slideAttrsCopy.isSlideNum, fieldType: 'sldNum' });
                });
            }

            // in the undo-redo process, the slide attributes might not be complete
            if (slideAttrsCopy.footerText && !_.contains(slideAttrsKeys, 'isFooter')) {
                savedSlideAttributes = savedSlideAttributes || self.getMergedSlideAttributesByFamily(slideID, 'slide');
                slideAttrsCopy.isFooter = savedSlideAttributes.isFooter;
                slideAttrsKeys = _.keys(slideAttrsCopy);
            }

            // handling the footer fields
            if (_.contains(slideAttrsKeys, 'isFooter')) {
                allFields = self.getFieldManager().getAllMasterFooterFields(masterSlideId);

                _.each(allFields, function (oneField) {
                    value = slideAttrsCopy.footerText || '';
                    odfSlideCollector.push({ fieldID: oneField.fid, masterViewField: true, slideID, value, visible: slideAttrsCopy.isFooter, fieldType: 'ftr' });
                });
            }

        }

        /**
         * Collecting the template texts for fields on the master slides. This function is only used in ODF format
         * an is required to inform the side pane about the template texts on the master slides.
         * Info: This is only required for ODF applications.
         *
         * @param {Array} odfSlideCollector
         *  The collector for the objects that will be triggered with the event to inform the slide pane about modified fields on
         *  master slide.
         */
        function collectODFMasterSlideDefaultValues(odfSlideCollector) {

            var // the replacement string for date fields on ODF master slides
                masterDateValue = getODFFooterRepresentation('dt'),
                // the replacement string for footer fields on ODF master slides
                masterFooterValue = getODFFooterRepresentation('ftr'),
                // the replacement string for slide number fields on ODF master slides
                masterSlideNumberValue = '<#>',
                // the IDs of all master slides
                allMasterSlideIDs = self.getMasterSlideOrder();

            _.each(allMasterSlideIDs, function (masterSlideId) {

                // handling the date fields
                var allFields = self.getFieldManager().getAllMasterDateFields(masterSlideId);
                _.each(allFields, function (oneField) {
                    odfSlideCollector.push({ fieldID: oneField.fid, masterViewField: true, slideID: masterSlideId, value: masterDateValue, visible: true, fieldType: 'dt' });
                });

                // handling the slide number fields
                allFields = self.getFieldManager().getAllMasterNumFields(masterSlideId);
                _.each(allFields, function (oneField) {
                    odfSlideCollector.push({ fieldID: oneField.fid, masterViewField: true, slideID: masterSlideId, value: masterSlideNumberValue, visible: true, fieldType: 'sldNum' });
                });

                // handling the footer fields
                allFields = self.getFieldManager().getAllMasterFooterFields(masterSlideId);
                _.each(allFields, function (oneField) {
                    odfSlideCollector.push({ fieldID: oneField.fid, masterViewField: true, slideID: masterSlideId, value: masterFooterValue, visible: true, fieldType: 'ftr' });
                });
            });
        }

        // private listener functions -----------------------------------------

        /**
         * Function that is executed after the 'import success' event of the application.
         */
        function importSuccessHandler() {

            // no functionality of page layout required
            self.deletePageLayout();
            // no functionality of drawing layer required
            self.disableDrawingLayer();
            // deleting place holder for header and footer
            DOM.getMarginalTemplateNode(pagediv).remove();

            // set the local slide pane width to the width previously saved in the document
            localViewSlidePaneWidth = self.getSlidePaneWidthInDocAttributes();

            // the active slide was already set in updateDocumentFormatting -> now the slide can be selected
            self.getSelection().setSlideSelection();

            // registering the selection box handler at the application content root (not in SlideTouchMode)
            if (!self.getSlideTouchMode()) {
                selectionBox = new SelectionBox(app, app.getView().getContentRootNode());
                selectionBox.registerSelectionBoxMode(PresentationModel.SET_SELECTION_MODE, { contentRootClassActive: PresentationModel.SET_SELECTION_MODE, selectionFilter: PresentationModel.MULTI_SELECTION_BOX_FILTER, startHandler: selectionBoxStartHandler, stopHandler: self.getSelection().selectAllDrawingsInSelectionBox });
                selectionBox.setActiveMode(PresentationModel.SET_SELECTION_MODE);
            } else {
                // 'touchstart' event next to the slide needs to be handled in slide touch mode (because selection box is not active)
                app.getView().getContentRootNode().on('touchstart', touchSlideBackgroundHandler);
            }

            // registering the vertical scroll handler to switch between the slides (not on touch devices)
            if (!TOUCH_DEVICE) { app.getView().enableVerticalScrollBar(); }

            // register the listener for 'change:slidelayout'.
            // -> this update shall not be triggered during document load.
            self.on('change:slidelayout', updateDrawingLayoutFormatting);

            // register the listener for 'change:activeView'.
            // -> this update shall not be triggered during document load.
            self.on('change:activeView:after', updateActiveViewHandler);

            // register the listener for 'operations:after'.
            // -> this update shall not be triggered during document load.
            self.on('operations:after', getOperationAfterHandler());

            // register the listener for 'operations:after'.
            // -> this update shall not be triggered during document load.
            self.on('operations:before', operationBeforeHandler);

            // register the listener to handle the visibility of the page node
            // -> this node must be made invisible after removing the last document slide
            self.on('removed:slide inserted:slide change:activeView', pageVisibilityHandler);

            // register the listener to handle the active slide (of read-only user) after slide removal
            self.on('removed:slide', handleActiveSlideInReadOnlyMode);

            // the slide format manager triggered, that some slides are formatted
            self.on('slidestate:formatted', slideFormattingHandler);

            // the table formatting process triggered, that table formatting is done
            self.on('table:formatting:done', afterTableFormattingHandler);

            // in ODF application, the table height must be send to the filter (52176)
            if (app.isODF()) { self.on('drawingHeight:update', createDebouncedDrawingHeightUpdateHandler()); }

            // Creating handler for full update of changed slides
            // -> this can be used for example after changing the layout of a slide
            implSlideChanged = createDebouncedUpdateSlideHandler();

            // Creating the handler that informs the slide pane about changes (not handled by 'operations:after')
            debouncedUpdateSlideStateHandler = getDebouncedUpdateSlideStateHandler();

            // handling selection change events
            selection.on('change', selectionChangeHandler);

            // registering touchmove handler at the page
            if (TOUCH_DEVICE) { pagediv.on({ touchmove: handleTouchMove }); }

            // starting the slide visibility manager to remove formatted slide from the DOM
            self.getSlideVisibilityManager().initializeManager();

            // after loading it might be necessary to set the selection (because of reload or specified selection)
            setSelectionAfterLoad();
        }

        /**
         * Updating the model for the presentation application. This is necessary, after the document
         * was loaded with fast load or from local storage.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.deleteModel=false]
         *   Whether the existing model needs to be deleting before updating it.
         *  @param {Boolean} [options.documentReloaded=false]
         *   Whether the document was reloaded with a new snapshot.
         */
        function updatePresentationModel(options) {

            var // whether the existing model needs to be removed
                deleteExistingModel = getBooleanOption(options, 'deleteModel', false),
                // whether the document was reloaded (snapshot was applied)
                documentReloaded = getBooleanOption(options, 'documentReloaded', false),
                // whether the document was loaded from local storage
                usedLocalStorage = getBooleanOption(options, 'usedLocalStorage', false);

            // deleting the model is required, if a new snapshot is applied
            if (deleteExistingModel) { emptyPresentationModel(); }

            // setting the master and layout layer node
            layoutSlideLayerNode = pagediv.find('>' + DOM.LAYOUTSLIDELAYER_SELECTOR);
            masterSlideLayerNode = pagediv.find('>' + DOM.MASTERSLIDELAYER_SELECTOR);

            // Handling for master slides and layout slides in ODP for fast load process
            // -> attaching all children of master slide layer to layout slide layer before filling the model
            if (app.isODF()) {
                _.each(masterSlideLayerNode.children(), function (slideContainer) { layoutSlideLayerNode.append(slideContainer); });
            }

            // Setting contenteditable 'false' to layout and master slide layer node in IE (46752, 47070)
            DOM.handleReadOnly(layoutSlideLayerNode);
            DOM.handleReadOnly(masterSlideLayerNode);

            // filling the slide model
            addAllSlidesOfLayerIntoSlideModel(masterSlideLayerNode, '>' + DOM.SLIDECONTAINER_SELECTOR + '>' + DOM.SLIDE_SELECTOR, self.getMasterSlideType(), { documentReloaded });
            addAllSlidesOfLayerIntoSlideModel(layoutSlideLayerNode, '>' + DOM.SLIDECONTAINER_SELECTOR + '>' + DOM.SLIDE_SELECTOR, self.getLayoutSlideType(), { documentReloaded });
            addAllSlidesOfLayerIntoSlideModel(pagediv, '>' + DOM.PAGECONTENT_NODE_SELECTOR + '>' + DOM.SLIDE_SELECTOR, self.getStandardSlideType(), { documentReloaded });

            updateSlideContainer(masterSlideLayerNode);
            updateSlideContainer(layoutSlideLayerNode);
            // filling the drawing place holder model
            _.each(pagediv.find('.drawing'), function (drawing) {

                var // the target attribute of the drawing
                    target = null;

                drawing = $(drawing);
                target = drawing.attr('target');  // this attribute is set in fast load

                // removing the attributes from fast load, because they are not updated by the model
                drawing.removeAttr('target');

                // trying to get target also after loading using snapshot or loading from local storage (load without fast load (and unit test))
                if (!target && (deleteExistingModel || usedLocalStorage) && isPlaceHolderDrawing(drawing)) {
                    // getting the slide of this drawing to determine the target (if it is a master or layout slide)
                    target = self.getSlideId(drawing.closest(DOM.SLIDE_SELECTOR));
                    if (target && !self.isLayoutOrMasterId(target)) { target = null; }
                }

                if (target) {
                    self.drawingStyles.savePlaceHolderAttributes(target, drawing.data('attributes'), drawing);
                }

                var drawingAttrs = getExplicitAttributeSet(drawing);
                if (drawingAttrs && drawingAttrs.drawing && drawingAttrs.drawing.id) { self.storeDrawingId(drawingAttrs.drawing.id); }
            });
        }

        /**
         * Presentation specific handler for the selection 'change' event.
         */
        function selectionChangeHandler() {
            self.handleListItemVisibility(); // handling the visibility of the list items in empty paragraphs
            collectPlaceHolderTextUpdates();

            // check for an unsupported video or audio 'drawing' (DOCS-3863)
            if (selection.isDrawingSelection()) {

                const drawing = selection.getSelectedDrawing();
                const attrs = getExplicitAttributeSet(drawing);
                let counter = drawing.data("videocounter") || 0;

                if ((attrs?.presentation?.phVideo || attrs?.presentation?.phAudio || attrs?.presentation?.phAudioFile) && counter < 2) {
                    const message = gt("Audio and video are not supported yet.");
                    app.getView().yell({ type: 'info', duration: 10000, message });
                    counter++;
                    drawing.data("videocounter", counter); // showing the yell only twice for each drawing
                }
            }
        }

        /**
         * Handler for touch devices. Avoiding a scrolling page, if the user has a text selection
         * inside a drawing.
         *
         * @param {jQuery.Event} event
         *  The jQuery event object.
         */
        function handleTouchMove(event) {
            if (selection.isAdditionalTextframeSelection()) {
                event.preventDefault(); // required for avoiding moving of page on touch devices
                event.stopPropagation();
            }
        }

        /**
         * Handler function for the event 'templatetext:updated'. After loading a document and
         * inserting the template texts on layout slides, it might be necessary to update this
         * template texts also on the related document slides.
         *
         * @param {String} slideId
         *  The id of the slide containing the specified drawing.
         *
         * @param {jQuery} drawing
         *  The jQuerified drawing node.
         */
        function templateTextHandler(slideId, drawing) {
            collectPlaceHolderTextUpdates(slideId, drawing); // filling the templateTextCollector
            collectPlaceHolderTextUpdates(); // resolving the templateTextCollector
        }

        /**
         * Check, whether a slide change is required before running the redo operations. Typically
         * this slide change and also the change of masterView/documentView is required. But there
         * is a very special exception:
         *
         * Avoiding the switch to the master slide, if only the background of a master or layout slide
         * is changed. This can be triggered from a document slide and therefore the switch to the
         * master slide after redo could irritate the user (DOCS-1738).
         *
         * @param {Object[]} operations
         *  An array containing the redo operations.
         */
        function suppressSlideChangeInRedo(operations) {

            var suppressSlideChange = false;

            // not changing to a master/layout slide, if only a slide attribute of the master slide was changed (DOCS-1738)
            if (operations && operations.length === 1 && operations[0].target && operations[0].name && operations[0].name === Op.SET_ATTRIBUTES && operations[0].start && operations[0].start.length === 1) { suppressSlideChange = true; }

            return suppressSlideChange;
        }

        /**
         * Listener to the events 'undo:before' and 'redo:before'.
         *
         * @param {boolean} isRedo
         *  Whether to process redo operations (`true`), or undo operations
         *  (`false`).
         *
         * @param {Object[]} operations
         *  An array containing the undo or redo operations.
         *
         * @param {Object} options
         *  Options with information about the selection that needs to be set.
         */
        function undoRedoBeforeHandler(isRedo, operations, options) {

            // the slide ID of the first operation
            var slideId = null;
            // whether the slide ID belongs to a master or layout slide
            var isLayoutMasterSlide = false;
            // the seection state to be applied
            var selectionState = options && options.selectionState;

            if (isRedo) { // ignore selection state of redo operations
                selectionState = null; // TODO: Is it still required to empty the selectionState in redo?
                if (suppressSlideChangeInRedo(operations)) { return; }
            }

            if (selectionState && _.isString(selectionState.target) && selectionState.start) {
                isLayoutMasterSlide = !!selectionState.target;
                slideId = getSlideIdFromOperation({ name: 'state', target: selectionState.target, start: selectionState.start });
            } else if (operations.length > 0) {
                slideId = getSlideIdFromOperation(operations[0]); // using the first operation
                isLayoutMasterSlide = self.isLayoutOrMasterId(slideId);
            }

            if (isLayoutMasterSlide !== isMasterView) { setActiveView(isLayoutMasterSlide); }  // activating the new view, if required
            self.setActiveSlideId(slideId); // finally activating the slide
        }

        /**
         * Listener to the events 'undo:after' and 'redo:after'.
         *
         * @param {boolean} isRedo
         *  Whether to process redo operations (`true`), or undo operations
         *  (`false`).
         *
         * @param {Object[]} operations
         *  An array containing the undo or redo operations.
         *
         * @param {Object} options
         *  Options with information about the selection that needs to be set.
         */
        function undoRedoAfterHandler(isRedo, operations, options) {

            var // finding first insert slide operation
                handleOperation = null,
                // the ID of the inserted slide
                slideId = null,
                // whether the new inserted slide is a layout or master slide
                isLayoutMasterSlide = false,
                // whether a change of the active view is required
                changeActiveView = false,
                // the start position of the first operation
                start = null,
                // the text frame node of a selected drawing
                textFrame = null,
                // the seection state to be applied
                selectionState = options && options.selectionState;

            // ignore selection state object in redo operations
            if (isRedo) { selectionState = null; }

            // helper function to activate the slide, that is affected by the operation
            function switchToSlide(id) {
                isLayoutMasterSlide = self.isLayoutOrMasterId(id);
                changeActiveView = (isLayoutMasterSlide !== isMasterView);  // change of view required?
                if (changeActiveView) { setActiveView(isLayoutMasterSlide); }  // activating the new view, if required
                self.setActiveSlideId(id); // finally activating the new inserted slide
            }

            // handling insert slide operations
            function handleInsertSlideOperation() {
                slideId = getSlideIdFromOperation(handleOperation); // getting the slide ID from the operation
                switchToSlide(slideId);
            }

            // setting the slide selection after an undo of a slide-move operation (49836)
            function handleMoveSlideOperation() {
                slideId = handleOperation.id ? handleOperation.id : slideOrder[_.first(handleOperation.end)]; // getting the slide ID from the operation
                switchToSlide(slideId);
                self.getSelection().setSlideSelection();
            }

            // handling insert slide operations
            function handleDeleteSlideOperation() {
                isLayoutMasterSlide = _.isString(handleOperation.target);
                changeActiveView = (isLayoutMasterSlide !== isMasterView);  // change of view required?
                if (changeActiveView) { setActiveView(isLayoutMasterSlide); } // activating the new view, if required
                self.changeToSlideAtIndex(handleOperation.start[0]); // activating the slide in the correct view
            }

            // handling group/ungroup operations
            function handleGroupOperations() {
                var allGroupingPositions = []; // collector for all logical positions of drawings that will be selected
                slideId = getSlideIdFromOperation(handleOperation[0]); // getting the slide ID from the operation
                if (!self.isActiveSlideId(slideId)) { switchToSlide(slideId); }

                // there can only be more than one group and more than one ungroup operation
                if (handleOperation[0].name === Op.GROUP) {
                    selection.clearMultiSelection(); // clearing the existing multi selection
                    if (handleOperation.length === 1) {
                        selection.setTextSelection(handleOperation[0].start, increaseLastIndex(handleOperation[0].start)); // selecting the grouped drawing
                    } else {
                        // undo of several combined 'ungroup' operations
                        _.each(handleOperation, function (op) { allGroupingPositions.push(op.start); });
                        selection.setMultiDrawingSelectionByPosition(allGroupingPositions); // selecting all grouped drawings
                    }
                } else {
                    _.each(handleOperation, function (op) {
                        var slidePos = _.initial(op.start);
                        var fullDrawingPos = _.map(op.drawings, function (index) {
                            var newPos = _.clone(slidePos);
                            newPos.push(index);
                            return newPos;
                        });
                        allGroupingPositions = allGroupingPositions.concat(fullDrawingPos);
                    });
                    selection.setMultiDrawingSelectionByPosition(allGroupingPositions); // selecting all ungrouped drawings
                }
            }

            // handling move operations
            function handleMoveOperations() {
                // changing the active slide and selecting all moved drawings
                var allDrawingPositions = []; // collector for all logical positions of drawings that will be selected
                slideId = getSlideIdFromOperation(handleOperation[0]); // getting the slide ID from the operation
                if (!self.isActiveSlideId(slideId)) { switchToSlide(slideId); }

                if (handleOperation.length === 1) {
                    selection.setTextSelection(handleOperation[0].to, increaseLastIndex(handleOperation[0].to)); // selecting the one drawing
                } else {
                    // undo of several combined 'ungroup' operations
                    _.each(handleOperation, function (op) { allDrawingPositions.push(op.to); });
                    selection.setMultiDrawingSelectionByPosition(allDrawingPositions); // selecting all moved drawings
                }
            }

            // handling insertDrawing operations (without any insertSlide operation)
            function handleInsertDrawingOperations() {

                var allDrawingPositions = []; // collector for all logical positions of drawings that will be selected
                var groupOperations = null; // a collector for the top level group operations

                slideId = getSlideIdFromOperation(handleOperation[0]); // getting the slide ID from the operation
                if (!self.isActiveSlideId(slideId)) { switchToSlide(slideId); }

                // formatting the top level groups once more (50010)
                groupOperations = _.filter(handleOperation, function (operation) { return operation.type === 'group' && operation.start.length === 2; });

                _.each(groupOperations, function (oneOp) {
                    var oneDrawing = getDOMPosition(self.getSlideById(activeSlideId), [_.last(oneOp.start)], true);
                    if (oneDrawing?.node) { self.drawingStyles.updateElementFormatting(oneDrawing.node); }
                });

                if (isRedo) {
                    // #63168 - try to set multiselection or text selection only if redo is not part of the change layout operation group
                    // after changeLayout operation, there can be one or many insertDrawing operations, they shouldn't trick us to set selection.
                    // Layouts can have zero or different amount of drawings, so trying to set selection after redo of chageLayout leads to unpredicted results.
                    var changeLayoutOperation = operations.find(op => op.name === Op.LAYOUT_CHANGE);
                    if (!changeLayoutOperation) {
                        if (handleOperation.length > 1) { // setting selection only required in redo

                            // -> setting multi selections, if more than one drawing was inserted
                            _.each(handleOperation, function (op) {
                                if (op.start.length === 2) { allDrawingPositions.push(op.start); } // collecting all top level drawings, no grouped drawings
                            });

                            if (allDrawingPositions.length > 0) {
                                selection.setMultiDrawingSelectionByPosition(allDrawingPositions); // selecting all inserted drawings
                            }
                        } else if (handleOperation.length === 1) {
                            selection.setTextSelection(handleOperation[0].start, increaseLastIndex(handleOperation[0].start));
                        }
                    } else {
                        // remove any selection that is left before redo of change layout - different layout, no garantee that the drawing with same position will exist
                        if (!selection.isSlideSelection()) { selection.setSlideSelection(); }
                    }
                }

            }

            // handling setAttribute operations that modify the slide attributes (needed in ODF to update fields in footer)
            function handleSetSlideAttributeOperations() {
                self.getFieldManager().updateMasterSlideFields();
            }

            // checking for special operations in the undo group
            // finding first insert slide operation
            handleOperation = operations.find(operation => PresentationModel.INSERT_SLIDE_OPERATIONS.includes(operation.name));
            if (handleOperation) {
                handleInsertSlideOperation();
            } else {
                handleOperation = operations.find(operation => PresentationModel.MOVE_SLIDE_OPERATIONS.includes(operation.name));
                if (handleOperation) {
                    handleMoveSlideOperation();
                } else {
                    // finding first delete operation of a slide
                    handleOperation = operations.find(operation => isDeleteSlideOperation(operation));
                    if (handleOperation) {
                        handleDeleteSlideOperation();
                    } else {
                        // finding group/ungroup operations (only required for 'redo' without selectionState)
                        handleOperation = selectionState ? null : _.filter(operations, function (operation) { return (operation.name === Op.GROUP || operation.name === Op.UNGROUP); });
                        if (handleOperation && handleOperation.length > 0) {
                            handleGroupOperations();
                        } else {
                            // finding move operations (only required for 'redo' without selectionState)
                            handleOperation = selectionState ? null : _.filter(operations, function (operation) { return (operation.name === Op.MOVE); });
                            if (handleOperation && handleOperation.length > 0) {
                                handleMoveOperations();
                            } else {
                                // finding insertDrawing operations (without insertSlide)
                                handleOperation = _.filter(operations, function (operation) { return operation.name === Op.INSERT_DRAWING; });
                                if (handleOperation && handleOperation.length > 0) {
                                    handleInsertDrawingOperations();
                                } else {
                                    // finding setAttributes operations that modify the slide attributes (updating footer fields on the active slide)
                                    handleOperation = app.isODF() && _.filter(operations, function (operation) { return operation.name === Op.SET_ATTRIBUTES && operation.attrs && operation.attrs.slide; });
                                    if (handleOperation && handleOperation.length > 0) {
                                        handleSetSlideAttributeOperations();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // generating a simple selection state for redo (needs to be expanded in the future) -> used for setAttributes and drawings
            if (isRedo && !selectionState && !_.isEmpty(operations)) {
                if (_.every(operations, function (op) { return op.name === Op.SET_ATTRIBUTES; })) { // only setAttributes operations
                    selectionState = { start: _.first(operations).start, end: increaseLastIndex(_.last(operations).end) };
                }
            }

            // evaluation the selectionState object
            if (selectionState && selectionState.start && selectionState.start.length > 0 && selectionState.end && selectionState.end.length > 0) {
                if (selectionState.isMultiSelection && selectionState.allPositions) {
                    selection.setMultiDrawingSelectionByPosition(selectionState.allPositions); // selecting all inserted drawings
                } else {
                    selection.setTextSelection(selectionState.start, selectionState.end, { remoteSelectionUpdate: true });
                }
            }

            // redo handling (needs to be improved)
            if (!selectionState && isRedo && operations.length > 0 && (!handleOperation || handleOperation.length === 0)) {
                // if not handled before, set slide selection (avoiding problems with deleting a drawing in redo (52628))
                start = operations[0].start; // using first operation (like in before handler)
                if (start && isValidElementPosition(self.getRootNode(operations[0].target), start, { allowFinalPosition: true })) {
                    selection.setTextSelection(start);
                } else {
                    if (!selection.isSlideSelection()) { selection.setSlideSelection(); }
                }
            }

            // in Edge browser it is necessary, to set the focus into the textframe
            if (_.browser.Edge && selection.isAdditionalTextframeSelection()) {
                textFrame = selection.getSelectedTextFrameDrawing().find('div.textframe');
                if (textFrame.length > 0) { setFocus(textFrame); }
            }

            // check, if the last slide was deleted -> the selection must be empty (48215)
            if (self.isEmptySlideView()) {
                // delete the complete selection, set cursor into clipboard
                self.getSelection().setEmptySelection();
                // removing active slide ID
                self.setActiveSlideId('');
            }

            // in ODF format, the filter needs to be informed about changes of table heights
            if (app.isODF() && selection.isAdditionalTextframeSelection() && isTableDrawingFrame(selection.getSelectedTextFrameDrawing())) {
                self.trigger('drawingHeight:update', selection.getSelectedTextFrameDrawing());
            }

        }

        /**
         * Event handler for preparing the removal of the currently active slide.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {HTMLElement|jQuery} [options.startNode=null]
         *   The node that will be removed soon.
         */
        function prepareSlideDeletion(options) {
            var slide = options && options.startNode;
            if (slide && selectionBox && selectionBox.isSelectionBoxActive() && DOM.isSlideNode(slide) && self.getSlideId(slide) === self.getActiveSlideId()) {
                selectionBox.cancelSelectionBox();
            }
        }

        /**
         * Listener function for the event 'change:slidelayout'. This function is called after modifications
         * of layout or master slide. Then it is necessary to update all slides, that use the modified slide
         * as target (or whose target uses the modified slide as target).
         *
         * Info: This function is critical for the performance. It might be necessary to update
         *       many elements. Therefore this should be done asynchronous (TODO).
         *
         * -> TODO: Shifting this into another class?
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.target='']
         *   The target of the layout or master slide that was modified
         *  @param {String} [options.placeHolderType='']
         *   The place holder type that was modified.
         *  @param {Number} [options.placeHolderIndex=null]
         *   The place holder type that was modified.
         *  @param {Object} [options.attrs=null]
         *   The set of modified attributes.
         *  @param {Object} [options.oldAttrs=null]
         *   The old set of attributes.
         *  @param {Boolean} [options.slideBackgroundUpdate=false]
         *   Whether the slide background of the overlaying slides need
         *   to be updated. Only required for theming colors. (48471)
         */
        function updateDrawingLayoutFormatting(options) {

            var // the target of the modified master or layout slide
                target = getStringOption(options, 'target', ''),
                // a container with the IDs of all slides affected by the modification of the master/layout slide
                allSlideIDs = self.getAllSlideIDsWithSpecificTargetAncestor(target),
                // the type of place holder that was modified
                type = allSlideIDs.length > 0 ? getStringOption(options, 'placeHolderType', '') : null,
                // the index of place holder that was modified
                index = allSlideIDs.length > 0 ? getNumberOption(options, 'placeHolderIndex', null) : null,
                // the changed attributes
                attrs = allSlideIDs.length > 0 ? getObjectOption(options, 'attrs', null) : null,
                // whether the modification was done on the master slide (-> update should be independent from index?)
                isMasterSlideChange = target && self.isMasterSlideId(target),
                // the slide background needs to be updated
                slideBackgroundUpdate = getBooleanOption(options, 'slideBackgroundUpdate', false);

            // updating attributes and/or list styles of one drawing
            function updateOneDrawing(drawing) {

                // updating the drawing itself, if required (change of position, size, background, border, vertical paragraph alignment, ... (47594))
                if (attrs.drawing || attrs.fill || attrs.line || attrs.shape) {
                    self.drawingStyles.updateElementFormatting($(drawing));
                }

                // updating the paragraphs, if required
                if (attrs.listStyle) {
                    _.each($(drawing).find(DOM.PARAGRAPH_NODE_SELECTOR), function (para) {
                        // updating all paragraphs, that have a modified attribute set
                        if (self.generateListKeyFromLevel(self.paragraphStyles.getParagraphLevel(para)) in attrs.listStyle) {
                            self.paragraphStyles.updateElementFormatting($(para));
                            self.updateListsDebounced($(para), { singleParagraph: true }); // only list update for the specified paragraph
                        }
                    });
                }
                self.updateDynFontSizeDebounced($(drawing));
            }

            if (target && type && _.isNumber(index) && attrs) {

                _.each(allSlideIDs, function (id) {

                    var // the list of all indizes that will be updated
                        allIndices = null,
                        // an object containing the conversion for place holder drawings on master slides
                        // -> 'ctrTitle' is 'title' on master, 'subTitle' is 'body' on master
                        masterTypes = _.invert(self.drawingStyles.getMasterSlideTypeConverter()),
                        // a type array, if more than one type needs to be adapted
                        typeArray = null;

                    self.appendSlideToDom(activeSlideId); // attaching the slide node to the DOM (really necessary?)

                    if (isMasterSlideChange && masterTypes[type]) {

                        // not only searching for 'body', but also for 'subTitle' place holders
                        typeArray = [type, masterTypes[type]];

                        // updating all drawings, that have the specified place holder type
                        // -> using options.baseAttributes for the place holder attributes from layout slide and master slide
                        _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(id, typeArray), function (drawing) {
                            updateOneDrawing(drawing);
                        });

                    } else {

                        allIndices = [index];

                        // changes in the master slide affect all 'unassigned placeholder' drawings
                        if (isMasterSlideChange) { allIndices.push(self.drawingStyles.getMsDefaultPlaceHolderIndex()); }

                        _.each(allIndices, function (oneIndex) {

                            // ignoring index for footer indices
                            if (_.contains(PresentationModel.FOOTER_TYPES, type)) { oneIndex = null; }

                            // updating all drawings, that have the specified place holder type
                            // -> using options.baseAttributes for the place holder attributes from layout slide and master slide
                            _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(id, type, oneIndex), function (drawing) {
                                updateOneDrawing(drawing);
                            });
                        });
                    }
                });
            } else if (target && slideBackgroundUpdate) {
                _.each(allSlideIDs, function (slideId) {
                    var fillAttrs = null;
                    // updating all slides that have a specified theme but no specified background (48471)
                    if (self.hasDefaultTheme(slideId)) {
                        fillAttrs = self.getSlideAttributesByFamily(slideId, 'fill');
                        if (!fillAttrs || (fillAttrs && fillAttrs.type && fillAttrs.type === 'none')) {
                            // theme defined, but no background specified -> reformatting required
                            self.slideStyles.forceSlideBackgroundUpdateWithSpecificId(self.getSlideById(slideId), { fill: { type: 'none' } }, slideId);
                        }
                    }
                });
            }
        }

        /**
         * Listener function for the event 'change:activeView:after'. This function is called when the active view
         * (model) is changed, or to put it simply, when the view is switched between the 'normal' slides and
         * the master/layout slides. After that it is necessary to activate the appropriate slide in the view.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.isMasterView=false]
         *   Whether the masterView was activated.
         */
        function updateActiveViewHandler(options) {

            var // the id of the slide that will be activated
                newSlideId = null,
                // whether the master/layout view will be activated or not
                showMaster = getBooleanOption(options, 'isMasterView', false);

            // activating a slide in the view
            if (showMaster) {
                lastActiveStandardSlideId = activeSlideId;
                newSlideId = self.getLayoutOrMasterSlideId(activeSlideId);
            } else {
                newSlideId = lastActiveStandardSlideId;
            }

            if (newSlideId === '' && !showMaster) {
                setActiveSlideId('', { forceUpdate: true }); // allowing the empty string for 'newSlideId', if this is not the master view
            } else {
                setActiveSlideId(newSlideId || self.getIdOfFirstSlideOfActiveView(), { forceUpdate: true });
            }
        }

        /**
         * Listener function for the event 'document:reloaded', that is triggered, if the user cancels a long
         * running action.
         */
        function documentReloadHandler() {
            // deleting and updating the complete presentation model
            updatePresentationModel({ deleteModel: true, documentReloaded: true });
        }

        /**
         * Listener function for the event 'document:reloaded:after', that is triggered, if the user cancels a long
         * running action.
         *
         * @param {Object} snapShot
         *  The snapshot object.
         */
        function documentReloadAfterHandler(snapShot) {

            var allSelections = snapShot.getStoredPositions();

            self.getNode().children('.drawingselection-overlay').empty(); // clearing old selection frames

            if (allSelections) {
                if (allSelections.length > 1) {
                    selection.setMultiDrawingSelectionByPosition(allSelections);
                } else {
                    selection.setTextSelection(allSelections[0].startPosition, allSelections[0].endPosition);
                }
            }
        }

        /**
         * Handler for the event 'image:loaded'. It is possible, that the slide pane
         * needs to be informed about a loaded image (44971).
         *
         * @param {Node|jQuery} node
         *  The image drawing node.
         */
        function imageLoadedHandler(node) {

            // do nothing, if this is triggered by the drawing formatting dialog (DOCS-4733)
            if ($(node).hasClass(TEMPORARY_MARKER_CLASS)) { return; }

            var // the slide node
                slideNode = $(node).parent(),
                // the slide ID
                slideId = null;

            if (DOM.isSlideNode(slideNode)) {
                slideId = self.getSlideId(slideNode);
                if (!slideFormatManager.isUnformattedSlide(slideId)) {
                    debouncedUpdateSlideStateHandler(slideId); // registering the ID
                    // TODO: Also handling master/layout slide dependencies (?)
                }
            }

        }

        //        /**
        //         * Updating the top value of a drawing node. This is necessary, if a drawing node has its vertical
        //         * text content aligned to bottom or centered and if this drawing has 'autoResizeHeight' set to true.
        //         * In that case, the drawing needs to grow in top direction.
        //         * -> Problem: Can this happen without an operation?
        //         *
        //         * @param {jQuery} node
        //         *  The drawing node.
        //         */
        //        function updateVerticalDrawingPosition(node) {
        //
        //            var // the text frame node inside the drawing node
        //                textFrame = null,
        //                // the vertical text aligment property
        //                verticalAlign = null,
        //                // the height saved at the node
        //                savedHeight = null,
        //                // the new height of the drawing node
        //                newHeight = null,
        //                // the difference of old and new height
        //                difference = null,
        //                // the new value for the top offset
        //                newTopOffset = null;
        //
        //            if (DrawingFrame.isAutoResizeHeightDrawingFrame(node)) {
        //
        //                // the text frame node inside the drawing node
        //                textFrame = DrawingFrame.getTextFrameNode(node);
        //                // the vertical text alignment property
        //                verticalAlign = (textFrame && textFrame.length > 0) ? textFrame.attr(DrawingFrame.VERTICAL_ALIGNMENT_ATTRIBUTE) : '';
        //
        //                if (verticalAlign && (verticalAlign === 'bottom' || verticalAlign === 'centered')) {
        //
        //                    if (!_.isString(node.attr('verticalTop'))) { saveVerticalPositionAttributes(node); }
        //
        //                    savedHeight = getElementAttributeAsInteger(node, 'verticalHeight', 0);
        //                    newHeight = node.height();
        //
        //                    if (savedHeight !== newHeight) {
        //                        difference = savedHeight - newHeight;
        //                        if (verticalAlign === 'centered') { difference = Math.round(difference / 2); }
        //                        newTopOffset = getElementAttributeAsInteger(node, 'verticalTop', 0) + difference;
        //                        node.css('top', newTopOffset + 'px');
        //                        node.attr({ verticalTop: newTopOffset, verticalHeight: newHeight });
        //                    }
        //                }
        //            }
        //        }
        //
        //        /**
        //         * Saving the vertical position and height of the text frame. This values can be used
        //         * to calculate a possible vertical movement 'upwards' of the text frame. This is only
        //         * necessary, if the vertical text alignment is set to 'bottom' or 'centered'.
        //         * This values need to be set for example from 'handleAdditionalTextFrameSelection'
        //         * -> never overwrite existing values
        //         *
        //         * @param {jQuery} node
        //         *  The drawing node.
        //         */
        //        function saveVerticalPositionAttributes(node) {
        //            if (DrawingFrame.isAutoResizeHeightDrawingFrame(node) && !_.isNumber(node.attr('verticalTop'))) {
        //                node.attr({ verticalTop: convertCssLength($(node).css('top'), 'px', 1), verticalHeight: node.height() });
        //            }
        //        }

        /**
         * This function returns the ID of that slide, that was modified
         * by the specified operation.
         *
         * @param {Object} operation
         *  The object containing one operation.
         *
         * @returns {String}
         *  The ID of the slide, that was modified by the specified operation.
         *  If this ID cannot be determined, null is returned.
         */
        function getSlideIdFromOperation(operation) {

            var // the slide ID of the slide affected by this operation
                slideID = null,
                // the name of the operation
                name = operation.name;

            if (operation.id && _.contains([Op.LAYOUT_SLIDE_INSERT, Op.MASTER_SLIDE_INSERT], name)) {

                slideID = operation.id;

            } else if (operation.target && name !== Op.SLIDE_INSERT && name !== Op.LAYOUT_CHANGE) {

                // handling for content of master and layout slides -> the target is already the ID
                slideID = operation.target;

            } else if (name === Op.SLIDE_MOVE && !operation.target) {

                // if a slide was moved, the destination must be used instead of the start position
                slideID = slideOrder[_.first(operation.end)];

            } else {

                // handling for document slides -> the ID is saved in the sorted 'slideOrder' collector
                // -> the start position of the operation can be used to determine the ID of the slide.
                if (operation.start) {
                    slideID = slideOrder[_.first(operation.start)];
                }
            }

            return slideID;
        }

        /**
         * Setting the visibility of the page node. This node must be made invisible,
         * if the last document node was deleted.
         *
         * @returns {Boolean}
         *  Whether the visibility of the page node has changed.
         */
        function pageVisibilityHandler() {

            // whether the visibility was changed
            var visibilityChanged = false;
            // whether the current visible view is empty
            var isEmptyView = self.isEmptySlideView();
            // whether the page node is hidden
            var isHiddenPage = pagediv.hasClass('hiddenpage');

            if (isEmptyView && !isHiddenPage) {
                pagediv.addClass('hiddenpage'); // do hide the page
                visibilityChanged = true;
            } else if (isHiddenPage && !isEmptyView) {
                pagediv.removeClass('hiddenpage'); // do hide the page
                visibilityChanged = true;
            }

            return visibilityChanged;
        }

        /**
         * Switching to the first slide in read-only mode, if no active slide can be determined.
         * Example: Two slides, read-only user is on slide 1. Active user deletes slide 1.
         */
        function handleActiveSlideInReadOnlyMode() {
            if (!app.isEditable() && self.getActiveSlideId() === '' && !self.isMasterView() && !self.isEmptySlideView()) { self.changeToSlide(self.getSlideIdByPosition(0)); }
        }

        /**
         * Handler for formatted slides. If the slideFormatManager triggers that slide formatting is
         * done for specific slides, it might be possible to display the active slide.
         */
        function slideFormattingHandler() {
            // checking if a not formatted active slide can now be made visible
            if (isUnformattedActiveSlide && slideFormatManager.isSlideSetFormatted(self.getSlideIdSet(activeSlideId))) {
                setActiveSlideId(activeSlideId, { forceUpdate: true });
            }
        }

        /**
         * Handler function that is triggered after the table formatting is done.
         * This can be used to update the height of the selection node after a new
         * row is inserted.
         */
        function afterTableFormattingHandler() {

            var // the selected table node
                drawingFrame = selection.getAnyTextFrameDrawing();

            if (isTableDrawingFrame(drawingFrame)) { updateSelectionDrawingSize(drawingFrame); }
        }

        /**
         * Handler function that is triggered in ODF format, if the height of a drawing frame
         * has changed. This is required, because the ODF filter needs to be informed about a
         * modified table height.
         *
         * @returns {(event: jQuery.Event, drawingFrame: jQuery|Node) => void}
         *  The debounced callback function with the following parameters:
         *  - drawingFrame: The (jQuerified) drawing node, whose height has changed.
         */
        function createDebouncedDrawingHeightUpdateHandler() {

            var // the modified table drawing node
                drawingFrame = null;

            // direct callback: saving drawing
            function registerDrawing(drawing) {
                if (app.isEditable() && isTableDrawingFrame(drawing) && getDomNode(drawing).parentNode) {

                    // for grouped tables it can happen, that the drawing is no longer in the DOM (62716).
                    if (isGroupedDrawingFrame(drawing) && !self.getSlideIdForNode(drawing)) { return; }

                    drawingFrame = drawing;

                    // setting root element attribute for automated tests
                    app.setRootAttribute("data-resize-drawing-height", true);
                }
            }

            // deferred callback: called once
            function drawingHeightUpdateHandler() {

                var // the current target
                    target = null,
                    // the logical position of the table drawing
                    pos = null,
                    // the attributes operation to inform filter about drawing height
                    operation = null;

                // deleting root element attribute for automated tests
                app.setRootAttribute("data-resize-drawing-height", null);

                if (drawingFrame) {

                    // Check, that the drawing was not removed from the DOM in the meantime (DOCS-2233)
                    if (!getDomNode(drawingFrame).parentNode) { return; }

                    target = self.getActiveTarget();
                    pos = getOxoPosition(self.getRootNode(self.getActiveSlideId()), drawingFrame);

                    operation = {
                        name: Op.SET_ATTRIBUTES,
                        noUndo: true,
                        start: _.copy(pos),
                        attrs: { drawing: { height: convertLengthToHmm($(drawingFrame).height(), 'px') } },
                        target: target ? target : self.getOperationTargetBlocker()
                    };

                    self.applyOperations(operation);
                }

                drawingFrame = null; // reset
            }

            // create and return the deferred implTableChanged() method
            return self.debounce(registerDrawing, drawingHeightUpdateHandler, { delay: 200 });
        }

        function collectParentDependentSlideData(collector, slidId/*, idx, list*/) {
            if (collector.isParentSlideId(slidId)) {
                var
                    dependencyMap,
                    lookupValue,

                    slideIdList = self.getAllSlideIDsWithSpecificTargetParent(slidId);

                if (slideIdList.length > 0) {

                    dependencyMap = collector.dependencyMap;
                    lookupValue   = dependencyMap[slidId];

                    if (!lookupValue) {
                        lookupValue = dependencyMap[slidId] = {};
                    }
                    lookupValue['is' + collector.parentKey] = true;       // e.g. 'isMaster', 'isLayout'

                    slideIdList.forEach(function (slidId) {

                        lookupValue = dependencyMap[slidId];
                        if (!lookupValue) {
                            lookupValue = dependencyMap[slidId] = {};
                        }
                        lookupValue['from' + collector.parentKey] = true; // e.g. 'fromMaster', 'fromLayout'
                    });
                    collector.slideIdList   = collector.slideIdList.concat(slideIdList);
                    collector.isCallUnique  = true;
                }
            }
            return collector;
        }

        function branchUpdateSpecificSlideIdLists(collector, slidId/*, idx, list*/) {
            var
                dependencyMap = collector.dependencyMap;

            if (self.isStandardSlideId(slidId)) {
                if (slidId in dependencyMap) {

                    collector.standardDependend.push(slidId);
                } else {
                    collector.standardIndependend.push(slidId);
                }
            } else {
                if (slidId in dependencyMap) {

                    collector.nonStandardDependend.push(slidId);
                } else {
                    collector.nonStandardIndependend.push(slidId);
                }
            }
            return collector;
        }

        /**
         * @param {Array} modifiedSlideIdList
         * @param {boolean} onlyDocumentSlides
         * @returns {{standardDependend: Array, standardIndependend: Array, nonStandardDependend: Array, nonStandardIndependend: Array}}
         */
        function createPreviewUpdateRelatedSlideIdListData(modifiedSlideIdList, onlyDocumentSlides) {
            var
                slideIdListData = {
                    standardDependend:      [],
                    standardIndependend:    [],
                    nonStandardDependend:   [],
                    nonStandardIndependend: []
                };

            if (modifiedSlideIdList.length > 0) {
                if (onlyDocumentSlides) {

                    slideIdListData.standardIndependend = Array.from(modifiedSlideIdList);

                } else {

                    // layout or master slides were modified -> update of dependent slides is also required.
                    // Info: In layout view, document slides do not need to be updated. This is done, when
                    //       when switching from layout view to document view.
                    //       -> but a remote client might need an update of the slides in the 'normal' view.
                    var
                        dependentSlideData = modifiedSlideIdList.reduce(collectParentDependentSlideData, {
                            slideIdList:      [],
                            isCallUnique:     false,
                            dependencyMap:    {},
                            isParentSlideId:  self.isMasterSlideId,
                            parentKey:        'Master'
                        });

                    modifiedSlideIdList = modifiedSlideIdList.concat(dependentSlideData.slideIdList);

                    if (dependentSlideData.isCallUnique) {
                        modifiedSlideIdList = _.unique(modifiedSlideIdList);
                    }

                    dependentSlideData = modifiedSlideIdList.reduce(collectParentDependentSlideData, {
                        slideIdList:      [],
                        isCallUnique:     false,
                        dependencyMap:    dependentSlideData.dependencyMap,
                        isParentSlideId:  self.isLayoutSlideId,
                        parentKey:        'Layout'
                    });

                    modifiedSlideIdList = modifiedSlideIdList.concat(dependentSlideData.slideIdList);

                    if (dependentSlideData.isCallUnique) {
                        modifiedSlideIdList = _.unique(modifiedSlideIdList);
                    }

                    slideIdListData = modifiedSlideIdList.reduce(branchUpdateSpecificSlideIdLists, {
                        standardDependend:      [],
                        standardIndependend:    [],
                        nonStandardDependend:   [],
                        nonStandardIndependend: [],
                        dependencyMap:          dependentSlideData.dependencyMap
                    });
                    //delete slideIdListData.dependencyMap;
                }
            }
            return slideIdListData;
        }

        /**
         * Before an operation can be applied it is necessary, that the slide is
         * attached to the DOM. This process is especially important for remote
         * operations or undo operations. In both cases the active slide is often
         * not affected.
         *
         * @param {Object[]} operations
         *  An array with operations.
         */
        function operationBeforeHandler(operations) {

            // collector for the IDs of the slides
            var allSlides = operations.length > 1 ? {} : null;
            // whether at least one operation changes the slide positions
            var containsSlidePositionOperation = false;

            // attach slide to DOM
            // collect all slide IDs -> they will not be removed within the next 10 seconds

            _.each(operations, function (operation) {
                var slideId = null;
                if (!operation.target) { // layout and master slides are always in the DOM
                    slideId = getSlideIdFromOperation(operation);
                    if (slideId) { // slideId is not defined for new or duplicated slides
                        self.appendSlideToDom(slideId);
                        if (allSlides) { allSlides[slideId] = 1; } // collecting slide IDs
                        if (isSlidePositionOperation(operation)) { containsSlidePositionOperation = true; }
                    }
                }
            });

            // if at least operation modifies the slide positions and more than one slide is affected by the
            // operations, it might happen, that not the correct slides are attached to the DOM (DOCS-2033).
            if (containsSlidePositionOperation && _.keys(allSlides).length > 1) { self.appendAllSlidesToDom(); }
        }

        /**
         * A helper function to create the debounced handler for 'operations:after' event.
         * It contains a direct callback that collects modified slides and an asynchronous
         * callback that can be used to update the content of the slide pane.
         */
        function getOperationAfterHandler() {
            var
                // collecting all slide IDs for those operations that had attribute 'followMasterShapes'
                allFollowMasterShapeIDs = {},

                // whether only document slides were modified, no layout or master slides
                onlyDocumentSlides = true,

                // a data object that will be written as soon as a document attribute operation takes place
                // that also features a `page` attribute and its `orientation` key.
                slideRatioChangeData = null,

                // a collector for changes on master slide fields in ODP format
                odfSlideCollector = [];

            // This direct callback is triggered after every operation and therefore
            // very performance critical. It is used to collect the IDs of the modified
            // slides.
            // First parameter: The 'operations:after' event.
            // Second parameter: An array of operations.
            function registerSlide(operations) {

                if (operations && operations.length > 0) {

                    _.each(operations, function (op) {

                        var // the ID of the slide modified by the specified operation
                            slideID = getSlideIdFromOperation(op),
                            // whether the specified slide is a layout or master slide
                            isLayoutOrMasterSlide = self.isLayoutOrMasterId(slideID);

                        if (slideID && !_.contains(allModifiedSlideIDs, slideID)) { allModifiedSlideIDs.push(slideID); }

                        // check, whether this is a master or layout slide
                        if (isLayoutOrMasterSlide) { onlyDocumentSlides = false; }

                        // special handling for 'followMasterShapes'
                        // -> collecting all attribute values, if this property was switched
                        if (op.name === Op.SET_ATTRIBUTES) {
                            if (op.attrs && op.attrs.slide) {
                                if (op.attrs.slide.followMasterShapes === null) {

                                    allFollowMasterShapeIDs[slideID] = true;

                                } else if (_.isBoolean(op.attrs.slide.followMasterShapes)) {

                                    allFollowMasterShapeIDs[slideID] = op.attrs.slide.followMasterShapes;

                                } else if (app.isODF() && self.isImportFinished()) {
                                    if (op.attrs.slide) {
                                        collectODFMasterSlideUpdates(slideID, op.attrs.slide, odfSlideCollector);
                                    }

                                }
                            } else if (app.isODF() && self.isImportFinished() && isLayoutOrMasterSlide && op.attrs.drawing && op.start.length === 2) {
                                checkODFFooterUpdate(slideID, op.start, odfSlideCollector); // a footer drawing was moved or resized on ODF master slide -> informing the side pane
                            }
                        } else if (op.name === Op.CHANGE_CONFIG) {
                            if (op.attrs && op.attrs.page && (_.isNumber(op.attrs.page.width) || _.isNumber(op.attrs.page.height))) {
                                var width = op.attrs.page.width;
                                var height = op.attrs.page.height;
                                if (!_.isNumber(width)) {
                                    width = self.pageStyles.getElementAttributes(self.getNode()).page.width;
                                }
                                if (!_.isNumber(height)) {
                                    height = self.pageStyles.getElementAttributes(self.getNode()).page.height;
                                }
                                var ratio = width / height;
                                if (Number.isFinite(ratio)) {
                                    slideRatioChangeData = {
                                        slideRatio: ratio
                                    };
                                }
                            }
                        } else if (op.name === Op.LAYOUT_CHANGE) {

                            allFollowMasterShapeIDs[slideID] = true;

                        } else if (app.isODF() && self.isImportFinished()) {

                            if (isLayoutOrMasterSlide && (op.name === Op.DELETE || op.name === Op.INSERT_DRAWING) && op.start.length === 2) {
                                checkODFFooterUpdate(slideID, op.start, odfSlideCollector); // a footer drawing was inserted or deleted on ODF master slide -> informing the side pane
                            } else if (op.attrs && op.attrs.slide) {
                                // collecting slide attributes also for insert slide attributes
                                collectODFMasterSlideUpdates(slideID, op.attrs.slide, odfSlideCollector);
                            }
                        }

                        // registering changes on the master slide (ODP only)
                        if (app.isODF() && self.isImportFinished() && isLayoutOrMasterSlide && !masterModified) { masterModified = true; }
                    });
                }
            }

            // this indirect callback might update a large number of modified drawings
            // and is therefore also very performance critical.
            function updateSlidePreview() {
                var
                    slideIdListData = createPreviewUpdateRelatedSlideIdListData(allModifiedSlideIDs, onlyDocumentSlides),
                    followMasterMap = { ...allFollowMasterShapeIDs };

                // resetting the containers
                allModifiedSlideIDs     = [];
                allFollowMasterShapeIDs = {};

                // resetting also the boolean that registers, if only document slide were modified (no master or layout slides).
                onlyDocumentSlides      = true;

                // If 'onlyDocumentSlides' is set to true, only document slide need to be cloned, no layout or master slide!

                // TODO in listener: Iterating sliced over all slides, whose IDs are collected in 'allModifiedSlideIDs'.
                //                   self.iterateArraySliced(allModifiedSlideIDs, ...) {}
                // TODO in listener: Check, if a specified ID is again in the global collector using the function
                //                   'this.isModifiedSlideID(id)'. In this case the update of the slide in the slide
                //                   pane can be skipped.

                self.trigger('slidestate:update', slideIdListData, followMasterMap);

                if (slideRatioChangeData) {
                    self.trigger('slidesize:change', slideRatioChangeData);

                    slideRatioChangeData = null;
                }

                if (odfSlideCollector.length > 0) {
                    self.trigger('masterfield:update', odfSlideCollector); // only required in ODF applications
                    odfSlideCollector = [];
                }

            }

            // create and return the deferred method
            return self.debounce(registerSlide, updateSlidePreview, { delay: 500 });
        }

        /**
         * Helper function to inform the slide pane about changes, that were NOT
         * handled by 'operations:after' event. This are the insertion of template
         * placeholder texts in layout slides or the load of a (large) image.
         *
         * @returns {Function}
         *  The debounced function for triggering 'slidestate:update' debounced without
         *  'operations:after' event. The usage of this function should be the exception,
         *  the regular update happens via 'operations:after'.
         */
        function getDebouncedUpdateSlideStateHandler() {

            var
                // all slide nodes that need to be updated
                allSlideIDs         = [],
                onlyDocumentSlides  = true,

                isStandardSlideId   = self.isStandardSlideId;

            // direct callback: saving slide IDs
            function registerSlidesForSlidePaneUpdate(slideIDs) {
                if (_.isString(slideIDs)) {

                    allSlideIDs.push(slideIDs);

                    onlyDocumentSlides = (onlyDocumentSlides && isStandardSlideId(slideIDs));
                } else {
                    onlyDocumentSlides = (onlyDocumentSlides && slideIDs.every(isStandardSlideId));

                    allSlideIDs = allSlideIDs.concat(slideIDs);
                }
            }

            // deferred callback: called once, after current script ends
            function triggerSlidePaneUpdate() {
                var
                    slideIdListData = createPreviewUpdateRelatedSlideIdListData(allSlideIDs, onlyDocumentSlides);

                allSlideIDs         = [];   // - reset collector.
                onlyDocumentSlides  = true; // - reset boolean flag.

                self.trigger('slidestate:update', slideIdListData);
            }

            // create and return the deferred method
            return self.debounce(registerSlidesForSlidePaneUpdate, triggerSlidePaneUpdate, { delay: 1000 });
        }

        /**
         * Helper function to set the selection after loading or reloading a document.
         * The selection must be specified in the launch options.
         * The correct slide is already specified in the process of 'updateDocumentFormatting'.
         */
        function setSelectionAfterLoad() {
            var selectionOptions = app.getLaunchOption('selection');

            if (selectionOptions) {
                setActiveView(!!selectionOptions.target); // activating the master slide view, if required
                if (selectionOptions.target) { setActiveSlideId(selectionOptions.target); } // document slides are already set

                if (selectionOptions.isMultiSelection) {
                    selection.setMultiDrawingSelectionByPosition(selectionOptions.allPositions);
                } else {
                    var startPos = selectionOptions.start;
                    var endPos = selectionOptions.end ? selectionOptions.end : _.copy(startPos);
                    selection.setTextSelection(startPos, endPos);
                }
            }
        }

        /**
         * Appending the stringified drawing counter to the specified drawing user id.
         *
         * @param {String} prefix
         *  The prefix to which the drawing counter is appended.
         *
         * @returns {String}
         *  The full drawing id containing the user id and the drawing counter.
         */
        function appendDrawingCounter(prefix) {
            userDrawingCounter++;
            return prefix + String(userDrawingCounter).padStart(5, '0');
        }

        /**
         * Getting the index of a neighbouring visible slide in the document view.
         *
         * @param {number} index
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.next=true]
         *   Whether the next (default) or the previous slide shall be searched.
         */
        function getNeighbourIndexOfVisibleSlide(index, options) {

            // whether the next slide shall be made visible
            var next = getBooleanOption(options, 'next', true);
            // whether a visible slide exists
            var found = false;
            // the ID of the checked slide
            var slideId = null;

            if (next) {
                while (!found && index < slideOrder.length) {
                    index++;
                    slideId = self.getSlideIdByPosition(index);
                    if (!self.isHiddenSlide(slideId)) { found = true; }
                }
            } else {
                while (!found && index > 0) {
                    index--;
                    slideId = self.getSlideIdByPosition(index);
                    if (!self.isHiddenSlide(slideId)) { found = true; }
                }
            }

            return found ? index : -1;
        }

        /**
         * Getting the ID of the first visible slide in the document view.
         */
        function getIdOfFirstVisibleSlide() {
            var index = getNeighbourIndexOfVisibleSlide(-1);
            return (index > -1) ? self.getSlideIdByPosition(index) : null;
        }

        /**
         * Getting the ID of the last visible slide in the document view.
         */
        function getIdOfLastVisibleSlide() {
            var index = getNeighbourIndexOfVisibleSlide(slideOrder.length, { next: false });
            return (index > -1) ? self.getSlideIdByPosition(index) : null;
        }

        /**
         * Getting an ID consisting of digits as string. An exclude list can be
         * specified.
         *
         * @returns {string|null}
         *  A digit string. Or null, if no valid value can be found and the option 'forceNull'
         *  is true.
         */
        function generateDigitId() {

            // the new id
            let id = null;

            for (let counter = 0; counter < 100000; counter += 1) {
                id = String(math.randomInt(1000, 9999));
                if (!drawingUserIdCollection.has(id)) { break; }
            }

            return id;
        }

        // protected methods --------------------------------------------------

        /**
         * Returns an arbitrary but unambiguous JSON representation of this
         * document model.
         *
         * @returns {object}
         *  An unambiguous JSON representation of this document model.
         */
        this.serializeToJSON = function () {
            self.appendAllSlidesToDom();
            return DOM.serializeElementToJSON(this.getNode());
        };

        // public methods -----------------------------------------------------

        /**
         * Returns the current selection state of the document.
         *
         * @returns {object}
         *  The current selection state of this document.
         */
        this.getSelectionState = function () {
            var selection = self.getSelection();
            return {
                start: selection.getStartPosition(),
                end: selection.getEndPosition(),
                target: selection.getRootNodeTarget(),
                type: selection.getSelectionType(),
                dir: selection.getDirection(),
                isMultiSelection: selection.isMultiSelection(),
                isSlideSelection: selection.isTopLevelTextCursor(),
                allPositions: selection.getArrayOfLogicalPositions()
            };
        };

        /**
         * Changes the selection state of the document.
         *
         * @param {object} _selectionState
         *  The new selection state to be set at this document.
         */
        this.setSelectionState = function (_selectionState) {
            // restoring the selection state needs to be done in undo-redo-after handler
        };

        /**
         * Public helper function: Adding a slide in the model.
         * See description at private function 'addIntoSlideModel'.
         */
        this.addIntoSlideModel = function (slide, id, options) {
            return addIntoSlideModel(slide, id, options);
        };

        /**
         * Public helper function: Removing a slide from the model.
         * See description at private function 'removeFromSlideModel'.
         */
        this.removeFromSlideModel = function (id) {
            return removeFromSlideModel(id);
        };

        /**
         * Public helper function: Moving a slide inside the model.
         * See description at private function 'moveSlideIdInModel'.
         */
        this.moveSlideIdInModel = function (start, end, isStandardSlide) {
            return moveSlideIdInModel(start, end, isStandardSlide);
        };

        /**
         * Public helper function: Changing the layout id for a specified
         * document slide id.
         * See description at private function 'changeLayoutIdInModel'.
         */
        this.changeLayoutIdInModel = function (id, layoutId) {
            return changeLayoutIdInModel(id, layoutId);
        };

        /**
         * Public helper function: Changing the master id for a specified
         * layout slide id.
         * See description at private function 'changeMasterIdInModel'.
         */
        this.changeMasterIdInModel = function (id, layoutId) {
            return changeMasterIdInModel(id, layoutId);
        };

        /**
         * Public helper function: Provides a unique ID for the standard slides.
         * See description at private function 'getNextSlideID'.
         */
        this.getNextSlideID = function () {
            return getNextSlideID();
        };

        /**
         * Public helper function: Provides a unique ID for the custom layout slides.
         * See description at private function 'getNextCustomLayoutId'.
         */
        this.getNextCustomLayoutId = function () {
            return getNextCustomLayoutId();
        };

        /**
         * Getting the ID of that standard slide that was active, when the switch to master/layout view
         * was done. If it is not specified, it contains the empty string.
         *
         * @returns {String}
         * The ID of that standard slide that was active, when the switch to master/layout view
         * was done. If it is not specified, it contains the empty string.
         */
        this.getLastActiveStandardSlideId = function () {
            return lastActiveStandardSlideId;
        };

        /**
         * Public helper function: Receiving the layer node for the layout slides.
         * If it does not exist, it is created.
         * See description at private function 'getOrCreateLayoutSlideLayerNode'.
         */
        this.getOrCreateLayoutSlideLayerNode = function () {
            return getOrCreateLayoutSlideLayerNode();
        };

        /**
         * Public helper function: Receiving the layer node for the master slides.
         * If it does not exist, it is created.
         * See description at private function 'getOrCreateMasterSlideLayerNode'.
         */
        this.getOrCreateMasterSlideLayerNode = function () {
            return getOrCreateMasterSlideLayerNode();
        };

        /**
         * Receiving the layer node for the layout slides, if it exists.
         *
         * @returns {jQuery|Null}
         *  The layout slide layer node, or null if it does not exist.
         */
        this.getLayoutSlideLayerNode = function () {
            return layoutSlideLayerNode;
        };

        /**
         * Receiving the layer node for the master slides, if it exists.
         *
         * @returns {jQuery|Null}
         *  The master slide layer node, or null if it does not exist.
         */
        this.getMasterSlideLayerNode = function () {
            return masterSlideLayerNode;
        };

        /**
         * Public helper function: Setting the id of the active slide.
         * See description at private function 'setActiveSlideId'.
         */
        this.setActiveSlideId = function (id, options) {
            return setActiveSlideId(id, options);
        };

        /**
         * Public helper function: Getting the last top level position inside the slide,
         * that has the current selection.
         * See description at private function 'getNextAvailablePositionInActiveSlide'.
         */
        this.getNextAvailablePositionInActiveSlide = function () {
            return getNextAvailablePositionInActiveSlide();
        };

        /**
         * Public helper function: Getting the last top level position inside the slide,
         * that has the given id.
         * See description at private function 'getNextAvailablePositionInSlide'.
         */
        this.getNextAvailablePositionInSlide = function (id) {
            return getNextAvailablePositionInSlide(id);
        };

        /**
         * Public helper function: Triggering a full update of a slide. This is for
         * example required after changing the layout of a slide.
         * 'implSlideChanged' is a debounced function, with a direct callback that
         * registers the slides for update.
         *
         * @param {Node|jQuery} slide
         *  The slide node that will be registered for a deferred update.
         */
        this.implSlideChanged = function (slide) {
            return implSlideChanged(slide);
        };

        /**
         * Getting the id of the first slide in the currently active view.
         *
         * @returns {String}
         *  The id of the first slide of the currently active view.
         */
        this.getIdOfFirstSlideOfActiveView = function () {
            return self.getIdOfSlideOfActiveViewAtIndex(0);
        };

        /**
         * Getting the id of the last slide in the currently active view.
         *
         * @returns {String}
         *  The id of the last slide of the currently active view.
         */
        this.getIdOfLastSlideOfActiveView = function () {
            return isMasterView ? _.last(masterSlideOrder) : _.last(slideOrder);
        };

        /**
         * Getting the id of the specified slide in the currently active view. The
         * specification is done by the index of the slide (which is its logical
         * position).
         *
         * @param {Number} index
         *  The zero-based index of the slide inside the currently active view.
         *
         * @returns {String}
         *  The id of the first slide of the currently active view.
         */
        this.getIdOfSlideOfActiveViewAtIndex = function (index) {
            return isMasterView ? masterSlideOrder[index] : slideOrder[index];
        };

        /**
         * Getting the key, that is used to mark standard slides.
         *
         * @returns {String}
         *  The key, that is used to mark standard slides.
         */
        this.getStandardSlideType = function () {
            return PresentationModel.TYPE_SLIDE;
        };

        /**
         * Getting the key, that is used to mark master slides.
         *
         * @returns {String}
         *  The key, that is used to mark master slides.
         */
        this.getMasterSlideType = function () {
            return PresentationModel.TYPE_MASTER_SLIDE;
        };

        /**
         * Getting the key, that is used to mark layout slides.
         *
         * @returns {String}
         *  The key, that is used to mark layout slides.
         */
        this.getLayoutSlideType = function () {
            return PresentationModel.TYPE_LAYOUT_SLIDE;
        };

        /**
         * Getting an object that contains the default drawing attributes
         * for new inserted drawings.
         *
         * @returns {Object}
         *  An object containing the default drawing attributes for new inserted
         *  drawings.
         */
        this.getInsertDrawingAttibutes = function () {
            return _.clone(PresentationModel.INSERT_DRAWING_ATTRIBUTES);
        };

        /**
         * Getting the slide format manager of the model.
         *
         * @returns {Object}
         *  The slide format manager of the model.
         */
        this.getSlideFormatManager = function () {
            return slideFormatManager;
        };

        /**
         * Getting the slide visibility manager of the model.
         *
         * @returns {Object}
         *  The slide visibility manager of the model.
         */
        this.getSlideVisibilityManager = function () {
            return slideVisibilityManager;
        };

        /**
         * Getting the presentation mode manager from the model.
         *
         * @returns {Object}
         *  The presentation mode manager of the model.
         */
        this.getPresentationModeManager = function () {
            return presentationModeManager;
        };

        /**
         * Convenience function for attaching a slide to the DOM. This function
         * uses the slide visibility manager that handles this request.
         *
         * @param {String} slideId
         *  The ID of the slide that inserted into the DOM.
         */
        this.appendSlideToDom = function (slideId) {
            self.getSlideVisibilityManager().appendSlideToDom(slideId);
        };

        /**
         * Convenience function for attaching all detached document slides
         * to the DOM. This function uses the slide visibility manager that
         * handles this request.
         */
        this.appendAllSlidesToDom = function () {
            self.getSlideVisibilityManager().appendAllSlidesToDom();
        };

        /**
         * Getting the id of the currently activated slide.
         *
         * @returns {String}
         *  The id of the currently active slide.
         */
        this.getActiveSlideId = function () {
            return activeSlideId;
        };

        /**
         * Checks whether a specified slide id is the id of the active slide.
         *
         * @param {String} id
         *  The id to be checked.
         *
         * @returns {Boolean}
         *  Whether a specified slide id is the id of the active slide.
         */
        this.isActiveSlideId = function (id) {
            return id && (id === activeSlideId);
        };

        /**
         * Getting the jQuerified active slide node.
         *
         * @returns {jQuery|null}
         *  The active slide node. Or null, if no active slide node exists.
         */
        this.getActiveSlide = function () {
            return self.getSlideById(self.getActiveSlideId());
        };

        /**
         * Getting the logical position of the active slide
         *
         * @returns {Array<Number>}
         *  The logical position of the active slide in its content container.
         */
        this.getActiveSlidePosition = function () {
            return isMasterView ? [0] : [getActiveSlideIndex()];
        };

        /**
         * Getting the index of the active slide in its current view.
         *
         * @returns {Number}
         *  The index of the active slide in its current view.
         */
        this.getActiveSlideIndex = function () {
            return getActiveSlideIndex();
        };

        /**
         * Getting the index of the layout slide relative to it's master slide.
         *
         * @param {String} masterId
         *  Id of the master slide to whom layout slide belongs.
         * @param {String} layoutId
         *  Id of the layout slide for which we want to get index.
         * @returns {Number|Null}
         *  The index of the layout slide relative to it's master slide.
         */
        this.getRelativeLayoutIndex = function (masterId, layoutId) {
            var masterIndex = _.indexOf(masterSlideOrder, masterId);
            var layoutIndex = _.indexOf(masterSlideOrder, layoutId);

            return (masterIndex > -1 && layoutIndex > -1) ? layoutIndex - masterIndex : null;
        };

        /**
         * Check, whether the master and layout slides are visible.
         *
         * @returns {Boolean}
         *  Whether the master and layout slides are visible.
         */
        this.isMasterView = function () {
            return isMasterView;
        };

        /**
         * Receiving the type of the slide for a specified id.
         *
         * @param {String} id
         *  The slide id.
         *
         * @returns {String|Null}
         *  The type of the slide. Or null, if no type could be determined.
         */
        this.getSlideType = function (id) {
            return idTypeConnection[id] || null;
        };

        /**
         * Receiving a complete copy of the id-type-connection model object.
         *
         * @returns {Object}
         *  A copy of the id-type-connection model object
         */
        this.getIdTypeConnectionModel = function () {
            return _.clone(idTypeConnection);
        };

        /**
         * Check, whether the specified id string is a valid id of a standard slide.
         *
         * @param {String} id
         *  The id string.
         *
         * @returns {Boolean}
         *  Whether the specified id string is a valid id for a standard slide.
         */
        this.isStandardSlideId = function (id) {
            return allStandardSlides[id] !== undefined;
        };

        /**
         * Check, whether the specified id string is a valid id of a layout slide.
         *
         * @param {String} id
         *  The id string.
         *
         * @returns {Boolean}
         *  Whether the specified id string is a valid id for a layout slide.
         */
        this.isLayoutSlideId = function (id) {
            return allLayoutSlides[id] !== undefined;
        };

        /**
         * Check, whether the specified id string is a valid id for a master slide.
         *
         * @param {String} id
         *  The id string.
         *
         * @returns {Boolean}
         *  Whether the specified id string is a valid id for a master slide.
         */
        this.isMasterSlideId = function (id) {
            return allMasterSlides[id] !== undefined;
        };

        /**
         * Check, whether the specified id string is a valid id of a layout or master slide.
         *
         * @param {String} id
         *  The id string.
         *
         * @returns {Boolean}
         *  Whether the specified id string is a valid id for a layout or master slide node.
         */
        this.isLayoutOrMasterId = function (id) {
            return self.isLayoutSlideId(id) || self.isMasterSlideId(id);
        };

        /**
         * Provides the layout slide node or master slide node in the layout slide layer or master slide
         * layer corresponding to the specified target string.
         *
         * @param {String} id
         *  The slide id.
         *
         * @returns {jQuery|null}
         *  The slide node inside the layout or master slide layer node. Or null, if no node with the
         *  specified target exists.
         */
        this.getLayoutOrMasterSlide = function (id) {
            return getLayoutSlide(id) || getMasterSlide(id) || null;
        };

        /**
         * Getting the ID of the first slide in the standard view.
         *
         * @returns {String|Null}
         *  The ID of the first slide in the standard view. Or null, if no slide exists.
         */
        this.getFirstStandardSlideId = function () {
            return _.first(slideOrder) || null;
        };

        /**
         * Convenience function at the model, to ask the presentation mode manager,
         * whether the presentation mode is currently active.
         *
         * @returns {Boolean}
         *  Whether the presentation mode is currently active.
         */
        this.isPresentationMode = function () {
            return presentationModeManager.isPresentationMode();
        };

        /**
         * Whether it is fine, that the document was loaded in read-only mode.
         *
         * @returns {Boolean}
         *  Whether read-only mode is valid after loading the document.
         */
        this.isValidReadOnlyMode = function () {
            return self.isPresentationMode();
        };

        /**
         * Convenience function at the model, to ask the presentation mode manager,
         * whether the presentation mode is currently active and the full screen mode
         * is used.
         *
         * @returns {Boolean}
         *  Whether the presentation mode is currently active in full screen mode.
         */
        this.isFullscreenPresentationMode = function () {
            return presentationModeManager.isPresentationMode() && presentationModeManager.useFullscreenPresentation();
        };

        /**
         * Recieving the slide set consisting of document slide id, layout slide id and master slide
         * id for a specified id.
         * If the id is not specified, the active slide id is used.
         *
         * @param {String} [id]
         *  The slide id. If not specified, the id of the active slide is used.
         *
         * @returns {Object}
         *  An object containing the properties 'id', 'layoutId' and 'masterId', that represent the
         *  group of IDs for one specified id.
         */
        this.getSlideIdSet = function (id) {

            var // the IDs for document slide, layout slide and master slide
                slideId = null, layoutId = null, masterId = null;

            if (!id) { id = activeSlideId; }  // if not specified, the active slide is used

            // setting all IDs that need to be activated
            if (self.isStandardSlideId(id)) {
                slideId = id;
                layoutId = self.getLayoutSlideId(slideId);
                masterId = self.getMasterSlideId(layoutId);
            } else if (self.isLayoutSlideId(id)) {
                layoutId = id;
                masterId = self.getMasterSlideId(layoutId);
            } else if (self.isMasterSlideId(id)) {
                masterId = id;
            }

            return { id: slideId, layoutId, masterId };
        };

        /**
         * Recieving the slide set consisting of document slide, layout slide and master slide for
         * a specified id.
         * If the id is not specified, the active slide id is used.
         *
         * @param {String} [id]
         *  The slide id. If not specified, the id of the active slide is used.
         *
         * @returns {Object}
         *  An object containing the properties 'slide', 'layoutSlide' and 'masterSlide'. The values
         *  are the jQuerified slides (or null, if the slide is not set or cannot be determined).
         */
        this.getSlideSet = function (id) {

            var // the grouped document slide, layout slide and master slide
                slide = null, layoutSlide = null, masterSlide = null,
                // the ID set for the specified slide id
                slideIdSet = null;

            if (!id) { id = activeSlideId; }  // if not specified, the active slide is used

            slideIdSet = self.getSlideIdSet(id);

            if (slideIdSet.id) { slide =  self.getSlideById(slideIdSet.id); }
            if (slideIdSet.layoutId) { layoutSlide =  self.getSlideById(slideIdSet.layoutId); }
            if (slideIdSet.masterId) { masterSlide =  self.getSlideById(slideIdSet.masterId); }

            return { slide, layoutSlide, masterSlide };
        };

        /**
         * Getting the target inheritance chain for a slide specified by its ID. This chain
         * is an array containing the IDs of three slides at maximum. This IDs are in the
         * order the ID of the document slide, the layout slide and the master slide.
         * Because the document slides have no 'official' ID, that can be used by other
         * operations, the ID of a document slide is always the empty string, if not
         * specified via 'all' option.
         * This target inheritance chain is for example used by the theme collection. A theme
         * can be assigned to a target. If it is not assigned to a target, it is automatically
         * assigned to all document slides.
         *
         * @param {string} id
         *  The slide id.
         *
         * @param {boolean} [all=false]
         *  Optional parameter: Whether the target chain shall contain the id of the document
         *  slide (55556).
         *
         * @returns {string[]}
         *  An array cointaining the target inheritance chain for document slide, layout slide
         *  and master slide. For all document slides, the target is the empty string, because
         *  the document slides do not have an 'official' ID, that can be used as target for
         *  other operations.
         */
        this.getTargetChain = function (id, all) {

            var // the target array
                targetContainer = [],
                // the set of slide IDs
                slideIdSet = self.getSlideIdSet(id);

            if (slideIdSet.id) {
                if (all) {
                    targetContainer.push(slideIdSet.id);
                } else {
                    targetContainer.push('');
                }
            } // the target for document slides is the empty string!
            if (slideIdSet.layoutId) { targetContainer.push(slideIdSet.layoutId); }
            if (slideIdSet.masterId) { targetContainer.push(slideIdSet.masterId); }

            return targetContainer;
        };

        /**
         * Getting the target inheritance chain for a node, that is located inside a slide or
         * a slide node itself. This chain is an array containing the IDs of three slides at
         * maximum. This IDs are in the order the ID of the document slide, the layout slide
         * and the master slide.
         * Because the document slides have no 'official' ID, that can be used by other
         * operations, the ID of a document slide is always the empty string.
         * This target inheritance chain is for example used by the theme collection. A theme
         * can be assigned to a target. If it is not assigned to a target, it is automatically
         * assigned to all document slides.
         *
         * @param {String} node
         *  The node that is located in a slide (or is the slide itself).
         *
         * @param {Boolean} [all=false]
         *  Optional parameter: Whether the target chain shall contain the id of the document
         *  slide (55556).
         *
         * @returns {String[]}
         *  An array cointaining the target inheritance chain for document slide, layout slide
         *  and master slide. For all document slides, the target is the empty string, because
         *  the document slides do not have an 'official' ID, that can be used as target for
         *  other operations.
         */
        this.getTargetChainForNode = function (node, all) {
            return self.getTargetChain(self.getSlideIdForNode(node), all);
        };

        /**
         * Returns the theme target chain for the passed DOM element node, or
         * the theme target chain for the active slide.
         *
         * @param {NodeOrJQuery} [element]
         *  The DOM element to resolve the theme target for. If omitted, the
         *  active theme target will be returned.
         *
         * @returns {string[]}
         *  The theme targets for the specified element, or the theme target
         *  for the active slide.
         */
        /*public override*/ this.getThemeTargets = function (element) {
            // bug 55556: chain has to include the ID of the document slide, because themes can also be assigned to this type of slides.
            return element ? this.getTargetChainForNode(element, true) : activeTargetChain;
        };

        /**
         * Getting the slide ID for a specified node, that is located inside a slide or is
         * a slide node itself.
         *
         * @param {HTMLElement|jQuery} node
         *  The node that is located in a slide (or is the slide itself).
         *
         * @returns {String|Null}
         *  The ID of the slide that contains the specified node. Or null, if it cannot be
         *  determined.
         */
        this.getSlideIdForNode = function (node) {

            var // the slide node that contains the specified drawing
                slide = DOM.isSlideNode(node) ? $(node) : $(node).closest(DOM.SLIDE_SELECTOR),
                // the slide ID of the affected slide
                slideId = (slide && slide.length > 0) ? self.getSlideId(slide) : null;

            return slideId;
        };

        /**
         * Provides a jQuerified slide node specified by its id. Or null, if no slide is registered with
         * this id.
         *
         * @param {String} id
         *  The slide id.
         *
         * @returns {jQuery|null}
         *  The slide node specified by the id. Or null, if no node with the specified id exists.
         */
        this.getSlideById = function (id) {
            return getStandardSlide(id) || getLayoutSlide(id) || getMasterSlide(id) || null;
        };

        /**
         * Receiving all slides of the currently active view. This can be the standard view or the
         * master/layout view.
         *
         * @returns {Object|null}
         *  An object containing the IDs as key and the jQuerified slides as value for all slides
         *  of the currently active view.
         */
        this.getAllSlidesOfActiveView = function () {
            return isMasterView ? { ...allLayoutSlides, ...allMasterSlides } : allStandardSlides;
        };

        /**
         * Receiving all slides from given group.
         *
         * @param {String} group
         *  Can be master, layout or standard group.
         * @returns {Object|null}
         *  An object containing the IDs as key and the jQuerified slides as value for all slides of one group.
         */
        this.getAllGroupSlides = function (group) {
            switch (group) {
                case 'master':
                    return _.clone(allMasterSlides);
                case 'layout':
                    return _.clone(allLayoutSlides);
                case 'standard':
                    return _.clone(allStandardSlides);
                default:
                    return null;
            }
        };

        /**
         * Receiving all slides from all views.
         *
         * @returns {Object|null}
         *  An object containing the IDs as key and the jQuerified slides as value for all slides.
         */
        this.getAllSlides = function () {
            return { ...allStandardSlides, ...allLayoutSlides, ...allMasterSlides };
        };

        /**
         * Check, whether the number of document slides is equal with an expected number of document slides.
         *
         * @param {Number} count
         *  The expected number of document slides.
         *
         * @returns {Boolean}
         *  Whether the number of document slides is the expected number.
         */
        this.isStandardSlideCount = function (count) {
            return _.keys(allStandardSlides).length === count;
        };

        /**
         * Receiving all slides selected in slidepane.
         *
         * @returns {Object|null}
         *  An object containing the IDs as key and the jQuerified slides as value for all slides.
         */
        this.getAllSelectedSlides = function () {
            var slidePaneSelection = self.getSlidePaneSelection();
            var collection = {};

            if (!slidePaneSelection || !slidePaneSelection.length) { // just a fallback if there is no slidepane selection, i.e. for tests
                collection[activeSlideId] = self.getSlideById(activeSlideId);
            } else {
                _.each(slidePaneSelection, function (slideIndex) {
                    var slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
                    var $slide = self.getSlideById(slideId);

                    collection[slideId] = $slide;
                });
            }

            return collection;
        };

        /**
         * Activating a slide with a specified id.
         *
         * @param {String} id
         *  The id of the slide, that shall be activated.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Number} [options.delayedVisibility=0]
         *   Whether the 'new' slide shall be made visible with a delay.
         *   This is sometimes required to avoid a flickering (DOCS-3084).
         *
         * @returns {Boolean}
         *  Whether the slide was changed.
         */
        this.changeToSlide = function (id, options) {

            var // whether the slide changed
                slideChange = false;

            if (id !== activeSlideId) { slideChange = setActiveSlideId(id, options); }

            return slideChange;
        };

        /**
         * Getter for the ID of the specified slide node.
         *
         * @param {Node|jQuery|Null} slideNode
         *  The DOM node to be checked. If this object is a jQuery collection, uses
         *  the first DOM node it contains. If missing or null, returns empty string.
         *
         * @returns {String}
         *  The ID of the specified slide. If this is no slide or the ID cannot be
         *  determined, an empty string is returned.
         */
        this.getSlideId = function (slideNode) {
            return DOM.isSlideNode(slideNode) ? DOM.getTargetContainerId(slideNode) : '';
        };

        /**
         * Check, whether the active slide view is empty.
         *
         * @returns {Boolean}
         *  Whether the active slide view is empty.
         */
        this.isEmptySlideView = function () {
            return (isMasterView ? masterSlideOrder.length === 0 : slideOrder.length === 0);
        };

        /**
         * Check, whether there are no document slides.
         *
         * @returns {Boolean}
         *  Whether there is no document slide.
         */
        this.isEmptyPresentation = function () {
            return slideOrder.length === 0;
        };

        /**
         * Check, whether there is only one slide in the active slide view.
         *
         * @returns {Boolean}
         *  Whether there is only one slide in the active slide view.
         */
        this.isOneSlideInActiveView = function () {
            return (isMasterView ? masterSlideOrder.length === 1 : slideOrder.length === 1);
        };

        /**
         * Check, if the active slide is the first slide inside the standard or the master/layout view.
         *
         * @returns {Boolean}
         *  Whether the active slide is the first slide inside the standard or the master/layout view.
         */
        this.isFirstSlideActive = function () {
            return (isMasterView ? _.first(masterSlideOrder) : _.first(slideOrder)) === activeSlideId;
        };

        /**
         * Check, if the active slide is the last slide inside the standard or the master/layout view.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether hidden slides shall be ignored. This is only supported in the document view, not
         *   in the master view.
         *
         * @returns {Boolean}
         *  Whether the active slide is the last slide inside the standard or the master/layout view.
         */
        this.isLastSlideActive = function (options) {

            // whether hidden slides shall be ignored
            var onlyVisible = !isMasterView && getBooleanOption(options, 'onlyVisible', false);
            // the slide id of the last visible slide
            var slideId = null;

            if (onlyVisible) {
                slideId = getIdOfLastVisibleSlide();
                return slideId === activeSlideId;
            } else {
                return (isMasterView ? _.last(masterSlideOrder) : _.last(slideOrder)) === activeSlideId;
            }
        };

        /**
         * Check, if the specified slide ID is the first inside its container.
         *
         * @returns {Boolean}
         *  Whether the specified slide is the first slide inside its container.
         */
        this.isFirstSlideInContainer = function (id) {
            return (self.isLayoutOrMasterId(id) ? _.first(masterSlideOrder) : _.first(slideOrder)) === id;
        };

        /**
         * Check, if the specified slide ID is the last inside its container.
         *
         * @returns {Boolean}
         *  Whether the specified slide is the last slide inside its container.
         */
        this.isLastSlideInContainer = function (id) {
            return (self.isLayoutOrMasterId(id) ? _.last(masterSlideOrder) : _.last(slideOrder)) === id;
        };

        /**
         * Check, whether a slide specified by its id, is the only slide in its view. This can be the view
         * for the standard slides or the view for the master/layout slides.
         *
         * @param {String} [id]
         *  The id of the slide. If not specified, the active slide id is used.
         *
         * @returns {Boolean}
         *  Whether the specified slide is the only slide in its view.
         */
        this.isOnlySlideInView = function (id) {

            var // whether the specified slide is the only slide in the view (standard view or master/layout view)
                isOnlySlide = false,
                // the id of the slide
                localId = id || activeSlideId;

            if (self.isStandardSlideId(localId)) {
                isOnlySlide = (slideOrder.length === 1);
            } else if (self.isLayoutOrMasterId(localId)) {
                isOnlySlide = (masterSlideOrder.length === 1);
            }

            return isOnlySlide;
        };

        /**
         * Getting the index of the specified slide in the current container. If
         * no id is specified, the active slide is used.
         *
         * @param {String} [id]
         *  The id of the slide. If not specified, the active slide id is used.
         *
         * @returns {Number}
         *  The index of the specified or active slide in its container. If the
         *  slide id could not be found in the array, '-1' is returned.
         */
        this.getSortedSlideIndexById = function (id) {

            var // the id of the slide
                localId = id || activeSlideId,
                // the container that contains the order of the slides
                orderContainer = self.isLayoutOrMasterId(localId) ? masterSlideOrder : slideOrder;

            return _.indexOf(orderContainer, localId);
        };

        /**
         * Receiving the ID of a document slide from its logical position or from
         * its index in the sorted slide order container. This function CANNOT be
         * used for layout or master slides.
         *
         * @param {Number|Number[]} pos
         *  The index or the logical position specifying a document slide.
         *
         * @returns {String|Null}
         *  The ID of the document slide. Or null, if it cannot be determined.
         */
        this.getSlideIdByPosition = function (pos) {

            var // the index of the slide in the sorted
                slideIndex = _.isArray(pos) ? _.first(pos) : pos;

            return (_.isNumber(slideIndex) && slideOrder[slideIndex]) ? slideOrder[slideIndex] : null;
        };

        /**
         * Getting the logical position of a slide specified by its ID. If the ID is not
         * valid, null is returned.
         *
         * @param {String} id
         *  The ID of the slide
         *
         * @returns {Number[]|Null}
         *  The logical position of the specified slide. Or null, if it cannot be determined.
         */
        this.getSlidePositionById = function (id) {
            return self.isStandardSlideId(id) ? [_.indexOf(slideOrder, id)] : (self.isLayoutOrMasterId(id) ? [0] : null);
        };

        /**
         * Getting the index of the document slide specified by its ID. If the ID is not
         * the ID of a document slide, 0 is returned.
         *
         * @param {String} id
         *  The ID of the document slide
         *
         * @returns {Number[]|Null}
         *  The index of the specified slide. Or 0, if it cannot be determined.
         */
        this.getDocumentSlideIndexById = function (id) {
            return self.isStandardSlideId(id) ? _.indexOf(slideOrder, id) : 0;
        };

        /**
         * Returns slide number for given slide id.
         * This function CANNOT be used for layout or master slides.
         *
         * @param {String} slideID
         *  The ID of the document slide
         *
         * @returns {Number}
         *  The number of the document slide. Or null, if it cannot be determined.
         */
        this.getSlideNumByID = function (slideID) {
            var index = _.indexOf(slideOrder, slideID);

            return index > -1 ? index + 1 : null;
        };

        /**
         * Whether the document contains more than one master slide.
         *
         * @returns {Boolean}
         *  Whether the document contains more than one master slide.
         */
        this.isMultiMasterSlideDocument = function () {
            return _.keys(allMasterSlides).length > 1;
        };

        /**
         * Collecting the position information for the master slides in the model container 'masterSlideOrder'.
         *
         * @returns {Object}
         *  An object that has as properties the IDs of the master slides and as values the index for each
         *  slide in the container 'masterSlideOrder'.
         *  Additionally a sorted array with all master slide indices is returned with the property 'order'.
         */
        this.getAllMasterSlideIndices = function () {

            var // an array with all master slide IDs
                allMasterSlideIDs = _.keys(allMasterSlides),
                // a sorted array with all master slide indices
                allMasterSlideIndices = [],
                // a helper object, that has as keys the master slide IDs and a special key 'order'
                infoObject = {};

            _.each(allMasterSlideIDs, function (id) {
                var // the index for the specified id
                    index = _.indexOf(masterSlideOrder, id);

                infoObject[id] = index;
                allMasterSlideIndices.push(index);
            });

            infoObject.order = allMasterSlideIndices.sort(function (a, b) { return a - b; });

            return infoObject;
        };

        /**
         * Getting a valid index to insert a slide as last slide behind a specified master slide. If -1
         * is returned, the slide can be added to the end of the container.
         *
         * @param {String} id
         *  The id of a master slide.
         *
         * @returns {Number}
         *  The index in the container 'masterSlideOrder' that can be used as final index behind the specified
         *  master slide (it is the position of the following master slide). If there is only one master slide
         *  -1 is returned. This means, that the new element can be added to the end of the container.
         */
        this.getLastPositionInModelBehindMaster = function (id) {

            var // a helper object with all informations about the master slide indices in the model 'masterSlideOrder'
                indexObject = self.getAllMasterSlideIndices(),
                // the new index
                newIndex = -1;

            if (indexObject.order.length > 1 && _.isNumber(indexObject[id])) {

                if (indexObject[id] !== _.last(indexObject.order)) {
                    newIndex = indexObject.order[_.indexOf(indexObject.order, indexObject[id]) + 1];
                }

            }

            return newIndex;
        };

        /**
         * Getting the number of layout slides that have the master specified by
         * its ID as target.
         *
         * @param {String} id
         *  The ID of the master slide
         *
         * @returns {Number}
         *  The number of layout slides belonging to the master. Or -1, if the
         *  specified ID is not the ID of a master slide.
         */
        this.getNumberOfLayoutSlidesOfSpecifiedMaster = function (id) {

            var // the number of layout slides
                count = -1,
                // a search index in the master slide container
                index = 0;

            if (self.isMasterSlideId(id)) {

                count = 0;
                index = _.indexOf(masterSlideOrder, id) + 1;

                while (index < masterSlideOrder.length && self.isLayoutSlideId(masterSlideOrder[index])) {
                    count++;
                    index++;
                }

            }

            return count;
        };

        /**
         * Getting the maximum number of layout slides for all master slides.
         *
         * @returns {Number}
         *  The maximum number of layout slides for all master slides.
         */
        this.getMaxLayoutSlideNumber = function () {

            var // a counter for the layout slides
                count = 0,
                // the largest number of layout slides belonging to a master slide
                largestCounter = 0;

            masterSlideOrder.forEach(function (id) {

                if (self.isMasterSlideId(id)) {
                    if (count > largestCounter) { largestCounter = count; }
                    count = 0;
                }

                if (self.isLayoutSlideId(id)) {
                    count++;
                }
            });

            if (count > largestCounter) { largestCounter = count; }

            return largestCounter;
        };

        /**
         * Check, whether a specified ID is the ID of a layout slide and if this layout slide is
         * used by at least one standard slide.
         * The model 'layoutConnection' can be used, when the specified ID is at least once the value.
         *
         * @param {String} id
         *  The ID of the slide.
         *
         * @returns {Boolean}
         *  Whether the slide specified by the ID is a layout slide that is referenced by at least
         *  one standard slide.
         */
        this.isUsedLayoutSlide = function (id) {
            return self.isLayoutSlideId(id) && _.contains(_.values(layoutConnection), id);
        };

        /**
         * Check, whether a master slide specified by its id is used as target by at least one layout
         * slide. If this is not the case, the slide can be removed.
         *
         * @param {String} id
         *  The id of the slide.
         *
         * @returns {Boolean}
         *  Whether the specified slide is used as target by another slide.
         */
        this.isUsedMasterSlideByLayoutSlide = function (id) {
            return _.contains(_.values(layoutMasterConnection), id);
        };

        /**
         * Check, whether a master slide specified by its id is used by at least one document
         * slide (standard slide). This means, that at least one document slide is based on
         * a layout slide, that has the specified master slide as target.
         *
         * @param {String} id
         *  The id of the slide.
         *
         * @returns {Boolean}
         *  Whether the specified slide is used as target by another slide.
         */
        this.isUsedMasterSlideByStandardSlide = function (id) {

            var // whether the ID specifies a master slide, that is used by at least one document slide
                isUsed = false,
                // a collection of all layout slide, that have the specified master as target
                allLayoutSlides = null;

            if (self.isMasterSlideId(id)) {

                allLayoutSlides = self.getAllSlideIDsWithSpecificTargetParent(id);

                if (allLayoutSlides && allLayoutSlides.length > 0) {
                    isUsed = _.isString(allLayoutSlides.find(self.isUsedLayoutSlide));
                }
            }

            return isUsed;
        };

        /**
         * Check, whether a layout or master slide specified by its id is used as target by another slide.
         * If this is the case, the slide cannot be removed.
         *
         * @param {String} id
         *  The id of the slide.
         *
         * @returns {Boolean}
         *  Whether the specified slide is used as target by a standard (document) slide.
         */
        this.isUsedMasterOrLayoutSlide = function (id) {
            return self.isLayoutOrMasterId(id) && (self.isUsedLayoutSlide(id) || self.isUsedMasterSlideByStandardSlide(id));
        };

        /**
         * Receiving the layout slide id for a document slide specified by its id.
         *
         * @param {String} id
         *  The id of the document slide, whose layout slide id shall be determined.
         *
         * @returns {String|Null}
         *  The id of the layout slide that belongs to the specified slide. If no layout slide exists,
         *  null is returned.
         */
        this.getLayoutSlideId = function (id) {
            return layoutConnection[id] || null;
        };

        /**
         * Receiving the master slide id for a layout slide specified by its id.
         *
         * @param {String} id
         *  The id of the layout slide, whose master slide id shall be determined.
         *
         * @returns {String|Null}
         *  The id of the master slide that belongs to the specified layout slide. If no master slide
         *  exists, null is returned.
         */
        this.getMasterSlideId = function (id) {
            return layoutMasterConnection[id] || null;
        };

        /**
         * Receiving the layout slide id or master slide id for a slide specified by its id.
         *
         * @param {String} id
         *  The id of the slide, whose layout slide id or master slide id shall be determined.
         *
         * @returns {String|Null}
         *  The id of the layout or master slide that belongs to the specified slide. If no
         *  layout or master slide exists, null is returned.
         */
        this.getLayoutOrMasterSlideId = function (id) {
            return self.getLayoutSlideId(id) || self.getMasterSlideId(id);
        };

        /**
         * Receiving the master slide ID of a slide specified by its ID. This can be the ID
         * of a document slide, a layout slide or a master slide. If the specified ID is already
         * the ID of a master slide, the same value is returned. If no master slide can be
         * determined, null is returned.
         *
         * @param {String} id
         *  The ID of a document slide, a layout slide or a master slide. The ID of the master slide
         *  belonging to this specified ID shall be determined.
         *
         * @returns {String|Null}
         *  The ID of the master slide that belongs to the specified slide ID. If no master slide
         *  exists, null is returned. If the specified ID is already a master slide ID, the same
         *  value is returned.
         */
        this.getCorrespondingMasterSlideId = function (id) {

            var // the ID of the master slide
                masterId = null;

            if (self.isStandardSlideId(id)) {
                masterId = self.getMasterSlideId(self.getLayoutSlideId(id));
            } else if (self.isLayoutSlideId(id)) {
                masterId = self.getMasterSlideId(id);
            } else if (self.isMasterSlideId(id)) {
                masterId = id;
            }

            return masterId;
        };

        /**
         * Receiving the model object for the connection between standard and layout slides.
         *
         * @returns {Object}
         *  The connection object for standard and layout slide IDs.
         */
        this.getLayoutConnection = function () {
            return _.copy(layoutConnection);
        };

        /**
         * Receiving the model object for the connection between layout and master slides.
         *
         * @returns {Object}
         *  The connection object for layout and master slide IDs.
         */
        this.getLayoutMasterConnection = function () {
            return _.copy(layoutMasterConnection);
        };

        /**
         * Check, whether a slide specified by its ID can be deleted. This is not the case, if:
         * - the slide is the last slide in its view.
         * - the slide is a master or layout slide, that is referenced by at least one document
         *   slide.
         *
         * @param {String} [id]
         *  The slide ID. If not specified, the ID of the active slide is used.
         *
         * @returns {Boolean}
         *  Whether the slide can be deleted.
         */
        this.isDeletableSlide = function (id) {
            return !self.isUsedMasterOrLayoutSlide(id ? id : activeSlideId);
        };

        /**
         * Check, whether a slide selection (in the slide pane) can be deleted. This is not the case, if:
         * - there are no slides
         * - the final master slide would be deleted
         * - a slide in the selection is a master or layout slide, that is referenced by at least one document
         *   slide.
         *
         * @param {Number[]} selection
         *  An Array containing the position as index from all selected slides.
         *
         * @returns {Boolean}
         *  Whether the slides can be deleted.
         */
        this.isSlideSelectionDeletable = function (selection) {

            // whether the selected slides can be deleted
            var deletable = true;
            // a container for all master slides that shall be deleted
            var masterSlideIds = null;

            // for master/layout slides
            if (self.isMasterView()) {

                masterSlideIds = [];

                _.each(selection, function (slideIndex) {
                    // when an element in the selection is not deletable
                    var slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
                    if (self.isUsedMasterOrLayoutSlide(slideId)) { deletable = false; }
                    if (deletable && self.isMasterSlideId(slideId)) { masterSlideIds.push(slideId); }
                });

                if (app.isODF()) {
                    if (self.getMasterSlideCount() === 1) { deletable = false; }
                } else {
                    if (deletable && (masterSlideIds.length === self.getNumberOfMasterSlides())) { deletable = false; }  // never delete the final master slide
                }
            } else if (app.isODF() && selection.length === self.getActiveViewCount()) {
                deletable = false; // in odp is not allowed to delete all document slides, at least one must exist
            }

            return deletable && !self.isEmptySlideView();
        };

        /**
         * Receiving the number of all normal (standard) slides.
         *
         * @returns {Number}
         *  The number of all normal (standard) slides
         */
        this.getStandardSlideCount = function () {
            return slideOrder.length;
        };

        /**
         * Receiving the sorted array of normal slide IDs.
         *
         * @param {Boolean} doCopy
         *  Whether a copy of the order container shall be returned. If not
         *  specified, no copy is made.
         *
         * @returns {String[]}
         *  The sorted array of normal slide IDs.
         */
        this.getStandardSlideOrder = function (doCopy) {
            return doCopy ? _.clone(slideOrder) : slideOrder;
        };

        /**
         * Checking, whether a specified id is a valid and registered slide id.
         *
         * @param {String} id
         *  The slide id to be checked.
         *
         * @returns {Boolean}
         *  Whether the specified id is a valid value for a slide id.
         */
        this.isValidSlideId = function (id) {
            return isValidSlideId(id);
        };

        /**
         * Receiving the slide id of the direct parent slide. This is the
         * layout slide for a standard slide and the master slide id for
         * a layout slide.
         *
         * @param {String} [id]
         *  The ID of the slide, whose direct parent slide shall be determined.
         *  If not specified, the id of the active slide is used.
         *
         * @returns {String|Null}
         *  The ID of the direct parent slide. If not specified, null is
         *  returned.
         */
        this.getParentSlideId = function (id) {

            var // the ID of the direct parent slide
                parentId = null;

            id = id || activeSlideId;

            if (self.isStandardSlideId(id)) {
                parentId = self.getLayoutSlideId(id);
            } else if (self.isLayoutSlideId(id)) {
                parentId = self.getMasterSlideId(id);
            }

            return parentId;
        };

        /**
         * Receiving the sorted array of master and layout slide IDs.
         *
         * @param {Boolean} doCopy
         *  Whether a copy of the order container shall be returned. If not
         *  specified, no copy is made.
         *
         * @returns {String[]}
         *  The sorted array of master and layout slide IDs.
         */
        this.getMasterSlideOrder = function (doCopy) {
            return doCopy ? _.clone(masterSlideOrder) : masterSlideOrder;
        };

        /**
         * Receiving the number of master and layout slides.
         *
         * @returns {Number}
         *  The number of all master and layout slides
         */
        this.getMasterSlideCount = function () {
            return masterSlideOrder.length;
        };

        /**
         * Receiving the number of master slides, not layout slides.
         *
         * @returns {Number}
         *  The number of master slides, not layout slides
         */
        this.getNumberOfMasterSlides = function () {
            return _.keys(allMasterSlides).length;
        };

        /**
         * Receiving the number of slides in active view.
         *
         * @returns {Number}
         *  The number of slides in the active view.
         */
        this.getActiveViewCount = function () {

            var // the container that contains the order of the slides
                orderContainer = isMasterView ? masterSlideOrder : slideOrder;

            return orderContainer.length;
        };

        /**
         * Receiving the next valid slide (up or downwards). A valid slide is,
         * (A) a master slide, or (B) a normal/layout slide. By default the next valid
         * slide is a normal or layout slide. It returns -1 when there are no valid
         * slides in the given direction anymore.
         *
         * @param {String} id
         *  The id of the slide, whose next slide (normal/layout or master) shall be determined.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.masterIsValidSlide=false]
         *      Whether next valid slide should be a master slide or a normal/layout slide.
         *      Default are the normal and layout slides.
         *  @param {Boolean} [options.upwards=true]
         *   Whether the next slide is upwards or downwards from the given slide ID.
         *   Default is upwards.
         *
         * @returns {Number}
         *  The index of the next valid slide. If the index is not valid
         *  (e.g. there is not normal or layout slide up/downwards anymore), -1 is returned.
         */
        this.getNextValidSlideIndex = function (id, options) {

            var // whether the next slide is upwards or downwards
                upwards = getBooleanOption(options, 'upwards', true),
                // whether a valid slide is a master or normal/layout slide
                masterIsValidSlide = getBooleanOption(options, 'masterIsValidSlide', false),
                // whether the slide id is in master view or not
                isMasterOrLayoutId = self.isLayoutOrMasterId(id),
                // container that contains the order of the slides
                container = isMasterOrLayoutId ? masterSlideOrder : slideOrder,
                // the index of the given slide id
                currentIndex = _.indexOf(container, id),
                // next slide index up or down for the given id
                nextSlideIndex = upwards ? currentIndex - 1 : currentIndex + 1;

            // returns whether the 'nextSlideIndex' is in a valid range or not
            function inRange(nextSlideIndex) {
                return (nextSlideIndex > -1 && nextSlideIndex < container.length);
            }
            // returns whether the slide at the 'nextSlideIndex' is a valid slide or not
            function isValidSlide(masterIsValidSlide, nextSlideIndex) {
                return (masterIsValidSlide ? !(self.isMasterSlideId(container[nextSlideIndex])) : self.isMasterSlideId(container[nextSlideIndex]));
            }

            // Find the next valid slide position:
            // - 1)  check if the index is in range to prevent undefined values for 'isValidSlide()' and prevent a infinite loop
            // -- 2A) masterIsValidSlide = true -> go to the next position until it's not a normal/layout slide or out of range
            // -- 2B) masterIsValidSlide = false -> go to the next position until it's not a master slide or out of range
            while (inRange(nextSlideIndex) && isValidSlide(masterIsValidSlide, nextSlideIndex)) {

                nextSlideIndex = upwards ? nextSlideIndex - 1 : nextSlideIndex + 1;
            }

            // check if the index is in a valid range, when not there is no next valid slide in that direction and -1 is returned
            return inRange(nextSlideIndex) ? nextSlideIndex : -1;
        };

        /**
         * Receiving a list of all slide IDs of those slides, that have as parent the specified target.
         * The target parameter can be the id of the corresponding layout slide or master slide.
         *
         * @param {String} target
         *  The id of the target slide. The target slide is a layout slide or a master slide.
         *
         * @returns {String[]}
         *  An array containing the IDs of those slides, that have the specified target as parent.
         */
        this.getAllSlideIDsWithSpecificTargetParent = function (target) {

            var // a collector for all affected slide IDs
                allSlideIDs = [];

            if (self.isLayoutSlideId(target)) {
                _.each(_.keys(layoutConnection), function (key) {
                    if (layoutConnection[key] === target) { allSlideIDs.push(key); }
                });
            } else if (self.isMasterSlideId(target)) {
                _.each(_.keys(layoutMasterConnection), function (key) {
                    if (layoutMasterConnection[key] === target) { allSlideIDs.push(key); }
                });
            }

            return allSlideIDs;
        };

        /**
         * Receiving a list of all slide IDs of those slides, that have as target the specified target
         * or whose target slide has the specified target.
         *
         * The target can be the id of the corresponding layout slide or master slide.
         *
         * @param {String} target
         *  The id of the target slide. The target slide is a layout slide or a master slide.
         *
         * @returns {String[]}
         *  An array containing the IDs of those slides, that have the specified target or whose
         *  target slide have the specified target.
         */
        this.getAllSlideIDsWithSpecificTargetAncestor = function (target) {

            var // a collector for all affected slide IDs
                allSlideIDs = self.getAllSlideIDsWithSpecificTargetParent(target);

            // additionally search all 'standard' slides affected by the changes of the layout slides
            if (self.isMasterSlideId(target)) {
                _.each(_.clone(allSlideIDs), function (layoutID) {
                    allSlideIDs = allSlideIDs.concat(self.getAllSlideIDsWithSpecificTargetParent(layoutID));
                });
            }

            return allSlideIDs;
        };

        /**
         * Switching the view for displaying the 'normal' slides or the master and
         * layout slides.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.showMaster=false]
         *      Whether the normal slides shall be displayed or the master and layout slides.
         *      Default are the normal slides.
         */
        this.selectSlideView = function (options) {

            var // whether the master/layout view will be activated or not
                showMaster = getBooleanOption(options, 'showMaster', false);

            // setting value in the model
            setActiveView(showMaster);
        };

        /**
         * Check, if a specified slide or the currently active slide displays content of layout or master slide.
         *
         * @param {String} [id]
         *  The id of a slide. If not specified, the id of the currently active slide is used.
         *
         * @returns {Boolean}
         *  Whether the specified or the active slide has the attribute 'followmastershapes' set or not.
         */
        this.isFollowMasterShapeSlide = function (id) {
            return (id || activeSlideId) ? (self.getSlideFamilyAttributeForSlide(id ? id : activeSlideId, 'slide', 'followMasterShapes') !== false) : true;
        };

        /**
         * Check, if a specified slide or the currently active slide is a 'hidden' slide.
         *
         * @param {String} [id]
         *  The id of a slide. If not specified, the id of the currently active slide is used.
         *
         * @returns {Boolean}
         *  Whether the specified or the active slide has the attribute 'hidden' set or not.
         */
        this.isHiddenSlide = function (id) {
            return (id || activeSlideId) ? (self.getSlideFamilyAttributeForSlide(id ? id : activeSlideId, 'slide', 'hidden') === true) : false;
        };

        /**
         * Check, if all document slides of the presentation are hidden.
         *
         * @returns {Boolean}
         *  Whether all document slides of the presentation are hidden.
         */
        this.allSlidesHidden = function () {
            return !slideOrder.find(slideId => !self.isHiddenSlide(slideId));
        };

        /**
         * Generating an operation for the slide attribute 'followMasterShapes'.
         *
         * @param {Boolean} value
         *  The value for the attribute 'followMasterShapes'.
         */
        this.setFollowMasterShape = function (value) {

            var // the operations generator
                generator = null,
                // whether a master slide is active
                isMasterSlide = self.isMasterSlideId(activeSlideId),
                // the options for the setAttributes operation
                operationOptions = null;

            // no 'followMasterShape' operation for master slides
            if (!isMasterSlide) {

                generator = self.createOperationGenerator();
                operationOptions = { start: _.copy(self.getActiveSlidePosition()), attrs: { slide: { followMasterShapes: value } } };

                // generate the 'setAttributes' operation
                generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);

                // apply all collected operations
                this.applyOperations(generator);

                // an update of the vertical scrollbar might be required, because some of the drawings, that become
                // visible can be located above the active slide
                if (value) { self.trigger('update:verticalscrollbar', { keepScrollPosition: true, pos: self.getActiveSlideIndex() }); }
            }
        };

        /**
         * Generating an operation to set the 'customPrompt' property to a place holder drawing on a
         * layout slide.
         *
         * TODO: In the moment it is only supported, to set the value 'true'. It is not possible, to
         *       remove this property later, because the default is not known.
         *
         * @param {String} layoutId
         *  The ID of a layout slide.
         *
         * @param {String} type
         *  The type of the place holder drawing.
         *
         * @param {Number} index
         *  The index of the place holder drawing.
         *
         * @param {Boolean} value
         *  The value of the 'customPrompt' property.
         */
        this.generateCustomPromptOperation = function (layoutId, type, index, value) {

            var // the generator for all operations
                generator = self.createOperationGenerator(),
                // the options for the operation
                operationOptions = {},
                // the drawing on the layout slide
                drawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId, type, index);

            if (drawing.length === 1) {

                if (isCustomPromptPlaceHolderDrawing(drawing) && value) { return; } // no new operation required

                operationOptions.start = getOxoPosition(self.getRootNode(layoutId), drawing, 0);
                operationOptions.attrs = { presentation: { customPrompt: value } };
                operationOptions.target = layoutId;

                // generate the 'setAttributes' operation
                generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);

                // apply all collected operations
                self.applyOperations(generator);
            }
        };

        /**
         * Change the hidden state of a selection of slides (can also be one). It uses a generator
         * and the 'setHiddenSlide()' function to group all individual hide/unhide operations.
         *
         * @param {Number[]} selection
         *  An array containing the indices from all selected slides.
         *
         * @param {Boolean} isHidden
         *  The new value for the attribute 'hidden'.
         */
        this.hideMultipleSlides = function (selection, isHidden) {

            var // the generator for all operations
                generator = self.createOperationGenerator(),
                // the currently used slideId
                slideId;

            _.each(selection, function (slideIndex) {

                slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
                self.setHiddenSlide(slideId, isHidden, generator);
            });

            // ... and finally applying the operations
            self.applyOperations(generator);
        };

        /**
         * Generating an operation for the slide attribute 'hidden'. This is only
         * possible in the normal view. Layout or master slides cannot be 'hidden'.
         *
         * @param {String} [slideId]
         *  The ID of the slide which is affected. Is uses the activeSlide when not specified.
         *
         * @param {Boolean} isHidden
         *  The new value for the attribute 'hidden'.
         *
         * @param {Object} [globalGenerator]
         *  The operations generator to collect all affected operations. This is only required
         *  for a multi slide selection. If it is not specified, this function generates its
         *  own operation generator.
         */
        this.setHiddenSlide = function (slideId, isHidden, globalGenerator) {

            var // the operations generator
                generator = null,
                // the options for the setAttributes operation
                operationOptions = null;

            // fall-back to the ID of the active slide
            slideId = slideId || activeSlideId;

            // no valid operation in master view / only generate operations when the state for the slide is really changed
            if (!isMasterView && (self.isHiddenSlide(slideId) !== isHidden)) {

                generator = globalGenerator || self.createOperationGenerator();

                operationOptions = { start: _.copy(self.getSlidePositionById(slideId)), attrs: { slide: { hidden: isHidden } } };

                // generate the 'setAttributes' operation
                generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);

                // ... and finally applying the operations (but only, if no global generator was provided)
                if (!globalGenerator) { self.applyOperations(generator); }
            }
        };

        /**
         * Generating an operation to set the slidePaneWidth attribute.
         * Note: The 'localViewSlidePaneWidth' is always rounded to
         * thousandths (e.g. to 15.123), so the 'slidePaneWidth' in this operation is
         * also always rounded to thousandths.
         *
         * @returns {Boolean}
         *  A boolean value specifying whether applying the operation was
         *  successful in synchronous mode (option 'async' has not been set).
         */
        this.setSlidePaneWidthInDocAttributes = function () {
            return self.applyOperations({ name: Op.CHANGE_CONFIG, attrs: { layout: { slidePaneWidth: localViewSlidePaneWidth } } });
        };

        /**
         * Setting the currently used slidePaneWidth by the user in the browser.
         *
         * @param {Number} width
         *  The width for the slide pane in percent. It is rounded to
         *  thousandths (e.g. to 15.123)
         */
        this.setLocalViewSlidePaneWidth = function (width) {
            localViewSlidePaneWidth = math.roundp(width, 0.001);
            self.sendChangedViewAttributes();
        };

        /**
         * Getting the currently used slidePaneWidth by the user in the browser.
         *
         * @returns {Number}
         *  The width for the slide pane in percent.
         */

        this.getSlidePaneWidth = function () {
            return localViewSlidePaneWidth;

        };

        /**
         * Getting the slidePaneWidth from operations.
         *
         * @returns {Number}
         *  The width for the slide pane width in percent.
         *  We get a value rounded to thousandths from the
         *  filter (e.g. to 15.123)
         */
        this.getSlidePaneWidthInDocAttributes = function () {
            return self.getLayoutAttributes().slidePaneWidth;
        };

        /**
         * Send changed view attributes from the slide pane with an operation,
         * so that the attributes are stored in the document. Only send changed
         * view attributes when the user has edit rights, has modified the
         * document locally, is not on mobile or tablets.
         *
         * @returns {Boolean}
         *  A boolean value specifying whether applying the operation was
         *  successful in synchronous mode (option 'async' has not been set).
         */
        this.sendChangedViewAttributes = function () {

            // don't save it on tablets/mobile, because the slide pane is not resizable there
            if (COMPACT_DEVICE) {
                return false;
            }

            // only send changed attributes when user has edit rights
            if (!app.isEditable()) {
                return false;
            }

            // do not send attributes, if undo/redo is running (48211)
            // Also not generating operation in long running processes (recursive call), 53641
            if (self.isUndoRedoRunning() || self.isProcessingActions()) {
                return false;
            }

            // // whether the document is modified // TODO recheck and remove this disabled block
            // var modified = app.isLocallyModified();
            //
            // // if file was not modified, check if the slide pane width has been changed compared to the document
            // if (!modified) {
            //     if (self.getSlidePaneWidthInDocAttributes() !== self.getSlidePaneWidth()) {
            //         modified = true;
            //     }
            // }
            // // if the file was not modified, nothing to do
            // if (!modified) {
            //     return false;
            // }

            // without a change, no need to send  (case: change width, flush/download directly after that)
            if (self.getSlidePaneWidthInDocAttributes() === self.getSlidePaneWidth()) {
                return false;
            }

            // send the slidePaneWidth with an operation
            return self.setSlidePaneWidthInDocAttributes();
        };

        /**
         * Returns whether the currently active slide or any slide specified by a certain slide id,
         * either of normal, layout or master type, does feature its very own background fill.
         *
         * Thus, it returns true only for an explicitly set background fill, but not for an inherited one.
         *
         * @param {String} [slideId]
         * Id of a specific slide. If not specified, the id of the currently active slide is used.
         *
         * @returns {Boolean}
         * Whether a specific or the currently active slide does feature its very own background fill.
         */
        this.isSlideWithExplicitBackground = function (slideId) {
            var
                fillType = self.getSlideFamilyAttributeForSlide(slideId || activeSlideId, 'fill', 'type');

            return (fillType && (fillType !== 'none')) || false;
        };

        /*
            * Returns whether a specified slide needs its very own background fill. This is the case, if
            * it has explicit fill attributes, or if it has a specfied theme and an underlying slide has
            * an explicit themed background fill (special handling for task 48471, 49863).
            *
            * @returns {Boolean}
            *  Whether a specified slide needs its very own background fill.
            */
        this.isSlideWithOwnBackground = function (slideId) {

            var // whether the slide node specified by the slide ID must have a visible background
                hasVisibleBackground = self.isSlideWithExplicitBackground(slideId),
                // the ID of an underlying slide with specified background
                backgroundSlideId = null,
                // the inherited fill attributes
                inheritedFillAttrs = null;

            if (!hasVisibleBackground && !self.isMasterSlideId(slideId) && self.hasDefaultTheme(slideId)) {
                backgroundSlideId = self.getMostTopBackgroundSlide(_.rest(self.getTargetChain(slideId)));
                if (backgroundSlideId) {
                    inheritedFillAttrs = self.getSlideAttributesByFamily(backgroundSlideId, 'fill');
                    if (isFillThemed(inheritedFillAttrs)) {
                        hasVisibleBackground = true;
                    }
                }
            }

            return hasVisibleBackground;
        };

        /**
         * Deleting the slide background.
         */
        this.resetBackground = function () {

            var // the operations generator
                generator = self.createOperationGenerator(),
                // collection of all currently selected slides
                selectedSlidesCollection = self.getAllSelectedSlides();

            _.each(selectedSlidesCollection, function (oneSlide, slideId) {
                // the options for the setAttributes operation
                var operationOptions = { attrs: getEmptySlideBackgroundAttributes() };

                if (self.isLayoutOrMasterId(slideId)) {
                    operationOptions.target = slideId;
                    operationOptions.start = [0];
                } else {
                    operationOptions.start = getOxoPosition(self.getNode(), oneSlide);
                }
                // generate the 'setAttributes' operation
                generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);
            });

            // apply all collected operations
            this.applyOperations(generator);
        };

        /**
         * Setting the background color of the active slide.
         * Info: The filter does not like the color 'auto'. Therefore this is handled
         *       like removing an existing background (color and image). In pptx file
         *       the background color is also only removed, if 'auto' is selected by
         *       the user.
         *
         * @param {Object} color
         *  The color object.
         */
        this.setBackgroundColor = function (color) {

            var // the operations generator
                generator = self.createOperationGenerator(),
                // the options for the setAttributes operation
                operationOptions = { start: _.copy(self.getActiveSlidePosition()), attrs: { fill: { type: 'solid', color, imageUrl: null } } },
                // whether the color 'auto' was selected -> removing any background
                isAutoColor = (color && color.type && color.type === 'auto');

            if (isAutoColor) { operationOptions.attrs = getEmptySlideBackgroundAttributes(); }

            // generate the 'setAttributes' operation
            generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);

            // apply all collected operations
            this.applyOperations(generator);
        };

        /**
         * Getter method for root container node with passed target, or if target is not passed, default page node.
         *
         * @param {String} [target]
         *  ID of root container node
         *
         * @returns {jQuery}
         *  The root node specified by the target string, or the page node, if no target was specified.
         */
        this.getRootNode = function (target) {
            return (target && self.isLayoutOrMasterId(target)) ? getLayoutOrMasterSlideContentNode(target) : pagediv;
        };

        /**
         * Helper function to check the visibility of the page node. This node must be invisible, if there
         * is no remaining slide in the active view. But during loading the 'pageVisibilityHandler' must be called
         * with this public function.
         */
        this.checkPageVisibility = function () {
            pageVisibilityHandler();
        };

        /**
         * Getter method for currently active root container node.
         *
         * @param {String} [target]
         *  ID of root container node
         *
         * @returns {jQuery}
         *  The root node specified by the target string
         */
        this.getCurrentRootNode = function (target) {

            // target for operation - if exists
            var currTarget = target || self.getActiveTarget();

            // checking, if this is a target from a master or layout slide
            if (currTarget && self.isLayoutOrMasterId(currTarget)) { return getLayoutOrMasterSlideContentNode(currTarget); }

            // returning the page node, if no other root node was found before
            return pagediv;
        };

        /**
         * Getter method to receive all available layout slide IDs.
         *
         * @returns {String[]}
         *  An array with all defined IDs of layout slides.
         */
        this.getAllLayoutSlideIds = function () {
            return _.keys(allLayoutSlides);
        };

        /**
         * Changing the active slide, if possible
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.next=true]
         *   Whether the next or the previous slide shall be selected.
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether hidden slides shall be ignored. This is only
         *   supported in the document view, not in the master view.
         *  @param {Number} [options.delayedVisibility=0]
         *   Whether the 'new' slide shall be made visible with a delay.
         *   This is sometimes required to avoid a flickering (DOCS-3084).
         *
         * @returns {Boolean}
         *  Whether the slide was changed.
         */
        this.changeToNeighbourSlide = function (options) {

            var // whether the next slide shall be made visible
                next = getBooleanOption(options, 'next', true),
                // whether hidden slides shall be ignored
                onlyVisible = !isMasterView && getBooleanOption(options, 'onlyVisible', false),
                // the container that contains the order of the slides
                orderContainer = isMasterView ? masterSlideOrder : slideOrder,
                // the index of the currently active slide inside the ordered container
                index = _.indexOf(orderContainer, activeSlideId),
                // the index of the slide that shall be activated
                newIndex = -1,
                // whether the slide changed
                slideChange = false;

            if (onlyVisible) {

                if (next) {
                    if (index < slideOrder.length) { newIndex = getNeighbourIndexOfVisibleSlide(index, { next }); }
                } else {
                    if (index > 0) { newIndex = getNeighbourIndexOfVisibleSlide(index, { next }); }
                }

            } else {

                if (next) {
                    if (index < (isMasterView ? masterSlideOrder.length : slideOrder.length)) { newIndex = index + 1; }
                } else {
                    if (index > 0) { newIndex = index - 1; }
                }

            }

            if (newIndex > -1) { slideChange = self.changeToSlide(self.getIdOfSlideOfActiveViewAtIndex(newIndex), { delayedVisibility: getNumberOption(options, 'delayedVisibility', 0) }); }

            return slideChange;
        };

        /**
         * Changing to a slide specified by its index in the order containers.
         * The view is not changed. If the index does not exist in the container,
         * the last slide in the view is selected.
         *
         * @param {Number} index
         *  The index in the order container.
         */
        this.changeToSlideAtIndex = function (index) {

            var // the container that contains the order of the slides
                orderContainer = isMasterView ? masterSlideOrder : slideOrder;

            if (orderContainer[index]) {
                self.changeToSlide(orderContainer[index]);
            } else {
                self.changeToLastSlideInView();
            }

        };

        /**
         * Activating the first slide in the current view.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether hidden slides shall be ignored. This is only
         *   supported in the document view, not in the master view.
         */
        this.changeToFirstSlideInView = function (options) {

            // whether hidden slides shall be ignored
            var onlyVisible = !isMasterView && getBooleanOption(options, 'onlyVisible', false);

            if (onlyVisible) {
                var id = getIdOfFirstVisibleSlide();
                if (id) { self.changeToSlide(id); }
            } else {
                self.changeToSlide(isMasterView ? _.first(masterSlideOrder) : _.first(slideOrder));
            }
        };

        /**
         * Activating the last slide in the current view.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether hidden slides shall be ignored. This is only
         *   supported in the document view, not in the master view.
         */
        this.changeToLastSlideInView = function (options) {

            // whether hidden slides shall be ignored
            var onlyVisible = !isMasterView && getBooleanOption(options, 'onlyVisible', false);

            if (onlyVisible) {
                var id = getIdOfLastVisibleSlide();
                if (id) { self.changeToSlide(id); }
            } else {
                self.changeToSlide(isMasterView ? _.last(masterSlideOrder) : _.last(slideOrder));
            }
        };

        /**
         * Finding the ID of a visible neighbouring slide. Specified is the
         * ID of the slide where the search begins.
         *
         * @param {String} slideId
         *  The ID of a slide, for that a visible neighbour is searched.
         */
        this.findVisibleNeighbourSlide = function (slideId) {

            // the index of the specified slide inside the ordered container
            var index = _.indexOf(slideOrder, slideId);
            // the index of the slide that shall be activated
            var newIndex = -1;

            // search backward
            if (index > 0) { newIndex = getNeighbourIndexOfVisibleSlide(index, { next: false }); }

            // search forward, if not found
            if (newIndex === -1 && index < slideOrder.length) { newIndex = getNeighbourIndexOfVisibleSlide(index, { next: true }); }

            return (newIndex > -1) ? self.getSlideIdByPosition(newIndex) : null;
        };

        /**
         * Activating the first slide in the document slide view.
         */
        this.changeToFirstSlideInDocumentView = function () {
            // switching to document view and selecting the first slide
            if (isMasterView) { setActiveView(false); }
            self.changeToSlide(_.first(slideOrder));
        };

        /**
         * OT: Finding a valid position after the external operation made the current selection invalid.
         */
        this.changeToValidPosition = function () {

            var currentSlidePosition = selection.getStartPosition()[0];
            var slideId = null;
            var target = null;

            if (isMasterView) {
                target = self.getActiveTarget();
                if (self.isLayoutOrMasterId(target)) {
                    selection.setSlideSelection();
                } else {
                    self.changeToFirstSlideInView();
                }
            } else {
                while (!slideId && currentSlidePosition >= 0) {
                    slideId = self.getSlideIdByPosition(currentSlidePosition);
                    currentSlidePosition--;
                }
                if (slideId) {
                    if (self.isActiveSlideId(slideId)) {
                        selection.setSlideSelection();
                    } else {
                        self.changeToSlide(slideId);
                    }
                } else {
                    self.getSelection().setEmptySelection(); // no valid slide was found anymore
                }
            }
        };

        /**
         * Modifying the selection in that way, that only complete paragraphs are selected. The start
         * position has to be set to the beginning of its current paragraph and the end position has
         * to be set to the end of the current paragraph.
         *
         * This function is currently used for generating setAttribute operations, if the selection
         * is inside a layout or master slide. In this case a character attribute is assigned to the
         * complete paragraph.
         *
         * This automatic expansion of selection is NOT applied to header or footer place holder types.
         * In this case the selection is used as it is, so that only parts of the text inside a paragraph
         * can get a specific character attribute assigned
         * (see PresentationModel.NOT_COMPLETE_SELECTION_PLACEHOLDER_TYPES).
         */
        this.setMasterLayoutSelection = function () {

            var // the logical start position
                startPos = _.clone(selection.getStartPosition()),
                // the logical end position
                endPos = _.clone(selection.getEndPosition());

            // checking if the selection is inside a place holder drawing
            if (selection.isAdditionalTextframeSelection() && isPlaceHolderDrawing(selection.getSelectedTextFrameDrawing()) && !_.contains(PresentationModel.NOT_COMPLETE_SELECTION_PLACEHOLDER_TYPES, getPlaceHolderDrawingType(selection.getSelectedTextFrameDrawing()))) {

                // saving the current selection
                masterLayoutSelection = [_.clone(startPos), _.clone(endPos)];

                // expanding the selection
                startPos[startPos.length - 1] = 0;
                endPos[endPos.length - 1] = getParagraphLength(self.getCurrentRootNode(), _.initial(endPos));
                selection.setTextSelection(startPos, endPos);
            }

        };

        /**
         * After a setAttributes operation on a master or layout slide, it is necessary to update
         * the styles defined at the drawing/slide.
         * After this 'styling' update, all slides that used this style need to be updated.
         *
         * @param {HTMLElement|jQuery} element
         *  The element node, that received the new attributes.
         *
         * @param {String} target
         *  The active target for the specified element.
         *
         * @param {String} family
         *  The family of the attribute set.
         *
         * @param {Object} attributes
         *  A map with formatting attribute values, mapped by the attribute
         *  names, and by attribute family names.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.immediateUpdate=true]
         *      Whether slides that are dependent from the layout or master slide change
         *      shall be updated immediately. Default is the immediate update.
         *  @param {String} [options.saveOldAttrs=false]
         *      Whether the old attributes shall be saved and returned in the return object.
         *
         * @returns {Object|Null}
         *  If the attributes are registered, an object with the complete registration
         *  information is returned. The object's keys are: 'target', 'placeHolderType',
         *  'placeHolderIndex', 'attrs' and 'oldAttrs'. If the attributes were not registered,
         *  because target, type or index are not defined, null is returned.
         */
        this.updateMasterLayoutStyles = function (element, target, family, attributes, options) {

            // restoring a previous selection (if user has edit privileges)
            if (masterLayoutSelection) {
                if (target && self.isLayoutOrMasterId(target) && app.isEditable()) { selection.setTextSelection(...masterLayoutSelection); }
                masterLayoutSelection = null;
            }

            return registerPlaceHolderAttributes(element, target, family, attributes, options);
        };

        /**
         * Updating the model for the 'slide' family.
         *
         * @param {Number[]} start
         *  The logical position of the slide, whose attributes will be updated.
         *
         * @param {Object} attrs
         *  The object containing the attributes.
         *
         * @param {String} [target]
         *  The target of the slide, if exists.
         *
         * @returns {Object|null}
         *  If the attributes are registered, an object with the complete registration
         *  information is returned. The object's keys are: 'id' and 'attrs'.
         *  If the attributes were not registered, null is returned.
         */
        this.updateSlideFamilyModel = function (start, attrs, target) {

            var // the slide id
                slideId = target ? target : slideOrder[_.first(start)];

            return self.saveSlideAttributes(slideId, attrs);
        };

        /**
         * Merging the place holder attributes into a specified attribute object.
         *
         * @param {HTMLElement|jQuery} element
         *  The element node, that received the new attributes.
         *
         * @param {String} family
         *  The family of the attribute set.
         *
         * @param {Object} attrs
         *  A map with formatting attribute values, mapped by the attribute
         *  names, and by attribute family names.
         *
         * @param {Object} placeHolderAttrs
         *  A map with formatting attribute values, mapped by the attribute
         *  names, and by attribute family names that is saved in the place holder model.
         *
         * @returns {Object}
         *  An object with the merged attributes of the specified attributes
         *  and the specified place holder attributes.
         */
        this.mergePlaceHolderAttributes = function (element, family, attrs, placeHolderAttrs) {

            var // a paragraph node
                paragraph = null,
                // the paragraph level
                paraLevel = 0,
                // the key in the list styles corresponding to the paragraph level
                paraKey = null,
                // whether drawing attributes shall be registered
                isDrawing = (family === 'drawing'),
                // the merged attributes object
                mergedAttrs = attrs;

            if (isDrawing) {
                if (placeHolderAttrs[family]) {
                    mergedAttrs = this.defAttrPool.extendAttrSet(placeHolderAttrs, attrs);
                }
            } else {
                if (placeHolderAttrs.listStyle) {
                    paragraph = (family === 'paragraph') ? $(element) : $(element).closest(DOM.PARAGRAPH_NODE_SELECTOR);
                    paraLevel = self.paragraphStyles.getParagraphLevel(paragraph);
                    paraKey = self.generateListKeyFromLevel(paraLevel);

                    if (placeHolderAttrs.listStyle[paraKey] && placeHolderAttrs.listStyle[paraKey][family]) {
                        mergedAttrs = this.defAttrPool.extendAttrSet(placeHolderAttrs.listStyle[paraKey], attrs);
                    }
                }
            }

            return mergedAttrs;
        };

        /**
         * Public function to force an update of slides after a change of a master or layout
         * slide.
         *
         * @param {Object} data
         *  This object is generated inside the function 'savePlaceHolderAttributes' in the
         *  drawing styles. The object's keys are: 'target', 'placeHolderType', 'placeHolderIndex'
         *  and 'attrs'.
         */
        this.forceMasterLayoutUpdate = function (data) {
            triggerLayoutChangeEvent(data);
        };

        /**
         * Triggering the event 'slidestate:update' with one specified slide id. Typically this
         * event is only triggered after an 'operations:after' event. But there are some special
         * cases, in which the slide pane needs to be updated without an operation. For example,
         * if a template text is inserted into a place holder drawing on a layout slide (44991).
         *
         * @param {String} id
         *  The id of the slide that need to be updated in the slide pane.
         */
        this.forceTriggerOfSlideStateUpdateEvent = function (id) {
            debouncedUpdateSlideStateHandler(id);
        };

        /**
         * Triggering the event 'slidestate:update' for all those slides that contain at least
         * one node in the specified 'allNodes' container. This is for example useful, when a
         * list of drawings is updated deferred. In that case the slide pane need to be informed
         * about the modified slides.
         *
         * @param {HTMLElement[]|jQuery[]} allNodes
         *  A container with nodes.
         */
        this.updateAllAffectedSlidesInSlidePane = function (allNodes) {

            // a collector for all affected slide IDs
            var allSlideIDs = {};

            // collecting all affected slide IDs
            _.each(allNodes, function (oneNode) {
                var slideId = self.getSlideIdForNode(oneNode);
                if (slideId) { allSlideIDs[slideId] = 1; }
            });

            // iterating over all slide IDs
            _.each(_.keys(allSlideIDs), function (id) {
                self.forceTriggerOfSlideStateUpdateEvent(id);
            });
        };

        /**
         * Triggering a delayed event 'templatetext:updated' in a specified drawing on a slide
         * with the specified ID. This is required for the loading process, when the placeholder
         * drawings on layout slides get the template text inserted ('Chart', 'Media Clip' ...).
         * The event triggering needs to be delayed, because the formatting of the text might
         * still be required.
         *
         * @param {String} slideId
         *  The id of the slide containing the specified drawing.
         *
         * @param {jQuery} drawing
         *  The jQuerified drawing node.
         */
        this.forceTriggerOfTemplateTextUpdatedEvent = function (slideId, drawing) {
            self.executeDelayed(function () { // triggering delayed because of slide formatting
                self.trigger('templatetext:updated', slideId, drawing);
            }, 200);
        };

        /**
         * Returning the ratio of width divided by height of the page.
         *
         * @returns {Number}
         *  The ratio of width divided by height of the page. If it cannot be determined, 1 is returned.
         */
        this.getSlideRatio = function () {

            var // the attributes of the page
                pageAttributes = self.pageStyles.getElementAttributes(self.getNode()).page;

            return pageAttributes && pageAttributes.width && pageAttributes.height ? math.roundp(pageAttributes.width / pageAttributes.height, 0.0001) : 1;
        };

        /**
         * Returning the width and height of the slide in the document.
         *
         * @returns {Object}
         *  The width and height in Hmm from the slide in the document.
         */
        this.getSlideDocumentSize = function () {

            var // the attributes of the page
                pageAttributes = self.pageStyles.getElementAttributes(self.getNode()).page;

            return (pageAttributes && pageAttributes.width && pageAttributes.height) ? { width: pageAttributes.width, height: pageAttributes.height } : null;
        };

        /**
         * Check, whether at a specified position the list auto detection shall be executed. In
         * the text application there are not limitations yet.
         *
         * @returns {Boolean}
         *  Whether the list auto detection can be executed in the specified paragraph.
         */
        this.isForcedHardBreakPosition = function (paragraph) {
            return isTitlePlaceHolderDrawing($(paragraph).closest('div.drawing'));
        };

        /**
         * Check, whether the specified drawing is a drawing that contains place holder attributes. This means, that
         * the phType is set, or that the phIndex is specified. It is possible, that only the phIndex is specified. In
         * this case the phType is defaulted to 'body'.
         *
         * @param {HTMLElement|jQuery} drawing
         *  The drawing node.
         *
         * @returns {Boolean}
         *  Whether the specified drawing contains presentation place holder attributes.
         */
        this.isPlaceHolderDrawing = function (drawing) {
            return Boolean(isPlaceHolderDrawing(drawing));
        };

        /**
         * Check, whether the specified drawing is a place holder drawing, in that no text can be inserted. In this
         * case it contains no paragraphs (in master/layout view a paragraph is inserted, but not in the document
         * view). Instead a button is available that can be used to insert a table or an image.
         *
         * @param {HTMLElement|jQuery} drawing
         *  The drawing node.
         *
         * @returns {Boolean}
         *  Whether the specified drawing is a place holder drawing, that does not support text input.
         */
        this.isPlaceHolderWithoutTextDrawing = function (drawing) {
            return isPlaceHolderWithoutTextDrawing(drawing);
        };

        /**
         * Check, whether the specified drawing is an empty place holder drawing. For all types of place
         * holders it is checked, if the place holder contains the possible content.
         *
         * @returns {Boolean}
         *  Whether the specified drawing is an empty place holder drawing.
         */
        this.isEmptyPlaceHolderDrawing = function (drawing) {
            return isEmptyPlaceHolderDrawing(drawing);
        };

        /**
         * Check, whether the specified attribute set (explicit attributes from a drawing) contain the presentation
         * place holder attributes. This means, that the phType is set, or that the phIndex is specified. It is possible,
         * that only the phIndex is specified. In this case the phType is defaulted to 'body'.
         *
         * @param {Object} attrs
         *  The attribute object. This should be the explicit attributes from a drawing.
         *
         * @returns {Boolean}
         *  Whether the specified attribute set contains presentation place holder attributes.
         */
        this.isPlaceHolderAttributeSet = function (attrs) {
            return isPlaceHolderAttributeSet(attrs);
        };

        /**
         * Returns whether the current selection is a text selection inside a place holder drawing.
         * Grouping can be ignored in this scenario, because place holder drawings cannot be grouped.
         *
         * @returns {Boolean}
         *  Whether the current selection is a text selection inside a place holder drawing.
         */
        this.isAdditionalPlaceHolderDrawingSelection = function () {
            return selection.isAdditionalTextframeSelection() && self.isPlaceHolderDrawing(selection.getSelectedTextFrameDrawing());
        };

        /**
         * Check, whether at a specified paragraph position the list auto detection shall be executed. In
         * the text application or spreadsheet application there are not limitations yet.
         *
         * @param {HTMLElement|jQuery} paragraph
         *  The paragraph node.
         *
         * @returns {Boolean}
         *  Whether the list auto detection can be executed in the specified paragraph.
         */
        this.autoDetectionPositionCheck = function (paragraph) {
            return !isTitlePlaceHolderDrawing($(paragraph).closest('div.drawing'));
        };

        /**
         * Returns the layout attributes of the document.
         *
         * @returns {Object}
         *  The layout attributes.
         */
        this.getLayoutAttributes = function () {
            return this.defAttrPool.getDefaultValues('layout');
        };

        /**
         * Getting the start position of a layout slide relative to its master slide. This
         * is a zero-based number value, not a logical position. It can be used in the
         * operation 'moveLayoutSlide'. The first layout slide behind the master slide
         * has the value 0.
         *
         * @param {String} id
         *  The ID of the layout slide.
         *
         * @returns {Number}
         *  The zero based position of the layout slide behind its master slide. If it cannot
         *  be determined, -1 is returned.
         */
        this.getLayoutSlideStartPosition = function (id) {

            var // the zero based position of the layout slide relative to its master slide
                start = -1,
                // the ID of the master slide
                masterId = null;

            if (self.isLayoutSlideId(id)) {

                masterId = self.getMasterSlideId(id);

                if (masterId) {
                    start = self.getSortedSlideIndexById(id) - self.getSortedSlideIndexById(masterId) - 1; // zero-based value
                }

            }

            return start;
        };

        /**
         * Getting the start position of a master slide relative to all other master slides.
         * This is a zero-based number value, not a logical position. It can be used in the
         * operation 'insertMasterSlide'.
         *
         * @param {String} id
         *  The ID of the master slide.
         *
         * @returns {Number}
         *  The zero based position of the master slide relative to all other master slides.
         *  If it cannot be determined, -1 is returned.
         */
        this.getMasterSlideStartPosition = function (id) {

            var // the zero based position of the layout slide relative to its master slide
                start = -1,
                // a helper object with all informations about the master slide indices in the model 'masterSlideOrder'
                indexObject = null;

            if (self.isMasterSlideId(id)) {

                // a helper object with all informations about the master slide indices in the model 'masterSlideOrder'
                indexObject = self.getAllMasterSlideIndices();

                if (indexObject[id]) { start = _.indexOf(indexObject.order, indexObject[id]); }
            }

            return start;

        };

        /**
         * Check, whether a specified slide ID is in the container of the modified slide IDs.
         * Modified means, that the slide was modified since the last trigger of the event
         * 'slidestate:update'.
         *
         * @param {String} id
         *  The ID of that slide that is checked for modification.
         *
         * @returns {Boolean}
         *  Whether the specified id is in the collector of slide IDs of all modified slides.
         */
        this.isModifiedSlideID = function (id) {
            return _.contains(allModifiedSlideIDs, id);
        };

        /**
         * Getter for the selection box object, that can be used to create a specified
         * rectangle on the application content root node.
         *
         * @returns {Object}
         *  The selection box object.
         */
        this.getSelectionBox = function () {
            return selectionBox;
        };

        /**
         * Handling the page up and page down event, independent
         * from the current cursor position.
         *
         * @param {jQuery.Event} event
         *  A jQuery event object.
         *
         * @returns {Boolean}
         *  Whether the event was handled within this function.
         */
        this.handleSlideChangeByPageUpDown = function (event) {

            var // whether the event was handled within this function
                handledEvent = false;

            if (hasKeyCode(event, 'PAGE_UP')) {
                if (!self.isFirstSlideActive()) {
                    self.changeToNeighbourSlide({ next: false });
                }
                handledEvent = true;
            } else if (hasKeyCode(event, 'PAGE_DOWN')) {
                if (!self.isLastSlideActive()) {
                    self.changeToNeighbourSlide({ next: true });
                }
                handledEvent = true;
            }

            return handledEvent;
        };

        /**
         * Modifying the active silde, if the user uses a cursor key and the selection is
         * a slide selection ('isTopLevelTextCursor'). In this case the active slide needs
         * to be modified.
         *
         * @param {jQuery.Event} event
         *  A jQuery event object.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether hidden slides shall be ignored. This is only
         *   supported in the document view, not in the master view.
         */
        this.setActiveSlideByCursorEvent = function (event, options) {

            // whether only visible slides shall be selected (presentation mode)
            var onlyVisible = !isMasterView && getBooleanOption(options, 'onlyVisible', false);

            if (!self.getSelection().isTopLevelTextCursor() || !DOM.isCursorKey(event.keyCode)) { return; }

            if (hasKeyCode(event, 'LEFT_ARROW', 'UP_ARROW', 'PAGE_UP', 'HOME')) {
                // switch to previous or first slide
                if (!self.isFirstSlideActive({ onlyVisible })) {
                    if (hasKeyCode(event, 'HOME')) {
                        self.changeToFirstSlideInView({ onlyVisible });
                    } else {
                        self.changeToNeighbourSlide({ next: false, onlyVisible });
                    }
                }
            } else if (hasKeyCode(event, 'RIGHT_ARROW', 'DOWN_ARROW', 'PAGE_DOWN', 'END')) {
                // switch to next or last slide
                if (!self.isLastSlideActive({ onlyVisible })) {
                    if (hasKeyCode(event, 'END')) {
                        self.changeToLastSlideInView({ onlyVisible });
                    } else {
                        self.changeToNeighbourSlide({ next: true, onlyVisible });
                    }
                }
            }
        };

        /**
         * Returns whether a multi selection contains at least one drawing that cannot be
         * grouped.
         *
         * @returns {Boolean}
         *  Whether a multi selection contains at least one drawing that cannot be grouped.
         */
        this.isAtLeastOneNotGroupableDrawingInSelection = function () {

            var // an ungroupable drawing
                ungroupableDrawing = false;

            if (app.isODF()) {
                // ODF: No grouping of place holder drawings (but for example tables are allowed)
                ungroupableDrawing = selection.getArrayOfSelectedDrawingNodes().find(drawing => isPlaceHolderDrawing(drawing));
            } else {
                // in PP the ungroupable drawings have a flag
                ungroupableDrawing = selection.getArrayOfSelectedDrawingNodes().find(drawing => drawing.hasClass(NO_GROUP_CLASS));
            }

            return !!ungroupableDrawing;
        };

        /**
         * Saving all insertTheme operations in a collector object. These operations can
         * then be used for a full undo, after a slide was removed (63372).
         *
         * @param {Sting} id
         *  The ID of the slide that uses the specified theme. This can be a master or
         *  layout slide, but also a document slide.
         *
         * @param {Object} operation
         *  The complete insertTheme operation.
         */
        this.saveInsertThemeOperation = function (id, operation) {
            insertThemeOperations[id] = _.copy(operation, true);
        };

        /**
         * Getting the name for the 'insert'-operation for the specified node.
         * In the case of a slide, this can be one of:
         * - Operations.SLIDE_INSERT
         * - Operations.LAYOUT_SLIDE_INSERT
         * - Operations.MASTER_SLIDE_INSERT
         *
         * @param {Node|jQuery|Null} node
         *  The DOM node to be checked. If this object is a jQuery collection, uses
         *  the first DOM node it contains. If missing or null, returns empty string.
         *
         * @returns {Object|Null}
         *  An object containing required data for the insert operation of the slide:
         *   - 'name': The name of the required 'insert'-operation.
         *   - 'index': The index for a master or layout slide.
         *   - 'insertThemeOp': An optionally registered insertTheme operation for this
         *                      slide. Or null, if there is no registered operation.
         */
        this.getInsertOperationNameForNode = function (node, options) {

            var // the name of the operation
                operationName = null,
                // the index value required for master and layout slides
                operationIndex = null,
                // the list styles specified at the slide
                slideListStyles = null,
                // the id of the slide
                id = self.getSlideId(node),
                // an optional insertTheme operation that is required for a full undo of the slide
                insertThemeOp = insertThemeOperations[id] ? insertThemeOperations[id] : null;

            if (self.isMasterSlideId(id) || (app.isODF() && self.isLayoutSlideId(id))) {
                operationName = Op.MASTER_SLIDE_INSERT;
                operationIndex =  app.isODF() ? self.getSortedSlideIndexById(id) : getOperationIndexForSlideById(id);
                if (options) { options.id = id; }  // saving also the slide ID at the options object
                // saving the slide list styles for the undo operation in 'options' parameter
                slideListStyles = self.getAllListStylesAttributesOfSlide(id);
                if (slideListStyles) { options.slideListStyles = slideListStyles; }
            } else if (self.isLayoutSlideId(id)) {
                operationName = Op.LAYOUT_SLIDE_INSERT;
                operationIndex = getOperationIndexForSlideById(id);
                if (options) { options.id = id; }  // saving also the slide ID at the options object
            } else {
                operationName = Op.SLIDE_INSERT;
            }

            return operationName ? { name: operationName, index: operationIndex, insertThemeOp } : null;
        };

        /**
         * Getting the default text for an empty text frame drawing.
         * This function is application specific.
         *
         * @param {HTMLElement|jQuery} drawing
         *  The drawing node.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.id='']
         *      The id of the slide, that contains this drawing. This ID must be specified for
         *      document slides, so that user specified text from the corresponding layout slide
         *      can be found.
         *  @param {Boolean} [options.ignoreCustomPrompt=false]
         *      Whether the customPrompt property of a place holder drawing on a layout slide
         *      can be ignored. This might be the case
         *      The id of the slide, that contains this drawing. This ID must be specified for
         *      document slides, so that user specified text from the corresponding layout slide
         *      can be found.
         *
         * @returns {String|Null}
         *  The default text for a place holder drawing. Or null, if it cannot be determined.
         */
        this.getDefaultTextForTextFrame = function (drawing, options) {

            // drawing must be a place holder drawing.
            // -> text is dependent from the place holder type

            var // the template text
                templateText = null,
                // the explicit drawing attributes
                attrs = null,
                // the ID of the slide that contains the specified drawing
                slideId = null,
                // the ID of the layout slide
                layoutId = null,
                // a valid key set for type and index
                keySet = null,
                // the place holder attribute set for a specified place holder drawing
                allPlaceHolderAttributes = null,
                // the corresponding place holder drawing on the layout slide
                layoutDrawing = null;

            if (drawing) {

                attrs = getExplicitAttributeSet(drawing);

                if (attrs && isPlaceHolderAttributeSet(attrs)) {

                    slideId = getStringOption(options, 'id', '');

                    if (!slideId) { slideId = self.getSlideIdForNode(drawing); } // slide ID is required for the correct place holder text (49288)

                    if (slideId && self.isStandardSlideId(slideId)) { // searching for user defined template texts
                        layoutId = self.getLayoutSlideId(slideId);
                        keySet = getValidTypeIndexSet(attrs);
                        allPlaceHolderAttributes = self.drawingStyles.getPlaceHolderAttributes(layoutId, keySet.type, keySet.index);

                        if (isCustomPromptPlaceHolderAttributeSet(allPlaceHolderAttributes)) {
                            layoutDrawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId, keySet.type, keySet.index);
                            // using the template text, if it is defined by customer. Only using first paragraph and only text inside the text span.
                            templateText = layoutDrawing.find(DOM.PARAGRAPH_NODE_SELECTOR).first().children('span').text();
                        }
                    }

                    // using the default template text, if no user defined template text was found
                    if (!templateText) { templateText = getPlaceHolderTemplateText(drawing, app.isODF()); }
                }
            }

            return templateText;
        };

        /**
         * Changing the visibility of a slide, that is specified by a drawing that is
         * located in the slide. This is necessary for formatting reasons, where the
         * slide must not be invisible. In this case the drawing dimensions would not
         * be correct.
         *
         * @param {HTMLElement|jQuery} node
         *  The node in the affected slide or the slide itself.
         *
         * @param {Object} options
         *  Optional parameters:
         *  @param {Boolean} [options.makeVisible=true]
         *   Whether the slide shall be made visible or not.
         *  @param {Boolean} [options.invisibleWithReflow=true]
         *   Using 'invisibleWithReflow' class.
         *
         * @returns {Boolean}
         *  Whether the visibility of the slide has changed.
         */
        this.handleContainerVisibility = function (node, options) {

            var // whether the visibility was changed
                visibilityChanged = false,
                // the slide node (containing the specified node)
                slide = DOM.isSlideNode(node) ? $(node) : $(node).closest(DOM.SLIDE_SELECTOR),
                // whether the container shall be made visible or not
                makeVisible = getBooleanOption(options, 'makeVisible', true),
                // activates the "invisiblewithreflow" which is only active if slide is invisible
                // instead of display none, it uses visibility "hidden" to keep dom-layout
                invisibleWithReflow = getBooleanOption(options, 'invisibleWithReflow', false);

            if (slide && slide.length > 0) {

                if (makeVisible && slide.hasClass('invisibleslide')) {
                    slide.removeClass('invisibleslide'); // making slide visible for formatting
                    visibilityChanged = true;
                } else if (!makeVisible && !slide.hasClass('invisibleslide')) {
                    slide.addClass('invisibleslide'); // making slide invisible after formatting
                    visibilityChanged = true;
                }

                if (invisibleWithReflow & !slide.hasClass('invisiblewithreflow')) {
                    slide.addClass('invisiblewithreflow');
                } else if (!invisibleWithReflow && slide.hasClass('invisiblewithreflow')) {
                    slide.removeClass('invisiblewithreflow');
                }
            }

            return visibilityChanged;
        };

        /**
         * Getting the selection in the slide pane.
         *
         * @returns {Number[]}
         *  An Array containing the position as index from all selected slides in the slide pane.
         */
        this.getSlidePaneSelection = function () {
            return app.getView().getSlidePane().getSlidePaneSelection();
        };

        /**
         * Receiving some data from the model, that need to be restored after applying a snap shot.
         *
         * @returns {Object}
         *  An object containing some data from the model, that need to be restored after applying a snap shot.
         */
        this.getModelDataObject = function () {
            return { mainSlideCounter, activeSlideId, isMasterView };
        };

        /**
         * Setting some data into the model during applying a snapshot.
         *
         * @param {Object} [data]
         *  An optional object, that contains the values of some data from the model.
         */
        this.setModelDataObject = function (data) {

            if (data) {
                if (_.isNumber(data.mainSlideCounter)) { mainSlideCounter = data.mainSlideCounter; }
                if (_.isString(data.activeSlideId)) { activeSlideId = data.activeSlideId; }
                isMasterView = !!data.isMasterView;
            }

        };

        /**
         * Getting the paragraph that contains the current selection.
         *
         * @returns {jQuery}
         *  The paragraph that contains the current selection. If there is
         *  no paragraph, the jQuery collection contains no element.
         */
        this.getSelectedParagraph = function () {
            return selectedParagraph;
        };

        /**
         * Setting the paragraph that contains the current selection.
         *
         * @param {jQuery} [para]
         *  Setting the paragraph that contains the current selection. If the
         *  paragraph is not specified, the global 'selectedParagraph' is set
         *  to an empty jQuery collection.
         */
        this.setSelectedParagraph = function (para) {
            selectedParagraph = para || $();
        };

        /**
         * Tries to find layout id with queried slide type. If no id is found, returns first layout id from list.
         *
         * @param {String} type
         *  Queried type of slide.
         *
         * @returns {String|Null}
         */
        this.getLayoutIdFromType = function (type) {
            var layoutIDs = self.getAllLayoutSlideIds() || [];
            var first = _.first(layoutIDs) || null;
            var result = layoutIDs.find(layoutID => type === self.getSlideFamilyAttributeForSlide(layoutID, 'slide', 'type'));
            return result || first;
        };

        /**
         * A function, that returns all visible drawings of the active slide. This includes the drawings
         * of all three layers. Only drawings that are directly on the slide are collected, no grouped
         * drawings.
         *
         * @returns {jQuery|Null}
         *  A collector with all visible drawings on the active slide. Or Null, if the slide contains no
         *  visible drawing.
         */
        this.getAllVisibleDrawingsOnSlide = function () {

            // the ID of the active slide
            var currentId = activeSlideId;
            // the slide ID set
            var allSlideIdSet = self.getSlideIdSet(); // return { id: slideId, layoutId: layoutId, masterId: masterId };
            // the collector for all drawings
            var allDrawings = $();
            // whether non-placeholder drawings on the layout slide are visible
            var ignoreLayout = false;
            // whether non-placeholder drawings on the master slide are visible
            var ignoreMaster = false;
            // whether placeholder drawings on the layout slide can be ignored
            var ignoreLayoutPlaceHolder = true;
            // whether placeholder drawings on the master slide can be ignored
            var ignoreMasterPlaceHolder = true;
            // the selector for finding the drawings on the slide
            var selector = 'div.drawing';
            // the selector for excluding the place holder drawings on the slide
            var placeholderexclusion = ':not([data-placeholdertype])';

            if (self.isLayoutOrMasterId(currentId)) {
                if (self.isLayoutSlideId(currentId)) {
                    ignoreLayoutPlaceHolder = false; // not excluding layout placeholder drawings on layout slide
                } else {
                    ignoreMasterPlaceHolder = false; // not excluding master placeholder drawings on master slide
                }
            }

            // iterating over the keys 'id', 'layoutId' and 'masterId' of the object 'allSlideIdSet'

            if (allSlideIdSet.id) {
                allDrawings = allDrawings.add(self.getSlideById(allSlideIdSet.id).children(selector));
                if (!self.isFollowMasterShapeSlide(allSlideIdSet.id)) {
                    ignoreLayout = true;
                    ignoreMaster = true;
                }
            }

            if (allSlideIdSet.layoutId && !ignoreLayout) {

                allDrawings = allDrawings.add(self.getSlideById(allSlideIdSet.layoutId).children(selector + (ignoreLayoutPlaceHolder ? placeholderexclusion : '')));
                if (!self.isFollowMasterShapeSlide(allSlideIdSet.layoutId)) {
                    ignoreMaster = true;
                }
            }

            if (allSlideIdSet.masterId && !ignoreMaster) {
                allDrawings = allDrawings.add(self.getSlideById(allSlideIdSet.masterId).children(selector + (ignoreMasterPlaceHolder ? placeholderexclusion : '')));
            }

            return allDrawings.length > 0 ? allDrawings : null;
        };

        /**
         * Finding for a specified slide ID or array of slide IDs that ID, that
         * belongs to a slide, that has a defined background.
         * If only a string is specified, the target chain is used to find the
         * slide with background. For example, the parameter can specify a document
         * slide. Then it is possible via 'getTargetChain' to find out, if the
         * corresponding layout or master slide specify a background.
         *
         * @param {String|String[]} ids
         *  An array of slide IDs or only one slide Id.
         *
         * @returns {String|null}
         *  The ID of the top most slide, that has a defined background. Or null,
         *  if no slide has a specified background.
         */
        this.getMostTopBackgroundSlide = function (ids) {

            var // the id of the top most slide that has a background defined
                backgroundSlideId = null;

            if (_.isString(ids)) { ids = self.getTargetChain(ids); }

            backgroundSlideId = _.find(ids, function (id) {
                var fillAttrs = self.getSlideAttributesByFamily(id, 'fill');
                return fillAttrs && fillAttrs.type && fillAttrs.type !== 'none'; // -> this slide has a specified background
            });

            return backgroundSlideId;
        };

        /**
         * Creates and returns helper jQuery node, used for preview of transparend background image,
         * but only as long as dialog is open. It also adds class to active slide to remove it's css background property.
         * It must be destroyed before any operation is applied!
         *
         * @param {jQuery} $slide
         *  Slide DOM node for which helper node is created and appended after.
         */
        this.getHelpBackgroundNode = function ($slide) {
            var $tempPreviewSlide = $('<div class="temp-preview-slide">');
            $slide.addClass(DOM.LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS).parent().append($tempPreviewSlide);

            return $tempPreviewSlide;
        };

        /**
         * Removes helper node from DOM and restores original background css property for active node.
         *
         * @param {jQuery} $slide
         *  Slide node for which live preview is made.
         */
        this.removeHelpBackgroundNode = function ($slide) {
            if ($slide && $slide.length) {
                DOM.getTempSlideNodeForSlide($slide).remove();
                $slide.removeClass(DOM.LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS);
            }
        };

        /**
         * Check, if a specified selection, is a complete content selection of a place holder drawing.
         *
         * @param {Number[]} start
         *  The logical start position.
         *
         * @param {Number[]} end
         *  The logical end position.
         *
         * @returns {Boolean}
         *  Whether the specified start and end position describe a range that contains the complete content of the
         *  specified drawing (that must be a place holder drawing).
         */
        this.isCompletePlaceHolderContentSelection = function (start, end, drawing) {

            var length = start.length;
            var isCompleteSelection = false;
            var endParagraph = null;

            // Fast exit:
            // - the final two number of start position must be 0.
            // - the drawing must be a place holder drawing.
            if (start[length - 1] !== 0 || start[length - 2] !== 0 || !drawing || drawing.length === 0 || !isPlaceHolderDrawing(drawing)) { return isCompleteSelection; }

            // start and end position must be identical, except the last two numbers
            if (!hasSameParentComponent(start, end, 2)) { return false; }

            endParagraph = getParagraphElement(self.getCurrentRootNode(), _.initial(end));

            if (endParagraph && $(endParagraph).next(DOM.PARAGRAPH_NODE_SELECTOR).length === 0 && getParagraphNodeLength(endParagraph) === _.last(end)) {
                isCompleteSelection = true;
            }

            return isCompleteSelection;
        };

        /**
         * Returns css selector filter to specify the event target for the selection box to
         * create multi drawing selections.
         *
         * @returns {String}
         */
        this.getMultiSelectionBoxFilterSelector = function () {
            return PresentationModel.MULTI_SELECTION_BOX_FILTER;
        };

        /**
         * Getting a container with all available footer types.
         *
         * @returns {String[]}
         *  A container with all footer types.
         */
        this.getPresentationFooterTypes = function () {
            return PresentationModel.FOOTER_TYPES;
        };

        /**
         * Checks, if a specified drawing type is one of the available footer types ('dt', ftr' or 'sldNum').
         *
         * @param {String} drawingType
         *  The drawing type.
         *
         * @returns {Boolean}
         *  Whether the specified drawing type is a footer type.
         */
        this.isPresentationFooterType = function (drawingType) {
            return _.contains(PresentationModel.FOOTER_TYPES, drawingType);
        };

        /**
         * Getting a container with all available ODF master types.
         *
         * @returns {String[]}
         *  A container with all ODF master types.
         */
        this.getODFMasterTypes = function () {
            return PresentationModel.ODF_MASTER_TYPES;
        };

        /**
         * Check, whether the selection contains a place holder drawing, that is in the master view in ODF
         * handled as read-only drawing. At least it is not possible to remove the drawing or to modify its
         * content (only setting attributes is allowed).
         *
         * @returns {Boolean}
         *  Whether the selection contains a place holder drawing, that is in the master view in ODF
         *  handled as read-only drawing.
         */
        this.isODFReadOnyPlaceHolderDrawingProcessing = function () {

            var isReadOnly = false;

            if (app.isODF() && isMasterView) {
                if ((selection.isAdditionalTextframeSelection() && isODFReadOnlyPlaceHolderDrawing(selection.getSelectedTextFrameDrawing())) ||
                    (selection.isDrawingSelection() && isODFReadOnlyPlaceHolderDrawing(selection.getSelectedDrawing())) ||
                    (selection.isMultiSelection() && _.isObject(selection.getAllSelectedDrawingNodes().find(isODFReadOnlyPlaceHolderDrawing)))) {
                    isReadOnly = true;
                }
            }

            return isReadOnly;
        };

        /**
         * Check, if the insertion of a paragraph via operation as an replacement of an implicit paragraph
         * requires additional attributes. This is for example the case for paragraphs in title place holder
         * drawings in ODP, because they must not inherit bullet attributes via inheritance.
         *
         * @param {Node|jQuery} para
         *  The implicit paragraph that will be replaced via operation.
         *
         * @returns {Object|Null}
         *  The attributes that are required for the insertParagraph operation.
         */
        this.getReplaceImplicitParagraphAttrs = function (para) {

            var // the drawing containing the specified paragraph
                drawing = null,
                // the explicit attributes at the implicit paragraph
                expAttrs = null,
                // the attributes for the paragraph operation
                attrs = null;

            if (app.isODF()) {

                drawing = $(para).closest('div.drawing');

                if (drawing.length > 0 && isTitleOrSubtitlePlaceHolderDrawing(drawing)) {
                    attrs = { paragraph: { bullet: { type: 'none' }, indentLeft: 0, indentFirstLine: 0 } }; // no bullets in title place holders via inheritance
                }

                // also checking implicit attributes that can be transferred to real paragraph
                expAttrs = getExplicitAttributeSet(para);

                if (expAttrs) {
                    attrs = attrs || {};
                    _.extend(attrs, expAttrs);
                }

            }

            return attrs;
        };

        /**
         * ODP only.
         * After loading the document, the slide pane needs to be informed about the state of the fields on the
         * master slide for every single document slide. This is necessary to update the visibility and the content
         * of the date fields, the slide number fields and the footer fields that are located on the master slide
         * but shown on the document slides with specific values.
         * The event masterfield:update is used for this field udpates. After successful loading it must be
         * triggered once with the master field information for every document slide, so that the slide pane can
         * display each slide correctly.
         * Info: This function is also called, if the user switches from master view to document view, so that the
         * fiels on the document slides can be updated.
         */
        this.createMasterSlideFieldsEvent = function () {

            var // the collector for the master field objects that is part of the event
                odfSlideCollector = [];

            // Collect slide information for every document slide
            _.each(self.getStandardSlideOrder(), function (slideID) {
                var slideAttrs = self.slideStyles.getElementAttributes(self.getSlideById(slideID)); // using the full stack of attributes
                collectODFMasterSlideUpdates(slideID, slideAttrs.slide, odfSlideCollector);
            });

            // also sending the information about the place holders on the master slide(s)
            collectODFMasterSlideDefaultValues(odfSlideCollector);

            if (odfSlideCollector.length > 0) {
                self.trigger('masterfield:update', odfSlideCollector);
            }
        };

        /**
         * Save used drawing id into collection, for further unique id generation and for check, if a new generated
         * user id is or was already used. This might also happen, if a document is opened very often.
         *
         * @param {String} id
         *  An already used drawing id.
         */
        this.storeDrawingId = function (id) {
            var intId = parseInt(id, 10);
            if (_.isFinite(intId) && !drawingIdCollection[intId]) {
                drawingIdCollection[intId] = 1;
            }
            // collecting also the '4' digit user IDs
            if (id.length > 4) { drawingUserIdCollection.add(id.slice(0, 4)); }
        };

        /**
         * Receiving a user ID that is used for identifying drawings. This is a stringified number between
         * 1000 and 9999.
         *
         * @returns {String}
         *  The 4 digit user ID used to set marker at drawings
         */
        this.getUserDrawingId = function () {
            return (userDrawingId ??= generateDigitId());
        };

        /**
         * Creating a unique id for drawings using a user specific ID (caused by OT) and a counter part.
         *
         * @returns {String}
         *  The full drawing id containing the user id and the drawing counter.
         */
        this.getNewDrawingId = function () {
            var foundNewId = false;
            var drawingId = null;
            while (!foundNewId) {
                drawingId = appendDrawingCounter(self.getUserDrawingId());
                if (!drawingIdCollection[drawingId]) { foundNewId = true; }
            }
            return drawingId;
        };

        /**
         * Preparing that data object for local storage for slide nodes.
         *
         * @param {jQuery} slideNode
         *  The slide node. The caller of this function is responsible to
         *  check validity of the node.
         *
         * @param {Object} dataObject
         *  The data object of the slideNode that is returned by the jQuery
         *  data function.
         *
         * @returns {Object}
         *  The data object. This object might be modified or even generated
         *  within this function.
         */
        this.prepareSlideNodesForStorage = function (slideNode, dataObject) {

            // the ID of the slide
            var slideId = self.getSlideId(slideNode);
            // the list styles of the slide
            var listStyles = self.getAllListStylesAttributesOfSlide(slideId);

            // attaching the listStyles of slides to the data object
            if (listStyles) {
                dataObject = dataObject || {};
                dataObject.attributes = dataObject.attributes || {};
                dataObject.attributes.listStyles = listStyles;
            }

            return dataObject;
        };

        /**
         * Preparing drawing nodes for the local storage. The 'src' property of the image nodes is
         * renamed to improve the loading from local storage. If the string is attached to the DOM again,
         * the browser does not try to load all images immediately.
         *
         * @param {jQuery} drawing
         *  The jQuerified drawing node.
         *
         * @param {Object} dataObject
         *  The data object of the drawing node that is returned by the jQuery
         *  data function.
         *
         * @returns {Object}
         *  The data object. This object might be modified or even generated
         *  within this function.
         */
        this.prepareDrawingNodesForStorage = function (drawing, dataObject) {

            // the list styles of the slide
            var listStyles = null;
            // the image node in the drawing
            var imageNode = null;
            // the source property of the image node
            var src = null;

            if (getDrawingType(drawing) === 'image') {

                imageNode = drawing.find('img');
                src = imageNode.attr('src');

                if (src) {
                    imageNode.removeAttr('src');
                    imageNode.attr(DOM.IMAGESOURCE_ATTR, src);
                }

            } else if (isPlaceHolderDrawing(drawing)) {

                // updating the list styles from the drawing
                listStyles = self.drawingStyles.getAllListStylesAtPlaceHolderDrawing(drawing);

                if (listStyles) {
                    dataObject = dataObject || {};
                    dataObject.attributes = dataObject.attributes || {};
                    dataObject.attributes.listStyle = listStyles.listStyle;
                }

            }

            return dataObject;
        };

        /**
         * Helper function that renames the attribute 'imagesource' saved at the image node
         * back to its real name 'src'. With this trick, not all images are requested by the
         * browser after the html string is attached to the DOM in fast load or local storage
         * loading process.
         *
         * @param {jQuery} drawing
         *  The jQuerified drawing node.
         */
        this.prepareImageNodeFromStorage = function (drawing) {

            var imageNode = drawing.find('img');
            var imageSource = imageNode.attr(DOM.IMAGESOURCE_ATTR);

            if (imageSource) {
                imageNode.removeAttr(DOM.IMAGESOURCE_ATTR);
                imageNode.attr('src', imageSource);
            }
        };

        /**
         * Saving the jQueryfied drawing node, that might be shifted into edit mode.
         *
         * @param {jQuery} drawing
         *  The jQueryfied drawing node.
         */
        this.setTouchStartDrawing = function (drawing) {
            touchStartDrawing = drawing;
        };

        /**
         * Getting the jQueryfied drawing node, that was saved inside the
         * 'touchstart' handler and that might be shifted into edit mode.
         *
         * @returns {jQuery} drawing
         *  The jQueryfied drawing node.
         */
        this.getTouchStartDrawing = function () {
            return touchStartDrawing;
        };

        /**
         * Saving the jQueryfied group drawing node, that might be shifted into edit mode.
         *
         * @param {jQuery} drawing
         *  The jQueryfied group drawing node.
         */
        this.setTouchStartDrawingGroup = function (drawing) {
            touchStartDrawingGroup = drawing;
        };

        /**
         * Getting the jQueryfied group drawing node, that was saved inside the
         * 'touchstart' handler and that might be shifted into edit mode.
         *
         * @returns {jQuery} drawing
         *  The jQueryfied drawing node.
         */
        this.getTouchStartDrawingGroup = function () {
            return touchStartDrawingGroup;
        };

        /**
         * Saving the 'touchstart' event for later usage in 'touchend'.
         *
         * @param {jQuery.Event} event
         *  The 'touchstart' event.
         */
        this.setTouchStartEvent = function (event) {
            touchStartEvent = event;
        };

        /**
         * Getter for the saved 'touchstart' event.
         *
         * @returns {jQuery.Event} event
         *  The 'touchstart' event.
         */
        this.getTouchStartEvent = function () {
            return touchStartEvent;
        };

        /**
         * Setter for the information, if the 'touchstart' event received a 'preventDefault'.
         * This is important on IOS where it must be avoided, that the edit mode is active
         * without visible keyboard.
         *
         * @param {Boolean} value
         *  Whether 'preventDefault' was called on the corresponding 'touchstart' event.
         */
        this.setPreventDefaultActivated = function (value) {
            touchStartPreventDefault = value;
        };

        /**
         * Activating the edit mode on a touch device for a drawing with text content. This requires:
         * - this is a 'touchend' event
         * - in the previous touchstart the drawing node was saved together with the touchstart event
         * - no move operation was generated (dependent from the timer MIN_MOVE_DURATION)
         *
         * @param {jQuery.Event} event
         *  The 'touchend' event.
         */
        this.handleTouchEditMode = function (event) {

            if (event.type === 'touchend' && app.isEditable()) {

                // the drawing saved during 'touchstart'
                var currentDrawing = self.getTouchStartDrawing();
                // the grouped drawing saved during 'touchstart'
                var currentGroupDrawing = self.getTouchStartDrawingGroup();
                // the event saved during 'touchstart'
                var touchStartEvent = self.getTouchStartEvent();
                // the drawing that gets the active-edit text frame
                var selectionDrawing = null;

                if (currentDrawing && !touchStartPreventDefault && currentDrawing.length > 0 && touchStartEvent && ((event.timeStamp - touchStartEvent.timeStamp) < MIN_MOVE_DURATION)) {
                    selectionDrawing = currentGroupDrawing ? currentGroupDrawing : currentDrawing;
                    selectionDrawing.attr('contenteditable', 'true').addClass('active-edit');
                    if (selectionDrawing.data('selection')) { selectionDrawing.data('selection').addClass('active-edit'); }
                }
            }
        };

        /**
         * Checking and replacing template text in a specified node.
         * If this node is a text span or a paragraph with a template text, this text is removed.
         * In the future it might be necessary to handling drawings or content nodes, too.
         *
         * @param {HTMLElement|jQuery} node
         *  The element node, that is investigated for template text spans.
         *
         * @returns {Boolean}
         *  Whether a template text was removed in the specified node.
         */
        this.handleTemplateTextForNode = function (node) {
            var replaced = false;
            if (node && DOM.isParagraphNode(node)) { node = $(node).children('span').first(); }
            if (DOM.isTextFrameTemplateTextSpan(node)) {
                self.removeTemplateTextFromTextSpan(node);
                replaced = true;
            }
            return replaced;
        };

        /**
         * OT: Checking, if an external operation leads to an 'unrepairable' local selection, so that a simplified
         *     selection (in OX Presentation the slide selection) must be activated. This happens for example in
         *     OX Presentation after an external group or ungroup operation, if the current selection contains at
         *     least one of the grouped or ungrouped drawings.
         *
         * @param {Object} operation
         *  The external operation that will be checked.
         *
         * @param {Number[]} currentPosition
         *  The current start position of the selection.
         *
         * @param {String|undefined} currentTarget
         *  The current target of the selection.
         */
        this.checkForSimpleSelection = function (operation, currentPosition, currentTarget) {

            var forceSimpleSelection = false;

            if (currentTarget === null) { currentTarget = undefined; }

            if (!selection.isSlideSelection()) {
                if (operation.name === Op.GROUP) {
                    if (currentPosition[0] === operation.start[0] && operation.target === currentTarget && _.contains(operation.drawings, currentPosition[1])) {
                        forceSimpleSelection = true;
                    }
                } else if (operation.name === Op.UNGROUP) {
                    if (currentPosition[0] === operation.start[0] && operation.target === currentTarget && currentPosition[1] === operation.start[1]) {
                        forceSimpleSelection = true;
                    }
                }
            }

            return forceSimpleSelection;
        };

        /**
         * OT: Setting a simplified selection. In the case of OX Presentation this is a slide selection.
         */
        this.setSimpleSelection = function () {
            selection.setSlideSelection();
        };

        /**
         * OT: Handling an external insertSlide operation, if there is currently an empty selection (DOCS-1734).
         *
         * @param {Object} operation
         *  The external operation that will be checked.
         *
         * @returns {Boolean}
         *  Whether an empty selection was handled with the external operation.
         */
        this.handledEmptySelection = function (operation) {
            if (selection.isEmptySelection() && operation.name === Op.SLIDE_INSERT) {
                self.setActiveSlideId(self.getIdOfSlideOfActiveViewAtIndex(0)); // activating the new inserted single slide
                return true;
            }
            return false;
        };

        /**
         * When the document needs to be flushed (print, download, close, ...) it might be necessary to send
         * an additional setAttributes operation to the filter, so that PP shows the correct height for text
         * frames with autoresize height. This operation is typically sent, when the selection is no longer
         * inside the drawing. But if the cursor is still inside the drawing, the required operation is
         * triggered from the prepareFlushHandler, that calls this function.
         */
        this.checkTextFrameLeaveHandler = function () {
            if (selection.isAdditionalTextframeSelection()) {
                selection.trigger('docs:textframe:leave', selection.getSelectedTextFrameDrawing());
            }
        };

        /**
         * Whether the remote selections shall be shown. In presentation mode, the remote selection
         * shall not be visible.
         *
         * Overwriting the base class.
         *
         * @returns {Boolean}
         *  Whether the remote selections shall be shown.
         */
        this.showRemoteSelections = function () {
            return !presentationModeManager.isPresentationMode();
        };

        /**
         * Whether popup menus must be blocked. In presentation mode, the popup menu for the
         * collaborators must not be shown (DOCS-2831).
         *
         * Overwriting the base class.
         *
         * @returns {Boolean}
         *  Whether popup menus must be blocked.
         */
        this.blockPopupMenus = function () {
            return presentationModeManager.isPresentationMode();
        };

        /**
         * Finding the best focus node in the slide view. This can be the clipboard node or a text frame
         * node, when the text cursor is inside a drawing.
         */
        this.setFocusToSlideView = function () {

            const selection = self.getSelection();

            if (selection.isAdditionalTextframeSelection()) {
                const textFrameNode = selection.getSelectedTextFrameDrawing().find(TEXTFRAME_NODE_SELECTOR);
                if (textFrameNode.length) {
                    setFocus(textFrameNode);
                } else {
                    selection.setFocusIntoClipboardNode();
                }
            } else {
                // if (selection.isSlideSelection()) { // setting drawing selection keeps focus in the slide pane (?)
                //     if (self.getActiveSlide().children('.drawing').length) {
                //         const startPosition = selection.getStartPosition();
                //         selection.setTextSelection(_.copy(startPosition), increaseLastIndex(startPosition, 1));
                //     }
                // }
                selection.setFocusIntoClipboardNode();
            }

        };

        /**
         * Forcing the formatting of specified slides. This is required for example after scrolling in the
         * side pane or resizing the side pane, when not all slides are formatted.
         *
         * @param {String[]} slideIDs
         *  An array with all required slideIDs.
         */
        this.forceFormattingOfSlideGroup = function (slideIDs) {
            slideIDs.forEach(slideId => { slideFormatManager.forceSlideFormatting(slideId); });
        };

        /**
         * Whether the slide specified by its ID is an unformatted slide. It is
         * possible, that the slide is currently formatted.
         *
         * @param {String} slideID
         *  The id of a slide.
         *
         * @returns {Boolean}
         *  Whether the slide specified by its ID is an unformatted slide.
         */
        this.isUnformattedSlide = function (slideID) {
            return slideFormatManager.isUnformattedSlide(slideID);
        };

        // registering the handler functions for the operations ---------------

        this.registerPlainOperationHandler(Op.MASTER_SLIDE_INSERT, self.insertMasterSlideHandler);
        this.registerPlainOperationHandler(Op.LAYOUT_SLIDE_INSERT, self.insertLayoutSlideHandler);
        this.registerPlainOperationHandler(Op.SLIDE_INSERT, self.insertSlideHandler);
        this.registerPlainOperationHandler(Op.TABLE_INSERT, self.insertTableHandler);
        this.registerPlainOperationHandler(Op.INSERT_DRAWING, self.insertDrawingHandler);
        this.registerPlainOperationHandler(Op.INSERT_COMMENT, self.handleInsertComment);
        this.registerPlainOperationHandler(Op.CHANGE_COMMENT, self.handleChangeComment);
        this.registerPlainOperationHandler(Op.DELETE_COMMENT, self.handleDeleteComment);
        this.registerPlainOperationHandler(Op.LAYOUT_SLIDE_MOVE, self.moveLayoutSlideHandler);
        this.registerPlainOperationHandler(Op.LAYOUT_CHANGE, self.changeLayoutHandler);
        this.registerPlainOperationHandler(Op.MASTER_CHANGE, self.changeMasterHandler);
        this.registerPlainOperationHandler(Op.SLIDE_MOVE, self.moveSlideHandler);
        this.registerPlainOperationHandler(Op.MOVE, self.moveHandler);
        this.registerPlainOperationHandler(Op.DELETE_TARGET_SLIDE, function () { return true; }); // this operation is only required for OT. There is nothing to do.

        // initialization -----------------------------------------------------

        pagediv = self.getNode();

        // remove in ios to prevent that the browser starts a text selection on a longer tap at the page
        if (self.getSlideTouchMode()) { pagediv.removeClass('user-select-text'); }

        selection = self.getSelection();

        this.tableStyles.setUseDefaultStyleIfMissing(false); // no fallback to default table style in Presentation app (55547)

        // setting the handler function for moving and resizing drawings
        self.getSelection().setDrawingResizeHandler(drawDrawingSelection);

        // setting the manager that handles formatting of slides (for performance reasons)
        slideFormatManager = this.member(new SlideFormatManager(this));

        // setting the manager that handles visibility of slides in the DOM (for performance reasons)
        slideVisibilityManager = this.member(new SlideVisibilityManager(this));

        // setting the manager that handles the presentation mode
        presentationModeManager = this.member(new PresentationModeManager(app));

        // This field is required because of problematic typing for "getFieldManager()""
        this.slideFieldManager = new SlideFieldManager(this);

        // setting handler for handling fields in presentation
        self.setFieldManager(this.slideFieldManager);

        // create the number formatter
        self.setNumberFormatter(new NumberFormatter(this));

        self.waitForImportSuccess(importSuccessHandler);

        self.one('fastload:done fastemptyload:done update:model', updatePresentationModel);

        //        // register the listener for 'textframeHeight:update'.
        //        self.on('textframeHeight:update', updateVerticalDrawingPosition);
        //
        //        // register the listener for 'addtionalTextFrame:selected' of the selection
        //        self.listenTo(selection, 'addtionalTextFrame:selected', saveVerticalPositionAttributes);

        // registering the default list styles
        self.defAttrPool.on("change:defaults", registerDefaultTextListStyles);

        // registering the handler for document reload event. This happens, if the user cancels a long running action
        self.on('document:reloaded', documentReloadHandler);

        // registering the handler for document reload after event. This happens, if the user cancels a long running action
        self.on('document:reloaded:after', documentReloadAfterHandler);

        // registering the handler for the event 'image:loaded'
        self.on('image:loaded', imageLoadedHandler);

        // update theme target chain of the active slide, if the layout slide changes
        self.on('change:layoutslide', function () {
            activeTargetChain = self.getTargetChain(activeSlideId);
        });

        // a template text in a place holder drawing on the layout slide was inserted
        self.on('templatetext:updated', templateTextHandler);

        // preparing a slide before it will be removed
        self.on('delete:paragraph:before', prepareSlideDeletion);

        // registering handler for undo:before and redo:before
        self.getUndoManager().on('undo:before', (operations, options) => undoRedoBeforeHandler(false, operations, options));
        self.getUndoManager().on('redo:before', (operations, options) => undoRedoBeforeHandler(true, operations, options));

        // registering handler for undo:after and redo:after
        self.getUndoManager().on('undo:after', (operations, options) => undoRedoAfterHandler(false, operations, options));
        self.getUndoManager().on('redo:after', (operations, options) => undoRedoAfterHandler(true, operations, options));

        // registering process mouse down handler at the page
        pagediv.on({ 'mousedown touchstart': self.getPageProcessMouseDownHandler() });
        if (self.getListenerList()) { self.getListenerList()['mousedown touchstart'] = self.getPageProcessMouseDownHandler(); }

        // destroy all class members on destruction
        this.registerDestructor(function () {
            self.trigger("delete:allslides:before");
            selectionBox?.deRegisterSelectionBox();
        });
    }
}

// constants --------------------------------------------------------------

/**
 * The attributes that are used for new inserted drawings.
 */
PresentationModel.INSERT_DRAWING_ATTRIBUTES = {
    left: 5000,
    top: 3000
};

/**
 * The type used to recognize master slides.
 */
PresentationModel.TYPE_MASTER_SLIDE = 'masterSlide';

/**
 * The type used to recognize layout slides.
 */
PresentationModel.TYPE_LAYOUT_SLIDE = 'layoutSlide';

/**
 * The type used to recognize 'standard' slides.
 */
PresentationModel.TYPE_SLIDE = 'standardSlide';

/**
 * A CSS filter to specify the event target for the selection box to
 * create multi drawing selections.
 */
PresentationModel.SET_SELECTION_MODE = 'setselection';

/**
 * A CSS filter to specify the event target for the selection box to
 * create multi drawing selections.
 */
PresentationModel.MULTI_SELECTION_BOX_FILTER = DOM.SLIDE_SELECTOR + ', ' + DOM.APPCONTENT_NODE_SELECTOR + ', ' + DOM.APP_CONTENT_ROOT_SELECTOR;

/**
 * A list of the footer place holder drawing types.
 */
PresentationModel.FOOTER_TYPES = ['dt', 'ftr', 'sldNum'];

/**
 * A list of those types allowed in ODF master slide.
 */
PresentationModel.ODF_MASTER_TYPES = ['title', 'body', 'dt', 'ftr', 'sldNum'];

/**
 * A list of those place holder drawing types, for that the selection is NOT automatically expanded
 * to the complete paragraph, if character attributes are assigned.
 */
PresentationModel.NOT_COMPLETE_SELECTION_PLACEHOLDER_TYPES = ['ftr'];

/**
 * A list of all operations, that insert a slide
 */
PresentationModel.INSERT_SLIDE_OPERATIONS = [Op.SLIDE_INSERT, Op.LAYOUT_SLIDE_INSERT, Op.MASTER_SLIDE_INSERT];

/**
 * A list of all operations, that move a slide
 */
PresentationModel.MOVE_SLIDE_OPERATIONS = [Op.SLIDE_MOVE, Op.LAYOUT_SLIDE_MOVE];

/**
 * A list of all operations, that change the positions of the document slides
 */
PresentationModel.SLIDE_POSITION_OPERATIONS = [Op.DELETE, Op.SLIDE_INSERT, Op.SLIDE_MOVE];

// exports ================================================================

export default PresentationModel;
