/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { PARAGRAPH_NODE_SELECTOR, SLIDECONTAINER_CLASS, createSlideNode, getPageContentNode, isEmptyTextframe,
    isImplicitParagraphNode, setTargetContainerId } from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, decreaseLastIndex, getOxoPosition, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { getBooleanOption, getEmptySlideBackgroundAttributes, getStringOption } from '@/io.ox/office/textframework/utils/textutils';

import { DELETE, DELETE_TARGET_SLIDE, INSERT_DRAWING, LAYOUT_CHANGE, LAYOUT_SLIDE_INSERT, LAYOUT_SLIDE_MOVE, MASTER_CHANGE,
    MASTER_SLIDE_INSERT, PARA_INSERT, SET_ATTRIBUTES, SLIDE_INSERT, SLIDE_MOVE } from '@/io.ox/office/presentation/utils/operations';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { CONTENTBODY, NO_TEXT_PLACEHOLDER_TYPES, getPlaceHolderDrawingType, isEmptyPlaceHolderDrawing,
    isUserModifiedPlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';

// mix-in class SlideOperationMixin ======================================

/**
 * A mix-in class for the document model class PresentationModel providing
 * the slide operation handling used in a presentation document.
 */
export default function SlideOperationMixin(app) {

    var // self reference for local functions
        self = this;

    // private methods ----------------------------------------------------

    /**
     * Handler for inserting a new master slide.
     *
     * @param {String} id
     *  The id of the master slide.
     *
     * @param {Number} [index]
     *  The 0-based index of the master slide in the list of all master slides. If not specified,
     *  the master slide is appended as last master. This is not the index of the global container
     *  'masterSlideOrder'.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied to the new inserted layout slide, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @returns {Boolean}
     *  Whether the master slide has been inserted successfully.
     */
    function implInsertMasterSlide(id, index, attrs) {

        var // the new slide node
            slide = null,
            // the layer node for the master slides
            masterSlideLayerNode = self.getOrCreateMasterSlideLayerNode(),
            // the id of the master slide, BEFORE that the current master slide needs to be inserted
            destinationSlideId = null,
            // the index for the global container 'masterSlideOrder'
            masterIndex = -1;

        // getting the valid destination slide, if the new slide is NOT inserted at the end of the container
        if (_.isNumber(index) && index > -1) { destinationSlideId = getMasterSlideAtSpecifiedPosition(index); }

        // adding the master slide into the master slide layer node
        slide = appendSlideToLayerNode(masterSlideLayerNode, id, destinationSlideId);

        // registering the id at the slide
        setTargetContainerId(slide, id);

        // for the model the index must be the index in the global container 'masterSlideOrder'
        // -> the new master slide gets the index of that master slide, that was used to insert it in the DOM. If this does
        //    not exist, the new master slide is inserted at the end of the global container (layout slides that use this
        //    new master slide cannot exist yet).
        // -> always using masterIndex -1 during loading.
        if (self.isImportFinished() && destinationSlideId) { masterIndex = _.indexOf(self.getMasterSlideOrder(), destinationSlideId); }

        // registering the slide in the model
        self.addIntoSlideModel(slide, id, { type: self.getMasterSlideType(), attrs, index: masterIndex });

        // apply the passed slide attributes
        self.slideStyles.setElementAttributes(slide, attrs ? attrs : {}); // 56111, must happen after 'addIntoSlideModel'

        // applying all attributes
        if (self.isImportFinished()) {
            self.slideStyles.updateElementFormatting(slide);
            slide.addClass('invisibleslide'); // all new slides need to be invisible (required for undo after delete)
        }

        return true;
    }

    /**
     * Handler for inserting a new layout slide.
     *
     * @param {String} id
     *  The id of the layout slide.
     *
     * @param {String} target
     *  The id of the master slide, that is the base for the specified layout slide.
     *
     * @param {Number} [index]
     *  The 0-based index of the layout slide behind the master slide. If not specified, the
     *  layout slide will be appended as last layout slide of its master slide.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied to the new inserted layout slide, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @returns {Boolean}
     *  Whether the layout slide has been inserted successfully.
     */
    function implInsertLayoutSlide(id, target, index, attrs) {

        var // the layout slide node
            slide = null,
            // the layer node for the layout slides
            layoutSlideLayerNode = self.getOrCreateLayoutSlideLayerNode(),
            // the id of the layout slide, BEFORE that the current layout slide needs to be inserted
            destinationSlideId = null,
            // whether the document contains more than one master slide
            isMultiMasterSlide = self.isMultiMasterSlideDocument(),
            // the container for the master and layout slide ordering
            order = self.getMasterSlideOrder(),
            // the index for the global container 'masterSlideOrder'
            masterIndex = -1;

        // if the document is loaded, the new layout slide needs to be inserted at the end of its master slide, if no index is specified.
        if (!_.isNumber(index) && self.isImportFinished() && isMultiMasterSlide && target) { index = self.getLastPositionInModelBehindMaster(target); }

        // getting the valid destination slide, if the new slide is NOT inserted at the end of the container
        if (_.isNumber(index) && index > -1 && target) { destinationSlideId = getLayoutSlideAtSpecifiedPosition(target, index); }

        // adding the layout slide into the layout slide layer node
        slide = appendSlideToLayerNode(layoutSlideLayerNode, id, destinationSlideId);

        // registering the id at the slide
        setTargetContainerId(slide, id);

        // for the model the index must be the index in the global container 'masterSlideOrder' (always -1 during loading)
        if (self.isImportFinished() && _.isNumber(index) && index > -1) { masterIndex = (isMultiMasterSlide && target) ? (_.indexOf(order, target) + index + 1) : (index + 1); }

        // registering the slide in the model
        self.addIntoSlideModel(slide, id, { type: self.getLayoutSlideType(), target, attrs, index: masterIndex });

        // apply the passed slide attributes
        self.slideStyles.setElementAttributes(slide, attrs ? attrs : {}); // 56111, must happen after 'addIntoSlideModel'

        // applying all attributes
        if (self.isImportFinished()) {
            self.slideStyles.updateElementFormatting(slide);
            slide.addClass('invisibleslide emptyplaceholdercheck'); // all new slides need to be invisible (required for undo after delete)
        }

        return true;
    }

    /**
     * Handler for inserting a new slide
     *
     * @param {Number[]} start
     *  The logical position of the slide.
     *
     * @param {String} target
     *  The id of the layout slide, that is the base for the specified slide.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied to the new inserted slide, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @returns {Boolean}
     *  Whether the slide has been inserted successfully.
     */
    function implInsertSlide(start, target, attrs/*, external*/) {

        var // the new slide (jQueryfied)
            slide = createSlideNode(),
            // the new position of the slide
            position = _.clone(start),
            // the position number of the slide (there are only top level slides)
            slideNumber = _.isArray(position) ? position[0] : position,  // TODO: Handle operations correctly
            // the page content node (parent of the slides)
            pageContentNode = getPageContentNode(self.getNode()),
            // the current slide at the position
            currentSlide = pageContentNode[0].childNodes[slideNumber],
            // a new generated id for the slide
            id = self.getNextSlideID(),
            // the slide format manager
            slideFormatManager = self.getSlideFormatManager(),
            // a collector for the children of the page node
            pageNodeChildren = null;

        if (currentSlide) {
            slide.insertBefore(currentSlide);
        } else {
            pageNodeChildren = pageContentNode.children(PARAGRAPH_NODE_SELECTOR);

            if (pageNodeChildren.length > 0) {
                slide.insertAfter(pageContentNode.children(PARAGRAPH_NODE_SELECTOR).last());
            } else {
                pageContentNode.append(slide);
            }
        }

        // removing a following implicit slide
        // -> exchanging an implicit slide with a non-implicit slide
        if (isImplicitParagraphNode(slide.next())) {
            slide.next().remove();
        }

        // registering the id at the slide
        setTargetContainerId(slide, id);

        // registering the slide in the model (with locally generated id)
        self.addIntoSlideModel(slide, id, { type: self.getStandardSlideType(), target, index: slideNumber, attrs });

        // apply the passed slide attributes
        self.slideStyles.setElementAttributes(slide, attrs ? attrs : {}); // 56111, must happen after 'addIntoSlideModel'

        // insert required helper nodes and applying all attributes
        self.validateParagraphNode(slide);

        if (self.isImportFinished()) {
            self.slideStyles.updateElementFormatting(slide);
            slide.addClass('invisibleslide emptyplaceholdercheck'); // all new slides need to be invisible (required for undo after delete)

            // in read-only mode the user needs to activate the first slide, if this was new inserted (63174)
            if (!app.isEditable() && !self.isMasterView() && self.getActiveSlideId() === '' && self.isOneSlideInActiveView()) { self.changeToSlide(self.getSlideIdByPosition(0)); }
        }

        // if not all slides are formatted (especially layout slides), it might be necessary to force their formatting (47402)
        // -> using 'addTasks' might be a generic process for formatting new slides (instead of using 'updateElementFormatting' directly).
        if (slideFormatManager.unformattedSlideExists()) { slideFormatManager.addTasks(id); }

        return true;
    }

    /**
     * Handler for moving a layout slide. This might cause the assignment
     * of a new master slide ID for the specified layout slide.
     *
     * @param {String} id
     *  The ID of the layout slide.
     *
     * @param {String} target
     *  The ID of the (new) master slide for the specified layout slide.
     *
     * @param {Number} start
     *  The new position of the layout slide relative to its master slide.
     *  This is a number, not a logical position.
     *
     * @returns {Boolean}
     *  Whether the move of the layout slide was successful.
     */
    function implMoveLayoutSlide(id, target, start) {

        var // the old master slide for the specified layout slide
            oldMasterId = self.getMasterSlideId(id),
            // whether the master slide was modified
            sameMaster = (oldMasterId === target),
            // the id of the moved slide
            movedId = null,
            // at this time, it is not clear if the slide itself is part of the counting
            // -> when moving upwards on same master, it is still counted
            correction = (sameMaster && (self.getLayoutSlideStartPosition(id) < start)) ? 1 : 0,
            // the slide, that is currently at the specified position (reference for moving)
            // -> the moved slide is inserted BEFORE this slide.
            destinationSlideId = getLayoutSlideAtSpecifiedPosition(target, start + correction),
            // the reference slide for moving
            destinationSlide = null,
            // the moved slide
            moveSlide = null,
            // at this time, it is not clear if the slide itself is part of the counting
            // -> when moving upwards on same master, it is still counted
            // the position inside the master/layout container before moving
            containerStart = self.getSortedSlideIndexById(id),
            // the position inside the master/layout container after moving
            containerEnd = self.getSortedSlideIndexById(target) + start + 1,  // +1, because it is zero-based
            // a helper object to collect all affected slides
            viewCollector = {},
            // whether the implementation function was successful
            success = false;

        // This function needs to handle two different cases:
        // 1. The position of a layout slide was modified.
        // 2. The master slide of a layout slide might have changed.

        // if the specified target is no master slide id, this is an invalid operation
        if (!self.isMasterSlideId(target)) { return false; }
        // fast exit if this is a zero operation (start and end slide are the same)
        if (containerStart === containerEnd && sameMaster) { return true; }
        // reducing the end position, if the new master is shifted forward by 1
        if (!sameMaster && containerStart < containerEnd) { containerEnd -= 1; }

        // updating the position for this target in the sorted collector
        movedId = self.moveSlideIdInModel(containerStart, containerEnd, false); // -> model triggers update (asynchronously?)

        // also moving the slide in the DOM (is this really necessary? All layout slides have position [0].)
        if (movedId && movedId === id) {

            moveSlide = self.getSlideById(id).parent();

            if (destinationSlideId) {
                destinationSlide = self.getSlideById(destinationSlideId).parent();
                $(moveSlide).insertBefore(destinationSlide);
            } else {
                $(moveSlide).insertAfter(self.getOrCreateLayoutSlideLayerNode().children().last()); // adding to the end of layout slide layer
            }

            if (!sameMaster) {
                // new assignment in model from layout to master ID required.
                self.changeMasterIdInModel(id, target);

                // switching the slide. This is required, because the old master
                // slide need to be made invisible and the new master slide need
                // to be made visible.
                viewCollector.oldMasterId = oldMasterId;
                viewCollector.newMasterId = target;
                viewCollector.isMasterChange = true; // special case: layout (active or not) gets new master

                // not sending the value for
                // 'viewCollector.newLayoutId = id;'
                // because it can happen, that this is not the active slide, if more than one
                // layout slide gets a new master slide assigned.

                // informing the listeners, that the slide and its master and layout need to be updated
                self.trigger('change:layoutslide', viewCollector);

                // updating the slide and all place holder drawings in the slide
                self.implSlideChanged(self.getSlideById(id));
            }

            success = true;
        }

        return success;
    }

    /**
     * Handler for changing the layout slide for a specified document slide. This is
     * NOT a handler for changing the master slide of a layout slide.
     *
     * @param {Number} start
     *  The logical position of the document slide that get a new layout slide assigned.
     *
     * @param {String} target
     *  The ID of the layout slide for the specified document slide.
     *
     * @returns {Boolean}
     *  Whether the change of the layout slide was successful.
     */
    function implChangeLayout(start, target) {

        var // the id of the document slide
            id = self.getSlideIdByPosition(start),
            // the old layout id assigned to the document slide
            oldLayoutId = self.getLayoutSlideId(id),
            // a helper object to collect all affected slides
            viewCollector = {};

        // checking, whether the specified target is a valid layout ID
        if (!self.isLayoutSlideId(target)) { return false; }

        if (id && (target !== oldLayoutId)) {
            // updating the model
            self.changeLayoutIdInModel(id, target); // -> model triggers update

            // do not change visibility for slides that are not visible at the moment (e.g. change multi layout slides)
            if (self.isActiveSlideId(id)) {
                // switching the slide. This is required, because the old layout and master
                // slide need to be made invisible and the new layout and master slide need
                // to be made visible (not defining old slide ID).
                viewCollector.oldLayoutId = oldLayoutId;
                viewCollector.oldMasterId = self.getMasterSlideId(oldLayoutId);

                viewCollector.newSlideId = id;
                viewCollector.newLayoutId = target;
                viewCollector.newMasterId = self.getMasterSlideId(target);

                // informing the listeners, that the slide and its master and layout need to be updated
                self.trigger('change:layoutslide', viewCollector);
            }
            self.trigger('change:layoutslide:view-agnostic');
            // e.g. in case of a standard slide gets applied
            // a different layout template but a remote client
            // in master/layout view mode needs to update its
            // tooltips according to the now new slide cooverage.

            // updating the slide and all place holder drawings in the slide
            // -> this needs to be done deferred, because there might be additional
            //    setAttributes operations when changing the layout slide.
            self.implSlideChanged(self.getSlideById(id));

            // forcing an update of the layout slide (57544, background color not updated)
            self.forceTriggerOfSlideStateUpdateEvent(target);
        }

        return true;
    }

    /**
     * Handler for moving a document slides (not layout or master slides).
     *
     * @param {Number[]} start
     *  The logical position of the slide before moving.
     *
     * @param {Number[]} end
     *  The logical position of the slide after moving. This means that at the
     *  end position [4], there are 4 other slides before the moved slide.
     *
     * @param {Boolean} external
     *  Whether this function call was triggered by an external operation.
     *
     * @returns {Boolean}
     *  Whether the move of the slide was successful.
     */
    function implMoveSlide(start, end, external) {

        var // the id of the moved slide
            id = null,
            // whether the implementation function was successful
            success = false,
            // the page content node below the page node
            pageContentNode = null,
            // the slide, that will be moved
            moveSlide = null,
            // the slide that will be used for inserting the moved slide
            destinationSlide = null,
            // an additional value that must be added, because the slide is not removed from its old position
            addIndex = start[0] < end[0] ? 1 : 0,
            // the index of the currently active slide before the move
            activeSlideIndexBefore = self.getActiveSlideIndex();

        if (start[0] === end[0]) { return true; } // can happen in OT case, valid operation

        // sorting the array
        id = self.moveSlideIdInModel(start[0], end[0], true); // -> model triggers update (asynchronously?)

        if (id) {

            // the slides are the direct children of the page content node
            pageContentNode = getPageContentNode(self.getNode());
            // moveSlide = pageContentNode[0].childNodes[start];
            moveSlide = pageContentNode.children(PARAGRAPH_NODE_SELECTOR)[start[0]];
            destinationSlide = pageContentNode.children(PARAGRAPH_NODE_SELECTOR)[end[0] + addIndex]; // increasing by 1, because node is still in the DOM

            if (destinationSlide) {
                $(moveSlide).insertBefore(destinationSlide);
            } else {
                $(moveSlide).insertAfter(pageContentNode.children(PARAGRAPH_NODE_SELECTOR).last());  // adding to the end
            }

            success = true;
        }

        // restoring the browser selection, if this was an external operation that move the currently active slide
        // -> but only after the new logical positions are calculated
        if (external && self.getSelection().isAdditionalTextframeSelection() && !self.isMasterView() && activeSlideIndexBefore === start[0]) {
            self.executeDelayed(function () { self.getSelection().restoreBrowserSelection(); }, 0);
        }

        return success;
    }

    /**
     * Adding a new master or layout slide into the layer node.
     *
     * @param {jQuery} layerNode
     *  The layout layer node or master layer node, to which the new slide will be
     *  appended.
     *
     * @param {String} id
     *  The id string of the layout or master slide.
     *
     * @param {String} [destinationId]
     *  The optional ID of that slide, before that the new layout slide is inserted.
     *
     * @returns {jQuery}
     *  The new created slide node inside the layout layer node or master layer node.
     */
    function appendSlideToLayerNode(layerNode, id, destinationId) {

        var // the new slide node
            slide = createSlideNode(),
            // the slide container (used for positioning)
            slideContainer = $('<div>').addClass(SLIDECONTAINER_CLASS).append(slide);
        slideContainer.attr('tabindex', '0');

        // assigning the id of the slide also to the content node (the slide's parent)
        setTargetContainerId(slideContainer, id);

        if (destinationId) {
            slideContainer.insertBefore(self.getSlideById(destinationId).parent());
        } else {
            // appending the new slide
            layerNode.append(slideContainer);
        }

        // insert required helper nodes
        self.validateParagraphNode(slide);

        return slide;
    }

    /**
     * Trying to get the following layout slide ID relative to a specified layout id.
     *
     * @param {String} layoutId
     *  The id of a layout slide.
     *
     * @returns {String}
     *  The id of a layout slide that follows the specified layout slide.
     */
    function getFollowingLayoutId(layoutId) {

        var // the master layout slide order array
            order = self.getMasterSlideOrder(),
            // the index of an id in the order array
            index = _.indexOf(order, layoutId);

        return index > -1 && index < order.length && self.isLayoutSlideId(layoutId) && self.isLayoutSlideId(order[index + 1]) ? order[index + 1] : null;
    }

    /**
     * Trying to find a second layout id following the first master id. If this
     * cannot be determined, the first found layout id will be returned.
     *
     * @returns {String}
     *  The id of a layout slide that can be used for a new slide.
     */
    function getSecondLayoutIdBehindMaster() {

        var // the layoutId
            layoutId = null,
            // the master layout slide order array
            order = self.getMasterSlideOrder(),
            // the index of an id in the order array
            index = 0,
            // the master id
            masterId = null;

        masterId = _.find(order, self.isMasterSlideId);

        index = _.indexOf(order, masterId);

        if (index > -1 && index < order.length - 2) { layoutId = order[index + 2]; }

        // otherwise take the first possible layout id
        if (!layoutId) { layoutId = _.find(order, self.isLayoutSlideId); }

        return layoutId;
    }

    /**
     * Checking, whether the specified id is a layout id and it is the first
     * id behind the master id.
     *
     * @param {String} layoutId
     *  The id of a layout slide.
     *
     * @returns {Boolean}
     *  Whether the specified id is the first layout id behind the master id.
     */
    function isFirstLayoutIdBehindMaster(layoutId) {

        var // the master layout slide order array
            order = self.getMasterSlideOrder(),
            // the index of the specified layout id inside the order array
            index = _.indexOf(order, layoutId);

        return index > 0 && self.isLayoutSlideId(layoutId) && self.isMasterSlideId(order[index - 1]);
    }

    /**
     * Trying to get the best possible layout id for a new slide. The specified
     * id is the id of the currently active id or of a user specified id.
     *
     * @param {String} id
     *  The id of a document (standard) slide.
     *
     * @param {Boolean} isODF
     *  Whether this is an ODF application.
     *
     * @returns {String}
     *  The id of a layout slide that can be used for a new slide.
     */
    function getValidLayoutIdForNewSlide(id, isODF) {

        var // the layout id of the specified slide
            layoutId = null,
            // the layout id of the following layout slide
            nextLayoutId = null,
            // whether the second layout slide shall be used (the default)
            useSecondLayoutSlide = true;

        if (isODF) {

            // TODO: Try to evaluate a valid odf layout slide ID. This is not the ID of the master slide!
            layoutId = self.getODFDefaultLayoutSlideId();

        } else {

            if (self.isStandardSlideId(id)) {

                layoutId = self.getLayoutSlideId(id);

                if (layoutId) {

                    if (isFirstLayoutIdBehindMaster(layoutId)) {
                        // special handling required -> PowerPoint uses the next layout id
                        nextLayoutId = getFollowingLayoutId(layoutId);
                        layoutId = nextLayoutId ? nextLayoutId : layoutId;
                    }

                    useSecondLayoutSlide = false; // the layout id can also be used for the new slide
                }

            }

            // try to find any valid layout id
            if (useSecondLayoutSlide) { layoutId = getSecondLayoutIdBehindMaster(); }

        }

        return layoutId;
    }

    /**
     * Getting the ID of a layout slide specified by its master slide and the position
     * behind the master slide. The position needs to be specified as zero-based number,
     * not as logical position. If the position behind a specified master does not exist
     * (example: master slide has 3 layout slide and the value for the position (start)
     * is 3) the id of the following layout ID (of a following master) is returned. Or
     * null, if there is no following layout slide.
     * This process guarantees, that a slide can be inserted in the DOM BEFORE that slide,
     * whose ID is calculated within this function.
     *
     * @param {String} masterId
     *  The ID of the master slide.
     *
     * @param {Number} start
     *  The zero-based number of the layout slide behind its master slide.
     *
     * @returns {String|Null}
     *  The ID of the layout slide. Or null, if could not be determined.
     */
    function getLayoutSlideAtSpecifiedPosition(masterId, start) {

        var // the position of the master slide in the master/layout slide container
            masterContainerPosition = self.getSortedSlideIndexById(masterId),
            // the position of the searched layout slide in the master/layout slide container
            layoutContainerPosition = masterContainerPosition + start + 1, // '+1' because 'start' is zero-based
            // the ID of the searched layout slide
            layoutId = null,
            // the order container for the master and layout slides
            masterSlideOrder = self.getMasterSlideOrder(),
            // whether a search for a following layout slide can continue
            doContinue = true;

        if (layoutContainerPosition < masterSlideOrder.length) {

            layoutId = masterSlideOrder[layoutContainerPosition];

            // -> this could be the following master slide.
            // If a master slide has 3 layout slides (at position 0, 1 and 2) and the value
            // of start is 3, then this calculation will lead to the following master. Then
            // it is sufficient to search the following layout slide in the container.
            if (self.isMasterSlideId(layoutId)) {
                layoutId = null;

                while (doContinue && layoutContainerPosition < masterSlideOrder.length) {
                    layoutContainerPosition += 1;
                    if (self.isLayoutSlideId(masterSlideOrder[layoutContainerPosition])) {
                        doContinue = false;
                        layoutId = masterSlideOrder[layoutContainerPosition];
                    }
                }
            }
        }

        return layoutId;
    }

    /**
     * Getting the ID of a master slide at a specified position. This position is NOT the position
     * inside the global container 'masterSlideOrder', but a position, that contains only the master
     * slides. With this function it is possible to insert a master slide as first master slide, or
     * second master slide, or third master slide .... .
     * Because there is no collector specific for master slides, the container 'masterSlideOrder' has
     * to be searched for all master slides (happens in 'getAllMasterSlideIndices'). The the result
     * can be used to find a master slide ID of a master slide, BEFORE that the new master slide
     * will be inserted in the DOM and in the container 'masterSlideOrder'.
     *
     * @param {Number} index
     *  The 0-based index for the master slide. This index takes only the master slides into account,
     *  not the layout slides.
     *
     * @returns {String|Null}
     *  The ID of the master slide. Or null, if could not be determined.
     */
    function getMasterSlideAtSpecifiedPosition(index) {

        var // the ID of the searched master slide
            masterId = null,
            // a helper object with all informations about the master slide indices in the model 'masterSlideOrder'
            indexObject = self.getAllMasterSlideIndices(),
            // the position of the master slide ID inside the container 'masterSlideOrder'
            masterPos = null;

        if (indexObject && index < indexObject.order.length) {
            masterPos = indexObject.order[index];
            masterId = self.getMasterSlideOrder()[masterPos];
        }

        return masterId;
    }

    /**
     * Returning the master slide ID and the index relative to the master slide
     * from a layout slide.
     *
     * @param {Number} end
     *  The index of the layout slide in the slide pane.
     *
     * @param {Boolean} downwards
     *  If the layout slide was moved up or downwards.
     *
     * @returns {Object | void} // TODO: check-void-return
     *  An Object containing the ID from the master slide
     *  and the index relative to the master slide from a layout slide.
     */
    function getLayoutLayerEndPosition(end, downwards) {

        var // the ID of the master slide
            target = null,
            // the index relative to the master slide
            startIndex = 0;

        // getting the id of the slide at the end position
        var endId = self.getIdOfSlideOfActiveViewAtIndex(end);

        // after the last layout slide is no master slide, so we need to check the slide before it to get the endId
        if (end === self.getMasterSlideCount()) {  endId = self.getIdOfSlideOfActiveViewAtIndex(end - 1);  }

        if (self.isMasterSlideId(endId)) {

            if (!downwards && end <= 0) {
                return; // Not jumping upwards over first master, generating no operation
            }

            // searching previous layout slide
            endId = self.getIdOfSlideOfActiveViewAtIndex(end - 1); // this must be a layout slide, not a master slide

            if (self.isMasterSlideId(endId)) { // this is also a master slide (44832) -> two directly following master slides
                target = endId; // already found the new target
                startIndex = 0; // first position behind the master
            } else {
                // the id of the master slide
                target = self.getMasterSlideId(endId);
                // the new index behind the last layout slide
                startIndex = end - self.getSortedSlideIndexById(target) - 1; // -1 because the index is 0-based
            }

        } else {
            // the id of the master slide
            target = self.getMasterSlideId(endId);
            // start index is specified end value minus index of the master slide in the sorted container
            startIndex = end - self.getSortedSlideIndexById(target) - 1;
        }

        return { target, index: startIndex };
    }

    /**
     * Generating the operations for the content of a place holder drawing. This can be used
     * to add the content of a (place holder) drawing into a new inserted drawing in a new
     * slide.
     *
     * @param {String} layoutId
     *  The ID of the layout slide
     *
     * @param {String} type
     *  The type of the place holder drawing
     *
     * @param {Number[]} pos
     *  The logical position of the paragraph
     *
     * @returns {Object[]|Null}
     *  The list of operations to generate the place holder content or null
     *  if no operations were generated.
     */
    function getPlaceHolderContentOperations(layoutId, type, pos) {

        var // the new created operations
            contentOperations = null,
            // the operations generator
            generator = null,
            // the place holder drawing in the layout slide
            drawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId, type);

        if (drawing && drawing.length > 0 && !isEmptyTextframe(drawing)) {
            generator = self.createOperationGenerator();
            generator.generateContentOperations(drawing, pos);
            contentOperations = generator.getOperations();
        }

        return contentOperations;
    }

    /**
     * Using only some of the attributes of the place holder drawing on the layout slide, if a place
     * holder drawing is inserted into a document slide. Especially the place holder drawing is
     * important to support the inheritance chain (47404).
     *
     * @param {Object} attrs
     *  The attribute set of the place holder drawing on the layout slide.
     *
     * @returns {Object|Null}
     *  An object containing the reduced set of drawing attributes for a place holder drawing.
     *  Or null, if this object cannot be determined.
     */
    function reduceNewPlaceHolderDrawingAttributeSet(attrs) {

        var // the new place holder attribute set
            newAttrs = null;

        if (_.isObject(attrs)) {
            newAttrs = {};
            if (attrs.presentation) { newAttrs.presentation = _.copy(attrs.presentation, true); }
            if (attrs.drawing && attrs.drawing.name) { newAttrs.drawing = { name: attrs.drawing.name }; }
        }

        return newAttrs;
    }

    /**
     * Calculating the difference on two specified master slides for a specified
     * place holder drawing type. This function returns a helper object, that contains
     * information about the drawing attributes of the place holder drawing on the
     * old and new master slide and additionally calculates the shift in position
     * and the size ratio, if the specified drawing can be found on both slides.
     *
     * Info: This can only be used for ODF specific master slides.
     *
     * @param {String} type
     *  The type of the place holder drawing. Must be one of the types that can
     *  be located on a ODF master slide.
     *
     * @param {String} newMasterSlideId
     *  The ID of the new master slide.
     *
     * @param {String} oldMasterSlideId
     *  The ID of the old master slide.
     *
     * @returns {undefined}
     */
    function getMasterSlideDiffForOneDrawingType(type, newMasterSlideId, oldMasterSlideId) {

        // the return object with some default values
        var changeValues = { both: null, newAttrs: null, oldAttrs: null, onlyNew: false, onlyOld: false };
        // the place holder drawing with specified type on the new master slide
        var newMasterDrawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(newMasterSlideId, type);
        // the place holder drawing with specified type on the old master slide
        var oldMasterDrawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(oldMasterSlideId, type);
        // the drawing attributes of the place holder drawing on the new master slide
        var drawingAttrsNew = newMasterDrawing ? getExplicitAttributes(newMasterDrawing, 'drawing') : null;
        // the drawing attributes of the place holder drawing on the old master slide
        var drawingAttrsOld = oldMasterDrawing ? getExplicitAttributes(oldMasterDrawing, 'drawing') : null;

        if (drawingAttrsNew) { changeValues.newAttrs = drawingAttrsNew; }
        if (drawingAttrsOld) { changeValues.oldAttrs = drawingAttrsOld; }

        if (drawingAttrsNew && drawingAttrsOld) {
            changeValues.both = {};
            changeValues.both.shiftLeft = drawingAttrsNew.left - drawingAttrsOld.left;
            changeValues.both.shiftTop = drawingAttrsNew.top - drawingAttrsOld.top;
            changeValues.both.factorWidth = math.roundp(drawingAttrsNew.width / drawingAttrsOld.width, 0.001);
            changeValues.both.factorHeight = math.roundp(drawingAttrsNew.height / drawingAttrsOld.height, 0.001);
        } else if (drawingAttrsNew) {
            changeValues.onlyNew = true;
        } else if (drawingAttrsOld) {
            changeValues.onlyOld = true;
        }

        return changeValues;
    }

    /**
     * Getting a new unused master slide ID for ODF master slides (that are internally handled as
     * layout slides).
     *
     * @param {String} id
     *  The ID of the master slide that can be used as reference to generate a new master slide ID.
     *
     * @param {String[]} [slideIdCollection=undefined]
     *  An optional collector with IDs that must not be used as new master slide ID.
     *
     * @returns {String}
     *  A new generated unused ODF master slide ID.
     */
    function getNewODFMasterSlideId(id, slideIdCollection) {

        // a counter for increasing the index inside the new generated ID
        var counter = 1;
        // the base of the ID without a counter at the end
        var baseId = id;
        // the new generated slide ID
        var newSlideID = null;
        // matcher to find the ending counter inside the ID
        var matches = id.match(/(.*)_(\d+)$/);

        if (matches && matches.length === 3) {
            baseId = matches[1];
            counter = matches[2]; // must be interpreted as number
        }

        newSlideID = baseId + '_' + counter;

        while (self.isLayoutSlideId(newSlideID) || (slideIdCollection && _.contains(slideIdCollection, newSlideID))) {
            counter++;
            newSlideID = baseId + '_' + counter;
        }

        return newSlideID;
    }

    // public methods -----------------------------------------------------

    /**
     * Inserting a new slide into the document. The new slide is inserted behind the
     * active slide.
     *
     * The layout of the new slide is the same as the layout of the active slide
     * (or a slide specified by its id). But if this layout is the first layout slide
     * behind the master slide, PowerPoint interprets this as a 'Title'. In this case
     * the following layout slide is used.
     *
     * @param {String} [layoutId]
     *  An optional layout id. If specified, it is used. If not specified, the layout
     *  of the currently active slide is used (or the following layout, if this is the
     *  first layout slide behind the master slide).
     *
     * @param {String} [id]
     *  An optional slide id. If not specified, the active slide is used. This id is
     *  used to determine the layout of the new inserted slide. If the parameter
     *  'layoutId' specifies a valid layout slide, this value for 'id' is ignored.
     */
    this.insertSlide = function (layoutId, id) {

        var // the operations generator
            generator = self.createOperationGenerator(),
            // whether the layoutId is specified
            isFixedLayoutId = layoutId && (self.isLayoutSlideId(layoutId) || (app.isODF() && self.isOdfLayoutSlideId(layoutId))),
            // the slide attributes of the master and layout slide
            mergedSlideAttributes = null,
            // the local id
            localId = id || self.getActiveSlideId(),
            // an object with all place holder attributes that are handled by the layout slide
            allPlaceHoldersAttributes = null,
            // the logical position of the slide to be inserted
            start = increaseLastIndex(self.getActiveSlidePosition()),
            // the position of the drawing and the paragraph
            drawingPos = null, paraPos = null,
            // created operation
            newOperation = null,
            // some additional operations for the content of a drawing
            contentOperations = null,
            // the counter for the top level drawing
            drawingCounter = 0,
            // the attribute set for the new inserted drawing
            newDrawingAttrs = null,
            // helper var to store found insertField operation
            insertSlideFieldOp = null,
            // whether this is an ODF application
            isODF = app.isODF(),
            // the target for odf slides
            odfTarget = isODF ? self.getLayoutSlideId(localId) : null,
            // the attributes of family 'slide' of the active slide
            oldSlideAttrs = null;

        if (app.isODF() && !odfTarget) { odfTarget = self.getMasterSlideOrder()[0]; } // 66367

        // the layout id for the new slide
        layoutId = isFixedLayoutId ? layoutId : getValidLayoutIdForNewSlide(localId, isODF);

        if (_.isUndefined(layoutId) && !isODF) { // #53655 - inserting slide when no layout slide in doc
            self.insertLayoutSlide({ predefMasterId: getMasterSlideAtSpecifiedPosition(0) });
            layoutId = getValidLayoutIdForNewSlide(localId, isODF);
        }

        // getting the merged slide attributes, to determine, which place holder drawings will be inserted
        mergedSlideAttributes = self.getMergedSlideAttributesByFamily(layoutId, 'slide');

        if (isODF) {
            allPlaceHoldersAttributes = self.drawingStyles.getAllPlaceHolderAttributesForId(odfTarget); // place holders from master
            allPlaceHoldersAttributes = self.getODFPlaceHoldersAttributes(layoutId, allPlaceHoldersAttributes); // place holders from ODF layout
        } else {
            allPlaceHoldersAttributes = self.drawingStyles.getAllPlaceHolderAttributesForId(layoutId);
        }

        // creating new slide
        newOperation = { start: _.clone(start), target: (isODF ? odfTarget : layoutId) };

        // transferring 'slide' attributes from the active slide (use the same footer place holder drawing settings in ODF)
        if (isODF && !self.isMasterView() && self.getActiveSlideId()) {
            oldSlideAttrs = self.getSlideAttributesByFamily(self.getActiveSlideId(), 'slide'); // only ODF and only 'slide' family?
            if (oldSlideAttrs?.hidden) { delete oldSlideAttrs.hidden; } // 58331 -> new inserted slide shall not be hidden
            if (oldSlideAttrs) { newOperation.attrs = { slide: oldSlideAttrs }; }
        }

        generator.generateOperation(SLIDE_INSERT, newOperation);

        // iterating over all place holder drawings
        _.each(allPlaceHoldersAttributes, function (attributes, phType) {

            if (self.drawingStyles.isSupportedPlaceHolderForSlideId(layoutId, phType, mergedSlideAttributes)) {

                // iterating over the index
                _.each(attributes, function (attrs) {

                    drawingPos = _.clone(start);
                    drawingPos.push(drawingCounter);

                    paraPos = _.clone(drawingPos);
                    paraPos.push(0);

                    newDrawingAttrs = isODF ? attrs : reduceNewPlaceHolderDrawingAttributeSet(attrs);

                    if (isODF) {
                        newDrawingAttrs.shape = newDrawingAttrs.shape || {};
                        newDrawingAttrs.shape.autoResizeHeight = false; // the filter needs this value explicitely (DOCS-2124)
                    }

                    newOperation = { attrs: newDrawingAttrs, start: _.copy(drawingPos), type: 'shape' };
                    generator.generateOperation(INSERT_DRAWING, newOperation);

                    // adding the text content of the footer of layout or master slide
                    if (phType === 'ftr' || phType === 'sldNum' || phType === 'dt') {
                        contentOperations = getPlaceHolderContentOperations(layoutId, phType, _.clone(drawingPos));
                        if (contentOperations && contentOperations.length > 0) {
                            // if drawing is placeholder for slidenum field, fix operation to get correct representations
                            if (phType === 'sldNum') {
                                insertSlideFieldOp = _.findWhere(contentOperations, { name: 'insertField', type: 'slidenum' });
                                if (insertSlideFieldOp) {
                                    insertSlideFieldOp.representation = String(start[0] + 1); // slide number of newly inserted slide
                                }
                            }
                            generator.appendOperations(contentOperations);
                        } else if (!app.isODF()) {
                            // add a paragraph into the shape, so that the cursor can be set into the text frame
                            newOperation = { start: _.copy(paraPos) };
                            generator.generateOperation(PARA_INSERT, newOperation);
                        }
                    } else if (!app.isODF() && !NO_TEXT_PLACEHOLDER_TYPES.has(phType)) {
                        // add a paragraph into the shape, so that the cursor can be set into the text frame
                        // -> but not in read-only place holder drawings
                        newOperation = { start: _.copy(paraPos) };
                        generator.generateOperation(PARA_INSERT, newOperation);
                    }

                    // increasing the counter for the position of the next drawing
                    drawingCounter++;

                });

            }
        });

        // applying operation
        this.applyOperations(generator);

        // activating the new inserted slide
        self.setActiveSlideId(self.getIdOfSlideOfActiveViewAtIndex(start[0]));

        // setting cursor into text frame
        self.getSelection().setSlideSelection();
    };

    /**
     * Inserting a new custom layout slide into the document. The new slide is inserted behind the
     * active layout slide, or at the end of layout slide if master slide is active.
     *
     * @param {Object} [options]
     *  @param {String} [options.predefMasterId=null]
     *      Optional master id to use, if passed to function.
     */
    this.insertLayoutSlide = function (options) {
        // the operations generator
        var generator = self.createOperationGenerator();
        // layout id
        var newLayoutId = self.getNextCustomLayoutId();
        // get slide pane selection
        var selection = self.getSlidePaneSelection() && self.getSlidePaneSelection().sort(function (a, b) { return a - b; });
        // active slide id; if multiselection than last from selection
        var activeId = self.getIdOfSlideOfActiveViewAtIndex(_.last(selection));
        // predefined master id
        var predefMasterId = getStringOption(options, 'predefMasterId', null);
        // master id
        var masterId = self.isMasterSlideId(activeId) ? activeId : (self.getMasterSlideId(activeId) || predefMasterId);
        // the logical index of the layout slide to be inserted
        var start =  !self.isMasterSlideId(activeId) ? self.getRelativeLayoutIndex(masterId, activeId) : null;
        // the logical position of the slide to be inserted
        var startPos = [0];
        // the position of the drawing, the paragraph and the first character
        var drawingPos = null, paraPos = null;
        // created operation
        var newOperation = null;
        // some additional operations for the content of a drawing
        var contentOperations = null;
        // the counter for the top level drawing
        var drawingCounter = 0;
        // allowed types of placeholders for custom layout slide
        var phTypes = ['title', 'dt', 'ftr', 'sldNum'];
        // template names for placeholders
        var phTypesNames = { title: 'Titelplatzhalter 1', dt: 'Datumsplatzhalter 1', ftr: 'Fu\xdfzeilenplatzhalter 1', sldNum: 'Foliennummernplatzhalter 1' };
        // an object with all place holder attributes that are handled by master slide
        var allPlaceHoldersAttributes = self.drawingStyles.getAllPlaceHolderAttributesForId(masterId);

        // creating new slide
        newOperation = { id: newLayoutId, target: masterId, attrs: { slide: { type: 'cust' } } };
        if (start) {
            newOperation.start = start;
        }
        generator.generateOperation(LAYOUT_SLIDE_INSERT, newOperation);

        // iterating over all place holder drawings
        _.each(allPlaceHoldersAttributes, function (attributes, phType) {
            var elementAttrs = {};

            if (_.indexOf(phTypes, phType) > -1) {
                elementAttrs.presentation = { phType };
                elementAttrs.drawing = { name: phTypesNames[phType], noGroup: true };

                drawingPos = _.clone(startPos);
                drawingPos.push(drawingCounter);

                paraPos = _.clone(drawingPos);
                paraPos.push(0);

                newOperation = { attrs: elementAttrs, start: _.copy(drawingPos), type: 'shape', target: newLayoutId };
                generator.generateOperation(INSERT_DRAWING, newOperation);

                // adding the text content of the footer of layout or master slide
                contentOperations = getPlaceHolderContentOperations(masterId, phType, _.clone(drawingPos));
                if (contentOperations && contentOperations.length > 0) {
                    _.each(contentOperations, function (contentOp) {
                        self.extendPropertiesWithTarget(contentOp, newLayoutId);
                    });
                    generator.appendOperations(contentOperations);
                } else {
                    // add a paragraph into the shape, so that the cursor can be set into the text frame
                    newOperation = { start: _.copy(paraPos), target: newLayoutId };
                    generator.generateOperation(PARA_INSERT, newOperation);
                }

                // increasing the counter for the position of the next drawing
                drawingCounter++;
            }
        });

        // applying operation
        this.applyOperations(generator);
        if (!predefMasterId) { self.setActiveSlideId(newLayoutId); }
    };

    /**
     * Method to duplicate selected slide or slides.
     * They are placed after last currently selected slide.
     * Info: Duplication of master slides for OOXML is not handled yet. For ODP it works.
     *
     * @param {Array} [selection]
     *  Optional parameter for slide pane selection.
     */
    this.duplicateSlides = function (selection) {
        // the operations generator
        var generator = self.createOperationGenerator();
        // the selected slide(s) in slide pane
        var slidePaneSelection = selection || self.getSlidePaneSelection();
        // master or layout id
        var nextLayoutId = null;
        // zero-based position of the current slide
        var anchorPosition = null;
        // deferred for the operations
        var operationsDef = null;
        // a snapshot object
        var snapshot = null;
        // collection of newly created slide ids
        var slideIdCollection = [];
        // the ID of the last selected layout slide
        var lastLayoutId = null;
        // the ID of the master slide of the last selected layout slide
        var lastMasterId = null;
        // the index of the last selected layout slide relative to its master slide
        var lastLayoutIndex = 0;
        // whether this is an ODF application (in this case the master slides are handled as layout slides)
        var isODFApp = app.isODF();
        // a translation table for all drawing IDs
        var drawingIdsTransTable = [];
        // the collection of all generated operations
        var allOperations = null;

        // sort selected slides ascending
        if (_.isArray(slidePaneSelection)) { slidePaneSelection.sort(function (a, b) { return a - b; }); }

        anchorPosition = _.last(slidePaneSelection); // the position of the last selected slide (counted by slide pane)

        if (_.isNumber(anchorPosition)) {
            if (self.isMasterView() && !isODFApp) { // determining the final layout and its master slide, because all slides will be inserted behind it
                lastLayoutId = self.getIdOfSlideOfActiveViewAtIndex(anchorPosition);
                lastMasterId = self.getMasterSlideId(lastLayoutId);
                lastLayoutIndex = self.getRelativeLayoutIndex(lastMasterId, lastLayoutId); // the index of the last selected layout slide relative to its master
            }

            // generate operations
            _.each(slidePaneSelection, function (slideIndex) {
                var slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
                var slideNode = self.getSlideById(slideId);
                var operation;
                var options = {};
                var slideAttrs = getExplicitAttributeSet(slideNode);
                var slideFamilyAttrs = null;
                var slideListStylesAttrs = null;

                // insert slide op
                if (self.isMasterView()) {

                    if (isODFApp) { // in ODF all slides in slide pane are master slides (that are internally handled as layout slides)

                        nextLayoutId = getNewODFMasterSlideId(slideId, slideIdCollection);

                        // getting all attributes from the reference master slide and adding them to the new master slide
                        slideFamilyAttrs = self.getAllFamilySlideAttributes(slideId);
                        slideListStylesAttrs = self.getAllListStylesAttributesOfSlide(slideId);

                        if (!slideAttrs) { slideAttrs = {}; }
                        if (slideFamilyAttrs) { slideAttrs = _.extend(slideAttrs, slideFamilyAttrs); }
                        if (slideListStylesAttrs) { slideAttrs.listStyles = slideListStylesAttrs; }

                        operation = { id: nextLayoutId, attrs: slideAttrs };
                        generator.generateOperation(MASTER_SLIDE_INSERT, operation);

                    } else {

                        if (!nextLayoutId) {
                            nextLayoutId = self.getNextCustomLayoutId();
                        } else {
                            nextLayoutId = String(parseInt(nextLayoutId, 10) + 1); // increase manually next layout Id, since the operations are not applied yet
                        }

                        operation = { start: _.copy(lastLayoutIndex), target: lastMasterId, id: nextLayoutId, attrs: _.extend({ slide: { type: self.getSlideFamilyAttributeForSlide(slideId, 'slide', 'type') } }, slideAttrs) };
                        generator.generateOperation(LAYOUT_SLIDE_INSERT, operation);
                        // increasing the counter, so that the following layout slide will be inserted behind this
                        lastLayoutIndex++;
                    }

                    // generating operations for all drawings on the slide
                    options.getDrawingListStyles = true;
                    options.target = nextLayoutId;
                    options.getNewDrawingId = true;
                    options.drawingIdsTransTable = drawingIdsTransTable;
                    // generate operations for the drawing (including its attributes)
                    generator.generateParagraphChildOperations(slideNode, [0], options);
                    slideIdCollection.push(nextLayoutId);

                } else {

                    // increase position for new slide
                    anchorPosition += 1;
                    operation = { start: [anchorPosition], target: self.getLayoutSlideId(slideId) };
                    if (!_.isEmpty(slideAttrs)) {
                        operation.attrs = slideAttrs;
                    }
                    generator.generateOperation(SLIDE_INSERT, operation);
                    // generate operations for the drawing (including its attributes)
                    generator.generateParagraphChildOperations(slideNode, [anchorPosition], { getDrawingListStyles: true, getNewDrawingId: true, drawingIdsTransTable });
                    app.getModel().copySlideComments(slideIndex, anchorPosition, generator);
                    slideIdCollection.push(anchorPosition);
                }
            });

            // blocking keyboard input during applying of operations
            self.setBlockKeyboardEvent(true);

            // creating snapshot so that the document is restored after cancelling action
            snapshot = new Snapshot(self);

            allOperations = generator.getOperations();
            // if there is at least one connector drawing on the slide, the drawing IDs for the connectors must be adapted
            if (drawingIdsTransTable.foundConnector && drawingIdsTransTable.length > 0) { self.refreshConnectorLinks(allOperations, drawingIdsTransTable); }

            // fire apply operations asynchronously
            operationsDef = self.applyOperationsAsync(allOperations);

            app.getView().enterBusy({
                cancelHandler() {
                    if (operationsDef && operationsDef.abort) {
                        snapshot.apply();  // restoring the old state
                        app.enterBlockOperationsMode(function () { operationsDef.abort(); }); // block sending of operations
                    }
                },
                immediate: true,
                warningLabel: /* slides in a presentation document */ gt('Duplicating selected slides, please wait...')
            });

            // handle the result of duplicate slides operations
            return operationsDef
                .progress(function (progress) {
                    // update the progress bar according to progress of the operations promise
                    app.getView().updateBusyProgress(progress);
                })
                .done(function () {
                    // change to newly created slides
                    if (!self.isMasterView()) {
                        slideIdCollection = slideIdCollection.map(function (id) {
                            return self.getSlideIdByPosition(id);
                        });
                    }
                    if (!slideIdCollection.length) { globalLogger.error('SlideOperationMixin.duplicateSlides(): no valid ids!'); }

                    self.changeToSlide(slideIdCollection[0]);
                    // setting multi slides in slide pane selected after duplicate

                    // TODO: check if delaying can be avoided. 200ms are added for debounced clearMultiSelection,
                    // so this should come after clearSlidesInMultiSelectionDebounced() is called in handleInsertSlide
                    if (slideIdCollection.length > 1) {
                        self.executeDelayed(function () {
                            app.getView().getSlidePane().setSlidePaneSelection(slideIdCollection);
                        }, 200);
                    }
                })
                .always(function () {
                    app.getView().leaveBusy();
                    // allowing keyboard events again
                    self.setBlockKeyboardEvent(false);
                });
        }
    };

    /**
     * Deleting a selection of slides (can also be one). It uses a generator
     * and the 'deleteSlide()' function to group all individual delete operations.
     *
     * TODO: Preparing asynchronous deleting of slides
     *
     * @param {Number[]} selection
     *  An array containing the indices from all selected slides
     */
    this.deleteMultipleSlides = function (selection) {

        var // the generator for all operations
            generator = self.createOperationGenerator(),
            // the position of the slide
            slidePos,
            // the slide id
            slideId,
            // whether the last deleted slide in the selection will be the last slide in its view
            isLastSlide,
            // whether it is necessary to ask the user before deleting the slide(s)
            askUser = false,
            // the current slide order
            order = self.isMasterView() ? _.clone(self.getMasterSlideOrder()) : _.clone(self.getStandardSlideOrder());

        // helper function for applying the generated delete operations
        function doDeleteSlides() {

            isLastSlide = _.last(order) === slideId;

            // note: the handling after 'applyOperations' should be the same as in 'deleteSlide()'
            self.applyOperations(generator);

            // check, if the last slide was deleted
            if (self.isEmptySlideView()) {
                // delete the complete selection, set cursor into clipboard
                self.getSelection().setEmptySelection();

                // removing active slide ID
                self.setActiveSlideId('');

            } else {
                // special handling for the last slide
                if (isLastSlide) { slidePos = decreaseLastIndex(slidePos); }

                // activating a neighboring slide
                self.setActiveSlideId(self.getIdOfSlideOfActiveViewAtIndex(slidePos[0]));

                // selecting the first object
                self.getSelection().setSlideSelection();
            }

        }

        // slides are deleted from the highest index to the lowest
        selection.sort(function (a, b) { return b - a; });

        // position from last deleted slide in the selection
        // important:  must be [0] for master view
        slidePos = self.isMasterView() ? [0] : [_.last(selection)];

        _.each(selection, function (slideIndex, i) {

            slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);

            // calculating the resulting slide order in the model BEFORE the final slide in this selection is deleted,
            // it's needed to calculate 'isLastSlide' later in this function
            if (i < (selection.length - 1)) { order = _.without(order, slideId); }

            self.deleteSlide(generator, slideId);

            askUser = askUser || self.containsUnrestorableElements(self.getSlideById(slideId));
        });

        // deleting the slides (after asking the user, if required)
        if (askUser) {
            self.askUserHandler(askUser, false).then(doDeleteSlides);
        } else {
            doDeleteSlides();
        }
    };

    /**
     * Deleting the currently active slide or a slide with a given Id. Or when
     * a 'globalGenerator' is used, just generate operations to gather them.
     *
     * @param {Generator} globalGenerator
     *  The operations generator from a different function that gathers operations.
     *
     * @param {String} [globalId]
     *  The id of a slide. If not specified, the id of the currently active slide is used.
     */
    this.deleteSlide = function (globalGenerator, globalId) {

        var // the operations generator
            generator = globalGenerator || self.createOperationGenerator(),
            // the used slide id
            id = globalId || self.getActiveSlideId(),
            // the position of the active slide or from the given globalId - important:  must be [0] for master/layout slides
            slidePos = _.isString(globalId) ? self.isLayoutOrMasterId(globalId) ? [0] : [self.getSortedSlideIndexById(globalId)] : this.getActiveSlidePosition(),
            // created operation
            newOperation = { start: _.clone(slidePos) },
            // created target operation
            deleteTargetOperation = null,
            // whether the last slide is active in its view
            isLastSlide,
            // the target of the master slide for a layout slide (required for OT)
            parentTarget = null;

        // Required checks:
        // self.isUsedMasterOrLayoutSlide(id) -> not removing master/layout slides that are referenced by at least
        //                                       one standard slide.
        // self.isOnlySlideInView(id)         -> not removing the last slide in its view.
        if (!self.isDeletableSlide(id)) { return; }

        // if this is an (unused) master slide, all layout slide can be removed, too
        if (self.isMasterSlideId(id)) {
            // setting target at operation for layout slides
            _.each(self.getAllSlideIDsWithSpecificTargetParent(id).reverse(), function (layoutId) {
                generator.generateOperation(DELETE, { start: [0], target: layoutId });
                // generating the deleteTargetSlide operation that is required for OT calculations
                if (app.isOTEnabled()) {
                    var delTargetOperation = { id: layoutId };
                    if (!app.isODF()) {
                        delTargetOperation.index = self.getLayoutSlideStartPosition(layoutId);
                        delTargetOperation.parentTarget = id;
                    }
                    generator.generateOperation(DELETE_TARGET_SLIDE, delTargetOperation);
                }
            });
        }

        // setting target at operation for layout or master slides
        if (self.isLayoutOrMasterId(id)) { newOperation.target = id; }

        generator.generateOperation(DELETE, newOperation);

        // generating the deleteTargetSlide operation that is required for OT calculations
        if (app.isOTEnabled() && self.isLayoutOrMasterId(id)) {
            deleteTargetOperation = { id };
            if (!app.isODF()) {
                if (self.isLayoutSlideId(id)) {
                    deleteTargetOperation.index = self.getLayoutSlideStartPosition(id);
                    parentTarget = this.getMasterSlideId(id);
                    if (parentTarget) { deleteTargetOperation.parenttarget = parentTarget; }
                } else {
                    deleteTargetOperation.index = self.getMasterSlideStartPosition(id);
                }
            }
            generator.generateOperation(DELETE_TARGET_SLIDE, deleteTargetOperation);
        }

        // applying operation
        if (!globalGenerator) {

            isLastSlide = self.isLastSlideActive();

            // note: the handling after 'applyOperations' should be the same as in 'deleteMultipleSlides()'
            self.applyOperations(generator);

            // check, if the last slide was deleted
            if (self.isEmptySlideView()) {
                // delete the complete selection, set cursor into clipboard
                self.getSelection().setEmptySelection();

                // removing active slide ID
                self.setActiveSlideId('');

            } else {
                // special handling for the last slide
                if (isLastSlide) { slidePos = decreaseLastIndex(slidePos); }

                // activating a neighboring slide
                self.setActiveSlideId(self.getIdOfSlideOfActiveViewAtIndex(slidePos[0]));

                // selecting the first object
                self.getSelection().setSlideSelection();
            }
        }
    };

    /**
     * Moving a selection of slides (can also be one) one step downwards or upwards.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.downwards=false]
     *   Whether the slide shall be moved one step downwards or upwards.
     *   Default is upwards.
     *
     * @param {Array} [selection]
     *   An array containing the indices from all selected slides
     */
    this.moveSlectedSlidesOneStep = function (options, selection) {

        var // whether the slide shall be moved one step downwards or upwards
            downwards = getBooleanOption(options, 'downwards', false),
            // first element in the selection
            start,
            // last element in the selection
            end;

        // important: array must be sorted!
        selection.sort(function (a, b) { return a - b; });
        start = Number(_.first(selection, 1));
        end = Number(_.last(selection, 1));

        // check if it is continuous selection or not
        if ((start + selection.length - 1) === end) {

            // moved downwards
            if (downwards) {
                start = start + selection.length + 1;
            // moved upwards
            } else {
                start -= 1;
            }
            // check that the inserting point is not smaller & larger than possible
            if (start > -1 && start <= (self.isMasterView() ? self.getMasterSlideCount() : self.getStandardSlideCount())) {
                self.moveMultipleSlides(selection, start);
            }
        }
    };

    /**
     * Moving a selection of slides (can also be one) to an end position.
     * The selection can be continuous and not continuous. In the later case,
     * the moved slides will be in one range afterwards.
     *
     * @param {Array} [start]
     *  An array containing the indices from all selected slides
     *
     * @param {Number} [end]
     *  Index of the slide where the selection should be inserted.
     *  The slides are inserted directly before it.
     */
    this.moveMultipleSlides = function (start, end) {

        var // modifier for the start position when calculation positions in normal view
            startModifier = 0,
            // modified end position
            newEnd = end,
            // modifier for the end position when calculation positions in normal view
            endModifier = 0,
            // modifier for the position when calculation positions in normal view
            modifierfromEqual = 0,

            // the generator for all operations
            generator = self.createOperationGenerator(),

            // ID of the current layout slide from the selection in master/layout view
            layoutId = '',
            // object containing the master ID at the 'end' position in master/layout view
            destObject = null,
            // whether the move is upwards or downwards
            downwards = false,
            // start position for inserting slides in master/layout view
            newIndexInMasterView = 0;

        // important: array must be sorted!
        start.sort(function (a, b) { return a - b; });

        // handling for master view
        if (self.isMasterView()) {
            // no move slide for master/layout slides in ODF
            if (app.isODF()) { return; }

            downwards = end > start[0];

            //layoutId = self.getIdOfSlideOfActiveViewAtIndex(start[0]); we seem not to need that? TODO

            destObject = getLayoutLayerEndPosition(end, downwards);

            if (destObject && destObject.target && _.isNumber(destObject.index)) {

                newIndexInMasterView = destObject.index;

                _.each(start, function (slideIndex, i) {

                    var sameMaster = false;
                    var isFirst = (i === 0);
                    var downwardsInSameMaster = false;

                    layoutId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
                    sameMaster = destObject.target === self.getMasterSlideId(layoutId);
                    downwardsInSameMaster = (sameMaster && slideIndex < end);

                    // do nothing, if a master slide was moved
                    if (self.isMasterSlideId(layoutId)) { return; }
                    // always increment the new index position, except the first time
                    newIndexInMasterView = isFirst ? newIndexInMasterView : newIndexInMasterView + 1;
                    // when a slide is moved downwards and it stays in the same master, we must subtract one again
                    if (downwardsInSameMaster) { newIndexInMasterView--; }

                    self.moveLayoutSlide(layoutId, destObject.target, newIndexInMasterView, generator);
                });
            }

        // handling for normal view
        } else {
            // handling for multiselection
            if (start.length > 1) {

                _.each(start, function (slideIndex) {

                    // 1) start is qual to the end target
                    if (slideIndex === end) {
                        startModifier += 1; modifierfromEqual += 1; // TODO one var only here?
                    }

                    // 2) start is smaller than end target
                    if (slideIndex < end) {

                        // only one time because we take one slide per iteration end move it to the same end position
                        newEnd = end - 1;
                        slideIndex -= startModifier;

                        startModifier += 1;

                        // don't move the slide when start and end position are equal
                        if ((slideIndex - newEnd) !== 0) {
                            self.moveSlide(slideIndex, newEnd, generator);
                        }
                    }

                    // 3) start is greater than end target
                    if (slideIndex > end) {

                        newEnd = end + endModifier + modifierfromEqual;

                        // don't move the slide when start and end position are equal
                        if ((slideIndex - newEnd) !== 0) {
                            self.moveSlide(slideIndex, newEnd, generator);
                        }
                        endModifier += 1;
                    }
                });

            // handling for for just one slide
            } else {
                // we need to correct the index when moved downwards
                if (start[0] < end) { end -= 1; }

                // don't move the slide when start and end position are equal
                if (start[0] - end !== 0) {
                    self.moveSlide(start[0], end, generator);
                }
            }
        }

        // ... and finally applying the operations if there is at least one
        if (generator.getOperations().length > 0) {
            self.applyOperations(generator);
            self.getSelection().setSlideSelection(); // resetting an existing selection to avoid further text input (57738)
        }
    };

    /**
     * Generating operations for layout slides (moveLayoutSlide).
     *
     * @param {Generator} id
     *  The ID from the layout slide.
     *
     * @param {Generator} target
     *  The associated master slide ID from the layout slide.
     *
     * @param {Number} index
     *  The index from the layout slide in it's master section.
     *
     * @param {Generator} globalGenerator
     *  The operations generator from a different function that gathers operations.
     */
    this.moveLayoutSlide = function (id, target, index, globalGenerator) {

        // no move slide for master/layout slides in ODF
        // and also prevent that a masterslide is moved
        if (app.isODF() || self.isMasterSlideId(id)) { return; }

        var // the operations generator
            generator = globalGenerator || self.createOperationGenerator(),
            // the current master slide id (required for OT)
            currentMasterSlideId = self.getMasterSlideId(id),
            // the current layout slide position relative to its master slide (required for OT)
            currentPos = self.getLayoutSlideStartPosition(id),
            // created operation
            newOperation = { id, target, oldtarget: currentMasterSlideId, oldindex: currentPos, start: index };

        generator.generateOperation(LAYOUT_SLIDE_MOVE, newOperation);

        // applying operation
        if (!globalGenerator) { self.applyOperations(generator); }
    };

    /**
     * Generating operations for document slides (moveSlide).
     *
     * @param {Number} start
     *  The index of the slide in the slide pane before moving.
     *
     * @param {Number} end
     *  The index of the slide in the slide pane after moving.
     *
     * @param {Generator} globalGenerator
     *  The operations generator from a different function that gathers operations.
     */
    this.moveSlide = function (start, end, globalGenerator) {

        var // the operations generator
            generator = globalGenerator || self.createOperationGenerator(),
            // created operation
            newOperation = { start: [start], end: [end] };

        generator.generateOperation(SLIDE_MOVE, newOperation);

        // applying operation
        if (!globalGenerator) {
            self.applyOperations(generator);
            self.getSelection().setSlideSelection(); // resetting an existing selection to avoid further text input (57738)
        }
    };

    /**
     * Generating the operation for changing the master slide assigned to a
     * slide specified by its ID or to the active document slide.
     *
     * @param {String} masterSlideId
     *  The ID of the master slide.
     *
     * @param {String} [slideId]
     *  The ID of the slide, that will get a new layout assigned. This is only required
     *  for a multi slide selection. If it is not specified, the active slide is used.
     *
     * @param {Object} [globalGenerator]
     *  The operations generator to collect all affected operations.
     */
    this.changeODFMaster = function (masterSlideId, slideId, globalGenerator) {

        var // the operations generator
            generator = globalGenerator || self.createOperationGenerator(),
            // the ID of the active slide
            activeSlideId = slideId || self.getActiveSlideId(),
            // the existing layout ID of the active slide
            oldMasterId = self.getLayoutSlideId(activeSlideId);

        // a helper function to generate the move/resize operations for the drawings
        // on the dependent slides
        // masterDiff: { both: null, newAttrs: null, oldAttrs: null, onlyNew: false, onlyOld: false, newAttrs: null, oldAttrs: null }
        // The 'both' object contains the properties 'shiftLeft', 'shiftTop', 'factorWidth' and 'factorHeight'.
        function generateOperationForDrawing(oneDrawing, slideId, masterDiff) {

            var // the options for the generated setAttributes operation
                operationOptions = null,
                // the old drawing attributes of the specified drawing
                oldDrawingAttrs = null,
                // the new value for 'left' of the specified drawing
                newLeft = 0,
                // the new value for 'top' of the specified drawing
                newTop = 0,
                // the new value for 'width' of the specified drawing
                newWidth = 0,
                // the new value for 'height' of the specified drawing
                newHeight = 0,
                // the logical position of the slide, that contains the specified drawing
                slidePos = null,
                // the logical position of the specified drawing on its slide
                start = null,
                // the old 'left' value of the master slide drawing
                oldPhLeft = 0,
                // the old 'top' value of the master slide drawing
                oldPhTop = 0,
                // the old 'left' distance between the specified drawing and the master slide drawing
                oldLeftDistanceToPh = 0,
                // the old 'top' distance between the specified drawing and the master slide drawing
                oldTopDistanceToPh = 0;

            if (masterDiff.both) {

                oldDrawingAttrs = getExplicitAttributes(oneDrawing, 'drawing');

                oldPhLeft = _.isNumber(masterDiff.oldAttrs.left) ? masterDiff.oldAttrs.left : 0;
                oldPhTop = _.isNumber(masterDiff.oldAttrs.top) ? masterDiff.oldAttrs.top : 0;
                oldLeftDistanceToPh = oldDrawingAttrs.left - oldPhLeft; // this value needs to be stretched
                oldTopDistanceToPh = oldDrawingAttrs.top - oldPhTop; // this value needs to be stretched

                newWidth = Math.round(oldDrawingAttrs.width * masterDiff.both.factorWidth);
                newHeight = Math.round(oldDrawingAttrs.height * masterDiff.both.factorHeight);
                newLeft = oldPhLeft + masterDiff.both.shiftLeft + Math.round(oldLeftDistanceToPh * masterDiff.both.factorWidth);
                newTop = oldPhTop + masterDiff.both.shiftTop + Math.round(oldTopDistanceToPh * masterDiff.both.factorHeight);

                slidePos = self.getSlidePositionById(slideId);
                start = getOxoPosition(self.getSlideById(slideId), oneDrawing); // always on document slide
                operationOptions = { start: slidePos.concat(start), attrs: { drawing: { left: newLeft, top: newTop, width: newWidth, height: newHeight } }, target: self.getOperationTargetBlocker() };

            } else if (masterDiff.onlyNew) {

                // Special handling for footer types:
                // Is it required to add or insert them corresponding to the master slide.
                // -> No insertDrawing operation, because footers are never part of the document slide.

            } else if (masterDiff.onlyOld) {

                // Special handling for footer types:
                // Is it required to add or remove them corresponding to the master slide.
                // -> No delete operation, because footers are never part of the document slide.

            }

            // generate the 'setAttributes' operation
            if (operationOptions) { generator.generateOperation(SET_ATTRIBUTES, operationOptions); }
        }

        // Is this is not the ODF document slide view, leave immediately.
        if (!app.isODF() || self.isMasterView()) { return; }

        if (!self.isMasterView()) {

            self.appendSlideToDom(activeSlideId); // the node must be attached to the DOM

            // the 'changeLayout' operation itself
            if (masterSlideId !== oldMasterId) {
                generator.generateOperation(MASTER_CHANGE, { start: self.getSlidePositionById(activeSlideId), target: masterSlideId });
            }

            // generating operation for all place holder drawings, to assign them
            // the new position and size.
            // -> the master slide can only have 'body', 'title' and optionally
            //    the footer placeholders 'dt', 'ftr', 'sldNum'.
            //    'body' also influences 'subTitle', 'title' influences 'ctrTitle'.

            _.each(self.getODFMasterTypes(), function (oneType) {

                var // an object containing the conversion for place holder drawings on master slides
                    // -> 'ctrTitle' is 'title' on master, 'subTitle' is 'body' on master
                    masterTypes = _.invert(self.drawingStyles.getMasterSlideTypeConverter()),
                    // a type array, if more than one type needs to be adapted
                    typeArray = masterTypes[oneType] ? [oneType, masterTypes[oneType]] : [oneType],
                    // a diff of the master drawing attributes for old and new master slide
                    masterSlideDiff = getMasterSlideDiffForOneDrawingType(oneType, masterSlideId, oldMasterId);

                // updating all drawings, that have the specified place holder type
                _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(slideId, typeArray), function (drawing) {
                    if (!isUserModifiedPlaceHolderDrawing(drawing)) {
                        generateOperationForDrawing(drawing, slideId, masterSlideDiff);
                    }
                });

            });

            // ... and finally applying the operations (but only, if no global generator was provided)
            if (!globalGenerator) { self.applyOperations(generator); }

        }

    };

    /**
     * Generating the operation for changing the layout slide assigned to the
     * active document slide.
     *
     * Inside this function the 'changeLayout' operation is generated and additional
     * operations to delete place holder drawings, to insert new place holder drawings
     * and to modify the attributes at place holder drawings.
     *
     * There are three groups of place holder drawings to handle:
     * 1. Place holder drawings of the old slide layout.
     * 2. Unassigned place holder drawings (that have no definitions in the old layout).
     * 3. Place holder drawings of the new slide layout.
     *
     * Unassigned place holder drawings are created, after a switch of layout, if an old
     * place holder drawing contains some content, but the new layout does not contain
     * a definition for that place holder. In this case the place holder drawing is still
     * handled as place holder but is marked with phIndex set to '#FFFFFFFF'. Changes on
     * the master slide still affect this unassigned place holder drawings.
     *
     * During change of layout slide the following steps are executed in a specified order:
     * 1. Collecting information about place holder drawings that:
     *       - exist only in the old layout.
     *       - exist only in the new layout.
     *       - exist in both layouts (exact matching) -> only update required, no operation.
     *       - are unassigned.
     * 2. Delete empty place holder drawings from old layout.
     * 3. Delete empty unassigned place holder drawings.
     * 4. Converting type matching old place holders to new place holders by changing index.
     * 5. Converting matching unassigned place holders to new place holders.
     * 6. 'Magic' conversion from old place holder types to new place holders.
     * 7. 'Magic' conversion from unassigned place holder types to new place holders.
     * 8. Creating place holder drawings that exist only in new layout.
     * 9. Converting old place holders to unassigned place holders.
     * 10. Removing hard attributes, so that setting a new layout can be used to reset to the original layout state.
     * 11. Generate delete operations for unassigned and old place holder drawings.
     *
     * @param {String} id
     *  The ID of the layout slide.
     *
     * @param {String} [slideId]
     *  The ID of the slide, that will get a new layout assigned. This is only required
     *  for a multi slide selection. If it is not specified, the active slide is used.
     *
     * @param {Object} [globalGenerator]
     *  The operations generator to collect all affected operations. This is only required
     *  for a multi slide selection. If it is not specified, this function generates its
     *  own operation generator.
     */
    this.changeLayout = function (id, slideId, globalGenerator) {

        var // the operations generator
            generator = globalGenerator || self.createOperationGenerator(),
            // the ID of the active slide
            activeSlideId = slideId || self.getActiveSlideId(),
            // the existing layout ID of the active slide
            oldLayoutId = self.getLayoutSlideId(activeSlideId),
            // a helper object with differences of old and new layout id
            comparePlaceHolder = null,
            // a helper array to find allowed switches of place holder drawings
            switchDrawings = null,
            // an array with all place holder drawings with special MS PP index,
            // that have no corresponding place holder drawing in the layout slide.
            unassignedPhDrawings = null,
            // collector for delete operations
            deletePositions = [];

        // helper function to reset to default place holder attributes
        function setDefaultLayoutAttributes(allPlaceHolderInfo) {

            _.each(allPlaceHolderInfo, function (placeHolderInfo) {

                _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, placeHolderInfo.type, placeHolderInfo.index), function (drawing) {

                    var // the attribute objects
                        drawingAttrs = null, // presentationAttrs = null,
                        // the options for the setAttributes operation
                        operationOptions = {};

                    // removing hard attributes 'left', 'top', 'width', 'height' and 'rotation' because they are determined by the layout slide
                    drawingAttrs = { left: null, top: null, width: null, height: null, rotation: null };

                    operationOptions.start = getOxoPosition(self.getCurrentRootNode(), drawing);
                    // operationOptions.attrs = { presentation: presentationAttrs, drawing: drawingAttrs };
                    operationOptions.attrs = { drawing: drawingAttrs };

                    generator.generateOperation(SET_ATTRIBUTES, operationOptions);
                });
            });

        }

        if (!self.isMasterView()) {

            // Info: Not comparing (id !== oldLayoutId) because:
            // 1. The layout slide might have changed in the meantime.
            // 2. This process can be used to remove the hard attributes from moved or resized drawings,
            //    so that setting a new layout can be used to reset to the original layout state.
            // -> therefore is must be possible to assign the already assigned layout again

            self.appendSlideToDom(activeSlideId); // the node must be attached to the DOM

            // setting slide selection before changing layout, because drawings might be removed and inserted
            // -> a non-slide selection might be invalid during and after layout change (56932)
            self.getSelection().setSlideSelection();

            // Step 1: Collecting all information about place holder drawings in old layout and in new layout.

            // Adding further operations for new place holder drawings or for no longer existing place holder drawings.
            comparePlaceHolder = self.drawingStyles.comparePlaceHolderSet(activeSlideId, oldLayoutId, id, self.getMergedSlideAttributesByFamily(oldLayoutId, 'slide'), self.getMergedSlideAttributesByFamily(id, 'slide'));

            // collecting all drawings on the active slide, that have the default MS PP index, because they
            // have no place holder definition in the layout slide.
            unassignedPhDrawings = self.drawingStyles.getAllUnassignedPhDrawings(activeSlideId);

            // the 'changeLayout' operation itself
            if (id !== oldLayoutId) {
                generator.generateOperation(LAYOUT_CHANGE, { start: self.getSlidePositionById(activeSlideId), target: id });
            }

            // Step 2: Delete empty place holder drawings (without any text input) from 'old' layout
            if (comparePlaceHolder.onlyFirst.length > 0) {
                _.each(comparePlaceHolder.onlyFirst, function (placeHolderInfo) {

                    // updating all drawings, that have the specified place holder type
                    // -> using options.baseAttributes for the place holder attributes from layout slide and master slide
                    _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, placeHolderInfo.type, placeHolderInfo.index), function (drawing) {

                        var // the logical position of the drawing
                            drawingPos = null;

                        if (isEmptyPlaceHolderDrawing(drawing)) {
                            drawingPos = getOxoPosition(self.getCurrentRootNode(), drawing);
                            deletePositions.push(_.copy(drawingPos, true)); // collecting all drawing positions
                            comparePlaceHolder.onlyFirst = _.without(comparePlaceHolder.onlyFirst, placeHolderInfo); // removing from collector
                        }
                    });
                });
            }

            // Step 3: Delete empty unassigned place holder drawings (without any text input), content might be deleted in the meantime
            if (unassignedPhDrawings.length > 0) {
                _.each(unassignedPhDrawings, function (unassignedDrawing) {

                    var // the logical position of the drawing
                        drawingPos = null;

                    if (isEmptyPlaceHolderDrawing(unassignedDrawing)) {
                        drawingPos = getOxoPosition(self.getCurrentRootNode(), unassignedDrawing);
                        deletePositions.push(_.copy(drawingPos, true)); // collecting all drawing positions (operations must be generated at end)
                        unassignedPhDrawings = _.without(unassignedPhDrawings, unassignedDrawing); // removing from collector
                    }
                });
            }

            // Step 4: Find the exact match of 'old' place holders and 'new' place holder drawings that have a different phIndex.
            if (comparePlaceHolder.onlyFirst.length > 0 && comparePlaceHolder.onlySecond.length > 0) {

                _.each(comparePlaceHolder.onlyFirst, function (firstPlaceHolderInfo) {

                    var // the type of the unassigned drawing
                        type = firstPlaceHolderInfo.type || 'body',
                        // whether the same type was already found (so that only one drawing is replaced)
                        found = false;

                    _.each(comparePlaceHolder.onlySecond, function (secondPlaceHolderInfo) {

                        if (!found && type === secondPlaceHolderInfo.type) { // handling exactly the same type

                            var // the options for the setAttributes operation
                                operationOptions = {},
                                // the drawing in the slide
                                drawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, firstPlaceHolderInfo.type, firstPlaceHolderInfo.index);

                            if (drawing && drawing.length > 0) {

                                // Calculation position of drawing
                                operationOptions.start = getOxoPosition(self.getCurrentRootNode(), drawing);
                                // converting 'old' place holder to 'new' place holder drawing -> only setting new index

                                operationOptions.attrs = { presentation: { phIndex: secondPlaceHolderInfo.index } };
                                if (type !== 'contentbody') { operationOptions.attrs.presentation.phType = type; }

                                generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                                comparePlaceHolder.onlyFirst = _.without(comparePlaceHolder.onlyFirst, firstPlaceHolderInfo); // removing from collector
                                comparePlaceHolder.onlySecond = _.without(comparePlaceHolder.onlySecond, secondPlaceHolderInfo); // removing from collector

                                found = true;
                            }
                        }
                    });
                });
            }

            // Step 5: Find the exact match of unassigned place holders and 'new' place holder drawings
            if (unassignedPhDrawings.length > 0 && comparePlaceHolder.onlySecond.length > 0) {

                _.each(unassignedPhDrawings, function (unassignedDrawing) {

                    var // the type of the unassigned drawing
                        type = getPlaceHolderDrawingType(unassignedDrawing) || CONTENTBODY,
                        // whether the same type was already found (so that only one drawing is replaced
                        found = false;

                    _.each(comparePlaceHolder.onlySecond, function (placeHolderInfo) {

                        if (!found && type === placeHolderInfo.type) { // handling exactly the same type

                            var // the attribute objects
                                drawingAttrs = null, presentationAttrs = null,
                                // the options for the setAttributes operation
                                operationOptions = {};

                            // converting unassigned place holder to 'new' place holder drawing -> setting new index, removing hard drawing attributes
                            presentationAttrs = (type === CONTENTBODY) ? { phIndex: placeHolderInfo.index } : { phIndex: placeHolderInfo.index, phType: type };
                            // removing hard attributes 'left', 'top', 'width' and 'height' because they are determined by the layout slide
                            drawingAttrs = { left: null, top: null, width: null, height: null, rotation: null };

                            operationOptions.start = getOxoPosition(self.getCurrentRootNode(), unassignedDrawing);
                            operationOptions.attrs = { presentation: presentationAttrs, drawing: drawingAttrs };

                            generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                            unassignedPhDrawings = _.without(unassignedPhDrawings, unassignedDrawing); // removing from collector
                            comparePlaceHolder.onlySecond = _.without(comparePlaceHolder.onlySecond, placeHolderInfo); // removing from collector

                            found = true;
                        }
                    });
                });
            }

            // Step 6: Trying to reduce mismatch of 'comparePlaceHolder.onlyFirst' and 'comparePlaceHolder.onlySecond'
            //         by some 'magic': title <-> ctrTitle, body <-> subTitle
            switchDrawings = self.drawingStyles.analyzeComparePlaceHolder(comparePlaceHolder);

            // modifying the type of the place holder drawing via setAttribute operation
            if (switchDrawings.length > 0) {

                _.each(switchDrawings, function (oneSwitch) {

                    var // the options for the setAttributes operation
                        operationOptions = {},
                        // the place holder info of the source
                        from = oneSwitch.from,
                        // the place holder info of the destination
                        to = oneSwitch.to,
                        // the drawing in the slide
                        drawing = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, from.type, from.index);

                    if (drawing && drawing.length > 0) {
                        operationOptions.start = getOxoPosition(self.getCurrentRootNode(), drawing);
                        operationOptions.attrs = { presentation: { phType: (to.type === CONTENTBODY) ? null : to.type, phIndex: to.index }, drawing: { name: to.type + ' ' + (to.index + 1) } };

                        generator.generateOperation(SET_ATTRIBUTES, operationOptions);
                    }
                });
            }

            // Step 7: Trying to reduce the mismatch of 'unassigned place holders and 'comparePlaceHolder.onlySecond'
            //         by the 'magic' of 'PresentationDrawingStyles.ALLOWED_DRAWING_TYPE_SWITCHES'.
            if (unassignedPhDrawings.length > 0 && comparePlaceHolder.onlySecond.length > 0) {

                _.each(unassignedPhDrawings, function (unassignedDrawing) {

                    var // the 'real' type of the unassigned drawing (this can be empty for content bodies)
                        realType = getPlaceHolderDrawingType(unassignedDrawing),
                        // the type of the unassigned drawing
                        type = realType || 'body',
                        // an alternative type that can also be used for the place holder drawing
                        newType = self.drawingStyles.getListOfAllowedDrwawingTypeSwitches()[type],
                        // whether the same type was already found (so that only one drawing is replaced
                        found = false;

                    if (newType) {

                        _.each(comparePlaceHolder.onlySecond, function (placeHolderInfo) {

                            // allowing conversion described in 'PresentationDrawingStyles.ALLOWED_DRAWING_TYPE_SWITCHES'

                            var compareType = (placeHolderInfo.type === CONTENTBODY) ? 'body' : placeHolderInfo.type;

                            if (!found && newType === compareType) { // handling exactly the same type

                                var // the attribute objects
                                    drawingAttrs = null, presentationAttrs = null,
                                    // the options for the setAttributes operation
                                    operationOptions = {};

                                // converting unassigned place holder to 'new' place holder drawing
                                // -> setting new index, new type and removing hard drawing attributes
                                presentationAttrs = { phIndex: placeHolderInfo.index, phType: (newType === 'body') ? null : newType };
                                // removing hard attributes 'left', 'top', 'width' and 'height' because they are determined by the layout slide
                                drawingAttrs = { left: null, top: null, width: null, height: null, rotation: null };

                                operationOptions.start = getOxoPosition(self.getCurrentRootNode(), unassignedDrawing);
                                operationOptions.attrs = { presentation: presentationAttrs, drawing: drawingAttrs };

                                generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                                unassignedPhDrawings = _.without(unassignedPhDrawings, unassignedDrawing); // removing from collector
                                comparePlaceHolder.onlySecond = _.without(comparePlaceHolder.onlySecond, placeHolderInfo); // removing from collector

                                found = true;
                            }
                        });
                    }
                });
            }

            // Step 8: Place holder drawings that exist only in the new layout -> creating new place holder drawings
            if (comparePlaceHolder.onlySecond.length > 0) {

                _.each(comparePlaceHolder.onlySecond, function (placeHolderInfo) {

                    var // the options for the insertDrawing operation
                        operationOptions = {};

                    operationOptions.start = self.getNextAvailablePositionInSlide(activeSlideId);
                    operationOptions.type = 'shape';
                    operationOptions.attrs = { presentation: { phIndex: placeHolderInfo.index }, drawing: { name: placeHolderInfo.type + ' ' + (placeHolderInfo.index + 1) } };
                    if (placeHolderInfo.type !== CONTENTBODY) { operationOptions.attrs.presentation.phType = placeHolderInfo.type; }

                    generator.generateOperation(INSERT_DRAWING, operationOptions);

                    // adding a paragraph, if this is valid in this place holder
                    if (!NO_TEXT_PLACEHOLDER_TYPES.has(placeHolderInfo.type)) {
                        generator.generateOperation(PARA_INSERT, { start: appendNewIndex(operationOptions.start) });
                    }
                });
            }

            // Step 9: Place holder drawings that exist only in the old layout (and are not empty (empty drawings are already removed in step 2))
            //         -> special handling required, setting phIndex to '#FFFFFFFF' and saving drawing attributes at the drawing node.
            if (comparePlaceHolder.onlyFirst.length > 0) {

                _.each(comparePlaceHolder.onlyFirst, function (placeHolderInfo) {

                    var // the place holder type
                        type = placeHolderInfo.type,
                        // the place holder index
                        index = placeHolderInfo.index;

                    // updating all drawings, that have the specified place holder type
                    // -> using options.baseAttributes for the place holder attributes from layout slide and master slide
                    _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type, index), function (drawing) {

                        var // the logical position of the drawing
                            drawingPos = getOxoPosition(self.getCurrentRootNode(), drawing),
                            // the options for the insertDrawing operation
                            operationOptions = null,
                            // the complete element attributes of a drawing
                            elementAttrs = null,
                            // the attributes of family 'drawing' and 'presentation'
                            newDrawingAttrs = null, drawingAttrs = null, presentationAttrs = null,
                            // the drawing type
                            drawingType = null;

                        // Handling place holder drawings, that no longer exist in the new slide layout.
                        // 1. saving the drawing attributes directly at the drawing.
                        // 2. setting the phIndex to '4294967295' ('#FFFFFFFF') so that it is still handled as place holder

                        elementAttrs = self.drawingStyles.getElementAttributes(drawing);
                        drawingAttrs = elementAttrs.drawing;
                        // defining new 'hard' drawing attributes with the properties 'name', 'left', 'top', 'width' and 'height'
                        newDrawingAttrs = { left: drawingAttrs.left, top: drawingAttrs.top, width: drawingAttrs.width, height: drawingAttrs.height };

                        drawingType = getPlaceHolderDrawingType(drawing);
                        presentationAttrs = { phIndex: self.drawingStyles.getMsDefaultPlaceHolderIndex() };
                        if (drawingType) { presentationAttrs.phType = drawingType; }

                        operationOptions = {};
                        operationOptions.start = drawingPos;
                        operationOptions.attrs = { presentation: presentationAttrs, drawing: newDrawingAttrs };

                        generator.generateOperation(SET_ATTRIBUTES, operationOptions);
                    });
                });
            }

            // Step 10: Removing hard attributes, so that setting a new layout can be used to reset to the original layout state
            if (comparePlaceHolder.both.length > 0) {
                setDefaultLayoutAttributes(comparePlaceHolder.both);
            }

            // Step 11: Generating the 'delete' operations at the end of all operations
            if (deletePositions.length > 0) {
                // sorting the drawings for logical positions and reverting the order
                deletePositions = _.sortBy(deletePositions, function (pos) { return _.last(pos); }).reverse();
                // ... and generating operations
                _.each(deletePositions, function (pos) {
                    generator.generateOperation(DELETE, { start: _.copy(pos) });
                });
            }

            // ... and finally applying the operations (but only, if no global generator was provided)
            if (!globalGenerator) { self.applyOperations(generator); }

            // ... and because the drawings have changed, it might be necessary to update dynamic font sizes (52805)
            _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlide(activeSlideId), function (drawing) { self.updateDynFontSizeDebounced($(drawing)); });
        }

    };

    /**
     * Changing the layout slide for a selection of slides (can also be one). It uses a generator
     * and the 'changeLayout()' function to group all individual operations.
     *
     * @param {String} layoutId
     *  The ID of the (new) layout slide.
     *
     * @param {Number[]} selection
     *  An array containing the indices from all selected slides.
     */
    this.changeLayoutForMultipleSlides = function (layoutId, selection) {

        var // the generator for all operations
            generator = self.createOperationGenerator(),
            // the currently used slideId
            slideId;

        _.each(selection, function (slideIndex) {

            slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
            if (app.isODF()) {
                self.changeODFLayout(layoutId, slideId, generator);
            } else {
                self.changeLayout(layoutId, slideId, generator);
            }
        });

        // ... and finally applying the operations
        self.applyOperations(generator);
    };

    /**
     * Changing the master slide for a selection of slides (can also be one).
     * This can only be done in ODF format.
     * Info: In the client model, the master slides are handled as layout slides.
     *
     * @param {String} masterSlideId
     *  The ID of the master slide.
     *
     * @param {Number[]} selection
     *  An array containing the indices from all selected slides.
     */
    this.changeODFMasterForMultipleSlides = function (masterSlideId, selection) {

        var // the generator for all operations
            generator = self.createOperationGenerator(),
            // the currently used slideId
            slideId = null;

        _.each(selection, function (slideIndex) {

            slideId = self.getIdOfSlideOfActiveViewAtIndex(slideIndex);
            self.changeODFMaster(masterSlideId, slideId, generator);
        });

        // ... and finally applying the operations
        self.applyOperations(generator);
    };

    /**
     * Generate and apply operations for slide background(s), after choosing attributes from dialog.
     *
     * @param  {jQuery} $slide
     *     Node of currently active slide.
     * @param  {Object} [options]
     *   @param {Boolean} [options.allSlides=false]
     *      Whether slide background attributes should be applied to all slides, or not.
     *      If not, operations will be applied only to currently active slide.
     */
    this.applySlideBackgroundOperations = function ($slide, options) {
        if (!$slide.length) { globalLogger.error('PresentationModel.applySlideBackgroundOperations(): missing slide node argument!'); }

        // the generator for all operations
        var generator = self.createOperationGenerator();
        var previewAttrs = $slide.data('previewAttrs');
        var allSlides = getBooleanOption(options, 'allSlides', false);
        var activeSlideId = self.getActiveSlideId();
        var activeSlideObject = {};
        var slidesCollection;
        // deffered for the operations
        var operationsDef = null;
        // a snapshot object
        var snapshot = null;

        function isRootParentSlideId(slideId) {
            return self.isMasterSlideId(slideId) || (app.isODF() && self.isLayoutSlideId(slideId));
        }

        if (!previewAttrs) { return; }

        activeSlideObject[activeSlideId] = $slide;
        slidesCollection = allSlides ? self.getAllSlides() : self.getAllSelectedSlides();

        _.each(slidesCollection, function (oneSlide, slideId) {
            var operationOptions = {};
            var oldFollowMasterShapes = null;
            var newFollowMasterShapes = null;
            var slidePos = self.getSlidePositionById(slideId);

            //self.appendSlideToDom(slideId); // the node must be attached to the DOM
            // Note: if we get slide position by id, we dont need to call getOxoPosition,
            // and therefore no need to append slide to DOM

            if (isRootParentSlideId(slideId)) {
                operationOptions.start = [0];
                operationOptions.attrs = { fill: previewAttrs.fill };
                operationOptions.target = slideId;
            } else {
                if (allSlides) {
                    if (self.isSlideWithExplicitBackground(slideId)) { // resets bg color set before, as all slides will get color from root slide
                        operationOptions.start = slidePos;
                        operationOptions.attrs = getEmptySlideBackgroundAttributes();
                        if (self.isLayoutSlideId(slideId)) { operationOptions.target = slideId; }
                    }
                    oldFollowMasterShapes = self.isFollowMasterShapeSlide(slideId);

                    if (self.isMasterSlideId(activeSlideId)) {
                        newFollowMasterShapes = true; // if master is active, followMasterShapes is hardset to true
                    } else {
                        if (previewAttrs && previewAttrs.slide && !_.isUndefined(previewAttrs.slide.followMasterShapes)) {
                            newFollowMasterShapes = previewAttrs.slide.followMasterShapes;
                        } else {
                            newFollowMasterShapes = oldFollowMasterShapes; // no change
                        }
                    }
                    if (newFollowMasterShapes !== oldFollowMasterShapes) {
                        operationOptions.start = operationOptions.start || slidePos;
                        operationOptions.attrs = operationOptions.attrs || {};
                        operationOptions.attrs.slide = { followMasterShapes: newFollowMasterShapes };
                        if (self.isLayoutSlideId(slideId)) { operationOptions.target = slideId; }
                    }
                } else {
                    operationOptions.attrs = previewAttrs;
                    operationOptions.start = slidePos;
                    if (self.isLayoutSlideId(slideId)) { operationOptions.target = slideId; }
                }
            }
            if (!_.isEmpty(operationOptions)) {
                generator.generateOperation(SET_ATTRIBUTES, operationOptions);
            }
        });

        // blocking keyboard input during applying of operations
        self.setBlockKeyboardEvent(true);

        // creating snapshot so that the document is restored after cancelling action
        snapshot = new Snapshot(self);

        // fire apply operations asynchronously
        operationsDef = self.applyOperationsAsync(generator);

        app.getView().enterBusy({
            cancelHandler() {
                if (operationsDef && operationsDef.abort) {
                    snapshot.apply();  // restoring the old state
                    app.enterBlockOperationsMode(function () { operationsDef.abort(); }); // block sending of operations
                }
            },
            immediate: true,
            warningLabel: /*#. shown while setting background property to all slides in document */ gt('Applying background to all slides, please wait...')
        });

        return operationsDef
            .progress(function (progress) {
                // update the progress bar according to progress of the operations promise
                app.getView().updateBusyProgress(progress);
            })
            .done(function () {
                app.getView().handleSlideBackgroundVisibility(activeSlideId);
            })
            .always(function () {
                app.getView().leaveBusy();
                // allowing keyboard events again
                self.setBlockKeyboardEvent(false);
            });
    };

    // operation handler --------------------------------------------------

    /**
     * The handler for the insertMasterSlide operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the master slide was inserted successfully.
     */
    this.insertMasterSlideHandler = function (operation, external) {

        var // the undo operation for the insertMasterSlide operation
            undoOperation = null;

        if (app.isODF()) {
            if (!implInsertLayoutSlide(operation.id, null, operation.start, operation.attrs, external)) { return false; }
        } else {
            if (!implInsertMasterSlide(operation.id, operation.start, operation.attrs, external)) { return false; }
        }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: DELETE, start: [0], target: operation.id };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the insertLayoutSlide operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the layout slide was inserted successfully.
     */
    this.insertLayoutSlideHandler = function (operation, external) {

        var // the undo operation for the insertLayoutSlide operation
            undoOperation = null;

        if (app.isODF()) { return true; } // no insertLayout operations in ODF

        if (!implInsertLayoutSlide(operation.id, operation.target, operation.start, operation.attrs, external)) { return false; }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: DELETE, start: [0], target: operation.id };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the insertSlide operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the document slide was inserted successfully.
     */
    this.insertSlideHandler = function (operation, external) {

        var // the undo operation for the insertSlide operation
            undoOperation = null;

        if (!implInsertSlide(operation.start, operation.target, operation.attrs, external)) { return false; }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: DELETE, start: _.clone(operation.start) };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the moveSlide operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the slide was moved successfully.
     */
    this.moveSlideHandler = function (operation, external) {

        var // the undo operation for the insertSlide operation
            undoOperation = null;

        if (!implMoveSlide(operation.start, operation.end, external)) { return false; }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: SLIDE_MOVE, start: _.clone(operation.end), end: _.clone(operation.start) };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the moveLayoutSlide operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the layout slide was moved successfully.
     */
    this.moveLayoutSlideHandler = function (operation, external) {

        var // the undo operation for the insertSlide operation
            undoOperation = null,
            // the old layout id assigned to the document slide
            oldMasterId = self.getMasterSlideId(operation.id),
            // the old position of the layout slide behind its master
            oldStartPosition = self.getLayoutSlideStartPosition(operation.id);

        if (!implMoveLayoutSlide(operation.id, operation.target, operation.start, external)) { return false; }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: LAYOUT_SLIDE_MOVE, id: operation.id, target: oldMasterId, start: _.copy(oldStartPosition) };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the changeLayout operation, that assigns a new layout slide to a specified
     * document slide. This handle does NOT change the master slide for a specified layout slide.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the layout was changed successfully.
     */
    this.changeLayoutHandler = function (operation, external) {

        var // the undo operation for the insertSlide operation
            undoOperation = null,
            // the id of the document slide
            id = self.getSlideIdByPosition(operation.start),
            // the old layout id assigned to the document slide
            oldLayoutId = self.getLayoutSlideId(id);

        if (!implChangeLayout(operation.start, operation.target)) { return false; }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: LAYOUT_CHANGE, start: _.clone(operation.start), target: oldLayoutId };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the changeMaster operation, that assigns a new ODF master slide to a specified
     * document slide. This is only supported for ODF documents.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the master was changed successfully.
     */
    this.changeMasterHandler = function (operation, external) {

        var // the undo operation for the insertSlide operation
            undoOperation = null,
            // the id of the document slide
            id = self.getSlideIdByPosition(operation.start),
            // the old layout (master) id assigned to the document slide
            oldLayoutId = self.getLayoutSlideId(id);

        if (!app.isODF()) { return false; } // only allowed for odf documents!

        if (!implChangeLayout(operation.start, operation.target)) { return false; } // ODF: Handling with layout slide handler

        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: MASTER_CHANGE, start: _.clone(operation.start), target: oldLayoutId };
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };
}
