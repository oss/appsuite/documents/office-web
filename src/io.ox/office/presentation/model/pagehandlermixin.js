/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { IOS_SAFARI_DEVICE } from '@/io.ox/office/tk/dom';
import { CHROME_ON_ANDROID, getDomNode } from '@/io.ox/office/tk/utils';

import { PRESENTATION_COMMENTBUBBLENODE_SELECTOR, SLIDECONTAINER_SELECTOR, TAB_NODE_SELECTOR, TABLE_CELLNODE_SELECTOR,
    Point, Range, getTargetContainerId, isAppContentNode, isInlineComponentNode, isInsideSlideContainerNode,
    isPlaceholderTemplateButton, isSlideContainerNode, isSlideNode, isResizeNode } from '@/io.ox/office/textframework/utils/dom';
import { getOxoPosition, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { TableResizeObserver } from '@/io.ox/office/textframework/components/table/tableresize';
import { NODE_SELECTOR, TEXTFRAME_NODE_SELECTOR, TEXTFRAMECONTENT_NODE_SELECTOR, getFarthestGroupNode, isGroupedDrawingFrame,
    isOnlyBorderMoveableDrawing, isSelected, isTextFrameNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { checkEventPosition, findValidDrawingNodeBehindDrawing, getBackgroundTextPosition } from '@/io.ox/office/presentation/utils/presentationutils';

// mix-in class PageHandlerMixin ======================================

/**
 * A mix-in class for the document model class PresentationModel providing
 * the event handlers for the main page used in a presentation document.
 *
 * @param {Application} app
 *  The application containing this instance.
 */
export default function PageHandlerMixin(app) {

    var // self reference for local functions
        self = this,
        // time stamp from the touchstart
        touchEventTimeStamp = 0,
        // the touched drawing
        touchEventDrawing = null,
        // the time of the mousedown/touchstart event
        lastTouchEventTime = 0,
        // the touched drawing node (most outer group)
        lastTouchEventDrawing = null,
        // the time between two touchstart events on IOS that is evaluated to distinguish
        // between move of drawing and text selection (only needed for IOS)
        iosDoubleTouchTime = 1000;

    // start to observe table resizer elements, and generate the appropriate operations
    // (needs to be in an `app.onInit` block because class requires access to document view)
    // TODO: move this initialization block somewhere into view code
    app.onInit(() => {
        const tableResizeObserver = this.member(new TableResizeObserver(app, prepareTableResize));
        tableResizeObserver.observe(this.getNode()[0]);
    });

    // private methods ----------------------------------------------------

    /**
     * Predicate function called by `TableResizeObserver` before starting a new
     * tracking sequence on a table resizer element.
     */
    function prepareTableResize(event) {

        // selecting the drawing, if it is not already selected (57469)
        const drawingNode = $(event.target).closest(NODE_SELECTOR); // finding the table drawing
        if (!isSelected(drawingNode)) {
            const activeRootNode = self.getCurrentRootNode();
            const startPosition = getOxoPosition(activeRootNode, drawingNode[0], 0);
            self.getSelection().setTextSelection(startPosition, increaseLastIndex(startPosition), { event });
        }

        // veto for grouped drawings in ODP (avoiding browser freezing, 59284))
        return !(app.isODF() && isGroupedDrawingFrame(drawingNode));
    }

    /**
     * The handler for the 'mousedown' and the 'touchstart' event in the presentation
     * application.
     *
     * @param {jQuery.Event} event
     *  The jQuery event object.
     */
    function pageProcessMouseDownHandler(event) {

        var // whether the user has privileges to edit the document
            readOnly = !app.isEditable(),
            // the slide node
            slideRootNode = null,
            // jQuerified target of the event
            $eventTarget = $(event.target),
            // a table cell node
            tableCell = $eventTarget.closest(TABLE_CELLNODE_SELECTOR),
            // a table cell point
            tableCellPoint = null,
            // the id of an optionally activated slide node
            activatedSlideId = '',
            // the currently activated node
            activeRootNode = self.getCurrentRootNode(),
            // whether the right mouse-button was clicked, or not
            rightClick = (event.button === 2),
            // the time difference between two following 'touchstart' events
            timeDiff = 0,
            // whether this was a doubleTap
            doubleTap = false,
            // time stamp for the previous touchstart
            prevTouchEventTimeStamp = 0,
            // time stamp for the previous touchstart
            prevTouchEventDrawing = null,
            // the drawing node for touch start events
            currentDrawing = null,
            // the farthest group drawing node for touch start events
            groupDrawing = null,
            // whether the found drawing is a selection drawing
            isSelectionDrawing = false,
            // whether the touchstart is on the slide or app-content
            touchClickedOnSlide = false,
            // whether a drawing below the drawing the got the event needs to be handled
            handleDeepDrawing = false,
            // whether the event happened on the already selected drawing
            clickOnSelectedDrawing = false,
            // whether this event is of type 'touchstart'
            isTouchStartEvent = event.type === 'touchstart',
            // whether the selected drawing is already in edit mode (touch events)
            isInActiveEditMode = false;

        if (_.device('smartphone') && $eventTarget.parent().is(PRESENTATION_COMMENTBUBBLENODE_SELECTOR)) {
            return;
        }

        // special behavior for iOS and (Chrome on) Android in Presentation
        if (self.getSlideTouchMode()) {

            if (CHROME_ON_ANDROID && $eventTarget.is('textarea')) { return; } // happens after double click in edit-mode, not to be handled here

            // bug 50009: do not execute this function when the layoutpicker is open, because it would be closed by this
            if (app.getView().getContentRootNode().hasClass('popup-open')) { return; }

            // ignore mouse events that are triggered after touch events in iOS
            if (event.type === 'mousedown') { return; }

            // the drawing for the current touchstart event
            currentDrawing = $eventTarget.closest(TEXTFRAMECONTENT_NODE_SELECTOR).parent();

            if (isGroupedDrawingFrame(currentDrawing)) {
                groupDrawing = getFarthestGroupNode(self.getCurrentRootNode(), currentDrawing) || $();
            }

            isInActiveEditMode = groupDrawing ? groupDrawing.hasClass('active-edit') : currentDrawing.hasClass('active-edit');

            // doubletap detection (without any time limit) on drawings with 'textframecontent'
            // -> Android: If active mode is already set, it is not necessary to measure time.
            // -> IOS: To differentiate between move or text selection, a time measurement is required,
            //         so that preventDefault() can be used to distinguish between move and text selection.
            if (isTouchStartEvent && (!isInActiveEditMode || IOS_SAFARI_DEVICE)) {
                prevTouchEventTimeStamp = touchEventTimeStamp;
                touchEventTimeStamp = event.timeStamp;
                timeDiff = touchEventTimeStamp - prevTouchEventTimeStamp;

                if (timeDiff > 0) { // ignoring the event triggered for move drawing on same event
                    prevTouchEventDrawing = touchEventDrawing;
                    touchEventDrawing = getDomNode(currentDrawing);
                    // doubleTap is true, if:
                    // - both clicks on same drawing
                    // - the previous drawing needs still to be selected (for example not the case after slide change)
                    // - it has to be checked if 'touchEventDrawing' is not undefined, because currentDrawing might be an empty jQuery selection (image selected).
                    doubleTap = self.getSelection().isDrawingSelection() && touchEventDrawing && (touchEventDrawing === prevTouchEventDrawing);
                }
            }

            // setting timer (that can be compared in drawingResize handler to check for move of drawing, 68414)
            // -> event.timestamp from this 'touchstart' cannot be compared with timestamp from tracking start event
            if (!event._HANDLED_EVENT_) {
                lastTouchEventTime = Date.now();
                lastTouchEventDrawing = (groupDrawing && groupDrawing.length > 0) ? getDomNode(groupDrawing) : (touchEventDrawing ? touchEventDrawing : lastTouchEventDrawing);
                event._HANDLED_EVENT_ = true; // setting marker to avoid additional setting in grouped drawings (first the child, then the parent)
            }

            // in event triggered for immediate move, the time stamp is identical and the required classes are already set
            if (timeDiff === 0) { return; }

            self.setTouchStartDrawing(null); // resetting an optionally saved last drawing (to be used in mouseup handler)
            self.setTouchStartDrawingGroup(null); // resetting an optionally saved last drawing group (to be used in mouseup handler)
            self.setPreventDefaultActivated(false);

            // 1) set drawing in editable state
            if (doubleTap) {
                // touchstart into an already selected drawing -> activating edit mode in 'touchend' handler (for better separation of move handler)
                self.setTouchStartDrawing(currentDrawing);
                self.setTouchStartDrawingGroup(groupDrawing);
                self.setTouchStartEvent(event);
                if (IOS_SAFARI_DEVICE && !isInActiveEditMode) {
                    if (timeDiff > iosDoubleTouchTime) {
                        self.setPreventDefaultActivated(true);
                        event.preventDefault(); // move of drawing (preventDefault is required for clean moving of an already selected drawing)
                    } else {
                        currentDrawing.attr('contenteditable', 'true'); // text selection, setting contenteditable immediately for this touchstart event
                    }
                }
                // The drawing was already selected and the user taps into it. At this point it is not clear, if
                // the user wants to select text or move the drawing. This is especially problematic on IOS. Therfore
                // a time separator ('iosDoubleTouchTime', 1 sec.) is introduced. The drawing is already selected for:
                // - more than one second -> The user wants to move the drawing.
                // - less than one second -> The user wants to make a text selection (simulating double tap)
                // This time separator is a compromise and results in two problems in IOS:
                // 1. selecting a drawing with textframe and immediately start to move
                //    -> the complete page is 'swimming' because at the drawing 'contenteditable' is set to true.
                // 2. selecting a drawing and wait with text selection for more than one second
                //    -> activates edit mode without setting cursor because the event got preventDefault and
                //       contenteditable is not set. Therefore a further 'touchstart' is required. This problem is
                //       reduced by setting marker 'setPreventDefaultActivated', so that in 'touchend' handler the
                //       edit mode is not activated.
                // On Android this compromise is not required, because there is only a horizontal 'swimming' during
                // move of an already selected drawing. And the handling in 'touchend' (function 'handleTouchEditMode')
                // where the time diff between 'touchstart' and 'touchend' is evaluated to activate the edit mode, is
                // sufficient.
                return;
            // 2) when drawing is in editable state
            } else if (isInActiveEditMode) {
                return;
            // 3) touched elsewhere or in all other cases: remove drawing edit state, prevent setting the browser selection and close the soft-keyboard
            } else {
                touchClickedOnSlide = isSlideNode($eventTarget) || isAppContentNode($eventTarget);
                // preventDefault to prevent selectionChange events
                // but not preventDefault on the slide/app-content, because that would prevent panning
                if (!touchClickedOnSlide) {
                    event.preventDefault();
                    if (currentDrawing.length === 0 && self.getSelection().isAnyDrawingSelection()) { // check, if this is a click on currently selected drawing -> avoid jumping to slide selection
                        clickOnSelectedDrawing = (self.getSelection().getAnyDrawingSelection()[0] === $eventTarget.closest('.drawing')[0]);
                    }
                }
                // set to default position in the slide, triggerChange is false to prevent scrolling to 0.0 (not setting slide selection, if another drawing is selected, also outside the slide)
                if (!clickOnSelectedDrawing && !self.getSelection().isSlideSelection() && currentDrawing.length === 0) { self.getSelection().setSlideSelection({ triggerChange: false }); }
                // move focus to close a open soft-keyboard
                self.getSelection().setFocusIntoClipboardNode({ specialTouchHandling: true });
                // reset the browser selection
                window.getSelection().removeAllRanges();

                var appPaneNode = app.docView.$appPaneNode;
                // remove all contenteditable
                appPaneNode.find('[contenteditable="true"]').removeAttr('contenteditable');
                // remove the active-edit state
                appPaneNode.find('.active-edit').removeClass('active-edit');
            }
            // when touched on the slide/app-content, it may be the start from a touchmove for panning - therefore return here so that no preventDefault is called later
            if (touchClickedOnSlide) { return; }
        }

        // fix for not clearing annoying selection range in Chrome on click
        if (_.browser.Chrome && self.getSelection().hasRange() && !rightClick && !event.shiftKey) { window.getSelection().empty(); }

        // expanding a waiting double click to a triple click
        if (self.getDoubleClickEventWaiting()) {
            self.setTripleClickActive(true);
            event.preventDefault();
            return;
        }

        // -> if the click happened on the slide, the selection box already handles the event
        if (isSlideNode($eventTarget)) {
            if (rightClick) {
                app.getView().grabFocus(); // #48689
            }
            event.preventDefault();
            return;
        }

        // avoid in FF white background, if user double clicked on textframe node (54187)
        if (_.browser.Firefox && isTextFrameNode($eventTarget) && self.getSelection().isAdditionalTextframeSelection()) {
            // check, if this drawing is already selected
            if (getDomNode(self.getSelection().getSelectedTextFrameDrawing()) === getDomNode($eventTarget.closest('.drawing'))) {
                $eventTarget.removeAttr('_moz_abspos'); // this attribute was already set by the browser
                event.preventDefault();
                return;
            }
        }

        // activation mouse down
        self.setActiveMouseDownEvent(true);
        // clear a possible multi selection in the slide pane
        self.trigger('slidepane:clearselection');

        // set the virtual focus to the document
        app.getView().setVirtualFocusSlidepane(false);

        // handling the image template in empty place holder drawings
        if (isPlaceholderTemplateButton($eventTarget) && !self.isMasterView() && !readOnly) {
            self.handleTemplateImageEvent($eventTarget);
            event.preventDefault();
            return;
        }

        // Fix for 29409: preventing default to avoid grabbers in table cells or inline components
        if ((_.browser.IE === 11) && (
            $eventTarget.is('div.cell') ||
            isInlineComponentNode(event.target) ||
            (event.target.parentNode && isInlineComponentNode(event.target.parentNode)))
        ) {
            event.preventDefault(); // -> not returning !
        }

        if (tableCell.length > 0) {
            // saving initial cell as anchor cell for table cell selection
            // -> this has to work also in read-only mode
            tableCellPoint = Point.createPointForNode(tableCell);
            self.getSelection().setAnchorCellRange(new Range(tableCellPoint, new Point(tableCellPoint.node, tableCellPoint.node + 1)));
        }

        // tabulator handler (handle in processMouseUp event)
        if ($eventTarget.closest(TAB_NODE_SELECTOR).length > 0) { return; }

        // checking for a selection on a drawing node. But if this is a text frame drawing node, this is not a drawing
        // selection, if the click happened on the text frame sub element.
        let drawingNode = $eventTarget.closest(NODE_SELECTOR);

        // Check, if the click on a drawing shall be handled by this drawing (docs-977)
        if (drawingNode.length > 0 && !checkEventPosition(app, event, drawingNode)) {
            // trying to find a valid drawing node behind the specified drawing
            var drawingNodeObj = findValidDrawingNodeBehindDrawing(app, event, drawingNode);
            drawingNode = drawingNodeObj ? drawingNodeObj.drawing : null;
            if (!drawingNode) { // no drawing found behind the drawing that got the event
                // if ((event.shiftKey || event.ctrlKey) && self.getSelection().checkMultipleSelection(drawingNode)) {
                //     event.preventDefault(); // keeping the existing drawing selection, if 'shift' or 'ctrl' is pressed
                //     return;
                // } else {
                var activeSlide = self.getActiveSlide(); // handling this like a click on the slide
                event.target = activeSlide[0];
                activeSlide.trigger(event); // enabling selection box, even if started on drawing
                return;
                // }
            } else {
                // maybe the click happened inside the text frame of the lower drawing
                if (!self.getSlideTouchMode() && drawingNodeObj && drawingNodeObj.textFrameNode) {
                    var pos = getBackgroundTextPosition(app, drawingNode, drawingNodeObj.textFrameNode, drawingNodeObj.info);
                    self.getSelection().setTextSelection(pos);
                    event.preventDefault();
                    return;
                }
                handleDeepDrawing = true;
            }
        }

        if (drawingNode.length > 0) {

            // check, if the drawing is the drawing inside the selection overlay (class 'drawingselection' is set)
            isSelectionDrawing = drawingNode.hasClass('selectiondrawing');

            // converting selection overlay node to 'real' drawing node in the document (if required)
            var realDrawingNode = self.getSelection().getDrawingNodeFromDrawingSelection(drawingNode);

            // check, if a drawing multiple selection can be created or expanded or decreased or removed
            if ((event.shiftKey || event.ctrlKey) && realDrawingNode && self.getSelection().checkMultipleSelection(realDrawingNode)) {
                self.getSelection().handleMultipleSelection(realDrawingNode);
                event.preventDefault();
                return; // TODO: This 'return' avoids immediate move, if selection is expanded to more than one drawing
            }

            // presentation specific code
            if (isInsideSlideContainerNode(drawingNode)) {  // This should always be the case, except if 'isSelectionDrawing' is true

                slideRootNode = drawingNode.closest(SLIDECONTAINER_SELECTOR);

                // handling unselectable drawings in presentation
                if (slideRootNode.children('.slide').is('.notselectable')) {
                    event.preventDefault();
                    return;
                }

                self.getSelection().setNewRootNode(slideRootNode);
                activatedSlideId = getTargetContainerId(slideRootNode);
                self.setActiveTarget(activatedSlideId);
            }
            // do not clear the drawing node in slideTouchMode, so that in every case just the drawing is selected and not the text inside the drawing
            if (!self.getSlideTouchMode()) {
                if (isOnlyBorderMoveableDrawing(drawingNode) && $eventTarget.closest(TEXTFRAME_NODE_SELECTOR).length > 0 && !(rightClick && self.getSelection().isMultiSelection())) {
                    drawingNode = $();
                    if (!app.isFirstTabActivated()) { // avoid very fast cursor setting in drawing without browser selection (53148)
                        event.preventDefault();
                        return;
                    }
                }
            }
        } else {
            // no drawing selected -> click on slide
            if (isSlideNode($eventTarget) && isSlideContainerNode($eventTarget.parent())) {
                slideRootNode = $eventTarget.parent();
                self.getSelection().setNewRootNode(slideRootNode);
                activatedSlideId = getTargetContainerId(slideRootNode);
                self.setActiveTarget(activatedSlideId);
            }
        }

        // deactivating active target, but not if it was activated before (presentation specific code)
        // -> checking for 'isSelectionDrawing', so that the target is not removed  (50005, 50067)
        if (self.getActiveTarget() !== '' && !activatedSlideId && !isSelectionDrawing) {
            self.getSelection().setNewRootNode(self.getNode());
            self.setActiveTarget('');
        }

        // in read only mode allow text selection only (and drawing selection, task 30041)
        if (readOnly && drawingNode.length === 0) {
            self.getSelection().processBrowserEvent(event);
            return;
        }

        if (drawingNode.length > 0) {

            if (!isTouchStartEvent) {
                // prevent default click handling of the browser
                event.preventDefault();
            }

            // set focus to the document container (may be located in GUI edit fields)
            app.getView().grabFocus();

            if (drawingNode.is('.selectiondrawing') && self.getSelection().isAdditionalTextframeSelection()) {
                self.getSelection().toggleTextFrameAndTextSelection({ keepDraggingActive: true }); // selecting the drawing, but keeping dragging active (DOCS-3822)
                drawingNode = self.getSelection().getSelectedDrawing();
                drawingNode.data("dispatcher")?.process(drawingNode[0], event.originalEvent);
                return isTouchStartEvent;
            }

            // do nothing if the drawing or its surrounding group is already selected (prevent endless
            // recursion when re-triggering the event, to be able to start
            // moving the drawing immediately, see below)
            // Also create a drawing selection, if there is an additional text frame selection,
            // that also has a text selection. In this case the text selection is removed.
            if (!isSelected(drawingNode) || handleDeepDrawing || self.getSelection().isAdditionalTextframeSelection()) {

                // also do nothing if the surrounding group is already selected (47786)
                // -> but set a new selection, if the selected surrounding group is an additional text frame selection (48177)
                if (handleDeepDrawing || (!(isGroupedDrawingFrame(drawingNode) && !self.getSelection().isAdditionalTextframeSelection() && isSelected(getFarthestGroupNode(activeRootNode, drawingNode))))) {

                    var groupDrawingNode = null; // an optional group drawing node

                    // select the drawing, if not already selected (caused by handleDeepDrawing)
                    if (!isSelected(drawingNode)) {
                        let startPosition;
                        if (isGroupedDrawingFrame(drawingNode)) {
                            groupDrawingNode = getFarthestGroupNode(activeRootNode, drawingNode);
                            startPosition = getOxoPosition(activeRootNode, groupDrawingNode, 0);
                        } else {
                            startPosition = getOxoPosition(activeRootNode, drawingNode[0], 0);

                        }
                        const endPosition = increaseLastIndex(startPosition);
                        self.getSelection().setTextSelection(startPosition, endPosition, { event });
                    }

                    // explicitly start movement tracking (current DOM event has already bubbled up to the page element,
                    // therefore tracking observer will not see it anymore)
                    drawingNode.data("dispatcher")?.process(drawingNode[0], event.originalEvent);
                }
            }

            return isTouchStartEvent;
        }

        // checking for a selection on a resize node
        if (isResizeNode($eventTarget)) {
            return; // no browser handling for this event
        }

        // Avoiding that the Safari Browser expands a right click to a word selection
        if (rightClick && _.browser.Safari) { self.getSelection().storeCurrentSelectionState(); }

        // clicked somewhere else: calculate logical selection from browser selection, after browser has processed the mouse event.
        self.getSelection().processBrowserEvent(event);
    }

    // public methods -----------------------------------------------------

    /**
     * Getting the handler function that is used for process mouse down and tochstart
     * events on the page node.
     *
     * @returns {Function}
     *  The handler function for the 'mousedown' and the 'touchstart' event.
     */
    this.getPageProcessMouseDownHandler = function () {
        return pageProcessMouseDownHandler;
    };

    /**
     * Receiving the time of the last 'touchstart' event.
     *
     * @returns {Number}
     *  The time of the last 'touchstart' event (in ms since 01/01/1970).
     */
    this.getTouchEventTime = function () {
        return lastTouchEventTime;
    };

    /**
     * Receiving the drawing node, that catched the last 'touchstart' event. In
     * case of a drawing group, this is the group node, no child node.
     *
     * @returns {HTMLElement}
     *  The drawing node, that received the last 'touchstart' event.
     */
    this.getTouchEventDrawing = function () {
        return lastTouchEventDrawing;
    };

    /**
     * Receiving the specified time for an IOS double touch to distinguish
     * between move of drawing and text selection.
     *
     * @returns {HTMLElement}
     *  The drawing node, that received the last 'touchstart' event.
     */
    this.getIOSDoubleTouchTime = function () {
        return iosDoubleTouchTime;
    };
}
