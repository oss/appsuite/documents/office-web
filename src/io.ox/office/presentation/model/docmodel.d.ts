/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import CharacterStyleCollection from "@/io.ox/office/textframework/format/characterstyles";
import ParagraphStyleCollection from "@/io.ox/office/textframework/format/paragraphstyles";
import TableStyleCollection from "@/io.ox/office/textframework/format/tablestyles";
import TableRowStyleCollection from "@/io.ox/office/textframework/format/tablerowstyles";
import TextBaseModel, { TextBaseAttributePoolMap } from "@/io.ox/office/textframework/model/editor";
import TableCellStyleCollection, { TableCellAttributes } from "@/io.ox/office/presentation/format/tablecellstyles";
import DrawingStyleCollection from "@/io.ox/office/presentation/format/drawingstyles";
import PageStyleCollection, { PageAttributes } from "@/io.ox/office/presentation/format/pagestyles";
import SlideFieldManager from "@/io.ox/office/presentation/components/field/slidefieldmanager";
import PresentationApp from "@/io.ox/office/presentation/app/application";

// types ======================================================================

export interface DocAttributePoolMap extends TextBaseAttributePoolMap {
    cell: [TableCellAttributes];
    page: [PageAttributes];
}

// class PresentationModel ====================================================

export default class PresentationModel extends TextBaseModel<DocAttributePoolMap> {

    readonly characterStyles: CharacterStyleCollection<PresentationModel>;
    readonly paragraphStyles: ParagraphStyleCollection<PresentationModel>;
    readonly tableStyles: TableStyleCollection<PresentationModel>;
    readonly tableRowStyles: TableRowStyleCollection<PresentationModel>;
    readonly tableCellStyles: TableCellStyleCollection;
    readonly drawingStyles: DrawingStyleCollection;
    readonly pageStyles: PageStyleCollection;
    readonly slideFieldManager: SlideFieldManager; // required, because getFieldManager cannot be typed

    public constructor(docApp: PresentationApp, options?: object);

    // Cannot be used because of problematic typing
    // public getFieldManager(): SlideFieldManager;
}
