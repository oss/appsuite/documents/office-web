/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { getArrayOption, getBooleanOption, getNumberOption, getStringOption } from '@/io.ox/office/tk/utils';
import { resolveUserId, OX_PROVIDER_ID } from '@/io.ox/office/editframework/utils/operationutils';
import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import ThreadedCommentAddress from '@/io.ox/office/presentation/model/threadedcommentaddress';

// class ThreadedCommentModel =============================================

class ThreadedCommentModel extends ModelObject {

    constructor(docModel, commentOption, threadedParentComment, isInsertComment) {

        super(docModel);

        // self reference
        var comment = this;

        commentOption = (commentOption || {});

        var attributes = commentOption.attributes;

        var text = getStringOption(attributes, 'text', '');
        var mentions = getArrayOption(attributes, 'mentions', '');
        var date = !isInsertComment && getStringOption(attributes, 'date', 0);

        var longDate;

        var posX = getNumberOption(attributes, 'posX', 0);
        var posY = getNumberOption(attributes, 'posY', 0);

        var colorIndex = getNumberOption(attributes, 'colorIndex', -1);

        var authorId    = getStringOption(attributes, 'authorId', null); // backend needs {Null} value as failure case.
        var authorName  = getStringOption(attributes, 'author', '');

        var userId      = getStringOption(attributes, 'userId', null);
        var providerId  = getStringOption(attributes, 'providerId', null);

        var uuid    = getStringOption(commentOption, 'uuid', '');
        var slideId = getStringOption(commentOption, 'slideId', '');

        var parent  = (commentOption.parent && threadedParentComment);

        /**
         *  - presentation slides do not feature any column/row based anchors like spreadsheets do.
         *
         *  - But in order to not break the API of either the `ThreadedCommentCollection` nor
         *    the `CommentsPane` a comment model in presentation has to provide some
         *    address/anchor information too.
         */
        var address = new ThreadedCommentAddress(slideId, (parent ? parent.getId() : uuid)); // slide and thread specific address.

        var done = getBooleanOption(commentOption, 'done', false);

        comment.getAnchor = function () {
            return address;
        };
        comment.getAnchorString = function () { // satisfy the APIs of both `ThreadedCommentCollection` and `CommentsPane`
            return '';
        };

        comment.getText = function () {
            return text;
        };
        comment.setText = function (textValue) {
            text = textValue;
        };

        comment.getMentions = function () {
            return mentions;
        };
        comment.setMentions = function (mentionsValue) {
            mentions = mentionsValue;
        };

        comment.getDate = function () {
            return date;
        };
        comment.getDateAsNumber = function () {
            if (!longDate) {
                longDate = Date.parse(date);
            }
            return longDate;
        };

        comment.getAuthor = function () {
            return authorName;
        };
        comment.getAuthorId = function () {
            return authorId;
        };

        /**
         *  Calculating the app suite user id from the available(provided/not provided) userId.
         *  Only used from the class CommentsPane to show the author picture and information.
         *  return { number }
         */
        comment.getInternalUserId = function () {
            var numberValue = parseInt(((providerId === OX_PROVIDER_ID) ? userId : null), 10);
            return Number.isFinite(numberValue) ? resolveUserId(numberValue) : -1;
        };
        comment.getUserId = function () {
            return userId;
        };
        comment.getProviderId = function () {
            return providerId;
        };

        comment.getId = function () {
            return uuid;
        };

        comment.getSlideId = function () {
            return slideId;
        };

        comment.getParent = function () {
            return parent;
        };

        /**
         * Setting a new parent for a comment. This can only happen in the context of undo,
         * if a parent comment is removed (DOCS-2702).
         */
        comment.setParent = function (newParent) {
            parent = newParent;
            address = new ThreadedCommentAddress(slideId, uuid); // calculating new address
        };

        comment.getPositionList = function () {
            return [posX, posY]; // all values in 'hmm'.
        };
        comment.setPosition = function (xInHmm, yInHmm) {
            posX = xInHmm;
            posY = yInHmm;
        };

        comment.getColorIndex = function () {
            return colorIndex;
        };

        comment.hasInvalidParent = function () {
            return !!commentOption.hasInvalidParent;
        };

        comment.getReplacedParentId = function () {
            return commentOption.replacedParentId;
        };

        comment.isReply = function () {
            return !!parent;
        };

        comment.isDone = function () {
            return done;
        };

        comment.equals = function (threadedCommentModel) {
            return (threadedCommentModel && threadedCommentModel.getId && (threadedCommentModel.getId() === uuid));
        };
    }
}

// exports ================================================================

export default ThreadedCommentModel;
