/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
// class ThreadedCommentAddress ===========================================

/**
 *  - presentation slides do not feature any column/row based anchors like spreadsheets do.
 *
 *  - But in order to not break the API of either the `ThreadedCommentCollection` nor
 *    the `CommentsPane` a comment model in presentation has to provide some
 *    address/anchor information too.
 */
function ThreadedCommentAddress(slideId, uuid) {
    this.slideId = slideId;
    this.uuid = uuid;
}

ThreadedCommentAddress.prototype.equals = function (other) {
    return (this.slideId === other.slideId) && (this.uuid === other.uuid);
};

ThreadedCommentAddress.prototype.toString = function () {
    return this.slideId + ':' + this.uuid;
};

// exports ================================================================

export default ThreadedCommentAddress;
