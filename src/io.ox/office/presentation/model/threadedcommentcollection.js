/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { convertLengthToHmm } from '@/io.ox/office/tk/utils';
import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import ThreadedCommentModel from '@/io.ox/office/presentation/model/threadedcommentmodel';
import ThreadedCommentAddress from '@/io.ox/office/presentation/model/threadedcommentaddress';

export default class ThreadedCommentCollection extends ModelObject {

    constructor(docModel) {

        super(docModel);

        var collection = this;

        var app = docModel.getApp();
        var docView;

        var unsavedComment = null;
        var selectedThread      = null;

        /**
         *  Even though comment indices are integer based in an ascending order, there might be "index-gaps",
         *  thus, at one point, an array-based list might turn into a sparse array.
         *  For this the main data-structure that handles registering/deregistering of threaded-comment-models
         *  will be an ordinary object ("key-value-bag"). Current lists then will be generated from this index.
         */

        function getSlideIndexBySlideId(slideId) {
            var slidePosition = docModel.getSlidePositionById(slideId);
            return ((slidePosition !== null) ? slidePosition[0] : -1);
        }

        function getCommentsPositionList() {
            var commentModels = collection.getCommentModels();
            var positionList = commentModels.map(function (commentModel) {
                return commentModel.getPositionList();
            });

            return positionList;
        }

        function getCommentIndex(threadedCommentModel) {
            // return _.findIndex(threadedCommentList, function (model) {
            //
            //     return (threadedCommentModel.uuid === model.uuid);
            // });
            return threadedCommentList.indexOf(threadedCommentModel);
        }

        const threadedCommentList = this.member([]);

        // globalLogger.info('+++ ThreadedCommentCollection :: threadedCommentList : ', threadedCommentList);

        collection.hasComments = function () {
            return (threadedCommentList.length >= 1);
        };

        collection.getCommentModels = function () {
            return Array.from(threadedCommentList);
        };

        /**
         * Returns the threaded comment models anchored at the specified cell address.
         *
         * @param {ThreadedCommentAddress} commentAddress
         *  The address of the comment's anchor cell.
         *
         * @param {Boolean} isInReverseOrder
         *  A flag that indicates whether or not reverting the model-order within the list of threaded comment models.
         *
         * @returns {[ThreadedCommentModel]|Null}
         *  List of threaded comment models that are addressed by a slide-specific comment-thread (be it a sole comment or a parent comment); otherwise null.
         */
        collection.getByAddress = function (commentAddress, isInReverseOrder, ignoreCommentsWithInvalidParent) {
            var commentList = [];

            if (commentAddress instanceof ThreadedCommentAddress) {

                var isFirstThreadOnSlide = threadedCommentList.length && commentAddress.equals(threadedCommentList[0].getAnchor()); // is this the first thread -> take care of children

                commentList = threadedCommentList.filter(function (commentModel) {
                    return commentAddress.equals(commentModel.getAnchor()) || (isFirstThreadOnSlide && !ignoreCommentsWithInvalidParent && commentModel.hasInvalidParent());
                });
                if (isInReverseOrder) {
                    commentList.reverse();
                }
            }
            return (commentList.length >= 1) ? commentList : null;
        };

        /**
         * Returns the threaded comment models, each related to it's parent thread.
         *
         * @param {ThreadedCommentModel} parentCommentModel
         *  The address of the comment's anchor cell.
         *
         * @param {Boolean} isInReverseOrder
         *  A flag that indicates whether or not reverting the model-order within the list of threaded comment models.
         *
         * @returns {[ThreadedCommentModel]|Null}
         *  List of threaded comment model children that are all related to one parent-thread.
         */
        collection.getChildComments = function (parentCommentModel, isInReverseOrder) {
            var commentList = [];

            if (parentCommentModel instanceof ThreadedCommentModel) {
                var commentAddress = parentCommentModel.getAnchor();

                commentList = threadedCommentList.filter(function (commentModel) {
                    return commentModel.isReply() && commentAddress.equals(commentModel.getAnchor());
                });
                if (isInReverseOrder) {
                    commentList.reverse();
                }
            }
            return (commentList.length >= 1) ? commentList : null;
        };

        /**
         * In some special cases it can happen, that a child comment has to become a parent comment.
         * This can happen, if the parent is removed with an undo operation (DOCS-2702, in OX Text
         * this problem was handled with DOCS-2674, DOCS-2684 and DOCS-2687).
         *
         * To achieve this, the parent has to be set to null for all comment models in the comment
         * collection. But it has also to be set to null in the specified commentList that is the
         * slide specific list of comments of the commentRegistry.
         *
         * @param {ThreadedCommentModel} parentCommentModel
         *  The comment model that will be removed.
         *
         * @param {Object[]} commentList
         *  The slide specific list of comments from the commentRegistry.
         */
        collection.makeChildrenToParents = function (parentCommentModel, commentList) {

            var children = collection.getChildComments(parentCommentModel);

            // keeping the collection up-to-date
            _.each(children, function (oneChild) {
                oneChild.setParent(null); // removing the parent
            });

            // keeping also the commentRegistry up-to-date -> TTODO: Two lists?
            _.each(commentList, function (oneComment) {
                if (oneComment.parent && oneComment.parent.uuid === parentCommentModel.getId()) { oneComment.parent = null; }
            });
        };

        collection.getParentComments = function () {
            return threadedCommentList.filter(function (commentModel) {

                return !commentModel.isReply();
            });
        };

        collection.getCommentById = function (uuid) {
            // globalLogger.info('+++ ThreadedCommentCollection :: getCommentById :: uuid : ', uuid);
            uuid = String(uuid);

            return _.find(threadedCommentList, function (model) {
                return (model.getId() === uuid);
            });
        };

        collection.getByIndex = function (index) {
            // globalLogger.info('+++ ThreadedCommentCollection :: getByIndex :: index ', index);

            index = Number(index);
            index = Number.isFinite(index) ? index : -1;

            return threadedCommentList[index];
        };

        collection.getSelectedThread = function () {
            return selectedThread; // {ThreadedCommentModel|Null}
        };

        collection.getSelectedComments = function () {
            return (selectedThread && collection.getByAddress(selectedThread.getAnchor())); // {[ThreadedCommentModel]|Null}.
        };

        collection.selectComment = function (indexOrModel) {
            // globalLogger.info('+++ ThreadedCommentCollection :: selectComment :: indexOrModel ', indexOrModel);

            var index = Number(indexOrModel);
            index = Number.isFinite(index) ? index : -1;

            var commentModel  = threadedCommentList[index] || ((indexOrModel instanceof ThreadedCommentModel) ? indexOrModel : null);

            var commentChild  = ((commentModel && commentModel.isReply() && commentModel) || null);
            var commentParent = ((commentChild && commentChild.getParent()) || commentModel);

            if (getCommentIndex(commentModel) >= 0) {

                selectedThread = commentParent;
            }
            docModel.trigger('select:threadedcomment:collection', { commentParent, commentChild });

            return docModel.getApp().getController().update();
        };

        collection.deleteComment = function (commentModel) {
            // globalLogger.info('+++ ThreadedCommentCollection :: deleteComment :: commentModel ', commentModel);
            if (selectedThread && selectedThread.equals(commentModel)) {

                selectedThread = null;
            }
            docModel.applyDeleteComment(commentModel);

            return $.when();
        };

        collection.deleteThread = function (threadList) {
            // globalLogger.info('+++ ThreadedCommentCollection :: deleteThread :: threadList ', threadList);
            if (selectedThread && selectedThread.equals(_.first(threadList))) {

                selectedThread = null;
            }
            docModel.applyDeleteThread(threadList);

            return $.when();
        };

        // // Note yet used ... Delete All Comments of a Slide
        //
        // collection.deleteAllComments = function () {
        //     selectedThread = null;
        //
        //     docModel.applyDeleteEveryListedComment(array_from(threadedCommentList));
        //
        //     return $.when();
        // };

        /**
         *  This naming is due to the implementation/expectation of the threaded-comments-pane.
         *  A more precise naming was `changeComment`.
         */
        collection.changeThread = function (threadedCommentModel, commentInfo) {
            // globalLogger.info('+++ ThreadedCommentCollection :: changeThread :: threadedCommentModel, text ', threadedCommentModel, text);

            docModel.applyChangeComment(threadedCommentModel, commentInfo);

            return $.when();
        };

        /**
         *  This naming is due to the implementation/expectation of the threaded-comments-pane.
         *  A more precise naming was `insertComment`.
         */
        collection.insertThread = function (anchor, commentInfo, positionList) {
            // globalLogger.info('+++ ThreadedCommentCollection :: insertThread :: anchor, text, positionList ', anchor, text, positionList);

            docModel.applyInsertComment(anchor, commentInfo, positionList);

            return $.when();
        };

        // public methods that are related to the direct manipulation of ------
        // `ThreadedCommentModel`s within the collections internal list. ------

        collection.getCommentIndex = function (commentModel) {
            return getCommentIndex(commentModel);
        };

        collection.getCommentStart = function (commentModel) {

            var slideIndex    = getSlideIndexBySlideId(commentModel.getSlideId());
            var commentIndex  = getCommentIndex(commentModel);

            return [slideIndex, commentIndex];
        };

        collection.removeCommentFromList = function (commentModel, commentList, external) {
            commentModel = threadedCommentList.splice(getCommentIndex(commentModel), 1)[0]; // {threadedCommentModel} | {Undefined}

            if (commentModel) {

                var parentThread = commentModel.getParent();
                if (!parentThread) {
                    // no parent for `threadedCommentModel` means the comment model itself represents a parent thread.

                    // checking, it the parent still has children -> this can only happen via undo (DOCS-2702)
                    var childComments = (docModel.isUndoRedoRunning() || external) ? collection.getChildComments(commentModel) : null;

                    if (childComments) {
                        collection.makeChildrenToParents(commentModel, commentList); // making all children to parent comments
                        docModel.trigger('after:move:threadedcomments'); // full repaint of comments pane required
                    } else {

                        // the collection's thread-count got decremented ... broadcast both counts.
                        var count = collection.getParentComments().length;
                        docModel.trigger('threadedcommentcollection:threadcount:change', { slideId: commentModel.getSlideId(), current: count, recent: (count + 1) });

                        // additionally check too whether this specific comment was the one that got removed last from the list.
                        // if (threadedCommentList.length === 0) {
                        //
                        //     docModel.trigger('threadedcommentcollection:empty', { commentModel: threadedCommentModel });
                        // }
                    }
                } else if (collection.getChildComments(parentThread) === null) {

                    // single comment thread ... no other replies.
                    docModel.trigger('change:threadedcomment:noreply', commentModel);
                }
                if ((commentModel === selectedThread) || (threadedCommentList.length === 0)) {

                    selectedThread = null;
                }
            }
            return commentModel; // {threadedCommentModel} | {Undefined}
        };

        collection.removeThreadFromList = function (commentModel) {
            var returnValue;

            var commentList = collection.getByAddress(commentModel.getAnchor()); // {threadedCommentModel[]} | {Null}
            if (commentList && commentList.length) {

                var parentThread = commentList[0];
                var commentIndex = getCommentIndex(parentThread);
                var commentCount = commentList.length;

                commentList = threadedCommentList.splice(commentIndex, commentCount);
                if (commentList && commentList.length) {
                    returnValue = commentList;

                    docModel.trigger('delete:threadedcomment:thread', parentThread);

                    // the collection's thread-count got decremented ... broadcast both counts.
                    var count = collection.getParentComments().length;
                    docModel.trigger('threadedcommentcollection:threadcount:change', { slideId: commentModel.getSlideId(), current: count, recent: (count + 1) });

                    // additionally check too whether this specific thread was the one that got removed last from the list.
                    // if (threadedCommentList.length === 0) {
                    //
                    //     docModel.trigger('threadedcommentcollection:empty', { commentList: returnValue });
                    // }
                }
                if ((commentModel === selectedThread) || (threadedCommentList.length === 0)) {

                    selectedThread = null;
                }
            }
            return returnValue; // {threadedCommentModel[]} | {Undefined}
        };

        collection.insertCommentIntoList = function (commentModel, commentIndex) {
            var threadCountRecent = collection.getParentComments().length;

            threadedCommentList.splice(commentIndex, 0, commentModel);

            var threadCountCurrent = collection.getParentComments().length;
            if (threadCountCurrent !== threadCountRecent) {

                // the collection's thread-count got incremented ... broadcast both counts.
                docModel.trigger('threadedcommentcollection:threadcount:change', { slideId: commentModel.getSlideId(), current: threadCountCurrent, recent: threadCountRecent });

            } else {
                var parentThread      = commentModel.getParent();
                var childCommentList  = ((parentThread && collection.getChildComments(parentThread)) || []);

                if (childCommentList.length === 1) {

                    // multi comment thread ... first reply added right now.
                    docModel.trigger('change:threadedcomment:firstreply', commentModel);
                }
            }
        };

        collection.setUnsavedComment = function (comment) {
            unsavedComment = comment;
        };

        collection.getUnsavedComment = function () {
            return unsavedComment;
        };

        /**
         * Removing an optionally set unsaved comment. It might be possible, that the model can be destroyed,
         * because it is no longer required. In OX Presentation this is the case, when a new thread is generated.
         * When the user presses "Cancel" or also when she presses "Save", the temporary new generated
         * ThreadedCommentModel can be destroyed.
         */
        collection.deleteUnsavedComment = function () {
            if (unsavedComment?.isNewThread) { unsavedComment?.model?.destroy(); }
            unsavedComment = null;
        };

        collection.hasUnsavedComment = function () {
            return !!unsavedComment;
        };

        collection.createNewCommentPosition = function (defaultPosition) {
            //var boundElement = null; // the parent element limiting the movement of the comment
            // var boundElementWidth = null;
            //var boundElementHeight = null;
            var commentsPositionList = getCommentsPositionList();

            var resultPosition = null;
            var bubbleSideLenght = null;

            if (!commentsPositionList.length) {
                resultPosition = defaultPosition;
            } else {
                //boundElement = $('.app-content .page');
                bubbleSideLenght = convertLengthToHmm(25, 'px'); // fixed css bubble value
                // boundElementWidth = convertLengthToHmm(boundElement.width() - bubbleSideLenght, 'px');
                //boundElementHeight = convertLengthToHmm(boundElement.height(), 'px') - bubbleSideLenght;
                // var defaultPxValue = defaultPosition[0];

                resultPosition = reduceToPositionList(defaultPosition, commentsPositionList);
            }

            return resultPosition;

            /**
             * Returns the reduced to a new empty area comment bubble position value.
             *
             * @param {Array} position
             *  a position of a comment bubble existing on the slide
             *
             * @param {Array} commentPositions
             *  if false, then the X and Y coordinates are not necessarily the same
             *
             * @returns {Array}
             *  New position value
             */
            function reduceToPositionList(position, commentPositions) {
                var xStart = position[0]; // new bubble X start value in hmm
                var yStart = position[1]; // new bubble Y start value in hmm
                var xEnd = xStart + bubbleSideLenght; // new bubble X end value in hmm
                var yEnd = yStart + bubbleSideLenght; // new bubble Y end value in hmm
                var spaceBetweenComments = 100; // bubbleSideLenght / 2;

                var newPosition = position;
                var filterList = [];

                // var positionExists = commentsPositionList.forEach(function (commentPosition) {
                //     var bubbleXStart = commentPosition[0]; // existing bubble X start value
                //     var bubbleYStart = commentPosition[1]; // existing bubble Y start value
                //     var bubbleXEnd = bubbleXStart + bubbleSideLenght; // existing bubble X end value
                //     var bubbleYEnd = bubbleYStart + bubbleSideLenght; // existing bubble Y end value

                //     if (((bubbleXStart >= xStart && bubbleXStart <= xEnd) || (bubbleXEnd >= xStart && bubbleXEnd <= xEnd)) &&
                //         ((bubbleYStart >= yStart && bubbleYStart <= yEnd) || (bubbleYEnd >= yStart && bubbleYEnd <= yEnd))
                //     ) {
                //         return true;
                //     }
                //     return false;
                // });

                // if (positionExists) {
                //     var bubbleXStart = positionExists[0]; // existing bubble X start value
                //     var bubbleYStart = positionExists[1]; // existing bubble Y start value
                //     var bubbleXEnd = bubbleXStart + bubbleSideLenght + spaceBetweenComments; // existing bubble X end value
                //     var bubbleYEnd = bubbleYStart + bubbleSideLenght + spaceBetweenComments; // existing bubble Y end value
                //     return reduceToPositionList([bubbleXEnd, bubbleYEnd]);
                // }

                commentPositions.forEach(function (commentPosition) {
                    var bubbleXStart = commentPosition[0]; // existing bubble X start value
                    var bubbleYStart = commentPosition[1]; // existing bubble Y start value
                    var bubbleXEnd = bubbleXStart + bubbleSideLenght; // existing bubble X end value
                    var bubbleYEnd = bubbleYStart + bubbleSideLenght; // existing bubble Y end value

                    if (((bubbleXStart >= xStart && bubbleXStart <= xEnd) || (bubbleXEnd >= xStart && bubbleXEnd <= xEnd)) &&
                        ((bubbleYStart >= yStart && bubbleYStart <= yEnd) || (bubbleYEnd >= yStart && bubbleYEnd <= yEnd))
                    ) {
                        newPosition = [bubbleXEnd + spaceBetweenComments, bubbleYEnd + spaceBetweenComments];
                    } else {
                        filterList.push(commentPosition);
                    }
                    return false;
                });

                if (position !== newPosition) {
                    return reduceToPositionList(newPosition, filterList);
                }

                return newPosition;
            }
        };

        // initialization -----------------------------------------------------

        app.onInit(function () {
            docView = app.getView();
        });

        docModel.waitForImportSuccess(function () {

            collection.listenTo(docModel, 'select:threadedcomment', function (threadedCommentModel) {
                // globalLogger.info('+++ ThreadedCommentCollection :: handle "select:threadedcomment" :: evt, threadedCommentModel : ', evt, threadedCommentModel);
                if (threadedCommentModel instanceof ThreadedCommentModel) {

                    selectedThread = (threadedCommentModel.isReply() ? threadedCommentModel.getParent() : threadedCommentModel);
                } else {
                    selectedThread = null;
                }
            });

            collection.listenTo(docView, 'thread:deselect:all', function () {
                selectedThread = null;
            });
        });
    }
}
