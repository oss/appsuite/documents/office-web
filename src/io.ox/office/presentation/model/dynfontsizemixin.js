/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { convertLengthToHmm, insertHiddenNodes } from '@/io.ox/office/tk/utils';
import { AUTORESIZEHEIGHT_CLASS, NO_WORDWRAP_CLASS, getAllGroupDrawingChildren, getContentNode,
    getDrawingRotationAngle, isAutoResizeHeightDrawingFrame, isGroupDrawingFrame, isGroupDrawingFrameWithShape,
    isShapeDrawingFrame, isTableDrawingFrame, isTextFrameShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { PARAGRAPH_NODE_LIST_EMPTY_CLASS, SLIDE_SELECTOR, isEmptyParagraph } from '@/io.ox/office/textframework/utils/dom';
import { getOxoPosition } from '@/io.ox/office/textframework/utils/position';
import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { isTitlePlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';
import { extendPlaceholderProp } from '@/io.ox/office/presentation/utils/presentationutils';

var // The maximum scaled font-size (the characters have 2.3333em)
    MAX_FONT_SIZE = 16,

    // Steps to check. The maximum steps are 6
    INDEX_ARRAY = [0, 1, 2, 3, 4, 5],

    // The font sizes steps. Each size has an adder of 1.2
    FONT_ARRAY = [4, 5.2, 6.4, 7.6, 8.8, 10, 11.2, 12.4, 13.6, 14.8, 14.8, 16];

// mix-in class DynFontSizeMixin ==========================================
function DynFontSizeMixin(app) {

    var // self reference (the document model)
        self = this,

        // all drawings nodes that need to be updated
        drawingQueue = $(),

        // last step
        lastStep,
        checkedSteps,

        // a promise indicating that operations will be generated and applied
        formatDef = null;

    // private methods ----------------------------------------------------

    /**
     * Calculating the dynamic font size by binary search.
     *
     * @param {jQuery|Node} textFrameContent
     *  the element which contains the text
     *
     * @param {jQuery|Node} clonedTextFrameContent
     *  a cloned frame which contains the textframe for calculation
     *
     * @param {jQuery|Node} clonedTextFrame
     *  the element which contains the text
     *
     * @param {Integer} maxHeight
     *  the maxHeight of the container
     *
     * @param {Integer} i
     *  current step number
     *
     * @returns {Integer|null}
     *  if the current step found the fitting size null, would be returned.
     *  if the current step has not found the fitting size, the current height would be returned
     */
    function updateStep(textFrameContent, clonedTextFrameContent, clonedTextFrame, maxHeight, i) {

        var // the current values
            currentHeight        = 0,

            newStepId,

            // the cildren of the outer textframe
            paraChildren = clonedTextFrame.children().get(),

            // if a paragraph with content found, this value is true
            emptyParagraphsChecked = false;

        /**
         * Checks if the current height of the content is between 90% and 100% of the container height
         *
         * @returns {boolean}
         */
        function checkValues() {
            return currentHeight > maxHeight * 0.95 && currentHeight < maxHeight;
        }

        /**
         * Updates fontSize, fontScale and lineReduction of the current textframe.
         *
         * @param {Integer} stepId
         */
        function updateTextFrame(stepId) {
            textFrameContent.css('font-size', FONT_ARRAY[stepId] + 'px');
            textFrameContent.attr('font-scale', Math.min(FONT_ARRAY[stepId] / MAX_FONT_SIZE, 1));
            // Bug-57232: line reduction disabled due to jumping issue
        }

        /**
         * Updates fontSize, fontScale and lineReduction of the cloned textframe.
         *
         * @param {Integer} stepId
         */
        function updateClone(stepId) {
            clonedTextFrameContent.css('font-size', FONT_ARRAY[stepId] + 'px');
            clonedTextFrameContent.attr('font-scale', Math.min(FONT_ARRAY[stepId] / MAX_FONT_SIZE, 1));
            // Bug-57232: line reduction disabled due to jumping issue
            updateHeight();
        }

        /**
         * Finding the last paragraph which is not empty. Also check for empty spans inside a paragraph.
         */
        function updateHeight() {
            currentHeight = 0;
            paraChildren.reverse().forEach(function (para) {
                para = $(para);
                if (emptyParagraphsChecked) {
                    // found content before
                    currentHeight += para.outerHeight(true);
                } else if (!para.hasClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS) && !isEmptyParagraph(para)) {
                    // found no content before
                    currentHeight += para.outerHeight(true);
                    emptyParagraphsChecked = true;
                }
            });
        }

        if (i === 0) {
            // Set font-size initial to the middle between min and max font-size.
            lastStep = 5;
            updateClone(lastStep);
        }

        updateHeight();

        if (checkValues()) {
            updateTextFrame(lastStep);
            return null;
        } else {
            checkedSteps.push(lastStep);
            // Update to a new font size and line height
            if (currentHeight > maxHeight) {
                // size must be reduced
                newStepId = Math.max(lastStep - 1, 0);
            } else {
                // size must be expanded
                newStepId = Math.min(lastStep + 1, FONT_ARRAY.length - 1);
            }
            if (_.contains(checkedSteps, newStepId)) {
                updateTextFrame(newStepId);
                return null;
            }
            // Setting the new values
            updateClone(newStepId);
            lastStep = newStepId;
        }

        if (i === INDEX_ARRAY.length - 1) {
            // last step reached and final check of the height
            if (checkValues()) {
                updateTextFrame(lastStep);
            } else if (currentHeight > maxHeight) {
                updateTextFrame(lastStep - 1);
            } else {
                updateTextFrame(lastStep);
            }
            return null;
        }
        return currentHeight;
    }

    function updateDrawing(drawing) {

        var $drawing = $(drawing),
            textframecontent = $drawing.find('>.textframecontent'),
            initialFontSize = parseFloat(textframecontent.css('font-size')),
            initialLineReduction = parseFloat(textframecontent.attr('linereduction')) || 0, // avoid NaN
            textFrame = textframecontent.find('>.textframe'),
            drawingHeight = null,
            currentHeight = null,
            drawingWidth = null,
            currentWidth = null,
            drawingAttrs = null,

            // the surrounding DOM for the clonded element
            hiddenFrame = null,
            // the clone of the textFrameContent
            clonedTextFrameContent = null,
            // the clone of the textFrame
            clonedTextFrame = null,

            angle = getDrawingRotationAngle(self, drawing);

        // local method to get left position value for drawing with auto resize and no wrap word,
        // depending on alignment of first paragraph child
        function getLeftPostion() {

            var pAlignment = null;
            var leftPos = null;

            if (_.isNumber(angle) && angle % 360 !== 0) {
                // if rotated, treat like center alignment
                leftPos = parseFloat(drawing.style.left) - (currentWidth - drawingWidth) / 2;
            } else {
                pAlignment = getExplicitAttributeSet($drawing.find('.p').first());
                pAlignment = pAlignment && pAlignment.paragraph && pAlignment.paragraph.alignment;

                switch (pAlignment) {
                    case 'left':
                        leftPos = parseFloat(drawing.style.left);
                        break;
                    case 'center':
                    case 'justify':
                        leftPos = parseFloat(drawing.style.left) - (currentWidth - drawingWidth) / 2;
                        break;
                    case 'right':
                        leftPos = parseFloat(drawing.style.left) - (currentWidth - drawingWidth);
                        break;
                    default:
                        leftPos = parseFloat(drawing.style.left);
                }
            }

            return convertLengthToHmm(leftPos, 'px');
        }

        // local method to get top position value for drawing with auto resize and wrap word,
        // depending on shape vertical alignment
        function getTopPostion() {

            var vAlignment = null;
            var topPos = null;

            if (_.isNumber(angle) && angle % 360 !== 0) {
                // if rotated, treat like centered vertical alignment
                topPos = parseFloat(drawing.style.top) - (currentHeight - drawingHeight) / 2;
            } else {
                vAlignment = getExplicitAttributeSet($drawing);
                vAlignment = vAlignment && vAlignment.shape && vAlignment.shape.anchor;

                switch (vAlignment) {
                    case 'top':
                        topPos = parseFloat(drawing.style.top);
                        break;
                    case 'centered':
                        topPos = parseFloat(drawing.style.top) - (currentHeight - drawingHeight) / 2;
                        break;
                    case 'bottom':
                        topPos = parseFloat(drawing.style.top) - (currentHeight - drawingHeight);
                        break;
                    default:
                        topPos = parseFloat(drawing.style.top);
                }
            }

            return convertLengthToHmm(topPos, 'px');
        }

        /**
         * Creates the DOM structure for the cloned element and appends it
         */
        function createClone() {
            var clonedDrawing = textframecontent.closest('.drawing').clone();

            clonedTextFrameContent = clonedDrawing.find('.textframecontent');
            clonedTextFrame = clonedTextFrameContent.find('.textframe');

            hiddenFrame = $('<div class="io-ox-office-main io-ox-office-presentation-main io-ox-office-edit-main">')
                .append($('<div class="page formatted-content">')
                    .append($('<div class="p slide">')
                        .append(clonedDrawing)
                    )
                );

            insertHiddenNodes(hiddenFrame);
        }

        /**
         * Removes everything related to the cloned element
         */
        function removeClone() {
            hiddenFrame.remove();
        }

        /**
         * Function which is called so long the INDEX_ARRAY. Each call executes the updateStep function.
         * If the updateStep function founds the correct height, the next function calls are skipped.
         *
         * @param {Integer} i
         *  current step number
         */
        function innerUpdate(i) {
            if (lastHeight === null) {
                return;
            }
            lastHeight = updateStep(textframecontent, clonedTextFrameContent, clonedTextFrame, maxHeight, i);
        }

        function returnFontSize() {
            var newFontScale = parseFloat(textframecontent.css('font-size'));
            var newLineReduction = parseFloat(textframecontent.attr('linereduction'));

            if (Math.abs(newFontScale - initialFontSize) < 0.001 && Math.abs(newLineReduction - initialLineReduction) < 0.001) {
                return;
            }
            return {
                attrs: {
                    shape: {
                        fontScale: Math.min(newFontScale / MAX_FONT_SIZE, 1),
                        lineReduction: newLineReduction / 100,
                        autoResizeText: true,
                        autoResizeHeight: false,
                        noAutoResize: false
                    }
                },
                drawing
            };
        }

        if (!textFrame.children().length) { return; } // empty drawing

        // console.log('updateDrawing', textframecontent.attr('class'));
        if (textframecontent.hasClass('autoresizeheight') || !textframecontent.hasClass('autoresizetext')) {

            if (initialFontSize !== MAX_FONT_SIZE || initialLineReduction !== 0) {

                textframecontent.css('font-size', MAX_FONT_SIZE + 'px');
                textframecontent.attr('font-scale', null);
                textframecontent.attr('linereduction', null);
            }
            if (textframecontent.hasClass('autoresizeheight')) {

                drawingHeight = $drawing.data('drawingHeight') || 0;
                currentHeight = $drawing.height();
                drawingWidth = $drawing.data('drawingWidth') || 0;
                currentWidth = $drawing.width();

                if (textframecontent.hasClass(NO_WORDWRAP_CLASS) && Math.abs(drawingWidth - currentWidth) > 1) {
                    $drawing.data('drawingWidth', currentWidth);

                    drawingAttrs = {
                        height: convertLengthToHmm(currentHeight, 'px'),
                        width: convertLengthToHmm(currentWidth, 'px'),
                        left: getLeftPostion(),
                        top: convertLengthToHmm(parseFloat(drawing.style.top), 'px')
                    };
                    return $.when({ attrs: { drawing: drawingAttrs }, drawing });
                } else if (Math.abs(drawingHeight - currentHeight) > 1) {

                    $drawing.data('drawingHeight', currentHeight);

                    drawingAttrs = {
                        height: convertLengthToHmm(currentHeight, 'px'),
                        width: convertLengthToHmm($drawing.width(), 'px'),
                        left: convertLengthToHmm(parseFloat(drawing.style.left), 'px'),
                        top: getTopPostion()
                    };
                    return $.when({ attrs: { drawing: drawingAttrs }, drawing });
                }
            }
            return;
        }

        // fix for Bug 48665
        var maxHeight = $drawing.data('widthHeightRev') ? $drawing.width() : $drawing.height();
        maxHeight -= (parseFloat(textFrame[0].style.paddingBottom) || 0);

        if (!maxHeight) { return; }

        var lastHeight = -1;

        // create a clone for the calculation
        createClone();

        // call the innerUpdate function related to the INDEX_ARRAY
        var def = null;
        checkedSteps = [];
        if (_.browser.IE && !_.browser.Edge) {
            def = self.iterateArraySliced(INDEX_ARRAY, innerUpdate, { slice: 1 });
        } else {
            _.each(INDEX_ARRAY, innerUpdate);
            def = $.when(true);
        }

        // clean up the cloned elements
        removeClone();

        return def.then(returnFontSize);
    }

    function handleContainerVisibility(slide, invisibleWithReflow) {
        var makeVisible = !slide.hasClass('invisibleslide');
        self.handleContainerVisibility(slide, { invisibleWithReflow, makeVisible });
    }

    /**
     * Updating some specified drawings in a specified slide. During loading a document
     * the slides are formatted at all. In that context also the font size is set and it is not
     * necessary to specify the drawings, because all of them are updated.
     *
     * After the document is loaded, it is only necessary to update those drawings, who got new
     * attributes or whose content has changed. In this case, it is necessary to specify the
     * drawings, so that not all drawings of a slide need to be updated.
     *
     * @param {jQuery} slide
     *  The slide of the drawing
     *
     * @param {String} slideID
     *  The slideID of the slide
     *
     * @param {jQuery} [drawings]
     *  this drawings will be updated.
     *
     * @returns {jQuery.Promise | void} // TODO: check-void-return
     *  A promise that will be resolved or rejected after all drawings are updated.
     */
    function updateSlide(slide, slideID, drawings) {

        // ignoring layout or master slides
        if (!self.isStandardSlideId(slideID)) {
            return;
        }

        var defs = [];

        handleContainerVisibility(slide, true);

        _.each(drawings, function (drawing) {
            var def = updateDrawing(drawing);
            if (def) { defs.push(def); }
        });

        if (defs.length) {
            return $.when(...defs).then(function (...args) {
                handleContainerVisibility(slide, false);
                return args;
            });
        } else {
            handleContainerVisibility(slide, false);
        }
    }

    // The drawing element as DOM node or jQuery object.
    function registerDrawing(newDrawing) {
        // OT: Only storing drawings, if they are selected, but not if they are resized, moved, ... . External operations must not lead to an operation for updating the font size.
        if (app.isOTEnabled() && (!self.getSelection().isSelectedDrawingNode(newDrawing) || self.isDrawingChangeActive())) { return; }
        // store the new drawing(s) in the collection (jQuery keeps the collection unique) -> but not storing images or tables
        if (newDrawing && (isShapeDrawingFrame(newDrawing) || isGroupDrawingFrame(newDrawing)) && !isTableDrawingFrame(newDrawing)) {
            drawingQueue = drawingQueue.add(newDrawing);
            if (!formatDef) { formatDef = $.Deferred(); } // creating the promise synchronously
            // setting root element attribute for automated tests
            app.setRootAttribute("data-dynamic-font-size", true);
        }
        // TODO: Remove asap check for tables (problem in DrawingFrame.isShapeDrawingFrame)
        // if (newDrawing && (DrawingFrame.isShapeDrawingFrame(newDrawing) || DrawingFrame.isGroupDrawingFrame(newDrawing))) { drawingQueue = drawingQueue.add(newDrawing); }
    }

    function updateDynFontSizeInDrawings() {

        if (!formatDef) {
            // deleting root element attribute for automated tests
            app.setRootAttribute("data-dynamic-font-size", null);
            return; // nothing to do, no registered drawing
        }

        var promise = updateDynFontSizeForDrawings(drawingQueue);

        drawingQueue = $();

        // saving the global synchronously generated promise in a local promise and set it to null,
        // so that following drawings in "registerDrawing" can be handled with a new promise.
        var localFormatDef = formatDef;
        formatDef = null;

        promise.then(function () {
            localFormatDef.resolve();
        }, function () {
            localFormatDef.reject();
        }).always(function () {
            // deleting root element attribute for automated tests
            app.setRootAttribute("data-dynamic-font-size", null);
        });
    }

    // updating the specified drawings
    //
    // Info: This function handles only drawings on document slides, not on layout
    //       or master slides. Therefore the 'target' is always NOT defined.
    //
    //  Because of the 'target'
    function updateDynFontSizeForDrawings(drawings) {

        var defs = [];
        var slideID = null;
        // iterating over all drawings
        _.each(drawings, function (drawing) {

            var slide = $(drawing).closest(SLIDE_SELECTOR);
            slideID = self.getSlideId(slide);

            self.appendSlideToDom(slideID); // attaching the slide node to the DOM

            if ($(drawing).is('.grouped') && isAutoResizeHeightDrawingFrame(drawing)) { // convert autfit grouped shapes to no autofit for simplified handling
                defs.push($.when({ attrs: { shape: { autoResizeText: false, autoResizeHeight: false, noAutoResize: true } }, drawing }));
                getContentNode(drawing).removeClass(AUTORESIZEHEIGHT_CLASS); // removing marker class immediately, so that drawing formatting code does not ignore height (57892)
                return;
            }
            // TODO: operate correct on grouped drawings (width/height are different to ungrouped drawings)
            // Also no text resize for 'title' place holders
            if ($(drawing).is('.grouped') || isTitlePlaceHolderDrawing(drawing)) { return; }

            // TODO: This listener introduces a race condition with event 'drawingHeight:update' and causes task 63034
            // self.listenTo(self.getUndoManager(), 'undo:after redo:after', function () {
            //     updateSlide(slide, slideID, $(drawing));
            // });

            // skip slides if they are not formatted at all -> asking slide format manager
            if (!self.getSlideFormatManager().isUnformattedSlide(slideID)) {
                var change = updateSlide(slide, slideID, $(drawing));
                if (change) { defs.push(change); }
            }
        });

        if (defs.length) {
            return $.when(...defs).then(function (...args) {

                //controller update must be called for changed font-size in the toolbar
                app.getController().update();

                if (!app.isEditable()) { return; }

                if (app.isOTEnabled() && self.isDrawingChangeActive()) { return; } // no update for drawings that are in this moment resized, moved, rotated, ...

                if (self.isLayoutOrMasterId(slideID)) { return; } // master and layout slides are not supported by this function

                if (self.isProcessingActions()) { return; } // return, to avoid recursive calls (but come back later -> drawings might no longer exist)

                var ops = [];

                _.flatten(args).forEach(function (change) {

                    if (!change) { return; }

                    var position = getOxoPosition(self.getRootNode(slideID), change.drawing);

                    ops.push({
                        noUndo: true,
                        name: SET_ATTRIBUTES,
                        start:  _.copy(position),
                        attrs: change.attrs,
                        target: self.getOperationTargetBlocker()
                    });

                    // Info: Adding the operationsTargetBlocker is necessary because the operation is not
                    //       executed on the active slide. Therefore the target must not be added automatically
                    //       by the finalizeOperations handler.
                    //       Because all affected slides are document slides, it is never necessary to add any
                    //       target. If this is required, this target must be set explicitely to the operation.
                });
                self.applyOperations(ops);

            });
        } else {
            return $.when();
        }
    }

    // public methods -----------------------------------------------------

    /**
     * Handling the property 'autoResizeHeight' of a selected text frame node.
     *
     * @param {Boolean} autoResizeHeight
     *  Whether the property 'autoResizeHeight' of a selected text frame shall be
     *  enabled or disabled.
     */
    self.handleTextFrameAutoFit = function (autoResizeHeight, autoResizeText) {

        var selection = self.getSelection();
        // the operations generator
        var generator = self.createOperationGenerator();
        // the options for the setAttributes operation
        var operationOptions = {};
        // a selected text frame node or the text frame containing the selection
        var textFrame = null;
        // a collector for all affected drawings in a multi drawing selection
        var textFrameCollector = null;
        // a selected drawing node
        var selectedDrawing = null;

        /**
         * Helper function to handle all shapes inside a specified drawing group node.
         */
        function handleShapesInGroup(groupNode) {

            _.each(getAllGroupDrawingChildren(groupNode), function (child) {

                if (isTextFrameShapeDrawingFrame(child)) {

                    textFrame = $(child);

                    // collecting the attributes for the operation
                    operationOptions.attrs =  {};
                    operationOptions.attrs.shape = {};

                    if (!app.isODF()) { operationOptions.attrs.shape.autoResizeText   = autoResizeText || false; }
                    operationOptions.attrs.shape.autoResizeHeight = autoResizeHeight || false;
                    operationOptions.attrs.shape.noAutoResize = (!autoResizeText && !autoResizeHeight);
                    if (app.isODF()) { operationOptions.attrs.shape.wordWrap = true; } // odf requires wordWrap value, later this might be (autoResizeHeight && !autoResizeText)

                    operationOptions.start = getOxoPosition(self.getNode(), textFrame, 0);

                    if (autoResizeText || autoResizeHeight) { // Bug 48714 fix for undo
                        operationOptions.attrs.drawing = {};
                        extendPlaceholderProp(app, textFrame, operationOptions);
                    }

                    // generate the 'setAttributes' operation
                    generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                    // collecting the affected drawings
                    textFrameCollector = textFrameCollector || [];
                    textFrameCollector.push(textFrame);
                }
            });
        }

        // multiselection
        if (selection.isMultiSelectionSupported() && selection.isMultiSelection()) {

            _.each(selection.getMultiSelection(), function (drawingItem) {

                var drawingNode = selection.getDrawingNodeFromMultiSelection(drawingItem);

                // filter to modify only text frames
                if (isTextFrameShapeDrawingFrame(drawingNode)) { // selected drawing text frame in a multi drawing selection

                    textFrame = drawingNode;

                    // collecting the attributes for the operation
                    operationOptions.attrs =  {};
                    operationOptions.attrs.shape = {};

                    if (!app.isODF()) { operationOptions.attrs.shape.autoResizeText   = autoResizeText || false; }
                    operationOptions.attrs.shape.autoResizeHeight = autoResizeHeight || false;
                    operationOptions.attrs.shape.noAutoResize = (!autoResizeText && !autoResizeHeight);
                    if (app.isODF()) { operationOptions.attrs.shape.wordWrap = true; } // odf requires wordWrap value, later this might be (autoResizeHeight && !autoResizeText)

                    operationOptions.start = _.clone(drawingItem.startPosition);

                    if (autoResizeText || autoResizeHeight) { // Bug 48714 fix for undo
                        operationOptions.attrs.drawing = {};
                        extendPlaceholderProp(app, textFrame, operationOptions);
                    }

                    // generate the 'setAttributes' operation
                    generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                    // collecting the affected drawings
                    textFrameCollector = textFrameCollector || [];
                    textFrameCollector.push(textFrame);

                } else if (isGroupDrawingFrameWithShape(drawingNode)) { // selected drawing groups in a multi drawing selection
                    handleShapesInGroup(drawingNode);
                }
            });

        } else if (selection.isAnyDrawingSelection()) {

            textFrame = selection.getAnyTextFrameDrawing({ forceTextFrame: true });

            // collecting the attributes for the operation
            operationOptions.attrs =  {};
            operationOptions.attrs.shape = {};

            if (!app.isODF()) { operationOptions.attrs.shape.autoResizeText   = autoResizeText || false; }
            operationOptions.attrs.shape.autoResizeHeight = autoResizeHeight || false;
            operationOptions.attrs.shape.noAutoResize = (!autoResizeText && !autoResizeHeight);
            if (app.isODF()) { operationOptions.attrs.shape.wordWrap = true; } // odf requires wordWrap value, later this might be (autoResizeHeight && !autoResizeText)

            if (selection.isAdditionalTextframeSelection()) {  // a text inside a text frame is selected
                operationOptions.start = getOxoPosition(self.getNode(), textFrame, 0);
                if (autoResizeText || autoResizeHeight) { // Bug 48714 fix for undo
                    operationOptions.attrs.drawing = {};
                    extendPlaceholderProp(app, textFrame, operationOptions);
                }
                generator.generateOperation(SET_ATTRIBUTES, operationOptions); // generate the 'setAttributes' operation
            } else {
                if (textFrame && textFrame.length > 0) { // the text frame itself is selected
                    operationOptions.start = selection.getStartPosition();
                    if (autoResizeText || autoResizeHeight) { // Bug 48714 fix for undo
                        operationOptions.attrs.drawing = {};
                        extendPlaceholderProp(app, textFrame, operationOptions);
                    }
                    generator.generateOperation(SET_ATTRIBUTES, operationOptions); // generate the 'setAttributes' operation
                } else {
                    selectedDrawing = selection.getSelectedDrawing(); // this might be a group drawing, that is selected
                    if (isGroupDrawingFrameWithShape(selectedDrawing)) {
                        handleShapesInGroup(selectedDrawing);
                    }
                }
            }

        }

        // apply all collected operations
        self.applyOperations(generator);

        if (textFrameCollector) {
            _.each(textFrameCollector, function (oneTextFrame) {
                self.updateDynFontSizeDebounced(oneTextFrame);
            });
        } else {
            self.updateDynFontSizeDebounced(textFrame);
        }
    };

    self.updateDynFontSizeDebounced = app.isODF() ? function () { return $.when(); } : self.debounce(registerDrawing, updateDynFontSizeInDrawings, { delay: 500 });

    self.getPreFlushDocumentPromise = function () {
        return formatDef || $.when();
    };

    // initialization -----------------------------------------------------

} // mixin DynFontSizeMixin

// exports ================================================================

export default DynFontSizeMixin;
