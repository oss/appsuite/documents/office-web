/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import CharacterStyles from '@/io.ox/office/textframework/format/characterstyles';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';
import TableRowStyles from '@/io.ox/office/textframework/format/tablerowstyles';

import DrawingStyles from '@/io.ox/office/presentation/format/drawingstyles';
import ParagraphStyles from '@/io.ox/office/presentation/format/paragraphstyles';
import PageStyles from '@/io.ox/office/presentation/format/pagestyles';
import SlideStyles from '@/io.ox/office/presentation/format/slidestyles';
import TableCellStyles from '@/io.ox/office/presentation/format/tablecellstyles';

// constants ==================================================================

// definitions for global document attributes
const DOCUMENT_ATTRIBUTES = {
};

// definitions for the default text list style attributes
const DEFAULT_TEXT_LISTSTYLE_ATTRIBUTES = {
    l1: { def: {} },
    l2: { def: {} },
    l3: { def: {} },
    l4: { def: {} },
    l5: { def: {} },
    l6: { def: {} },
    l7: { def: {} },
    l8: { def: {} },
    l9: { def: {} }
};

// definitions for presentation attributes
const PRESENTATION_ATTRIBUTES = {
    phType: { def: 'obj', scope: 'element' },
    phSize: { def: 'full', scope: 'element' },
    phIndex: { def: 0, scope: 'element' },
    customPrompt: { def: false, scope: 'element' },
    userTransformed: { def: false, scope: 'element' },
    phAudio: { def: false, scope: 'element' },
    phAudioFile: { def: false, scope: 'element' },
    phVideo: { def: false, scope: 'element' }
};

// definitions for change tracking attributes (TODO: Removing this attributes asap)
const CHANGES_ATTRIBUTES = {
    inserted: { def: '', scope: 'element' },
    removed: { def: '', scope: 'element' },
    modified: { def: '', scope: 'element' }
};

// definitions for layout attributes

const LAYOUT_ATTRIBUTES = {
    slidePaneWidth: { def: 10 }
};

// mix-in class ModelAttributesMixin ======================================

/**
 * A mix-in class for the document model class PresentationModel providing
 * the style sheet containers for all attribute families used in a
 * presentation document.
 */
export default function ModelAttributesMixin() {

    // formatting attributes registration
    this.defAttrPool.registerAttrFamily("document", DOCUMENT_ATTRIBUTES);
    this.defAttrPool.registerAttrFamily('presentation', PRESENTATION_ATTRIBUTES);
    this.defAttrPool.registerAttrFamily('changes', CHANGES_ATTRIBUTES);
    this.defAttrPool.registerAttrFamily('layout', LAYOUT_ATTRIBUTES);
    this.defAttrPool.registerAttrFamily('defaultTextListStyles', DEFAULT_TEXT_LISTSTYLE_ATTRIBUTES);

    // style sheet collections
    this.characterStyles = this.addStyleCollection(new CharacterStyles(this, { families: ['changes'] }));
    this.paragraphStyles = this.addStyleCollection(new ParagraphStyles(this));
    this.tableStyles = this.addStyleCollection(new TableStyles(this));
    this.tableRowStyles = this.addStyleCollection(new TableRowStyles(this));
    this.tableCellStyles = this.addStyleCollection(new TableCellStyles(this));
    this.drawingStyles = this.addStyleCollection(new DrawingStyles(this));
    this.pageStyles = this.addStyleCollection(new PageStyles(this));
    this.slideStyles = this.addStyleCollection(new SlideStyles(this));
}
