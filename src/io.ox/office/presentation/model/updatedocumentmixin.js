/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';
import { locationHash } from '$/url';

import { getUrlFlag } from '@/io.ox/office/textframework/utils/config';
import { HIDDENAFTERATTACH_CLASSNAME, getPageContentNode, handleReadOnly } from '@/io.ox/office/textframework/utils/dom';
import { getNumberOption } from '@/io.ox/office/textframework/utils/textutils';

// mix-in class UpdateDocumentMixin ======================================

/**
 * A mix-in class for the 'updateDocumentFormatting' function.
 *
 * @param {PresentationApp} app
 *  The application object.
 */
export default function UpdateDocumentMixin(app) {

    // self reference for local functions
    var self = this;
    // the page node
    var pagediv = null;
    // presentation mode options sent via launch options or URL parameters
    // -> these options are specified, if the apps shall start in presentation mode
    var presentationModeOptions = null;

    // private methods ----------------------------------------------------

    /**
     * Determining the first slide that needs to be formatted. If not specified, the first
     * slide of the active view (the document view) will be formatted.
     * But this can be overwritten in the reload process (using app.getLaunchOption('selection'))
     * or with a deep linking URL.
     *
     * @returns {String}
     *  The ID of the first slide to be formatted.
     */
    function getIdOfFirstSlideToFormat() {

        // the ID of the first slide to be formatted
        var firstSlideId = null;
        // optionally available launch options in reload process
        var selectionOptions = app.getLaunchOption('selection');
        // a zero-based number specifying the start slide for the presentation mode
        var presentationModeStartSlide = 0;
        // optionally available launch option when loading a specified comment
        var commentIdOption = null;

        if (presentationModeOptions) {
            presentationModeStartSlide = _.isObject(presentationModeOptions) ? getNumberOption(presentationModeOptions, 'startSlide', 0) : 0;
            firstSlideId = self.getSlideIdByPosition(presentationModeStartSlide);
        } else if (selectionOptions) {
            firstSlideId = selectionOptions.target ? selectionOptions.target : self.getSlideIdByPosition(selectionOptions.start[0]);
        } else {
            commentIdOption = app.getLaunchOption('comment_id');
            if (commentIdOption) { firstSlideId = self.getSlideIdFromComment(commentIdOption); }
        }

        return firstSlideId ? firstSlideId : self.getIdOfFirstSlideOfActiveView();
    }

    /**
     * Collecting the parameters that are required, if the application shall start
     * in the presentation mode. These parameters can be inserted into the application
     * using the launch options (reload, single tab) or as URL parameters (multi tab).
     *
     *  It is possible to specify the following URL parameters optionally:
     *   - presentationMode (Boolean)
     *      Whether the app shall start in presentation mode.
     *   - fullscreen (Boolean, default value is true)
     *      Whether the presentation mode shall be started in full screen mode.
     *      Only evaluated if 'presentationMode' is true.
     *   - startSlide (Number, default value is 0)
     *      The zero-base position of the slide with that the presentation starts.
     *      Only evaluated if 'presentationMode' is true.
     *
     *  Using the launch option, the key 'presentationMode' can be an object or a
     *  boolean value.
     *  It it is set to 'true', the default value for 'fullscreen' (true) and
     *  startSlide (0) are used.
     *  Example: { presentationMode: true }
     *  If it is an object, 'fullscreen' and 'startSlide' can be set independently.
     *  Example: { presentationMode: { fullscreen: false, startSlide: 2 } }
     */
    function collectPresentationModeOptions() {

        var presentationMode = getUrlFlag('presentationMode');
        var fullscreen = null;
        var startSlide = null;

        if (presentationMode) { // checking URL

            fullscreen = getUrlFlag('fullscreen');
            startSlide = locationHash('startSlide');

            presentationModeOptions = { startInPresentationMode: true };

            if (_.isBoolean(fullscreen)) { presentationModeOptions.fullscreen = fullscreen; }

            if (_.isString(startSlide)) {
                startSlide = parseInt(startSlide, 10) || 0;
                presentationModeOptions.startSlide = startSlide;
            }

        } else if (app.getLaunchOption('presentationMode')) {
            presentationModeOptions = app.getLaunchOption('presentationMode');
            if (_.isBoolean(presentationModeOptions)) { presentationModeOptions = {}; }
            presentationModeOptions.startInPresentationMode = true;
        }
    }

    /**
     * Registering a listener function for the 'tab:activated' event, that is triggered after
     * the first finished activation of a toolbar tab.
     */
    function registerFirstTabActivatedHandler() {

        function activateThumbNails() {
            // adding delay for slide formatting, so that toolbar is really rendered by the browser
            self.executeDelayed(function () {
                if (!app.isReloading()) {
                    self.trigger('slideModel:init:thumbnail'); // starting creation of thumbnails in slide pane
                } else {
                    activateThumbNails(); // coming back later
                }
            }, 200);
        }

        if (app.showMainToolPaneAtStart()) {
            app.docView.toolPaneManager.one('tab:activated', activateThumbNails);
        } else {
            self.waitForImportSuccess(activateThumbNails); // alternative event, triggered earlier
        }
    }

    /**
     * A helper function that collects all functionality that needs to be done, after the
     * active slide (if it exists) is set.
     */
    function postActiveSlideSettings() {

        // making the (empty) slide pane visible, so that the slide is positioned correctly
        app.getView().getSlidePane().setSidePaneWidthDuringLoad(self.getSlidePaneWidthInDocAttributes());

        // checking the visibility of the page (not visible for empty views)
        self.checkPageVisibility();

        // making the slide (immediately) visible
        const zoomType = app.getLaunchOption('zoomType') || 'slide';
        const zoomFactor = app.getLaunchOption('zoomFactor');
        return app.leaveBusyDuringImport({ zoomType, zoomFactor }).always(leaveBusyCleanupHandler);
    }

    /**
     * A helper function for the functionality that must be called after leaveBusy is no longer active
     * during the import.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.leaveAfterStartInPresentationMode=false]
     *    If set to true, the optional presentationModeOptions are ignored and the presentation mode
     *    is not started. This is only true, when the application was started in presentation mode and
     *    this is now left.
     */
    function leaveBusyCleanupHandler(options) {

        // whether this function is triggered after leaving presentation mode when it was started in presentation mode
        const startedInPresentationMode = options?.leaveAfterStartInPresentationMode;

        // evaluating the launch options -> starting app in presentation mode immediately
        if (presentationModeOptions && !startedInPresentationMode) {
            self.getPresentationModeManager().startPresentation(presentationModeOptions);
            return; // fast exit for the presentation mode
        }

        // Setting contenteditable 'false' to pagecontent node in IE (46752)
        handleReadOnly(getPageContentNode(pagediv));

        // writing some load logging information into the console (only in debug mode)
        self.dumpProfilingInformation();

        if (!startedInPresentationMode) {
            // registering the handler for the activation of the first tab (then creating of thumbnails
            // in slide pane can start deferred)
            registerFirstTabActivatedHandler();
        } else {
            // starting creation of thumbnails in slide pane directly when the presentation has finished
            // AND the document was started in presentation mode (DOCS-4700)
            self.trigger('slideModel:init:thumbnail');
        }

        // trigger event to initialize the (empty) SlidePane, before the controller updates the GUI (looks better than vice versa)
        // -> triggering synchronously so that empty slide pane is immediately visible together with first slide
        // -> the generation of thumbnails needs its own event ('slideModel:init:thumbnail'), because otherwise the
        //    visibility of the first slide is deferred dramatically. An alternative would be, to trigger this event
        //    with a delay, but then slide thumbnail generation competes with toolbar generation.
        self.trigger('slideModel:init', { isMasterView: self.isMasterView(), activeSlideId: self.getActiveSlideId(), width: self.getSlidePaneWidth(), slideRatio: self.getSlideRatio() });
    }

    // public methods -----------------------------------------------------

    /**
     * Updates the formatting of all elements, after the import operations
     * of the document have been applied.
     * This function handles also the case, if the document was loaded from
     * local storage.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the formatting has been
     *  updated successfully, or rejected when an error has occurred.
     */
    this.updateDocumentFormatting = function () {

        var // an object containing all slide IDs and slides
            allSlides = null,
            // the id of the active slide (TODO: This is not always the first slide in standard view)
            // -> this can also be undefined, if there is not slide in the view
            activeSlide = '',
            // the format manager
            slideFormatManager = self.getSlideFormatManager(),
            // the object containing the page attributes
            pageAttributes = null,
            // whether the document is loaded from local storage
            isLocalStorageImport = self.isLocalStorageImport();

        // checking if presentation mode shall be started -> evaluating launch options and URL parameters
        collectPresentationModeOptions();

        // setting globals
        pagediv = self.getNode();
        allSlides = self.getAllSlides();
        activeSlide = getIdOfFirstSlideToFormat();
        pageAttributes = this.pageStyles.getElementAttributes(pagediv);

        // setting tasks at the slide format manager
        slideFormatManager.initializeManager(self.getIdTypeConnectionModel(), self.getStandardSlideOrder(true), self.getMasterSlideOrder(true), { localStorage: isLocalStorageImport });

        // hiding all slides
        _.invoke(allSlides, 'addClass', 'invisibleslide');

        // localStorage: Removing superfluous classes at slides that could not be removed when saving file to local storage
        if (isLocalStorageImport) { _.invoke(allSlides, 'removeClass', 'notselectable'); }

        // resetting an existing selection, after all operations are applied
        self.getSelection().resetSelection();

        // saving page attributes at model for convenience reasons
        self.setPageAttributes(pageAttributes);

        // update the root page node
        this.pageStyles.updateElementFormatting(pagediv);

        // Performance: Removing the marker class set from fast load
        pagediv.removeClass(HIDDENAFTERATTACH_CLASSNAME);

        // in localStorage the slides need to be formatted, because of Canvas and because they might have never been
        // formatted before (example: slide_100 is often not formatted before the document is closed and written into
        // local storage -> support of class 'slideformatted' in slideFormatManager).

        if (activeSlide) {
            return slideFormatManager.forceSlideFormatting(activeSlide, { isReloading: app.isReloading() }).done(function () {
                // activating the first slide after formatting it and its parents completely
                self.setActiveSlideId(activeSlide, { forceUpdate: true });
            }).then(postActiveSlideSettings);
        } else {
            postActiveSlideSettings();
            return $.when();
        }
    };

    /**
     * Handler for loading empty default documents with fast load. It applies html string,
     * then actions, starts formatting the document, and at the end, leaves busy mode.
     *
     * @param {String} markup
     *  The HTML mark-up to be shown as initial document contents.
     *
     * @param {Operation[]} operations
     *  The document operations to be applied to finalize the fast import.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the actions have been applied.
     */
    this.fastEmptyLoadHandler = function (markup, operations) {
        // markup is in form of object, parse and get data from mainDocument
        markup = JSON.parse(markup);
        if (markup.fv) { self.setFastLoadFilterVersion(markup.fv); }
        self.setFullModelNode(markup.mainDocument);

        self.trigger('fastemptyload:done'); // updating the model

        return self.applyExternalOperations(operations, { expand: true }).then(function () {
            return self.updateDocumentFormatting();
        });
    };

    /**
     * A helper function to complete the loading process after the end of the presentation mode.
     * This is required, because the installation is not completed, if the presentation application
     * directly startet in presentation mode. Therefore the final steps must be done, when the
     * presentation mode is left.
     */
    this.leavePresentationModeHelper = function () {
        leaveBusyCleanupHandler({ leaveAfterStartInPresentationMode: true });
    };
}
