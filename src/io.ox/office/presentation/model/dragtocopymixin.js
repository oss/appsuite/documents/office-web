/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';

import { matchKeyCode } from '@/io.ox/office/tk/dom';

// mixin DragToCopyMixin ==================================================

function DragToCopyMixin(app) {

    const self = this;
    const editDiv = self.getNode();
    const winBodyNode = app.getWindowNode();
    const isMacOS = _.browser.MacOS;
    const keyString = isMacOS ? 'ALT' : 'CONTROL';
    const keyOptionStart = isMacOS ? { alt: true } : { ctrl: true };
    const keyOptionEnd = isMacOS ? { alt: false } : { ctrl: false };

    // local / private methods --------------------------------------------

    function processKeyDown(evt) {
        if (matchKeyCode(evt, keyString, keyOptionStart)) {
            if (self.isInsertTextFrameActive() || self.isInsertShapeActive()) {
                return;
            }
            toggleState(true);
        }
    }

    function processKeyUp(evt) {
        if (matchKeyCode(evt, keyString, keyOptionEnd)) {
            toggleState(false);
        }
    }

    function toggleState(state) {
        editDiv.toggleClass('drag-to-copy', state);
        if (_.browser.Chrome) {
            // https://bugs.chromium.org/p/chromium/issues/detail?id=26723
            editDiv[0].scrollLeft += 1;
            editDiv[0].scrollLeft -= 1;
        }
    }

    // initialization -----------------------------------------------------

    self.waitForImportSuccess(function () {
        self.listenTo(app.docView, 'view:hide', function () { toggleState(false); });
        winBodyNode.on('keydown', processKeyDown);
        winBodyNode.on('keyup', processKeyUp);
    });
}

// exports ================================================================

export default DragToCopyMixin;
