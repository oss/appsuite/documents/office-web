/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getBooleanOption } from '@/io.ox/office/tk/utils';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { CANVAS_NODE_SELECTOR, isAutoResizeHeightGroup } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { HIDDENAFTERATTACH_CLASSNAME, PARAGRAPH_NODE_SELECTOR } from '@/io.ox/office/textframework/utils/dom';
import { isBodyPlaceHolderWithoutIndexDrawing, isCustomPromptPlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';

// constants ==============================================================

// the idle time (in ms), after that a slide formatting is triggered
const IDLE_TIME = 2500;

// class SlideFormatManager ===============================================

/**
 * An instance of this class represents the slide formatting manager. This manager
 * keeps track of unformatted slides. They are formatted on demand for performance
 * reasons.
 *
 * @param {PresentationApplication} app
 *  The application instance.
 */
export default class SlideFormatManager extends ModelObject {

    // a collector for all unformatted slides
    // -> this is an object, whose keys are the IDs of the unformatted slides
    #allUnformattedSlides;
    // a collector for all unformatted slides loaded from local storage
    #localStorageSlides;
    // a collector for all slides that are currently formatted
    // -> avoiding that slides, whose formatting takes a long time, are formatted more than once
    #runningTasks = {};
    // the order of the document slides
    #docOrder;
    // the order of the master and layout slides
    #layoutOrder;
    // a function that checks for idle time to update slides in background
    #idleTimeChecker = $.noop;
    // a collector for all those slides, that need to be shifted during formatting of the active slide
    #shiftedSlides;
    // a collector for slide IDs, that are waiting to inform the listeners, because dependent slides are currently formatted
    #waitingIDs;
    // whether ther first tab is already activated after loading the document (performance)
    #firstTabActivated = false;
    // a queue for slide IDs that are externally demanded during the loading phase (performance)
    #loadingQueue = [];

    constructor(docModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        // destroy all class members on destruction
        this.registerDestructor(() => {
            this.#deregisterIdleTimeChecker();
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Whether there is at least one unformatted slide.
     *
     * @returns {Boolean}
     *  Whether there is at least one unformatted slide.
     */
    unformattedSlideExists() {
        return this.#allUnformattedSlides && !_.isEmpty(this.#allUnformattedSlides);
    }

    /**
     * Whether all slides are formatted.
     *
     * @returns {Boolean}
     *  Whether all slides are formatted.
     */
    allSlidesFormatted() {
        return !this.unformattedSlideExists();
    }

    /**
     * Whehter at least one slide is currently formatted.
     *
     * @returns {Boolean}
     *  Whehter at least one slide is currently formatted.
     */
    runningTasksExist() {
        return !_.isEmpty(this.#runningTasks);
    }

    /**
     * Whether the slide specified by its ID is an unformatted slide. It is
     * possible, that the slide is currently formatted.
     *
     * @param {String} id
     *  The id of a slide.
     *
     * @returns {Boolean}
     *  Whether the slide specified by its ID is an unformatted slide.
     */
    isUnformattedSlide(id) {
        return this.#allUnformattedSlides && !_.isUndefined(this.#allUnformattedSlides[id]);
    }

    /**
     * Whether the slide specified ty its ID is already formatted.
     *
     * @param {String} id
     *  The id of a slide.
     *
     * @returns {Boolean}
     *  Whether the slide specified by its ID is formatted.
     */
    isFormattedSlide(id) {
        return !this.isUnformattedSlide(id);
    }

    /**
     * Checks whether a specified slide is currently formatted.
     *
     * @param {String} id
     *  The ID of the slide, that needs to be formatted.
     *
     * @returns {Boolean}
     *  Whether the slide with the specified id is currently formatted.
     */
    isRunningTask(id) {
        return this.#runningTasks[id] === 1;
    }

    /**
     * Whether the specified slide set is already completely formatted.
     *
     * @param {String[]} set
     *  The set of slide IDs.
     *
     * @returns {Boolean}
     *  Whether all the slides specified by its IDs are formatted.
     */
    isSlideSetFormatted(set) {
        return _.every(set, id => {
            return this.isFormattedSlide(id);
        });
    }

    /**
     * Whether the specified slide set is already completely formatted
     * or the slides are currently formatted. In this case no new formatting
     * process must be triggered.
     *
     * @param {String[]} set
     *  The set of slide IDs.
     *
     * @returns {Boolean}
     *  Whether all the slides specified by its IDs are already formatted
     *  or are currently formatted.
     */
    isSlideSetCurrentlyFormatted(set) {
        return _.every(set, id => {
            return this.isFormattedSlide(id) || this.isRunningTask(id);
        });
    }

    /**
     * Adding additional IDs to the container of unformatted slide IDs.
     *
     * @param {String|String[]} IDs
     *  A single string or an array of strings with the IDs of slides, that need
     *  to be formatted later.
     */
    addTasks(IDs) {

        if (_.isString(IDs)) {
            if (this.docModel.isValidSlideId(IDs)) {
                if (!this.#allUnformattedSlides) { this.#allUnformattedSlides = {}; }
                this.#allUnformattedSlides[IDs] = 1;
            }
        } else if (_.isArray(IDs)) {
            if (!this.#allUnformattedSlides) { this.#allUnformattedSlides = {}; }
            _.each(IDs, id => {
                this.#allUnformattedSlides[id] = 1;
            });
        }
    }

    /**
     * Shifting a specified slide set far to the left, so that it cannot be seen by the user. This is necessary
     * during formatting of the slide. In this case the complete set of slide IDs need to be shifted, even though
     * some slides of it might already be completely formatted.
     *
     * @param {String[]} idSet
     *  The set of slide IDs that will be shifted to the left, so that the user cannot see it.
     */
    shiftSlideSetOutOfView(idSet) {

        if (this.#shiftedSlides) { return; } // only one set of shifted slides is supported

        _.each(idSet, id => {
            const oneSlide = this.docModel.getSlideById(id);
            if (oneSlide) {
                oneSlide.addClass('leftshift');  // using negative left offset, so that the scroll bar is not influenced
                this.#shiftedSlides = this.#shiftedSlides || [];
                this.#shiftedSlides.push(id); // avoing 'null' values in shiftSlides container
            }
        });
    }

    /**
     * Shifting back the slides that were shifted using 'shiftSlideSetOutOfView'. This must be done, if the active
     * slide does not need this shifting, because all slides are formatted.
     */
    shiftSlideSetIntoView() {

        _.each(this.#shiftedSlides, id => {
            let oneSlide = null;
            if (!this.isRunningTask(id)) { // not shifting into view, if the slide is currently formatted
                oneSlide = this.docModel.getSlideById(id);
                if (oneSlide) { oneSlide.removeClass('leftshift'); } // using negative left offset, so that the scroll bar is not influenced
            }
        });

        this.#shiftedSlides = null;
    }

    /**
     * Whether there are some slides shifted due to formatting reasons. During a long running formatting process
     * all three slide nodes are shifted far to the left, so that the user cannot see half formatted slides. Even
     * if master or layout slide are completely formatted, it makes no sense to show them until the document
     * slide is completely formatted. Therefore the array 'this.#shiftedSlides' contains the complete collection
     * of a slide ID set.
     *
     * @returns {Boolean}
     *  Whether there are some slides shifted due to formatting reasons.
     */
    shiftedSlideExist() {
        return this.#shiftedSlides !== null;
    }

    /**
     * Whether a slide with a specified ID is shifted due to formatting reasons. This slide can already be
     * completely formatted, but it is shifted, because another slide of the slide ID set of the active slide
     * is not completely formatted.
     *
     * @param {String} id
     *  The slide id.
     *
     * @returns {Boolean}
     *  Whether the specified slide is shifted due to formatting reasons.
     */
    isShiftedSlide(id) {
        return _.contains(this.#shiftedSlides, id);
    }

    /**
     * Forcing an update of one specified slide and its layout and master slide (if required).
     * This process is especially required for formatting the first visible slide.
     *
     * Important: The caller of this function has to check, if the slide with the specified
     *            id is not currently formatted. Otherwise it might happen, that the function
     *            'changeSlideHandler' returns a resolved promise, even though the slide is
     *            currently formatted and this formatting has not finished yet.
     *            Check for the caller: if (!slideFormatManager.isRunningTask(id)) { ...
     *
     * @param {String|String[]} id
     *  The ID of the slide, that needs to be formatted.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.isReloading=false]
     *      Whether this handler is called in reload process.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the specified slide and all its parent slides
     *  are formatted.
     */
    forceSlideFormatting(id, options) {
        // the set of affected slides
        const set = _.isString(id) ? this.docModel.getSlideIdSet(id) : id;
        return this.#changeSlideHandler(null, { newSlideId: set.id, newLayoutId: set.layoutId, newMasterId: set.masterId, isReloading: getBooleanOption(options, 'isReloading', false) });
    }

    /**
     * Setting the initial tasks for the slide formatting manager. This is typically done with
     * the object 'idTypeConnection' from the model.
     *
     * @param {Object} idObject
     *  An object, that contains the IDs of the slides, that need to be formatted as keys.
     *
     * @param {Array} docSlideOrder
     *  Copy of the container with the document slide IDs in the correct order.
     *
     * @param {Array} layoutSlideOrder
     *  Copy of the container with the slide IDs of master and layout slides in the correct order.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.LocalStorage=false]
     *      Whether the document was loaded from local storage.
     */
    initializeManager(idObject, docSlideOrder, layoutSlideOrder, options) {

        this.#allUnformattedSlides = idObject;
        this.#docOrder = docSlideOrder;
        this.#layoutOrder = layoutSlideOrder;

        // slides loaded from local storage can be formatted faster
        if (getBooleanOption(options, 'localStorage', false)) { this.#localStorageSlides = _.copy(this.#allUnformattedSlides, true); }

        // registering a listener for the event 'request:formatting', that can be triggered
        // to force the slide format manager to format some slides specified by their IDs.
        this.listenTo(this.docModel, 'request:formatting', this.#externalFormattingHandler);

        // registering a listener for the activation of the first tab
        this.#registerFirstTabActivatedHandler();

        // starting the process to format slides, if there is no user input for a specified time
        this.#registerIdleTimeChecker();
        this.#idleTimeChecker(); // starting process

        //this.docModel.trigger('formatmanager:init');
    }

    // private methods ----------------------------------------------------

    /**
     * Removing a task from the collector for all unformatted slides.
     *
     * @param {String} id
     *  The ID of the slide that is no longer unformatted.
     */
    #removeTask(id) {

        // removing slide from the collector of unformatted slides
        if (this.#allUnformattedSlides && this.#allUnformattedSlides[id]) {
            delete this.#allUnformattedSlides[id];
            if (this.#localStorageSlides) { delete this.#localStorageSlides[id]; }
        } else {
            if (this.#allUnformattedSlides) {
                globalLogger.warn('SlideFormatManager: Trying to remove non existing task: ' + id);
            } else {
                globalLogger.warn('SlideFormatManager: Trying to remove task: ' + id + ' from non existent container of unformatted slides.');
            }
        }

        if (this.docModel.isStandardSlideId(id)) {
            this.#docOrder = _.without(this.#docOrder, id);
        } else {
            this.#layoutOrder = _.without(this.#layoutOrder, id);
        }

        if (_.isEmpty(this.#allUnformattedSlides)) {
            this.#allUnformattedSlides = null;
            this.#docOrder = null;
            this.#layoutOrder = null;
            this.#localStorageSlides = null;
        }
    }

    /**
     * Updating the document slides belonging to a specified layout slide in that way, that
     * the correct customized template texts are shown.
     *
     * @param {String} id
     *  The ID of a (layout) slide.
     */
    #updateAllCustomizedTemplateTextOnSlide(id) {
        _.each(this.docModel.getAllSlideIDsWithSpecificTargetParent(id), slideId => {
            this.docModel.drawingStyles.handleAllEmptyPlaceHolderDrawingsOnSlide(this.docModel.getSlideById(slideId), { ignoreTemplateText: true });
        });
    }

    /**
     * Element formatting for one slide and its content.
     *
     * @param {jQuery} slide
     *  The jQuerified slide node.
     *
     * @param {String} id
     *  The ID of the slide that will be formatted.
     *
     * @returns {jQuery.Promise}
     *  The promise of a deferred object that will be resolved after the specified slide is formatted
     *  completely.
     */
    #formatOneSlide(slide, id) {

        if (this.docApp.isInQuit()) {
            return $.Deferred().reject();
        }

        // whether the visibility of the slide has changed for formatting reasons
        let visibilityChanged = false;
        // the paragraphs in the slide
        let paragraphContainer = $();
        // collector for all drawings on the slide
        let allDrawings = slide.find('.drawing');
        // whether the document slides need to be updated because of a customized template text
        let updateCustomizedTemplateTexts = false;
        // a helper index for body place holder drawings without index (after loading)
        let odfBodyIndex = 0;
        // whether the slide was loaded from local storage and can be formatted faster (and it has been formatted before)
        const useLessFormatting = this.#localStorageSlides && this.#localStorageSlides[id] && slide.is('.slideformatted') && !this.docModel.externalLocalStorageOperationsExist();
        // a jQuery promise object
        let promise = null;

        // updating one drawing on the slide
        const updateOneDrawing = oneDrawing => {

            // the paragraphs in one drawing
            // -> reading the paragraphs after drawingStyles.updateElementFormatting because of implicit paragraphs in ODP
            let paragraphs = null;
            // the attribute object of family 'presentation' of the drawing
            let presentationAttrs = null;
            // the jQueryfied drawing node
            const drawing = $(oneDrawing);
            // whether this is an image drawing
            const isImage = drawing.attr('data-type') === 'image';
            // whether this is a table drawing
            const isTable = drawing.attr('data-type') === 'table';

            if (!this.docModel || this.docModel.destroyed) { return; } // the model might have been destroyed in the meantime

            // Performance: When loaded from local storage or fast load the 'src' property has to be renamed
            if (isImage) { this.docModel.prepareImageNodeFromStorage(drawing); }

            // Preformance: When loaded from local storage, only drawings with Canvas need to be restored
            if (useLessFormatting && drawing.find(CANVAS_NODE_SELECTOR).length === 0) { return; }

            if (this.docApp.isODF() && isBodyPlaceHolderWithoutIndexDrawing(drawing)) { // repairing the missing index
                presentationAttrs = getExplicitAttributes(drawing, 'presentation', true);
                presentationAttrs.phIndex = odfBodyIndex++; // modifying the original presentation attribute
            }

            // Performance: Grouped images and grouped tables need to be formatted once completely
            if (isImage || isTable) { drawing.addClass('formatrequired'); }

            this.docModel.drawingStyles.updateElementFormatting(drawing);

            drawing.removeClass('slideformat'); // removing marker after updateElementFormatting, it is no longer needed

            paragraphs = drawing.find(PARAGRAPH_NODE_SELECTOR); // but not in tables

            // also updating all paragraphs inside text frames (not searching twice for the drawings)
            // The synchronous call is required for example for DOCS-1673.
            // -> using option 'suppressFontSize', so that the dynamic font size calculation is NOT
            //    started during formatting of a complete slide.
            this.docModel.implParagraphChangedSync(paragraphs, { suppressFontSize: true });

            paragraphContainer = paragraphContainer.add(paragraphs);

            // marking collecting all drawings with customized template text
            if (this.docModel.isLayoutSlideId(id) && isCustomPromptPlaceHolderDrawing(drawing)) { updateCustomizedTemplateTexts = true; }
        };

        // Performance: Changing order of drawings, so that the group drawings are formatted after
        //              all child drawings are added to the group.
        const handleGroupNodes = () => {

            // collecting all drawings of type 'group'
            const splittedValues = _.partition(allDrawings, drawing => { return $(drawing).attr('data-type') === 'group'; });
            // a container for all drawings of type 'group' on the slide
            const groupDrawings = splittedValues[0];

            // partition generates two separated arrays
            allDrawings = splittedValues[1];  // not group drawings

            if (groupDrawings.length > 0) {
                // adding the group drawings to the end of the collection
                groupDrawings.reverse(); // formatting inner groups before outer groups
                allDrawings = allDrawings.concat(groupDrawings);
                // a second formatting run is required for group drawings with at least one autoresizeheight child drawing
                const autoResizeGroups = _.filter(groupDrawings, oneGroupDrawing => isAutoResizeHeightGroup(oneGroupDrawing));
                if (autoResizeGroups.length > 0) { allDrawings = allDrawings.concat(autoResizeGroups); }
            }
        };

        // attaching the slide node to the DOM, if required
        this.docModel.appendSlideToDom(id);

        // Performance: Removing marker class from slides that they received after fast load handling
        slide.removeClass(HIDDENAFTERATTACH_CLASSNAME);

        // making slide visible for formatting
        visibilityChanged = this.docModel.handleContainerVisibility(slide, { makeVisible: true });

        slide.addClass('leftshift');

        // setting a marker to group nodes, so that this process is recognized as slide format manager formatting
        allDrawings.addClass('slideformat');

        // appending drawings of type 'group' to the end of the drawing list, so that it is not necessary
        // to update the complete group after each client
        handleGroupNodes();

        // Important: Using a local promise, not an external created by 'updateElementFormatting'
        promise = this.createResolvedPromise();

        // updating the slide ...
        promise = promise.then(() => { return this.docModel.slideStyles.updateElementFormatting(slide); });

        // updating all drawings via iteration (in case of success, but also in error case (58241))
        promise = promise.then(() => {
            return this.iterateArraySliced(allDrawings, updateOneDrawing);
        }, () => {
            return this.iterateArraySliced(allDrawings, updateOneDrawing);
        });

        promise.done(() => {
            // updating the lists also synchronously
            this.docModel.updateLists(paragraphContainer);
            // updating the customized template texts on document slides
            if (updateCustomizedTemplateTexts) { this.#updateAllCustomizedTemplateTextOnSlide(id); }
        });

        promise.always(() => {

            // shifting back the slide after formatting
            slide.css('left', '');
            slide.removeClass('leftshift');

            if (visibilityChanged) {
                // making the slide invisible again
                if (!this.docModel || this.docModel.destroyed) { return; } // the model might have been destroyed in the meantime
                this.docModel.handleContainerVisibility(slide, { makeVisible: false });
            }

            // let the new formatted slide stay in the DOM for some seconds
            this.docModel.getSlideVisibilityManager().registerOperationSlideId(id);

            // this task is no longer running
            if (this.#runningTasks[id]) { delete this.#runningTasks[id]; }

            // remove slide from local storage collector in any case
            if (this.#localStorageSlides) { delete this.#localStorageSlides[id]; }
        });

        promise.done(() => {
            this.#removeTask(id); // updating the collector of unformatted slides
            // Setting marker class 'slideformatted' for following loading with local storage.
            // This marker is saved in local storage and specifies, that the drawings on this
            // slide do not need to be formatted (except those drawings that contain a Canvas
            // node).
            slide.addClass('slideformatted');
        });

        promise.fail(() => {
            globalLogger.error('Error during slide formatting! ID: ' + id);
            this.#removeTask(id); // updating the collector of unformatted slides -> avoiding endless formatting (58241)
        });

        return promise;
    }

    /**
     * Formatting a full set of slides. The caller of this function must make sure,
     * that after all specified slides (in container slideIDs) are formatted, the listener
     * can be informed about a fully formatted slide (all required layout and master
     * slides are also formatted).
     *
     * Info: All slide IDs that are specified in the container 'slideIDs' must exist
     *       in the global collector 'allUnformattedSlides'.
     *
     * @param {String[]} slideIDs
     *  The IDs of the slide(s) that need to be updated. It is necessary, that this
     *  is always
     *
     * @returns {jQuery.Promise}
     *  The promise of a deferred object that will be resolved after all specified slides
     *  are formatted completely.
     */
    #formatSlides(slideIDs) {

        // -> check for running tasks, so that a slide is not formatted again
        const newSlideIDs = _.reject(slideIDs, id => {
            return this.#runningTasks[id];
        });

        // blocking these IDs for further formatting
        _.each(newSlideIDs, id => {
            this.#runningTasks[id] = 1;
        });

        // -> iterating asynchronously over the collected slide IDs (formatting slide(s) asynchronously)
        return this.iterateArraySliced(newSlideIDs, id => {
            return this.#formatOneSlide(this.docModel.getSlideById(id), id);
        });
    }

    /**
     * Registering the handler to find idle time for formatting slides.
     *
     * @returns {undefined}
     */
    #registerIdleTimeChecker() {
        this.#idleTimeChecker = this.debounce(this.#idleSlideFormatHandler, { delay: IDLE_TIME });
        this.docModel.getNode().on('keydown mousedown touchstart', this.#idleTimeChecker);
    }

    /**
     * Deregistering the handler to find idle time for formatting slides. This
     * can be done, if there are no more open tasks.
     */
    #deregisterIdleTimeChecker() {
        this.docModel.getNode().off('keydown mousedown touchstart', this.#idleTimeChecker);
        this.#idleTimeChecker = _.noop;
    }

    /**
     * Determining the ID of the slide that needs to be formatted next. This is done
     * with an idle time, so that it is possible to format any of the remaining
     * unformatted slides.
     *
     * @returns {String}
     *  The ID of the slide that shall be formatted.
     */
    #getNextFormatSlideId() {

        // whether the layout/master view is visible
        const isMasterView = this.docModel.isMasterView();
        // the id of the slide, that shall be formatted
        let slideID = isMasterView ? _.first(this.#layoutOrder) : _.first(this.#docOrder);

        // Fallback: simply take one arbitrary slide ID
        if (!slideID || !this.#allUnformattedSlides[slideID]) { slideID = _.first(_.keys(this.#allUnformattedSlides)); }

        return slideID;
    }

    /**
     * Handler to make slide formatting, if there was no user input for a specified time.
     */
    #idleSlideFormatHandler() {

        // the ID of the slide that will be formatted
        let slideID = null;
        // the set of affected slides
        let set = null;
        // a collector for the unformatted slides
        let formatCollector = null;

        if (!this.docModel || this.docModel.destroyed) { return; } // handling closing of application

        if (!this.#firstTabActivated) { // handling loading of document
            this.#idleTimeChecker();
            return;
        }

        if (this.unformattedSlideExists()) {

            // Trigger formatting one slide
            slideID = this.#getNextFormatSlideId();

            // -> this must be a full set of slides
            set = this.docModel.getSlideIdSet(slideID);

            if (set.id && this.#allUnformattedSlides[set.id] && !this.#runningTasks[set.id]) {
                formatCollector = [set.id];
            }

            if (set.layoutId && this.#allUnformattedSlides[set.layoutId] && !this.#runningTasks[set.layoutId]) {
                if (formatCollector) {
                    formatCollector.push(set.layoutId);
                } else {
                    formatCollector = [set.layoutId];
                }
            }

            if (set.masterId && this.#allUnformattedSlides[set.masterId] && !this.#runningTasks[set.masterId]) {
                if (formatCollector) {
                    formatCollector.push(set.masterId);
                } else {
                    formatCollector = [set.masterId];
                }
            }

            if (formatCollector) {
                // formatting the collected slides
                this.#formatSlides(formatCollector).done(() => {
                    // informing the listeners
                    this.#informListeners(formatCollector);
                    // check for waiting listeners
                    this.#checkWaitingListeners(formatCollector); // checking, if waiting listeners can be informed
                    // triggering next slide update
                    if (this.unformattedSlideExists()) {
                        this.#idleTimeChecker();
                    } else {
                        this.#deregisterIdleTimeChecker();
                    }
                });
            } else {
                // triggering next slide update
                if (this.unformattedSlideExists()) {
                    this.#idleTimeChecker();
                } else {
                    this.#deregisterIdleTimeChecker();
                }
            }

        } else {
            this.#deregisterIdleTimeChecker();
        }
    }

    /**
     * Listener for the event 'change:slide', that is triggered from the model,
     * if the active slide was modified.
     *
     * @param {jQuery.Event} event
     *  A jQuery event object.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.newSlideId='']
     *      The id of the standard slide that will be activated.
     *  @param {String} [options.newLayoutId='']
     *      The id of the layout slide that will be activated.
     *  @param {String} [options.newMasterId='']
     *      The id of the master slide that will be activated.
     *  @param {String} [options.isReloading=false]
     *      Whether this handler is called in reload process.
     *
     * @returns {jQuery.Promise}
     *  The promise of a deferred object that will be resolved after all specified
     *  slides are formatted completely.
     *  Info: This promise should not be used, except in the case of the function
     *        'forceSlideFormatting'. This is required for the first visible slide
     *        that needs to block the document load process. Typically all affected
     *        processes must be informed via the event 'slidestate:formatted'.
     */
    #changeSlideHandler(event, options) {

        // a collector for the unformatted slides
        let formatCollector = null;

        if (!this.unformattedSlideExists()) { return $.when(); }

        if (options.newSlideId && this.#allUnformattedSlides[options.newSlideId] && !this.#runningTasks[options.newSlideId]) {
            formatCollector = [options.newSlideId];
        }

        if (options.newLayoutId && this.#allUnformattedSlides[options.newLayoutId] && !this.#runningTasks[options.newLayoutId]) {
            if (formatCollector) {
                formatCollector.push(options.newLayoutId);
            } else {
                formatCollector = [options.newLayoutId];
            }
        }

        if (options.newMasterId && this.#allUnformattedSlides[options.newMasterId] && !this.#runningTasks[options.newMasterId]) {
            if (formatCollector) {
                formatCollector.push(options.newMasterId);
            } else {
                formatCollector = [options.newMasterId];
            }
        }

        if (formatCollector) {

            // formatting the collected slides
            return this.#formatSlides(formatCollector).done(() => {
                // informing the listeners
                this.#informListeners(formatCollector);
                // check for waiting listeners
                this.#checkWaitingListeners(formatCollector); // checking, if waiting listeners can be informed
            });
        } else {
            // ignoring that slides might be currently formatted (this has to be handled by the caller)
            return $.when();
        }
    }

    /**
     * Listener for the event 'request:formatting' that can be triggered by an
     * external source to request a formatting update of the specified slides.
     * external trigger of slide formatting
     *
     * The formatting of the slides is done asynchronously.
     *
     * This function also handles that only 'complete' formatting is done. If
     * a document slide is in the specified list 'allSlides', the corresponding
     * layout and master slide are also formatted, if this is necessary.
     *
     * Listeners are informed with the event 'slidestate:formatted' from the
     * model, that the specified slides are formatted.
     *
     * @param {String|String[]|Object} allSlides
     *  The IDs of the slide(s) that need to be updated. This can be a single string,
     *  an array of strings or an object, whose values are interpreted as string IDs.
     *  Such an object is created by the function 'getSlideIdSet' from the model.
     */
    #externalFormattingHandler(allSlides) {

        // the container for the slide IDs
        let container = null;

        if (_.isString(allSlides)) {
            container = [allSlides];
        } else if (_.isArray(allSlides)) {
            container = allSlides;
        } else if (_.isObject(allSlides)) {
            container = _.values(allSlides);
        }

        // shortcut, if there are no unformatted slides
        if (!this.unformattedSlideExists()) {
            // informing the listeners that all slides are formatted
            this.#informListeners(container);
            return;
        }

        if (!this.#firstTabActivated) {
            _.each(container, id => { this.#loadingQueue.push(id); });
            return; // do nothing, until first tab is activated
        }

        _.each(container, id => {

            // the set of affected slides
            const set = this.docModel.getSlideIdSet(id);
            // a collector for the unformatted slides
            let formatCollector = null;
            // whether the triggering needs to be debounced
            let waitingRequired = false;
            // a collector for all IDs of those slides, that are currently formatted
            let waitForIDs = null;

            if (set.id && this.#allUnformattedSlides[set.id]) {
                if (this.#runningTasks[set.id]) {
                    waitForIDs = {};
                    waitForIDs[set.id] = 1;
                } else {
                    formatCollector = [set.id];
                }
            }

            if (set.layoutId && this.#allUnformattedSlides[set.layoutId]) {
                if (this.#runningTasks[set.layoutId]) {
                    waitForIDs = waitForIDs || {};
                    waitForIDs[set.layoutId] = 1;
                } else {
                    if (formatCollector) { // the document slide is already in the format collector
                        waitForIDs = waitForIDs || {};
                        waitForIDs[set.layoutId] = 1; // the document slide needs to wait for finished formatting of layout slide
                    }

                    if (formatCollector) {
                        formatCollector.push(set.layoutId);
                    } else {
                        formatCollector = [set.layoutId];
                    }
                }
            }

            if (set.masterId && this.#allUnformattedSlides[set.masterId]) {
                if (this.#runningTasks[set.masterId]) {
                    waitForIDs = waitForIDs || {};
                    waitForIDs[set.masterId] = 1;
                } else {
                    if (formatCollector) { // the document and/or layout slides are already in the format collector
                        waitForIDs = waitForIDs || {};
                        waitForIDs[set.masterId] = 1; // the document and/or layout slide needs to wait for finished formatting of master slide
                    }

                    if (formatCollector) {
                        formatCollector.push(set.masterId);
                    } else {
                        formatCollector = [set.masterId];
                    }
                }
            }

            // Is it necessary to inform the listener, even if all slides are already formatted -> yes

            if (formatCollector) {
                if (waitForIDs) { // some slides are currently formatted
                    // adding the IDs of the format collector to the 'waitForIDs' collector
                    _.each(formatCollector, oneID => { waitForIDs[oneID] = 1; });
                    this.#informListenersLater(waitForIDs, id); // the listeners need to be informed later
                    waitingRequired = true;
                }

                // formatting the collected slides
                this.#formatSlides(formatCollector).done(() => {
                    this.#checkWaitingListeners(formatCollector); // checking, if waiting listeners can be informed
                    if (!waitingRequired) {
                        this.#informListeners([id]); // informing the listeners, that the requested slide can be used
                    }
                });
            } else {
                if (waitForIDs) {
                    this.#informListenersLater(waitForIDs, id); // the listeners need to be informed later
                } else {
                    this.#informListeners([id]); // informing the listeners, that the requested slide can be used
                }
            }
        });

    }

    /**
     * Registering a slide ID for deferred information of listeners. The listeners can only be
     * informed, if all dependent slides (saved in waitObject) are also formatted.
     *
     * @param {Object} waitObject
     *  An object containing as keys all those slide IDs of those slides, that need to be formatted,
     *  before the listeners can be informed, that the slide with the specified ID is formatted
     *  completely.
     *
     * @param {String} id
     *  The ID of that slide, that can only be used by listeners, if all dependent slides (whose
     *  ID is saved in waitObject) are also formatted.
     */
    #informListenersLater(waitObject, id) {
        this.#waitingIDs = this.#waitingIDs || {};
        this.#waitingIDs[id] = waitObject;
    }

    /**
     * Checking, if listeners can be informed, that a specified slide can be used.
     *
     * @param {String[]} formattedIDs
     *  An array containing the IDs of all new formatted slides.
     */
    #checkWaitingListeners(formattedIDs) {
        if (this.#waitingIDs) {
            _.each(this.#waitingIDs, (waitIDs, id) => {

                _.each(formattedIDs, formattedID => {
                    delete waitIDs[formattedID];
                });

                if (_.isEmpty(waitIDs)) {
                    delete this.#waitingIDs[id];
                    this.#informListeners([id]); // finally the waiting listeners can be informed
                }
            });
            if (_.isEmpty(this.#waitingIDs)) { this.#waitingIDs = null; }
        }
    }

    /**
     * Informing the listeners, that the slide with the specified IDs are formatted.
     * This includes a check, if the model was destroyed in the meantime, because the trigger
     * is executed with a delay (required for performance reasons).
     *
     * @param {String[]} allSlideIDs
     *  An array with all formatted slide IDs.
     */
    #informListeners(allSlideIDs) {
        this.executeDelayed(() => {
            if (this.docModel && !this.docModel.destroyed) { this.docModel.trigger('slidestate:formatted', allSlideIDs); }
        }, 50);
    }

    /**
     * Registering a listener function for the 'tab:activated' event, that registers
     * the first finished activation of a toolbar tab.
     */
    #registerFirstTabActivatedHandler() {

        const registerTabActivation = () => {
            // adding delay for slide formatting, so that toolbar is really rendered by the browser
            this.executeDelayed(() => {
                this.#firstTabActivated = true;
                if (!_.isEmpty(this.#loadingQueue)) {
                    this.#externalFormattingHandler(this.#loadingQueue); // calling external handler directly
                }
            }, 100);
        };

        if (this.docApp.showMainToolPaneAtStart()) {
            this.docApp.docView.toolPaneManager.one('tab:activated', registerTabActivation);
        } else {
            this.docModel.waitForImportSuccess(registerTabActivation); // alternative event, triggered earlier
        }
    }
}
