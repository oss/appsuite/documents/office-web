/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { createSlideNode, setTargetContainerId } from '@/io.ox/office/textframework/utils/dom';

// constants ==============================================================

// the timer interval until that a check for slide can be done (in ms).
const CHECK_SLIDES_INTERVAL = 10000;

// class SlideVisibilityManager ===========================================

/**
 * An instance of this class represents the slide visisibility manager. This manager
 * keeps track of slides that do not need to be inserted into the DOM for performance
 * reasons.
 * This visibility manager handles only document slides. No master or layout slides.
 *
 * @param {PresentationApplication} app
 *  The application instance.
 */
class SlideVisibilityManager extends ModelObject {

    // a container with all empty slide nodes
    #emptySlideNodes = {};
    // a container that keeps track of the slides that are removed from the DOM
    #tempSlideContainer = {};
    // a collector with the IDs of all modified slides
    #operationSlideIds = {};

    // methods ------------------------------------------------------------

    /**
     * Putting all slides from the cache container into the DOM.
     */
    appendAllSlidesToDom() {

        // the order of the document slides
        let slideOrder = null;

        if (!this.docModel || !this.docModel.getSlideFormatManager) { return; }

        slideOrder = this.docModel.getStandardSlideOrder();

        _.each(slideOrder, slideId => {
            this.appendSlideToDom(slideId);
        });
    }

    /**
     * Putting one slide from the cache container into the DOM.
     *
     * @param {String} slideId
     *  The ID of the slide that inserted into the DOM.
     */
    appendSlideToDom(slideId) {

        // the empty slide node, that is currently in the DOM
        let emptySlide = null;

        if (this.#emptySlideNodes[slideId]) { // check, if the slide is cached

            emptySlide = this.#emptySlideNodes[slideId];
            this.#tempSlideContainer[slideId].insertAfter(emptySlide);
            emptySlide.remove();
            // emptySlide.replaceWith(tempSlideContainer[slideId]);

            delete this.#tempSlideContainer[slideId];
            delete this.#emptySlideNodes[slideId];

            emptySlide = null;
        }

        // register the slide, so that it is not removed immediately again
        // in the next run of the function 'checkSlidesInDom'
        // -> also register the slide, if it is already in the DOM, so that
        //    it is also save for at least 10 seconds.
        this.registerOperationSlideId(slideId);
    }

    /**
     * Check, whether a slide specified by its ID is attached to the DOM.
     *
     * @param {String} slideId
     *  The ID of the slide that is checked.
     *
     * @returns {Boolean}
     *  Whether the slide with the specified ID is attached to the DOM.
     */
    isSlideInDom(slideId) {
        return !this.#emptySlideNodes[slideId];
    }

    /**
     * Registering the slide IDs for all those slides, that receive an operation.
     * These slides shall not be formatted in the next run of the function
     * 'checkSlidesInDom'.
     *
     * @param {String} slideId
     *  The ID of the slide that is registered because of an operation.
     */
    registerOperationSlideId(slideId) {
        this.#operationSlideIds[slideId] = 1;
    }

    /**
     * Setting the initial tasks for the slide visibility manager.
     */
    initializeManager() {
        // start the process to remove formatted slides from the DOM
        this.setInterval(this.#checkSlidesInDom, CHECK_SLIDES_INTERVAL);
    }

    // private methods ----------------------------------------------------

    /**
     * Repeated function that checks, if slides can be removed from the DOM.
     * The following slides will NOT be removed from the DOM:
     *   - the first slide (caused by selection problems).
     *   - the currently active slide.
     *   - the slide is currently formatted by the slide format manager.
     *   - the slide is already removed from the DOM.
     *   - external operations are currently applied asynchronously.
     *   - it is registered in the collector 'operationSlideIds'. Then it will be removed in the next run.
     *     Registering a slide in this collector guarantees, that it stays in the DOM for at least the
     *     delay of this repeated function (10 seconds).
     */
    #checkSlidesInDom() {

        // the slide format manager
        let formatManager = null;
        // the order of the document slides
        let slideOrder = null;

        if (!this.docModel || !this.docModel.getSlideFormatManager) { return; }

        formatManager = this.docModel.getSlideFormatManager();
        slideOrder = this.docModel.getStandardSlideOrder();

        if (!formatManager) { return; }

        if (this.docApp.getView().isBusy()) { return; } // not removing slides from the DOM in long running processes (48796)

        // remove all slides from the DOM, except:
        // - the first slide (caused by selection problems).
        // - the currently active slide.
        // - the slide is currently formatted by the slide format manager.
        // - the slide is already removed from the DOM.
        // - external operations are currently applied asynchronously.
        // - it is registered in the collector 'operationSlideIds'. Then it will be removed in the next run.
        _.each(slideOrder, slideId => {
            if (!this.#emptySlideNodes[slideId] && !this.docModel.isActiveSlideId(slideId) && !formatManager.isRunningTask(slideId) && slideId !== slideOrder[0] && !this.docModel.isProcessingExternalOperations() && !this.#operationSlideIds[slideId]) {
                this.#removeSlideFromDom(slideId);
            }
        });

        // clearing the registered operation IDs, so that they can be deleted in the next run
        this.#operationSlideIds = {};
    }

    /**
     * Removing one slide specified by its ID from the DOM.
     *
     * @param {String} slideId
     *  The ID of the slide that shall be removed from the DOM.
     */
    #removeSlideFromDom(slideId) {

        // the slide node that will be replaced with an empty slide
        let slideNode = null;
        // the empty slide node, that will replace the specified slide in the DOM
        let emptySlide = null;

        if (!this.#tempSlideContainer[slideId]) { // check, if the slide is already cached

            slideNode = this.docModel.getSlideById(slideId);
            emptySlide = createSlideNode();

            emptySlide.addClass('replacementslide'); // marker for empty slides
            setTargetContainerId(emptySlide, slideId); // saving ID at new empty slide

            emptySlide.insertAfter(slideNode);
            slideNode.detach();
            // slideNode.replaceWith(emptySlide);

            this.#tempSlideContainer[slideId] = slideNode; // saving the slide node in the container
            this.#emptySlideNodes[slideId] = emptySlide; // saving a reference to the empty slide
        }
    }

}

// export =================================================================

export default SlideVisibilityManager;
