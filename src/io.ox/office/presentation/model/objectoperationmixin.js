/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math, jpromise } from '@/io.ox/office/tk/algorithms';
import { containsNode, hasKeyCode, Rectangle } from '@/io.ox/office/tk/dom';
import { TrackingObserver } from '@/io.ox/office/tk/tracking';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { SMALL_DEVICE, convertLengthToHmm, convertHmmToLength, extendOptions, getArrayOption, getBooleanOption,
    getNumberOption, getObjectOption, getStringOption } from '@/io.ox/office/tk/utils';

import { getExplicitAttributes, getExplicitAttributeSet, insertAttribute } from '@/io.ox/office/editframework/utils/attributeutils';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { getWidthForPreset } from '@/io.ox/office/editframework/utils/border';

import { checkForLateralTableStyle } from '@/io.ox/office/textframework/components/table/table';
import { APPCONTENT_NODE_SELECTOR, getTableRows, isImagePlaceholderTemplateButton, isTablePlaceholderTemplateButton,
    splitTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, getContentNodeElement, getDOMPosition, getOxoPosition, increaseLastIndex,
    isPositionInsideDrawingGroup } from '@/io.ox/office/textframework/utils/position';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import { CHANGE_CONFIG, DELETE, FIELD_INSERT, INSERT_DRAWING, MOVE, PARA_INSERT, ROWS_INSERT, TEXT_INSERT,
    SET_ATTRIBUTES } from '@/io.ox/office/presentation/utils/operations';
import { IMAGE_PLACEHOLDER_TYPE, PLACEHOLDER_DRAWING_NAMES, PLACEHOLDER_TEXT_CONTENT, PLACEHOLDER_TITLE_CONTENT,
    TABLE_PLACEHOLDER_TYPE, getPlaceHolderDrawingIndex, getPlaceHolderDrawingType, isContentBodyPlaceHolderAttributeSet,
    isFooterPlaceHolderDrawing, isPlaceHolderAttributeSet, isPlaceHolderDrawing, isPlaceHolderWithoutTextAttributeSet,
    isEmptyPlaceHolderDrawing, isUserModifiedPlaceHolderDrawing, requiresUserTransformedProperty } from '@/io.ox/office/presentation/utils/placeholderutils';
import { convertAppContentBoxToOperationBox, extendPlaceholderProp } from '@/io.ox/office/presentation/utils/presentationutils';
import { createDefaultShapeAttributeSet, createDefaultSizeForShapeId, generateRecalcConnectorOptions, getParagraphAttrsForDefaultShape,
    getRotatedDrawingPoints, getUniqueConnections, pointCloseToSnapPoint, pointInsideRect, rotatePointWithAngle, transformAttributeSet
} from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { applyImageTransformation, getFileUrl, insertDataUrl, insertURL, isAbsoluteUrl, isBase64 } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { getPresetAspectRatio, getPresetShape, isPresetConnectorId, isTextShapeId } from '@/io.ox/office/drawinglayer/view/drawinglabels';
import { END_LINKED_CLASS, NODE_SELECTOR, START_LINKED_CLASS, TABLE_NODE_IN_TABLE_DRAWING_CLASS, createDrawingFrame, createSnapPoints,
    drawPreviewShape, getDrawingNode, getDrawingRotationAngle, getGroupDrawingChildren, getGroupDrawingCount, isAutoResizeHeightDrawingFrame,
    isConnectorDrawingFrame, isFlippedHorz, isFlippedVert, isGroupDrawingFrame, isGroupDrawingFrameWithShape, isMinHeightDrawingFrame,
    isNoWordWrapDrawingFrame, isTableDrawingFrame, recalcConnectorPoints, updateResizersMousePointers } from '@/io.ox/office/drawinglayer/view/drawingframe';

const { round } = Math;

// static functions =======================================================

/**
 *  A utility method that accepts two configuration parameters
 *  and does return a newly computed set of attribute values.
 *
 *  @param {Object} [drawingAttrs]
 *   configuration parameters:
 *   @param {Number} [drawingAttrs.width]
 *   @param {Number} [drawingAttrs.height]
 *   @param {Number} [drawingAttrs.left]
 *   @param {Number} [drawingAttrs.top]
 *
 *  @param {Object} [ratioDeltas]
 *   configuration parameters:
 *   @param {Number} [ratioDeltas.horizontal]
 *   @param {Number} [ratioDeltas.vertical]
 *
 *  @returns {Object} [attrs]
 *   a set of drawing attributes, newly computed from [drawingAttrs] and [ratioDeltas] values.
 */
function recalculateDrawingAttributes(drawingAttrs, ratioDeltas) {
    var
        width   = drawingAttrs.width  || 0, // does dafault to ZERO for easch value if (explicit) attribute value could not be retrieved.
        height  = drawingAttrs.height || 0, //
        left    = drawingAttrs.left   || 0, //
        top     = drawingAttrs.top    || 0, //

        ratio   = width / height,

        deltaRatioX = ratioDeltas.horizontal,
        deltaRatioY = ratioDeltas.vertical,

        attrs   = {};

    if (drawingAttrs.aspectLocked === true) {

        ratio = (Number.isFinite(ratio) && (ratio !== 0)) ? ratio : 1; // does fall back to 1 when running into edge cases.

        if ((deltaRatioX !== 1) && (deltaRatioX < deltaRatioY)) {

            attrs.width   = round(width * deltaRatioX);
            attrs.height  = round(attrs.width * ratio);

        } else if ((deltaRatioY !== 1) && (deltaRatioY < deltaRatioX)) {

            attrs.height  = round(height * deltaRatioY);
            attrs.width   = round(attrs.height * ratio);
        }
        if ('width' in attrs) {

            attrs.left  = round(left * deltaRatioX);
            attrs.top   = round(top  * deltaRatioY);
        }
    } else {
        attrs.width   = round(width  * deltaRatioX);
        attrs.height  = round(height * deltaRatioY);
        attrs.left    = round(left   * deltaRatioX);
        attrs.top     = round(top    * deltaRatioY);
    }
    return attrs;
}

/**
 *  A utility method that accepts one configuration parameter
 *  and does trigger recalculation of drawing attributes in
 *  order to generate new "set attribute"-type operations.
 *
 *  @param {Object} [data]
 *   configuration parameters:
 *   @param {PresentationModel} [data.model]
 *    the presentation model.
 *   @param {OperationGenerator} [data.generator]
 *    the operations generator.
 *   @param {jQuery.Node} [data.$slide]
 *    the currently handled slide, a DOM Element Node in its jQuery state.
 *   @param {Number: {}} [data.ratioDeltas]
 *    an object(hash) that features two number types, both the vertical and the horizontal delta each as a factor.
 *   @param {Boolean} [data.isDocSlide=false]
 *    whether the handled slide is a document slide (in this case the target must not be used).
 */
function reformatAnySlide(data) {
    var
        model       = data.model,
        generator   = data.generator,
        $slide      = data.$slide,
        ratioDeltas = data.ratioDeltas,
        slideId     = (_.isString(data.slideId) && data.slideId) || model.getSlideId($slide), // `slideId` is optional.
        isDocSlide  = getBooleanOption(data, 'isDocSlide', false);

    $slide.children('.drawing').toArray().forEach(function (drawingNode) {
        var
            drawingAttrs   = getExplicitAttributes(drawingNode, 'drawing'),
            operationAttrs = null,
            slidePos = null,
            drawingPos = null;

        //globalLogger.info('+++ reformatAnySlide +++ forEach drawingNode :: drawingAttrs : ', drawingAttrs);

        if (_.isNumber(drawingAttrs.left) || _.isNumber(drawingAttrs.width) || _.isNumber(drawingAttrs.height) || _.isNumber(drawingAttrs.top)) {

            drawingAttrs  = recalculateDrawingAttributes(drawingAttrs, ratioDeltas);

            // allowing to calculate the drawing position, even if the slide is not attached to the DOM (56016)
            slidePos = model.getSlidePositionById(slideId);
            drawingPos = getOxoPosition($slide, drawingNode, 0);
            drawingPos = slidePos.concat(drawingPos);

            operationAttrs = {
                start:   _.copy(drawingPos),
                attrs:  { drawing: drawingAttrs }
            };

            // no target at document slides (53108)
            if (!isDocSlide) { operationAttrs.target = slideId; }

            generator.generateOperation(SET_ATTRIBUTES, operationAttrs);

        }
    });
}

// mix-in class ObjectOperationMixin ======================================

/**
 * A mix-in class for the document model class PresentationModel providing
 * the style sheet containers for all attribute families used in a
 * presentation document.
 *
 * @param {Application} app
 *  The application containing this instance.
 */
export default function ObjectOperationMixin(app) {
    // self reference for local functions
    var self = this;
    var MAX_NUM_DRAWINGS_ON_SLIDE_ALLOWED = 50;
    var MAX_NUM_SNAPPOINTS_ON_SLIDE_ALLOWED = 200;
    // how manu times has information for disabled connector functionality appeared
    var infoCounter = 0;

    // private methods ----------------------------------------------------

    /**
     * Calculates the size of the image defined by the given url,
     * limiting the size to the paragraph size of the current selection.
     *
     * @param {String} url
     *  The image url or base64 data url
     *
     * @returns {jQuery.Promise}
     *  A promise where the resolve result contains the size object
     *  including width and height in Hmm {width: 123, height: 456}
     */
    function getImageSize(url) {

        var // the result deferred
            def = self.createDeferred(),
            // the image for size rendering
            image = $('<img>'),
            // the image url
            absUrl,
            // the clipboard holding the image
            clipboard = null;

        if (!url) { return def.reject(); }

        absUrl = getFileUrl(app, url);

        // append the clipboard div to the body and place the image into it
        clipboard = app.getView().createClipboardNode();
        clipboard.append(image);

        // if you set the load handler BEFORE you set the .src property on a new image, you will reliably get the load event.
        image.one('load', function () {

            // Workaround for a strange Chrome behavior, even if we use .one() Chrome fires the 'load' event twice.
            // One time for the image node rendered and the other time for a not rendered image node.
            // We check for the rendered image node
            if (containsNode(clipboard, image)) {

                // maybe the slide is not so big
                // -> reducing the image size to slide size, if required
                def.resolve({ width: convertLengthToHmm(image.width(), 'px'), height: convertLengthToHmm(image.height(), 'px') });
            }
        })
            .one('error', function () {
                globalLogger.warn('Editor.getImageSize(): image load error');
                def.reject();
            })
            .attr('src', absUrl);

        // always remove the clipboard again
        def.always(function () {
            clipboard.remove();
        });

        return def.promise();
    }

    /**
     * Inserts a drawing component into the document DOM.
     *
     * @param {String} type
     *  The type of the drawing. Supported values are 'shape', 'group', 'table',
     *  'image', 'diagram', 'chart', 'ole', 'horizontal_line', 'undefined'
     *
     * @param {Number[]} start
     *  The logical start position for the new drawing.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new drawing component, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {String} [target]
     *  ID of root container node.
     *
     * @returns {Boolean}
     *  Whether the drawing has been inserted successfully.
     */
    function implInsertDrawing(type, start, attrs, target) {

        var // text span that will precede the field
            span = null,
            // deep copy of attributes, because they are modified in webkit browsers
            attributes = _.copy(attrs, true),
            // new drawing node
            drawingNode = null,
            // root node container
            rootNode = self.getRootNode(target),
            // image aspect ratio
            currentElement = getContentNodeElement(rootNode, start.slice(0, -1), { allowDrawingGroup: true }),
            // whether the drawing is a group drawing node
            isGroupNode = currentElement && isGroupDrawingFrame(currentElement),
            // whether the drawing is inserted into a drawing group
            insertIntoDrawingGroup = false,
            // the function used to insert the new drawing frame
            insertFunction = 'insertAfter',
            // a table node for drawings of type 'table'
            table = null,
            // the number of children in the drawing group
            childrenCount = 0;

        // helper function to insert a drawing frame into an existing drawing group
        function addDrawingFrameIntoDrawingGroup() {

            childrenCount = getGroupDrawingCount(currentElement);

            if (_.isNumber(childrenCount)) {
                if (childrenCount === 0) {
                    if (_.last(start) === 0) {
                        span = $(currentElement).children().first(); // using 'span' for the content element in the drawing group
                        insertFunction = 'appendTo';
                    }
                } else {
                    if (_.last(start) === 0) {
                        // inserting before the first element
                        span = getGroupDrawingChildren(currentElement, 0);
                        insertFunction = 'insertBefore';
                    } else if (_.last(start) <= childrenCount) {
                        // inserting after the span element
                        span = getGroupDrawingChildren(currentElement, _.last(start) - 1);
                    }
                }
                insertIntoDrawingGroup = true;
            }
        }

        if (isGroupNode) {
            addDrawingFrameIntoDrawingGroup(); // adding drawing into the group, without using a text span
        } else {
            try {
                span = self.prepareTextSpanForInsertion(start, {}, target);
            } catch {
                // do nothing, try to repair missing text spans
            }
        }

        // insert the drawing with default settings between the two text nodes (store original URL for later use)
        drawingNode = createDrawingFrame(type)[insertFunction](span);

        // setting marker for drawing type 'image', that need to be formatted at least once (if grouped) (56742)
        if (type === 'image') { drawingNode.addClass('formatrequired'); }

        if (_.isObject(attributes)) {
            // apply the passed drawing attributes
            self.drawingStyles.setElementAttributes(drawingNode, attributes);
            // saving list style attributes, that are assigned directly to non-place holder drawings (45279)
            self.drawingStyles.setDrawingListStyleAttributes(drawingNode, attributes);
            // store drawing id in the model collection
            if (attributes.drawing && attributes.drawing.id) { self.storeDrawingId(attributes.drawing.id); }
        }

        // applying all attributes (TODO: Also handling attributes from other layers)
        if (self.isImportFinished() && !insertIntoDrawingGroup) { self.drawingStyles.updateElementFormatting(drawingNode); } // TODO (is this necessary?)

        // inserting the table node into a drawing of type 'table'
        if (type === 'table') {

            table = $('<table>').attr('role', 'grid').addClass(TABLE_NODE_IN_TABLE_DRAWING_CLASS).append($('<colgroup>'));
            self.insertContentNode(appendNewIndex(start), table, (target ? target : undefined));

            // apply the passed table attributes
            if (attrs && attrs.table) { self.tableStyles.setElementAttributes(table, attrs); }
        }

        // saving the drawing attributes in drawing styles (ignoring the return value, no event is fired)
        if (target) { self.drawingStyles.savePlaceHolderAttributes(target, attrs, drawingNode); }

        self.setLastOperationEnd(increaseLastIndex(start));

        return true;
    }

    // simplified version of implInsertDrawing, that allows only drawings inside slides

    //        function implInsertDrawing(type, start, attrs, target) {
    //
    //            var // deep copy of attributes, because they are modified in webkit browsers
    //                attributes = _.copy(attrs, true),
    //                // new drawing node
    //                drawingNode = null,
    //                // determining the parent of the new drawing (this can only be a group or a slide, not a paragraph)
    //                drawingParentPoint = Position.getDOMPosition(self.getRootNode(target), _.initial(start), true),
    //                // the parent node
    //                drawingParent = drawingParentPoint ? drawingParentPoint.node : null,
    //                // the children of the drawing container
    //                children = drawingParent ? $(drawingParent).children(DrawingFrame.NODE_SELECTOR) : null,
    //                // the number of children in the drawing group
    //                childrenCount = children ? children.length : 0,
    //                // the function used to insert the new drawing frame
    //                insertFunction = 'insertAfter',
    //                // the element, relative to that the new drawing is inserted
    //                insertElement = drawingParent;
    //
    //            // checking, if the drawing parent is a slide or a group container
    //            if (!(DOM.isSlideNode(drawingParent) || DrawingFrame.isGroupDrawingFrame(drawingParent))) { return false; }
    //
    //            // checking, if the insert position is valid -> it is not possible to insert at position 2, if there is only
    //            // one drawing inside slide or group container
    //            if (_.last(start) > childrenCount) { return false; }
    //
    //            // check, if 'appendTo' must be used instead of 'insertAfter'
    //            if (_.last(start) === 0) {
    //                // inserting the new drawing as first child of the parent
    //                insertFunction = 'prependTo';
    //            } else {
    //                // finding that drawing, after which the new drawing will be inserted
    //                insertElement = children[_.last(start) - 1];
    //            }
    //
    //            // insert the drawing with default settings between the two text nodes (store original URL for later use)
    //            drawingNode = DrawingFrame.createDrawingFrame(type)[insertFunction](insertElement);
    //
    //            // apply the passed drawing attributes
    //            if (_.isObject(attributes)) { drawingStyles.setElementAttributes(drawingNode, attributes); }
    //
    //            // saving the attributes in drawing styles (ignoring the return value, no event is fired)
    //            if (target) { self.drawingStyles.savePlaceHolderAttributes(target, attrs); }
    //
    //            return true;
    //        }

    /**
     * Calculating size for inserting default placeholder on master or layout slide.
     *
     * @param {String} type
     *  Type of placeholder to be inserted.
     *
     * @returns {Object}
     *  The rectangle with the properties 'top', 'left', 'width' and 'height'.
     *  The values are given in 1/100 mm relative to the slide.
     */
    function getPlaceholderRectangleForInsert(type) {

        var slideSize = self.getSlideDocumentSize(); // the active slide node
        // the new box object
        var operationBox = { left: 0, top: 0, width: slideSize.width, height: slideSize.height };
        // a factor to influence the top position of the footer place holder drawings
        var footerTopFactor = app.isODF() ? 0.911 : 0.93;
        // a factor to influence the height of the footer place holder drawings
        var footerHeightFactor = app.isODF() ? 0.069 : 0.05;

        switch (type) {
            case 'title':
                operationBox.left += operationBox.width * 0.05;
                operationBox.top += operationBox.height * 0.04;
                operationBox.width *= 0.9;
                operationBox.height *= 0.17;
                break;
            case 'body':
                operationBox.left += operationBox.width * 0.05;
                operationBox.top += operationBox.height * 0.23;
                operationBox.width *= 0.9;
                operationBox.height *= 0.67;
                break;
            case 'dt':
                operationBox.left += operationBox.width * 0.05;
                operationBox.top += operationBox.height * footerTopFactor;
                operationBox.width *= 0.25;
                operationBox.height *= footerHeightFactor;
                break;
            case 'ftr':
                operationBox.left += operationBox.width * 0.35;
                operationBox.top += operationBox.height * footerTopFactor;
                operationBox.width *= 0.3;
                operationBox.height *= footerHeightFactor;
                break;
            case 'sldNum':
                operationBox.left += operationBox.width * 0.7;
                operationBox.top += operationBox.height * footerTopFactor;
                operationBox.width *= 0.25;
                operationBox.height *= footerHeightFactor;
                break;
        }

        return operationBox;
    }

    /**
     * Inserts a table component into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new table.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new table component, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {Object} [target]
     *  If exists, defines node, to which start position is related.
     *
     * @returns {Boolean}
     *  Whether the table has been inserted successfully.
     */
    //        function implInsertTable(start, attrs, target) {
    //
    //            var // the new table node
    //                table = $('<table>').attr('role', 'grid').append($('<colgroup>')),
    //                // whether the table was inserted into the DOM
    //                inserted = self.insertContentNode(_.clone(start), table, (target ? target : undefined));
    //
    //            // insertContentNode() writes warning to console
    //            if (!inserted) { return false; }
    //
    //            // apply the passed table attributes
    //            if (attrs && attrs.table) { self.tableStyles.setElementAttributes(table, attrs); }
    //
    //            return true;
    //        }

    /**
     * Calculating new value for position, size and rotate of a drawing node. The new values depend
     * on the old values, the event properties and some addtional properties.
     *
     * @param {jQuery.Event} event
     *  The jQuery event object.
     *
     * @param {jQuery|Node} drawingNode
     *  The drawing node that will get new values assigned.
     *
     * @param {Object[]} allProperties
     *  A collector with objects for the properties that need to be modified. It is necessary
     *  that each property is specified by its 'name'. Additional optional values can be
     *  specified to influence the calculation. These properties are 'type', 'delta' or
     *  'ctrlDoubles'.
     *
     * @returns {Object|Null}
     *  The new drawing object for a setAttributes operation. If no valid new value can be
     *  calculated, null is returned.
     */
    function getDrawingPropertiesObject(event, drawingNode, allProperties) {

        var // the explicit attributes at the specified element
            elemAttrs = getExplicitAttributeSet(drawingNode),
            // the merged attributes of the specified element (required for positions of place holder drawings)
            mergedAttrs = null,
            // the previously calculated value
            previousDelta = null,
            // whether the return value must be 0
            invalidAttributes = false,
            // the old value of the specified property
            attrs = {};

        _.each(allProperties, function (property) {

            var // the old and the new value for the property
                oldValue = null, newValue = null,
                // whether the new value must be increased relative to the current position
                increase = getBooleanOption(property, 'increase', false);

            if (elemAttrs && elemAttrs.drawing && _.isNumber(elemAttrs.drawing[property.name])) {
                oldValue = elemAttrs.drawing[property.name];
            } else {
                mergedAttrs = mergedAttrs || self.drawingStyles.getElementAttributes(drawingNode);
                oldValue = mergedAttrs.drawing[property.name];
            }

            if (property.name === 'rotation') {
                newValue = oldValue + property.delta;
                // normalize negative and angles greater than 360
                if (newValue < 0) {
                    newValue += 360;
                }
                if (newValue > 360) {
                    newValue -= 360;
                }
            } else {
                if (property.delta === 'halfOfPrevious') { property.delta = round(0.5 * previousDelta); }

                if (property.type === 'percentage') { property.delta = round((property.delta * oldValue) / 100); }

                if (property.ctrlDoubles && event.ctrlKey) { property.delta *= 2; }

                newValue = increase ? oldValue + property.delta : oldValue - property.delta;

                // avoiding operations without change, because the percentage is not changing anything (very small drawings)
                if (property.type === 'percentage' && !increase && (oldValue - newValue < 2)) { invalidAttributes = true; }
            }

            attrs[property.name] = newValue;

            previousDelta = property.delta; // saving for the following property
        });

        // setting a minimum height at frames (ODP only), 55236
        if (app.isODF() && isMinHeightDrawingFrame(drawingNode) && attrs.height) { attrs.minFrameHeight = attrs.height; }

        if (invalidAttributes) { attrs = null; }

        return attrs;
    }

    /**
     * Generating one operation for a drawing specified by its drawing selection object. This can be defined
     * by a single drawing or a multiple drawing selection.
     *
     * @param {Object} generator
     *  The operation generator object that generates and collects all operations.
     *
     * @param {jQuery.Event} event
     *  The jQuery event object.
     *
     * @param {Object} drawingSelection
     *  A drawing selection object as it is created by a multiple drawing selection. This object contains
     *  information about the selected drawing and its logical position.
     *
     * @param {Boolean} leftRightCursor
     *  Whether this event was triggered by pressing left or right cursor key.
     *
     * @param {Object} connectorOptions
     *  @param {Object} connectorOptions.activeSlideOffset
     *      Left and top position values of active slide.
     *  @param {Array} connectorOptions.allSlideDrawings
     *      Collection of all drawings on active slide.
     *  @param {Array} connectorOptions.allSlideConnectors
     *      Collection of all connector drawings on active slide.
     */
    function generateDrawingAttributeOperation(generator, event, drawingSelection, leftRightCursor, connectorOptions) {

        var // the key for setting the top left corner position
            positionProperty = leftRightCursor ? 'left' : 'top',
            // the property name for changing width or height
            sizeProperty = leftRightCursor ? 'width' : 'height',
            // whether a new operation needs to be created
            createOperation = true,
            // the properties for the new setAttributes operation (cloning the position to avoid OT problems, DOCS-2612)
            operationProperties = { start: _.clone(drawingSelection.startPosition), attrs: { drawing: {} } },
            // the delta value for the moving of a drawing (in 1/100 mm)
            positionDelta = 100,
            // the delta value for the width and height of a drawing (percentage)
            sizeDeltaPercentage = 5,
            // drawing node from multiselection
            drawingNode = self.getSelection().getDrawingNodeFromMultiSelection(drawingSelection),
            // if key combination for rotation is pressed
            isRotate = event.altKey && leftRightCursor,
            // current root level node (slide container)
            rootNode = self.getCurrentRootNode(),
            // collection of all drawings on active slide
            allSlideDrawings = getArrayOption(connectorOptions, 'allSlideDrawings', []),
            // collection of all connector drawings on active slide
            allSlideConnectors = getArrayOption(connectorOptions, 'allSlideConnectors', {}),
            // left and top position values of active slide
            activeSlideOffset = getObjectOption(connectorOptions, 'activeSlideOffset', []);

        if (event.shiftKey) {
            // generating a resize operation for all selected drawings
            operationProperties.attrs.drawing = getDrawingPropertiesObject(event, drawingNode, [{ name: sizeProperty, delta: sizeDeltaPercentage, type: 'percentage', increase: hasKeyCode(event, 'RIGHT_ARROW', 'UP_ARROW') }, { name: positionProperty, delta: 'halfOfPrevious', increase: hasKeyCode(event, 'LEFT_ARROW', 'DOWN_ARROW') }]);
            if (!operationProperties.attrs.drawing) {
                createOperation = false;  // avoiding OPs without change or negative width/height
            } else {
                self.generateConnectorModifyOps(generator, operationProperties, 'resize', drawingNode, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
            }
            // generating multiple resize operations for each row of a table drawing
            if (hasKeyCode(event, 'UP_ARROW', 'DOWN_ARROW') && isTableDrawingFrame(drawingNode)) {
                createOperation = self.generateTableHeightOps(generator, drawingNode, event);
            }
        } else if (isRotate) {
            if (isTableDrawingFrame(drawingNode)) {
                createOperation = false; // no rotation of tables
            } else {
                operationProperties.attrs.drawing = getDrawingPropertiesObject(event, drawingNode, [{ name: 'rotation', delta: hasKeyCode(event, 'RIGHT_ARROW') ? 15 : -15 }]);
                self.generateConnectorModifyOps(generator, operationProperties, 'rotate', drawingNode, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
            }
        } else {
            // generating a move operation for all selected drawings (different velocity, if Ctrl is pressed)
            operationProperties.attrs.drawing = getDrawingPropertiesObject(event, drawingNode, [{ name: positionProperty, delta: positionDelta, ctrlDoubles: true, increase: hasKeyCode(event, 'RIGHT_ARROW', 'DOWN_ARROW') }]);
            self.generateConnectorModifyOps(generator, operationProperties, 'move', drawingNode, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
        }

        // marking place holder drawings in ODF as modified by the user
        if (app.isODF() && requiresUserTransformedProperty(drawingNode)) { operationProperties.attrs.presentation = { userTransformed: true }; }

        if (createOperation) {
            extendPlaceholderProp(app, drawingNode, operationProperties);
            self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
            generator.generateOperation(SET_ATTRIBUTES, operationProperties);
        }

    }

    /**
     * Handler function for moving one drawing from the specified start position to the
     * specified end position.
     * Info: In Presentation app only drawings can be moved.
     *
     * @param {Number[]} start
     *  The logical start position of the drawing.
     *
     * @param {Number[]} to
     *  The logical end position of the drawing.
     *
     * @param {Object} [target]
     *  If exists, defines node, to which the logical positions are related.
     *
     * @returns {Boolean}
     *  Whether the move of the drawing has been successful.
     */
    function implMove(start, to, target) {

        var // the active root node
            activeRootNode = self.getRootNode(target),
            // the source point (node and offset) at the logical start position
            sourcePoint = getDOMPosition(activeRootNode, start, true),
            // the source point (node and offset) at the logical end position
            destPoint = getDOMPosition(activeRootNode, to, true),
            // the drawing node at the logical start position
            sourceNode = null,
            // the drawing node at the logical end position
            destNode = null,
            // whether the drawing is moved backwards (to the foreground) or forwards (to the background)
            backwards = _.last(to) > _.last(start),
            // whether the moved drawing is inserted before the drawing node that is currently at the
            // specified destination position
            insertBefore = !backwards;

        if (sourcePoint && destPoint && sourcePoint.node && destPoint.node) {

            sourceNode = $(sourcePoint.node);
            destNode = $(destPoint.node);

            // moving the drawing (and handle text spans)
            if (insertBefore) {
                sourceNode.next().remove();
                destNode = splitTextSpan(destNode.prev(), 0, { append: true });
                sourceNode.insertBefore(destNode);
            } else {
                sourceNode.prev().remove();
                destNode = splitTextSpan(destNode.next(), 0);
                $(sourceNode).insertAfter(destNode);
            }

        }

        return true;
    }

    /**
     * Return the drawing attributes of the given drawingNode.
     *
     * @param {HTMLElement|jQuery} drawingNode
     * @returns {Object} the drawing attributes.
     */
    function getDrawingAttrsByNode(drawingNode) {
        var expAttrs = getExplicitAttributeSet(drawingNode),
            drawing = expAttrs.drawing;

        if (isPlaceHolderAttributeSet(expAttrs)) {
            drawing = self.drawingStyles.getElementAttributes(drawingNode).drawing;
        }
        // tables have no height defined in attributes. Also auto-resize shapes might have no (ODF) or wrong (53176) height attribute
        if (isTableDrawingFrame(drawingNode) || isAutoResizeHeightDrawingFrame(drawingNode)) {
            drawing.height = convertLengthToHmm(drawingNode.height(), 'px');
            if (isNoWordWrapDrawingFrame(drawingNode)) {
                drawing.width = convertLengthToHmm(drawingNode.width(), 'px'); // #57304
            }
        }
        return drawing;
    }

    /**
     * Class to change the alignment of the selected drawings.
     */
    function ChangeAlignment() {

        var self2 = this,
            baseObject = {},
            moveAttrName = null,
            rightBottomPosition = null;

        // private methods ----------------------------------------------------

        /**
         * Get the drawing from a drawing selection.
         */
        function getRotatedDrawingByDrawingSelection(drawingSelection) {
            var drawingNode = self.getSelection().getDrawingNodeFromMultiSelection(drawingSelection),
                drawing = getDrawingAttrsByNode(drawingNode),
                angle = getDrawingRotationAngle(app.getModel(), drawingNode);
            return getRotatedDrawingPoints(drawing, angle);
        }
        /**
         * Set the drawing which is on the lowest left or top position, to move the selected drawings.
         */
        function setLeftTopDrawing(drawingSelection) {
            var drawing = getRotatedDrawingByDrawingSelection(drawingSelection);
            if (baseObject[moveAttrName] === null || drawing[moveAttrName] < baseObject[moveAttrName]) {
                baseObject.object = drawingSelection;
                baseObject[moveAttrName] = drawing[moveAttrName];
            }
        }

        /**
         * Return the operation properties to move the drawingNode.
         *
         * @param {HTMLElement|jQuery} drawingNode
         * @param {Number} position the new position of the drawing.
         * @param {Number[]} startPosition
         *  The logical start position of this drawing.
         * @returns {Object|null} the new operations to move the drawing or null if the drawing position is not changed,
         *  this happens if the drawing is the base object.
         */
        function getOperationsProperties(drawingNode, position, startPosition) {
            if (position !== null && _.isNumber(position) && !_.isNaN(position)) {
                var operationProperties = { start:  _.copy(startPosition), attrs: { drawing: { } } };
                operationProperties.attrs.drawing[moveAttrName] = position;
                extendPlaceholderProp(app, drawingNode, operationProperties);
                return operationProperties;
            }
            return null;
        }

        /**
         * Returns a helper object wich contains some objects which are used in different
         * function. This helper function is created to avoid code doubling.
         *
         * @param {Object} drawingSelection the drawing to calculate the new Postition.
         *
         * @returns {Object}
         *  - {HTMLElement|jQuery} drawingNode
         *  - {Object} drawing the drawing attributes
         *  - {Number} angle the rotation angle of the drawing
         *  - {Object} rotPos
         *      the result of the getRotatedDrawingPoints(drawing, angle) function
         */
        function getDrawingHelperObject(drawingSelection) {
            var drawingNode = self.getSelection().getDrawingNodeFromMultiSelection(drawingSelection),
                drawing = getDrawingAttrsByNode(drawingNode),
                angle = getDrawingRotationAngle(app.getModel(), drawingNode),
                rotPos = getRotatedDrawingPoints(drawing, angle);

            return {
                drawingNode,
                drawing,
                angle,
                rotPos
            };
        }

        /**
         * Returns the new left or top position for the given drawing.
         */
        function getLeftTopPosition(drawingSelection, startPosition) {
            if (drawingSelection !== baseObject.object) {
                var obj = getDrawingHelperObject(drawingSelection),
                    moveLeft = baseObject.left - obj.rotPos.left,
                    moveTop = baseObject.top - obj.rotPos.top,
                    centerLeft = obj.drawing.left + obj.drawing.width / 2,
                    centerTop = obj.drawing.top  + obj.drawing.height / 2,
                    position = round(rotatePointWithAngle(centerLeft + moveLeft, centerTop + moveTop, obj.rotPos.leftTopPoint.left + moveLeft, obj.rotPos.leftTopPoint.top + moveTop, -obj.angle)[moveAttrName]);

                return getOperationsProperties(obj.drawingNode, position, startPosition);
            }
            return null;
        }

        /**
         * Set the drawings which are on the lowest left and most left position
         * to calculate the center of this two drawings.
         */
        function setCenterPosition(drawingSelection) {
            setLeftTopDrawing(drawingSelection);
            setRightBottomDrawing(drawingSelection);
        }

        /**
         * Set the drawings which are on the lowest top and most top position
         * to calculate the middle of this two drawings.
         */
        function setMiddlePosition(drawingSelection) {
            setLeftTopDrawing(drawingSelection);
            setRightBottomDrawing(drawingSelection);
        }

        /**
         * Returns the new center or middle position of the given drawing. If it returns center or middle
         * will be set in the setAlignmentDirection function.
         */
        function getCenterMiddlePosition(drawingSelection, startPosition) {
            if (baseObject.centerMiddle === null) {
                baseObject.centerMiddle = round((baseObject[rightBottomPosition] - baseObject[moveAttrName]) / 2 + baseObject[moveAttrName]);
            }
            var obj = getDrawingHelperObject(drawingSelection),
                moveLeft = baseObject.centerMiddle - obj.rotPos.width / 2 - obj.rotPos.left,
                moveTop = baseObject.centerMiddle - obj.rotPos.height / 2 - obj.rotPos.top,
                centerLeft = obj.drawing.left + obj.drawing.width / 2,
                centerTop = obj.drawing.top  + obj.drawing.height / 2,
                position = round(rotatePointWithAngle(centerLeft + moveLeft, centerTop + moveTop, obj.rotPos.leftTopPoint.left + moveLeft, obj.rotPos.leftTopPoint.top + moveTop, -obj.angle)[moveAttrName]);

            return getOperationsProperties(obj.drawingNode, position, startPosition);
        }

        /**
         * Set the drawing which is on the most left or top position, to move the selected drawings.
         */
        function setRightBottomDrawing(drawingSelection) {
            var drawing = getRotatedDrawingByDrawingSelection(drawingSelection),
                pos = drawing[rightBottomPosition];

            if (baseObject[rightBottomPosition] === null || pos > baseObject[rightBottomPosition]) {
                baseObject.object = drawingSelection;
                baseObject[rightBottomPosition] = pos;
            }
        }

        /**
         * Returns the new left or top position for the given drawing.
         */
        function getRightBottomPosition(drawingSelection, startPosition) {
            if (drawingSelection !== baseObject.object) {
                var obj = getDrawingHelperObject(drawingSelection),
                    moveLeft = baseObject.right - obj.rotPos.width - obj.rotPos.left,
                    moveTop = baseObject.bottom - obj.rotPos.height - obj.rotPos.top,
                    centerLeft = obj.drawing.left + obj.drawing.width / 2,
                    centerTop = obj.drawing.top  + obj.drawing.height / 2,
                    position = round(rotatePointWithAngle(centerLeft + moveLeft, centerTop + moveTop, obj.rotPos.leftTopPoint.left + moveLeft, obj.rotPos.leftTopPoint.top + moveTop, -obj.angle)[moveAttrName]);
                return getOperationsProperties(obj.drawingNode, position, startPosition);
            }
            return null;
        }

        /**
         * Set the direction of the alignment for the selected drawings.
         *
         * @param {String} direction of the alignment.
         */
        this.setAlignmentDirection = function (direction) {
            baseObject = {
                left: null,
                right: null,
                top: null,
                bottom: null,
                centerMiddle: null,
                object: null
            };
            moveAttrName = null;
            self2.setBaseDrawing = $.noop;
            self2.getOperationPropertiesForMultiSelection = $.noop;
            rightBottomPosition = null;

            switch (direction) {
                case 'left':
                    moveAttrName = 'left';
                    self2.setBaseDrawing = setLeftTopDrawing;
                    self2.getOperationPropertiesForMultiSelection = getLeftTopPosition;
                    break;
                case 'center':
                    moveAttrName = 'left';
                    rightBottomPosition = 'right';
                    self2.setBaseDrawing = setCenterPosition;
                    self2.getOperationPropertiesForMultiSelection = getCenterMiddlePosition;
                    break;
                case 'right':
                    moveAttrName = 'left';
                    rightBottomPosition = 'right';
                    self2.setBaseDrawing = setRightBottomDrawing;
                    self2.getOperationPropertiesForMultiSelection = getRightBottomPosition;
                    break;
                case 'top':
                    moveAttrName = 'top';
                    self2.setBaseDrawing = setLeftTopDrawing;
                    self2.getOperationPropertiesForMultiSelection = getLeftTopPosition;
                    break;
                case 'middle':
                    moveAttrName = 'top';
                    rightBottomPosition = 'bottom';
                    self2.setBaseDrawing = setMiddlePosition;
                    self2.getOperationPropertiesForMultiSelection = getCenterMiddlePosition;
                    break;
                case 'bottom':
                    moveAttrName = 'top';
                    rightBottomPosition = 'bottom';
                    self2.setBaseDrawing = setRightBottomDrawing;
                    self2.getOperationPropertiesForMultiSelection = getRightBottomPosition;
                    break;
                default:
                    return false;
            }
        };

        // public methods -----------------------------------------------------

        /**
         * Get the name for the move operation attribute.
         * It's left or top.
         *
         * @returns {String} left or top
         */
        this.getMoveAttrName = function () {
            return moveAttrName;
        };

        /**
         * Set the drawing to check if the base object e.g. if it is the drawing with the most left position.
         * The function will be set on the init function.
         *
         * @param {Object} drawingSelect the drawing to get the base object.
         * @param {Object} drawing the drawing attributes of the given drawing object.
         */
        this.setBaseDrawing = $.noop;

        /**
         * Get the operation properties for the new position of the drawing, which
         * is a part of a multiselection.
         * The function will be set on the init function.
         *
         * @param {Object} drawingSelection the drawing to calculate the new Postition.
         *
         * @param {Number[]} startPosition
         *  The logical start position of this drawing.
         *
         * @returns {Object|null} the new operations to move the drawing or null if the drawing position is not changed,
         * this happens if the drawing is the base object.
         */
        this.getOperationPropertiesForMultiSelection = $.noop;

        /**
         * Get the operation properties for the new position of a single selected drawing.
         * The drawing will be alligned on the page.
         * The function will be set on the init function.
         *
         * @param {Object} drawingSelection the drawing to calculate the new Postition.
         *
         * @param {string} direction
         *
         * @returns {Object|null} the new operations to move the drawing or null if the drawing position is not changed,
         * this happens if the drawing is the base object.
         */
        this.getOperationPropertiesForSingleSelection = function (drawingSelection, direction) {
            var selection = self.getSelection(),
                drawingNode = selection.getDrawingNodeFromMultiSelection(drawingSelection),
                pageAttrs = self.pageStyles.getElementAttributes(self.getRootNode(self.getActiveSlideId())).page,
                startPosition;

            if (selection.isAdditionalTextframeSelection()) {
                startPosition = getOxoPosition(self.getCurrentRootNode(), drawingNode, 0);
            } else {
                startPosition = _.clone(drawingSelection.startPosition);
            }

            self2.setAlignmentDirection(direction);

            baseObject = {
                left: 0,
                right: pageAttrs.width,
                top: 0,
                bottom: pageAttrs.height,
                centerMiddle: null,
                object: { left: 0, top: 0, width: pageAttrs.width, height: pageAttrs.height }
            };

            return self2.getOperationPropertiesForMultiSelection(drawingSelection, startPosition);
        };
    }

    /** Single alignment object to calculate the new position of the selected drawings. */
    var changeAlignment = new ChangeAlignment();

    // public methods -----------------------------------------------------

    this.reformatDocument = function reformatDocument(newPageAttrs) {
        //globalLogger.info('+++ reformatDocument +++ [newPageAttrs] : ', newPageAttrs);
        var
            operationsDeferred,
            operationList,

            snapshot,

            generator     = self.createOperationGenerator(),
            recentAttrs   = self.getPageAttributes(),

            recentWidth   = recentAttrs.page.width,
            recentHeight  = recentAttrs.page.height,

            //orientation   = newPageAttrs.orientation,

            pageWidth     = _.isNumber(newPageAttrs.width) ? newPageAttrs.width : recentWidth, // 48537
            pageHeight    = _.isNumber(newPageAttrs.height) ? newPageAttrs.height : recentHeight,

            ratioDeltas   = {
                vertical:   (pageHeight / recentHeight),
                horizontal: (pageWidth / recentWidth)
            },
            //isPageOperationBeforeSlideOperations = (pageHeight < recentHeight),

            activeSlideId         = self.getActiveSlideId(),
            slideIdSet            = self.getSlideIdSet(activeSlideId),

            //activeStandardId      = slideIdSet.slideId,
            activeLayoutId        = slideIdSet.layoutId,
            activeMasterId        = slideIdSet.masterId,

            //$activeStandardSlide  = self.getSlideById(activeStandardId),
            $activeLayoutSlide    = self.getSlideById(activeLayoutId),
            $activeMasterSlide    = self.getSlideById(activeMasterId);

        //globalLogger.info('+++ reformatDocument +++ [recentWidth, recentHeight, recentAttrs] : ', recentWidth, recentHeight, recentAttrs);
        //globalLogger.info('+++ reformatDocument +++ [pageWidth, pageHeight, orientation] : ', pageWidth, pageHeight, orientation);
        //globalLogger.info('+++ reformatDocument +++ [isPageOperationBeforeSlideOperations] : ', isPageOperationBeforeSlideOperations);

        generator.generateOperation(CHANGE_CONFIG, { attrs: { page: newPageAttrs } });

        if ($activeMasterSlide) {
            reformatAnySlide({ model: self, generator, $slide: $activeMasterSlide, ratioDeltas, slideId: activeMasterId/*optional*/ });
        }

        if ($activeLayoutSlide) {
            reformatAnySlide({ model: self, generator, $slide: $activeLayoutSlide, ratioDeltas, slideId: activeLayoutId/*optional*/ });
        }

        // `true` flag for a deep clone of this slide-id-list.
        self.getMasterSlideOrder(true).forEach(function (slideId/*, idx, list*/) {
            var
                $slide = null;

            if (slideId !== activeLayoutId && slideId !== activeMasterId) { // not iterating twice over active layout or master slide
                $slide = self.getSlideById(slideId);
                //globalLogger.info('+++ reformatDocument +++ forEach slideId :: $slide : ', $slide);
                reformatAnySlide({ model: self, generator, $slide, ratioDeltas, slideId/*optional*/ });
            }
        });

        if (app.isODF()) {
            self.getStandardSlideOrder(true).forEach(function (slideId/*, idx, list*/) {
                var
                    $slide = self.getSlideById(slideId);

                //globalLogger.info('+++ reformatDocument +++ forEach slideId :: $slide : ', $slide);
                reformatAnySlide({ model: self, generator, $slide, ratioDeltas, slideId/*optional*/, isDocSlide: true });
            });
        }

        self.setBlockKeyboardEvent(true);

        snapshot            = new Snapshot(self);

        operationList       = generator.getOperations(); // operationList.reverse();
        operationsDeferred  = self.applyOperationsAsync(operationList);

        app.getView().enterBusy({

            cancelHandler() {
                if (operationsDeferred && operationsDeferred.abort) {

                    snapshot.apply();  // restoring the old state
                    app.enterBlockOperationsMode(function () { operationsDeferred.abort(); }); // block sending of operations
                }
            },
            immediate: true,

            warningLabel: /*#. shown while all recorded changes will be applied to the document */ gt('Applying changes, please wait...')
        });

        operationsDeferred.progress(function (progress) { // handle the result of change track operations.

            // update the progress bar according to progress of the operations promise.
            app.getView().updateBusyProgress(progress);

        }).always(function () {

            app.getView().leaveBusy();
            self.setBlockKeyboardEvent(false);

            //app.getModel().trigger('reformat:document:after', { slideRatio: (pageWidth / pageHeight) });
        });
    };

    /**
     * Inserting an image into a slide.
     *
     * @param {ImageDescriptor} imageDesc
     *  The object containing the image attributes. This are 'url' or 'substring'.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Number[]} [options.removeExisting=false]
     *   Whether an existing drawing at the specified position shall be removed. This can
     *   only be used, if a position was specified as option.
     *  @param {jQuery} [options.templateNode=undefined]
     *   The jQueryfied template node in a placeholder drawing.
     */
    this.insertImageURL = function (imageDesc, options) {

        var attrs = null; // the attributes for the image
        var def = null;
        var url = null;
        var name = null;

        if (!imageDesc) {
            def = this.createRejectedPromise();
        } else {

            attrs = { drawing: self.getInsertDrawingAttibutes(), image: {}, line: { type: 'none' } };
            url = (imageDesc.url) ? imageDesc.url : imageDesc;
            name = (imageDesc.name) ? imageDesc.name : null;
            applyImageTransformation(attrs.drawing, imageDesc.transform);

            if (isBase64(url)) {
                def = insertDataUrl(app, url); // resolve base64 to media URL
            } else if (isAbsoluteUrl(url)) {
                def = insertURL(app, url); // resolve external URl to media URL
            } else {
                def = $.when({ url, name: name || 'image' });
            }
        }

        def = def.then(function (result) {
            // media url and file name
            attrs.image.imageUrl = result.url;
            attrs.drawing.name = name || result.name;
            // image size
            return getImageSize(result.url);
        });

        def = def.then(function (size) {
            var // the deferred for inserting the image
                applyDef = self.createDeferred(),
                // whether the image was imported successfully
                result = false,
                // created operation
                newOperation = null,
                // target for operation - if exists
                target = self.getActiveTarget(),
                // the operations generator
                generator = self.createOperationGenerator(),
                // whether an existing drawing at the specified position shall be removed
                removeExisting = getBooleanOption(options, 'removeExisting', false),
                // the explicit attributes of a place holder drawing that will be replaced
                expAttrs = null,
                // the position of the inserted drawing
                position = null,
                // the position width and height of the text frame
                rectangle = null,
                // whether a place holder of type 'pic' shall be removed
                replacePicPlaceHoder = false,
                // the logical position of the drawing
                start = null,
                // a cropping object for inserting images into drawing place holders of type 'pic'
                cropAttrs = null,
                // a modified image size, if it necessary to reduce the size (content place holders)
                newSize = null,
                // a drawing object that describes a place holder drawing that will be replaced
                drawingObj = null,
                // selection before insert the image
                startSelection = self.getSelection().getStartPosition(),
                // the drawing node of a picture place holder node
                drawing = null;

            /**
             * Setting the cursor position (selecting the inserted drawing), after the image was loaded.
             * If the image needs long time to load and the user change the position the selection will not
             * changed to the image.
             */
            function setPostionAfterLoad() {
                if (_.isEqual(startSelection, self.getSelection().getStartPosition())) {
                    self.getSelection().setTextSelection(_.copy(start), increaseLastIndex(start));
                }
            }

            // exit silently if we lost the edit rights
            if (!app.isEditable()) {
                return applyDef.resolve();
            }

            // exit silently if the drawing or the slide with the picture placeholder was removed in the meantime
            if (removeExisting && options.templateNode) {
                drawing = options.templateNode.closest('.drawing');
                if (!drawing.length || !drawing.parent().length || !drawing.parent().parent().length) { // the drawing or the complete slide is no longer in the DOM
                    return applyDef.resolve();
                }
            }

            self.one('image:loaded', setPostionAfterLoad);

            self.getUndoManager().enterUndoGroup(function () {

                // the drawing attributes of the affected drawing node
                var drawingAttrs = null;

                if (removeExisting && options.templateNode && drawing.length) {
                    start = getOxoPosition(self.getRootNode(self.getActiveSlideId()), drawing, 0);
                    drawingAttrs = self.drawingStyles.getElementAttributes(drawing).drawing;
                    expAttrs = getExplicitAttributeSet(drawing);
                    rectangle = { left: drawingAttrs.left, top: drawingAttrs.top, width: drawingAttrs.width, height: drawingAttrs.height };
                } else if (!self.isMasterView() && !position) {
                    // finding a valid position for the image if not specified (but not in master view)
                    if (!self.isMasterView() && !position) {
                        drawingObj = self.getValidPlaceHolderPositionForInsertDrawing(IMAGE_PLACEHOLDER_TYPE);
                        if (drawingObj) {
                            start = drawingObj.position;
                            rectangle = drawingObj.rectangle;
                            expAttrs = drawingObj.attributes;
                            removeExisting = true; // removing an existing place holder drawing
                        }
                    }
                }

                if (!start) { start = self.getNextAvailablePositionInActiveSlide(); }

                // removing an existing drawing, if specified
                if (removeExisting) {
                    newOperation = { start: _.copy(start) };
                    self.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);

                    if (expAttrs) {

                        replacePicPlaceHoder = (getPlaceHolderDrawingType(null, expAttrs) === 'pic');

                        // expanding the drawing attributes with the explicit attributes of the existing place holder drawing
                        _.extend(attrs, expAttrs); // simply overwriting existing drawing attributes 'name', 'left' and 'top'
                        // ... but not sending phSize to the server
                        if (attrs.presentation && attrs.presentation.phSize) { delete attrs.presentation.phSize; }

                        // inserting an image into the image place holder requires a cropping of the drawing
                        if (replacePicPlaceHoder) {

                            cropAttrs = self.calculateCroppingValue(rectangle, size);

                            if (cropAttrs && cropAttrs.value) {
                                if (cropAttrs.isHorizontal) {
                                    attrs.image.cropLeft = cropAttrs.value;
                                    attrs.image.cropRight = cropAttrs.value;
                                } else {
                                    attrs.image.cropTop = cropAttrs.value;
                                    attrs.image.cropBottom = cropAttrs.value;
                                }
                            }
                        } else {
                            // in content body place holder the inserted image must not be larger than the drawing
                            newSize = self.checkImageSizeInDrawing(rectangle, size);
                            if (newSize) { size = newSize; }
                        }
                    }

                }

                // expanding the drawing attributes with the default position and size of the image
                // -> but not for drawing place holders of type 'pic'
                if (!replacePicPlaceHoder) {

                    _.extend(attrs.drawing, size); // adding width and height to the drawing attributes

                    if (rectangle) {
                        attrs.drawing.top = rectangle.top + (0.5 * (rectangle.height - attrs.drawing.height));
                        attrs.drawing.left = rectangle.left + (0.5 * (rectangle.width - attrs.drawing.width));
                    } else {
                        self.adaptImageSizeAndPositionToSlide(attrs.drawing); // adapting the drawing size to the slide
                    }
                }

                // no change of aspect ratio
                attrs.drawing.aspectLocked = true;

                // if a place holder drawing is replaced, the new drawing needs to get the noGroup attributes
                // if (removeExisting) { attrs.drawing.noGroup = true; }

                // creating the operation
                newOperation = { start: _.copy(start), type: 'image', attrs };
                self.extendPropertiesWithTarget(newOperation, target);
                insertAttribute(newOperation.attrs, 'drawing', 'id', self.getNewDrawingId());
                generator.generateOperation(INSERT_DRAWING, newOperation);

                result = self.applyOperations(generator);

            }); // enterUndoGroup()

            if (!result) {
                self.stopListeningTo(self, 'image:loaded', setPostionAfterLoad);

                // setting the cursor position (selecting the inserted drawing)
                self.getSelection().setTextSelection(_.copy(start), increaseLastIndex(start));
            }

            return result ? applyDef.resolve() : applyDef.reject();
        });

        def.fail(function () {
            app.rejectEditAttempt('image');
        });

        return def;
    };

    /**
     * Calculation the cropping that is required, so that an image fits into a specified drawing
     * without changing the ratio of width to height. If the drawing does not have the same ratio
     * of width-to-height as the image, the image need to be cropped on the left/right side or
     * the top/bottom side. Both sides shall be cropped by the same value in percent.
     *
     * @param {Object} drawingSize
     *  An object containing at least the properties 'width' and 'height' of the drawing, in that
     *  the image shall be inserted.
     *
     * @param {Object} imageSize
     *  An object containing at least the properties 'width' and 'height' of the image, that
     *  shall be inserted into a specified drawing.
     *
     * @returns {Object|Null}
     *  An object that contains a boolean value 'isHorizontal' that is set to true, if the left
     *  and right side of the image shall be cropped. If it is false, the top and bottom side
     *  of the image shall be cropped. Additionally a value in percent is specified. If drawing
     *  and image have the same height-to-width ratio, this value is 0 and no croppping is
     *  required.
     *  If the parameter are not sufficient for a calculation, null is returned.
     */
    this.calculateCroppingValue = function (drawingSize, imageSize) {

        if (!drawingSize || !imageSize || !_.isNumber(drawingSize.width) || !_.isNumber(drawingSize.height) || !_.isNumber(imageSize.width) || !_.isNumber(imageSize.height)) {
            return null;
        }

        var drawingRatio = drawingSize.height / drawingSize.width;
        var imageRatio = imageSize.height / imageSize.width;

        var horizontalCrop = drawingRatio > imageRatio;

        // calculating the percentage for left/right or top/bottom cropping
        // -> using 0.5 of 100%, because cropping is done on two sides (maximum is 50% on each side)
        var value = horizontalCrop ? round((1 - imageRatio / drawingRatio) * 50) : round((1 - drawingRatio / imageRatio) * 50);

        return { isHorizontal: horizontalCrop, value };
    };

    /**
     * If an image is inserted into a place holder drawing this is not of type 'pic', its size
     * must be reduced to the size of the drawing (it must be a content place holder drawing).
     * If the image is inserted into a 'pic' place holder, it is cropped.
     * The function calculates the reduced size of the drawing, if it does not fit into the
     * content place holder drawing.
     *
     * @param {Object} drawingSize
     *  An object containing at least the properties 'width' and 'height' of the drawing, in that
     *  the image shall be inserted.
     *
     * @param {Object} imageSize
     *  An object containing at least the properties 'width' and 'height' of the image, that
     *  shall be inserted into a specified drawing.
     *
     * @returns {Object|Null}
     *  An object that contains a boolean values 'width' and 'height' for the image size, if the
     *  specified image size must be reduced. If it is not necessary to reduce the image size,
     *  null is returned.
     */
    this.checkImageSizeInDrawing = function (drawingSize, imageSize) {

        var newImageSize = null;

        if (!drawingSize || !imageSize || !_.isNumber(drawingSize.width) || !_.isNumber(drawingSize.height) || !_.isNumber(imageSize.width) || !_.isNumber(imageSize.height)) {
            return null;
        }

        if ((imageSize.width > drawingSize.width) || (imageSize.height > drawingSize.height)) {

            var horzFactor = imageSize.width / drawingSize.width;
            var vertFactor = imageSize.height / drawingSize.height;
            var factor = Math.max(horzFactor, vertFactor);

            newImageSize = { width: round(imageSize.width / factor), height: round(imageSize.height / factor) };
        }

        return newImageSize;
    };

    /**
     * Preparations for inserting a text frame. A selection box can be opened
     * so that the user can determine the position of the text frame.
     */
    this.insertTextFramePreparation = function () {

        var // the active selection box
            selectionBox = self.getSelectionBox(),
            // the name of the text frame mode
            textFrameMode = 'textframe',
            // the stop handler for the text frame mode
            stopHandler = function (rect) {
                var options = {
                    // converting the pixel relative to app content root node in 1/100 mm relative to slide
                    rectangle: convertAppContentBoxToOperationBox(app, rect)
                };
                // fix for Bug 48882
                if (options.rectangle.width < 300) { options.wordWrap = false; }

                // inserting the new text frame
                self.insertTextFrame(options);
                // reset to the previous selection box mode
                selectionBox.setPreviousMode();
            };

        if (self.isInsertTextFrameActive()) {
            selectionBox.cancelSelectionBox();
            return false;
        }
        if (self.isInsertShapeActive()) { selectionBox.cancelSelectionBox(); } // if shape insertion is active, don't return false, just clean-up

        // registering the mode for text frame insertion (if not already done)
        if (!selectionBox.isModeRegistered(textFrameMode)) {
            selectionBox.registerSelectionBoxMode(textFrameMode, { contentRootClass: 'textframemode', preventScrolling: true, leaveModeOnCancel: true, stopHandler });
        }

        // activating the mode for text frame insertion
        selectionBox.setActiveMode(textFrameMode);

        return true;
    };

    /**
     * Whether the 'textframe' mode is the active mode.
     *
     * @returns {Boolean}
     *  Whether the 'textframe' mode is the active mode.
     */
    this.isInsertTextFrameActive = function () {
        return self.getSelectionBox() && self.getSelectionBox().getActiveMode() === 'textframe';
    };

    /**
     * Whether the 'shape' mode is the active mode.
     *
     * @returns {Boolean}
     *  Whether the 'shape' mode is the active mode.
     */
    this.isInsertShapeActive = function () {
        return self.getSelectionBox() && self.getSelectionBox().getActiveMode() === 'shape';
    };

    /**
     * Inserting a text frame element with a default size. This is useful for
     * keyboard handling.
     *
     * @param {Object} [options]
     *  For Optional parameters see insertTextFrame.
     *
     * @returns {jQuery.Promise}
     *  A promise that is already resolved.
     */
    this.insertTextFrameWithDefaultBox = function (options) {
        return self.insertTextFrame({ ...options, rectangle: self.getDefaultRectangleForInsert({ type: 'textframe' }) });
    };

    /**
     * Inserting a shape with a default size. This is useful for keyboard handling.
     *
     * @param {String} presetShapeId
     *  The identifier for the shape.
     *
     * @param {Object} [options]
     *  For Optional parameters see insertShape.
     *
     * @returns {undefined}
     *  The return value of 'self.insertShape'.
     */
    this.insertShapeWithDefaultBox = function (presetShapeId, options) {
        var rectangle = this.getDefaultRectangleForInsert({ type: 'shape', presetShapeId });
        return self.insertShape(presetShapeId, rectangle, options);
    };

    /**
     * Inserting a text frame drawing node of type 'shape' or inserting a table drawing.
     * This insertion can be done via a selection box (text frame), via the table size
     * dialog or via one of the buttons in a place holder drawing.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.insertTable=false]
     *   Whether a text frame containing a table shall be inserted.
     *  @param {Object} [options.size=null]
     *   The size of the table to be inserted. This is only evaluated, if the option
     *   'insertTable' is true. The size object must contain the two properties
     *   'width' and 'height'.
     *  @param {Object} [options.rectangle=null]
     *   A user defined rectangle containing the properties 'top', 'left', 'width'
     *   and 'height' in 1/100 mm.
     *  @param {Object} [options.attributes=null]
     *   The explicit attributes of the drawing to be replaced. This will only be
     *   evaluated, if the option 'removeExisting' is set to true and if 'insertTable'
     *   is set to true.
     *  @param {Number[]} [options.position=null]
     *   The logical position at that the text frame drawing shall be inserted.
     *  @param {Number[]} [options.removeExisting=false]
     *   Whether an existing drawing at the specified position shall be removed. This can
     *   only be used, if a position was specified as option.
     *  @param {Boolean} [options.selectDrawingOnly=false]
     *   If the inserted drawing is selected or if the selection is a text
     *   selection inside the drawing.
     *
     * @returns {jQuery.Promise}
     *  A promise of the call back function. This is already resolved.
     */
    this.insertTextFrame = function (options) {

        // the undo manager returns the return value of the callback function
        return self.getUndoManager().enterUndoGroup(function () {

            // helper function, that generates the operations
            function doInsertTextFrame() {

                var // the operations generator
                    generator = self.createOperationGenerator(),
                    // the default width of the text frame in 1/100 mm
                    defaultWidth = 8000,
                    // whether a paragraph or a table shall be included into the text frame
                    insertTable = getBooleanOption(options, 'insertTable', false),
                    // the table size
                    size = getObjectOption(options, 'size', null),
                    // the position width and height of the text frame
                    userBox = getObjectOption(options, 'rectangle', null),
                    // the logical position at which the drawing shall be inserted
                    startPos = getArrayOption(options, 'position', null),
                    // the explicit attributes of a place holder drawing that will be replaced
                    expAttrs = getObjectOption(options, 'attributes', null),
                    // the current cursor position
                    start = startPos || self.getNextAvailablePositionInActiveSlide(),
                    // whether an existing drawing at the specified position shall be removed (position must be specified)
                    removeExisting = startPos && getBooleanOption(options, 'removeExisting', false),
                    // the default drawing attributes
                    drawingAttrs = extendOptions({ width: defaultWidth }, self.getInsertDrawingAttibutes()),
                    // the default border attributes
                    lineAttrs = { color: Color.BLACK, style: 'single', type: 'none', width: getWidthForPreset('thin') },
                    // the default fill color attributes (pptx filter requires no background, the default in PP)
                    fillAttrs = { type: 'none' },
                    // the default geometry attributes
                    geometryAttrs = { presetShape: 'rect', avList: {} },
                    // wordWrap for shape attributes
                    wordWrap = getBooleanOption(options, 'wordWrap', true),
                    // the default attributes
                    attrs = { drawing: drawingAttrs, shape: { autoResizeHeight: true, noAutoResize: false, wordWrap }, line: lineAttrs, fill: fillAttrs, geometry: geometryAttrs },
                    // the position of the first paragraph inside the text frame
                    paraPos = _.clone(start),
                    // a required style ID for style handling
                    parentStyleId = null,
                    // the attributes of the inserted table
                    tableAttributes = null,
                    // default table style
                    tableStyleId = null,
                    // the text position
                    textPos = null,
                    // target for operation - if exists
                    target = self.getActiveTarget(),
                    // a drawing object that describes a place holder drawing that will be replaced
                    drawingObj = null,
                    // created operation
                    newOperation = null,
                    // if the drawing is selected or if the selection is inside the drawing after insert
                    selectDrawingOnly = getBooleanOption(options, 'selectDrawingOnly', false);

                // finding a valid position for the table if not specified (but not in master view)
                if (insertTable && !self.isMasterView()) {
                    if (!userBox && !startPos) {
                        // finding a valid position for the table if not specified (but not in master view)
                        drawingObj = self.getValidPlaceHolderPositionForInsertDrawing(TABLE_PLACEHOLDER_TYPE);
                        if (drawingObj) {
                            start = drawingObj.position;
                            userBox = drawingObj.rectangle;
                            expAttrs = drawingObj.attributes;
                            paraPos = _.clone(start);
                            removeExisting = true; // removing an existing place holder drawing
                        }
                    }

                    if (removeExisting && expAttrs) {
                        // the drawing attributes must be set to the table, rotation must be removed
                        expAttrs.drawing = expAttrs.drawing || {};
                        expAttrs.drawing.rotation = 0; // no rotation for tables
                        // expanding the drawing attributes with the explicit attributes of the existing place holder drawing
                        _.extend(attrs, expAttrs);
                        // ... but not sending phSize to the server
                        if (attrs.presentation && attrs.presentation.phSize) { delete attrs.presentation.phSize; }
                    }
                }

                // overwriting default attribute values for left, top, width and height with user defined values in option 'box'.
                if (userBox) { _.extend(attrs.drawing, userBox); }

                // removing an existing drawing, if specified
                if (removeExisting) {
                    newOperation = { start: _.copy(start) };
                    self.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);
                }

                // Adding styleId (only for ODT, but not for tables)
                if (app.isODF() && !insertTable) { attrs.styleId = 'Frame'; }

                // handling the required styles for the text frame
                if (_.isString(attrs.styleId) && self.drawingStyles.isDirty(attrs.styleId)) {

                    // checking the parent style of the specified style
                    parentStyleId = self.drawingStyles.getParentId(attrs.styleId);

                    if (_.isString(parentStyleId) && self.drawingStyles.isDirty(parentStyleId)) {
                        // inserting parent of text frame style to document
                        self.generateInsertStyleOp(generator, 'drawing', parentStyleId, true);
                        self.drawingStyles.setDirty(parentStyleId, false);
                    }

                    // insert text frame style to document
                    self.generateInsertStyleOp(generator, 'drawing', attrs.styleId);
                }

                // position of the paragraph/row inside the text frame
                paraPos.push(0);

                if (insertTable) {

                    tableAttributes = { tableGrid: [], width: 'auto' };
                    tableStyleId = self.getDefaultUITableStylesheet();

                    // prepare table column widths (values are relative to each other)
                    _(size.width).times(function () { tableAttributes.tableGrid.push(1000); });

                    // set default table style
                    if (_.isString(tableStyleId)) {

                        // insert a pending table style if needed
                        checkForLateralTableStyle(generator, self, tableStyleId);

                        // add table style name to attributes
                        attrs.styleId = tableStyleId;
                        // default: tables do not have first column, last column, last row and vertical bands
                        tableAttributes.exclude = ['bandsVert', 'firstCol', 'lastCol', 'lastRow'];
                    }

                    attrs.table = tableAttributes;
                    attrs.drawing.noGroup = true; // table cannot be grouped

                    // text frames are drawings of type shape
                    newOperation = { attrs, start: _.copy(start), type: 'table' };
                    self.extendPropertiesWithTarget(newOperation, target);
                    insertAttribute(newOperation.attrs, 'drawing', 'id', self.getNewDrawingId());
                    generator.generateOperation(INSERT_DRAWING, newOperation);  // this includes insertion of a table node

                    // inserting the rows without inserting a table via operation

                    newOperation = { start: _.clone(paraPos), count: size.height, insertDefaultCells: true, attrs: { row: { height: 1500 } } }; // TODO: 1500 fix?
                    self.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(ROWS_INSERT, newOperation);

                    textPos = _.clone(paraPos).concat([0, 0, 0]); // adding position of cell, paragraph and text

                } else {

                    // text frames are drawings of type shape
                    newOperation = { attrs, start: _.copy(start), type: 'shape' };
                    self.extendPropertiesWithTarget(newOperation, target);
                    insertAttribute(newOperation.attrs, 'drawing', 'id', self.getNewDrawingId());
                    generator.generateOperation(INSERT_DRAWING, newOperation);

                    // add a paragraph into the shape, so that the cursor can be set into the text frame
                    // -> an operation is required for this step, so that remote clients are also prepared (-> no implicit paragraph)
                    newOperation = { start: _.copy(paraPos) };
                    self.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(PARA_INSERT, newOperation);

                    // reducing distance of paragraphs inside the text frame
                    newOperation = { start: _.copy(paraPos), attrs: { paragraph: { marginBottom: 0 } } };
                    if (app.isODF()) {
                        newOperation.attrs.paragraph.bullet = { type: 'none' }; // no bullets in new text frames
                        newOperation.attrs.paragraph.indentFirstLine = 0;
                        newOperation.attrs.paragraph.indentLeft = 0;
                    }

                    // workaround for Bug 52977
                    if (SMALL_DEVICE) { newOperation.attrs.character = { fontSize: 32 }; }

                    self.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(SET_ATTRIBUTES, newOperation);

                    textPos = appendNewIndex(_.clone(paraPos), 0);
                }

                // apply all collected operations
                self.applyOperations(generator);

                if (selectDrawingOnly) {
                    // select the drawing only
                    self.getSelection().setTextSelection(start, increaseLastIndex(start));
                } else {
                    // setting cursor into text frame
                    self.getSelection().setTextSelection(textPos);
                }
            }

            doInsertTextFrame();
            return $.when();

        }, this);  // enterUndoGroup()
    };

    /**
     * Preparations for inserting a shape. A selection box can be opened
     * so that the user can determine the position of the shape.
     *
     * @param {String} presetShapeId
     *  The identifier for the shape.
     *
     * @returns {Boolean}
     *  Whether a selection box could be activated.
     */
    this.insertShapePreparation = function (presetShapeId) {

        var selectionBox = self.getSelectionBox();
        var shapeMode = 'shape';
        var canvasDiv = $('<div style="position:absolute;z-index:99">');
        var contentRootNode = app.getView().getContentRootNode();
        var contentRootOffset = contentRootNode.offset();
        var contentHeight = contentRootNode.children(APPCONTENT_NODE_SELECTOR).outerHeight() + 100;
        var vertStartLine = $('<div class="guide-line v"></div>').height(contentHeight);
        var horzStartLine = $('<div class="guide-line h"></div>');
        var vertEndLine = $('<div class="guide-line v"></div>').height(contentHeight);
        var horzEndLine = $('<div class="guide-line h"></div>');
        // special behavior for line/connector shapes
        var twoPointMode = isPresetConnectorId(presetShapeId);
        // custom aspect ratio for special shapes
        var aspectRatio = getPresetAspectRatio(presetShapeId);
        var activeSlide = self.getSlideById(self.getActiveSlideId());
        var zoomFactor = app.getView().getZoomFactor();
        // TODO
        var drawingRectCollection = [];
        var slideOffset = null;
        var startX = 0;
        var startY = 0;
        var activeSnapContainer = null;
        var startSnapNode = null;
        var endSnapNode = null;

        function initGuidelinesHandler(event) {
            var eventPos = selectionBox.getEventPosition(event, (event.type === 'touchmove') ? 'touch' : 'mouse');
            if (eventPos) {
                var startX = eventPos.x + contentRootNode.scrollLeft() - contentRootOffset.left;
                var startY = eventPos.y + contentRootNode.scrollTop() - contentRootOffset.top;
                horzStartLine.css('top', startY);
                vertStartLine.css('left', startX);
            }
        }

        function initConnectorMode(event) {
            var currentPoint = { x: (event.pageX - slideOffset.left), y: (event.pageY - slideOffset.top) };
            clearSnapPoints();
            _.each(drawingRectCollection, function (rectObj) {
                var isInside = pointInsideRect(currentPoint, rectObj.rect);
                var snapPointsContainer = $(rectObj.node).children('.snap-points-container').toggleClass('connector-hover', isInside);
                if (isInside) {
                    clearSnapPoints();
                    activeSnapContainer = snapPointsContainer;
                    // scale points according to zoom
                    snapPointsContainer.find('.snap-point').css('transform', 'scale(' + (1 / zoomFactor) + ')');
                }
            });
        }

        function clearGuidelines() {
            contentRootNode.children('.guide-line').remove();
        }

        function clearSnapPoints() {
            if (activeSnapContainer) {
                activeSnapContainer.removeClass('connector-hover');
                activeSnapContainer = null;
            }
        }

        function clearInsertShapePreviewHandler() {
            clearGuidelines();
            clearSnapPoints();
            self.stopListeningTo(contentRootNode, 'mousemove touchmove', initGuidelinesHandler);
            self.stopListeningTo(contentRootNode, 'mousemove touchmove', initConnectorMode);
        }

        // returns the flipping options for connector/line shapes
        function getFlippingOptions(frameRect) {
            return twoPointMode ? { flipH: frameRect.reverseX, flipV: frameRect.reverseY } : null;
        }

        function startHandler(frameRect) {
            contentRootNode.append(canvasDiv, vertEndLine, horzEndLine);
            self.stopListeningTo(contentRootNode, 'mousemove touchmove', initGuidelinesHandler);
            if (activeSnapContainer) {
                _.each(activeSnapContainer.children(), function (snapPointNode) {
                    var snapPointRect = snapPointNode.getBoundingClientRect();
                    var snapPoint = {
                        x: snapPointRect.left + snapPointRect.width / 2 + contentRootNode.scrollLeft() - contentRootOffset.left,
                        y: snapPointRect.top + snapPointRect.height / 2 + contentRootNode.scrollTop() - contentRootOffset.top
                    };
                    var point = { x: frameRect.left, y: frameRect.top };

                    if (pointCloseToSnapPoint(point, snapPoint)) {
                        // re-initialize deltas with new values of snap points
                        //console.warn('close to snap point');
                        startSnapNode = snapPointNode;
                        startX = snapPoint.x;
                        startY = snapPoint.y;
                        frameRect.left = startX;
                        frameRect.top = startY;
                    }
                });
            }
            moveHandler(frameRect);
        }

        function moveHandler(frameRect) {
            if (startX) { frameRect.left = frameRect.reverseX ? startX - frameRect.width : startX; }
            if (startY) { frameRect.top = frameRect.reverseY ? startY - frameRect.height : startY; }

            if (!_.isUndefined(frameRect.origShift) && frameRect.origShift > 3 && activeSnapContainer) {
                endSnapNode = null; // reset the node holder
                _.each(activeSnapContainer.children(), function (snapPointNode) {
                    var snapPointRect = snapPointNode.getBoundingClientRect();
                    var snapPoint = {
                        x: snapPointRect.left + snapPointRect.width / 2 + contentRootNode.scrollLeft() - contentRootOffset.left,
                        y: snapPointRect.top + snapPointRect.height / 2 + contentRootNode.scrollTop() - contentRootOffset.top
                    };
                    var point = { x: frameRect.reverseX ? frameRect.left : (frameRect.left + frameRect.width), y: frameRect.reverseY ? frameRect.top : (frameRect.top + frameRect.height) };

                    if (pointCloseToSnapPoint(point, snapPoint)) {
                        endSnapNode = snapPointNode;
                        // re-initialize deltas with new values of snap points
                        if (frameRect.reverseX) {
                            frameRect.width += frameRect.left - snapPoint.x;
                            frameRect.left = snapPoint.x;
                        } else {
                            frameRect.width = snapPoint.x - frameRect.left;
                        }
                        if (frameRect.reverseY) {
                            frameRect.height += frameRect.top - snapPoint.y;
                            frameRect.top = snapPoint.y;
                        } else {
                            frameRect.height = snapPoint.y - frameRect.top;
                        }
                        frameRect.width = Math.max(1, frameRect.width);
                        frameRect.height = Math.max(1, frameRect.height);
                    }
                });
            }

            canvasDiv.css({ left: frameRect.left, top: frameRect.top });
            horzStartLine.css('top', frameRect.top);
            vertStartLine.css('left', frameRect.left);
            vertEndLine.css('left', frameRect.right());
            horzEndLine.css('top', frameRect.bottom());
            var options = _.extend({ transparent: true }, getFlippingOptions(frameRect));
            drawPreviewShape(app, canvasDiv, frameRect, presetShapeId, options);
        }

        function stopHandler(frameRect) {

            // the options for the insertShape operation
            var insertShapeOptions = getFlippingOptions(frameRect);
            var connectorOptions = { connector: {} };

            // use a defaultBox when only a very small or no area is defined, e.g. when the user just clicks on the slide
            if (frameRect.area() === 0) {
                frameRect = self.getDefaultRectangleForInsert({ type: 'shape', presetShapeId }, { insertPosTop: frameRect.top, insertPosLeft: frameRect.left });
            } else {
                // converting the pixel relative to app content root node in 1/100 mm relative to slide
                frameRect = convertAppContentBoxToOperationBox(app, frameRect);
            }

            if (twoPointMode) { // add connector properties
                _.each([startSnapNode, endSnapNode], function (refNode) {
                    if (refNode) {
                        var isStart = refNode === startSnapNode;
                        var refIndStr = isStart ? 'startIndex' : 'endIndex';
                        var refIdStr = isStart ? 'startId' : 'endId';
                        var refInd = parseInt($(refNode).data('cxnInd'), 10);
                        var drawingAttrs = self.getDrawingAttrsByNode(getDrawingNode(refNode));
                        var refId = (drawingAttrs && drawingAttrs.id) || null;
                        if (_.isNumber(refInd)) { connectorOptions.connector[refIndStr] = refInd; }
                        if (refId) {
                            connectorOptions.connector[refIdStr] = refId;
                        } else {
                            globalLogger.error('ObjectOperationMixin.insertShapePreparation(): missing id for drawing: ', getDrawingNode(refNode));
                        }
                    }
                });

                insertShapeOptions = _.extend(insertShapeOptions, connectorOptions);
            }
            // insert the new shape object
            self.insertShape(presetShapeId, frameRect, insertShapeOptions);
        }

        function finalizeHandler() {
            self.executeDelayed(function () {
                canvasDiv.remove();
                clearGuidelines();
                clearSnapPoints();
                self.stopListeningTo(contentRootNode, 'mousemove touchmove', initConnectorMode);
            }, 50);
            // reset to the previous selection box mode
            selectionBox.setPreviousMode();
        }

        self.getSelection().setSlideSelection();

        if (self.isInsertShapeActive()) {
            selectionBox.cancelSelectionBox();
            clearGuidelines();
            return false;
        }

        contentRootNode.append(vertStartLine, horzStartLine);
        self.listenTo(contentRootNode, 'mousemove touchmove', initGuidelinesHandler);
        if (twoPointMode) {
            slideOffset = activeSlide.offset();
            drawingRectCollection = self.getAllDrawingRectsOnSlide(activeSlide, slideOffset, zoomFactor);
            self.listenTo(contentRootNode, 'mousemove touchmove', initConnectorMode);
        }

        // always registering the mode for text shape insertion
        selectionBox.registerSelectionBoxMode(shapeMode, {
            contentRootClass: 'textframemode',
            preventScrolling: true,
            leaveModeOnCancel: true,
            trackingOptions: { twoPointMode, aspectRatio },
            stopHandler,
            startHandler,
            moveHandler,
            finalizeHandler,
            clearGuidelinesHandler: clearInsertShapePreviewHandler
        });

        // activating the mode for text frame insertion
        selectionBox.setActiveMode(shapeMode);

        return true;
    };

    /**
     * Inserting a shape element at the end of the slide (logical position).
     *
     * @param {String} presetShapeId
     *  A string specifying a default shape object.
     *
     * @param {Object} rectangle
     *  A user defined rectangle in 1/100 mm.
     *
     * @param {Object} [options]
     *  Optional parameters for the default formatting attributes. Will be
     *  passed to the method DrawingUtils.createDefaultShapeAttributeSet().
     *  Additionally, the following options are supported:
     *  - {Boolean} [options.selectDrawingOnly=false]
     *      If the inserted drawing is selected or if the selection is a
     *      text selection inside the drawing.
     */
    this.insertShape = function (presetShapeId, rectangle, options) {

        var target = self.getActiveTarget();
        var start = self.getNextAvailablePositionInActiveSlide();
        var paraPos = _.copy(start);
        var paraInsert = null;
        paraPos.push(0);

        var attrOptions = _.extend({ targets: self.getThemeTargets() }, options);
        var attrSet = createDefaultShapeAttributeSet(self, presetShapeId, attrOptions);
        insertAttribute(attrSet, 'drawing', 'name', presetShapeId);
        insertAttribute(attrSet, 'drawing', 'id', self.getNewDrawingId());
        _.extend(attrSet.drawing, rectangle);
        attrSet.shape = { anchor: 'centered', anchorCentered: false, wordWrap: true, horzOverflow: 'overflow', vertOverflow: 'overflow' };
        attrSet.shape.paddingLeft = attrSet.shape.paddingRight = round(rectangle.width / 10);
        attrSet.shape.paddingTop = attrSet.shape.paddingBottom = round(rectangle.height / 10);

        // whether the shapes can contain text (in ODF: connectors can contain text too)
        var shapeHasText = app.isODF() || isTextShapeId(presetShapeId);

        var operationOptions = {};
        operationOptions.attrs = attrSet;
        operationOptions.start = _.copy(start);
        operationOptions.type = getPresetShape(presetShapeId).type;
        self.extendPropertiesWithTarget(operationOptions, target);

        var generator = self.createOperationGenerator();
        generator.generateOperation(INSERT_DRAWING, operationOptions);

        var setAttrs = { attrs: { shape: { anchor: 'centered' } }, start: _.copy(start) };
        self.extendPropertiesWithTarget(setAttrs, target);
        generator.generateOperation(SET_ATTRIBUTES, setAttrs);

        if (shapeHasText) {
            paraInsert = { start: _.copy(paraPos), attrs: _.extend({ character: attrSet.character }, getParagraphAttrsForDefaultShape(self)) };
            if (app.isODF()) {
                paraInsert.attrs.paragraph = paraInsert.attrs.paragraph || {};
                paraInsert.attrs.paragraph.bullet = { type: 'none' }; // no bullets in new shapes
                paraInsert.attrs.paragraph.indentFirstLine = 0;
                paraInsert.attrs.paragraph.indentLeft = 0;
            }
            self.extendPropertiesWithTarget(paraInsert, target);
            generator.generateOperation(PARA_INSERT, paraInsert);
        }

        this.applyOperations(generator);

        if (!shapeHasText || getBooleanOption(options, 'selectDrawingOnly', false)) {
            // select the drawing only
            self.getSelection().setTextSelection(start, increaseLastIndex(start));
        } else {
            // setting cursor into text frame
            // in IE 11 there is a native browser selection visible -> give browser the chance to render drawing (DOCS-2355)
            if (_.browser.IE && _.browser.IE < 12) {
                self.executeDelayed(function () { self.getSelection().setTextSelection(appendNewIndex(_.clone(paraPos), 0)); }, 0);
            } else {
                self.getSelection().setTextSelection(appendNewIndex(_.clone(paraPos), 0));
            }
        }
    };

    /**
     * Preparation function for inserting placeholder drawing on master and layout slides.
     *
     * @param {String} value
     *  Type of placeholder drawing object to be inserted
     *
     * @param {Object} [inputOptions]
     *  @param {String} [inputOptions.sourceType='']
     *      If function was triggered by keyboard or mouse.
     */
    this.insertPlaceholderDrawingPreparation = function (value, inputOptions) {
        if (getStringOption(inputOptions, 'sourceType') === 'keyboard') {
            return self.insertPlaceholderDrawing(value);
        } else if (self.getSlideTouchMode()) {
            return self.insertPlaceholderDrawing(value, { selectDrawingOnly: true });
        } else {
            var // the active selection box
                selectionBox = self.getSelectionBox(),
                // the name of the text frame mode
                textFrameMode = 'placeholder-frame',
                // value selected from dropdown menu needs to be saved, because it will be overwritten in stopHandler closure
                selectedValue = value,
                // the stop handler for the text frame mode
                stopHandler = function (rect) {

                    var options = {
                        // use a defaultBox when only a very small or no area is defined, e.g. when the user just clicks on the slide,
                        // otherwise convert the pixel relative to app content root node in 1/100 mm relative to slide
                        rectangle: (rect.area() === 0) ? self.getDefaultRectangleForInsert({ type: 'placeholder' }, { insertPosTop: rect.top, insertPosLeft: rect.left }) : convertAppContentBoxToOperationBox(app, rect)
                    };

                    // inserting the new text frame
                    self.insertPlaceholderDrawing(selectedValue, options);
                    // reset to the previous selection box mode
                    selectionBox.setPreviousMode();
                };

            if (self.isInsertTextFrameActive()) {
                selectionBox.cancelSelectionBox();
                return false;
            }

            selectionBox.registerSelectionBoxMode(textFrameMode, { contentRootClass: 'textframemode', preventScrolling: true, leaveModeOnCancel: true, stopHandler });

            // activating the mode for text frame insertion
            selectionBox.setActiveMode(textFrameMode);

            return true;
        }
    };

    /**
     * Inserts selected type of placeholder drawing object on the currently active layout slide.
     *
     * @param {String} selectedType
     *  Selected type of placeholder drawing object.
     *
     * @param {Object} [options]
     *  @param {Object} [options.rectangle=null]
     *   A user defined rectangle containing the properties 'top', 'left', 'width'
     *   and 'height' in 1/100 mm.
     */
    this.insertPlaceholderDrawing = function (selectedType, options) {

        var textLabels = _.clone(PLACEHOLDER_TEXT_CONTENT); // needs to be cloned, because array is modified later
        var nameLabels =  PLACEHOLDER_DRAWING_NAMES;
        var rectangleProperties = getObjectOption(options, 'rectangle', self.getDefaultRectangleForInsert({ type: 'placeholder' }));
        var target = self.getActiveTarget();
        var start = self.getNextAvailablePositionInActiveSlide();
        var paraPos = _.clone(start);
        var paraInsert, textPos, textInsert;
        var generator = self.createOperationGenerator();
        var operationOptions = {};
        var drawing = { name: nameLabels[selectedType], noGroup: true };
        var phIndex = self.drawingStyles.getHighestPlaceholderIndexForId(target) + 1;
        var presentation = { phIndex };

        if (selectedType === 'text' || selectedType === 'textv') { presentation.phType = 'body'; }
        if (selectedType === 'picture') { presentation.phType = 'pic'; }
        if (selectedType === 'table') { presentation.phType = 'tbl'; }

        if (rectangleProperties) { _.extend(drawing, rectangleProperties); }

        operationOptions.attrs = { drawing, presentation };
        if (selectedType === 'contentv' || selectedType === 'textv') { operationOptions.attrs.shape = { vert: 'eaVert' }; }
        operationOptions.start = start;
        operationOptions.type = 'shape';

        self.extendPropertiesWithTarget(operationOptions, target);
        insertAttribute(operationOptions.attrs, 'drawing', 'id', self.getNewDrawingId());
        generator.generateOperation(INSERT_DRAWING, operationOptions);

        paraPos.push(0);
        paraInsert = { start: _.copy(paraPos) };
        if (selectedType !== 'picture' && selectedType !== 'table') { paraInsert.attrs = { paragraph: { level: 0 } }; }
        self.extendPropertiesWithTarget(paraInsert, target);
        generator.generateOperation(PARA_INSERT, paraInsert);

        if (selectedType !== 'picture' && selectedType !== 'table') {
            textPos = _.clone(paraPos);
            textPos.push(0);
            textInsert = { start: _.copy(textPos), text: textLabels.shift() };
            self.extendPropertiesWithTarget(textInsert, target);
            generator.generateOperation(TEXT_INSERT, textInsert);

            _.each(textLabels, function (textLabel, index) {
                paraPos = increaseLastIndex(paraPos);
                paraInsert = { start: _.copy(paraPos) };
                paraInsert.attrs = { paragraph: { level: index + 1 } };
                self.extendPropertiesWithTarget(paraInsert, target);
                generator.generateOperation(PARA_INSERT, paraInsert);

                textPos = _.clone(paraPos);
                textPos.push(0);
                textInsert = { start: _.copy(textPos), text: textLabel };
                self.extendPropertiesWithTarget(textInsert, target);
                generator.generateOperation(TEXT_INSERT, textInsert);
            });
        }

        this.applyOperations(generator);

        // selecting the new inserted drawing (not the text inside)
        self.getSelection().setTextSelection(start, increaseLastIndex(start));
    };

    /**
     * Method that checks if given type of placeholder object is existing on active slide.
     *
     * @param {String} type
     *  Type of the placeholder object to be checked for existence on active slide.
     */
    this.hasPlaceholderObjectOnSlide = function (type) {
        // called from controller getter, very often
        // performance: not needed to execute on standard slides
        if (!self.isMasterView()) { return; }

        var foundObjects = [];
        var activeSlideId = self.getActiveSlideId();
        var isMasterSlideId = self.isMasterSlideId(activeSlideId);

        switch (type) {
            case 'title':
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);
                if (!foundObjects.length && !isMasterSlideId) {
                    foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, self.drawingStyles.getListOfAllowedDrwawingTypeSwitches()[type]);
                }
                break;
            case 'body':
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);
                break;
            case 'dt':
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);
                break;
            case 'ftr':
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);
                break;
            case 'sldNum':
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);
                break;
            case 'footers':
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, self.getPresentationFooterTypes());
                break;
        }
        return foundObjects.length > 0;
    };

    /**
     * Toggles standard placeholders like title and footers on layout and master slides.
     *
     * @param {String} type
     *  Type of the placeholder object to be toggled on active slide.
     *  Can be 'footers', 'title', or 'body' (only on master slide).
     */
    this.togglePlaceholderObjectOnSlide = function (type) {

        var foundObjects = [];
        var notFoundFtrTypes = [];
        var activeSlideId = self.getActiveSlideId();
        var isMasterSlideId = self.isMasterSlideId(activeSlideId);
        var masterSlideId = self.getMasterSlideId(activeSlideId);
        var footerTypes = self.getPresentationFooterTypes();
        var operationOptions = {};
        var deleteOperations = [];
        var generator = self.createOperationGenerator();
        var target = self.getActiveTarget();
        var start = self.getNextAvailablePositionInActiveSlide();
        var paraPos = null;
        var textPos = null;
        var phIndex = self.drawingStyles.getHighestPlaceholderIndexForId(activeSlideId) + 1;
        var textLabels = _.clone(PLACEHOLDER_TEXT_CONTENT); // needs to be cloned, because array is modified later
        var titleLabel = PLACEHOLDER_TITLE_CONTENT;
        var paraInsert, textInsert;
        var foundFooterObj = null;
        var fieldPos = null;
        var paragraphAlignment = '';
        var fieldType = '';
        var fieldRepresentation = '';

        if (app.isODF() && _.contains(footerTypes, type)) {

            foundFooterObj = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);

            if (foundFooterObj.length > 0) {

                operationOptions = { start: getOxoPosition(self.getRootNode(activeSlideId), foundFooterObj, 0), target };
                deleteOperations.push(operationOptions);
                self.handlePlaceHolderAttributeTransfer(foundFooterObj, activeSlideId, generator);
                generator.generateOperation(DELETE, operationOptions);

            } else {

                if (type === 'dt') {
                    paragraphAlignment = 'left';
                    fieldType = 'date-time';
                    fieldRepresentation = '';
                } else if (type === 'ftr') {
                    paragraphAlignment = 'center';
                    fieldType = 'footer';
                    fieldRepresentation = '';
                } else if (type === 'sldNum') {
                    paragraphAlignment = 'right';
                    fieldType = 'page-number';
                    fieldRepresentation = '<#>';
                }

                operationOptions = { start: _.copy(start), target, type: 'shape', attrs: { drawing: { noGroup: true }, presentation: { phType: type, phIndex }, paragraph: { alignment: paragraphAlignment }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 }, shape: { anchor: 'centered' }, fill: { color: Color.WHITE, type: 'none' }, line: { type: 'none' } } };
                _.extend(operationOptions.attrs.drawing, getPlaceholderRectangleForInsert(type));
                insertAttribute(operationOptions.attrs, 'drawing', 'id', self.getNewDrawingId());
                generator.generateOperation(INSERT_DRAWING, operationOptions);

                paraPos = appendNewIndex(start);
                operationOptions = { start: _.copy(paraPos), target, attrs: { character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 }, paragraph: { alignment: paragraphAlignment, bullet: { type: 'none' } } } };
                generator.generateOperation(PARA_INSERT, operationOptions);

                fieldPos = appendNewIndex(paraPos);
                operationOptions = { start: _.copy(fieldPos), target, type: fieldType, representation: fieldRepresentation };
                generator.generateOperation(FIELD_INSERT, operationOptions);

                start = increaseLastIndex(start);
                phIndex += 1;
            }

        } else if (type === 'footers') {

            _.each(footerTypes, function (ftrType) {
                var foundObj = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, ftrType);
                if (foundObj.length) {
                    foundObjects.push(foundObj);
                } else {
                    notFoundFtrTypes.push(ftrType);
                }
            });
            if (foundObjects.length === footerTypes.length) { // all are there, delete them
                _.each(foundObjects, function (ftrObj) {
                    operationOptions = { start: getOxoPosition(self.getRootNode(activeSlideId), ftrObj, 0), target };
                    deleteOperations.push(operationOptions);
                    self.handlePlaceHolderAttributeTransfer(ftrObj, activeSlideId, generator);
                });
                if (deleteOperations.length) {
                    deleteOperations = _.sortBy(deleteOperations, 'start');
                    deleteOperations.reverse();
                    _.each(deleteOperations, function (deleteOp) {
                        generator.generateOperation(DELETE, deleteOp);
                    });
                }
            } else { // some are missing, add them
                _.each(notFoundFtrTypes, function (ftrType) {
                    operationOptions = { start: _.copy(start), target, type: 'shape', attrs: { drawing: { noGroup: true }, presentation: { phType: ftrType, phIndex }, shape: { anchor: 'centered' } } };
                    if (isMasterSlideId || !self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterSlideId, ftrType).length) { // is not existing on master slide
                        _.extend(operationOptions.attrs.drawing, getPlaceholderRectangleForInsert(ftrType));
                    }
                    insertAttribute(operationOptions.attrs, 'drawing', 'id', self.getNewDrawingId());
                    generator.generateOperation(INSERT_DRAWING, operationOptions);

                    paraPos = _.clone(start);
                    paraPos.push(0);
                    operationOptions = { start: _.copy(paraPos), target, attrs: { character: { fontSize: 12 }, paragraph: { alignment: ftrType === 'ftr' ? 'center' : (ftrType === 'sldNum' ? 'right' : 'left') } } };
                    generator.generateOperation(PARA_INSERT, operationOptions);

                    if (ftrType !== 'ftr') {
                        fieldPos = _.clone(paraPos);
                        fieldPos.push(0);
                        operationOptions = { start: _.copy(fieldPos), target, type: ftrType === 'dt' ? 'datetimeFigureOut' : 'slidenum', representation: ftrType === 'dt' ? self.getFieldManager().getCurrentShortLocalDate() : '<#>' };
                        generator.generateOperation(FIELD_INSERT, operationOptions);
                    }
                    start = increaseLastIndex(start);
                    phIndex += 1;
                });
            }

        } else { // toggling title or body placeholder

            foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, type);
            if (!foundObjects.length && type === 'title' && !isMasterSlideId) {
                foundObjects = self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(activeSlideId, self.drawingStyles.getListOfAllowedDrwawingTypeSwitches()[type]);
            }
            if (foundObjects.length) { // delete if exists
                operationOptions = { start: getOxoPosition(self.getRootNode(activeSlideId), foundObjects, 0), target };
                generator.generateOperation(DELETE, operationOptions);
                self.handlePlaceHolderAttributeTransfer(foundObjects, activeSlideId, generator);
            } else { // otherwise insert
                operationOptions = { start: _.copy(start), target, type: 'shape', attrs: { drawing: { noGroup: true }, presentation: { phType: type, phIndex }, shape: { autoResizeText: true }, geometry: { presetShape: 'rect' } } };
                if (type === 'title') { operationOptions.attrs.shape.anchor = 'centered'; }
                if (isMasterSlideId || !self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterSlideId, type).length) { // is not existing on master slide
                    _.extend(operationOptions.attrs.drawing, getPlaceholderRectangleForInsert(type));
                }
                insertAttribute(operationOptions.attrs, 'drawing', 'id', self.getNewDrawingId());
                generator.generateOperation(INSERT_DRAWING, operationOptions);

                paraPos = _.clone(start);
                paraPos.push(0);
                operationOptions = { start: _.copy(paraPos), target };
                generator.generateOperation(PARA_INSERT, operationOptions);

                textPos = _.clone(paraPos);
                textPos.push(0);
                textInsert = { start: _.copy(textPos), target, text: type === 'title' ? titleLabel : textLabels.shift() };
                generator.generateOperation(TEXT_INSERT, textInsert);

                if (type === 'body') {
                    _.each(textLabels, function (textLabel, index) {
                        paraPos = increaseLastIndex(paraPos);
                        paraInsert = { start: _.copy(paraPos), target, attrs: { paragraph: { level: index + 1 } } };
                        generator.generateOperation(PARA_INSERT, paraInsert);

                        textPos = _.clone(paraPos);
                        textPos.push(0);
                        textInsert = { start: _.copy(textPos), target, text: textLabel };
                        generator.generateOperation(TEXT_INSERT, textInsert);
                    });
                }
            }
        }

        this.applyOperations(generator);

        // switching to slide selection (62998)
        self.getSelection().setSlideSelection();
    };

    /**
     * Generating the operations for moving drawing(s) in z-order. Drawings
     * can be moved one level upwards or backwards or they can be moved to
     * the background or the foreground.
     *
     * @param {DrawingOrderType} orderType
     *  The move direction used to reorder the drawing objects.
     *
     * @returns {number}
     *  The number of generated operations.
     */
    this.changeDrawingOrder = function (orderType) {

        var // whether the drawing shall be moved upwards or downwards
            upwards = (orderType === 'front' || orderType === 'forward'),
            // whether the drawing shall be moved to foreground or background
            full = (orderType === 'front' || orderType === 'back'),
            // the selection object
            selection = self.getSelection(),
            // the logical start position of the selected drawing
            startPosition = selection.getStartPosition(),
            // the logical new destination position of the selected drawing
            destPosition = null,
            // the operations generator
            generator = null,
            // the options for the operation
            operationOptions = null,
            // the number of drawings on the current level (only needed, if upwards is true)
            drawingCount = 0,
            // an array with all logical positions in a multi drawing selection
            allPositions = null,
            // an array with all indices (the last numbers of the logical positions) in a multi drawing selection
            allSelectedIndices = null,
            // the logical position of the parent of the drawing
            rootPosition = null,
            // a container for all destination indices
            destIndices = null,
            // a container for all indices for the move operation(s)
            moveIndices = null,
            // a container for all selected drawing nodes
            allSelectedDrawings = null,
            // the number of generated operations
            operationCounter = 0;

        if (selection.isMultiSelectionSupported() && selection.isMultiSelection()) {

            // handle multi drawing selection
            allPositions = selection.getArrayOfLogicalPositions();
            allSelectedIndices = _.map(allPositions, function (pos) { return _.last(pos); });
            rootPosition = _.initial(allPositions[0]);
            destIndices = [];
            moveIndices = [];

            if (upwards) {
                allSelectedIndices.reverse();  // reverting the order of the selected drawings when moving upwards
                drawingCount = selection.getFirstSelectedDrawingNode().parent().children(NODE_SELECTOR).length; // only needed, if upwards is true
            }

            if (full) {

                if (upwards) {

                    _.each(allSelectedIndices, function (index) {

                        var newIndex = drawingCount - 1;

                        // finding the smallest number (greater or equal 0) smaller than index, that is not part of the selection
                        while (newIndex > index && _.contains(allSelectedIndices, newIndex)) { newIndex--; }

                        if (newIndex > index) {
                            while (_.contains(destIndices, newIndex)) { newIndex--; }
                            destIndices.push(newIndex);
                            moveIndices.push({ from: index, to: newIndex });
                        }
                    });

                } else {

                    _.each(allSelectedIndices, function (index) {

                        var newIndex = 0;

                        // finding the smallest number (greater or equal 0) smaller than index, that is not part of the selection
                        while (newIndex < index && _.contains(allSelectedIndices, newIndex)) { newIndex++; }

                        if (newIndex < index) {
                            while (_.contains(destIndices, newIndex)) { newIndex++; }
                            destIndices.push(newIndex);
                            moveIndices.push({ from: index, to: newIndex });
                        }
                    });
                }

            } else {

                if (upwards) {

                    _.each(allSelectedIndices, function (index) {

                        // Not upwards: every index need to be increased to a position of a not selected drawing
                        var newIndex = index + 1;

                        // starting at the last index, trying to find the previous position that is not in the selection
                        while (newIndex < drawingCount && _.contains(allSelectedIndices, newIndex)) { newIndex++; }

                        if (newIndex < drawingCount && newIndex !== index) {
                            while (_.contains(destIndices, newIndex)) { newIndex--; }
                            destIndices.push(newIndex);
                            moveIndices.push({ from: index, to: newIndex });
                        }
                    });

                } else {

                    _.each(allSelectedIndices, function (index) {

                        // Not upwards: every index need to be reduced to a position of a not selected drawing
                        var newIndex = index - 1;

                        // starting at the last index, trying to find the previous position that is not in the selection
                        while (newIndex >= 0 && _.contains(allSelectedIndices, newIndex)) { newIndex--; }

                        if (newIndex >= 0 && newIndex !== index) {
                            while (_.contains(destIndices, newIndex)) { newIndex++; }
                            destIndices.push(newIndex);
                            moveIndices.push({ from: index, to: newIndex });
                        }
                    });
                }
            }

            if (moveIndices.length > 0) {

                allSelectedDrawings = selection.getArrayOfSelectedDrawingNodes();

                generator = self.createOperationGenerator();

                _.each(moveIndices, function (index) {
                    var dest = appendNewIndex(rootPosition, index.to);
                    operationOptions = { start: appendNewIndex(rootPosition, index.from), to: dest };
                    generator.generateOperation(MOVE, operationOptions);
                });

                operationCounter = generator.getOperationCount();

                self.applyOperations(generator);

                // setting the multi selection using the selected drawing nodes
                selection.setMultiDrawingSelection(allSelectedDrawings);
            }

        } else {

            // single drawing selection

            drawingCount = upwards ? selection.getAnyDrawingSelection().parent().children(NODE_SELECTOR).length : 0; // only needed, if upwards is true

            // also handling the case, if the cursor is positioned inside a shape
            if (selection.isAdditionalTextframeSelection()) {
                startPosition = getOxoPosition(self.getRootNode(self.getActiveSlideId()), selection.getSelectedTextFrameDrawing(), 0);
            }

            // this might be a group drawing, that is selected (56122)
            var selectedDrawing = selection.getSelectedDrawing();
            if (isGroupDrawingFrameWithShape(selectedDrawing)) {
                startPosition = getOxoPosition(self.getRootNode(self.getActiveSlideId()), selectedDrawing, 0);
            }

            if ((!upwards && _.last(startPosition) > 0) || (upwards && _.last(startPosition) < (drawingCount - 1))) {

                if (full) {
                    destPosition = upwards ? appendNewIndex(_.initial(startPosition), drawingCount - 1) : appendNewIndex(_.initial(startPosition), 0);
                } else {
                    destPosition = increaseLastIndex(startPosition, upwards ? 1 : -1);
                }

                // generating the new operation
                generator = self.createOperationGenerator();
                operationOptions = { start: _.copy(startPosition), to: _.copy(destPosition) };
                generator.generateOperation(MOVE, operationOptions);

                operationCounter = generator.getOperationCount();

                // self.extendPropertiesWithTarget(newOperation, self.getActiveTarget()); // TODO: Is it necessary to add target to operation?
                self.applyOperations(generator);

                // setting the selection to the new drawing position
                selection.setTextSelection(destPosition, increaseLastIndex(destPosition));
            }
        }

        return operationCounter;
    };

    /**
     * Rotates and/or flips the selected drawing objects.
     *
     * @param {Number} angle
     *  The rotation angle to be added to the current rotation angle of the
     *  drawing objects.
     *
     * @param {Boolean} flipH
     *  Whether to flip the drawing objects horizontally.
     *
     * @param {Boolean} flipV
     *  Whether to flip the drawing objects vertically.
     */
    this.transformDrawings = function (angle, flipH, flipV) {

        // the operations generator
        var generator = self.createOperationGenerator();
        // the selection object
        var selection = self.getSelection();
        // collection of all currently selected drawings
        var allDrawingSelections;
        var activeSlideId = self.getActiveSlideId();
        var activeSlideNode = self.getSlideById(activeSlideId);
        var rootNode = self.getCurrentRootNode();
        var allSlideDrawings = self.getAllDrawingsOnSlide(activeSlideNode);
        var allSlideConnectors = self.getAllConnectorsOnSlide(activeSlideNode);
        var activeSlideOffset = activeSlideNode.offset();

        if (selection.isMultiSelection()) {
            allDrawingSelections = selection.getMultiSelection();
        } else if (selection.isAdditionalTextframeSelection()) {
            var startPos = getOxoPosition(self.getRootNode(self.getActiveSlideId()), selection.getSelectedTextFrameDrawing(), 0);
            var endPos = increaseLastIndex(startPos);
            allDrawingSelections = [{ drawing: selection.getSelectedTextFrameDrawing(), startPosition: startPos, endPosition: endPos }];
        } else {
            allDrawingSelections = [{ drawing: selection.getSelectedDrawing(), startPosition: selection.getStartPosition(), endPosition: selection.getEndPosition() }];
        }

        _.each(allDrawingSelections, function (oneDrawingSelection) {

            // get the drawing node from multiselection (no transformation of tables)
            var drawingNode = self.getSelection().getDrawingNodeFromMultiSelection(oneDrawingSelection);
            if (isTableDrawingFrame(drawingNode)) { return; }

            // create the new drawing attributes
            var drawingAttrs = self.drawingStyles.getElementAttributes(drawingNode).drawing;
            drawingAttrs = transformAttributeSet(drawingAttrs, angle, flipH, flipV, app.isODF());

            // create the new 'setAttributes' operation
            if (drawingAttrs) {
                var properties = { start: _.clone(oneDrawingSelection.startPosition), attrs: { drawing: drawingAttrs } };
                extendPlaceholderProp(app, drawingNode, properties);
                self.extendPropertiesWithTarget(properties, self.getActiveTarget());
                generator.generateOperation(SET_ATTRIBUTES, properties);
                self.generateConnectorModifyOps(generator, properties, 'rotate', oneDrawingSelection.drawing, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
            }
        });
        if (selection.isCropMode()) { self.exitCropMode(); }

        // apply the operations (undo group is created automatically)
        self.applyOperations(generator);

        // after operations are applied, update resize mouse poiters
        var selectedDrawings = selection.getAllDrawingsInSelection();
        if (selectedDrawings) {
            _.each(selectedDrawings, function (drawingNode) {
                var angle = getDrawingRotationAngle(self, drawingNode) || 0;
                updateResizersMousePointers(drawingNode, angle);
            });
        }
    };

    /**
     * This function generates setAttributes operation for selected drawings that are moved or
     * resized with the cursor keys via keyboard.
     *
     * @param {jQuery.Event} event
     *  A jQuery event object.
     *
     * @returns {Boolean}
     *  Whether this keyboard event was handled within this function. This is the case, if this
     *  is a right/left/up/down cursor key together with any shift or ctrl and if at least one
     *  drawing is selected.
     */
    this.handleDrawingOperations = function (event) {

        var // the operations generator
            generator = self.createOperationGenerator(),
            // whether event is a Left/Right cursor key
            leftRightCursor = hasKeyCode(event, 'LEFT_ARROW', 'RIGHT_ARROW'),
            // whether event is a Left/Right cursor key
            upDownCursor = hasKeyCode(event, 'UP_ARROW', 'DOWN_ARROW'),
            // a container for the selected drawings
            allDrawingSelections = null,
            // the selection object
            selection = self.getSelection(),
            // id of the currently active slide
            activeSlideId = self.getActiveSlideId(),
            // node of the active slide
            activeSlideNode = self.getSlideById(activeSlideId),
            // collection of all drawings on active slide
            allSlideDrawings = self.getAllDrawingsOnSlide(activeSlideNode),
            // collection of all connector drawings on active slide
            allSlideConnectors = self.getAllConnectorsOnSlide(activeSlideNode),
            // left and top position values of active slide
            activeSlideOffset = activeSlideNode.offset(),
            // connector related values grouped in one object
            connectorOptions = { allSlideDrawings, allSlideConnectors, activeSlideOffset };

        if (!leftRightCursor && !upDownCursor) { return false; }

        allDrawingSelections = selection.getSelectedDrawingsInfoObject();

        _.each(allDrawingSelections, function (oneDrawingSelection) {
            generateDrawingAttributeOperation(generator, event, oneDrawingSelection, leftRightCursor, connectorOptions);
        });

        if (selection.isCropMode()) { self.exitCropMode(); }

        // Applying the operations (undo group is created automatically)
        self.applyOperations(generator);

        // no further handling of event required
        return true;
    };

    /**
     * Handling the insertion of a table or image by clicking on the template image
     * inside an empty place holder drawing.
     *
     * @param {jQuery} node
     *  The jQueryfied parent node of the node that is the event target.
     */
    this.handleTemplateImageEvent = function (node) {

        // cancelling an already started tracking process (DOCS-3915)
        TrackingObserver.cancelActive();

        if (isImagePlaceholderTemplateButton(node)) {

            // showing the dialog to select an image -> asynchronous process, so that the position must be calculated later (DOCS-3995)
            jpromise.floating(app.docView.insertImageWithDialog('drive', { removeExisting: true, templateNode: node }));

        } else if (isTablePlaceholderTemplateButton(node)) {
            // inserting a table drawing with specified size (2 rows, 5 columns) and remove an existing drawing
            const drawing = node.closest('.drawing');
            const position = getOxoPosition(self.getRootNode(self.getActiveSlideId()), drawing, 0);
            const drawingAttrs = self.drawingStyles.getElementAttributes(drawing).drawing;
            const explicitAttributes = getExplicitAttributeSet(drawing);
            const rectangle = { left: drawingAttrs.left, top: drawingAttrs.top, width: drawingAttrs.width, height: drawingAttrs.height };

            self.insertTextFrame({ insertTable: true, position: _.clone(position), size: { width: 5, height: 2 }, attributes: _.copy(explicitAttributes, true), rectangle, removeExisting: true });
        }
    };

    /**
     * For large images it might be necessary to reduce the size to size of the slide.
     *
     * @param {Object} drawingAttrs
     *  The set of drawing attributes of the image. These attributes might be modified
     *  within this function.
     */
    this.adaptImageSizeAndPositionToSlide = function (drawingAttrs) {

        var activeSlide = self.getSlideById(self.getActiveSlideId());
        var slideWidth = convertLengthToHmm(activeSlide.width(), 'px');
        var slideHeight = convertLengthToHmm(activeSlide.height(), 'px');

        // first step: Reducing width and heigth of the image
        var imageWidth = drawingAttrs.width;
        var imageHeight = drawingAttrs.height;

        if (imageWidth > slideWidth || imageHeight > slideHeight) {
            // change of image size required
            var widthRatio = imageWidth / slideWidth;
            var heightRatio = imageHeight / slideHeight;
            var ratio = Math.max(widthRatio, heightRatio);

            imageWidth = round(imageWidth / ratio);
            imageHeight = round(imageHeight / ratio);

            // modifying the attributes
            drawingAttrs.width = imageWidth;
            drawingAttrs.height = imageHeight;
        }

        // second step: Adapting the top left corner position of the image -> center the drawing
        var imageLeft = drawingAttrs.left || 0;
        var imageTop = drawingAttrs.top || 0;

        if (((imageLeft + imageWidth) > slideWidth) || ((imageTop + imageHeight) > slideHeight)) {
            imageLeft = round((slideWidth - imageWidth) / 2);
            imageTop = round((slideHeight - imageHeight) / 2);

            // modifying the attributes
            drawingAttrs.left = imageLeft;
            drawingAttrs.top = imageTop;
        }
    };

    // operation handler --------------------------------------------------

    /**
     * The handler for the insertDrawing operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the drawing was inserted successfully.
     */
    this.insertDrawingHandler = function (operation, external) {

        var // the undo operation for the insertSlide operation
            undoOperation = null;

        if (!implInsertDrawing(operation.type, operation.start, operation.attrs, operation.target)) {
            return false;
        }

        if (self.getUndoManager().isUndoEnabled() && !external) {
            // not registering for undo if this is a drawing inside a drawing group (36150)
            undoOperation = isPositionInsideDrawingGroup(self.getNode(), operation.start) ? null : { name: DELETE, start: _.copy(operation.start) };
            if (undoOperation) { self.extendPropertiesWithTarget(undoOperation, operation.target); }
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the move operation.
     *
     * @param {Object} operation
     *  The operation object.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the move operation was handled successfully.
     */
    this.moveHandler = function (operation, external) {

        var // the undo operation for the move operation
            undoOperation = null;

        if (!implMove(operation.start, operation.to, operation.target)) {
            return false;
        }

        // generate undo/redo operations
        if (self.getUndoManager().isUndoEnabled() && !external) {
            undoOperation = { name: MOVE, start: _.copy(operation.to), to: _.copy(operation.start) };
            self.extendPropertiesWithTarget(undoOperation, operation.target);
            self.getUndoManager().addUndo(undoOperation, operation);
        }

        return true;
    };

    /**
     * The handler for the change alignment of the selected drawings operations.
     */
    this.changeAlignment = function (direction) {

        var selection = self.getSelection(),
            allDrawingSelections = selection.getSelectedDrawingsInfoObject({ includeTextFrameSelection: true }),
            generator = self.createOperationGenerator(),
            operationProperties;
        // change the alignment of multi selected drawings
        var activeSlideId = self.getActiveSlideId();
        var activeSlideNode = self.getSlideById(activeSlideId);
        var rootNode = self.getCurrentRootNode();
        var allSlideDrawings = self.getAllDrawingsOnSlide(activeSlideNode);
        var allSlideConnectors = self.getAllConnectorsOnSlide(activeSlideNode);
        var activeSlideOffset = activeSlideNode.offset();

        // Only one drawing is selected, the alignment of the drawing will be oriented on the page.
        if (allDrawingSelections.length === 1) {
            operationProperties = changeAlignment.getOperationPropertiesForSingleSelection(allDrawingSelections[0], direction);
            if (operationProperties !== null) {
                self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
                generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                self.generateConnectorModifyOps(generator, operationProperties, 'move', allDrawingSelections[0].drawing, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
                if (selection.isCropMode()) { self.exitCropMode(); }
            }
        } else {

            changeAlignment.setAlignmentDirection(direction);

            // derermine the drawing/s which is e.g. on the lower left position, to the change the alignment
            _.each(allDrawingSelections, function (drawingSelection) {
                changeAlignment.setBaseDrawing(drawingSelection);
            });

            // generate the operations to move the drawings to the new position
            _.each(allDrawingSelections, function (drawingSelection) {
                operationProperties = changeAlignment.getOperationPropertiesForMultiSelection(drawingSelection, _.clone(drawingSelection.startPosition));

                if (operationProperties !== null) {
                    self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
                    generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                    self.generateConnectorModifyOps(generator, operationProperties, 'move',  drawingSelection.drawing, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
                }
            });
        }
        self.applyOperations(generator);

        return true;
    };

    /**
     * The handler for transparency of selected drawings operations.
     */
    this.changeFill = function (drawing, fill, options) {
        // var selection = self.getSelection();
        // var allDrawingSelections = selection.getSelectedDrawingsInfoObject({ includeTextFrameSelection: true });
        var generator = self.createOperationGenerator();

        // console.log(allDrawingSelections);

        var operationProperties = {
            attrs: {
                fill
            },
            start: _.copy(options.startPosition)
        };

        if (operationProperties !== null) {
            self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
            generator.generateOperation(SET_ATTRIBUTES, operationProperties);
        }
        self.applyOperations(generator);
        // _.each(allDrawingSelections, function (drawingSelection) {
        // });
    };

    /**
     * The handler for transparency of selected drawings operations.
     */
    this.operationWithFormatChanges = function (operations) {
        var generator = self.createOperationGenerator();

        operations.forEach(function (operationProperties) {
            if (operationProperties !== null) {
                self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
                generator.generateOperation(SET_ATTRIBUTES, operationProperties);
            }
        });

        self.applyOperations(generator);

        return true;
    };

    /**
     * Creates coresponding modify operations for connectors linked to the drawingNode,
     * and pushes them to passed generator. Type of modification can be: move, resize or rotate.
     *
     * @param {OperationGenerator} generator
     * @param {Object} operationProperties
     * @param {String} type
     * @param {jQuery} drawingNode
     * @param {Array} allSlideDrawings
     * @param {Array} allSlideConnectors
     * @param {Object} activeSlideOffset
     * @param {jQuery} rootNode
     */
    this.generateConnectorModifyOps = function (generator, operationProperties, type, drawingNode, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode) {
        var attrs = getExplicitAttributeSet(drawingNode);
        if (!isConnectorDrawingFrame(drawingNode)) {
            var drawingAttrs = attrs.drawing;
            var connections = getUniqueConnections(allSlideConnectors, drawingAttrs.id);
            if (connections.length) {
                var opDrawingAttrs = operationProperties.attrs.drawing;
                var optionFlipH = _.isUndefined(opDrawingAttrs.flipH) ? isFlippedHorz(drawingNode) : opDrawingAttrs.flipH;
                var optionFlipV = _.isUndefined(opDrawingAttrs.flipV) ? isFlippedVert(drawingNode) : opDrawingAttrs.flipV;
                var currentAngle = (type === 'rotate') ? (opDrawingAttrs.rotation || 0) : (drawingAttrs.rotation || 0);
                var startAngle = (type === 'rotate') ? (drawingAttrs.rotation || 0) : null;
                var zoomFactor = app.getView().getZoomFactor();
                var optionObj = generateRecalcConnectorOptions(drawingAttrs, opDrawingAttrs, type, optionFlipH, optionFlipV, drawingNode, currentAngle, startAngle, zoomFactor);

                _.each(connections, function (connObj) {
                    var connProp = recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor);
                    if (connProp) {
                        connProp.start = getOxoPosition(rootNode, connObj.node, 0);
                        self.extendPropertiesWithTarget(connProp, self.getActiveTarget());
                        generator.generateOperation(SET_ATTRIBUTES, connProp);
                    }
                });
            }
        } else if (!self.getSelection().isMultiSelection() && attrs.connector && (attrs.connector.startId || attrs.connector.endId)) {
            operationProperties.attrs.connector = operationProperties.attrs.connector || {};
            var selectionNode = drawingNode.data('selection');
            if (attrs.connector.startId) {
                operationProperties.attrs.connector.startId = null;
                operationProperties.attrs.connector.startIndex = null;
                drawingNode.removeClass(START_LINKED_CLASS);
                if (selectionNode) { selectionNode.removeClass(START_LINKED_CLASS); }
            }
            if (attrs.connector.endId) {
                operationProperties.attrs.connector.endId = null;
                operationProperties.attrs.connector.endIndex = null;
                drawingNode.removeClass(END_LINKED_CLASS);
                if (selectionNode) { selectionNode.removeClass(END_LINKED_CLASS); }
            }

        }
    };

    /**
     * Creates modify operations to change the height of each tableRow in a drawingNode
     * and pushes them to passed generator.
     *
     * @param {OperationGenerator} generator
     *  The operations generator
     *
     * @param {jQuery} drawingNode
     *  A single drawing node that has the type table
     *
     * @param {Event} event
     *  A jQuery keycode event which is triggered by requesting a height change
     *
     * @returns {Boolean} createOperation
     *  Whether the operations are to be created or not
     */
    this.generateTableHeightOps = function (generator, drawingNode, event) {

        var // grep all rows inside the table drawing
            rows = getTableRows(drawingNode),
            // active root container node
            activeRootNode = self.getCurrentRootNode(),
            // jQuery row node
            rowNode,
            // factor between old and new height
            factor,
            // the sum of the row heights
            oldHeight = 0,
            // new height of the table with predefined steps
            newHeight,
            // new calculated height of the current row
            newRowHeight,
            // array which stores the heights of cellcontent for each row
            minRowsHeight = [],
            // return variable whether the operations to be created
            createOperation = false;

        // sums the old heights of each row and stores also the minimal height of each row including a 2mm margin
        $(drawingNode).find('tr').each(function () {
            rowNode = $(this);
            oldHeight += convertLengthToHmm(rowNode.outerHeight(true), 'px');
            minRowsHeight.push(convertLengthToHmm(rowNode.find('.cellcontent').outerHeight(true), 'px') + 200);
        });

        newHeight = oldHeight + (hasKeyCode(event, 'UP_ARROW') ? 500 : -500);
        factor = newHeight / oldHeight;

        // generate height change operations for each row
        _.each(rows, function (row) {
            newRowHeight = round(convertLengthToHmm($(row).outerHeight(true), 'px') * factor);
            if (minRowsHeight[row.rowIndex] < newRowHeight) {
                createOperation = true;
                generator.generateOperation(SET_ATTRIBUTES, { start: getOxoPosition(activeRootNode, row, 0), attrs: { row: { height: newRowHeight } } });
            }
        });

        return createOperation;
    };

    /**
     * The handler for distribution of drawings horizontally or vertically,
     * on slide or among selection of drawings.
     *
     * @param {String} mode
     *  Direction: horizontal or vertical, and on slide or among selection of drawings.
     */
    this.distributeDrawings = function (mode) {
        var selection = self.getSelection();
        var allDrawingSelections = selection.getSelectedDrawingsInfoObject({ includeTextFrameSelection: true });
        var allDrawingsCopy = [];
        var allDrawingSelLength = allDrawingSelections.length;
        var activeSlideId = self.getActiveSlideId();
        var activeSlideNode = self.getSlideById(activeSlideId);
        var rootNode = self.getCurrentRootNode();
        var generator = self.createOperationGenerator();
        var pageAttrs = this.pageStyles.getElementAttributes(activeSlideNode);
        var page = pageAttrs && pageAttrs.page;
        var operationProperties;
        var oneDirection, numSplits;
        var minBoundary, maxBoundary, distRange;
        var usedSpace = 0, startPoint = 0, distributionSpace, calculatedPosition = 0;
        var isHorizontal = mode.indexOf('horz') > -1;
        var isSlideDist = mode.indexOf('Slide') > -1;
        var ltPosProp = isHorizontal ? 'left' : 'top';
        var whProperty = isHorizontal ? 'width' : 'height';
        var allSlideDrawings = self.getAllDrawingsOnSlide(activeSlideNode);
        var allSlideConnectors = self.getAllConnectorsOnSlide(activeSlideNode);
        var activeSlideOffset = activeSlideNode.offset();

        if (allDrawingSelLength === 1) { // for only one drawing in selection, use align to slide
            oneDirection = isHorizontal ? 'center' : 'middle';
            operationProperties = changeAlignment.getOperationPropertiesForSingleSelection(allDrawingSelections[0], oneDirection);
            if (operationProperties !== null) {
                self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
                generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                var drawingNode = allDrawingSelections[0].drawing;
                self.generateConnectorModifyOps(generator, operationProperties, 'move', drawingNode, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
                if (selection.isCropMode()) { self.exitCropMode(); }
            }
        } else {
            _.each(allDrawingSelections, function (drawingSelection) {
                var drawingAttrs = getDrawingAttrsByNode(drawingSelection.drawing);
                var rotationAngle = getDrawingRotationAngle(self, drawingSelection.drawing);
                var rotatedAttrs = getRotatedDrawingPoints(drawingAttrs, rotationAngle);
                var finalAttrs = (Math.abs(drawingAttrs.width - drawingAttrs.height) < 300) ? drawingAttrs : rotatedAttrs; // ignore rotation for (almost) squared shapes

                allDrawingsCopy.push({
                    originalAttrs:  drawingAttrs,
                    posAttrs:       finalAttrs,
                    centerPos:      rotatedAttrs[ltPosProp] + rotatedAttrs[whProperty] / 2,
                    leftDiff:       finalAttrs.left - drawingAttrs.left,
                    topDiff:        finalAttrs.top - drawingAttrs.top,
                    startPosition:  _.clone(drawingSelection.startPosition),
                    node:           drawingSelection.drawing
                });
                usedSpace += isHorizontal ? finalAttrs.width : finalAttrs.height;
            });
            allDrawingsCopy = _.sortBy(allDrawingsCopy, 'centerPos'); // sort collection ascending by center position value (top or left depending on vertical/horizontal alignment)

            if (isSlideDist || allDrawingSelLength === 2) { // if only 2 drawings, behavior is like slide distribution
                minBoundary = 0;
                maxBoundary = isHorizontal ? page.width : page.height;
                distRange = maxBoundary - minBoundary;
                numSplits = allDrawingSelLength + (distRange >= usedSpace ?  1 : -1);
                distributionSpace = (distRange - usedSpace) / numSplits;
                startPoint = Math.max(0, (distRange > usedSpace ? distributionSpace : 0));
            } else {
                minBoundary = _.first(allDrawingsCopy).posAttrs[ltPosProp];
                maxBoundary = _.last(allDrawingsCopy).posAttrs[ltPosProp] + _.last(allDrawingsCopy).posAttrs[whProperty];
                distRange = maxBoundary - minBoundary;
                numSplits = allDrawingSelLength - 1;
                distributionSpace = (distRange - usedSpace) / numSplits;
                startPoint = minBoundary;
            }
            calculatedPosition = startPoint;

            _.each(allDrawingsCopy, function (drawingSelection, index) {
                var drawingSpace = isHorizontal ? drawingSelection.posAttrs.width : drawingSelection.posAttrs.height;
                var rotationDiff = isHorizontal ? drawingSelection.leftDiff : drawingSelection.topDiff;
                var newAttrValue = round(calculatedPosition - rotationDiff);

                if (isSlideDist || (index !== 0 && index !== allDrawingSelLength - 1)) { // if is not slide distribution, don't send ops for first and last drawing in range
                    if (drawingSelection.originalAttrs[ltPosProp] !== newAttrValue) { // send operation only if value differs from original one
                        operationProperties = { start: _.copy(drawingSelection.startPosition), attrs: { drawing: {} } };
                        operationProperties.attrs.drawing[ltPosProp] = newAttrValue;
                        self.extendPropertiesWithTarget(operationProperties, self.getActiveTarget());
                        generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                        self.generateConnectorModifyOps(generator, operationProperties, 'move', drawingSelection.node, allSlideDrawings, allSlideConnectors, activeSlideOffset, rootNode);
                    }
                }
                calculatedPosition += drawingSpace + distributionSpace;
            });
        }

        self.applyOperations(generator);
        return true;
    };

    /**
     * Finding a valid logical position for inserting a table or an image.
     * First a selected empty text frame is used, then the first not selected
     * empty text frame.
     * Order of inserting a table or image: First a 'specialized' template
     * place holder of type 'tbl' is used. Then a 'global' body place holder,
     * at which no type is explicitely set. This is determined by
     * 'isContentBodyPlaceHolderAttributeSet'.
     * In this case a selected content body is not preferred to a specialized table
     * place holder drawing.
     *
     * @param {String} type
     *  The type of the drawing that shall be inserted. Currently the types
     *  'pic' and 'tbl' are supported.
     *
     * @returns {Object|Null}
     *  An object containing the logical position of an empty place holder drawing,
     *  its rectangle position and its explicit attributes. The property names are
     *  'position', 'rectangle' and 'attributes'.
     *  Or null, if no valid placeholder can be found.
     */
    this.getValidPlaceHolderPositionForInsertDrawing = function (type) {
        return _.first(this.getAllValidPlaceHolderPositionsForInsertDrawing(type, { firstMatch: true })) || null;
    };

    /**
     * Finding all valid logical positions for inserting a table or an image.
     * First a selected empty text frame is used, then the first not selected
     * empty text frame.
     * Order of inserting a table or image: First a 'specialized' template
     * place holder of type 'tbl' is used. Then a 'global' body place holder,
     * at which no type is explicitely set. This is determined by
     * 'isContentBodyPlaceHolderAttributeSet'.
     * In this case a selected content body is not preferred to a specialized table
     * place holder drawing.
     *
     * @param {String} type
     *  The type of the drawing that shall be inserted. Currently the types
     *  'pic' and 'tbl' are supported.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.firstMatch=false]
     *      If true returns the first and best match only.
     *
     * @returns {Array}
     *  An array of objects containing the logical position of an empty place holder drawing,
     *  its rectangle position and its explicit attributes. The property names are
     *  'position', 'rectangle' and 'attributes'.
     */
    this.getAllValidPlaceHolderPositionsForInsertDrawing = function (type, options) {

        // whether to look for all or the first valid place holder
        var firstMatch = getBooleanOption(options, 'firstMatch', false);
        // the active slide
        var slide = self.getSlideById(self.getActiveSlideId());
        // an optionally selected drawing
        var selectedDrawing = self.getSelection().getAnyDrawingSelection();
        // the drawings on the slide
        var allDrawings = $(slide).children('div.drawing');
        // the resulting array of place holder positions
        var result = [];

        // helper function to find a valid empty place holder drawing of specified type or of type 'body'
        function isValidDrawing(drawing, localType, allowContentPlaceHolder) {

            var isValid = false;
            var attrs = getExplicitAttributeSet(drawing);
            var drawingType = null;

            if (isPlaceHolderAttributeSet(attrs)) {

                drawingType = getPlaceHolderDrawingType(drawing, attrs);

                if (allowContentPlaceHolder) {
                    isValid = (!drawingType || drawingType === localType) && isEmptyPlaceHolderDrawing(drawing);
                } else {
                    isValid = (drawingType === localType) && isEmptyPlaceHolderDrawing(drawing);
                }
            }

            return isValid;
        }

        // helper function to get place holder data from the drawing node
        function getPlaceHolderData(drawingNode) {

            // the logical position of a valid empty place holder drawing
            var pos = getOxoPosition(self.getCurrentRootNode(), drawingNode, 0);
            // the merged drawing attributes
            var drawingAttrs = self.drawingStyles.getElementAttributes(drawingNode).drawing;
            // the explicit drawing attributes
            var explicitAttrs = getExplicitAttributeSet(drawingNode);
            // the position of the drawing
            var rectangle = { left: drawingAttrs.left, top: drawingAttrs.top, width: drawingAttrs.width, height: drawingAttrs.height };

            return { position: pos, rectangle, attributes: explicitAttrs };
        }

        if (selectedDrawing && isValidDrawing(selectedDrawing, type, true)) {
            // using the already selected drawing. This shall always be used first. In PP it seems, that
            // the selected drawing is used as first drawing for images, but not for tables.
            result.push(getPlaceHolderData(selectedDrawing));
        }

        if (!firstMatch || _.isEmpty(result)) {
            // iterating over all drawings on slide that have the correct type
            allDrawings.get().some(function (oneDrawing) {
                if (isValidDrawing(oneDrawing, type, false)) { // not allowing content body place holders -> only those with the correct type
                    result.push(getPlaceHolderData(oneDrawing));
                    return firstMatch; // leaving the loop
                }
            });
        }

        if (!firstMatch || _.isEmpty(result)) {
            // iterating over all drawings on slide that can also be content body place holders
            allDrawings.get().some(function (oneDrawing) {
                if (isValidDrawing(oneDrawing, type, true)) {
                    result.push(getPlaceHolderData(oneDrawing));
                    return firstMatch; // leaving the loop
                }
            });
        }

        if (result.length > 1) {
            // remove duplicates
            result = _.uniq(result, false, function (item) { return item.position.join(','); });
        }

        return result;
    };

    /**
     * Special handling for non empty place holder drawings that are deleted. In this case the content
     * of the place holder drawing is removed, not the place holder drawing itself. So after the delete
     * operation for the drawing, an insertDrawing operation is generated, to insert an empty place
     * holder drawing.
     *
     * Info: This restauration process of placeholder drawings is not used for footer placeholder
     *       drawings (DOCS-4852).
     *
     * @param {jQuery|Node} drawing
     *  The drawing node that will be removed.
     *
     * @param {Number[]} pos
     *  The logical position at which the new drawing will be inserted. This is not necessarily
     *  the logical position of the removed drawing, because in a multi drawing selection, there
     *  might be additional drawings removed before, that are not replaced.
     *
     * @param {Object} [globalGenerator]
     *  An optional global operation generator. If it is specified, it is used. Otherwise a new
     *  operation generator is created.
     *
     * @returns {Object|Null}
     *  The operations generator object, if created or specified. Otherwise null is returned.
     */
    this.handleNonEmptyPlaceHolderDrawings = function (drawing, pos, globalGenerator) {

        // the operations generator
        var generator = globalGenerator || null;
        // the explicit drawing attributes
        var expAttrs = null;
        // the drawing attributes for the new empty placeholder drawing
        var attrs = null;
        // some attributes that need to be transferred fro the old drawing to the place holder drawing (ODF only)
        var drawingAttrs = app.isODF() ? ['name', 'left', 'top', 'width', 'height'] : null;

        if (isPlaceHolderDrawing(drawing) && !isFooterPlaceHolderDrawing(drawing) && !isEmptyPlaceHolderDrawing(drawing)) {

            // using the specified operation generator or create a new one
            generator = generator || self.createOperationGenerator();

            // the explicit drawing attributes
            expAttrs = getExplicitAttributeSet(drawing);

            attrs = {};
            attrs.presentation = _.clone(expAttrs.presentation);
            if (attrs.presentation.phSize) { delete attrs.presentation.phSize; }

            if (app.isODF()) {
                // in ODF some drawing attributes must be restored, but not all like flip or rotation (57698)
                if (expAttrs.drawing) {
                    attrs.drawing = {};
                    _.each(drawingAttrs, function (attribute) { if (!_.isUndefined(expAttrs.drawing[attribute])) { attrs.drawing[attribute] = expAttrs.drawing[attribute]; } });
                }
            } else {
                attrs.drawing = {};
                attrs.drawing.noGroup = true;
                if (expAttrs.drawing && expAttrs.drawing.name) { attrs.drawing.name = expAttrs.drawing.name; }
            }

            insertAttribute(attrs, 'drawing', 'id', self.getNewDrawingId());

            // generating operation for inserting empty place holder drawing (families 'presentation' and 'drawing')
            generator.generateOperation(INSERT_DRAWING, { start: _.clone(pos), type: 'shape', attrs });

            // inserting an empty paragraph in all body place holders, but also into 'title', 'ctrTitle', ...
            if (!app.isODF() && !isPlaceHolderWithoutTextAttributeSet(attrs)) {
                generator.generateOperation(PARA_INSERT, { start: appendNewIndex(pos) });
            }
        }

        return generator;
    };

    /**
     * Special handling for place holder drawings on layout slides. If these slides are deleted,
     * the attributes must be transferred to the dependent drawings on the document slides (47435).
     *
     * @param {jQuery|Node} drawing
     *  The drawing node on the layout slide that will be removed.
     *
     * @param {String} target
     *  The target of the affected layout slide.
     *
     * @param {Object} [globalGenerator]
     *  An optional global operation generator. If it is specified, it is used. Otherwise a new
     *  operation generator is created.
     *
     * @returns {Object|Null}
     *  The operations generator object, if created or specified. Otherwise null is returned.
     */
    this.handlePlaceHolderAttributeTransfer = function (drawing, target, globalGenerator) {

        // the operations generator
        var generator = globalGenerator || null;
        // the explicit attributes of the place holder drawing on the layout slide
        var expAttrs = null;
        // the IDs of all affected document slides
        var allSlideIDs = null;
        // the type of place holder that was modified
        var type = null;
        // the type of place holder that was modified
        var index = 0;
        // whether the target is a target of a master slide
        var isMasterSlide = false;

        if (self.isLayoutOrMasterId(target) && isPlaceHolderDrawing(drawing)) {

            isMasterSlide = self.isMasterSlideId(target);

            // a container with the IDs of all slides affected by the modification of the master/layout slide
            allSlideIDs = self.getAllSlideIDsWithSpecificTargetParent(target);

            expAttrs = getExplicitAttributeSet(drawing);
            if (expAttrs.listStyle) { delete expAttrs.listStyle; }

            type = getPlaceHolderDrawingType(drawing, expAttrs) || 'body';
            index = getPlaceHolderDrawingIndex(drawing, expAttrs);

            _.each(allSlideIDs, function (id) {

                var finderIndex = _.contains(self.getPresentationFooterTypes(), type) ? null : finderIndex;
                // -> ignoring the index for footer drawing place holders

                // updating all drawings, that have the specified place holder type and index
                _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(id, type, finderIndex), function (drawingNode) {

                    // the existing explicit attributes
                    var expDrawingAttrs = getExplicitAttributeSet(drawingNode);
                    // whether the drawing is a generic place holder (type is not defined)
                    var isGenericContentBody = isContentBodyPlaceHolderAttributeSet(expDrawingAttrs);
                    // the new attributes for the drawing
                    var newAttrs = extendOptions(expAttrs, expDrawingAttrs);
                    // the attributes of the place holder drawing on the master slide
                    var masterAttrs = null;
                    // the logical positions of the slide
                    var slidePos = null;
                    // the logical positions of the drawing
                    var drawingPos = null;

                    // using attributes from master slide, if required (no width defined at the layout slide)
                    if (!isMasterSlide && (!(newAttrs && newAttrs.drawing && _.isNumber(newAttrs.drawing.width)))) {
                        masterAttrs = self.drawingStyles.getPlaceHolderAttributes(self.getMasterSlideId(target), type, index);
                        if (masterAttrs && masterAttrs.drawing && _.isNumber(masterAttrs.drawing.width)) {
                            newAttrs.drawing.left = masterAttrs.drawing.left;
                            newAttrs.drawing.top = masterAttrs.drawing.top;
                            newAttrs.drawing.width = masterAttrs.drawing.width;
                            newAttrs.drawing.height = masterAttrs.drawing.height;
                        }
                    }

                    // avoiding to set type 'body' at content body place holders
                    if (isGenericContentBody && newAttrs && newAttrs.presentation && newAttrs.presentation.phType) { delete newAttrs.presentation.phType; }

                    // setting default value for place holder index (only on document slides)
                    if (!isMasterSlide) { newAttrs.presentation.phIndex = self.drawingStyles.getMsDefaultPlaceHolderIndex(); }

                    // using the specified operation generator or create a new one
                    generator = generator || self.createOperationGenerator();

                    slidePos = self.getSlidePositionById(id);
                    drawingPos = getOxoPosition(self.getSlideById(id), drawingNode, 0);
                    drawingPos = slidePos.concat(drawingPos);

                    generator.generateOperation(SET_ATTRIBUTES, {
                        start: _.copy(drawingPos),
                        attrs: newAttrs,
                        target: isMasterSlide ? id : self.getOperationTargetBlocker()
                    });
                });
            });
        }

        return generator;
    };

    /**
     * Calculating a default size for a inserted shape, text frame, placeholder. This is necessary, if the element
     * is inserted via keyboard, on touch or when the user just clicked with the mouse without moving the mouse.
     *
     * @param {Object} drawingInfo
     *  Information about which drawing type is used.
     *  @param {String} drawingInfo.type
     *   The drawing type, e.g. textframe, shape, placeholder.
     *  @param {String} [drawingInfo.presetShapeId=null]
     *   An optional presetShapeId from the inserted drawing.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *   @param {Number} [options.insertPosTop]
     *   An optional top position in px where the shape is inserted. When not used,
     *   a default value is used.
     *  @param {Number} [options.insertPosLeft]
     *   An optional left position in px where the shape is inserted.When not used,
     *   a default value is used.
     *
     * @returns {Object}
     *  The rectangle with the properties 'top', 'left', 'width' and 'height'.
     *  The values are given in 1/100 mm relative to the slide.
     */
    this.getDefaultRectangleForInsert = function (drawingInfo, options) {

        var scrollNode = app.getView().getContentRootNode();
        var drawingInfoObj = _.isObject(drawingInfo) ?  drawingInfo : null;
        // the default size in hmm for the current drawing
        var defaultSizeHmm = getDefaultSizeForDrawingTypeInHmm(drawingInfoObj);
        var width = convertHmmToLength(defaultSizeHmm.width, 'px', 1);
        var height = convertHmmToLength(defaultSizeHmm.height, 'px', 1);

        // defaultLeft is the current slide middle position in px
        var defaultLeft = scrollNode.scrollLeft() - (width / 2) + (scrollNode.width() * 0.5);
        var defaultTop = scrollNode.scrollTop() + scrollNode.height() * 0.1;

        // the position where the drawing is inserted
        var left = getNumberOption(options, 'insertPosLeft', defaultLeft);
        var top = getNumberOption(options, 'insertPosTop', defaultTop);

        function getDefaultSizeForDrawingTypeInHmm(drawingInfoObj) {
            var size = {};
            // the drawing type
            var type = getStringOption(drawingInfoObj, 'type', '');
            // only 'shapes' have a 'presetShapeId'
            var presetShapeId = null;

            switch (type) {
                case 'shape':
                    presetShapeId = getStringOption(drawingInfoObj, 'presetShapeId', '');
                    // the width and height is dependent on the used shape
                    size = createDefaultSizeForShapeId(presetShapeId, 2500, 2500);
                    break;
                case 'textframe':
                    size = { width: 10000, height: 2500 };
                    break;
                case 'placeholder':
                    size = { width: 10000, height: 10000 };
                    break;
                default:
                    globalLogger.error('No drawing type information provided for the defaultBox');
            }
            return size;
        }

        return convertAppContentBoxToOperationBox(app, { left, top, width, height }, true);
    };

    /**
     * This function generates move or drawing operations for place holder drawings
     * located on document slides. This is only necessary for ODF applications! This
     * is caused by the fact, that every drawing contains all attributes for 'left',
     * 'top', 'width' and 'height' in odf file format, so that these values cannot
     * be inherited from the underlying layers.
     * Therefore in OOXML files, it is only necessary to update all drawings on the
     * dependent slides, if a drawing on a master/layout slide was moved or resized.
     * But in ODF there must be an operation for every drawing, so that these drawings
     * get the new size and position. Excluded are those drawings, that have the
     * flag 'userTransformed' in the 'presentation' family.
     *
     * @param {Object} generator
     *  The operations generator object
     *
     * @param {jQuery|Node} drawing
     *  The drawing node on the master slide.
     *
     * @param {Object} props
     *  The properties for the setAttributes operation of the drawing on the master
     *  slide.
     *
     * @param {String} [changeType='move']
     *  The change of the drawing on the master slide. Move or resize. If not specified
     *  'move' is evaluated.
     */
    this.checkODFDependentDrawings = function (generator, drawing, props, changeType) {

        var // the new drawing attributes
            newAttrs = null,
            // the old drawing attributes
            oldAttrs = null,
            // the difference of old and new 'left' value of the place holder drawing
            deltaLeft = 0,
            // the difference of old and new 'top' value of the place holder drawing
            deltaTop = 0,
            // the ratio of new width divided by old width
            factorWidth = 1,
            // the ratio of new height divided by old height
            factorHeight = 1,
            // a container with the IDs of all slides affected by the modification of the master slide
            allSlideIDs = null,
            // the type of the place holder drawing
            type = null;

        // a helper function to generate the move/resize operations for the drawings
        // on the dependent slides
        function generateOperationForDrawing(oneDrawing, slideId) {

            var // the old drawing attributes of the specified drawing
                oldDrawingAttrs = getExplicitAttributes(oneDrawing, 'drawing'),
                // the options for the generated setAttributes operation
                operationOptions = null,
                // the new value for 'left' of the specified drawing
                newLeft = 0,
                // the new value for 'top' of the specified drawing
                newTop = 0,
                // the new value for 'width' of the specified drawing
                newWidth = 0,
                // the new value for 'height' of the specified drawing
                newHeight = 0,
                // the logical position of the slide, that contains the specified drawing
                slidePos = null,
                // the logical position of the specified drawing on its slide
                start = null,
                // the old 'left' value of the master slide drawing
                oldPhLeft = 0,
                // the old 'top' value of the master slide drawing
                oldPhTop = 0,
                // the old 'left' distance between the specified drawing and the master slide drawing
                oldLeftDistanceToPh = 0,
                // the old 'top' distance between the specified drawing and the master slide drawing
                oldTopDistanceToPh = 0;

            if (changeType === 'move') {

                newLeft = oldDrawingAttrs.left + deltaLeft;
                newTop = oldDrawingAttrs.top + deltaTop;
                slidePos = self.getSlidePositionById(slideId);
                start = getOxoPosition(self.getSlideById(slideId), oneDrawing); // always on document slide
                operationOptions = { start: slidePos.concat(start), attrs: { drawing: { left: newLeft, top: newTop } }, target: self.getOperationTargetBlocker() };

            } else if (changeType === 'resize') {

                oldPhLeft = _.isNumber(oldAttrs.left) ? oldAttrs.left : 0;
                oldPhTop = _.isNumber(oldAttrs.top) ? oldAttrs.top : 0;
                oldLeftDistanceToPh = oldDrawingAttrs.left - oldPhLeft; // this value needs to be stretched
                oldTopDistanceToPh = oldDrawingAttrs.top - oldPhTop; // this value needs to be stretched

                newWidth = round(oldDrawingAttrs.width * factorWidth);
                newHeight = round(oldDrawingAttrs.height * factorHeight);
                newLeft = oldPhLeft + deltaLeft + round(oldLeftDistanceToPh * factorWidth);
                newTop = oldPhTop + deltaTop + round(oldTopDistanceToPh * factorHeight);
                slidePos = self.getSlidePositionById(slideId);
                start = getOxoPosition(self.getSlideById(slideId), oneDrawing); // always on document slide
                operationOptions = { start: slidePos.concat(start), attrs: { drawing: { left: newLeft, top: newTop, width: newWidth, height: newHeight } }, target: self.getOperationTargetBlocker() };
            }

            // generate the 'setAttributes' operation
            generator.generateOperation(SET_ATTRIBUTES, operationOptions);
        }

        // Is this a change of a place holder drawing on an ODF master slide?
        // If not, leave immediately.
        if (!app.isODF() || !self.isMasterView() || !isPlaceHolderDrawing(drawing)) { return; }

        // a container with the IDs of all slides affected by the modification of the master slide
        allSlideIDs = self.getAllSlideIDsWithSpecificTargetAncestor(self.getActiveSlideId());

        // leave immediately, if there is no dependent slide
        if (!allSlideIDs || allSlideIDs.length === 0) { return; } // nothing to do

        // leave immediately, if this is a 'move' without change of left or top property
        oldAttrs = getExplicitAttributes(drawing, 'drawing'); // drawing attributes are assigned to every drawing in ODP
        newAttrs = props.attrs.drawing;

        // this should be a move operation -> calculating change of left and top
        deltaLeft = (_.isNumber(newAttrs.left) && _.isNumber(oldAttrs.left)) ? (newAttrs.left - oldAttrs.left) : 0;
        deltaTop = (_.isNumber(newAttrs.top) && _.isNumber(oldAttrs.top)) ? (newAttrs.top - oldAttrs.top) : 0;

        if (changeType !== 'resize') { changeType = 'move'; } // fallback to change type 'move'

        if (changeType === 'move' && deltaLeft === 0 && deltaTop === 0) { return; }  // nothing to do

        if (changeType === 'resize') {
            // calculating the change of width and height
            factorWidth = (_.isNumber(newAttrs.width) && _.isNumber(oldAttrs.width)) ? math.roundp(newAttrs.width / oldAttrs.width, 0.001) : 1;
            factorHeight = (_.isNumber(newAttrs.height) && _.isNumber(oldAttrs.height)) ? math.roundp(newAttrs.height / oldAttrs.height, 0.001) : 1;
        }

        type = getPlaceHolderDrawingType(drawing);

        _.each(allSlideIDs, function (id) {

            var // an object containing the conversion for place holder drawings on master slides
                // -> 'ctrTitle' is 'title' on master, 'subTitle' is 'body' on master
                masterTypes = _.invert(self.drawingStyles.getMasterSlideTypeConverter()),
                // a type array, if more than one type needs to be adapted
                typeArray = masterTypes[type] ? [type, masterTypes[type]] : [type];

            // updating all drawings, that have the specified place holder type
            _.each(self.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(id, typeArray), function (drawing) {
                if (!isUserModifiedPlaceHolderDrawing(drawing)) {
                    generateOperationForDrawing(drawing, id);
                }
            });
        });

    };

    /**
     * Collects and returns all drawing rects (left, top, width, height, etc) on current slide.
     * Important: grouped and two point shapes are not included!
     *
     * @param {jQuery} activeSlide
     * @param {Object} slideOffset
     * @param {Number} zoomFactor
     *
     * @returns {Array}
     */
    this.getAllDrawingRectsOnSlide = function (activeSlide, slideOffset, zoomFactor) {
        var tempCollection = [];
        var allDrawingsOnSlide = activeSlide.find('.drawing').not('[data-type="connector"]').not('.grouped');
        var snapPointCount = 0;

        function createSnapPointsLocal(drawingNode, width, height, isGroup) {
            var shapeGeoData = drawingNode.data('shapeGeoData');
            if (shapeGeoData && snapPointCount < MAX_NUM_SNAPPOINTS_ON_SLIDE_ALLOWED) {
                var refRect = new Rectangle(0, 0, width, height);
                createSnapPoints(drawingNode, refRect, shapeGeoData.avList, shapeGeoData.gdList, shapeGeoData.cxnList);
                snapPointCount++;
            } else if (snapPointCount > MAX_NUM_SNAPPOINTS_ON_SLIDE_ALLOWED) {
                globalLogger.warn('ObjectOperationMixin.getAllDrawingRectsOnSlide(): More snap points on slide than max number allowed!');
            } else if (!isGroup) {
                globalLogger.warn('ObjectOperationMixin.getAllDrawingRectsOnSlide(): No shapeGeoData!');
            }
        }

        if (allDrawingsOnSlide.length < MAX_NUM_DRAWINGS_ON_SLIDE_ALLOWED) {
            _.each(allDrawingsOnSlide, function (drawingOnSlide) {
                if (isGroupDrawingFrame(drawingOnSlide)) {
                    _.each($(drawingOnSlide).find('.grouped'), function (groupedDrawing) {
                        var boundingRect = groupedDrawing.getBoundingClientRect();
                        var left = (boundingRect.left - slideOffset.left);
                        var top = (boundingRect.top - slideOffset.top);
                        var width = boundingRect.width;
                        var height = boundingRect.height;
                        var rect = new Rectangle(left, top, width, height);
                        rect.rotation = getDrawingRotationAngle(self, groupedDrawing);
                        tempCollection.push({ node: groupedDrawing, rect });
                        createSnapPointsLocal($(groupedDrawing), groupedDrawing.offsetWidth, groupedDrawing.offsetHeight, true);
                    });
                } else {
                    var attrs = getExplicitAttributeSet(drawingOnSlide);
                    if (isPlaceHolderAttributeSet(attrs)) {
                        attrs = self.drawingStyles.getElementAttributes(drawingOnSlide);
                    }
                    if (attrs && attrs.drawing) {
                        var left = convertHmmToLength(attrs.drawing.left, 'px', 1) * zoomFactor;
                        var top = convertHmmToLength(attrs.drawing.top, 'px', 1) * zoomFactor;
                        var isAutoHeight = attrs.shape ? attrs.shape.autoResizeHeight : false;
                        var width = ((attrs.drawing.width && !isAutoHeight) ? convertHmmToLength(attrs.drawing.width, 'px', 1) : $(drawingOnSlide).width()) * zoomFactor;
                        var height = ((attrs.drawing.height && !isAutoHeight) ? convertHmmToLength(attrs.drawing.height, 'px', 1) : $(drawingOnSlide).height()) * zoomFactor;
                        var rect = new Rectangle(left, top, width, height);
                        rect.rotation = attrs.drawing.rotation;
                        tempCollection.push({ node: drawingOnSlide, rect });
                        createSnapPointsLocal($(drawingOnSlide), width / zoomFactor, height / zoomFactor);
                    } else {
                        globalLogger.warn('objectOperationMixin.getAllDrawingRectsOnSlide(): missing explicit attributes of drawing node!');
                    }
                }
            });
            infoCounter = 0;
        } else {
            if (infoCounter < 3) {
                app.getView().yell({ type: 'info', message: gt('Connector functionality is disabled because of too many drawings on current slide.') });
                infoCounter++;
            }
        }
        return tempCollection;
    };

    /**
     * Collects all connectors (on given slide node) that are linked with at least one end to a drawing.
     * Makes a data structure with jQuery node of the connector, start and end indexes and ids, if used.
     *
     * @param {jQuery} activeSlide
     *
     * @returns {Array}
     */
    this.getAllConnectorsOnSlide = function (activeSlide) {
        var tempCollection = [];
        var allCNodes = activeSlide.find('[data-type="connector"]').not('.grouped');
        var allGroups = activeSlide.children('[data-type="group"]');
        var groupLinks = {};

        // create structure with linked ids of base groups and children
        _.each(allGroups, function (group) {

            var attrs = getExplicitAttributeSet(group);
            var children = $(group).find('.drawing.grouped');

            _.each(children, function (child) {
                var childAttrs = getExplicitAttributeSet(child);
                groupLinks[childAttrs.drawing.id] = attrs.drawing.id;
            });
        });

        _.each(allCNodes, function (cNode) {
            var attrs = getExplicitAttributeSet(cNode);
            var cAttrs = attrs && attrs.connector;
            if (cAttrs && (cAttrs.startId || cAttrs.endId)) {
                var obj = { node: cNode, attrs };
                if (cAttrs.startId) {
                    obj.startId = cAttrs.startId;
                    obj.startIndex = cAttrs.startIndex;

                    if (groupLinks[obj.startId]) {  obj.parentStartId = groupLinks[obj.startId]; }
                }
                if (cAttrs.endId) {
                    obj.endId = cAttrs.endId;
                    obj.endIndex = cAttrs.endIndex;

                    if (groupLinks[obj.endId]) {  obj.parentEndId = groupLinks[obj.endId]; }
                }
                tempCollection.push(obj);
            }
        });

        return tempCollection;
    };

    /**
     * When pasting connectors linked to drawings with new IDs or when duplicating slides, the
     * insertDrawing operations for connectors must be adapted, so that the connectors also
     * work after pasting or after copying the slide.
     *
     * @param {Object[]} operations
     *  An array containing the operations. This operations might be modified within this function.
     *
     * @param {String[][]} drawingIdsTranslationTable
     *  The translation table for the drawing IDs. It is an array of arrays, that contain two string.
     *  The first string is the new drawing ID, the second string the old drawing ID.
     */
    this.refreshConnectorLinks = function (operations, drawingIdsTranslationTable) {
        _.each(operations, function (operation) {
            if (operation.name === INSERT_DRAWING && operation.attrs && operation.attrs.connector) {
                var newStartIdCouple = _.find(drawingIdsTranslationTable, function (couple) { return couple[1] === operation.attrs.connector.startId; });
                operation.attrs.connector.startId = newStartIdCouple ? newStartIdCouple[0] : null;
                if (!operation.attrs.connector.startId) { operation.attrs.connector.startIndex = null; }
                var newEndIdCouple = _.find(drawingIdsTranslationTable, function (couple) { return couple[1] === operation.attrs.connector.endId; });
                operation.attrs.connector.endId = newEndIdCouple ? newEndIdCouple[0] : null;
                if (!operation.attrs.connector.endId) { operation.attrs.connector.endIndex = null; }
            }
        });
    };

    /**
     * Collects all drawings on given slide, and makes a data structure
     * with jQuery nodes, explicit attributes and drawing ids.
     * Important: grouped and two point shapes are not included!
     *
     * @param {jQuery} activeSlide
     *
     * @returns {Array}
     */
    this.getAllDrawingsOnSlide = function (activeSlide) {
        var tempCollection = [];
        var allDrawings = activeSlide.find('.drawing').not('.two-point-shape');

        _.each(allDrawings, function (dNode) {
            var attrs = getExplicitAttributeSet(dNode);
            if (isPlaceHolderAttributeSet(attrs)) {
                attrs = self.drawingStyles.getElementAttributes(dNode);
            }
            var dAttrs = attrs && attrs.drawing;
            if (dAttrs && dAttrs.id) {
                tempCollection.push({ node: dNode, attrs, id: dAttrs.id });
            }
        });

        return tempCollection;
    };

    /**
     * Public getter for drawing attributes of the drawing node.
     *
     * @param {HTMLElement|jQuery} drawingNode
     * @returns {Object} the drawing attributes.
     */
    this.getDrawingAttrsByNode = function (drawingNode) {
        return getDrawingAttrsByNode(drawingNode);
    };

    /**
     * Public method used to snap and paint snap guidelines during move or resize of shapes.
     * Checks if shiftX and shifY values need correction (close to some of snaplines), and if true, returns modified versions of them as xy object.
     * It will also activate visibility and position of corresponing snap lines on the slide.
     *
     * @param {Number} shiftX
     * @param {Number} shiftY
     * @param {Object} snapLinesContainer
     * @param {Object} guideLinesObj
     * @param {Number} vertGuidelineOffset
     * @param {Number} zoomFactor
     *
     * @returns {{x: Number, y: Number}}
     */
    this.checkSnapLines = function (shiftX, shiftY, snapLinesContainer, guideLinesObj, vertGuidelineOffset, zoomFactor, activeSlide) {
        var SNAP_THRESHOLD = 5;
        var EXTRA_PADDING = 7;
        var collectorX = [], collectorY = [], collectorDx = [], collectorDy = [], collectorW = [], collectorH = [];
        var snapX, snapY, snapDx, snapDy, snapW, snapH;
        var minLeft, minTop, lineLength;
        var activeRect, staticRect;
        var originalShiftX = shiftX;
        var originalShiftY = shiftY;
        // for zooms lower than 100%, there is extra 3px margin added, to avoid crop of shadows arond slide/page (this is implemented in view.js recalculateDocumentMargin fn)
        var marginAddition = zoomFactor < 1 ? 3 : 0;

        function getMinVal(memorized, current) { return current.v < memorized.v ? current : memorized; }

        function closeToEqual(a, b, threshold) { return Math.abs(a - b) < threshold; }

        function getActivatedSnaplines(snaplinesArr, diff, collector) {
            for (var i = 0; i < snaplinesArr.length; i++) {
                if (closeToEqual(diff, snaplinesArr[i].v, SNAP_THRESHOLD)) {
                    collector.push(snaplinesArr[i]);
                }
            }
        }

        function areRectsOverlapping(aRectTop, aRectBottom, sRectTop, sRectBottom) {
            return (aRectTop >= sRectTop && aRectTop <= sRectBottom) || // top edge of active rect is between top and bottom edges of static rect
                (aRectBottom >= sRectTop && aRectBottom <= sRectBottom) || // bottom edge is in between edges of static rect
                (aRectTop <= sRectTop && aRectBottom >= sRectBottom); // active rect is wrapping static rect
        }

        function areRectsXoverlapping(aRect, sRect, diff) { return areRectsOverlapping(aRect.top + diff, aRect.bottom + diff, sRect.top, sRect.bottom); }

        function areRectsYoverlapping(aRect, sRect, diff) { return areRectsOverlapping(aRect.left + diff, aRect.right + diff, sRect.left, sRect.right); }

        function areRectsXorYOverlapping(aRect, sRect, shiftX, shiftY) {
            return areRectsXoverlapping(aRect, sRect, shiftY) || areRectsYoverlapping(aRect, sRect, shiftX); // horizontal or vertical overlap
        }

        function getActivatedLengthLines(linesCollector, reduceCollector, isVertical) {
            for (var i = 0; i < linesCollector.length; i++) {
                var sRect = linesCollector[i].props.staticRect;
                var aRect = linesCollector[i].props.activeRect;
                var useFirstEdge = isVertical ? linesCollector[i].norm.top === aRect.top : linesCollector[i].norm.left === aRect.left;
                var tempShift = isVertical ? shiftY : shiftX;
                var normShift = useFirstEdge ? tempShift * -1 : tempShift;
                var activeDistance = isVertical ? (aRect.bottom - aRect.top + normShift) : (aRect.right - aRect.left + normShift);
                var staticDistance = isVertical ? sRect.height : sRect.width;
                if (closeToEqual(tempShift, linesCollector[i].v, SNAP_THRESHOLD) && closeToEqual(activeDistance, staticDistance, SNAP_THRESHOLD)) {
                    if (areRectsXorYOverlapping(aRect, sRect, shiftX, shiftY)) {
                        reduceCollector.push(linesCollector[i]);
                    }
                }
            }
        }

        function getActivatedDistanceLines(linesCollector, diff, oppositeDiff, reduceCollector, isVertical) {
            for (var i = 0; i < linesCollector.length; i++) {
                if (closeToEqual(diff, linesCollector[i].v, SNAP_THRESHOLD)) {
                    var sRect = linesCollector[i].props.staticRect;
                    var aRect = linesCollector[i].props.activeRect;
                    var overlapFn = isVertical ? areRectsYoverlapping : areRectsXoverlapping;
                    if (overlapFn(aRect, sRect, oppositeDiff)) {
                        var slideLength = linesCollector[i].slideLength;
                        var firstProp = isVertical ? 'top' : 'left';
                        var lastProp = isVertical ? 'bottom' : 'right';
                        var isActiveFirst = aRect[firstProp] < sRect[firstProp];
                        var firstRectFirstEdge = isActiveFirst ? aRect[firstProp] + diff : sRect[firstProp];
                        var secondRectFirstEdge = isActiveFirst ? sRect[firstProp] : aRect[firstProp] + diff;
                        var firstRectLastEdge = isActiveFirst ? aRect[lastProp] + diff : sRect[lastProp];
                        var secondRectLastEdge = isActiveFirst ? sRect[lastProp] : aRect[lastProp] + diff;
                        if (closeToEqual(slideLength - secondRectLastEdge, firstRectFirstEdge, SNAP_THRESHOLD) || closeToEqual(slideLength - secondRectFirstEdge, firstRectLastEdge, SNAP_THRESHOLD)) {
                            reduceCollector.push(linesCollector[i]);
                        }
                    }
                }
            }
        }

        function paintDistanceLine(propsObj, shiftDiff, otherDiff, staticLine, activeLine, middleLine, isVertical) {
            var activeRect = propsObj.props.activeRect;
            var staticRect = propsObj.props.staticRect;
            var firstProp = isVertical ? 'top' : 'left';
            var lastProp = isVertical ? 'bottom' : 'right';
            var lastOpositeProp = isVertical ? 'right' : 'bottom';
            var minLastEdge = Math.min(staticRect[lastProp], activeRect[lastProp] + shiftDiff);
            var maxLastEdge = Math.max(staticRect[lastProp], activeRect[lastProp] + shiftDiff);
            var minFirstEdge = Math.min(staticRect[firstProp], activeRect[firstProp] + shiftDiff);
            var maxFirstEdge = Math.max(staticRect[firstProp], activeRect[firstProp] + shiftDiff);
            var isMiddleAlign = propsObj.norm !== (maxLastEdge * zoomFactor) && propsObj.norm !== (minFirstEdge * zoomFactor);
            var distanceLength = (isMiddleAlign ? (maxFirstEdge - minLastEdge) / 2 : minFirstEdge) * zoomFactor;
            var activeEndCss = (activeRect[lastOpositeProp] + otherDiff) * zoomFactor + EXTRA_PADDING;
            var staticEndCss = staticRect[lastOpositeProp] * zoomFactor + EXTRA_PADDING;
            var minFirstCssStatic = isMiddleAlign ? (minLastEdge * zoomFactor) : 0;
            var minFirstCssActive = isMiddleAlign ? minFirstCssStatic + distanceLength : maxLastEdge * zoomFactor;
            var staticStyles, activeStyles;

            if (isVertical) {
                staticStyles = {
                    top: minFirstCssStatic + vertGuidelineOffset,
                    height: distanceLength,
                    left: activeRect[firstProp] < staticRect[firstProp] ? activeEndCss : staticEndCss
                };

                activeStyles = {
                    top: minFirstCssActive + vertGuidelineOffset,
                    height: distanceLength,
                    left: activeRect[firstProp] < staticRect[firstProp] ? staticEndCss : activeEndCss
                };
            } else {
                activeEndCss += vertGuidelineOffset;
                staticEndCss += vertGuidelineOffset;

                staticStyles = {
                    left: minFirstCssStatic + marginAddition,
                    width: distanceLength,
                    top: activeRect[firstProp] < staticRect[firstProp] ? activeEndCss : staticEndCss
                };

                activeStyles = {
                    left: minFirstCssActive + marginAddition,
                    width: distanceLength,
                    top: activeRect[firstProp] < staticRect[firstProp] ? staticEndCss : activeEndCss
                };
            }
            if (isMiddleAlign) { // special middle horz or vert slide center line
                var slideWidth = activeSlide.width() * zoomFactor;
                var slideHeight = activeSlide.height() * zoomFactor;
                if (isVertical) {
                    middleLine.css({ top: vertGuidelineOffset + slideHeight / 2, left: 0, width: slideWidth }).removeClass('hidden'); // horizontal middle separator
                } else {
                    middleLine.css({ top: vertGuidelineOffset, left: slideWidth / 2 + marginAddition, height: slideHeight }).removeClass('hidden'); // vertical middle separator
                }
            }
            staticLine.css(staticStyles).removeClass('hidden');
            activeLine.css(activeStyles).removeClass('hidden');
        }

        function paintLengthLine(propsObj, activeLine, staticLine, isHeightLine) {
            var activeRect = propsObj.props.activeRect;
            var staticRect = propsObj.props.staticRect;
            var staticStyles, activeStyles;

            if (isHeightLine) {
                staticStyles = {
                    top: staticRect.top * zoomFactor + vertGuidelineOffset,
                    height: staticRect.height * zoomFactor,
                    left: staticRect.cx * zoomFactor
                };
                var usedTop = snapH.norm.top === activeRect.top;
                var activeTop = activeRect.top + (usedTop ? shiftY : 0);
                activeStyles = {
                    top: activeTop * zoomFactor + vertGuidelineOffset,
                    height: staticRect.height * zoomFactor,
                    left: (activeRect.centerX + shiftX / 2) * zoomFactor
                };
            } else {
                staticStyles = {
                    left: staticRect.left * zoomFactor + marginAddition,
                    width: staticRect.width * zoomFactor,
                    top: staticRect.cy * zoomFactor + vertGuidelineOffset
                };
                var usedLeft = propsObj.norm.left === activeRect.left;
                var activeLeft = activeRect.left + (usedLeft ? shiftX : 0);
                activeStyles = {
                    left: activeLeft * zoomFactor + marginAddition,
                    width: staticRect.width * zoomFactor,
                    top: (activeRect.centerY + shiftY / 2) * zoomFactor + vertGuidelineOffset
                };
            }

            staticLine.css(staticStyles).removeClass('hidden');
            activeLine.css(activeStyles).removeClass('hidden');
        }

        _.each(_.keys(snapLinesContainer), function (key) {
            var snapLinesX = snapLinesContainer[key].x || [];
            var snapLinesY = snapLinesContainer[key].y || [];
            var snapLinesDx = snapLinesContainer[key].dx || [];
            var snapLinesDy = snapLinesContainer[key].dy || [];
            var snapLinesW = snapLinesContainer[key].w || [];
            var snapLinesH = snapLinesContainer[key].h || [];

            getActivatedSnaplines(snapLinesX, shiftX, collectorX); // horizontal snap lines
            getActivatedSnaplines(snapLinesY, shiftY, collectorY); // vertical snap lines

            getActivatedDistanceLines(snapLinesDx, shiftX, shiftY, collectorDx, false); // horizontal distance lines
            getActivatedDistanceLines(snapLinesDy, shiftY, shiftX, collectorDy, true); // vertical distance lines

            getActivatedLengthLines(snapLinesW, collectorW, false); // width hint lines
            getActivatedLengthLines(snapLinesH, collectorH, true); // height hint lines
        });
        snapX = _.reduce(collectorX, getMinVal, collectorX[0]);
        snapY = _.reduce(collectorY, getMinVal, collectorY[0]);
        snapDx = _.reduce(collectorDx, getMinVal, collectorDx[0]);
        snapDy = _.reduce(collectorDy, getMinVal, collectorDy[0]);
        snapW = _.reduce(collectorW, getMinVal, collectorW[0]);
        snapH = _.reduce(collectorH, getMinVal, collectorH[0]);

        if (snapX) { // horz snap line positioning
            shiftX = snapX.v;
            activeRect = snapX.props.activeRect;
            staticRect = snapX.props.staticRect;
            minTop = snapX.props ?
                Math.min(staticRect.top, (activeRect.top + shiftY)) * zoomFactor + vertGuidelineOffset - EXTRA_PADDING :
                0 - EXTRA_PADDING;
            lineLength = snapX.props ?
                (Math.max(staticRect.bottom, (activeRect.bottom + shiftY)) * zoomFactor - minTop + vertGuidelineOffset + EXTRA_PADDING) :
                (100 + EXTRA_PADDING + '%');
            guideLinesObj.ys.css({
                left: snapX.norm + marginAddition,
                top: minTop,
                height: lineLength
            }).removeClass('hidden');
        } else {
            guideLinesObj.ys.addClass('hidden');
        }

        if (snapY) { // vert snap line positioning
            shiftY = snapY.v;
            activeRect = snapY.props.activeRect;
            staticRect = snapY.props.staticRect;
            minLeft = snapY.props ?
                Math.min(staticRect.left, (activeRect.left + shiftX)) * zoomFactor - EXTRA_PADDING :
                0 - EXTRA_PADDING;
            lineLength = snapY.props ?
                (Math.max(staticRect.right, (activeRect.right + shiftX)) * zoomFactor - minLeft + EXTRA_PADDING) :
                (100 + EXTRA_PADDING + '%');
            guideLinesObj.xs.css({
                top: snapY.norm + vertGuidelineOffset,
                left: minLeft,
                width: lineLength
            }).removeClass('hidden');
        } else {
            guideLinesObj.xs.addClass('hidden');
        }
        // horizontal distance hint arrows
        if (snapDx && originalShiftX) {
            // first, remove vertical snap line, if shift is going to be modified again
            if (!closeToEqual(shiftX, snapDx.v, 2)) { guideLinesObj.ys.addClass('hidden'); }
            shiftX = snapDx.v;
            paintDistanceLine(snapDx, shiftX, shiftY, guideLinesObj.dxs, guideLinesObj.dxa, guideLinesObj.dxmiddle, false);
        } else {
            guideLinesObj.dxs.addClass('hidden');
            guideLinesObj.dxa.addClass('hidden');
            guideLinesObj.dxmiddle.addClass('hidden');
        }
        // vertical distance hint arrows
        if (snapDy && originalShiftY) {
            if (!closeToEqual(shiftY, snapDy.v, 2)) { guideLinesObj.xs.addClass('hidden'); }
            shiftY = snapDy.v;
            paintDistanceLine(snapDy, shiftY, shiftX, guideLinesObj.dys, guideLinesObj.dya, guideLinesObj.dymiddle, true);
        } else {
            guideLinesObj.dys.addClass('hidden');
            guideLinesObj.dya.addClass('hidden');
            guideLinesObj.dymiddle.addClass('hidden');
        }
        // width active and static hint arrows
        if (snapW && originalShiftX) {
            shiftX = snapW.v;
            paintLengthLine(snapW, guideLinesObj.wa, guideLinesObj.ws, false);
        } else {
            guideLinesObj.ws.addClass('hidden');
            guideLinesObj.wa.addClass('hidden');
        }
        // height active and static hint arrow lines
        if (snapH && originalShiftY) {
            shiftY = snapH.v;
            paintLengthLine(snapH, guideLinesObj.ha, guideLinesObj.hs, true);
        } else {
            guideLinesObj.hs.addClass('hidden');
            guideLinesObj.ha.addClass('hidden');
        }

        return { x: shiftX, y: shiftY };
    };
}
