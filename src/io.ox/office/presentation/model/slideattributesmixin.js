/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';

import { getPlaceHolderDrawingType } from '@/io.ox/office/presentation/utils/placeholderutils';

// mix-in class SlideAttributesMixin ======================================

/**
 * A mix-in class for the document model class PresentationModel providing
 * the slide attributes containers for all slide attribute families and
 * list styles used in a presentation document.
 */
export default function SlideAttributesMixin() {

    var // self reference
        self = this,
        // collector for the list style attributes in layout or master slides
        listStylesCollector = {},
        // the collector for the slide attributes
        slideFamilyAttributesCollector = {};

    // private methods ----------------------------------------------------

    /**
     * Saving the list style attributes in a container, if the slide is a master
     * or layout slide.
     *
     * This function can be called directly from insertDrawing operation. In that
     * case the attributes already contain the full set of properties from the
     * families 'drawing' and 'presentation'.
     *
     * @param {String} id
     *  The id of the layout or master slide, that defines the attributes for
     *  the presentation place holder type.
     *
     * @param {Object} attrs
     *  The attributes object, that contains the attributes for the specified
     *  target and place holder type. This are the explicit attributes set at
     *  the drawing.
     *
     * @returns {Object|Null}
     *  If the attributes are registered, an object with the complete registration
     *  information is returned. The object's keys are: 'target', 'placeHolderType',
     *  'placeHolderIndex' and 'attrs'. If the attributes were not registered,
     *  because target is not set (no master or layout slide) or the attributes
     *  contain no place holder information (no place holder drawing), null is
     *  returned.
     */
    function saveListStylesAttributes(id, attrs) {

        var // the properties inside the listStyles object
            type = null;

        // id must be defined, it must be a layout or master slide id and the attributes
        // must have the property 'listStyles' specified.
        // -> the attributes are saved in the model.
        if (!id || !self.isLayoutOrMasterId(id) || !attrs || !attrs.listStyles) { return null; }

        // using type: 'title', 'body', 'other'
        for (type in attrs.listStyles) {
            if (_.contains(SlideAttributesMixin.LIST_STYLES_TYPES, type)) {
                listStylesCollector[id] = listStylesCollector[id] || {};
                listStylesCollector[id][type] = attrs.listStyles[type];
            }
        }

        // returning an object with the complete new registration information
        return { id, listStyles: attrs.listStyles };
    }

    /**
     * Saving the slide attributes of family 'slide' in a container.
     *
     * @param {String} id
     *  The id of the slide. This can be a standard, a layout or a master slide.
     *
     * @param {Object} attrs
     *  The attributes object, that can contain the family 'slide'.
     */
    function saveSlideFamilyAttributes(id, attrs) {

        if (!id || !attrs) { return; }

        // iterate over all supported families (not 'listStyles')
        for (const family of self.slideStyles.yieldSupportedFamilies()) {
            if (attrs[family]) {
                slideFamilyAttributesCollector[id] = slideFamilyAttributesCollector[id] || {};
                slideFamilyAttributesCollector[id][family] = slideFamilyAttributesCollector[id][family] ? _.extend(slideFamilyAttributesCollector[id][family], attrs[family]) : _.clone(attrs[family]);
            }
        }
    }

    /**
     * Emptying the complete slide attributes model. This is necessary after, if a
     * new snapshot is applied.
     */
    function emptySlideAttributesModel() {
        listStylesCollector = {};
        slideFamilyAttributesCollector = {};
    }

    /**
     * Deleting the saved attributes of a slide specified by its ID. This happens
     * if a slide is removed. This is an update of the model.
     *
     * @param {String} id
     *  The id of the slide. This can be a standard, a layout or a master slide.
     */
    function deleteSlideAttributes(id) {
        delete listStylesCollector[id];
        delete slideFamilyAttributesCollector[id];
    }

    // public methods -----------------------------------------------------

    /**
     * Saving the attributes of the family 'slide' for a slide specified by its
     * id in the model.
     * See description at private function 'saveSlideAttributes'.
     */
    this.saveSlideAttributes = function (id, attrs) {
        saveSlideFamilyAttributes(id, attrs);
        saveListStylesAttributes(id, attrs);
    };

    /**
     * Receiving the list styles attributes defined at a specified slide. This is a layout or master slide.
     * If the list styles are defined, the attributes from the attributes from the parent layers can be used.
     *
     * @param {String} id
     *  The slide ID.
     *
     * @param {String} type
     *  The 'type' of the drawing element, for that the list styles are searched. This can be for example
     *  'title'. Typically only 'title', 'body' and 'other' are supported at the slide. All other types need
     *  to be converted to one of these three types.
     *
     * @returns {Object|Null}
     *  The list styles attributes defined for the specified slide and the specified drawing place holder type.
     */
    this.getListStylesAttributesOfSlide = function (id, type) {
        if (type && SlideAttributesMixin.LIST_TYPE_CONVERTER[type]) { type = SlideAttributesMixin.LIST_TYPE_CONVERTER[type]; }
        if (type && !_.contains(SlideAttributesMixin.LIST_STYLES_TYPES, type)) { type = SlideAttributesMixin.DEFAULT_LIST_STYLES_TYPE; }
        return (listStylesCollector && listStylesCollector[id] && listStylesCollector[id][type]) ? _.clone(listStylesCollector[id][type]) : null;
    };

    /**
     * Receiving all list styles attributes defined at a specified slide. This is a layout or master slide.
     *
     * @param {String} id
     *  The slide ID.
     *
     * @returns {Object|Null}
     *  The list styles attributes defined at the specified slide.
     */
    this.getAllListStylesAttributesOfSlide = function (id) {
        return (listStylesCollector && listStylesCollector[id]) ? _.clone(listStylesCollector[id]) : null;
    };

    /**
     * Receiving the complete list style object. This is necessary, if a snap shot is created, because these
     * list styles are not saved in the data object of the slide. The other attributes are all saved in the
     * data-object, so that they can be restored later.
     *
     * Info: This function shall only be used for snap shot creation.
     *
     * @returns {Object}
     *  The collector object for the list style attributes in layout or master slides.
     */
    this.getAllSlideListStyleAttributes = function () {
        return _.copy(listStylesCollector, true);
    };

    /**
     * Setting the full object for the slide list styles. This is necessary after applying a snapshot.
     *
     * Info: This function shall only be used for applying snapshot.
     *
     * @param {Object} listStyles
     *  The object containing the full set of slide list styles. This was typically saved before using
     *  the function 'getAllSlideListStyleAttributes'.
     */
    this.setAllSlideListStyleAttributes = function (listStyles) {
        listStylesCollector = _.copy(listStyles, true);
    };

    /**
     * Getting the attributes of the supported families for a slide specified by its ID.
     *
     * @param {String} [id]
     *  The slide ID. If not specified, the ID of the active slide is used.
     *
     * @returns {Object|Null}
     *  The slide attributes defined for the specified slide. Or null, if no attributes
     *  of any supported family exist for the slide.
     */
    this.getAllFamilySlideAttributes = function (id) {

        var // the slide id
            localId = id || self.getActiveSlideId();

        return slideFamilyAttributesCollector[localId] ? _.clone(slideFamilyAttributesCollector[localId]) : null;
    };

    /**
     * Getting the slide attributes of a specified family for a slide specified
     * by its ID.
     *
     * @param {String} id
     *  The slide ID.
     *
     * @param {String} family
     *  The family of the property.
     *
     * @returns {Object|Null}
     *  The slide attributes defined for the specified slide. Or null, if no attributes
     *  of family 'slide' exist for the slide.
     */
    this.getSlideAttributesByFamily = function (id, family) {
        return (slideFamilyAttributesCollector[id] && slideFamilyAttributesCollector[id][family]) ? _.clone(slideFamilyAttributesCollector[id][family]) : null;
    };

    /**
     * Getting the value of one property of the 'slide' family or a slide specified by
     * its ID.
     *
     * @param {String} id
     *  The slide ID.
     *
     * @param {String} family
     *  The family of the property.
     *
     * @param {String} property
     *  The name of the property of the 'slide' family..
     *
     * @returns {Any}
     *  The value of the specified 'slide' family property. Null, if it does not exist.
     */
    this.getSlideFamilyAttributeForSlide = function (id, family, property) {

        var // the value of the specified property
            value = null,
            // the slide attributes of the specified slide
            attrs = slideFamilyAttributesCollector[id];

        if (attrs && attrs[family] && attrs[family][property] !== undefined) { value = attrs[family][property]; }

        return value;
    };

    /**
     * Getting the merged attributes of the 'slide' family for a slide specified by its ID.
     * This contains the slide specified by the ID and its layout and master slide. If the
     * parameter 'id' specifies a layout slide, it is only merged with the master slide
     * attributes. If the 'id' specifies a master slide, only the attributes of family 'slide'
     * of the master slide are returned.
     *
     * @param {String} id
     *  The slide ID.
     *
     * @param {String} family
     *  The family of the property.
     *
     * @returns {Object|Null}
     *  The merged slide attributes defined for the specified slide. Or null, if no attributes
     *  of family 'slide' exist for the slide.
     */
    this.getMergedSlideAttributesByFamily = function (id, family) {

        var // the set of slide IDs
            slideIdSet = self.getSlideIdSet(id),
            // the slide attributes
            slideAttrs = slideIdSet.id ? self.getSlideAttributesByFamily(slideIdSet.id, family) : null,
            // the layout slide attributes
            layoutAttrs = slideIdSet.layoutId ? self.getSlideAttributesByFamily(slideIdSet.layoutId, family) : null,
            // the master slide attributes
            masterAttrs = slideIdSet.masterId ? self.getSlideAttributesByFamily(slideIdSet.masterId, family) : null,
            // the default attributes of the family
            attributes = this.defAttrPool.getDefaultValues(family);

        if (attributes && attributes[family]) { attributes = attributes[family]; }

        // merging the collected attributes from master and layout slides and drawings (never overwrite attribute set)
        if (masterAttrs) { attributes = _.extend({}, masterAttrs); }
        if (layoutAttrs) { attributes = _.extend(attributes || {}, layoutAttrs); }
        if (slideAttrs) { attributes = _.extend(attributes || {}, slideAttrs); }

        return attributes;
    };

    /**
     * Getter function for the default list style type for place holder attributes.
     *
     * @returns {String}
     *  The default list style type for place holder attributes.
     */
    this.getDefaultPlaceHolderListStyleType = function () {
        return SlideAttributesMixin.DEFAULT_LIST_STYLES_TYPE;
    };

    /**
     * Deleting the saved attributes of a slide specified by its ID. This happens
     * if a slide is removed.
     *
     * @param {String} id
     *  The id of the slide. This can be a standard, a layout or a master slide.
     */
    this.deleteSlideAttributes = function (id) {
        deleteSlideAttributes(id);
    };

    /**
     * Deleting the complete model of slide attributes. This is necessary, if a new
     * snapshot is applied.
     */
    this.emptySlideAttributesModel = function () {
        emptySlideAttributesModel();
    };

    /**
     * Convenience function for receiving the 'isDate' property of the slide family of
     * a slide specified by its ID converted to a boolean (ODP functionality).
     *
     * @param {String} id
     *  The slide ID.
     *
     * @returns {Boolean}
     *  Whether the slide specified by the ID needs a visible 'date' footer object.
     */
    this.isDateDrawingSlide = function (id) {
        return !!self.getSlideFamilyAttributeForSlide(id, 'slide', 'isDate');
    };

    /**
     * Convenience function for receiving the 'isFooter' property of the slide family of
     * a slide specified by its ID converted to a boolean (ODP functionality).
     *
     * @param {String} id
     *  The slide ID.
     *
     * @returns {Boolean}
     *  Whether the slide specified by the ID needs a visible 'ftr' footer object.
     */
    this.isFooterDrawingSlide = function (id) {
        return !!self.getSlideFamilyAttributeForSlide(id, 'slide', 'isFooter');
    };

    /**
     * Convenience function for receiving the 'isSlideNum' property of the slide family of
     * a slide specified by its ID converted to a boolean (ODP functionality).
     *
     * @param {String} id
     *  The slide ID.
     *
     * @returns {Boolean}
     *  Whether the slide specified by the ID needs a visible 'sldNum' footer object.
     */
    this.isSlideNumberDrawingSlide = function (id) {
        return !!self.getSlideFamilyAttributeForSlide(id, 'slide', 'isSlideNum');
    };

    /**
     * ODP only.
     * Convenience function that checks, if a specified drawing is a footer place holder drawing
     * (type 'dt', 'ftr' or 'sldNum') that is visible on a document slide specified by its ID.
     *
     * @param {String} id
     *  The document slide ID.
     *
     * @param {HTMLElement|jQuery} drawing
     *  The drawing node.
     *
     * @returns {Boolean}
     *  Whether the specified drawing is a footer place holder drawing that is visible on the
     *  document slide specified by its ID.
     */
    this.isVisiblePlaceHolderFooterDrawing = function (id, drawing) {

        return self.isVisiblePlaceHolderFooterDrawingType(getPlaceHolderDrawingType(drawing));
    };

    this.isVisiblePlaceHolderFooterDrawingType = function (id, type) {
        var
            isVisible = !!type;

        if (isVisible) {
            isVisible = (
                ((type === 'dt') && self.isDateDrawingSlide(id)) ||
                ((type === 'ftr') && self.isFooterDrawingSlide(id)) ||
                ((type === 'sldNum') && self.isSlideNumberDrawingSlide(id))
            );
        }
        return isVisible;
    };

    this.isPlaceHolderFooterDrawingType = function (type) {
        return (!!type && _.contains(self.getPresentationFooterTypes(), type));
    };
}

/**
 * The allowed types for the slide's list styles.
 */
SlideAttributesMixin.LIST_STYLES_TYPES = ['title', 'body', 'other'];

/**
 * The default type for the slide's list styles.
 */
SlideAttributesMixin.DEFAULT_LIST_STYLES_TYPE = 'other';

/**
 * Converter for specific types for the slide's list styles.
 *
 * Info: Please also check 'PresentationDrawingStyles.MASTER_PLACEHOLDER_TYPE_SWITCHES'.
 */
SlideAttributesMixin.LIST_TYPE_CONVERTER = {
    ctrTitle: 'title',
    subTitle: 'body'
};
