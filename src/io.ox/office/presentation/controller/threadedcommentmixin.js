/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import { is } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, convertLengthToHmm } from '@/io.ox/office/tk/dom';

// mix-in class CommentMixin ==============================================

/**
 * Implementations of all controller items for manipulating threaded comments
 * in an active slide, intended to be mixed into the presentation controller.
 *
 * @param {PresentationModel} docModel
 *  The document model.
 *
 * @param {PresentationView} docView
 *  The document view that e.g. provides access to current view settings.
 */
function ThreadedCommentMixin(docModel, docView) {

    // threaded comment collection of the active/selected slide
    var commentCollection = null;

    var contextMenuMouseX = 0;
    var contextMenuMouseY = 0;

    function insertThreadedComment() {
        docModel.startNewThread();
    }

    function insertReply() {
        var commentModel = commentCollection.getSelectedThread();
        if (commentModel) {
            docModel.trigger('start:threadedcomment:reply', commentModel);
        }
    }

    function insertThreadedCommentFromContextMenu() {
        var zoomFactor = docView.getZoomFactor();
        var pageOffset = docModel.getNode().offset();

        // window.console.log('+++ threadedcomment/insert/fromcontextmenu :: zoomFactor, pageNode : ', zoomFactor, docModel.getNode());

        var pageX = Math.round((contextMenuMouseX - pageOffset.left) / zoomFactor);
        var pageY = Math.round((contextMenuMouseY - pageOffset.top) / zoomFactor);

        return docModel.startNewThread(null, { position: { x: convertLengthToHmm(pageX, 'px'), y: convertLengthToHmm(pageY, 'px') } });
    }

    // register item definitions
    this.registerItems({

        'slides/any': {
            enable() { return (!docModel.isMasterView() && (docModel.getStandardSlideOrder().length >= 1)); }
        },
        'threadedcomment/any': {
            parent: 'slides/any',
            enable() { return docModel.isAnyThreadedComment(); }
        },

        // visibility state of the comments pane (setter expects a boolean state)
        //
        'view/commentspane/show': {
            parent: ['app/valid', 'slides/any'],
            enable() { return !SMALL_DEVICE && !docView.docApp.isODF(); },

            get() { return docView.commentsPane.isVisible(); },
            set() { docView.commentsPane.toggle(); }
        },

        /**
         *  usage of `commentCollection` as a guard in the next three items
         *  is due to prevent behavior as described with the following bug ...
         *
         *  ... #Bug 66076 - Close document during loading:=> typeError
         *  [https://bugs.open-xchange.com/show_bug.cgi?id=66076]
         */
        'threadedcomment/selected': {
            parent: ['document/editable', 'slides/any'],
            enable() { return !!(commentCollection && !commentCollection.destroyed && commentCollection.getSelectedThread()); }
        },

        'threadedcomment/editable': {
            parent: ['document/editable', 'slides/any'],
            enable() { return !!commentCollection; }
        },

        'threadedcomment/model': {
            parent: 'threadedcomment/editable',
            get() {
                var commentModel = (commentCollection && !commentCollection.destroyed && commentCollection.getSelectedThread());
                return ((commentModel && !commentModel.destroyed && commentCollection.getByAddress(commentModel.getAnchor())) || null); // returns the `threadList`.
            }
        },

        'threadedcomment/delete/all': {
            parent: 'threadedcomment/any',
            set() {
                var title = gt('Delete comments');
                var message = gt('All comments in this presentation will be removed. Do you want to continue?');

                var promise = docView.showQueryDialog(title, message, { width: 550 });

                return promise.then(function () {
                    return docModel.deleteEveryComment();
                });
            }
        },

        'threadedcomment/delete/thread': {
            parent: 'threadedcomment/model',
            enable() { return is.array(this.parentValue); },
            set() { return commentCollection.deleteThread(this.parentValue); }
        },

        'threadedcomment/delete/reply': {
            parent: 'document/editable',
            set(commentModel) {
                if (commentModel.isReply()) {
                    commentCollection.deleteComment(commentModel);
                } else {
                    commentCollection.deleteThread(commentCollection.getByAddress(commentModel.getAnchor()));
                }
            }
        },

        'threadedcomment/insert': {
            parent: 'threadedcomment/editable',
            set() { docView.processUnsavedComment().then(insertThreadedComment); }
        },

        'threadedcomment/insert/reply': {
            parent: 'threadedcomment/editable',
            set() { docView.processUnsavedComment().then(insertReply); }
        },

        'threadedcomment/insert/fromcontextmenu': {
            parent: 'threadedcomment/insert',
            set() { docView.processUnsavedComment().then(insertThreadedCommentFromContextMenu); }
        },

        'threadedcomment/goto/enabled': {
            parent: 'threadedcomment/any',
            get() {
                var overallThreadCount      = docModel.getOverallThreadCount();
                var isAlertQueryBoundaries  = (overallThreadCount >= 2);

                return {
                    // overallThreadCount:     overallThreadCount,
                    isEnableGotoPrevious:   isAlertQueryBoundaries,
                    isAlertQueryBoundaries
                };
            }
        },

        'threadedcomment/goto/next': {
            parent: 'threadedcomment/goto/enabled',
            set() { searchThread(false, this.parentValue.isAlertQueryBoundaries); }
        },

        'threadedcomment/goto/prev': {
            parent: 'threadedcomment/goto/enabled',
            enable() { return this.parentValue.isEnableGotoPrevious; },
            set() { searchThread(true, this.parentValue.isAlertQueryBoundaries); }
        }
    });

    function queryNextNonEmptyCollection(activeSlideId, isReverse, isAlertQueryBoundaries) {

        var nextCommentModel = false;

        var slideIdList = Array.from(docModel.getStandardSlideOrder());
        if (isReverse) { slideIdList.reverse(); }

        var slideIdCount  = slideIdList.length;
        var recentIndex   = slideIdList.indexOf(activeSlideId);
        var activeIndex   = -1;

        if (recentIndex >= 0) {  activeIndex = (recentIndex + 1); }
        if (activeIndex > slideIdCount) { activeIndex = 0; }
        if (activeIndex >= 0) {

            // make list of `slideId`'s iterable ...
            // ... but transform it's `activeSlideId` to a "Zero Value" based index.
            //
            slideIdList = slideIdList.concat(slideIdList.splice(0, activeIndex));

            // stop iteration at the first non empty threaded-comment-collection.
            //
            var newSlideId = activeSlideId;
            nextCommentModel = slideIdList.reduce(function (commentModel, slideId) {
                var collection  = null;
                var hasComments = null;
                var commentModels = null; // all comment models in a collection (on the slide)

                if (!commentModel) {
                    collection = docModel.getCommentCollection(slideId);
                    hasComments = collection.hasComments();

                    if (hasComments) {
                        newSlideId = slideId;
                        commentCollection = collection;
                        commentModels = commentCollection.getCommentModels();
                        commentModel = isReverse ? commentModels[commentModels.length - 1] : commentModels[0];
                    }
                }

                return commentModel;
            }, null);

            if (nextCommentModel) {

                if (isAlertQueryBoundaries) {
                    if ((!isReverse && docModel.getSortedSlideIndexById(newSlideId) < docModel.getSortedSlideIndexById(activeSlideId)) ||
                        (isReverse && docModel.getSortedSlideIndexById(newSlideId) > docModel.getSortedSlideIndexById(activeSlideId))) {
                        docView.yell({
                            type: 'info',
                            message: (isReverse ? gt('The search continues from the end of this presentation.') : gt('The search continues from the beginning of this presentation.'))
                        });
                    }
                }

                // change model ... force model and view update ... results in mutated local reference of `commentCollection`.
                if (newSlideId !== activeSlideId) {
                    docModel.setActiveSlideId(newSlideId, { forceUpdate: true, index: 1 });
                }

            } else {
                // this should never happen because of both guarding controller items 'threadedcomment/goto/enabled' and  'threadedcomment/any'.
                docView.yell({ type: 'error', message: gt('Searching for a thread did fail.') });
            }
        }
        return nextCommentModel;
    }

    function getNextThread(isReverse, isAlertQueryBoundaries) {
        var commentModel;

        var isProceed = (commentCollection.hasComments() || queryNextNonEmptyCollection(docModel.getActiveSlideId(), isReverse, isAlertQueryBoundaries));
        if (isProceed) {

            var threadList  = commentCollection.getParentComments();
            var threadCount = threadList.length;

            if (isReverse) {
                threadList.reverse();
            }
            var selectedCommentModel  = commentCollection.getSelectedThread(); // {ThreadedCommentModel|Null}
            var index                 = threadList.indexOf(selectedCommentModel); // -1|[0..(count-1)]

            if ((index + 1) === threadCount) { // end of list.
                commentModel = queryNextNonEmptyCollection(docModel.getActiveSlideId(), isReverse, isAlertQueryBoundaries);
            } else {
                commentModel = threadList[++index]; // incrementing.
            }
        }
        return commentModel;
    }

    function searchThread(isReverse, isAlertQueryBoundaries) {
        var commentModel = getNextThread(isReverse, isAlertQueryBoundaries);
        if (commentModel) {

            if (!SMALL_DEVICE) {
                docView.commentsPane.show();
            }
            docView.commentsPane.show();
            commentCollection.selectComment(commentModel);
        }
    }

    // initialization -----------------------------------------------------

    this.listenTo(docModel, 'change:activeslide:done', function (slideId) {
        commentCollection = docModel.getCommentCollection(slideId);
    });

    this.listenTo(docView, 'contextmenu:init:mouseposition', function (payload) {
        contextMenuMouseX = payload.mouseX;
        contextMenuMouseY = payload.mouseY;
    });

    // focus back to application pane after editing a comment thread
    this.listenTo(docView, "commenteditor:closed", () => this.grabFocus());
}

// exports ================================================================

export default ThreadedCommentMixin;
