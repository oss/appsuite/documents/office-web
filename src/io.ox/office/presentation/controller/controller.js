/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { getBooleanOption, getStringOption } from '@/io.ox/office/tk/utils';
import { fun } from '@/io.ox/office/tk/algorithms';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { getBorderAttributes, getBorderFlags } from '@/io.ox/office/editframework/utils/mixedborder';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { hasSupportedProtocol, isValidURL } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { EditController } from '@/io.ox/office/editframework/app/editcontroller';
import { getPresetBorder } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { isUnsupportedDrawing } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getDrawingTypeLabel, isPresetConnectorId } from '@/io.ox/office/drawinglayer/view/drawinglabels';
import ThreadedCommentMixin from '@/io.ox/office/presentation/controller/threadedcommentmixin';
import ParagraphStyles from '@/io.ox/office/presentation/format/paragraphstyles';
import { ImageCropDialog } from '@/io.ox/office/presentation/view/dialogs';
import { COMBINED_TOOL_PANES, MAX_TABLE_CELLS, SPELLING_ENABLED } from '@/io.ox/office/textframework/utils/config';
import { getOxoPosition, getParagraphElement, getWordSelection, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';

// class TextController ===================================================

/**
 * The controller of a OX Presentation application.
 */
class PresentationController extends EditController {

    constructor(app, docModel, docView) {

        // base constructor
        super(app, docModel, docView);

        // self reference
        var self = this;

        // protected methods --------------------------------------------------

        /**
         * Executes a "Search" command.
         */
        this.executeSearch = function (command, query, config) {
            return docModel.getSearchHandler().executeSearch(command, query, config);
        };

        /**
         * Executes a "Replace" command.
         */
        this.executeReplace = function (command, query, replace, config) {
            return docModel.getSearchHandler().executeReplace(command, query, replace, config);
        };

        // initialization -----------------------------------------------------

        if (_.browser.Android) {
            var originalUpdate = _.wrap(self.update, function (update) {
                return update.call(self);
            });

            self.update = function () {
                if (docModel.isAndroidTyping()) {
                    return self.executeDelayed(self.update, 1000);
                } else {
                    return originalUpdate();
                }
            };
        }

        // register item definitions
        this.registerItems({

            // view -------------------------------------------------------

            'view/slidepane/show': {
                parent: '!view/combinedpanes',
                get() { return docView.getSlidePane().isVisible(); },
                set(state) { docView.getSlidePane().toggle(state); }
            },

            'view/usesnaplines': {
                get() { return docView.areSnaplinesEnabled(); },
                set(state) { docView.toggleSnapGuideLines(state); }
            },

            'view/toolpane/format/visible': {
                parent: ['document/editable/text/format', '!view/combinedpanes']
            },

            'view/toolpane/font/visible': {
                parent: ['document/editable/text', 'view/combinedpanes']
            },

            'view/toolpane/paragraph/visible': {
                parent: ['document/editable/text', 'view/combinedpanes']
            },

            'view/toolpane/insert/visible': {
                parent: 'document/editable'
            },

            'view/toolpane/slide/visible': {
                parent: 'document/editable'
            },

            'view/toolpane/present/visible': {
                parent: 'document/slideexists'
            },

            'view/toolpane/review/visible': {
                parent: 'document/editable',
                enable() { return !COMBINED_TOOL_PANES || SPELLING_ENABLED; }
            },

            'view/toolpane/table/visible': {
                parent: 'document/editable/table'
            },

            'view/toolpane/drawing/visible': {
                parent: 'document/editable/anydrawingexceptsingletable'
            },

            'document/noslidepanefocus': { // whether the slidepane or the document has the virtual focus
                enable() { return !docView.hasSlidepaneVirtualFocus(); }
            },

            'document/nomultiselection': {
                enable() { return !docModel.getSelection().isMultiSelection(); }
            },

            'document/slideexists': {
                parent: 'app/valid',
                enable() { return !docModel.isEmptySlideView(); }
            },

            'document/editable/slideexists': {
                parent: 'document/editable',
                enable() { return !docModel.isEmptySlideView(); }
            },

            'document/editable/text/format': { // used to set visibility of 'Format' top bar
                parent: 'document/editable',
                enable() { return docModel.isTextSelected() || docModel.getSelection().isAnyTextFrameDrawingSelection(); }
            },

            'document/text': {
                parent: 'document/slideexists',
                enable() { return (docModel.isTextSelected() && !docModel.getSelection().isTopLevelTextCursor()) || docModel.getSelection().isAnyTextFrameDrawingSelection(); }
            },

            'document/editable/text': {
                parent: 'document/editable/slideexists',
                enable() { return (docModel.isTextSelected() && !docModel.getSelection().isTopLevelTextCursor()) || docModel.getSelection().isAnyTextFrameDrawingSelection(); }
            },

            'document/editable/text/notextframe': {
                parent: 'document/editable/text',
                enable() {
                    return !docModel.getSelection().isAdditionalTextframeSelection();
                }
            },

            'document/selectall': {
                parent: 'document/noslidepanefocus',
                // enabled in read-only mode
                set() { docModel.selectAll(); },
                // restrict keyboard shortcut to application pane
                shortcut: { keyCode: 'A', ctrlOrMeta: true, scope: '.app-pane' },
                preserveFocus: true // do not return focus to document
            },

            'document/selectDrawing': {
                parent: 'document/noslidepanefocus',
                set(options) {
                    docModel.getSelection().selectNextDrawing({ backwards: getBooleanOption(options, 'backwards', false) });
                },
                shortcut: { keyCode: 'F4', shift: true }
            },
            'document/setCursorIntoTextframe': {
                set() { docModel.getSelection().setSelectionIntoTextframe({ complete: true }); }
            },
            'document/switchCursorIntoTextframe': {
                parent: 'document/noslidepanefocus',
                set() { docModel.getSelection().toggleTextFrameAndTextSelection({ complete: true }); },
                shortcut: { keyCode: 'F2' }
            },

            // document contents
            'document/cut': {
                parent: ['document/copy', 'selection/editable'],
                set() { docModel.cut(); }
            },
            'document/copy': {
                // enabled in read-only mode
                enable() { return docModel.hasSelectedRange(); },
                set() { docModel.copy(); }
            },
            'document/paste': {
                parent: 'selection/editable',
                set() { return docView.showClipboardNoticeDialog(); }
            },
            'document/paste/internal': {
                parent: 'document/editable',
                enable() { return docModel.hasInternalClipboard(); },
                set() { docModel.pasteInternalClipboard(); }
            },

            // spelling

            'document/spelling/available': {
                parent: 'document/editable',
                enable() { return SPELLING_ENABLED; }
            },
            'document/onlinespelling': {
                parent: 'document/spelling/available',
                get() { return docModel.getSpellChecker().isOnlineSpelling(); },
                set(state) { docModel.getSpellChecker().setOnlineSpelling(state); }
            },
            'document/spelling/replace': {
                parent: 'document/editable',
                set(replacement) {
                    var selection = docModel.getSelection(),
                        startPosition = selection.getStartPosition(),
                        wordPos = getWordSelection(selection.getEnclosingParagraph(), startPosition[startPosition.length - 1]);

                    docModel.getSpellChecker().replaceWord(startPosition, wordPos, replacement);
                }
            },
            'document/spelling/ignoreword': {
                parent: 'document/editable',
                set(options) {
                    return docModel.getSpellChecker().addWordToIgnoreSpellcheck(options);
                }
            },
            'document/spelling/userdictionary': {
                parent: 'document/editable',
                set(options) {
                    return docModel.getSpellChecker().addWordToIgnoreSpellcheck(options);
                }
            },
            // Page settings

            'document/pagesettings': {
                parent: 'document/editable',
                set() { return docView.showPageSettingsDialog(); }
            },

            // paragraphs

            'paragraph/group': {
                parent: 'document/editable/text',
                get() { return docModel.getAttributes('paragraph', { maxIterations: 10 }) || {}; }
            },
            'paragraph/odf/implicit': {
                enable() { return !app.isODF() || !docModel.getSelection().isCursorInImplicitParagraph(); } // no paragraph handling in implicit paragraphs in ODP
            },
            'paragraph/attributes': {
                parent: 'paragraph/group',
                get(paragraph) { return paragraph.paragraph || {}; }
            },
            'paragraph/alignment': {
                parent: ['paragraph/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return attributes.alignment; },
                set(alignment) { docModel.setAttribute('paragraph', 'alignment', alignment); }
                // removed for the 7.2.0 due to clashes with browser shortcuts
                /*shortcut: [
                    { keyCode: 'L', ctrlOrMeta: true, value: 'left' },
                    { keyCode: 'R', ctrlOrMeta: true, value: 'right' },
                    { keyCode: 'E', ctrlOrMeta: true, value: 'center' },
                    { keyCode: 'J', ctrlOrMeta: true, value: 'justify' }
                ]*/
            },
            'paragraph/lineheight': {
                parent: ['paragraph/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return attributes.lineHeight; },
                set(lineHeight) { docModel.setAttribute('paragraph', 'lineHeight', lineHeight); }
                // removed for the 7.2.0 due to clashes with browser shortcuts
                /*shortcut: [
                    { keyCode: '1', ctrlOrMeta: true, value: LineHeight.SINGLE },
                    { keyCode: '5', ctrlOrMeta: true, value: LineHeight.ONE_HALF },
                    { keyCode: '2', ctrlOrMeta: true, value: LineHeight.DOUBLE }
                ]*/
            },
            'paragraph/spacing': {
                parent: ['paragraph/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return docModel.getParagraphSpacing(attributes); },
                set(multiplier) { return docModel.setParagraphSpacing(multiplier); }
            },

            'paragraph/explicit': {
                parent: 'paragraph/attributes',
                get() {
                    // the selection object
                    var selection = docModel.getSelection();
                    // target for operation
                    var target = docModel.getActiveTarget();
                    // the paragraph node at the start position
                    var paragraph = getParagraphElement(docModel.getCurrentRootNode(target), _.initial(selection.getStartPosition()));
                    // the explicit attributes
                    var explicitAttrs = paragraph ? getExplicitAttributeSet(paragraph) : null;
                    // the explicit paragraph attributes
                    return explicitAttrs && explicitAttrs.paragraph ? explicitAttrs.paragraph : null;
                }
            },

            // parent for controller items only enabled inside lists
            'paragraph/list/enabled': {
                parent: 'paragraph/attributes',
                enable() {
                    var indent = this.value.level;
                    return _.isNumber(indent) && (indent >= 0) && (indent <= 8);
                }
            },

            // toggle default bullet list, or select bullet type
            'paragraph/list/bullet': {
                parent: 'paragraph/attributes',
                get(attributes) { return docModel.getEffectiveListStyleId('bullet', attributes); },
                set(value) { docModel.setListStyleId('bullet', value, this.value); }
            },
            // toggle default numbered list, or select numbering type
            'paragraph/list/numbered': {
                parent: 'paragraph/attributes',
                get(attributes) { return docModel.getEffectiveListStyleId('numbering', attributes); },
                set(value) { docModel.setListStyleId('numbering', value, this.value); }
            },

            // change list level
            'paragraph/list/indent': {
                parent: 'paragraph/list/enabled',
                get(attributes) { return attributes.level; },
                set(indent) { docModel.setAttribute('paragraph', 'level', indent); }
            },
            // increase list level by one
            'paragraph/list/incindent': {
                parent: ['paragraph/explicit', 'paragraph/list/indent', 'slide/noodfmasterview', 'insert/placeholder/odf/supported'],
                enable(paraAttrs) { return docModel.isListIndentChangeable(paraAttrs, { increase: true }); },
                set() { docModel.changeListIndent({ increase: true, validatedLevel: true }); }
            },
            // decrease list level by one
            'paragraph/list/decindent': {
                parent: ['paragraph/explicit', 'paragraph/list/indent', 'slide/noodfmasterview'],
                enable(paraAttrs) { return docModel.isListIndentChangeable(paraAttrs, { increase: false }); },
                set() { docModel.changeListIndent({ increase: false, validatedLevel: true }); }
            },
            // slides

            'slide/insertslide': {
                parent: 'document/editable',
                enable() { return !docModel.isMasterView(); },
                set() { docModel.insertSlide(); },
                shortcut: { keyCode: 'M', ctrl: true }
            },
            'slide/insertlayoutslide': {
                parent: 'document/editable',
                enable() { return docModel.isMasterView() && !app.isODF(); },
                set() { docModel.insertLayoutSlide(); }
            },
            'slide/duplicateslides': {
                parent: 'document/editable',
                enable() { return !docModel.isMasterSlideId(docModel.getActiveSlideId()) && !docModel.isEmptySlideView(); },
                set() { return docModel.duplicateSlides(); }
            },
            'slide/deleteslide': {
                parent: 'document/editable',
                enable() { return docModel.isSlideSelectionDeletable(docModel.getSlidePaneSelection()); },
                set() { docModel.deleteMultipleSlides(docModel.getSlidePaneSelection()); }
            },
            'slide/hideslide': {
                parent: 'document/editable/slideexists',
                enable() { return !docModel.isMasterView() && !docModel.isHiddenSlide() && !docModel.isEmptySlideView(); },
                set() { docModel.hideMultipleSlides(docModel.getSlidePaneSelection(), true); }
            },
            'slide/unhideslide': {
                parent: 'document/editable',
                enable() { return !docModel.isMasterView() &&  docModel.isHiddenSlide() && !docModel.isEmptySlideView(); },
                set() { docModel.hideMultipleSlides(docModel.getSlidePaneSelection(), false); }
            },
            'slide/move': {
                parent: 'document/editable',
                set(value) { docModel.moveMultipleSlides(value.indexStart, value.indexEnd); }
            },
            'slide/moveslideupwards': {
                enable() { return !docModel.isFirstSlideActive(); },
                set(selection) { docModel.moveSlectedSlidesOneStep({ downwards: false }, selection); }
            },
            'slide/moveslidedownwards': {
                enable() { return !docModel.isLastSlideActive(); },
                set(selection) { docModel.moveSlectedSlidesOneStep({ downwards: true }, selection); }
            },
            'slide/masternormalslide': {
                parent: 'app/valid',
                set() { docModel.selectSlideView({ showMaster: !docModel.isMasterView() }); }
            },
            'slide/activemasterslide': {
                parent: 'app/valid',
                get() { return docModel.isMasterView(); },
                set() { docModel.selectSlideView({ showMaster: !docModel.isMasterView() }); }
            },
            'slide/noodfmasterview': {
                enable() { return !(app.isODF() && docModel.isMasterView()); }
            },

            // characters

            'character/group': {
                parent: 'document/editable/text',
                get() { return docModel.getAttributes('character', { maxIterations: 10 }) || {}; }
            },
            'character/text/group': {
                parent: 'document/text',
                get() { return docModel.getAttributes('character', { maxIterations: 10 }) || {}; }
            },
            'character/stylesheet': {
                parent: 'character/group',
                get(character) { return character.styleId; },
                set(styleId) { docModel.setAttributes('character', { styleId }, { clear: true }); }
            },
            'character/attributes': {
                parent: 'character/group',
                get(character) { return character.character || {}; }
            },
            'character/text/attributes': {
                parent: 'character/text/group',
                get(character) { return character.character || {}; }
            },
            'character/fontname': {
                parent: 'character/attributes',
                get(attributes) { return attributes.fontName; },
                set(fontName) { docModel.setAttribute('character', 'fontName', fontName); }
            },
            'character/fontsize': {
                parent: ['character/attributes', 'insert/comment/supported/odf', 'drawing/fontscale'],
                get(attributes) { return attributes.fontSize ? Math.round(attributes.fontSize * this.parentValues[2]) : attributes.fontSize; },
                set(fontSize) { docModel.setAttribute('character', 'fontSize', Math.round(fontSize / this.parentValues[2])); }
            },
            'character/bold': {
                parent: 'character/attributes',
                get(attributes) { return attributes.bold; },
                set(state) { docModel.setAttribute('character', 'bold', state); },
                shortcut: { keyCode: 'B', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },
            'character/italic': {
                parent: 'character/attributes',
                get(attributes) { return attributes.italic; },
                set(state) { docModel.setAttribute('character', 'italic', state); },
                shortcut: { keyCode: 'I', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },
            'character/underline': {
                parent: 'character/attributes',
                get(attributes) { return attributes.underline; },
                set(state) { docModel.setAttribute('character', 'underline', state); },
                shortcut: { keyCode: 'U', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },
            'character/strike': {
                parent: 'character/attributes',
                get(attributes) { return _.isString(attributes.strike) ? (attributes.strike !== 'none') : null; },
                set(state) { docModel.setAttribute('character', 'strike', state ? 'single' : 'none'); }
            },
            'character/vertalign': {
                parent: 'character/attributes',
                get(attributes) {
                    return (attributes.baseline < 0) ? 'sub' : ((attributes.baseline > 0) ? 'super' : 'baseline');
                },
                set(align) {
                    var baseline = (align === 'super') ? 30 : ((align === 'sub') ? -25 : 0);
                    docModel.setAttribute('character', 'baseline', baseline);
                }
            },
            'character/color': {
                parent: ['character/attributes', 'insert/comment/odf/supported'],
                get(attributes) { return attributes.color; },
                set(color) {
                    if (color && color.type === 'auto' && !app.isODF() && docModel.getSelection().isAnyDrawingSelection()) { color = docModel.findTextAutoColorForTextframe(); }
                    docModel.setAttribute('character', 'color', color);
                }
            },
            'character/fillcolor': {
                parent: ['character/attributes', 'insert/comment/odf/supported'],
                enable: _.constant(false) // TODO
            },
            'character/language': {
                parent: 'character/attributes',
                get(attributes) { return attributes.language; },
                set(language) { docModel.setAttribute('character', 'language', language); }
            },
            'character/hyperlink': {
                parent: 'character/attributes',
                get(attributes) { return attributes && attributes.url; }
                // no direct setter (see item 'character/hyperlink/dialog')
            },
            'character/hyperlink/insert': {
                parent: 'document/editable/slideexists',
                enable() { return docModel.hasEnclosingParagraph() || docModel.getSelection().isSlideSelection(); }, // DOCS-1737, DOCS-2327
                get(attributes) { return attributes && attributes.url; }
                // no direct setter (see item 'character/hyperlink/dialog')
            },
            'character/hyperlink/dialog': {
                parent: ['document/editable/slideexists', 'character/hyperlink/insert'],
                set() { return docModel.insertHyperlinkDialog(); }
                // removed for the 7.2.0 due to clashes with browser shortcuts
                //shortcut: { keyCode: 'K', ctrlOrMeta: true }
            },
            'character/hyperlink/remove': {
                parent: 'character/hyperlink',
                set() { return docModel.removeHyperlink(); }
            },

            'character/hyperlink/valid': {
                parent: 'character/text/attributes', // not using 'character/attributes' -> must also work in read-only mode
                enable(attrs) { return !!attrs.url && hasSupportedProtocol(attrs.url) && isValidURL(attrs.url); }
            },

            'character/reset': {
                parent: 'document/editable/text',
                set() { docModel.resetAttributes(); },
                shortcut: { keyCode: 'SPACE', ctrl: true, scope: '.app-pane' }
            },

            'character/insert/break': {
                parent: 'document/editable/text',
                set() { return docModel.insertHardBreak(); }
            },
            'character/insert/tab': {
                parent: ['document/editable/text', 'document/nomultiselection'],
                set() { return docModel.insertTab(); }
            },

            // reduced odf text frame functionality
            'insert/textframe/supported': {
                enable() { return !docModel.isReducedOdfTextframeFunctionality(); }
            },

            // only enabled in pptx or in place holder drawings in odf
            'insert/placeholder/odf/supported': {
                enable() { return !app.isODF() || docModel.isAdditionalPlaceHolderDrawingSelection(); }
            },

            // reduced odf comment functionality
            'insert/comment/odf/supported': {
                enable() { return !docModel.isOdfCommentFunctionality(); }
            },

            'insert/comment/supported': {
                enable() { return !docModel.isCommentFunctionality(); }
            },

            'insert/comment/supported/odf': {
                enable() { return app.isODF() || !docModel.isCommentFunctionality(); }
            },

            'insert/margin/supported': {
                enable() { return !docModel.isHeaderFooterEditState(); }
            },

            'insert/margin/supported/odf': {
                enable() { return app.isODF() || !docModel.isHeaderFooterEditState(); }
            },

            // tables

            'table/group': {
                parent: 'document/editable/slideexists',
                enable() {
                    var selection = docModel.getSelection();
                    if (selection.isCellSelection()) { return true; }
                    var drawing = selection.getSelectedDrawing();
                    if (drawing && drawing.attr('data-type') === 'table') { return true; }
                    // Bug 55997: "Table"-Tab stays without Table-Selection
                    if (selection.isAnyTableDrawingSelection()) { return true; }
                    return false;
                },
                get() { return docModel.getAttributes('table') || {}; }
            },
            'table/insert/available': {
                enable() { return MAX_TABLE_CELLS > 0; }
            },

            'table/insert': {
                parent: ['document/editable/slideexists', 'table/insert/available'],
                set(size) {
                    // special ios behavior
                    if (docModel.getSlideTouchMode()) {
                        docModel.insertTextFrame({ insertTable: true, size, selectDrawingOnly: true  });
                    } else {
                        // normal
                        docModel.insertTextFrame({ insertTable: true, size });
                    }
                },
                preserveFocus: true // do not return focus to document
            },

            'document/editable/table': {
                parent: 'table/group'
            },

            'document/editable/table/nocell': {
                parent: 'document/editable/table',
                enable() { return !docModel.getSelection().isCellSelection(); }
            },

            'document/editable/insidetable': {
                parent: 'table/group',
                // the selection must be inside a selected table drawing, but must not be the selected drawing itself (57568)
                enable() { return (docModel.getSelection().isAnyTableDrawingSelection() && !docModel.getSelection().isTableDrawingSelection()) || docModel.getSelection().isCellSelection(); }
            },

            'document/editable/insidetableortable': {
                parent: 'table/group',
                // the selection must be inside a selected table drawing or the selected drawing itself
                enable() { return docModel.getSelection().isAnyTableDrawingSelection() || docModel.getSelection().isCellSelection(); }
            },

            'table/delete': {
                parent: 'document/editable/table',
                set() { return docModel.deleteTable(); }
            },
            'table/insert/row': {
                parent: 'document/editable/insidetable',
                set() { docModel.insertRow(); }
            },
            'table/insert/column': {
                parent: 'document/editable/insidetable',
                set() { docModel.insertColumn(); }
            },
            'table/delete/row': {
                parent: 'document/editable/insidetable',
                set() { return docModel.deleteRows(); }
            },
            'table/delete/column': {
                parent: 'document/editable/insidetable',
                set() { return docModel.deleteColumns(); }
            },

            'table/stylesheet': {
                parent: 'document/editable/table/nocell',
                get(table) { return table.styleId; },
                set(styleId) { docModel.setAttributes('table', { styleId }, { clear: true }); }
            },
            'table/attributes': {
                parent: 'document/editable/table',
                get(table) { return table.table || {}; }
            },
            'table/attributes/nocell': {
                parent: 'table/attributes',
                enable() { return !docModel.getSelection().isCellSelection(); }
            },
            'table/cellattributes': {
                parent: 'document/editable/table',
                get() {
                    return docModel.getAttributes('cell').cell || {};
                }
            },
            'table/cellattributes/nocell': {
                parent: 'table/cellattributes',
                enable() { return !docModel.getSelection().isCellSelection(); }
            },
            'table/cellborder': {
                parent: 'table/attributes/nocell',
                get(attributes) { return getBorderFlags(attributes); },
                set(borderFlags) {
                    var attrSet = this.parentValue;
                    var borderAttrs = getBorderAttributes(borderFlags, attrSet, docModel.getDefaultBorderStyle(attrSet, ParagraphStyles.SINGLE_BORDER), { fullBorderSet: true });
                    docModel.setAttributes('table', { table: borderAttrs }, { cellSpecificTableAttribute: true });
                }
            },
            'table/borderwidth': {
                parent: 'table/cellattributes/nocell',
                get(attributes) {
                    return TableStyles.getBorderStyleFromAttributes(attributes, true);
                },
                set(borderWidth) {
                    docModel.setAttributes('table', {
                        table: TableStyles.getAttributesFromBorderStyle(borderWidth, docModel.getAttributes('table').table)
                    }, { onlyVisibleBorders: true });
                }
            },
            'table/fillcolor': {
                parent: 'table/cellattributes',
                get(attributes) { return attributes.fillColor; },
                set(color) {
                    docModel.setAttributes('cell', { cell: { fillColor: color, fillType: 'solid' } });
                }
            },
            'table/alignment/menu': {
                parent: 'table/cellattributes',
                get: _.noop // do not store formatting attributes in "data-state" attribute of controls
            },
            'table/verticalalignment': {
                parent: ['table/cellattributes'],
                enable() { return docModel.getSelection().isAnyDrawingSelection(); },
                get(attributes) { return attributes.alignVert === 'center' ? 'centered' : attributes.alignVert; },
                set(verticalalignment) { docModel.setAttribute('cell', 'alignVert', verticalalignment === 'centered' ? 'center' : verticalalignment); }
            },

            // drawing

            'document/editable/drawing': {
                parent: 'document/editable',
                enable() { return docModel.isDrawingSelected(); }
            },
            'document/editable/anydrawing': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().isAnyDrawingSelection(); }
            },
            'document/editable/anydrawingexcepttable': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().isAnyDrawingSelectionExceptTable(); }
            },
            'document/editable/anydrawingexceptsingletable': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().isAnyDrawingSelectionExceptTable() || docModel.getSelection().isMultiSelection(); }
            },
            'document/editable/anydrawingexceptodfreadonly': {
                parent: 'document/editable/anydrawing',
                enable() { return !docModel.isODFReadOnyPlaceHolderDrawingProcessing(); }
            },
            'document/editable/onlysupporteddrawings': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().areOnlySupportedDrawingsSelected(); }
            },
            'drawing/shapes/support/fill': {
                parent: 'document/editable/onlysupporteddrawings',
                enable() {
                    var allSelectedTypes = docModel.getSelection().getSelectedDrawingsTypes();
                    return allSelectedTypes.has('shape') || allSelectedTypes.has('group');
                }
            },
            'drawing/shapes/support/line': {
                parent: 'document/editable/onlysupporteddrawings',
                enable() {
                    var allSelectedTypes = docModel.getSelection().getSelectedDrawingsTypes();
                    return allSelectedTypes.has('shape') || allSelectedTypes.has('group') || allSelectedTypes.has('image') || allSelectedTypes.has('connector');
                }
            },
            'drawing/shapes/support/lineendings': {
                parent: ['drawing/attributes/full', 'document/editable/onlysupporteddrawings'],
                enable(attrs) {
                    return (attrs && attrs.geometry) ? isPresetConnectorId(attrs.geometry.presetShape) : false;
                }
            },

            // a single image drawing is selected
            'drawing/cropselection/visible': {
                parent: 'document/editable/drawing',
                enable() {
                    var selection = docModel.getSelection();
                    var isSupportedDrawing = !isUnsupportedDrawing(selection.getSelectedDrawing());
                    return isSupportedDrawing && selection.isCropDrawingSelection() && selection.isOnlyImageSelection();
                }
            },

            'drawing/cropselection': {
                parent: 'drawing/cropselection/visible',
                enable() {
                    return !docModel.getSelection().isMultiSelection(); // in multi image selections the button is visible, but disabled
                }
            },

            'drawing/attributes/full': {
                parent: 'document/editable/anydrawingexcepttable',
                get() { return docModel.getAttributes('drawing') || {}; }
            },

            'drawing/attributes': {
                parent: 'drawing/attributes/full',
                get(attributes) { return attributes.drawing || {}; }
            },
            'drawing/group/attributes': {
                parent: 'document/editable/anydrawing',
                get() { return docModel.getAttributes('drawing', { groupOnly: true }).drawing || {}; }
            },

            'drawing/delete': {
                parent: 'document/editable/anydrawingexceptodfreadonly',
                set() {
                    var selection = docModel.getSelection();
                    if (docModel.isDrawingSelected()) {
                        return docModel.deleteSelected({ deleteKey: true });

                    } else if (selection.isAdditionalTextframeSelection()) {
                        var start = getOxoPosition(docModel.getCurrentRootNode(), selection.getSelectedTextFrameDrawing(), 0),
                            end = increaseLastIndex(start);

                        selection.setTextSelection(start, end);
                        return docModel.deleteSelected();
                    }
                }
            },

            'drawing/type/shape': {
                parent: 'document/editable',
                enable() { return (docModel.getSelection().getClosestSelectedDrawingType() === 'shape' || docModel.getSelection().getClosestSelectedDrawingType() === 'connector'); }
            },

            'drawing/type/label': {
                parent: ['drawing/attributes/full', 'document/editable/drawing'],
                get(attrs) {
                    var connector       = (attrs && attrs.geometry) ? isPresetConnectorId(attrs.geometry.presetShape) : false,
                        originalType    = docModel.getSelection().getClosestSelectedDrawingType(),
                        type            = (originalType === 'shape' && connector) ? 'connector' : originalType;

                    return getDrawingTypeLabel(type);
                }
            },

            'drawing/anydrawing': {
                enable() { return docModel.getSelection().isAnyTextFrameSelection({ allDrawingTypesAllowed: true }); }
            },

            'drawing/anydrawingselectionwithouttable': {
                enable() { return docModel.getSelection().isDrawingSelectionWithoutTable(); }
            },

            'drawing/anytextframeselection': {
                enable() { return docModel.getSelection().isAnyTextFrameSelection({ allowGroup: true }); }
            },

            'drawing/crop': {
                parent: 'drawing/cropselection',
                get() { return docModel.getSelection().isCropMode(); },
                set(state) { docModel.handleDrawingCrop(state); }
            },

            'drawing/cropposition/fill': {
                parent: 'drawing/crop',
                set() { return docModel.cropFillFitSpace('fill'); }
            },

            'drawing/cropposition/fit': {
                parent: 'drawing/crop',
                set() { return docModel.cropFillFitSpace('fit'); }
            },

            'drawing/cropposition/dialog': {
                parent: 'drawing/crop',
                set() { return new ImageCropDialog(docView).show(); }
            },

            'drawing/notsingledrawing': {
                parent: 'document/editable/anydrawing',
                enable() { return docModel.getSelection().isMultiSelection() || docModel.getSelection().isUngroupableSelection(); }
            },

            'drawing/textframeautofit': {
                parent: ['drawing/anydrawingselectionwithouttable', 'drawing/anytextframeselection'], // at least one shape, but not with a table
                get() { return docModel.getDrawingAutoResizeState(); },
                set(fitState) { docModel.handleTextFrameAutoFit(fitState === 'autofit', fitState === 'autotextheight'); }
            },

            'drawing/lockratio': {
                parent: ['drawing/anydrawingselectionwithouttable'],
                get() { return docModel.isLockedRatioDrawing(); },
                set(state) { docModel.handleDrawingLockRatio(state); }
            },

            'drawing/verticalalignment': {
                enable() { return docModel.getSelection().isAnyTextFrameSelection({ allDrawingTypesAllowed: true }) || docModel.getSelection().isAnyTableDrawingSelection(); },
                get() { return docModel.getVerticalAlignmentMode(); },
                set(mode) { docModel.setVerticalAlignmentMode(mode); }
            },

            // dummy item for the "options" dropdown menu in in the drawing toolbar
            'drawing/options/menu': {},

            // drawing fill formatting

            'drawing/fill/attributes': {
                parent: 'drawing/attributes/full',
                get(attributes) { return attributes.fill || {}; }
            },

            'drawing/fill/style/support': {
                parent: 'drawing/attributes/full',
                enable(attributes) { return !((attributes && attributes.geometry) ? isPresetConnectorId(attributes.geometry.presetShape) : false); }
            },

            'drawing/fill/color': {
                parent: 'drawing/fill/attributes',
                get(attributes) {
                    return attributes.color;
                },
                set(color) {
                    return docModel.setDrawingFillColor(color);
                }
            },

            // drawing formatting
            'drawing/format/dialog': {
                parent: ['drawing/anydrawingselectionwithouttable', 'drawing/attributes/full'],
                set() { return docView.showDrawingFormatDialog(this.parentValues[1]); }
            },

            // drawing line formatting
            'drawing/line/attributes': {
                parent: 'drawing/attributes/full',
                get(attributes) { return attributes.line || {}; }
            },

            'drawing/line/endings': {
                parent: ['drawing/line/attributes', 'drawing/shapes/support/lineendings'],
                get(lineAttrs) { return lineAttrs.headEndType + ':' + lineAttrs.tailEndType; },
                set(endings) { docModel.setLineEnds(endings); }
            },

            'drawing/border/style/support': {
                parent: 'drawing/attributes/full',
                enable(attributes) { return !((attributes && attributes.geometry) ? isPresetConnectorId(attributes.geometry.presetShape) : false); }
            },

            'drawing/line/style/support': {
                parent: 'drawing/attributes/full',
                enable(attributes) { return (attributes && attributes.geometry) ? isPresetConnectorId(attributes.geometry.presetShape) : false; }
            },

            'drawing/border/style': {
                parent: 'drawing/line/attributes',
                get(attributes) { return getPresetBorder(attributes); },
                set(border) { docModel.setDrawingBorder(border); }
            },

            'drawing/border/color': {
                parent: 'drawing/line/attributes',
                get(attributes) { return (attributes.type !== 'none') ? attributes.color : Color.AUTO; },
                set(color) { docModel.setDrawingBorderColor(color); }
            },

            'drawing/fontscale': {
                parent: 'document/editable',
                get() {
                    var selectedDrawings = $(docModel.getSelection().getSelectedDrawings({ forceArray: true }));
                    return selectedDrawings.length === 1 ? parseFloat(selectedDrawings[0].find('>.textframecontent').attr('font-scale') || 1) : 1;
                },
                enable() { return docModel.getSelection().getSelectedDrawings({ forceArray: true }).length > 0; }
            },

            // group and ungroup of drawings

            'drawing/group': {
                enable() { return docModel.getSelection().isGroupableSelection(); },
                set() { docModel.groupDrawings(); }
            },

            'drawing/ungroup': {
                enable() { return docModel.getSelection().isUngroupableSelection(); },
                set() { docModel.ungroupDrawings(); }
            },

            'drawing/order': {
                enable() { return docModel.getSelection().isAnyDrawingSelection(); },
                set(orderType) { docModel.changeDrawingOrder(orderType); }
            },

            // parent item for drawing objects that can be flipped and rotated
            'drawing/operation/transform': {
                parent: 'drawing/anydrawingselectionwithouttable'
            },

            // (stateless command) flips the selected drawing objects horizontally
            'drawing/transform/fliph': {
                parent: 'drawing/operation/transform',
                set() { docModel.transformDrawings(0, true, false); }
            },

            // (stateless command) flips the selected drawing objects vertically
            'drawing/transform/flipv': {
                parent: 'drawing/operation/transform',
                set() { docModel.transformDrawings(0, false, true); }
            },

            // (stateless command) rotates the selected drawing objects by the passed angle
            'drawing/transform/rotate': {
                parent: 'drawing/operation/transform',
                set(angle) { docModel.transformDrawings(angle, false, false); }
            },

            // drawing/s alignment

            'drawing/align': {
                enable() { return docModel.getSelection().isAnyDrawingSelection(); },
                set(position) { docModel.changeAlignment(position); }
            },

            'drawing/distribute': {
                enable() { return docModel.getSelection().isAnyDrawingSelection(); },
                set(mode) { docModel.distributeDrawings(mode); }
            },

            'drawing/distributeamong': {
                enable() { return docModel.getSelection().isMultiSelection() && docModel.getSelection().getMultiSelectionCount() > 2; },
                set(mode) { docModel.distributeDrawings(mode); }
            },

            // text frames

            'textframe/insert': {
                parent: ['document/editable/slideexists'],
                get() { return docModel.isInsertTextFrameActive(); },
                set(value, inputOptions) {
                    if (getStringOption(inputOptions, 'sourceType') === 'keyboard') {
                        return docModel.insertTextFrameWithDefaultBox();
                    } else if (docModel.getSlideTouchMode()) {
                        return docModel.insertTextFrameWithDefaultBox({ selectDrawingOnly: true });
                    } else {
                        return docModel.insertTextFramePreparation();
                    }
                }
            },

            // images

            'image/insert/dialog': {
                parent: ['document/editable/slideexists'],
                set(sourceType) { return docView.insertImageWithDialog(sourceType); }
            },

            'shape/insert': {
                parent: ['document/editable/slideexists'],
                get() { return docModel.isInsertShapeActive(); },
                set(presetShape, inputOptions) {
                    if (getStringOption(inputOptions, 'sourceType') === 'keyboard') {
                        return docModel.insertShapeWithDefaultBox(presetShape);
                    } else if (docModel.getSlideTouchMode()) {
                        return docModel.insertShapeWithDefaultBox(presetShape, { selectDrawingOnly: true });
                    } else {
                        return docModel.insertShapePreparation(presetShape);
                    }
                }
            },

            // placeholders

            'placeholder/insert': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView(); }
            },

            'placeholder/title': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView(); },
                get() { return docModel.hasPlaceholderObjectOnSlide('title'); },
                set() { return docModel.togglePlaceholderObjectOnSlide('title'); }
            },

            'placeholder/body': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView() && docModel.isMasterSlideId(docModel.getActiveSlideId()); },
                get() { return docModel.hasPlaceholderObjectOnSlide('body'); },
                set() { return docModel.togglePlaceholderObjectOnSlide('body'); }
            },

            'placeholder/footers': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView(); },
                get() { return docModel.hasPlaceholderObjectOnSlide('footers'); },
                set() { return docModel.togglePlaceholderObjectOnSlide('footers'); }
            },

            'placeholder/insertcustom': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView() && !docModel.isMasterSlideId(docModel.getActiveSlideId()); },
                set(value, inputOptions) {
                    return docModel.insertPlaceholderDrawingPreparation(value, inputOptions);
                }
            },

            'placeholder/odfdatetime': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView() && docModel.isLayoutSlideId(docModel.getActiveSlideId()); },
                get() { return docModel.hasPlaceholderObjectOnSlide('dt'); },
                set() { return docModel.togglePlaceholderObjectOnSlide('dt'); }
            },

            'placeholder/odffooter': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView() && docModel.isLayoutSlideId(docModel.getActiveSlideId()); },
                get() { return docModel.hasPlaceholderObjectOnSlide('ftr'); },
                set() { return docModel.togglePlaceholderObjectOnSlide('ftr'); }
            },

            'placeholder/odfslidenum': {
                parent: ['document/editable/slideexists'],
                enable() { return docModel.isMasterView() && docModel.isLayoutSlideId(docModel.getActiveSlideId()); },
                get() { return docModel.hasPlaceholderObjectOnSlide('sldNum'); },
                set() { return docModel.togglePlaceholderObjectOnSlide('sldNum'); }
            },

            // insert a new slide, either blank or with a specifically chosen layout

            'layoutslidepicker/insertslide': {
                parent: ['document/editable'],
                enable() {
                    return (
                        !docModel.isMasterView() &&
                        (docModel.getMasterSlideCount() >= 1)
                    );
                },
                // - the `data-value` value of option buttons will be passed along with all
                //   form control events as payload - in this case acting as `slideId`.
                // - visa verse `get` will be able to preselect the corresponding layout slide
                //   of any currently selected standard slide from the former one's return value.
                get() {
                    return app.isODF() ? '' : docModel.getLayoutSlideId(docModel.getActiveSlideId());
                },
                set(slideId) {
                    //globalLogger.info('+++ slide/insertslide +++ slideId : ', slideId);
                    var UNDEFINED_VALUE;

                    if (slideId === 'blank_slide') {
                        slideId = UNDEFINED_VALUE;
                    }
                    docModel.insertSlide(slideId);
                }
            },

            // change respectively reset the currently selected slide's layout

            'layoutslidepicker/changelayout': {
                parent: ['layoutslidepicker/insertslide'],
                enable() { return !docModel.isEmptySlideView(); },
                set(slideId) {
                    docModel.changeLayoutForMultipleSlides(slideId, docModel.getSlidePaneSelection());
                }
            },

            // changing the master slide -> only possible in ODF
            'layoutslidepicker/changemaster': {
                parent: ['layoutslidepicker/insertslide'],
                enable() { return !docModel.isEmptySlideView(); },
                get() {
                    return docModel.getLayoutSlideId(docModel.getActiveSlideId());
                },
                set(slideId) {
                    docModel.changeODFMasterForMultipleSlides(slideId, docModel.getSlidePaneSelection());
                }
            },

            'slide/setbackground': {
                parent: 'document/editable/slideexists',
                set() { return docView.openSlideBackgroundDialog(); }
            },
            'slide/resetbackground': {
                enable() { return docModel.isSlideWithExplicitBackground(); },
                set() { docModel.resetBackground(); }
            },

            'debug/slideattributes': {
                parent: 'document/editable/slideexists',
                get() { return docModel.getAllFamilySlideAttributes(); }
            },
            'debug/hiddenslide': {
                parent: 'document/editable/slideexists',
                enable() { return !docModel.isMasterView(); },
                get() { return docModel.isHiddenSlide(); },
                set() { docModel.hideMultipleSlides(docModel.getSlidePaneSelection(), !docModel.isHiddenSlide()); }
            },
            'debug/followmastershapes': {
                parent: 'document/editable/slideexists',
                enable() { return !docModel.isMasterSlideId(docModel.getActiveSlideId()); },
                get() { return docModel.isFollowMasterShapeSlide(); },
                set() { docModel.setFollowMasterShape(!docModel.isFollowMasterShapeSlide()); }
            },

            'view/slidemasterview/close': {
                enable() { return docModel.isMasterView(); },
                set() { docModel.selectSlideView(); }
            },

            // presentation mode
            'present/startpresentation': {
                parent: 'document/slideexists',
                enable() { return !docModel.isEmptySlideView() && !docModel.isLayoutOrMasterId(docModel.getActiveSlideId()); },
                set(startMode) { docModel.getPresentationModeManager().startPresentation({ startMode }); }
            },
            'present/fullscreenpresentation': {
                get() { return docModel.getPresentationModeManager().useFullscreenPresentation(); },
                set(state) { docModel.getPresentationModeManager().setFullscreenPresentation(state); }
            },
            'present/slideeffectpresentation': {
                get() { return docModel.getPresentationModeManager().getActiveSlideEffectId() || 'NONE'; },
                set(id) { docModel.getPresentationModeManager().setSlideEffect(id); }
            },

            // fields
            updateField: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().updateHighlightedField(); }
            },
            updateAllFields: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().updateAllFields(); }
            },
            removeField: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().removeField(); }
            },
            'document/insertfooter/dialog': {
                parent: ['document/editable/slideexists', 'slide/noodfmasterview'],
                set() { return docModel.getFieldManager().openFooterDialog(); }
            },
            'document/insertfield': {
                parent: 'document/editable/slideexists',
                enable() { return !docModel.isMasterView() || (docModel.isMasterView() && !app.isODF()); },
                set(value) { docModel.getFieldManager().dispatchInsertField(value); }
            },
            insertSlideNumber: {
                parent: ['document/editable/slideexists', 'selection/editable'],
                set() { docModel.getFieldManager().dispatchInsertField('slidenum'); }
            },
            insertDateTime: {
                parent: ['document/editable/slideexists', 'selection/editable'],
                set() { docModel.getFieldManager().dispatchInsertField('datetime'); }
            },
            'document/formatfield': {
                parent: 'document/editable',
                get() { return docModel.getFieldManager().getSelectedFieldFormat(); },
                set(value) { docModel.getFieldManager().updateFieldFormatting(value); }
            },

            // slide pane
            slidePaneSetSize: {
                set(width) { docModel.setLocalViewSlidePaneWidth(width); }
            },

            slidePaneShowFirstSlide: {
                set() {  docModel.setActiveSlideId(docModel.getIdOfFirstSlideOfActiveView()); }
            },

            slidePaneShowLastSlide: {
                set() { docModel.setActiveSlideId(docModel.getIdOfLastSlideOfActiveView()); }
            },

            slidePaneSlideUp: {
                set() { docModel.changeToNeighbourSlide({ next: false }); }
            },

            slidePaneSlideDown: {
                set() { docModel.changeToNeighbourSlide({ next: true }); }
            },

            slidePaneSetActiveSlide: {
                set(id) { docModel.setActiveSlideId(id); }
            },

            slidePaneSetLastSlideActive: {
                set() { docModel.changeToLastSlideInView(); }
            },

            // android-entry for selection which cant be deleted by softkeyboard
            'document/editable/selection': {
                parent: 'document/editable',
                enable() {
                    var selection = docModel.getSelection();
                    return !_.isEqual(selection.getStartPosition(), selection.getEndPosition());
                }
            },
            'selection/delete': {
                parent: 'document/editable/selection',
                set() { return docModel.deleteSelected(); }
            },
            'selection/editable': {
                parent: 'document/editable',
                enable() { return !docModel.isODFReadOnyPlaceHolderDrawingProcessing(); }
            }

        });

        /**
         *  providing a `isOOXML` guard in order to fix Bug #66009 - ODP files fail to open: 'Cannot read property 'isVisible' of undefined'
         *  - [https://bugs.open-xchange.com/show_bug.cgi?id=66009]
         */
        if (app.isOOXML()) {
            ThreadedCommentMixin.call(this, docModel, docView);
        }

        // update GUI after changed selection
        this.updateOnEvent(docModel, 'selection');
    }
}

// exports ================================================================

export default PresentationController;
