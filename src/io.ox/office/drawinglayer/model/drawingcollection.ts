/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

import type { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import type { DrawingAttrSetConstraint, DrawingModelConfig, DrawingModelType, DrawingModelIterable, DrawingModelIterator } from "@/io.ox/office/drawinglayer/model/drawingmodel";

// types ======================================================================

/**
 * Options to resolve drawing models from a drawing collection.
 */
export interface DrawingModelOptions {

    /**
     * If specified, the drawing model must be of the specified type.
     */
    type?: DrawingType;
}

/**
 * Options to iterate over drawing models in a drawing collection.
 */
export interface DrawingModelDeepOptions {

    /**
     * If set to `true`, all drawing objects embedded deeply in other drawing
     * objects will be visited too. Default value is `false`.
     */
    deep?: boolean;
}

/**
 * Options to iterate over drawing models in a drawing collection.
 */
export interface YieldDrawingModelsOptions extends DrawingModelDeepOptions {

    /**
     * If set to `true`, only visible drawing objects will be visited. Default
     * value is `false`.
     */
    visible?: boolean;
}

/**
 * Detailed information about a drawing model that has been looked up in a
 * drawing collection.
 */
export interface DrawingModelInfo<DocModelT extends EditModel, ModelMixinT> {

    /**
     * The model of the drawing object, either directly from this collection,
     * or resolved from an embedded drawing object.
     */
    model: DrawingModelType<DocModelT, ModelMixinT>;

    /**
     * The exact document position of the drawing object in the collection (the
     * leading part of the document position used to look up the drawing
     * object).
     */
    position: Position;

    /**
     * The remaining part of the document position (trailing array elements)
     * used to look up the drawing object, addressing embedded content inside
     * the drawing object (e.g. text contents).
     */
    remaining: Opt<Position>;
}

/**
 * Type mapping for the events emitted by `DrawingCollection` instances.
 */
export interface DrawingCollectionEventMap<DocModelT extends EditModel, ModelMixinT> {

    /**
     * Will be emitted after a new drawing model has been inserted into a
     * drawing collection.
     *
     * @param drawingModel
     *  The new drawing model inserted into the drawing collection.
     */
    "insert:drawing": [drawingModel: DrawingModelType<DocModelT, ModelMixinT>];

    /**
     * Will be emitted before a drawing model will be removed from a drawing
     * collection.
     *
     * @param drawingModel
     *  The drawing model to be deleted from the drawing collection.
     */
    "delete:drawing": [drawingModel: DrawingModelType<DocModelT, ModelMixinT>];

    /**
     * Will be emitted after the contents or formatting attributes of a drawing
     * object have been changed.
     *
     * @param drawingModel
     *  The drawing model that caused the change event.
     *
     * @param changeType
     *  The exact type of the change event. The possible values are dependent
     *  on the type of the drawing object. The value "attributes" denotes
     *  changed formatting attributes.
     */
    "change:drawing": [drawingModel: DrawingModelType<DocModelT, ModelMixinT>, changeType: string];

    /**
     * Will be emitted after a drawing model in an indexed drawing collection
     * has been moved to a new index. May bubble up from embedded child
     * collections, e.g. from a group object.
     *
     * @param drawingModel
     *  The moved drawing model.
     *
     * @param fromIndex
     *  The old sorting index of the drawing model before it has been moved to
     *  its new position.
     */
    "move:drawing": [drawingModel: DrawingModelType<DocModelT, ModelMixinT>, fromIndex: number];
}

// class DrawingCollection ====================================================

/**
 * A collection containing models of drawing objects.
 */
export abstract class DrawingCollection<
    DocModelT extends EditModel,
    ModelMixinT,
    EvtMapT extends DrawingCollectionEventMap<DocModelT, ModelMixinT> = DrawingCollectionEventMap<DocModelT, ModelMixinT>
> extends ModelObject<DocModelT, EvtMapT> {

    /**
     * The ancestor top-level drawing collection that contains all top-level
     * drawing models; or a reference to this instance, if it is a top-level
     * drawing collection by itself.
     */
    readonly rootCollection: DrawingCollection<DocModelT, ModelMixinT>;

    /**
     * The direct parent drawing collection that contains the drawing model
     * owning this embedded drawing collection; or `undefined`, if this
     * instance is a root collection withoput parent.
     */
    readonly parentCollection: Opt<DrawingCollection<DocModelT, ModelMixinT>>;

    /**
     * The parent drawing model, if this instance is an embedded collection of
     * a group object; otherwise `undefined`.
     */
    readonly parentModel: Opt<DrawingModelType<DocModelT, ModelMixinT>>;

    /**
     * All drawing models, mapped by their unique object identifiers.
     */
    readonly #drawingModelSet = this.member(new Set<DrawingModelType<DocModelT, ModelMixinT>>());

    /**
     * Cached number of drawing models in this collection (faster than counting
     * map properties).
     */
    #modelCount = 0;

    // workaround for type errors with extensible event maps
    get #evt(): DrawingCollection<DocModelT, ModelMixinT> { return this as DrawingCollection<DocModelT, ModelMixinT>; }

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The document model containing this instance.
     *
     * @param [parentModel]
     *  The parent drawing model owning this instance as embedded drawing
     *  collection. If omitted, this instance will be a root collection without
     *  parent drawing model.
     */
    protected constructor(docModel: DocModelT, parentModel?: DrawingModelType<DocModelT, ModelMixinT>) {
        super(docModel);
        this.rootCollection = parentModel?.rootCollection ?? this;
        this.parentCollection = parentModel?.parentCollection;
        this.parentModel = parentModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the number of drawings contained in this drawing collection.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The number of drawing objects contained in this collection.
     */
    getModelCount(options?: DrawingModelDeepOptions): number {

        // the resulting model count (start with the cached number of own drawing models)
        let totalCount = this.#modelCount;

        // count all deeply embedded models if specified
        if (options?.deep) {
            for (const model of this.#drawingModelSet) {
                totalCount += model.childCollection?.getModelCount(options) ?? 0;
            }
        }

        return totalCount;
    }

    /**
     * Returns detailed information for a drawing model at the passed document
     * position.
     *
     * @param position
     *  The document position of the drawing model. May specify a position
     *  inside another drawing object (group objects), and may specify a child
     *  component inside a drawing object (e.g. text contents). This method
     *  traverses as deep as possible into the document position array, as long
     *  as the current drawing object contains child drawing objects.
     *
     * @returns
     *  The result data for en existing drawing model; otherwise `undefined`.
     */
    getModelInfo(position: Position): Opt<DrawingModelInfo<DocModelT, ModelMixinT>> {

        // find the root drawing object in this collection
        const rootDesc = this.#getRootModel(position);
        if (!rootDesc) { return undefined; }

        // no remaining elements in the position array: resulting drawing model found
        if (!rootDesc.remaining) { return rootDesc; }

        // resolve embedded drawing models, if there are remaining elements in the position array
        const { childCollection } = rootDesc.model;
        if (!childCollection) { return rootDesc; }

        // resolve an embedded drawing model (return null, if there is no such embedded drawing model)
        const embedInfo = childCollection.getModelInfo(rootDesc.remaining);
        if (!embedInfo) { return undefined; }

        // restore the complete position of the drawing model
        embedInfo.position = [...rootDesc.position, ...embedInfo.position];
        return embedInfo;
    }

    /**
     * Returns the drawing model at the passed document position.
     *
     * @param position
     *  The exact document position of the drawing model. May specify a
     *  position inside another drawing object (group objects).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The drawing model at the specified document position; or `undefined`,
     *  if no drawing model has been found.
     */
    getModel(position: Position, options?: DrawingModelOptions): Opt<DrawingModelType<DocModelT, ModelMixinT>> {

        // find the drawing model (do not accept document position arrays with trailing elements)
        const modelInfo = this.getModelInfo(position);
        if (!modelInfo || modelInfo.remaining) { return undefined; }

        // check the type of the drawing object if specified
        const { model } = modelInfo;
        const type = options?.type;
        return (!type || (type === model.drawingType)) ? model : undefined;
    }

    /**
     * Creates an iterator that visits all drawing models in this collection.
     * The drawing models will be visited in no specific order.
     *
     * @param [options]
     *  Optional parameters to filter the drawing models to be visited.
     *
     * @yields
     *  The matching drawing models.
     */
    *yieldModels(options?: YieldDrawingModelsOptions): DrawingModelIterator<DocModelT, ModelMixinT> {
        yield* this.implYieldModels(this.#drawingModelSet, options);
    }

    /**
     * Creates and stores the model of a new drawing object based on the passed
     * settings.
     *
     * @param position
     *  The document position of the new drawing model. May specify a position
     *  inside another drawing object (if the array without the last element
     *  points to an existing drawing object). If that drawing model supports
     *  embedding drawing objects, the new drawing model will be inserted into
     *  the drawing collection of that drawing object instead into this
     *  collection.
     *
     * @param drawingType
     *  The type of the drawing object to be created.
     *
     * @param [attrSet]
     *  An attribute set with initial formatting attributes for the drawing
     *  object.
     *
     * @param [modelConfig]
     *  Additional model settings to be passed to the model factory.
     *
     * @returns
     *  The new drawing model, if it has been created successfully; otherwise
     *  `undefined`.
     */
    insertModel<AttrSetT extends DrawingAttrSetConstraint<AttrSetT>>(
        position: Position,
        drawingType: DrawingType,
        attrSet?: PtAttrSet<AttrSetT>,
        modelConfig?: DrawingModelConfig<AttrSetT>
    ): Opt<DrawingModelType<DocModelT, ModelMixinT>> {

        // try to find a root drawing object in this collection to insert the new drawing into
        const modelInfo = this.#getRootModel(position);
        if (modelInfo?.remaining) {
            return modelInfo.model.childCollection?.insertModel(modelInfo.remaining, drawingType, attrSet, modelConfig);
        }

        // create and store the new drawing model
        const model = this.clientCreateModel(this, drawingType, attrSet, modelConfig);
        return (model && this.implInsertModel(position, model)) ? model : undefined;
    }

    /**
     * Removes the model of a drawing object located at the passed document
     * position.
     *
     * @param position
     *  The document position of the drawing model. May specify a position
     *  inside another drawing object (if the array without the last element
     *  points to an existing drawing object).
     *
     * @returns
     *  Whether the drawing model has been removed successfully.
     */
    deleteModel(position: Position): boolean {

        // find the root drawing object in this collection
        const modelInfo = this.#getRootModel(position);
        if (!modelInfo) { return false; }

        // remaining elements: delete drawing model in an embedded collection
        if (modelInfo.remaining) {
            return !!modelInfo.model.childCollection?.deleteModel(modelInfo.remaining);
        }

        // notify all listeners
        this.#evt.trigger("delete:drawing", modelInfo.model);

        // let the client application unregister the deleted drawing model
        if (!this.clientUnregisterModel(modelInfo.model, modelInfo.position)) {
            return false;
        }

        // remove and destroy the drawing model
        this.#drawingModelSet.delete(modelInfo.model);
        modelInfo.model.destroy();
        this.#modelCount -= 1;

        return true;
    }

    // client API methods -----------------------------------------------------

    /**
     * A factory function that constructs drawing model instances for the
     * specified drawing type.
     *
     * @param parentCollection
     *  The parent drawing collection that will contain the new drawing object
     *  (may be this instance, or an embedded drawing collection of a group
     *  object).
     *
     * @param drawingType
     *  The type of the drawing model to be created.
     *
     * @param [attrSet]
     *  The initial formatting attributes for the new drawing model.
     *
     * @param [modelConfig]
     *  Additional model settings to be passed to the model constructor.
     *
     * @returns
     *  A new instance of a drawing model according to the passed type, or
     *  `undefined` for unsupported drawing types.
     */
    abstract clientCreateModel<AttrSetT extends DrawingAttrSetConstraint<AttrSetT>>(
        parentCollection: DrawingCollection<DocModelT, ModelMixinT>,
        drawingType: DrawingType,
        attrSet?: PtAttrSet<AttrSetT>,
        modelConfig?: DrawingModelConfig<AttrSetT>
    ): Opt<DrawingModelType<DocModelT, ModelMixinT>>;

    /**
     * Must be implemented by subclasses to resolve a drawing model at a
     * specific document position.
     *
     * @param position
     *  The document position of a drawing object in this collection.
     *
     * @returns
     *  The model of a drawing object at the specified position, if available;
     *  otherwise `undefined`.
     */
    abstract clientResolveModel(position: Position): Opt<DrawingModelType<DocModelT, ModelMixinT>>;

    /**
     * Must be implemented by subclasses to resolve the position of a specific
     * drawing model.
     *
     * @param drawingModel
     *  An arbitrary drawing model.
     *
     * @returns
     *  The position of the passed drawing model, if it is part of this drawing
     *  collection, otherwise `undefined`.
     */
    abstract clientResolvePosition(drawingModel: DrawingModelType<DocModelT, ModelMixinT>): Opt<Position>;

    /**
     * Must be implemented by subclasses to resolve the array index of a
     * drawing model in the list of all its siblings, which is used to define
     * the rendering order ("Z order").
     *
     * @param drawingModel
     *  An arbitrary drawing model.
     *
     * @returns
     *  The Z order index of the passed drawing model.
     */
    abstract clientResolveZIndex(model: DrawingModelType<DocModelT, ModelMixinT>): number;

    /**
     * Must be implemented by subclasses to register a new drawing model that
     * has just been inserted into this drawing collection.
     *
     * @param drawingModel
     *  The new drawing model.
     *
     * @param position
     *  The position of the new drawing object.
     *
     * @returns
     *  Whether the passed position is valid, and the drawing model has been
     *  registered successfully.
     */
    protected abstract clientRegisterModel(drawingModel: DrawingModelType<DocModelT, ModelMixinT>, position: Position): boolean;

    /**
     * Must be implemented by subclasses to unregister a drawing model that has
     * just been removed from this drawing collection.
     *
     * @param drawingModel
     *  The drawing model about to be removed from this collection.
     *
     * @param position
     *  The position of the drawing object.
     *
     * @returns
     *  Whether the passed position is valid, and the drawing model has been
     *  unregistered successfully.
     */
    protected abstract clientUnregisterModel(drawingModel: DrawingModelType<DocModelT, ModelMixinT>, position: Position): boolean;

    // protected methods ------------------------------------------------------

    /**
     * Stores the passed new drawing model into this drawing collection.
     *
     * @param position
     *  The document position of the new drawing model.
     *
     * @param model
     *  The model instance of the new drawing object. This collection takes
     *  ownership of this drawing model!
     *
     * @returns
     *  Whether the new drawing model has been inserted successfully.
     */
    protected implInsertModel(position: Position, model: DrawingModelType<DocModelT, ModelMixinT>): boolean {

        // register the new drawing model at the client application
        if (!this.clientRegisterModel(model, position)) { return false; }

        // store the new drawing model
        this.#drawingModelSet.add(model);
        this.#modelCount += 1;

        // notify all listeners
        this.#evt.trigger("insert:drawing", model);

        // forward attribute change events of the drawing model to own listeners
        model.on("change:attributes", () => this.#evt.trigger("change:drawing", model, "attributes"));

        // forward custom change events of the drawing model to own listeners
        model.on("change:drawing", changeType => this.#evt.trigger("change:drawing", model, changeType));

        // forward events of the embedded drawing collection to own listeners
        if (model.childCollection) {
            this.listenToAllEvents(model.childCollection, (type, ...args) => this.#evt.trigger(type, ...args));
        }

        return true;
    }

    /**
     * Implementation helper for subclasses that creates the effective model
     * iterator filtering and extending the passed raw child model iterator
     * according to the passed options.
     *
     * @param iterable
     *  The data source providing the drawing models.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The matching drawing models.
     */
    protected *implYieldModels(iterable: DrawingModelIterable<DocModelT, ModelMixinT>, options?: YieldDrawingModelsOptions): DrawingModelIterator<DocModelT, ModelMixinT> {

        // process all drawing models in the iterator
        for (const drawingModel of iterable) {

            // filter for visible drawing models if specified
            if (options?.visible && !drawingModel.isVisible()) {
                continue;
            }

            // first, visits the drawing model itself
            yield drawingModel;

            // visit the embedded child models of the current drawing model
            if (options?.deep) {
                yield* drawingModel.childModels(options);
            }
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Tries to find the root drawing object inside this drawing collection
     * specified by some leading elements of the passed position array.
     *
     * @param position
     *  The document position of a drawing model, with arbitrary length.
     *
     * @returns
     *  The result data of an existing drawing model.
     */
    #getRootModel(position: Position): Opt<DrawingModelInfo<DocModelT, ModelMixinT>> {

        for (let length = 1; length <= position.length; length += 1) {

            // the leading part of the passed drawing position
            const rootPos = position.slice(0, length) as Position;

            // let the client resolve the drawing model by its position
            const model = this.clientResolveModel(rootPos);
            if (!model) { continue; }

            // ensure that the model is part of this collection
            if (!this.#drawingModelSet.has(model)) {
                globalLogger.warn("$badge{DrawingCollection} getRootModel: unregistered drawing model");
                return undefined;
            }

            // the remaining part of the position
            const remaining = (length < position.length) ? position.slice(length) as Position : undefined;

            // return the drawing model and its exact position
            return { model, position: rootPos, remaining };
        }

        return undefined;
    }
}
