/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

import type { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import type { DrawingAttrSetConstraint, DrawingModelConfig, DrawingModelType } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import type { DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";
import { IndexedCollection } from "@/io.ox/office/drawinglayer/model/indexedcollection";

// class EmbeddedCollection ===================================================

/**
 * Represents a drawing collection that is embedded in a drawing model, used
 * e.g. by group objects.
 *
 * @param parentModel
 *  The parent drawing model that contains this embedded collection.
 */
export class EmbeddedCollection<DocModelT extends EditModel, ModelMixinT> extends IndexedCollection<DocModelT, ModelMixinT> {

    // constructor ------------------------------------------------------------

    constructor(parentModel: DrawingModelType<DocModelT, ModelMixinT>) {
        super(parentModel.docModel, parentModel);
    }

    // client API methods -----------------------------------------------------

    /**
     * Constructs a drawing model instance for the specified drawing type.
     *
     * @param parentCollection
     *  The parent drawing collection that will contain the new drawing
     *  object.
     *
     * @param drawingType
     *  The type of the drawing model to be created.
     *
     * @param [attrSet]
     *  Initial formatting attributes for the drawing model.
     *
     * @returns
     *  The new drawing model instance, if the passed type is supported.
     */
    clientCreateModel<AttrSetT extends DrawingAttrSetConstraint<AttrSetT>>(
        parentCollection: DrawingCollection<DocModelT, ModelMixinT>,
        drawingType: DrawingType,
        attrSet?: PtAttrSet<AttrSetT>,
        modelConfig?: DrawingModelConfig<AttrSetT>
    ): Opt<DrawingModelType<DocModelT, ModelMixinT>> {
        // use the factory of the root collection to create the drawing models
        return this.rootCollection.clientCreateModel(parentCollection, drawingType, attrSet, modelConfig);
    }
}
