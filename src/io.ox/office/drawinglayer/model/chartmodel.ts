/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { type DrawingFillAttributes, type DrawingAttributeSet, DrawingModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import type { DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";

// types ======================================================================

export interface ChartAttributes { }

/**
 * The type shape of attribute sets supported by chart objects.
 */
export interface ChartAttributeSet extends DrawingAttributeSet {
    chart: ChartAttributes;
    fill: DrawingFillAttributes;
}

/**
 * Partial chart attribute set (all families and attributes are optional).
 */
export type PtChartAttributeSet = PtAttrSet<ChartAttributeSet>;

// class ChartModel ===========================================================

/**
 * The model of a chart object.
 *
 * Additionally to the events triggered by the base class `DrawingModel`,
 * instances of this class trigger the following "change:drawing" events:
 *
 * - "insert:series" -- After a new data series has been inserted into this
 *   chart. Event handlers receive the array index of the new data series.
 *
 * - "delete:series" -- After an existing data series has been deleted from
 *   this chart. Event handlers receive the last array index of the data
 *   series.
 *
 * - "change:series" -- After an existing data series has been changed in this
 *   chart. Event handlers receive the array index of the data series.
 *
 * @param parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param [attrSet]
 *  An attribute set with initial formatting attributes for the object.
 */
export class ChartModel<DocModelT extends EditModel, MixinT> extends DrawingModel<DocModelT, ChartAttributeSet, MixinT> {
    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, attrSet?: PtChartAttributeSet) {
        super(parentCollection, DrawingType.CHART, attrSet, { families: ["chart", "fill"] });
    }
}
