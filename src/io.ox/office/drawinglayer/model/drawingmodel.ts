/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ArrayCallbackFn, itr, ary } from "@/io.ox/office/tk/algorithms";
import { Rectangle } from "@/io.ox/office/tk/dom";

import type { AttrFamiliesOf, AttrSetConstraint, PtAttrSet, StyledAttributeSet, CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import type { OpColor } from "@/io.ox/office/editframework/utils/color";
import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { OperationContext } from "@/io.ox/office/editframework/model/operation/context";
import { type AttributedModelEventMap, AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

import { DrawingType, isTwoPointShapeAttributes } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import type { DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";
import type { YieldIndexedDrawingModelsOptions, IndexedCollection } from "@/io.ox/office/drawinglayer/model/indexedcollection";
import { EmbeddedCollection } from "@/io.ox/office/drawinglayer/model/embeddedcollection";
import type { ChartAttributes } from "@/io.ox/office/drawinglayer/model/chartmodel";

// types ======================================================================

/**
 * The type of the attributes map of the "drawing" family.
 */
export interface DrawingAttributes {
    left: number;
    top: number;
    width: number;
    height: number;
    name: string;
    description: string;
    rotation: number;
    flipH: boolean;
    flipV: boolean;
    hidden: boolean;
    aspectLocked: boolean;
    noSelect: boolean;
    childLeft: number;
    childTop: number;
    childWidth: number;
    childHeight: number;
    replacementData: string;
}

/**
 * The type of the attributes map of the "fill" family.
 */
export interface DrawingFillAttributes {
    type: string;
    color: OpColor;
    color2: OpColor;
    gradient: Dict; // TODO
    bitmap: Dict; // TODO
    pattern: string;
}

/**
 * The type of the attributes map of the "line" family.
 */
export interface DrawingLineAttributes extends DrawingFillAttributes {
    width: number;
    style: string;
    headEndType: string;
    headEndLength: string;
    headEndWidth: string;
    tailEndType: string;
    tailEndLength: string;
    tailEndWidth: string;
}

/**
 * The type of the attributes map of the "text" family.
 */
export interface DrawingTextAttributes {
    link: string | Array<number | string>;
}

/**
 * The type of the attributes map of the "shape" family.
 */
export interface ShapeAttributes {
    paddingLeft: number;
    paddingRight: number;
    paddingTop: number;
    paddingBottom: number;
    anchor: string;
    anchorCentered: boolean;
    wordWrap: boolean;
    horzOverflow: string;
    vertOverflow: string;
    autoResizeHeight: boolean;
    autoResizeText: boolean;
    fontScale: number;
    lineReduction: number;
    noAutoResize: boolean;
    vert: string;
}

/**
 * The type of the attributes map of the "geometry" family.
 */
export interface GeometryAttributes {
    presetShape: string;
    avList: unknown[];
}

/**
 * The type of the attributes map of the "image" family.
 */
export interface ImageAttributes {
    imageUrl: string;
    imageData: string;
    cropLeft: number;
    cropRight: number;
    cropTop: number;
    cropBottom: number;
}

/**
 * The type shape of attribute sets supported by all types of drawing objects.
 */
export interface DrawingAttributeSet extends StyledAttributeSet {
    drawing: DrawingAttributes;
}

/**
 * Partial drawing attribute set (all families and attributes are optional).
 */
export type PtDrawingAttributeSet = PtAttrSet<DrawingAttributeSet>;

/**
 * The type shape of attribute sets supported by shape objects.
 */
export interface ShapeAttributeSet extends DrawingAttributeSet {
    fill: DrawingFillAttributes;
    line: DrawingLineAttributes;
    text: DrawingTextAttributes;
    shape: ShapeAttributes;
    geometry: GeometryAttributes;
    character: CharacterAttributes;
}

/**
 * Partial shape attribute set (all families and attributes are optional).
 */
export type PtShapeAttributeSet = PtAttrSet<ShapeAttributeSet>;

/**
 * The type shape of attribute sets supported by connector objects.
 */
export interface ConnectorAttributeSet extends DrawingAttributeSet {
    line: DrawingLineAttributes;
    shape: ShapeAttributes;
    geometry: GeometryAttributes;
}

/**
 * Partial connector attribute set (all families and attributes are optional).
 */
export type PtConnectorAttributeSet = PtAttrSet<ConnectorAttributeSet>;

/**
 * The type shape of attribute sets supported by image objects.
 */
export interface ImageAttributeSet extends DrawingAttributeSet {
    fill: DrawingFillAttributes;
    line: DrawingLineAttributes;
    image: ImageAttributes;
}

/**
 * Partial image attribute set (all families and attributes are optional).
 */
export type PtImageAttributeSet = PtAttrSet<ImageAttributeSet>;

/**
 * The type shape of attribute sets supported by undefined objects.
 */
export interface UndefinedAttributeSet extends DrawingAttributeSet {
    image: ImageAttributes;
}

/**
 * Partial image attribute set (all families and attributes are optional).
 */
export type PtUndefinedAttributeSet = PtAttrSet<UndefinedAttributeSet>;

export type DrawingAttrSetConstraint<AttrSetT> = AttrSetConstraint<AttrSetT> & DrawingAttributeSet;

export interface DrawingAttributePoolMap {
    drawing:    [DrawingAttributes];
    fill:       [DrawingFillAttributes];
    line:       [DrawingLineAttributes];
    text:       [DrawingTextAttributes];
    shape:      [ShapeAttributes];
    geometry:   [GeometryAttributes];
    character:  [CharacterAttributes];
    image:      [ImageAttributes];
    chart:      [ChartAttributes];
}

/**
 * Generic configuration of a drawing model.
 */
export interface DrawingModelConfig<AttrSetT extends DrawingAttrSetConstraint<AttrSetT>> {

    /**
     * Additional explicit attribute families supported by this drawing model
     * object. The main attribute family "drawing" supported by all drawing
     * objects will be registered implicitly, and does not have to be added
     * here.
     */
    families?: ReadonlyArray<AttrFamiliesOf<AttrSetT>>;

    /**
     * If set to `true`, the drawing model will contain an embedded drawing
     * model collection, and it will be possible to insert embedded drawing
     * objects into the drawing model. Default value is `false`.
     */
    children?: boolean;

    /**
     * If set to `true`, the drawing model can be rotated and flipped, i.e. the
     * attributes "rotation", "flipH", and "flipV" will have an effect when
     * rendering the drawing object. Default value is `false`.
     */
    rotatable?: boolean;
}

/**
 * Effective flipping settings of a drawing object.
 */
export interface DrawingFlippingInfo {

    /**
     * Whether this drawing model is effectively flipped horizontally (i.e.
     * whether the number of "flipH" attributes of this drawing model and all
     * its ancestor models is an odd number).
     */
    flipH: boolean;

    /**
     * Whether this drawing model is effectively flipped vertically (i.e.
     * whether the number of "flipV" attributes of this drawing model and all
     * its ancestor models is an odd number).
     */
    flipV: boolean;

    /**
     * Whether the rotation angle appears reversed, i.e. whether only exactly
     * one of the effective flipping flags is active.
     */
    reverseRot: boolean;
}

/**
 * Type definition of a `DrawingModel` with additional methods from `MixinT`.
 */
export type DrawingModelType<DocModelT extends EditModel, MixinT> = DrawingModel<DocModelT, DrawingAttributeSet, MixinT> & MixinT;

/**
 * Type definition of an iterable for `DrawingModel` instances with additional
 * methods from `MixinT`.
 */
export type DrawingModelIterable<DocModelT extends EditModel, MixinT> = Iterable<DrawingModelType<DocModelT, MixinT>>;

/**
 * Type definition of an iterator for `DrawingModel` instances with additional
 * methods from `MixinT`.
 */
export type DrawingModelIterator<DocModelT extends EditModel, MixinT> = IterableIterator<DrawingModelType<DocModelT, MixinT>>;

/**
 * Type mapping for the events emitted by `DrawingCollection` instances.
 */
export interface DrawingModelEventMap<AttrSetT extends AttrSetConstraint<AttrSetT>> extends AttributedModelEventMap<AttrSetT> {

    /**
     * Will be emitted after some contents of a drawing model have been changed
     * (dependent on the type of the drawing object).
     *
     * @param changeType
     *  The exact type of the change event.
     */
    "change:drawing": [changeType: string];
}

// private types --------------------------------------------------------------

interface AbsoluteLocationInfo {
    rect: Rectangle;
    flipH: boolean;
    flipV: boolean;
    aRad: number;
}

// constants ==================================================================

const PI = Math.PI;
const PI_180 = PI / 180;

// private functions ==========================================================

function getEffectiveAngle(aRad: number, flipH: boolean, revRot: boolean): number {
    return (revRot ? -aRad : aRad) + (flipH ? PI : 0);
}

// class DrawingModel =========================================================

/**
 * The base class for the model of a drawing object.
 *
 * @template DocModelT
 *  The type of the document model containing the drawing model.
 *
 * @template MixinT
 *  Additional methods that will be mixed into all classes of drawing objects
 *  handled by the client application.
 *
 * @template AttrSetT
 *  The type of the attribute sets supported by model subclasses which may add
 *  additional attribute families to the interface `DrawingAttributeSet`.
 */
export class DrawingModel<
    DocModelT extends EditModel,
    AttrSetT extends DrawingAttrSetConstraint<AttrSetT> = DrawingAttributeSet,
    MixinT = Empty
> extends AttributedModel<DocModelT, DrawingAttributePoolMap, AttrSetT, DrawingModelEventMap<AttrSetT>> {

    /**
     * The ancestor top-level drawing collection that contains all top-level
     * drawing models.
     */
    readonly rootCollection: DrawingCollection<DocModelT, MixinT>;

    /**
     * The parent drawing collection that owns this drawing model.
     */
    readonly parentCollection: DrawingCollection<DocModelT, MixinT>;

    /**
     * The child drawing collection contained in this instance; or `undefined`,
     * if this drawing object does not contain a child collection.
     */
    readonly childCollection: Opt<IndexedCollection<DocModelT, MixinT>>;

    /**
     * The parent drawing model, if this instance is a group object; otherwise
     * `undefined`.
     */
    readonly parentModel: Opt<DrawingModelType<DocModelT, MixinT>>;

    /**
     * The ancestor top-level drawing model containing this instance as
     * embedded drawing model; or a reference to this drawing model, if it is a
     * top-level drawing model by itself.
     */
    readonly rootModel: DrawingModelType<DocModelT, MixinT>;

    /**
     * The type of this drawing model, as specified by the document operation
     * that has created the drawing model.
     */
    readonly drawingType: DrawingType;

    /**
     * Whether this drawing model is embedded in another drawing model (e.g. a
     * group object).
     */
    readonly isEmbedded: boolean;

    /**
     * Whether this model can be rotated and flipped.
     */
    readonly isRotatable: boolean;

    // constructor ------------------------------------------------------------

    /**
     * @param parentCollection
     *  The parent drawing collection that will contain this drawing object.
     *
     * @param drawingType
     *  The type of this drawing object.
     *
     * @param [attrSet]
     *  An attribute set with initial formatting attributes for the drawing object.
     *
     * @param [config]
     *  Configuration settings.
     */
    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, drawingType: DrawingType, attrSet?: PtAttrSet<AttrSetT>, config?: DrawingModelConfig<AttrSetT>) {

        super(parentCollection.docModel, attrSet, {
            styleFamily: "drawing",
            families: ary.concat<KeysOf<AttrSetT>>("drawing", config?.families),
            skipStyleFamilies: true // attribute families need to be specified individually by drawing type
        });

        this.rootCollection = parentCollection.rootCollection;
        this.parentCollection = parentCollection;
        this.childCollection = config?.children ? this.member(new EmbeddedCollection(this.ref)) : undefined;

        this.parentModel = parentCollection.parentModel;
        this.rootModel = this.parentModel?.rootModel ?? this.ref;

        this.drawingType = drawingType;
        this.isEmbedded = !!this.parentModel;
        this.isRotatable = !!config?.rotatable;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the merged drawing attributes (the attributes of the "drawing"
     * family) of this model.
     *
     * @returns
     *  The merged drawing attributes of this model.
     */
    getDrawingAttrs(direct: true): Readonly<DrawingAttributes>;
    getDrawingAttrs(direct?: boolean): DrawingAttributes;
    // implementation
    getDrawingAttrs(direct?: boolean): DrawingAttributes {
        return this.getMergedAttributeSet(direct).drawing;
    }

    /**
     * Whether this drawing model can be restored completely by document
     * operations after deletion. This method may be overwritten by subclasses.
     *
     * @returns
     *  Whether this drawing model can be restored completely by document
     *  operations after deletion.
     */
    isRestorable(): boolean {
        return this.drawingType !== DrawingType.UNDEFINED;
    }

    /**
     * Returns whether this drawing model is a line/connector shape (i.e. it
     * will be selected in two-point mode instead of the standard rectangle
     * mode).
     *
     * @returns
     *  Whether this drawing model is a line/connector shape.
     */
    isTwoPointShape(): boolean {
        return false;
    }

    /**
     * Returns whether this drawing model is in visible state (the attribute
     * "hidden" is not set to true).
     *
     * @returns
     *  Whether this drawing model is in visible state.
     */
    isVisible(): boolean {
        return !this.getDrawingAttrs(true).hidden;
    }

    /**
     * Returns whether this drawing object keeps its width/height ratio during
     * resizing (i.e. the value of the formatting attribute "aspectLocked").
     *
     * @returns
     *  The value of the formatting attribute "aspectLocked".
     */
    isAspectLocked(): boolean {
        return this.getDrawingAttrs(true).aspectLocked;
    }

    /**
     * Returns whether this drawing object has been flipped horizontally (i.e.
     * the value of the formatting attribute "flipH").
     *
     * @returns
     *  The value of the formatting attribute "flipH".
     */
    isFlippedH(): boolean {
        return this.isRotatable && this.getDrawingAttrs(true).flipH;
    }

    /**
     * Returns whether this drawing model has been flipped vertically (i.e. the
     * value of the formatting attribute "flipV").
     *
     * @returns
     *  The value of the formatting attribute "flipV".
     */
    isFlippedV(): boolean {
        return this.isRotatable && this.getDrawingAttrs(true).flipV;
    }

    /**
     * Returns the value of the formatting attribute 'rotation' (i.e. the
     * rotation angle in degrees).
     *
     * @returns
     *  The value of the formatting attribute "rotation", in degrees.
     */
    getRotationDeg(): number {
        return this.isRotatable ? this.getDrawingAttrs(true).rotation : 0;
    }

    /**
     * Returns the rotation angle of this drawing mdoel in radians (i.e. the
     * value of the formatting attribute "rotation").
     *
     * @returns
     *  The value of the formatting attribute "rotation", in radians.
     */
    getRotationRad(): number {
        return this.getRotationDeg() * PI_180;
    }

    /**
     * Creates an iterator that visits all drawing models embedded in the child
     * collection of this drawing model.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The matching child models.
     */
    *childModels(options?: YieldIndexedDrawingModelsOptions): DrawingModelIterator<DocModelT, MixinT> {
        if (this.childCollection) {
            yield* this.childCollection.yieldModels(options);
        }
    }

    /**
     * Invokes the passed callback function for all direct child models that
     * are contained in the child drawing collection of this drawing model.
     *
     * @param fn
     *  The callback function that will be invoked for each child drawing model
     *  contained in the child collection of this drawing model. Receives the
     *  child drawing model currently visited as first parameter, and the array
     *  index of the visited child drawing model in the child collection.
     */
    forEachChildModel(fn: ArrayCallbackFn<void, DrawingModelType<DocModelT, MixinT>>): void {
        if (this.childCollection) {
            for (const [index, model] of this.childCollection.yieldModelEntries()) {
                fn(model, index);
            }
        }
    }

    /**
     * Returns the document position of this drawing model.
     *
     * @returns
     *  The document position of the drawing object.
     *
     * @throws
     *  A `ReferenceError`, if this drawing object is not contained in its
     *  parent collection.
     */
    getPosition(): Position {

        // get the position of this drawing model in its parent collection
        let position = this.parentCollection.clientResolvePosition(this.ref);
        if (!position) { throw new ReferenceError("detached drawing object"); }

        // add the position of all parent drawing models, if this is an embedded model
        if (this.parentModel) {
            const parentPosition = this.parentModel.getPosition();
            position = [...parentPosition, ...position];
        }

        return position;
    }

    /**
     * Returns the own array index of this drawing model in the list of all its
     * siblings. This index is used to define the rendering order ("Z order").
     *
     * @returns
     *  The own index in the list of the siblings of this drawing model.
     */
    getZIndex(): number {
        return this.parentCollection.clientResolveZIndex(this.ref);
    }

    /**
     * Returns the location of this drawing model relative to its parent area,
     * in 1/100 of millimeters, regardless of its current flipping and rotation
     * attributes.
     *
     * @returns
     *  The location of this drawing model in its parent area.
     */
    getRectangleHmm(): Rectangle {

        // the raw position from the drawing attributes of this drawing model
        const rectangle = Rectangle.from(this.getDrawingAttrs(true));
        // directly resolve location of top-level drawing objects from the attributes
        if (!this.parentModel) { return rectangle; }

        // the location of the parent drawing model, in 1/100 mm
        const parentRectHmm = this.parentModel.getRectangleHmm();
        // the drawing attributes of the parent drawing model
        const parentAttrs = this.parentModel.getDrawingAttrs(true);

        // move relative to parent coordinates
        rectangle.translateSelf(-parentAttrs.childLeft, -parentAttrs.childTop);

        // scale by parent coordinate system
        const widthRatio = (parentAttrs.childWidth > 0) ? (parentRectHmm.width / parentAttrs.childWidth) : 0;
        const heightRatio = (parentAttrs.childHeight > 0) ? (parentRectHmm.height / parentAttrs.childHeight) : 0;
        rectangle.scaleSelf(widthRatio, heightRatio).roundSelf();

        return rectangle;
    }

    /**
     * Returns whether this drawing model and all its embedded descendant
     * drawing models can be completely restored by document operations after
     * deletion.
     *
     * @returns
     *  Whether this drawing model and all its embedded descendants can be
     *  completely restored by document operations after deletion.
     */
    isDeepRestorable(): boolean {
        return this.isRestorable() && itr.every(this.childModels(), childModel => childModel.isDeepRestorable());
    }

    /**
     * Returns whether this drawing model and all its embedded descendant
     * drawing models are rotatable.
     *
     * @returns
     *  Whether this drawing model and all its embedded descendants are
     *  rotatable.
     */
    isDeepRotatable(): boolean {
        return this.isRotatable && itr.every(this.childModels(), childModel => childModel.isDeepRotatable());
    }

    /**
     * Returns whether this drawing model is effectively visible (i.e. the
     * drawing model itself, and all of its parent models, are visible).
     *
     * @returns
     *  Returns whether this drawing model is effectively visible.
     */
    isEffectivelyVisible(): boolean {
        for (let model: Opt<DrawingModelType<DocModelT, MixinT>> = this.ref; model; model = model.parentModel) {
            if (!model.isVisible()) { return false; }
        }
        return true;
    }

    /**
     * Returns the effective flipping settings of this drawing model, acording
     * to the flipping attributes of this drawing model and all its ancestors.
     *
     * @returns
     *  A result object with the effective flipping settings.
     */
    getEffectiveFlipping(): DrawingFlippingInfo {

        // the current flipping states
        let flipH = false;
        let flipV = false;

        // process this drawing model, and the entire chain of ancestors
        for (let model: Opt<DrawingModelType<DocModelT, MixinT>> = this.ref; model; model = model.parentModel) {
            flipH = flipH !== model.isFlippedH();
            flipV = flipV !== model.isFlippedV();
        }

        // rotation is reversed if exactly one flipping flag is active
        return { flipH, flipV, reverseRot: flipH !== flipV };
    }

    /**
     * Returns the effective absolute location of this drawing model in the
     * root area of all top-level drawings, in 1/100 of millimeters. The
     * rotation angle of the drawing object and all of its ancestor group
     * objects will be taken into account (this method will return the bounding
     * rectangle of the rotated drawing at its effective position, according to
     * the rotation of the parent groups). The flipping attributes of group
     * objects will be determined when calculating the absolute position of
     * their embedded children.
     *
     * @returns
     *  The effective absolute location of this drawing model in the root area
     *  of all top-level drawings (the bounding box, if the rectangle is
     *  effectively rotated).
     */
    getEffectiveRectangleHmm(): Rectangle {

        // reesolve the effective location recursively through the chain of ancestors
        const locationData = this.#resolveAbsoluteLocation();

        // expand to the bounding box of the effectively rotated drawing model
        return locationData.rect.rotatedBoundingBox(locationData.aRad).roundSelf();
    }

    // operation handlers -----------------------------------------------------

    /**
     * Handler for the document operation "changeDrawing".
     *
     * @param context
     *  A wrapper representing the "changeDrawing" operation.
     */
    applyChangeOperation(context: OperationContext): void {
        this.setAttributes(context.getDict("attrs") as PtAttrSet<AttrSetT>);
    }

    // protected getters ------------------------------------------------------

    /**
     * Helper for correctly typed `this` including `MixinT`.
     */
    protected get ref(): DrawingModelType<DocModelT, MixinT> {
        return this as unknown as DrawingModelType<DocModelT, MixinT>;
    }

    // private methods --------------------------------------------------------

    #resolveAbsoluteLocation(): AbsoluteLocationInfo {

        // the location of the current drawing object relative to its parent
        const rect = this.getRectangleHmm();
        // flipping flags of the drawing model
        let flipH = this.isFlippedH();
        let flipV = this.isFlippedV();
        // effective rotation of the positive X axis, according to flipping
        let aRad: number;

        // calculate the location of embedded drawing model according to its parent
        if (this.parentModel) {

            // resolve all positioning data of the parent model recursively
            const parentData = this.parentModel.#resolveAbsoluteLocation();

            // the unrotated/unflipped absolute center point of the drawing model
            const cx = parentData.rect.left + rect.centerX();
            const cy = parentData.rect.top + rect.centerY();
            // polar coordinates of the unrotated/unflipped absolute center point
            const polar = parentData.rect.pointToPolar(cx, cy);
            // effective absolute angle of the polar center point, according to parent rotation/flipping
            const revRot = parentData.flipH !== parentData.flipV;
            polar.a = parentData.aRad + (revRot ? -polar.a : polar.a);
            // effective absolute position of the center point
            const center = parentData.rect.polarToPoint(polar);
            // update the effective absolute location of the drawing model
            rect.left = center.x - rect.width / 2;
            rect.top = center.y - rect.height / 2;

            // effective flipping and rotation of the drawing model
            flipH = parentData.flipH !== flipH;
            flipV = parentData.flipV !== flipV;
            aRad = parentData.aRad + getEffectiveAngle(this.getRotationRad(), this.isFlippedH(), flipH !== flipV);

        } else {

            // effective rotation of top-level drawings
            aRad = getEffectiveAngle(this.getRotationRad(), flipH, flipH !== flipV);
        }

        return { rect, flipH, flipV, aRad };
    }
}

// class ShapeModel ===========================================================

const SHAPE_FAMILIES = ["shape", "geometry", "fill", "line", "character", "text"] as const;

/**
 * The model of a shape object.
 *
 * @param parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param [attrSet]
 *  An attribute set with initial formatting attributes for the object.
 */
export class ShapeModel<DocModelT extends EditModel, MixinT> extends DrawingModel<DocModelT, ShapeAttributeSet, MixinT> {

    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, attrSet?: PtShapeAttributeSet) {
        super(parentCollection, DrawingType.SHAPE, attrSet, { families: SHAPE_FAMILIES, rotatable: true });
    }

    override isTwoPointShape(): boolean {
        return isTwoPointShapeAttributes(this.getMergedAttributeSet(true).geometry);
    }
}

// class ConnectorModel =======================================================

const CONNECTOR_FAMILIES = ["shape", "geometry", "line"] as const;

/**
 * The model of a connector line object.
 *
 * @param parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param [attrSet]
 *  An attribute set with initial formatting attributes for the object.
 */
export class ConnectorModel<DocModelT extends EditModel, MixinT> extends DrawingModel<DocModelT, ConnectorAttributeSet, MixinT> {

    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, attrSet?: PtConnectorAttributeSet) {
        super(parentCollection, DrawingType.CONNECTOR, attrSet, { families: CONNECTOR_FAMILIES, rotatable: true });
    }

    override isTwoPointShape(): boolean {
        return true;
    }
}

// class ImageModel ===========================================================

const IMAGE_FAMILIES = ["image", "line", "fill"] as const;

/**
 * The model of a picture object.
 *
 * @param parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param [attrSet]
 *  An attribute set with initial formatting attributes for the object.
 */
export class ImageModel<DocModelT extends EditModel, MixinT> extends DrawingModel<DocModelT, ImageAttributeSet, MixinT> {
    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, attrSet?: PtImageAttributeSet) {
        super(parentCollection, DrawingType.IMAGE, attrSet, { families: IMAGE_FAMILIES, rotatable: true });
    }
}

// class GroupModel ===========================================================

/**
 * The model of a group object containing an embedded drawing collection with
 * child drawing objects.
 *
 * @param parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param [attrSet]
 *  An attribute set with initial formatting attributes for the object.
 */
export class GroupModel<DocModelT extends EditModel, MixinT> extends DrawingModel<DocModelT, DrawingAttributeSet, MixinT> {
    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, attrSet?: PtDrawingAttributeSet) {
        super(parentCollection, DrawingType.GROUP, attrSet, { children: true, rotatable: true });
    }
}

// class UndefinedModel ===========================================================

const UNDEFINED_FAMILIES = ["image"] as const;

/**
 * The model of a picture object.
 *
 * @param parentCollection
 *  The parent drawing collection that will contain this drawing object.
 *
 * @param [attrSet]
 *  An attribute set with initial formatting attributes for the object.
 */
export class UndefinedModel<DocModelT extends EditModel, MixinT> extends DrawingModel<DocModelT, UndefinedAttributeSet, MixinT> {
    constructor(parentCollection: DrawingCollection<DocModelT, MixinT>, attrSet?: PtUndefinedAttributeSet) {
        super(parentCollection, DrawingType.UNDEFINED, attrSet, { families: UNDEFINED_FAMILIES, rotatable: true });
    }
}
