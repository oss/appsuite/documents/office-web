/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { ary } from '@/io.ox/office/tk/algorithms';

import { Color } from '@/io.ox/office/editframework/utils/color';
import Gradient from '@/io.ox/office/editframework/utils/gradient';

import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';

// constants ==============================================================

// definitions for common drawing attributes ------------------------------

var DRAWING_ATTRIBUTE_DEFINITIONS = {

    /**
     * Absolute horizontal position of the drawing, in 1/100 of millimeters.
     */
    left: { def: 0 },

    /**
     * Absolute vertical position of the drawing, in 1/100 of millimeters.
     */
    top: { def: 0 },

    /**
     * Width of the drawing, in 1/100 of millimeters.
     */
    width: { def: 0 },

    /**
     * Height of the drawing, in 1/100 of millimeters.
     */
    height: { def: 0 },

    /**
     * The name of the drawing object.
     */
    name: { def: '' },

    /**
     * A detailed description for the drawing object.
     */
    description: { def: '' },

    /**
     * Rotation of the drawing.
     */
    rotation: { def: 0 },

    /**
     * Flip the drawing horizontally (swap left and right side).
     */
    flipH: { def: false },

    /**
     * Flip the drawing vertically (swap top and bottom side).
     */
    flipV: { def: false },

    /**
     * Whether the drawing object is hidden in the view.
     */
    hidden: { def: false },

    /**
     * Whether the aspect ratio of the drawing object is locked and cannot
     * be changed when the drawing object will be resized.
     */
    aspectLocked: { def: false },

    /**
     * The minimal height of text frames (ODF only).
     */
    minFrameHeight: { def: 0 },

    /**
     * Left border of the rectangle used to set the position of embedded
     * child objects.
     */
    childLeft: { def: 0 },

    /**
     * Top border of the rectangle used to set the position of embedded
     * child objects.
     */
    childTop: { def: 0 },

    /**
     * Width of the rectangle used to set the position of embedded child
     * objects.
     */
    childWidth: { def: 0 },

    /**
     * Height of the rectangle used to set the position of embedded child
     * objects.
     */
    childHeight: { def: 0 },

    /**
     * Replacement image for unsupported drawing objects. The string
     * contains either BASE64 encoded bitmap data, or SVG mark-up.
     */
    replacementData: { def: '', scope: 'element' },

    /**
     * Disables selection of the drawing
     */
    noSelect: { def: false },

    /**
     * Whether the drawing can be grouped or not.
     */
    noGroup: { def: false },

    /**
     * Drawing id, used to identify drawing objects (needed for connector objects).
     */
    id: { def: '0' }
};

// definitions for drawing fill attributes --------------------------------

var FILL_ATTRIBUTE_DEFINITIONS = {

    /**
     * Type of the fill. Supported values are:
     *  - 'none': not filled (transparent).
     *  - 'auto': specific default fill, dependent on context.
     *  - 'solid': single fill color ('color' attribute).
     *  - 'gradient': color gradient.
     *  - 'pattern': monochrome pattern.
     *  - 'bitmap': background image.
     *
     * The property 'undo' contains the names of all attributes that need
     * to be inserted into the undo operation that will be generated when
     * changing this attribute (e.g. when a document operation changes the
     * value of this attribute from 'bitmap' to 'none', the undo operation
     * has to contain the value of the 'bitmap' attribute, although it has
     * not been reset by the document operation).
     */
    type: { def: 'none', undo: '*' },

    /**
     * Primary fill color. Used for fill type 'solid' (solid fill color),
     * 'gradient' (start color of a simple color gradient), and 'pattern'
     * (pattern foreground color).
     */
    color: { def: Color.AUTO },

    /**
     * Secondary fill color. Used for fill type 'gradient' (end color of a
     * simple color gradient), or 'pattern' (pattern background color).
     */
    color2: { def: Color.AUTO },

    /**
     * Gradient descriptor object (used for fill type 'gradient').
     */
    gradient: { def: Gradient.LINEAR },

    /**
     * Pattern type (used for fill type 'pattern').
     */
    pattern: { def: 'none' },

    /**
     * Bitmap descriptor object (used for fill type 'bitmap').
     */
    bitmap: { def: { imageUrl: '', transparency: 0 } }
};

// definitions for drawing outline attributes -----------------------------

// supports ALL fill attributes
var LINE_ATTRIBUTE_DEFINITIONS = {
    ...FILL_ATTRIBUTE_DEFINITIONS,

    /**
     * The line dash style. Supported values are:
     * - 'solid': No line dash pattern.
     * - 'dashed': Repeating dashes.
     * - 'dotted': Repeating dots.
     * - 'dashDot': Repeating dash-dot pattern.
     * - 'dashDotDot': Repeating dash-dot-dot pattern.
     */
    style: { def: 'solid' },

    /**
     * Width of the outline, in 1/100 of millimeters.
     */
    width: { def: 1 },

    /**
     * Specifies the head end type. Supported values are:
     * - 'none': No head end is drawn..
     * - 'triangle': A head end is drawn using triangle style.
     * - 'stealth': A head end is drawn using stealth style.
     * - 'diamond': A head end is drawn using diamond style.
     * - 'oval': A head end is drawn using oval style.
     * - 'arrow': A head end is drawn using arrow style.
     */
    headEndType: { def: 'none' },

    /**
     * Specifies the head end length. Supported values are:
     * - 'small': A small length head end is drawn.
     * - 'medium': A medium length head end is drawn.
     * - 'large': A large length head end is drawn.
     */
    headEndLength: { def: 'medium' },

    /**
     * Specifies the head end width. Supported values are:
     * - 'small': A small width head end is drawn.
     * - 'medium': A medium width head end is drawn.
     * - 'large': A large width head end is drawn.
     */
    headEndWidth: { def: 'medium' },

    /**
     * Specifies the tail end type. Supported values are:
     * - 'none': No tail end is drawn..
     * - 'triangle': A tail end is drawn using triangle style.
     * - 'stealth': A tail end is drawn using stealth style.
     * - 'diamond': A tail end is drawn using diamond style.
     * - 'oval': A tail end is drawn using oval style.
     * - 'arrow': A tail end is drawn using arrow style.
     */
    tailEndType: { def: 'none' },

    /**
     * Specifies the tail end length. Supported values are:
     * - 'small': A small length tail end is drawn.
     * - 'medium': A medium length tail end is drawn.
     * - 'large': A large length tail end is drawn.
     */
    tailEndLength: { def: 'medium' },

    /**
     * Specifies the tail end width. Supported values are:
     * - 'small': A small width tail end is drawn.
     * - 'medium': A medium width tail end is drawn.
     * - 'large': A large width tail end is drawn.
     */
    tailEndWidth: { def: 'medium' }
};

// definitions for linked text attributes ---------------------------------

var TEXT_ATTRIBUTE_DEFINITIONS = {

    /**
     * A specification for the source of the text contents of a drawing
     * object, or of a sub object in a complex drawing (e.g. a chart
     * title). Can be a plain array with literal strings, or a string with
     * an application-dependent descriptor of the text source (e.g. a
     * spreadsheet formula with cell addresses).
     */
    link: { def: [] }
};

// definitions for common image attributes --------------------------------

var IMAGE_ATTRIBUTE_DEFINITIONS = {

    /**
     * URL pointing to the image data. If the image is embedded in the
     * document file, the URL will be relative to the document.
     */
    imageUrl: { def: '', scope: 'element' },

    /**
     * Image data.
     */
    imageData: { def: '', scope: 'element' },

    /**
     * Amount of left part of the image cropped outside the object border,
     * in percent.
     */
    cropLeft: { def: 0 },

    /**
     * Amount of right part of the image cropped outside the object border,
     * in percent.
     */
    cropRight: { def: 0 },

    /**
     * Amount of top part of the image cropped outside the object border,
     * in percent.
     */
    cropTop: { def: 0 },

    /**
     * Amount of bottom part of the image cropped outside the object
     * border, in percent.
     */
    cropBottom: { def: 0 }
};

// definitions for common image attributes --------------------------------

var SHAPE_ATTRIBUTE_DEFINITIONS = {

    /**
     * Distance between left border and text, in 1/100 of millimeters.
     */
    paddingLeft: { def: 250 },

    /**
     * Distance between right border and text, in 1/100 of millimeters.
     */
    paddingRight: { def: 250 },

    /**
     * Distance between top border and text, in 1/100 of millimeters.
     */
    paddingTop: { def: 125 },

    /**
     * Distance between bottom border and text, in 1/100 of millimeters.
     */
    paddingBottom: { def: 125 },

    /**
     * Specifies the vertical alignment of text inside of a text body. Possible
     * values: 'bottom', 'centered', 'distributed', 'justified' or 'top'.
     */
    anchor: { def: 'top' },

    /**
     * Specifies centering of the text box by determining the smallest possible bound box.
     */
    anchorCentered: { def: false },

    /**
     * Specifies if the text is word wrapped.
     */
    wordWrap: { def: true },

    /**
     * Specifies if the text is allowed to flow out of the bounding box or if it cut.
     * Possible values are 'overflow' or 'clip'.
     */
    horzOverflow: { def: 'overflow' },

    /**
     * Specifies if the text is allowed to flow out of the bounding box or if it cut.
     * Possible values are 'overflow', 'clip' or 'ellipsis'.
     */
    vertOverflow: { def: 'overflow' },

    /**
     * Whether the size of the shape shall be adapted to the text content.
     */
    autoResizeHeight: { def: false },

    /**
     * Whether the fontsize of the text content shall be adapted to the shape height.
     */
    autoResizeText: { def: false },

    /**
     *  0 - 1
     */
    fontScale:      { def: 1 },

    /**
     *  0 - 1
     */
    lineReduction:  { def: 0 },

    /**
     * disables autoResizeHeight & autoResizeText
     */
    noAutoResize:   { def: false },

    /**
     * Specifies the text orientation
     */
    vert: { def: 'horz' }
};

// definitions for geometry attributes --------------------------------

var GEOMETRY_ATTRIBUTE_DEFINITIONS = {

    /**
     * Geometry attributes defines the geometry that should be drawn by providing
     * a pathList whose points are determined by a combination of constant,
     * guide and absolute values (using null instead of empty array and objects
     * because of task 57331)
     */
    presetShape: { def: '' },

    avList: { def: null },

    ahList: { def: null },

    gdList: { def: null },

    cxnList: { def: null },

    pathList: { def: null },

    textRect: { def: null },

    textPath: { def: '' },

    hostData: { def: '' }
};

// definitions for connector attributes --------------------------------

var CONNECTOR_ATTRIBUTE_DEFINITIONS = {

    /**
     * Connector attributes defines the shape the connector is connected to
     */
    startId: { def: '' },

    startIndex: { def: 0 },

    endId: { def: '' },

    endIndex: { def: 0 }
};

// definitions for common chart attributes --------------------------------

var CHART_ATTRIBUTE_DEFINITIONS = {

    /**
     * standard, clustered, stacked, percent, the way how multi series lay
     * to each other (bar, column and area)
     */
    stacking: { def: 'standard', scope: 'element' },

    /**
     * if true line charts are smooth spline charts.
     * Only for ods, ooxml curved is on the series_attributes
     */
    curved: { def: false, scope: 'element' },

    /**
     * color patterns, background and highlights
     */
    chartStyleId: { def: 0, scope: 'element' },

    rotation: { def: 0, scope: 'element' },

    /**
     * defines if the points of the first series are same color or not
     */
    varyColors: { def: false, scope: 'element' },

    /**
     * Rotation of a 3D Chart.
     */
    rotationX: { def: 0, scope: 'element' },
    rotationY: { def: 0, scope: 'element' },
    /**
     * if true the chart contains a errorbar
     */
    eb: { dev: false, scope: 'element' },

    /**
     * See only visible cell data.
     */
    sovcd: { def: true, scope: 'element' }

};

// definitions for chart data series attributes ---------------------------

var CHART_SERIES_ATTRIBUTE_DEFINITIONS = {

    /**
     * Data source for the series title. May be an array of constant
     * strings, or a string with an application-dependent dynamic data
     * source definition.
     */
    title: { def: [] },

    /**
     * Data source for the values of all data points. May be an array of
     * constant values, or a string with an application-dependent dynamic
     * data source definition.
     */
    values: { def: [] },

    /**
     * Data source for the categories of all data points, or the X values
     * in scatter and bubble charts. May be an array of constant values, or
     * a string with an application-dependent dynamic data source
     * definition.
     */
    names: { def: [] },

    /**
     * Data source for the bubble sizes of all data points in bubble
     * charts. May be an array of constant values, or a string with an
     * application-dependent dynamic data source definition.
     */
    bubbles: { def: [] },

    /**
     * The type of the chart. Specifies how the data series will be
     * rendered.
     */
    type: { def: 'column' },

    /**
     * holder for fill & line attributes for colors
     */
    dataPoints: { def: [] },

    /**
     * optional number format code
     */
    format: { def: '' },

    dataLabel: { def: '' },

    axisXIndex: { def: 0 },

    axisYIndex: { def: 0 },

    axisZIndex: { def: 0 },

    chart3d: { def: false },

    bubble3d: { def: false },

    noFill: { def: false },

    noLine: { def: false },
    // curved line only for ooxml, ods curved is on the chart_attributes
    curved: { def: false }
};

// definitions for chart axis attributes ----------------------------------

var CHART_AXIS_ATTRIBUTE_DEFINITIONS = {

    type: { def: 'value' },

    min: { def: 'auto' },

    max: { def: 'auto' },

    label: { def: false },

    format: { def: '' }
};

// definitions for chart legend attributes --------------------------------

var CHART_LEGEND_ATTRIBUTE_DEFINITIONS = {

    /**
     * Position of the legend object. The value 'off' hides the legend,
     * otherwise, the values 'bottom', 'top', 'left', 'right', or
     * 'topRight' show the legend at the respective border in the chart
     * area.
     */
    pos: { def: 'off' }
};

var MARKERFILL_ATTRIBUTE_DEFINITIONS = {
    ...FILL_ATTRIBUTE_DEFINITIONS,
    onlyMarker: { def: false }
};

const DRAWING_FAMILIES = ["line", "fill", "shape", "geometry", "connector", "image", "text", "paragraph", "character", "chart", "series", "axis", "legend", "markerFill", "markerBorder"];

// class DrawingStyleCollection ===========================================

/**
 * Contains the style sheets for drawing formatting attributes.
 *
 * @param {EditModel} docModel
 *  The document model containing this instance.
 *
 * @param {Object} [config]
 *  Optional parameters passed to the base class StyleCollection.
 */
export default class DrawingStyleCollection extends StyleCollection {

    constructor(docModel, config) {

        // base constructor
        super(docModel, 'drawing', {
            ...config,
            families: ary.concat(DRAWING_FAMILIES, config?.families)
        });

        // register all drawing attribute families
        const attrPool = docModel.defAttrPool;
        attrPool.registerAttrFamily('drawing', DRAWING_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('line', LINE_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('fill', FILL_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('shape', SHAPE_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('geometry', GEOMETRY_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('connector', CONNECTOR_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('image', IMAGE_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('text', TEXT_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('chart', CHART_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('series', CHART_SERIES_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('axis', CHART_AXIS_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('legend', CHART_LEGEND_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('markerFill', MARKERFILL_ATTRIBUTE_DEFINITIONS);
        attrPool.registerAttrFamily('markerBorder', LINE_ATTRIBUTE_DEFINITIONS);
    }
}
