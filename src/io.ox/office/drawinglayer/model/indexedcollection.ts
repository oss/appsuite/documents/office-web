/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ArrayLoopOptions, ary } from "@/io.ox/office/tk/algorithms";

import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

import type { DrawingModelType, DrawingModelIterator } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import type { YieldDrawingModelsOptions, DrawingCollectionEventMap } from "@/io.ox/office/drawinglayer/model/drawingcollection";
import { DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";

// types ======================================================================

export interface YieldIndexedDrawingModelsOptions extends YieldDrawingModelsOptions, ArrayLoopOptions { }

// class IndexedCollection ====================================================

/**
 * Represents a drawing collection that stores and addresses its drawing models
 * by a simple integral index.
 */
export abstract class IndexedCollection<
    DocModelT extends EditModel,
    ModelMixinT,
    EvtMapT extends DrawingCollectionEventMap<DocModelT, ModelMixinT> = DrawingCollectionEventMap<DocModelT, ModelMixinT>
> extends DrawingCollection<DocModelT, ModelMixinT, EvtMapT> {

    readonly #drawingModels: Array<DrawingModelType<DocModelT, ModelMixinT>> = [];

    // workaround for type errors with extensible event maps
    get #evt(): IndexedCollection<DocModelT, ModelMixinT> { return this as IndexedCollection<DocModelT, ModelMixinT>; }

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The document model containing this instance.
     *
     * @param [parentModel]
     *  The parent drawing model owning this instance as embedded drawing
     *  collection. If omitted, this instance will be a root collection without
     *  parent drawing model.
     */
    protected constructor(docModel: DocModelT, parentModel?: DrawingModelType<DocModelT, ModelMixinT>) {
        super(docModel, parentModel);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates an iterator that visits all drawing models in this collection.
     * The drawing models will be visited in order of their document positions
     * (Z order).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The matching drawing models.
     */
    override *yieldModels(options?: YieldIndexedDrawingModelsOptions): DrawingModelIterator<DocModelT, ModelMixinT> {
        yield* this.implYieldModels(ary.values(this.#drawingModels, options), options);
    }

    /**
     * Creates an iterator that visits all drawing models in this collection
     * together with their indexes. The drawing models will be visited in order
     * of their indexes (Z order).
     *
     * @yields
     *  The drawing models with their indexes.
     */
    *yieldModelEntries(): IndexedIterator<DrawingModelType<DocModelT, ModelMixinT>> {
        yield* this.#drawingModels.entries();
    }

    /**
     * Moves the model of a drawing object to a new sorting index (Z order).
     *
     * @param position
     *  The document position of the drawing model. May specify a position
     *  inside another drawing object (if the array without the last element
     *  points to an existing drawing object).
     *
     * @param index
     *  The new sorting index for the drawing model inside its parent
     *  collection.
     *
     * @returns
     *  Whether the drawing model has been moved successfully.
     */
    moveModel(position: Position, index: number): boolean {

        // find the drawing object in this collection
        const model = this.#drawingModels[position[0]];
        if (!model) { return false; }

        // remaining elements: move a drawing model in an embedded collection
        if (position.length > 1) {
            const childPos = position.slice(1) as Position;
            return !!model.childCollection?.moveModel(childPos, index);
        }

        // validate the target index
        if ((index < 0) || (index >= this.#drawingModels.length)) {
            return false;
        }

        // nothing to do if the index does not change
        const oldIndex = position[0];
        if (oldIndex === index) { return true; }

        // remove drawing mode from old position, and insert it at the new position
        ary.deleteAt(this.#drawingModels, oldIndex);
        ary.insertAt(this.#drawingModels, index, model);

        // notify all listeners
        this.#evt.trigger("move:drawing", model, oldIndex);
        return true;
    }

    // client API methods -----------------------------------------------------

    /**
     * Tries to return a drawing model contained in this collection at the
     * specified position.
     *
     * @param position
     *  An arbitrary document position.
     *
     * @returns
     *  The drawing model at the passed position, if available; otherwise
     *  `undefined`.
     */
    clientResolveModel(position: Position): Opt<DrawingModelType<DocModelT, ModelMixinT>> {
        return (position.length === 1) ? this.#drawingModels[position[0]] : undefined;
    }

    /**
     * Tries to return the position of the specified drawing model.
     *
     * @param drawingModel
     *  An arbitrary drawing model.
     *
     * @returns
     *  The position of the passed drawing model, if it is part of this drawing
     *  collection, otherwise `undefined`.
     */
    clientResolvePosition(drawingModel: DrawingModelType<DocModelT, ModelMixinT>): Opt<Position> {
        const index = this.#drawingModels.indexOf(drawingModel);
        return (index >= 0) ? [index] : undefined;
    }

    /**
     * Tries to return the Z order index of the specified drawing model.
     *
     * @param drawingModel
     *  An arbitrary drawing model.
     *
     * @returns
     *  The Z order index of the passed drawing model, if it is part of
     *  this drawing collection, otherwise `undefined`.
     */
    clientResolveZIndex(drawingModel: DrawingModelType<DocModelT, ModelMixinT>): number {
        return this.#drawingModels.indexOf(drawingModel);
    }

    /**
     * Registers a new drawing model that has just been inserted into this
     * drawing collection.
     *
     * @param drawingModel
     *  The new drawing model.
     *
     * @param position
     *  The position of the new drawing object.
     *
     * @returns
     *  Whether the passed position is valid, and the drawing model has been
     *  registered successfully.
     */
    protected clientRegisterModel(drawingModel: DrawingModelType<DocModelT, ModelMixinT>, position: Position): boolean {
        const index = (position.length === 1) ? position[0] : -1;
        if ((index >= 0) && (index <= this.#drawingModels.length)) {
            ary.insertAt(this.#drawingModels, index, drawingModel);
            return true;
        }
        return false;
    }

    /**
     * Unregisters a drawing model that has been deleted from this drawing
     * collection.
     *
     * @param drawingModel
     *  The deleted drawing model.
     *
     * @param position
     *  The position of the deleted drawing object.
     *
     * @returns
     *  Whether the passed position is valid, and the drawing model has
     *  been unregistered successfully.
     */
    protected clientUnregisterModel(drawingModel: DrawingModelType<DocModelT, ModelMixinT>, position: Position): boolean {
        const index = (position.length === 1) ? position[0] : -1;
        if (drawingModel === this.#drawingModels[index]) {
            ary.deleteAt(this.#drawingModels, index);
            return true;
        }
        return false;
    }
}
