/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { DrawingAttributeSet, DrawingModelType } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// constants ==================================================================

export const NODE_SELECTOR: string;
export const INTERNAL_MODEL_CLASS: string;
export const FLIPPED_HORIZONTALLY_CLASSNAME: string;
export const FLIPPED_VERTICALLY_CLASSNAME: string;
export const ROTATED_DRAWING_CLASSNAME: string;
export const TEXTFRAMECONTENT_NODE_CLASS: string;

// functions ==================================================================

export function createDrawingFrame<DocModelT extends EditModel, MixinT>(drawingModel: DrawingModelType<DocModelT, MixinT>): JQuery;

export function getContentNode(drawingFrame: NodeOrJQuery): JQuery;
export function getAndClearContentNode(drawingFrame: NodeOrJQuery): JQuery;
export function getCanvasNode(drawingFrame: NodeOrJQuery): JQuery;
export function getGroupNode(drawingNode: NodeOrJQuery): JQuery | null;
export function getFarthestGroupNode(rootNode: NodeOrJQuery, drawingNode: NodeOrJQuery): JQuery | null;
export function getTextFrameNode(drawingFrame: NodeOrJQuery): JQuery;

export function isConnectorDrawingFrame(drawingFrame: NodeOrJQuery): boolean;

export function isFlippedHorz(drawingFrame: NodeOrJQuery): boolean;
export function isFlippedVert(drawingFrame: NodeOrJQuery): boolean;

export function updateFormatting(docApp: EditApplication, drawingFrame: NodeOrJQuery, mergedAttributes: DrawingAttributeSet, options?: object): void;
export function setCssTransform(drawingFrame: NodeOrJQuery, degrees: number, flipH: boolean, flipV: boolean): void;

export function isSelected(drawingFrame: NodeOrJQuery): boolean;
export function drawSelection(drawingFrame: NodeOrJQuery, options?: object): JQuery;
export function clearSelection(drawingFrame: NodeOrJQuery, options?: { keepDraggingActive?: boolean }): JQuery;
export function checkEmptyTextShape(docModel: EditModel, drawingFrame: NodeOrJQuery): boolean;
