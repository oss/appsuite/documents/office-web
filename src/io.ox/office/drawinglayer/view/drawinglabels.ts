/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { is, fun, map, dict, pick } from "@/io.ox/office/tk/algorithms";
import type { IconId } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import presetGeometry from "@/io.ox/office/drawinglayer/view/presetGeometries.json";

// types ======================================================================

/**
 * The type specifier of a drawing model.
 */
export enum DrawingType {
    SHAPE = "shape",
    CONNECTOR = "connector",
    IMAGE = "image",
    CHART = "chart",
    GROUP = "group",
    NOTE = "note",
    DIAGRAM = "diagram",
    OLE = "ole",
    UNDEFINED = "undefined"
}

/**
 * Configuration settings for a predefined shape type.
 */
export interface ShapeConfig {

    /**
     * The resolved root identifier of the shape geometry.
     */
    id: string;

    /**
     * The drawing type identifier for the shape. Will be either `SHAPE` for
     * regular shapes, or `CONNECTOR` for connector line shapes.
     */
    type: DrawingType;

    /**
     * The JSON geometry data needed to render the shape.
     */
    shape: Dict;

    /**
     * The tooltip string to be shown in the GUI for the shape.
     */
    tooltip: string;

    /**
     * The default aspect ratio (quotient of width and height) according to the
     * default format defined in the property `shape`.
     */
    aspectRatio: number;

    /**
     * Whether the area of the shape will NOT be filled by default.
     */
    noFill: boolean;

    /**
     * Whether the text frame will be located outside the shape geometry
     * (especially used for filigree shapes, e.g. narrow bent arrows).
     */
    outerText: boolean;

    /**
     * Additional line attributes to be added to the shape (usually, this
     * property contains arrow attributes for connector sub types).
     */
    lineAttrs?: Dict;

    /**
     * A label to be rendered into the shape center when generating an icon for
     * the shape. Can be used for example to specify the number of corners for
     * polygons, or the number of tips for star shapes.
     */
    iconLabel?: string;
}

/**
 * The configuration settings of a specific category of preset shapes.
 */
export interface ShapeCategoryConfig {

    /**
     * The UI label of the shape cateory.
     */
    title: string;

    /**
     * The configurations of all shapes that are part of the shape category,
     * mapped by preset shape identifier.
     */
    shapes: Dict<ShapeConfig>;
}

// private types --------------------------------------------------------------

interface DrawingTypeInfo {
    icon: IconId;
    label: string;
}

interface ShapeConfigImpl {
    type?: DrawingType;
    tooltip: string;
    linkedId?: string;
    lineAttrs?: Dict;
    iconLabel?: string;
    aspectRatio?: number;
    noFill?: boolean;
    outerText?: boolean;
}

interface ShapeCategoryConfigImpl {
    title: string;
    shapes: Dict<string | ShapeConfigImpl>;
}

interface ChartTypeSeriesInfo {

    /**
     * The base chart type, as used in document operations.
     */
    type: string;

    /**
     * The chart type identifier for the CanvasJS library.
     */
    cjs: string;

    /**
     * Show only point markers, but no series lines.
     */
    markersOnly: boolean;
}

/**
 * Configuration options of a specific chart type.
 */
interface ChartTypeInfo {

    /**
     * The chart type identifier, as used in document operations.
     */
    type: string;

    /**
     * Default settings for the data series in the chart.
     */
    series: ChartTypeSeriesInfo;

    /**
     * The stacking mode: "standard", "stacked", or "percentStacked".
     */
    stacking: string;

    /**
     * The localized name of the chart type.
     */
    title: string;

    /**
     * Whether the lines in the chart area are smoothed.
     */
    curved?: boolean;

    /**
     * Whether the data points of a single series show different colors.
     */
    varyColors?: boolean;

    /**
     * See only visible cell data.
     */
    sovcd?: boolean;
}

// constants ==================================================================

// maps drawing object types as defined/used in operations to GUI names and icons
const DRAWING_TYPE_INFOS: Dict<DrawingTypeInfo> = {
    chart:     { icon: "bi:bar-chart",      label: /*#. bar charts, line charts, pie charts, etc. */ gt.pgettext("drawing", "Chart") },
    image:     { icon: "bi:image",          label: /*#. bitmaps, vector graphics, etc. */ gt.pgettext("drawing", "Image") },
    shape:     { icon: "bi:image",         label: /*#. rectangles, circles, stars, etc. */ gt.pgettext("drawing", "Shape") },
    connector: { icon: "bi:image-fill",     label: /*#. lines, arrows, straight/bent/curved connectors, etc. */ gt.pgettext("drawing", "Line") },
    textframe: { icon: "bi:card-list",      label: /*#. rectangles, circles, etc. with arbitrary text contents */ gt.pgettext("drawing", "Text frame") },
    diagram:   { icon: "bi:diagram-3-fill", label: /*#. complex diagrams, organigrams, etc. */ gt.pgettext("drawing", "Diagram") }
};

// fall-back settings for unknown/unsupported drawing objects
const DRAWING_TYPE_DEFAULT_INFO: DrawingTypeInfo = { icon: "bi:image", label: gt.pgettext("drawing", "Drawing") };

// settings for predefined shape types, mapped by shape identifier
const PRESET_SHAPE_MAP = new Map<string, ShapeConfig>();

// the identifiers of preset two-point shapes (to be used if drawing type is "shape" instead of "connector")
const TWO_POINT_SHAPES = dict.fill("line straightConnector1 bentConnector2 bentConnector3 bentConnector4 bentConnector5 curvedConnector2 curvedConnector3 curvedConnector4 curvedConnector5", true);

// the identifiers of preset shapes that are not connectors but cannot contain text either
const NO_TEXT_SHAPES = dict.fill("chartPlus chartX", true);

// shape definitions ----------------------------------------------------------

const SHAPE_CONFIG_OOX_IMPL: Dict<ShapeCategoryConfigImpl> = {

    line: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Lines"),
        shapes: {
            line: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line")
            },
            straightConnector1: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line arrow"),
                lineAttrs: { tailEndType: "arrow" }
            },
            straightConnector1_2: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line arrow: double"),
                linkedId: "straightConnector1",
                lineAttrs: { headEndType: "arrow", tailEndType: "arrow" }
            },

            bentConnector3: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Connector: elbow")
            },
            bentConnector3_1: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Connector: elbow arrow"),
                linkedId: "bentConnector3",
                lineAttrs: { tailEndType: "arrow" }
            },
            bentConnector3_2: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Connector: elbow double-arrow"),
                linkedId: "bentConnector3",
                lineAttrs: { headEndType: "arrow", tailEndType: "arrow" }
            },

            curvedConnector3: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Connector: curved")
            },
            curvedConnector3_1: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Connector: curved arrow"),
                linkedId: "curvedConnector3",
                lineAttrs: { tailEndType: "arrow" }
            },
            curvedConnector3_2: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Connector: curved double-arrow"),
                linkedId: "curvedConnector3",
                lineAttrs: { headEndType: "arrow", tailEndType: "arrow" }
            }
        }
    },

    shape: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Basic shapes"),
        shapes: {
            ellipse: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ellipse"),
            triangle: /*#. predefined drawing shape type */ gt.pgettext("shape", "Isosceles triangle"),
            rtTriangle: /*#. predefined drawing shape type */ gt.pgettext("shape", "Right triangle"),
            parallelogram: /*#. predefined drawing shape type */ gt.pgettext("shape", "Parallelogram"),
            trapezoid: /*#. predefined drawing shape type */ gt.pgettext("shape", "Trapezoid"),
            diamond: /*#. predefined drawing shape type */ gt.pgettext("shape", "Diamond"),
            pentagon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Pentagon"),
            hexagon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Hexagon"),
            heptagon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Heptagon"), iconLabel: "7" },
            octagon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Octagon"), iconLabel: "8" },
            decagon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Decagon"), iconLabel: "10" },
            dodecagon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Dodecagon"), iconLabel: "12" },
            pie: /*#. predefined drawing shape type */ gt.pgettext("shape", "Circle pie"),
            chord: /*#. predefined drawing shape type */ gt.pgettext("shape", "Circle segment"),
            teardrop: /*#. predefined drawing shape type */ gt.pgettext("shape", "Teardrop"),
            frame: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Frame"), outerText: true },
            halfFrame: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Half frame"), outerText: true },
            corner: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Corner"), outerText: true },
            diagStripe: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Diagonal stripe"), outerText: true },
            plus: /*#. predefined drawing shape type */ gt.pgettext("shape", "Plus"),
            plaque: /*#. predefined drawing shape type */ gt.pgettext("shape", "Plaque"),
            can: /*#. predefined drawing shape type */ gt.pgettext("shape", "Cylinder"),
            cube: /*#. predefined drawing shape type */ gt.pgettext("shape", "Cube"),
            funnel: /*#. predefined drawing shape type */ gt.pgettext("shape", "Funnel"), // no excel
            bevel: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle bevel"),
            donut: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ring"), outerText: true },
            noSmoking: /*#. predefined drawing shape type */ gt.pgettext("shape", "No symbol"),
            blockArc: /*#. predefined drawing shape type */ gt.pgettext("shape", "Block arc"),
            foldedCorner: /*#. predefined drawing shape type */ gt.pgettext("shape", "Folded corner"),
            smileyFace: /*#. predefined drawing shape type */ gt.pgettext("shape", "Smiley face"),
            heart: /*#. predefined drawing shape type */ gt.pgettext("shape", "Heart"),
            lightningBolt: /*#. predefined drawing shape type */ gt.pgettext("shape", "Lightning bolt"),
            sun: /*#. predefined drawing shape type */ gt.pgettext("shape", "Sun"),
            moon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Moon"), aspectRatio: 0.5 },
            cloud: /*#. predefined drawing shape type */ gt.pgettext("shape", "Cloud"),
            arc: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arc"), noFill: true },
            bracketPair: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Double bracket"), noFill: true },
            bracePair: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Double brace"), noFill: true },
            leftBracket: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Left bracket"), aspectRatio: 0.1, noFill: true },
            rightBracket: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Right bracket"), aspectRatio: 0.1, noFill: true },
            leftBrace: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Left brace"), aspectRatio: 0.2, noFill: true },
            rightBrace: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Right brace"), aspectRatio: 0.2, noFill: true },
            // no excel
            squareTabs: { tooltip: /*#. predefined drawing shape type (4 small squares, one per corner, like photo corners) */ gt.pgettext("shape", "Square tabs"), noFill: true },
            cornerTabs: { tooltip: /*#. predefined drawing shape type (4 triangles, one per corner, like photo corners with straight borders inside) */ gt.pgettext("shape", "Corner tabs"), noFill: true },
            plaqueTabs: { tooltip: /*#. predefined drawing shape type (4 quarters of a circle, one per corner, like photo corners with rounded borders inside) */ gt.pgettext("shape", "Plaque tabs"), noFill: true },
            pieWedge: /*#. predefined drawing shape type (one quarter of a circle) */ gt.pgettext("shape", "Quarter pie"),
            chartPlus: { tooltip: /*#. predefined drawing shape type ("plus" sign) */ gt.pgettext("shape", "Plus sign"), noFill: true },
            chartX: { tooltip: /*#. predefined drawing shape type (X letter, "times" sign) */ gt.pgettext("shape", "Times sign"), noFill: true }
        }
    },

    rect: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Rectangles"),
        shapes: {
            rect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle"),
            roundRect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: rounded corners"),
            snip1Rect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: single corner snipped"),
            snip2SameRect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: top corners snipped"),
            snip2DiagRect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: diagonal corners snipped"),
            snipRoundRect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: top corners one rounded one snipped"),
            round1Rect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: single corner rounded"),
            round2SameRect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: top corners rounded"),
            round2DiagRect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle: diagonal corners rounded")
        }
    },

    arrow: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Block arrows"),
        shapes: {
            rightArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: right"),
            leftArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: left"),
            upArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: up"),
            downArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: down"),
            leftRightArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: left-right"), aspectRatio: 2 },
            upDownArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: up-down"), aspectRatio: 0.5 },
            quadArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: quad"),
            leftRightUpArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: left-right-up"), aspectRatio: 1.4 },
            bentArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: right-bent"), outerText: true },
            uturnArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: u-turn"), outerText: true },
            leftUpArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: left-up"),
            bentUpArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: up-bent"),
            curvedRightArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: curved right"), outerText: true },
            curvedLeftArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: curved left"), outerText: true },
            curvedUpArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: curved up"), outerText: true },
            curvedDownArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: curved down"), outerText: true },
            leftCircularArrow:  { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: circular right"), outerText: true },
            circularArrow:  { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: circular left"), outerText: true },
            stripedRightArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: striped right"),
            notchedRightArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: notched right"),
            homePlate: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: pentagon"),
            chevron: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: chevron"), outerText: true },
            swooshArrow:  { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: swoosh"), outerText: true }
        }
    },

    flowchart: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Flow charts"),
        shapes: {
            flowChartProcess: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: process"),
            flowChartAlternateProcess: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: alternate process"),
            flowChartDecision: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: decision"),
            flowChartInputOutput: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: data"),
            flowChartPredefinedProcess: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: predefined process"),
            flowChartInternalStorage: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: internal storage"),
            flowChartDocument: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: document"),
            flowChartMultidocument: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: multidocument"),
            flowChartTerminator: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: terminator"),
            flowChartPreparation: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: preparation"),
            flowChartManualInput: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: manual input"),
            flowChartManualOperation: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: manual operation"),
            flowChartConnector: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: connector"),
            flowChartOffpageConnector: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: off-page connector"),
            flowChartPunchedCard: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: Card"),
            flowChartPunchedTape: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: punched tape"),
            flowChartSummingJunction: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: summing junction"),
            flowChartOr: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: or"),
            flowChartCollate: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: collate"),
            flowChartSort: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: sort"),
            flowChartExtract: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: extract"),
            flowChartMerge: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: merge"),
            flowChartOnlineStorage: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: stored data"),
            flowChartDelay: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: delay"),
            flowChartMagneticTape: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: magnetic tape"),
            flowChartMagneticDisk: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: magnetic disk"),
            flowChartMagneticDrum: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: magnetic drum"),
            flowChartDisplay: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: display"),
            flowChartOfflineStorage: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: offline storage")
        }
    },

    starbanner: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Stars and banners"),
        shapes: {
            irregularSeal1: /*#. predefined drawing shape type */ gt.pgettext("shape", "Explosion: 12 points"),
            irregularSeal2: /*#. predefined drawing shape type */ gt.pgettext("shape", "Explosion: 14 points"),
            star4: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 4 points"),
            star5: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 5 points"),
            star6: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 6 points"),
            star7: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 7 points"), iconLabel: "7" },
            star8: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 8 points"), iconLabel: "8" },
            star10: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 10 points"), iconLabel: "10" },
            star12: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 12 points"), iconLabel: "12" },
            star16: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 16 points"), iconLabel: "16" },
            star24: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 24 points"), iconLabel: "24" },
            star32: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 32 points"), iconLabel: "32" },
            ribbon2: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: tilted up"),
            ribbon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: tilted down"),
            ellipseRibbon2: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: curved and tilted up"),
            ellipseRibbon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: curved and tilted down"),
            leftRightRibbon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: left-right arrow"), aspectRatio: 2 },
            verticalScroll: /*#. predefined drawing shape type */ gt.pgettext("shape", "Scroll: vertical"),
            horizontalScroll: /*#. predefined drawing shape type */ gt.pgettext("shape", "Scroll: horizontal"),
            wave: /*#. predefined drawing shape type */ gt.pgettext("shape", "Wave"),
            doubleWave: /*#. predefined drawing shape type */ gt.pgettext("shape", "Double wave")
        }
    },

    math: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Equation"),
        shapes: {
            mathPlus: /*#. predefined drawing shape type */ gt.pgettext("shape", "Plus sign"),
            mathMinus: /*#. predefined drawing shape type */ gt.pgettext("shape", "Minus sign"),
            mathMultiply: /*#. predefined drawing shape type */ gt.pgettext("shape", "Multiplication sign"),
            mathDivide: /*#. predefined drawing shape type */ gt.pgettext("shape", "Division sign"),
            mathEqual: /*#. predefined drawing shape type */ gt.pgettext("shape", "Equal"),
            mathNotEqual: /*#. predefined drawing shape type */ gt.pgettext("shape", "Not equal")
        }
    },

    callouts: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Callouts"),
        shapes: {
        }
    }
};

/**
 * Settings for all custom shape types used in ODF, grouped by shape category
 * identifiers.
 */
const SHAPE_CONFIG_ODF_IMPL: Dict<ShapeCategoryConfigImpl> = {

    line: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Lines"),
        shapes: {
            line: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line")
            },
            line_2: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line arrow"),
                linkedId: "line",
                lineAttrs: { tailEndType: "arrow" }
            },
            line_3: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line arrow"),
                linkedId: "line",
                lineAttrs: { headEndType: "arrow" }
            },
            line_4: {
                type: DrawingType.CONNECTOR,
                tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Line arrow: double"),
                linkedId: "line",
                lineAttrs: { headEndType: "arrow", tailEndType: "arrow" }
            }
        }
    },

    shape: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Basic shapes"),
        shapes: {
            rect: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle"),
            ellipse: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ellipse"),
            triangle: /*#. predefined drawing shape type */ gt.pgettext("shape", "Isosceles triangle"),
            rtTriangle: /*#. predefined drawing shape type */ gt.pgettext("shape", "Right triangle"),
            diamond: /*#. predefined drawing shape type */ gt.pgettext("shape", "Diamond"),
            pentagon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Pentagon"),
            hexagon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Hexagon"),
            octagon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Octagon"), iconLabel: "8" },
            plus: /*#. predefined drawing shape type */ gt.pgettext("shape", "Plus"),
            plaque: /*#. predefined drawing shape type */ gt.pgettext("shape", "Plaque"),
            can: /*#. predefined drawing shape type */ gt.pgettext("shape", "Cylinder"),
            cube: /*#. predefined drawing shape type */ gt.pgettext("shape", "Cube"),
            bevel: /*#. predefined drawing shape type */ gt.pgettext("shape", "Rectangle bevel"),
            foldedCorner: /*#. predefined drawing shape type */ gt.pgettext("shape", "Folded corner"),
            smileyFace: /*#. predefined drawing shape type */ gt.pgettext("shape", "Smiley face"),
            lightningBolt: /*#. predefined drawing shape type */ gt.pgettext("shape", "Lightning bolt"),
            sun: /*#. predefined drawing shape type */ gt.pgettext("shape", "Sun"),
            moon: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Moon"), aspectRatio: 0.5 },
            leftBracket: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Left bracket"), aspectRatio: 0.1, noFill: true },
            rightBracket: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Right bracket"), aspectRatio: 0.1, noFill: true },
            leftBrace: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Left brace"), aspectRatio: 0.2, noFill: true },
            rightBrace: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Right brace"), aspectRatio: 0.2, noFill: true }
        }
    },

    arrow: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Block arrows"),
        shapes: {
            rightArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: right"),
            leftArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: left"),
            upArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: up"),
            downArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: down"),
            leftRightArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: left-right"), aspectRatio: 2 },
            upDownArrow: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: up-down"), aspectRatio: 0.5 },
            notchedRightArrow: /*#. predefined drawing shape type */ gt.pgettext("shape", "Arrow: notched right")
        }
    },

    flowchart: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Flow charts"),
        shapes: {
            flowChartProcess: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: process"),
            flowChartDecision: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: decision"),
            flowChartInputOutput: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: data"),
            flowChartPredefinedProcess: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: predefined process"),
            flowChartInternalStorage: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: internal storage"),
            flowChartDocument: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: document"),
            flowChartMultidocument: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: multidocument"),
            flowChartTerminator: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: terminator"),
            flowChartManualInput: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: manual input"),
            flowChartManualOperation: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: manual operation"),
            flowChartConnector: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: connector"),
            flowChartOffpageConnector: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: off-page connector"),
            flowChartPunchedCard: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: Card"),
            flowChartPunchedTape: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: punched tape"),
            flowChartOr: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: or"),
            flowChartCollate: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: collate"),
            flowChartSort: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: sort"),
            flowChartExtract: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: extract"),
            flowChartMerge: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: merge"),
            flowChartOnlineStorage: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: stored data"),
            flowChartDelay: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: delay"),
            flowChartMagneticTape: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: magnetic tape"),
            flowChartMagneticDisk: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: magnetic disk"),
            flowChartMagneticDrum: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: magnetic drum"),
            flowChartDisplay: /*#. predefined drawing shape type */ gt.pgettext("shape", "Flowchart: display")
        }
    },

    starbanner: {
        title: /*#. category name for shape objects */ gt.pgettext("shape", "Stars and banners"),
        shapes: {
            irregularSeal1: /*#. predefined drawing shape type */ gt.pgettext("shape", "Explosion: 12 points"),
            irregularSeal2: /*#. predefined drawing shape type */ gt.pgettext("shape", "Explosion: 14 points"),
            star4: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 4 points"),
            star8: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 8 points"), iconLabel: "8" },
            star16: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 16 points"), iconLabel: "16" },
            star32: { tooltip: /*#. predefined drawing shape type */ gt.pgettext("shape", "Star: 32 points"), iconLabel: "32" },
            ribbon2: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: tilted up"),
            ribbon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: tilted down"),
            ellipseRibbon2: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: curved and tilted up"),
            ellipseRibbon: /*#. predefined drawing shape type */ gt.pgettext("shape", "Ribbon: curved and tilted down"),
            verticalScroll: /*#. predefined drawing shape type */ gt.pgettext("shape", "Scroll: vertical"),
            horizontalScroll: /*#. predefined drawing shape type */ gt.pgettext("shape", "Scroll: horizontal"),
            wave: /*#. predefined drawing shape type */ gt.pgettext("shape", "Wave"),
            doubleWave: /*#. predefined drawing shape type */ gt.pgettext("shape", "Double wave")
        }
    }
};

// processor function for the internal configuration of preset shapes
const parsePresetShapeConfig = fun.do(() => {

    return (configImplMap: Dict<ShapeCategoryConfigImpl>): Dict<ShapeCategoryConfig> => {
        return dict.mapDict(configImplMap, categoryConfigImpl => ({
            title: categoryConfigImpl.title,
            shapes: dict.mapDict(categoryConfigImpl.shapes, (shapeConfigImpl, presetShapeId) => {
                return map.update(PRESET_SHAPE_MAP, presetShapeId, shapeConfig => {

                    // do not process entries twice (OOXML vs. ODF)
                    if (shapeConfig) { return shapeConfig; }

                    // convert simple tooltip string to configuration object
                    if (is.string(shapeConfigImpl)) {
                        shapeConfigImpl = { tooltip: shapeConfigImpl };
                    }

                    // resolve base shape identifier linked to the current shape
                    const shapeId = shapeConfigImpl.linkedId || presetShapeId;

                    // get shape geometry definition
                    const shapeGeometry = pick.dict(presetGeometry, shapeId);
                    if (!shapeGeometry) {
                        globalLogger.error(`$badge{DrawingLabels} missing geometry data for preset shape "${shapeId}"`);
                        return undefined;
                    }

                    // create the shape configuration object
                    const isConnector = (shapeConfigImpl.type === DrawingType.CONNECTOR);
                    const noFill = isConnector || !!shapeConfigImpl.noFill;
                    const outerText = noFill || !!shapeConfigImpl.outerText;
                    shapeConfig = {
                        id: shapeId,
                        type: shapeConfigImpl.type ?? DrawingType.SHAPE,
                        shape: shapeGeometry,
                        tooltip: shapeConfigImpl.tooltip,
                        aspectRatio: shapeConfigImpl.aspectRatio ?? 1,
                        noFill,
                        outerText
                    };
                    if (shapeConfigImpl.lineAttrs) { shapeConfig.lineAttrs = shapeConfigImpl.lineAttrs; }
                    if (shapeConfigImpl.iconLabel) { shapeConfig.iconLabel = shapeConfigImpl.iconLabel; }
                    return shapeConfig;
                });
            })
        }));
    };
});

/**
 * Settings for all custom shape types used in OOXML, grouped by shape category
 * identifiers.
 */
export const PRESET_SHAPE_CONFIG_OOX: Dict<ShapeCategoryConfig> = parsePresetShapeConfig(SHAPE_CONFIG_OOX_IMPL);

/**
 * Settings for all custom shape types used in ODF, grouped by shape category
 * identifiers.
 */
export const PRESET_SHAPE_CONFIG_ODF: Dict<ShapeCategoryConfig> = parsePresetShapeConfig(SHAPE_CONFIG_ODF_IMPL);

// chart type definitions -----------------------------------------------------

const CHART_TYPE_STYLES_IMPL: ChartTypeInfo[] = [
    { type: "column standard",         series: { type: "column",  cjs: "column",            markersOnly: false }, stacking: "standard",                   title: /*#. Chart type: vertical bars (clustered) */                                                                     gt.pgettext("chart-type", "Column") },
    { type: "column stacked",          series: { type: "column",  cjs: "stackedColumn",     markersOnly: false }, stacking: "stacked",                    title: /*#. Chart type: vertical bars (stacked from bottom to top) */                                                    gt.pgettext("chart-type", "Column (stacked)") },
    { type: "column percentStacked",   series: { type: "column",  cjs: "stackedColumn100",  markersOnly: false }, stacking: "percentStacked",             title: /*#. Chart type: vertical bars (stacked from bottom to top with percent scaling) */                               gt.pgettext("chart-type", "Column (percent)") },
    { type: "bar standard",            series: { type: "bar",     cjs: "bar",               markersOnly: false }, stacking: "standard",                   title: /*#. Chart type: horizontal bars (clustered) */                                                                   gt.pgettext("chart-type", "Bar") },
    { type: "bar stacked",             series: { type: "bar",     cjs: "stackedBar",        markersOnly: false }, stacking: "stacked",                    title: /*#. Chart type: horizontal bars (stacked from left to right) */                                                  gt.pgettext("chart-type", "Bar (stacked)") },
    { type: "bar percentStacked",      series: { type: "bar",     cjs: "stackedBar100",     markersOnly: false }, stacking: "percentStacked",             title: /*#. Chart type: horizontal bars (stacked from left to right with percent scaling) */                             gt.pgettext("chart-type", "Bar (percent)") },
    { type: "line standard",           series: { type: "line",    cjs: "line",              markersOnly: false }, stacking: "standard",                   title: /*#. Chart type: data points connected with lines */                                                              gt.pgettext("chart-type", "Line") },
    { type: "line standard curved",    series: { type: "line",    cjs: "spline",            markersOnly: false }, stacking: "standard", curved: true,     title: /*#. Chart type: data points connected with curved lines */                                                       gt.pgettext("chart-type", "Line (curved)") },
    { type: "line standard marker",    series: { type: "line",    cjs: "spline",            markersOnly: true  }, stacking: "standard", curved: false,    title: /*#. Chart type: data point markers without connected lines */                                                    gt.pgettext("chart-type", "Line (marker)") },
    { type: "scatter standard",        series: { type: "scatter", cjs: "line",              markersOnly: false }, stacking: "standard",                   title: /*#. Chart type: data points with free X/Y coordinates connected with lines */                                    gt.pgettext("chart-type", "Scatter") },
    { type: "scatter standard curved", series: { type: "scatter", cjs: "spline",            markersOnly: false }, stacking: "standard", curved: true,     title: /*#. Chart type: data points with free X/Y coordinates connected with curved lines */                             gt.pgettext("chart-type", "Scatter (curved)") },
    { type: "scatter standard marker", series: { type: "scatter", cjs: "spline",            markersOnly: true  }, stacking: "standard", curved: false,    title: /*#. Chart type: data points with free X/Y markers without connected lines */                                     gt.pgettext("chart-type", "Scatter (marker)") },
    { type: "bubble standard",         series: { type: "bubble",  cjs: "bubble",            markersOnly: false }, stacking: "standard",                   title: /*#. Chart type: data points with free X/Y coordinates drawn as circles/bubbles */                                gt.pgettext("chart-type", "Bubble") },
    { type: "pie standard",            series: { type: "pie",     cjs: "pie",               markersOnly: false }, stacking: "standard", varyColors: true, title: /*#. Chart type: single pie */                                                                                    gt.pgettext("chart-type", "Pie") },
    { type: "donut standard",          series: { type: "donut",   cjs: "doughnut",          markersOnly: false }, stacking: "standard", varyColors: true, title: /*#. Chart type: one or multiple concentric circles */                                                            gt.pgettext("chart-type", "Donut") },
    { type: "area standard",           series: { type: "area",    cjs: "area",              markersOnly: false }, stacking: "standard",                   title: /*#. Chart type: filled areas between X axis and data points (areas overlay each other) */                        gt.pgettext("chart-type", "Area") },
    { type: "area stacked",            series: { type: "area",    cjs: "stackedArea",       markersOnly: false }, stacking: "stacked",                    title: /*#. Chart type: filled areas between X axis and data points (stacked from bottom to top) */                      gt.pgettext("chart-type", "Area (stacked)") },
    { type: "area percentStacked",     series: { type: "area",    cjs: "stackedArea100",    markersOnly: false }, stacking: "percentStacked",             title: /*#. Chart type: filled areas between X axis and data points (stacked from bottom to top with percent scaling) */ gt.pgettext("chart-type", "Area (percent)") }
];

/**
 * Type descriptors for all supported chart types.
 */
export const CHART_TYPE_STYLES: ReadonlyArray<Required<ChartTypeInfo>> = CHART_TYPE_STYLES_IMPL.map(
    // set missing boolean properties
    entry => ({ curved: false, varyColors: false, sovcd: true, ...entry })
);

/**
 * Returns the type descriptor for the specified chart type.
 */
export function getChartTypeStyle(chartType: string): Opt<Required<ChartTypeInfo>> {
    return CHART_TYPE_STYLES.find(entry => entry.type === chartType);
}

// drawing types --------------------------------------------------------------

/**
 * Returns an appropriate icon identifier for the passed drawing type.
 *
 * @param type
 *  The type identifier of a drawing object. If not supported or nullish, the
 *  default icon for a generic drawing object will be returned.
 *
 * @returns
 *  The icon identifier for the passed drawing type.
 */
export function getDrawingTypeIcon(type: Nullable<string>): string {
    return ((type && DRAWING_TYPE_INFOS[type]) || DRAWING_TYPE_DEFAULT_INFO).icon;
}

/**
 * Returns an appropriate text label for the passed drawing type.
 *
 * @param type
 *  The type identifier of a drawing object. If not supported or nullish, the
 *  default label for a generic drawing object will be returned.
 *
 * @returns
 *  A text label for the passed drawing type.
 */
export function getDrawingTypeLabel(type: Nullable<string>): string {
    return ((type && DRAWING_TYPE_INFOS[type]) || DRAWING_TYPE_DEFAULT_INFO).label;
}

// constant labels and tooltips -------------------------------------------

/**
 * Standard "Insert drawing" label.
 */
export const INSERT_DRAWING_LABEL = getDrawingTypeLabel("image");

/**
 * Standard "Insert drawing" tooltip.
 */
export const INSERT_DRAWING_TOOLTIP = gt.pgettext("drawing", "Insert an image");

/**
 * Standard "Delete drawing" label.
 */
//#. delete a drawing object from the document
export const DELETE_DRAWING_LABEL = gt.pgettext("drawing", "Delete");

/**
 * Standard "Delete drawing" tooltip.
 */
export const DELETE_DRAWING_TOOLTIP = gt.pgettext("drawing", "Delete the drawing object");

/**
 * Standard "No text wrap" label.
 */
//#. text is positioned behind the drawing and not wrapped around the drawing
export const NO_TEXTWRAP_DRAWING_LABEL = gt.pgettext("drawing", "In front of text");

/**
 * Standard "No text wrap" tooltip.
 */
export const NO_TEXTWRAP_DRAWING_TOOLTIP = gt.pgettext("drawing", "In front of text");

/**
 * Standard "Format drawing" label.
 */
//#. change the formatting of a drawing object
export const FORMAT_DRAWING_LABEL = gt.pgettext("drawing", "Format");

/**
 * Standard "Format drawing" tooltip.
 */
//#. change the formatting of a drawing object
export const FORMAT_DRAWING_TOOLTIP = gt.pgettext("drawing", "Format the drawing object");

/**
 * Standard "Group drawing" label.
 */
//#. group the selected drawings
export const GROUP_DRAWING_LABEL = gt.pgettext("drawing", "Group");

/**
 * Standard "Group drawing" tooltip.
 */
export const GROUP_DRAWING_TOOLTIP = gt.pgettext("drawing", "Group the selected drawings");

/**
 * Standard the "Ungroup drawing" label.
 */
//#. ungroup the selected drawing(s)
export const UNGROUP_DRAWING_LABEL = gt.pgettext("drawing", "Ungroup");

/**
 * Standard "Ungroup drawing" tooltip.
 */
export const UNGROUP_DRAWING_TOOLTIP = gt.pgettext("drawing", "Ungroup the selected drawings");

/**
 * Standard "Crop image" label.
 */
//#. Crop image to remove unwanted areas
export const CROP_IMAGE_LABEL = gt.pgettext("drawing", "Crop");

/**
 * Standard "Crop image" tooltip.
 */
//#. Crop image to remove unwanted areas
export const CROP_IMAGE_TOOLTIP = gt.pgettext("drawing", "Crop image");

// constants for controls -----------------------------------------------------

/**
 * Standard options for the "Delete drawing" button.
 */
export const DELETE_DRAWING_BUTTON_OPTIONS = {
    icon: "bi:trash",
    tooltip: DELETE_DRAWING_TOOLTIP
};

/**
 * Standard options for insert image.
 */
export const INSERT_IMAGE_OPTIONS = {
    label: INSERT_DRAWING_LABEL,
    tooltip: INSERT_DRAWING_TOOLTIP
};

/**
 * Standard options for a "Insert chart" button.
 */
export const INSERT_CHART_OPTIONS = {
    label: getDrawingTypeLabel("chart"),
    tooltip: gt.pgettext("drawing", "Insert a chart")
};

/**
 * Standard options for the "Insert chart" button.
 */
export const INSERT_CHART_BUTTON_OPTIONS = {
    width: null,
    icon: getDrawingTypeIcon("chart"),
    label: INSERT_CHART_OPTIONS.label,
    tooltip: INSERT_CHART_OPTIONS.tooltip,
    updateCaptionMode: "none",
    value: "column standard"
};

export const CHART_DATA_POINTS_BUTTON_OPTIONS = {
    //#. menu title: options to format data points (bars, lines, pie segments) in chart objects
    label: gt.pgettext("chart-format", "Data points")
};

export const CHART_SHOW_POINT_LABELS_BUTTON_OPTIONS = {
    //#. check box label: show/hide small text labels next to the single data points in charts objects
    label: gt.pgettext("chart-format", "Show data point labels"),
    attributes: { class: "highlight" }
};

export const CHART_VARY_POINT_COLORS_BUTTON_OPTIONS = {
    //#. check box label: if active, a bar/pie chart will have different colors for each data point (default is the same color for all points)
    label: gt.pgettext("chart-format", "Vary colors by point"),
    attributes: { class: "highlight" }
};

export const CHART_LABELS_BUTTON_OPTIONS = {
    //#. button label to open a menu to modify chart objects
    label: gt.pgettext("chart-format", "Labels"),
    //#. tooltip: menu to modify chart objects
    tooltip: gt.pgettext("chart-format", "Settings for labels and axes"),
    toggle: true,
    haspopup: true
};

/**
 * A collection of the names of those preset shapes, that can be larger than
 * the drawing itself. In this case it is necessary to recalculate the size of
 * the canvas inside the drawing.
 */
export const CANVAS_EXPANSION_PRESET_SHAPES = dict.fill([
    "accentBorderCallout1", "accentBorderCallout2", "accentBorderCallout3",
    "accentCallout1", "accentCallout2", "accentCallout3",
    "bentConnector3", "bentConnector4", "bentConnector5",
    "borderCallout1", "borderCallout2", "borderCallout3",
    "callout1", "callout2", "callout3",
    "curvedConnector3", "curvedConnector4", "curvedConnector5",
    "cloudCallout", "teardrop",
    "wedgeEllipseCallout", "wedgeRectCallout", "wedgeRoundRectCallout"
], true);

// custom shapes --------------------------------------------------------------

/**
 * Returns detailed information for a predefined shape type.
 *
 * @param presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @returns
 *  A configuration descriptor for the specified predefined shape type; or
 *  `undefined`, if the passed identifier is not supported. The descriptor will
 *  contain an `id` property containing the root identifier of the shape
 *  geometry. If the identifier passed to this function is a sub type (for
 *  example a line connector that adds arrow styles to the line ends), this
 *  identifier specifies the base geometry of the shape.
 */
export function getPresetShape(presetShapeId: string): Opt<ShapeConfig> {
    return PRESET_SHAPE_MAP.get(presetShapeId);
}

/**
 * Returns whether the specified predefined shape type is a connector line.
 *
 * @param presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @returns
 *  Whether the specified predefined shape type is a connector line.
 */
export function isPresetConnectorId(presetShapeId: string): boolean {
    return (getPresetShape(presetShapeId)?.type === DrawingType.CONNECTOR) || (presetShapeId in TWO_POINT_SHAPES);
}

/**
 * Returns whether the specified predefined shape type needs to be selected
 * with a two point selection instead of a selection frame.
 *
 * @param presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @returns
 *  Whether the specified predefined shape type has a two point selection.
 */
export function isTwoPointShapeId(presetShapeId: string): boolean {
    return presetShapeId in TWO_POINT_SHAPES;
}

/**
 * Returns whether the specified predefined shape type is able to contain text.
 *
 * @param presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @returns
 *  Whether the specified predefined shape type is able to contain text.
 */
export function isTextShapeId(presetShapeId: string): boolean {
    return !isPresetConnectorId(presetShapeId) && !(presetShapeId in NO_TEXT_SHAPES);
}

/**
 * Returns the default aspect ratio for the specified predefined shape type.
 *
 * @param presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @returns
 *  The default aspect ratio for the specified predefined shape type.
 */
export function getPresetAspectRatio(presetShapeId: string): number {
    return getPresetShape(presetShapeId)?.aspectRatio ?? 1;
}
