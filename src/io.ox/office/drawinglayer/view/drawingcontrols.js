/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button, CheckBox, RadioList, CompoundSplitButton } from "@/io.ox/office/baseframework/view/basecontrols";

import { opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';
import { ColorPicker } from "@/io.ox/office/editframework/view/control/colorpicker";

import { ImageSourceType } from "@/io.ox/office/drawinglayer/utils/imageutils";
import { CROP_IMAGE_LABEL, CROP_IMAGE_TOOLTIP, INSERT_DRAWING_LABEL, INSERT_DRAWING_TOOLTIP, getDrawingTypeIcon, getDrawingTypeLabel } from "@/io.ox/office/drawinglayer/view/drawinglabels";

// re-exports =================================================================

export * from "@/io.ox/office/drawinglayer/view/control/drawinglinestylepicker";
export * from "@/io.ox/office/drawinglayer/view/control/drawingarrowstylepicker";
export * from "@/io.ox/office/drawinglayer/view/control/drawingalignmentpicker";
export * from "@/io.ox/office/drawinglayer/view/control/drawingarrangementpicker";
export * from "@/io.ox/office/drawinglayer/view/control/shapetypepicker";
export * from "@/io.ox/office/drawinglayer/view/control/charttypepicker";
export * from "@/io.ox/office/drawinglayer/view/control/chartcolorsetpicker";
export * from "@/io.ox/office/drawinglayer/view/control/chartstylesetpicker";
export * from "@/io.ox/office/drawinglayer/view/control/chartlegendpicker";

// class DrawingLineColorPicker ===============================================

/**
 * A color picker control for selecting a stroke color for drawing objects.
 *
 * @param {EditView} docView
 *  The document view that contains this color picker.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the `ColorPicker` base
 *  class.
 */
export class DrawingLineColorPicker extends ColorPicker {
    constructor(docView, config) {
        super(docView, {
            icon: "png:drawing-line-color",
            label: config?.line ? gt.pgettext("drawing", "Line color") : gt.pgettext("drawing", "Border color"),
            splitValue: opRgbColor("0070C0"),
            sharedSplitValueKey: "DrawingLineColorPicker",
            autoColorType: null,
            ...config
        });
    }
}

// class DrawingFillColorPicker ===============================================

/**
 * A color picker control for selecting a background color for drawing objects.
 *
 * @param {EditView} docView
 *  The document view that contains this color picker.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the `ColorPicker` base
 *  class.
 */
export class DrawingFillColorPicker extends ColorPicker {
    constructor(docView, config) {
        super(docView, {
            icon: "png:drawing-fill-color",
            label: gt.pgettext("drawing", "Background color"),
            splitValue: Color.ACCENT2,
            sharedSplitValueKey: "DrawingFillColorPicker",
            autoColorType: "fill",
            ...config
        });
    }
}

// class InsertTextFrameButton ================================================

export class InsertTextFrameButton extends Button {
    constructor(config) {
        super({
            icon: getDrawingTypeIcon("textframe"),
            label: getDrawingTypeLabel("textframe"),
            tooltip: gt.pgettext("drawing", "Insert a text frame"),
            ...config
        });
    }
}

// class ImagePicker ==========================================================

export class ImagePicker extends RadioList {

    constructor(config) {

        // base constructor
        super({
            icon: "png:image-insert",
            label: INSERT_DRAWING_LABEL,
            tooltip: INSERT_DRAWING_TOOLTIP,
            splitValue: ImageSourceType.DRIVE,
            ...config,
            updateCaptionMode: "none"
        });

        if (!config?.hideLocal) {
            //#. Label for insert image from the local file system.
            this.addOption(ImageSourceType.LOCAL, { label: gt("Local file") });
        }

        if (!config?.hideDrive) {
            //#. Label for insert image from drive. Please do not translate the productname "Drive"
            this.addOption(ImageSourceType.DRIVE, { label: gt("From Drive") });
        }

        if (!config?.hideUrl) {
            //#. Label for insert image from an url.
            this.addOption(ImageSourceType.URL, { label: gt("From URL") });
        }
    }
}

// class ImageCropPosition ====================================================

export class ImageCropPosition extends CompoundSplitButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            icon: "bi:crop",
            label: CROP_IMAGE_LABEL,
            tooltip: CROP_IMAGE_TOOLTIP,
            toggle: true,
            ...config
        });

        this.addSection("onoff");
        this.addControl("drawing/crop", new CheckBox({ value: "crop", label: /*#. Label for cropping selected image. */ gt("Crop"), tooltip: gt("Crop image"), boxed: true }));

        this.addSection("options");
        this.addControl("drawing/cropposition/fill", new Button({ value: "fill", label: /*#. Label for fill selected image completely over frame. */ gt("Fill"), tooltip: gt("Scale the image to fill the frame. The image aspect ratio is maintained. The image might be cropped") }));
        this.addControl("drawing/cropposition/fit", new Button({ value: "fit", label: /*#. Label for fit selected image completely into frame. */ gt("Fit"), tooltip: gt("Scale the image to fit the frame. The image aspect ratio is maintained. There might be empty space inside the frame.") }));

        this.addSection("dialog");
        this.addControl("drawing/cropposition/dialog", new Button({ value: "cropposition", label: /*#. Label for openinig crop position dialog. */ gt("Position ..."), tooltip: gt("Open the crop position dialog") }));
    }
}
