/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import CanvasJS from '@canvasjs/charts';

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import {
    TOUCH_DEVICE, Rectangle,
    convertHmmToCssLength, convertHmmToLength, convertLengthToHmm, resolveCssVar,
    createDiv, createSpan, createIcon, insertHiddenNodes
} from '@/io.ox/office/tk/dom';
import { Canvas, Path } from '@/io.ox/office/tk/canvas';
import { LOCALE_DATA } from '@/io.ox/office/tk/locale';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { findFarthest, getBooleanOption, getNumberOption, getObjectOption, parseAndSanitizeHTML } from '@/io.ox/office/tk/utils';
import { SELECTED_CLASS, isSelectedNode } from '@/io.ox/office/tk/forms';

import { getExplicitAttributes, getExplicitAttributeSet, isFillThemed, isLineThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { getBorderPattern, getCssBorderStyle } from '@/io.ox/office/editframework/utils/border';
import Gradient from '@/io.ox/office/editframework/utils/gradient';
import { createCanvasPattern } from '@/io.ox/office/editframework/utils/pattern';
import { getTextureFill } from '@/io.ox/office/editframework/utils/texture';

import {
    DEFAULT_DRAWING_TYPE, DRAWING_TYPES, checkConnectorFlip, createDefaultShapeAttributeSet, getCssTransform, getParagraphAttrsForDefaultShape,
    getRotatedDrawingPoints, isAngleInButterflyArea, isLineEndingShape, isTwoPointShape as isTwoPointShapeImport, rotatePointWithAngle
} from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { calculateBitmapSettings, getFileUrl } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { DrawingModel } from '@/io.ox/office/drawinglayer/model/drawingmodel';
import { CANVAS_EXPANSION_PRESET_SHAPES, getDrawingTypeIcon, getDrawingTypeLabel, getPresetShape, isTwoPointShapeId } from '@/io.ox/office/drawinglayer/view/drawinglabels';
import '@/io.ox/office/drawinglayer/view/drawingstyle.less';
import PRESET_GEOMETRIES from '@/io.ox/office/drawinglayer/view/presetGeometries.json';

const { abs, floor, ceil, round, min, max, sqrt, sin, cos, tan, atan2 } = Math;

// constants ==============================================================

var PI_180 = Math.PI / 180;
var PI_RAD = Math.PI / 10800000;

// name of the jQuery data attribute that stores the unique identifier of a drawing frame node
var DATA_UID = 'uid';

// name of the jQuery data attribute that stores the drawing model instance for a drawing frame node
var DATA_MODEL = 'model';

// the CSS class name of the drawing content node
var CONTENT_CLASS = 'content';

// the CSS class used to crop images
var CROPPING_CLASS = 'cropping-frame';

// the CSS class name of the selection root node
var SELECTION_CLASS = 'selection';

// the CSS class name for the tracker node inside the selection
var TRACKER_CLASS = 'tracker';

// the CSS class name for active tracking mode
var TRACKING_CLASS = 'tracking-active';

var CULTURE_INFO = null;

var PRESET_RECT_CXNLIST = [{ x: 'hc', y: 't', ang: '3cd4' }, { x: 'l', y: 'vc', ang: 'cd2' }, { x: 'hc', y: 'b', ang: 'cd4' }, { x: 'r', y: 'vc', ang: 0 }];

var MAX_CANVAS_SIZE = (_.browser.iOS || _.browser.Android || _.browser.Safari) ? 2156 : 4096;

// cache during resize for texture bitmap used as shape backgound
var textureCanvasPatternCache = [];

// private global functions ===============================================

/**
 * Returns the selection root node of the specified drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The selection root node of the specified drawing frame. Will be an
 *  empty collection, if the drawing frame is not selected currently.
 */
function getSelectionNode(drawingFrame) {
    return $(drawingFrame).first().children('.' + SELECTION_CLASS);
}

/**
 * Returns the path rendering mode for the specified flags, as expected by
 * the rendering methods of a canvas context.
 *
 * @param {Boolean} hasFill
 *  Whether the shape area will be filled.
 *
 * @param {Boolean} hasLine
 *  Whether the shape outline will be rendered.
 *
 * @returns {String|Null}
 *  The path rendering mode as string, if either of the passed flags is
 *  true, otherwise null to indicate that nothing needs to be drawn.
 */
function getRenderMode(hasFill, hasLine) {
    return hasFill ? (hasLine ? 'all' : 'fill') : (hasLine ? 'stroke' : null);
}

/**
 * Calculating an optional increase of a canvas node inside a drawing node caused
 * by specified line endings.
 *
 * @param {Object} lineAttrs
 *  The line attributes of the drawing.
 *
 * @param {Object} geometry
 *  The geometry attribute of a drawing.
 *
 * @param {Number} expansion
 *  The precalculated expansion.
 *
 * @returns {Number[]|Number}
 *  The expansion (in pixel) required for a canvas node, so that the path together
 *  with the line ends can be displayed completely. This can be a single number, so
 *  that every side gets the same expansion, or an object of numbers, with the
 *  properties 'left', 'top', 'right' and 'bottom' representing the specific canvas
 *  expansions for left, top, right and bottom.
 */
function getCanvasExpansionForLineEnds(lineAttrs, geometry, expansion) {

    var // the minimum expansion for arrows in pixel
        arrowExpansion = 12;

    // additional expansion for arrows at line end
    if (lineAttrs && geometry) {
        if ((lineAttrs.headEndType && lineAttrs.headEndType !== 'none') ||
            (lineAttrs.tailEndType && lineAttrs.tailEndType !== 'none')) {

            if ('pathList' in geometry && _.isArray(geometry.pathList) && (geometry.pathList.length > 0)) {

                arrowExpansion =  max(round(2.5 * convertHmmToLength((lineAttrs.width  || 100), 'px')), arrowExpansion);

                if (_.isNumber(expansion)) {
                    expansion += arrowExpansion;
                } else if (_.isObject(expansion)) {
                    for (var key in expansion) { expansion[key] += arrowExpansion; }
                }
            }
        }
    }

    return expansion;
}

/**
 * Calculating an optional increase of a canvas node inside a drawing node.
 * Info: If the canvas needs to be expanded to the left or top, the value for the
 *       expansion needs to be positive.
 *
 * @param {Object} geometry
 *  The geometry attribute of a drawing.
 *
 * @param {Object} snapRect
 *  The dimension of the drawing without any expansion.
 *
 * @param {Object} guideCache
 *  Caching the already calculated guide variables.
 *
 * @param {Number} expansion
 *  The precalculated expansion.
 *
 * @returns {Number[]|Number}
 *  The expansion (in pixel) required for a canvas node, so that the path together
 *  with the line ends can be displayed completely. This can be a single number, so
 *  that every side gets the same expansion, or an object of numbers, with the
 *  properties 'left', 'top', 'right' and 'bottom' representing the specific canvas
 *  expansions for left, top, right and bottom.
 */
function getCanvasExpansion(geometry, snapRect, guideCache, expansion) {

    var // the required expansion to the left
        dLeft = 0,
        // the required expansion to the top
        dTop = 0,
        // the required expansion to the right
        dRight = 0,
        // the required expansion to the bottom
        dBottom = 0,
        // the collector for the calculated extreme values inside a shape
        collector = { cxMin: 0, cxMax: 0, cyMin: 0, cyMax: 0 };

    // helper function for calculating the geometry values
    function getGeoValue(value) {
        return getGeometryValueLocal(value, snapRect, geometry.avList, geometry.gdList, guideCache);
    }

    if (!geometry || !geometry.pathList) { return expansion; }  // do nothing, if no path specified

    // iterating over the path list
    geometry.pathList.forEach(function (onePath) {

        var path        = _.copy(onePath, true),

            widthPx     = snapRect.width,
            heightPx    = snapRect.height,

            logWidth    = ('width' in path) ? path.width : widthPx,
            logHeight   = ('height' in path) ? path.height : heightPx,

            fXScale     = (widthPx / logWidth),
            fYScale     = (heightPx / logHeight);

        if (onePath.commands) {

            const onePathCollector = onePath.commands.reduce(executePathCommands, {
                canvasPath: new Path(),
                cx: 0,
                cy: 0,
                fXScale,
                fYScale,

                getGeometryValue: getGeoValue,

                cxMin: 0,
                cxMax: 0,
                cyMin: 0,
                cyMax: 0
            });

            // collecting the extreme values for every path
            if (onePathCollector.cxMin < collector.cxMin) { collector.cxMin = onePathCollector.cxMin; }
            if (onePathCollector.cxMax > collector.cxMax) { collector.cxMax = onePathCollector.cxMax; }
            if (onePathCollector.cyMin < collector.cyMin) { collector.cyMin = onePathCollector.cyMin; }
            if (onePathCollector.cyMax > collector.cyMax) { collector.cyMax = onePathCollector.cyMax; }
        }
    });

    // comparing the calculated values with the specified snapRect
    if (collector.cxMin < snapRect.left) { dLeft = -(collector.cxMin - snapRect.left); } // using positive value
    if (collector.cxMax > (snapRect.width - snapRect.left)) { dRight = collector.cxMax - (snapRect.width - snapRect.left); }
    if (collector.cyMin < snapRect.top) { dTop = -(collector.cyMin - snapRect.top); } // using positive value
    if (collector.cyMax > (snapRect.height - snapRect.top)) { dBottom = collector.cyMax - (snapRect.height - snapRect.top); }

    if (dLeft || dRight || dTop || dBottom) {
        expansion = { left: round(dLeft), top: round(dTop), right: round(dRight), bottom: round(dBottom) };
    }

    return expansion;
}

/**
 * Checking whether a line specified by two points p1 (x1, y1) and p2 (x2, y2)
 * goes from p2 to p1 to the right direction. If it does not go from left to
 * right, it is checked, if it goes from top to down.
 * This function can be used to check the direction of an arrowhead. This can
 * typically only be used for 'simple' arrows, that are drawn in a canvas from
 * top left to bottom right.
 *
 * @param {Number} x1
 *  The x-position of point 1.
 *
 * @param {Number} y1
 *  The y-position of point 1.
 *
 * @param {Number} x2
 *  The x-position of point 2.
 *
 * @param {Number} y2
 *  The y-position of point 2.
 *
 * @returns {Boolean}
 *  Whether the specified two points describe a line from left-to-right or if
 *  the line is exact vertical from top-to-bottom.
 */
function isRightOrDownDirection(x1, y1, x2, y2) {
    return (x2 < x1) || ((x2 === x1) && (y2 < y1));
}

/**
 * Analyzing a path to get a start or an end point together with a reference
 * point. The reference point can be used to calculate an angle of an arrowhead
 * at the head or the tail of the path.
 *
 * @param {Object} docModel
 *  The application model object
 *
 * @param {Object[]} commands
 *  The command to draw the path. There must be at least two commands in the path.
 *  For the head the first two commands are evaluated, for the tail the final two
 *  paths.
 *
 * @param {Object} [options]
 *  Optional parameter:
 *  - {Boolean} [options.first=false]
 *      If set to true, the head of the path is evaluated, otherwise the tail.
 *  - {Object} [snapRect]
 *     The rectangle with the properties left, top, width and height
 *     for setting the size of the canvas.
 *  - {Object} [avList]
 *     The used adjustment values.
 *  - {Object[]} [gdList]
 *     An array with formulas that can be used.
 *  - {Object} [guideCache]
 *     The cache with already calculated positioning values.
 *
 * @returns {Object}
 *  An object containing the properties 'start' and 'end'. The values of these
 *  properties are again objects with the properties 'x' and 'y' that are
 *  calculated from the commands.
 */
function getReferencePointsFromPath(docModel, commands, options) {

    // whether the first visible line of the path is searched
    var first = getBooleanOption(options, 'first', false);
    // the first visible line of the path
    var length = 0;
    // the point of the first or the second last command
    var start = {};
    // the point of the second or the last command
    var end = {};
    // the first command of the command list
    var firstCommand = null;
    // the last command of the command list
    var secondCommand = null;
    // the end point of a curved line
    var endPoint = null;
    // the reference point of a curved line
    var refPoint = null;
    // the number of point of a specific path command
    var pointCount = 0;
    // whether this is an ODF document
    var isODF = docModel.getApp().isODF();
    // a largeNumber that can be used for atan calculations
    var LARGE_NUMBER = 100;

    // helper function, that calculates, if a specified reference point is left
    // of the start point or right from the end point at bezier curves.
    // pointA is the start or end point, pointB the corresponding reference point.
    function isOutside(pointA, pointB, isStart) {

        var pointAX = 0;
        var pointBX = 0;
        var isOutside = false;

        if (options.snapRect && options.avList && options.gdList && options.guideCache) {
            pointAX = getGeometryValueLocal(pointA.x, options.snapRect, options.avList, options.gdList, options.guideCache);
            pointBX = getGeometryValueLocal(pointB.x, options.snapRect, options.avList, options.gdList, options.guideCache);
            isOutside = isStart ? (pointBX < pointAX) : (pointBX > pointAX);
        } else if (_.isNumber(pointA.x) && _.isNumber(pointB.x)) { // ODT
            isOutside = isStart ? (pointB.x < pointA.x) : (pointB.x > pointA.x);
        }

        return isOutside;
    }

    if (first) {

        firstCommand = commands[0];
        secondCommand = commands[1];

        start.x = firstCommand.x;
        start.y = firstCommand.y;

        end.x = secondCommand.x;
        end.y = secondCommand.y;

        if (end.x === undefined && start.x !== undefined &&  secondCommand.pts) { // handling of non-linear curves (cubicBezierTo)
            end.x = start.x;
            end.y = start.y;

            // Improving handling in ODF (58464)
            var firstPoint = (isODF && _.isNumber(firstCommand.x)) ? firstCommand : secondCommand.pts[0];
            var secondPoint = (isODF && _.isNumber(firstCommand.x)) ? secondCommand.pts[0] : secondCommand.pts[1];

            // comparing start point with first reference point to check direction of curve
            end.xOffset = isOutside(firstPoint, secondPoint, true) ? -5 : +5; // must be negative, if the bezier point is outside the shape -> enabling valid direction of arrow at line start

            // handling vertical alignment of arrow, if height is larger than width (58464)
            if (isODF && _.isNumber(firstCommand.x)) {
                end.yOffset = (secondCommand.pts[0].y > secondCommand.pts[1].x) ? LARGE_NUMBER : 0;
            }
        }

    } else {

        length = commands.length;
        firstCommand = commands[length - 2];
        secondCommand = commands[length - 1];

        start.x = firstCommand.x;
        start.y = firstCommand.y;

        end.x = secondCommand.x;
        end.y = secondCommand.y;

        if ((start.x === undefined || end.x === undefined)  &&  secondCommand.pts) { // handling of non-linear curves (cubicBezierTo)

            // start.x can be undefined in PP files, end.x is undefined in odf files (example 58464)

            pointCount = secondCommand.pts.length;

            endPoint = secondCommand.pts[pointCount - 1];
            refPoint = secondCommand.pts[pointCount - 2];

            start.x = endPoint.x;
            start.y = endPoint.y;

            end.x = endPoint.x;
            end.y = endPoint.y;

            // comparing end point with last reference point to check direction of curve
            start.xOffset = isOutside(endPoint, refPoint, false) ? 5 : -5; // must be positive, if the bezier point is outside the shape -> enabling valid direction of arrow at line end

            // handling vertical alignment of arrow, if height is larger than width (58464)
            if (isODF && _.isNumber(firstCommand.x)) {
                start.yOffset = (secondCommand.pts[0].y > secondCommand.pts[1].x) ? LARGE_NUMBER : 0;
            }

        }
    }

    return { start, end };
}

/**
 * Handling the line ends for a specified path. The line end is handled by the line attributes
 * "headEndType", "headEndLength", "headEndWidth", "tailEndType", "tailEndLength" and "tailEndWidth".
 *
 * @param {Object} docModel
 *  The application model object
 *
 * @param {Canvas2DContext} context
 *  Canvas context wrapper.
 *
 * @param {Object} lineAttrs
 *  Object containing line attributes for the canvas
 *
 * @param {Number} lineWidth
 *  The line width in pixel.
 *
 * @param {Object} color
 *  The line color object.
 *
 * @param {Object} path
 *  The line path element.
 *
 * @param {String} arrowType
 *  The type of the line ending. Supported values are:
 *  'oval', 'diamond', 'triangle', 'stealth' and 'arrow'.
 *
 * @param {String} endType
 *  The end type for the specified path. Supported values are:
 *  'head' and 'tail'.
 *
 * @param {Object} [options]
 *  Optional parameter:
 *  - {Object} [snapRect]
 *     The rectangle with the properties left, top, width and height
 *     for setting the size of the canvas.
 *  - {Object} [avList]
 *     The used adjustment values.
 *  - {Object[]} [gdList]
 *     An array with formulas that can be used.
 *  - {Object} [guideCache]
 *     The cache with already calculated positioning values.
 *
 * @returns {Array<Object>|Null}
 *  The collector for the paths that are necessary to draw the line endings. This can be an
 *  array, that already contains paths, or an empty array, or null.
 */
function handleLineEnd(docModel, context, lineAttrs, lineWidth, color, path, arrowType, endType, options) {

    // an additional line end path
    var lineEndPath = null;
    // the length of the arrow (in pixel)
    var length = 0;
    // the width of the arrow (in pixel)
    var width = 0;
    // whether the arrow head required a 'fill' additionally to the 'stroke' for the path
    var fillRequired = false;
    // a collector for all commands of the path
    var allCommands = null;
    // an object containing a start and an end point received from the commands of the path
    var refPoints = null;
    // the start point object received from the specified path
    var start = null;
    // the end point object received from the specified path
    var end = null;

    // helper function to get display factors for different line widths
    // values 2, 4, 6 for lineWidth < 2
    // values 1, 2, 3 for lineWidth 2 < 8
    // values 0.5, 1, 1.5 for lineWidth > 8
    function getFactor(type, wid, aType) {

        var factor = 1;
        var arrowFactor = 1;
        var ovalFactor = 1.3;

        if (wid <= 2) {  // larger values for thickness below 3 px
            factor = 3;
            if (type === 'small') {
                factor = 2;
            } else if (type === 'large') {
                factor = 5;
            }
        } else {
            factor = 0.75; //  1.5;
            if (type === 'small') {
                factor = 0.5; // 1;
            } else if (type === 'large') {
                factor = 1.25; // 2.5;
            }
        }

        // additionally increasing length and width for arrows
        if (aType === 'arrow') {
            arrowFactor = 1.5;
            if (type === 'small') {
                arrowFactor = 1.8;
            } else if (type === 'large') {
                arrowFactor = 1.2;
            }

            factor *= arrowFactor;
        } else if (aType === 'oval') {
            factor *= ovalFactor; // small increasement for circles and ellipses
        }

        return factor;
    }

    // helper function to get line property values for width and height in pixel
    function getLineEndAttribute(attrs, property, type) {

        var value = getFactor('medium', lineWidth, type) * lineWidth;
        if (attrs && attrs[property] === 'small') {
            value = getFactor('small', lineWidth, type) * lineWidth;
        } else if (attrs && attrs[property] === 'large') {
            value = getFactor('large', lineWidth, type) * lineWidth;
        }

        return value;
    }

    if (endType === 'head') {

        if (path.commands.length < 2) { return null; }

        lineEndPath = _.copy(path, true);
        lineEndPath.commands = [];
        allCommands = path.commands; // the array with the commands

        options = options || {};
        options.first = true;
        refPoints = getReferencePointsFromPath(docModel, allCommands, options);

        start = refPoints.start;
        end = refPoints.end;

        length = getLineEndAttribute(lineAttrs, 'headEndLength', arrowType);
        width = getLineEndAttribute(lineAttrs, 'headEndWidth', arrowType);

        if (arrowType === 'oval') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'oval', x: start.x, y: start.y, r1: length, r2: width, rpx: end.x, rpy: end.y, rpxOffset: (end.xOffset || 0), rpyOffset: (end.yOffset || 0) });
        } else if (arrowType === 'diamond') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'diamond', x: start.x, y: start.y, r1: length, r2: width, rpx: end.x, rpy: end.y, rpxOffset: (end.xOffset || 0), rpyOffset: (end.yOffset || 0) });
        } else if (arrowType === 'triangle') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'triangle', x: start.x, y: start.y, r1: length, r2: width, rpx: end.x, rpy: end.y, rpxOffset: (end.xOffset || 0), rpyOffset: (end.yOffset || 0) });
        } else if (arrowType === 'stealth') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'stealth', x: start.x, y: start.y, r1: length, r2: width, rpx: end.x, rpy: end.y, rpxOffset: (end.xOffset || 0), rpyOffset: (end.yOffset || 0) });
        } else if (arrowType === 'arrow') {
            lineEndPath.commands.push({ c: 'arrow', x: start.x, y: start.y, r1: length, r2: width, rpx: end.x, rpy: end.y, rpxOffset: (end.xOffset || 0), rpyOffset: (end.yOffset || 0) });
        }

    } else if (endType === 'tail') {

        if (path.commands.length < 2) { return null; }

        lineEndPath = _.copy(path, true);
        lineEndPath.commands = [];
        allCommands = path.commands; // the array with the commands

        options = options || {};
        options.first = false;
        refPoints = getReferencePointsFromPath(docModel, allCommands, options);

        start = refPoints.start;
        end = refPoints.end;

        length = getLineEndAttribute(lineAttrs, 'tailEndLength', arrowType);
        width = getLineEndAttribute(lineAttrs, 'tailEndWidth', arrowType);

        if (arrowType === 'oval') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'oval', x: end.x, y: end.y, r1: length, r2: width, rpx: start.x, rpy: start.y, rpxOffset: (start.xOffset || 0), rpyOffset: (start.yOffset || 0) });
        } else if (arrowType === 'diamond') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'diamond', x: end.x, y: end.y, r1: length, r2: width, rpx: start.x, rpy: start.y, rpxOffset: (start.xOffset || 0), rpyOffset: (start.yOffset || 0) });
        } else if (arrowType === 'triangle') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'triangle', x: end.x, y: end.y, r1: length, r2: width, rpx: start.x, rpy: start.y, rpxOffset: (start.xOffset || 0), rpyOffset: (start.yOffset || 0) });
        } else if (arrowType === 'stealth') {
            context.setFillStyle(color);
            fillRequired = true;
            lineEndPath.commands.push({ c: 'stealth', x: end.x, y: end.y, r1: length, r2: width, rpx: start.x, rpy: start.y, rpxOffset: (start.xOffset || 0), rpyOffset: (start.yOffset || 0) });
        } else if (arrowType === 'arrow') {
            lineEndPath.commands.push({ c: 'arrow', x: end.x, y: end.y, r1: length, r2: width, rpx: start.x, rpy: start.y, rpxOffset: (start.xOffset || 0), rpyOffset: (start.yOffset || 0) });
        }

    }

    if (lineEndPath && lineEndPath.commands && lineEndPath.commands.length > 0) {
        lineEndPath.drawMode = fillRequired ? 'all' : 'stroke';
        return lineEndPath;
    }

    return null;
}

function getLineWidthPx(lineAttrs) {
    return lineAttrs.width ? max(1, convertHmmToLength(lineAttrs.width, 'px', 1)) : 1;
}

/**
 * Sets the line attributes to the given canvas context.
 *
 * @param {Object} docModel
 *  The application model object
 *
 * @param {Canvas2DContext} context
 *  Canvas context wrapper.
 *
 * @param {Object} lineAttrs
 *  Object containing line attributes for the canvas
 *
 * @param {Number} [lineWidth]
 *  Optional parameter for line width in px.
 *
 * @param {Object} [path]
 *  Optional parameter for the line path.
 *
 * @param {Object} [options]
 *  Optional parameter for the line handling:
 *  - {Boolean} [options.isFirstPath=false]
 *      If set to true, this is the first path element of a specified path.
 *  - {Boolean} [options.isLastPath=false]
 *      If set to true, this is the last path element of a specified path.
 *  - {Object} [snapRect]
 *     The rectangle with the properties left, top, width and height
 *     for setting the size of the canvas.
 *  - {Object} [avList]
 *     The used adjustment values.
 *  - {Object[]} [gdList]
 *     An array with formulas that can be used.
 *  - {Object} [guideCache]
 *     The cache with already calculated positioning values.
 *
 * @returns {Array<Objects>}
 *  An array containing additional paths that are necessary to draw the line
 *  endings.
 */
function setLineAttributes(docModel, context, lineAttrs, lineWidth, path, options) {

    var lineColor = docModel.getCssColor(lineAttrs.color, 'line', lineAttrs[TARGET_CHAIN_PROPERTY]);
    var locLineWidth = lineWidth || getLineWidthPx(lineAttrs);
    // make line width always even value (bug 53500: but not for preview shapes in dropdown to prevent blurryness)
    if (!options?.preview && (locLineWidth % 2 > 0)) { locLineWidth++; }
    var pattern = getBorderPattern(lineAttrs.style, locLineWidth);
    var isArrowShape = getBooleanOption(options, 'isArrowShape', false); // taking care of 54222

    context.setLineStyle({ style: lineColor, width: locLineWidth, pattern });

    var newPaths = [];
    if (isArrowShape && path && lineAttrs) {
        if (getBooleanOption(options, 'isFirstPath', false) && lineAttrs.headEndType && lineAttrs.headEndType !== 'none') {
            var firstPath = handleLineEnd(docModel, context, lineAttrs, locLineWidth, lineColor, path, lineAttrs.headEndType, 'head', options);
            if (firstPath) { newPaths.push(firstPath); }
        }
        if (getBooleanOption(options, 'isLastPath', false) && lineAttrs.tailEndType && lineAttrs.tailEndType !== 'none') {
            var lastPath = handleLineEnd(docModel, context, lineAttrs, locLineWidth, lineColor, path, lineAttrs.tailEndType, 'tail', options);
            if (lastPath) { newPaths.push(lastPath); }
        }
    }
    return newPaths;
}

/**
 * Sets the bitmap texture fill style to the given canvas context, according to different properties.
 *
 * @param {DocumentModel}  docModel
 *  Current document model.
 * @param {Canvas2DContext}  context
 *  Canvas context wrapper.
 * @param {function}  drawMethod
 *   Method which should be called for drawing canvas
 * @param {CanvasPattern}  fillStyle
 * @param {Number}  widthPx
 *  Width of shape in pixels.
 * @param {Number}  heightPx
 *  Height of shape in pixels.
 * @param {Boolean} hasLine
 *  If shape has line or not.
 * @param {Object}  lineAttrs
 *  Attribute set for the line.
 * @param {Boolean} isTiling
 *  If bitmap is tiling.
 * @param {Boolean}  notRotatedWithShape
 *  If bitmap should contain its own rotation angle and not rotate with shape.
 * @param {Number|null}  rotationAngle
 *  If exists and shape shouldn't rotate with shape, use it as counter angle for texture.
 */
function setBitmapTextureFillStyle(docModel, context, drawMethod, fillStyle, widthPx, heightPx, hasLine, lineAttrs, isTiling, notRotatedWithShape, rotationAngle) {
    var nWidth, nHeight;

    context.clearRect(0, 0, widthPx, heightPx);
    if (notRotatedWithShape && rotationAngle) {
        var rectPos = max(widthPx, heightPx);
        // initialize context with fill to mask path with pattern
        context.setFillStyle('#000000');
        drawMethod({ mode: 'fill' });

        // local transformations will be reset after the callback has been executed
        context.render(function () {

            context.translate(widthPx / 2, heightPx / 2);
            context.rotate(rotationAngle);
            context.setFillStyle(fillStyle);
            context.setGlobalComposite('source-in'); // The new shape is drawn only where both the new shape and the destination canvas overlap. Everything else is made transparent.

            if (notRotatedWithShape && !isTiling) {
                nWidth = widthPx * abs(cos(rotationAngle)) + heightPx * abs(sin(rotationAngle));
                nHeight = heightPx * abs(cos(rotationAngle)) + widthPx * abs(sin(rotationAngle));
                context.translate(-nWidth / 2, -nHeight / 2);
                context.drawRect(0, 0, nWidth, nHeight, 'fill'); // cover whole canvas area with pattern fill
            } else {
                context.drawRect(2 * -rectPos, 2 * -rectPos, 4 * rectPos, 4 * rectPos, 'fill'); // cover whole canvas area with pattern fill
            }
        });

        if (hasLine) {
            setLineAttributes(docModel, context, lineAttrs);
        }
        drawMethod({ mode: 'stroke' }); // stroke needs to be painted again after overlap
    } else {
        context.setFillStyle(fillStyle);
        // initialize context with proper line attributes for the next path
        if (hasLine) {
            setLineAttributes(docModel, context, lineAttrs);
        }
        drawMethod();
    }
}

/**
 * fix for Bug 48477 explicit style-entries contentNode got lost,
 * so we changed to css-classes
 * html-attributes (interpredet by css would be also a nice solution)
 */
function handleFlipping(docModel, drawingFrame, drawingAttributes) {

    var angle = drawingAttributes.rotation || 0;
    var textFrameNode = getOriginalTextFrameNode(drawingFrame);
    var isFlipH = false;
    var isFlipV = false;

    if (drawingAttributes) {
        // convert to single jQuery object
        drawingFrame = $(drawingFrame).first();

        isFlipH = !!drawingAttributes.flipH;
        isFlipV = !!drawingAttributes.flipV;
    }
    setCssTransform(drawingFrame, angle, isFlipH, isFlipV);

    drawingFrame.toggleClass(FLIPPED_HORIZONTALLY_CLASSNAME, isFlipH);
    drawingFrame.toggleClass(FLIPPED_VERTICALLY_CLASSNAME, isFlipV);

    textFrameNode.toggleClass(FLIPPED_HORIZONTALLY_CLASSNAME, isOddFlipHVCount(textFrameNode));
}

// updating the size attributes at an image inside a drawing (also supported inside a drawing group)
function setImageCssSizeAttributes(app, drawing, imageParent, drawingWidth, drawingHeight, drawingAttrs, imageAttrs) {

    var docModel = app.getModel(),
        // IE requires to swap cropping settings if image is flipped
        swapHorCrop = _.browser.IE && drawingAttrs.flipH,
        swapVertCrop = _.browser.IE && drawingAttrs.flipV,
        // effective cropping values (checking for 0, because explicit attributes might be used)
        cropLeft = swapHorCrop ? ((imageAttrs && imageAttrs.cropRight) || 0) : ((imageAttrs && imageAttrs.cropLeft) || 0),
        cropRight = swapHorCrop ? ((imageAttrs && imageAttrs.cropLeft) || 0) : ((imageAttrs && imageAttrs.cropRight) || 0),
        cropTop = swapVertCrop ? ((imageAttrs && imageAttrs.cropBottom) || 0) : ((imageAttrs && imageAttrs.cropTop) || 0),
        cropBottom = swapVertCrop ? ((imageAttrs && imageAttrs.cropTop) || 0) : ((imageAttrs && imageAttrs.cropBottom) || 0),
        // horizontal offset/size of cropped bitmaps, as CSS attributes
        horizontalSettings = calculateBitmapSettings(app, drawingWidth, cropLeft, cropRight),
        // vertical offset/size of cropped bitmaps, as CSS attributes
        verticalSettings = calculateBitmapSettings(app, drawingHeight, cropTop, cropBottom),
        cssProps = {
            left: horizontalSettings.offset,
            top: verticalSettings.offset,
            width: horizontalSettings.size,
            height: verticalSettings.size
        },
        isTextAppPageAligned = (drawingAttrs.anchorHorBase === 'page' && drawingAttrs.anchorVertBase === 'page');

    // workaround for Bug 48435, and 55564
    if (app.isPresentationApp() || isTextAppPageAligned) { cssProps.position = 'absolute'; }
    if (drawing.hasClass(FORCE_INLINE_CLASS)) { cssProps.position = ''; } // removing position 'absolute' in draft mode or on small devices

    // leave crop mode on undo/redo
    if (docModel.isUndoRedoRunning() && docModel.getSelection().isCropMode()) {
        docModel.exitCropMode();
        //docModel.refreshCropFrame();
    }
    imageParent.find('img').css(cssProps);
}

/**
 * Updates the fill formatting of the drawing frame.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {jQuery} [contentNode]
 *  An optional jQuery node, to which the css properties are
 *  assigned. If it is not set, the content node inside the
 *  drawing frame is used. Setting this node, is only required
 *  for drawing groups, where all children need to be modified.
 *
 * @param {Object} mergedAttributes
 *  Merged attributes of the given drawing node.
 */
function updateFillFormatting(app, contentNode, mergedAttributes) {

    var // the document model
        docModel = app.getModel(),
        // the CSS properties for the drawing frame
        cssProps = {},
        // the node, whose properties will be set
        cssNode = contentNode,
        // the fill attributes
        fillAttrs = mergedAttributes.fill;

    if (fillAttrs) {
        // calculate the CSS fill attributes
        switch (fillAttrs.type) {
            case 'none':
                // clear everything: color and bitmaps
                cssProps.background = '';
                break;
            case 'solid':
                // use 'background' compound attribute to clear bitmaps
                cssProps.background = docModel.getCssColor(fillAttrs.color, 'fill', fillAttrs[TARGET_CHAIN_PROPERTY]);
                break;
            default:
                globalLogger.warn('DrawingFrame.updateFillFormatting(): unknown fill type "' + fillAttrs.type + '"');
        }

        // apply the fill attributes
        cssNode.css(cssProps);
    }
}

/**
 * Updates the border and filling formatting of the drawing frame.
 * This function generates a Canvas node, if required. It is not used for
 * shapes, but first of all for images. Setting background for images
 * is visible for png-files or cropped images. This is handled by
 * task 57884.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {jQuery} node
 *  jQuery node, to which the css properties are
 *  assigned. If it is not used, the content node inside the
 *  drawing frame is passed. Setting this node, is only required
 *  for drawing groups, where all children need to be modified.
 *
 * @param {Object} mergedAttributes
 *  Merged attributes of the given drawing node.
 *
 * @param {Object} [options]
 *  - {Boolean} [options.useExplicitAttrs=false]
 *      If explicit drawing attributes should be used
 *  - {Boolean} [options.linesOnly=false]
 *      Whether only line attributes shall be handled. This is required for
 *      legacy reasons.
 */
function updateLineAndFillFormatting(app, node, mergedAttributes, options) {

    var // gets the correct node to work on (parent() used at grouped drawings)
        currentNode = getDrawingNode(node),
        // if explicit drawing attributes should be used
        useExplicit = getBooleanOption(options, 'useExplicitAttrs', false),
        // if explicit drawing attributes should be used
        linesOnly = getBooleanOption(options, 'linesOnly', false),
        // the line attributes
        lineAttrs = useExplicit ? getExplicitAttributes(currentNode, 'line') : mergedAttributes.line,
        // the fill attributes
        fillAttrs = linesOnly ? null : (useExplicit ? getExplicitAttributes(currentNode, 'fill') : mergedAttributes.fill);

    // Explicit attributes are not sufficient, if default line attributes are used (36892)
    if (currentNode && useExplicit && (!lineAttrs || !lineAttrs.type)) {
        lineAttrs = app.docModel.drawingStyles.getElementAttributes(node).line;
    }

    if (lineAttrs && lineAttrs.type && lineAttrs.type !== 'solid') { // type solid is fully supported for lines
        if (lineAttrs.type !== 'none') { globalLogger.warn('DrawingFrame.updateLineAndFillFormatting(): unknown line type "' + lineAttrs.type + '"'); }
        lineAttrs = null;
    }

    if (fillAttrs && fillAttrs.type && fillAttrs.type === 'none') { fillAttrs = null; }

    if (lineAttrs || fillAttrs) {
        drawCanvasBorderAndFilling(app, currentNode, lineAttrs, fillAttrs);
    } else {
        clearCanvas(app, currentNode);
    }
}

/**
 * Handling an optionally required div-element in those drawings, that contain
 * a canvas node larger than the drawing itself. This helper drawing allows moving
 * and resizing and acts like a filter to catch events for the drawing.
 *
 * @param {jQuery} node
 *  The drawing node.
 *
 * @param {Number|Number[]} expansion
 *  The object that contains the information about the size of the canvas.
 */
function handleDrawingExpansionNode(node, expansion) {

    var // an optionally already existing expansion node
        oldExpansionNode = null,
        // an optionally required new expansion node
        expansionNode = null,
        // the minimum width and height for a drawing without expansion node in px
        minValue = 8,
        // the width and height of the drawing node
        width = 0, height = 0;

    // helper function to calculate the required size for the
    function calculateDrawingExpansion(drawingNode, exp) {

        var width = drawingNode.width();
        var height = drawingNode.height();
        var left = 0;
        var top = 0;

        if (_.isNumber(exp)) {
            left = -exp;
            top = -exp;
            width += (2 * exp);
            height += (2 * exp);
        } else if (_.isObject(exp)) {
            left = -exp.left;
            top = -exp.top;
            width = width + exp.left + exp.right;
            height = height + exp.top + exp.bottom;
        }

        return { left, top, width, height };
    }

    oldExpansionNode = node.children(CANVAS_EXPANSION_SELECTOR); // searching an already existing child node

    if (!expansion) {

        width = node.width();
        height = node.height();

        if (width < minValue || height < minValue) {
            expansion = minValue - min(width, height); // setting expansion also for thin lines
        }
    }

    if (expansion) {
        expansionNode = (oldExpansionNode.length > 0) ? oldExpansionNode : $('<div class="' + CANVAS_EXPANSION_CLASS + '">');
        expansionNode.css(calculateDrawingExpansion(node, expansion));
        if (oldExpansionNode.length === 0) { node.append(expansionNode); }
        // node.addClass(DrawingFrame.CONTAINS_CANVAS_EXPANSION); // Setting marker at the drawing node itself
    } else {
        if (oldExpansionNode.length > 0) { oldExpansionNode.remove(); }
        // node.removeClass(DrawingFrame.CONTAINS_CANVAS_EXPANSION); // Setting marker at the drawing node itself
    }

}

/**
 * Tries to find cached canvas texture bitmap pattern.
 *
 * @param {String} drawingUID
 *  Unique id of the drawing.
 *
 * @returns {CanvasWrapperObject|Null}
 */
function getCachedTextureFill(drawingUID) {
    return _.find(textureCanvasPatternCache, function (obj) { return obj.id === drawingUID; });
}

/**
 * Saves canvas texture bitmap pattern for reuse.
 *
 * @param {String} drawingUID
 * @param {CanvasWrapperObject} textureFillStyle
 */
function setCachedTextureFill(drawingUID, textureFillStyle) {
    textureCanvasPatternCache.push({ id: drawingUID, texture: textureFillStyle });
}

/**
 * Updating the 'shape' attributes at the drawing.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {jQuery} drawingFrame
 *  The DOM drawing frame.
 *
 * @param {Object} mergedAttributes
 *  Merged attributes of the drawing node.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.isResizeActive=false]
 *      If this method is called during resizing of the shape.
 *  - {Boolean} [options.textFrameOnly=false]
 *      If set to true, only the position and size of the text frame will
 *      be updated.
 *  - {Boolean} [options.useExplicitAttrs=false]
 *      If explicit drawing attributes should be used
 */
function updateShapeFormattingLocal(app, drawingFrame, mergedAttributes, options) {

    var // the document model
        docModel = app.getModel(),
        // if formatting is called during resize
        isResizeActive = getBooleanOption(options, 'isResizeActive', false),
        // if explicit drawing attributes should be used
        useExplicit = getBooleanOption(options, 'useExplicitAttrs', false),
        // whether to use modified geometry attributes (e.g. live preview of adjustment points shape)
        modAttrs = getObjectOption(options, 'modAttrs', null),
        // whether the drawing frame is used as internal model container (reduced rendering)
        internalModel = (options && options.textFrameOnly) || drawingFrame.hasClass(INTERNAL_MODEL_CLASS),
        // gets the correct node to work on (parent() used at grouped drawings)
        currentNode = getDrawingNode(drawingFrame),
        // the content node inside the drawing frame
        contentNode = getContentNode(currentNode, options), // needs to be fetched each time, because of grouped drawings, see #47962
        // type of the drawing object
        drawingType = getDrawingType(drawingFrame),
        // the explicit attributes of the handled node
        explicitAttributes = getExplicitAttributeSet(currentNode),
        // the shape attributes
        shapeAttrs = useExplicit ? explicitAttributes.shape : mergedAttributes.shape,
        // the line attributes
        lineAttrs = useExplicit ? explicitAttributes.line : mergedAttributes.line,
        // the fill attributes (not supporting fill color for images (57748))
        fillAttrs =  (drawingType === 'image') ? null : (useExplicit ? explicitAttributes.fill : mergedAttributes.fill),
        // shape geometry
        geometryAttrs = explicitAttributes.geometry ? (modAttrs ? _.extend(explicitAttributes.geometry, modAttrs) : explicitAttributes.geometry) : mergedAttributes.geometry,
        // the canvas wrapper
        canvas = null,
        // copied node of textframecontent
        textFrameContentCopy = currentNode.children('.copy'),
        // the text frame node of the drawing
        textFrame = isResizeActive ? textFrameContentCopy.children('.textframe') : getTextFrameNode(currentNode),
        // if no word wrap property is set
        noWordWrap = shapeAttrs && shapeAttrs.wordWrap === false,
        // variables for storing current width and height
        currentHeight = null,
        currentWidth = null,
        // whether autoResizeHeight is enabled for the drawing
        autoResizeHeight = isAutoResizeHeightAttributes(shapeAttrs);

    // depending on angle, orientate text (horz/vert/vert reversed) inside shape
    function orientateTextInShape(angle) {
        // width and height of shape node
        var nodeWidth, nodeHeight, maxHeight;
        // delta values to normalize top and left positions of rotated textframe inside shape
        var topDelta, leftDelta;
        // deviation from default rotation
        var notDefaultAngle = angle !== 0;
        // horizontal flip of textframe
        var textFrameFlipH = isFlippedHorz(textFrame);
        // part of the transform string responsible for flipping, that goes infront
        var scaleString = textFrameFlipH ? 'scaleX(-1) ' : '';

        currentNode.data({ widthHeightRev: notDefaultAngle });
        if (notDefaultAngle) {
            nodeWidth = textFrame.width();
            nodeHeight = textFrame.height();
            leftDelta = parseInt(textFrame[0].style.left, 10) + (nodeWidth - nodeHeight) / 2;
            topDelta = parseInt(textFrame[0].style.top, 10) + (nodeHeight - nodeWidth) / 2;
            maxHeight = textFrame[0].style.maxHeight;

            textFrame
                .css({ width: nodeHeight,
                    height: nodeWidth,
                    maxHeight: '',
                    maxWidth: maxHeight,
                    top: topDelta,
                    left: leftDelta,
                    transform: scaleString + 'rotate(' + angle + 'deg)' }) // scale needs to go before rotate (opposite from getCssTransform)
                .addClass(ROTATED_TEXT_IN_DRAWING_CLASSNAME);
        } else {
            textFrame.css({ transform: scaleString + 'rotate(0deg)' }).removeClass(ROTATED_TEXT_IN_DRAWING_CLASSNAME);
        }
    }

    /**
     * Getting width and height of the drawing.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Boolean} [options.getminimumheight=false]
     *      If the drawing grows or shrinks with its content ('autoResizeHeight' is 'true'), it is necessary to receive the minimum height
     *      of the drawing content (52953). This is the height of the text frame (without padding), not the height of the drawing itself.
     *      This value is only evaluated, if 'autoResizeHeight' is 'true'.
     *      The default is, that the height of the drawing node is returned.
     *  - {Boolean} [options.getminimumwidth=false]
     *      If the drawing grows horizontally with its content ('noWordWrap' is 'true'), it is necessary to receive the minimum width
     *      of the drawing content (55717).
     *      This value is only evaluated, if 'noWordWrap' is 'true'.
     *      The default is, that the width of the drawing node is returned.
     */
    function getSnapRect(options) {
        var snapRect = new Rectangle(0, 0, 0, 0);
        if (isResizeActive) {
            snapRect.width = textFrameContentCopy.outerWidth(true);
            snapRect.height = textFrameContentCopy.outerHeight(true);
        } else {
            snapRect.width = (noWordWrap && getBooleanOption(options, 'getminimumwidth', false)) ? textFrame.outerWidth(true) : currentNode.first().width();
            snapRect.height = (autoResizeHeight && getBooleanOption(options, 'getminimumheight', false)) ? textFrame.first().height() : currentNode.first().height();
        }

        // Bug 57730: the minimum size of a snapRect should be 1px. otherwise the vertical direction will be calculated incorrectly.
        if (snapRect.width < 1) { snapRect.width = 1; }
        if (snapRect.height < 1) { snapRect.height = 1; }

        return snapRect;
    }

    /**
     * Returns preset geometry attributes of the current drawing shape.
     *
     * @param {Object} geometryAttrs
     *  Geometry attributes of the current shape, stored from the operation.
     * @param {Object} PRESET_SHAPE_GEO
     *  Preset geometry of the current shape.
     *
     * @returns {Object}
     */
    function getPresetShapeGeometry(geometryAttrs, PRESET_SHAPE_GEO) {
        var presetGeo = _.copy(PRESET_SHAPE_GEO, true); // always generate a clone to avoid modifications (55791)
        var geo = _.extend({}, geometryAttrs, presetGeo);

        if (!_.isEmpty(geometryAttrs.avList)) { // explicit attributes in avList must overwrite again
            geo.avList = _.extend({}, geo.avList, geometryAttrs.avList);
        }
        return geo;
    }

    function drawGeometry(geometry) {

        if (!docModel || docModel.destroyed) { return; } // the model might have been destroyed in the meantime

        function getTextRectValue(value, defaultValue) {
            return value ? round(getGeometryValueLocal(value, snapRect, geometry.avList, geometry.gdList, guideCache)) : defaultValue;
        }

        var visibilityChanged = false;
        var lineWidth = 1;
        var expansion = 0; // additional canvas size outside the snap rect
        var guideCache = {}; // caching the already calculated guide variables.
        var presetShape = (geometry && geometry.presetShape) ? geometry.presetShape : ''; // the value of the preset shape property
        var maxSizeScale = 0;
        var boundRect = null;
        // the canvas wrapper of the border node
        var snapRect = getSnapRect({ getminimumheight: true, getminimumwidth: true });

        if (lineAttrs && lineAttrs.width) { lineWidth = getLineWidthPx(lineAttrs); }

        // taking care of special text rects
        if (textFrame.length > 0) {
            var paddingLeft     = convertHmmToLength(mergedAttributes.shape.paddingLeft, 'px');
            var paddingRight    = convertHmmToLength(mergedAttributes.shape.paddingRight, 'px');
            var left            = getTextRectValue(geometry.textRect ? geometry.textRect.l : null, snapRect.left) + paddingLeft;
            var top             = getTextRectValue(geometry.textRect ? geometry.textRect.t : null, snapRect.top) + convertHmmToLength(mergedAttributes.shape.paddingTop, 'px');
            var right           = getTextRectValue(geometry.textRect ? geometry.textRect.r : null, snapRect.left + snapRect.width) - paddingRight;
            var paddingBottom   = (snapRect.height - getTextRectValue(geometry.textRect ? geometry.textRect.b : null, (snapRect.top + snapRect.height) - 1));

            if (app.isPresentationApp()) {
                paddingBottom += convertHmmToLength(mergedAttributes.shape.paddingBottom, 'px');
            }

            if (docModel.useSlideMode()) {
                // workaround for Bug 48476, drawings in powerpoint are 2px bigger than they save in document, reduce padding is easier than changing all sizes
                left -= 1; right += 2;
            }

            left            = round(left);
            top             = round(top);
            right           = round(right);
            paddingBottom   = round(paddingBottom);

            // workarounds for vertical aligment @see drawingstyle.less div.textframe
            // Bug 48665, Bug 46803, Bug 42893, Bug 44965, Bug 52808, Bug 52226 & Bug 51824

            textFrame.css({ left: left + 'px', top: top + 'px', width: (right - left) + 'px', height: 'calc(100% - ' + (top + paddingBottom) + 'px)', marginBottom: round(paddingBottom + top + convertHmmToLength(mergedAttributes.shape.paddingBottom, 'px')) + 'px' });
            if (noWordWrap) {
                textFrame.css({ position: 'relative', width: 'auto', left: '', marginLeft: paddingLeft, marginRight: paddingRight, whiteSpace: 'nowrap' });
            }

            // IE 11 has focus problems with combination of contenteditable & flexbox
            textFrame.toggleClass('no-flex', _.browser.IE <= 11);

            // restored this code from history for Bug 51185
            // the textframe been changed so also the snaprect, we have to reset the guideCache and also
            // reset the new snaprect
            guideCache = {};

            if (docModel.handleContainerVisibility) {
                visibilityChanged = docModel.handleContainerVisibility(currentNode.first(), { makeVisible: true });
            }
            snapRect = getSnapRect();  // getting snap rect again, this time using current node also if autoResizeHeight is true
            if (visibilityChanged && docModel.handleContainerVisibility) {
                docModel.handleContainerVisibility(currentNode.first(), { makeVisible: false });
            }
        }

        // nothing more to do when updating an internal model frame
        if (internalModel) { return; }

        var hasFill = fillAttrs && (fillAttrs.type !== 'none');
        var hasLine = lineAttrs && (lineAttrs.type !== 'none');
        var canvasRequired = hasFill || hasLine;

        // adding canvas node, if 'fill' or 'line' attributes are specified
        if (canvasRequired) {
            if (!PRESET_GEOMETRIES[presetShape] || CANVAS_EXPANSION_PRESET_SHAPES[presetShape]) {
                expansion = getCanvasExpansion(geometry, snapRect, guideCache, expansion);
                // options = options || {};
                // options.isToKeepSize = false;
            }

            if (lineAttrs && geometry) { // adding the canvas expansion required by line ends
                expansion = getCanvasExpansionForLineEnds(lineAttrs, geometry, expansion);
            }

            if (expansion) { currentNode.data('canvasexpansion', expansion); }

            if (!isResizeActive) { handleDrawingExpansionNode(currentNode, expansion); } // handle an additional child node, if canvas exceeds the drawing size to simplify selection

            // bug 48101: reduce canvas size to supported dimensions
            maxSizeScale = max(snapRect.width, snapRect.height) / MAX_CANVAS_SIZE;
            if (maxSizeScale > 1) {
                snapRect.width = round(snapRect.width / maxSizeScale);
                snapRect.height = round(snapRect.height / maxSizeScale);
            }

            if (_.isNumber(expansion)) {
                boundRect = snapRect.clone().expandSelf(expansion);
            } else if (_.isObject(expansion)) {
                boundRect = snapRect.clone().expandSelf(expansion.left, expansion.top, expansion.right, expansion.bottom);
            } else {
                boundRect = snapRect.clone();
            }

            if (isResizeActive) {
                options = options || {};
                options.resizeIsActive = true;
            }

            var isPathShape = ('pathList' in geometry) && geometry.pathList && (geometry.pathList.length > 0);
            var isBitmapFill = fillAttrs && fillAttrs.type === 'bitmap' && !_.isEmpty(fillAttrs.bitmap);

            if (isBitmapFill) {
                options = options || {};
                options.typeOfFill = 'bitmap';
            }

            // canvas = initializeCanvas(app, currentNode, boundRect, lineWidth, isResizeActive ? { resizeIsActive: true } : null);
            canvas = initializeCanvas(app, currentNode, boundRect, lineWidth, options);
            if (maxSizeScale > 1) { canvas.$el.css({ width: boundRect.width * maxSizeScale, height: boundRect.height * maxSizeScale, left: boundRect.left * maxSizeScale, top: boundRect.top * maxSizeScale }); } // #52379

            if (isPathShape && isBitmapFill) { // because imgs for fill are loaded async, canvas context get reset in the meantime, drawShape needs to be async also (#56046)
                var drawingUID = getUid(currentNode);
                var cachedTextureObj = getCachedTextureFill(drawingUID);
                if (isResizeActive && cachedTextureObj && cachedTextureObj.texture) { // Performance: during resize, try to fetch texture pattern from cache
                    canvas.render(function (context) {
                        var options = { isResizeActive, currentNode, textureStyle: cachedTextureObj.texture, boundRect };
                        drawShape(app, context, snapRect, geometry, { fill: fillAttrs, line: lineAttrs }, guideCache, options);
                    });
                } else {
                    // var textureCanvas = initializeCanvas(app, currentNode, boundRect, lineWidth, isResizeActive ? { resizeIsActive: true } : null);
                    var textureCanvas = initializeCanvas(app, currentNode, boundRect, lineWidth, options);
                    textureCanvas.render(function (txtCtx) {
                        var rotationAngle = getDrawingRotationAngle(docModel, currentNode) || 0;
                        rotationAngle = ((360 - rotationAngle) % 360) * PI_180; // normalize and convert to radians
                        return getTextureFill(docModel, txtCtx, fillAttrs, snapRect.width, snapRect.height, rotationAngle, { themeTargets: docModel.getThemeTargets(currentNode) }).done(function (textureFillStyle) {
                            var newAttrs = getExplicitAttributeSet(currentNode);
                            newAttrs.fill = fillAttrs;
                            newAttrs.line = lineAttrs; // DOCS-1322 - the new line attributes are needed, not the old ones from the DOM
                            var newLineAttrs = newAttrs ? newAttrs.line : null;
                            var newFillAttrs = newAttrs ? newAttrs.fill : null;
                            if (newFillAttrs && newFillAttrs.bitmap /*&& newFillAttrs.bitmap.imageUrl === fillAttrs.bitmap.imageUrl*/) { // #57720
                                canvas.clear();
                                canvas.render(function (context) {
                                    var options = { isResizeActive, currentNode, textureStyle: textureFillStyle, boundRect };
                                    drawShape(app, context, snapRect, geometry, { fill: newFillAttrs, line: newLineAttrs }, guideCache, options);
                                });
                            } else {
                                globalLogger.warn('DrawingFrame.drawGeometry(): while image load, fillAttributes have changed!');
                            }
                        }).then(function () {
                            if (app.isPresentationApp() && !isResizeActive) {
                                docModel.trigger('image:loaded', currentNode); // this might be interesting for other apps, too (56093)
                            }
                        });
                    });
                    // storing geometry data used to calculate snap or adjustment points later
                    if (!isResizeActive) {
                        if (geometry.cxnList || geometry.ahList) { currentNode.data('shapeGeoData', { avList: geometry.avList, ahList: geometry.ahList, gdList: geometry.gdList, cxnList: geometry.cxnList }); }
                    }
                }
            } else {
                canvas.render(function (context) {
                    if (isPathShape) {
                        drawShape(app, context, snapRect, geometry, { fill: fillAttrs, line: lineAttrs }, guideCache, { isResizeActive, currentNode });
                        return;
                    }

                    var renderMode = getRenderMode(hasFill, hasLine);
                    var rotationAngle = null;
                    var notRotatedWithShape = null;

                    function drawRect(options) {
                        context.drawRect(boundRect, options?.mode ?? renderMode);
                    }

                    if (hasFill) {
                        var targets = fillAttrs[TARGET_CHAIN_PROPERTY];

                        if (fillAttrs.type === 'gradient') {
                            var gradient = Gradient.create(Gradient.getDescriptorFromAttributes(fillAttrs));
                            context.setFillStyle(docModel.getGradientFill(gradient, boundRect, context, targets));
                        } else if (fillAttrs.type === 'pattern') {
                            context.setFillStyle(createCanvasPattern(fillAttrs.pattern, fillAttrs.color2, fillAttrs.color, docModel.getThemeModel(targets)));
                        } else if (fillAttrs.type === 'bitmap') {
                            notRotatedWithShape = fillAttrs.bitmap.rotateWithShape === false;
                            if (notRotatedWithShape && currentNode) {
                                rotationAngle = getDrawingRotationAngle(docModel, currentNode) || 0;
                                rotationAngle = ((360 - rotationAngle) % 360) * PI_180; // normalize and convert to radians
                            }

                            var texturePromise = getTextureFill(docModel, context, fillAttrs, snapRect.width, snapRect.height, rotationAngle).done(function (textureFillStyle) {
                                setBitmapTextureFillStyle(docModel, context, drawRect, textureFillStyle, snapRect.width, snapRect.height, hasLine, lineAttrs, fillAttrs.bitmap.tiling, notRotatedWithShape, rotationAngle);
                            });

                            return texturePromise.then(function () {
                                if (app.isPresentationApp()) { docModel.trigger('image:loaded', currentNode); } // this might be interesting for other apps, too (56093)
                            });
                        } else {
                            context.setFillStyle(docModel.getCssColor(fillAttrs.color, 'fill', targets));
                        }
                    }

                    if (hasLine) {
                        setLineAttributes(docModel, context, lineAttrs, lineWidth);
                    }

                    if (renderMode) {
                        context.drawRect(boundRect, renderMode);
                    }
                });
            }
        } else {
            // removing an optionally existing canvas node, if neither 'fill' nor 'line' are specified
            getCanvasNode(currentNode).remove();
            currentNode.removeData('canvasexpansion');
            currentNode.children(CANVAS_EXPANSION_SELECTOR).remove(); // removing an already existing child node

            // storing geometry data used to calculate snap or adjustment points later
            if (!isResizeActive && ('presetShape' in geometryAttrs) && PRESET_GEOMETRIES[geometryAttrs.presetShape]) {
                var geo = getPresetShapeGeometry(geometryAttrs, PRESET_GEOMETRIES[geometryAttrs.presetShape]);
                if (geo.cxnList || geo.ahList) {
                    currentNode.data('shapeGeoData', { avList: geo.avList, ahList: geo.ahList, gdList: geo.gdList, cxnList: geo.cxnList });
                }
            }
        }

        // bug 58180: after inserting a shape the adjustment handles must be created
        var geoData = drawingFrame.data('shapeGeoData');
        if (geoData) {
            var zoomFactor = app.getView().getZoomFactor();
            createAdjustmentHandles(drawingFrame, geoData.avList, geoData.gdList, geoData.ahList, zoomFactor);
        }
    }

    // Handling the target chain inheritance also for grouped drawings (48096)
    // Or during active resizing of shape to fetch correct color from themes
    if (isGroupedDrawingFrame(drawingFrame) || isResizeActive) {
        handleTargetChainInheritance(docModel, drawingFrame, explicitAttributes);

        // Fixing problem with grouped shapes, that have no explicit fill color set. In ODF the color of the
        // default drawing style is used then. Info: The mergedAttributes are the merged attributes of the
        // group in that case!
        if (app.isODF() && useExplicit) {
            if (!lineAttrs) { lineAttrs = mergedAttributes.line; }
            if (!fillAttrs) { fillAttrs = mergedAttributes.fill; }
        }
    }

    // applying text padding & vertical alignment
    if (shapeAttrs && (textFrame.length > 0)) {
        if (shapeAttrs.anchor) {
            textFrame.attr(VERTICAL_ALIGNMENT_ATTRIBUTE, shapeAttrs.anchor);
        }
        if (!geometryAttrs) {
            textFrame.css({
                paddingTop: convertHmmToCssLength(mergedAttributes.shape.paddingTop, 'px', 1),
                paddingLeft: convertHmmToCssLength(mergedAttributes.shape.paddingLeft, 'px', 1),
                paddingBottom: convertHmmToCssLength(mergedAttributes.shape.paddingBottom, 'px', 1),
                paddingRight: convertHmmToCssLength(mergedAttributes.shape.paddingRight, 'px', 1)
            });
        }
    }

    // handling non-selectable and watermark drawings (56484)
    if (isNoSelectAttributeSet(mergedAttributes)) {
        drawingFrame.addClass(NOSELECT_CLASS);
        if (isWatermarkAttributeSet(mergedAttributes)) { drawingFrame.addClass(WATERMARK_CLASS); }
    }

    // setting the value for automatic vertical resizing of the shape.
    if (isAutoResizeHeightAttributes(shapeAttrs)) {
        if (currentNode.data('widthHeightRev')) {
            textFrame.css('height', 'auto');
            currentNode.width(textFrame.outerHeight(true));
            if (shapeAttrs.vert !== 'vert270') { currentNode.height(textFrame.outerWidth(true)); } // not changing height for drawings with vertical text direction (62977)
        }
        if (!currentNode.data('drawingHeight') && !isGroupContentNode(currentNode.parent())) {
            currentHeight = (explicitAttributes && explicitAttributes.drawing && convertHmmToLength(explicitAttributes.drawing.height, 'px', 1)) || currentNode.height();
            currentNode.data('drawingHeight', currentHeight);
        }
        contentNode.addClass(AUTORESIZEHEIGHT_CLASS);
        // updating the height of a surrounding group node
        if (isGroupedDrawingFrame(contentNode.parent())) { updateDrawingGroupHeight(app, contentNode.parent()); }
    } else {
        contentNode.removeClass(AUTORESIZEHEIGHT_CLASS);
        currentNode.data('drawingHeight', null);
        // OX Text specific handling of overflown textframes (vert align, hint arrow)
        if (app.isTextApp()) {
            handleTextframeOverflow(currentNode, app.isODF(), geometryAttrs);
        }
    }

    // setting a minimum height at frames (ODP only), 55236
    if (app.isODF() && mergedAttributes.drawing.minFrameHeight && !isGroupedDrawingFrame(drawingFrame)) {
        var minHeightTextFrame = convertHmmToLength(mergedAttributes.drawing.minFrameHeight, 'px', 1);
        var marBottom = textFrame.css('margin-bottom'); // if the text frame has a margin specified, this value must be reduced (56026)

        if (marBottom) {
            marBottom = parseInt(marBottom, 10) || 0;
            minHeightTextFrame -= marBottom;
        }

        textFrame.css('min-height', minHeightTextFrame + 'px');
        getContentNode(drawingFrame).addClass(MINFRAMEHEIGHT_CLASS); // setting a marker class
    }

    if (docModel.useSlideMode()) {
        // setting the value for automatic text height in the shape.
        if (isAutoTextHeightAttributes(shapeAttrs)) {
            contentNode.css('font-size', (shapeAttrs.fontScale * 16) + 'px');
            contentNode.attr('font-scale', shapeAttrs.fontScale);
            contentNode.attr('lineReduction', round(min(0.2, shapeAttrs.lineReduction) * 100));
            contentNode.addClass(AUTORESIZETEXT_CLASS);
        } else {
            contentNode.css('font-scale', null);
            contentNode.css('font-size', null);
            contentNode.attr('lineReduction', null);
            contentNode.removeClass(AUTORESIZETEXT_CLASS);
        }
        if (noWordWrap) {
            textFrame.css('width', 'auto');
            if (isAutoResizeHeightAttributes(shapeAttrs) && !isTwoPointShape(currentNode)) { // #47690, #48153, #55717
                currentNode.css('width', 'auto');
            }
            contentNode.addClass(NO_WORDWRAP_CLASS);
            if (!currentNode.data('drawingWidth') && !isGroupContentNode(currentNode.parent())) {
                currentWidth = (explicitAttributes && explicitAttributes.drawing && convertHmmToLength(explicitAttributes.drawing.width, 'px', 1)) || currentNode.width();
                currentNode.data('drawingWidth', currentWidth);
            }
        } else {
            contentNode.removeClass(NO_WORDWRAP_CLASS);
            currentNode.data('drawingWidth', null);
            textFrame.css('white-space', '');
        }
    } else { // OX Text specific, not implemented for noWordWrap shapes in groups yet
        if (noWordWrap && !isGroupedDrawingFrame(currentNode)) {
            if (isAutoResizeHeightAttributes(shapeAttrs)) { // 62922
                var parentPara = currentNode.parent('.p');
                var parentParaWidth = parentPara.length ? parentPara.width() : '';
                currentNode.css({ width: 'auto', maxWidth: parentParaWidth });
            }
            contentNode.css('position', 'relative');
            contentNode.addClass(NO_WORDWRAP_CLASS);
        } else {
            contentNode.removeClass(NO_WORDWRAP_CLASS);
        }
    }

    if (geometryAttrs) {
        // bugfix :: BUG#45141 :: https://bugs.open-xchange.com/show_bug.cgi?id=45141 :: Vertical drawing object is shown diagonally
        if (isTwoPointShapeId(geometryAttrs.presetShape)) {
            if (currentNode.width() === 0) {
                currentNode.width(1);
            }
            if (currentNode.height() === 0) {
                currentNode.height(1);
            }
        }

        if (('presetShape' in geometryAttrs) && PRESET_GEOMETRIES[geometryAttrs.presetShape]) {
            var geo = getPresetShapeGeometry(geometryAttrs, PRESET_GEOMETRIES[geometryAttrs.presetShape]);
            drawGeometry(geo);
        } else {
            drawGeometry(geometryAttrs);
        }
    } else if (!internalModel) {
        // do not format internal model frames
        updateFillFormatting(app, contentNode, mergedAttributes);
        updateLineAndFillFormatting(app, drawingFrame, mergedAttributes, { useExplicitAttrs: true, linesOnly: true });
    }

    // applying text direction and stacking
    if (shapeAttrs && shapeAttrs.vert && (textFrame.length > 0)) {
        switch (shapeAttrs.vert) {
            case 'vert':
            case 'eaVert':
                orientateTextInShape(90); // vertical (90deg)
                break;
            case 'vert270':
                orientateTextInShape(270); // vertical reversed (270deg)
                break;
            case 'horz':
                if (isRotatedTextInDrawingNode(textFrame)) {
                    orientateTextInShape(0); // revert from vertical to horizontal
                }
                break;
        }
    }

    return true;
}

/**
 * Updates the size and position of all children of a drawing group.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {jQuery} drawing
 *  Drawing node in jQuery format
 *
 * @param {Object} mergedAttributes
 *  Merged attributes of the given drawing node.
 *
 * @param {Object} [options]
 *  - {Boolean} [options.resizeActive=false]
 *      If function is called during resizing of drawing
 */
function resizeDrawingsInGroup(app, drawing, mergedAttributes, options) {

    var // the document model
        docModel = app.getModel(),
        // if resize is active, calculate live dimensions, not from attributes
        isResizeActive = getBooleanOption(options, 'resizeActive', false),
        // copied content of the drawing
        $content = isGroupedDrawingFrame(drawing) ? drawing.children('.content') : drawing.children('.copy'),
        // the direct drawing children of the drawing group
        allChildren = isResizeActive ? $content.children(NODE_SELECTOR) : getAllGroupDrawingChildren(drawing, { onlyDirectChildren: true }),
        // rotation angle of the drawing group element
        rotationAngle = getDrawingRotationAngle(docModel, drawing),
        // the horizontal and vertical stretch factor for the children
        horzFactor = 1, vertFactor = 1,
        // if rotation angle is set
        isRotationAngle = _.isNumber(rotationAngle) && rotationAngle !== 0,
        // if drawing is flipped horizontally
        isFlipH = isFlippedHorz(drawing),
        // if drawing is flipped vertically
        isFlipV = isFlippedVert(drawing),
        // if some of transformations is applied to the drawing
        isTransformed = isRotationAngle || isFlipH || isFlipV,
        // dimensions of the group, like width, height, childLeft, childTop, etc.
        groupDimensions = null,
        // width and height properties of the group root node
        groupWidth, groupHeight,
        // properties of the group child bounding rectangle
        childWidth, childHeight, childTop, childLeft;

    function getGroupDimensions() {
        var groupWidth, groupHeight;

        // whether the explicit drawing attributes can be used (this is not the case for groups in groups, where
        // the explicit attributes do not contain values in hmm, but in an arbitrary unit).
        var isToplevelGroup = !isDrawingContentNode(drawing.parent());  // Info: 'grouped' not set for groups in groups in first run (46784)
        // the explicit attributes at the group node (should be used for precision)
        var explicitGroupAttributes = getExplicitAttributeSet(drawing);
        // the explicit drawing attributes
        var explDrawingAttrs = explicitGroupAttributes && explicitGroupAttributes.drawing;
        // implicit child group attributes stored to root node on group creation
        var childGroupAttrs = drawing.data('childGroupAttrs');

        if (isResizeActive || !isToplevelGroup) {
            groupWidth = convertLengthToHmm($content.width(), 'px');
            groupHeight = convertLengthToHmm($content.height(), 'px');
            // the height and width of the content node may not be correct, if the current slide is hidden (DOCS-1756)
            if (!isToplevelGroup && groupWidth <= 0) { groupWidth = convertLengthToHmm(drawing.width(), 'px'); }
            if (!isToplevelGroup && groupHeight <= 0) { groupHeight = convertLengthToHmm(drawing.height(), 'px'); }
        } else {
            groupWidth = (explDrawingAttrs && _.isNumber(explDrawingAttrs.width)) ? explDrawingAttrs.width : convertLengthToHmm(drawing.children().first().width(), 'px');
            groupHeight = (explDrawingAttrs && _.isNumber(explDrawingAttrs.height)) ? explDrawingAttrs.height : convertLengthToHmm(drawing.children().first().height(), 'px');
        }
        var childWidth = (explDrawingAttrs && _.isNumber(explDrawingAttrs.childWidth)) ? explDrawingAttrs.childWidth : (childGroupAttrs && childGroupAttrs.childWidth);
        var childHeight = (explDrawingAttrs && _.isNumber(explDrawingAttrs.childHeight)) ? explDrawingAttrs.childHeight : (childGroupAttrs && childGroupAttrs.childHeight);
        var childLeft = (explDrawingAttrs && _.isNumber(explDrawingAttrs.childLeft)) ? explDrawingAttrs.childLeft : (childGroupAttrs && childGroupAttrs.childLeft);
        var childTop = (explDrawingAttrs && _.isNumber(explDrawingAttrs.childTop)) ? explDrawingAttrs.childTop : (childGroupAttrs && childGroupAttrs.childTop);

        return { width: groupWidth, height: groupHeight, childWidth, childHeight, childLeft, childTop };
    }

    // updates the size and position of one drawing child inside a drawing group
    function handleDrawingInsideDrawingGroup(drawing, attrs, horzFactor, vertFactor, offsetX, offsetY) {

        var // the attributes at the drawing property
            drawingAttrs = attrs.drawing,
            // the required css attributes
            cssAttrs = {},
            // an optional horizontal resize factor
            horzResizeFactor = horzFactor || 1,
            // an optional vertical resize factor
            vertResizeFactor = vertFactor || 1,
            // type of the drawing object inside the group: 'image', ...
            type = getDrawingType(drawing),
            // scaled values for shape attributes
            scaledWidth, scaledHeight, scaledLeft, scaledTop,
            isOOXMLbutterflyArea = false,
            // the full set of child attributes
            mergedChildAttributes = null;

        // updates attributes of the image node of a drawing frame inside a drawing group container
        function updateImageAttributesInGroup() {

            var // current width of the image, in 1/100 mm
                drawingWidth = max(round(drawingAttrs.width * horzResizeFactor), 100),
                // current height of the image, in 1/100 mm
                drawingHeight = max(round(drawingAttrs.height * vertResizeFactor), 100),
                drawingContentNode = drawing.children().first(),
                imgContentNode = drawingContentNode.children('img').first();

            // #56732 - if image node is direct child of content node, wrap it with crop frame
            if (imgContentNode.length) { imgContentNode.wrap($('<div class="' + CROPPING_CLASS + '">')); }

            setImageCssSizeAttributes(app, drawing, drawingContentNode, drawingWidth, drawingHeight, attrs.drawing, attrs.image);

            // update the border and filling of grouped images
            updateLineAndFillFormatting(app, drawing, attrs);
        }

        if (!drawingAttrs) { return; } // drawing attributes must be defined (47633)

        // ooxml uses switching of width and height for certain angle values
        isOOXMLbutterflyArea = !app.isODF() && isAngleInButterflyArea(drawingAttrs.rotation, drawingAttrs.flipH, drawingAttrs.flipV);

        scaledWidth = drawingAttrs.width * (isOOXMLbutterflyArea ? vertResizeFactor : horzResizeFactor);
        scaledHeight = drawingAttrs.height * (isOOXMLbutterflyArea ? horzResizeFactor : vertResizeFactor);
        scaledLeft = (drawingAttrs.left - offsetX) * horzResizeFactor;
        scaledTop = (drawingAttrs.top - offsetY) * vertResizeFactor;

        if (isOOXMLbutterflyArea) {
            scaledLeft += (drawingAttrs.width * horzResizeFactor - drawingAttrs.width * vertResizeFactor) / 2;
            scaledTop -= (drawingAttrs.height * horzResizeFactor - drawingAttrs.height * vertResizeFactor) / 2;
        }

        // set the CSS attributes for the drawing
        if (_.isNumber(drawingAttrs.left))  { cssAttrs.left     = convertHmmToCssLength(scaledLeft, 'px', 1); }
        if (_.isNumber(drawingAttrs.top))   { cssAttrs.top      = convertHmmToCssLength(scaledTop, 'px', 1); }
        if (_.isNumber(drawingAttrs.width)) { cssAttrs.width    = max(convertHmmToLength(scaledWidth, 'px', 1), 1) + 'px'; }
        if (_.isNumber(drawingAttrs.height) && !isAutoResizeHeightDrawingFrame(drawing)) { // not setting height for auto resize drawings
            cssAttrs.height = max(convertHmmToLength(scaledHeight, 'px', 1), 1) + 'px';
        }

        drawing.css(cssAttrs);

        // updating the size of the images inside the drawing and handling an optionally required canvas for borders
        if (type === 'image') { updateImageAttributesInGroup(drawing); }

        // updating the canvas (border and background), but not for group drawings, images or tables (#59783)
        if (type !== 'group' && type !== 'image' && type !== 'table') {
            mergedChildAttributes = docModel.drawingStyles.getElementAttributes(drawing); // using the merged attributes of the child (#65381)
            updateShapeFormattingLocal(app, drawing, mergedChildAttributes, { useExplicitAttrs: true });
        }

        // handle grouped drawing flip, as it is not done in the updateFormatting method, see also #55717
        handleFlipping(docModel, drawing, drawingAttrs);

        // handling groups in groups
        if (isGroupDrawingFrame(drawing)) {
            resizeDrawingsInGroup(app, drawing, mergedAttributes, options);
        }
    }

    if (drawing.hasClass('activedragging') && !isResizeActive && !drawing.hasClass('activeresizing')) {
        return; // if updateShape was triggered by move operation, resizing inside groups is unnecessary
    }

    if (isResizeActive && isTransformed) {
        drawing.css({ transform: '' }); // reset to def to get values
    }
    groupDimensions = getGroupDimensions();
    groupWidth = groupDimensions.width;
    groupHeight = groupDimensions.height;
    childWidth = groupDimensions.childWidth;
    childLeft = groupDimensions.childLeft;
    childHeight = groupDimensions.childHeight;
    childTop = groupDimensions.childTop;

    // iterating over all drawing children of the group
    if (allChildren.length > 0) {
        horzFactor = groupWidth / childWidth;
        vertFactor = groupHeight / childHeight;
        _.each(allChildren, function (drawing) {
            handleDrawingInsideDrawingGroup($(drawing), getExplicitAttributeSet(drawing), horzFactor, vertFactor, childLeft, childTop);
        });
    }
    if (isResizeActive && isTransformed) {
        setCssTransform(drawing, rotationAngle, isFlipH, isFlipV);
    }
}


// constants --------------------------------------------------------------

/**
 * The CSS class used to mark drawing frame nodes.
 */
export const NODE_CLASS = 'drawing';

/**
 * A jQuery selector that matches nodes representing a drawing frame.
 */
export const NODE_SELECTOR = '.' + NODE_CLASS;

/**
 * A jQuery selector that matches nodes representing the content nodes in a drawing frame.
 */
export const CONTENT_NODE_SELECTOR = '.' + CONTENT_CLASS;

/**
 * The CSS class used to mark drawing content nodes as place holder.
 */
export const PLACEHOLDER_CLASS = 'placeholder';

/**
 * A jQuery selector that matches nodes representing a place holder element
 * of type 'div'.
 */
export const PLACEHOLDER_SELECTOR = 'div.' + PLACEHOLDER_CLASS;

/**
 * A jQuery selector that matches content nodes of group drawing objects.
 */
export const GROUPCONTENT_SELECTOR = NODE_SELECTOR + '[data-type="group"]>.' + CONTENT_CLASS;

/**
 * The CSS class used to mark non selectable drawing frame nodes.
 */
export const NOSELECT_CLASS = 'noselectdrawing';

/**
 * The CSS class used to mark watermark drawing frame nodes.
 */
export const WATERMARK_CLASS = 'watermark';

/**
 * The CSS class used to mark drawing content nodes inside text frames
 * for automatic resizing of height.
 */
export const AUTORESIZEHEIGHT_CLASS = 'autoresizeheight';

/**
 * The CSS class used to mark drawing content node for no wrapping of words.
 */
export const NO_WORDWRAP_CLASS = 'no-wordwrap';

/**
 * A jQuery selector that matches nodes representing a content node inside a
 * test frame that automatically resizes its height.
 */
export const AUTORESIZEHEIGHT_SELECTOR = 'div.' + AUTORESIZEHEIGHT_CLASS;

export const AUTORESIZETEXT_CLASS = 'autoresizetext';

export const AUTORESIZETEXT_SELECTOR = 'div.' + AUTORESIZETEXT_CLASS;

/**
 * The CSS class used to mark drawing nodes with minimum frame height specified.
 */
export const MINFRAMEHEIGHT_CLASS = 'minframeheight';

/**
 * A jQuery selector that matches nodes representing a drawing node with
 * minimum frame height specified.
 */
export const MINFRAMEHEIGHT_SELECTOR = 'div.' + MINFRAMEHEIGHT_CLASS;

/**
 * The CSS class used to mark drawing content nodes inside text frames
 * for automatic resizing of height.
 */
export const VERTICAL_ALIGNMENT_ATTRIBUTE = 'verticalalign';

/**
 * The CSS class used to mark drawing content nodes inside text frames
 * for automatic resizing of height.
 */
export const ODFTEXTFRAME_CLASS = 'odftextframe';

/**
 * A jQuery selector that matches nodes representing a text frame node that
 * are 'classical' text frames in odf format.
 */
export const ODFTEXTFRAME_SELECTOR = 'div.' + ODFTEXTFRAME_CLASS;

/**
 * The CSS class used to mark empty text frames.
 */
export const EMPTYTEXTFRAME_CLASS = 'emptytextframe';

/**
 * The CSS class used to mark empty text frames that require an artificial border.
 */
export const EMPTYTEXTFRAMEBORDER_CLASS = 'emptytextframeborder';

/**
 * The CSS class used to mark empty text frames with thick border.
 */
export const EMPTY_THICKBORDER_CLASS = 'emptythickborder';

/**
 * A jQuery selector that matches elements representing a table text frame element.
 */
export const TABLE_TEXTFRAME_NODE_SELECTOR = '.drawing[data-type="table"]';

/**
 * The CSS class used to mark table nodes inside drawings of type 'table'.
 */
export const TABLE_NODE_IN_TABLE_DRAWING_CLASS = 'isdrawingtablenode';

/**
 * A jQuery selector that matches elements representing a text frame element
 * inside a drawing frame of type shape.
 */
export const TEXTFRAME_NODE_SELECTOR = 'div.textframe';

/**
 * The CSS class used to mark text frame content nodes inside a
 * drawing node.
 */
export const TEXTFRAMECONTENT_NODE_CLASS = 'textframecontent';

/**
 * A jQuery selector that matches nodes representing a text frame content
 * element inside a drawing node.
 */
export const TEXTFRAMECONTENT_NODE_SELECTOR = 'div.' + TEXTFRAMECONTENT_NODE_CLASS;

/**
 * The CSS class used to mark drawing nodes inside a group container.
 */
export const GROUPED_NODE_CLASS = 'grouped';

/**
 * A jQuery selector that matches nodes representing a grouped element.
 */
export const GROUPED_NODE_SELECTOR = '.' + GROUPED_NODE_CLASS;

/**
 * The CSS class used to mark drawing nodes that cannot be grouped.
 */
export const NO_GROUP_CLASS = 'nogroup';

/**
 * The CSS class used to mark canvas nodes of a drawing.
 */
export const CANVAS_CLASS = 'canvasgeometry';

/**
 * A jQuery selector that matches nodes representing a canvas element.
 */
export const CANVAS_NODE_SELECTOR = 'canvas.' + CANVAS_CLASS;

/**
 * The CSS class used to mark nodes inside drawing nodes, that are used to
 * handle canvas nodes exceeding the drawing.
 */
export const CANVAS_EXPANSION_CLASS = 'canvasexpansion';

/**
 * A jQuery selector that matches nodes inside drawing nodes, that are used to
 * handle canvas nodes exceeding the drawing.
 */
export const CANVAS_EXPANSION_SELECTOR = '.' + CANVAS_EXPANSION_CLASS;

/**
 * The CSS class used to mark drawing nodes, that contain a canvas expansion node
 * because of a canvas with exceeded size.
 */
export const CONTAINS_CANVAS_EXPANSION = 'containsexpansionnode';

/**
 * The CSS class used to mark unsupported drawing nodes.
 */
export const UNSUPPORTED_CLASS = 'unsupported';

/**
 * A jQuery selector that matches nodes representing unsupported drawings.
 */
export const UNSUPPORTED_SELECTOR = '.' + UNSUPPORTED_CLASS;

/**
 * The CSS class used to mark snap point nodes.
 */
export const SNAP_POINT_CLASS = 'snap-point';

/**
 * A jQuery selector that matches nodes representing snap-points of drawings.
 */
export const SNAP_POINT_SELECTOR = '.' + SNAP_POINT_CLASS;

/**
 * A class name that matches elements representing a rotated drawing.
 */
export const ROTATED_DRAWING_CLASSNAME = 'rotated-drawing';

/**
 * A class name that matches elements representing a state of drawing beeing currently rotated.
 */
export const ROTATING_STATE_CLASSNAME = 'rotating-state';

/**
 * A class name that matches rotating angle tooltip shown over drawing during rotation.
 */
export const ROTATE_ANGLE_HINT_CLASSNAME = 'angle-tooltip';

/**
 * A jQuery selector that matches rotating angle tooltip shown over drawing during rotation.
 */
export const ROTATE_ANGLE_HINT_SELECTOR = '.' + ROTATE_ANGLE_HINT_CLASSNAME;

/**
 * A class name that matches elements representing a rotated text in drawing.
 */
export const ROTATED_TEXT_IN_DRAWING_CLASSNAME = 'rotated-text-drawing';

/**
 * A class name that matches elements representing a drawing mirrored horizontally.
 */
export const FLIPPED_HORIZONTALLY_CLASSNAME = 'flipH';

/**
 * A class name that matches elements representing a drawing mirrored vertically.
 */
export const FLIPPED_VERTICALLY_CLASSNAME = 'flipV';

/**
 * The name of the property for the target chain that needs to be added to
 * the 'fill' and the 'line' family.
 */
export const TARGET_CHAIN_PROPERTY = 'targetchain';

/**
 * Class name that maches elements that need temporary border drawn around them,
 * usually on resize or move.
 */
export const FRAME_WITH_TEMP_BORDER = 'temp-border-frame';

/**
 * Class name that maches elements that need temporary border drawn around them,
 * usually on resize or move.
 * Note: this class uses css property 'border', instead of 'box-shadow',
 * which is used for first classname.
 */
export const FRAME_WITH_TEMP_BORDER2 = 'temp-border2';

/**
 * A CSS class that can be added to a DOM drawing frame to specify its only
 * usage as internal model container. Existence of this class will cause
 * significantly reduced rendering (e.g. no canvas elements in shape
 * objects).
 */
export const INTERNAL_MODEL_CLASS = 'internal-model-frame';

/**
 * A CSS class that can be added to text frame in drawing,
 * to overwrite the vertical align set to the drawing,
 * with css value justify-content: flex-start!important.
 */
export const VERT_ALIGN_TOP_IMPORTANT_CLASS = 'vert-align-important';

/**
 * A CSS class that can be added to drawing,
 * that is currentlly beeing cropped.
 */
export const ACTIVE_CROPPING_CLASS = 'active-cropping';

/**
 * A CSS selector that is used for a drawing,
 * that is currentlly beeing cropped.
 */
export const ACTIVE_CROPPING_SELECTOR = '.active-cropping';
/**
 * A CSS class that is added to canvas elements of drawing node,
 * that is currentlly beeing cropped.
 */
export const HIDE_CANVAS_ON_CROP_CLASS = 'hide-canvas';

/**
 * A CSS class that is added to the image resizer nodes
 * for drawings.
 */
export const CROPPING_RESIZER_CLASS = 'cropping-resizer';

/**
 * A CSS class that is added to the image resizer nodes
 * for drawings.
 */
export const CROPPING_RESIZER_SELECTOR = '.' + CROPPING_RESIZER_CLASS;

/**
 * A CSS class that can be added to drawing, that is forced to the
 * inline mode (small devices and draft mode).
 */
export const FORCE_INLINE_CLASS = 'force-inline';

/**
 * A CSS selector that is used for a drawing, that is forced to the
 * inline mode (small devices and draft mode).
 */
export const FORCE_INLINE_SELECTOR = '.' + FORCE_INLINE_CLASS;

/**
 * A CSS class used to define linked connector start to a drawing node.
 */
export const START_LINKED_CLASS = 'start-linked';

/**
 * A CSS class used to define linked connector end to a drawing node.
 */
export const END_LINKED_CLASS = 'end-linked';

// methods ----------------------------------------------------------------

/**
 * Returns a new drawing frame node without specific contents.
 *
 * @param {DrawingModel|String} modelOrType
 *  The drawing model the new drawing frame will be based on. Alternatively
 *  (for applications not supporting drawing models), this value can be a
 *  string with the type of the drawing object.
 *
 * @returns {jQuery}
 *  A new drawing frame with empty content node, as jQuery object.
 */
export function createDrawingFrame(modelOrType) {

    // the drawing model
    var model = (modelOrType instanceof DrawingModel) ? modelOrType : null;
    // the drawing type identifier
    var type = model ? model.drawingType : modelOrType;

    // avoiding unspecified types (DOCS-4926)
    type = DRAWING_TYPES.includes(type) ? type : DEFAULT_DRAWING_TYPE;

    // the resulting drawing frame node
    var drawingFrame = $('<div class="' + NODE_CLASS + '" data-type="' + type + '" contenteditable="false"><div class="' + CONTENT_CLASS + '"></div></div>');

    // add data attributes, event handlers, and the content node
    drawingFrame
        .data(DATA_UID, 'frame' + _.uniqueId())
        .data(DATA_MODEL, model)
        .on('dragstart', false);

    return drawingFrame;
}

/**
 * Returns whether the passed node is the root node of a drawing frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the root node of a drawing frame.
 */
export function isDrawingFrame(node) {
    return $(node).is(NODE_SELECTOR);
}

/**
 * Returns the type of the specified drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {String|undefined}
 *  The type of the specified drawing frame, as specified while the frame
 *  has been created. Or undefined, if the type cannot be determined.
 */
export function getDrawingType(drawingFrame) {
    return $(drawingFrame).first().attr('data-type');
}

/**
 * Returns whether the specified drawing frame is a line/connector shape
 * (i.e. it will be selected in two-point mode instead of the standard
 * rectangle mode).
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed drawing frame is a line/connector shape.
 */
export function isTwoPointShape(drawingFrame) {
    var drawingType = getDrawingType(drawingFrame);
    var geoAttrs = getExplicitAttributes(drawingFrame, 'geometry');
    return isTwoPointShapeImport(drawingType, geoAttrs);
}

/**
 * Returns whether the passed node is a content node inside the drawing frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a content node inside a drawing frame.
 */
export function isDrawingContentNode(node) {
    return $(node).is('.' + CONTENT_CLASS);
}

/**
 * Returns whether the passed node is a content node inside the drawing frame
 * that has no content.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a content node inside a drawing frame that has
 *  no content.
 */
export function isEmptyDrawingContentNode(node) {
    return isDrawingContentNode(node) && $(node).children().not(CANVAS_NODE_SELECTOR).length === 0;
}

/**
 * Returns whether the passed node is a placeholder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a place holder node (of a drawing frame).
 */
export function isPlaceHolderNode(node) {
    return $(node).is(PLACEHOLDER_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'shape'.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame of type 'shape'.
 */
export function isShapeDrawingFrame(node) { // TODO: type 'table' should be handled differently
    var nodeType = getDrawingType(node);
    return isDrawingFrame(node) && (nodeType === 'shape' || nodeType === 'table' || nodeType === 'connector');
    // TODO This should be activated asap: return DrawingFrame.isDrawingFrame(node) && DrawingFrame.getDrawingType(node) === 'shape';
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'shape'.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame of type 'shape'.
 */
export function isTableDrawingFrame(node) {
    return isDrawingFrame(node) && getDrawingType(node) === 'table';
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'shape' AND
 * that is used as text frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame of type 'shape' and it is used
 *  as text frame.
 */
export function isTextFrameShapeDrawingFrame(node) {
    return isShapeDrawingFrame(node) && getTextFrameNode(node).length > 0;
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'connector'.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame of type 'connector'.
 */
export function isConnectorDrawingFrame(node) {
    var nodeType = getDrawingType(node);
    return isDrawingFrame(node) && nodeType === 'connector';
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'group'.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame of type 'group'.
 */
export function isGroupDrawingFrame(node) {
    return isDrawingFrame(node) && getDrawingType(node) === 'group';
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'group' and that contains
 * at least one child drawing of type 'shape'.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame of type 'group', that contains at least one child
 *  drawing of type 'shape'.
 */
export function isGroupDrawingFrameWithShape(node) {
    return isDrawingFrame(node) && getDrawingType(node) === 'group' && _.isObject(_.find(getAllGroupDrawingChildren(node), function (child) { return isTextFrameShapeDrawingFrame(child); }));
}

/**
 * Returns whether the passed node is a drawing frame that is grouped inside a
 * group.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a grouped drawing frame.
 */
export function isGroupedDrawingFrame(node) {
    return isDrawingFrame(node) && $(node).is(GROUPED_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing frame, that can be moved only
 * in the region near the border. In its center, it can contain text frames.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame, that cannot be moved by clicking
 *  in its center.
 */
export function isOnlyBorderMoveableDrawing(node) {
    return isTextFrameShapeDrawingFrame(node) || isGroupDrawingFrame(node);
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'shape'
 * and that contains text content. Furthermore the drawing is NOT marked as
 * 'classical' odf text frame, so that there is only reduced text functionality
 * available.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame, that is of type 'shape'
 *  and that contains text content. Furthermore the drawing is NOT marked as
 *  'classical' odf text frame, so that there is only reduced text functionality
 *  available.
 */
export function isReducedOdfTextframeNode(node) {
    return isTextFrameShapeDrawingFrame(node) && !$(node).is(ODFTEXTFRAME_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing frame, that is of type 'shape'
 * and that contains text content. Furthermore the drawing is marked as
 * 'classical' odf text frame, so that there is no text functionality
 * available.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing frame, that is of type 'shape'
 *  and that contains text content. Furthermore the drawing is marked as
 *  'classical' odf text frame, so that there is no reduced text functionality
 *  available.
 */
export function isFullOdfTextframeNode(node) {
    return isTextFrameShapeDrawingFrame(node) && $(node).is(ODFTEXTFRAME_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing node that's rotated by some angle.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing node that has rotation applied.
 */
export function isRotatedDrawingNode(node) {
    return $(node).hasClass(ROTATED_DRAWING_CLASSNAME);
}

/**
 * Returns whether the passed node is text node in drawing, that is rotated by some angle.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text node in drawing that has rotation applied.
 */
export function isRotatedTextInDrawingNode(node) {
    return $(node).hasClass(ROTATED_TEXT_IN_DRAWING_CLASSNAME);
}

/**
 * Returns whether the passed node is a drawing node that cannot be selected.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing node that is not selectable.
 */
export function isNonSelectableDrawingNode(node) {
    return $(node).hasClass(NOSELECT_CLASS);
}

/**
 * Returns whether the passed node is a drawing node that is a watermark.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing node that is a watermark drawing.
 */
export function isWatermarkDrawingNode(node) {
    return $(node).hasClass(WATERMARK_CLASS);
}

/**
 * Returns whether the passed node is drawing flipped horizontally.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is drawing flipped horizontally.
 */
export function isFlippedHorz(node) {
    return $(node).hasClass(FLIPPED_HORIZONTALLY_CLASSNAME);
}

/**
 * Returns whether the passed node is drawing flipped vertically.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is drawing flipped vertically.
 */
export function isFlippedVert(node) {
    return $(node).hasClass(FLIPPED_VERTICALLY_CLASSNAME);
}

/**
 * Returns whether the passed node is a snap point in a drawing frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a snap point node.
 */
export function isSnapPoint(node) {
    return $(node).is(SNAP_POINT_SELECTOR);
}

/**
 * Returns whether the passed node is in active cropping image mode.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is in a cropping mode.
 */
export function isActiveCropping(node) {
    return $(node).hasClass(ACTIVE_CROPPING_CLASS);
}

/**
 * Returns the number of drawing frames inside a specified drawing group. Only
 * the direct children are counted, not the children in sub groups.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Number|Null}
 *  The number of drawing frames inside a specified drawing group
 */
export function getGroupDrawingCount(node) {
    if (!isGroupDrawingFrame(node)) { return null; }
    return $(node).children().first().children().length;
}

/**
 * Returns the child drawing frame of a drawing group at a specified
 * position. Or null, if it cannot be determined.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @param {Number} [number]
 *  The zero based number of the child to be returned.
 *
 * @returns {Node|Null}
 *  The child drawing frame of a drawing group at a specified position.
 *  Or null, if it cannot be determined.
 */
export function getGroupDrawingChildren(node, number) {
    if (!isGroupDrawingFrame(node) || !_.isNumber(number)) { return null; }
    return $(node).children().first()[0].childNodes[number];
}

/**
 * Returns all drawing frames inside a specified drawing group. This does
 * not only contain direct children, but also the children of further groups
 * inside the group. If the option 'onlyDirectChildren' is set to true,
 * only the direct children are returned.
 * Returns null, if the specified node is not a group drawing node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @param {Object} [options]
 *  A map with options to control the selection. The following options are
 *  supported:
 *  - {Boolean} [options.onlyDirectChildren=false]
 *      If set to true, only those drawing nodes are returned, that are
 *      direct children of the group.
 *
 * @returns {Node|Null}
 *  The child drawing frames of a drawing group at a specified position.
 *  Or null, if it cannot be determined.
 */
export function getAllGroupDrawingChildren(node, options) {

    if (!isGroupDrawingFrame(node)) { return null; }

    if (getBooleanOption(options, 'onlyDirectChildren', false)) {
        return $(node).children().children(NODE_SELECTOR);
    } else {
        return $(node).find(NODE_SELECTOR);
    }
}

/**
 * Returns the closest ancestor drawing frame that contains the passed DOM
 * node.
 *
 * @param {HTMLDivElement|jQuery} node
 *  The descendant DOM node of a drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery|Null}
 *  The closest ancestor drawing frame that contains the passed DOM node;
 *  or null, if the passed node is not a descendant of a drawing frame.
 */
export function getDrawingNode(node) {
    var drawingFrame = $(node).closest(NODE_SELECTOR);
    return (drawingFrame.length === 1) ? drawingFrame : null;
}

/**
 * Returns the closest drawing node of a given node that is a text frame shape
 * drawing node. If this cannot be found, null is returned.
 *
 * @param {HTMLDivElement|jQuery} node
 *  If this object is a jQuery collection, uses the first DOM node it contains.
 *
 * @returns {jQuery|Null}
 *  The text frame drawing node, or null, if it cannot be determined.
 */
export function getClosestTextFrameDrawingNode(node) {

    // the closest drawing node that is ancestor of the specified node
    var drawingNode = getDrawingNode(node);
    if (!drawingNode) { return null; }

    return isTextFrameShapeDrawingFrame(drawingNode) ? drawingNode : null;
}

/**
 * Returns the closest drawing group node of a drawing node that is grouped.
 * If this cannot be found, null is returned.
 *
 * @param {NodeOrJQuery} drawingNode
 *  The grouped drawing node.
 *
 * @returns {jQuery|Null}
 *  The closest or farthest drawing group node, or null, if it cannot be determined.
 */
export function getGroupNode(drawingNode) {
    const contentNode = $(drawingNode).closest(GROUPCONTENT_SELECTOR);
    return contentNode?.length ? contentNode.parent() : null;
}

/**
 * Returns the farthest drawing group node of a drawing node that is grouped.
 * If this cannot be found, null is returned.
 *
 * @param {NodeOrJQuery} rootNode
 *  The upper bound for searching a parent group node.
 *
 * @param {NodeOrJQuery} drawingNode
 *  The grouped drawing node.
 *
 * @returns {JQuery|Null}
 *  The closest or farthest drawing group node, or null, if it cannot be determined.
 */
export function getFarthestGroupNode(rootNode, drawingNode) {
    const contentNode = findFarthest(rootNode, drawingNode, GROUPCONTENT_SELECTOR);
    return contentNode ? $(contentNode).parent() : null;
}

/**
 * Returns all container nodes of a specified drawing frame. The container nodes
 * contain the paragraphs and tables and have the class 'textframe' set. Typically
 * a drawing has only one of this container nodes. But if the specified drawing
 * is a group drawing, there can also be more than one container node.
 *
 * @param {HTMLDivElement|jQuery} drawingNode
 *  The drawing frame DOM node. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @param {Object} [options]
 *  A map with supported options:
 *  - {Boolean} [options.deep=false]
 *      If set to true, all text frames inside the specified drawing are searched. This
 *      is especially relevant for group drawings, that can contain several text frames
 *      inside the child drawings.
 *      If set to false, only the text frame that belongs to the specified drawing, is
 *      searched.
 *
 * @returns {jQuery}
 *  The container DOM node from the passed drawing frame that contains all
 *  top-level content nodes (paragraphs and tables). This can be a collection
 *  of several container nodes, if the specified drawing node is a group
 *  drawing.
 */
export function getTextFrameNode(drawingNode, options) {
    return getBooleanOption(options, 'deep', false) ? $(drawingNode).find(TEXTFRAME_NODE_SELECTOR) : $(drawingNode).find('> * > ' + TEXTFRAME_NODE_SELECTOR);
}

/**
 * Returns the container node of a drawing frame, that contains all
 * top-level content nodes (paragraphs and tables).
 * Info: In addition to getTextFrameNode function, this will not return any cloned node,
 * which can be the case when calling it during move or resize.
 *
 * @param {HTMLDivElement|jQuery} drawingNode
 *  The drawing frame DOM node. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The container DOM node from the passed drawing frame that contains all
 *  top-level content nodes (paragraphs and tables).
 */
export function getOriginalTextFrameNode(drawingNode) {
    return $(drawingNode).children(':not(.copy)').find('>' + TEXTFRAME_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a text frame `<div>` element, that is
 * used inside drawing frames
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div textframe element.
 */
export function isTextFrameNode(node) {
    return $(node).is(TEXTFRAME_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a text frame content `<div>` element,
 * that is used inside drawing frames
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div textframecontent element.
 */
export function isTextFrameContentNode(node) {
    return $(node).is(TEXTFRAMECONTENT_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing node, that is a text frame, that
 * automatically resizes its height. In this case the content node contains a
 * specific class.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an automatically resizing text frame drawing node.
 */
export function isAutoResizeHeightDrawingFrame(node) {
    // checking if the content node inside the text frame drawing has class 'autoresizeheight'
    return getContentNode(node).is(AUTORESIZEHEIGHT_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing node, that is a text frame, that
 * automatically resizes its font size. In this case the content node contains a
 * specific class.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text frame drawing node which automatically resizes its text.
 */
export function isAutoTextHeightDrawingFrame(node) {
    // checking if the content node inside the text frame drawing has class 'autotextheight'
    return getContentNode(node).is(AUTORESIZETEXT_SELECTOR);
}

/**
 * Returns whether the passed shape attributes "autoResizeHeight" is true
 * but only if "noAutoResize" is false
 *
 * @param {Object} [shapeAttrs]
 *  shape attributes
 *
 * @returns {Boolean}
 *  Whether the attributes automatically resizing height.
 */
export function isAutoResizeHeightAttributes(shapeAttrs) {
    return shapeAttrs && (shapeAttrs.autoResizeHeight && !shapeAttrs.noAutoResize);
}

/**
 * Whether there is at least one drawing with property "autoresizeheight" inside the
 * specified drawing group node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a group drawing node that contains at least one descendant
 *  which automatically adapts its height.
 */
export function isAutoResizeHeightGroup(node) {
    if (!isGroupDrawingFrame(node)) { return false; }
    var allDrawings = $(node).find(NODE_SELECTOR);
    return _.find(allDrawings, oneDrawing => isAutoResizeHeightAttributes(getExplicitAttributes(oneDrawing, 'shape'))) !== undefined;
}


/**
 * Returns whether the passed shape attributes "autoResizeText" is true
 * but only if "noAutoResize" is false
 *
 * @param {Object} [shapeAttrs]
 *  shape attributes
 *
 * @returns {Boolean}
 *  Whether the attributes automatically resizing font size.
 */
export function isAutoTextHeightAttributes(shapeAttrs) {
    return shapeAttrs && (shapeAttrs.autoResizeText && !shapeAttrs.noAutoResize);
}

/**
 * Returns whether the passed attributes describe a drawing that
 * is used as watermark.
 *
 * @param {Object} [attrs]
 *  The attribute set.
 *
 * @returns {Boolean}
 *  Whether the attributes describe a watermark drawing.
 */
export function isWatermarkAttributeSet(attrs) {
    return attrs && attrs.drawing && attrs.drawing.noSelect && ((attrs.geometry && attrs.geometry.textPath) || (/[Ww]atermark/.test(attrs.drawing.id)));
}

/**
 * Returns whether the passed attributes describe a drawing that cannot be selected.
 *
 * @param {Object} [attrs]
 *  The attribute set.
 *
 * @returns {Boolean}
 *  Whether the attributes describe a non selectable drawing.
 */
export function isNoSelectAttributeSet(attrs) {
    return attrs && attrs.drawing && attrs.drawing.noSelect;
}

/**
 * Returns whether the passed node is a drawing node, that is a text frame, that
 * has word wrap property set to false
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node has no word wrap property.
 */
export function isNoWordWrapDrawingFrame(node) {
    // checking if the content node inside the text frame drawing has class 'no-wordwrap'
    return getContentNode(node).hasClass(NO_WORDWRAP_CLASS);
}

/**
 * Returns whether the passed shape attributes of a drawing describe vertical
 * oriented text inside the drawing.
 *
 * @param {Object} [shapeAttrs]
 *  shape attributes
 *
 * @returns {Boolean}
 *  Whether the text inside the drawing with the specified shape attributes is
 *  vertically oriented.
 */
export function isVerticalTextDrawingFrame(shapeAttrs) {
    return !!(shapeAttrs && shapeAttrs.vert && shapeAttrs.vert !== 'horz');
}

/**
 * Returns whether the passed drawing with the passed shape attributes describe
 * a drawing with autoResizeHeight set to true and horizontal text alignment.
 * Additionally the drawing must not be selectable with a two point selection.
 * Info: The height of the drawing must not be set explicitely for such a
 *       drawing. But if for example the text is vertically oriented, the
 *       height must be set (55562).
 *
 * @param {Node|jQuery|Null} drawing
 *  The drawing node.
 *
 * @param {Object} [shapeAttrs]
 *  The shape attributes
 *
 * @returns {Boolean}
 *  Whether the passed drawing with the passed shape attributes describe a
 *  drawing with autoResizeHeight set to true and horizontal text alignment.
 */
export function isAutoResizeHeightDrawingFrameWithHorizontalText(drawing, shapeAttrs) {
    return isAutoResizeHeightAttributes(shapeAttrs) && !isTwoPointShape(drawing) && !isVerticalTextDrawingFrame(shapeAttrs);
}

/**
 * Returns whether the passed node is a drawing node, that is a text frame, that
 * has a fixed height.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text frame drawing node with fixed height.
 */
export function isFixedHeightDrawingFrame(node) {
    // checking if the content node inside the text frame drawing has NOT the class 'autoresizeheight'
    return isTextFrameShapeDrawingFrame(node) && !getContentNode(node).is(AUTORESIZEHEIGHT_SELECTOR);
}

/**
 * Returns whether the passed node is a drawing node, that has a minimum height specified.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text frame drawing node with minimum height.
 */
export function isMinHeightDrawingFrame(node) {
    // checking if the content node inside the text frame drawing has NOT the class 'autoresizeheight'
    return isTextFrameShapeDrawingFrame(node) && getContentNode(node).hasClass(MINFRAMEHEIGHT_CLASS);
}

/**
 * Returns whether the passed node is a text frame `<div>` element, that is
 * used inside drawing frames as container for additional drawings.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div groupcontent element.
 */
export function isGroupContentNode(node) {
    return $(node).is(GROUPCONTENT_SELECTOR);
}

/**
 * Returns whether the passed node is a unsupported drawing.
 *
 * @param {Node|jQuery|Null} [drawingFrame]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a unsupported drawing element.
 */
export function isUnsupportedDrawing(drawingFrame) {
    return $(drawingFrame).hasClass(UNSUPPORTED_CLASS);
}

/**
 * Returns the model of the specified drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {Object|Null}
 *  The model object of the specified drawing frame, as specified while the
 *  frame has been created.
 */
export function getModel(drawingFrame) {
    return $(drawingFrame).first().data(DATA_MODEL) || null;
}

/**
 * Returns the unique identifier of the specified drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {String}
 *  The unique identifier of the specified drawing frame, as specified
 *  while the frame has been created.
 */
export function getUid(drawingFrame) {
    return $(drawingFrame).first().data(DATA_UID);
}

/**
 * Returns the root content node of the specified drawing frame containing
 * all type specific contents of the drawing.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Object} [options]
 *  A map with additional options. The following options are supported:
 *  - {Boolean} [options.isResizeActive=false]
 *     If this method is called during resizing of the shape.
 *
 * @returns {jQuery}
 *  The root content node of the specified drawing frame.
 */
export function getContentNode(drawingFrame, options) {
    var selector = '.' + CONTENT_CLASS + (getBooleanOption(options, 'isResizeActive', false) ? '.copy' : ':not(.copy)');
    return $(drawingFrame).first().children(selector);
}

/**
 * Returns the Canvas node (canvas) of the specified drawing frame
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The canvas node of the specified drawing frame.
 */
export function getCanvasNode(drawingFrame) {
    // images inside shapes (OX Text) can have border which is also implemented as canvas,
    // therefore search query needs to be more specific than just "find", see #50908, #50909
    return $(drawingFrame).children(CONTENT_NODE_SELECTOR).children(CANVAS_NODE_SELECTOR);
}

/**
 * Helper function to clone one drawing and the canvas elements
 * it contains. This function also handles a drawing of type 'group'
 * correctly.
 *
 * @param {EditModel} docModel
 *  The document model.
 *
 * @param {jQuery} drawing
 *  The jQuerified drawing node.
 *
 * @param {Boolean} isResizeActive
 *  On resize don't replace cloned canvas, just prepend img, for better UX.
 *
 * @returns {jQuery}
 *  The jQueryfied content node, that is a direct child of the
 *  specified drawing node.
 */
export function cloneDiv(docModel, drawing, isResizeActive) {
    drawing.find('>.copy').remove();
    var content = drawing.children('.content:not(.copy)'); // only direct child (take care of groups)
    var clone = content.clone(isGroupContentNode(content));

    function cloneCanvas(canvas) {
        var img = document.createElement('img');
        img.src = canvas.toDataURL();
        img.width         = canvas.width;
        img.height        = canvas.height;
        img.className     = canvas.className;
        img.style.cssText = canvas.style.cssText;
        img.style.position = 'absolute';
        return img;
    }

    if (isResizeActive) {
        if (docModel.getApp().isPresentationApp() && docModel.isEmptyPlaceHolderDrawing(drawing)) {
            clone.find('.templatetext, .helper').remove();
        }
        drawing.prepend(clone);
    } else {
        drawing.append(clone);
    }

    var orgCanvases = content.find('canvas').get();  // the original canvas elements in the content node

    clone.find('canvas').get().forEach(function (canvas, index) {
        var img = cloneCanvas(orgCanvases[index]);
        if (!isResizeActive) {
            $(canvas).replaceWith(img);
        } else {
            if (isGroupContentNode(clone)) {
                clone.find('.textframecontent').eq(index).prepend(img);
            } else {
                clone.prepend(img);
            }
        }
    });

    clone.addClass('copy');

    return clone;
}

/**
 * Returns whether the specified node is a canvas node (canvas)
 *
 * @param {HTMLElement|jQuery} node
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery}
 *  Whether the specified node is a canvas node.
 */
export function isCanvasNode(node) {
    return $(node).is(CANVAS_NODE_SELECTOR);
}

/**
 * Returns whether the specified node is currently modified by move, resize or rotate operations.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node element.
 *
 * @returns {Boolean}
 */
export function isModifyingDrawingActive(drawingNode) {
    var $node = $(drawingNode);

    return $node.hasClass('activedragging') || $node.hasClass(ROTATING_STATE_CLASSNAME);
}

/**
 * Clears and returns the root content node of the specified drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The empty root content node of the specified drawing frame.
 */
export function getAndClearContentNode(drawingFrame) {
    return getContentNode(drawingFrame).empty().removeClass(PLACEHOLDER_CLASS).attr('style', '');
}

/**
 * For text shapes without paragraph it is necessary to insert an implicit paragraph, so that
 * the user can set the selection into the shape and insert text content (50644, 52378).
 *
 * @param {Object} docModel
 *  The application model.
 *
 * @param {HTMLElement|jQuery} drawing
 *  The drawing node.
 *
 * @returns {Boolean}
 *  Whether the text frame was prepared (and an implicit paragraph inserted). In this case a
 *  reformatting of the drawing might be required.
 */
export function checkEmptyTextShape(docModel, drawing) {

    var // a required text frame node in the shape
        textFrameNode = null,
        // the attributes of families 'character' and 'paragraph' for the implicit paragraph
        attributes = null,
        // whether the text frame was modified an implicit paragraph was inserted into the shape
        textFramePrepared = false;

    if (getDrawingType(drawing) === 'shape' && !isTextFrameContentNode(getContentNode(drawing))) {
        if (!isTwoPointShape(drawing)) { // not all shapes get a paragraph
            textFrameNode = prepareDrawingFrameForTextInsertion(drawing);  // inserting an implicit paragraph into the drawing
            if (textFrameNode) {
                attributes = getParagraphAttrsForDefaultShape(docModel);
                textFrameNode.append(docModel.getValidImplicitParagraphNode(attributes));
            }
            textFramePrepared = true;
        }
    }

    return textFramePrepared;
}

/**
 * Generating the target inheritance chain for a specified drawing node. This inheritance
 * chain need to be defined, if a color scheme is used in the merged attributes. Because
 * the drawing is painted in 'DrawingFrame.drawShape', where the drawing node is no longer
 * available, it is necessary to add the target chain into the merged attributes. For
 * performance reasons this is only done, is the attributes contain a scheme color for
 * fill color or line color.
 *
 * @param {Object} docModel
 *  The application model.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The drawing node.
 *
 * @param {Object} mergedAttrs
 *  The complete set of drawing attributes. It can happen, that this object is modified
 *  within this function!
 *  This can also be an incomplete set of attributes, if it is called for grouped
 *  drawings (48096).
 */
export function handleTargetChainInheritance(docModel, drawingFrame, mergedAttrs) {

    if (!mergedAttrs) { return; }
    if (!isFillThemed(mergedAttrs.fill) && !isLineThemed(mergedAttrs.line)) { return; }

    // setting theme information for colors at 'line' and 'fill' family
    // -> setting the target chain always for gradients, because the theme color might be hidden in
    //    color stops (47924)
    var themeTargets = docModel.getThemeTargets(drawingFrame);
    if (!themeTargets) { return; }

    mergedAttrs.fill = mergedAttrs.fill || {};
    mergedAttrs.line = mergedAttrs.line || {};
    mergedAttrs.fill[TARGET_CHAIN_PROPERTY] = themeTargets;
    mergedAttrs.line[TARGET_CHAIN_PROPERTY] = themeTargets;
}

/**
 * Increasing the height of a group drawing frame, that contains text frames
 * with auto resize height functionality. Those text frames expand automatically
 * the height of the drawing, if required.
 *
 * @param {EditApplication} app
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The text frame drawing node with auto resize height functionality.
 */
export function updateDrawingGroupHeight(app, drawingNode) {

    // comparing lower border of auto resize text frame node with the group

    var // the offset of the bottom border in pixel
        drawingBottom = 0,
        // an optional group node surrounding the specified text frame drawing node
        groupNode = null,
        // the offset of the group border in pixel
        groupBottom = 0,
        // the new height of the group drawing node
        newHeight = 0,
        // whether the group iteration need to be continued
        doContinue = true,
        // zoom factor of the view
        zoomFactor = app.getView().getZoomFactor(),
        // collection of all parent group drawing nodes of current drawing node
        rotatedGroupParentNodes = [],
        // whether there is at least one child drawing with invalid values for height or offset (57892)
        isInvalidChildDrawing = false;

    // helper function to find the pixel positions of all bottom borders inside a group
    function getLowestChildDrawingBottom(node) {

        var // the lowest bottom pixel position of all child drawings inside the specified node
            lowestBottom = null;

        // iterating over all children of the specified (group) node
        _.each(node.children(), function (oneDrawing) {

            // the height of the drawing
            var drawingHeight = $(oneDrawing).height();
            // the (jQuery) offset of the drawing
            var offset = $(oneDrawing).offset();
            // the bottom pixel position of one child node
            var localBottom = round((offset.top / zoomFactor) + drawingHeight);

            isInvalidChildDrawing = (drawingHeight === 0) || (offset.left === 0 && offset.top === 0); // TODO: Check, when this happens

            // the larger the value, the deeper the drawing
            if (lowestBottom === null || localBottom > lowestBottom) { lowestBottom = localBottom; }
        });

        return lowestBottom;
    }

    // helper function to unrotate all rotated group parent nodes of current node,
    // so that calling offset on that node will return correct value.
    function unrotateAllGroupParentNodes(drawingNode) {
        var rotatedGroupParentNodes = [];
        var groupParentNodes = $(drawingNode).parents(NODE_SELECTOR);

        _.each(groupParentNodes, function (parentNode) {
            var rotationAngle = getDrawingRotationAngle(app.getModel(), parentNode);
            if (_.isNumber(rotationAngle) && rotationAngle !== 0) {
                $(parentNode).css({ transform: '' }); // reset to def to get values
                rotatedGroupParentNodes.push({ node: parentNode, angle: rotationAngle });
            }
        });
        return rotatedGroupParentNodes;
    }

    var rotationAngle = null;

    // iterating over all groups
    while (doContinue && isGroupedDrawingFrame(drawingNode)) {
        rotatedGroupParentNodes = unrotateAllGroupParentNodes(drawingNode);
        rotationAngle = getDrawingRotationAngle(app.getModel(), groupNode);
        if (_.isNumber(rotationAngle) && rotationAngle !== 0) {
            $(groupNode).css({ transform: '' }); // reset to def to get values
            rotatedGroupParentNodes.push({ node: groupNode, angle: rotationAngle });
        }

        groupNode = getGroupNode(drawingNode);
        groupBottom = groupNode ? round(groupNode.offset().top / zoomFactor + groupNode.height()) : null;
        drawingBottom = getLowestChildDrawingBottom($(drawingNode).parent());

        // comparing the bottom borders of group and specified text frame
        if (!isInvalidChildDrawing && _.isNumber(groupBottom) && _.isNumber(drawingBottom) && (groupBottom !== drawingBottom)) {
            // increasing or decreasing the height of the group without operation
            newHeight = round(groupNode.height() + drawingBottom - groupBottom);
            groupNode.height(newHeight);
            drawingNode = groupNode;
        } else {
            doContinue = false;
        }
    }
    _.each(rotatedGroupParentNodes, function (obj) { // all collected rotated group root nodes need to be returned to original rotation
        $(obj.node).css({ transform: 'rotate(' + obj.angle + 'deg)' });
    });
}

/**
 * Prepares a specified drawing frame for text insertion. This requires
 * that the content node inside drawing frame is cleared and then a new
 * text frame child is appended. This new text frame element is returned
 * and can be used as parent for paragraphs, tables, ... .
 * Only drawingFrames of type 'shape' allow this conversion.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The new text frame 'div' element, that can be used as container for
 *  the text content.
 */
export function prepareDrawingFrameForTextInsertion(drawingFrame) {

    // only drawing frames of type 'shape' can be converted
    if (!isShapeDrawingFrame(drawingFrame) || isWatermarkDrawingNode(drawingFrame)) { return $(); }

    // the content node of the drawing
    var contentNode = getAndClearContentNode(drawingFrame);
    // the text frame node inside the content node, parent of paragraphs and tables
    // -> required for setting cursor type, distance to content node and click event
    var textFrameNode = $('<div class="textframe" contenteditable="true" data-gramm="false">');

    // making content node visible by assigning a border via class 'textframecontent'
    contentNode.addClass(TEXTFRAMECONTENT_NODE_CLASS).append(textFrameNode);
    // in IE the drawing node itself must have contenteditable 'false', otherwise the
    // grabbers will be visible. Other browsers have to use the value 'true'.
    $(drawingFrame).attr('contenteditable', false);

    // returning the new created text frame node
    return textFrameNode;
}

/**
 * Inserts replacement layout nodes for unsupported drawing types. Inserts
 * the passed name and description of the drawing as text.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {String} [name]
 *  The name of the drawing frame.
 *
 * @param {String} [description]
 *  The description text for the drawing frame.
 */
export function insertReplacementNodes(drawingFrame, name, description) {

    var // the type of the drawing frame
        type = getDrawingType(drawingFrame),
        // the empty content node
        contentNode = getAndClearContentNode(drawingFrame),
        // the inner width and height available in the content node
        innerWidth = max(0, drawingFrame.outerWidth() - 2),
        innerHeight = max(0, drawingFrame.outerHeight() - 2),
        // the vertical padding in the content node
        verticalPadding = math.clamp(min(innerWidth, innerHeight) / 24, 1, 6),
        // the font size of the picture icon
        pictureIconSize = math.clamp(min(innerWidth - 16, innerHeight - 28), 8, 72),
        // the base font size of the text
        fontSize = math.clamp(min(innerWidth, innerHeight) / 4, 9, 13);

    // set border width at the content node, insert the picture icon
    contentNode
        .addClass(PLACEHOLDER_CLASS)
        .css({
            padding: floor(verticalPadding) + 'px ' + floor(verticalPadding * 2) + 'px',
            fontSize: fontSize + 'px'
        })
        .append(
            $(`<div class="abs background-icon" style="line-height:${innerHeight}px;">`).append(
                createIcon(getDrawingTypeIcon(type), { style: { fontSize: pictureIconSize + 'px' } })
            ),
            $('<p>').text(name ? _.noI18n(name) : getDrawingTypeLabel(type))
        );

    // insert description if there is a reasonable amount of space available
    if ((innerWidth >= 20) && (innerHeight >= 20)) {
        contentNode.append($('<p>').text(_.noI18n(description)));
    }
}

// formatting -------------------------------------------------------------

/**
 * Updates the CSS formatting of the passed drawing frame, according to the
 * passed generic formatting attributes.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame to be updated. If this object is a
 *  jQuery collection, uses the first DOM node it contains.
 *
 * @param {Object} mergedAttributes
 *  The drawing attribute set (a map of attribute maps as name/value pairs,
 *  keyed by the 'drawing' attribute family), containing the effective
 *  attribute values merged from style sheets and explicit attributes.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.skipFlipping=false]
 *      If set to true, all flipping attributes will be ignored, and the
 *      drawing frame will be rendered unflipped.
 */
export function updateFormatting(app, drawingFrame, mergedAttributes, options) {

    // ensure to have a jQuery object
    drawingFrame = $(drawingFrame);

    // the document model
    var docModel = app.getModel();
    // the drawing attributes of the passed attribute set
    var drawingAttributes = mergedAttributes.drawing;
    // type of the drawing object: 'image', ...
    var type = getDrawingType(drawingFrame);
    // the content node inside the drawing frame
    var contentNode = getContentNode(drawingFrame);
    // whether to skip flipping attributes
    var skipFlipping = getBooleanOption(options, 'skipFlipping', false);
    // whether the drawing frame is used as internal model container (reduced rendering)
    var internalModel = drawingFrame.hasClass(INTERNAL_MODEL_CLASS);
    // the group node belonging to a grouped drawing node
    var drawingGroupNode = null;
    // the replacement data for unsupported drawings
    var replacementData = drawingAttributes.replacementData || '';
    // rendering result (false to fall-back to placeholder graphic)
    var rendered = false;
    var drawingAngle = (drawingAttributes.rotation) || 0;
    var isInlineDrawing = (drawingAttributes.inline) || false;

    // first remove the class which marks this drawing as unsupported
    drawingFrame.removeClass(UNSUPPORTED_CLASS);

    // creates an image node in the content node with the specified source URL
    function createImageNodeFromUrl(srcUrl) {
        return app.createImageNode(srcUrl, { timeout: 15000 }).done(function (imgNode) {
            var cropNode = $('<div class="' + CROPPING_CLASS + '">').append(imgNode);
            getAndClearContentNode(drawingFrame).append(cropNode);
        });
    }

    // creates an image node or an SVG image according to the passed image data
    function createImageNodeFromData(imageData) {
        if (/^data:/.test(imageData)) {
            return createImageNodeFromUrl(imageData);
        }
        if (/^<svg/.test(imageData)) {
            getAndClearContentNode(drawingFrame).append(parseAndSanitizeHTML(imageData));
            return $.when();
        }
        globalLogger.warn('DrawingFrame.createImageNodeFromData() - unsupported source data for the image object');
        return $.Deferred().reject();
    }

    // updates all formatting for unsupported objects
    function updateUnsupportedFormatting() {
        if (replacementData.length === 0) {
            // insert default replacement nodes, if no replacement data is available
            insertReplacementNodes(drawingFrame, drawingAttributes.name, drawingAttributes.description);
        } else if (!drawingFrame.data('has-replacement')) {
            // replacement data MUST NOT change at runtime
            createImageNodeFromData(replacementData);
            drawingFrame.data('has-replacement', true);
        }
        // add class to mark this drawing as a unsupported drawing.
        drawingFrame.addClass(UNSUPPORTED_CLASS);
    }

    // updates all formatting for image objects
    function updateImageFormatting() {

        var // special attributes for images
            imageAttributes = mergedAttributes.image,
            // the promise that handles the loading of the image
            imageLoadPromise = null,
            snapRect;

        // updates attributes of the image node after loading
        // -> but not for grouped drawings. Those drawings are handled by the group container
        function updateImageAttributes(options) {

            var // current width of the drawing frame, in 1/100 mm
                drawingWidth = convertLengthToHmm(drawingFrame.width(), 'px'),
                // current height of the drawing frame, in 1/100 mm
                drawingHeight = convertLengthToHmm(drawingFrame.height(), 'px'),
                // whether the call of this function is asynchronous
                isAsync = getBooleanOption(options, 'isAsync', true),
                // whether the visibility was changed (sometimes required in async mode)
                visibilityChanged = false;

            if ((drawingWidth > 0) && (drawingHeight > 0)) {

                if (isGroupedDrawingFrame(drawingFrame)) {
                    // For grouped drawings, it is useless to set the image attributes directly,
                    // because these attributes are dependent from the group itself.
                    // -> after the image is loaded (deferred!), the image attributes need to be set again
                    // to be correct for the complete group.
                    // The call of resizeDrawingsInGroup() is only required after loading the document for
                    // grouped drawings when loading happens without fast load and without
                    // local storage.
                    if (isAsync) {
                        if (docModel.handleContainerVisibility) {
                            visibilityChanged = docModel.handleContainerVisibility(drawingFrame, { makeVisible: true });
                        }
                        resizeDrawingsInGroup(app, getGroupNode(drawingFrame), mergedAttributes);
                        if (visibilityChanged && docModel.handleContainerVisibility) {
                            docModel.handleContainerVisibility(drawingFrame, { makeVisible: false });
                        }
                    }
                } else {
                    setImageCssSizeAttributes(app, drawingFrame, contentNode, drawingWidth, drawingHeight, drawingAttributes, imageAttributes);
                }
                // remove class which marks this drawing as unsupported
                drawingFrame.removeClass(UNSUPPORTED_CLASS);
                // update the border and filling of image drawings
                updateLineAndFillFormatting(app, drawingFrame, mergedAttributes);
            }
        }

        updateLineAndFillFormatting(app, drawingFrame, mergedAttributes); // update the border and filling of image drawings

        // create the image or SVG node (considered to be constant at runtime)
        if (contentNode.find('>.' + CROPPING_CLASS).children().length === 0) {
            // start with replacement graphics (image data will be loaded asynchronously)
            updateUnsupportedFormatting();
            if (imageAttributes.imageData.length > 0) {
                imageLoadPromise = createImageNodeFromData(imageAttributes.imageData).done(updateImageAttributes);
            } else if (imageAttributes.imageUrl.length > 0) {
                // convert relative URL to an application-specific absolute URL
                var serverURL = getFileUrl(app, imageAttributes.imageUrl, { uid: false });
                imageLoadPromise = createImageNodeFromUrl(serverURL).done(updateImageAttributes);
            } else {
                globalLogger.warn('DrawingFrame.updateFormatting() - missing source data for the image object');
                imageLoadPromise = $.when();
            }
        } else {
            // update attributes of an existing image element
            updateImageAttributes({ isAsync: false });
            imageLoadPromise = $.when();
        }

        imageLoadPromise.then(function () {
            if (app.isPresentationApp() || app.isTextApp()) { docModel.trigger('image:loaded', drawingFrame); } // this might be interesting for other apps, too
        });

        if (app.isPresentationApp()) {
            snapRect = new Rectangle(0, 0, 0, 0);
            snapRect.width = drawingFrame.outerWidth(true);
            snapRect.height = drawingFrame.outerHeight(true);
            drawingFrame.data('shapeGeoData', { avList: [], gdList: [], cxnList: PRESET_RECT_CXNLIST });
        }

        // image successfully rendered
        return true;
    }

    // updates chart formatting
    function updateChartFormatting() {

        // the drawing object model
        var model = getModel(drawingFrame);
        // drawing object model must exist for chart objects
        if (!model) { return false; }

        if (!model.isValidChartType()) { return false; }

        // the chart rendering engine (lazy initialization, see below)
        var renderer = drawingFrame.data('chart-renderer');

        // lazy creation of the renderer
        if (!renderer) {
            // unique DOM element identifier for the renderer
            var chartId = 'io-ox-documents-chart-' + getUid(drawingFrame);
            contentNode.addClass('chartholder');

            // localization for CanvasJS
            if (!CULTURE_INFO) {
                CULTURE_INFO = {
                    decimalSeparator: LOCALE_DATA.dec,
                    digitGroupSeparator: LOCALE_DATA.group,
                    shortDays: LOCALE_DATA.shortWeekdays.slice(),
                    days: LOCALE_DATA.longWeekdays.slice(),
                    shortMonths: LOCALE_DATA.shortMonths.slice(),
                    months: LOCALE_DATA.longMonths.slice()
                };
                CanvasJS.addCultureInfo('en', CULTURE_INFO);
            }

            contentNode.append('<div id="' + chartId + '" class="chartnode"></div>');

            // really important call, because of a bug in canvasJS
            model.resetData();

            renderer = new CanvasJS.Chart(chartId, model.getModelData());

            // workaround for broken tooltip when changing chart type
            var updateToolTip = renderer.toolTip._updateToolTip;
            renderer.toolTip._updateToolTip = function (...args) {
                try {
                    updateToolTip.apply(this, args);
                } catch (e) {
                    globalLogger.warn('chart tooltip error', e);
                }
            };

            drawingFrame.data('chart-renderer', renderer);
            model.firstInit();
        }

        // render the chart object
        model.updateRenderInfo();
        try {
            // FIX for bug 53632, if the height is to small canvasjs throws a exception.
            // If canvasjs fix the bug, this workaround can be removed.
            if (contentNode.height() < 20) {
                renderer.set('data', []);
            }

            renderer.resetOverlayedCanvas();
            renderer.render();
        } catch (ex) {
            globalLogger.exception(ex, 'chart rendering error');
        }

        // background color of model data is already prepared for css
        contentNode.css('backgroundColor', model.getModelData().cssBackgroundColor);

        // chart successfully rendered
        return true;
    }

    // updates drawings of type 'table'
    function updateTableFormatting() {
        var snapRect = new Rectangle(0, 0, 0, 0);
        snapRect.width = drawingFrame.outerWidth(true);
        snapRect.height = drawingFrame.outerHeight(true);

        // adding auto resize height to the table
        drawingFrame.addClass(AUTORESIZEHEIGHT_CLASS);

        if (app.isPresentationApp()) { // TODO: height is not correct as row and cell format is happened later
            drawingFrame.data('shapeGeoData', { avList: [], gdList: [], cxnList: PRESET_RECT_CXNLIST });
        }

        // table successfully rendered
        return true;
    }

    // updates the class at the drawing, that is used to find standard odt text frames (and not shapes with text)
    function updateTextFrameState() {
        if (app.isODF()) {
            // the explicit drawing attributes (not using merge attributes, because they always contain a style id)
            var attrs = getExplicitAttributeSet(drawingFrame);
            drawingFrame.toggleClass(ODFTEXTFRAME_CLASS, _.isString(attrs.styleId));
        }
    }

    function updateNoteFormatting() {

        // update the fill style
        updateFillFormatting(app, drawingFrame, mergedAttributes);

        // the line attributes
        var lineAttrs = mergedAttributes.line;
        switch (lineAttrs.type) {
            case 'none':
                contentNode.css({ left: 0, top: 0, right: 0, bottom: 0, border: '' });
                break;
            case 'solid':
                var lineWidth = getLineWidthPx(lineAttrs);
                var lineStyle = getCssBorderStyle(lineAttrs.style);
                var lineColor = docModel.getCssColor(lineAttrs.color, 'line', lineAttrs[TARGET_CHAIN_PROPERTY]);
                var offset = 1 - lineWidth;
                contentNode.css({ left: offset, top: offset, right: offset, bottom: offset, border: lineWidth + 'px ' + lineStyle + ' ' + lineColor });
                break;
            default:
                globalLogger.warn('DrawingFrame.updateNoteFormatting(): unknown line type "' + lineAttrs.type + '"');
        }

        return true;
    }

    /**
     * Rotated inline shape should have correction margins, so that text flows around surrounding bound box.
     *
     * @param {Number} drawingWidth
     * @param {Number} drawingHeight
     * @param {Number} rad
     *
     * @returns {Object} horizontal and vertical correction values.
     */
    function getCorrectionMarginsForRotatedShape(drawingWidth, drawingHeight, rad) {
        var boundingHeight = drawingWidth * abs(sin(rad)) + drawingHeight * abs(cos(rad));
        var boundingWidth = drawingWidth * abs(cos(rad)) + drawingHeight * abs(sin(rad));
        var marginDiffH = (boundingWidth - drawingWidth) / 2;
        var marginDiffV = (boundingHeight - drawingHeight) / 2;

        return { tb: marginDiffV, lr: marginDiffH };
    }

    // handle theme color inheritance chain (the merged attributes object might be modified)
    handleTargetChainInheritance(docModel, drawingFrame, mergedAttributes);

    if (!skipFlipping && !isGroupedDrawingFrame(drawingFrame)) { // grouped drawing flipping is handled inside handleDrawingInsideDrawingGroup
        handleFlipping(docModel, drawingFrame, drawingAttributes);
    }

    // update drawing frame specific to the drawing type
    switch (type) {

        case 'image':
            rendered = internalModel || updateImageFormatting();
            break;

        case 'ole':
            rendered = internalModel || updateImageFormatting();
            break;

        case 'chart':
            rendered = internalModel || updateChartFormatting();
            break;

        case 'table':
            rendered = internalModel || updateTableFormatting();
            updateTextFrameState();
            break;

        case 'connector':
        case 'shape':
            rendered = updateShapeFormattingLocal(app, drawingFrame, mergedAttributes, options);
            updateTextFrameState();
            if (app.isPresentationApp() && type === 'connector') {
                drawingFrame.toggleClass(START_LINKED_CLASS, !!(mergedAttributes.connector && mergedAttributes.connector.startId));
                drawingFrame.toggleClass(END_LINKED_CLASS, !!(mergedAttributes.connector && mergedAttributes.connector.endId));
            }
            break;

        case 'note':
            rendered = internalModel || updateNoteFormatting();
            break;

        case 'group':
            // the group needs to take care of the size of all children
            resizeDrawingsInGroup(app, drawingFrame, mergedAttributes);
            rendered = true;
            break;
    }

    // an additional resize of drawings in groups is required after all children are added to the group.
    // -> during working with document, operations are only generated for the group itself, so that
    //    'DrawingFrame.isGroupedDrawingFrame()' should always be false.
    // -> but during working with the document, this step is also necessary after undo (47204)
    // -> Performance: In the Presentation app the slide format manager has to handle correct order
    //                 of the drawing formatting during loading (also undo does not cause problems
    //                 in the Presentation app).
    // TODO: This step should be avoided for all applications (performance)!
    if (!app.isPresentationApp() && isGroupedDrawingFrame(drawingFrame)) {
        drawingGroupNode = getGroupNode(drawingFrame);
        if (drawingGroupNode && drawingGroupNode.length > 0) {
            resizeDrawingsInGroup(app, drawingGroupNode, mergedAttributes);
        }
    }

    // add replacement in case the drawing type is not supported, or if rendering was not successful
    if (!rendered && !internalModel) {
        if (mergedAttributes.image) { updateImageFormatting(); } else { updateUnsupportedFormatting(); }
    }

    if (isInlineDrawing && drawingAngle) { // rotated inline drawings need corrected margins not to colide with surrounding text
        var corMargins = getCorrectionMarginsForRotatedShape(drawingFrame.width(), drawingFrame.height(), drawingAngle * PI_180);
        drawingFrame.css({ marginTop: corMargins.tb, marginBottom: corMargins.tb, marginLeft: corMargins.lr, marginRight: corMargins.lr });
    }
}

/**
 * returns the wrapped canvas for the current node.
 *
 * @param {EditApplication} app
 *
 * @param {jQuery} [currentNode]
 *
 * @param {Object} [options]
 *  - {Boolean} [options.resizeIsActive=false]
 *      If this method is called during resizing of the shape.
 */
function createWrappedCanvas(app, currentNode, options) {

    // if process of resizing of drawing is currently active
    var resizeIsActive  = getBooleanOption(options, 'resizeIsActive', false);
    // the canvasNode inside the drawing frame
    var canvasNode = getCanvasNode(currentNode);
    // the canvas wrapper
    var canvas = null;

    // while resizing of the drawing is active, there are cloned and original canvas,
    // only first has to be fetched
    if (resizeIsActive) {
        canvasNode = canvasNode.first();
    } else if (canvasNode.length > 1) {
        canvasNode = canvasNode.last();
    }

    if (canvasNode.length > 0) {
        canvas = new Canvas({ classes: CANVAS_CLASS, node: canvasNode });
    } else {
        canvas = new Canvas({ classes: CANVAS_CLASS });
        getContentNode(currentNode).prepend(canvas.el);
    }
    return canvas;
}

/**
 * Initializes the position and size of the passed canvas wrapper, relative
 * to its parent container element.
 *
 * @param {Canvas} canvas
 *  The canvas wrapper to be initialized.
 *
 * @param {String} drawingType
 *  The type of the drawing frame used to decide how much to pull out the
 *  canvas element from the parent element (full border for images, half
 *  border otherwise).
 *
 * @param {Rectangle} snapRect
 *  The location of the shape.
 *
 * @param {Number} lineWidth
 *  The width of the border lines of the shape, in pixels.
 *
 * @param {Object} [options]
 *  - {String|null} [options.typeOfFill]
 *      The type of the fill attributes of the drawing, if specfied. Otherwise null.
 */
function initializeCanvasSize(canvas, drawingType, snapRect, lineWidth, options) {

    // whether the drawing is of type image (in this case the border is completely outside of the drawing)
    var isImageDrawing = drawingType === 'image';
    // the (rounded) half width of the line -> only required for non-image drawings
    var halfLineWidth = ceil(lineWidth / 2);
    // how much to pull out the canvas element from the parent element (full border for images)
    var offsetSize = isImageDrawing ? lineWidth : halfLineWidth;
    // total size of the canvas element
    var totalWidth = snapRect.width + 2 * offsetSize;
    var totalHeight = snapRect.height + 2 * offsetSize;
    // CSS offset for the canvas element
    var nodeLeft = snapRect.left - offsetSize;
    var nodeTop = snapRect.top - offsetSize;
    // translation for the canvas rectangle (middle of leading borders on zero coordinates)
    var translateLeft = snapRect.left;
    var translateTop = snapRect.top;

    // for non image drawings, the border is half outside the drawing node
    if (!isImageDrawing) {
        translateLeft -= halfLineWidth;
        translateTop -= halfLineWidth;
    }

    var newRect = new Rectangle(translateLeft, translateTop, totalWidth, totalHeight);
    if (options && options.typeOfFill === 'bitmap') {
        // initialize effective rectangle of the canvas wrapper, do not clear the contents
        canvas.relocate(newRect);
    } else {
        // initialize and clear the canvas
        canvas.initialize(newRect);
    }

    // set the CSS location of the canvas element relative to its parent element
    canvas.$el.css({ left: nodeLeft, top: nodeTop });
}

/**
 * a initialized canvas is returned for the current node
 *
 * @param {EditApplication} app
 *
 * @param {jQuery} [currentNode]
 */
function initializeCanvas(app, currentNode, snapRect, lineWidth, options) {

    // the canvas wrapper of the border node
    var canvas = createWrappedCanvas(app, currentNode, options);

    // initialize location of the canvas element
    initializeCanvasSize(canvas, getDrawingType(currentNode), snapRect, lineWidth, options);

    return canvas;
}

function collectPathCoordinates(collector, command) {
    var
        pathCoords  = collector.pathCoords,

        cx          = collector.cx || 0,
        cy          = collector.cy || 0,
        fXScale     = collector.fXScale || 1,
        fYScale     = collector.fYScale || 1,

        getValue    = collector.getGeometryValue,

        i           = 0,
        pts         = null,
        length      = null,

        commandType = command.c;

    // calculates the point on an arc that corresponds to the given angle (ms like)
    function calcArcPoint(rx, ry, angle) {
        var at2 = atan2(rx * sin(angle), ry * cos(angle));
        return { x: rx * cos(at2) * fXScale, y: ry * sin(at2) * fYScale };
    }

    if (commandType === 'moveTo') {

        pathCoords.xValues.push(round(getValue(command.x) * fXScale));
        pathCoords.yValues.push(round(getValue(command.y) * fYScale));

    } else if (commandType === 'lineTo') {

        pathCoords.xValues.push(round(getValue(command.x) * fXScale));
        pathCoords.yValues.push(round(getValue(command.y) * fYScale));

    } else if (commandType === 'arcTo') {
        var
            rx    = getValue(command.wr),
            ry    = getValue(command.hr),
            stAng = getValue(command.stAng) * PI_RAD,
            swAng = getValue(command.swAng),

            // calculating new currentPoint
            startPoint  = calcArcPoint(rx, ry, stAng),
            endPoint    = calcArcPoint(rx, ry, stAng + swAng * PI_RAD);

        pathCoords.xValues.push(cx);
        pathCoords.yValues.push(cy);
        pathCoords.xValues.push(cx = round(cx + endPoint.x - startPoint.x));
        pathCoords.yValues.push(cy = round(cy + endPoint.y - startPoint.y));

    } else if (commandType === 'quadBezierTo') {

        if (command.pts) {
            pts     = command.pts;
            length  = pts.length;

            if (length % 2 === 0) {
                for (i = 0; i < length; i += 2) {

                    // pathCoords.push({
                    //     type: 'quad',
                    //
                    //     x1: getValue(pts[i].x) * fXScale,
                    //     y1: getValue(pts[i].y) * fYScale,
                    //     x2: (cx = getValue(pts[i + 1].x) * fXScale),
                    //     y2: (cy = getValue(pts[i + 1].y) * fYScale)
                    // });
                    pathCoords.xValues.push(round(getValue(pts[i].x) * fXScale));
                    pathCoords.yValues.push(round(getValue(pts[i].y) * fYScale));
                    pathCoords.xValues.push(cx = round(getValue(pts[i + 1].x) * fXScale));
                    pathCoords.yValues.push(cy = round(getValue(pts[i + 1].y) * fYScale));
                }
            }

        }
    } else if (commandType === 'cubicBezierTo') {

        if (command.pts) {
            pts     = command.pts;
            length  = pts.length;

            if (length % 3 === 0) {
                for (i = 0; i < length; i += 3) {

                    // pathCoords.push({
                    //     type: 'cubic',
                    //
                    //     x1: getValue(pts[i].x) * fXScale,
                    //     y1: getValue(pts[i].y) * fYScale,
                    //     x2: getValue(pts[i + 1].x) * fXScale,
                    //     y2: getValue(pts[i + 1].y) * fYScale,
                    //     x3: (cx = getValue(pts[i + 2].x) * fXScale),
                    //     y3: (cy = getValue(pts[i + 2].y) * fYScale)
                    // });
                    pathCoords.xValues.push(round(getValue(pts[i].x) * fXScale));
                    pathCoords.yValues.push(round(getValue(pts[i].y) * fYScale));
                    pathCoords.xValues.push(round(getValue(pts[i + 1].x) * fXScale));
                    pathCoords.yValues.push(round(getValue(pts[i + 1].y) * fYScale));
                    pathCoords.xValues.push(cx = round(getValue(pts[i + 2].x) * fXScale));
                    pathCoords.yValues.push(cy = round(getValue(pts[i + 2].y) * fYScale));
                }
            }
        }
    }
    collector.cx = cx;
    collector.cy = cy;

    return collector;
}

function executePathCommands(collector, command) {

    var canvasPath  = collector.canvasPath,

        cx          = collector.cx || 0,
        cy          = collector.cy || 0,
        fXScale     = collector.fXScale || 1,
        fYScale     = collector.fYScale || 1,

        getValue    = collector.getGeometryValue,

        i           = 0,
        pts         = null,
        length      = null,

        r           = 0,
        r1          = 0,
        r2          = 0,
        rpx         = 0,
        rpy         = 0,

        rxArc       = 0,
        ryArc       = 0,

        angle       = 0,
        rightDirection = false, // the direction of triangle, arrow, ...

        checkAllPoints = false, // whether all points of one command need to be checked (54004)

        c0x = 0,
        c0y = 0,
        c1x = 0,
        c1y = 0;

    // calculates the point on an arc that corresponds to the given angle (ms like)
    function calcArcPoint(rx, ry, angle) {
        var at2 = atan2(rx * sin(angle), ry * cos(angle));
        return { x: rx * cos(at2) * fXScale, y: ry * sin(at2) * fYScale };
    }

    switch (command.c) {
        case 'moveTo':
            cx = getValue(command.x) * fXScale;
            cy = getValue(command.y) * fYScale;
            canvasPath.moveTo(cx, cy);
            break;

        case 'lineTo':
            cx = getValue(command.x) * fXScale;
            cy = getValue(command.y) * fYScale;
            canvasPath.lineTo(cx, cy);
            break;

        case 'circle':
            r = getValue(command.r) * fXScale;
            cx = getValue(command.x) * fXScale;
            cy = getValue(command.y) * fYScale;
            canvasPath.pushCircle(cx, cy, r);
            break;

        case 'oval':
        case 'diamond':
        case 'triangle':
        case 'stealth':
        case 'arrow':
            r1 = getValue(command.r1);
            r2 = getValue(command.r2);

            cx = getValue(command.x);
            cy = getValue(command.y);

            // reference point for calculating the angle (of the line)
            rpx = getValue(command.rpx);
            rpy = getValue(command.rpy);

            // an optional offset, for calculation arrow direction at bezier curves
            if (command.rpxOffset) { rpx += command.rpxOffset; }
            if (command.rpyOffset) { rpy += command.rpyOffset; }

            if (rpy !== cy) { angle = atan2(abs(rpy - cy), abs(rpx - cx)); }

            switch (command.c) {
                case 'oval':
                    canvasPath.pushEllipse(cx * fXScale, cy * fYScale, r1, r2, angle);
                    break;
                case 'diamond':
                    canvasPath.pushDiamond(cx * fXScale, cy * fYScale, r1, r2, angle);
                    break;
                case 'triangle':
                    if (isRightOrDownDirection(cx, cy, rpx, rpy)) {
                        rightDirection = true; // setting the direction of the triangle
                    }
                    canvasPath.pushTriangle(cx * fXScale, cy * fYScale, r1, r2, angle, rightDirection);
                    break;
                case 'stealth':
                    if (isRightOrDownDirection(cx, cy, rpx, rpy)) {
                        rightDirection = true;  // setting the direction of the stealth arrow head
                    }
                    canvasPath.pushStealth(cx * fXScale, cy * fYScale, r1, r2, angle, rightDirection);
                    break;
                case 'arrow':
                    if (isRightOrDownDirection(cx, cy, rpx, rpy)) {
                        rightDirection = true; // setting the direction of the arrow head
                    }
                    canvasPath.pushArrow(cx * fXScale, cy * fYScale, r1, r2, angle, rightDirection);
                    break;
            }
            break;

        case 'arcTo':
            var rx    = getValue(command.wr),
                ry    = getValue(command.hr),
                stAng = getValue(command.stAng) * PI_RAD,
                swAng = getValue(command.swAng),

                // calculating new currentPoint
                startPoint  = calcArcPoint(rx, ry, stAng),
                endPoint    = calcArcPoint(rx, ry, stAng + swAng * PI_RAD),

                calcAngle = function (x, y) {
                    var x1 = rx * fXScale;
                    var y1 = ry * fYScale;
                    return (x1 > y1) ? atan2(y * x1 / y1, x) : atan2(y, x * y1 / x1);
                },

                eStAng  = calcAngle(startPoint.x, startPoint.y),
                eEndAng = calcAngle(endPoint.x, endPoint.y);

            // check if a full circle has to be drawn
            if (abs(swAng) >= 21600000) {
                swAng   = -1;
                eEndAng = eStAng + 0.1 * PI_180; // increase for one tenth of radian for better precision (#57664)
            }
            canvasPath.ellipseTo(cx, cy, rx * fXScale, ry * fYScale, eStAng, eEndAng, swAng < 0);

            cx = cx + endPoint.x - startPoint.x;
            cy = cy + endPoint.y - startPoint.y;

            rxArc = 2 * rx * fXScale;
            ryArc = 2 * ry * fYScale;

            break;

        case 'quadBezierTo':
            if (command.pts) {
                pts     = command.pts;
                length  = pts.length;

                if (length % 2 === 0) {
                    for (i = 0; i < length; i += 2) {

                        canvasPath.quadraticCurveTo(
                            getValue(pts[i].x) * fXScale,
                            getValue(pts[i].y) * fYScale,
                            cx = getValue(pts[i + 1].x) * fXScale,
                            cy = getValue(pts[i + 1].y) * fYScale
                        );
                    }
                }
            }
            break;

        case 'cubicBezierTo':
            if (command.pts) {
                pts     = command.pts;
                length  = pts.length;

                if (length % 3 === 0) {
                    for (i = 0; i < length; i += 3) {

                        canvasPath.bezierCurveTo(
                            c0x = getValue(pts[i].x) * fXScale,
                            c0y = getValue(pts[i].y) * fYScale,
                            c1x = getValue(pts[i + 1].x) * fXScale,
                            c1y = getValue(pts[i + 1].y) * fYScale,
                            cx = getValue(pts[i + 2].x) * fXScale,
                            cy = getValue(pts[i + 2].y) * fYScale
                        );
                    }
                }

                checkAllPoints = true;
            }
            break;

        case 'close':
            canvasPath.close();
            break;
    }

    collector.cx = cx;
    collector.cy = cy;

    if (cx < collector.cxMin) { collector.cxMin = cx; }
    if (cx > collector.cxMax) { collector.cxMax = cx; }
    if (cy < collector.cyMin) { collector.cyMin = cy; }
    if (cy > collector.cyMax) { collector.cyMax = cy; }

    if (checkAllPoints) {

        if (c0x < collector.cxMin) { collector.cxMin = c0x; }
        if (c0x > collector.cxMax) { collector.cxMax = c0x; }
        if (c0y < collector.cyMin) { collector.cyMin = c0y; }
        if (c0y > collector.cyMax) { collector.cyMax = c0y; }

        if (c1x < collector.cxMin) { collector.cxMin = c1x; }
        if (c1x > collector.cxMax) { collector.cxMax = c1x; }
        if (c1y < collector.cyMin) { collector.cyMin = c1y; }
        if (c1y > collector.cyMax) { collector.cyMax = c1y; }
    }

    if (rxArc) {
        if ((cx - rxArc) < collector.cxMin) { collector.cxMin = cx - rxArc; }
        if ((cx + rxArc) > collector.cxMax) { collector.cxMax = cx + rxArc; }
    }

    if (ryArc) {
        if ((cy - ryArc) < collector.cyMin) { collector.cyMin = cy - ryArc; }
        if ((cy + ryArc) > collector.cyMax) { collector.cyMax = cy + ryArc; }
    }

    return collector;
}

// overlay colors for a shaded fill color, mapped by fill mode attribute values
var SHADED_OVERLAY_MAP = {
    lighten:     { color: Color.parseJSON(Color.WHITE), alpha: 0.4 },
    lightenLess: { color: Color.parseJSON(Color.WHITE), alpha: 0.2 },
    darken:      { color: Color.parseJSON(Color.BLACK), alpha: 0.4 },
    darkenLess:  { color: Color.parseJSON(Color.BLACK), alpha: 0.2 }
};

function drawShape(app, context, snapRect, geometryAttrs, attrSet, guideCache, options) {

    if (!geometryAttrs.pathList) { return; }

    var docModel = app.getModel(),

        widthPx  = snapRect.width,
        heightPx = snapRect.height,

        avList  = geometryAttrs.avList,
        ahList  = geometryAttrs.ahList,
        gdList  = geometryAttrs.gdList,
        cxnList = geometryAttrs.cxnList,

        fillAttrs = attrSet.fill,
        lineAttrs = attrSet.line,

        targets = fillAttrs ? fillAttrs[TARGET_CHAIN_PROPERTY] : null,
        themeModel = docModel.getThemeModel(targets),

        isResizeActive          = getBooleanOption(options, 'isResizeActive', false),
        currentNode             = getObjectOption(options, 'currentNode', null),
        textureStyle            = getObjectOption(options, 'textureStyle', null),
        boundRect               = getObjectOption(options, 'boundRect', null),
        drawingUID              = getUid(currentNode),
        rotationAngle           = null,
        isBitmapFill            = fillAttrs && fillAttrs.type === 'bitmap' && !_.isEmpty(fillAttrs.bitmap),
        notRotatedWithShape     = fillAttrs && fillAttrs.bitmap && fillAttrs.bitmap.rotateWithShape === false,
        isArrowShape            = isLineEndingShape(geometryAttrs),
        preview                 = options?.preview;

    function getGeoValue(value) {
        return getGeometryValueLocal(value, snapRect, avList, gdList, guideCache);
    }

    function calcFinalPath(origPath, scaleX, scaleY) {
        return origPath.commands.reduce(executePathCommands, {
            canvasPath: context.createPath(),
            cx: 0,
            cy: 0,
            fXScale: scaleX,
            fYScale: scaleY,
            getGeometryValue: getGeoValue
        }).canvasPath;
    }

    function setBitmapFillForPathShape(textureFillStyle, drawingUID) {
        var boundWidth = boundRect ? boundRect.width : widthPx;
        var boundHeight = boundRect ? boundRect.height : heightPx;
        // store texture for performance
        if (isResizeActive) {
            if (!getCachedTextureFill(drawingUID)) { // save texture canvas pattern fill to cache with drawing id, if not there already
                setCachedTextureFill(drawingUID, textureFillStyle);
            }
        } else {
            textureCanvasPatternCache = [];
        }

        // first pass - needed for blending with fill
        geometryAttrs.pathList.forEach(function (onePath, index) {
            processOnePath(onePath, index, { isArrowShape });
        });

        context.render(function () {
            context.setGlobalComposite('source-in'); // The new shape is drawn only where both the new shape and the destination canvas overlap. Everything else is made transparent.
            if (notRotatedWithShape && rotationAngle) {
                var rectPos = max(boundWidth, boundHeight);
                var tiling = fillAttrs.bitmap.tiling;
                var nWidth, nHeight;
                context.translate(boundWidth / 2, boundHeight / 2);
                context.rotate(rotationAngle);
                context.setFillStyle(textureFillStyle);

                if (tiling) { // #58294 - for different alignments, different correction to top-left point has to be added, depending on the angle
                    if (tiling.rectAlignment === 'center') {
                        context.translate(-boundWidth / 2, -boundHeight / 2);
                    } else if (tiling.rectAlignment === 'left') {
                        nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                        nHeight = boundHeight;
                        context.translate(-nWidth / 2, -nHeight / 2);
                    } else if (tiling.rectAlignment === 'right') {
                        nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                        nHeight = boundHeight;
                        context.translate(nWidth / 2 - boundWidth, -nHeight / 2);
                    } else if (tiling.rectAlignment === 'topLeft') {
                        nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                        nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                        context.translate(-nWidth / 2, -nHeight / 2);
                    } else if (tiling.rectAlignment === 'top') {
                        nWidth = boundWidth;
                        nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                        context.translate(-nWidth / 2, -nHeight / 2);
                    } else if (tiling.rectAlignment === 'bottom') {
                        nWidth = boundWidth;
                        nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                        context.translate(-nWidth / 2, nHeight / 2 - boundHeight);
                    } else if (tiling.rectAlignment === 'topRight') {
                        nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                        nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                        context.translate(nWidth / 2 - boundWidth, -nHeight / 2);
                    } else if (tiling.rectAlignment === 'bottomRight') {
                        nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                        nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                        context.translate(nWidth / 2 - boundWidth, nHeight / 2 - boundHeight);
                    } else if (tiling.rectAlignment === 'bottomLeft') {
                        nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                        nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                        context.translate(-nWidth / 2, nHeight / 2 - boundHeight);
                    }
                    context.drawRect(2 * -rectPos, 2 * -rectPos, 4 * rectPos, 4 * rectPos, 'fill'); // cover whole canvas area with pattern fill
                } else {
                    nWidth = boundWidth * abs(cos(rotationAngle)) + boundHeight * abs(sin(rotationAngle));
                    nHeight = boundHeight * abs(cos(rotationAngle)) + boundWidth * abs(sin(rotationAngle));
                    context.translate(-nWidth / 2, -nHeight / 2);
                    context.drawRect(0, 0, nWidth, nHeight, 'fill'); // cover whole canvas area with pattern fill
                }
            } else {
                context.setFillStyle(textureFillStyle);
                context.drawRect(0, 0, boundWidth, boundHeight, 'fill');
            }
        });
        // second pass for rendering path on top, after bitmap fill is applied
        geometryAttrs.pathList.forEach(function (onePath, index) {
            processOnePath(onePath, index, { secondPass: true });
        });
    }

    function processOnePath(onePath, index, options) {
        // initialize context with proper fill attributes for the next path
        var path            = _.copy(onePath, true); // creating a deep copy of the path, so that it can be expanded
        var logWidth        = ('width' in path) ? path.width : widthPx;
        var logHeight       = ('height' in path) ? path.height : heightPx;
        var fXScale         = widthPx / logWidth;
        var fYScale         = heightPx / logHeight;
        var hasFill         = fillAttrs && (fillAttrs.type !== 'none') && (path.fillMode !== 'none');
        var hasLine         = lineAttrs && (lineAttrs.type !== 'none') && (path.isStroke !== false);
        var renderMode      = getRenderMode(hasFill, hasLine);
        var shadeMode       = path.fillMode;
        var isSecondPass    = (options && options.secondPass) || false;
        var fillStyle       = null;
        var isArrowShape    = getBooleanOption(options, 'isArrowShape', false);

        // nothing to do without valid path size
        if ((logWidth <= 0) || (logHeight <= 0) || !renderMode) { return; }

        function shadeColor(jsonColor) {
            var overlayData = shadeMode ? SHADED_OVERLAY_MAP[shadeMode] : null;
            if (!overlayData) { return jsonColor; }

            var color = Color.parseJSON(jsonColor);
            var colorDesc = color.resolve('fill', themeModel);
            var mixedDesc = color.resolveMixed(overlayData.color, overlayData.alpha, 'fill', themeModel);
            return Color.fromRgb(mixedDesc.hex, { alpha: colorDesc.a * 100000 }).toJSON();
        }

        function getShadedBitmapFill(bitmapFill) {
            var overlayColor, sumAlpha, bitmapColor, shadedFill;
            var overlayDataBitmap = SHADED_OVERLAY_MAP[shadeMode];
            if (overlayDataBitmap) {
                overlayColor = overlayDataBitmap.color.toJSON();
                sumAlpha = _.isFinite(bitmapFill.transparency) ? overlayDataBitmap.alpha * bitmapFill.transparency : overlayDataBitmap.alpha;
                bitmapColor = Color.fromRgb(overlayColor.value, { alpha: sumAlpha * 100000 }).toJSON();
                shadedFill = docModel.getCssColor(bitmapColor, 'fill', targets);
            } else {
                shadedFill = 'transparent';
            }
            return shadedFill;
        }

        /**
         * Local function to draw final path of canvas.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  - {String} [options.mode]
         *      A specific rendering mode used to override the original
         *      rendering mode of the path to be drawn (one of 'stroke',
         *      'fill', or 'all').
         */
        function drawFinalPath(options) {
            // the new converted path with resolved path commands
            var newPath = calcFinalPath(path, fXScale, fYScale);
            context.drawPath(newPath, options?.mode ?? renderMode);
        }

        /**
         * The arrow direction is wrong for very small bent and curved connectors. If the
         * width is approximately 1 px, the connectors can be displayed like simply lines.
         * In this case the direction of the arrows might be wrong (58120).
         *
         * TODO: This fix solves only some of the problems. It would be more precise to
         *       check more positionof the path to determine the arrow direction.
         *
         * @param {Object[]} localPath
         *  The path describing the arrow.
         */
        function handleSmallConnectors(localPath) {
            if (widthPx < 2 && isConnectorDrawingFrame(currentNode) && !_.isEmpty(avList)) { // only bent and curved connectors are having avList
                var rotateOp = _.findWhere(localPath.ops, { name: 'rotate' });
                var endType = lineAttrs.tailEndType && lineAttrs.tailEndType !== 'none';
                var flipAngle = (endType || isFlippedVert(currentNode) ? 270 : 90) * PI_180;
                if (rotateOp && rotateOp.args) { rotateOp.args = [flipAngle]; }
            }
        }

        if (hasFill) {
            if (fillAttrs.type === 'gradient') {
                var angle = !fillAttrs.gradient.isRotateWithShape ? getDrawingRotationAngle(docModel, currentNode) || 0 : 0;
                if (angle) {
                    // rotate the triangle (rotate the gradient not with shape) to get the new width and height
                    var drawingAttrs = docModel.drawingStyles.getElementAttributes(currentNode).drawing; // #53436
                    var rotatedAttrs = getRotatedDrawingPoints(drawingAttrs, angle);
                    logWidth = convertHmmToLength(rotatedAttrs.width, 'px', 1);
                    logHeight = convertHmmToLength(rotatedAttrs.height, 'px', 1);
                }

                // extract the JSON representation of the gradient from the fill attributes
                var jsonGradient = Gradient.getDescriptorFromAttributes(fillAttrs);

                // apply shading effect to all color stops of the gradient
                if (shadeMode) {
                    jsonGradient.color = shadeColor(jsonGradient.color);
                    jsonGradient.color2 = shadeColor(jsonGradient.color2);
                    if (_.isArray(jsonGradient.colorStops)) {
                        jsonGradient.colorStops = jsonGradient.colorStops.map(function (colorStop) {
                            colorStop = _.clone(colorStop);
                            colorStop.color = shadeColor(colorStop.color);
                            return colorStop;
                        });
                    }
                }

                var gradient    = Gradient.create(jsonGradient),
                    halfWidth   = round(logWidth / 2),
                    halfHeight  = round(logHeight / 2),
                    pathCoords  = path.commands.reduce(collectPathCoordinates, {
                        pathCoords: {
                            xValues: [halfWidth, logWidth, halfWidth, 0],
                            yValues: [0, halfHeight, logHeight, halfHeight],
                            mx: halfWidth,
                            my: halfHeight
                        },
                        cx: 0,
                        cy: 0,
                        fXScale,
                        fYScale,

                        getGeometryValue: getGeoValue

                    }).pathCoords;

                if (angle) {
                    // rotate the triangle (rotate the gradient not with shape) coords
                    if (pathCoords.xValues.length === pathCoords.yValues.length) {
                        for (var i = 0; i < pathCoords.xValues.length; i++) {
                            var x = pathCoords.xValues[i];
                            var y = pathCoords.yValues[i];
                            var rotPoint = rotatePointWithAngle(pathCoords.mx, pathCoords.my, x, y, angle);
                            pathCoords.xValues[i] = rotPoint.left;
                            pathCoords.yValues[i] = rotPoint.top;
                        }
                    }
                }

                fillStyle = docModel.getGradientFill(gradient, pathCoords, context, targets);
            } else if (fillAttrs.type === 'pattern') {
                fillStyle = createCanvasPattern(fillAttrs.pattern, shadeColor(fillAttrs.color2), shadeColor(fillAttrs.color), themeModel);
            } else if (isBitmapFill) {
                if (isSecondPass) {
                    if (shadeMode) { fillStyle = getShadedBitmapFill(fillAttrs.bitmap); }
                } else {
                    fillStyle = '#f8f8f8'; // temp light value, used for masking in applying bitmap fill
                }
            } else {
                fillStyle = docModel.getCssColor(shadeColor(fillAttrs.color), 'fill', targets);
            }
            context.setFillStyle(fillStyle);
        }

        // initialize context with proper line attributes for the next path
        var additionalPaths = hasLine ? setLineAttributes(docModel, context, lineAttrs, null, path, {
            isFirstPath: index === 0,
            isLastPath: index + 1 === geometryAttrs.pathList.length,
            isArrowShape,
            snapRect,
            avList,
            gdList,
            guideCache,
            preview
        }) : null;

        drawFinalPath();

        if (additionalPaths && (additionalPaths.length > 0)) {
            // bug 50284: no line patterns for arrows at line ends
            context.setLineStyle({ pattern: null });
            _.each(additionalPaths, function (onePath) {
                var newPath = calcFinalPath(onePath, fXScale, fYScale);
                handleSmallConnectors(newPath);
                context.drawPath(newPath, onePath.drawMode);
            });
        }
    } // end of processOnePath function

    if (isBitmapFill) {
        if (notRotatedWithShape && currentNode) {
            rotationAngle = getDrawingRotationAngle(docModel, currentNode) || 0;
            rotationAngle = ((360 - rotationAngle) % 360) * PI_180; // normalize and convert to radians
        }
        if (textureStyle) { setBitmapFillForPathShape(textureStyle, drawingUID); }
    } else {
        geometryAttrs.pathList.forEach(function (onePath, index) {
            processOnePath(onePath, index, { isArrowShape });
        });
    }

    // store geometry data used to calculate snap or adjustment points later
    if (!preview && !isResizeActive && (cxnList || ahList)) {
        currentNode.data('shapeGeoData', { avList, ahList, gdList, cxnList });
    }
} // end of drawShape function

/**
 * Create adjustment handles or recalculate positions for existing handles
 *
 * @param {jQuery} currentNode
 *  The current node to be selected, as DOM node or jQuery object.
 *
 * @param {Array} avList
 *
 * @param {Array} gdList
 *
 * @param {Array} ahList
 *
 * @param {Number} zoomFactor
 *  Zoom factor of the document
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Number} [options.width]
 *   Current pixel width of the currentNode
 *  @param {Number} [options.height]
 *   Current pixel height of the currentNode
 */
function createAdjustmentHandles(currentNode, avList, gdList, ahList, zoomFactor, options) {
    var width = getNumberOption(options, 'width', currentNode.width());
    var height = getNumberOption(options, 'height', currentNode.height());
    var snapRect = new Rectangle(0, 0, width, height);
    var selectionNode = currentNode.find('.' + SELECTION_CLASS);
    var adjPointsParent = selectionNode.find('.adj-points-container');

    if (!adjPointsParent.length) {
        adjPointsParent = $('<div class="adj-points-container">');
        selectionNode.append(adjPointsParent);
    } else {
        adjPointsParent.empty();
    }
    _.each(ahList, function (ah, ind) {
        var x = getGeometryValueLocal(ah.x, snapRect, avList, gdList, {});
        var y = getGeometryValueLocal(ah.y, snapRect, avList, gdList, {});
        x = _.isFinite(x) ? x - 5 : 0;
        y = _.isFinite(y) ? y - 5 : 0;
        var scaleFactor = (TOUCH_DEVICE ? 2 : 1) / zoomFactor;
        var adjHandleNode = $('<div class="adj-handle">').css({ left: x, top: y, position: 'absolute', transform: 'scale(' + scaleFactor + ')' });
        adjHandleNode.data({ cxnObj: { ah, gdList, avList }, ahInd: ind });
        adjPointsParent.append(adjHandleNode);
    });
}

function createSnapPointsLocal(currentNode, snapRect, avList, gdList, cxnList) {
    var snapPointsParent = currentNode.find('.snap-points-container');
    if (!snapPointsParent.length) {
        snapPointsParent = $('<div class="snap-points-container">');
        currentNode.append(snapPointsParent);
    } else {
        snapPointsParent.empty();
    }
    _.each(cxnList, function (cxn, ind) {
        var x = getGeometryValueLocal(cxn.x, snapRect, avList, gdList, {});
        var y = getGeometryValueLocal(cxn.y, snapRect, avList, gdList, {});
        x = _.isFinite(x) ? x - 5 : 0;
        y = _.isFinite(y) ? y - 5 : 0;
        var snapPointNode = $('<div class="snap-point">').css({ left: x, top: y, position: 'absolute' });
        var approachAngle = getGeometryValueLocal(cxn.ang, snapRect, avList, gdList, {}) / 60000;
        snapPointNode.data({ cxnObj: { cxn, gdList, avList }, approachAng: approachAngle, cxnInd: ind });

        snapPointsParent.append(snapPointNode);
    });
}

function getGeometryValueLocal(value, snapRect, avList, gdList, guideCache, maxGdIndex) {
    if (typeof value === 'number') {
        return value;
    }
    if (value in guideCache) {
        return guideCache[value];
    }
    switch (value) {
        // 3 x 360deg / 4 = 270deg
        case '3cd4':
            return 270 * 60000;
        // 3 x 360deg / 8 = 135deg
        case '3cd8':
            return 135 * 60000;
        // 5 x 360deg / 8 = 225deg
        case '5cd8':
            return 225 * 60000;
        // 7 x 360deg / 8 = 315deg
        case '7cd8':
            return 315 * 60000;
        // bottom
        case 'b':
            return snapRect.top + snapRect.height - 1;
        // 360deg / 2 = 180deg
        case 'cd2':
            return 180 * 60000;
        // 360deg / 4 = 90deg
        case 'cd4':
            return 90 * 60000;
        // 360deg / 8 = 45deg
        case 'cd8':
            return 45 * 60000;
        // horizontal center
        case 'hc':
            return snapRect.left + snapRect.width / 2;
        // height
        case 'h':
            return snapRect.height;
        // height / 2
        case 'hd2':
            return snapRect.height / 2;
        // height / 3
        case 'hd3':
            return snapRect.height / 3;
        // height / 4
        case 'hd4':
            return snapRect.height / 4;
        // height / 5
        case 'hd5':
            return snapRect.height / 5;
        // height / 6
        case 'hd6':
            return snapRect.height / 6;
        // height / 8
        case 'hd8':
            return snapRect.height / 8;
        // left
        case 'l':
            return snapRect.left;
        // long side
        case 'ls':
            return max(snapRect.width, snapRect.height);
        // right
        case 'r':
            return snapRect.left + snapRect.width - 1;
        // short side
        case 'ss':
            return min(snapRect.width, snapRect.height);
        // short side / 2
        case 'ssd2':
            return min(snapRect.width, snapRect.height) / 2;
        // short side / 4
        case 'ssd4':
            return min(snapRect.width, snapRect.height) / 4;
        // short side / 6
        case 'ssd6':
            return min(snapRect.width, snapRect.height) / 6;
        // short side / 8
        case 'ssd8':
            return min(snapRect.width, snapRect.height) / 8;
        // short side / 16
        case 'ssd16':
            return min(snapRect.width, snapRect.height) / 16;
        // short side / 32
        case 'ssd32':
            return min(snapRect.width, snapRect.height) / 32;
        // top
        case 't':
            return snapRect.top;
        // vertical center
        case 'vc':
            return snapRect.top + snapRect.height / 2;
        // width
        case 'w':
            return snapRect.width;
        // width / 2
        case 'wd2':
            return snapRect.width / 2;
        case 'wd3':
            return snapRect.width / 3;
        // width / 4
        case 'wd4':
            return snapRect.width / 4;
        // width / 5
        case 'wd5':
            return snapRect.width / 5;
        // width / 6
        case 'wd6':
            return snapRect.width / 6;
        // width / 8
        case 'wd8':
            return snapRect.width / 8;
        // width / 10
        case 'wd10':
            return snapRect.width / 10;
        // width / 32
        case 'wd32':
            return snapRect.width / 32;
    }
    // the value is not a constant value, next check if this is a guide name
    if (avList && value in avList) {
        return avList[value];
    }
    if (gdList) {
        if (typeof maxGdIndex !== 'number') { maxGdIndex = 1000; }
        for (var i = 0; i < gdList.length; i += 1) {
            if (gdList[i].name === value) {
                if (i > maxGdIndex) {
                    return 0.0;
                }
                var result = calcGeometryValue(gdList[i], snapRect, avList, gdList, guideCache, i - 1);
                guideCache[value] = result;
                return result;
            }
        }
    }

    return parseInt(value, 10) || 0;
}

function calcGeometryValue(gdEntry, snapRect, avList, gdList, guideCache, maxGdIndex) {

    // formulas with one parameter
    var p0 = getGeometryValueLocal(gdEntry.p0, snapRect, avList, gdList, guideCache, maxGdIndex);
    switch (gdEntry.op) {
        // Absolute value: "abs x" := |x|
        case 'abs': return abs(p0);
        // Square root: "sqrt x" := sqrt(x)
        case 'sqrt': return sqrt(p0);
        // Literal value: "val x" := x
        case 'val': return p0;
    }

    // formulas with two parameters
    var p1 = getGeometryValueLocal(gdEntry.p1, snapRect, avList, gdList, guideCache, maxGdIndex);
    switch (gdEntry.op) {
        // Arcus tangent: "at2 x y" := arctan2(y,x)
        case 'at2': return atan2(p1, p0) / PI_RAD;
        // Cosine: "cos x y" := x * cos(y)
        case 'cos': return p0 * cos(p1 * PI_RAD);
        // Maximum value: "max x y" := max(x,y)
        case 'max': return max(p0, p1);
        // Minimum value: "min x y" := min(x,y)
        case 'min': return min(p0, p1);
        // Sine: "sin x y" := x * sin(y)
        case 'sin': return p0 * sin(p1 * PI_RAD);
        // Tangent: "tan x y" = x * tan(y)
        case 'tan': return p0 * tan(p1 * PI_RAD);
    }

    // formulas with three parameters
    var p2 = getGeometryValueLocal(gdEntry.p2, snapRect, avList, gdList, guideCache, maxGdIndex);
    switch (gdEntry.op) {
        // Multiply and divide: "*/ x y z" := x * y / z
        case '*/': return p0 * p1 / p2;
        // Add and subtract: "+- x y z" := x + y - z
        case '+-': return p0 + p1 - p2;
        // Add and divide: "+/ x y z" := (x + y) / z
        case '+/': return (p0 + p1) / p2;
        // If-then-else: "?: x y z" := if (x > 0) then y else z
        case '?:': return (p0 > 0) ? p1 : p2;
        // Cosine of arcus tangent: "cat2 x y z" := x * cos(arctan(z,y))
        case 'cat2': return p0 * cos(atan2(p2, p1));
        // Modulo: "mod x y z" := sqrt(x^2 + y^2 + z^2)
        case 'mod': return sqrt(p0 * p0 + p1 * p1 + p2 * p2);
        // Pin to: "pin x y z" := if (y < x) then x else if (y > z) then z else y
        case 'pin': return math.clamp(p1, p0, p2);
        // Sine of arcus tangent: "sat2 x y z" := x * sin(arctan(z,y))
        case 'sat2': return p0 * sin(atan2(p2, p1));
    }

    globalLogger.warn('DrawingFrame.calcGeometryValue(): invalid operation: ', gdEntry.op);
    return 0;
}

/**
 * Renders a preview of the specified predefined shape object into a canvas
 * that already exists in or will be embedded into the passed DOM element.
 *
 * @param {EditApplication} docApp
 *  The application instance containing the passed frame node.
 *
 * @param {jQuery|HTMLElement} frameNode
 *  The DOM container element for the canvas element to be rendered into.
 *  If this container element is empty, a new canvas element will be
 *  created and inserted automatically.
 *
 * @param {Object} shapeSize
 *  The total size of the shape, in pixels, in the properties 'width' and
 *  'height'.
 *
 * @param {String} presetShapeId
 *  The identifier for a predefined shape type.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Rectangle} [options.snapRect]
 *    The location of the shape in the bounding box defined by the size passed
 *    with the parameter "shapeSize". If omitted, the shape will be located in
 *    the entire bounding box.
 *  - {PtShapeAttributeSet|PtConnectorAttributeSet} [options.attrs]
 *    Additional formatting attributes to be merged over the attributes
 *    returned by `createDefaultShapeAttributeSet()` according to the specified
 *    shape type.
 *  - {boolean} [options.flipH=false]
 *    If set to `true`, the shape will be flipped horizontally.
 *  - {boolean} [options.flipV=false]
 *      If set to `true`, the shape will be flipped vertically.
 *  - {string} [options.lineColor]
 *    A CSS color to be used to render the shape stroke, instead of the color
 *    contained in the attribute set.
 *  - {boolean} [options.noFill=false]
 *    If set to `true`, the shape will be rendered without fill effect.
 *  - {boolean} [options.transparent=false]
 *    If set to `true`, the shape will be rendered semi-transparently.
 *  - {boolean} [options.drawIconLabel=false]
 *    Whether to render the predefined icon label into the shape center.
 *  - {string} [options.iconLabelFont]
 *    The font style of the icon label. This option will have any effect only
 *    if the option "drawIconLabel" has been set.
 *
 * @returns {JQuery | void} // TODO: check-void-return
 *  The DOM canvas node, as jQuery element.
 */
export function drawPreviewShape(docApp, frameNode, shapeSize, presetShapeId, options) {

    // geometry data for the specified shape type
    const presetData = getPresetShape(presetShapeId);
    if (!presetData) { return; }

    // location of the canvas (always at zero offset)
    const boundRect = new Rectangle(0, 0, shapeSize.width, shapeSize.height);
    // location of the shape in the canvas
    const snapRect = options?.snapRect ?? boundRect;

    // the canvas element (create a new element on demand)
    let $canvas = $(frameNode).find('>canvas').first();
    if ($canvas.length === 0) {
        $canvas = $(`<canvas class="${CANVAS_CLASS}" style="position:absolute;">`).appendTo(frameNode);
    }

    // set flipping styles to the canvas element
    const scaleX = options?.flipH ? -1 : 1;
    const scaleY = options?.flipV ? -1 : 1;
    $canvas.css('transform', 'scaleX(' + scaleX + ') scaleY(' + scaleY + ')');

    // set the canvas element to semi-transparency if specified
    $canvas.css('opacity', options?.transparent ? '0.6' : '');

    // create the formatting attributes for the shape
    const shapeAttrSet = createDefaultShapeAttributeSet(docApp.docModel, presetShapeId);
    if (!shapeAttrSet) { return; }

    if (options?.attrs) {
        docApp.docModel.drawingStyles.extendAttrSet(shapeAttrSet, options.attrs);
    }

    // the rendering helper for the canvas element in the passed container node
    const canvas = new Canvas({ node: $canvas });
    initializeCanvasSize(canvas, 'shape', boundRect, getLineWidthPx(shapeAttrSet.line));

    // remove fill attributes if specified
    if (options?.noFill && shapeAttrSet.fill) {
        shapeAttrSet.fill.type = 'none';
    }

    // add custom line color
    if (options?.lineColor) {
        shapeAttrSet.line.color = Color.parseCSS(options.lineColor)?.toJSON();
    }

    // render the shape into the canvas element
    canvas.render(context => {
        drawShape(docApp, context, snapRect, presetData.shape, shapeAttrSet, {}, { preview: true });
        if (presetData.iconLabel && options?.drawIconLabel) {
            const fontStyle = { align: 'center', baseline: 'middle' };
            if (options.iconLabelFont) { fontStyle.font = options.iconLabelFont; }
            context.setFillStyle(options.lineColor ?? resolveCssVar("--text"));
            context.setFontStyle(fontStyle);
            context.drawText(presetData.iconLabel, snapRect.centerX(), snapRect.centerY(), 'fill');
        }
    });

    // destroy the wrapper for the canvas node
    canvas.destroy();
    return $canvas;
}

// draws the border and the filling to the canvas
function drawCanvasBorderAndFilling(app, currentNode, lineAttrs, fillAttrs) {

    var // the document model
        docModel = app.getModel(),
        // whether the canvas needs to contain a filling
        hasFill = fillAttrs && (fillAttrs.type !== 'none'),
        // whether the canvas needs to contain a line
        hasLine = lineAttrs && (lineAttrs.type !== 'none'),
        // border width
        lineWidth = hasLine ? getLineWidthPx(lineAttrs) : 0,
        // the half size of the border
        halfBorderSize = lineWidth / 2, // TODO: Handling of half pixels required?
        // dimensions of the canvas element, special behavior for text frames. Their border covers their boundary
        cWidth = currentNode.first().width(),
        cHeight = currentNode.first().height(),
        // whether this is a canvas for a bitmap
        typeOfFill = (hasFill && fillAttrs.type) ? fillAttrs.type : null,
        // the canvas wrapper of the border node
        canvas = initializeCanvas(app, currentNode, new Rectangle(0, 0, cWidth, cHeight), lineWidth, { typeOfFill }),
        // the render mode for the canvas
        renderMode = getRenderMode(hasFill, hasLine);

    canvas.render(function (context, width, height) {

        var drawX = halfBorderSize,
            drawY = halfBorderSize,
            drawW = width - lineWidth,
            drawH = height - lineWidth;

        let imagePromise = null; // DOCS-5085

        if (hasLine) {
            var color = docModel.getCssColor(lineAttrs.color, 'line', lineAttrs[TARGET_CHAIN_PROPERTY]),
                pattern = getBorderPattern(lineAttrs.style, lineWidth),
                isImageDrawing = getDrawingType(currentNode) === 'image';

            context.setLineStyle({ style: color, width: lineWidth, pattern });

            if (!isImageDrawing) {
                drawW -= halfBorderSize;
                drawH -= halfBorderSize;
            }
        }

        if (hasFill) {
            var targets = fillAttrs[TARGET_CHAIN_PROPERTY];

            if (fillAttrs.type === 'gradient') {
                var gradient = Gradient.create(Gradient.getDescriptorFromAttributes(fillAttrs));
                var drawingRect = { width: currentNode.outerWidth(true), height: currentNode.outerHeight(true) };
                context.setFillStyle(docModel.getGradientFill(gradient, drawingRect, context, targets));
            } else if (fillAttrs.type === 'pattern') {
                context.setFillStyle(createCanvasPattern(fillAttrs.pattern, fillAttrs.color2, fillAttrs.color, docModel.getThemeModel(targets)));
            } else if (fillAttrs.type === 'bitmap') {
                if (fillAttrs.bitmap && fillAttrs.bitmap.imageUrl) {
                    var serverURL = getFileUrl(app, fillAttrs.bitmap.imageUrl, { uid: false });
                    imagePromise = app.createImageNode(serverURL, { timeout: 15000 }).done(function (imgNode) {
                        if (_.isNumber(fillAttrs.bitmap.transparency)) {
                            context.setGlobalAlpha(1 - fillAttrs.bitmap.transparency);
                        }
                        insertHiddenNodes(imgNode);
                        context.clearRect(lineWidth, lineWidth, drawW - lineWidth, drawH - lineWidth);
                        context.drawImage(imgNode, { left: lineWidth, top: lineWidth, width: drawW - lineWidth, height: drawH - lineWidth });
                        app.destroyImageNodes(imgNode); // remove image node again
                        context.setGlobalAlpha(1); // no transparency for the border
                    });
                    hasFill = false; // the filling is handled by drawImage after loading the image
                    renderMode = getRenderMode(hasFill, hasLine); // overwriting a previous value for the following 'drawRect' for the border
                    context.clearRect(0, 0, drawW + 2, drawH + 2); // 2 pixel more for different border thickness
                }
            } else {
                context.setFillStyle(docModel.getCssColor(fillAttrs.color, 'fill', targets));
            }
        }

        if (hasLine || hasFill) {
            context.drawRect(drawX, drawY, drawW, drawH, renderMode);
        }

        return imagePromise;
    });

    if (app.isSpreadsheetApp()) {
        //TODO: should be checked from outside!
        setCanvasDynHeight(currentNode);
    }
}

// clears the canvas
function clearCanvas(app, currentNode) {

    var // the border canvas node inside the drawing frame
        canvasNode = getCanvasNode(currentNode),
        // the canvas wrapper of the border node
        canvas = null;

    if (canvasNode.length > 0) {
        canvas = new Canvas({ classes: CANVAS_CLASS, node: canvasNode });
        canvas.initialize({ width: canvasNode.width(), height: canvasNode.height() });
    }
}

/**
 * Returns rotation angle of given drawing node.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {Number|null}
 *  Angle of rotation, or null if no rotation is set.
 */
export function getDrawingRotationAngle(docModel, drawingNode) {
    var attrs = null;
    var rotation = null;
    if (isRotatedDrawingNode(drawingNode)) {
        // first trying to fetch direct attributes of the element
        attrs = getExplicitAttributeSet(drawingNode);
        rotation = (attrs && attrs.drawing && attrs.drawing.rotation) || null;
        if (!rotation) {
            // if not fetched, get from inherited attributes
            attrs = docModel.drawingStyles.getElementAttributes(drawingNode);
            rotation = (attrs && attrs.drawing && attrs.drawing.rotation) || null;
        }
    }
    return rotation;
}

/**
 * Sets the value of the CSS attribute 'transform' for the passed rotation
 * angle and flipping flags.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame to be updated. If this object is a
 *  jQuery collection, uses the first DOM node it contains.
 *
 * @param {Number} degrees
 *  The rotation angle, in degrees.
 *
 * @param {Boolean} flipH
 *  Whether the drawing frame will be flipped horizontally.
 *
 * @param {Boolean} flipV
 *  Whether the drawing frame will be flipped vertically.
 */
export function setCssTransform(drawingFrame, degrees, flipH, flipV) {
    $(drawingFrame).css('transform', getCssTransform(degrees, flipH, flipV));
}

/**
 * Updates the value of the CSS attribute 'transform' for the passed
 * rotation angle, and the current flipping flags of the drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame to be updated. If this object is a
 *  jQuery collection, uses the first DOM node it contains.
 *
 * @param {Number} degrees
 *  The rotation angle, in degrees.
 */
export function updateCssTransform(drawingFrame, degrees) {
    var flipH = isFlippedHorz(drawingFrame);
    var flipV = isFlippedVert(drawingFrame);
    setCssTransform(drawingFrame, degrees, flipH, flipV);
}

/**
 * Normalizes pixel coordinates for move of rotated drawings.
 *
 * @param {Number} shiftX
 *  Original (from event) move shift in x axis.
 *
 * @param {Number} shiftY
 *  Original (from event) move shift in y axis.
 *
 * @param {Number} angle
 *  Rotation angle of the drawing object in degrees.
 *
 * @param {Boolean} flipH
 *  Whether or not drawing object is flipped (mirrored) horizontally.
 *
 * @param {Boolean} flipV
 *  Whether or not drawing object is flipped (mirrored) vertically.
 *
 * @returns {Object} normalized x and y values.
 */
export function getNormalizedMoveCoordinates(shiftX, shiftY, angle, flipH, flipV) {

    var rad = _.isNumber(angle) ? (angle * PI_180) : 0;

    if (rad) {
        var tempX = shiftX * cos(rad) + shiftY * sin(rad);
        var tempY = shiftY * cos(rad) - shiftX * sin(rad);
        shiftX = tempX;
        shiftY = tempY;
    }

    return {
        x: flipH ? -shiftX : shiftX,
        y: flipV ? -shiftY : shiftY
    };
}

/**
 * Normalizes pixel coordinates for resize of rotated drawings.
 *
 * @param {Number} deltaXo
 *  Original (from event) delta value of resized shape in x axis.
 *
 * @param {Number} deltaYo
 *  Original (from event) delta value of resized shape in y axis.
 *
 * @param {Boolean} useX
 *  Whether or not x resize is used.
 *
 * @param {Boolean} useY
 *  Whether or not y resize is used.
 *
 * @param {Number} scaleX
 *  If x value is scaled (inverted). Can be 1 or -1.
 *
 * @param {Number} scaleY
 *  If y value is scaled (inverted). Can be 1 or -1.
 *
 * @param {Number} angle
 *  Rotation angle of the drawing object
 *
 *
 * @returns {Object} normalized x and y values.
 */
export function getNormalizedResizeDeltas(deltaXo, deltaYo, useX, useY, scaleX, scaleY, angle) {
    var localX = null;
    var localY = null;
    var rad = _.isNumber(angle) ? (angle * PI_180) : null;

    if (rad) {
        localX = useX ? (deltaXo * cos(rad) + deltaYo * sin(rad)) * scaleX : 0;
        localY = useY ? (deltaYo * cos(rad) - deltaXo * sin(rad)) * scaleY : 0;
    } else {
        localX = useX ? deltaXo * scaleX : 0;
        localY = useY ? deltaYo * scaleY : 0;
    }

    return { x: localX, y: localY };
}

/**
 * Normalizes offset of rotated drawings. Returns object of top and left offset properties.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {HTMLElement|jQuery} moveBox
 *  Helper move box frame node used on move. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {Object} left and top calculated values.
 */
export function normalizeMoveOffset(docModel, drawingNode, moveBox) {
    var $drawingNode = $(drawingNode);
    var moveBoxOffset = moveBox.offset();
    var moveBoxOffsetLeft = moveBoxOffset.left;
    var moveBoxOffsetTop = moveBoxOffset.top;
    var angle = getDrawingRotationAngle(docModel, drawingNode);
    var tLeft, tTop, dLeft, dTop;
    var flipH = isFlippedHorz(drawingNode);
    var flipV = isFlippedVert(drawingNode);
    var drawingNodeOffset = null;

    if (_.isNumber(angle)) {
        $drawingNode.css('transform', ''); // reset to def to get values
        drawingNodeOffset = $drawingNode.offset();
        tLeft = drawingNodeOffset.left;
        tTop = drawingNodeOffset.top;
        setCssTransform($drawingNode, angle, flipH, flipV);

        drawingNodeOffset = $drawingNode.offset();
        dLeft = tLeft - drawingNodeOffset.left;
        dTop = tTop - drawingNodeOffset.top;

        moveBoxOffsetLeft += dLeft;
        moveBoxOffsetTop += dTop;
    }
    return { left: moveBoxOffsetLeft, top: moveBoxOffsetTop };
}

/**
 * Normalize resize offset of rotated drawings. Returns object of top and left offset properties,
 * and sendMoveOperation flag, to send appropriate move operations if drawing is rotated and needs to be translated to the position.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {HTMLElement|jQuery} moveBox
 *  Helper move box frame node used on resize. If this object is a jQuery
 *  collection, uses the first DOM node it contains.,
 *
 * @param {Boolean} useTop
 *  If top edge is used during resize.
 *
 * @param {Boolean} useLeft
 *  If left edge is used during resize.
 *
 * @returns {Object} left and top calculated values, plus flag for sending move operation or not.
 *
 */
export function normalizeResizeOffset(docModel, drawingNode, moveBox, useTop, useLeft) {
    var moveBoxOffset, moveBoxOffsetTop, moveBoxOffsetLeft;
    var sendMoveOperation = false;
    var angle = getDrawingRotationAngle(docModel, drawingNode);
    var isFlipH = isFlippedHorz(drawingNode);
    var isFlipV = isFlippedVert(drawingNode);

    if (_.isNumber(angle)) {
        if (isFlipH !== isFlipV) { angle = 360 - angle; }
        moveBox.css({ transform: 'rotate(' + (-angle) + 'deg)' });
        moveBoxOffset = moveBox.offset();
        moveBoxOffsetLeft = moveBoxOffset.left;
        moveBoxOffsetTop = moveBoxOffset.top;
        moveBox.css({ transform: '' });

        sendMoveOperation = true;
    } else {
        moveBoxOffset = moveBox.offset();
        moveBoxOffsetLeft = moveBoxOffset.left;
        moveBoxOffsetTop = moveBoxOffset.top;

        if ((!useTop && isFlipV) || (!useLeft && isFlipH)) { sendMoveOperation = true; }
    }

    return { left: moveBoxOffsetLeft, top: moveBoxOffsetTop, sendMoveOperation };
}

/**
 * Returns difference in top, left or bottom positions of original drawing node, and move box, on resize finalize.
 * It also normalizes position values against rotation angle or flips.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {HTMLElement|jQuery} moveBox
 *  Helper move box frame node used on resize. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Number|Null} angle
 *  Rotation angle of drawing, in degrees.
 *
 * @param {Boolean} [isFlipH]
 *  If passed, determines if shape is flipped horizontally.
 *
 * @param {Boolean} [isFlipV]
 *  If passed, determines if shape is flipped vertically.
 *
 * @returns {Object} left, top and bottom difference values.
 */
export function getPositionDiffAfterResize(drawingNode, moveBox, angle, isFlipH, isFlipV) {
    var $drawingNode = $(drawingNode);
    var drawingNodeRect = null;
    var moveBoxRect = null;
    if (typeof isFlipH !== 'boolean') { isFlipH = isFlippedHorz(drawingNode); }
    if (typeof isFlipV !== 'boolean') { isFlipV = isFlippedVert(drawingNode); }

    if (_.isFinite(angle) && angle !== 0) {
        $drawingNode.css('transform', ''); // reset to def to get values
        drawingNodeRect = $drawingNode[0].getBoundingClientRect();
        setCssTransform($drawingNode, angle, isFlipH, isFlipV);
        if (isFlipH !== isFlipV) { angle = 360 - angle; }
        moveBox.css('transform', 'rotate(' + (-angle) + 'deg)');
        moveBoxRect = moveBox[0].getBoundingClientRect();
        moveBox.css('transform', '');
    } else {
        drawingNodeRect = $drawingNode[0].getBoundingClientRect();
        moveBoxRect = moveBox[0].getBoundingClientRect();
    }
    return { leftDiff: drawingNodeRect.left - moveBoxRect.left, topDiff: drawingNodeRect.top - moveBoxRect.top, bottomDiff: drawingNodeRect.bottom - moveBoxRect.bottom };
}

export function setCanvasDynHeight(drawingFrame) {
    var canvasNode = getCanvasNode(drawingFrame);
    if (!canvasNode.length) { return; }

    var position = canvasNode.position();
    // css calc is used to keep canvas in its frame while zooming
    canvasNode.css({ width: 'calc(100% - ' + (2 * position.left) + 'px)',  height: 'calc(100% - ' + (2 * position.top) + 'px)' });
}

// selection --------------------------------------------------------------

/**
 * Returns whether the specified drawing frame is currently selected.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the specified drawing frame is selected.
 */
export function isSelected(drawingFrame) {
    return isSelectedNode(drawingFrame);
}

/**
 * Creates or updates additional nodes displayed while a drawing frame is
 * selected.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Object} [options]
 *  A map with options to control the appearance of the selection. The
 *  following options are supported:
 *  - {Boolean} [options.movable=false]
 *      If set to true, mouse pointer will be changed to a movable pointer
 *      while the mouse hovers the drawing.
 *  - {Boolean} [options.resizable=false]
 *      If set to true, mouse pointer will be changed to an appropriate
 *      resize pointer while the mouse hovers a resize handle.
 *  - {Boolean} [options.rotatable=false]
 *      If set to true, mouse pointer will be changed to an appropriate
 *  @param {Boolean} [options.adjustable=false]
 *      If set to true, additional adjustment points
 *       for shape modification will be appended.
 *  - {Number} [options.scaleHandles=1]
 *      the usable handles are scaled or themselves
 *      (without changing the size of the whole selection)
 *  - {Number} [options.rotation=0]
 *      The rotation angle of the drawing frame.
 *  - {Number} [options.zoomValue=100]
 *      The actual zoom value of the application.
 *  - {Boolean} [options.isMultiSelection=false]
 *      If selection contains of only one or more selected drawings.
 *
 * @returns {jQuery}
 *  The selection root node contained in the drawing frame, as jQuery
 *  object.
 */
export function drawSelection(drawingFrame, options) {

    // restrict to a single node
    drawingFrame = $(drawingFrame).first();

    // whether the two point selection for specified drawings can be used
    const twoPointShape = !!isTwoPointShape(drawingFrame); // toggleClass accepts Boolean only, no truthy/falsy values. (#54938)

    // create new selection nodes if missing
    if (!isSelected(drawingFrame)) {

        // create a new selection node
        const selectionNode = createDiv(SELECTION_CLASS);

        // create rotate handle (in the DOM before the resize handlers, DOCS-3479)
        if (options?.rotatable) {
            const rotateHandle = selectionNode.appendChild(createDiv("rotate-handle"));
            rotateHandle.appendChild(createSpan(ROTATE_ANGLE_HINT_CLASSNAME));
        }

        // create the border nodes
        const bordersNode = selectionNode.appendChild(createDiv("borders"));
        for (const pos of ["t", "b", "l", "r"]) {
            bordersNode.appendChild(createDiv({ dataset: { pos } }));
        }

        // create resizer handles
        const resizersNode = selectionNode.appendChild(createDiv("resizers"));
        let drawingResizers;
        if (twoPointShape) { // finding the required resize handler nodes for connector drawings
            const contentNode = getContentNode(drawingFrame);
            const isHorzFlip = isFlippedHorz(contentNode);
            const isVertFlip = isFlippedVert(contentNode);
            drawingResizers = (isHorzFlip !== isVertFlip) ? ["tr", "bl"] : ["br", "tl"];
        } else {
            drawingResizers = ["t", "tr", "r", "br", "b", "bl", "l", "tl"];
        }
        for (const pos of drawingResizers) {
            resizersNode.appendChild(createDiv({ dataset: { pos } }));
        }

        // create the tracker node
        selectionNode.appendChild(createDiv(TRACKER_CLASS));

        drawingFrame.append(selectionNode);
    }

    // update state classes at the drawing frame
    drawingFrame
        .addClass(SELECTED_CLASS)
        .toggleClass('movable', !!options?.movable)
        .toggleClass('resizable', !!options?.resizable)
        .toggleClass('adjustable', !!options?.adjustable)
        .toggleClass('rotatable', !!options?.rotatable)
        .toggleClass('two-point-shape', twoPointShape);

    updateScaleHandles(drawingFrame, options);
    updateResizersMousePointers(drawingFrame, options?.rotation || 0);

    // create adjustment handle points if is single drawing selection
    const geoData = drawingFrame.data('shapeGeoData');
    if (geoData && !options?.isMultiSelection && options?.adjustable) {
        const zoomFactor = (options?.zoomValue ?? 100) / 100;
        createAdjustmentHandles(drawingFrame, geoData.avList, geoData.gdList, geoData.ahList, zoomFactor);
    }

    // increasing resilience, avoiding invisible selection
    drawingFrame.removeClass(ROTATING_STATE_CLASSNAME);

    return getSelectionNode(drawingFrame);
}

/**
 * updates the additional nodes displayed while a drawing frame is selected.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Object} [options]
 *  A map with options to control the appearance of the selection. The
 *  following options are supported:
 *  - {Number} [options.scaleHandles=1]
 *      the usable handles are scaled or themselves
 *      (without changing the size of the whole selection)
 *      range is 1 - 3
 */
export function updateScaleHandles(drawingFrame, options) {
    var
        zoomValue     = getNumberOption(options, 'zoomValue', 100),
        scaleHandles  = getNumberOption(options, 'scaleHandles', 1, 1, 3), // scaleHandles for setting fontsize to scale inner elements

        selectionNode = getSelectionNode(drawingFrame);

    if (zoomValue >= 100) {
        selectionNode.attr('data-zoom-value', zoomValue); // - new css rule based approach for reasonable selection rendering for "zoom in".
    }
    selectionNode.css('font-size', scaleHandles + 'rem'); // - the already working "root element width" based approach for "zoo out".
}

/**
 * Updates mouse pointers for the resizers of the drawing frame, according to the rotation angle of the drawing.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Number} angle
 *  Rotation angle of the drawing, in degrees.
 */
export function updateResizersMousePointers(drawingFrame, angle) {
    var resizeHandles, shift;
    var isFlipH = isFlippedHorz(drawingFrame);
    var isFlipV = isFlippedVert(drawingFrame);
    var xorFlip = isFlipH !== isFlipV;
    var pointersClockwise = ['n-resize', 'ne-resize', 'e-resize', 'se-resize', 's-resize', 'sw-resize', 'w-resize', 'nw-resize'];
    var pointersCounterCw = ['n-resize', 'nw-resize', 'w-resize', 'sw-resize', 's-resize', 'se-resize', 'e-resize', 'ne-resize'];
    var pointers = xorFlip ? pointersCounterCw : pointersClockwise;

    angle = _.isNumber(angle) ? angle : 0;

    if (xorFlip) { angle = 360 - angle; }
    angle += 22.5; // start offset
    angle = math.modulo(angle, 360);
    shift = floor(angle / 45); // 8 handles makes 8 areas with 45 degrees each
    resizeHandles = drawingFrame.data('selection') ? drawingFrame.data('selection').find('.resizers').children() : drawingFrame.find('.resizers').children();
    _.each(resizeHandles, function (handle, index) {
        var pointerInd = (index + shift) % pointers.length;
        $(handle).css('cursor', pointers[pointerInd]);
    });
}

/**
 * Removes the selection node from the specified drawing frame.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.keepDraggingActive=false]
 *      Whether an active dragging must be kept, when the selection is removed.
 */
export function clearSelection(drawingFrame, options) {
    let removeClasses = SELECTED_CLASS + ' movable resizable';
    if (!options?.keepDraggingActive) { removeClasses += " activedragging"; }
    $(drawingFrame).first().removeClass(removeClasses);
    getSelectionNode(drawingFrame).remove();
    removeDrawingSelection(drawingFrame);
    // always disconnect tracking observer
    $(drawingFrame).data("dispatcher")?.disconnect();
    $(drawingFrame).data("dispatcher", null);
}

/**
 * Helper function to set the size, position and rotation of a selection of a drawing node.
 * This handles selection objects that are located in the selection overlay node.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node, whose selection size, position and rotation shall be updated.
 */
export function updateSelectionDrawingSize(drawingNode) {
    var drawing = $(drawingNode);
    if (drawing.data('selection')) {
        drawing.data('selection').css({ transform: drawing.css('transform'), left: drawing.css('left'), top: drawing.css('top'), width: drawing.width(), height: drawing.height() });
    }
}

/**
 * Helper function to remove the selection of a drawing node. This handles selection
 * objects that are located in the selection overlay node.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node, whose selection shall be removed.
 */
export function removeDrawingSelection(drawingNode) {
    var drawing = $(drawingNode);
    if (drawing.data('selection')) {
        drawing.data('selection').remove();
        drawing.removeData('selection');
    }
}

/**
 * Helper function to remove the rotation angle tooltip hint.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node, whose selection shall be removed.
 */
export function removeRotateAngleHint(drawingNode) {
    var drawing = $(drawingNode);
    var selectedDrawingNode = drawing.data('selection') || drawing;

    selectedDrawingNode.find(ROTATE_ANGLE_HINT_SELECTOR).empty();
}

/**
 * Adds rotation hint angle during drawing rotation.
 *
 * @param {jQuery} hintNode
 *  Node in which the hint angle will be inserted.
 * @param {Number} angle
 *  In degrees.
 * @param {Boolean} flipH
 *  If drawing is flipped horizontally.
 * @param {Boolean} flipV
 *  If drawing is flipped vertically.
 */
export function addRotationHint(hintNode, angle, flipH, flipV) {
    var corrAngleSign = flipH !== flipV ? 1 : -1;
    var scaleValue = 1.048;
    var scaleXStr = flipH ? 'scaleX(-' + scaleValue + ')' : 'scaleX(' + scaleValue + ')';
    var scaleYStr = flipV ? ' scaleY(-' + scaleValue + ')' : ' scaleY(' + scaleValue + ')';

    // 1.048 and translateZ(0) are attempt to reduce blurrines in Chrome browsers
    hintNode
        .css({ transform: 'rotate(' + (angle * corrAngleSign) + 'deg) ' + scaleXStr + scaleYStr + ' translateZ(0)' })
        .text(angle + '°');
}

/**
 * Returns the tracker node that will be used to visualize the current
 * position and size of a drawing frame during move/resize tracking.
 *
 * @param {HTMLElement|jQuery} drawingFrame
 *  The root node of the drawing frame. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The tracker node if available (especially, teh drawing frame must be
 *  selected to contain a tracker node); otherwise an empty jQuery
 *  collection.
 */
export function getTrackerNode(drawingFrame) {
    return getSelectionNode(drawingFrame).find('>.' + TRACKER_CLASS);
}

/**
 * Sets or resets the CSS class representing the active tracking mode for
 * the specified drawing frames.
 *
 * @param {HTMLElement|jQuery} drawingFrames
 *  The root node of a single drawing frame, or multiple drawing frames in
 *  a jQuery collection. This method changes the tracking mode of all
 *  drawing frames at once.
 *
 * @param {Boolean} active
 *  Whether tracking mode is active.
 */
export function toggleTracking(drawingFrames, active) {
    $(drawingFrames).find('>.' + SELECTION_CLASS).toggleClass(TRACKING_CLASS, active === true);
}

/**
 * Returns the tracking direction stored in a resizer handle node of a
 * drawing frame selection box.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String|Null}
 *  If the passed node is a resizer handle of a drawing frame selection
 *  box, returns its direction identifier (one of 'l', 'r', 't', 'b', 'tl',
 *  'tr', 'bl', or 'br'). Otherwise, returns null.
 */
export function getResizerHandleType(node) {
    return $(node).is(NODE_SELECTOR + '>.' + SELECTION_CLASS + '>.resizers>[data-pos]') ? $(node).attr('data-pos') : null;
}

/**
 * Returns whether the passed DOM node is the handle for rotation.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed DOM node is the handle for rotation.
 */
export function isRotateHandle(node) {
    return $(node).is(NODE_SELECTOR + '>.' + SELECTION_CLASS + '>.rotate-handle');
}

/**
 * Returns whether the passed DOM node is the handle for shape adjustments.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed DOM node is the handle for shape adjustments.
 */
export function isAdjustmentHandle(node) {
    return $(node).hasClass('adj-handle');
}

/**
 * Updates the size and position of all children of a drawing group,
 * but only temporary, during resize of the group.
 *
 * INFO: Use private method for final resize!
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 * @param {jQuery} drawing
 *  Shape node to be resized.
 * @param {Object} mergedAttributes
 *  Merged attributes of drawing node.
 */
export function previewOnResizeDrawingsInGroup(app, drawing, mergedAttributes) {
    resizeDrawingsInGroup(app, drawing, mergedAttributes, { resizeActive: true });
}

/**
 * Public method for updating shape formatting.
 * Intended for rendering preview of shape during active resizing.
 * Therefore, explicit shape attributes are used.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {jQuery} drawingFrame
 *  The DOM drawing frame of a shape object that will be formatted.
 *
 * @param {Object} mergedAttributes
 *  The merged attribute set of the drawing object.
 *
 * @param {Object} [options]
 *  - {Boolean} [options.isResizeActive=false]
 *      If this method is called during resizing of the shape.
 */
export function updateShapeFormatting(app, drawingFrame, mergedAttributes, options) {
    updateShapeFormattingLocal(app, drawingFrame, mergedAttributes, _.extend({}, options, { useExplicitAttrs: true }));
}

/**
 * Updates the position and size of the text frame located in the passed
 * DOM drawing frame, but does not update any other formatting.
 *
 * @param {EditApplication} app
 *  The application instance containing the drawing frame.
 *
 * @param {jQuery} drawingFrame
 *  The DOM drawing frame whose text frame will be updated.
 *
 * @param {Object} mergedAttributes
 *  The merged attribute set of the drawing object.
 */
export function updateTextFramePosition(app, drawingFrame, mergedAttributes) {
    updateShapeFormattingLocal(app, drawingFrame, mergedAttributes, { textFrameOnly: true });
}

/**
 * Adds hint bottom arrow class inside textframes with autoResizeHeight=false,
 * when text is hidden in overflow.
 * Overwrites vertical alignment to top when text is overflown.
 *
 * @param {jQuery} drawingNode
 *  Text frame drawing node that has fixed height.
 *
 * @param {Boolean} isODF
 *  Whether the format is odf or not.
 *
 * @param {Object} [geometryAttrs]
 *  Set of geometry attributes (if exist) for given node.
 */
export function handleTextframeOverflow(drawingNode, isODF, geometryAttrs) {
    var textFrameNode = drawingNode.find('.textframe');

    if (textFrameNode.length) {
        // needs to be added always, as only with flex-start value will scrollHeight return correct value
        textFrameNode.addClass(VERT_ALIGN_TOP_IMPORTANT_CLASS);
        var isOverflow = textFrameNode[0].scrollHeight > textFrameNode.outerHeight();
        var shouldVertAlign = isOverflow && !isODF;
        var contentNode = drawingNode.children('.textframecontent');
        if ((_.isEmpty(geometryAttrs) || _.isEmpty(geometryAttrs.presetShape) || geometryAttrs.presetShape === 'rect')) {
            contentNode.toggleClass('b-arrow', isOverflow);
        }
        textFrameNode.toggleClass(VERT_ALIGN_TOP_IMPORTANT_CLASS, shouldVertAlign);
        if (shouldVertAlign) { textFrameNode[0].scrollTop = 0; }
    }
}

/**
 * Used as fix for Bugs 45323, 48187
 * Special behavior of presentation files
 * Shapes do ignore their horizontal flip state,
 * if parent group has standard transform or completely inverted transform.
 *
 * @param {jQuery|Node} textFrameNode
 *  Text frame drawing node for which flip count is checked.
 * @param {String} [boundary]
 *  If specified, css selector as a string, which serves as end point until where the node parents are traversed.
 *
 * @returns {Boolean}
 *  If count of all flips is odd number.
 */
export function isOddFlipHVCount(textFrameNode, boundary) {
    var queryEnd = boundary || '.app-content';
    var flipHCount = $(textFrameNode).parentsUntil(queryEnd, '.flipH').length; // must be separate queries, because if element has flipH and flipV,
    var flipVCount = $(textFrameNode).parentsUntil(queryEnd, '.flipV').length; // both need to be counted
    return (flipHCount + flipVCount) % 2 !== 0;
}

/**
 * Gets the X and Y position values of snap point related to the passed shape attributes.
 *
 * @param {Object} cxn
 *  X and y descriptions for the geometry.
 * @param {Rectangle} snapRect
 *  Rectangle object with shape dimensions.
 * @param {Array} avList
 * @param {Array} gdList
 *
 * @returns {Object}
 *  X and Y position values of snap point (in pixels).
 */
export function getPointPixelPosition(cxn, snapRect, avList, gdList) {
    var x = getGeometryValueLocal(cxn.x, snapRect, avList, gdList, {});
    var y = getGeometryValueLocal(cxn.y, snapRect, avList, gdList, {});

    return { x, y };
}

/**
 * Gets the geometry values of snap point related to the passed shape attributes.
 *
 * @param {String|Number} value
 *  Geometry value description.
 * @param {Rectangle} snapRect
 *  Rectangle object with shape dimensions.
 * @param {Array} avList
 * @param {Array} gdList
 *
 * @returns {Number}
 *  Geometry value.
 */
export function getGeometryValue(value, snapRect, avList, gdList) {
    return getGeometryValueLocal(value, snapRect, avList, gdList, {});
}

/**
 * Scale current pixel position value to the proportion of absolute span of allowed min max boundaries.
 *
 * @param {Number} currentPos
 * @param {Number} minPosPx
 * @param {Number} maxPosPx
 * @param {Number} minValue
 * @param {Number} minMaxSpanPx
 * @param {Number} absMinMaxSpan
 * @param {Boolean} isRadius
 *
 * @returns {Number}
 */
export function scaleAdjustmentValue(currentPos, minPosPx, maxPosPx, minValue, minMaxSpanPx, absMinMaxSpan, isRadius) {
    var scaleFactor = (maxPosPx > minPosPx ? (currentPos - minPosPx) : (minPosPx - currentPos)) / minMaxSpanPx;
    var endValue = round(absMinMaxSpan * scaleFactor + minValue);

    return isRadius ? max(endValue, 1) : endValue;
}

/**
 * Returns pixel position of point, based on geometry value and snap rectangle.
 *
 * @param {Object} geoValue
 *  X and y descriptions for the geometry.
 * @param {Rectangle} snapRect
 *  Rectangle object with shape dimensions.
 * @param {Array} avList
 * @param {Array} ahList
 * @param {Array} gdList
 * @param {String} gdRef
 *  Reference value to avList
 *
 * @returns {Object} {x,y}
 *  X and Y position values of snap point (in pixels).
 */
export function getPxPosFromGeo(geoValue, snapRect, avList, ahList, gdList, gdRef) {
    var localAvList = _.copy(avList);
    localAvList[gdRef] = geoValue;

    return getPointPixelPosition(ahList, snapRect, localAvList, gdList);
}

/**
 * Retrieve snap point's pixel position inside shape, and calculate from that absolute position, relative to the slide start.
 * Return values for x and y are in Hmm!
 *
 * @param {Object} snapPointGeoObj
 * @param {String} snapPointIndex
 * @param {Number} leftHmm - in Hmm
 * @param {Number} topHmm - in Hmm
 * @param {Number} widthPx - in px
 * @param {Number} heightPx - in px
 * @param {Number} angle - in degrees
 * @param {Boolean} flipH
 * @param {Boolean} flipV
 *
 * @returns {Object} x,y values of point, relative to the slide
 */
export function getSnapPointAbsolutePosition(snapPointGeoObj, snapPointIndex, leftHmm, topHmm, widthPx, heightPx, angle, flipH, flipV) {
    var originalDrawSnapRect = new Rectangle(0, 0, widthPx, heightPx);
    var actSnapPoint = getPointPixelPosition(snapPointGeoObj.cxnList[snapPointIndex], originalDrawSnapRect, snapPointGeoObj.avList, snapPointGeoObj.gdList);
    var actSnapPointPxX = flipH ? widthPx - actSnapPoint.x : actSnapPoint.x;
    var actSnapPointPxY = flipV ? heightPx - actSnapPoint.y : actSnapPoint.y;
    var actSnapPointHmmX = leftHmm + convertLengthToHmm(actSnapPointPxX, 'px');
    var actSnapPointHmmY = topHmm + convertLengthToHmm(actSnapPointPxY, 'px');
    if (_.isNumber(angle)) {
        var centerPosX = leftHmm + convertLengthToHmm((widthPx / 2), 'px');
        var centerPosY = topHmm + convertLengthToHmm((heightPx / 2), 'px');
        var rotatedVal = rotatePointWithAngle(centerPosX, centerPosY, actSnapPointHmmX, actSnapPointHmmY, -angle);
        actSnapPointHmmX = rotatedVal.left;
        actSnapPointHmmY = rotatedVal.top;
    }

    return { x: actSnapPointHmmX, y: actSnapPointHmmY };
}

/**
 * Creates properties for connectors linked to shape, on shape change, with left and top diff values.
 *
 * @param {Array} allSlideDrawings
 * @param {Object} connObj
 * @param {Object} optionObj
 *  - {String} optionObj.type - move/resize/rotate
 *  - {Number} optionObj.widthScale
 *  - {Number} optionObj.heightScale
 *  - {jQuery} optionObj.activeNode
 *  - {Number} optionObj.rotation
 *
 * @returns {Object|Null}
 */
export function recalcConnectorPoints(allSlideDrawings, connObj, optionObj, rootNode, zoomFactor) {
    var connProp = {};
    var invertH, invertV;
    var finalFlipH, finalFlipV;
    var cDrawingAttrs = connObj.attrs.drawing;
    var origConnFlipH = cDrawingAttrs.flipH;
    var origConnFlipV = cDrawingAttrs.flipV;
    var parentIdsMatch = !_.isUndefined(connObj.parentStartId) && (connObj.parentStartId === connObj.parentEndId);
    var connectorShouldMove = optionObj.type === 'move' && (!connObj.startId || !connObj.endId || connObj.startId === connObj.endId || parentIdsMatch);
    var newAvList = optionObj.avList;

    function calcNewPointsPos(activeDrawing, activeAttrs, activeId, passiveId, snapPointObj, activeIndex, groupArgs) {
        var drawingAttrs = activeAttrs && activeAttrs.drawing;
        // group related properties
        var horzResizeFactor = groupArgs && groupArgs.horzResizeFactor;
        var vertResizeFactor = groupArgs && groupArgs.vertResizeFactor;
        var isGroupFactorScale = horzResizeFactor || vertResizeFactor;
        var factorWidth = horzResizeFactor ? (horzResizeFactor * drawingAttrs.width) : drawingAttrs.width;
        var factorHeight = vertResizeFactor ? (vertResizeFactor * drawingAttrs.height) : drawingAttrs.height;

        var newLeft = optionObj.newLeft;
        var newTop = optionObj.newTop;
        var drawingWidthPx = _.isNumber(factorWidth) ? round(convertHmmToLength(factorWidth, 'px')) : (activeDrawing.width() * zoomFactor);
        var drawingHeightPx = _.isNumber(factorHeight) ? round(convertHmmToLength(factorHeight, 'px')) : (activeDrawing.height() * zoomFactor);
        var scaledActWidth = drawingWidthPx * optionObj.widthScale;
        var scaledActHeight = drawingHeightPx * optionObj.heightScale;

        function getPassiveSnapPoint(connEdges, edgePos) {
            var origPasvPoint = connEdges[edgePos];
            if (cDrawingAttrs.rotation) {
                var rotObj = rotatePointWithAngle(connDrwRect.centerX(), connDrwRect.centerY(), origPasvPoint.x, origPasvPoint.y, -cDrawingAttrs.rotation);
                origPasvPoint.x = rotObj.left;
                origPasvPoint.y = rotObj.top;
            }
            origPasvPoint.diffX = 0;
            origPasvPoint.diffY = 0;

            if (connObj.passivePntObj && connObj.passivePntObj[passiveId]) {
                var modPasvPoint = connObj.passivePntObj[passiveId];
                modPasvPoint.diffX = modPasvPoint.x - origPasvPoint.x;
                modPasvPoint.diffY = modPasvPoint.y - origPasvPoint.y;
                return modPasvPoint;
            }

            return origPasvPoint;
        }

        // helper func to fetch drawing inside a group identical to given drawing,
        // but in copy container. Used for attaching snap points.
        function getSnapDrawing(activeDrawing) {
            var copyNode;
            if (activeDrawing.hasClass('grouped')) {
                var rootParent = activeDrawing.parentsUntil('.slide', '.drawing').last();
                var ind = rootParent.children('.content').not('.copy').find('.drawing').index(activeDrawing);
                copyNode = rootParent.children('.copy').find('.drawing').eq(ind);
                return copyNode.length ? copyNode : activeDrawing;
            } else {
                copyNode = activeDrawing.find('.copy');
                return copyNode.length ? copyNode : activeDrawing;
            }
        }

        var connDrwRect = Rectangle.from(cDrawingAttrs);
        // 4 connector edges:
        var connEdges = [
            { x: connDrwRect.left, y: connDrwRect.top },
            { x: connDrwRect.right(), y: connDrwRect.top },
            { x: connDrwRect.right(), y: connDrwRect.bottom() },
            { x: connDrwRect.left, y: connDrwRect.bottom() }
        ];

        var edgePos, actPointStart, actPointEnd;
        if (activeId === connObj.startId && activeIndex === connObj.startIndex) {
            edgePos = cDrawingAttrs.flipH && cDrawingAttrs.flipV ? 2 : (cDrawingAttrs.flipH ? 1 : (cDrawingAttrs.flipV ? 3 : 0));
        } else {
            edgePos = cDrawingAttrs.flipH && cDrawingAttrs.flipV ? 0 : (cDrawingAttrs.flipH ? 3 : (cDrawingAttrs.flipV ? 1 : 2));
        }
        actPointStart = connEdges[edgePos];
        if (cDrawingAttrs.rotation) {
            var rotObj = rotatePointWithAngle(connDrwRect.centerX(), connDrwRect.centerY(), actPointStart.x, actPointStart.y, -cDrawingAttrs.rotation);
            actPointStart.x = rotObj.left;
            actPointStart.y = rotObj.top;
        }
        edgePos = (edgePos + 2) % 4;

        if (newAvList) { snapPointObj.avList = newAvList; }
        if (isGroupFactorScale) {
            var newSnapRect = { left: 0, top: 0, width: scaledActWidth, height: scaledActHeight };
            var snapDrawing = getSnapDrawing(activeDrawing);
            createSnapPointsLocal(snapDrawing, newSnapRect, snapPointObj.avList, snapPointObj.gdList, snapPointObj.cxnList);
            // TODO: clean up snappoints at the end?

            var newActPointBRect = snapDrawing.find('.snap-points-container').children().eq(activeIndex)[0].getBoundingClientRect();
            actPointEnd = {
                x: convertLengthToHmm((newActPointBRect.left + newActPointBRect.width / 2 - groupArgs.rootOffset.left) / zoomFactor, 'px'),
                y: convertLengthToHmm((newActPointBRect.top + newActPointBRect.height / 2 - groupArgs.rootOffset.top) / zoomFactor, 'px')
            };
        } else {
            actPointEnd = getSnapPointAbsolutePosition(snapPointObj, activeIndex, newLeft, newTop, scaledActWidth, scaledActHeight, optionObj.rotation, optionObj.drawingFlipH, optionObj.drawingFlipV);
        }

        var pasvPoint = getPassiveSnapPoint(connEdges, edgePos);
        connObj.passivePntObj = {};
        connObj.passivePntObj[activeId] = actPointEnd;

        var startPasvPointX = pasvPoint.x - pasvPoint.diffX;
        var startPasvPointY = pasvPoint.y - pasvPoint.diffY;
        var treshold = convertLengthToHmm(2, 'px'); // if width or height is close to 1-2 pxs, we use threshold for getting near same level points
        var condition;
        // if width or height is close to couple of pxs, then is hard to determine if shape is flipped, and to get correct diagonal point;
        // therefore, final flip value is calculated, that overwrite inverted orginal values (see #55811)
        if (abs(actPointStart.x - startPasvPointX) <= treshold) {
            condition = origConnFlipV ? actPointStart.y > startPasvPointY : actPointStart.y < startPasvPointY;
            finalFlipH = condition ? actPointEnd.x > pasvPoint.x : actPointEnd.x < pasvPoint.x;
        }
        if (abs(actPointStart.y - startPasvPointY) <= treshold) {
            condition = origConnFlipH ? actPointStart.x > startPasvPointX : actPointStart.x < startPasvPointX;
            finalFlipV = condition ? actPointEnd.y > pasvPoint.y : actPointEnd.y < pasvPoint.y;
        }
        invertH = checkConnectorFlip(actPointStart.x, startPasvPointX, actPointEnd.x - actPointStart.x, pasvPoint.diffX);
        invertV = checkConnectorFlip(actPointStart.y, startPasvPointY, actPointEnd.y - actPointStart.y, pasvPoint.diffY);

        if (cDrawingAttrs.rotation) { // new flip values are needed if rotation angle of connector is reset
            origConnFlipH = actPointStart.x > pasvPoint.x;
            origConnFlipV = actPointStart.y > pasvPoint.y;

            if (activeId !== connObj.startId) { // if actively modified drawing is not the same Id as start Id of connector, inverting is neccessary
                origConnFlipH = !origConnFlipH;
                origConnFlipV = !origConnFlipV;
            }

        }

        return new Rectangle(min(actPointEnd.x, pasvPoint.x), min(actPointEnd.y, pasvPoint.y), max(abs(actPointEnd.x - pasvPoint.x), 1), max(abs(actPointEnd.y - pasvPoint.y), 1));
    }

    if (connectorShouldMove) {
        // move connector
        connProp = {
            attrs: { drawing: {
                left: cDrawingAttrs.left + optionObj.leftDiff,
                top: cDrawingAttrs.top + optionObj.topDiff }
            }
        };
    } else {
        // connector is resized and/or repositioned
        var drawing1 = _.findWhere(allSlideDrawings, { id: connObj.startId });
        var drawing2 = _.findWhere(allSlideDrawings, { id: connObj.endId });
        var isPoint1Active = drawing1 && (optionObj.activeNode[0] === drawing1.node || optionObj.activeNode[0].contains(drawing1.node));
        var activeDrawingNode = isPoint1Active ? drawing1 && $(drawing1.node) : drawing2 && $(drawing2.node);
        var activeAttrs = isPoint1Active ? drawing1 && drawing1.attrs : drawing2 && drawing2.attrs;
        var activeIndex = isPoint1Active ? connObj.startIndex : connObj.endIndex;
        var activeId = isPoint1Active ? connObj.startId : connObj.endId;
        var passiveId = isPoint1Active ? connObj.endId : connObj.startId;

        // group properties
        var groupParent1Obj = _.findWhere(allSlideDrawings, { id: connObj.parentStartId });
        var groupParent2Obj = _.findWhere(allSlideDrawings, { id: connObj.parentEndId });
        var group1ObjAttrs = groupParent1Obj ? groupParent1Obj.attrs : null;
        var group2ObjAttrs = groupParent2Obj ? groupParent2Obj.attrs : null;
        var activeGroupAttrs = isPoint1Active ? group1ObjAttrs : group2ObjAttrs;
        var groupDrawingAttrs = activeGroupAttrs ? activeGroupAttrs.drawing : null;
        var horzResizeFactor = groupDrawingAttrs ? (groupDrawingAttrs.width / groupDrawingAttrs.childWidth) : null;
        var vertResizeFactor = groupDrawingAttrs ? (groupDrawingAttrs.height / groupDrawingAttrs.childHeight) : null;
        var rootOffset = groupDrawingAttrs ? rootNode.offset() : null;
        var groupArgs = {
            horzResizeFactor,
            vertResizeFactor,
            rootOffset
        };

        if (!activeDrawingNode) {
            globalLogger.error('DrawingFrame.recalcConnectorPoints(): activeDrawingNode not found!');
            return null;
        }

        var shapeGeoData = _.copy(activeDrawingNode.data('shapeGeoData'));
        if (!shapeGeoData || !shapeGeoData.cxnList) {
            globalLogger.warn('DrawingFrame.recalcConnectorPoints(): snapPoint data not found!');
            return null;
        }
        var opRect = calcNewPointsPos(activeDrawingNode, activeAttrs, activeId, passiveId, shapeGeoData, activeIndex, groupArgs);

        if (connObj.startId === connObj.endId || parentIdsMatch) { // both ends linked to same drawing
            var otherActiveIndex = activeIndex === connObj.startIndex ? connObj.endIndex : connObj.startIndex;
            if (parentIdsMatch) { // linked are two different drawings inside one parent group, fetch other node
                activeDrawingNode = isPoint1Active ? drawing2 && $(drawing2.node) : drawing1 && $(drawing1.node);
                shapeGeoData = _.copy(activeDrawingNode.data('shapeGeoData'));
                if (!shapeGeoData || !shapeGeoData.cxnList) {
                    globalLogger.warn('DrawingFrame.recalcConnectorPoints(): snapPoint data not found!');
                    return null;
                }
                activeAttrs = isPoint1Active ? drawing2 && drawing2.attrs : drawing1 && drawing1.attrs;
                var tempId = activeId;
                activeId = passiveId;
                passiveId = tempId;
            }
            opRect = calcNewPointsPos(activeDrawingNode, activeAttrs, activeId, passiveId, shapeGeoData, otherActiveIndex, groupArgs);
        }

        connProp = {
            attrs: { drawing: {
                left: round(opRect.left),
                top: round(opRect.top),
                width: round(opRect.width),
                height: round(opRect.height) }
            }
        };
        if (cDrawingAttrs.rotation) { // if connector has rotation angle, it is reset to 0, and new values for flip are added
            connProp.attrs.drawing.rotation = 0;
            connProp.attrs.drawing.flipH = origConnFlipH;
            connProp.attrs.drawing.flipV = origConnFlipV;
        }
        if (invertH) { connProp.attrs.drawing.flipH = !origConnFlipH; }
        if (invertV) { connProp.attrs.drawing.flipV = !origConnFlipV; }
        if (!_.isUndefined(finalFlipH)) { connProp.attrs.drawing.flipH = finalFlipH; }
        if (!_.isUndefined(finalFlipV)) { connProp.attrs.drawing.flipV = finalFlipV; }
    }

    return connProp;
}

/**
 * Public method to create snap points inside drawing node, based on geometry properites.
 *
 * @param {jQuery} currentNode
 *  Node where the snap points will be appended.
 * @param {Rectangle} snapRect
 *  Rectangle of the drawing.
 * @param {Array} avList
 * @param {Array} gdList
 * @param {Array} cxnList
 */
export function createSnapPoints(currentNode, snapRect, avList, gdList, cxnList) {
    createSnapPointsLocal(currentNode, snapRect, avList, gdList, cxnList);
}

/**
 * @param {jQuery} drawingNode
 *  Node from which the adjustment points will be removed.
 */
export function clearAdjPoints(drawingNode) {
    $(drawingNode).find('.adj-points-container').remove();
}

/**
 * Repositioning the adjustment handles
 *
 * @param {jQuery} drawingNode
 *  The drawing node to be selected, as DOM node or jQuery object.
 *
 * @param {Number} zoomFactor
 *  Zoom factor of the document
 *
 * @param {Object} [options]
 *  @param {Number} [options.width]
 *   Current pixel width of the drawingNode
 *  @param {Number} [options.height]
 *   Current pixel height of the drawingNode
 */
export function repositionAdjPoints(drawingNode, zoomFactor, options) {
    var geoData = drawingNode.data('shapeGeoData');
    if (geoData) { createAdjustmentHandles(drawingNode, geoData.avList, geoData.gdList, geoData.ahList, zoomFactor, options); }
}
