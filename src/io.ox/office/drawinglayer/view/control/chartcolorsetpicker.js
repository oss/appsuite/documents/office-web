/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { createSpan } from "@/io.ox/office/tk/dom";
import { getCaptionNode } from "@/io.ox/office/tk/forms";

import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import { transformOpColor } from "@/io.ox/office/editframework/utils/color";
import { getColorSet } from "@/io.ox/office/drawinglayer/view/chartstyleutil";

// constants ==================================================================

// number of color boxes per list entry in the ColorSetPicker control
const COLOR_BOX_COUNT = 5;

// all predefined color sets
const CHART_COLOR_SETS = getColorSet();

const TRANSFORMS = [{ shade: 60000 }, { shade: 80000 }, null, { tint: 80000 }, { tint: 60000 }];

// class ChartColorSetPicker ==================================================

/**
 * A dropdown menu for picking different predefined chart color sets.
 */
export class ChartColorSetPicker extends AppRadioList {

    constructor(docView) {

        // base constructor
        super(docView, {
            //#. Menu title: predefined color sets for chart objects
            label: gt.pgettext("chart-format", "Color set"),
            //#. Menu tooltip: predefined color sets for chart objects
            tooltip: gt.pgettext("chart-format", "Predefined color set"),
            //#. Short menu title: predefined color sets for chart objects (shorter than "Color set", to be shown on small devices)
            shrinkLabel: gt.pgettext("chart-format", "Color"),
            updateCaptionMode: "none"
        });

        // CSS marker class for special formatting
        this.menu.$el.addClass("color-set-picker");

        // insert color buttons in the dropdown menu after changed themes
        this.menu.requestMenuRepaintOnAllEvents(this.docModel.themeCollection);
    }

    // protected methods ------------------------------------------------------

    /**
     * Fills the dropdown menu with all available color sets.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.clearOptions();

        CHART_COLOR_SETS.forEach((colorSet, index) => {

            // create the option button control
            const $button = this.addOption(`cs${index}`, { tooltip: colorSet.name });
            const $caption = getCaptionNode($button);

            // create all color boxes
            for (let index = 0; index < COLOR_BOX_COUNT; index += 1) {
                let color = colorSet.colors[index % colorSet.colors.length];
                const transform = (colorSet.type === "single") ? TRANSFORMS[index] : null;
                if (transform) { color = transformOpColor(color, transform); }
                const background = this.docModel.getCssColor(color, "fill");
                $caption.append(createSpan({ classes: "color-box", style: { background } }));
            }
        });
    }
}
