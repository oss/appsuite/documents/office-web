/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { is, dict } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE, Rectangle, resolveCssVar, convertLengthToHmm, getWindowWidth } from "@/io.ox/office/tk/dom";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

import { createDefaultSizeForShapeId } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { PRESET_SHAPE_CONFIG_ODF, PRESET_SHAPE_CONFIG_OOX, isPresetConnectorId } from "@/io.ox/office/drawinglayer/view/drawinglabels";
import { drawPreviewShape } from "@/io.ox/office/drawinglayer/view/drawingframe";

// constants ==================================================================

const ICON_SIZE = SMALL_DEVICE ? 21 : 25;

const ICON_CONTAINER_SIZE = SMALL_DEVICE ? 35 : 39;

const SHAPES_PER_ROW = Math.min(12, (getWindowWidth() / ICON_CONTAINER_SIZE) - 1);

// attribute set used to render the shape icons
const RENDER_ATTR_SET = {
    fill: {
        type: "none"
    },
    line: {
        type: "solid",
        style: "solid",
        width: convertLengthToHmm(1, "px"),
        headEndLength: "small",
        headEndWidth: "small",
        tailEndLength: "small",
        tailEndWidth: "small"
    }
};

// class ShapeTypePicker ======================================================

export class ShapeTypePicker extends RadioList {

    constructor(docView, config) {

        // base constructor
        super({
            tooltip: gt("Insert a shape"),
            label: gt("Shape"),
            icon: "png:drawing-shape",
            ...config,
            updateCaptionMode: "none",
            highlight: Boolean,
            listLayout: "grid",
            gridColumns: SHAPES_PER_ROW,
            mruSize: SHAPES_PER_ROW,
            mruSettingsKey: docView.docApp.isODF() ? "shapetypepicker-odf" : "shapetypepicker-ooxml"
        });

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // CSS marker class for styling
        this.menu.$el.addClass("shape-type-picker");

        // render the shape icon with canvas to new button element (event handler needed to catch MRU buttons!)
        this.listenTo(this.menu, "list:option:add", this._addOptionHandler);

        // repaint canvas-based bitmap icons
        this.menu.requestMenuRepaintOnGlobalEvent("change:theme");

        // add an activated shape to the MRU list
        this.on("group:commit", presetId => this.menu.setMRUValue(presetId));

        // force GPU rendering with translate3d
        if (_.device("touch && (macos || ios) && safari")) {
            this.listenTo(this.menu.$el, "scroll", _.throttle(() => {
                this.menu.$el.css("transform", "translate3d(0,0,0)");
                this.setTimeout(() => this.menu.$el.css("transform", "none"), 0);
            }, 100));
        }
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // remove all option buttons from the dropdown menu
        this.clearOptions();

        // use shape categories as menu sections
        dict.forEach(this.docApp.isODF() ? PRESET_SHAPE_CONFIG_ODF : PRESET_SHAPE_CONFIG_OOX, (categoryDef, categoryId) => {

            // create the menu section
            this.addSection(categoryId, categoryDef.title);

            // create shape buttons in the section
            dict.forEach(categoryDef.shapes, (shapeDef, presetId) => {
                const tooltip = is.string(shapeDef) ? shapeDef : (shapeDef?.tooltip || "");
                this.addOption(presetId, { tooltip });
            });
        });
    }

    // private methods --------------------------------------------------------

    /*private*/ _addOptionHandler($button, presetId) {

        // total size of the shape
        const shapeSize = createDefaultSizeForShapeId(presetId, ICON_SIZE, ICON_SIZE);
        // extra processing for complex (non-straight) connectors
        const complexConnector = isPresetConnectorId(presetId) && !/^(line|straight)/.test(presetId);
        // location of the shape inside the canvas
        const snapRect = complexConnector ? new Rectangle(0, 4, shapeSize.width, shapeSize.height - 8) : null;

        // initialize formatting of the button element
        $button.css({
            minWidth: ICON_CONTAINER_SIZE,
            maxWidth: ICON_CONTAINER_SIZE,
            maxHeight: ICON_CONTAINER_SIZE,
            padding: 0,
            lineHeight: "normal"
        });

        // render the shape object
        const $canvas = drawPreviewShape(this.docApp, $button, shapeSize, presetId, {
            attrs: RENDER_ATTR_SET,
            snapRect,
            lineColor: resolveCssVar("--text"),
            drawIconLabel: true,
            iconLabelFont: "11px OpenSans,sans-serif"
        });

        // align the canvas centered in the button element
        $canvas.css({
            left: Math.floor((ICON_CONTAINER_SIZE - $canvas[0].width - 6) / 2),
            top: Math.floor((ICON_CONTAINER_SIZE - $canvas[0].height - 6) / 2)
        });
    }
}
