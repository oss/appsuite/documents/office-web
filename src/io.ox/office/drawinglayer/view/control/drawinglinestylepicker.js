/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { BorderStylePicker } from "@/io.ox/office/editframework/view/control/borderstylepicker";

// class DrawingLineStylePicker ===============================================

// predefined border styles (line width, line style) supported by OOXML
const PRESET_LINE_STYLES_OOX = [
    { value: "none:none",         label: /*#. border line style (no border) */ gt.pgettext("borders", "No line"),                                         style: "none" },
    { value: "solid:hair",        label: /*#. border line style (width 0.5 pixels, solid) */ gt.pgettext("borders", "Hair line"),                         style: "solid",      width: 0 },
    { value: "solid:thin",        label: /*#. border line style (width 1 pixel, solid) */ gt.pgettext("borders", "Thin line"),                            style: "solid",      width: 1 },
    { value: "dashed:thin",       label: /*#. border line style (width 1 pixel, dashed) */ gt.pgettext("borders", "Dashed thin line"),                    style: "dashed",     width: 1 },
    { value: "dotted:thin",       label: /*#. border line style (width 1 pixel, dotted) */ gt.pgettext("borders", "Dotted thin line"),                    style: "dotted",     width: 1 },
    { value: "dashDot:thin",      label: /*#. border line style (width 1 pixel, dash-dot) */ gt.pgettext("borders", "Dot-and-dash thin line"),            style: "dashDot",    width: 1 },
    { value: "dashDotDot:thin",   label: /*#. border line style (width 1 pixel, dash-dot-dot) */ gt.pgettext("borders", "Two-dots-and-dash thin line"),   style: "dashDotDot", width: 1 },
    { value: "solid:medium",      label: /*#. border line style (width 2 pixels, solid) */ gt.pgettext("borders", "Medium line"),                         style: "solid",      width: 2 },
    { value: "dashed:medium",     label: /*#. border line style (width 2 pixels, dashed) */ gt.pgettext("borders", "Dashed medium line"),                 style: "dashed",     width: 2 },
    { value: "dotted:medium",     label: /*#. border line style (width 2 pixel, dotted) */ gt.pgettext("borders", "Dotted medium line"),                  style: "dotted",     width: 2 },
    { value: "dashDot:medium",    label: /*#. border line style (width 2 pixel, dash-dot) */ gt.pgettext("borders", "Dot-and-dash medium line"),          style: "dashDot",    width: 2 },
    { value: "dashDotDot:medium", label: /*#. border line style (width 2 pixel, dash-dot-dot) */ gt.pgettext("borders", "Two-dots-and-dash medium line"), style: "dashDotDot", width: 2 },
    { value: "solid:thick",       label: /*#. border line style (width 3 pixels, solid) */ gt.pgettext("borders", "Thick line"),                          style: "solid",      width: 3 }
];

// predefined border styles (line width, line style) supported by ODF
const PRESET_LINE_STYLES_ODF = [
    { value: "none:none",         label: /*#. border line style (width 0.5 pixels, solid) */ gt.pgettext("borders", "No line"),                           style: "none" },
    { value: "solid:hair",        label: /*#. border line style (width 0.5 pixels, solid) */ gt.pgettext("borders", "Hair line"),                         style: "solid",  width: 0 },
    { value: "solid:thin",        label: /*#. border line style (width 1 pixel, solid) */ gt.pgettext("borders", "Thin line"),                            style: "solid",  width: 1 },
    { value: "dashed:thin",       label: /*#. border line style (width 1 pixel, dashed) */ gt.pgettext("borders", "Dashed thin line"),                    style: "dashed", width: 1 },
    { value: "dotted:thin",       label: /*#. border line style (width 1 pixel, dotted) */ gt.pgettext("borders", "Dotted thin line"),                    style: "dotted", width: 1 },
    { value: "solid:medium",      label: /*#. border line style (width 2 pixels, solid) */ gt.pgettext("borders", "Medium line"),                         style: "solid",  width: 2 },
    { value: "dashed:medium",     label: /*#. border line style (width 2 pixels, dashed) */ gt.pgettext("borders", "Dashed medium line"),                 style: "dashed", width: 2 },
    { value: "dotted:medium",     label: /*#. border line style (width 2 pixel, dotted) */ gt.pgettext("borders", "Dotted medium line"),                  style: "dotted", width: 2 },
    { value: "solid:thick",       label: /*#. border line style (width 3 pixels, solid) */ gt.pgettext("borders", "Thick line"),                          style: "solid",  width: 3 }
];

/**
 * A dropdown list control for stroke styles for drawing objects.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `BorderStylePicker`.
 */
export class DrawingLineStylePicker extends BorderStylePicker {

    constructor(docView, config) {
        const PRESET_STYLES = docView.docApp.isODF() ? PRESET_LINE_STYLES_ODF : PRESET_LINE_STYLES_OOX;
        super(PRESET_STYLES, {
            label: config?.line ? gt.pgettext("drawing", "Line style") : gt.pgettext("drawing", "Border style"),
            ...config
        });
    }
}
