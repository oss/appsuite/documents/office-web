/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button, CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";

// class DrawingAlignmentPicker ===============================================

/**
 * Drop-down menu with commands to change the alignment of drawing objects
 * relative to each other.
 *
 * @param {EditView} docView
 *  The document view instance owning this control.
 */
export class DrawingAlignmentPicker extends CompoundButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            //#. drop-down menu to change the alignment of drawing objects
            label: gt.pgettext("drawing-pos", "Align"),
            icon: "png:drawing-h-align-left",
            ...config
        });

        // horizontal alignment
        this.addSection("horalign");
        this.addControl("drawing/align", new Button({
            value: "left",
            icon: "png:drawing-h-align-left",
            label: /*#. horizontal alignment of drawing objects */ gt.pgettext("drawing-pos", "Align left")
        }));
        this.addControl("drawing/align", new Button({
            value: "center",
            icon: "png:drawing-h-align-center",
            label: /*#. horizontal alignment of drawing objects */ gt.pgettext("drawing-pos", "Align center")
        }));
        this.addControl("drawing/align", new Button({
            value: "right",
            icon: "png:drawing-h-align-right",
            label: /*#. horizontal alignment of drawing objects */ gt.pgettext("drawing-pos", "Align right")
        }));

        // vertical alignment
        this.addSection("vertalign");
        this.addControl("drawing/align", new Button({
            value: "top",
            icon: "png:drawing-v-align-top",
            label: /*#. vertical alignment of drawing objects */ gt.pgettext("drawing-pos", "Align top")
        }));
        this.addControl("drawing/align", new Button({
            value: "middle",
            icon: "png:drawing-v-align-middle",
            label: /*#. vertical alignment of drawing objects */ gt.pgettext("drawing-pos", "Align middle")
        }));
        this.addControl("drawing/align", new Button({
            value: "bottom",
            icon: "png:drawing-v-align-bottom",
            label: /*#. vertical alignment of drawing objects */ gt.pgettext("drawing-pos", "Align bottom")
        }));

        // distribution
        this.addSection("distribution");
        this.addControl("drawing/distribute", new Button({
            value: "horzSlide",
            icon: "png:distribute-slide-h",
            label: gt.pgettext("drawing-pos", "Distribute horizontally on slide"),
            tooltip: gt("Distribute selected objects horizontally on slide")
        }));
        this.addControl("drawing/distribute", new Button({
            value: "vertSlide",
            icon: "png:distribute-slide-v",
            label: gt.pgettext("drawing-pos", "Distribute vertically on slide"),
            tooltip: gt("Distribute selected objects vertically on slide")
        }));
        this.addControl("drawing/distributeamong", new Button({
            value: "horzSelection",
            icon: "png:distribute-objects-h",
            label: gt.pgettext("drawing-pos", "Distribute horizontally among objects"),
            tooltip: gt("Distribute selected objects horizontally among themselves")
        }));
        this.addControl("drawing/distributeamong", new Button({
            value: "vertSelection",
            icon: "png:distribute-objects-v",
            label: gt.pgettext("drawing-pos", "Distribute vertically among objects"),
            tooltip: gt("Distribute selected objects vertically among themselves")
        }));
    }
}
