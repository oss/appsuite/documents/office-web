/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button, CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";
import { DrawingOrderType } from "@/io.ox/office/drawinglayer/utils/drawingutils";

// constants ==================================================================

//#. dropdown menu to change the Z order and rotation of drawing objects
const DRAWING_ARRANGE_LABEL = gt.pgettext("drawing-pos", "Arrange");

//#. dropdown menu to change the Z order of drawing objects
const DRAWING_REORDER_LABEL = gt.pgettext("drawing-pos", "Reorder");

//#. dropdown menu to change the rotation (and flipping) of drawing objects
const DDRAWING_ROTATE_LABEL = gt.pgettext("drawing-pos", "Rotate");

// class DrawingArrangementPicker =============================================

/**
 * Dropdown menu with commands to change the Z order of drawing objects, and
 * additional rotation and flipping commands.
 *
 * @param {EditView} docView
 *  The document view instance owning this control.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `CompoundButton`, and the following options:
 *  - {boolean} [config.rotationControls=false]
 *    If set to `true`, the list entries for rotation and flipping will be
 *    created.
 */
export class DrawingArrangementPicker extends CompoundButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            label: config?.rotationControls ? DRAWING_ARRANGE_LABEL : DRAWING_REORDER_LABEL,
            icon: "png:z-order",
            ...config
        });

        // create list entries for reordering
        this.addSection("reorder", config?.rotationControls ? DRAWING_REORDER_LABEL : null);
        this._createOrderButtons();

        // create list entries for rotation and flipping
        if (config?.rotationControls) {
            this.addSection("rotate", DDRAWING_ROTATE_LABEL);
            this._createRotationButtons();
        }
    }

    // private methods --------------------------------------------------------

    /*private*/ _createOrderButtons() {
        this.addControl("drawing/order", new Button({
            value: DrawingOrderType.FRONT,
            icon: "png:arrange-to-front",
            label: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Bring to front"),
            tooltip: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Bring object to the front")
        }));
        this.addControl("drawing/order", new Button({
            value: DrawingOrderType.FORWARD,
            icon: "png:arrange-forward",
            label: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Bring forward"),
            tooltip: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Move object up one placement to the front")
        }));
        this.addControl("drawing/order", new Button({
            value: DrawingOrderType.BACKWARD,
            icon: "png:arrange-backward",
            label: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Send backward"),
            tooltip: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Send object one placement to the back")
        }));
        this.addControl("drawing/order", new Button({
            value: DrawingOrderType.BACK,
            icon: "png:arrange-to-back",
            label: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Send to back"),
            tooltip: /*#. change the order of drawing objects */ gt.pgettext("drawing-pos", "Send object to the back")
        }));
    }

    /*private*/ _createRotationButtons() {
        this.addControl("drawing/transform/rotate", new Button({
            value: 90,
            icon: "png:drawing-rotate-right",
            label: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Rotate right 90\xb0"),
            tooltip: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Rotate right 90 degrees")
        }));
        this.addControl("drawing/transform/rotate", new Button({
            value: -90,
            icon: "png:drawing-rotate-left",
            label: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Rotate left 90\xb0"),
            tooltip: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Rotate left 90 degrees")
        }));
        this.addControl("drawing/transform/flipv", new Button({
            icon: "png:drawing-flip-v",
            label: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Flip vertical"),
            tooltip: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Flip object vertical")
        }));
        this.addControl("drawing/transform/fliph", new Button({
            icon: "png:drawing-flip-h",
            label: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Flip horizontal"),
            tooltip: /*#. rotate drawing objects */ gt.pgettext("drawing-pos", "Flip object horizontal")
        }));
    }
}
