/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";

// class ChartStyleSetPicker ==================================================

/**
 * A drop-down menu for picking different predefined chart style sets.
 */
export class ChartStyleSetPicker extends RadioList {

    constructor(docView) {

        // base constructor
        super({
            //#. menu title: predefined style sets for chart objects (e.g. "Light", "Dark", "Colored")
            label: gt.pgettext("chart-format", "Style set"),
            //#. menu tooltip: predefined style sets for chart objects (e.g. "Light", "Dark", "Colored")
            tooltip: gt.pgettext("chart-format", "Predefined style set"),
            //#. short menu title: predefined style sets for chart objects (e.g. "Light", "Dark", "Colored") (shorter than "Style set", to be shown on small devices)
            shrinkLabel: gt.pgettext("chart-format", "Style"),
            updateCaptionMode: "none"
        });

        // initialization
        this.addOption("ss0", { label: /*#. predefined chart style set: one light color (different shades) */ gt.pgettext("chart-format", "Light") });
        this.addOption("ss4", { label: /*#. predefined chart style set: different colors from color scheme */ gt.pgettext("chart-format", "Colored") });

        if (docView.docApp.isOOXML()) {
            this.addOption("ss5", { label: /*#. predefined chart style set: one dark color (different shades) */ gt.pgettext("chart-format", "Dark") });
        }
    }
}
