/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { map } from "@/io.ox/office/tk/algorithms";
import { Rectangle, resolveCssVar } from "@/io.ox/office/tk/dom";
import { Path, Canvas } from "@/io.ox/office/tk/canvas";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

// types ======================================================================

/**
 * Enumeration of predefined arrow styles for line endings.
 */
export const ArrowStyle = {
    NONE: "none",
    ARROW: "arrow",
    TRIANGLE: "triangle",
    DIAMOND: "diamond",
    OVAL: "oval"
};

// constants ==================================================================

// the canvas element used to generate the bitmaps
const canvas = new Canvas({ location: { width: 100, height: 18 }, _kind: "singleton" });

// maps unique bitmap keys to the data URLs
const bitmapUrlMap = new Map/*<string, string>*/();

// private functions ==========================================================

function createArrowPath(rect, style, tail) {

    const expand = (style === ArrowStyle.ARROW) || (style === ArrowStyle.TRIANGLE);
    const x = rect.left + (expand ? (tail ? rect.width : -rect.width) : 0);
    const y = rect.top;
    const { width, height } = rect;

    const path = new Path();
    switch (style) {
        case ArrowStyle.ARROW:
            path.pushArrow(x, y, width, height, 0, tail);
            break;
        case ArrowStyle.DIAMOND:
            path.pushDiamond(x, y, width, height, 0);
            break;
        case ArrowStyle.TRIANGLE:
            path.pushTriangle(x, y, width, height, 0, tail);
            break;
        case ArrowStyle.OVAL:
            path.pushEllipse(x, y, width, height, 0);
            break;
    }
    return path;
}

/**
 * Creates a bitmap for the specified border line style, and returns its data
 * URL.
 *
 * @param {ArrowStyle} headStyle
 *  The arrow type for the line head.
 *
 * @param {ArrowStyle} tailStyle
 *  The arrow type for the line tail.
 *
 * @param {string} color
 *  The effective CSS line color.
 *
 * @returns {string}
 *  The data URL of the cached or generated bitmap.
 */
function getArrowStyleBitmapUrl(headStyle, tailStyle, color) {

    // return data URL of a bitmap already created, or create a new bitmap
    return map.upsert(bitmapUrlMap, `${headStyle},${tailStyle},${color}`, () => {

        return canvas.clear().renderToDataURL((context, width, height) => {

            const y = Math.floor(height / 2) - 0.5;
            const arrowSize = height / 2;

            context.setLineStyle({ style: color, width: 1 });
            context.setFillStyle(color);

            const headRect = new Rectangle(arrowSize,         y, arrowSize - 1, arrowSize - 1);
            const tailRect = new Rectangle(width - arrowSize, y, arrowSize - 1, arrowSize - 1);

            const headPath = createArrowPath(headRect, headStyle, false);
            const tailPath = createArrowPath(tailRect, tailStyle, true);

            if ((headStyle === ArrowStyle.NONE) || (headStyle === ArrowStyle.ARROW)) { headRect.left = 1; }
            if ((tailStyle === ArrowStyle.NONE) || (tailStyle === ArrowStyle.ARROW)) { tailRect.left = width - 1; }

            context.drawLine(headRect.left, y, tailRect.left, y);
            context.drawPath(headPath, (headStyle === ArrowStyle.ARROW) ? "stroke" : "fill");
            context.drawPath(tailPath, (tailStyle === ArrowStyle.ARROW) ? "stroke" : "fill");
        });
    });
}

// class GenericArrowStylePicker ==============================================

/**
 * A generic dropdown list control for border styles.
 *
 * @param {object[]} entries
 *  An array of descriptors for the entries of the dropdown list. Each array
 *  element is an object with the following properties:
 *  - {string} value
 *    The value associated to the list item.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `RadioList`.
 */
class GenericArrowStylePicker extends RadioList {

    constructor(entries, config) {

        // base constructor
        super({
            icon: "bi:arrow-left-right",
            //#. arrow styles at the end of line and connector objects
            label: gt.pgettext("arrows", "Arrow style"),
            //#. arrow styles at the end of line and connector objects
            tooltip: gt.pgettext("arrows", "Change line endings"),
            updateCaptionMode: "none",
            dropDownVersion: { label: config?.tooltip },
            ...config
        });

        // properties
        this._entries = entries;

        // repaint canvas-based bitmap icons
        this.menu.requestMenuRepaintOnGlobalEvent("change:theme");
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.clearOptions();

        const textColor = resolveCssVar("--text");
        const selectColor = resolveCssVar("--listmenu-selected-text-color");

        this._entries.forEach(({ value, head, tail }) => {
            const bmpIcon1 = getArrowStyleBitmapUrl(head, tail, textColor);
            const bmpIcon2 = (selectColor === textColor) ? "" : getArrowStyleBitmapUrl(head, tail, selectColor);
            this.addOption(value, { bmpIcon: [bmpIcon1, bmpIcon2] });
        });
    }
}

// class DrawingArrowStylePicker ==============================================

// predefined arrow styles
const ARROW_STYLES = [
    ArrowStyle.NONE,
    ArrowStyle.ARROW,
    ArrowStyle.TRIANGLE,
    ArrowStyle.DIAMOND,
    ArrowStyle.OVAL
];

/**
 * A dropdown list control for predefined line-ending arrow styles for drawing
 * objects.
 */
export class DrawingArrowStylePicker extends GenericArrowStylePicker {
    constructor(config) {
        const tails = config?.tails;
        const entries = ARROW_STYLES.map(style => ({ value: style, head: tails ? "none" : style, tail: tails ? style : "none" }));
        super(entries, config);
    }
}

// class DrawingArrowPresetPicker =============================================

// predefined double-ended arrow styles
const ARROW_PRESET_STYLES = [
    { head: ArrowStyle.NONE,     tail: ArrowStyle.NONE },
    { head: ArrowStyle.NONE,     tail: ArrowStyle.ARROW },
    { head: ArrowStyle.ARROW,    tail: ArrowStyle.NONE },
    { head: ArrowStyle.ARROW,    tail: ArrowStyle.ARROW },
    { head: ArrowStyle.NONE,     tail: ArrowStyle.TRIANGLE },
    { head: ArrowStyle.TRIANGLE, tail: ArrowStyle.NONE },
    { head: ArrowStyle.TRIANGLE, tail: ArrowStyle.TRIANGLE },
    { head: ArrowStyle.DIAMOND,  tail: ArrowStyle.TRIANGLE },
    { head: ArrowStyle.OVAL,     tail: ArrowStyle.TRIANGLE },
    { head: ArrowStyle.DIAMOND,  tail: ArrowStyle.DIAMOND },
    { head: ArrowStyle.OVAL,     tail: ArrowStyle.OVAL }
];

/**
 * A dropdown list control for predefined line-ending arrow styles (both ends)
 * for drawing objects.
 */
export class DrawingArrowPresetPicker extends GenericArrowStylePicker {
    constructor(config) {
        const entries = ARROW_PRESET_STYLES.map(({ head, tail }) => ({ value: `${head}:${tail}`, head, tail }));
        super(entries, config);
    }
}
