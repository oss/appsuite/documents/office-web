/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";

// class ChartLegendPicker ====================================================

/**
 * A drop-down menu for picking the position of the chart legend.
 */
export class ChartLegendPicker extends RadioList {

    constructor() {

        // base constructor
        super({
            //#. menu title: positions for the legend in a chart object
            label: gt.pgettext("chart-legend", "Legend position"),
            //#. menu tooltip: positions for the legend in a chart object
            tooltip: gt.pgettext("chart-legend", "Visibility and position of the chart legend"),
            //#. Short menu title: positions for the legend in a chart object (shorter than "Legend position", to be shown on small devices)
            shrinkLabel: gt.pgettext("chart-legend", "Legend"),
            updateCaptionMode: "none"
        });

        // initialization
        this.addOption("off",    { section: "off", label: /*#. chart legend is hidden */ gt.pgettext("chart-legend", "Off") });
        this.addOption("top",    { section: "pos", label: /*#. chart legend at top of border of the chart */ gt.pgettext("chart-legend", "Top") });
        this.addOption("bottom", { section: "pos", label: /*#. chart legend at bottom border of the chart */ gt.pgettext("chart-legend", "Bottom") });
        this.addOption("left",   { section: "pos", label: /*#. chart legend at left border of the chart */ gt.pgettext("chart-legend", "Left") });
        this.addOption("right",  { section: "pos", label: /*#. chart legend at right border of the chart */ gt.pgettext("chart-legend", "Right") });
    }
}
