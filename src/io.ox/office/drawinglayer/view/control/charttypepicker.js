/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import CanvasJS from '@canvasjs/charts';

import gt from "gettext";

import { fun } from "@/io.ox/office/tk/algorithms";
import { createElement, createDiv, insertHiddenNodes } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { DataPipeline } from "@/io.ox/office/tk/workers";

import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import { opSchemeColor } from '@/io.ox/office/editframework/utils/color';
import { CHART_TYPE_STYLES } from "@/io.ox/office/drawinglayer/view/drawinglabels";

// constants ==================================================================

const DATA_POINTS = [[6, 4, 5, 3, 4, 1], [1, 2, 3, 4, 6, 7]];

const SCATTER_POINTS = [2.5, 4, 1.5, 3.5, 2, 2.5];

const MARKER_LIST = ["triangle", "cross"];

const AXIS_DATA = { labelFontColor: "transparent", lineColor: "transparent", tickColor: "transparent", gridColor: "transparent", labelFontSize: 3 };

// data model with disabled axes and random values
const MODEL_DATA = {
    creditHref: "",
    creditText: "",
    title: { text: "" },
    axisX: { ...AXIS_DATA },
    axisY: { ...AXIS_DATA },
    data: DATA_POINTS.map(values => {
        const dataPoints = values.map((y, x) => ({ x, y }));
        return { indexLabelFontColor: "transparent", indexLabelLineColor: "transparent", showInLegend: false, dataPoints, sourcePoints: {} };
    })
};

// private functions ==========================================================

const getRenderer = fun.once(() => {
    const container = insertHiddenNodes(createDiv({ style: { width: "100px", height: "80px" } }));
    const renderer = new CanvasJS.Chart(container, MODEL_DATA);
    return { renderer, container };
});

// class ChartTypePicker ======================================================

export class ChartTypePicker extends AppRadioList {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            width: "11em",
            icon: "bi:bar-chart",
            tooltip: gt("Chart type"),
            updateCaptionMode: "label",
            listLayout: "grid",
            gridColumns: 6,
            shrinkWidth: "7em",
            ...config
        });

        // background renderer for pending tables
        this._pipeline = this.member(new DataPipeline(buttonNode => this._renderPreviewChart(buttonNode), { paused: true }));
        // image nodes and chart style entries for rendering
        this._registry = [];

        // CSS marker class for styling
        this.menu.$el.addClass("chart-type-picker");

        // create the (empty) option buttons for each chart type
        CHART_TYPE_STYLES.forEach(typeEntry => {
            const $button = this.addOption(typeEntry.type, {
                classes: "mini-caption",
                label: typeEntry.title,
                tooltip: typeEntry.title,
                wrapText: true,
                data: typeEntry
            });
            // create an empty image element
            const imgNode = createElement("img");
            imgNode.alt = typeEntry.title || "";
            $button.prepend(imgNode);
            // register the image element for rendering the preview chart
            this._registry.push({ img: imgNode, ...typeEntry });
        });

        // render the pending chart elements while popup menu is open
        this.listenTo(this.menu, "popup:show", () => this._pipeline.resume());
        this.listenTo(this.menu, "popup:hide", () => this._pipeline.pause());

        // refresh preview charts in the drop-down menu after changed theme (color scheme)
        this.waitForImportSuccess(() => {
            this._renderAllPreviewCharts();
            this.listenToAllEvents(this.docModel.themeCollection, this._renderAllPreviewCharts);
        });
    }

    // private methods --------------------------------------------------------

    /*private*/ _renderAllPreviewCharts() {
        this._pipeline.abort();
        this._registry.forEach(entry => entry.img.classList.add("pending"));
        this._pipeline.pushValues(this._registry);
    }

    /*private*/ _renderPreviewChart(imgEntry) {

        // the CanvasJS chart renderer
        const { renderer, container } = getRenderer();

        // the <canvas> element created by the renderer
        const canvasNode = container.querySelector("canvas");
        if (!canvasNode) {
            globalLogger.warn("$badge{ChartTypePicker} renderPreviewChart: canvas element not found");
            return;
        }

        const chartType = imgEntry.type;
        const { cjs, markersOnly } = imgEntry.series;
        const single = /^(pie|doughnut)/.test(cjs);

        MODEL_DATA.data.forEach((dataSeries, index) => {
            dataSeries.type = cjs;
            const fillColor = this.docModel.getCssColor(opSchemeColor(`accent${index + 1}`), "fill");
            dataSeries.color = markersOnly ? "transparent" : fillColor;
            dataSeries.markerColor = markersOnly ? fillColor : dataSeries.color;
            dataSeries.markerType = markersOnly ? MARKER_LIST[index] : "circle";
            dataSeries.markerSize = markersOnly ? 10 : 1;
        });

        MODEL_DATA.data[0].dataPoints.forEach((dataPoint, index) => {
            dataPoint.color = single ? this.docModel.getCssColor(opSchemeColor(`accent${index + 1}`), "fill") : null;
        });

        MODEL_DATA.data[1].dataPoints.forEach((dataPoint, index) => {
            dataPoint.x = chartType.includes("scatter") ? SCATTER_POINTS[index] : index;
        });

        // render the preview chart
        renderer.render();

        // update the image element
        imgEntry.img.src = canvasNode.toDataURL();
        imgEntry.img.classList.remove("pending");
    }
}
