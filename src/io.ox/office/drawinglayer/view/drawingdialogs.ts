/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { createInput } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import type { EditFrameworkDialogConfig } from "@/io.ox/office/editframework/view/editlabels";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import { type ImageDescriptor, ImageSourceType, hasFileImageExtension, insertFile } from "@/io.ox/office/drawinglayer/utils/imageutils";
import { InsertImageDriveDialog } from "@/io.ox/office/drawinglayer/view/dialog/insertimagedrivedialog";
import { InsertImageURLDialog } from "@/io.ox/office/drawinglayer/view/dialog/insertimageurldialog";

// types ======================================================================

/**
 * A callback function that must implement inserting a selected image into the
 * document. May return a promise to indicate asynchronous insertion.
 *
 * @param imageDesc
 *  The descriptor of the image to be inserted.
 */
export type InsertImageHandlerFn = (imageDesc: ImageDescriptor) => MaybeAsync<unknown>;

// public functions ===========================================================

/**
 * Shows a modal dialog to insert an image into the document.
 *
 * @param docView
 *  The document view.
 *
 * @param sourceType
 *  The type of the image dialog to be shown.
 *
 * @param actionHandler
 *  A callback function that must implement inserting the selected image into
 *  the document. May return a promise to indicate asynchronous insertion.
 *
 * @param [dialogConfig]
 *  Configuration options for the dialog.
 *
 * @returns
 *  A promise that will fulfil if an image has been selected and inserted; or
 *  that will reject, if the dialog has been canceled.
 */
export async function showInsertImageDialog<ConfT extends EditFrameworkDialogConfig = EditFrameworkDialogConfig>(docView: EditView, sourceType: ImageSourceType, actionHandler: InsertImageHandlerFn, dialogConfig?: ConfT): Promise<void> {

    switch (sourceType) {

        // insert image from local file (shows a native OS filepicker dialog)
        case ImageSourceType.LOCAL: {

            // DOCS-3749: Selecting a local file is a bit tricky. There is no official (or otherwise reliable) way to
            // detect *cancelation* of the system filepicker dialog. Workarounds that try to sniff the focused state of
            // the used <input> element conflict with global controller focus handling.
            //
            // Therefore, we cannot expose a promise that reflects the entire image insertion process because it would
            // remain in pending state if the dialog has been canceled. Instead, this function will return immediately
            // after opening the system filepicker dialog (i.e. the document view will leave busy mode initiated by the
            // document controller calling this function). As long as the picker dialog is open, the browser blocks the
            // entire page, but browser events will still be processed in the background. When receiving the "change"
            // event from the <input> element, it is required to check if the application is still alive. The action
            // handler will then be executed while the document view has been set to busy mode explicitly.

            // create a <input type="file"> element accepting image files only
            const fileInput = docView.insertHiddenNodes(createInput({ type: "file", accept: "image/*" }));

            // handler for selecting a file in the system filepicker dialog ("docView.listenTo" prevents that the
            // handler will be called after the application has shut down, e.g. due to reload on external errors etc.)
            docView.listenTo(fileInput, "change", jpromise.wrapFloating(async () => {

                const { docApp } = docView;

                // extract the file descriptor, and remove the helper element from the DOM
                const fileDesc = fileInput.files?.[0];
                fileInput.remove();

                // application may have switched to error or closing state in the meantime
                if (docApp.isInternalError() || docApp.isInQuit()) {
                    return;
                }

                // check file extension (user may change dialog to be able to select other files)
                if (!docApp.isEditable() || !fileDesc || !hasFileImageExtension(fileDesc.name)) {
                    docApp.rejectEditAttempt("image");
                    return;
                }

                // enter busy mode, read and send file to server, and execute the passed handler
                try {
                    docView.enterBusy();
                    const imageDesc = await insertFile(docApp, fileDesc);
                    await actionHandler(imageDesc);
                } finally {
                    docView.leaveBusy();
                }
            }));

            // immediately click the input element to open the OS file picker
            // (everything else is up to the "change" event handler above)
            fileInput.click();
            break;
        }

        // insert image from Drive (shows a Core-UI filepicker dialog)
        case ImageSourceType.DRIVE: {
            const dialog = new InsertImageDriveDialog(docView, dialogConfig);
            const imageDesc = await dialog.show();
            await actionHandler(imageDesc);
            break;
        }

        // insert image from URL download (shows a text input dialog)
        case ImageSourceType.URL: {
            const dialog = new InsertImageURLDialog(docView, dialogConfig);
            const imageDesc = await dialog.show();
            await actionHandler(imageDesc);
            break;
        }

        default:
            globalLogger.error("$badge{DrawingLayer} showInsertImageDialog: invalid dialog type");
            throw new TypeError("showInsertImageDialog: invalid dialog type");
    }
}
