/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { convertLength } from '@/io.ox/office/tk/utils';

import { transformOpColor, opRgbColor, opSchemeColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getAccentColorName } from '@/io.ox/office/editframework/view/editlabels';

// constants ==============================================================

export const BACKGROUND_TRANSFORMATIONS = [{ satMod: 25000 }, { lumOff: 35000 }];

var STYLESET = [
    { bg: Color.LIGHT1, bevelEnabled: false },
    { bg: Color.LIGHT1, bevelEnabled: false },
    { bg: Color.LIGHT1, bevelEnabled: false },
    { bg: Color.LIGHT1, bevelEnabled: true },
    { bg: null, bevelEnabled: false },
    { bg: transformOpColor(Color.TEXT1, { lumOff: 25000 }), bevelEnabled: true }
];

var COLORSET = [];

(function () {
    var allAccents = [];
    for (var i = 1; i <= 6; i++) {
        allAccents.push(opSchemeColor(`accent${i}`));
    }

    COLORSET.push({
        //#. predefined formatting style for chart objects: black/gray/white only
        name: gt.pgettext('chart-format', 'Grayscale'),
        colors: [opRgbColor('555555'), opRgbColor('9E9E9E'), opRgbColor('727272'), opRgbColor('464646'), opRgbColor('838383'), opRgbColor('C1C1C1')],
        type: 'group'
    });

    COLORSET.push({
        //#. predefined formatting style for chart objects: all accent colors from the current color scheme
        name: gt.pgettext('chart-format', 'Accents'),
        colors: allAccents,
        type: 'group'
    });

    allAccents.forEach(function (color, index) {
        COLORSET.push({
            name: getAccentColorName(index + 1),
            colors: [color],
            type: 'single'
        });
    });
}());

// class ChartStyleUtil ============================================

/**
 * an infoholder for the 48 chartstyles, a combination of colorpatterns, backgroundcolors and highlighting
 */

// public methods ---------------------------------------------------------

/**
 * return a list of different combinations of backgrounds and highlights
 */
export function getStyleSet() {
    return STYLESET;
}

/**
 * return a list of different combinations of grayscales and accents
 */
export function getColorSet() {
    return COLORSET;
}

export function isAutoShape(shape) {
    if (!shape) { return true; }
    if (shape.gradient && shape.gradient.colorStops) {
        return isAutoColor(shape.gradient.colorStops[0].color);
    } else {
        return isAutoColor(shape.color);
    }
}

export function isAutoColor(color) {
    return !color || color.type === 'auto' || color.value === 'phClr';
}

export function getColorOfPattern(meth, type, index, schemeColors, count, docModel) {

    var colorIndex = null;
    var relVariation = 0;

    if (type === 'single') {
        relVariation = (index / count) - 0.5;
    } else if (type === 'group') {
        var group = Math.floor(index / schemeColors.length);
        var all = Math.floor(count / schemeColors.length);
        relVariation = group / all;

        if (count <= schemeColors.length * 2) {
            //TODO: workaround for small charts, should be calculated correctly
            relVariation *= 0.5;
        } else {
            relVariation -= 0.5;
        }
    } else {
        globalLogger.warn('cant handle colorize type:' + type);
        return null;
    }

    if (meth === 'cycle') {
        colorIndex = index % schemeColors.length;
    } else {
        globalLogger.warn('cant handle color meth:' + meth);
        return null;
    }

    var color = schemeColors[colorIndex];
    relVariation *= 1.4;

    const transforms = [];
    if (relVariation > 0) {
        transforms.push({ tint: Math.round((1 - relVariation) * 100000) });
    } else if (relVariation < 0) {
        transforms.push({ shade: Math.round((1 + relVariation) * 100000) });
    }

    color = transformOpColor(color, ...transforms);

    if (docModel) {
        color.fallbackValue = docModel.parseAndResolveColor(color, 'fill').hex;
    }

    return color;
}

function getLineColorForAuto(docModel, chartModel) {
    var colorDesc = docModel.parseAndResolveColor(chartModel.getBackgroundColor(), 'fill');
    return (colorDesc.y <= 0.4) ? 'white' : 'black';
}

/**
 * sets all character-model-properties to the target,
 * target is a CanvasJS-data-model
 *
 * @param {ChartModel} chartModel
 * @param {Object} character character-model comes from operations
 * @param {Object} target target-object is a inner object of CanvasJS
 * @param {Object} prefix necessary if there are more than one character-holder on the target object
 */
export function handleCharacterProps(chartModel, character, target, prefix) {
    var app = chartModel.getApp();
    var docModel = app.getModel();

    var usePrefix;
    if (prefix && prefix.length) {
        usePrefix = prefix + 'Font';
    } else {
        usePrefix = 'font';
    }

    var cssColor = null;
    var color = character.color;
    if (isAutoColor(color)) {
        cssColor = getLineColorForAuto(docModel, chartModel);
    } else {
        cssColor = docModel.getCssColor(color);
    }

    var targetJSON = target[usePrefix] || {};
    target[usePrefix] = targetJSON;

    targetJSON.Color  = cssColor;
    targetJSON.Family = docModel.getCssFontFamily(character.fontName);
    targetJSON.Size   = getHeightForCharacterProps(chartModel, character);
    targetJSON.Weight = character.bold ? 'bold' : 'normal';
    targetJSON.Style  = character.italic ? 'italic' : 'normal';

    _.each(targetJSON, function (entry, key) {
        target[usePrefix + key] = entry;
    });
}

/**
 * returns calculates pixel height from character's fontSize and current zoom factor from docView
 *
 * @param {ChartModel} chartModel
 * @param {Object} character character-model comes from operations
 */
export function getHeightForCharacterProps(chartModel, character) {
    var app = chartModel.getApp();
    var docView = app.getView();
    return docView.getZoomFactor() * convertLength(character.fontSize, 'pt', 'px');
}

/**
 * sets all lineshape-model-properties to the target,
 * target is a CanvasJS-data-model
 *
 * @param {ChartModel} chartModel
 * @param {object} line lineshape-model comes from operations
 * @param {object} target target-object is a inner object of CanvasJS
 * @param {object} prefix necessary for more than one character-holder on the target object
 */
export function handleLineProps(chartModel, line, target, prefix) {

    var app = chartModel.getApp();
    var docModel = app.getModel();
    var docView = app.getView();
    var w = line.width ? line.width * (72 / 2540) : 1;
    var usePrefix = prefix;

    var targetJSON = target[usePrefix] || {};
    target[usePrefix] = targetJSON;

    if (line.type === 'none') {
        targetJSON.Color = 'transparent';
    } else {
        var cssColor = null;
        var color = line.color;
        if (isAutoColor(color)) {
            cssColor = getLineColorForAuto(docModel, chartModel);
        } else {
            cssColor = docModel.getCssColor(color);
        }

        targetJSON.Color     = cssColor;
        targetJSON.Thickness = Math.max(1, Math.round(docView.getZoomFactor() * w));
    }

    _.each(targetJSON, function (entry, key) {
        target[usePrefix + key] = entry;
    });
}

export function toStyleSetIndex(chartStyleId) {
    var styleId = chartStyleId - 1;
    var styleSet = (styleId / 8) | 0;

    if (styleSet < 4) {
        return 0;
    } else {
        return styleSet;
    }
}

export function toColorSetIndex(chartStyleId) {
    if (!chartStyleId) {
        return 0;
    }
    var styleId = chartStyleId - 1;
    return styleId % 8;
}

export function toChartStyleId(colorSetIndex, styleSetIndex) {
    return styleSetIndex * 8 + colorSetIndex + 1;
}
