/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { TextField } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import type { EditFrameworkDialogConfig } from "@/io.ox/office/editframework/view/editlabels";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import { type ImageDescriptor, isBase64, insertDataUrl, insertURL } from "@/io.ox/office/drawinglayer/utils/imageutils";

// class InsertImageURLDialog =================================================

/**
 * A dialog that provides a URL text field for a new image to be inserted
 * into the document.
 *
 * @param docView
 *  The document view that is creating the dialog.
 */
export class InsertImageURLDialog extends BaseDialog<ImageDescriptor, EditFrameworkDialogConfig> {

    constructor(docView: EditView, config?: EditFrameworkDialogConfig) {

        // base constructor
        super({
            //#. dialog title: download image and insert into document
            title: gt("Insert image URL"),
            width: 800,
            ...config
        });

        // create the text input field
        const inputField = new TextField("", {
            //#. tooltip/placeholder text: URL of image to be downloaded and inserted into document
            placeholder: gt("Enter image URL"),
            autoTrim: true
        });

        // render the text control, and take ownership
        this.renderControl(this.bodyNode, inputField);

        // create a validator for the URL input field
        this.addOkValidator(() => docView.docApp.isEditable() && !!inputField.rawText);

        // handler for the OK button of the dialog
        this.setOkHandler(() => {
            const url = inputField.value;
            return isBase64(url) ? insertDataUrl(docView.docApp, url) : insertURL(docView.docApp, url);
        });
    }
}
