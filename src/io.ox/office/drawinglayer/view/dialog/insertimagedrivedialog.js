/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import FilePicker from '$/io.ox/files/filepicker';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { globalEvents } from '@/io.ox/office/tk/events';
import { SMALL_DEVICE, OK_LABEL } from '@/io.ox/office/tk/dom';
import { getStandardPicturesFolderId } from '@/io.ox/office/tk/utils/driveutils';
import { EObject } from '@/io.ox/office/tk/objects';

import { hasFileImageExtension, insertDriveImage } from '@/io.ox/office/drawinglayer/utils/imageutils';

// constants ==================================================================

const RECENT_IMAGE_FOLDER_KEY = 'recentImageFolder';

// class InsertImageDriveDialog ===============================================

/**
 * A dialog that provides a file picker from Drive for a new image to be
 * inserted into the document.
 *
 * @param {EditView} docView
 *  The document view that is creating the dialog.
 */
export class InsertImageDriveDialog extends EObject {

    constructor(docView, config) {

        // `EObject` needed for `listenTo()` for auto-close on document events
        super();

        // document access
        this.docView = docView;
        this.docApp = docView.docApp;

        // the core filepicker dialog
        this._coreDialog = null;

        // emit global event for generic processing of the dialog
        globalEvents.emit("dialog:construct", this, config);
    }

    // public methods ---------------------------------------------------------

    /**
     * Shows the dialog.
     *
     * @returns {JPromise<ImageDescriptor>}
     *  A promise that will fulfil if an image has been selected; or that will
     *  reject, if the dialog has been canceled.
     */
    show() {

        // TODO:
        const recentImageFolder = this.docApp.getUserSettingsValue(RECENT_IMAGE_FOLDER_KEY) || getStandardPicturesFolderId();

        let promise = new FilePicker({
            // filter supported image file types
            filter: file => hasFileImageExtension(file.filename),
            sorter: file => (file.filename || file.title).toLowerCase(),
            primaryButtonText: OK_LABEL,
            header: gt('Insert image'),
            folder: recentImageFolder,
            uploadButton: true,
            uploadButtonText: gt('Upload local image'),
            multiselect: false,
            hideTrashfolder: true,
            acceptLocalFileType: 'image/*',
            createFolderButton: false,
            // allow to close this dialog by external calls
            initialize: dialog => (this._coreDialog = dialog),
            // destroy the `EObject` part of this instance when closing the dialog
            close: () => this.destroy()
        });

        // rescue reference to application ("this" will be destroyed when closing the Core filepicker)
        const { docApp } = this;

        promise = promise.then(selectedFiles => {

            if (!docApp.isEditable()) {
                return jpromise.reject();
            }

            const selectedFile = selectedFiles[0];
            if (!selectedFile) { return jpromise.reject(); }

            docApp.setUserSettingsValue(RECENT_IMAGE_FOLDER_KEY, selectedFile.folder_id);

            return insertDriveImage(docApp, selectedFile);
        });

        return promise;
    }

    close() {
        this._coreDialog?.close();

        // remove unnecessary container on small devices on filepicker close
        if (SMALL_DEVICE) { $('.picker-pc-container').remove(); }
    }
}
