/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";

/* drawing frames in document contents ===================================== */

.io-ox-office-main .formatted-content .drawing {
    position: relative;
    outline: none;
    cursor: default;
    font-size: 0;

    /* z-index of the drawing contents rendered above the root nodes */
    @drawing-content-z-index: 30;
    /* z-index of the drawing selection rendered above the drawing contents */
    @drawing-selection-z-index: 40;
    /* z-index of the drawing tracker node shown while moving or resizing a drawing */
    @drawing-tracker-z-index: 32;

    /* content node for the visual appearance of the drawing */
    > .content {
        /* NO ".absolute-with-distance(0)" anymore, does not works solid in Safari browser */
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        overflow: visible;
        font-size: 16px;

        &, * { z-index: @drawing-content-z-index; }

        img {
            position: relative;
            /* remove max-width:100% derived from Bootstrap */
            max-width: none;
            vertical-align: initial;
            image-orientation: none; /* DOCS-2109 */
        }

        // content flipping
        &.flipV { transform: scaleY(-1); }
        &.flipH { transform: scaleX(-1); }
        &.flipV.flipH { transform: scale(-1); }

        /* crop drawing contents at border */
        > .cropping-frame {
            .absolute-with-distance(0);
            overflow: hidden;
        }

        /* dedicated node for border styles (not cropped at drawing frame) */
        > .canvasgeometry {
            .absolute-with-distance(0);
            pointer-events: none;
        }

        &.chartholder {
            // fix for Bug 51933
            overflow: hidden;

            & > .chartnode {
                .absolute-with-distance(10px);

                .canvasjs-chart-container,
                canvas {
                    width: 100% !important;
                }
                //!important is important for DevicePixelRatio bigger than 1

                .canvasjs-chart-tooltip {
                    pointer-events: none;
                }
            }
        }

        &.placeholder {
            overflow: hidden;
        }
    }

    &[data-type="chart"] {
        z-index: @drawing-content-z-index;
    }

    /* text frames, not inside table drawings */
    &:not([data-type="table"]) > div.content.textframecontent {
        position: absolute;
        outline: none; // avoid browser focus border around text frame (Firefox & Chrome)

        .textframe {
            position: absolute;
            padding-bottom: 2px; // required to make spell check lines visible
            outline: none; // avoid browser focus border around text frame (Firefox & Chrome)

            &:not(.noflex) {
                display: flex;
                flex-direction: column;

                &[verticalalign="top"] {
                    justify-content: flex-start;
                }

                &[verticalalign="centered"] {
                    justify-content: center;
                }

                &[verticalalign="bottom"] {
                    justify-content: flex-end;
                }
            }

            // bug 48187: flip back text contents
            &.flipH { transform: scaleX(-1); }

            /* vertical aligment @see DrawingFrame updateShapeFormatting() */
            width: 100%;

            &.no-flex {
                display: block;
            }

            .tab {
                max-width: 98%; /* #53707 */
            }
        }

        &.autoresizeheight {
            position: relative; // enabling auto grow of content node
            .textframe {
                position: relative;
            }

            &:not(.minframeheight) {
                .textframe {
                    justify-content: flex-start; // ignore vertical alignment
                }
            }
        }
    }

    >.canvasexpansion {
        position: absolute;
    }

    // fix for Bug 47859
    &.bulletdrawing {
        font-size: 100%;

        // fix for Bug 48023
        pointer-events: none;

        > .content {
            font-size: 100%;
        }
    }

    &.movable {
        cursor: move;
    }

    &.noselectdrawing {
        pointer-events: none !important;
    }

    &.watermark {
        visibility: hidden;
    }

    // for slideTouchMode/ios only!
    &.active-edit.selected {
        outline: 2px dashed #5f97b3 !important; // border would move the text a bit due to the boxmodel
        box-shadow: none;
        -webkit-user-select: text; // only on for the selected drawing
        .textframe {
            pointer-events: auto !important;
        }

        // temp hack, for slideTouchMode/ios only!
        .selection {
            display: none; // hack to prevent flickering selection at double click
        }
    }

    &.rotating-state {
        box-shadow: none;
        > .content {
            opacity: 0.7;
        }
        > .selection {
            visibility: hidden;
        }
        &.temp-border2 {
            border: 1px solid fade(@doc-selection-color, 70%);
        }
    }

    &.indirectconnectorchange {
        position: static !important;
        transform: unset !important;
        z-index: 100 !important; // put indirectly modifying connector on top of all drawings

        .copy {
            opacity: 0.7;
        }
    }

    /* selection box for drawing objects */
    > .selection {
        position: static;
        width: 0;
        height: 0;
        overflow: visible;
        font-size: 1rem;
        touch-action: none; // prevent native scrolling when interacting with selected drawing objects

        /* formatting for the border lines (no hover effects) */
        .border-handle-styles(0.3em; 0; 0; fade(@doc-selection-color, 70%));
        .border-handle-z-index(@drawing-selection-z-index);

        /* formatting for the resizer handles */
        :not(.touch) & { .resizer-handle-styles(0.7em; 0.3em; 0.1em; 0; @doc-selection-color; true; false; true); }
        .touch & { .resizer-handle-styles(1.5em; 0.5em; 0.1em; 0; @doc-selection-color; true; false; true); }
        .resizer-handle-z-index(@drawing-selection-z-index);

        /* formatting for the rotation handle */
        :not(.touch) & { .rotate-handle-styles(1.3em; 0.2em; 0.2em; 2.5em; 0.3em; @doc-selection-color; true); }
        .touch & { .rotate-handle-styles(3em; 0.3em; 0.2em; 3em; 0.3em; @doc-selection-color; true); }
        .rotate-handle-z-index(@drawing-selection-z-index);

        .angle-tooltip {
            position: absolute;
            top: -1em;
            left: 2em;
            padding: 2px 5px;
            visibility: hidden;
            pointer-events: none;
            background: var(--background-a80);
            border: 1px solid var(--border-overlay-color);
            border-radius: 8px;
            line-height: normal;
            font-size: var(--std-font-size);
            color: var(--text);

            &:not(:empty) {
                visibility: visible;
            }

            .touch & {
                font-size: calc(1.5 * var(--std-font-size));
            }
        }

        /* tracking node (will be moved around while tracking) */
        > .tracker {
            position: absolute;
            box-sizing: border-box;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: @drawing-tracker-z-index;
            background-color: fade(white, 20%);
            border: 0.1em solid @doc-selection-color;
            .box-shadow(0.3em);
        }

        &:not(.tracking-active) > .tracker {
            display: none;
        }

        &[data-zoom-value^="15"],
        &[data-zoom-value^="2"],
        &[data-zoom-value^="3"],
        &[data-zoom-value^="4"],
        &[data-zoom-value^="5"],
        &[data-zoom-value^="6"] {

            .borders {
                font-size: 5px;
            }
        }
        &[data-zoom-value^="2"],
        &[data-zoom-value^="3"],
        &[data-zoom-value^="4"],
        &[data-zoom-value^="5"],
        &[data-zoom-value^="6"] {

            .borders {
                & > [data-pos="l"],
                & > [data-pos="r"] {
                    width: 0.1em;
                }
                & > [data-pos="b"] {
                    height: 1px;
                    bottom: -1px;
                }
            }
        }
        &[data-zoom-value^="3"],
        &[data-zoom-value^="4"],
        &[data-zoom-value^="5"],
        &[data-zoom-value^="6"] {

            .borders {
                & > [data-pos="l"],
                & > [data-pos="r"] {
                    top: 0;
                    height: 100%;
                }
            }
        }
        &[data-zoom-value^="4"],
        &[data-zoom-value^="5"],
        &[data-zoom-value^="6"] {

            .borders {
                & > [data-pos="t"] {
                    top: -1px;
                    height: 1px;
                }
            }
        }

        &[data-zoom-value^="15"],
        &[data-zoom-value^="2"],
        &[data-zoom-value^="3"],
        &[data-zoom-value^="4"],
        &[data-zoom-value^="5"],
        &[data-zoom-value^="6"] {

            .resizers {
                > [data-pos="l"],
                > [data-pos="tl"],
                > [data-pos="bl"] {
                    left: -0.7em !important;
                }
            }
        }

        &[data-zoom-value^="15"] {

            .resizers > [data-pos] {
                transform: scale(0.7);
            }
            .rotate-handle {
                top: -2em;
                transform: scale(0.7);
            }
        }

        &[data-zoom-value^="2"] {

            .resizers > [data-pos] {
                transform: scale(0.6);
            }
            .rotate-handle {
                top: -1.7em;
                transform: scale(0.6);
            }
        }

        &[data-zoom-value^="3"],
        &[data-zoom-value^="4"] {

            .resizers {
                > [data-pos] {
                    transform: scale(0.4);
                }
                > [data-pos="t"],
                > [data-pos="tl"],
                > [data-pos="tr"] {
                    top: -0.7em !important;
                }
                > [data-pos="b"],
                > [data-pos="bl"],
                > [data-pos="br"] {
                    bottom: -0.7em !important;
                }
            }
            .rotate-handle {
                top: -1.1em;
                transform: scale(0.35);
            }
        }

        &[data-zoom-value^="5"],
        &[data-zoom-value^="6"] {

            .resizers {
                > [data-pos] {
                    transform: scale(0.3);
                }
                > [data-pos="t"],
                > [data-pos="tl"],
                > [data-pos="tr"] {
                    top: -0.7em !important;
                }
            }
            .rotate-handle {
                top: -0.9em;
                transform: scale(0.3);
            }
        }
    }

    &:not(.resizable) > .selection > .resizers {
        display: none;
    }

    &:not(.rotatable) > .selection > .rotate-handle {
        display: none;
    }

    &:not(.adjustable) > .selection > .adj-points-container {
        display: none;
    }

    /* restrict displayed selection elements in two-point selection mode */
    &.two-point-shape {
        &.resizable > .selection > .borders {
            display: none;
        }
        > .selection > .rotate-handle {
            display: none;
        }
        &.start-linked {
            [data-pos="tl"]::before, [data-pos="tr"]::before {
                background-color: #4caf50 !important;
                border-color: #fff !important;
            }
        }
        &.end-linked {
            [data-pos="bl"]::before, [data-pos="br"]::before {
                background-color: #4caf50 !important;
                border-color: #fff !important;
            }
        }
    }

    .snap-points-container {
        pointer-events: none;
        visibility: hidden;
        position: absolute;
        top: 0;

        &.connector-hover {
            visibility: visible;
        }

        .snap-point {
            width: 10px;
            height: 10px;
            z-index: 31;

            &::before {
                content: "";
                position: absolute;
                top: 0.3em;
                right: 0.3em;
                bottom: 0.3em;
                left: 0.3em;
                background-color: rgb(131 131 131 / 50%);
                border: 1px solid white;
                border-radius: 50%;
                box-shadow: 0 0 0 0.1em fade(white, 50%);
            }
        }
    }

    .adj-points-container {

        .adj-handle {
            width: 7px;
            height: 7px;
            z-index: 40;

            &:hover {
                cursor: pointer;
            }

            &::before {
                content: "";
                position: absolute;
                top: 1px;
                left: 1px;
                width: 7px;
                height: 7px;
                background-color: rgb(255 243 175);
                border: 1px solid #3c73aa;
                border-radius: 50%;
                box-shadow: 0 0 0 0.1em fade(white, 50%);
            }
        }
    }

    &.active-cropping {
        @crop-border-width: 0.3em solid;
        @crop-border-distance: 0.2em;
        z-index: 30 !important;

        .hide-canvas {
            display: none;
        }

        .content {
            /* formatting for the resizer handles */
            .resizer-handle-z-index(@drawing-selection-z-index);

            .crop {
                user-select: none;

                &.active-crop-event {
                    .borders, .resizers {
                        display: none;
                    }
                    .tracker {
                        border: unset;
                        box-shadow: unset;
                    }
                    img {
                        .box-shadow(0.3em);
                    }
                }

                .tracking-active {
                    .tracker {
                        background-color: unset;
                        border: unset;
                    }
                }
            }
        }

        .crop-selection {

            .borders > [data-pos]::before {
                background-color: rgba(131 131 131 / 70%);
            }

            .resizers {

                > [data-pos] {
                    width: 1.8em;
                    height: 1.8em;

                    &::before {
                        content: none !important;
                    }
                    img {
                        pointer-events: none;
                        width: 1.8em;
                        height: 1.8em;
                        filter: brightness(1.1);
                    }
                    &:hover {
                        filter: brightness(2);
                    }
                }
                .touch & {
                    > [data-pos] {
                        width: 2.5em;
                        height: 2.5em;
                        img {
                            width: 2.5em;
                            height: 2.5em;
                        }
                    }
                }

                > [data-pos="r"] img,
                > [data-pos="tl"] img {
                    transform: rotate(90deg);
                }
                > [data-pos="b"] img,
                > [data-pos="tr"] img {
                    transform: rotate(180deg);
                }
                > [data-pos="l"] img,
                > [data-pos="br"] img {
                    transform: rotate(270deg);
                }

                > [data-pos="t"],
                > [data-pos="tl"],
                > [data-pos="tr"] {
                    top: 0 !important;
                }
                > [data-pos="b"],
                > [data-pos="bl"],
                > [data-pos="br"] {
                    bottom: 0 !important;
                }

                > [data-pos="l"],
                > [data-pos="tl"],
                > [data-pos="bl"] {
                    left: 0 !important;
                }
                > [data-pos="r"],
                > [data-pos="tr"],
                > [data-pos="br"] {
                    right: 0 !important;
                }

                > [data-pos="l"],
                > [data-pos="r"] {
                    margin-top: -0.85em !important;
                }
                > [data-pos="t"],
                > [data-pos="b"] {
                    margin-left: -0.85em !important;
                }

                /* this is important for zooming (handles are scaled via transform) to avoid distance from border to handle */
                > [data-pos="t"] {
                    transform-origin: top;
                }
                > [data-pos="tl"] {
                    transform-origin: top left;
                }
                > [data-pos="tr"] {
                    transform-origin: top right;
                }
                > [data-pos="b"] {
                    transform-origin: bottom;
                }
                > [data-pos="bl"] {
                    transform-origin: bottom left;
                }
                > [data-pos="br"] {
                    transform-origin: bottom right;
                }
                > [data-pos="l"] {
                    transform-origin: left;
                }
                > [data-pos="r"] {
                    transform-origin: right;
                }
            }

            .rotate-handle {
                display: none;
            }
        }

        .cropping-frame img {
            position: absolute;
        }

        .copy {
            box-shadow: 0 0 0.3em fade(black, 70%);
            > div, img {
                display: none;
            }
        }
        &.activedragging .content {
            visibility: visible !important;
            .crop .selection, .cropping-frame, canvas {
                visibility: hidden;
            }
        }
    }
}

// Popup Menus ================================================================

.io-ox-office-main.popup-container {

    // class ColorSetPicker
    &.color-set-picker .color-box {
        width: 22px;
        height: 22px;
        border: 1px solid fade(black, 20%);
        border-radius: 2px;

        &:not(:first-child) { margin-left: 3px; }
    }

    // class ChartTypePicker
    &.chart-type-picker {

        a.btn {
            height: auto !important;
            padding: 3px !important;

            > img {
                width: 100px;
                height: 80px;
                box-sizing: content-box;
                border: 1px solid var(--border-bright);

                &.pending {
                    opacity: 0;
                }
            }

            > .caption {
                width: 100px;
            }
        }
    }

    // class ChartLabelsMenu
    &.floating-menu.chart-labels-menu .popup-content {
        min-width: 20rem;
    }
}
