/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { CheckBox } from "@/io.ox/office/tk/control/checkbox";
import { TextField } from "@/io.ox/office/tk/control/textfield";

import { DrawingType } from "@/io.ox/office/drawinglayer/utils/drawingutils";
import { DrawingFrameMenu } from "@/io.ox/office/drawinglayer/view/popup/drawingframemenu";

// constants ==================================================================

// menu titles for axis menus
const CHART_AXIS_HEADER_OPTIONS_LABELS = {
    //#. menu title: options for the X axis (categories) in a chart object
    x: gt.pgettext("menu-title", "X axis options"),
    //#. menu title: options for the Y axis (values) in a chart object
    y: gt.pgettext("menu-title", "Y axis options"),
    //#. menu title: options for the Z axis (deep axis in 3D charts) in a chart object
    z: gt.pgettext("menu-title", "Z axis options")
};

// menu titles for axis menus
const CHART_AXIS_HEADER_TITLE_LABELS = {
    //#. menu title: functions for the X axis title (categories) in a chart object
    x: gt.pgettext("menu-title", "X axis title"),
    //#. menu title: functions for the Y axis title (values) in a chart object
    y: gt.pgettext("menu-title", "Y axis title"),
    //#. menu title: functions for the Z axis title (deep axis in 3D charts) in a chart object
    z: gt.pgettext("menu-title", "Z axis title")
};

// options for text fields for chart titles
const CHART_TITLE_TEXTFIELD_OPTIONS = {
    //#. default placeholder for editing the main title in chart objects
    placeholder: gt.pgettext("chart-format", "Type text to add a title"),
    select: true
};

// class ChartLabelsMenu ======================================================

/**
 * A floating menu with options for axes and labels in a chart object.
 *
 * @param {EditView} docView
 *  The document view that contains this instance.
 */
export class ChartLabelsMenu extends DrawingFrameMenu {

    constructor(docView, config) {

        // base constructor
        //#. menu title: modify settings for chart objects
        super(docView, DrawingType.CHART, gt.pgettext("menu-title", "Labels & Axes"), config);

        // add marker class for secific formatting
        this.$el.addClass("chart-labels-menu");

        // add base menu items
        this.addSection("maintitle", gt.pgettext("menu-title", "Chart title"));
        this.addControl("drawing/chart/title/text", new TextField(CHART_TITLE_TEXTFIELD_OPTIONS));

        // add menu items for X and Y axis
        ["x", "y"].forEach(axisId => {

            // the base path of axis items
            const itemKey = key => `drawing/chart/axis/${axisId}/${key}`;

            this.addSection(`axistitle-${axisId}`, CHART_AXIS_HEADER_TITLE_LABELS[axisId]);
            this.addControl(itemKey("title/text"), new TextField(CHART_TITLE_TEXTFIELD_OPTIONS));

            this.addSection(`axisoptions-${axisId}`, CHART_AXIS_HEADER_OPTIONS_LABELS[axisId]);
            //#. (check box) show/hide the caption labels (e.g. the scaling numbers) on an axis in chart objects
            this.addControl(itemKey("labels/visible"), new CheckBox({ label: gt.pgettext("chart-format", "Labels"), boxed: true }));
            //#. (check box) show/hide the axis line in chart objects
            this.addControl(itemKey("line/visible"), new CheckBox({ label: gt.pgettext("chart-format", "Axis line"), boxed: true }));
            //#. (check box) show/hide the grid lines for a specific direction in chart objects
            this.addControl(itemKey("grid/visible"), new CheckBox({ label: gt.pgettext("chart-format", "Grid lines"), boxed: true }));
        });

        // scroll to chart object when showing the menu
        this.on("popup:beforeshow", () => {
            if (this.drawingFrame) {
                const drawingModel = docView.getActiveGridPane().drawingRenderer.getDrawingModelForFrame(this.drawingFrame);
                docView.scrollToDrawingFrame(drawingModel.getPosition());
            }
        });
    }
}
