/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { dict } from "@/io.ox/office/tk/algorithms";
import { KeyCode } from "@/io.ox/office/tk/dom";

import { FloatingMenu } from "@/io.ox/office/baseframework/view/popup/floatingmenu";
import { getDrawingType } from "@/io.ox/office/drawinglayer/view/drawingframe";

// class DrawingFrameMenu =====================================================

/**
 * A floating menu attached to the frame node of a specific drawing object.
 *
 * @param {EditView} docView
 *  The document view that contains this instance.
 *
 * @param {DrawingType|DrawingType[]} drawingTypes
 *  The types of drawing objects supported by this menu.
 */
export class DrawingFrameMenu extends FloatingMenu {

    constructor(docView, drawingTypes, titleLabel, config) {

        // base constructor
        super(docView, titleLabel, {
            anchorPadding: 6,
            ...config
        });

        // the current drawing frame used as menu anchor
        /*protected*/ this.drawingFrame = null;

        // convert space-separated list of drawing object types to flag set
        /*private*/ this._drawingTypeSet = dict.fill(drawingTypes, true);

        // hide the menu automatically in read-only mode
        this.listenTo(this.docApp, "docs:editmode:leave docs:beforequit", () => this.hide());

        // adjust position and visibility of the menu according to the current selection
        this.listenTo(this.docView, "render:drawingselection", this._renderDrawingSelectionHandler);

        // attach popup anchor to drawin frame everytime the menu will be shown
        // (moving the floating menu will switch to detached floating mode afterwards)
        this.on("popup:beforeshow", () => {
            this.setAnchor(this.drawingFrame, {
                anchorBorder: ["right", "left"],
                anchorAlign: "center"
            });
        });

        this.on("popup:hide", () => {
            this.docApp.docController.update();
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the position of the menu, after the drawing selection has
     * changed. If no matching drawing object is selected, the menu will be
     * hidden.
     */
    /*private*/ _renderDrawingSelectionHandler(drawingFrames) {

        // do nothing in read-only mode
        if (!this.docView.isEditable()) { return; }

        // hide menu if selection is anything but a single drawing object of the specified type (TODO: support multi-selections?)
        if ((drawingFrames.length === 1) && (getDrawingType(drawingFrames[0]) in this._drawingTypeSet)) {

            // workaround for Bug 51392, normal group event did not work as expected
            if (this.isVisible()) {
                this.$body.find(".group.text-field.focused input").trigger(new $.Event("keydown", { keyCode: KeyCode.ESCAPE }));
                this.show();
            }

            this.drawingFrame = drawingFrames[0];
            this.refresh({ immediate: true });
        } else {
            this.drawingFrame = null;
            this.hide();
        }
    }
}
