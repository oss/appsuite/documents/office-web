/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math, coord } from '@/io.ox/office/tk/algorithms';
import { Rectangle } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { convertLengthToHmm, getBooleanOption, getFunctionOption, getNumberOption, getObjectOption, getOption } from '@/io.ox/office/tk/utils';

import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { getPresetForWidth, getWidthForPreset } from '@/io.ox/office/editframework/utils/border';
import { insertAttribute, MINOR_FONT_KEY } from '@/io.ox/office/editframework/utils/attributeutils';

// TODO: utils module should not import a view module
import { DrawingType, getPresetShape, isPresetConnectorId, getPresetAspectRatio } from '@/io.ox/office/drawinglayer/view/drawinglabels';

// re-exports =================================================================

export { DrawingType };

// types ======================================================================

/**
 * Directions for reordering drawing models in their collection.
 */
export const DrawingOrderType = {

    /**
     * The drawing object will be moved to the front (above all siblings).
     */
    FRONT: 'front',

    /**
     * The drawing object will be moved before its nearest sibling.
     */
    FORWARD: 'forward',

    /**
     * The drawing object will be moved behind its nearest sibling.
     */
    BACKWARD: 'backward',

    /**
     * The drawing object will be moved to the back (below all siblings).
     */
    BACK: 'back'
};

// constants ==================================================================

const PI_180 = Math.PI / 180;

/**
 * A list of all supported drawing types
 */
export const DRAWING_TYPES = [
    "image",
    "shape",
    "group",
    "connector",
    "diagram",
    "chart",
    "table",
    "ole",
    "horizontal_line",
    "note",
    "undefined"
];

/**
 * The default drawing type
 */
export const DEFAULT_DRAWING_TYPE = "undefined";

// class TrackingRectangle ====================================================

export class TrackingRectangle extends Rectangle {
    constructor(left, top, width, height, shiftX, shiftY) {
        super(left, top, width, height);
        this.origShift = math.radius(shiftX, shiftY);
        this.reverseX = shiftX < 0;
        this.reverseY = shiftY < 0;
    }
}

// private functions ==========================================================

/**
 * Helper function to calculate mixed color for multiple drawing objects.
 */
function resolveSolidColor(attrs) {
    return (attrs.type === 'none') ? null : (attrs.type === 'solid') ? attrs.color : Color.AUTO;
}

/**
 * Rotates a point with the specified angle around a center point. Helper
 * function expecting precalculated sine and cosine for the rotation angle.
 */
function rotatePoint(cx, cy, x, y, cosA, sinA) {
    var dx = x - cx, dy = y - cy;
    return { left: cx + cosA * dx + sinA * dy, top: cy + cosA * dy - sinA * dx };
}

// public functions ===========================================================

/**
 * Sorts the passed array of positions of drawing objects. The positions of
 * embedded drawings will be kept behind the positions of their parent
 * drawings.
 *
 * @param {Position[]} positions
 *  An array with drawing positions to be sorted.
 *
 * @returns {Position[]}
 *  A new array containing the passed positions, sorted in ascending order.
 */
export function sortDrawingPositions(positions) {
    return positions.slice().sort(comparePositions);
}

/**
 * Sorts the passed array of positions of drawing objects in-place, and
 * removes the positions of all embedded drawings whose parent drawing
 * positions are contained in the array too.
 *
 * @param {Position[]} positions
 *  An array with drawing positions to be optimized.
 *
 * @returns {Position[]}
 *  A new array with the sorted and reduced positions.
 */
export function optimizeDrawingPositions(positions) {

    // array index for deleting positions from the array
    var index = 0;
    // array elements while deleting positions
    var pos1 = null, pos2 = null;

    // first, sort the positions
    positions = sortDrawingPositions(positions);

    // remove positions of embedded drawings whose parent position is contained too
    while (index + 1 < positions.length) {
        pos1 = positions[index];
        pos2 = positions[index + 1];
        if ((pos1.length < pos2.length) && _.isEqual(pos1, pos2.slice(0, pos1.length))) {
            positions.splice(index + 1, 1);
        } else {
            index += 1;
        }
    }

    return positions;
}

export function isTwoPointShapeAttributes(geoAttrs) {
    return _.isObject(geoAttrs) && (
        (_.isString(geoAttrs.presetShape) && isPresetConnectorId(geoAttrs.presetShape)) ||
        (_.isArray(geoAttrs.pathList) && _.isObject(geoAttrs.pathList[0]) && geoAttrs.pathList[0].lineSelection)
    );
}

/**
 * Returns whether a drawing object with the passed type identifier and
 * geometry attributes is a line/connector shape (i.e. it will be selected
 * in two-point mode instead of the standard rectangle mode).
 * In the pathList can also be the 'lineSelection' attribute specified (54045).
 *
 * @param {String} drawingType
 *  The type identifier of a drawing object.
 *
 * @param {Object|Null} geoAttrs
 *  An (incomplete) attribute map with geometry attributes. May be null to
 *  indicate missing geometry data.
 *
 * @returns {Boolean}
 *  Whether a drawing object with the passed type identifier and geometry
 *  attributes is a line/connector shape.
 */
export function isTwoPointShape(drawingType, geoAttrs) {
    return (drawingType === DrawingType.CONNECTOR) || (drawingType === DrawingType.SHAPE && isTwoPointShapeAttributes(geoAttrs));
}

/**
 * Returns whether a drawing object with the passed geometry attributes
 * describes an not-closed single path shape.
 *
 * @param {Object|Null} geoAttrs
 *  An (incomplete) attribute map with geometry attributes. May be null to
 *  indicate missing geometry data.
 *
 * @returns {Boolean}
 *  Whether a drawing object with the passed geometry attributes contains
 *  only one path that NOT ends with the command 'close'.
 */
export function isOpenSinglePathShape(geoAttrs) {
    var pathList = (geoAttrs && geoAttrs.pathList) || null;
    return pathList && pathList.length === 1 && _.isArray(pathList[0].commands) && _.last(pathList[0].commands).c !== 'close';
}

/**
 * Returns whether a drawing object with the passed geometry attributes
 * can get line endings assigned.
 *
 * @param {Object|Null} geoAttrs
 *  An (incomplete) attribute map with geometry attributes. May be null to
 *  indicate missing geometry data.
 *
 * @returns {Boolean}
 *  Whether a drawing object with the passed geometry attributes
 *  can get line head or line tail assigned.
 */
export function isLineEndingShape(geoAttrs) {
    return isPresetConnectorId(geoAttrs.presetShape) || isOpenSinglePathShape(geoAttrs);
}

/**
 * Returns whether the passed line attributes represent a visible border.
 *
 * @param {Object|Null} lineAttrs
 *  An (incomplete) attribute map with line attributes. May be null to
 *  indicate missing line formatting (invisible border).
 *
 * @returns {Boolean}
 *  Whether the passed line attributes represent a visible border.
 */
export function hasVisibleBorder(lineAttrs) {
    return !!lineAttrs && (typeof lineAttrs.type === 'string') && (lineAttrs.type !== 'none');
}

/**
 * Returns the identifier of a predefined border style for the passed line
 * attributes.
 *
 * @param {Object|Null} lineAttrs
 *  An (incomplete) attribute map width line attributes. May be null to
 *  indicate missing line formatting (invisible border).
 *
 * @returns {String}
 *  The identifier of a predefined border style, as string with two tokens
 *  separated by a colon character. The first token represents the line
 *  style (or 'none' for invisible lines), the second token represents the
 *  line width (one of 'hair', 'thin', 'medium', or 'thick').
 */
export function getPresetBorder(lineAttrs) {

    // special value 'none:none', if line attributes are not not supported
    // by the drawing object, or if the line style is set to invisible
    if (!hasVisibleBorder(lineAttrs)) { return 'none:none'; }

    return lineAttrs.style + ':' + getPresetForWidth(lineAttrs.width);
}

/**
 * Returns some line attributes for the passed predefined border style.
 *
 * @param {String} preset
 *  The identifier of a predefined border style, as returned by the
 *  function `getPresetBorder()`.
 *
 * @returns {Object}
 *  An attribute map with line attributes, containing the properties
 *  'type' (set to 'none' or 'solid'), 'style' (line style), and 'width'
 *  (line width in 1/100 of millimeters).
 */
export function resolvePresetBorder(preset) {

    // the tokens in the passed preset style
    var tokens = preset.split(':');

    // return attributes, if the preset has been split into two non-empty strings
    if ((tokens.length === 2) && (tokens[0].length > 0) && (tokens[1].length > 0)) {
        var width = getWidthForPreset(tokens[1]);
        return ((tokens[0] === 'none') || (width === 0)) ? { type: 'none' } : { type: 'solid', style: tokens[0], width };
    }

    globalLogger.warn('resolvePresetBorder: invalid preset style "' + preset + '"');
    return { type: 'none' };
}

/**
 * Returns the identifier of a predefined arrow start/end types for the passed line
 * attributes.
 *
 * @param {Object|Null} lineAttrs
 *  An (incomplete) attribute map width line attributes. May be null to
 *  indicate missing line formatting (invisible border).
 *
 * @returns {String}
 *  The identifier of a predefined arrow start/end types, as string with two tokens
 *  separated by a colon character. The first token represents the start arrow type, the second token represents the
 *  end arrow type.
 */
export function getPresetArrow(lineAttrs) {

    // special value 'none:none', if line attributes are not not supported
    // by the drawing object, or if the line style is set to invisible
    if (!hasVisibleBorder(lineAttrs)) { return 'none:none'; }

    return lineAttrs.headEndType + ':' + lineAttrs.tailEndType;
}

/**
 * Returns the models of all drawing objects contained in the passed array
 * that pass the specified truth test. If the array contains group objects,
 * the models of all matching embedded objects will be returned too.
 *
 * @param {Array<DrawingModel>} drawingModels
 *  An array with the drawing models to be processed.
 *
 * @param {Function} predicate
 *  A predicate callback function that returns whether a specific drawing
 *  model will be added to the result. Receives the current drawing model
 *  as first parameter.
 *
 * @param {Object} [context]
 *  The calling context for the callback function.
 *
 * @returns {Array<DrawingModel>}
 *  The models of all matching drawing objects.
 */
export function resolveModelsDeeply(drawingModels, predicate, context) {

    const resultModels = [];

    const resolveModel = drawingModel => {
        if (predicate.call(context, drawingModel)) {
            resultModels.push(drawingModel);
        }
        if (drawingModel.drawingType === DrawingType.GROUP) {
            drawingModel.forEachChildModel(resolveModel);
        }
    };

    drawingModels.forEach(resolveModel);

    return resultModels;
}

/**
 * Returns a specific data value for the specified drawing objects, if all
 * of them have the same value.
 *
 * @param {Array<DrawingModel>} drawingModels
 *  An array with the drawing models to be processed.
 *
 * @param {Function} callback
 *  A callback function that returns the data value for a single drawing
 *  model. Receives the current drawing model as first parameter. Must
 *  return the desired data value for that drawing model, or may return
 *  null to indicate to ignore the drawing object.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Function} [options.equals=_.isEqual]
 *      A predicate that returns whether two data values are equal.
 *  - {Any} [options.empty=null]
 *      A default value that will be returned if no valid data value has
 *      been returned at all from the callback function.
 *
 * @returns {Any|Null}
 *  The resulting unique value of the specified drawing objects, if all of
 *  them have the same; otherwise null to indicate a mixed state.
 */
export function getMixedValue(drawingModels, callback, options) {

    // default value for empty selections
    var emptyValue = getOption(options, 'empty', null);

    // early exit without valid drawing positions
    if (drawingModels.length === 0) { return emptyValue; }

    // comparison predicate function
    var equalsPred = getFunctionOption(options, 'equals', _.isEqual);
    // cache for the first non-null return value of the callback function
    var resultValue = null;

    // check that all drawing objects have the same value
    var unique = drawingModels.every(function (drawingModel) {

        // ignore null values returned by the callback function
        var currValue = callback(drawingModel);
        if (currValue === null) { return true; }

        // cache the first non-null value as result value
        if (resultValue === null) {
            resultValue = currValue;
            return true;
        }

        // otherwise, compare the values
        return equalsPred(resultValue, currValue);
    });

    // return the result value, if all values are equal
    return !unique ? null : (resultValue === null) ? emptyValue : resultValue;
}

/**
 * Returns the unique value of a formatting attribute of the specified
 * drawing objects, if available.
 *
 * @param {Array<DrawingModel>} drawingModels
 *  An array with the drawing models to be processed.
 *
 * @param {String} attrFamily
 *  The name of the attribute family.
 *
 * @param {String} attrName
 *  The name of the attribute.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Function} [options.equals=_.isEqual]
 *      A predicate that returns whether two data values are equal.
 *  - {Any} [options.empty=null]
 *      A default value that will be returned if no valid data value has
 *      been returned at all from the callback function.
 *
 * @returns {Any|Null}
 *  The unique value of a formatting attribute of the specified drawing
 *  objects, if available; otherwise null to indicate a mixed state.
 */
export function getMixedAttributeValue(drawingModels, attrFamily, attrName, options) {
    return getMixedValue(drawingModels, function (drawingModel) {
        return drawingModel.getMergedAttributeSet(true)[attrFamily][attrName];
    }, options);
}

/**
 * Returns the unique preset border style of the specified drawing objects,
 * if available.
 *
 * @param {Array<DrawingModel>} drawingModels
 *  An array with the drawing models to be processed.
 *
 * @returns {String|Null}
 *  The unique preset border style of the specified drawing objects, if
 *  available; otherwise null to indicate a mixed state.
 */
export function getMixedPresetBorder(drawingModels) {
    return getMixedValue(drawingModels, function (drawingModel) {
        return getPresetBorder(drawingModel.getMergedAttributeSet(true).line);
    });
}

/**
 * Returns the unique border color of the specified drawing objects, if
 * available.
 *
 * @param {Array<DrawingModel>} drawingModels
 *  An array with the drawing models to be processed.
 *
 * @returns {Object|Null}
 *  The unique JSON border color of the specified drawing objects, if
 *  available; otherwise null to indicate a mixed state.
 */
export function getMixedBorderColor(drawingModels) {
    return getMixedValue(drawingModels, function (drawingModel) {
        return resolveSolidColor(drawingModel.getMergedAttributeSet(true).line);
    }, { equals: Color.isEqual, empty: Color.AUTO });
}

/**
 * Returns the unique fill color of the specified drawing objects, if
 * available.
 *
 * @param {Array<DrawingModel>} drawingModels
 *  An array with the drawing models to be processed.
 *
 * @returns {Color|Null}
 *  The unique JSON fill color of the specified drawing objects, if
 *  available; otherwise null to indicate a mixed state.
 */
export function getMixedFillColor(drawingModels) {
    return getMixedValue(drawingModels, function (drawingModel) {
        return resolveSolidColor(drawingModel.getMergedAttributeSet(true).fill);
    }, { equals: Color.isEqual, empty: Color.AUTO });
}

/**
 * Creates the value of the CSS attribute 'transform' for the passed
 * rotation angle and flipping flags.
 *
 * @param {Number} degrees
 *  The rotation angle, in degrees.
 *
 * @param {Boolean} flipH
 *  Whether the element is flipped horizontally.
 *
 * @param {Boolean} flipV
 *  Whether the element is flipped vertically.
 *
 * @returns {String}
 *  The value of the CSS attribute 'transform' for the passed rotation
 *  angle and flipping flags.
 */
export function getCssTransform(degrees, flipH, flipV) {
    var transform = degrees ? ('rotate(' + degrees + 'deg)') : '';
    if (flipH) { transform += transform.length ? ' scaleX(-1)' : 'scaleX(-1)'; }
    if (flipV) { transform += transform.length ? ' scaleY(-1)' : 'scaleY(-1)'; }
    return transform;
}

/**
 * Returns the formatting attributes needed to transform a drawing obejct
 * with the passed merged attribute set.
 *
 * @param {Object} drawingAttrs
 *  The merged (complete) drawing attributes of a drawing object (i.e., the
 *  attribute map of the attribute family 'drawing').
 *
 * @param {Number} angle
 *  The rotation angle to be added to the current rotation angle of the
 *  drawing object, in degrees.
 *
 * @param {Boolean} flipH
 *  Whether to flip the drawing object horizontally.
 *
 * @param {Boolean} flipV
 *  Whether to flip the drawing object vertically.
 *
 * @param {Boolean} isODF
 *  Whether the document format is odf or not.
 *
 * @returns {Object|Null}
 *  The drawing attributes needed to transform the drawing object with the
 *  passed settings (i.e., the attribute map of the attribute family
 *  'drawing'); or null, if the drawing object does not need to be modified
 *  at all.
 */
export function transformAttributeSet(drawingAttrs, angle, flipH, flipV, isODF) {

    // old values of the transformation attributes
    var oldAngle = drawingAttrs.rotation;
    var oldFlipH = drawingAttrs.flipH;
    var oldFlipV = drawingAttrs.flipV;

    // new values of the transformation attributes
    var newAngle = math.modulo((oldAngle + angle) * ((flipH !== flipV) ? -1 : 1), 360);
    var newFlipH = flipH !== oldFlipH;
    var newFlipV = flipV !== oldFlipV;

    // the new drawing attributes
    drawingAttrs = {};
    if (oldAngle !== newAngle) { drawingAttrs.rotation = newAngle; }
    if (oldFlipH !== newFlipH) { drawingAttrs.flipH = newFlipH; }
    if (oldFlipV !== newFlipV && !isODF) { drawingAttrs.flipV = newFlipV; }
    if (isODF && flipV) {
        drawingAttrs.rotation = math.modulo(newAngle + 180, 360);
        drawingAttrs.flipH = !newFlipH;
    }

    // return null, if nothing changes
    return _.isEmpty(drawingAttrs) ? null : drawingAttrs;
}

/**
 * Checks if passed angle is inside predefined area, used by some MS algorithms.
 * (Called that as the boundaries form the shape of butterfly).
 *
 * @param {Number} angle
 *  The angle in degrees to be checked.
 *
 * @param {Boolean} flipH
 *  If shape is mirrored (flipped) horizontally.
 *
 * @param {Boolean} flipV
 *  If shape is mirrored (flipped) horizontally.
 *
 * @returns {Boolean}
 *  Whether the angle is inside special area bounded by predifined angles.
 */
export function isAngleInButterflyArea(angle, flipH, flipV) {
    angle = math.modulo((flipH !== flipV) ? -angle : angle,  180);
    return (angle >= 45) && (angle < 135);
}

/**
 * Returns whether the transformation attributes in the passed attribute
 * map will lead to swapped width and height of the drawing object.
 *
 * @param {Object} drawingAttrs
 *  A map with drawing attributes. Must contain the properties 'rotation',
 *  'flipH', and 'flipV'.
 *
 * @returns {Boolean}
 *  Whether the transformation attributes will lead to swapped width and
 *  height of the drawing object.
 */
export function hasSwappedDimensions(drawingAttrs) {
    return isAngleInButterflyArea(drawingAttrs.rotation, drawingAttrs.flipH, drawingAttrs.flipV);
}

/**
 * Rotates a point with the specified angle around a center point.
 *
 * @param {Number} cx
 *  The X coordinate of the center point.
 *
 * @param {Number} cy
 *  The Y coordinate of the center point.
 *
 * @param {Number} x
 *  The X coordinate of the point to be rotated.
 *
 * @param {Number} y
 *  The Y coordinate of the point to be rotated.
 *
 * @param {Number} angle
 *  The angle to rotate the point, in degrees.
 *
 * @returns {Object}
 *  The position of the rotated point, in the properties 'left' and 'top'.
 */
export function rotatePointWithAngle(cx, cy, x, y, angle) {
    var rad = angle * PI_180;
    return rotatePoint(cx, cy, x, y, Math.cos(rad), Math.sin(rad));
}

/**
 * Returns data for rotating a rectangle with the given angle around its
 * center point.
 *
 * @param {Rectangle|Object} rect
 *  The original unrotated location of the rectangle.
 *
 * @param {Number} angle
 *  The angle to rotate the rectangle, in degrees.
 *
 * @returns {Object}
 *  A descriptor object with information for the bounding box containing
 *  the rotated rectangle, with the following properties:
 *  - {Number} left
 *      The X coordinate of the left border of the bounding box.
 *  - {Number} top
 *      The Y coordinate of the top border of the bounding box.
 *  - {Number} right
 *      The X coordinate of the right border of the bounding box.
 *  - {Number} bottom
 *      The Y coordinate of the bottom border of the bounding box.
 *  - {Number} width
 *      The width of the bounding box.
 *  - {Number} height
 *      The height of the bounding box.
 *  - {Object} leftTopPoint
 *      The position of the rotated top-left corner, in the properties
 *      'left' and 'top'.
 */
export function getRotatedDrawingPoints(rect, angle) {

    rect = Rectangle.from(rect);

    var cx = rect.centerX(), cy = rect.centerY();
    var rad = angle * PI_180, cosA = Math.cos(rad), sinA = Math.sin(rad);
    var points = [rect.topLeft(), rect.topRight(), rect.bottomRight(), rect.bottomLeft()].map(function (point) {
        return rotatePoint(cx, cy, point.x, point.y, cosA, sinA);
    });

    // get the bounding coordinates
    var boundL = Infinity;
    var boundR = -Infinity;
    var boundT = Infinity;
    var boundB = -Infinity;
    points.forEach(function (point) {
        boundL = Math.min(boundL, point.left);
        boundR = Math.max(boundR, point.left);
        boundT = Math.min(boundT, point.top);
        boundB = Math.max(boundB, point.top);
    });

    return {
        left: boundL,
        top: boundT,
        right: boundR,
        bottom: boundB,
        width: boundR - boundL,
        height: boundB - boundT,
        leftTopPoint: points[0]
    };
}

/**
 * Returns the width and height for the specified predefined shape object
 * that will fit in the provided dimensions. The standard dimension for all
 * shapes is a square. Special cases are the format portrait and landscape.
 *
 * @param {String} presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @param {Number} width
 *  The default width in which the shape should fit.
 *
 * @param {Number} height
 *  The default height in which the shape should fit.
 *
 * @returns {Object}
 *  Returning an Object with the resulting width and height for the shape.
 */
export function createDefaultSizeForShapeId(presetShapeId, width, height) {

    // get the resulting aspect ratio
    var aspectRatio = getPresetAspectRatio(presetShapeId);

    // shorten either the width, or the height passed to this method
    if (aspectRatio < 1) {
        width = Math.round(width * aspectRatio);
    } else if (aspectRatio > 1) {
        height = Math.round(height / aspectRatio);
    }

    return { width, height };
}

/**
 * Creates the default formatting attributes for a new shape object to be
 * inserted into a document.
 *
 * @param {Object} docModel
 *  The document model.
 *
 * @param {String} presetShapeId
 *  The identifier of a predefined shape type.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {String|Array<String>|Null} [options.targets=null]
 *      A name or an array of names to specify the target theme. If omitted
 *      or set to null, the active theme of the document will be returned.
 *  - {Boolean} [options.flipH=false]
 *      If set to true, the 'flipH' attribute will be added causing to flip
 *      the shape horizontally.
 *  - {Boolean} [options.flipV=false]
 *      If set to true, the 'flipV' attribute will be added causing to flip
 *      the shape vertically.
 *  - {Object} [options.connector=null]
 *      If exists, contains attribute set for connector properties,
 *      like start and end id and index.
 *
 * @returns {Object|Null}
 *  An attribute set with the default formatting attributes for the
 *  specified shape type; or null, if the passed shape type identifier is
 *  invalid.
 */
export function createDefaultShapeAttributeSet(docModel, presetShapeId, options) {

    // predefined settings for the shape object
    var presetData = getPresetShape(presetShapeId);
    if (!presetData) { return null; }

    // the resulting attribute set
    var attrSet = {};
    // the theme targets
    var targets = getOption(options, 'targets', null);
    // start and end id and index of connector shape
    var connector = getOption(options, 'connector', null);

    // the fill attributes for the shape (some shapes are not filled)
    if (!isPresetConnectorId(presetShapeId)) {
        attrSet.fill = presetData.noFill ? { type: 'none' } : { type: 'solid', color: docModel.createSchemeColor('accent1', 0, targets) };
    }

    // geometry attributes
    attrSet.geometry = { presetShape: presetData.id };

    // flipping attributes
    if (getBooleanOption(options, 'flipH', false)) {
        insertAttribute(attrSet, 'drawing', 'flipH', true);
    }
    if (getBooleanOption(options, 'flipV', false)) {
        insertAttribute(attrSet, 'drawing', 'flipV', true);
    }

    // the line attributes (thin solid line, darker scheme color 1, additional arrow attributes)
    attrSet.line = resolvePresetBorder('solid:thin');
    attrSet.line.color = docModel.createSchemeColor('accent1', -50, targets, { drawing: true });
    _.extend(attrSet.line, presetData.lineAttrs);

    // character formatting (dark text color for shapes without fill color; in ODF files the text color is always dark)
    var odf = docModel.getApp().isODF();
    var colorName = (odf || presetData.outerText) ? 'dark1' : 'light1';

    if (odf) {
        attrSet.character = {};
        attrSet.character.fontName = docModel.resolveFontName(MINOR_FONT_KEY);
        attrSet.character.fontSize = 11;
    } else {
        attrSet.character = { color: docModel.createSchemeColor(colorName, 0, targets) }; // no text color in odf shapes (54343)
    }

    if (!_.isEmpty(connector)) { attrSet.connector = connector; }

    return attrSet;
}

/**
 * Get the default attributes for the paragraph in a new default shape.
 *
 * @param {Object} docModel
 *  The document model.
 *
 * @returns {Object|Null}
 *  An attribute set with the default formatting attributes for the
 *  paragraph in a shape. Or null, if there are no paragraph attributes.
 */
export function getParagraphAttrsForDefaultShape(docModel) {
    return docModel.getApp().isSpreadsheetApp() ? null : { paragraph: { alignment: 'center' } };
}

/**
 * Returns the location of a selection rectangle resulting from the current
 * state of a tracking cycle.
 *
 * @param {Point} anchor
 *  The coordinates of the tracking start pixel.
 *
 * @param {Point} current
 *  The coordinates of the current tracking pixel.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Partial<ModifierKeys>} [options.modifiers]
 *      An event object used to evaluate properties for modifier keys used
 *      for locked aspect ratio and center mode. With pressed SHIFT key,
 *      the aspect ratio of the resulting rectangle will be locked (see
 *      option 'aspectRatio' below). With pressed CTRL key (or ALT key on
 *      MacOS), the resulting rectangle will expand centered around the
 *      initial anchor point.
 *  - {Boolean} [options.twoPointMode=false]
 *      Whether to operate in two-point mode (used e.g. for line/connector
 *      shapes), instead of the default rectangle mode.
 *  - {Number} [options.aspectRatio=1]
 *      The aspect ratio to be used for locked aspect mode (pressed SHIFT
 *      key), as quotient of width and height (a value greater than 1 will
 *      create a wide rectangle). MUST be a positive floating-point number.
 *  - {Rectangle} [options.boundRect]
 *      The bounding rectangle used to restrict the resulting rectangle to.
 *      This rectangle MUST contain the initial anchor coordinates passed
 *      to this method (the current tracking coordinates may be outside the
 *      bounding rectangle though). If omitted, the resulting rectangle
 *      will not be restricted.
 *
 * @returns {TrackingRectangle}
 *  The resulting rectangle, according to the passed coordinates, with the
 *  specified minimum width and height. If the rectangle cannot be created
 *  with that minimum size according to the passed bounding rectangle, the
 *  according dimension property will be set to zero.on.
 */
export function getTrackingRectangle(anchor, current, options) {

    // shift distance in both directions
    var shift = coord.pointsDiff(anchor, current);

    // tracking modifiers
    var modifiers = getObjectOption(options, 'modifiers', null);
    var twoPointMode = getBooleanOption(options, 'twoPointMode', false);
    var center = !twoPointMode && getBooleanOption(modifiers, _.browser.MacOS ? 'altKey' : 'ctrlKey', false);
    var locked = getBooleanOption(modifiers, 'shiftKey', false);

    // get maximum rectangle sizes in all directions from the anchor offset
    var boundRect = getObjectOption(options, 'boundRect', null);
    var maxSizeL = boundRect ? (anchor.x - boundRect.left + 1) : Infinity;
    var maxSizeR = boundRect ? (boundRect.right() - anchor.x) : Infinity;
    var maxSizeT = boundRect ? (anchor.y - boundRect.top + 1) : Infinity;
    var maxSizeB = boundRect ? (boundRect.bottom() - anchor.y) : Infinity;

    // maximum size of the rectangle from anchor point, according to location of the bounding rectangle
    var maxWidth = center ? Math.min(maxSizeL, maxSizeR) : (shift.x < 0) ? maxSizeL : maxSizeR;
    var maxHeight = center ? Math.min(maxSizeT, maxSizeB) : (shift.y < 0) ? maxSizeT : maxSizeB;

    // the resulting rectangle, with added original shift distance
    var trackingRect = new TrackingRectangle(anchor.x, anchor.y, 0, 0, shift.x, shift.y);

    // return immediately with zero size if the bounding rectangle is invalid
    if ((maxWidth <= 0) || (maxHeight <= 0)) { return trackingRect; }

    // size of the rectangle from anchor point (one pixel more than shift distance,
    // this will include the anchor into the rectangle)
    var rawWidth = Math.abs(shift.x) + 1;
    var rawHeight = Math.abs(shift.y) + 1;

    // adjust target size if aspect ratio is locked
    if (locked) {

        if (twoPointMode) {
            var angle = math.modulo(Math.atan2(rawHeight, rawWidth) / PI_180, 360);
            if ((angle <= 22.5) || ((angle >= 157.5) && (angle <= 202.5)) || (angle >= 337.5)) {
                rawHeight = 1;
            } else if (((angle >= 67.5) && (angle <= 112.5)) || ((angle >= 247.5) && (angle <= 292.5))) {
                rawWidth = 1;
            } else {
                rawWidth = rawHeight = Math.max(rawWidth, rawHeight);
            }
        } else {
            // enlarge the shorter border of the rectangle to match the aspect ratio
            var ratio = getNumberOption(options, 'aspectRatio', 1);
            rawWidth = Math.max(rawWidth, rawHeight * ratio);
            rawHeight = rawWidth / ratio;
        }

        // shrink the rectangle with locked aspect ratio until it fits into the maximum size
        var scale = Math.min(1, maxWidth / rawWidth, maxHeight / rawHeight);
        rawWidth = Math.round(rawWidth * scale);
        rawHeight = Math.round(rawHeight * scale);

    } else {
        rawWidth = Math.min(rawWidth, maxWidth);
        rawHeight = Math.min(rawHeight, maxHeight);
    }

    // update the tracking rectangle
    trackingRect.set(
        (center || (shift.x < 0)) ? (anchor.x - rawWidth + 1) : anchor.x,
        (center || (shift.y < 0)) ? (anchor.y - rawHeight + 1) : anchor.y,
        center ? (rawWidth * 2 - 1) : rawWidth,
        center ? (rawHeight * 2 - 1) : rawHeight
    );

    // enlarge the rectangle, unless two-point mode is active
    if (!twoPointMode) {

        // set rectangle to a minimum width of 5 pixels
        if (trackingRect.width < 5) {
            if (center && (maxSizeL > 0) && (maxSizeR > 0)) {
                trackingRect.left = anchor.x - 2;
                trackingRect.width = 5;
            } else if ((shift.x < 0) && (maxSizeL >= 5)) {
                trackingRect.left = anchor.x - 4;
                trackingRect.width = 5;
            } else if ((shift.x >= 0) && (maxSizeR >= 5)) {
                trackingRect.left = anchor.x;
                trackingRect.width = 5;
            } else {
                trackingRect.left = anchor.x;
                trackingRect.width = 0;
            }
        }

        // set rectangle to a minimum height of 5 pixels
        if (trackingRect.height < 5) {
            if (center && (maxSizeT > 0) && (maxSizeB > 0)) {
                trackingRect.top = anchor.y - 2;
                trackingRect.height = 5;
            } else if ((shift.y < 0) && (maxSizeT >= 5)) {
                trackingRect.top = anchor.y - 4;
                trackingRect.height = 5;
            } else if ((shift.y >= 0) && (maxSizeB >= 5)) {
                trackingRect.top = anchor.y;
                trackingRect.height = 5;
            } else {
                trackingRect.top = anchor.y;
                trackingRect.height = 0;
            }
        }
    }

    return trackingRect;
}

/**
 * Check if given point is inside rectangle, with variation value, if provided.
 *
 * @param {Object} point {x,y}
 * @param {Rectangle} rect
 * @param {Number} [threshold=10]
 *
 * @returns {Boolean}
 */
export function pointInsideRect(point, rect, threshold) {
    var px = point.x;
    var py = point.y;
    var th = threshold || 10; // def threshold of 10

    if (_.isFinite(rect.rotation)) {
        var newP = rotatePointWithAngle(rect.centerX(), rect.centerY(), px, py, rect.rotation);
        px = newP.left;
        py = newP.top;
    }

    return (px >= rect.left - th && px <= rect.right() + th) && (py >= rect.top - th && py <= rect.bottom() + th);
}

/**
 * Check if point A is near point B, with variation value, if provided.
 *
 * @param {Object} point {x,y}
 * @param {Object} snapPoint {x,y}
 * @param {Number} [threshold=15]
 *
 * @returns {Boolean}
 */
export function pointCloseToSnapPoint(point, snapPoint, threshold) {
    var px = point.x;
    var py = point.y;
    var sx = snapPoint.x;
    var sy = snapPoint.y;
    var th = threshold || 15; // def threshold of 15px

    return (Math.abs(px - sx) < th) && (Math.abs(py - sy) < th);
}

/**
 * Check if values a and b are inverted (flipped) after value a is inc/dec by diff value.
 *
 * @param {Number} a
 * @param {Number} b
 * @param {Number} diffA
 * @param {Number} diffB
 *
 * @returns {Boolean}
 */
export function checkConnectorFlip(a, b, diffA, diffB) {
    return (a > b && a + diffA < b + diffB) || (a < b && a + diffA > b + diffB);
}

/**
 * Returns array of unique objects - connectors linked to the given drawing id.
 *
 * @param {Array} allConnectors
 * @param {String} actDrawingId
 *
 * @returns {Array}
 */
export function getUniqueConnections(allConnectors, actDrawingId) {
    var connections = _.filter(allConnectors, function (c) {
        return c.startId === actDrawingId || c.endId === actDrawingId || c.parentStartId === actDrawingId || c.parentEndId === actDrawingId;
    });
    connections = _.uniq(connections, function (c) { return c.attrs.drawing.id; });

    return connections;
}

/**
 * Creating set of options used later for calculating current connector position and size.
 *
 * @param {Object} oldAttrs
 *  Set of attributes before modification.
 * @param {Object} newAttrs
 *  Set of attributes after modification.
 * @param {String} type
 *  Type of modification: move/resize/rotate
 * @param {Boolean} flipH
 *  If active drawing is flipped horizontally.
 * @param {Boolean} flipV
 *  If active drawing is flipped vertically.
 * @param {jQuery} node
 *  Currently active drawing node, to which connector is linked.
 * @param {Number} currentAngle
 *  Current angle of the drawing node.
 * @param {Number} startAngle
 *  If exist, start rotation angle before applying rotation.
 * @param {Number} zoomFactor
 *  Factor for application's current zoom level.
 *
 * @returns {Object}
 */
export function generateRecalcConnectorOptions(oldAttrs, newAttrs, type, flipH, flipV, node, currentAngle, startAngle, zoomFactor) {
    var oldWidth = _.isNumber(oldAttrs.width) ? oldAttrs.width : convertLengthToHmm($(node).width() * zoomFactor, 'px');
    var oldHeight = _.isNumber(oldAttrs.height) ? oldAttrs.height : convertLengthToHmm($(node).height() * zoomFactor, 'px');
    var newLeft = _.isNumber(newAttrs.left) ? newAttrs.left : oldAttrs.left;
    var newTop = _.isNumber(newAttrs.top) ? newAttrs.top : oldAttrs.top;
    var newWidth = _.isNumber(newAttrs.width) ? newAttrs.width : oldWidth;
    var newHeight = _.isNumber(newAttrs.height) ? newAttrs.height : oldHeight;

    return {
        widthScale: newWidth / oldWidth,
        heightScale: newHeight / oldHeight,
        newLeft,
        newTop,
        leftDiff: newLeft - oldAttrs.left,
        topDiff: newTop - oldAttrs.top,
        rotation: currentAngle,
        rotationStart: startAngle,
        activeNode: node,
        drawingFlipH: flipH,
        drawingFlipV: flipV,
        type
    };
}

/**
 * Returns stretched values for left, top, width, height of a image/drawing node,
 * based on the passed stretching properties.
 *
 * @param {*} stretchingObj
 *  Object containing left, top, bottom, right attributes in %.
 * @param {*} nodeWidth
 *  Actual node width.
 * @param {*} nodeHeight
 *  Actual node height.
 *
 * @returns {Object} Calculated left, top, width, height values.
 */
export function getStrechedCoords(stretchingObj, nodeWidth, nodeHeight) {
    var sLeft = stretchingObj.left || 0;
    var sRight = stretchingObj.right || 0;
    var sTop = stretchingObj.top || 0;
    var sBottom = stretchingObj.bottom || 0;

    var tLeft = nodeWidth * (sLeft / 100);
    var tWidth = nodeWidth * ((100 - sRight - sLeft) / 100);
    var tTop = nodeHeight * (sTop / 100);
    var tHeight = nodeHeight * ((100 - sBottom - sTop) / 100);

    return { left: Math.round(tLeft), top: Math.round(tTop), width: Math.round(tWidth), height: Math.round(tHeight) };
}
