/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// types ======================================================================

/**
 * Descriptor for an image to be inserted into the document.
 */
export interface ImageDescriptor {
    url: string;
    name: string;
    transform: Dict | null;
}

/**
 * Enumeration for all available methods how an image can be inserted into a
 * document.
 */
export enum ImageSourceType {

    /**
     * Insert an image from local file system.
     */
    LOCAL = "local",

    /**
     * Insert an image from cloud storage.
     */
    DRIVE = "drive",

    /**
     * Insert an image by specifying a URL.
     */
    URL = "url"
}

// functions ==================================================================

export function hasFileImageExtension(fileName: string): boolean;
export function insertFile(docApp: EditApplication, file: File): JPromise<ImageDescriptor>;

export function isBase64(url: string): boolean;
export function insertDataUrl(docApp: EditApplication, url: string): JPromise<ImageDescriptor>;
export function insertURL(docApp: EditApplication, url: string): JPromise<ImageDescriptor>;
