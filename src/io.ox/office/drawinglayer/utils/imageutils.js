/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';
import gt from 'gettext';

import { settings as coreSettings } from '$/io.ox/core/settings';

import { pick, hash } from '@/io.ox/office/tk/algorithms';
import { FILTER_MODULE_NAME } from '@/io.ox/office/tk/utils/io';
import { convertHmmToCssLength, getIntegerOption } from '@/io.ox/office/tk/utils';

import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import { GENERAL } from '@/io.ox/office/baseframework/utils/errorcontext';
import { INFO_CANNOT_EMBED_IMAGE_MUST_USE_URL } from '@/io.ox/office/baseframework/utils/infostate';

// types ======================================================================

/**
 * Enumeration for all available methods how an image can be inserted into a
 * document.
 */
export const ImageSourceType = {

    /**
     * Insert an image from local file system.
     */
    LOCAL: "local",

    /**
     * Insert an image from cloud storage.
     */
    DRIVE: "drive",

    /**
     * Insert an image by specifying a URL.
     */
    URL: "url"
};

// constants ==============================================================

var IMAGE_EXTENSIONS = {
    jpg:  'image/jpeg',
    jpe:  'image/jpeg',
    jpeg: 'image/jpeg',
    png:  'image/png',
    gif:  'image/gif',
    bmp:  'image/bmp'
};

var RE_IMAGE_URL = /^(?:([a-z]+):)?(\/{0,3})([0-9.\-a-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/i;
var RE_IMAGE_MIME = /^image\/.*/i;

// private static functions ===============================================

/**
 * Returns the real image size of a base 64 data URL.
 *
 * @param {String} url
 *  The URL to check - it must be a base 64 data URL
 *
 * @returns {Number}
 *  The number of bytes used by the decoded image
 */
function calcImageSize(url) {
    var offset = url && url.indexOf('base64,') + 7;
    var padding = 0;
    if (url.endsWith('==')) { padding = 2; }
    if (url.endsWith('=')) { padding = 1; }
    return ((url.length  - offset) * (3 / 4)) - padding;
}

/**
 * calculates SHA256 value of base64 encoded data (SHA256 is then calculated from the decoded data)
 *
 * @param {String} data
 *  The String must contain base64 encoded data. The data length is determined by data.length - offset;
 *  It is not necessary to include '=' padding bytes at the end, the String size is sufficient.
 *
 * @param {Number} offset
 *  Specifies offset to the beginning of base64 encoded data.
 *
 * @returns {Promise<string|null>}
 *  Null is returned, if no base64 encoded data could be found in the data
 *  string. Otherwise the SHA256 value as String is returned.
 *
 */
async function getSHA256fromBase64(data, offset) {
    var dataLength = data.length - offset;
    // data length must be at least two bytes to encode one character (if no pad bytes are used)
    if (dataLength < 2) { return null; }
    return await hash.sha256(data.substring(offset), 'base64');
}

function insDataUrl(app, dataUrl, fileName) {
    var offset = dataUrl && dataUrl.indexOf('base64,') + 7,
        extension = null,
        properties = coreSettings.get('properties');

    if (!_.isString(fileName) || (fileName.length < 1)) {
        if (dataUrl.startsWith('data:image/jpeg')) {
            fileName = 'image.jpg';
        } else {
            fileName = 'image.img';
        }
    }

    // determine size of the base 64 to prevent "uploading" an
    // oversized images
    var size = calcImageSize(dataUrl, offset);
    if ((properties.infostoreMaxUploadSize > 0) && (size > properties.infostoreMaxUploadSize)) {
        app.rejectEditAttempt('image');
        return $.Deferred().reject();
    }

    // wrap native promise to keep jQuerry promise
    return $.when(getSHA256fromBase64(dataUrl, offset)).then(sha256 => {

        // if no hash value could be determined, the loading process must be stopped now.
        if (!sha256) { return $.Deferred().reject(); }

        extension = fileName.substring(fileName.lastIndexOf('.') + 1);

        var params = {
            add_hash: sha256,
            add_ext: extension,
            alt_filename: fileName,
            add_filedata: dataUrl
        };

        return app.sendActionRequest('addfile', params)
            .then(function (data) { return app.sendAddImageIdRequest(data); })
            .fail(function () { app.rejectEditAttempt('image'); });
    });

}

function insFile(app, file) {

    return app.readClientFile(file).then(function (dataUrl) {
        return insDataUrl(app, dataUrl, file.name);
    });
}

/**
 * Inserts a file from OX Drive.
 *
 * @param {EditApplication} app
 *  The application object representing the edited document.
 *
 * @param {Number} fileDescriptor
 *  The infostore descriptor of the file to be inserted
 *
 * @returns {jQuery.Promise}
 *  a Promise which be resolved when the insert action is successful
 */
function insDriveImage(app, fileDescriptor) {

    if (!fileDescriptor) { return $.Deferred().reject().promise(); }

    var params = {
        add_ext: fileDescriptor.filename.substring(fileDescriptor.filename.lastIndexOf('.') + 1),
        add_fileid: fileDescriptor.id,
        add_folderid: fileDescriptor.folder_id,
        alt_filename: fileDescriptor.filename
    };

    return app.sendActionRequest('addfile', params)
        .then(function (data) { return app.sendAddImageIdRequest(data); })
        .fail(function () { app.rejectEditAttempt('image'); });
}

// static class Image =====================================================

/**
 * Provides static helper methods for manipulation and calculation
 * of image nodes.
 */

// static methods ---------------------------------------------------------

/**
 * Checks if a file name has an image type extension.
 *
 * @param {String} file
 *
 * @returns {Boolean}
 *  Returns true if there is an extension and if has been detected
 *  as a image extension, otherwise false.
 */
export function hasFileImageExtension(file) {

    var ext = _.isString(file) ? file.split('.').pop().toLowerCase() : '';
    return ext in IMAGE_EXTENSIONS;
}

/**
 * Extracts the fileName from a hierarchical URI and checks if the extension
 * defines an supported image format.
 *
 * @param {String} uri
 *  A hierarchical uri
 *
 * @returns {Boolean}
 *  TRUE if the uri references a file which has a supported image extensions,
 *  otherwise FALSE.
 */
export function hasUrlImageExtension(uri) {
    return hasFileImageExtension(getFileNameFromImageUri(uri));
}

/**
 * Extracts the fileName from a hierarchical URI.
 *
 * @param {String} uri
 *  A hierarchical uri
 */
export function getFileNameFromImageUri(uri) {

    // Removes the fragment part (#), the query part (?) and chooses the last
    // segment
    var fileName = uri.substring(0, (uri.indexOf('#') === -1) ? uri.length : uri.indexOf('#'));
    fileName = fileName.substring(0, (fileName.indexOf('?') === -1) ? fileName.length : fileName.indexOf('?'));
    fileName = fileName.substring(fileName.lastIndexOf('/') + 1, fileName.length);
    return fileName;
}

export function getFileNameWithoutExt(url) {
    var filename = getFileNameFromImageUri(url);
    var pIndex = filename.lastIndexOf('.');
    return filename.substring(0, pIndex);
}

/**
 * Returns the document URL of the cacheserver for relative URLs,
 * or the given URL for 'bas64 data URLs' or 'absolute / external URLs'.
 *
 * @param {BaseApplication} app
 *
 * @param {String} url
 *
 * @param {Object} [options]
 *  Optional parameters, refer to BaseApplication.getServerModuleUrl()
 */
export function getFileUrl(app, url, options) {

    // base64 data URL, or absolute / external URL
    if (isBase64(url) || isAbsoluteUrl(url)) { return url; }

    // document URL
    return app.getServerModuleUrl(FILTER_MODULE_NAME, { action: 'getfile', get_filename: encodeURIComponent(url) }, options);
}

/**
 * Returns the MIME type for the given image uri according to the image file name extension.
 *
 * @param {String} uri
 *  A hierarchical uri
 *
 * @returns {String}
 *  The MIME type or an empty String.
 */
export function getMimeTypeFromImageUri(uri) {
    var ext = getFileExtensionFromImageUri(uri);
    return IMAGE_EXTENSIONS[ext] || '';
}

export function getFileExtensionFromImageUri(uri) {
    var fileName = getFileNameFromImageUri(uri);
    return (fileName ? fileName.split('.').pop().toLowerCase() : '');
}

/**
 * Returns true for base 64 data URLs.
 *
 * @param {String} url
 *  The URL to check.
 *
 * @returns {Boolean}
 *  Whether the URL is a base 64 data URL.
 */
export function isBase64(url) {
    return _.isString(url) && url.startsWith('data:image');
}

/**
 * Returns true for absolute URLs.
 *
 * @param {String} url
 *  The URL to check.
 *
 * @returns {Boolean}
 *  Whether the URL is an absolute URL.
 */
export function isAbsoluteUrl(url) {
    return (_.isString(url) && /:\/\//.test(url));
}

/**
 * Returns true if 'type' is an image MIME type.
 *
 * @param {String} type
 *  The MIME type.
 *
 * @returns {Boolean}
 *  Whether the MIME type is an image MIME type.
 */
export function isImageMimeType(type) {
    return RE_IMAGE_MIME.test(type);
}

/**
 * Returns true if 'type' is a supported image MIME type.
 *
 * @param {String} type
 *  The MIME type.
 *
 * @returns {Boolean}
 *  Whether the MIME type is a supported image MIME type.
 */
export function isSupportedImageMimeType(type) {
    return (_.values(IMAGE_EXTENSIONS).indexOf(type) > -1);
}

/**
 * Post-processes the attributes of insert drawing operations. Based on the
 * drawing attributes and the editor instance (the operation is applied to):
 *  - An image URL to an external location is always used.
 *  - The image media URL is used if the operation is applied within the
 *      same editor instance.
 *  - The BASE64 image data is used if the operation is applied in another
 *      editor instance.
 * Finally, the attributes will contain either the imageUrl or the
 * imageData attribute.
 *
 * @param {Object} imageAttrs
 *  The (incomplete) drawing attributes of the operation.
 *
 * @param {String} fileId
 *  The identifier of the file where the operation is applied to.
 */
export function postProcessOperationAttributes(imageAttrs, fileId) {

    //imageAttrs.fileId from the clipboard is encoded because of Drive-file-ids (66/66 -> 66%2F66)
    if ((imageAttrs.sessionId === ox.session) && _.isString(imageAttrs.imageUrl) && (imageAttrs.fileId === fileId || imageAttrs.fileId === encodeURIComponent(fileId))) {

        // target is the same editor instance: use the image URL and remove the image BASE64 data
        delete imageAttrs.imageData;

    } else if (isAbsoluteUrl(imageAttrs.imageUrl)) {

        // target is another editor instance but an external image URL is used: remove the image BASE64 data
        delete imageAttrs.imageData;

    } else if (isBase64(imageAttrs.imageData)) {

        // target is another editor instance, BASE64 data is available: remove the image URL
        delete imageAttrs.imageUrl;

    } else {

        // remove invalid image data attribute
        delete imageAttrs.imageData;
    }

    delete imageAttrs.fileId;
    delete imageAttrs.sessionId;
}

/**
 * Post-processes the attributes of insert drawing operations. Based on the
 * drawing attributes and the editor instance (the operation is applied to):
 *  - An image URL to an external location is always used.
 *  - The image media URL is used if the operation is applied within the
 *      same editor instance.
 *  - The image media URL is removed if the operation is applied in another
 *      editor instance.
 *
 * @param {Object} fillAttrs
 *  The drawing fill attributes of the operation.
 *
 * @param {BaseApplication} app
 *  The application instance.
 */
export function postProcessBackgroundImageOperationAttributes(fillAttrs, app) {
    var deleteBitmap = true;

    // remove the bitmap attribute only for copy&paste - #58560
    if (app.getModel().isClipboardPasteInProgress()) {

        if ((fillAttrs.sessionId === ox.session) && (fillAttrs.fileId === app.getFileDescriptor().id)) {

            // target is the same editor instance: use the image URL
            deleteBitmap = false;

        } else if (fillAttrs.bitmap && isAbsoluteUrl(fillAttrs.bitmap.imageUrl)) {

            // target is another editor instance but an external image URL is used
            deleteBitmap = false;
        }

        if (deleteBitmap) {
            delete fillAttrs.bitmap;
            fillAttrs.type = (_.isObject(fillAttrs.color)) ? 'solid' : 'none';
        }
    }

    // always remove the additional meta attributes
    delete fillAttrs.fileId;
    delete fillAttrs.sessionId;
}

/**
 * Helper function to return the image response data that was created
 * via insertDataUrl, InsertFile or InsertDriveImage
 *
 * @param {unknown} data
 *  Request response data to be parsed.
 *
 * @param {string} name
 *  The filename to be used for the image.
 *
 * @returns {ImageDescriptor}
 *  The image descriptor for the passed data.
 */
function createImageDesc(data, name) {
    return {
        url: pick.string(data, "added_filename", ""),
        name,
        transform: pick.dict(data, "image_transformation") ?? null
    };
}

/**
 * Inserts the image from the specified data URL into a document.
 *
 * @param {EditApplication} docApp
 *  The application containing the edited document.
 *
 * @param {string} dataUrl
 *  The image, Base64 encoded as data URL.
 *
 * @returns {JPromise<ImageDescriptor>}
 *  A promise that will fulfil if the image has been uploaded to the server.
 */
export function insertDataUrl(docApp, dataUrl, fileName) {
    return insDataUrl(docApp, dataUrl, fileName)
        .then(data => createImageDesc(data, getFileNameWithoutExt(fileName || 'image.img')))
        .fail(() => { docApp.docView.yell({ type: 'warning', message: gt('Image could not be loaded.') }); });
}

/**
 * Inserts the image from the specified file into a document.
 *
 * @param {EditApplication} docApp
 *  The application containing the edited document.
 *
 * @param {File} file
 *  The file object describing the image file to be inserted.
 *
 * @returns {JPromise<ImageDescriptor>}
 *  A promise that will fulfil if the image has been uploaded to the server.
 */
export function insertFile(docApp, file) {
    return insFile(docApp, file)
        .then(data => createImageDesc(data, getFileNameWithoutExt(file.name)))
        .fail(() => { docApp.docView.yell({ type: 'warning', message: gt('Image could not be loaded.') }); });
}

/**
 * Inserts the image file of the passed infostore descriptor into a document.
 *
 * @param {EditApplication} docApp
 *  The application containing the edited document.
 *
 * @param {object} file
 *  The infostore descriptor of the file being inserted.
 *
 * @returns {jQuery.Promise}
 *  The Promise of a Deferred object that will be resolved if the image
 *  has been uploaded to the server; or rejected, in case of an error.
 */
export function insertDriveImage(docApp, file) {
    return insDriveImage(docApp, file)
        .then(data => createImageDesc(data, getFileNameWithoutExt(file.filename)));
}

/**
 * Is appying the imageTranformation to drawing attributes.
 *
 * @param {Object} drawingAttrs
 *  The drawing attributes (e.g. attrs.drawing) where the image transformation
 *  is applyied to.
 *
 * @param {unknown} transformation
 *  The image transformation descriptor that might be returned from a
 *  insertDriveImage, insertDataUrl or insertFile request
 *
 */
export function applyImageTransformation(drawingAttrs, transformation) {
    // apply exif orientation if necessary docs-2992
    switch (getIntegerOption(transformation, 'exif_orientation', 1, -2, 8)) {
        case 2:
            drawingAttrs.flipH = true;
            break;
        case 3:
            drawingAttrs.rotation = 180;
            break;
        case 4:
            drawingAttrs.flipV = true;
            break;
        case 5:
            drawingAttrs.flipH = true;
            drawingAttrs.rotation = 90;
            break;
        case 6:
            drawingAttrs.rotation = 90;
            break;
        case 7:
            drawingAttrs.flipH = true;
            drawingAttrs.rotation = 270;
            break;
        case 8:
            drawingAttrs.rotation = 270;
            break;
        default:
    }
}

/**
 * Inserts the image specified by the URL into a document.
 *
 *  Attempts to upload the file to the server and create a media URL first,
 *  if that fails returns the external URL if valid.
 *  If the URL is not valid returns an error.
 *
 * @param {EditApplication} app
 *  The application object representing the edited document.
 *
 * @param {String} url
 *  The full URL of the image to be inserted.
 *
 * @returns {jQuery.Promise}
 *  The Promise of a Deferred object that will be resolved if the image
 *  has been uploaded to the server or if the image URL is valid;
 *  or rejected otherwise.
 */
export function insertURL(app, url) {

    if (!_.isString(url) || !RE_IMAGE_URL.test(url)) {
        return $.Deferred().reject();
    }

    // url data
    var fileExtension = getFileExtensionFromImageUri(url);
    var fileNameWithoutExt = getFileNameWithoutExt(url);
    // whether the image exceeds the size limit
    let exceedsSizeLimit = false;

    // request params
    var params = {
        add_imageurl: url,
        add_ext: fileExtension
    };

    // upload file to the server, the response contains the media URL
    var promise = app.sendActionRequest('addfile', params).then(function (responseData) {

        // the error status of the response
        var errorCode = new ErrorCode(responseData);

        if (errorCode.isError()) {
            // whether an error shall be thrown, when the response contains an error status
            let throwError = true;
            if (errorCode.getCodeAsConstant() === 'GENERAL_ARGUMENTS_ERROR') {
                app.setCurrentInfoState(INFO_CANNOT_EMBED_IMAGE_MUST_USE_URL);
            } else {
                errorCode.setErrorClass(ErrorCode.ERRORCLASS_WARNING);
                app.setInternalError(errorCode, GENERAL);
            }
            if (errorCode.getCodeAsConstant() === 'GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR') { // no error for exceeded image size (issue #66)
                exceedsSizeLimit = true;
                throwError = false;
            }
            if (throwError) { throw new Error(); } // middleware does not accept the image (DOCS-5012)
        } else {
            // return the media URL of the uploaded file
            url = responseData.added_filename;
            // needed for RT2
            return app.sendAddImageIdRequest(responseData);
        }
    }, function () {
        app.rejectEditAttempt('image'); // server problem
    });

    // resolve with the image descriptor object (URL and image name)
    return promise.then(function () {
        return { url, name: fileNameWithoutExt, exceedsSizeLimit };
    });
}

/**
 * Calculates the offset and size of the bitmap in an image frame for one
 * dimension (either horizontally or vertically), according to the passed
 * cropping settings.
 *
 * @param {EditApplication} app
 *
 * @param {Number} frameSize
 *  Visible size (with or height) of the drawing frame containing the image
 *  bitmap, in 1/100 of millimeters.
 *
 * @param {Number} leadingCrop
 *  The leading cropping value (left/top).
 *
 * @param {Number} trailingCrop
 *  The trailing cropping value (right/bottom).
 *
 * @returns {Object}
 *  An object containing 'offset' and 'size' CSS attributes specifying how
 *  to format the bitmap node (in pixels with 'px' unit name).
 */
export function calculateBitmapSettings(app, frameSize, leadingCrop, trailingCrop) {
    // resulting settings for the bitmap
    var size = 0, offset = 0;
    var isSpreadsheet = app.isSpreadsheetApp();

    leadingCrop /= 100;
    trailingCrop /= 100;

    // bitmap size and offset, according to object size and cropping
    size = frameSize / (1 - leadingCrop - trailingCrop);
    offset = size * leadingCrop;

    // convert to CSS pixels
    // for Spreadsheet app convert to percentage, to keep images in their frame while zooming
    return {
        offset: isSpreadsheet ? ((100 * -offset) / frameSize) + '%' : convertHmmToCssLength(-offset, 'px', 1),
        size: isSpreadsheet ? ((100 * size) / frameSize) + '%' : convertHmmToCssLength(size, 'px', 1)
    };
}
