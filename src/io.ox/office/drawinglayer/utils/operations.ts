/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Position, Operation, AttrsOperation, OptAttrsOperation } from "@/io.ox/office/editframework/utils/operations";

// types ======================================================================

/**
 * Base interface for all operations containing a `start` property with a
 * logical document position (with leading sheet index) addressig a drawing
 * object.
 */
export interface DrawingOperation extends Operation {

    /**
     * The position of a drawing object, or of a text component inside.
     */
    start: Position;
}

/**
 * The "insertDrawing" operation that insert a new drawing object into the
 * document.
 */
export interface InsertDrawingOperation extends DrawingOperation, OptAttrsOperation {
    name: typeof INSERT_DRAWING;
    type: string;
}

/**
 * The "deleteDrawing" operation that deletes an existing drawing object from
 * the document.
 */
export interface DeleteDrawingOperation extends DrawingOperation {
    name: typeof DELETE_DRAWING;
}

/**
 * The "changeDrawing" operation that changes the formatting attributes of an
 * existing drawing object.
 */
export interface ChangeDrawingOperation extends DrawingOperation, AttrsOperation {
    name: typeof CHANGE_DRAWING;
}

/**
 * The "moveDrawing" operation that moves an existing drawing object to a new
 * position (Z order).
 */
export interface MoveDrawingOperation extends DrawingOperation {
    name: typeof MOVE_DRAWING;
    to: Position;
}

export interface ChartSeriesOperation extends DrawingOperation {
    series: number;
}

export interface InsertChartSeriesOperation extends ChartSeriesOperation, AttrsOperation {
    name: typeof INSERT_CHART_SERIES;
}

export interface DeleteChartSeriesOperation extends ChartSeriesOperation {
    name: typeof DELETE_CHART_AXIS;
}

export interface ChangeChartSeriesOperation extends ChartSeriesOperation, AttrsOperation {
    name: typeof CHANGE_CHART_SERIES;
}

export interface ChartAxisOperation extends DrawingOperation {
    axis: number;
}

export interface DeleteChartAxisOperation extends ChartAxisOperation {
    name: typeof DELETE_CHART_AXIS;
}

export interface ChangeChartAxisOperation extends ChartAxisOperation, AttrsOperation {
    name: typeof CHANGE_CHART_AXIS;
    // TODO DOCS-1783: move extra properties of "changeChartAxis" operation into "attrs"
    axPos: string;
    crossAx: number;
    zAxis?: boolean;
}

export interface ChangeChartGridOperation extends ChartAxisOperation, AttrsOperation {
    name: typeof CHANGE_CHART_GRID;
}

export interface ChangeChartTitleOperation extends DrawingOperation, AttrsOperation {
    name: typeof CHANGE_CHART_TITLE;
    axis?: number;
}

export interface ChangeChartLegendOperation extends DrawingOperation, AttrsOperation {
    name: typeof CHANGE_CHART_LEGEND;
}

// constants ==================================================================

/**
 * An operation that inserts a new drawing object into the document.
 */
export const INSERT_DRAWING = "insertDrawing";

/**
 * An operation that deletes an existing drawing object from the document.
 */
export const DELETE_DRAWING = "deleteDrawing";

/**
 * An operation that changes formatting attributes of an existing drawing
 * object in the document.
 */
export const CHANGE_DRAWING = "changeDrawing";

/**
 * An operation that moves an existing drawing object to a new position.
 */
export const MOVE_DRAWING = "moveDrawing";

/**
 * An operation that creates a group object from several drawing objects.
 */
export const GROUP = "group";

/**
 * An operation that removes a group object and moves the contained drawing
 * objects to their parent container.
 */
export const UNGROUP = "ungroup";

/**
 * An operation that inserts a new data series into a chart object.
 */
export const INSERT_CHART_SERIES = "insertChartSeries";

/**
 * An operation that deletes an existing data series from a chart object.
 */
export const DELETE_CHART_SERIES = "deleteChartSeries";

/**
 * An operation that changes formatting attributes of a data series in a chart
 * object.
 */
export const CHANGE_CHART_SERIES = "changeChartSeries";

/**
 * An operation that deletes an axis in a chart object.
 */
export const DELETE_CHART_AXIS = "deleteChartAxis";

/**
 * An operation that changes formatting attributes of an axis in a chart
 * object.
 */
export const CHANGE_CHART_AXIS = "changeChartAxis";

/**
 * An operation that changes formatting attributes of the grid lines attached
 * to an axis in a chart object.
 */
export const CHANGE_CHART_GRID = "changeChartGrid";

/**
 * An operation that changes formatting attributes of a title (main title, or
 * axis title) in a chart object.
 */
export const CHANGE_CHART_TITLE = "changeChartTitle";

/**
 * An operation that changes formatting attributes of the legend in a chart
 * object.
 */
export const CHANGE_CHART_LEGEND = "changeChartLegend";

// public functions ===========================================================

/**
 * Creates a "deleteDrawing" JSON operation.
 */
export function deleteDrawing(position: Position): DeleteDrawingOperation {
    return { name: DELETE_DRAWING, start: position };
}
