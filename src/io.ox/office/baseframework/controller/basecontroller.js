/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { isGuest, isShareable } from '@/io.ox/office/tk/utils/driveutils';
import { CREATE_LINK_AVAILABLE, INVITE_USER_AVAILABLE, SHARING_AVAILABLE, inviteUser, shareLink } from '@/io.ox/office/tk/utils/shareutils';

import { FlushReason, hasFloatingApp } from '@/io.ox/office/baseframework/utils/apputils';
import { CONVERTER_AVAILABLE, DEBUG, MAIL_AVAILABLE } from '@/io.ox/office/baseframework/utils/baseconfig';
import { ControllerEngine } from '@/io.ox/office/baseframework/controller/controllerengine';
import { isStandaloneMode, isStandaloneDocsAppInTab } from '@/io.ox/office/baseframework/app/appfactory';

// class BaseController ===================================================

/**
 * Base application controller with predefined items shared by all kinds of
 * Documents applications.
 *
 * @param {BaseApplication} docApp
 *  The application that has created this controller instance.
 *
 * @param {BaseModel} docModel
 *  The document model created by the passed application.
 *
 * @param {BaseView} docView
 *  The document view created by the passed application.
 */
export class BaseController extends ControllerEngine {

    constructor(docApp, _docModel, docView) {

        // base constructor
        super(docView, { _kind: "root" });

        // initialization -----------------------------------------------------

        // register all basic controller items
        this.registerItems({

            // disabled in stand-alone mode
            'app/guest': {
                enable() { return isGuest(); }
            },

            // disabled in stand-alone mode
            'app/bundled': {
                enable() { return !isStandaloneMode(); }
            },

            // enabled if app is plugged by Viewer
            'app/viewermode': {
                enable() { return docApp.isViewerMode(); }
            },

            // enabled if the document has been stored encrypted
            'app/encrypted': {
                enable() { return docApp.isDocumentEncrypted(); }
            },

            // quit/destroy the application
            'app/quit': {
                // no Quit button in stand-alone mode, or when hovered by a floating app (e.g. mail composer)
                parent: 'app/bundled',
                enable() { return !isStandaloneDocsAppInTab(docApp.coreApp) && !hasFloatingApp(); },
                // do not return the promise, otherwise this controller would wait for its own destruction
                set() { jpromise.floating(docApp.quit()); },
                silent: true // bug 58409: no controller update when quitting
            },

            // disabled during import, enabled afterwards
            'app/imported': {
                enable() { return docApp.isImportFinished(); }
            },

            // disabled during import, enabled afterwards if import succeeded
            'app/imported/success': {
                enable() { return docApp.isImportSucceeded(); }
            },

            // disabled during import, enabled afterwards if import failed
            'app/imported/failure': {
                enable() { return docApp.isImportFailed(); }
            },

            // download the document: enabled in read-only mode, disabled during import
            'document/download': {
                parent: 'app/imported',
                set() { return docApp.download(); }
            },

            // print the document (download as PDF): enabled in read-only mode, disabled during import
            'document/print': {
                parent: 'app/imported',
                enable() { return CONVERTER_AVAILABLE; },
                set() { return docApp.print(); },
                shortcut: { keyCode: 'P', ctrlOrMeta: true }
            },

            'document/sendmail/enabled': {
                parent: ['app/imported', 'app/bundled', '!app/encrypted', '!app/guest'],
                enable() { return MAIL_AVAILABLE; }
            },

            // compose new mail with this document as attachment
            'document/sendmail': {
                parent: 'document/sendmail/enabled',
                set() { return docApp.sendMail(); }
            },

            // compose new mail with this document's content being the mail-body's content:
            // enabled in read-only mode, disabled during import, disabled in stand-alone-mode
            'document/sendmail/inline-html': {
                parent: 'document/sendmail/enabled',
                enable() { return docApp.canSendMail(); },
                set(config) { return docApp.sendMail(config); },
                async: _.browser.Safari ? 'background' : 'blocking'
            },

            'document/sendmail/pdf-attachment': {
                parent: 'document/sendmail/enabled',
                //enable() { return app.canSendMail(); },
                set(config) { return docApp.sendMail(config); }
            },

            'document/sendmail/menu': {
                parent: 'document/sendmail/enabled'
            },

            'document/share': {
                parent: ['app/imported', 'app/bundled'],
                enable() { return SHARING_AVAILABLE && isShareable(docApp.getFileDescriptor()); }
            },

            'document/share/getlink': {
                parent: ['document/share', '!app/encrypted'],
                enable() { return CREATE_LINK_AVAILABLE; },
                set() {
                    // DOCS-3042: flush all pending changes before sharing the document
                    return docApp.flushDocument(FlushReason.SHARE).then(function () {
                        return shareLink(docApp.getFileDescriptor());
                    });
                }
            },

            'document/share/inviteuser': {
                parent: 'document/share',
                enable() { return INVITE_USER_AVAILABLE; },
                set() {
                    // DOCS-3042: flush all pending changes before sharing the document
                    return docApp.flushDocument(FlushReason.SHARE).then(function () {
                        return inviteUser(docApp.getFileDescriptor());
                    });
                }
            },

            'view/zoom': {
                parent: 'app/valid',
                get() { return docView.zoomState.factor; },
                set(factor) { docView.zoomState.set("fixed", factor); }
            },

            'view/zoom/dec': {
                parent: 'app/valid',
                enable() { return docView.zoomState.canDec(); },
                set() { docView.zoomState.dec(); }
            },

            'view/zoom/inc': {
                parent: 'app/valid',
                enable() { return docView.zoomState.canInc(); },
                set() { docView.zoomState.inc(); }
            },

            'view/zoom/type': {
                parent: 'app/valid',
                get() { return docView.zoomState.type; },
                set(type) { docView.zoomState.set(type); }
            },

            // debug mode

            // debug mode enabled: to be used as parent for all debug action items
            // this controller item must exist in release builds too!
            'debug/enabled': {
                enable() { return DEBUG; }
            }
        });

        // update when any user setting has changed
        this.updateOnGlobalEvent("change:config:docs");

        // full update when locale data has changed
        this.updateOnGlobalEvent("change:locale", { full: true });

        // update depending on floating applications
        this.updateOnGlobalEvent("floatingapp:visible");

        // update once after successful import
        this.waitForImportSuccess(() => {
            this.updateOnEvent(docView, 'refresh:layout');
        });

        // automatically close modal dialogs, if the connected item becomes disabled
        this.listenTo(docView, "dialog:show", (dialog, config) => {

            // listen to dialogs with a configured "autoCloseKey" option
            var autoCloseKey = config?.autoCloseKey;
            if (!autoCloseKey) { return; }

            // close the dialog automatically if the controller item becomes disabled
            dialog.listenTo(this, 'change:items', changedItems => {
                const itemState = changedItems.get(autoCloseKey);
                if (itemState && !itemState.enabled) { dialog.close(); }
            });
        });
    }
}
