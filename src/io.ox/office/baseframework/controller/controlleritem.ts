/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, ary, jpromise } from "@/io.ox/office/tk/algorithms";
import type { FocusableElement, NodeOrJQuery, KeyCodeName, MatchModifierOptions } from "@/io.ox/office/tk/dom";
import type { Control } from "@/io.ox/office/tk/form";
import type { ControlTriggerType } from "@/io.ox/office/tk/controls";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { DObject } from "@/io.ox/office/tk/objects";

import type { FocusableObject, ShowableObject } from "@/io.ox/office/baseframework/view/baseview";

// types ======================================================================

/**
 * Shared options for a keyboard shortcut definition for a controller item.
 */
export interface SharedShortcutOptions<T> {

    /**
     * The value that will be passed to the setter function of this item. If
     * multiple shortcuts are defined for an item, each shortcut definition may
     * define its own value.
     *
     * A callback function will receive the current value of the controller
     * item as first parameter, and its return value will be passed to the
     * setter function.
     */
    value?: T | FuncType<T, [Nullable<T>]>;

    /**
     * If set to `true`, the event will propagate up to the DOM root element,
     * and the browser will execute its default action. If omitted or set to
     * `false`, the event will be cancelled immediately after calling the
     * setter function.
     */
    propagate?: boolean;

    /**
     * A CSS selector to restrict the keyboard shortcut to specific areas in
     * the document view, according to the browser focus. If omitted, the
     * shortcut will always be handled, if the application is active,
     * regardless of browser focus location.
     */
    scope?: string;
}

/**
 * The definition of a keyboard shortcut for a controller item that will be
 * matched against "keydown" events.
 *
 * Supports all options of `MatchModifierOptions` to specify behaviour of the
 * modifier keys SHIFT, ALT, CTRL, and META.
 */
export interface KeyDownShortcutDefinition<T> extends SharedShortcutOptions<T>, MatchModifierOptions {

    /**
     * The upper-case name of a key code (keys of the map `KeyCode`), or the
     * numeric DOM key code.
     *
     * Be careful with digit keys, for example, the number `9` matches the
     * "TAB" key (`KeyCode.TAB`), but the string "9" matches the digit key 9
     * (`KeyCode["9"]` with the key code `57`).
     */
    keyCode: KeyCodeName | number;
}

/**
 * The definition of a keyboard shortcut for a controller item that will be
 * matched against "keypress" events.
 */
export interface KeyPressShortcutDefinition<T> extends SharedShortcutOptions<T> {

    /**
     * The Unicode character as a string, or the numeric code point of a
     * Unicode character.
     */
    charCode: string | number;
}

/**
 * The definition of a keyboard shortcut for a controller item.
 */
export type ShortcutDefinition<T> = KeyDownShortcutDefinition<T> | KeyPressShortcutDefinition<T>;

/**
 * An object that can receive the browser focus, after a controller item has
 * been executed.
 */
export type FocusTarget = NodeOrJQuery<FocusableElement> | Control | (FocusableObject & Partial<ShowableObject>);

/**
 * Options for restoring the browser focus after executing a controller item.
 */
export interface ItemFocusOptions {

    /**
     * If set to `true`, the current browser focus will not be changed after
     * executing the setter function of a controller item.
     *
     * By default, the focus will return to the application pane after the
     * setter function has finished (will wait for asynchronous setters before
     * returning the focus).
     */
    preserveFocus?: boolean;

    /**
     * A DOM node that will receive the browser focus after a controller item
     * has been executed, an object that provides a method `grabFocus()`, or a
     * callback function that returns a focus target dynamically.
     *
     * Default focus target is the document view.
     *
     * Has no effect, if the option "preserveFocus" is set to `true`.
     */
    focusTarget?: FocusTarget | ((sourceType: ControlTriggerType) => FocusTarget);
}

/**
 * Additional settings passed to an item setter function.
 */
export interface ItemExecuteOptions extends ItemFocusOptions {

    /**
     * The DOM event that caused the item execution.
     */
    sourceEvent?: JEvent;

    /**
     * The cause of the item execution. Default value is "custom".
     */
    sourceType?: ControlTriggerType;

    /**
     * If set to `true`, the (synchronous) `preview` handler of the controller
     * item will be executed instead of the `set` function.
     */
    preview?: boolean;
}

/**
 * The configuration settings of a single controller item.
 */
export interface ItemDefinition<T = unknown> extends ItemFocusOptions {

    /**
     * The key of a parent item that will be used to calculate intermediate
     * results for the getter function and enabler function (see below), or an
     * array of item keys used as parents for this item.
     *
     * The key feature of parent items is that if a controller enables or
     * updates multiple items at once, the getter or enabler of the same parent
     * item registered at multiple items will be executed exactly once before
     * the first item getter or enabler is called, and its result will be
     * cached and passed to all item getters or enablers that are using this
     * parent item.
     *
     * If an item key starts with an exclamation mark, its "enabled" state will
     * be reversed.
     *
     * If omitted, the controller item will not be connected to a parent item.
     */
    parent?: string | string[];

    /**
     * Predicate function returning whether the item is enabled.
     *
     * If one or more parent items have been specified (see above) and at least
     * one of them returned `false`, the item is in disabled state already, and
     * this function will *not* be called anymore. Otherwise, the cached return
     * values of the parent getters (see option "get" below) will be passed to
     * this function, in the same order as specified.
     *
     * If omitted, the enabled state is dependent on the parent items only.
     */
    enable?(this: ControllerItem<T>, ...parentValues: unknown[]): boolean;

    /**
     * Getter function returning the current value of the item.
     *
     * Defaults to a function that returns the value of the first parent item;
     * or `undefined` without parent items.
     *
     * @param parentValues
     *  The cached return values of the getters of all parent items, in the
     *  same order as specified.
     *
     * @returns
     *  The current value of this item. The special value `null` is reserved to
     *  indicate the "ambiguous state" of this item. This state may be
     *  visualized by form controls in some specific way.
     */
    get?(this: ControllerItem<T>, ...parentValues: unknown[]): Nullable<T>;

    /**
     * Setter function changing the value of an item.
     *
     * Can be omitted for read-only items. Defaults to an empty function.
     *
     * @param value
     *  The new value for this item.
     *
     * @param options
     *  Additional settings passed in from the framework.
     *
     * @returns
     *  May return a promise to indicate that the setter executes asynchronous
     *  code. Other return values will be ignored. Thrown exceptions will be
     *  logged to the console but ignored.
     */
    set?(this: ControllerItem<T>, value: T, options: ItemExecuteOptions): MaybeAsync;

    /**
     * Similar to property `set` that can be used to dynamically update the
     * appearance without actually changing the state of this item.
     *
     * Defaults to an empty function. Must be synchronous.
     *
     * @param value
     *  The preview value for this item.
     */
    preview?(this: ControllerItem<T>, value: T): void;

    /**
     * The behaviour for asynchronous setter functions. Must be one of the
     * following values:
     *
     * - "blocking" (default): The application window will be blocked and a
     *   busy indicator will be shown while the setter is running.
     *
     * - "cancelable": Same behavior as "blocking". If the setter function of
     *   the item has returned a pending abortable promise, the busy blocker
     *   screen will show a "Cancel" button that will abort that promise. It is
     *   up to the implementation of the setter function to react properly on
     *   aborting the promise.
     *
     * - "background": The busy blocker screen will not be shown, and the
     *   promise returned by the setter function will be ignored (real
     *   background tasks).
     */
    async?: "blocking" | "cancelable" | "background";

    /**
     * If set to `true`, executing the item will not cause a controller update.
     * Default value if `false`.
     */
    silent?: boolean;

    /**
     * One or multiple keyboard shortcut definitions. If the window root node
     * of the application receives a "keydown" or "keypress" event that matches
     * a shortcut definition, the setter function of this item will be
     * executed. Can be as single shortcut definition, or an array of shortcut
     * definitions.
     */
    shortcut?: ShortcutDefinition<T> | Array<ShortcutDefinition<T>>;
}

/**
 * A map with controller item states, mapped by item keys.
 */
export type ItemStateMap = Map<string, ItemState>;

/**
 * Resolver callback function for an arbitrary controller item. Needed to
 * resolve parent items while calculating the state of an item.
 */
export type ResolveItemFn = (key: string) => Opt<ControllerItem>;

// globals ====================================================================

export const controllerLogger = new Logger("office:log-controller", {
    tag: "CONTR",
    tagColor: 0xFFFF00
});

// class ItemState ============================================================

/**
 * Represents the complete state of a controller item.
 */
export class ItemState<T = unknown> {

    // properties -------------------------------------------------------------

    /**
     * The enabled state of this item.
     */
    readonly enabled: boolean = false;

    /**
     * The current value (the result of the getter callback) of this item.
     */
    readonly value: Nullable<T> = undefined;

    /**
     * The item states of all parent items of the controller item, in order of
     * the parent keys registered for the item. If the item does not contain
     * any parent items, this property will be an empty array.
     */
    readonly parentStates: ItemState[];

    /**
     * The current values (results of all getter callbacks) of the parent
     * items, in order of the parent keys registered for the item. This
     * property is provided for convenience (the parent values are contained in
     * the property `parentStates` of this instance).
     */
    readonly parentValues: unknown[];

    /**
     * The accumulated enabled state of the parent items of this item. Will be
     * `true`, if all parent items are considered to be enabled (depending on
     * the existence of exclamation marks in the parent item specification).
     */
    readonly parentsEnabled: boolean;

    // constructor ------------------------------------------------------------

    constructor(parentStates: ItemState[], parentsEnabled: boolean) {
        this.parentStates = parentStates;
        this.parentValues = parentStates.map(state => state.value);
        this.parentsEnabled = parentsEnabled;
    }
}

// class ControllerItem =======================================================

export class ControllerItem<T = unknown> extends DObject {

    // properties -------------------------------------------------------------

    /**
     * The key of this controller item.
     */
    readonly key: string;

    /**
     * Keys of parent items whose values/states are needed to resolve the own
     * value/state.
     */
    readonly parentKeys: string[];

    /**
     * Whether an asynchronous setter is cancelable.
     */
    readonly cancelable: boolean;

    /**
     * Whether an asynchronous setter runs in the background.
     */
    readonly background: boolean;

    /**
     * Whether the setter does not cause a controller update.
     */
    readonly silent: boolean;

    /**
     * Behavior for returning browser focus to application.
     *
     */
    readonly preserveFocus: boolean;

    /**
     * Custom focus target node or callback handler.
     */
    readonly focusTarget: ItemDefinition<T>["focusTarget"];

    // callback function for the "enabled" state
    readonly #enableFn: NonNullable<ItemDefinition<T>["enable"]>;
    // callback function for the value getter
    readonly #getFn: NonNullable<ItemDefinition<T>["get"]>;
    // callback function for the value setter
    readonly #setFn: ItemDefinition<T>["set"];
    // callback function for the value preview setter
    readonly #previewFn: ItemDefinition<T>["preview"];

    // the current state of this item (enabled flag, value, and parent data)
    #state = new ItemState<T>([], false);

    // constructor ------------------------------------------------------------

    constructor(key: string, definition: ItemDefinition<T>) {
        super();

        this.key = key;
        this.parentKeys = ary.wrap(definition.parent);
        this.cancelable = definition.async === "cancelable";
        this.background = definition.async === "background";
        this.silent = !!definition.silent;
        this.preserveFocus = !!definition.preserveFocus;
        this.focusTarget = definition.focusTarget;

        this.#enableFn = definition.enable ?? fun.true;
        this.#getFn = definition.get ?? ((v: unknown) => v as Nullable<T>); // TODO: no getter: first parent must be of type "T"
        this.#setFn = definition.set;
        this.#previewFn = definition.preview;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns whether all parent items are enabled (`false`, if at least one
     * parent item is disabled; otherwise `true`).
     */
    get parentsEnabled(): boolean {
        return this.#state.parentsEnabled;
    }

    /**
     * Returns the values of all parent items.
     *
     * @returns
     *  The current values of all parent items.
     */
    get parentValues(): readonly unknown[] {
        return this.#state.parentValues;
    }

    /**
     * Returns the current value of the first parent item.
     *
     * @returns
     *  The current value of the first parent item.
     */
    get parentValue(): unknown {
        return this.#state.parentValues[0];
    }

    /**
     * Returns whether this item is effectively enabled (`false`, if at least
     * one parent item is disabled, or if the enable handler of this item has
     * returned `false`; otherwise `true`).
     */
    get enabled(): boolean {
        return this.#state.enabled;
    }

    /**
     * Returns the current value of this item.
     */
    get value(): Nullable<T> {
        return this.#state.value;
    }

    // public methods ---------------------------------------------------------

    /**
     * Executes the setter function of this item (passing in the new value),
     * and moves the browser focus back to the application pane.
     *
     * @param value
     *  The new value of the item.
     *
     * @param [options]
     *  Additional settings passed in from the framework to be passed to the
     *  setter function.
     *
     * @returns
     *  A promise that will fulfil or reject according to the result of the
     *  item setter function. If the item is disabled, a rejected promise will
     *  be returned. If the item setter returns a promise, that promise will be
     *  returned. Otherwise, the return value of the item setter function will
     *  be wrapped and returned in a fulfilled promise.
     */
    execute(value: T, options?: ItemExecuteOptions): JPromise<unknown> & Partial<Abortable> {
        return jpromise.invoke(() => this.#setFn?.(value, { ...options }));
    }

    /**
     * Executes the preview function of this item (passing in the new value).
     *
     * @param value
     *  The temporary value of the item to be previewed.
     */
    preview(value: T): void {
        try {
            this.#previewFn?.(value);
        } catch (err) {
            controllerLogger.exception(err);
        }
    }

    /**
     * Calculates the current state of this item, and updates the passed state
     * cache of the controller.
     *
     * _Attention_: Not for public use. Called from the document controller
     * while updating its internal cache.
     *
     * @internal
     */
    calculateState(stateCache: ItemStateMap, resolveItemFn: ResolveItemFn): ItemState<T> {

        // first check the state cache to prevent multiple calculation of shared parent states
        const oldState = stateCache.get(this.key) as ItemState<T>;
        if (oldState) { return oldState; }

        // resolve the states of all parent items
        let parentsEnabled = true;
        const parentStates = this.parentKeys.map(parentKey => {
            const reversed = parentKey.startsWith("!");
            if (reversed) { parentKey = parentKey.slice(1); }
            const parentItem = resolveItemFn(parentKey);
            if (!parentItem) { controllerLogger.warn(`$badge{ControllerItem} calculateState: item "${this.key}" refers to unknown parent "${parentKey}"`); }
            const parentState = parentItem?.calculateState(stateCache, resolveItemFn);
            if (!parentState || (reversed === parentState.enabled)) { parentsEnabled = false; }
            return parentState ?? new ItemState([], false);
        });

        // create a new item state instance
        const newState = new ItemState<T>(parentStates, parentsEnabled) as Writable<ItemState<T>>;

        // IMPORTANT: Insert the item state into the cache BEFORE resolving properties "enabled" and "value"!
        // This ensures that different parent items do not calculate a shared grandparent item multiple times.
        stateCache.set(this.key, newState);
        this.#state = newState;

        // resolve and store the value first (`enable` callback may want to query the own value)
        newState.value = this.#getFn(...newState.parentValues);
        // resolve and store the enabled state (pass parent values to the resolver functions)
        newState.enabled = newState.parentsEnabled && this.#enableFn(...newState.parentValues);

        // enable handlers must return a boolean
        if (!is.boolean(newState.enabled)) {
            controllerLogger.warn(`$badge{ControllerItem} calculateState: item "${this.key}" does not return boolean "enable" state`);
            newState.enabled = !!newState.enabled;
        }

        return newState;
    }
}
