/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { is, fun, ary, map, dict, jpromise, debug } from "@/io.ox/office/tk/algorithms";
import { KeyCode, matchModifierKeys, isSelectAllKeyEvent, isNodeSource, toJQuery, isTextInputElement, getFocus, setFocus } from "@/io.ox/office/tk/dom";
import { Control } from "@/io.ox/office/tk/form";
import type { CoreEmitter, AllEventsEmitter, AnyEmitter } from "@/io.ox/office/tk/events";
import { type DObjectConfig, debounceMethod } from "@/io.ox/office/tk/objects";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";
import type { FocusableObject, ShowableObject, BaseView } from "@/io.ox/office/baseframework/view/baseview";
import type { ItemState, ItemStateMap, KeyDownShortcutDefinition, KeyPressShortcutDefinition, ItemFocusOptions, ItemExecuteOptions, ItemDefinition, ResolveItemFn } from "@/io.ox/office/baseframework/controller/controlleritem";
import { ControllerItem, controllerLogger } from "@/io.ox/office/baseframework/controller/controlleritem";

// types ======================================================================

export type { ItemState, ItemStateMap };

export interface ControllerUpdateOptions {

    /**
     * If set to `true`, a full update of all registered controller items will
     * be performed (the event "change:items" will emit a complete state map).
     * By default, only items with a changed state (enabled state or value)
     * will be notified.
     */
    full?: boolean;
}

/**
 * Type mapping for the events emitted by `ControllerEngine` instances.
 */
export interface ControllerEngineEventMap {

    /**
     * Will be emitted after the current state (value and/or enabled state) of
     * at least one controller item has been changed, after the controller has
     * updated itself or the method `ControllerEngine::update` has been
     * invoked.
     *
     * @param itemStates
     *  The descriptors of the changed items in an `ItemStateMap` map.
     */
    "change:items": [itemStates: ItemStateMap];

    /**
     * Will be emitted directly after an item setter has been executed, but
     * before the controller states (of dependent items) have been updated.
     *
     * @param key
     *  The key of the executed item.
     *
     * @param value
     *  The value passed to the item setter function.
     */
    "execute:item": [key: string, value: unknown];
}

// private types --------------------------------------------------------------

interface KeyDownShortcutEntry extends KeyDownShortcutDefinition<unknown> {
    key: string;
}

interface KeyPressShortcutEntry extends KeyPressShortcutDefinition<unknown> {
    key: string;
}

interface NotifiedState {
    enabled: boolean;
    value: unknown;
}

// private functions ==========================================================

function isFocusableObject(obj: unknown): obj is FocusableObject {
    return is.object(obj) && ("grabFocus" in obj) && is.function(obj.grabFocus);
}

function isShowableObject(obj: unknown): obj is ShowableObject {
    return is.object(obj) && ("isVisible" in obj) && is.function(obj.isVisible);
}

function resolveFocusOptions(item: ControllerItem, options?: ItemExecuteOptions): ItemExecuteOptions {

    // focus options after executing the item
    const focusOptions: ItemExecuteOptions = { ...options };

    // mix item focus options and passed focus options
    focusOptions.preserveFocus = item.preserveFocus || !!options?.preserveFocus;
    focusOptions.focusTarget = options?.focusTarget ?? item.focusTarget;

    // bind focus target callback function to the item instance
    if (is.function(focusOptions.focusTarget)) {
        focusOptions.focusTarget = focusOptions.focusTarget.bind(item);
    }

    return focusOptions;
}

// class ControllerEngine =====================================================

/**
 * A controller instance contains a collection of controller items, consisting
 * of unique key and value, and providing arbitrary getter and setter methods
 * for their values.
 */
export class ControllerEngine<DocViewT extends BaseView> extends ViewObject<DocViewT, ControllerEngineEventMap> {

    // properties -------------------------------------------------------------

    // All registered controller items, mapped by item key.
    readonly #itemMap = this.member(new Map<string, ControllerItem>());

    // Cached item states, mapped by item keys.
    readonly #stateCache = new Map<string, ItemState>();

    // Shortcut definitions for "keydown", mapped by key code.
    readonly #keyShortcuts = new Map<number, KeyDownShortcutEntry[]>();

    // Shortcut definitions for "keypress", mapped by char code (single-letter string).
    readonly #charShortcuts = new Map<number, KeyPressShortcutEntry[]>();

    // Cached values of all controller items as notified the last time.
    readonly #notifiedStates = new Map<string, NotifiedState>();

    // All dirty keys (items that have been executed before the next update), as flag set.
    readonly #dirtyKeys = new Set<string>();

    // Unbound item resolver callback function (can be called without context).
    readonly #resolveItemFn: ResolveItemFn = key => this.getItem(key);

    // The keys of all unknown items already reported (prevent multiple warnings).
    readonly #unknownItemKeys = new Set<string>();

    // Promise for a running item setter (to prevent recursion).
    #runningPromise = jpromise.resolve<unknown>(undefined);

    // The internal deferred object representing the current update cycle.
    #updateDeferred: Opt<JDeferred<ItemStateMap>>;

    // Whether a full update has been requested at least once.
    #fullUpdate = false;

    // constructor ------------------------------------------------------------

    constructor(docView: DocViewT, config?: DObjectConfig) {
        super(docView, config);

        // create the initial update promise
        this.#createUpdateDeferred();

        // update once after successful import
        this.waitForImportSuccess(() => {
            this.#initializeKeyHandler();
            jpromise.floating(this.update());
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds the definition for a new item to this controller.
     *
     * @param key
     *  The key of the new item.
     *
     * @param definition
     *  The configuration of the controller item. All callback functions will
     *  be executed with a `ControllerItem` instance as `this` context.
     */
    registerItem<T>(key: string, definition: ItemDefinition<T>): void {

        // destroy an existing item instance
        this.#itemMap.get(key)?.destroy();

        // create a new item instance
        this.#itemMap.set(key, new ControllerItem(key, definition));

        // special preparations for global keyboard shortcuts
        if (definition.shortcut) {
            for (const shortcut of ary.wrap(definition.shortcut)) {
                if ("keyCode" in shortcut) {
                    let { keyCode } = shortcut;
                    if (is.string(keyCode)) {
                        keyCode = KeyCode[keyCode] || 0;
                    }
                    if (is.number(keyCode) && (keyCode > 0)) {
                        map.upsert(this.#keyShortcuts, keyCode, () => []).push({ key, ...shortcut });
                    }
                }
                if ("charCode" in shortcut) {
                    let { charCode } = shortcut;
                    if (is.string(charCode)) { charCode = charCode.charCodeAt(0); }
                    if (is.number(charCode) && (charCode > 0)) {
                        map.upsert(this.#charShortcuts, charCode, () => []).push({ key, ...shortcut });
                    }
                }
            }
        }
    }

    /**
     * Adds the definition for a new item to this controller that will be
     * registered only after the document has been imported successfully.
     *
     * @param key
     *  The key of the new item.
     *
     * @param definition
     *  The definition of the item. See method `registerItem()` for details.
     */
    registerItemOnSuccess<T>(key: string, definition: ItemDefinition<T>): void {
        this.waitForImportSuccess(() => this.registerItem(key, definition));
    }

    /**
     * Adds definitions for multiple items to this controller.
     *
     * @param definitions
     *  A dictionary with definitions for all new items, mapped by item key.
     *  Each new controller item will be defined by calling the method
     *  `registerItem()`.
     */
    registerItems(definitions: Dict<ItemDefinition>): void {
        dict.forEach(definitions, (definition, key) => this.registerItem(key, definition));
    }

    /**
     * Adds definitions for multiple items to this controller that will be
     * registered only after the document has been imported successfully.
     *
     * @param definitions
     *  A dictionary with definitions for all new items, mapped by item key.
     *  Each new controller item will be defined by calling the method
     *  `registerItem()`.
     */
    registerItemsOnSuccess(definitions: Dict<ItemDefinition>): void {
        this.waitForImportSuccess(() => this.registerItems(definitions));
    }

    /**
     * Updates this controller if the event emitter emits the specified event.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param type
     *  The type name of the event to start listening to. Can also be an array
     *  of event names.
     *
     * @param [options]
     *  Optional parameters.
     */
    updateOnEvent<E>(emitter: AnyEmitter<E>, type: OrArray<keyof E>, options?: ControllerUpdateOptions): void {
        this.listenTo(emitter as CoreEmitter<E>, type, () => {
            controllerLogger.trace(() => "$badge{ControllerEngine} updateOnEvent: received event from emitter");
            jpromise.floating(this.update(options));
        });
    }

    /**
     * Updates this controller if the event emitter emits any event.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param [options]
     *  Optional parameters.
     */
    updateOnAllEvents<E>(emitter: AllEventsEmitter<E>, options?: ControllerUpdateOptions): void {
        this.listenToAllEvents(emitter as CoreEmitter<E>, () => {
            controllerLogger.trace("$badge{ControllerEngine} updateOnAllEvents: received event from emitter");
            jpromise.floating(this.update(options));
        });
    }

    /**
     * Updates this controller if the global event emitter emits the specified
     * event.
     *
     * @param type
     *  The type name of the global event to start listening to.
     *
     * @param [options]
     *  Optional parameters.
     */
    updateOnGlobalEvent(type: keyof DocsGlobalEventMap, options?: ControllerUpdateOptions): void {
        this.listenToGlobal(type, () => {
            controllerLogger.trace(() => `$badge{ControllerEngine} updateOnGlobalEvent: received global event "${type}"`);
            jpromise.floating(this.update(options));
        });
    }

    /**
     * Refreshes the states and values of all registered items, and emits a
     * "change:items" event.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil after the controller has updated all
     *  registered items. The fulfillment value contains a map with the states
     *  of all changed controller items (the same map that has been emitted
     *  with the "change:items" event).
     */
    update(options?: ControllerUpdateOptions): JPromise<ItemStateMap> {
        controllerLogger.trace(() => `$badge{ControllerEngine} update: ${options?.full ? "full" : "partial"} update requested`);

        this.#fullUpdate = this.#fullUpdate || !!options?.full;
        this.#stateCache.clear();
        this.#createUpdateDeferred();
        this.docApp.setRootAttribute("data-controller-state", "updating");

        this.#implUpdate();

        return this.#updateDeferred!.promise();
    }

    /**
     * Returns the current enabled state of the specified item.
     *
     * @param key
     *  The key of the item. If the key starts with an exclamation mark, its
     *  "enabled" state will be reversed.
     *
     * @returns
     *  Whether the specified item exists and is enabled.
     */
    isItemEnabled(key: string): boolean {
        const reversed = key.startsWith("!");
        if (reversed) { key = key.slice(1); }
        const item = this.getItem(key);
        return !!item && (reversed !== item.enabled);
    }

    /**
     * Returns the current value of the specified item.
     *
     * @param key
     *  The key of the item.
     *
     * @returns
     *  The current value of the item; or `undefined`, if the item does not
     *  exist.
     */
    getItemValue(key: string): unknown {
        return this.getItem(key)?.value;
    }

    /**
     * Moves the browser focus back to the application pane, or to a custom
     * focus target node, if specified in the passed options.
     *
     * @param [options]
     *  Optional parameters.
     */
    grabFocus(options?: ItemExecuteOptions): void {

        // keep current focus, if option "preserveFocus" is set
        if (options?.preserveFocus && document.activeElement && $(document.activeElement).is(":visible")) {
            return;
        }

        // resolve callback function to focus target
        let focusTarget = options?.focusTarget;
        if (is.function(focusTarget)) {
            focusTarget = focusTarget(options?.sourceType || "custom");
        }

        // try to set focus into a form control (method `focus()` may reject focus attempt)
        if ((focusTarget instanceof Control) && focusTarget.focus()) {
            return;
        }

        // try to set focus with a `grabFocus()` method (e.g. view panes, form controls, etc.)
        if (isFocusableObject(focusTarget) && (!isShowableObject(focusTarget) || focusTarget.isVisible())) {
            focusTarget.grabFocus(options);
            return;
        }

        // try to set focus to a node from a DOM node source (`Node`, `NodeList`, `JQuery`)
        if (focusTarget && isNodeSource(focusTarget)) {
            const targetNode = toJQuery(focusTarget).filter(":visible")[0];
            if (targetNode) {
                setFocus(targetNode);
                return;
            }
        }

        // otherwise, move focus to the document view
        this.docView.grabFocus(options);
    }

    /**
     * Manually executes the setter function of the controller item associated
     * to the specified key, and passes in the new value.
     *
     * @param key
     *  The key of the controller item to be executed.
     *
     * @param value
     *  The new value of the item.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil or reject according to the result of the
     *  item setter function. If the item is disabled or does not exist, a
     *  rejected promise will be returned. If the item setter returns a
     *  promise, that promise will be returned. Otherwise, the return value of
     *  the item setter function will be wrapped and returned in a resolved
     *  promise.
     */
    @controllerLogger.profileMethod("$badge{ControllerEngine} executeItem")
    executeItem(key: string, value: unknown, options?: ItemExecuteOptions): JPromise<unknown> {
        controllerLogger.log(() => `key=${key}, value=${debug.stringify(value)}, preview=${!!options?.preview}`);

        const item = this.getItem(key);

        // execute preview handler of the item
        if (options?.preview) {
            // item does not exist: fail immediately
            if (!item) { return jpromise.reject(); }
            // execute preview handler, catch and log all errors
            try {
                item.preview(value);
                return jpromise.resolve();
            } catch (err) {
                controllerLogger.exception(err);
                return jpromise.reject(err);
            }
        }

        // reset preview state of the item
        item?.preview(undefined);

        // focus options after executing the item
        const focusOptions = item ? resolveFocusOptions(item, options) : options;

        // item does not exist: return focus to application
        if (!item) {
            this.grabFocus(focusOptions);
            jpromise.floating(this.update());
            return jpromise.reject();
        }

        // remember item key while setter is running
        this.#dirtyKeys.add(key);

        // enabled state of the item must be determined without using cached item states
        const tmpStateCache = new Map<string, ItemState>();
        item.calculateState(tmpStateCache, this.#resolveItemFn);

        // do nothing if item is disabled
        let pending = false;
        let promise: JPromise<unknown> & Partial<Abortable>;
        if (item.enabled) {

            // execute the item setter
            promise = item.execute(value, options);
            pending = jpromise.isPending(promise);

            // show busy screen if setter is running asynchronously
            if (pending && !item.background) {
                const showCancel = item.cancelable && is.abortable(promise);
                const cancelHandler = showCancel ? () => promise.abort!() : undefined;
                // DOCS-4983: busy screen steals focus
                if (focusOptions?.preserveFocus) {
                    focusOptions.preserveFocus = false;
                    focusOptions.focusTarget = getFocus() ?? undefined;
                }
                this.docView.enterBusy(cancelHandler && { cancelHandler });
            }
        } else {
            // item is disabled
            promise = jpromise.reject();
        }

        // focus back to application
        if (item.background) {
            this.grabFocus(focusOptions);
        } else {
            this.onSettled(promise, () => {
                if (pending) {
                    // execute in a timeout, needed for dialogs which are closed *after* resolve/reject
                    // ES6-LATER: still needed?
                    this.setTimeout(() => this.grabFocus(focusOptions), 0);
                } else {
                    this.grabFocus(focusOptions);
                }
            });
        }

        // post-processing after the setter is finished
        if (pending) {
            this.onSettled(promise, () => {
                if (!item.background) { this.docView.leaveBusy(); }
                this.#triggerAndUpdate(key, value);
            });
        } else {
            this.#triggerAndUpdate(key, value);
        }

        if (!item.background) {
            this.#runningPromise = jpromise.fastThen(this.#runningPromise, () => promise);
        }
        return promise;
    }

    /**
     * Manually performs actions to cancel a controller item associated to the
     * specified key. Specifically, the preview state of the item will be reset
     * to `undefined`, and the browser focus will be returned to the view.
     *
     * @param key
     *  The key of the controller item to be canceled.
     *
     * @param [options]
     *  Optional parameters.
     */
    cancelItem(key: string, options?: ItemFocusOptions): void {
        controllerLogger.log(`$badge{ControllerEngine} cancelItem: key=${key}`);

        // reset preview state of the item
        const item = this.getItem(key);
        item?.preview(undefined);

        // focus back to application
        this.grabFocus(item ? resolveFocusOptions(item, options) : options);

        // update controller
        jpromise.floating(this.update());
    }

    /**
     * Returns a promise that will fulfil after the item setter currently
     * running has finished. This method ignores item setters in background
     * mode. If no item setter is running, a fulfilled promise will be returned
     * directly.
     *
     * @returns
     *  A promise representing state of the item setter currently running.
     */
    getRunningItemPromise(): JPromise {
        // do not expose the failed state of the promise
        return jpromise.fastThen(this.#runningPromise, fun.undef, fun.undef);
    }

    // protected methods ------------------------------------------------------

    /**
     * Returns the specified controller item instance.
     *
     * @param key
     *  The key of a controller item.
     *
     * @returns
     *  The specified controller item instance; or `undefined`, if an item with
     *  the specified key does not exist.
     */
    protected getItem(key: string): Opt<ControllerItem> {
        const item = this.#itemMap.get(key);
        if (!item && this.docApp.isImportFinished() && !this.#unknownItemKeys.has(key)) {
            this.#unknownItemKeys.add(key);
            controllerLogger.warn(`$badge{ControllerEngine} getItem: unknown item key "${key}"`);
        }
        return item;
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a new JQuery deferred representing the next update cycle.
     */
    #createUpdateDeferred(): void {

        // pending deferred object represents the current controller update cycle
        if (!this.#updateDeferred || jpromise.isSettled(this.#updateDeferred)) {
            this.#updateDeferred = this.newJDeferred();
        }
    }

    /**
     * Executes the item setter defined in the passed shortcut.
     *
     * @returns
     *  Whether to stop propagation of the keyboard event.
     */
    #executeItemForShortcut(event: JTriggeredEvent, shortcut: KeyDownShortcutEntry | KeyPressShortcutEntry): boolean {

        // check whether to actually process the key event according to the shortcut scope
        const selector = shortcut.scope;
        if (selector && ($(event.target).closest(selector, this.docApp.getRootNode()[0]).length === 0)) {
            return false;
        }

        // bug 30096: do not allow to execute items while a blocking operation is still running
        if (jpromise.isSettled(this.#runningPromise)) {
            // call value resolver function, or take constant value
            const value = is.function(shortcut.value) ? shortcut.value(this.getItemValue(shortcut.key)) : shortcut.value;
            jpromise.floating(this.executeItem(shortcut.key, value, { sourceType: "keyboard" }));
        }

        // stop event propagation unless specified otherwise
        return !shortcut?.propagate;
    }

    /**
     * Handles "keydown" and "keypress" events and calls the setter of the
     * controller item with a matching keyboard shortcut definition.
     *
     * @param event
     *  The keyboard event object. If a matching shortcut definition has been
     *  found, propagation of the event will be stopped, and the browser
     *  default action will be suppressed.
     */
    #keyHandler(event: JTriggeredEvent): void | false {

        // whether to stop propagation and prevent the default action
        let stopPropagation = false;

        switch (event.type) {
            case "keydown":
                // process all shortcut definitions for the key code in the passed event
                this.#keyShortcuts.get(event.keyCode!)?.forEach(entry => {
                    // check if the additional modifier keys match the shortcut definition
                    if (matchModifierKeys(event, entry)) {
                        stopPropagation = this.#executeItemForShortcut(event, entry) || stopPropagation;
                    }
                });
                break;
            case "keypress":
                // process all shortcut definitions for the char code in the passed event,
                // but ignore "keypress" events in text fields
                if (!isTextInputElement(event.target)) {
                    this.#charShortcuts.get(event.charCode!)?.forEach(entry => {
                        stopPropagation = this.#executeItemForShortcut(event, entry) || stopPropagation;
                    });
                }
                break;
        }

        return stopPropagation ? false : undefined;
    }

    /**
     * Initializes the key handlers for all registered keyboard shortcuts.
     */
    #initializeKeyHandler(): void {

        // the root node of the application window
        const $rootNode = this.docApp.getRootNode();

        // register event listener for shortcuts
        this.listenTo($rootNode, "keydown", this.#keyHandler);
        this.listenTo($rootNode, "keypress", this.#keyHandler);

        // prevent that Ctrl+A shortcut selects the entire HTML document
        this.listenTo($rootNode, "keydown", event => {
            const stopPropagation = isSelectAllKeyEvent(event) && !isTextInputElement(event.target);
            return stopPropagation ? false : undefined;
        });
    }

    /**
     * Update implementation that needs to be debounced.
     */
    @debounceMethod({ delay: 100 })
    @controllerLogger.profileMethod("$badge{ControllerEngine} implUpdate")
    #implUpdate(): void {

        // collect all item states missing in the state cache
        controllerLogger.takeTime("calculate all states", () => {
            this.#itemMap.forEach(item => item.calculateState(this.#stateCache, this.#resolveItemFn));
        });

        // reduce to the properties that differ from previous state
        const changedStates = map.filterFrom(this.#stateCache, (state, key) => {
            if (this.#fullUpdate || this.#dirtyKeys.has(key)) { return true; }
            const prevState = this.#notifiedStates.get(key);
            return !prevState || (prevState.enabled !== state.enabled) || !_.isEqual(prevState.value, state.value);
        });

        // bug 40770: store the values of the current cache for next update() call
        this.#notifiedStates.clear();
        this.#stateCache.forEach((state, key) => {
            this.#notifiedStates.set(key, { enabled: state.enabled, value: state.value });
        });

        // return to partial update mode
        this.#fullUpdate = false;

        // notify all listeners (with locked GUI refresh for performance, view
        // elements are listening to controller items to change their visibility)
        if (!changedStates.size) {
            controllerLogger.trace('nothing changed, event "change:items" will not be emitted');
        } else {
            controllerLogger.takeTime('emitting "change:items" event', () => {
                controllerLogger.trace(() => ["emitted item states:", Array.from(changedStates, ([key, { enabled, value }]) => ({ key, enabled, value }))]);
                this.trigger("change:items", changedStates);
                this.#dirtyKeys.clear();
            });
        }

        // resolve the pending update promise with the changed states map
        this.#updateDeferred!.resolve(changedStates);

        // set a marker at the application root element for automated tests
        if (this.docApp.isImportFinished()) {
            this.docApp.setRootAttribute("data-controller-state", "done");
        }
    }

    /**
     * Triggers the "execute:item" event, and requests controller update.
     */
    @controllerLogger.profileMethod("$badge{ControllerEngine} triggerAndUpdate")
    #triggerAndUpdate(key: string, value: unknown): void {
        this.trigger("execute:item", key, value);
        jpromise.floating(this.update());
    }
}
