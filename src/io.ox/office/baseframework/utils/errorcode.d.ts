/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ClientErrorData } from "@/io.ox/office/baseframework/utils/clienterror";

// types ======================================================================

interface RTErrorDescription {
    code: number;
    prefix: string;
}

// class ErrorCode ============================================================

export default class ErrorCode {

    static readonly CONSTANT_NO_ERROR = "NO_ERROR";
    static readonly PROPERTY_ERRORDESCRIPTION = "errorDescription";
    static readonly PROPERTY_ERRORCODEASSTRING = "error";
    static readonly PROPERTY_ERRORCLASS = "errorClass";
    static readonly PROPERTY_ERRORSOURCE = "errorSource";
    static readonly PROPERTY_ERRORCONTEXTDATA = "errorContextData";
    static readonly PROPERTY_ERRORVALUE = "errorValue";
    static readonly PROPERTY_OX_ERRORID = "error_id";
    static readonly ERRORCLASS_NO_ERROR = 0;
    static readonly ERRORCLASS_WARNING = 1;
    static readonly ERRORCLASS_ERROR = 2;
    static readonly ERRORCLASS_FATAL_ERROR = 3;
    static readonly ERRORSOURCE_SERVER = 1;
    static readonly ERRORSOURCE_CLIENT = 2;
    static readonly GENERAL_ERROR: ClientErrorData;

    static extractErrorInformation(serverResult: unknown): { error: unknown } | null;
    static extractRealtimeError(rtResult: unknown): RTErrorDescription | null;
    static isError(value: unknown): value is ErrorCode;

    constructor(resultData?: Dict);

    isError(): boolean;
    isWarning(): boolean;
    getDescription(): string;
    getCodeAsConstant(): string;
    getErrorClass(): number;
    getErrorSource(): number;
    getErrorContext(): string | null;
    getErrorValue(): string;
    getErrorText(): string;
    getContextData(): Dict | null;

    setErrorClass(newErrorClass: number): void;
    setErrorSource(source: number): void;
    setErrorContext(context: string): void;
    setContextData(newContextData: Dict): void;
    getAsResult(): { error: Dict };
}
