/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ErrorCode from "@/io.ox/office/baseframework/utils/errorcode";

const { ERRORCLASS_ERROR, ERRORCLASS_WARNING } = ErrorCode;

// types ======================================================================

export interface ClientErrorData {
    error: string;
    errorClass: number;
}

// private functions ==========================================================

/**
 * Small helper function to create an instance of `ClientErrorData`.
 */
function makeError(error: string, errorClass: number): Readonly<ClientErrorData> {
    return { error, errorClass };
}

// constants ==================================================================

export const ERROR_CONNECTION_RESET_RECEIVED                  = makeError("ERROR_CONNECTION_RESET_RECEIVED", ERRORCLASS_ERROR);
export const ERROR_CONNECTION_NOT_MEMBER                      = makeError("ERROR_CONNECTION_NOT_MEMBER", ERRORCLASS_ERROR);
export const ERROR_GENERAL_SYNCHRONIZATION_ERROR              = makeError("ERROR_GENERAL_SYNCHRONIZATION_ERROR", ERRORCLASS_ERROR);
export const ERROR_OFFLINE_WHILE_SYNCHRONIZING                = makeError("ERROR_OFFLINE_WHILE_SYNCHRONIZING", ERRORCLASS_ERROR);
export const ERROR_OFFLINE_WHILE_SYNC_LOCAL_CHANGES           = makeError("ERROR_OFFLINE_WHILE_SYNC_LOCAL_CHANGES", ERRORCLASS_ERROR);
export const ERROR_OFFLINE_WHILE_LOADING                      = makeError("ERROR_OFFLINE_WHILE_LOADING", ERRORCLASS_ERROR);
export const ERROR_SYNCHRONIZATION_TIMEOUT                    = makeError("ERROR_SYNCHRONIZATION_TIMEOUT", ERRORCLASS_ERROR);
export const ERROR_SYNCHRONIZATION_NOT_POSSIBLE               = makeError("ERROR_SYNCHRONIZATION_NOT_POSSIBLE", ERRORCLASS_ERROR);
export const ERROR_SYNCHRONIZATION_DOC_CHANGED                = makeError("ERROR_SYNCHRONIZATION_DOC_CHANGED", ERRORCLASS_ERROR);
export const ERROR_SYNCHRONIZATION_LOST_EDIT_RIGHTS           = makeError("ERROR_SYNCHRONIZATION_LOST_EDIT_RIGHTS", ERRORCLASS_ERROR);
export const ERROR_SYNCHRONIZATION_NOT_SUPPORTED              = makeError("ERROR_SYNCHRONIZATION_NOT_SUPPORTED", ERRORCLASS_ERROR);
export const ERROR_WHILE_MODIFYING_DOCUMENT                   = makeError("ERROR_WHILE_MODIFYING_DOCUMENT", ERRORCLASS_ERROR);
export const ERROR_WHILE_LOADING_DOCUMENT                     = makeError("ERROR_WHILE_LOADING_DOCUMENT", ERRORCLASS_ERROR);
export const ERROR_SIRI_NOT_SUPPORTED                         = makeError("ERROR_SIRI_NOT_SUPPORTED", ERRORCLASS_ERROR);
export const ERROR_LOAD_DOC_FAILED_SERVER_TOO_BUSY            = makeError("ERROR_LOAD_DOC_FAILED_SERVER_TOO_BUSY", ERRORCLASS_ERROR);
export const ERROR_BAD_SERVER_COMPONENT_DETECTED              = makeError("ERROR_BAD_SERVER_COMPONENT_DETECTED", ERRORCLASS_ERROR);
export const ERROR_DOCUMENT_SIZE_EXCEEDED                     = makeError("ERROR_DOCUMENT_SIZE_EXCEEDED", ERRORCLASS_ERROR);
export const ERROR_DOCUMENT_PASSWORD_PROTECTED                = makeError("ERROR_DOCUMENT_PASSWORD_PROTECTED", ERRORCLASS_ERROR);
export const ERROR_CONNECTION_TIMEOUT                         = makeError("ERROR_CONNECTION_TIMEOUT", ERRORCLASS_ERROR);
export const ERROR_CONNECTION_HANGUP                          = makeError("ERROR_CONNECTION_HANGUP", ERRORCLASS_ERROR);
export const ERROR_CONNECTION_JOIN_FAILED                     = makeError("ERROR_CONNECTION_JOIN_FAILED", ERRORCLASS_ERROR);
export const ERROR_CONNECTIONINSTANCE_DISPOSED                = makeError("ERROR_CONNECTIONINSTANCE_DISPOSED", ERRORCLASS_ERROR);
export const ERROR_UNKNOWN_REALTIME_FAILURE                   = makeError("ERROR_UNKNOWN_REALTIME_FAILURE", ERRORCLASS_ERROR);
export const ERROR_SERVER_DENIES_CONNECTION                   = makeError("ERROR_SERVER_DENIES_CONNECTION", ERRORCLASS_ERROR);
export const ERROR_SERVER_SHUTDOWN_NO_RESCUE_POSSIBLE         = makeError("ERROR_SERVER_SHUTDOWN_NO_RESCUE_POSSIBLE", ERRORCLASS_ERROR);
export const ERROR_SERVER_GOING_TO_SHUTDOWN                   = makeError("ERROR_SERVER_GOING_TO_SHUTDOWN", ERRORCLASS_ERROR);
export const ERROR_SERVER_NODE_UNAVAILABLE                    = makeError("ERROR_SERVER_NODE_UNAVAILABLE", ERRORCLASS_ERROR);
export const ERROR_SERVER_NODE_UNAVAILABLE_NO_RESCUE_POSSIBLE = makeError("ERROR_SERVER_NODE_UNAVAILABLE_NO_RESCUE_POSSIBLE", ERRORCLASS_ERROR);
export const ERROR_SERVER_NOTIFIED_A_CRITICAL_DOC_PROBLEM     = makeError("ERROR_SERVER_NOTIFIED_A_CRITICAL_DOC_PROBLEM", ERRORCLASS_ERROR);
export const ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES = makeError("ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES", ERRORCLASS_ERROR);
export const ERROR_SESSION_LOST_WITH_LOCAL_CHANGES            = makeError("ERROR_SESSION_LOST_WITH_LOCAL_CHANGES", ERRORCLASS_ERROR);
export const ERROR_SESSION_LOST_WITH_UNKNOWN_DOCUMENT_STATE   = makeError("ERROR_SESSION_LOST_WITH_UNKNOWN_DOCUMENT_STATE", ERRORCLASS_ERROR);
export const ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR   = makeError("ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR", ERRORCLASS_ERROR);
export const ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR      = makeError("ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR", ERRORCLASS_ERROR);
export const WARNING_READONLY_DUE_TO_OFFLINE                  = makeError("WARNING_READONLY_DUE_TO_OFFLINE", ERRORCLASS_WARNING);
export const WARNING_CONNECTION_INSTABLE                      = makeError("WARNING_CONNECTION_INSTABLE", ERRORCLASS_WARNING);
export const WARNING_NO_PERMISSION_FOR_DOC                    = makeError("WARNING_NO_PERMISSION_FOR_DOC", ERRORCLASS_WARNING);
export const WARNING_DOC_IS_LOCKED                            = makeError("WARNING_DOC_IS_LOCKED", ERRORCLASS_WARNING);
export const WARNING_SYNC_AFTER_OFFLINE                       = makeError("WARNING_SYNC_AFTER_OFFLINE", ERRORCLASS_WARNING);
export const WARNING_SYNC_FOR_VIEWER                          = makeError("WARNING_SYNC_FOR_VIEWER", ERRORCLASS_WARNING);
export const WARNING_RESCUE_DOC_CANNOT_BE_CHANGED             = makeError("WARNING_RESCUE_DOC_CANNOT_BE_CHANGED", ERRORCLASS_WARNING);
export const WARNING_SESSION_INVALID_DETECTED                 = makeError("WARNING_SESSION_INVALID_DETECTED", ERRORCLASS_WARNING);
export const WARNING_DOC_MOVED_RELOAD_REQUIRED                = makeError("WARNING_DOC_MOVED_RELOAD_REQUIRED", ERRORCLASS_WARNING);
export const ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE       = makeError("ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE", ERRORCLASS_ERROR);
export const ERROR_DOC_FILE_DELETED                           = makeError("ERROR_DOC_FILE_DELETED", ERRORCLASS_ERROR);
