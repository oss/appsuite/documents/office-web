/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { getIntegerOption, getStringOption } from '@/io.ox/office/tk/utils';
import { RESTORED_FILENAME } from '@/io.ox/office/baseframework/utils/errorcontextdata';

// static class InfoMessages ==============================================

var // client-side mapping information error codes to info messages
    //
    // ATTENTION: Only define non-context data dependent messages here.
    //
    // There are the following possible entries for actions available:
    // 'acquireedit' - Acquire edit rights
    //
    // Optional poroperties available:
    // 'duration' - Specifies in ms how long the popup message is visible.
    INFO_MESSAGES = {
        INFO_SYNCHRONIZATION_SUCCESSFUL:      { headline: gt('Synchronization'), message: gt('Synchronization between client and server was successful. You can edit the document again.') },
        INFO_SYNCHRONIZATION_RO_SUCCESSFUL:   { headline: gt('Synchronization'), message: gt('Synchronization between client and server was successful. You see the latest document content.') },
        INFO_EDITRIGHTS_ARE_TRANSFERED:       { message: gt('Edit rights are being transferred to you. Please wait a moment.') },
        INFO_EDITRIGHTS_SAVE_IN_PROGRESS:     { type: 'warning', message: gt('Edit rights cannot be transferred. The server is busy to save the document. Please try again later.'), action: 'acquireedit' },
        INFO_EDITRIGHTS_PLEASE_WAIT:          { message: gt('Edit rights are still being transferred to you. The system is busy and will need more time. Please be patient.'), duration: 30000 },
        INFO_ALREADY_TRANSFERING_EDIT_RIGHTS: { type: 'warning', message: gt('Someone else already wants to receive the edit rights for this document. Please try again later.'), action: 'acquireedit' },
        INFO_CONNECTION_WAS_LOST_TRYING_SYNC: { message: gt('Connection to server was lost. Please wait, trying to synchronize to enable edit mode.') },
        INFO_CONNECTION_WAS_LOST:             { message: gt('Connection to server was lost. Enable client to acquire edit rights.') },
        INFO_CONNECTION_LOST_RELOAD_DOC:      { message: gt('Connection to server was lost. Please reopen the document to acquire edit rights.') },
        INFO_NO_PERMISSION_TO_CHANGE_DOC:     { message: gt('You do not have permissions to change this document.') },
        INFO_EDITRIGHTS_RECEIVED:             { type: 'success', message: gt('You have edit rights.') },
        INFO_LOADING_IN_PROGRESS:             { message: gt('Please wait until document is loaded completely.'), headline: gt('Read-Only Mode') },
        INFO_DOC_SAVE_FAILED:                 { message: gt('Saving your document failed, we can try to restore your document using backup data.'), headline: gt('Error') },
        INFO_SYNCHRONIZATION_FAILED:          { type: 'warning', headline: gt('Synchronization'), message: gt('The synchronization process between client and server was not successful.') },
        INFO_CANNOT_EMBED_IMAGE_MUST_USE_URL: { type: 'warning', headline: gt('Image'), message: gt('The image could not be embedded into the document. The external URL is used instead.') }
    };

// static methods ---------------------------------------------------------

/**
 * Provides the information message data to be shown to the user.
 *
 * @param {String} infoState
 *  The information state as constant string defined by InfoState.
 *
 * @param {Object} options
 *  Optional data for the information
 */

export function getMessageData(infoState, options) {
    var // information message
        infoMessage = null,
        // context data from options
        contextData, contextData2;

    if (_.isString(infoState)) {
        infoMessage = (infoState in INFO_MESSAGES) ? INFO_MESSAGES[infoState] : infoMessage;
    }

    if (!_.isObject(infoMessage) && _.isObject(options)) {
        // special handling of information messages which need external data - use options to retrieve
        // this data to call gt()
        infoMessage = {};
        switch (infoState) {
            case 'INFO_PREPARE_LOSING_EDIT_RIGHTS':
                contextData = getStringOption(options, 'wantsToEditUser', null);
                contextData2 = getStringOption(options, 'oldWantsToEditUser', null);
                // in case of a long edit-rights switch, we have to use 'oldWantsToEditUser'
                contextData = (_.isEmpty(contextData)) ? contextData2 : contextData;
                if (!_.isEmpty(contextData)) {
                    infoMessage.message =
                        //#. %1$s is the name of the user who will get the edit rights
                        gt('Transferring edit rights to %1$s. Your latest changes will be saved now.', contextData);
                } else {
                    infoMessage.message = gt('Transferring edit rights to another user. Your latest changes will be saved now.');
                }
                infoMessage.headline = gt('Read-Only Mode');
                break;
            case 'INFO_USER_IS_CURRENTLY_EDIT_DOC':
                contextData = getStringOption(options, 'oldEditUser', null);
                // message if we lost the edit rights
                if (!_.isEmpty(contextData)) {
                    infoMessage.message =
                        //#. %1$s is the name of the user who is currently editing the document
                        gt('%1$s is currently editing this document.', contextData);
                } else {
                    infoMessage.message = gt('Another user is currently editing this document.');
                }
                infoMessage.headline = gt('Read-Only Mode');
                infoMessage.action = 'acquireedit';
                break;
            case 'INFO_DOC_CONVERT_STORED_IN_DEFFOLDER':
                contextData = getStringOption(options, 'fullFileName', null);
                infoMessage.message =
                    //#. %1$s is the new file name
                    gt('Document was converted and stored in your documents folder as "%1$s".', contextData);
                break;
            case 'INFO_DOC_CONVERTED_AND_STORED':
                contextData = getStringOption(options, 'fullFileName', null);
                infoMessage.message =
                    //#. %1$s is the new file name
                    gt('Document was converted and stored as "%1$s".', contextData);
                break;
            case 'INFO_DOC_CREATED_IN_DEFAULTFOLDER':
                contextData = getStringOption(options, 'fullFileName', null);
                infoMessage.message =
                    //#. %1$s is the file name
                    gt('Document "%1$s" was created in your documents folder to allow editing.', contextData);
                break;
            case 'INFO_DOC_STORED_IN_DEFAULTFOLDER_AS':
                contextData = getStringOption(options, 'fullFileName', null);
                infoMessage.message =
                    //#. %1$s is the file name
                    gt('Document has been stored in your documents folder as "%1$s".', contextData);
                break;
            case 'INFO_DOC_SAVED_AS_TEMPLATE':
                contextData = getStringOption(options, 'fullFileName', null);
                contextData2 = getStringOption(options, 'fullPathName', null);
                infoMessage.message =
                    //#. %1$s is the file name of the copied document
                    gt('The document was saved as a template "%1$s" in folder "%2$s".', contextData, contextData2);
                break;
            case 'INFO_DOC_SAVED_IN_FOLDER':
                contextData = getStringOption(options, 'fullFileName', null);
                contextData2 = getStringOption(options, 'fullPathName', null);
                infoMessage.message =
                    //#. %1$s is the file name and %2$s is the entire folder path
                    gt('Document "%1$s" was created in folder "%2$s".', contextData, contextData2);
                break;
            case 'INFO_DOC_RESCUED_SUCCESSFULLY':
                contextData = getStringOption(options, RESTORED_FILENAME, null);
                infoMessage.message =
                    //#. %1$s is the file name of the restored document
                    gt('Your document could be restored as "%1$s" successfully.', contextData);
                infoMessage.headline = gt('Restore Document');
                break;
        }
    }

    if (_.isObject(infoMessage)) {
        // ensure that we have a defined headline and type for information messages
        infoMessage.type = infoMessage.type || 'info';

        //type is important
        infoMessage.type = getStringOption(options, 'type', infoMessage.type);
        infoMessage.duration = getIntegerOption(options, 'duration', infoMessage.duration);
    }

    return infoMessage;
}
