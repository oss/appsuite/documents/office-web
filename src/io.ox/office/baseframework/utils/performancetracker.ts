/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from "$/ox";

import { dict, json, jpromise } from "@/io.ox/office/tk/algorithms";
import { LOG_PERFORMANCE_DATA } from "@/io.ox/office/tk/config";
import { FILTER_MODULE_NAME, sendRequest } from "@/io.ox/office/tk/utils/io";
import { LogLevel, Logger } from "@/io.ox/office/tk/utils/logger";

import type { AppType } from "@/io.ox/office/baseframework/utils/apputils";

// class PerformanceTracker ===================================================

export class PerformanceTracker extends Logger {

    // properties -------------------------------------------------------------

    readonly #items = new Map<string, unknown>();

    #startTime = Date.now();
    _lapTime = this.#startTime;
    #pauseTime = 0;

    // constructor ------------------------------------------------------------

    constructor(perfTag: string, appType: AppType, title: string, message?: string) {

        super("office:log-perf", {
            defLogLevel: LogLevel.TRACE,
            recordAll: true,
            tag: perfTag,
            tagColor: 0xFF8000
        });

        // add global data items
        this.setItem("user-agent", navigator.userAgent);
        this.setItem("platform", navigator.platform);
        this.setItem("user", ox.user);
        this.setItem("version", ox.version);
        this.setItem("server", ox.abs);
        this.setItem("application", appType);
        this.setItem("title", title);

        // show welcome message
        if (message) { this.info(message); }
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds an arbitrary data item to this performance tracker.
     *
     * @param key
     *  The key of the data item.
     *
     * @param value
     *  The value of the data item.
     */
    setItem(key: string, value: unknown): void {
        this.#items.set(key, value);
    }

    /**
     * Adds data items to this performance tracker for the current time. All
     * data items are in milliseconds.
     *
     * @param baseKey
     *  The base key of the data items. This method creates three items:
     *  - "<baseKey>Absolute": the current absolute timestamp.
     *  - "<baseKey>Duration": the duration since launching the document (since
     *    construction of this instance).
     *  - "<baseKey>Step": the duration from last call of this method.
     *
     * @param message
     *  A message text for the console logger.
     *
     * @returns
     *  The current timestamp.
     */
    step(baseKey: string, message: string): number {

        if (this.#pauseTime) {
            this.warn("Performance tracker is paused!");
        }

        const currTime = Date.now();
        const fromTime = currTime - this.#startTime;
        const stepTime = currTime - this._lapTime;

        this.setItem(`${baseKey}Absolute`, currTime);
        this.setItem(`${baseKey}Duration`, fromTime);
        this.setItem(`${baseKey}Step`, stepTime);

        this.info(`${message} $duration{t=${stepTime}} $duration{total=${fromTime}}`);

        this._lapTime = currTime;
        return currTime;
    }

    /**
     * Adds data items to this performance tracker for the point in time when a
     * promise will be settled. All data items are in milliseconds.
     *
     * @param promise
     *  The promise to wait for.
     *
     * @param baseKey
     *  The base key of the data items. This method creates three items:
     *  - "<baseKey>Absolute": the current absolute timestamp.
     *  - "<baseKey>Duration": the duration since launching the document (since
     *    construction of this instance).
     *  - "<baseKey>Step": the duration from last call of this method.
     *
     * @param message
     *  A message text for the console logger.
     *
     * @returns
     *  The promise passed to this method, for convenience.
     */
    asyncStep<PT extends AnyPromise<any>>(promise: PT, baseKey: string, message: string): PT {
        // register finally handler, but return the original promise
        jpromise.floating(jpromise.fastFinally(promise, () => this.step(baseKey, message)));
        return promise;
    }

    /**
     * Pauses tracking performance durations at the current timestamp. Tracking
     * can be continued by calling the method `continue()` first.
     *
     * @param message
     *  A message to be logged stating the reason for pausing this tracker.
     */
    pause(message: string): void {
        if (!this.#pauseTime) {
            this.#pauseTime = Date.now();
            this.log(message);
        }
    }

    /**
     * Continues tracking performance durations at the current timestamp, after
     * the tracker has been paused by calling the method `pause()`.
     *
     * @param message
     *  A message to be logged stating the reason for continuing this tracker.
     */
    continue(message: string): void {
        if (this.#pauseTime) {
            const diff = Date.now() - this.#pauseTime;
            this.log(`${message} $duration{${diff}}`);
            this.#startTime += diff;
            this._lapTime += diff;
            this.#pauseTime = 0;
        }
    }

    /**
     * Sends all collected performance measurement data to the server.
     *
     * @returns
     *  A promise that will fulfil when the data have been sent to the server.
     */
    async sendData(): Promise<void> {

        // nothing to do if not enabled in server configuration
        if (!LOG_PERFORMANCE_DATA) {
            this.info("Skipped sending performance data to server:", this.#items);
            return;
        }

        // send JSON data to server (independently from lifetime of this application)
        this.info("Sending performance data to server:", this.#items);
        await sendRequest(FILTER_MODULE_NAME, {
            action: "logperformancedata",
            performanceData: json.safeStringify(dict.from(this.#items))
        }, { method: "POST" });
    }
}
