/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// constants ==================================================================

export const INFO_SYNCHRONIZATION_SUCCESSFUL      = "INFO_SYNCHRONIZATION_SUCCESSFUL";
export const INFO_SYNCHRONIZATION_RO_SUCCESSFUL   = "INFO_SYNCHRONIZATION_RO_SUCCESSFUL";
export const INFO_EDITRIGHTS_ARE_TRANSFERED       = "INFO_EDITRIGHTS_ARE_TRANSFERED";
export const INFO_EDITRIGHTS_SAVE_IN_PROGRESS     = "INFO_EDITRIGHTS_SAVE_IN_PROGRESS";
export const INFO_EDITRIGHTS_PLEASE_WAIT          = "INFO_EDITRIGHTS_PLEASE_WAIT";
export const INFO_ALREADY_TRANSFERING_EDIT_RIGHTS = "INFO_ALREADY_TRANSFERING_EDIT_RIGHTS";
export const INFO_CONNECTION_WAS_LOST_TRYING_SYNC = "INFO_CONNECTION_WAS_LOST_TRYING_SYNC";
export const INFO_CONNECTION_WAS_LOST             = "INFO_CONNECTION_WAS_LOST";
export const INFO_CONNECTION_LOST_RELOAD_DOC      = "INFO_CONNECTION_LOST_RELOAD_DOC";
export const INFO_NO_PERMISSION_TO_CHANGE_DOC     = "INFO_NO_PERMISSION_TO_CHANGE_DOC";
export const INFO_PREPARE_LOSING_EDIT_RIGHTS      = "INFO_PREPARE_LOSING_EDIT_RIGHTS";
export const INFO_USER_IS_CURRENTLY_EDIT_DOC      = "INFO_USER_IS_CURRENTLY_EDIT_DOC";
export const INFO_EDITRIGHTS_RECEIVED             = "INFO_EDITRIGHTS_RECEIVED";
export const INFO_DOC_CONVERT_STORED_IN_DEFFOLDER = "INFO_DOC_CONVERT_STORED_IN_DEFFOLDER";
export const INFO_DOC_CONVERTED_AND_STORED        = "INFO_DOC_CONVERTED_AND_STORED";
export const INFO_DOC_CREATED_IN_DEFAULTFOLDER    = "INFO_DOC_CREATED_IN_DEFAULTFOLDER";
export const INFO_LOADING_IN_PROGRESS             = "INFO_LOADING_IN_PROGRESS";
export const INFO_DOC_SAVED_IN_FOLDER             = "INFO_DOC_SAVED_IN_FOLDER";
export const INFO_DOC_SAVED_AS_TEMPLATE           = "INFO_DOC_SAVED_AS_TEMPLATE";
export const INFO_DOC_RESCUED_SUCCESSFULLY        = "INFO_DOC_RESCUED_SUCCESSFULLY";
export const INFO_DOC_SAVE_FAILED                 = "INFO_DOC_SAVE_FAILED";
export const INFO_SYNCHRONIZATION_FAILED          = "INFO_SYNCHRONIZATION_FAILED";
export const INFO_CANNOT_EMBED_IMAGE_MUST_USE_URL = "INFO_CANNOT_EMBED_IMAGE_MUST_USE_URL";
