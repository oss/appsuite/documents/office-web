/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { getBooleanOption, getOption, getStringOption } from '@/io.ox/office/tk/utils';
import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import * as ErrorContext from '@/io.ox/office/baseframework/utils/errorcontext';
import { MAX_DOCSIZE_ALLOWED, ORIGINAL_FILEID, RESTORED_FILENAME, RESTORED_PATHNAME } from '@/io.ox/office/baseframework/utils/errorcontextdata';

// constants ==================================================================

// client-side mapping error code to error messages
const CLIENT_ERROR_MESSAGES = {
    ERROR_OFFLINE_WHILE_SYNCHRONIZING: gt('Cannot synchronize while offline. Please reload the document as soon as you are online again.'),
    ERROR_OFFLINE_WHILE_SYNC_LOCAL_CHANGES: gt('Cannot synchronize while offline. You have changes which could not be saved. Please try to copy the whole content into the clipboard and paste it into a new document as soon as you are online again'),
    ERROR_OFFLINE_WHILE_LOADING: gt('The document could not be loaded. The server connection was lost during loading the document. If the problem only persists with the same document it can be an error in the document.'),
    ERROR_CONNECTION_RESET_RECEIVED: gt('Due to a server connection error the editor switched to read-only mode. Please reload the document.'),
    ERROR_CONNECTION_NOT_MEMBER: gt('Connection to the server hosting the document has been lost. Please reload the document.'),
    ERROR_GENERAL_SYNCHRONIZATION_ERROR: gt('Synchronization not possible, because an unknown error occured. Please reload the document for further usage.'),
    ERROR_SYNCHRONIZATION_TIMEOUT: gt('Synchronization not possible, because the server was not able to process the synchronization request. Please reload the document.'),
    ERROR_SYNCHRONIZATION_NOT_POSSIBLE: gt('Synchronization not possible, because the server is not able to synchronize this document. Please reload the document for further usage.'),
    ERROR_SYNCHRONIZATION_DOC_CHANGED: gt('Synchronization not possible, because the server is not able to synchronize this document. Please reload the document for further usage.'),
    ERROR_SYNCHRONIZATION_LOST_EDIT_RIGHTS: gt('You are not the editor of the document anymore. Synchronization is not possible, please reload the document.'),
    ERROR_SYNCHRONIZATION_NOT_SUPPORTED: gt('Connection to server was lost. Please reopen the document to acquire edit rights.'),
    ERROR_WHILE_MODIFYING_DOCUMENT: gt('An unrecoverable error occurred while modifying the document.'),
    ERROR_WHILE_LOADING_DOCUMENT: gt('An unrecoverable error occurred while loading the document.'),
    ERROR_SIRI_NOT_SUPPORTED: gt('Siri is not supported as input method. Please do not use to input text.'),
    ERROR_LOAD_DOC_FAILED_SERVER_TOO_BUSY: gt('The document could not be loaded. The server is currently too busy. Please try again later.'),
    ERROR_BAD_SERVER_COMPONENT_DETECTED: gt('A server component is not working. Please contact the server administrator.'),
    ERROR_DOCUMENT_PASSWORD_PROTECTED: gt('The document is protected with a password.'),
    ERROR_CONNECTION_TIMEOUT: gt('Connection to the server has been lost. Please reload the document.'),
    ERROR_CONNECTION_HANGUP: gt('The server detected a synchronization problem with your client. Please reload the doument.'),
    ERROR_CONNECTION_JOIN_FAILED: gt('The document could not be loaded, because the server could not process our request successfully. Please try again later.'),
    ERROR_CONNECTIONINSTANCE_DISPOSED: gt('The document could not be loaded, because the server is busy saving the requested document. Please try again later.'),
    ERROR_SERVER_DENIES_CONNECTION: gt('The document could not be loaded. The server is currently too busy. Please try again later.'),
    ERROR_UNKNOWN_REALTIME_FAILURE: gt('An unknown server error occurred while processing your request. Please retry again later.'),
    ERROR_SERVER_SHUTDOWN_NO_RESCUE_POSSIBLE: gt('The server we are connected to is going down. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    ERROR_SERVER_GOING_TO_SHUTDOWN: gt('The server we are connected to is going down. The document cannot be changed anymore. Please try to reload the document later.'),
    ERROR_SERVER_NODE_UNAVAILABLE: gt('The server we are connected to is not available. The document cannot be changed anymore. Please try to reload the document later.'),
    ERROR_SERVER_NODE_UNAVAILABLE_NO_RESCUE_POSSIBLE: gt('The server we are connected to is not available. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    ERROR_SESSION_INVALID_AND_RELOGIN_FAILED: gt('Your session is invalid and relogin failed. Please login again and to save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    WARNING_READONLY_DUE_TO_OFFLINE: gt('Connection to server was lost. Switched to read-only mode.'),
    WARNING_CONNECTION_INSTABLE: gt('The server does not answer our action requests. Maybe the connection is unstable or the server side has problems.'),
    WARNING_NO_PERMISSION_FOR_DOC: gt('You do not have permissions to change this document.'),
    WARNING_SYNC_AFTER_OFFLINE: gt('Connection to server was lost. Please wait, trying to synchronize to enable edit mode.'),
    WARNING_SYNC_FOR_VIEWER:  gt('Connection to server was lost. Enable client to acquire edit rights.'),
    WARNING_RESCUE_DOC_CANNOT_BE_CHANGED: gt('The document cannot be changed, because the format is not editable. To create an editable document, please use "save as" or copy the complete content into the clipboard and paste it into a new document.'),
    WARNING_SESSION_INVALID_DETECTED: gt('Your session is invalid. Please login again.'),
    ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES: gt('The server detected a synchronization problem with your client. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    ERROR_SESSION_LOST_WITH_LOCAL_CHANGES: gt('A previous session failure could be resolved but your latest changes could not be saved. Please copy the complete content to the clipboard and paste it into a new document.'),
    ERROR_SESSION_LOST_WITH_UNKNOWN_DOCUMENT_STATE: gt('A previous session failure could be resolved but your latest changes could not be saved. Please copy the complete content to the clipboard and paste it into a new document.'),
    ERROR_SERVER_NOTIFIED_A_CRITICAL_DOC_PROBLEM: gt('The server detected a problem with your document. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR: gt('A conflict has occurred during simultaneous editing. To solve this conflict the document is now reloaded.'),
    ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR: gt('The document was modified by an unsupported action. To solve this problem the document is now reloaded.'),
    WARNING_DOC_MOVED_RELOAD_REQUIRED: gt('The document has been moved to a different folder. To solve this problem the document is now reloaded for further usage.'),
    ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE: gt('The document has been moved to a different folder. You have lost permissions to change this document. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    ERROR_DOC_FILE_DELETED: gt('The document has been deleted. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.')
};

// server-side mapping error code to error messages, including error context for general errors
// ATTENTION: PLEASE KEEP IN MIND ABOUT THE MAPPING - WITH/WITHOUT CONTEXT!!
// Context specific error messages must be defined as: ERROR.CONTEXT
// Generic error messages must be defined as: ERROR
const SERVER_ERROR_MESSAGES = {
    'GENERAL_QUOTA_REACHED_ERROR.SAVE': gt('The document could not be saved. Your quota limit has been reached. Please free up storage space.'),
    'GENERAL_FILE_NOT_FOUND_ERROR.SAVE': gt('The document could not be written, because the document could not be found. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_PERMISSION_READ_MISSING_ERROR.SAVE': gt('The document could not be read, because you lost read permissions. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_PERMISSION_WRITE_MISSING_ERROR.SAVE': gt('The document could not be written, because you lost write permissions. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_MEMORY_TOO_LOW_ERROR.SAVE': gt('The document could not be saved. The server has not enough memory. To save your last changes, please copy the complete content into the clipboard and paste it into a new document. If you close the document, the last consistent version will be restored.'),
    'GENERAL_FILE_LOCKED_ERROR.SAVE': gt('The document could not be saved. The document is protected by a lock. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR: gt('The document is in an inconsistent state. To save your work, please copy the complete content into the clipboard and paste it into a new document. Furthermore the last consistent document was restored.'),
    SAVEDOCUMENT_BACKUPFILE_CREATE_PERMISSION_MISSING_ERROR: gt('The document was saved successfully, but a backup copy could not be created. You do not have the permission to create a file in the folder.'),
    SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR: gt('The document was saved successfully, but a backup copy could not be created. You do not have the correct permissions in the folder.'),
    SAVEDOCUMENT_BACKUPFILE_QUOTA_REACHED_ERROR: gt('The document was saved successfully, but a backup copy could not be created. Your quota limit has been reached. Please free up storage space. '),
    SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR: gt('The document was saved successfully, but a backup copy could not be created.'),
    SAVEDOCUMENT_BACKUPFILE_IS_LOCKED_ERROR: gt('The document was saved successfully, but a backup copy could not be written due to a lock protection.'),
    SAVEDOCUMENT_FAILED_NOBACKUP_ERROR: gt('The document is inconsistent and the server was not able to create a backup file. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR: gt('The document cannot be saved, because the server is too busy. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    SAVEDOCUMENT_PARTIAL_CHANGES_SAVED_ERROR: gt('The document was only saved with some of your changes. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR: gt('The document cannot be saved because a new version has been uploaded. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    CREATEDOCUMENT_CONVERSION_FAILED_ERROR: gt('Could not create document because the necessary conversion of the file failed.'),
    CREATEDOCUMENT_CANNOT_READ_TEMPLATEFILE_ERROR: gt('Could not create document because the necessary template file could not be read.'),
    CREATEDOCUMENT_CANNOT_READ_DEFAULTTEMPLATEFILE_ERROR: gt('Could not create document because the default template file could not be read.'),
    'GENERAL_PERMISSION_CREATE_MISSING_ERROR.CREATEDOC': gt('Could not create document due to missing folder permissions.'),
    'GENERAL_QUOTA_REACHED_ERROR.CREATEDOC': gt('Could not create document because the allowed quota is reached. Please delete some items in order to create new ones.'),
    'GENERAL_FILE_NOT_FOUND_ERROR.CREATEDOC': gt('Could not create document because the template file could not be found.'),
    'GENERAL_FILE_CORRUPT_ERROR.CREATEDOC': gt('Could not create document, because the template file is corrupt.'),
    'GENERAL_FILE_NOT_FOUND_ERROR.SAVEAS': gt('Saving the document to a new file failed, because the source document could not be found.'),
    'GENERAL_PERMISSION_READ_MISSING_ERROR.SAVEAS': gt('Saving the document to a new file failed, because you lost read permissions on the source document. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_PERMISSION_WRITE_MISSING_ERROR.SAVEAS': gt('Saving the document to a new file failed, because you have no write permissions.'),
    COPYDOCUMENT_USERFOLDER_UNKOWN_ERROR: gt('The new document could not be saved, because your default folder is unknown.'),
    'GENERAL_QUOTA_REACHED_ERROR.SAVEAS': gt('The new document could not be saved, because your quota has been reached.'),
    'GENERAL_PERMISSION_CREATE_MISSING_ERROR.SAVEAS': gt('The new document could not be saved, because you do not have permissions to create a file in the folder.'),
    'GENERAL_UNKNOWN_ERROR.SAVEAS': gt('The new document could not be saved, because an unknown error occurred.'),
    'GENERAL_FILE_NOT_FOUND_ERROR.CLOSE': gt('The document cannot be found. Saving the document is not possible. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_QUOTA_REACHED_ERROR.CLOSE': gt('The document could not be saved. Your quota limit has been reached. Please free up storage space.'),
    'GENERAL_PERMISSION_READ_MISSING_ERROR.CLOSE': gt('The document could not be saved, because you lost read permissions. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_PERMISSION_WRITE_MISSING_ERROR.CLOSE': gt('The document could not be saved, because you lost write permissions. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_MEMORY_TOO_LOW_ERROR.CLOSE': gt('The document could not be saved. The server has not enough memory. To save your last changes, please copy the complete content into the clipboard and paste it into a new document. If you close the document, the last consistent version will be restored.'),
    'GENERAL_FILE_LOCKED_ERROR.CLOSE': gt('The document could not be saved. The document is protected by a lock. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    HANGUP_INVALID_OPERATIONS_SENT_ERROR: gt('The server detected a synchronization problem with your client. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    HANGUP_NO_EDIT_RIGHTS_ERROR: gt('The server detected a synchronization problem with your client. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    HANGUP_CALCENGINE_NOT_RESPONDING_ERROR: gt('The server detected a synchronization problem with all clients of this document. Please reload the document.'),
    HANGUP_INVALID_OSN_DETECTED_ERROR: gt('The server detected a synchronization problem with your client. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    HANGUP_OT_UNRESOLVABLE_OP_CONFLICT_ERROR: gt('A conflict has occurred during simultaneous editing. To solve this conflict the document is now reloaded.'),
    RENAMEDOCUMENT_FAILED_ERROR: gt('Renaming the document failed.'),
    RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR: gt('Renaming the document failed. The file name contains invalid characters'),
    RENAMEDOCUMENT_NOT_SUPPORTED_ERROR: gt('Renaming the document failed. The storage containing the file does not support renaming a file.'),
    RENAMEDOCUMENT_SAVE_IN_PROGRESS_WARNING: gt('Please wait. The document will be renamed and must be reloaded.'),
    LOADDOCUMENT_FAILED_ERROR: gt('An error occurred while loading the document.'),
    LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR: gt('An error occurred while loading the document.'),
    LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR: gt('The document is protected with a password.'),
    LOADDOCUMENT_TIMEOUT_RETRIEVING_STREAM_ERROR: gt('The document could not be loaded, because the document data could not be read in time. Please try again later.'),
    LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR: gt('This document exceeds the size and complexity limits.'),
    'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR.TEXT': gt('This document exceeds the text document size and complexity limits.'),           // fulfilling task DOCS-733
    'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR.PRESENTATION': gt('This document exceeds the presentation size and complexity limits.'),    // "Improve OOM Error Message in Client"
    'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR.SPREADSHEET': gt('This document exceeds the spreadsheet size and complexity limits.'),      // [https://jira.open-xchange.com/browse/DOCS-733]
    LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR: gt('This document exceeds the number of sheets limit.'),
    'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR.SPREADSHEET': gt('This document exceeds the number of sheets limit.'),
    LOADDOCUMENT_CALCENGINE_NOT_WORKING_ERROR: gt('The engine used for calculating formulas is not available.'),
    LOADDOCUMENT_STRICT_OOXML_NOT_SUPPORTED_ERROR: gt('This document could not be loaded, because it was created with a version of Microsoft Office prior to Office 2007.'),
    LOADDOCUMENT_FEATURE_NOT_SUPPORTED_ERROR: gt('The document could not be loaded, because it contains an unsupported feature.'),
    LOADDOCUMENT_FEATURE_NOT_SUPPORTED_FRAME_ATTACHED_TO_FRAME_ERROR: gt('The document could not be loaded, because it contains grouped text frames.'),
    LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR: gt('The document could not be loaded, because the document format is unknown'),
    LOADDOCUMENT_PRESENTATION_IS_BEING_PRESENTED_ERROR: gt('The document could not be loaded into Presentation, because it\'s currently remotely presented. Please try again later.'),
    LOADODCUMENT_MEDIATYPE_NOT_SUPPORTED_ERROR: gt('The document could not be loaded. The file format is not supported.'),
    'GENERAL_PERMISSION_READ_MISSING_ERROR.LOAD': gt('The document could not be read, because you do not have read permissions.'),
    'GENERAL_FILE_NOT_FOUND_ERROR.LOAD': gt('The document cannot be found. Please check, if the document has been removed.'),
    'GENERAL_MEMORY_TOO_LOW_ERROR.LOAD': gt('The document could not be loaded. The server is currently too busy. Please try again later.'),
    'GENERAL_FILE_CORRUPT_ERROR.LOAD': gt('The document could not be loaded. The document is corrupt.'),
    'GENERAL_SYSTEM_BUSY_ERROR.LOAD': gt('The document could not be loaded. The server is currently too busy. Please try again later.'),
    'GENERAL_UNKNOWN_ERROR.LOAD': gt('The document could not be loaded. An unknown error occurred. Please try again later.'),
    GENERAL_PERMISSION_CREATE_MISSING_ERROR: gt('Could not create document due to missing folder permissions.'),
    GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR: gt('The system is currently busy to store the document. Please try again later.'),
    BACKUPFILE_WONT_WRITE_WARNING: gt('You do not have sufficient permissions to write the backup file.'),
    GENERAL_UNKNOWN_ERROR: gt('An unknown error occurred.'),
    GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR: gt('A server component is not working. Please contact the server administrator.'),
    GENERAL_SESSION_COMPONENT_NOT_WORKING_ERROR: gt('There is a configuration problem on the server. Please contact the server administrator.'),
    BACKUPDOCUMENT_SERVICE_NOT_AVAILABLE: gt('Your changes could not be written to the original document. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    BACKUPDOCUMENT_BASEDOCSTREAM_NOT_FOUND: gt('Your changes could not be written to the original document. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    BACKUPDOCUMENT_CLIENTACTIONS_MALFORMED: gt('Your changes could not be written to the original document. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    BACKUPDOCUMENT_SYNC_NOT_POSSIBLE: gt('Your changes could not be written to the original document. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    'GENERAL_FILE_NOT_FOUND_ERROR.FLUSH': gt('The document cannot be found. Saving the document is not possible. To save your work, please copy the complete content into the clipboard and paste it into a new document.'),
    GENERAL_SESSION_INVALID_ERROR: gt('Your session is invalid. Please login again.'),
    GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR: gt('The connection to the document has been lost. Please reload the document.'),
    GENERAL_IMAGE_TYPE_NOT_SUPPORTED_ERROR: gt('The image type is not supported. The image cannot be embedded into the document.'),
    GENERAL_MEMORY_TOO_LOW_ERROR: gt('The server detected a problem with your document. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.'),
    GENERAL_SERVICE_DOWN_ERROR: gt('A server component is not working correctly. Please contact the server administrator.'),
    GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR: gt('The document could not be loaded. The server is currently too busy. Please try again later.'),
    TOO_MANY_CONNECTIONS_ERROR: gt('The document could not be opened. The maximum number of simultaneously opened documents has been reached. Please close another document first and then open this document again.'),
    LOADDOCUMENT_ENCRYPTED_FILE_CANNOT_BE_DECRYPTED_ERROR: gt('The document could not be loaded. Decrypting the document failed.'),
    LOADDOCUMENT_EMPTY_DOCUMENT_FILE_ERROR: gt('The document could not be loaded. There is no document content available as the file size is zero.'),
    LOADDOCUMENT_GUARD_AUTH_FAILED_ERROR: gt('The document could not be loaded. The authentication for the Guard component failed. Please check your password.')
};

// opt-in list: error codes to be reported by "showOnceInDocLifetime"
const SHOW_ONCE_IN_DOC_LIFETIME_CODES = new Set([
    "SAVEDOCUMENT_BACKUPFILE_CREATE_PERMISSION_MISSING_ERROR",
    "SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR",
    "SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR",
    "SAVEDOCUMENT_BACKUPFILE_IS_LOCKED_ERROR"
]);

// opt-out list: error codes to NOT be reported by "suppressDirectlyRepeated"
const DONT_SUPPRESS_DIRECTLY_REPEATED = new Set([
    "GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR"
]);

// opt-out list: error codes to NOT be reported by "createCrashDataForError"
const DONT_CREATE_CRASHDATA_FOR_ERROR = new Set([
    "HANGUP_OT_UNRESOLVABLE_OP_CONFLICT_ERROR",
    "ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR",
    "ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR",
    "LOADDOCUMENT_STRICT_OOXML_NOT_SUPPORTED_ERROR", // DOCS-2260
    "LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR", // DOCS-2794
    "LOADDOCUMENT_FILE_HAS_CHANGED_ERROR", // DOCS-3382
    "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR", // DOCS-4435
    "WARNING_DOC_MOVED_RELOAD_REQUIRED", //DOCS-4532
    "ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE", //DOCS-4532
    "ERROR_DOC_FILE_DELETED" //DOCS-4532

]);

// private functions ==========================================================

function getHeadline(errorClass, headingError, headingWarning) {
    var // heading message
        headline = headingError || headingWarning || gt('Error');

    if (_.isNumber(errorClass)) {
        headline = (errorClass === ErrorCode.ERRORCLASS_WARNING) ? headingWarning : headingError;
    }

    return headline;
}

/**
 * Provides a merged context data view to context
 * dependent data.
 *
 * @param {Object} errorContextData
 *   Context data provided by a ErrorCode instance.
 *
 * @param {Object} options
 *  Options data provided as an argument.
 *
 * @returns {Object}
 *  A merged view of context and options data.
 */
function getContextData(errorContextData, options) {
    return _.extend({}, errorContextData, options);
}

// public functions ===========================================================

export function showOnceInDocLifetime(codeAsConstant) {
    return SHOW_ONCE_IN_DOC_LIFETIME_CODES.has(codeAsConstant);
}

export function suppressDirectlyRepeated(codeAsConstant) {
    return !DONT_SUPPRESS_DIRECTLY_REPEATED.has(codeAsConstant);
}

/**
 * Whether crashData should be created for this error. Some special
 *  errors should not do it.
 */
export function createCrashDataForError(codeAsConstant) {
    return !DONT_CREATE_CRASHDATA_FOR_ERROR.has(codeAsConstant);
}

/**
 * Creates a error message object from a provided error code object. It
 * contains the translated message and headline for the user interface.
 * An unkonwn error code will be translated to a general error/headline
 * message.
 *
 * @param {Object} errorCode
 *  The error code to be translated to a user-interface error message.
 *
 * @returns {Object}
 *  A error message data object that contains the translated error message
 *  strings, type, headline and additional properties, that can be used
 *  for the DocView.yell function.
 */
export function getMessageData(errorCode, options) {

    var // mapped translated error message for provided error code
        messageData = {},
        // error code as constant
        codeAsString = _.isObject(errorCode) ? errorCode.getCodeAsConstant() : null,
        // error context for this error
        errorContext = _.isObject(errorCode) ? errorCode.getErrorContext() : ErrorContext.GENERAL,
        // error type
        errorClass = _.isObject(errorCode) ? errorCode.getErrorClass() : ErrorCode.ERRORCLASS_ERROR,
        // error context data
        errorContextData = _.isObject(errorCode) ? errorCode.getContextData() : {},
        // optional error value to be used for extended error messages
        errorValue = _.isObject(errorCode) ? errorCode.getErrorValue() : null,
        // a flexible key to map error code/context to error message
        keyToMap = null,
        // context dependent data
        contextData1 = null,
        // context dependent data
        contextData2 = null,
        // client error
        clientError = false,
        // merged context data view
        mergedContextData = null,
        // whether the application initiating the error was launched by the OX Viewer.
        isViewerMode = getBooleanOption(options, 'viewerMode', false),
        // whether this client is editor or not
        isEditor = getBooleanOption(options, 'isEditor', false),
        // error value used for error message
        hasErrorValueForMessageUsed = false,
        // whether the yell shall offer a reload button in the case of a client error
        offerReloadForClientError = true;

    // set a defined message type dependent on the error class
    messageData.type = (errorClass === ErrorCode.ERRORCLASS_WARNING) ? 'warning' : 'error';

    // use mapping tables to map the error to a meaningful error message for the user
    if (_.isString(codeAsString)) {
        // client error messages
        messageData.message = (codeAsString in CLIENT_ERROR_MESSAGES) ? CLIENT_ERROR_MESSAGES[codeAsString] : null;
        clientError = _.isString(messageData.message);
        if (!messageData.message) {
            // try to map with server error messages
            keyToMap = (_.isString(errorContext) && errorContext.length) ? (codeAsString + '.' + errorContext) : codeAsString;
            messageData.message = (keyToMap in SERVER_ERROR_MESSAGES) ? SERVER_ERROR_MESSAGES[keyToMap] : null;
            if (!messageData.message && (keyToMap !== codeAsString)) {
                // last check - try to get a message without context, if we used it on
                // last attempt
                keyToMap = codeAsString;
                messageData.message = (keyToMap in SERVER_ERROR_MESSAGES) ? SERVER_ERROR_MESSAGES[keyToMap] : null;
            }
        }
    }

    // code to handle some errors in a special way
    if (_.isString(codeAsString)) {
        mergedContextData = getContextData(errorContextData, options);

        switch (codeAsString) {
            case 'ERROR_DOCUMENT_SIZE_EXCEEDED':
                contextData1 = getStringOption(mergedContextData, MAX_DOCSIZE_ALLOWED, 0);
                //#. %1$s is the maximum allowed source file size in MB
                messageData.message = gt('The document exceeds the maximum permitted size of %1$s MB.', contextData1);
                break;

            case 'GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR':
                if (_.isString(errorValue) && (errorValue.length > 0)) {
                    //#. %1$s is the maximum allowed source file size in MB
                    messageData.message = gt('The image cannot be embedded into the document because it exceeds the maximum permitted size of %1$s MB. The URL is used instead.', errorValue);
                } else {
                    messageData.message = gt('The image cannot be embedded into the document because it exceeds the maximum permitted size. The URL is used instead.');
                }
                break;

            case 'WARNING_DOC_IS_LOCKED':
                contextData1 = getStringOption(mergedContextData, 'documentFileLockedByUser', null);
                if (_.isString(contextData1)) {
                    //#. %1$s is the name of the user who has locked the document
                    messageData.message = gt('The document has been locked by %1$s.', contextData1);
                } else {
                    messageData.message = gt('The document has been locked by another user.');
                }
                break;
            case 'BACKUPDOCUMENT_RESTORE_DOCUMENT_WRITTEN':
                contextData1 = getStringOption(mergedContextData, RESTORED_FILENAME, null);
                contextData2 = getStringOption(mergedContextData, RESTORED_PATHNAME, null);
                if (_.isString(contextData1) && _.isString(contextData2)) {
                    //#. %1$s is the name of the restore the document
                    //#. %2$s is the path of the restored the document
                    messageData.message = gt('Your changes could not be written to the original document. A new document "%1$s" was stored in folder "%2$s" containing your latest changes.', contextData1, contextData2);
                    messageData.action = 'loadrestored';
                } else {
                    messageData.message = gt('Your changes could not be written to the original document. A new document was stored containing your latest changes.');
                }
                break;
            case 'BACKUPDOCUMENT_RESTORE_DOCUMENT_PARTIAL_WRITTEN':
                contextData1 = getStringOption(mergedContextData, RESTORED_FILENAME, null);
                contextData2 = getStringOption(mergedContextData, RESTORED_PATHNAME, null);
                if (_.isString(contextData1)) {
                    //#. %1$s is the name of the restore the document
                    //#. %2$s is the path of the restored the document
                    messageData.message = gt('Your changes could not be written to the original document. A new document "%1$s" was stored in folder "%2$s" but some of your latest changes could not be saved. To save all of your last changes, please copy the complete content into the clipboard and paste it into a new document.', contextData1, contextData2);
                    messageData.action = 'loadrestored';
                } else {
                    messageData.message = gt('Your changes could not be written to the original document. A new document was stored but some of your latest changes could not be saved. To save all of your last changes, please copy the complete content into the clipboard and paste it into a new document.');
                }
                break;
            case 'RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR':
                // use the optional error value to provide the illegal character to the user
                if (_.isString(errorValue) && (errorValue.length > 0)) {
                    hasErrorValueForMessageUsed = true;
                    messageData.message = messageData.message.concat(': "').concat(errorValue).concat('"');
                } else {
                    messageData.message = messageData.message.concat('.');
                }
                break;
            case 'LOADDOCUMENT_FILE_HAS_CHANGED_ERROR':
                // use the optional error value to provide the editors user name
                if (_.isString(errorValue) && (errorValue.length > 0)) {
                    hasErrorValueForMessageUsed = true;
                    //#. %1$s is the name of the editor of the document
                    messageData.message = gt('The document cannot be loaded, because an older version of the document is currently in use by "%1$s". Please contact "%1$s" or try again later.', errorValue);
                } else {
                    messageData.message = gt('The document cannot be loaded, because an older version of the document is currently in use by someone. Please try again later.');
                }
                break;
            case 'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR':
                messageData.quitOnClose = true;
                break;
            case 'GENERAL_PERMISSION_WRITE_MISSING_ERROR':
                // we want to show a different message for viewers - therefore
                // check if this client is viewer or editor
                contextData1 = getStringOption(mergedContextData, 'oldEditUser', null);
                if (!isEditor) {
                    if (_.isString(contextData1) && contextData1.length > 0) {
                        //#. %1$s is the name of the editor of the document
                        messageData.message = gt('The permissions of the document have changed. The document could not be saved, because "%1$s" lost write permission. Please contact "%1$s" to continue the collaboration or close the document and try to open it later.', contextData1);
                    } else {
                        messageData.message = gt('The permissions of the document have changed. The document could not be saved, because the editor lost write permission. Please contact the editor to continue the collaboration or close the document and try to open it later.');
                    }
                }
                break;
            case 'GENERAL_PERMISSION_READ_MISSING_ERROR':
                // use the normal error message in case we are in the context of load
                if (ErrorContext.LOAD !== errorContext) {
                    // we want to show a different message for viewers - therefore
                    // check if this client is viewer or editor
                    contextData1 = getStringOption(mergedContextData, 'oldEditUser', null);
                    if (!isEditor) {
                        if (_.isString(contextData1) && contextData1.length > 0) {
                            //#. %1$s is the name of the editor of the document
                            messageData.message = gt('The permissions of the document have changed. The document could not be saved, because "%1$s" lost read permission. Please contact "%1$s" to continue the collaboration or close the document and try to open it later.', contextData1);
                        } else {
                            messageData.message = gt('The permissions of the document have changed. The document could not be saved, because the editor lost read permission. Please contact the editor to continue the collaboration or close the document and try to open it later.');
                        }
                    }
                }
                break;
            case 'WARNING_NO_PERMISSION_FOR_DOC':
                offerReloadForClientError = false; // not offering a useless reload for missing write privileges
                break;
        }
    }

    if (!messageData.message) {
        // default error message, if no mapping could be found
        switch (errorContext) {
            case ErrorContext.FLUSH: messageData.message = gt('The document is in an inconsistent state. To save your work, please copy the complete content into the clipboard and paste it into a new document.'); break;
            case ErrorContext.CREATEDOC: messageData.message = gt('Could not create document, because of an unknown error.'); break;
            case ErrorContext.SAVEAS: messageData.message = gt('The new document could not be saved, because of an unknown error.'); break;
            case ErrorContext.RENAME: messageData.message = gt('Renaming the document failed.'); break;
            case ErrorContext.LOAD: messageData.message = gt('An error occurred while loading the document.'); break;
            case ErrorContext.SYNCHRONIZATION: messageData.message = gt('An unknown synchronization error occurred.'); break;
            default: messageData.message = gt('An unknown error ocurred.'); break;
        }
    }

    // In case error code doesn't fill a headline, use the context to get a translated headline
    // Distinguish between error and warnings!
    if (!messageData.headline) {
        switch (errorContext) {
            case ErrorContext.OFFLINE:
            case ErrorContext.SYNCHRONIZATION: messageData.headline = getHeadline(errorClass, gt('Synchronization Error'), gt('Synchronization')); break;
            case ErrorContext.CONNECTION: messageData.headline = getHeadline(errorClass, gt('Connection Error'), gt('Connection Warning')); break;
            case ErrorContext.CREATEDOC:
            case ErrorContext.FLUSH:
            case ErrorContext.SAVEAS:
            case ErrorContext.CLOSE:
            case ErrorContext.LOAD: messageData.headline = getHeadline(errorClass, gt('Error'), gt('Warning')); break;
            case ErrorContext.RENAME: messageData.headline = getHeadline(errorClass, gt('Rename Error'), gt('Rename Warning')); break;
            case ErrorContext.READONLY: messageData.headline = gt('Read-Only Mode'); break;
            case ErrorContext.GENERAL: messageData.headline = getHeadline(errorClass, gt('Error'), gt('Warning')); break;
            case ErrorContext.RESTOREDOC: messageData.headline = gt('Restore Document Error'); break;
            default: messageData.headline = getHeadline(errorClass, gt('Error'), gt('Warning')); break;
        }
    }

    // special handling for actions dependent on the error context, e.g. include the reload button
    if (errorClass !== ErrorCode.ERRORCLASS_WARNING) {
        switch (errorContext) {
            case ErrorContext.LOAD:
            case ErrorContext.CLOSE:
            case ErrorContext.SYNCHRONIZATION:
            case ErrorContext.CONNECTION:  {
                // set reload entry for these contexts in the popup if no
                // action was set before
                messageData.action = (messageData.action || 'reload');
                break;
            }
        }
    }

    // special handling for client errors - always provide the reload button
    if (clientError && offerReloadForClientError) {
        messageData.action = 'reload';
    }

    // in some very special cases we don't want to include the special behaviour - remove it again
    if (_.isString(codeAsString)) {
        switch (codeAsString) {
            case 'LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR':
            case 'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR':
            case 'LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR':
            case 'LOADDOCUMENT_CALCENGINE_NOT_WORKING_ERROR':
            case 'LOADDOCUMENT_STRICT_OOXML_NOT_SUPPORTED_ERROR':
            case 'LOADDOCUMENT_FEATURE_NOT_SUPPORTED_ERROR':
            case 'LOADDOCUMENT_FEATURE_NOT_SUPPORTED_FRAME_ATTACHED_TO_FRAME_ERROR':
            case 'GENERAL_FILE_CORRUPT_ERROR':
            case 'GENERAL_FILE_NOT_FOUND_ERROR':
            case 'LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR':
            case 'WARNING_SESSION_INVALID_DETECTED':
            case 'WARNING_READONLY_DUE_TO_OFFLINE':
            case 'WARNING_SYNC_FOR_VIEWER':
            case 'WARNING_SYNC_AFTER_OFFLINE':
            case 'LOADODCUMENT_MEDIATYPE_NOT_SUPPORTED_ERROR':
            case 'ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE':
            case 'ERROR_DOC_FILE_DELETED': delete messageData.action; break;
            case 'HANGUP_OT_UNRESOLVABLE_OP_CONFLICT_ERROR':
            case 'ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR':
            case 'ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR': delete messageData.action; messageData.type = 'warning'; messageData.duration = 30000; break;
            case 'LOADDOCUMENT_FAILED_ERROR':
                if (errorContext === ErrorContext.CLOSE) {
                    // special behavior for close - don't provide reload in this case
                    delete messageData.action;
                }
                break;
        }
    }

    if (messageData.type === 'error') {
        //use the cause error for extented message with errorcode & fileid
        var causeError = getOption(options, 'causeError', errorCode);
        if (_.isObject(causeError)) {
            messageData.errorCode = causeError.getCodeAsConstant();
            var subMessage = 'Error\xa0code:\xa0' + messageData.errorCode + '.';
            var originalFileId = String(getOption(options, ORIGINAL_FILEID, ''));
            if (originalFileId) {
                if (originalFileId.length > 30) { originalFileId = '...' + originalFileId.slice(-27); }
                subMessage += ' File\xa0id:\xa0' + originalFileId + '.';
            }
            if (_.isString(errorValue) && (errorValue.length > 0) && !hasErrorValueForMessageUsed) {
                // include possible error value for extended message if not been used yet
                if (errorValue.length > 30) { errorValue = '...' + errorValue.slice(-27); }
                subMessage += ' Error\xa0value:\xa0' + errorValue;
            }
            messageData.subMessage = _.noI18n(subMessage);
        }
    }

    if (isViewerMode) {
        // special behavior for Viewer mode:
        //  - don't provide reload
        //  - don't provide special close action
        delete messageData.action;
        delete messageData.quitOnClose;
    }

    return messageData;
}
