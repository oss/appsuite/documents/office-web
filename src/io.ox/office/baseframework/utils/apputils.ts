/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import ox from "$/ox";
import gt from "gettext";

import type { App } from "$/io.ox/core/desktop";
import FilesAPI from "$/io.ox/files/api";
import capabilities from "$/io.ox/core/capabilities";

import { is, to, str, set, debug } from "@/io.ox/office/tk/algorithms";
import { type IconId, createElement, createAnchor, insertHiddenNodes } from "@/io.ox/office/tk/dom";
import { globalEvents } from "@/io.ox/office/tk/events";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { DEBUG } from "@/io.ox/office/tk/config";
import { AbortError } from "@/io.ox/office/tk/objects";

import type { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";

// types ======================================================================

/**
 * Generic data for a Documents application.
 */
export interface AppTypeSpec {

    /**
     * The type specifier of the application.
     */
    appType: AppType;

    /**
     * Whether the application is a portal application.
     */
    isPortal: boolean;
}

/**
 * Global configuration for Documents portal/editor applications of a specific
 * type.
 */
export interface GlobalAppConfig {

    /**
     * The type identifier of the portal/editor application.
     */
    appType: AppType;

    /**
     * The identifier of an icon that will be shown in the "restore window" of
     * a minimized application.
     */
    appIcon: IconId;

    /**
     * The name of the CSS variable containing the color of the application
     * icon to be shown in the GUI.
     */
    appColor: string;

    /**
     * The translated title of the application to be shown in the GUI.
     */
    appTitle: string;

    /**
     * A regular expression matching the extensions of all file types supported
     * by the respective editor application.
     */
    appExtRE: RegExp;
}

/**
 * The shape of the common debug main module with all registration entry points
 * for a Documents application.
 */
export type DebugCommonMainModule = typeof import("@/io.ox/office/debug/common/main");

// enums ----------------------------------------------------------------------

/**
 * Enumeration for all supported Documents applications. The string values of
 * the enumeration members are equal to the path names of the source modules,
 * and to the capability names of the respective apps.
 */
export enum AppType {

    /**
     * The text processing application.
     */
    TEXT = "text",

    /**
     * The spreadsheet editor application.
     */
    SPREADSHEET = "spreadsheet",

    /**
     * The presentation editor application.
     */
    PRESENTATION = "presentation",

    /**
     * The presenter application.
     */
    PRESENTER = "presenter"
}

/**
 * An enumeration for file formats supported by Documents editor applications.
 */
export enum FileFormatType {

    /**
     * Office Open XML file format (Microsoft Office).
     */
    OOX = "ooxml",

    /**
     * Open Document Format (OpenOffice, LibreOffice).
     */
    ODF = "odf"
}

/**
 * An enumeration for different origins causing to request a document flush
 * operation.
 */
export enum FlushReason {

    /**
     * The document needs to be downloaded ("Download", "Print" actions), or
     * copied ("Save As" actions).
     */
    DOWNLOAD = "download",

    /**
     * The document will be sent as an email attachment.
     */
    EMAIL = "email",

    /**
     * The document will be shared with other users.
     */
    SHARE = "share",

    /**
     * The application is shutting down.
     */
    QUIT = "quit",

    /**
     * The Presenter application will start a presentation for the document.
     */
    PRESENT = "present",

    /**
     * The document needs to be flushed due to some other user interaction,
     * e.g. activating the browser tab.
     */
    USER = "user"
}

/**
 * An enumeration for different reasons causing to shutdown the application.
 */
export enum QuitReason {

    /**
     * The application will be closed manually by the user, e.g. by clicking
     * the "Quit" button.
     */
    USER = "user",

    /**
     * The application will be closed because it will be reloaded (a new
     * instance is being launched).
     */
    RELOAD = "reload",

    /**
     * The application will be closed automatically, e.g. by a timer.
     */
    AUTO = "auto",

    /**
     * The application will be closed due to a critical error, e.g. during
     * early initialization.
     */
    ERROR = "error",

    /**
     * The application will be closed due to the user's logout.
     */
    LOGOUT = "logout"
}

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted when a floating application opens or closes.
         *
         * @param visible
         *  Whether a floating application is currently hovering the main
         *  application.
         */
        "floatingapp:visible": [visible: boolean];
    }
}

// constants ==================================================================

/**
 * Common module path prefix for all Documents applications.
 */
export const DOCS_MODULE_PREFIX = "io.ox/office/";

/**
 * Module path prefix for all Documents portal applications.
 */
export const PORTAL_MODULE_PREFIX = `${DOCS_MODULE_PREFIX}portal/`;

/**
 * Global configuration of the Text application.
 */
export const TEXT_APP_CONFIG: GlobalAppConfig = {
    appType: AppType.TEXT,
    appIcon: "bi:file-earmark-text",
    appColor: "--app-color-text",
    appTitle: gt.pgettext("app", "Text"),
    appExtRE: FilesAPI.Model.prototype.types.doc
};

/**
 * Global configuration of the Spreadsheet application.
 */
export const SPREADSHEET_APP_CONFIG: GlobalAppConfig = {
    appType: AppType.SPREADSHEET,
    appIcon: "svg:spreadsheet",
    appColor: "--app-color-spreadsheet",
    appTitle: gt.pgettext("app", "Spreadsheet"),
    appExtRE: FilesAPI.Model.prototype.types.xls
};

/**
 * Global configuration of the Presentation application.
 */
export const PRESENTATION_APP_CONFIG: GlobalAppConfig = {
    appType: AppType.PRESENTATION,
    appIcon: "svg:presentation",
    appColor: "--app-color-presentation",
    appTitle: gt.pgettext("app", "Presentation"),
    appExtRE: FilesAPI.Model.prototype.types.ppt
};

/**
 * Array with global configuration of all Documents portal/editor applications.
 */
export const GLOBAL_APP_CONFIGS: readonly GlobalAppConfig[] = [
    TEXT_APP_CONFIG,
    SPREADSHEET_APP_CONFIG,
    PRESENTATION_APP_CONFIG
];

// singletons =================================================================

const metricsLogger = new Logger("office:log-metrics", { tag: "METRICS", tagColor: 0xFF8000 });

// all floating applications that are currently visible
const floatingApps = new Set<App>();

// private functions ==========================================================

function handleFloatingApp(app: App, insert: boolean): void {
    const wasEmpty = !floatingApps.size;
    set.toggle(floatingApps, app, insert);
    const isEmpty = !floatingApps.size;
    if (wasEmpty !== isEmpty) {
        globalEvents.emit("floatingapp:visible", !isEmpty);
    }
}

// listen to all starting applications
ox.on("app:start", app => {

    // handle applications with a floating window only
    const floatModel = app.getWindow()?.floating?.model;
    if (!floatModel) { return; }

    // register the application, and unregister it when it closes
    handleFloatingApp(app, true);
    app.on("quit", () => handleFloatingApp(app, false));

    // handle minimizing floating window
    floatModel.on("change:minimized", () => {
        handleFloatingApp(app, !floatModel.get("minimized"));
    });
});

// public functions ===========================================================

/**
 * Returns the global configuration of the specified Documents portal/editor
 * applications, if available.
 *
 * @param appType
 *  The type identifier of the Documents application.
 *
 * @returns
 *  The configuration settings of the application, if available.
 */
export function getGlobalAppConfig(appType: AppType): Opt<GlobalAppConfig> {
    return GLOBAL_APP_CONFIGS.find(entry => entry.appType === appType);
}

/**
 * Returns the module base path of the specified portal application.
 *
 * @param appType
 *  The type identifier of the application.
 *
 * @returns
 *  The module base path of the specified portal application.
 */
export function getPortalModulePath(appType: AppType): string {
    return PORTAL_MODULE_PREFIX + appType;
}

/**
 * Returns all required capabilities for the specified portal application.
 *
 * The portal applications are bound to the availability of the respective
 * editor application (capability `appType`), and the Drive application
 * (capability "infostore"). Furthermore, each portal application can be
 * disabled with its own capability "<apptype>portaldisabled", and needs a user
 * account (not for "guest" or "anonymous" sessions).
 *
 * @param appType
 *  The type identifier of the application.
 *
 * @returns
 *  All required capabilities for the specified portal application.
 */
export function getPortalCapabilities(appType: AppType): string {
    return `${appType} infostore !${appType}portaldisabled !guest !anonymous`;
}

/**
 * Returns whether the portal application for the specified editor application
 * is available.
 *
 * @param appType
 *  The type identifier of the application.
 *
 * @returns
 *  Whether the portal application for the specified editor application is
 *  available.
 */
export function isPortalAvailable(appType: AppType): boolean {
    return (appType !== AppType.PRESENTER) && capabilities.has(getPortalCapabilities(appType));
}

/**
 * Returns the module base path of the specified editor application.
 *
 * @param appType
 *  The type identifier of the application.
 *
 * @returns
 *  The module base path of the specified editor application.
 */
export function getEditorModulePath(appType: AppType): string {
    return DOCS_MODULE_PREFIX + appType;
}

/**
 * Returns all required capabilities for the specified editor application.
 *
 * @param appType
 *  The type identifier of the application.
 *
 * @returns
 *  All required capabilities for the specified editor application.
 */
export function getEditorCapabilities(appType: AppType): string {
    return `${appType} infostore`;
}

/**
 * Returns whether the portal application for the specified editor application
 * is available.
 *
 * @param appType
 *  The type identifier of the application.
 *
 * @returns
 *  Whether the portal application for the specified editor application is
 *  available.
 */
export function isEditorAvailable(appType: AppType): boolean {
    return capabilities.has(getEditorCapabilities(appType));
}

/**
 * Parses the passed application module path, and returns data for Documents
 * applications.
 *
 * @param modulePath
 *  The module path to be parsed.
 *
 * @returns
 *  The data for a Documents application; or `undefined` for any other module
 *  path.
 */
export function parseModulePath(modulePath: string): Opt<AppTypeSpec> {

    let appType: Opt<AppType>;
    const isPortal = modulePath.startsWith(PORTAL_MODULE_PREFIX);

    if (isPortal) {
        appType = to.enum(AppType, modulePath.slice(PORTAL_MODULE_PREFIX.length));
    } else if (modulePath.startsWith(DOCS_MODULE_PREFIX)) {
        appType = to.enum(AppType, modulePath.slice(DOCS_MODULE_PREFIX.length));
    }

    return appType ? { appType, isPortal } : undefined;
}

/**
 * Returns the module base path of the specified Documents application.
 *
 * @param appSpec
 *  The type identifier, and portal flag of the application.
 *
 * @returns
 *  The module base path of the specified Documents application.
 */
export function getModulePath(appSpec: AppTypeSpec): string {
    return appSpec.isPortal ? getPortalModulePath(appSpec.appType) : getEditorModulePath(appSpec.appType);
}

/**
 * Changes the favicon to the specified application.
 *
 * @param appType
 *  The application type identifier.
 */
export function setAppFavIcon(appType: AppType): void {

    // configuration of a Documents application favicon
    interface FavIconSpec {
        id: string;     // DOM element identifier of the `<link>` element
        rel: string;    // relation identifier for the `<link>` element
        file: string;   // filename to be used for the "href" attribute
    }

    // specifications of all application favicons to be updated
    const FAVICON_SPECS: FavIconSpec[] = [
        { id: "favicon",         rel: "icon",             file: "favicon.ico" },
        { id: "homescreen-icon", rel: "apple-touch-icon", file: "icon180.png" }
    ];

    // full path to the icon images of the specified application
    const path = `${ox.root}/io.ox/office/${appType}/resource/icons`;

    // create or update all known favicons
    for (const { id, rel, file } of FAVICON_SPECS) {
        const link = document.querySelector(`head>#${id}`) ?? document.head.appendChild(createElement("link", { id }));
        link.setAttribute("rel", rel);
        link.setAttribute("href", `${path}/${file}`);
        if (file.endsWith(".ico")) {
            link.setAttribute("type", "image/x-icon");
        }
    }
}

/**
 * Returns whether a floating application window is currently hovering the main
 * application.
 *
 * @returns
 *  Whether a floating application window is currently hovering the main
 *  application.
 */
export function hasFloatingApp(): boolean {
    return floatingApps.size > 0;
}

/**
 * Tracks a file action event for the specified application.
 *
 * @param appType
 *  The type of the application to track the event for.
 *
 * @param target
 *  The target specifier.
 *
 * @param action
 *  The action specifier.
 */
export function trackActionEvent(appType: AppType, target: string, action: string): void {
    const data = { app: `${appType}-portal`, target, action: `${appType}-${action}` };
    metricsLogger.log(`app="${data.app}" target="${data.target}" action="${data.action}"`);
    // DOCS-4110: TODO: replace old metrics with "OX Count"
    // Metrics.trackEvent({ app: `${appType}-portal`, target, action: `${appType}-${action}` });
}

/**
 * Creates a generic text file download in the browser.
 *
 * @param fileName
 *  The name of the file to be offered by the browser.
 *
 * @param contents
 *  The text contents of the file to be downloaded, as string blob, or as array
 *  of text lines which will be joined with newline characters.
 */
export function downloadTextFile(fileName: string, contents: string | string[]): void {

    // create a `Blob` object from the text contents to be downloaded
    const blob = new Blob([is.array(contents) ? str.joinTail(contents, "\n") : contents], { type: "text/plain" });
    const href = URL.createObjectURL(blob);

    // normal download code
    const downloadLink = createAnchor(href, { label: _.noI18n("Download") });
    downloadLink.download = fileName;
    insertHiddenNodes(downloadLink);

    // must be a native click on a DOM node to trigger the link
    downloadLink.click();
    downloadLink.remove();
}

/**
 * In debug mode, loads the main debug module with registration entry points
 * for the application. Does not fail if the module does not exist. In
 * production mode, this function does nothing, regardless whether the debug
 * main module exists.
 *
 * @param docApp
 *  The application instance that wants to load a debug module.
 *
 * @param fn
 *  A callback function what will be invoked with the module contents.
 *
 * @returns
 *  A promise that will *always* be fulfilled regardless of the result.
 */
export async function importDebugMainModule(docApp: BaseApplication, fn: (module: DebugCommonMainModule) => MaybeAsync): Promise<void> {
    if (DEBUG) {
        try {
            const module = await import("@/io.ox/office/debug/common/main");
            // DOCS-4813: application may have been closed while the module is loaded
            if (!docApp.docModel || docApp.isInQuit()) { throw new AbortError(true); }
            await fn(module);
        } catch (err) {
            debug.logScriptError(err, "importDebugMainModule: importing debug main module failed");
        }
    }
}
