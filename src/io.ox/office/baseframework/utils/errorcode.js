/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import { getNumberOption, getObjectOption, getStringOption } from '@/io.ox/office/tk/utils';
import _ from '$/underscore';

// class ErrorCode ============================================================

/**
 * Provides methods to check a server result for possible errors.
 *
 * @param {Object} [resultData]
 *  The result object directly provided by the server which contains error
 *  information that are used to initialize ErrorCode instance.
 */
export default class ErrorCode {

    constructor(resultData) {

        var // the error description
            description = '',

            // the error code as string constant
            codeAsStringConstant = 'NO_ERROR',

            // error class
            errorClass = ErrorCode.ERRORCLASS_NO_ERROR,

            // source of the error
            errorSource = ErrorCode.ERRORSOURCE_CLIENT,

            // error context
            errorContext = null,

            // optional error context data
            errorContextData = null,

            // optional error value
            errorValue = null;

        // public methods -----------------------------------------------------

        /**
         * Checks if this instance represents an error state or not.
         *
         * @returns {Boolean}
         *  TRUE if the instance represents an error state, FALSE if not.
         */
        this.isError = function () {
            return errorClass >= ErrorCode.ERRORCLASS_ERROR;
        };

        /**
         * Checks if this instance represents a warning state or not.
         *
         * @returns {Boolean}
         *  TRUE if the instance represents warning state, FALSE if not.
         */
        this.isWarning = function () {
            return errorClass === ErrorCode.ERRORCLASS_WARNING;
        };

        /**
         * Provides a textual description what error has happened.
         *
         * @returns {String}
         *  A description of the error.
         */
        this.getDescription = function () {
            return description;
        };

        /**
         * Set a textual description what error has happened.
         *
         * @param {String} errorDescription
         *  A description of the error.
         */
        this.setDescription = function (errorDescription) {
            description = errorDescription;
        };

        /**
         * Provides a unique string constant that specifies the error.
         *
         * @returns {String}
         *  A unique string constant for the error.
         */
        this.getCodeAsConstant = function () {
            return codeAsStringConstant;
        };

        /**
         * Provides the error class of the error, which describes the
         * severity of the error.
         *
         * @returns {Number}
         */
        this.getErrorClass = function () {
            return errorClass;
        };

        /**
         * Sets the error class of the error.
         *
         * @param {Number} newErrorClass
         *  The new error class.
         */
        this.setErrorClass = function (newErrorClass) {
            errorClass = newErrorClass;
        };

        /**
         * Provides the source of the error
         *
         * @returns {Number}
         */
        this.getErrorSource = function () {
            return errorSource;
        };

        /**
         * Provides the context of the error. A context describes the
         * specific area, where the error ocurred, e.g. load,
         * save, delete etc.
         *
         * @returns {String|null}
         *  The error context or null, if the context is unknown.
         */
        this.getErrorContext = function () {
            return errorContext;
        };

        /**
         * Provides an optional error value which describes in more
         * detail what caused the error. E.g. the illegal character
         * that resulted in a failed rename operation.
         *
         * @returns {String}
         *  The error value. Can be an empty string if no error
         *  value was set.
         */
        this.getErrorValue = function () {
            return errorValue;
        };

        /**
         * Provides the textual representation of the error code object.
         *
         * @returns {String}
         *  The textual representation of the error code object.
         */
        this.getErrorText = function () {
            return 'Error: constant=' + codeAsStringConstant + ', value=' + errorValue + ', description=' + description;
        };

        /**
         * Sets the source of the error code.
         *
         * @param {Number} source
         *  The source of the error code.
         */
        this.setErrorSource = function (source) {
            if (_.isNumber(source) && (source >= ErrorCode.ERRORSOURCE_SERVER) && (source <= ErrorCode.ERRORSOURCE_CLIENT)) {
                errorSource = source;
            }
        };

        /**
         * Sets the context of the error code.
         *
         * @param {String} context
         *  The new context of the error code.
         */
        this.setErrorContext = function (context) {
            errorContext = context;
        };

        /**
         * Gets the context data of this error code.
         *
         * @returns {Object}
         *  An optional object containing property/value pairs with
         *  additional data for this error.
         */
        this.getContextData = function () {
            return errorContextData;
        };

        /**
         * Sets the context data of this error code.
         *
         * @param {Object} newContextData
         *  An object containing property/value pairs with additional data for
         *  this error.
         */
        this.setContextData = function (newContextData) {
            errorContextData = newContextData;
        };

        /**
         * Provides the error code content as property object, which can
         * be used to initialize a new ErrorCode object.
         *
         * @returns {Object}
         *  An object which can be used to initialize a new error code
         *  object.
         */
        this.getAsResult = function () {
            var result = { error: {} };

            result.error[ErrorCode.PROPERTY_ERRORDESCRIPTION] = description;
            result.error[ErrorCode.PROPERTY_ERRORCODEASSTRING] = codeAsStringConstant;
            result.error[ErrorCode.PROPERTY_ERRORCLASS] = errorClass;
            result.error[ErrorCode.PROPERTY_ERRORSOURCE] = errorSource;
            result.error[ErrorCode.PROPERTY_ERRORCONTEXTDATA] = errorContextData;
            result.error[ErrorCode.PROPERTY_ERRORVALUE] = errorValue;
            return result;
        };

        // initialization -----------------------------------------------------

        if (_.isObject(resultData)) {
            if (_.isObject(resultData.error)) {
                // check the provided resultData object for error information - this
                // is the default error format of a server answer
                description = getStringOption(resultData.error, ErrorCode.PROPERTY_ERRORDESCRIPTION, '');
                codeAsStringConstant = getStringOption(resultData.error, ErrorCode.PROPERTY_ERRORCODEASSTRING, 'NO_ERROR');
                errorClass = getNumberOption(resultData.error, ErrorCode.PROPERTY_ERRORCLASS, ErrorCode.ERRORCLASS_NO_ERROR);
                errorContextData = getObjectOption(resultData.error, ErrorCode.PROPERTY_ERRORCONTEXTDATA, {});
                errorValue = getStringOption(resultData.error, ErrorCode.PROPERTY_ERRORVALUE, '');
            } else if (!_.isString(getStringOption(resultData, ErrorCode.PROPERTY_OX_ERRORID, null))) {
                // we also support to provide a error object directly - detect and ignore plain error data from backend
                description = getStringOption(resultData, ErrorCode.PROPERTY_ERRORDESCRIPTION, '');
                codeAsStringConstant = getStringOption(resultData, ErrorCode.PROPERTY_ERRORCODEASSTRING, 'NO_ERROR');
                errorClass = getNumberOption(resultData, ErrorCode.PROPERTY_ERRORCLASS, ErrorCode.ERRORCLASS_NO_ERROR);
                errorSource = getNumberOption(resultData, ErrorCode.PROPERTY_ERRORSOURCE, ErrorCode.ERRORSOURCE_CLIENT);
                errorContextData = getObjectOption(resultData, ErrorCode.PROPERTY_ERRORCONTEXTDATA, {});
                errorValue = getStringOption(resultData, ErrorCode.PROPERTY_ERRORVALUE, '');
            }
        }
    }

    // static functions -------------------------------------------------------

    /**
     * Extracts the error information from result data received from the server
     * side.
     *
     * @param {Object} serverResult
     *  The result data sent by the server.
     *
     * @returns {Object|Null}
     *  The error information extracted from the server data or null if no
     *  error information could be extracted.
     */
    static extractErrorInformation(serverResult) {
        return (_.isObject(serverResult) && serverResult.error) ? { error: serverResult.error } : null;
    }

    /**
     * Extracts the error information from a real-time result received from the
     * server side.
     *
     * @param {Object} rtResult
     *  The result data sent by the server.
     *
     * @returns {Object|Null}
     *  An error object containing the error code of the real-time answer or
     *  null if no error information could be extracted.
     */
    static extractRealtimeError(rtResult) {
        return (_.isObject(rtResult) && rtResult.error && _.isNumber(rtResult.error.code) && _.isString(rtResult.error.prefix)) ? { code: rtResult.error.code, prefix: rtResult.error.prefix } : null;
    }

    /**
     * Checks, if an arbitrary value is a error code object.
     *
     * @param {Any} value
     *  The value to be checked.
     *
     * @returns {Boolean}
     *  Whether the value supports vital functions and looks like a error code
     *  object.
     */
    static isErrorCode(value) {
        return _.isObject(value) && _.isFunction(value.isError) && _.isFunction(value.getErrorText);
    }

    /**
     * If the given error is an ErrorObject return this, if not convert the error value to an ErrorCode Object if possible
     * otherwise return the default ErrorCode GENERAL_ERROR.
     *
     * @param {Any} error
     *  The error to convert.
     *
     * @returns {ErrorCode} new ErrorObject or if the error parameter is a Error return this.
     */
    static getErrorCode(error) {
        return ErrorCode.isErrorCode(error) ? error :  new ErrorCode(error?.errorForErrorCode === true ? error : ErrorCode.GENERAL_ERROR);
    }
}

// constants --------------------------------------------------------------

// the constant error code as string for no error
ErrorCode.CONSTANT_NO_ERROR = 'NO_ERROR';

// the property names
ErrorCode.PROPERTY_ERRORDESCRIPTION = 'errorDescription';
ErrorCode.PROPERTY_ERRORCODEASSTRING = 'error';
ErrorCode.PROPERTY_ERRORCLASS = 'errorClass';
ErrorCode.PROPERTY_ERRORSOURCE = 'errorSource';
ErrorCode.PROPERTY_ERRORCONTEXTDATA = 'errorContextData';
ErrorCode.PROPERTY_ERRORVALUE = 'errorValue';

// normal error data sent by plain backend including RT
ErrorCode.PROPERTY_OX_ERRORID = 'error_id';

// the error classes describing the severity of an error
ErrorCode.ERRORCLASS_NO_ERROR = 0;
ErrorCode.ERRORCLASS_WARNING = 1;
ErrorCode.ERRORCLASS_ERROR = 2;
ErrorCode.ERRORCLASS_FATAL_ERROR = 3;

// the source of the error (server or client)
ErrorCode.ERRORSOURCE_SERVER = 1;
ErrorCode.ERRORSOURCE_CLIENT = 2;

// a general error
ErrorCode.GENERAL_ERROR = { error: 'GENERAL_UNKNOWN_ERROR', errorClass: ErrorCode.ERRORCLASS_ERROR };
