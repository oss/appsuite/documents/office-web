/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, fun, ary } from "@/io.ox/office/tk/algorithms";
import { EventHub } from "@/io.ox/office/tk/events";

// types ======================================================================

/**
 * A callback function that calculates the best zoom factor for the specified
 * zoom type.
 *
 * @param type
 *  The zoom type to calculate the zoom factor for.
 *
 * @returns
 *  The best zoom factor for the passed zoom type; or `null` to keep the
 *  current zoom factor.
 */
export type ZoomTypeResolverFn = (type: string) => number | null;

/**
 * Optional parameters for changing the zoom settings.
 */
export interface ZoomEventOptions {

    /**
     * If set to `true`, the zoom manager will not emit an event, if the zoom
     * state did not change. Mutually exclusive with the option "force".
     * Default value is `false`.
     */
    silent?: boolean;

    /**
     * If set to `true`, the zoom manager will always emit an event, also if
     * the zoom state did not change. Mutually exclusive with the option
     * "silent". Default value is `false`.
     */
    force?: boolean;
}

/**
 * Current zoom state emitted by change events of a `ZoomState` instance.
 */
export interface ZoomStateEvent {
    /** The new zoom type. */
    type: string;
    /** The new zoom factor. */
    factor: number;
    /** Scaling factor from old to new zoom factor. */
    scale: number;
    /** Options passed to the setter method. */
    options: Opt<ZoomEventOptions>;
}

/**
 * Types of events emitted by a `ZoomState` instance.
 */
export interface ZoomStateEventMap {

    /**
     * Will be emitted after the zoom type, or the effective zoom factor have
     * changed.
     *
     * @param state
     *  The event data containing the current zoom state settings.
     */
    "change:state": [state: ZoomStateEvent];
}

// class ZoomState ============================================================

/**
 * A generic wrapper for a zoom state (type identifier and effective factor),
 * and a list of predefined zoom steps. Used for sharing consistent zoom
 * functionality in all kinds of applications.
 *
 * Dynamic zoom types (like "Fit to screen" and similar) can be registered with
 * method `register`. A custom callback function takes responsibility of
 * recalculating the effective zoom factor.
 *
 * Method `set` sets a new zoom type and (optionally) zoom factor (or triggers
 * calculation of dynamic zoom factors), and triggers a change event afterwards
 * if necessary.
 */
export class ZoomState extends EventHub<ZoomStateEventMap> {

    /**
     * All predefined zoom steps.
     */
    readonly steps: readonly number[];

    /**
     * The smallest predefined zoom step.
     */
    readonly min: number;

    /**
     * The largest predefined zoom step.
     */
    readonly max: number;

    // the type registry
    readonly #types = new Map<string, ZoomTypeResolverFn>();
    // the current zoom type
    #type = "fixed";
    // the current zoom factor
    #factor: number;

    // constructor ------------------------------------------------------------

    constructor(steps: readonly number[]) {
        super();
        this.steps = steps.filter(step => step > 0).sort(math.compare);
        this.min = this.steps[0];
        this.max = this.steps[this.steps.length - 1];
        this.#factor = this.clampFactor(1);
        this.register("fixed", fun.null);
    }

    // accessors --------------------------------------------------------------

    /**
     * Returns the current zoom type identifier.
     */
    get type(): string {
        return this.#type;
    }

    /**
     * Returns the current zoom factor.
     */
    get factor(): number {
        return this.#factor;
    }

    // public methods ---------------------------------------------------------

    /**
     * Clamps the passed zoom factor into the range of supported zoom factors.
     *
     * Does *NOT* modify the state of this instance.
     *
     * @param factor
     *  The zoom factor to be clamped.
     *
     * @returns
     *  The resulting clamped zoom factor.
     */
    clampFactor(factor: number): number {
        return math.floorp(math.clamp(factor, this.min, this.max), 0.01);
    }

    /**
     * Clamps the passed scaling factor so that applying it to the current zoom
     * factor would not leave the supported zoom range.
     *
     * Does *NOT* modify the state of this instance.
     *
     * @param scale
     *  The scaling factor to be clamped.
     *
     * @returns
     *  The resulting clamped scaling factor.
     */
    clampScale(scale: number): number {
        return this.clampFactor(this.#factor * scale) / this.#factor;
    }

    /**
     * Registers a new zoom type to be supported by this instance, and a
     * callback function to be used to calculate the best zoom factor for this
     * zoom type.
     *
     * @param type
     *  The type identifier. Must not have been registered before.
     *
     * @param fn
     *  A callback function that calculates the best zoom factor for the new
     *  zoom type.
     */
    register(type: string, fn: ZoomTypeResolverFn): void {
        if (this.#types.has(type)) { throw new RangeError(`ZoomState: type "${type}" already registered`); }
        this.#types.set(type, fn);
    }

    /**
     * Resolves the dynamic zoom factor for the specified zoom type.
     *
     * Does *NOT* modify the state of this instance.
     *
     * @param type
     *  The zoom type to be resolved. Must be of the type keywords supported by
     *  this instance (see method `register`). The built-in type "fixed"
     *  returns the current zoom factor.
     *
     * @returns
     *  The calculated zoom factor for the specified zoom type.
     */
    resolve(type: string): number {
        const fn = this.#types.get(type);
        if (!fn) { throw new RangeError(`ZoomState: unknown type "${type}"`); }
        return fn(type) ?? this.#factor;
    }

    /**
     * Changes the current zoom type and factor, and fires an event.
     *
     * @param type
     *  The new zoom type. Must be of the type keywords supported by this
     *  instance (see method `register`). The built-in type "fixed" specifies a
     *  fixed zoom factor.
     *
     * @param [factor]
     *  A precalculated zoom factor to be set. If omitted or set to `null`, the
     *  callback function registered for the specified zoom type will be
     *  invoked to obtain the new zoom factor. Built-in zoom type "fixed" will
     *  just keep the current zoom factor.
     *
     * @param [options]
     *  Optional parameters for event handling.
     */
    set(type: string, factor?: Nullable<number>, options?: ZoomEventOptions): void {

        // resolve callback function for zoom factor calculation
        const fn = this.#types.get(type);
        if (!fn) { throw new RangeError(`ZoomState: unknown type "${type}"`); }

        // calculate dynamic zoom factor if no zoom factor has been passed
        if (!factor) { factor = fn(type) ?? this.#factor; }

        // clamp fixed zoom factors to range of predefined zoom steps (not for dynamic zoom types)
        if (type === "fixed") { factor = this.clampFactor(factor); }

        // trigger change event if something has changed
        if (options?.force || (this.#type !== type) || (this.#factor !== factor)) {
            const scale = factor / this.#factor;
            this.#type = type;
            this.#factor = factor;
            if (!options?.silent) {
                this.emit("change:state", { type, factor, scale, options });
            }
        }
    }

    /**
     * Sets the zoom type to "fixed", and multiplies the current zoom factor
     * with the passed scaling factor.
     *
     * @param scale
     *  The scaling factor to be applied to the current zoom factor.
     *
     * @param [options]
     *  Optional parameters for event handling.
     */
    scale(scale: number, options?: ZoomEventOptions): void {
        this.set("fixed", this.#factor * scale, options);
    }

    /**
     * Refreshes the effective zoom factor for a custom zoom type by invoking
     * the registered callback function.
     *
     * @param [options]
     *  Optional parameters for event handling.
     */
    refresh(options?: ZoomEventOptions): void {
        // Pass `null` as zoom factor to trigger invoking the callback function.
        // Type "fixed" sets current factor, but option "force" may be passed to fire again.
        this.set(this.#type, null, options);
    }

    /**
     * Returns whether the current zoom factor can be decreased.
     */
    canDec(): boolean {
        return this.min < this.#factor;
    }

    /**
     * Sets the zoom type to "fixed", and decreases the zoom factor to the
     * nearest smaller predefined zoom step.
     *
     * @param [options]
     *  Optional parameters for event handling.
     */
    dec(options?: ZoomEventOptions): void {
        const factor = ary.fastFindLastValue(this.steps, step => step < this.#factor);
        // `factor` may be undefined, but option "force" may be passed to fire again
        this.set("fixed", factor, options);
    }

    /**
     * Returns whether the current zoom factor can be increased.
     */
    canInc(): boolean {
        return this.#factor < this.max;
    }

    /**
     * Sets the zoom type to "fixed", and increases the zoom factor to the
     * nearest larger predefined zoom step.
     *
     * @param [options]
     *  Optional parameters for event handling.
     */
    inc(options?: ZoomEventOptions): void {
        const factor = ary.fastFindFirstValue(this.steps, step => this.#factor < step);
        // `factor` may be undefined, but option "force" may be passed to fire again
        this.set("fixed", factor, options);
    }
}
