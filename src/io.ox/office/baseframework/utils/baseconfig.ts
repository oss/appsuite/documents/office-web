/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import capabilities from "$/io.ox/core/capabilities";

// re-exports =================================================================

export * from "@/io.ox/office/tk/config";

// constants ==================================================================

/**
 * Specifies whether the OX Mail application is available.
 */
export const MAIL_AVAILABLE = capabilities.has("webmail");

/**
 * Specifies whether the OX Contacts application is available.
 */
export const CONTACTS_AVAILABLE = capabilities.has("contacts");

/**
 * Specifies whether the server-side document converter is available.
 */
export const CONVERTER_AVAILABLE = capabilities.has("document_preview");

/**
 * Specifies whether the OX Presenter application is available to play a
 * presentation document.
 */
export const PRESENTER_AVAILABLE = CONVERTER_AVAILABLE && capabilities.has("presenter");

/**
 * Specifies whether the OX Presenter application is able to play a remote
 * presentation.
 */
export const REMOTE_PRESENTER_AVAILABLE = PRESENTER_AVAILABLE && capabilities.has("remote_presenter");

/**
 * Specifies whether the text app is available.
 */
export const TEXT_AVAILABLE = capabilities.has("text");

/**
 * Specifies whether the spreadsheet app is available.
 */
export const SPREADSHEET_AVAILABLE = capabilities.has("spreadsheet");

/**
 * Specifies whether the presentation app is available.
 */
export const PRESENTATION_AVAILABLE = capabilities.has("presentation");

/**
 * Specifies whether the switchboard is available.
 */
export const SWITCHBOARD_AVAILABLE = capabilities.has("switchboard");

/**
 * Specifies whether the open AI service is available.
 */
export const OPENAI_AVAILABLE = capabilities.has("openai");

/**
 * Specifies whether Collabora online is available as document editor.
 */
export const DOCUMENTS_COLLABORA = capabilities.has("documents_collabora");

/**
 * Specifies whether encrypted documents can be loaded and edited.
 */
export const GUARD_AVAILABLE = capabilities.has("guard") && capabilities.has("guard-docs") && !DOCUMENTS_COLLABORA; // TODO WOPI (disabled with issue #53, reenable for issue #25)
