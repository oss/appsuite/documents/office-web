/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";

// class BaseMVCFactory =======================================================

/**
 * A global factory singleton (per application type) implementing creation of:
 *
 * - a resource manager that prefetches translated JSON resources,
 * - a document model instance,
 * - a document view instance,
 * - a document controller instance,
 *
 * (in this order) for a new application instance.
 */
export abstract class BaseMVCFactory<DocAppT extends BaseApplication> {

    // constructor ------------------------------------------------------------

    protected constructor() { }

    // public methods ---------------------------------------------------------

    /**
     * Creates and returns a new resource manager instance. This method will be
     * called once for each new application, before creating the model, view,
     * and controller instance.
     *
     * @returns
     *  The new resource manager.
     */
    abstract createResourceManager(docApp: DocAppT): DocAppT["resourceManager"];

    /**
     * Creates and returns a new document model instance. This method will be
     * called once for each new application, after creating the resource
     * manager, and before creating the view and the controller.
     *
     * @param docApp
     *  The application instance that will own the new document model returned
     *  from this method.
     *
     * @returns
     *  The new document model.
     */
    abstract createModel(docApp: DocAppT): DocAppT["docModel"];

    /**
     * Creates and returns a new document view instance. This method will be
     * called directly after creating the model, and before creating the
     * controller.
     *
     * @param docApp
     *  The application instance that will own the new document view returned
     *  from this method.
     *
     * @param docModel
     *  The document model just returned from the method `createModel()`.
     *
     * @returns
     *  The new document view.
     */
    abstract createView(docApp: DocAppT, docModel: DocAppT["docModel"]): DocAppT["docView"];

    /**
     * Creates and returns a new document controller instance. This method will
     * be called directly after creating the model and the view.
     *
     * @param docApp
     *  The application instance that will own the new document controller
     *  returned from this method.
     *
     * @param docModel
     *  The document model just returned from the method `createModel()`.
     *
     * @param docView
     *  The document view just returned from the method `createView()`.
     *
     * @returns
     *  The new document controller.
     */
    abstract createController(docApp: DocAppT, docModel: DocAppT["docModel"], docView: DocAppT["docView"]): DocAppT["docController"];
}
