/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AppWindow } from "$/io.ox/core/desktop";

import { NodeSource } from "@/io.ox/office/tk/dom";
import { SendRequestOptions, ResponsePromise } from "@/io.ox/office/tk/utils/io";

import { FlushReason, QuitReason } from "@/io.ox/office/baseframework/utils/apputils";
import { BaseResourceManager } from "@/io.ox/office/baseframework/resource/resourcemanager";
import { BaseModel } from "@/io.ox/office/baseframework/model/basemodel";
import { BaseView } from "@/io.ox/office/baseframework/view/baseview";
import { BaseController } from "@/io.ox/office/baseframework/controller/basecontroller";
import { FileDescriptor, CoreApp } from "@/io.ox/office/baseframework/app/appfactory";
import { AbstractApplication } from "@/io.ox/office/baseframework/app/abstractapplication";
import { BaseMVCFactory } from "@/io.ox/office/baseframework/app/mvcfactory";
import { XAppAccess } from "@/io.ox/office/baseframework/app/xappaccess";

// types ======================================================================

/**
 * Type mapping for the events emitted by `BaseApplication` instances.
 */
export interface BaseApplicationEventMap {

    /**
     * Will be emitted before the quit handlers of this application will be
     * called.
     */
    "docs:beforequit": [];

    /**
     * Will be emitted before the destructors of the document controller, view,
     * model, and this application will be called.
     */
    "docs:destroying": [];

    /**
     * Will be emitted after updating the file name of the document.
     *
     * @param fileName
     *  The current short filename (without extension).
     */
    "docs:filename": [fileName: string];

    /**
     * Will be emitted after all unsaved document contents have been flushed
     * successfully to the server.
     */
    "docs:flush:success": [];

    /**
     * Will be emitted after trying to flush unsaved document contents to the
     * server has failed.
     */
    "docs:flush:error": [];

    /**
     * Will be emitted after the session has changed.
     */
    "docs:session:changed": [];

    /**
     * Will be emitted after a global "relogin:required" event (a relogin is
     * required by core UI).
     */
    "docs:session:invalid": [];
}

export interface BaseApplicationQuitOptions {

    /**
     * The reason for quitting the application. The reason will be passed to
     * all registered before-quit handlers, and all registered quit handlers.
     * Default value is `QuitReason.USER`.
     */
    reason?: QuitReason;

    /**
     * Additional user data to be processed for specific quit reasons.
     */
    userData?: object;
}

// class BaseApplication ======================================================

export interface BaseApplication extends XAppAccess<BaseApplication> { }

export class BaseApplication<
    EvtMapT extends BaseApplicationEventMap = BaseApplicationEventMap
> extends AbstractApplication<BaseApplicationQuitOptions, EvtMapT> implements XAppAccess<BaseApplication> {

    readonly resourceManager: BaseResourceManager<BaseApplication, any>;
    readonly docModel: BaseModel;
    readonly docView: BaseView;
    readonly docController: BaseController;

    readonly importStartPromise: JPromise;
    readonly importFinishPromise: JPromise;

    constructor(
        coreApp: CoreApp,
        mvcFactory: BaseMVCFactory<BaseApplication>,
    );

    getLaunchOption(key: string): unknown;

    getWindow(): AppWindow;
    getWindowNode(): JQuery;

    getUserSettingsValue(key: string, defValue?: unknown): unknown;
    setUserSettingsValue(key: string, value: unknown): JPromise;

    getFullFileName(): string | null;
    getShortFileName(): string | null;
    getFileExtension(): string | null;
    getFilePath(): string[];

    isViewerMode(): boolean;
    isInQuit(): boolean;

    hasFileDescriptor(): boolean;
    getFileDescriptor(): Opt<Readonly<FileDescriptor>>;
    getFileParameters(): Dict;

    sendRequest(module: string, params?: Dict, options?: SendRequestOptions): ResponsePromise;
    sendFileRequest(module: string, params?: Dict, options?: SendRequestOptions): ResponsePromise;
    requireServerFeatures(features: string | string[]): JPromise;

    hasUnsavedChanges(): boolean;
    hasUnsavedChangesOffice(): boolean;

    destroyImageNodes(nodes: NodeSource): void;
    getRootNode(): JQuery;
    setRootAttribute(name: string, value: string): void;

    isTextApp(): boolean;
    isSpreadsheetApp(): boolean;
    isPresentationApp(): boolean;

    protected registerConverter(type: string, converterFn: FuncType<JPromise<unknown>>): void;

    protected implQuit(options?: BaseApplicationQuitOptions): Promise<void>;
    protected implInitializeApp(): MaybeAsync;
    protected implInitializeFile(): MaybeAsync<FileDescriptor>;
    protected implImportDocument(): MaybeAsync;
    protected implFlushDocument(reason: FlushReason): MaybeAsync;
    protected implExtendMessageData(message: object): void;
    protected implGetPrintParams(): Opt<Dict>;
}
