/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import Wizard from "$/io.ox/core/tk/wizard";

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import { Logger, globalLogger } from "@/io.ox/office/tk/utils/logger";
import { isDialogOpen } from "@/io.ox/office/tk/dialogs";

import { getDebugFlag, getFlag, setValue, registerDebugCommand } from "@/io.ox/office/baseframework/utils/baseconfig";
import type { AppType } from "@/io.ox/office/baseframework/utils/apputils";
import { type CoreApp, getCurrentEditorCoreApp, getCurrentEditorApp } from "@/io.ox/office/baseframework/app/appfactory";
import type { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";
import { type GuidedStepConfig, type GuidedStep, guidedStep } from "@/io.ox/office/baseframework/app/guidedstep";

// re-exports =================================================================

export type { GuidedStepConfig, GuidedStep };

// types ======================================================================

/**
 * Configuration options for a `GuidedTour` instance.
 */
export interface GuidedTourConfig {

    /**
     * If set to `true`, the tour will only be started, if it has not been
     * shown ever (according to a specific configuration item). Default value
     * is `false`.
     */
    once?: boolean;

    /**
     * Specifies whether the guided tour has been started automatically as
     * result of some application state (`true`), or has been started manually
     * by the user (`false`).
     */
    auto?: boolean;
}

/**
 * A factory function that must return a new instance of `GuidedTour` to be
 * started.
 */
export type GuidedTourFactoryFn<DocAppT extends BaseApplication> = (config?: GuidedTourConfig) => MaybeAsync<GuidedTour<DocAppT>>;

// private types --------------------------------------------------------------

interface AppTourRegistryEntry {
    factoryFn: GuidedTourFactoryFn<any>;
    shownConfigKey: string;
}

// globals ====================================================================

// configuration of all registered application tours
const appTourRegistry = new Map<string, AppTourRegistryEntry>();

// logger for guided tour events
export const tourLogger = new Logger("office:log-tours", {
    tag: "TOUR",
    tagColor: 0xFFFF00
});

// class GuidedTour ===========================================================

/**
 * A guided tour through an OX Documents application. Provided to add some
 * convenience and special behavior to generic AppSuite tours.
 */
export class GuidedTour<DocAppT extends BaseApplication> extends Wizard {

    // static properties ------------------------------------------------------

    // whether a tour is currently running, for to prevent recursion
    static #isRunning = false;

    // static functions -------------------------------------------------------

    /**
     * Starts the specified tour, and sets a server configuration item that
     * specifies that the tour has been shown.
     *
     * @param factoryFn
     *  A factory function that must return a new instance of `GuidedTour` to
     *  be started. The options passed to this method will be forwarded to the
     *  factory function.
     *
     * @param shownConfigKey
     *  The key of the user configuration item used to save the "shown" flag
     *  for the guided tour.
     *
     * @param [config]
     *  Configuration options for the tour instance (will be passed to the tour
     *  factory function).
     *
     * @returns
     *  Whether the tour has actually been started. The tour may reject to
     *  start e.g. when the option "once" has been passed.
     */
    static run<DocAppT extends BaseApplication>(factoryFn: GuidedTourFactoryFn<DocAppT>, shownConfigKey: string, config?: GuidedTourConfig): boolean {

        // do not run a tour twice if specified (override the "once" flag with debug flag)
        if (config?.once && !getDebugFlag("office:show-tours") && getFlag(shownConfigKey)) {
            tourLogger.trace("$badge{GuidedTour} run: aborted (already shown once)");
            return false;
        }

        // do not top-level `await` the factory (synchronous boolean return value)
        jpromise.invokeFloating(async () => {
            try {

                // construct a new tour instance
                const tour = await factoryFn(config);

                // immediately write the "shown" flag into the configuration
                jpromise.floating(setValue(shownConfigKey, true));

                // Workaround for bug 62755: Write the "shown" flag again after the tour
                // has finished. Backend server may have written other configuration data
                // which might have deleted the "shown" flag set initially.
                tour.on("stop", () => jpromise.floating(setValue(shownConfigKey, true)));

                // run the tour
                tour.start(config);

            } catch (err) {
                tourLogger.exception(err);
            }
        });

        return true;
    }

    /**
     * Registers a welcome tour for the specified application type.
     *
     * A menu entry starting the tour will be added to the global settings
     * dropdown menu of the AppSuite.
     *
     * @param moduleName
     *  The full module name of the application.
     *
     * @param factoryFn
     *  A factory function that must return a new instance of `GuidedTour` to
     *  be started.
     *
     * @param shownConfigKey
     *  The key of the user configuration item used to save the "shown" flag
     *  for the application tour.
     */
    static registerAppTour<DocAppT extends BaseApplication>(moduleName: string, factoryFn: GuidedTourFactoryFn<DocAppT>, shownConfigKey: string): void {
        tourLogger.info(`$badge{GuidedTour} registerAppTour: registering guided tour for module "${moduleName}"`);
        appTourRegistry.set(moduleName, { factoryFn, shownConfigKey });
        Wizard.registry.add({ id: `default/${moduleName}` }, () => GuidedTour.runAppTour(moduleName));
    }

    /**
     * Returns whether a welcome tour has been registered for the specified
     * application type with the static function `registerAppTour`.
     *
     * @param moduleName
     *  The full module name of the application.
     *
     * @returns
     *  Whether a welcome tour has been registered for the specified application
     *  type.
     */
    static hasAppTour(moduleName: string): boolean {
        return appTourRegistry.has(moduleName);
    }

    /**
     * Returns whether the welcome tour for the specified application type has
     * been shown at least once.
     *
     * @param moduleName
     *  The full module name of the application.
     *
     * @returns
     *  Whether the specified welcome tour has been shown at least once.
     */
    static isAppTourShown(moduleName: string): boolean {
        const registryEntry = appTourRegistry.get(moduleName);
        return !!registryEntry && getFlag(registryEntry.shownConfigKey);
    }

    /**
     * Starts the welcome tour for the specified application type.
     *
     * @param moduleName
     *  The full module name of the application.
     *
     * @param [config]
     *  Configuration options for the tour instance.
     *
     * @returns
     *  Whether the tour has actually been started. The tour may reject to start
     *  e.g. when the option `once` has been passed.
     */
    static runAppTour(moduleName: string, config?: GuidedTourConfig): boolean {
        const registryEntry = appTourRegistry.get(moduleName);
        return !!registryEntry && GuidedTour.run(registryEntry.factoryFn, registryEntry.shownConfigKey, config);
    }

    // properties -------------------------------------------------------------

    /** The type specifier of the application. */
    readonly appType: AppType;

    /** Public access to instance configuration. */
    readonly config: GuidedTourConfig;

    // constructor ------------------------------------------------------------

    constructor(appType: AppType, config?: GuidedTourConfig) {

        // base constructor
        super();

        // properties
        this.appType = appType;
        this.config = { ...config };

        // log all events to console
        this.on("all", (type: string, index?: number) => tourLogger.log(() => {
            const title = (type.startsWith("step:") && is.number(index)) ? this.get(index)?.$el?.find(".wizard-title").text() : "";
            return `$badge{GuidedTour} event $badge{${type}} fired${title ? ` from step "${title}"` : ""}`;
        }));

        // initialize the new tour
        this.on("before:start", () => {
            GuidedTour.#isRunning = true;
            tourLogger.info("$badge{GuidedTour} starting tour");
        });

        // tear down the tour (finished or canceled)
        this.on("stop", () => {
            GuidedTour.#isRunning = false;
            tourLogger.info("$badge{GuidedTour} tour stopped");
        });
    }

    // public getters ---------------------------------------------------------

    /**
     * @returns
     *  The core editor application that is currently active.
     *
     * @throws
     *  If called without active core editor application.
     */
    get coreApp(): CoreApp {
        const coreApp = getCurrentEditorCoreApp({ appType: this.appType, allowWopi: true });
        if (coreApp) { return coreApp; }
        throw new ReferenceError("GuidedTour: no active Documents application found");
    }

    /**
     * @returns
     *  The Documents editor application that is currently active.
     *
     * @throws
     *  If called without active internal Documents application.
     */
    get docApp(): DocAppT {
        const docApp = getCurrentEditorApp({ appType: this.appType, allowWopi: true });
        if (docApp) { return docApp as DocAppT; }
        throw new ReferenceError("GuidedTour: no active Documents application found");
    }

    /**
     * @returns
     *  The document model of the active Documents editor application.
     *
     * @throws
     *  If called without active internal Documents application.
     */
    get docModel(): DocAppT["docModel"] {
        return this.docApp.docModel;
    }

    /**
     * @returns
     *  The document view of the active Documents editor application.
     *
     * @throws
     *  If called without active internal Documents application.
     */
    get docView(): DocAppT["docView"] {
        return this.docApp.docView;
    }

    /**
     * @returns
     *  The document controller of the active Documents editor application.
     *
     * @throws
     *  If called without active internal Documents application.
     */
    get docController(): DocAppT["docController"] {
        return this.docApp.docController;
    }

    // public methods ---------------------------------------------------------

    /**
     * Starts this tour, except if another tour is currently running.
     */
    override start(_config?: GuidedTourConfig): this {

        // do not start the tour if already running
        if (GuidedTour.#isRunning) {
            tourLogger.warn("$badge{GuidedTour} start: aborted (another tour is currently running)");
            return this;
        }

        // do not start the tour if a modal dialog is open (e.g. Settings in multi-tab mode)
        if (isDialogOpen()) {
            tourLogger.warn("$badge{GuidedTour} start: aborted (modal dialog is open)");
            return this;
        }

        // DOCS-3928: hide floating window for "Help" app
        $('.window-container.floating-window.io-ox-help-window:visible .floating-header .btn[data-action="close"]').trigger("click");

        // do not start the tour if other floating windows are open (e.g. "Compose Mail" which may trigger an "Unsaved changes" dialog)
        // (Help window closes asynchronously, needed to exclude it explicitly in the CSS selector)
        if ($(".window-container.floating-window:not(.io-ox-help-window):visible").length) {
            tourLogger.warn("$badge{GuidedTour} start: aborted (floating window is open)");
            return this;
        }

        // start the guided tour
        return super.start();
    }

    /**
     * Creates a new tour step that has been extended with specific Documents
     * functionality.
     *
     * @returns
     *  A new tour step.
     */
    override step(config?: GuidedStepConfig): GuidedStep<this> {
        return guidedStep(super.step(config), config);
    }
}

// static initialization ======================================================

// register handler for the debug command "office.debug.resetAllTours"
registerDebugCommand("resetAllTours", () => {
    appTourRegistry.forEach((registryEntry, moduleName) => {
        globalLogger.log(`$badge{GuidedTour} Resetting shown flag for app "${moduleName}".`);
        jpromise.floating(setValue(registryEntry.shownConfigKey, null));
    });
});
