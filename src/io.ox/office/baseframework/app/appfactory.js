/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';
import { locationHash } from '$/url';

import ext from '$/io.ox/core/extensions';
import ui from '$/io.ox/core/desktop';
import apps from '$/io.ox/core/api/apps';

import { map, dict, jpromise } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, createIcon, createButton } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { BROWSER_TAB_SUPPORT, isDocsChildTab } from '@/io.ox/office/tk/utils/tabutils';
import { authorizeGuard, getFile, getFileBaseName, getSanitizedFileName, hasGuardExt } from '@/io.ox/office/tk/utils/driveutils';

import { AppType, getPortalModulePath, isPortalAvailable, getEditorModulePath, setAppFavIcon } from '@/io.ox/office/baseframework/utils/apputils';
import { PerformanceTracker } from '@/io.ox/office/baseframework/utils/performancetracker';
import { DOCUMENTS_COLLABORA, getFlag, getUrlFlag } from '@/io.ox/office/baseframework/utils/baseconfig';
import { QUIT_BUTTON_OPTIONS } from '@/io.ox/office/baseframework/view/baselabels';
import { AbstractAppFactory, prefetchApp, launchApp } from '@/io.ox/office/baseframework/appregistry';

import '@/io.ox/office/baseframework/app/appfactory.less';

// globals ====================================================================

// number of documents already launched, per application type
const launchCountMap = new Map/*<AppType, number>*/();

// private functions ==========================================================

/**
 * Creates and initializes an application window for a Documents application.
 *
 * @param {CoreApp} coreApp
 *  The application to create the window for.
 *
 * @returns {AppWindow}
 *  The application window.
 */
function createAppWindow(coreApp) {

    // the root application node to embed the window into
    const rootNode = coreApp.get("docsLaunchConfig").page;

    // create the core application window
    var appWindow = ui.createWindow({
        name: coreApp.getName(),
        search: false,
        chromeless: true,
        page: rootNode
    });
    coreApp.setWindow(appWindow);

    // bug 53506: track whether the busy mode is currently active
    var isBusy = false;

    // add the new method isBusy()
    appWindow.isBusy = function () {
        return isBusy;
    };

    // override the method busy() to add specific behavior
    appWindow.busy = _.wrap(appWindow.busy, function (busyMethod, pct, sub, callback, options) {

        // invoke core method
        return busyMethod.call(this, pct, sub, function () {

            // the window blocker element (bound to 'this')
            var blockerNode = this;
            // the header/footer container nodes
            var headerNode = blockerNode.find('.header');
            var footerNode = blockerNode.find('.footer');

            // initialization when busy mode starts
            if (!isBusy) {
                isBusy = true;

                // clean header/footer container nodes
                headerNode.empty();
                footerNode.empty();

                // add special marker class for custom CSS formatting
                blockerNode.addClass('io-ox-office-main').css('opacity', '');

                // add top label to header area (but not in tabbed mode where the
                // filename is already visible in the topbar's text edit field)
                if (!BROWSER_TAB_SUPPORT && options?.topLabel) {
                    headerNode.append($('<div class="clear-title">').text(options.topLabel));
                }

                // bug 59041: add a custom quit button (no quit button anymore on top-bar application tabs)
                // bug 63978: don't show custom quit button if app is plugged by the Viewer
                if (!coreApp.get('plugged') && !isStandaloneDocsAppInTab(coreApp) && !isDocsAppInMultiTabMode(coreApp)) {
                    headerNode.append(createButton({
                        type: "bare",
                        action: "quit",
                        icon: QUIT_BUTTON_OPTIONS.icon,
                        tooltip: QUIT_BUTTON_OPTIONS.tooltip,
                        click() {
                            coreApp.trigger('docs:busy:quit'); // bug 53941: Quit button must trigger busy cancel handler
                            handleLaunchError(coreApp, false);
                        }
                    }));
                }
            }

            // always invoke the user-defined callback function
            return callback?.call(appWindow, blockerNode, headerNode, footerNode);
        });
    });

    // override the method idle() to add specific behavior
    appWindow.idle = _.wrap(appWindow.idle, function (idleMethod) {
        if (isBusy) {
            idleMethod.call(this);
            isBusy = false;
        }
        return this;
    });

    return appWindow;
}

// set application title
function setAppTitle(coreApp, fileDesc) {

    // no title for plugged applications (viewer mode)
    const appWindow = coreApp.get("plugged") ? undefined : coreApp.getWindow();
    if (!appWindow) { return; }

    // the name of the edited file without extension
    const shortFileName = getFileBaseName(getSanitizedFileName(fileDesc));
    coreApp.setTitle(shortFileName);
    appWindow.setTitle(shortFileName);
}

/**
 * Determine the location after an error happens while launching the application.
 *
 * @param {CoreApp} coreApp
 *  The application to create the window for.
 *
 * @param {boolean} isReloading
 *  Whether the new application is launched due to a fast reload.
 */
function handleLaunchError(coreApp, isReloading) {

    // the old reloaded app must be destroyed explicitly by setting reload to
    // done or it will stay in background
    if (isReloading) { setAppReloadDone(coreApp); }

    // without another reachable app (Drive or Portal), do nothing and stay in the app
    if (!isStandaloneDocsAppInTab(coreApp)) {
        // DOCS-4216: do not switch to portal when app is plugged into Viewer
        if (BROWSER_TAB_SUPPORT && !coreApp.get('plugged')) {
            launchPortalOnQuit(coreApp);
        }
        coreApp.quit();
    }
}

/**
 * Deregisters listeners and removes the taskbar item for the specified editor
 * application in the Appsuite taskbar.
 *
 * @param {CoreApp} coreApp
 *  The application (most likely a quitting app) that should deregister the
 *  taskbar item.
 */
function deregisterTaskbar(coreApp) {
    coreApp.trigger('app:deregistertaskbar');
}

/**
 * Loads the WOPI application class module.
 *
 * @returns {Promise<AppClassType<WopiApplication>>}
 */
async function loadWopiAppClass() {
    const { default: WopiApp } = await import("@/io.ox/office/wopi/app/wopiapplication");
    return WopiApp;
}

// public functions ===========================================================

/**
 * Returns whether the passed application instance is a Documents editor core
 * application.
 *
 * @param {App} anyApp
 *  The application instance to be checked.
 *
 * @param {MatchEditorAppOptions} [options]
 *  Additional options to match the editor application.
 *
 * @returns {coreApp is CoreApp}
 *  Whether the passed application is a Documents editor core application.
 */
export function isEditorCoreApp(anyApp, options) {
    const typeTag = anyApp.get("docsAppTypeTag");
    const isEditorApp = (typeTag === "docs") || (options?.allowWopi && (typeTag === "wopi"));
    return isEditorApp && !anyApp.get("plugged") && (!options?.appType || (anyApp.get("docsAppType") === options.appType));
}

/**
 * Returns the current Documents editor core application instance if available,
 * otherwise `undefined`.
 *
 * @param {MatchEditorAppOptions} [options]
 *  Additional options to specify the current editor application.
 *
 * @returns {Opt<CoreApp>}
 *  The current Documents editor core application if available; otherwise
 *  `undefined`.
 */
export function getCurrentEditorCoreApp(options) {
    const currApp = ui.App.getCurrentApp();
    const isEditorApp = !!currApp && isEditorCoreApp(currApp, options);
    return isEditorApp ? currApp : undefined;
}

/**
 * Returns the current Documents editor application instance if available,
 * otherwise `undefined`.
 *
 * @param {MatchEditorAppOptions} [options]
 *  Additional options to specify the current editor application.
 *
 * @returns {Opt<AbstractApplication>}
 *  The current Documents editor application if available; otherwise
 *  `undefined`.
 */
export function getCurrentEditorApp(options) {
    const currApp = ui.App.getCurrentApp();
    const isEditorApp = !!currApp && isEditorCoreApp(currApp, options);
    return isEditorApp ? currApp.get("docsAppInstance") : undefined;
}

/**
 * Returns a running application instance (not necessarily the application that
 * is currently active/visible) for the specified file descriptor.
 *
 * @param {AppType} appType
 *  The type specifier of the application to be found.
 *
 * @param {FileDescriptor} fileDesc
 *  A file descriptor containing the file identifier, file name, and other
 *  important settings for a file.
 *
 * @param {MatchEditorAppOptions} [options]
 *  Additional options to specify the editor application.
 *
 * @returns {Opt<AbstractApplication>}
 *  A running Documents editor application instance for the specified file
 *  descriptor if available; otherwise `undefined`.
 */
export function getRunningEditorAppForFile(appType, fileDesc, options) {

    // find running applications for the specified application type and file descriptor
    const moduleName = getEditorModulePath(appType);
    const runningApps = ui.App.get(moduleName).filter(coreApp => {
        if (!isEditorCoreApp(coreApp, options)) { return false; }
        const appFileDesc = coreApp.get("docsFileDesc");
        return !!appFileDesc && dict.equals(fileDesc, appFileDesc, ["source", "folder_id", "id", "attached"]);
    });

    // return running application if existing
    if (runningApps.length > 1) {
        globalLogger.warn('$badge{launch} getRunningEditorAppForFile: found multiple applications for the same file.');
    }
    return runningApps[0]?.get("docsAppInstance");
}

/**
 * Launches the specified Documents Portal application.
 *
 * @param {AppType} appType
 *  The type specifier of the Portal application to be launched.
 *
 * @param {object} [launchConfig]
 *  The launch options to be passed to the Portal application constructor.
 *
 * @returns {Promise<App>}
 *  A promise that fulfils with the launching Portal application instance.
 */
export function launchPortalApp(appType, launchConfig) {
    return launchApp(getPortalModulePath(appType), launchConfig);
}

/**
 * Launches the specified Documents portal application.
 *
 * @param {CoreApp} quittingApp
 *  A reference to the quitting app.
 *
 * @returns {Promise<App>}
 *  A promise that fulfils with the launching Portal application instance.
 */
export function launchPortalOnQuit(quittingApp) {
    // bug OXUIB-81: remove taskbar items immediately when launching new app out of a non quitted one
    deregisterTaskbar(quittingApp);
    return launchPortalApp(quittingApp.get("docsAppType"));
}

/**
 * Fetches the source code of the specified Documents application.
 *
 * @param {AppType} appType
 *  The type specifier of the application to be prefetched.
 */
export function prefetchDocsApp(appType) {
    // do not prefetch internal application code when using external WOPI editors
    if (!DOCUMENTS_COLLABORA) {
        prefetchApp(getEditorModulePath(appType));
    }
}

/**
 * Launches the specified Documents application.
 *
 * @param {AppType} appType
 *  The type specifier of the application to be launched.
 *
 * @param {BaseLaunchConfig} [launchConfig]
 *  The launch options to be passed to the application constructor.
 *
 * @returns {Promise<CoreApp>}
 *  A promise that fulfils with the launching application instance.
 */
export function launchDocsApp(appType, launchConfig) {
    return launchApp(getEditorModulePath(appType), launchConfig);
}

/**
 * Returns the stand-alone mode from the server-side configuration flag
 * 'standalone', or the application's stand-alone URL hash fragment. This
 * function also checks if the application is loaded in an `<iframe>` and
 * gets the correct hash fragment from the it's source URL.
 *
 * @returns {Boolean}
 *  The state of the stand-alone mode.
 */
export function isStandaloneMode() {
    if (getFlag('module/standalone')) { return true; }
    var standalone = (window.self !== window.top) ? _.deserialize(window.self.location.hash.substr(1)).standalone : locationHash('standalone');
    return standalone ? (standalone.toLowerCase() === 'true' || standalone === '1') : false;
}

/**
 * Returns whether the specified Documents application is the only possible
 * application in this browser tab. For certain capabilities and also for guest
 * users, the Documents Portal application can be disabled. In combination with
 * browser tab support it can mean that there is no other application available
 * in the browser tab.
 *
 * @param {CoreApp} coreApp
 *  The core application instance.
 *
 * @returns {boolean}
 *  Whether the specified Documents editor application is the only possible app
 *  in this browser tab.
 */
export function isStandaloneDocsAppInTab(coreApp) {
    const appType = coreApp.get("docsAppType");
    // presenter does not have a portal app
    return BROWSER_TAB_SUPPORT && ((appType === AppType.PRESENTER) || !isPortalAvailable(appType));
}

/**
 * Returns whether the passed application is OX Text, OX Spreadsheet, or OX
 * Presentation in multi tab mode.
 *
 * @param {CoreApp} coreApp
 *  The core application instance.
 *
 * @returns {boolean}
 * Whether the specified application is OX Text, OX Spreadsheet, or OX
 * Presentation in multi tab mode.
 */
export function isDocsAppInMultiTabMode(coreApp) {
    const appType = coreApp.get("docsAppType");
    return BROWSER_TAB_SUPPORT && (appType === AppType.TEXT || appType === AppType.SPREADSHEET || appType === AppType.PRESENTATION);
}

/**
 * Relaunches the passed application in the background. When launching in the
 * background finishes, or when an error happens, the returned promise will be
 * fulfilled.
 *
 * @param {CoreApp} quittingApp
 *  The quitting application that will be reloaded.
 *
 * @param {BaseLaunchConfig} [launchConfig]
 *  The launch options to be passed to the application constructor.
 *
 * @returns {Promise<void>}
 *  A promise that will be fulfilled when launching the new application has
 *  been finished, or when an error occurred.
 */
export function reloadApplication(quittingApp, launchConfig) {

    return new Promise((resolve, reject) => {

        // the unique identifier of the old application shutting down
        const appId = quittingApp.id;

        // wait for the "docs:reload:done" event that will be fired by the new application to be launched
        ox.once(`docs:reload:done:${appId}`, () => {
            // Do 'deregisterTaskbar' not earlier when reloading the app. The old app is waiting until
            // the new app has been finished and therefore the task item needs to stay so long.
            deregisterTaskbar(quittingApp);
            resolve();
        });

        // launch the new application, ignore the launch promise (promise will be fulfilled in event handler above)
        const appType = quittingApp.get("docsAppType");
        jpromise.floating(launchDocsApp(appType, { ...launchConfig, reloadedByApp: appId }), reject);
    });
}

/**
 * Set the a reloading process for a currently reloading app to done.
 * Calling this will reveal the reloaded app for the user and notify the
 * reloaded app so that is can be finally destroyed.
 *
 * @param {CoreApp} reloadingApp
 *  A reference to the app that should deregister the taskbar item.
 */
export function setAppReloadDone(reloadingApp) {
    var appIdFromReloadedApp = reloadingApp.options.reloadedByApp;
    // notify the reloaded app to finish the quitting process
    ox.trigger(`docs:reload:done:${appIdFromReloadedApp}`, reloadingApp);
    // show reloading app and create taskBarItem when finished
    reloadingApp.trigger('revealApp');
}

// class BaseAppFactory =======================================================

/**
 * A factory for new applications of a specific type.
 *
 * @param {GlobalAppConfig} factoryConfig
 *  Configuration options for the application factory.
 */
export class BaseAppFactory extends AbstractAppFactory {

    constructor(appConfig) {

        // base constructor
        super({ moduleName: getEditorModulePath(appConfig.appType) });

        // public properties
        this.appType = appConfig.appType;

        // the icon shown in the top bar launcher
        this.appIcon = appConfig.appIcon;

        // hide core AppSuite topbar in standalone mode
        if (isStandaloneMode()) {
            ext.point('io.ox/core/topbar').disable('default');
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Tries to find a running application which is working on a file described
     * in the passed launch options. If no such application exists, a new
     * application instance will be created and returned.
     *
     * @param {BaseLaunchConfig} [launchConfig]
     *  The configuration to be passed to the application constructor.
     *
     * @returns {CoreApp}
     *  An existing or new application instance for the passed launch options.
     *  The new application is partially initialized, and will complete its
     *  initialization to a `BaseApplication` asynchronously.
     */
    /*protected*/ implCreateApp(launchConfig) {

        // always use an object for configuration
        const hasConfig = !!launchConfig;
        launchConfig = launchConfig || {};

        // whether the current app launch is a reload from an previous document
        const isReloading = !!launchConfig.reloadedByApp;
        // whether the application is plugged by another application (e.g. Viewer app for spreadsheets)
        const isPlugged = !!launchConfig.plugged;

        // create the import performance tracker (constructor takes current timestamp!)
        const launchTracker = new PerformanceTracker('LAUNCH', this.appType, 'launch_application', '$badge{AppFactory} Start launching new application instance...');

        // DOCS-1469: log number of launches per document type
        const count = map.add(launchCountMap, this.appType, 1);
        launchTracker.setItem('launchCount', count);

        // adjustments for browser child tabs (DOCS-4137: not in main tab, e.g. Spreadsheet viewer app)
        if (isDocsChildTab()) {
            // adjust CSS classes at root element for tabbed mode
            document.documentElement.classList.add('office-tab');
            // update app favicon for tabbed mode (e.g. open a text document in spreadsheet portal, close Doc and go back to portal)
            setAppFavIcon(this.appType);
        }

        // all has parameters in the current URL
        const hash = locationHash();

        // create new launchOptions from url for creating an empty document if the "new" url flag is set.
        if (BROWSER_TAB_SUPPORT || !launchConfig.id) {
            if (getUrlFlag('new')) {
                launchConfig = {
                    moduleName: this.moduleName,
                    action: 'new',
                    file: null,
                    target_filename: hash.target_filename,
                    target_folder_id: hash.target_folder_id
                };
                if (hash.folder && hash.id) {
                    launchConfig.templateFile = { folder_id: hash.folder, id: hash.id };
                }
                if (hash.crypto_action && hash.auth_code) {
                    launchConfig.cryptoAction = hash.crypto_action;
                    launchConfig.auth_code = hash.auth_code;
                }
                if (hash.decrypt_auth_code) {
                    launchConfig.decrypt_auth_code = hash.decrypt_auth_code;
                }
            }
        }

        // Load from URL: different launch options are given, check if we have 'simple' file descriptor (file id, folder id)
        // in the URL hash params and build new simple launch options from it
        if (!hasConfig || launchConfig.id) {
            if (getUrlFlag('convert')) {
                launchConfig = {
                    moduleName: this.moduleName,
                    action: 'convert',
                    file: null,
                    template: true,
                    keepFile: true,
                    target_filename: hash.destfilename,
                    target_folder_id: hash.destfolderid,
                    templateFile: { folder_id: launchConfig.folder, id: launchConfig.id }
                };
            } else {

                var fileObj = {
                    folder_id: launchConfig.folder,
                    id: launchConfig.id,
                    source: 'drive'
                };

                // needed e.g. for edit as new from mail
                if (hash.origin) {
                    try {
                        fileObj.origin = JSON.parse(hash.origin);
                    } catch (err) {
                        globalLogger.exception(err, '$badge{AppFactory} getApp: error while adding the origin object');
                    }
                }

                launchConfig = { action: 'load', file: fileObj };
            }
            if (!launchConfig.auth_code && hash.auth_code) {
                launchConfig.auth_code = hash.auth_code;
            }
            launchConfig.actionSource = 'url';
        }

        // Setting the launch options for attachment files
        if (BROWSER_TAB_SUPPORT && getUrlFlag('isAttachmentFile')) {
            launchConfig.isAttachmentFile = true;
            launchConfig.presentationMode = getUrlFlag('presentationMode');

            var newFileDesc = {};
            newFileDesc.folder_id = hash.folder_id;
            newFileDesc.id = hash.id;
            newFileDesc.version = hash.version;
            newFileDesc.source = hash.source;

            launchConfig.file = newFileDesc;
        }

        // extract some options from the URL, but do not unset by unspecified URL flag (DOCS-5053)
        launchConfig.preserveFileName = getUrlFlag('preserveFileName', { default: launchConfig.preserveFileName });
        launchConfig.showDocStoredNotification = getUrlFlag('showDocStoredNotification', { default: launchConfig.showDocStoredNotification });

        // get file descriptor from options
        var fileDesc = launchConfig.file ?? null;
        // workaround for missing source info, if missing it must be drive
        if (fileDesc && !fileDesc.source) { fileDesc.source = 'drive'; }

        // "plugged mode" (OX Viewer) does not start WOPI editor
        const useWopi = DOCUMENTS_COLLABORA && !isPlugged;

        // find running application for the specified document (do not prevent to run two document
        // apps in case of a fast document reload which runs in the background)
        const runningDocApp = fileDesc && !isPlugged && !isReloading && getRunningEditorAppForFile(this.appType, fileDesc, { allowWopi: useWopi });
        if (runningDocApp) {
            launchTracker.step('launchEnd', '$badge{AppFactory} Running application instance found');
            return runningDocApp.coreApp;
        }

        // identifier of a comment thread to be selected after importing the document
        if (hash.comment) {
            launchConfig.comment_id = hash.comment;
        } else if (!launchConfig.comment_id && launchConfig.params && launchConfig.params.comment) {
            launchConfig.comment_id = launchConfig.params.comment; // DOCS-2921
        }

        // track initialization time of launch options
        launchTracker.step('initLaunchOptions', '$badge{AppFactory} Launch options initialized');

        // no running application: create and initialize a new application object
        var coreApp = ui.createApp({
            name: this.moduleName,
            title: fileDesc ? fileDesc.title : '',
            closable: !_.device('smartphone'), // DOCS-5117: "false" for smartphones -> when editors are opened, portals must get "display: none"
            plugged: isPlugged,
            userContentIcon: createIcon(this.appIcon), // pass DOM element to bypass icon creation of Core UI
            help: { base: 'help-documents', target: `ox.documents.user.chap.${this.appType}.html` },
            trackingId: `io.ox/${this.appType}-editor`,
            hideTaskbarEntry: isPlugged,
            startHidden: isReloading,
            prefetchDom: true,
            // no need to load anything, but callback must exist
            load() { return Promise.resolve(true); }
        });

        // attach the common core application attributes (additional `CoreApp` parts)
        coreApp.set("docsAppTypeTag", useWopi ? "wopi" : "docs");
        coreApp.set("docsAppType", this.appType);
        coreApp.set("docsLaunchConfig", launchConfig);
        coreApp.set("docsLaunchTracker", launchTracker);

        // add `onInit` method to be able to immediately wait for async docApp construction
        const appInitDef = jpromise.deferred();
        coreApp.onInit = fn => void appInitDef.done(docApp => docApp.onInit(() => fn.call(coreApp, docApp)));

        // create and initialize the application window
        const appWindow = createAppWindow(coreApp);

        // bug 57537: make sure the Portal window is closed on smartphones when a Documents app is launched
        if (!isPlugged && SMALL_DEVICE) {
            appWindow.on('beforeshow', function () {
                apps.some(function (app) {
                    if (app.getName() === 'io.ox/portal') {
                        var portalWindow = app.getWindow();
                        if (portalWindow) { portalWindow.hide(); }
                        return true;
                    }
                });
            });
        }

        if (DOCUMENTS_COLLABORA && !BROWSER_TAB_SUPPORT) { // #46: no online help for Collabora
            appWindow.on('show', function () { document.documentElement.classList.add('documents-collabora-singletab-editor'); });
            appWindow.on('hide', function () { document.documentElement.classList.remove('documents-collabora-singletab-editor'); });
        }

        // bug 38117: a title must be set to always get the launcher button in the top bar
        setAppTitle(coreApp, fileDesc);

        // remove all one-way URL hash parameters
        locationHash('new', null);
        locationHash('target_filename', null);
        locationHash('target_folder_id', null);
        locationHash('convert', null);
        locationHash('destfilename', null);
        locationHash('destfolderid', null);
        locationHash('templatename', null);
        locationHash('auth_code', null);
        locationHash('crypto_action', null);
        locationHash('decrypt_auth_code, null');
        locationHash('origin', null);
        locationHash('showDocStoredNotification', null);
        locationHash('comment', null);

        // TODO (needs fix in core UI):
        // add mock methods to the core application that are called by the Spreadsheet Viewer
        // (see io.ox/core/viewer/views/types/spreadsheetview.js)
        // or even better, move the spreadsheet-specific code from core UI to some extension point
        if (isPlugged && (this.appType === AppType.SPREADSHEET)) {
            let docApp;
            coreApp.onInit(app => { docApp = app; });
            coreApp.waitForImportSuccess = fn => docApp.waitForImportSuccess(fn.bind(coreApp));
            coreApp.waitForImportFailure = fn => docApp.waitForImportFailure(fn.bind(coreApp));
            coreApp.getView = () => docApp.docView;
            coreApp.getController = () => docApp.docController;
            coreApp.isInQuit = () => docApp.isInQuit();
        }

        // bug 46405: override the launch method of the application instance to be able to fetch
        // the application mix-in class on demand, and to defer invocation of the original launch
        // method until the application has been extended with that mix-in class
        coreApp.setLauncher(() => {

            // performance: immediately start to load the application class module in the background
            const appClassPromise = useWopi ? loadWopiAppClass() : this.loadAppClass();

            // bug 46405: do not return a pending promise from `setLauncher` (otherwise it would
            // be possible to open multiple application instances for the same document)
            jpromise.invokeFloating(async () => {

                // if anything fails during launching, roll back into a defined state
                try {

                    // track time between finishing core launch (outside this callback), and actually invoking this callback
                    launchTracker.step('launchStart', '$badge{AppFactory} Documents application launcher invoked');

                    // the name of the edited file, with and without extension
                    let fullFileName = getSanitizedFileName(fileDesc);

                    // show a busy blocker screen to give the user a quick feedback that the app is starting
                    if (launchConfig.hidden !== true) {
                        appWindow.show();
                        appWindow.busy(null, null, null, { topLabel: _.noI18n(fullFileName) });
                    }

                    // Bug 52041: After a browser refresh, the file name does not exist in the launch
                    // options object. To check whether the file is encrypted, the file descriptor
                    // needs to be fetched from the server.
                    if (fileDesc && !fileDesc.filename && (fileDesc.source === 'drive')) {
                        // eslint-disable-next-line require-atomic-updates
                        launchConfig.file = fileDesc = await getFile(fileDesc);
                        fullFileName = getSanitizedFileName(fileDesc);
                        setAppTitle(coreApp, fileDesc);
                        if (appWindow.isBusy()) {
                            appWindow.idle().busy(null, null, null, { topLabel: _.noI18n(fullFileName) });
                        }
                        launchTracker.step('fetchFileName', '$badge{AppFactory} Missing file name requested from server');
                    }

                    // run all registered extension points
                    await ext.point('io.ox/office/app/launch').cascade(coreApp, new ext.Baton({ launchConfig, fileDesc }));
                    launchTracker.step('extPoints', '$badge{AppFactory} Launcher extensions finished');

                    // add authorization code for encrypted files to launch options
                    if (hasGuardExt(fullFileName) && !launchConfig.auth_code) {
                        // eslint-disable-next-line require-atomic-updates
                        launchConfig.auth_code = await authorizeGuard({ data: fileDesc });
                        launchTracker.step('authGuard', '$badge{AppFactory} Guard authorization finished');
                    }

                    // resolve template file
                    if (launchConfig.templateFile && launchConfig.templateFile.folder_id !== 'template:local') {
                        launchConfig.actionSource = null;
                        // eslint-disable-next-line require-atomic-updates
                        launchConfig.templateFile = await getFile(launchConfig.templateFile);
                        launchTracker.step('getTemplateFile', '$badge{AppFactory} Template file descriptor resolved');
                    }

                    // wait for importing the application class module
                    const ApplicationClass = await appClassPromise;
                    launchTracker.step('requireAppSource', '$badge{AppFactory} JS source code loaded and parsed');

                    // the application may have been closed very quickly before the application module is loaded
                    if (!coreApp.cid) { return; }

                    // construct an instance of the editor application
                    const docApp = new ApplicationClass(coreApp);
                    coreApp.set("docsAppInstance", docApp);
                    appInitDef.resolve(docApp);
                    launchTracker.step('constructDocsApp', '$badge{AppFactory} Docs application instance constructed');

                } catch {

                    // close the application immediately if anything fails during launching
                    handleLaunchError(coreApp, isReloading);
                }
            });
        });

        // track construction time of the core application
        launchTracker.step('constructCoreApp', '$badge{AppFactory} Core application instance constructed');

        // return the pre-initialized core application (waits for calling the launcher callback)
        return coreApp;
    }
}
