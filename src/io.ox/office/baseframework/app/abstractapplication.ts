/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from "$/ox";

import { pick, jpromise } from "@/io.ox/office/tk/algorithms";
import { BaseObject } from "@/io.ox/office/tk/objects";

import { FILTER_MODULE_NAME, sendRequest } from "@/io.ox/office/tk/utils/io";
import { BROWSER_TAB_SUPPORT } from "@/io.ox/office/tk/utils/tabutils";
import { propagateChangeFile } from "@/io.ox/office/tk/utils/driveutils";

import type { AppType } from "@/io.ox/office/baseframework/utils/apputils";
import type { PerformanceTracker } from "@/io.ox/office/baseframework/utils/performancetracker";
import { DEFAULT_FILE_BASE_NAME } from "@/io.ox/office/baseframework/view/baselabels";
import type { FileDescriptor, BaseLaunchConfig, CoreApp } from "@/io.ox/office/baseframework/app/appfactory";

// class AbstractApplication ==================================================

/**
 * Base class with shared properties and methods for the internal editor
 * application classes (class `BaseApplication` and subclasses), and external
 * editor application classes (e.g. class `WopiApplication`).
 *
 * Implements integration into the quit process of the core application wrapped
 * by instances of this class.
 *
 * @template QuitOptionsT
 *  Type of the options parameter for the methods `quit` and `implQuit`.
 *
 * @template EvtMapT
 *  Type mapping for the events emitted by application instances.
 */
export abstract class AbstractApplication<
    QuitOptionsT extends object = Empty,
    EvtMapT extends object = Empty,
> extends BaseObject<EvtMapT> {

    /** The CoreUI application instance wrapped by this app. */
    readonly coreApp: CoreApp;
    /** The type of the wrapped editor application. */
    readonly appType: AppType;
    /** The complete module name of the editor application. */
    readonly moduleName: string;
    /** Shortcut to the launch configuration carried by the core app. */
    readonly launchConfig: BaseLaunchConfig;
    /** Shortcut to the launch tracker carried by the core app. */
    readonly launchTracker: PerformanceTracker;

    /** Pending initialization handlers (for method `onInit`). */
    #initHandlers: Opt<VoidFunction[]> = [];
    /** Promise representing a running call to `quit`. */
    #quitPromise: Opt<Promise<void>>;

    // constructor ------------------------------------------------------------

    constructor(coreApp: CoreApp) {
        super({ _kind: "root" });

        // public properties
        this.coreApp = coreApp;
        this.moduleName = coreApp.getName();
        this.appType = coreApp.get("docsAppType")!;
        this.launchConfig = coreApp.get("docsLaunchConfig")!;
        this.launchTracker = coreApp.get("docsLaunchTracker")!;

        // register quit handler
        coreApp.setQuit(async options => {
            await this.implQuit(options as Opt<QuitOptionsT>);
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Invokes the specified callback function after completing the early
     * construction phase of the BaseApplication.
     *
     * During early construction, some internal properties may not be available
     * yet. All client code relying on a complete application need to use this
     * method to safely wait for construction.
     *
     * If this method is used after early construction, the passed callback
     * function will be invoked immediately (synchronously).
     *
     * @param fn
     *  The callback function to be deferred after early construction phase has
     *  completed.
     */
    onInit(fn: VoidFunction): void {
        if (this.#initHandlers) {
            this.#initHandlers.push(fn);
        } else {
            jpromise.invokeFloating(fn);
        }
    }

    /**
     * Checks if this application is currently in the quit process (it is
     * processing before-quit or quit callback handlers), or already destroyed.
     *
     * @returns
     *  Whether the application is currently in the quit process or destroyed.
     */
    isInQuit(): boolean {
        return this.destroyed || !!this.#quitPromise;
    }

    /**
     * Closes this application instance. Calls the abstract method `implQuit`
     * internally that has to be implemented by subclasses.
     *
     * _Attention:_ After calling this method, this application instance may be
     * destroyed. An external caller MUST NOT do anything else with this
     * instance when the returned promise has been fulfilled.
     *
     * @param options
     *  Optional parameters that will be passed to the internal quit handler.
     *
     * @returns
     *  A promise that will fulfil if the application has actually been closed
     *  and destroyed (the `implQuit` quit handler has not vetoed quitting); or
     *  that will reject if the application continues running.
     */
    async quit(options?: QuitOptionsT): Promise<void> {

        // prevent repeated calls while quit is running
        await (this.#quitPromise ??= new Promise((resolve, reject) => {

            // wait for early construction phase before calling destruction code
            this.onInit(jpromise.wrapFloating(async () => {
                try {
                    // call core `quit` method for external cleanup (this will also invoke `implQuit`)
                    await this.coreApp.quit(false, options);
                    // remove event listeners, cancel running timers, etc.
                    this.destroy();
                    resolve();
                } catch (error) {
                    // rejected: resume to running state of application
                    this.#quitPromise = undefined;
                    reject(error as Error);
                }
            }));
        }));
    }

    // protected methods ------------------------------------------------------

    /**
     * Runs all initialization handlers that have been registered with the
     * method `onInit`. After this method has been called, all initialization
     * handlers registered via `onInit` will be invoked immediately.
     */
    /*protected*/ runInitHandlers(): void {
        for (const fn of this.#initHandlers!) { jpromise.invokeFloating(fn); }
        this.#initHandlers = undefined;
    }

    /**
     * Subclasses implement quitting the application instance. This method will
     * be registered through the core application's `setQuit` method, and will
     * be called from the core application's `quit` method.
     *
     * @param options
     *  Optional parameters passed to the `quit` method call.
     *
     * @returns
     *  A promise that will fulfil if the application has actually been closed
     *  and destroyed; or that will reject if the application continues
     *  running.
     */
    protected abstract implQuit(options?: QuitOptionsT): Promise<void>;

    /**
     * Creates and initializes a new document file, either from scratch, or
     * from an existing template file, according to the launch options of this
     * application.
     *
     * @returns
     *  A promise that will fulfil with a file descriptor object when the
     *  document file has been initialized successfully.
     */
    async createNewDocumentFile(): Promise<FileDescriptor> {

        // application settings and launch configuration
        const appUid = this.coreApp.get("uniqueID");
        const appType = this.coreApp.get("docsAppType");
        const launchConfig = this.coreApp.get("docsLaunchConfig");
        if (!launchConfig) { throw new Error("missing launch configuration"); }

        // parameters for the "create default document" middleware action
        const requestParams: Dict = {
            action: "createdefaultdocument",
            document_type: appType,
            target_filename: launchConfig.target_filename || DEFAULT_FILE_BASE_NAME,
            target_folder_id: launchConfig.target_folder_id || "",
            uid: appUid,
            app: appType,
            initial_language: ox.language.replace(/_/g, "-"), // DOCS-1600: pass UI language to server
        };

        // adapt parameters for the "edit as new" middleware action
        if (launchConfig.templateFile) {
            requestParams.file_id = launchConfig.templateFile.id;
            requestParams.version = launchConfig.templateFile.version;
        }

        // adapt parameters for the "create from template" middleware action
        if (launchConfig.action === "convert") {
            if (!launchConfig.templateFile) { throw new Error("missing 'templateFile' in launch configuration"); }
            requestParams.action = "createfromtemplate";
            requestParams.convert = true;
            requestParams.preserve_filename = launchConfig.preserveFileName;
        }

        // send the middleware request to create the new document
        const data = await sendRequest(FILTER_MODULE_NAME, requestParams);
        const file = pick.dict(data, "file");
        if (!file) { throw new Error("missing file descriptor in response"); }
        const fileDesc = file as unknown as FileDescriptor;

        // in multi-tab environment: propagate new file to the Drive app
        if (BROWSER_TAB_SUPPORT) {
            jpromise.floating(propagateChangeFile(fileDesc));
        }

        return fileDesc;
    }
}
