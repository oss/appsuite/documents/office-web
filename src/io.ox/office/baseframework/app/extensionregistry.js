/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import Capabilities from '$/io.ox/core/capabilities';
import ext from '$/io.ox/core/extensions';

import { is, pick } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getFileExtension, hasGuardExt, replaceFileExtension } from '@/io.ox/office/tk/utils/driveutils';

import { GUARD_AVAILABLE, DOCUMENTS_COLLABORA } from '@/io.ox/office/baseframework/utils/baseconfig';
import { getEditorModulePath } from '@/io.ox/office/baseframework/utils/apputils';

// constants ==============================================================

// the extension suffix for files with an internal error
var INTERNAL_ERROR_SUFFIX = '_ox';

// bug 44262, 48777, 48784, 65899: minimum supported browser versions (especially for Android and macOS)
var DOM_EDIT_MIN_VERSIONS = {
    Chrome: _.browser.Android  ? 74 : _.browser.MacOS ? 75 : 1,
    Firefox: 67,
    IE: 11,
    Safari: 10
};

// `true` => version supported; `false` => version not supported; `null` => unknown browser
var DOM_EDIT_HAS_VERSION = _.reduce(DOM_EDIT_MIN_VERSIONS, function (result, minVersion, browserId) {
    var version = _.browser[browserId];
    return (result !== null) ? result : is.number(version) ? (minVersion <= version) : null;
}, null);

/**
 * Specifies whether text editing in the DOM (especially IME) is supported
 * by the current browser and operating system.
 *
 * - Bug 44262: On Android systems, Chrome 49+ (and no other browser!).
 * - Bug 48777: On macOS systems, Chrome 53+, or other browsers.
 */
export const DOM_TEXT_EDIT_SUPPORT = _.browser.Android ? (is.number(_.browser.Chrome) && DOM_EDIT_HAS_VERSION) : (DOM_EDIT_HAS_VERSION !== false);

/**
 * The message text for an alert box specifying the minimum browser version
 * needed for text editing.
 */
export const DOM_TEXT_EDIT_ALERT = _.device('android') ? gt('Please use the most recent version of Google Chrome for full support.') : _.device('ios') ? gt('Please use the most recent version of Safari for full support.') : gt('Please use the most recent version of either Chrome, Firefox, Internet Explorer or Safari.');

// File extension configuration
//
// Each extension configuration entry supports the following properties:
// - {String} [format]
//      The file format used by the server-side filter component to load
//      and save the contents of the document. If omitted, documents with
//      the current file extension cannot be edited. The following file
//      formats are supported:
//      - 'ooxml': Office Open XML (Microsoft Office)
//      - 'odf' OpenDocument Format (OpenOffice/LibreOffice)
// - {String} [template]
//      The file extension of the template file used to create a new
//      document with the current file extension. If omitted, there is no
//      template file extension available for the current file format (i.e.
//      it is not possible to create a template for the edited document).
// - {Boolean} [macros=false]
//      If set to true, the document may contain scripting macros.
// - {Boolean} [convert=false]
//      If set to true, the server needs to convert the original file to
//      the file format specified with the property 'format'.
// - {boolean} [collabora=false]
//      If set to `true`, the file format can be edited by a remote Collabora
//      editor.
//
var FILE_EXTENSION_CONFIGURATION = {

    text: {
        module: getEditorModulePath('text'),
        requires: 'text',
        editable: DOM_TEXT_EDIT_SUPPORT,
        extensions: {
            docx: { format: 'ooxml', template: 'dotx', collabora: true },
            docm: { format: 'ooxml', template: 'dotm', macros: true, collabora: true },
            doc:  { format: 'ooxml', template: 'dot', macros: true, convert: true, collabora: true },
            odt:  { format: 'odf', template: 'ott',  macros: true, collabora: true },
            rtf:  { format: 'ooxml', convert: true },
        }
    },

    spreadsheet: {
        module: getEditorModulePath('spreadsheet'),
        requires: 'spreadsheet',
        editable: true,
        extensions: {
            xlsx: { format: 'ooxml', template: 'xltx', collabora: true },
            xlsm: { format: 'ooxml', template: 'xltm', macros: true, collabora: true },
            xlsb: { format: 'ooxml', macros: true, convert: true },
            xlam: { macros: true }, // add-in
            xls:  { format: 'ooxml', template: 'xlt', macros: true, convert: true, collabora: true },
            xla:  { macros: true }, // add-in
            ods:  { format: 'odf', template: 'ots', macros: true, collabora: true },
        }
    },

    presentation: {
        module: getEditorModulePath('presentation'),
        requires: 'presentation',
        editable: DOM_TEXT_EDIT_SUPPORT,
        extensions: {
            pptx: { format: 'ooxml', template: 'potx', collabora: true },
            pptm: { format: 'ooxml', template: 'potm', macros: true, collabora: true },
            ppsx: { format: 'ooxml' },
            ppsm: { format: 'ooxml', macros: true },
            ppam: { macros: true }, // add-in
            ppt:  { format: 'ooxml', template: 'pot', macros: true, convert: true, collabora: true },
            pps:  { format: 'ooxml', macros: true, convert: true }, // slide show
            ppa:  { format: 'ooxml', macros: true, convert: true }, // add-in
            odp:  { format: 'odf', template: 'otp', macros: true, collabora: true },
        }
    }
};

// extension to template extension map
var TEMPLATE_EXT_MAP = {};

// all application configurations, mapped by application identifier
var applicationMap = {};

// all configurations, mapped by lower-case extension
var fileExtensionMap = {};

// private global functions ===============================================

function registerFileExtension(extension, settings) {

    if (extension in fileExtensionMap) {
        globalLogger.warn('ExtensionRegistry: extension "' + extension + '" already registered');
    }

    fileExtensionMap[extension] = settings;

    // add extension with internal error suffix (native only)
    if (settings.native) {
        fileExtensionMap[extension + INTERNAL_ERROR_SUFFIX] = settings;
    }
}

// public functions =======================================================

/**
 * Returns whether the passed module name specifies an OX Documents
 * application that can edit documents on the current platform.
 *
 * @param {String} editModule
 *  The module name of the application.
 *
 * @returns {Boolean}
 *  Whether the application is able to edit documents.
 */
export function supportsEditMode(editModule) {
    return pick.boolean(applicationMap[editModule], 'editable', false);
}

/**
 * Returns the configuration settings of the file extension contained by
 * the passed file name.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {Object|Null}
 *  The file extension settings; or null, if the extension is not known.
 */
export function getExtensionSettings(fileName, editModule) {

    // the lower-case extension of the passed file name
    var extension = getFileExtension(fileName);
    // the extension settings object
    var extensionSettings = (extension in fileExtensionMap) ? fileExtensionMap[extension] : null;
    // the module name of the edit application for the extension
    var module = pick.string(extensionSettings, 'module');

    // passed module name must match if specified
    return (editModule && (editModule !== module)) ? null : extensionSettings;
}

/**
 * Returns an array containing all file extensions that can be edited.
 *
 * @returns {Array<String>}
 *  An array containing all file extensions that can be edited.
 */
export function getEditableExtensions() {
    var extensions = [];
    _.forEach(fileExtensionMap, function (settings, extension) {
        if (settings.editable) {
            extensions.push(extension);
        }
    });
    return extensions;
}

/**
 * Returns an array containing all file extensions that can used as a template.
 *
 *  @param {String} appType
 *   The type of the app e.g. 'text' to get the template extensions.
 *
 * @returns {Array<String>}
 *  An array containing all file extensions that can used as a template.
 */
export function getTemplateExtensions(appType) {
    const extensions = [];
    const module = getEditorModulePath(appType);
    _.forEach(fileExtensionMap, function (settings, extension) {
        if (settings.module === module && settings.editable && settings.native && !extension.endsWith(INTERNAL_ERROR_SUFFIX)) {
            extensions.push(extension);
        }
    });
    return extensions;
}

/**
 * Returns the application type identifier of the file with the passed name.
 *
 * @param {string} fileName
 *  The file name (case-insensitive).
 *
 * @returns {AppType|null}
 *  The application type identifier of the file with the passed name; or
 *  `null`, if the file extension is not registered.
 */
export function getAppType(fileName) {
    var extensionSettings = getExtensionSettings(fileName);
    return pick.string(extensionSettings, 'type') || null;
}

/**
 * Returns the module name of the edit application that is able to modify
 * the file with the passed file name.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {String|Null}
 *  The module name of the edit application that is able to modify the file
 *  with the passed file name; or null, if no edit module exists for the
 *  file extension.
 */
export function getEditModule(fileName) {
    var extensionSettings = getExtensionSettings(fileName);
    return pick.string(extensionSettings, 'module') || null;
}

/**
 * Returns whether the file with the passed name is editable by any of the
 * OX Documents edit applications.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {Boolean}
 *  Whether the specified file is editable by one of the OX Documents edit
 *  applications.
 */
export function isEditable(fileName, editModule) {
    if (!GUARD_AVAILABLE && hasGuardExt(fileName)) { return false; }
    var extensionSettings = getExtensionSettings(fileName, editModule);
    return pick.boolean(extensionSettings, 'editable', false);
}

/**
 * Returns whether the file with the passed name is editable natively with
 * any of the OX Documents edit applications, according to the file format.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {Boolean}
 *  Whether the specified file is editable natively with one of the OX
 *  Documents edit applications.
 */
export function isNative(fileName, editModule) {
    if (!isEditable(fileName, editModule)) { return false; }
    var extensionSettings = getExtensionSettings(fileName, editModule);
    return !!extensionSettings && (DOCUMENTS_COLLABORA ? extensionSettings.collabora : extensionSettings.native);
}

/**
 * Returns whether the file with the passed name is editable with any of
 * the OX Documents edit applications by conversion to a natively supported
 * file format, according to the file format.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {Boolean}
 *  Whether the specified file is editable with one of the OX Documents
 *  edit applications by conversion to a native file format.
 */
export function isConvertible(fileName, editModule) {
    if (!isEditable(fileName, editModule)) { return false; }
    var extensionSettings = getExtensionSettings(fileName, editModule);
    return !pick.boolean(extensionSettings, 'native', false);
}

/**
 * Returns the file format identifier of the file with the passed name.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {String}
 *  The file format identifier of the file with the passed name:
 *  - 'ooxml' for a file that will be edited in the OfficeOpenXML file
 *      format (either natively, or by conversion from a file format not
 *      supported natively).
 *  - 'odf' for a file that will be edited in the OpenDocument file format.
 *  - An empty string for all other file names.
 */
export function getFileFormat(fileName, editModule) {
    var extensionSettings = getExtensionSettings(fileName, editModule);
    return pick.string(extensionSettings, 'format', '');
}

/**
 * Returns whether the file with the passed name is a template file.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {Boolean}
 *  Whether the specified file is a document template file.
 */
export function isTemplate(fileName, editModule) {
    var extensionSettings = getExtensionSettings(fileName, editModule);
    return pick.boolean(extensionSettings, 'template', false);
}

/**
 * Provides the template file extension for a given file name.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {String|Null}
 *  The template extension for the file name; or null if the format cannot
 *  be written to a template format. If the passed file name refers to a
 *  template file by itself, its own extension will be returned.
 */
export function getTemplateExtension(fileName) {
    var extension = getFileExtension(fileName);
    return TEMPLATE_EXT_MAP[extension] || null;
}

/**
 * Returns whether the file with the passed name may contain macro scripts.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @param {String} [editModule]
 *  If specified, must match the module name of the edit application
 *  registered for the extension of the passed file name. If omitted, the
 *  edit application module name for the extension will be ignored.
 *
 * @returns {Boolean}
 *  Whether the specified file may contain macro scripts.
 */
export function isScriptable(fileName, editModule) {
    var extensionSettings = getExtensionSettings(fileName, editModule);
    return pick.boolean(extensionSettings, 'macros', false);
}

/**
 * Returns whether the file with the passed name contains the 'internal
 * error' extension suffix.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {Boolean}
 *  Whether the file with the passed name contains the 'internal error'
 *  extension suffix.
 */
export function isError(fileName) {
    var extension = getFileExtension(fileName);
    return extension.endsWith(INTERNAL_ERROR_SUFFIX);
}

/**
 * Extends and returns the passed file name with the 'internal error'
 * extension suffix. If the file name already contains this suffix, it will
 * be returned as is.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {String}
 *  The passed passed file name with the 'internal error' extension suffix.
 */
export function createErrorFileName(fileName) {
    if (isError(fileName)) { return fileName; }
    var fileExt = getFileExtension(fileName);
    return replaceFileExtension(fileName, fileExt + INTERNAL_ERROR_SUFFIX);
}

// static initialization ==================================================

// process the configuration of all modules
_.forEach(FILE_EXTENSION_CONFIGURATION, function (moduleConfiguration, appType) {

    // the capability required to edit the file
    var editable = pick.boolean(moduleConfiguration, 'editable', false);
    // the capability required to edit the file
    var requires = pick.string(moduleConfiguration, 'requires');
    // the module name of the edit application
    var module = pick.string(moduleConfiguration, 'module');
    // whether the edit module is available at all
    var editAvailable = editable && !!requires && Capabilities.has(requires) && !!module;

    // application configuration entry
    applicationMap[appType] = { editable: editAvailable };

    // process all extensions registered for the module
    _.forEach(moduleConfiguration.extensions, function (extensionSettings, extension) {

        // the file format of files with the current extension
        var format = pick.string(extensionSettings, 'format', '');
        // whether the file extension is supported natively
        var native = !pick.boolean(extensionSettings, 'convert', false);
        // the file extension of template files
        var template = pick.string(extensionSettings, 'template');

        // initialize all properties of the current extension
        registerFileExtension(extension, extensionSettings = {
            module,
            type: appType,
            format,
            template: false,
            editable: editAvailable && pick.boolean(extensionSettings, 'editable', true),
            native,
            macros: pick.boolean(extensionSettings, 'macros', false),
            collabora: pick.boolean(extensionSettings, 'collabora', false),
        });

        // register the template extension, store the template file extension for the
        // original extension, AND for the template extension
        if (template) {
            registerFileExtension(template, _.extend({}, extensionSettings, { template: true }));
            TEMPLATE_EXT_MAP[extension] = TEMPLATE_EXT_MAP[template] = template;
        }
    });
});

// extension point to call isNative(). Used by OX Viewer when plugging OX Spreadsheet.
ext.point('io.ox/office/extensionregistry').extend({
    id: 'native',
    index: 10000,
    isNative(baton) {
        return isNative(baton.data.get('filename'));
    }
});
