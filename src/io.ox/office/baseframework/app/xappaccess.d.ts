/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { JQueryEmitter, DOMEventHandlerFn, ResolveDOMEventKeys, JQueryEventHandlerFn } from "@/io.ox/office/tk/events";

import { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";
import { CoreApp } from "@/io.ox/office/baseframework/app/appfactory";

// types ======================================================================

/**
 * A callback function that will be invoked when the application will start or
 * finish; or has already started or finished importing its document.
 *
 * @param immediate
 *  Whether the import was already started/finished while invoking the method
 *  (i.e. this callback has been invoked immediately).
 */
export type AwaitImportFn = (immediate: boolean) => void;

/**
 * A callback function that will be invoked when the application will fail or
 * has failed to import its document.
 *
 * @param immediate
 *  Whether the import was already started/finished while invoking the method
 *  (i.e. this callback has been invoked immediately).
 *
 * @param response
 *  The failure response passed to the rejected import promise.
 */
export type CatchImportFn = (immediate: boolean, response: unknown) => void;

// mixin XAppAccess ===========================================================

export interface XAppAccess<DocAppT extends BaseApplication> {

    readonly docApp: DocAppT;
    readonly coreApp: CoreApp;

    isImportStarted(): boolean;
    isImportSucceeded(): boolean;
    isImportFailed(): boolean;
    isImportFinished(): boolean;

    waitForImportStart(handlerFn: AwaitImportFn, context?: object): void;
    waitForImport(handlerFn: AwaitImportFn, context?: object): void;
    waitForImportSuccess(handlerFn: AwaitImportFn, context?: object): void;
    waitForImportFailure(handlerFn: CatchImportFn, context?: object): void;

    listenToDOMWhenVisible<T extends EventTarget, K extends ResolveDOMEventKeys<T>>(target: T, type: OrArray<K>, handler: DOMEventHandlerFn<this, T, K>): void;
    listenToDOMWhenVisible<K extends keyof JTriggeredEventMap>(emitter: JQueryEmitter, type: OrArray<K>, handler: JQueryEventHandlerFn<this, K>): void;
    listenToWindowResizeWhenVisible(handler: VoidFunction): void;
}
