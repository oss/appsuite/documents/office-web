/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AppWindow, AppAttributeMap, AppEventMap, App } from "$/io.ox/core/desktop";

import { GlobalAppConfig, AppType } from "@/io.ox/office/baseframework/utils/apputils";
import { PerformanceTracker } from "@/io.ox/office/baseframework/utils/performancetracker";

import { AbstractApplication } from "@/io.ox/office/baseframework/app/abstractapplication";
import { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";
import { AbstractLaunchConfig, AbstractAppFactory } from "@/io.ox/office/baseframework/appregistry";

// types ======================================================================

export interface FileDescriptor {
    source: "drive" | "mail" | "task";
    folder_id: string;
    id: string;
    filename: string;
    version?: string;
    attachment?: string;
}

/**
 * Additional options to specify the editor application to be matched.
 */
export interface MatchEditorAppOptions {

    /**
     * The expected type identifier of the application. If omitted, any kind of
     * Documents editor application will be considered.
     */
    appType?: AppType;

    /**
     * Whether to recognize and return WOPI editor applications. By default,
     * only the internal editor applications (`BaseApplication` and subclasses)
     * will be recognized.
     */
    allowWopi?: boolean;
}

/**
 * Application configuration passed to new application instances that determine
 * the actions to perform during application launch.
 */
export interface BaseLaunchConfig extends AbstractLaunchConfig {

    /**
     * Controls how to connect the application to a document file.
     */
    action: "new" | "load" | "convert";

    /**
     * The descriptor of the file to be imported. May be missing for specific
     * actions, e.g. when creating a new file.
     */
    file?: FileDescriptor;

    /**
     * Filename for a new document file (from scratch or from template).
     */
    target_filename?: string;

    /**
     * Identifier of the target folder for a new document file (from scratch or
     * from template).
     */
    target_folder_id?: string;

    /**
     * The descriptor of the file to be used as template for a new file.
     */
    templateFile?: FileDescriptor;

    /**
     * Whether to preserve the filename of the template file (exchanges the
     * file extension only).
     */
    preserveFileName?: boolean;

    /**
     * The authentication code for encrypted documents.
     */
    auth_code?: string;
}

/**
 * Data attributes that can be set at the application instance (Backbone model
 * attributes).
 */
export interface CoreAppAttributeMap extends AppAttributeMap {
    docsAppTypeTag: string;
    docsAppType: AppType;
    docsAppInstance: AbstractApplication;
    docsLaunchConfig: BaseLaunchConfig;
    docsLaunchTracker: PerformanceTracker;
    docsFileDesc?: FileDescriptor;
}

/**
 * Type mapping for the events emitted by `CoreApp` instances.
 */
export interface CoreAppEventMap extends AppEventMap<CoreAppAttributeMap> {

    /**
     * Will be emitted when the "Quit" button of the early launch busy blocker
     * has been clicked.
     */
    "docs:busy:quit": [];
}

/**
 * Root interface of all Documents applications: the "classic" internal editor
 * applications, and WOPI applications plugging an external editor.
 */
export interface CoreApp extends App<CoreAppAttributeMap, CoreAppEventMap> {
    getWindow(): AppWindow;
    onInit(fn: (docApp: BaseApplication) => void): void;
}

/**
 * Type of the application constructor, taking the launch configuration.
 */
export type AppClassType<DocAppT extends AbstractApplication> = new (coreApp: CoreApp) => DocAppT;

/**
 * Shape of an ES6 module exporting an application class. The module is
 * expected to expose the application class as its default export.
 */
export interface ApplicationClassModule<DocAppT extends AbstractApplication> {
    default: AppClassType<DocAppT>;
}

// functions ==================================================================

export function isEditorCoreApp(anyApp: App, options?: MatchEditorAppOptions): anyApp is CoreApp;
export function getCurrentEditorCoreApp(options?: MatchEditorAppOptions): Opt<CoreApp>;

export function getCurrentEditorApp(options?: MatchEditorAppOptions & { allowWopi?: false }): Opt<BaseApplication>;
export function getCurrentEditorApp(options?: MatchEditorAppOptions): Opt<AbstractApplication>;

export function getRunningEditorAppForFile(appType: AppType, fileDesc: FileDescriptor, options?: MatchEditorAppOptions & { allowWopi?: false }): Opt<BaseApplication>;
export function getRunningEditorAppForFile(appType: AppType, fileDesc: FileDescriptor, options?: MatchEditorAppOptions): Opt<AbstractApplication>;

export function launchPortalApp(appType: AppType, launchConfig?: AbstractLaunchConfig): Promise<App>;
export function launchPortalOnQuit(quittingApp: CoreApp): Promise<App>;

export function prefetchDocsApp(appType: AppType): void;
export function launchDocsApp(appType: AppType, launchConfig?: BaseLaunchConfig): Promise<CoreApp>;

export function isStandaloneMode(): boolean;
export function isStandaloneDocsAppInTab(coreApp: CoreApp): boolean;
export function isDocsAppInMultiTabMode(coreApp: CoreApp): boolean;

export function reloadApplication(quittingApp: CoreApp, launchConfig?: BaseLaunchConfig): Promise<void>;
export function setAppReloadDone(reloadingApp: CoreApp): void;

// class BaseAppFactory =======================================================

export abstract class BaseAppFactory<DocAppT extends AbstractApplication> extends AbstractAppFactory<AppClassType<DocAppT>> {

    readonly appType: AppType;

    protected constructor(appConfig: GlobalAppConfig);

    protected implCreateApp(launchConfig?: BaseLaunchConfig): CoreApp;
}
