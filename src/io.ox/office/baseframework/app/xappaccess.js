/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";

// private functions ==========================================================

function mixinAppAccess(BaseClass) {

    return class AppAccessBase extends BaseClass {

        /**
         * @property {DocAppT extends BaseApplication} docApp
         *  The document application containing this instance.
         *
         * @param {DocAppT extends BaseApplication} docApp
         *  The document application containing this instance.
         *
         * @param {unknown[]} baseCtorArgs
         *  All following arguments will be passed to the constructor of the
         *  base class `BaseClass`.
         */
        constructor(docApp, ...baseCtorArgs) {
            super(...baseCtorArgs);

            // public properties
            this.docApp = docApp;
            this.coreApp = docApp.coreApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Returns the application that owns this instance.
         *
         * @deprecated
         *  Use the property `docApp` of this instance.
         *
         * @returns {DocAppT}
         *  The application that owns this instance.
         */
        getApp() {
            return this.docApp;
        }

        /**
         * Return whether importing the document has started, or is already
         * finished.
         *
         * @returns {boolean}
         *  Whether importing the document has started. Will be false before
         *  importing the document; and will be true after import has started,
         *  or even finished.
         */
        isImportStarted() {
            return jpromise.isSettled(this.docApp.importStartPromise);
        }

        /**
         * Return whether importing the document was successful.
         *
         * @returns {boolean}
         *  Whether importing the document has been completed successfully.
         *  Will return `false` before or while importing the document, or
         *  after import has failed; will return `true` after import has
         *  succeeded.
         */
        isImportSucceeded() {
            return jpromise.isFulfilled(this.docApp.importFinishPromise);
        }

        /**
         * Return whether importing the document has failed.
         *
         * @returns {boolean}
         *  Whether importing the document has failed. Will return `false`
         *  before or while importing the document, or after import has
         *  finished successfully; will return `true` after import has failed.
         */
        isImportFailed() {
            return jpromise.isRejected(this.docApp.importFinishPromise);
        }

        /**
         * Return whether importing the document is completed, regardless
         * whether it was successful.
         *
         * @returns {boolean}
         *  Whether importing the document is completed. Will return `false`
         *  before or while importing the document; will return `true` after
         *  import has succeeded or failed.
         */
        isImportFinished() {
            return jpromise.isSettled(this.docApp.importFinishPromise);
        }

        /**
         * Invokes the passed callback function before the application passed
         * to the constructor starts importing the document. If this method is
         * used after importing the document has started already, the callback
         * function will be invoked immediately (unless initialization of the
         * application has failed).
         *
         * @param {AwaitImportFn} handlerFn
         *  The callback function that will be invoked when the application
         *  starts importing the document.
         *
         * @param {object} [context]
         *  The context bound to the invoked callback function.
         */
        waitForImportStart(handlerFn, context) {
            const isStarted = this.isImportStarted();
            this.onFulfilled(this.docApp.importStartPromise, () => handlerFn.call(context || this, isStarted));
        }

        /**
         * Invokes the passed callback function as soon as the application
         * passed to the constructor has finished importing the document
         * _successfully_. If this method is used after importing the document,
         * the callback function will be invoked immediately (unless import has
         * failed).
         *
         * @param {AwaitImportFn} handlerFn
         *  The callback function invoked when the application has finished
         *  importing the document *successfully*.
         *
         * @param {object} [context]
         *  The context bound to the invoked callback function.
         */
        waitForImportSuccess(handlerFn, context) {
            const isFinished = this.isImportFinished();
            this.onFulfilled(this.docApp.importFinishPromise, () => handlerFn.call(context || this, isFinished));
        }

        /**
         * Invokes the passed callback function as soon as the application
         * passed to the constructor has *failed* importing the document. If
         * this method is used after importing the document, the callback
         * function will be invoked immediately (unless import has succeeded).
         *
         * @param {CatchImportFn} handlerFn
         *  The callback function invoked when the application has failed
         *  importing the document.
         *
         * @param {object} [context]
         *  The context bound to the invoked callback function.
         */
        waitForImportFailure(handlerFn, context) {
            const isFinished = this.isImportFinished();
            this.onRejected(this.docApp.importFinishPromise, response => handlerFn.call(context || this, isFinished, response));
        }

        /**
         * Invokes the passed callback function as soon as the application
         * passed to the constructor has finished importing the document,
         * regardless whether the import was successful. If this method is used
         * after importing the document, the callback function will be invoked
         * immediately.
         *
         * @param {AwaitImportFn} handlerFn
         *  The callback function invoked when the application has finished
         *  importing the document.
         *
         * @param {object} [context]
         *  The context bound to the invoked callback function.
         */
        waitForImport(handlerFn, context) {
            const isFinished = this.isImportFinished();
            this.onSettled(this.docApp.importFinishPromise, () => handlerFn.call(context || this, isFinished));
        }

        /**
         * Registers an event handler at a global DOM object (e.g. the window,
         * the document, or the `<body>` element) that listens to the specified
         * event. The event handler will only be active while the application
         * window is visible, and will be deactivated automatically while the
         * application window is hidden.
         *
         * @template {T extends EventTarget}
         * @template {K extends ResolveDOMEventKeys<T> | keyof JTriggeredEventMap}
         *
         * @param {T | JQueryEmitter} emitter
         *  The event target to start listening to. May also be a JQuery object
         *  wrapping such a DOM object, in this case a `JQuery.Event` object
         *  will be passed to the event handler callback.
         *
         * @param {OrArray<K>} type
         *  The type name of the event to start listening to. Can also be an
         *  array of event names.
         *
         * @param {DOMEventHandlerFn<this, T, K> | JQueryEventHandlerFn<this, K>} handler
         *  The event handler function bound to the specified events.
         */
        listenToDOMWhenVisible(emitter, type, handler) {
            this.docApp.onInit(() => {
                const startListening = () => this.listenTo(emitter, type, handler);
                const stopListening = () => this.stopListeningTo(emitter, type, handler);
                const { docView } = this.docApp;
                this.listenTo(docView, "view:show", startListening);
                this.listenTo(docView, "view:hide", stopListening);
                if (docView.isVisible()) { startListening(); }
            });
        }

        /**
         * Registers an event handler at the browser window that listens to
         * "resize" events. The event handler will only be active while the
         * application window is visible, and will be deactivated automatically
         * while the application window is hidden.
         *
         * @param {VoidFunction} handler
         *  The event handler function bound to "resize" events of the browser
         *  window. Will be called when:
         *  - the application is visible, and the browser window triggers a
         *    "resize" event,
         *  - the application window becomes visible,
         *  - immediately on registration if the application window is visible.
         */
        listenToWindowResizeWhenVisible(handler) {
            const invoke = () => handler.call(this); // drop arguments (event object)
            this.listenToDOMWhenVisible(window, "resize", invoke);
            this.docApp.onInit(() => {
                const { docView } = this.docApp;
                this.listenTo(docView, "view:show", invoke);
                if (docView.isVisible()) { invoke(); }
            });
        }
    };
}

// exports ====================================================================

export const XAppAccess = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XAppAccess>}
     *  The extended class with application access.
     */
    mixin: mixinAppAccess
};
