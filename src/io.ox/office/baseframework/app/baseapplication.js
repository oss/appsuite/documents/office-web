/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';
import { locationHash } from '$/url';

import ext from '$/io.ox/core/extensions';
import Download from '$/io.ox/core/download';
import yell from '$/io.ox/core/yell';
import TabApi from '$/io.ox/core/api/tab';
import Registry from '$/io.ox/core/main/registry';
import apps from '$/io.ox/core/api/apps';

import FilesApi from '$/io.ox/files/api';
import MailApi from '$/io.ox/mail/api';

import { fun, ary, pick, jpromise, debug } from '@/io.ox/office/tk/algorithms';

import { insertHiddenNodes } from '@/io.ox/office/tk/dom';
import { getFile, getFileBaseName, getFileExtension, getPath, getSanitizedFileName, getStandardTrashFolderId,
    getWriteState, isDriveFile, isExternalFolder, propagateChangeFile, propagateNewFile,
    removeGuardExt } from '@/io.ox/office/tk/utils/driveutils';
import { BROWSER_TAB_SUPPORT, openNewWindow } from '@/io.ox/office/tk/utils/tabutils';
import { FILTER_MODULE_NAME, sendRequest } from '@/io.ox/office/tk/utils/io';
import { Logger, globalLogger } from '@/io.ox/office/tk/utils/logger';
import { DObject } from '@/io.ox/office/tk/objects';

import { getValue, setValue } from '@/io.ox/office/baseframework/utils/baseconfig';
import { FlushReason, QuitReason } from '@/io.ox/office/baseframework/utils/apputils';
import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import { ERROR_WHILE_LOADING_DOCUMENT } from '@/io.ox/office/baseframework/utils/clienterror';
import { getMessageData as getErrorMessageData } from '@/io.ox/office/baseframework/utils/errormessages';
import { getMessageData } from '@/io.ox/office/baseframework/utils/infomessages';
import { PerformanceTracker } from '@/io.ox/office/baseframework/utils/performancetracker';
import { isStandaloneDocsAppInTab, launchPortalOnQuit, reloadApplication, setAppReloadDone } from '@/io.ox/office/baseframework/app/appfactory';
import { AbstractApplication } from '@/io.ox/office/baseframework/app/abstractapplication';
import { SEND_AS_MAIL_LABEL } from '@/io.ox/office/baseframework/view/baselabels';
import { XAppAccess } from "@/io.ox/office/baseframework/app/xappaccess";

// globals ====================================================================

// set with all running instances of Documents application
const runningAppSet = new Set/*<BaseApplication>*/();

// whether the logout process in office was entered
var officeLogoutEntered = false;

// whether a invalid session was detected for the Appsuite on a global level,
// could be detected by Core http or Office RT
var invalidSessionDetected = false;

// keep the last ox.session to compare it in case of session changes
var previousOxSession = ox.session;

// private functions ==========================================================

/**
 * Launches a mail composer (floating window).
 */
function launchMailCompose(options) {
    return Registry.call("io.ox/mail/compose", "open", options);
}

// class BaseApplication ======================================================

/**
 * A mix-in class that defines common public methods for an application
 * that is based on a document file.
 *
 * @mixes XAppAccess
 *
 * @property {BaseLaunchConfig} launchConfig
 *  The launch configuration passed to the constructor.
 *
 * @property {JPromise<void>} importStartPromise
 *  A promise that is pending as long as the document import has not been
 *  started, and that will fulfil right before importing the document.
 *
 * @property {JPromise<void>} importFinishPromise
 *  A promise that is pending as long as the document will be imported, and
 *  that will fulfil right after successful import, or reject right after
 *  import has failed.
 */
export class BaseApplication extends AbstractApplication {

    /** The window instance of the core application. */
    #appWindow;

    /** Root DOM element of the entire application. */
    #winRootNode;

    /** The full path to the document edited by this application. */
    #filePath = [];

    /** All registered before-quit handlers. */
    #beforeQuitHandlers = [];

    /** All registered quit handlers. */
    #quitHandlers = [];

    /** Map with running server requests, created on demand. */
    #pendingRequests;

    /** The "import started" state (pending: before import, settled: import started). */
    #importStartDef = this.createDeferred();

    /** The "import finished" state (pending: importing, settled: import finished with state). */
    #importFinishDef = this.createDeferred();

    /** Error state of the application. */
    #errorState = new ErrorCode();

    /** Info state of the application (the last info state). */
    #infoState = null;

    /** Whether the application document is stored in an external storage. */
    #isExternalStorage = false;

    /** Whether the app is currently reloading. */
    #isReloading = false;

    /** Registry with document converters, e.g. for sending as attachment. */
    #converterRegistry = new Map/*<string, MailConverterFn>*/();

    // constructor ------------------------------------------------------------

    /**
     * @param {CoreApp} coreApp
     *  The core application instance.
     *
     * @param {BaseMVCFactory} mvcFactory
     *  A global factory singleton that implements creation of a new document
     *  model, a document view, and a document controller (in this order).
     */
    constructor(coreApp, mvcFactory) {
        super(coreApp);

        // public properties
        this.docModel = null;
        this.docView = null;
        this.docController = null;
        this.docApp = this; // `XAppAccess` mixin expects this property

        // private properties
        this.#appWindow = coreApp.getWindow();
        this.#winRootNode = this.#appWindow.nodes.outer;
        this.#isReloading = !!this.launchConfig.reloadedByApp;

        // public promises for import state (will be rejected automatically
        // when closing the application early)
        this.importStartPromise = this.#importStartDef.promise();
        this.importFinishPromise = this.#importFinishDef.promise();

        // The AppSuite core uses the optional application method "hasUnsavedChanges" to
        // decide whether to show a warning before the browser refreshes or closes the page.
        coreApp.hasUnsavedChanges = () => this.hasUnsavedChanges();

        // register application in global set
        runningAppSet.add(this);

        // file descriptor of the document edited by this application
        if (this.launchConfig.file) {
            this.#setFileDescriptor(this.launchConfig.file);
        }

        // disable global message recorder when opening an encrypted document
        if (this.launchConfig.auth_code) { Logger.lockRecorder(); }

        // launch source
        var actionSource = this.launchConfig.actionSource || null;
        // the performance tracker for launch/import
        var launchTracker = this.launchTracker;

        // resolve file descriptor from URL options (before starting to construct other components)
        var filePromise = null;
        if (actionSource === 'url') {
            if (this.launchConfig.template) {
                if (!this.launchConfig.templateFile) {
                    filePromise = sendRequest(FILTER_MODULE_NAME, {
                        action: 'gettemplateandrecentlist',
                        type: this.appType
                    }).done(data => {
                        var templateFile = _.find(data.templates, templ => {
                            return (templ.id === this.launchConfig.templateFile.id) && (templ.folder_id === this.launchConfig.templateFile.folder_id);
                        });
                        this.launchConfig.templateFile = templateFile;
                    });
                } else {
                    filePromise = $.when();
                }
            } else if (BROWSER_TAB_SUPPORT && this.launchConfig.isAttachmentFile) {
                filePromise = $.when();// keeping the file descriptor with the data from the launchConfig
            } else {
                filePromise = getFile(this.launchConfig.file).done(fileDesc => {
                    this.#setFileDescriptor(fileDesc);
                });
            }
        } else {
            filePromise = $.when();
        }

        // Start construction after file descriptor has been resolved or rejected.
        // Must also be done in case of error, to initialize the application window
        // which is required by the following call to app.quit().
        filePromise.always(async () => {
            launchTracker.step('resolveFileDesc', '$badge{BaseApp} File descriptor for document resolved');

            // create the MVC instances in the next tick (first, all application constructors have to complete)
            await Promise.resolve();

            // create the resource manager, fetch all localized JSON resources
            this.resourceManager = mvcFactory.createResourceManager(this);
            await this.resourceManager.loadResources();

            // create the model/view/controller instances
            try {
                const docModel = this.docModel = mvcFactory.createModel(this);
                const docView = this.docView = mvcFactory.createView(this, docModel);
                this.docController = mvcFactory.createController(this, docModel, docView);
            } catch (err) {
                debug.logScriptError(err);
                throw err;
            }

            // run all pending initialization handlers
            this.runInitHandlers();

            launchTracker.step('constructMVC', '$badge{BaseApp} Application MVC instances constructed');
        });

        // early exit if file descriptor cannot be resolved (no yell or auto quit in Viewer mode)
        if (!this.isViewerMode()) {
            filePromise.fail(response => {
                var showMessageForCode = response?.errorForErrorCode;
                var messageText = showMessageForCode ? getErrorMessageData(new ErrorCode(response)).message : pick.string(response, "error", "unknown");
                yell('error', messageText);
                this.quit({ reason: QuitReason.ERROR });
            });
        }

        // show a document store notification for certain use cases
        if (this.launchConfig.showDocStoredNotification) {
            filePromise.done(() => {
                yell('success', getMessageData('INFO_DOC_STORED_IN_DEFAULTFOLDER_AS', { fullFileName: this.getFullFileName() }).message);
            });
        }

        // wait for initialization before showing the application window in order to get
        // the 'open' event of the window at all, it must be shown (also without file).
        this.onInit(() => {

            // whether the application is being launched in visible state (global
            // application launcher has shown the application window already)
            var isActive = this.isActive();
            // the value of the 'spellcheck' attribute of the <body> element
            var spellState = null;

            // disables the global browser spellchecker while a Documents application is active
            function disableGlobalSpelling() {
                spellState = $(document.body).attr('spellcheck') || null;
                $(document.body).attr('spellcheck', false);
            }

            // restores the state of the global browser spellchecker
            function restoreGlobalSpelling() {
                $(document.body).attr('spellcheck', spellState);
            }

            // disable global spell checking while this application is active
            this.listenTo(this.#appWindow, 'show', disableGlobalSpelling);
            this.listenTo(this.#appWindow, 'beforehide', restoreGlobalSpelling);

            if (isActive) { disableGlobalSpelling(); }

            // bug 29711: update URL when applications becomes active
            this.listenTo(this.#appWindow, 'show', this.#updateUrl);
            if (isActive) { this.#updateUrl(); }

            // set initial application title (document file name) early
            this.#updateTitle();

            // DOCS-4342: put zoom factor into DOM for automated tests
            this.listenTo(this.docView.zoomState, "change:state", this.#updateZoomAttrs);
            this.#updateZoomAttrs();

            // wait for all other initialization handlers registered with `onInit()`
            this.setTimeout(jpromise.wrapFloating(async () => {

                // run the asynchronous initialization of subclasses
                await this.implInitializeApp();
                launchTracker.step('appInitialized', '$badge{BaseApp} Application instance initialized');

                // nothing to do, if application is already shutting down
                if (this.isInQuit()) { return; }

                // initialize the GUI, start import process (also when app is reloading in background)
                if (isActive || this.#isReloading) {
                    return this.#initializeGui();
                }

                // do not show the application, if it is being restored in hidden mode (after browser reload)
                launchTracker.pause('$badge{BaseApp} Application is inactive, waiting for activation...');
                this.listenTo(this.#appWindow, 'show', () => {
                    launchTracker.continue('$badge{BaseApp} Application activated, continuing with initialization...');
                    jpromise.floating(this.#initializeGui());
                }, { once: true });
            }), 0);
        });

        // show notification alerts when import fails
        this.waitForImportFailure(() => {

            const errorState = this.#errorState;
            const fileDesc = this.getFileDescriptor();
            const messageData = getErrorMessageData(errorState, { originalFileId: fileDesc?.id, viewerMode: this.isViewerMode() });

            if (messageData && !this.isViewerMode()) {
                this.implExtendMessageData(messageData);
                this.docView.yell(messageData);
            }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a globally unique identifier for this application.
     *
     * @returns {string}
     *  A globally unique identifier for this application that includes the
     *  session ID.
     */
    getGlobalUid() {
        return ox.session + ":" + this.coreApp.get("uniqueID");
    }

    /**
     * Returns the value of the specified launch option.
     *
     * @param {string} name
     *  The key of the launch option to be returned.
     *
     * @returns {unknown}
     *  The value of the specified launch option.
     */
    getLaunchOption(name) {
        return this.launchConfig[name];
    }

    /**
     * Returns the document model instance of this application.
     *
     * @deprecated
     *  Use property `docModel` instead.
     *
     * @returns {DocumentModel}
     *  The document model instance of this application.
     */
    getModel() {
        return this.docModel;
    }

    /**
     * Returns the view instance of this application.
     *
     * @deprecated
     *  Use property `docView` instead.
     *
     * @returns {View}
     *  The view instance of this application.
     */
    getView() {
        return this.docView;
    }

    /**
     * Returns the controller instance of this application.
     *
     * @deprecated
     *  Use property `docController` instead.
     *
     * @returns {Controller}
     *  The controller instance of this application.
     */
    getController() {
        return this.docController;
    }

    /**
     * Returns the specific error state of this application.
     *
     * @returns {ErrorCode}
     */
    getErrorState() {
        return this.#errorState;
    }

    /**
     * Sets the error state of this application.
     *
     * @param {ErrorCode} errorState
     *  The new error state to be set for this application.
     */
    setErrorState(errorState) {
        this.#errorState = errorState;
    }

    /**
     * Sets the info state of this application.
     *
     * @returns {InfoState}
     *  The info state of this application.
     */
    getInfoState() {
        return this.#infoState;
    }

    /**
     * Sets the info state of this application.
     *
     * @param {InfoState} infoState
     *  The new info state to be set for this application.
     */
    setInfoState(infoState) {
        this.#infoState = infoState;
    }

    /**
     * Returns the global user settings for all applications of the same type.
     *
     * @param {string} key
     *  The unique key of the user setting.
     *
     * @param {unknown} defValue
     *  The default value in case the user setting does not exist yet.
     *
     * @returns {unknown}
     *  The value of the global user setting.
     */
    getUserSettingsValue(key, defValue) {
        return getValue(this.appType + '/' + key, defValue);
    }

    /**
     * Changes a global user setting for all applications of the same type.
     *
     * @param {string} key
     *  The unique key of the user setting.
     *
     * @param {unknown} value
     *  The new value of the user setting.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the setting has been sent to the
     *  server.
     */
    setUserSettingsValue(key, value) {
        return setValue(this.appType + '/' + key, value);
    }

    /**
     * Returns whether this application caches any contents that have not been
     * sent successfully to the server.
     *
     * This method is intended to be overwritten on demand by subclasses.
     *
     * @returns {boolean}
     *  Whether the application currently caches unsaved contents.
     */
    hasUnsavedChangesOffice() {
        return false;
    }

    /**
     * Returns whether the application contains pending document changes not
     * yet sent to the server.
     *
     * This method is intended to be overwritten on demand by subclasses.
     *
     * @returns {boolean}
     *  Whether the application contains pending document changes.
     */
    hasUnsavedChanges() {
        return false;
    }

    /**
     * Set a running app reload to done. This will update internal states,
     * reveal the reloaded app visually and finish the destruction of the old
     * reloaded app.
     */
    setAppReloadDone() {
        if (this.#isReloading) {
            this.#isReloading = false;
            setAppReloadDone(this.coreApp);
        }
    }

    /**
     * Checks if this application is currently in reloading state.
     *
     * @returns {boolean}
     *  Whether the application is currently in reloading state.
     */
    isReloading() {
        return this.#isReloading;
    }

    /**
     * Returns whether the application was launched by the OX Viewer.
     *
     * @returns {boolean}
     *  Whether the application was launched by the OX Viewer.
     */
    isViewerMode() {
        return !!this.launchConfig.plugged;
    }

    /**
     * Returns true if this is a Text application.
     *
     * @returns {boolean}
     *  Whether this application is a Text application.
     */
    isTextApp() {
        return this.appType === 'text';
    }

    /**
     * Returns true if this is a Spreadsheet application.
     *
     * @returns {boolean}
     *  Whether this application is a Spreadsheet application.
     */
    isSpreadsheetApp() {
        return this.appType === 'spreadsheet';
    }

    /**
     * Returns true if this is a Presentation application.
     *
     * @returns {boolean}
     *  Whether this application is a Presentation application.
     */
    isPresentationApp() {
        return this.appType === 'presentation';
    }

    // application window object ----------------------------------------------

    /**
     * Returns the window instance of the core application.
     *
     * @returns {AppWindow}
     *  The window instance of the core application.
     */
    getWindow() {
        // Note: This method must exist because it is used in SpreadsheetViewer (Core UI)!
        return this.#appWindow;
    }

    /**
     * Returns the window instance of the core application.
     *
     * @returns {AppWindow}
     *  The window instance of the core application.
     */
    getWindowNode() {
        return this.coreApp.getWindowNode();
    }

    /**
     * Returns whether this application is currently active in OX AppSuite.
     *
     * @returns {boolean}
     *  Whether this application is currently active in OX AppSuite.
     */
    isActive() {
        return !!this.#appWindow.state.visible;
    }

    /**
     * Returns the root DOM node of the entire application window, in
     * difference to the function `App::getWindowNode` which returns the body
     * node of the application window (a descendant element of the window root
     * node).
     *
     * @returns {JQuery}
     *  The root DOM node of the application window, as JQuery collection.
     */
    getRootNode() {
        return this.#winRootNode;
    }

    /**
     * Changes the value of a DOM element attribute of the root node of this
     * application.
     *
     * @param {string} name
     *  The name of the attribute to be changed.
     *
     * @param {string|number|boolean} value
     *  The attribute value.
     */
    setRootAttribute(name, value) {
        this.#winRootNode.attr(name, value);
    }

    // file descriptor --------------------------------------------------------

    /**
     * Returns whether this application contains a valid file descriptor.
     *
     * @returns {boolean}
     *  Whether this application contains a valid file descriptor.
     */
    hasFileDescriptor() {
        return !!this.getFileDescriptor();
    }

    /**
     * Returns a clone of the file descriptor of the document edited by this
     * application.
     *
     * @returns {Readonly<FileDescriptor>}
     *  A clone of the current file descriptor.
     */
    getFileDescriptor() {
        return this.coreApp.get("docsFileDesc");
    }

    /**
     * Updates the current file descriptor of the document edited by this
     * application.
     *
     * @param {Partial<FileDescriptor>} fileProps
     *  All file descriptor properties to be updated.
     */
    updateFileDescriptor(fileProps) {
        const fileDesc = this.getFileDescriptor();
        if (fileDesc) {
            // create new object to let application triggger "change:docsFileDesc" event
            this.#setFileDescriptor({ ...fileDesc, ...fileProps });
            propagateChangeFile(this.getFileDescriptor());
            this.#updateTitle();
            this.#updateUrl();
            this.#setExternalStorageFlag();
            this.docController.update();
        }
    }

    /**
     * Returns an object with attributes describing the file currently opened
     * by this application.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  @param {boolean} [options.encodeUrl=false]
     *      If set to `true`, special characters not allowed in URLs will be
     *      encoded.
     *  @param {boolean} [options.currentVersion=false]
     *      If set to `true`, the version stored in the file descriptor will
     *      NOT be inserted into the result (thus, the server will always
     *      access the current version of the document).
     *
     * @returns {object|null}
     *  An object with file attributes, if existing; otherwise `null`.
     */
    getFileParameters(options) {

        const fileDesc = this.getFileDescriptor();
        if (!fileDesc) { return null; }

        // function to encode a string to be URI conforming if specified
        const encodeString = options?.encodeUrl ? encodeURIComponent : _.identity;
        // the resulting file parameters
        const parameters = {};

        // add the parameters to the result object, if they exist in the file descriptor
        for (const name of ['id', 'folder_id', 'filename', 'version', 'source', 'attached', 'module', 'com.openexchange.realtime.resourceID']) {
            const value = fileDesc[name];
            if (_.isString(value)) {
                parameters[name] = encodeString(value);
            } else if (_.isNumber(value)) {
                parameters[name] = value;
            }
        }

        // remove the version identifier, if specified
        if (options?.currentVersion) {
            delete parameters.version;
        }

        return parameters;
    }

    /**
     * Returns the full file name of the current file, with file extension.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  @param {boolean} [options.path=false]
     *      If set to `true`, the full file path will be returned (for example
     *      "drive/myfiles/test.docx" instead of "test.docx").
     *
     * @returns {string|null}
     *  The file name of the current file descriptor; or `null`, if no file
     *  descriptor exists.
     */
    getFullFileName(options) {

        const fileDesc = this.getFileDescriptor();
        var fileName = (fileDesc && getSanitizedFileName(fileDesc)) || null;
        if (fileName && options?.path) {
            var filePath = this.getFilePath();
            if (filePath.length > 0) {
                fileName = filePath.join('/') + '/' + fileName;
            }
        }
        return fileName;
    }

    /**
     * Returns the short file name of the current file, without file extension.
     *
     * @returns {string|null}
     *  The short file name of the current file descriptor; or `null`, if no
     *  file descriptor exists.
     */
    getShortFileName() {
        var fileName = this.getFullFileName();
        return fileName ? getFileBaseName(fileName) : null;
    }

    /**
     * Returns the file extension of the current file, without leading period.
     *
     * @returns {string|null}
     *  The file extension of the current file descriptor; or `null`, if no
     *  file descriptor exists.
     */
    getFileExtension() {
        var fileName = this.getFullFileName();
        return fileName ? getFileExtension(fileName) : null;
    }

    /**
     * Returns the full path to the document edited by this application.
     *
     * @returns {string[]}
     *  The current file path, as array of strings with the titles of all path
     *  components.
     */
    getFilePath() {
        return this.#filePath.map(item => item.title);
    }

    /**
     * Returns whether document edited by this application instance is
     * encrypted.
     *
     * @returns {boolean}
     *  Whether document edited by this application instance is encrypted.
     */
    isDocumentEncrypted() {
        return !!this.launchConfig.auth_code;
    }

    /**
     * Check if the file is stored in a external storage.
     */
    isExternalStorage() {
        return this.#isExternalStorage;
    }

    // server requests --------------------------------------------------------

    /**
     * Sends a request to the server and returns a promise waiting for the
     * response. The unique identifier of the application will be added to the
     * request parameters automatically. See global function `sendRequest` for
     * further details.
     *
     * @param {string} module
     *  The name of the server module that will receive the request.
     *
     * @param {Dict} [params]
     *  Parameters that will be inserted into the request URL (method GET), or
     *  into the request body (method POST).
     *
     * @param {SendRequestOptions} [options]
     *  Optional parameters.
     *
     * @returns {ResponsePromise}
     *  The abortable promise representing the server request. See global
     *  function `sendRequest` for details.
     */
    sendRequest(module, params, options) {

        // add the required application UID and document type to the request parameters
        params = { ...params, uid: this.coreApp.get("uniqueID"), app: this.appType };

        // send the request (returns an abortable promise)
        const request = sendRequest(module, params, options);

        // store the request internally for automatic abort on destruction
        const requests = this.#pendingRequests ??= new Set/*<ResponsePromise>*/();
        requests.add(request);

        // remove the request from the storage after it is finished
        this.onSettled(request, () => this.#pendingRequests.delete(request));

        return request;
    }

    /**
     * Sends a request to the server and returns a promise waiting for the
     * response. This method automatically adds the unique identifier of the
     * application, and the parameters of the file currently opened. See global
     * function `sendRequest` for further details.
     *
     * @param {string} module
     *  The name of the server module that will receive the request.
     *
     * @param {Dict} [params]
     *  Parameters that will be inserted into the request URL (method GET), or
     *  into the request body (method POST).
     *
     * @param {SendRequestOptions} [options]
     *  Optional parameters.
     *
     * @returns {ResponsePromise}
     *  The abortable promise representing the server request. Will be rejected
     *  immediately, if the application is not connected to a document file.
     */
    sendFileRequest(module, params, options) {

        // reject immediately if no file is present
        if (!this.hasFileDescriptor()) {
            const promise = jpromise.reject();
            promise.abort = () => undefined;
            return promise;
        }

        // extend parameters with file settings
        params = { ...params, ...this.getFileParameters() };

        // send the request
        return this.sendRequest(module, params, options);
    }

    /**
     * Sends a request to the document filter server module to determine if
     * certain server features are available.
     *
     * @param {string|string[]} features
     *  The names of all required server features. Can be a simple string for a
     *  single server feature, or an array of strings for multiple server
     *  features.
     *
     * @returns {JPromise}
     *  A promise that will fulfil if ALL passed server features are available;
     *  or that will reject if at least one server feature is missing. The
     *  rejected promise will provide an error object with the additional
     *  property `missing` being an array containing all missing feature names.
     */
    requireServerFeatures(features) {

        // send the feature request
        const request = this.sendRequest(FILTER_MODULE_NAME, { action: "getfeatures" });

        // evaluate the server response
        return request.then(data => {

            // convert parameter to an array
            const requiredFeatures = ary.wrap(features);
            // get the features from the result
            const availableFeatures = pick.string(data, "features", "").split(",");
            // reduce the feature names passed to this method, result contains missing features
            const missingFeatures = _.difference(requiredFeatures, availableFeatures);

            if (missingFeatures.length > 0) {
                const error = new Error("missig server features");
                error.missing = missingFeatures;
                throw error;
            }
        });
    }

    /**
     * Creates and returns the URL of a server request.
     *
     * @param {string} module
     *  The name of the server module.
     *
     * @param {object} [params]
     *  Additional parameters inserted into the URL.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  @param {boolean} [options.currentVersion=false]
     *      If set to `true`, the version stored in the file descriptor will
     *      NOT be inserted into the generated URL (thus, the server will
     *      always access the current version of the document).
     *  @param {boolean} [options.uid=true]
     *      If set to `false`, the UID to prevent the browser from caching will
     *      not be set. Therefore the browser can cache and reuse cached data.
     *      This SHOULD NOT be set to `false` if a request must retrieve latest
     *      data (e.g. download/print etc.)
     *
     * @returns {string|null}
     *  The URL of the server request; or `null`, if the application is not
     *  connected to a document file, or the current session is invalid.
     */
    getServerModuleUrl(module, params, options) {

        // return nothing if no file is present
        if (!this.hasFileDescriptor()) { return null; }

        // the parameters for the file currently loaded
        var fileParams = this.getFileParameters({ ...options, encodeUrl: true });

        if (this.launchConfig.auth_code && fileParams.filename && options?.removeGuardExt) {
            fileParams.filename = removeGuardExt(fileParams.filename);
        }

        // add default file parameters
        params = { ...fileParams, ...params };

        // add application UID to prevent browser caching problems
        if (!params.uid && (options?.uid ?? true)) {
            params.uid = this.coreApp.get('uniqueID');
        }

        // build and return the resulting URL
        return ox.apiRoot + '/' + module + '?' + _.map(params, (value, name) => name + '=' + value).join('&');
    }

    /**
     * Reads the specified file and returns a promise that will be resolved or
     * rejected depending on the result of the read operation. The file will be
     * converted to a data URL containing the file contents as Base-64 encoded
     * data and passed to the resolved promise.
     *
     * @param {FileDescriptor} fileDesc
     *  The descriptor of the file to be loaded.
     *
     * @returns {JPromise}
     *  A promise that will be resolved with the result object containing the
     *  data URL, or rejected if the read operation failed. If the file size is
     *  known, the promise will be notified about the progress of the operation
     *  (a floating-point number between 0.0 and 1.0). Contains the additional
     *  method 'abort()' that allows to abort the running file reader which
     *  rejects the promise. Calling this method has no effect, if importing
     *  the file is finished already.
     */
    readClientFile(fileDesc) {

        // resulting deferred object
        var deferred = this.createDeferred();
        // create a browser file reader instance
        var reader = window.FileReader ? new window.FileReader() : null;

        if (reader) {

            // register the load event handler, deferred object will be resolved with data URL
            reader.onload = function (event) {
                if (event && event.target && _.isString(event.target.result)) {
                    deferred.resolve(event.target.result);
                } else {
                    deferred.reject();
                }
            };

            // register error event handlers, deferred object will be rejected
            reader.onerror = function () {
                deferred.reject();
            };

            // register abort event handlers, deferred object will be rejected
            reader.onabort = function () {
                deferred.reject('abort');
            };

            // register progress handler, deferred object will be notified
            reader.onprogress = function (event) {
                if (event.lengthComputable) {
                    deferred.notify(event.loaded / event.total);
                }
            };

            // forget reference to the file reader after import
            deferred.always(() => { reader = null; });

            // read the file and generate a data URL
            reader.readAsDataURL(fileDesc);

        } else {
            // file reader not supported
            deferred.reject();
        }

        // add an abort() method, forward invocation to AJAX request
        return this.createAbortablePromise(deferred, () => reader?.abort());
    }

    /**
     * Creates an `<img>` element, sets the passed URL, and returns a promise
     * waiting that the image has been loaded. If the browser does not trigger
     * the appropriate events after the specified timeout, the promise will be
     * rejected automatically. If the application has been shut down before the
     * image has been loaded, the pending promise will be rejected
     * automatically.
     *
     * @param {string} url
     *  The image URL.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  @param {number} [options.timeout=10000]
     *      The time before the promise will be rejected without response from
     *      the image node.
     *
     * @returns {JPromise<JQuery<HTMLImageElement>>}
     *  A promise that will fulfil with the image element (as JQuery object),
     *  when the 'load' event has been received from the image node; or reject,
     *  if the 'error' event has been received from the image, or after the
     *  specified timeout delay without response.
     */
    createImageNode(url, options) {

        // the resulting deferred object
        var deferred = $.Deferred();
        // the image node
        var imgNode = $('<img>');
        // the duration of the failure timeout
        var timeout = options?.timeout ?? 10000;
        // the resulting promise
        var promise = this.createAbortablePromise(deferred, _.noop, timeout);
        // the event handlers for the image node
        var handlers = {
            load() { deferred.resolve(imgNode); },
            error() { deferred.reject(); }
        };

        // wait that the image is loaded
        imgNode.one(handlers).attr('src', url);
        return promise.always(() => imgNode.off(handlers));
    }

    /**
     * Safely destroys the passed image nodes.
     *
     * On platforms with restricted memory (especially iPad and iPhone),
     * allocating too many images may result in an immediate browser crash.
     * Simply removing image nodes from the DOM does not free the image
     * resource data allocated by the browser.
     *
     * The suggested workaround is to replace the 'src' attribute of the image
     * node with a local small image which will result in releasing the
     * original image data by the internal resource manager. To be really sure
     * that the garbage collector does not destroy the DOM node instance before
     * the resource manager releases the image data, a reference to the node
     * will be kept in memory for additional 60 seconds.
     *
     * @param {NodeSource} nodes
     *  The image elements, or any other container elements with descendant
     *  image nodes, either as single DOM node, or as jQuery collection with
     *  one or multiple elements.
     */
    destroyImageNodes(nodes) {

        // filter passed image nodes, find descendant image nodes
        var imgNodes = $(nodes).filter('img').add($(nodes).find('img'));
        if (imgNodes.length === 0) { return; }

        // move image into temporary container, release original image data
        insertHiddenNodes(imgNodes);
        $(imgNodes).off().attr('src', 'data:image/gif;base64,R0lGODdhAgACAIAAAAAAAP///ywAAAAAAgACAAACAoRRADs=');
        window.setTimeout(() => { imgNodes.remove(); imgNodes = null; }, 60000);
    }

    // application setup ------------------------------------------------------

    /**
     * Registers a quit handler function that will be executed before the
     * application will be closed. The callback handler function may veto the
     * quit request to keep the application alive.
     *
     * @param {Function} beforeQuitHandler
     *  A function that will be called before the application will be closed.
     *  Will be called in the context of this application instance. Receives
     *  the reason for quitting that has been passed to the method `quit` as
     *  first parameter. May return a promise which must be settled by the quit
     *  handler function. If the promise will be rejected, the application
     *  remains alive.
     */
    registerBeforeQuitHandler(beforeQuitHandler) {
        this.#beforeQuitHandlers.push(beforeQuitHandler);
    }

    /**
     * Registers a quit handler function that will be executed before the
     * application will be destroyed. This may happen if the application has
     * been closed normally, if the user logs out from the entire AppSuite, or
     * before the browser window or browser tab will be closed or refreshed. If
     * the quit handlers return promises. Closing the application, and logging
     * out will be deferred until the promises have been resolved or rejected.
     *
     * @param {Function} quitHandler
     *  A function that will be called before the application will be closed.
     *  Receives the reason for quitting that has been passed to the method
     *  `quit` as first parameter. Will be called in the context of this
     *  application instance. May return a promise which must be settled by the
     *  quit handler function.
     */
    registerQuitHandler(quitHandler) {
        this.#quitHandlers.push(quitHandler);
    }

    /**
     * Executes all registered quit handlers and returns a promise that will be
     * resolved or rejected according to the results of the handlers.
     *
     * ATTENTION: Not for external use. Only used in the implementation of the
     * extension point "io.ox/core/logout".
     *
     * @returns {JPromise}
     *  A promise that will settle when all quit handlers have been executed.
     */
    executeQuitHandlers(reason) {

        // Bug 32989: Create a local copy of the quit handler array, and clear
        // the original array, before executing the quit handlers.
        return this.#quitHandlers ? this.#callHandlers(this.#quitHandlers.splice(0), [reason], { ignoreErrors: true }) : $.when(reason);
    }

    // application runtime ----------------------------------------------------

    /**
     * Flushes the document contents currently opened by this application on
     * the server. Triggers a "docs:flush:success" or "docs:flush:error" event
     * afterwards according to the result.
     *
     * @param {FlushReason} reason
     *  The origin of the flush request.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the document contents have been flushed
     *  successfully, or rejected otherwise.
     */
    flushDocument(reason) {

        // invoke the subclass implementation
        const promise = jpromise.invoke(() => this.implFlushDocument(reason));

        // notify listeners
        promise.done(() => this.trigger('docs:flush:success'));
        promise.fail(err => this.trigger('docs:flush:error', err));

        return promise;
    }

    /**
     * Downloads the document file currently opened by this application, and
     * shows an appropriate error alert on failure.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the download action of the browser has
     *  been triggered for the current document.
     */
    download() {

        // Block application window while waiting for the file URL. Workaround for
        // unreproducable bug 48895: Force to leave the busy blocker after 15 seconds
        // in case of endless pending promise.
        this.docView.enterBusy({ immediate: true, timeout: 15000 });

        // nothing to do, if no file descriptor is available (internal error, server
        // error, etc.); document import must have been finished successfully
        const fileDesc = this.getFileDescriptor();
        var imported = fileDesc && this.isImportSucceeded();
        var promise = imported ? jpromise.resolve() : jpromise.reject({ cause: 'importfailed' });

        // first, flush unsaved document contents to the server
        promise = promise.then(() => this.flushDocument(FlushReason.DOWNLOAD));

        // propagate changed file to the files API
        promise = promise.then(() => {
            // bug 30246: not for mail/task attachments!
            return isDriveFile(fileDesc) ? propagateChangeFile(fileDesc) : null;
        });

        // download the file
        promise = promise.then(() => {
            // download the resulting file (bug 51508: without calling document converter)
            var fileParams = this.getFileParameters({ currentVersion: true });
            if (this.launchConfig.auth_code) {
                fileParams.params = { cryptoAction: 'Decrypt', cryptoAuth: this.launchConfig.auth_code, session: ox.session };
            }
            // on ios the download button shall not display "Show" next to "Download" (DOCS-5328)
            fileParams.onlyDownload = !!_.device('ios');
            // opens new browser tab, or browser download dialog, or ...
            Download.file(fileParams);
        });

        // leave busy mode after download
        promise.always(() => this.docView.leaveBusy());

        // show error message on failure (nothing to do for user cancel, reported as string "cancel")
        promise.fail(response => {
            var alertConfig = fun.do(() => {
                if (!_.isObject(response)) { return null; }
                switch (response.cause) {
                    case 'importfailed': return { message: gt('The document was not loaded successfully. Download is not possible.') };
                    case 'popupblocker': return { message: gt('The browser has blocked a pop-up window. Please enable pop-ups for download.'), type: 'warning' };
                    default:             return { message: gt('The server does not answer the request.') };
                }
            });
            if (alertConfig) {
                this.docView.yell({ type: 'error', headline: gt('Download Error'), ...alertConfig });
            }
        });

        return promise;
    }

    /**
     * Prints the document file currently opened by this application by
     * requesting a PDF conversion from the document converter service.
     */
    print() {

        const fileDesc = this.getFileDescriptor();
        if (!fileDesc) { return; }

        // parameters inserted into the URL
        var params = {
            action: 'getdocument',
            documentformat: 'pdf',
            mimetype: encodeURIComponent(fileDesc.file_mimetype || ""),
            app: this.appType,
            nocache: _.uniqueId(), // needed to trick the browser cache (is not evaluated by the backend)
            ...this.implGetPrintParams(),
        };

        // add Guard settings to to able to access an encrypted file
        if (this.launchConfig.auth_code) {
            params.cryptoAction = 'Decrypt';
            params.cryptoAuth = encodeURIComponent(this.launchConfig.auth_code);
            params.source = 'guard';
            params.priority = 'instant';
        }

        var url = this.getServerModuleUrl(FILTER_MODULE_NAME, params);
        openNewWindow(url);
    }

    /**
     * Helper that indicates whether an application is capable of translating
     * and converting the currently worked document into another format.
     *
     * @param {string} type
     *  The type that is associated with its converter function.
     *
     * @returns {boolean}
     */
    canConvertTo(type) {
        return this.#converterRegistry.has(type);
    }

    /**
     * Default guard method that decides whether an application is able of
     * sending its document as mail.
     *
     * Other applications that do feature additional send-mail functionality
     * have to provide theirs own implemantation of `canSendMail`, overwriting
     * the base application's one.
     *
     * @returns {boolean}
     *  Whether an application is able of sending its document as mail.
     */
    canSendMail() {
        return false;
    }

    /**
     * Creates a new mail message either with this current document's content
     * being the content of the new mail's body, or with the current document
     * attached as file.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.attachMode="inlineHtml"]
     *    One of "attachment" (default), "pdfAttachment", or "inlineHtml".
     *
     * @returns {Promise<void>}
     *  A promise that will be resolved if the mail application has been
     *  launched successfully, or rejected otherwise.
     */
    async sendMail(options) {

        // flush document contents
        try {
            await this.flushDocument(FlushReason.EMAIL);
        } catch {
            this.#handleMailCompositionFailure();
            return;
        }

        let promise;
        // launch the mail composer with correct contents or attachment
        switch (options?.attachMode) {
            case "inlineHtml":
                promise = this.#handleMailCompositionForInlineHTML();
                break;
            case "pdfAttachment":
                promise = this.#handleMailCompositionForPDFAttachment();
                break;
            default:
                promise = this.#handleMailCompositionForFileAttachment();
        }

        promise.fail(error => {
            error = ErrorCode.getErrorCode(error);
            // show an alert banner
            this.docView.yell(getErrorMessageData(error));
        });

        await promise;
    }

    /**
     * Initiate a relogin process in Core.
     */
    requestRelogin() {

        // only one global relogin request by office at a time
        if (invalidSessionDetected) { return; }
        // set state directly, and not only later, delayed when receiving a 'relogin:required' by http.js
        invalidSessionDetected = true;

        // two possibilities to request a new session via core:
        // 1) use a failing http request to trigger Core relogin process, this way we have a very loose
        // coupling with Core, which makes it more robust against changes in Core
        // 2) create a public function in http.js, move trigger 'relogin:required' and setting the
        //  "ox.session to  '' to this function an call it here, this would be syncrounous and a bit
        //   faster than the http request, but would need some effort
        this.requireServerFeatures();
    }

    /**
     * Whether the AI service is available and can be used.
     *
     * @returns {Boolean}
     *   Returns true, when the AI service is available and can be used.
     */
    canUseAI() {
        return false; // DOCS-5293
        // return SWITCHBOARD_AVAILABLE && OPENAI_AVAILABLE && hasFeature('pe') && switchboardSettings.get('host');
    }

    // protected methods ------------------------------------------------------

    /**
     * Core implementation for closing this application.
     *
     * @param {BaseApplicationQuitOptions} [options]
     *  Optional parameters passed to the `app.quit()` call.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil when the application has been closed
     *  completely; or reject when the application shall continue running.
     */
    /*protected override*/ async implQuit(options) {

        // the reason code for quitting the application
        const quitReason = options?.reason ?? QuitReason.USER;
        // performance tracker for quitting the application (constructor takes current timestamp!)
        const quitTracker = new PerformanceTracker('QUIT', this.appType, 'quit_application', '$badge{BaseApp} Quitting application instance (reason="' + quitReason + '")...');

        // when the app is quitting while reloading, the reload must
        // be set to done to continue quitting of the waiting reloaded app
        this.setAppReloadDone();

        // call all before-quit handlers (rejecting one will resume application)
        await this.#callHandlers(this.#beforeQuitHandlers, [quitReason]);
        quitTracker.step('executeBeforeQuitHandlers', '$badge{BaseApp} All "beforeQuit" callback handlers executed');

        // abort all running server requests, before invoking the quit handlers
        this.#pendingRequests?.forEach(request => request.abort());

        // notify all listeners
        this.trigger('docs:beforequit', quitReason);
        quitTracker.step('triggerDocsBeforeQuit', '$badge{BaseApp} Event "docs:beforequit" triggered');

        // run all registered quit handlers
        // Bug 32989: Create a local copy of the quit handler array, and clear the
        // original array, before executing the quit handlers. This allows a quit
        // handler to register other special quit handlers that will be executed e.g.
        // when calling app.executeQuitHandlers() from logout while the quit handlers are
        // still running.
        await this.#callHandlers(this.#quitHandlers.splice(0), [quitReason], { ignoreErrors: true });
        quitTracker.step('executeQuitHandlers', '$badge{BaseApp} All "quit" callback handlers executed');

        // Determine whether a specific app should be launched.
        // - This must be done after all quit handlers are processed (Bug 65566).
        // - This must be done before the app has finished quitting, otherwise a default app is launched by core.
        if (!this.isViewerMode()) {

            // a reloading app should stay open until the new app has been finished to exchange seamlessly
            if (quitReason === QuitReason.RELOAD) {
                await reloadApplication(this.coreApp, options?.userData);
                quitTracker.step('reloadApplication', '$badge{BaseApp} Application reload initiated');
            } else {
                // there is no other app in the docs tab possible: leave promise in pending state to stay in the app
                if (isStandaloneDocsAppInTab(this.coreApp)) { return; }
                // with multi-tab support the user should see the portal
                // normal behavior: just close app, last open or default app will be visible
                if (BROWSER_TAB_SUPPORT) {
                    await launchPortalOnQuit(this.coreApp);
                    quitTracker.step('launchPortal', '$badge{BaseApp} Portal application launched');
                }
            }
        }

        // destroy MVC instances
        this.trigger('docs:destroying');
        quitTracker.step('triggerDocsDestroying', '$badge{BaseApp} Event "docs:destroying" triggered');
        this.docController?.destroy();
        quitTracker.step('destroyController', '$badge{BaseApp} Document controller destroyed');
        this.docView?.destroy();
        quitTracker.step('destroyView', '$badge{BaseApp} Document view destroyed');
        this.docModel?.destroy();
        quitTracker.step('destroyModel', '$badge{BaseApp} Document model destroyed');
        this.resourceManager?.destroy();
        quitTracker.step('destroyResMan', '$badge{BaseApp} Document resource manager destroyed');

        // remove application from global set
        runningAppSet.delete(this);

        // enable global message recorder when closing an encrypted document
        if (this.isDocumentEncrypted()) { Logger.unlockRecorder(); }

        // always clean custom URL hash params on quit
        locationHash('standalone', null);
        locationHash('closetimer', null);
    }

    /**
     * Registers a converter callback function for the document contents.
     *
     * @param {string} type
     *  The type identifier of the converter function.
     *
     * @param {() => JPromise<object>} converterFn
     *  The converter callback function.
     */
    /*protected*/ registerConverter(type, converterFn) {
        this.#converterRegistry.set(type, converterFn);
    }

    /**
     * Will be invoked immediately after constructing the application and its
     * model, view, and controller instances.
     *
     * @returns {MaybeAsync}
     *  May return a promise, if asynchronous initialization will be performed.
     */
    /*protected*/ implInitializeApp() {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked before importing the document, used to initialize the
     * file descriptor. Will be called once after launching the application.
     *
     * @returns {MaybeAsync<FileDescriptor>}
     *  A promise that will fulfil with the new file descriptor, or will reject
     *  on any error.
     */
    /*abstract protected*/ implInitializeFile() {
        // intended to be overridden by subclasses
        throw new Error("missing implementation of abstract method BaseApplication::implInitializeFile");
    }

    /**
     * Will be imvoked to import the document described in the file descriptor
     * of this application. Will be called once after launching the
     * application.
     *
     * @returns {MaybeAsync}
     *  May return a promise, if asynchronous loading will be performed.
     */
    /*abstract protected*/ implImportDocument() {
        // intended to be overridden by subclasses
        throw new Error("missing implementation of abstract method BaseApplication::implImportDocument");
    }

    /**
     * Will be invoked before the document will be accessed at its source
     * location, e.g. before downloading it, printing it, or sending it as
     * email.
     *
     * @param {FlushReason} _reason
     *  The origin of the flush request.
     *
     * @returns {MaybeAsync}
     *  May return a promise, if asynchronous operations will be performed.
     */
    /*protected*/ implFlushDocument(_reason) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked before the message data is used to display a message.
     * This enables different applications to extend the message data in their
     * needed way.
     */
    /*protected*/ implExtendMessageData(_message) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be called before sending a request to the printing service.
     *
     * @returns {Opt<object>}
     *  Additional data like the realtime id and the application type.
     */
    /*protected*/ implGetPrintParams() {
        // intended to be overridden by subclasses
        return undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the original file descriptor of the document edited by this
     * application.
     *
     * @param {Readonly<FileDescriptor>} fileDesc
     *  The new file descriptor.
     */
    #setFileDescriptor(fileDesc) {
        this.coreApp.set("docsFileDesc", fileDesc);
    }

    /**
     * Sets the value of a file descriptor property as element attribute of the
     * applicazion's root node.
     */
    #setFileRootAttribute(fileProp, rootAttr) {
        const fileDesc = this.getFileDescriptor();
        this.setRootAttribute(rootAttr, fileDesc?.[fileProp] || null);
    }

    /**
     * Updates the application title according to the current file name. If the
     * application does not contain a file descriptor, shows the localized word
     * "Unnamed" as title.
     */
    #updateTitle() {

        // the title for the application launcher and browser window
        var title = this.getShortFileName();

        if (title) {
            this.coreApp.setTitle(title);
            this.#appWindow.setTitle(title);
            this.trigger('docs:filename', title);
            if (_.device('smartphone')) {
                apps.trigger('launcher:update', this);
            }
        }

        // add data from file descriptor to application root node (for automated testing)
        this.#setFileRootAttribute('folder_id', 'data-file-folder');
        this.#setFileRootAttribute('id', 'data-file-id');
        this.#setFileRootAttribute('filename', 'data-file-name');
        this.#setFileRootAttribute('version', 'data-file-version');
        this.#setFileRootAttribute('source', 'data-file-source');
        this.#setFileRootAttribute('attachment', 'data-file-attachment');
        this.#setFileRootAttribute('com.openexchange.realtime.resourceID', 'data-file-resourceID');

        // add the file extension to the application root node
        this.setRootAttribute('data-fileformat', this.getFileExtension());
    }

    /**
     * Updates the browser URL according to the current file descriptor.
     */
    #updateUrl() {
        // only for files from OX Drive, or from OX Documents Portal
        const fileDesc = this.getFileDescriptor();
        if (isDriveFile(fileDesc) && this.isActive() && !this.isViewerMode()) {
            this.coreApp.setState({ folder: fileDesc.folder_id, id: fileDesc.id });
        }
    }

    #updateZoomAttrs() {
        const { type, factor } = this.docView.zoomState;
        this.setRootAttribute('data-zoom-type', type);
        this.setRootAttribute('data-zoom-factor', Math.round(factor * 100));
    }

    #setExternalStorageFlag() {
        const fileDesc = this.getFileDescriptor();
        if (fileDesc) {
            isExternalFolder(fileDesc).done(isExternal => {
                this.#isExternalStorage = isExternal;
            });
        }
    }

    /**
     * Calls the file initialization handler passed to the constructor, and
     * updates the internal settings.
     *
     * @returns {JPromise<FileDescriptor>}
     *  A promise that will fulfil when the file descriptor has been
     *  initialized successfully.
     */
    #initializeFile() {

        // call the file handler callback
        var promise = jpromise.invoke(() => this.implInitializeFile());

        // process and set the file descriptor
        this.onFulfilled(promise, fileDesc => {

            // TODO: can this ever happen? error handling?
            if (this.isInQuit() || !fileDesc) { return; }

            // when a new file is created (.action = new & convert) a drive list reload plus thumbnail update is needed
            const action = this.launchConfig.action || "load";
            if (action !== 'load') {
                propagateNewFile(fileDesc);
            }

            this.#setFileDescriptor(fileDesc);
            this.#updateTitle();
            this.#updateUrl();
            this.#setExternalStorageFlag();

            this.launchTracker.setItem('filename', fileDesc.filename || '');

            // need to update cache of FolderApi, which is done indirectly in DriveUtils
            if (fileDesc.folder_id) {
                getWriteState(fileDesc.folder_id);
            }
        });

        // track time needed to create/initialize the file in the remote storage
        return this.launchTracker.asyncStep(promise, 'fileInitialized', '$badge{BaseApp} File initialized in remote storage');
    }

    /**
     * Initializes the user interface of the application, and invokes the
     * import handler.
     */
    #initializeGui() {

        // special handling for viewer mode
        var viewerMode = this.isViewerMode();
        // the performance tracker for launch/import
        var launchTracker = this.launchTracker;

        // hide the view, call the view initialization handler
        var initViewPromise = this.docView.initBeforeImport();
        // call the file initialization handler
        var initFilePromise = this.#initializeFile();

        // wait for view and file initialization simultaneously
        var initGuiPromise = $.when(initViewPromise, initFilePromise);

        // resolve/reject the before-import promise exported to external listaners
        initGuiPromise.done(() => this.#importStartDef.resolve());
        initGuiPromise.fail(() => this.#importStartDef.reject());

        // call the import handler
        initGuiPromise = initGuiPromise.then(async () => {

            // nothing to do, if application is shutting down
            if (this.isInQuit()) { return; }

            // resolve the full file path while the document is being loading (never fail, fall-back to empty array)
            const fileDesc = this.getFileDescriptor();
            const filePathPromise = (fileDesc?.folder_id && isDriveFile(fileDesc)) ? getPath(fileDesc.folder_id).catch(() => undefined) : undefined;

            // invoke the import callback handler (in parallel to running file path promise)
            const [importResult, filePath] = await Promise.all([this.implImportDocument(), filePathPromise]);

            // store the resulting file path, and return the result of the import handler
            this.#filePath = filePath || [];
            this.#setExternalStorageFlag();

            // track time needed to finalize the import process
            launchTracker.step('documentImported', '$badge{BaseApp} Import process finalized');
            return importResult;
        });

        // catch failures and return as value (to be able to leave busy mode regardless of state)
        initGuiPromise = initGuiPromise.then(
            value => ({ resolved: true, value }),
            err => ({ resolved: false, value: err })
        );

        // make the view visible (also if anything before has failed)
        initGuiPromise = initGuiPromise.then(async state => {

            // nothing to do, if application is shutting down
            if (this.isInQuit()) { return null; }

            // make the view visible, catch and ignore failures
            try {
                await this.docView.showAfterImport({ globalFailure: !state.resolved });
            } catch (err) {
                this.setAppReloadDone();
                launchTracker.exception(err, 'showAfterImport failed');
            }

            // track duration to initialize and show the GUI
            launchTracker.step('guiInitialized', '$badge{BaseApp} GUI initialization finished');

            // return the state and result of the promise chain to next step
            return state;
        });

        // post-process the state and result after the view has become visible
        initGuiPromise = initGuiPromise.then(state => {

            // nothing to do, if application is shutting down (e.g., import cancelled)
            if (!state || this.isInQuit()) { return; }

            // always update application title and other settings
            this.#updateTitle();
            this.#setExternalStorageFlag();

            // setup application auto-close timeout, if specified
            var autoCloseDelay = parseInt(locationHash('closetimer'), 10);
            if (state.resolved && (autoCloseDelay > 0)) {
                this.executeDelayed(() => {
                    this.quit({ reason: QuitReason.AUTO });
                }, Math.min(600, autoCloseDelay) * 1000);
            }

            // resolve or reject the global import promise depending on the passed state
            if (state.resolved) {

                this.#importFinishDef.resolve(state.value);

            } else {

                var result = state.value;
                var error = new ErrorCode(result);
                var fileName = this.getFullFileName();
                var message = null;

                if (viewerMode) {
                    if (!error.isError()) {
                        error = new ErrorCode(ERROR_WHILE_LOADING_DOCUMENT);
                    }
                    message = getErrorMessageData(error, { viewerMode: true }).message;
                }

                if (result?.errorForErrorCode) {
                    globalLogger.error('Importing document "' + fileName + '" failed. ' + error.getErrorText());
                    this.setErrorState(error);
                    result = viewerMode ? { error, message } : error;
                } else {
                    var cause = pick.string(result, "cause", "unknown");
                    globalLogger.error('Importing document "' + fileName + '" failed. Error code: "' + cause + '".');
                    result = viewerMode ? _.extend({}, result, { message }) : result;
                }

                this.setAppReloadDone();
                this.#importFinishDef.reject(result);
            }

            // add marker attribute to application root node (for automated testing)
            this.setRootAttribute('data-doc-loaded', true);
        });

        // track the duration of the final post-processing step
        return launchTracker.asyncStep(initGuiPromise, 'launchEnd', '$badge{BaseApp} Postprocessing after import finished');
    }

    /**
     * Invokes all passed callback functions in parallel, and returns a promise
     * that accumulates the result of all functions.
     *
     * @param {Array<FuncType<MaybeAsync, ArgsT>>} handlers
     *  All callback functions to be invoked.
     *
     * @param {ArgsT} args
     *  The arguments to be passed to all callback functions.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.ignoreErrors=false]
     *      If set to `true`, all thrown exceptions and rejected promises will
     *      be ignored, and will not cause to reject the promise returned from
     *      this method. Internal errors (JavaScript engine exceptions such as
     *      `SyntaxError`) will always be ignored.
     */
    #callHandlers(handlers, args, options) {

        // whether a callback has returned a rejected promise
        var rejected = false;

        // execute all handlers and store the promises in an array
        var promises = handlers.map(handler => {
            var promise = jpromise.invoke(() => handler.call(this, ...args));
            return promise.catch(() => { rejected = true; });
        });

        // accumulate all results into a single promise (all promises will fulfil)
        return $.when(...promises).then(() => {
            if (rejected && !options?.ignoreErrors) {
                throw new Error('callback handler was rejected');
            }
        });
    }

    // mail composition -------------------------------------------------------

    /**
     * generic method that directly triggers either the converter
     * associated with the passed type or that fails with throwing
     * a `TypeError`.
     *
     * _Attention:_ A converter function's direct return value might be a
     * `Promise` or a `Deferred` too. In this case the converter functionality
     * is encapsulated by its *Deferrable* and the result of the converting
     * process gets passed along the resolve chain.
     *
     * @param {string} type
     *  The type that is ought to be associated with a converter function.
     *
     * @returns {JPromise<any>}
     *  The return value of the called converter function.
     */
    #convertTo(type) {

        const converterFn = this.#converterRegistry.get(type);
        if (converterFn) { return converterFn.call(this); }

        globalLogger.warn('The converter does not support a "' + type + '" type.');
        return jpromise.resolve({ title: "", content: "", id: "", filename: '' });
    }

    #handleMailCompositionFailure(message) {
        if (!_.isString(message)) {
            var
                cause = this.isImportSucceeded() ? 'unknown' : 'importfailed';

            switch (cause) {
                case 'importfailed':
                    message = gt('The document was not loaded successfully. Send as mail is not possible.');
                    break;
                default:
                    message = gt('The server does not answer the request. Send as mail is not possible.');
            }
        }
        this.docView.yell({ type: 'error', headline: SEND_AS_MAIL_LABEL, message: (message || null) });
    }

    /**
     * Creates a new mail message with the current document attached as file.
     *
     * @returns {JPromise}
     *  A promise that will be resolved if the mail application has been
     *  launched successfully, or rejected otherwise.
     */
    #handleMailCompositionForFileAttachment() {

        function reply(mail, attachment) {
            /**
             *  changes according to OX Documents task #DOCS-1195
             *  "Adapt to new Mail Compose API"
             *  [https://jira.open-xchange.com/browse/DOCS-1195]
             *
             *  Confluence / 2018 / New Mail Compose API / Concept / API Endpoints
             *  [https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-APIEndpoints]
             */
            const promise = launchMailCompose({
                type:     'replyall',
                original: {
                    id:       mail.id,        // - id of original email
                    folderId: mail.folder_id, // - folder of original email
                    security: mail.security
                },
                attachments: [{ // https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-Datatypes
                    id:       attachment.id,
                    folderId: attachment.folder_id,
                    origin:   'drive' // Origin of file, i.e. File from drive instead of local attachment.
                }]
            });

            return promise.then(compose => {
                /**
                 *  INFO:
                 *
                 *  partially kept from the former(old) mail-compose-api code
                 *
                 *  - `reply` function was written due to fixing bug #40376 "Not able to edit and send back XLSX"
                 *  - see [https://bugs.open-xchange.com/show_bug.cgi?id=40376]
                 */
                if (!compose) {

                    globalLogger.warn('compose element is not present, when mail is retriggered for the same file');
                }
            });
        }

        // Bug #48214, Bug #45775
        // update file data, that preview of the attached file is "current version"
        return propagateChangeFile(this.getFileDescriptor()).done(updatedFile => {
            var attach = _.clone(updatedFile);
            if (attach.source === 'mail') {

                // Bug #32760: special handling, if file is an attachment, e.g. of a mail or task
                // TODO: probably deprecated
                MailApi.get({ id: attach.id, folder_id: attach.folder_id }).done(mail => {
                    reply(mail, mail.attachments[1]);
                });

            } else if (_.isObject(attach.origin) && attach.origin.source === 'mail') {

                // Bug #34300: mail attachments copied to Drive
                MailApi.get({ id: attach.origin.id, folder_id: attach.origin.folder_id }).done(mail => {
                    reply(mail, attach);
                });

            } else {
                /**
                 *  changes according to OX Documents task #DOCS-1195
                 *  "Adapt to new Mail Compose API"
                 *  [https://jira.open-xchange.com/browse/DOCS-1195]
                 *
                 *  Confluence / 2018 / New Mail Compose API / Concept / API Endpoints
                 *  [https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-APIEndpoints]
                 */
                return launchMailCompose({
                    type: 'new',
                    attachments: [{ // https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-Datatypes
                        id:       attach.id,
                        folderId: attach.folder_id,
                        origin:   'drive' // Origin of file, i.e. File from drive instead of local attachment.
                    }]
                });
            }
        });
    }

    #handleMailCompositionForPDFAttachment() {
        return propagateChangeFile(this.getFileDescriptor()).then(() => {

            var promisedAttachmentData = this.#convertTo('pdfAttachment');

            promisedAttachmentData.done(data => {

                /**
                 *  changes according to OX Documents task #DOCS-1195
                 *  "Adapt to new Mail Compose API"
                 *  [https://jira.open-xchange.com/browse/DOCS-1195]
                 *
                 *  Confluence / 2018 / New Mail Compose API / Concept / API Endpoints
                 *  [https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-APIEndpoints]
                 */
                var
                    temporaryAttachment = { // https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-Datatypes
                        id:       String(data.id),
                        folderId: getStandardTrashFolderId(),
                        origin:   'drive' // Origin of file, i.e. File from drive instead of local attachment.
                    },
                    promisedMail = launchMailCompose({
                        type: 'new',
                        attachments: [temporaryAttachment]
                    });

                promisedMail.finally(() => {
                    FilesApi.remove([{

                        id:         temporaryAttachment.id,
                        folder_id:  temporaryAttachment.folderId

                    }], true);
                });
            });

            promisedAttachmentData.fail(msg => {
                if (msg?.text) {
                    this.#handleMailCompositionFailure(msg.text);
                }
            });

            return promisedAttachmentData;
        });
    }

    /**
     * Creates a new mail message with this current document's content
     * being the content of the new mail's body.
     *
     * @returns {JPromise}
     *  A promise that will be resolved if the mail application has been
     *  launched successfully, or rejected otherwise.
     */
    #handleMailCompositionForInlineHTML() {

        var promisedDocument = this.#convertTo('inlineHtml');

        return promisedDocument.then(document => {
            /**
             *  changes according to OX Documents task #DOCS-1195
             *  "Adapt to new Mail Compose API"
             *  [https://jira.open-xchange.com/browse/DOCS-1195]
             *
             *  Confluence / 2018 / New Mail Compose API / Concept / API Endpoints
             *  [https://confluence.open-xchange.com/pages/viewpage.action?pageId=167215176#id-2018/NewMailComposeAPI-APIEndpoints]
             */
            var mailComposeOptions = {
                type: 'new',
                contentType: 'text/html',
                subject: document.title,
                content: document.content
            };
            //  editorMode: 'html',                  - despite these properties continue to be supported, they are considered
            //  preferredEditorMode: 'alternative',    to be deprecated. The new `contentType` "tells" the editor how to operate.

            // // bugfix: Bug #50399 - "Sending shapes as email content doesn't make sense."
            // if (document.isMainlyNonConvertibleContent) {
            //
            // bugfix: Bug #58402 - "When sending content as mail, shapes are not included / visible."
            // change to ... >> The alert will show up as soon as 'non convertible content' is in the document. <<
            //
            // bugfix Bug #58768 - "Misleading alert for empty 'document content as email"
            // Follow up for Bug #58402.
            //
            if (document.isNotConvertible) {
                var isEmptyDocument = document.isEmpty;

                var title = isEmptyDocument ? gt('Empty document') : gt('Non convertible document content');
                var message = isEmptyDocument ?
                    gt('This document is empty. Do you want to send this document as email content?') :
                    gt('This document contains elements that cannot be sent in an email. Do you want to send this document as email content?');
                var promise = this.docView.showQueryDialog(title, message, { showReadOnly: true });

                // show the dialog, then open mail compose
                return promise.then(
                    () => { launchMailCompose(mailComposeOptions); },
                    () => undefined, // no error, when user rejects (DOCS-5316)
                );
            } else {
                launchMailCompose(mailComposeOptions);
            }
        }).fail(msg => {
            if (msg?.text) {
                this.#handleMailCompositionFailure(msg.text);
            }
        });
    }
}

// XAppAccessMixin ------------------------------------------------------------

const XAppAccessProto = XAppAccess.mixin(DObject).prototype;
for (const name of Object.getOwnPropertyNames(XAppAccessProto)) {
    if ((name !== "constructor") && _.isFunction(XAppAccessProto[name])) {
        BaseApplication.prototype[name] = XAppAccessProto[name];
    }
}

// static initialization ======================================================

// Whether the appsuite logout process was started.
export function appsuiteLoggedOut() {
    // 1) We MUST use ox.tabHandlingEnabled here because it's not about the office tab behavior, but about the used login process!
    // 2) For tabbing enabled: The not focused office tabs (follower) will not do a normal quit process, therefore the 'loggingout' state is determined differently.
    // 3) For tabbing enabled: Be aware that the follower tab may have a already destroyed session here at logout. Don't use network activity that require a session.
    return ox.tabHandlingEnabled ? !!TabApi.getLoggingOutState() : officeLogoutEntered;
}

// listen to possible session changes, typical use-cases:
// - relogin after a lost session in the same tab
// - a new login received by another tab, e.g. due to a relogin in another tab
//
// Note: Theoretically it's possible that a new relogin/login with a new session
// happens while the document is reloading, resulting in a session error again.
// Currently, looking at the worst case i.e. session error while loading the document,
// we think the added complexity to handle this case is not worth the effort. // TODO CHECK TEXT AGAIN
ox.on('relogin:success login:success', () => {
    var sessionHasChanged = previousOxSession !== ox.session;

    // why is invalidSessionDetected needed? the session could just be invalid a short time, therefore 'sessionHasChanged'
    // could be 'false' but handling of an invalid session is needed anyhow
    var mustHandleSessionChange = sessionHasChanged || invalidSessionDetected;

    // after a login or relogin there is a valid session
    invalidSessionDetected = false;
    previousOxSession = ox.session;

    if (mustHandleSessionChange) {
        for (const docApp of runningAppSet) {
            docApp.trigger("docs:session:changed");
        }
    }
});

// for http errors in the same tab, triggered by core http.js
ox.on('relogin:required', () => {
    invalidSessionDetected = true;
    for (const docApp of runningAppSet) {
        docApp.trigger("docs:session:invalid");
    }
});

// Bug 28664: remove all save points before they are checked to decide
// whether to show the 'unsaved documents' dialog
ext.point('io.ox/core/logout').extend({
    id: '/io.ox/office/logout/before',
    index: 101, // run before the save points are checked by the core, but after point with id: 'confirmLogout'
    async logout() {
        // `App#removeRestorePoint` is asynchronous: wait for all promises to prevent race condition
        await Promise.all(Array.from(runningAppSet, docApp => {
            const allChangesSaved = !docApp.hasUnsavedChangesOffice();
            return allChangesSaved ? docApp.coreApp.removeRestorePoint() : null;
        }));
    }
});

// listen to user logout and notify all running applications
ext.point('io.ox/core/logout').extend({
    id: '/io.ox/office/logout/quit',
    index: 'last', // run after the dialog (not called if logout was rejected)
    async logout() {
        officeLogoutEntered = true;
        // bug 33152: ignore the result of the quit handlers (rejected promise cancels logout)
        await Promise.all(Array.from(runningAppSet, docApp => docApp.executeQuitHandlers(QuitReason.LOGOUT).catch(fun.undef)));
    }
});
