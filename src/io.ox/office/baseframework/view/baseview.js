/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";

import { is, str, fun, map, jpromise } from "@/io.ox/office/tk/algorithms";
import { COMPACT_DEVICE, isLeadingPosition, parseCssLength, containsNode, createDiv, insertHiddenNodes, setFocus, containsFocus, setFocusInto, clearBrowserSelection, isEscapeKey, addDeviceMarkers } from "@/io.ox/office/tk/dom";
import { guardMethod, onceMethod, debounceMethod } from "@/io.ox/office/tk/objects";
import { DEBUG } from "@/io.ox/office/tk/config";
import { getActiveDialogNode, MessageDialog, QueryDialog } from "@/io.ox/office/tk/dialogs";
import { layoutLogger, a11yLogger } from "@/io.ox/office/tk/utils/logger";

import { ZoomState } from "@/io.ox/office/baseframework/utils/zoomstate";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { CANCEL_LABEL } from "@/io.ox/office/baseframework/view/baselabels";
import { isPriorityAlert, yellLogger, AlertBanner } from "@/io.ox/office/baseframework/view/popup/alertbanner";

import "@/io.ox/office/baseframework/view/basestyle.less";

// class BaseView =============================================================

/**
 * Base class for the view instance of an office application. Creates the
 * application window, and provides functionality to create and control the
 * top, bottom, and side pane elements.
 *
 * @property {ZoomState} zoomState
 *  Wrapper object for the current zoom factor, and the predefined zoom steps
 *  used by this view instance.
 *
 * @property {JQuery} $winBodyNode
 *  The body node of the application window (a descendant element of the window
 *  root node).
 *
 * @property {JQuery} $appPaneNode
 *  The DOM node of the application pane (the complete inner area between all
 *  existing view panes). Note that this is NOT the container node where
 *  applications insert their own contents. The method `insertContents()` is
 *  intended to be used to insert own contents into the application pane.
 *
 * @property {JQuery} $contentRootNode
 *  The root container node for the application contents. Note that this is NOT
 *  the direct parent node where applications insert their own contents, but
 *  the (optionally scrollable) root node containing the target container node
 *  for document contents. The method `insertContents()` is intended to be used
 *  to insert own contents into the application pane.
 *
 * @property {JQuery} $appContentNode
 *  The container node for application contents (child of `$contentRootNode`).
 */
export class BaseView extends ModelObject {

    /** The helper element for custom A11Y announcements. */
    #a11yAlertNode = createDiv({
        classes: "io-ox-office-a11y-alert",
        attrs: { role: "alert", "aria-atomic": true }
    });

    // constructor ------------------------------------------------------------

    /**
     * @param {BaseApplication} docApp
     *  The application containing this view instance.
     *
     * @param {BaseModel} docModel
     *  The document model created by the passed application.
     *
     * @param {BaseViewConfig} config
     *  Configuration options.
     */
    constructor(docApp, docModel, config) {

        // base constructor
        super(docModel, { _kind: "root" });

        // [readonly] public access to configuration
        this.config = { ...config };
        // [readonly] {AppWindow} the application window instance
        this.appWindow = docApp.getWindow();
        // [readonly] {ZoomState} wrapper for the current zoom factor
        this.zoomState = this.member(new ZoomState(config.zoomSteps));

        // the DOM node of the application window body
        this.$winBodyNode = docApp.getWindowNode();
        // root node of the application pane (remaining space for document contents)
        this.$appPaneNode = $('<div class="app-pane">');
        // root container node for visible document contents (may be scrollable)
        this.$contentRootNode = $('<div class="app-content-root">');
        // container node for application contents
        this.$appContentNode = $('<div class="app-content">');

        // root container node for invisible document contents
        this._$hiddenRoot = $('<div class="app-hidden-root noI18n" aria-hidden="true">');
        // the temporary container for all nodes while application is hidden
        this._$tempNode = $('<div aria-hidden="true">');

        // all registered view panes
        this._viewPaneRegistry = this.member(new Set/*<BasePane>*/());
        // whether this application view is hidden explicitly
        this._viewHiddenFlag = false;
        // whether the application is effectively visible (combines application's state and "viewHiddenFlag")
        this._effectiveVisible = null;
        // cached alert banner, shown when application becomes visible
        this._currentAlert = null;
        // the current progress value to be shown at the progress bar
        this._currentProgress = 0;

        // add CSS marker class for flexible view pane layout to application root node
        this.appWindow.addClass("io-ox-office-flexible-view-panes");

        // add application-specific classes to the window body node, and the temporary container node
        const windowNodeClasses = str.concatTokens("io-ox-office-main", `${docApp.moduleName.replace(/[./]/g, "-")}-main`, config.classes);
        this.$winBodyNode.addClass(windowNodeClasses);
        this._$tempNode.addClass(windowNodeClasses);

        // top-level containers for top/bottom panes, and center area for side panes and application pane
        const headerPanesNode = $('<div class="view-pane-container" data-pos="top">');
        const centerPanesNode = $('<div class="view-pane-container" data-pos="center">');
        const footerPanesNode = $('<div class="view-pane-container" data-pos="bottom">');
        this.$winBodyNode.append(headerPanesNode, centerPanesNode, footerPanesNode);

        // markers for touch devices and browser types
        addDeviceMarkers(this.$appPaneNode[0]);
        // add class "translucent-shade" for blurry, greyish images to selected elements
        this.$appPaneNode.addClass("translucent-shade");

        // embedded containers for side panes
        const leftPanesNode = $('<div class="view-pane-container translucent-constant" data-pos="left">');
        const rightPanesNode = $('<div class="view-pane-container translucent-constant" data-pos="right">');
        centerPanesNode.append(leftPanesNode, this.$appPaneNode, rightPanesNode);

        // contents of the application pane
        this.$appPaneNode.append(this._$hiddenRoot, this.$contentRootNode);
        this.$contentRootNode.append(this.$appContentNode);

        // add the temporary container for hidden state into the DOM
        insertHiddenNodes(this._$tempNode);

        this.insertContents(this.#a11yAlertNode);

        // forward controller change events
        docApp.onInit(() => {
            this.listenTo(docApp.docController, "change:items", changedItems => this.trigger("controller:update", changedItems));
            this.listenTo(docApp.docController, "execute:item", (key, value) => this.trigger("controller:execute", key, value));
        });

        // generate and forward modal dialog events
        let dialogStackLevel = 0;
        this.listenToGlobal("dialog:construct", (dialog, config) => {
            if (docApp.isActive()) {
                this.listenTo(dialog, "show", () => {
                    dialogStackLevel += 1;
                    this.trigger("dialog:show", dialog, config, dialogStackLevel);
                });
                this.listenTo(dialog, "close", () => {
                    this.trigger("dialog:close", dialog, config, dialogStackLevel);
                    dialogStackLevel -= 1;
                });
            }
        });

        // log layout events
        this.logEvents(layoutLogger, "refresh:layout");

        // keep application in DOM while application is hidden, applications
        // may want to access element geometry in background tasks
        this.listenTo(this.appWindow, "show", this._windowShowHandler);
        this.listenTo(this.appWindow, "hide", this._windowHideHandler);

        // explicitly close permanent alert banners that are not closable manually
        this.listenTo(docApp, "docs:beforequit", () => {
            if (this._currentAlert && !this._currentAlert.isClosable()) {
                this.hideYell({ priority: true });
            }
        });

        // listen to browser window resize events
        this.listenTo(window, "resize", this.debounce(this.refreshPaneLayout, { delay: 50, maxDelay: 200 }));

        // close all app menus on orientationchange on compact devices
        // .grabFocus() has an isVisible check so it's not necessary here
        if (COMPACT_DEVICE) {
            this.listenTo(window, "orientationchange", () => {
                if (this._effectiveVisible) {
                    this.grabFocus();

                    // <BUG-ID: 67944> This delay is necessary as after the resize we need to calculate the height of the window
                    // which differs in case of iPhone Safari
                    if (_.browser.Safari) {
                        this.setTimeout(() => {
                            document.documentElement.style.height = `${window.innerHeight}px`;
                        }, 290);
                    }
                }
            });
        }

        // initialize content node from passed options
        this.$contentRootNode.toggleClass("scrolling", !!config.contentScrollable);
        this.setContentMargin(config.contentMargin || 0);

        // make the content root node focusable for global navigation with F6 key
        if (config.contentFocusable) {
            this.$contentRootNode.addClass("f6-target").attr("tabindex", 1);
        }

        // update view and pane layout after import
        this.waitForImport(this._windowShowHandler);
    }

    destructor() {

        // hide and destroy current alert banner
        this.hideYell({ priority: true });

        // destroy all image nodes (release the browser-internal bitmap data)
        this.docApp.destroyImageNodes(this.$winBodyNode);
        this.docApp.destroyImageNodes(this._$tempNode);

        this._$tempNode.remove();
        this.$appPaneNode.remove();

        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the browser focus is located inside the application.
     *
     * @returns {boolean}
     *  Whether the browser focus is located inside the application.
     */
    hasFocus() {
        return containsFocus(this.$winBodyNode, { allowSelf: true });
    }

    /**
     * Returns whether the browser focus is located inside the application
     * pane.
     *
     * @returns {boolean}
     *  Whether the browser focus is located inside the application pane.
     */
    hasAppFocus() {
        return containsFocus(this.$appPaneNode, { allowSelf: true });
    }

    /**
     * Moves the browser focus into the application pane. Calls the handler
     * function passed to the constructor of this instance.
     *
     * @param {GrabFocusOptions} [options]
     *  Optional parameters that will be passed to the grab focus handler.
     */
    grabFocus(options) {

        // nothing to do, if the view is in hidden state, or the application is quitting
        if (!this._effectiveVisible || this.docApp.isInQuit()) { return; }

        // move focus into open modal dialog (not if already focused)
        const dialogNode = getActiveDialogNode();
        if (dialogNode) {
            if (!containsFocus(dialogNode, { allowSelf: true })) {
                setFocusInto(dialogNode);
            }
            return;
        }

        // move focus into busy blocker element (not if already focused)
        if (this.isBusy()) {
            if (!containsFocus(this.appWindow.nodes.blocker, { allowSelf: true })) {
                setFocus(this.appWindow.nodes.blocker);
            }
            return;
        }

        // invoke the callback handler of the application, fallback to some root node
        if (this.config.grabFocusHandler) {
            this.config.grabFocusHandler.call(this, options);
        } else if (this.config.contentFocusable) {
            setFocus(this.$contentRootNode);
        } else {
            setFocusInto(this.$appPaneNode);
        }
    }

    /**
     * Returns whether the contents of the view are currently visible.
     *
     * @returns {boolean}
     *  Whether the application is active, and the view has not been hidden
     *  manually.
     */
    isVisible() {
        return !!this._effectiveVisible; // is `null` initially
    }

    /**
     * Hides all contents of the application window and moves them to the
     * internal temporary DOM storage node.
     *
     * @returns {BaseView}
     *  A reference to this instance.
     */
    hide() {
        this._viewHiddenFlag = true;
        this._windowHideHandler();
        return this;
    }

    /**
     * Shows all contents of the application window, if the application itself
     * is currently active. Otherwise, the contents will be shown when the
     * application becomes visible.
     *
     * @returns {BaseView}
     *  A reference to this instance.
     */
    show() {
        this._viewHiddenFlag = false;
        this._windowShowHandler();
        return this;
    }

    /**
     * Sets the application into the busy state by displaying a window blocker
     * element covering the entire GUI of the application. The contents of the
     * header and footer in the blocker element are cleared, and the passed
     * initialization callback function may insert new contents into these
     * elements.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {Function} [options.initHandler]
     *    A function that can fill custom contents into the header and footer
     *    of the window blocker element. Will be called in the context of this
     *    view instance. Receives the following parameters:
     *    1. {JQuery<HTMLDivElement>} headerNode
     *       The header element above the centered progress bar.
     *    2. {JQuery<HTMLDivElement>} footerNode
     *       The footer element below the centered progress bar.
     *    3. {JQuery<HTMLDivElement>} blockerNode
     *       The entire window blocker element (containing the header,
     *       footer, and progress bar elements).
     *  - {Function} [options.cancelHandler]
     *    If specified, a "Cancel" button will be shown after a short delay.
     *    Pressing that button, or pressing the ESCAPE key, will execute this
     *    callback function, and will leave the busy mode afterwards. Will be
     *    called in the context of this view instance.
     *  - {number} [options.delay=500]
     *    The delay time before the busy blocker element becomes visible (by
     *    fading to a half-transparent fill color). Note that the busy blocker
     *    element exists and blocks everything right after calling this method,
     *    the delay time just specifies when it becomes visible.
     *  - {boolean} [options.immediate=false]
     *    If set to `true`, the half-transparent busy blocker element will be
     *    shown immediately. Otherwise, the element will be set to transparent
     *    initially, and will fade in after the specified delay (see option
     *    "delay").
     *  - {number} [options.timeout]
     *    If set to a positive number, the busy blocker will be hidden
     *    automatically after the specified time (in milliseconds). By default,
     *    the busy state remains active until calling the method `leaveBusy()`.
     *  - {string} [options.topLabel]
     *    If specified, the string will be shown in the top-left corner of the
     *    blocker element.
     *  - {string} [options.warningLabel]
     *    If specified, an alert box containing the passed text will be shown
     *    after a specific delay (see option "warningDelay"). Intended to be
     *    used to show a message for long-running actions.
     *  - {number} [options.warningDelay=3000]
     *    The delay time before the warning alert becomes visible. If the
     *    blocker element will be faded in delayed by itself, the warning delay
     *    will start after that delay time.
     */
    enterBusy(options) {

        // for safety against repeated calls: stops pending animations etc.
        this.leaveBusy();

        // notify all listeners
        this.trigger("view:busy");

        // enter busy state, and extend the blocker element
        this.appWindow.busy(null, null, (blockerNode, headerNode, footerNode) => {

            // the delay time before the blocker element becomes visible
            const delay = options?.delay ?? 500;
            // the timer waiting to fade in the blocker element in busy mode
            let blockerFadeTimer = null;
            // the timer waiting to show a warning message during busy mode
            let blockerWarningTimer = null;
            // timer for automatically leaving busy mode (option `timeout`)
            let autoLeaveTimer = null;

            // the DOM element containing the warning alert box, or the single cancel button
            const warningNode = $('<div class="hidden">');
            // the delay time before the warning alert box becomes visible
            let warningDelay = options?.warningDelay ?? 3000;

            // the cancel button element
            let cancelButton = null;
            // invokes the cancel handler exactly once, if it exists
            const cancelHandler = options?.cancelHandler ? fun.once(() => options.cancelHandler.call(this)) : undefined;

            // event handler scope for unbinding
            const scope = Symbol("scope");

            // bug 38770: grab focus to prevent modifications of document
            blockerNode.attr("tabindex", 1);
            this.grabFocus(); // DOCS-2369: use `grabFocus()` (do not steal focus from open dialog)
            this._invalidateTabIndexes();
            this.listenToGlobal("change:focus", focusEvent => {
                const { focusNode } = focusEvent;
                if (focusNode && containsNode(this.$winBodyNode, focusNode) && !containsNode(blockerNode, focusNode, { allowSelf: true })) {
                    setFocus(cancelButton?.is(":visible") ? cancelButton : blockerNode);
                }
            }, { scope });

            // bug 41397: destroying browser selection helps to prevent document modifications
            clearBrowserSelection();

            // register a keyboard handler for the ESCAPE key
            this.listenTo(blockerNode, "keydown", event => {
                if (isEscapeKey(event)) {
                    cancelHandler?.();
                    return false;
                }
            }, { scope });

            // unregister all event handlers when leaving busy mode
            this.listenTo(this.appWindow, "idle", () => {
                // abort all running timers
                blockerFadeTimer?.abort();
                blockerWarningTimer?.abort();
                autoLeaveTimer?.abort();
                // unregister event listeners
                this.stopListeningToScope(scope);
                if (this._effectiveVisible) { this._restoreTabIndexes(); }
                this.trigger("view:idle");
            }, { scope });

            // immediately run cancel handler when the quit button of the busy screen has been clicked (53941)
            if (cancelHandler) {
                this.listenTo(this.coreApp, "docs:busy:quit", cancelHandler, { scope });
            }

            // execute initialization handler
            options?.initHandler?.call(this, headerNode, footerNode, blockerNode);

            // append the container node for the warning alert box and/or cancel button
            footerNode.append(warningNode);

            // fade in blocker node after a delay
            if (!options?.immediate) {
                blockerNode.css("opacity", 0);
                blockerFadeTimer = this.setInterval(index => {
                    blockerNode.css("opacity", (index + 1) / 10);
                }, { delay, interval: 50, count: 10 });
                // defer showing the warning box until blocker is visible
                warningDelay += delay + 500;
            }

            // automatically leave busy mode after the specified time
            if (options && (options.timeout > 0)) {
                autoLeaveTimer = this.setTimeout(() => {
                    this.leaveBusy();
                    autoLeaveTimer = null;
                }, options.timeout);
            }

            // initialize a cancel button, depending on the passed options
            if (cancelHandler) {
                cancelButton = $.button({ label: CANCEL_LABEL }).addClass("btn-warning");
                cancelButton.on("click", cancelHandler);
            }

            // create a warning alert box if specified, insert cancel button into the alert box
            if (options?.warningLabel) {
                const alertNode = $('<div class="alert alert-warning">').append($("<div>").text(options.warningLabel));
                if (cancelButton) { alertNode.append($("<div>").append(cancelButton)); }
                warningNode.append(alertNode);
            } else if (cancelButton) {
                warningNode.append(cancelButton);
            }

            // show warning alert box after a delay
            if (warningNode.children().length > 0) {
                blockerWarningTimer = this.setTimeout(() => {
                    warningNode.removeClass("hidden");
                    if (cancelButton) {
                        setFocus(cancelButton);
                        blockerNode.removeAttr("tabindex");
                    }
                }, warningDelay);
            }
        }, options);
    }

    /**
     * Returns whether this application is currently in busy state.
     *
     * @returns {boolean}
     *  Whether this application is currently in busy state.
     */
    isBusy() {
        return this.appWindow.isBusy();
    }

    /**
     * Updates the progress bar of the busy blocker element.
     *
     * @param {number} progress
     *  The progress value (floating-point number between 0 and 1).
     */
    updateBusyProgress(progress) {
        this._currentProgress = progress;
        this._updateBusyProgressDebounced();
    }

    /**
     * Leaves the busy state of the application. Hides the window blocker
     * element covering the entire GUI of the application.
     */
    leaveBusy() {
        this.appWindow.idle();
    }

    /**
     * Shows an alert banner, if the application is currently visible.
     * Otherwise, the most recent alert banner will be cached until the
     * application becomes visible again.
     *
     * @param {AlertBannerConfig} config
     *  The settings for the alert banner.
     *
     * @returns {boolean}
     *  Whether the alert banner could be shown. The return value `false` means
     *  there is already a high-priority alert banner visible, and the
     *  configuration passed to this method does not contain the "priority"
     *  flag for the new alert banner.
     */
    yell(config) {

        yellLogger.info("$badge{BaseView} yell", config);

        // skip notifications in plugged mode
        if (config.skipPlugged && this.docApp.isViewerMode()) { return false; }

        // hide and destroy current alert banner
        const priority = isPriorityAlert(config);
        if (!this.hideYell({ priority })) { return false; }

        // create a new alert banner wrapper
        this._currentAlert = new AlertBanner(this, config);
        return true;
    }

    /**
     * Explicitly hides the alert banner currently shown, and clears the cached
     * settings for an alert banner to be shown in the future when this view
     * becomes visible.
     *
     * @param {AlertBannerPriorityOptions} options
     *  Optional parameters. Without the option "priority" set, alert banners
     *  with high priority will not be hidden.
     *
     * @returns {boolean}
     *  Whether the alert banner is gone (according to priority mode). This
     *  method also returns `true`, if there was no alert banner before.
     */
    hideYell(options) {

        yellLogger.info("$badge{BaseView} hideYell", this._currentAlert, options);

        // no alert available (return `true` to indicate a new alert can be shown)
        if (!this._currentAlert) { return true; }

        // prevent hiding an active high-priority alert banner
        if (!options?.priority && this._currentAlert.isActive() && this._currentAlert.isPriority()) {
            return false;
        }

        // destroy the alert banner (will hide itself automatically)
        this._currentAlert.destroy();
        this._currentAlert = null;
        return true;
    }

    /**
     * Shows a modal message dialog with OK button (an instance of the class
     * `MessageDialog`).
     *
     * @param {string} title
     *  The title text for the dialog.
     *
     * @param {string} message
     *  The message text shown in the dialog body.
     *
     * @param {MessageDialogConfig} [options]
     *  Additional options passed to the dialog constructor.
     *
     * @returns {JPromise}
     *  A promise representing the dialog. Will fulfil after pressing the "OK"
     *  button.
     */
    showMessageDialog(title, message, options) {
        return new MessageDialog(message, { ...options, title, message }).show();
    }

    /**
     * Shows a modal query dialog with Yes/No buttons (an instance of the class
     * `QueryDialog`).
     *
     * @param {string} title
     *  The title text for the dialog.
     *
     * @param {string} message
     *  The message text shown in the dialog body.
     *
     * @param {QueryDialogConfig} [options]
     *  Additional options passed to the dialog constructor.
     *
     * @returns {JPromise}
     *  A promise representing the dialog. Will fulfil after pressing the "Yes"
     *  button; or reject after pressing the "No" button, or losing edit
     *  rights.
     */
    showQueryDialog(title, message, options) {
        return new QueryDialog({ ...options, title, message }).show();
    }

    /**
     * Announces some custom text to screen readers.
     *
     * @param {string} text
     *  The text to be announced to screen readers.
     */
    sendA11YAnnouncement(text) {
        this.#a11yAlertNode.textContent = "";
        this.#a11yAlertNode.textContent = text;
        a11yLogger.log(text);
    }

    /**
     * A shortcut to receive the enabled state of the specified controller item
     * that directly invokes the method `ControllerEngine#isItemEnabled` of the
     * application controller owning this view instance.
     *
     * @param {string} key
     *  The key of the item. If the key starts with an exclamation mark, its
     *  "enabled" state will be reversed.
     *
     * @returns {boolean}
     *  Whether the specified item exists and is enabled.
     */
    isControllerItemEnabled(key) {
        return this.docApp.docController.isItemEnabled(key);
    }

    /**
     * A shortcut to receive the value of the specified controller item that
     * directly invokes the method `ControllerEngine#getItemValue` of the
     * application controller owning this view instance.
     *
     * @param {string} key
     *  The key of the item.
     *
     * @returns {unknown}
     *  The current value of the item; or `undefined`, if the item does not
     *  exist.
     */
    getControllerItemValue(key) {
        return this.docApp.docController.getItemValue(key);
    }

    /**
     * A shortcut to execute the specified controller item that directly
     * invokes the method `ControllerEngine#executeItem` of the application
     * controller owning this view instance.
     *
     * @param {string} key
     *  The key of the controller item to be changed.
     *
     * @param {unknown} [value]
     *  The new value of the controller item.
     *
     * @param {object} [options]
     *  Optional parameters passed to the controller.
     *
     * @returns {JPromise}
     *  A promise that will settle according to the result of the item setter
     *  function.
     */
    executeControllerItem(key, value, options) {
        return this.docApp.docController.executeItem(key, value, options);
    }

    /**
     * Binds the specified form control to a controller item. This view will
     * install event listeners to update the control's enabled state and value
     * according to the state of the controller item, and will execute the
     * controller item when the form control triggers.
     *
     * @param {string} key
     *  The key of the controller item the form control will be bound to.
     *
     * @param {Group} control
     *  The form control to be bound to the controller item.
     */
    bindGroupToControllerItem(key, control) {

        // set the "data-key" at the control root element for automated testing
        control.el.dataset.key = key;

        // execute controller item when control triggers commit event
        this.listenTo(control, "group:commit", (value, options) => {
            jpromise.floating(this.executeControllerItem(key, value, options));
        });

        // execute controller item when control triggers preview event
        this.listenTo(control, "group:preview", value => {
            jpromise.floating(this.executeControllerItem(key, value, { preview: true }));
        });

        // handle cancel events triggered by the control
        this.listenTo(control, "group:cancel", options => {
            this.docApp.docController.grabFocus(options);
            jpromise.floating(this.docApp.docController.update());
        });

        // update the enabled state and value of the control
        control.listenTo(this, "controller:update", changedItems => {
            map.visit(changedItems, key, itemState => {
                control.enable(itemState.enabled);
                control.setValue(itemState.value);
            });
        });

        // trigger update to initialize the control
        jpromise.floating(this.docApp.docController.update());
    }

    /**
     * Unbinds the specified form control from a controller item.
     *
     * @param {Group} control
     *  The form control to be unbound.
     */
    unbindGroupFromControllerItem(control) {

        // remove the "data-key" at the control root element
        delete control.el.dataset.key;

        // stop listening to the control
        this.stopListeningTo(control);
        control.stopListeningTo(this, "controller:update");
    }

    /**
     * Adds the passed view pane instance into this view.
     *
     * @param {PaneT extends BasePane} viewPane
     *  The view pane instance to be inserted into this view.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.outer=false]
     *    If set to `true`, the view pane will be inserted at the outer border
     *    of the application window. By default, new view panes will be
     *    inserted inside the remaining space not yet occupied by other view
     *    panes.
     *
     * @returns {PaneT}
     *  The view pane passed to this method.
     */
    addPane(viewPane, options) {

        // insert the new view pane into the registry
        this._viewPaneRegistry.add(viewPane);

        // insert the view pane into the correct target container
        const targetRootNode = this._viewHiddenFlag ? this._$tempNode : this.$winBodyNode;
        const targetNode = targetRootNode.find(`.view-pane-container[data-pos="${viewPane.position}"]`);
        if (!options?.outer === isLeadingPosition(viewPane.position)) {
            targetNode.append(viewPane.el);
        } else {
            targetNode.prepend(viewPane.el);
        }

        // refresh pane layout when the pane or its contents are changed
        this.listenTo(viewPane, "pane:show pane:hide pane:resize", this._paneResizeHandler);
        this._paneResizeHandler();

        // forward special repaint events from toolpanes
        this.listenTo(viewPane, "toolpane:showcontents", () => this.trigger("toolpane:showcontents"));

        return viewPane;
    }

    /**
     * Adjusts the positions of all view pane nodes.
     */
    @guardMethod
    refreshPaneLayout() {

        // do nothing if the window is hidden (but refresh if import runs (fast loading))
        if (!this._effectiveVisible || this.docApp.isInQuit()) {
            return;
        }

        // let all view panes update their internal size (this may even change the pane size again due to internal refresh)
        try {
            layoutLogger.takeTime("$badge{BaseView} refreshPaneLayout", () => {
                this._viewPaneRegistry.forEach(viewPane => viewPane.refresh({ immediate: true }));
            });
        } catch (err) {
            layoutLogger.exception(err);
        }

        // notify listeners
        this.trigger("refresh:layout");
    }

    /**
     * Detaches the document contents in the application pane from the DOM.
     */
    detachAppPane() {
        this.$appContentNode.detach();
    }

    /**
     * Attaches the document contents in the application pane to the DOM.
     */
    attachAppPane() {
        this.$contentRootNode.append(this.$appContentNode);
    }

    /**
     * Returns the root container node for the application contents.
     *
     * Note that this is NOT the direct parent node where applications insert
     * their own contents, but the (optionally scrollable) root node containing
     * the target container node for document contents. The method
     * `insertContents()` is intended to be used to insert own contents into
     * the application pane.
     *
     * @returns {JQuery}
     *  The DOM root node for visible document contents.
     */
    getContentRootNode() {
        return this.$contentRootNode;
    }

    /**
     * Returns the current margins between the fixed application pane and the
     * embedded application container node.
     *
     * @returns {object}
     *  The margins between the fixed application pane and the embedded
     * application container node (in pixels), in the properties "left",
     * "right", "top", and "bottom".
     */
    getContentMargin() {
        return {
            left: parseCssLength(this.$appContentNode, "marginLeft"),
            right: parseCssLength(this.$appContentNode, "marginRight"),
            top: parseCssLength(this.$appContentNode, "marginTop"),
            bottom: parseCssLength(this.$appContentNode, "marginBottom")
        };
    }

    /**
     * Changes the margin between the fixed application pane and the embedded
     * application container node.
     *
     * @param {number|object} margin
     *  The margins between the fixed application pane and the embedded
     *  application container node, in pixels. If set to a number, all margins
     *  will be set to the specified value. Otherwise, an object with the
     *  optional properties "left", "right", "top", and "bottom" for specific
     *  margins for each border. Missing properties default to the value zero.
     */
    setContentMargin(margin) {

        if (is.dict(margin)) {
            margin = { left: 0, right: 0, top: 0, bottom: 0, ...margin };
        } else {
            if (!is.number(margin)) { margin = 0; }
            margin = { left: margin, right: margin, top: margin, bottom: margin };
        }

        this.$appContentNode.css("margin", `${margin.top}px ${margin.right}px ${margin.bottom}px ${margin.left}px`);
    }

    /**
     * Inserts new DOM nodes into the container node of the application pane.
     *
     * @param {NT extends NodeOrJQuery} nodes
     *  The DOM node(s) to be inserted into the application pane.
     *
     * @returns {NT}
     *  The nodes passed to this method.
     */
    insertContents(nodes) {
        this.$appContentNode.append(nodes);
        this.refreshPaneLayout();
        return nodes;
    }

    /**
     * Inserts new DOM nodes into the hidden container node of the application
     * pane.
     *
     * @param {NT extends NodeOrJQuery} nodes
     *  The DOM node(s) to be inserted into the hidden container of the
     *  application pane.
     *
     * @returns {NT}
     *  The nodes passed to this method.
     */
    insertHiddenNodes(nodes) {
        this._$hiddenRoot.append(nodes);
        return nodes;
    }

    /**
     * Returns the effective zoom factor of the document.
     *
     * @returns {number}
     *  The effective zoom factor for the document.
     */
    getZoomFactor() {
        return this.zoomState.factor;
    }

    /**
     * Changes the current zoom type and factor, and updates the view.
     *
     * @param {string} type
     *  The new zoom type. Must be of the type keywords registered by this
     *  view. The built-in type "fixed" specifies a fixed zoom factor.
     *
     * @param {Nullable<number>} [factor]
     *  A precalculated zoom factor to be set. If omitted or set to `null`, the
     *  callback function registered for the specified zoom type will be
     *  invoked to obtain the new zoom factor. Built-in zoom type "fixed" will
     *  just keep the current zoom factor.
     *
     * @param {ZoomEventOptions} [options]
     *  Optional parameters.
     */
    changeZoom(type, factor, options) {
        this.zoomState.set(type, factor, options);
    }

    /**
     * Prepares the view for importing the document. Sets the application to
     * busy state, and invokes the view initialization handler.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil when the initialization handler has finished.
     */
    @onceMethod
    async initBeforeImport() {

        // set window to busy state
        this.appWindow.busy();
        this.hide();

        // invoke initialization methods, track initialization time
        try {
            await this.implInitialize();
            if (DEBUG) { await this.implInitializeDebug(); }
        } finally {
            this.docApp.launchTracker.step("guiPrepared", "$badge{BaseView} Initialized/hidden before import");
        }
    }

    /**
     * Initializes the view after importing the document. Sets the view to idle
     * state, and invokes the GUI initialization handler.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.globalFailure=false]
     *    If set to `true`, the application is in an internal error state, and
     *    the view shall not be initialized further.
     *
     *  May contain additional custom options. All options will also be passed
     *  to the method `implInitializeGui`.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil when initialization of the GUI is finished.
     */
    @onceMethod
    async showAfterImport(options) {

        // leave hidden view mode (still in busy mode)
        this.show();

        // refresh all view panes, and leave busy mode
        this.refreshPaneLayout();
        this.leaveBusy();

        // invoke GUI initialization (not when import has failed, or in embedded viewer mode)
        try {
            if (!options?.globalFailure && !this.docApp.isViewerMode()) {
                await this.implInitializeGui(options);
            }
        } finally {
            // track duration to make the document visible (including GUI initialization handler)
            this.docApp.launchTracker.step("documentVisible", "$badge{BaseView} Shown during/after import");
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * This method will be called to initialize this view instance after
     * construction. Will be invoked by the method `initBeforeImport`.
     *
     * @returns {MaybeAsync}
     *  May return a promise to indicate asynchronous initialization.
     */
    /*protected abstract*/ implInitialize() {
        throw new ReferenceError("abstract method called");
    }

    /**
     * This method will be called to additionally initialize debug mode for
     * this view instance after construction. Will be invoked by the method
     * `initBeforeImport` after running `implInitialize`.
     *
     * @returns {MaybeAsync}
     *  May return a promise to indicate asynchronous initialization.
     */
    /*protected*/ implInitializeDebug() {
        // intentionally empty
    }

    /**
     * This method will be called to initialize the graphical elements of this
     * view instance, after the document has been imported. Will be invoked by
     * the method `showAfterImport`.
     *
     * @param {object} [options]
     *  Optional parameters passed to the method `showAfterImport`.
     *
     * @returns {MaybeAsync}
     *  May return a promise to indicate asynchronous initialization.
     */
    /*protected abstract*/ implInitializeGui(/*options*/) {
        throw new ReferenceError("abstract method called");
    }

    // private methods --------------------------------------------------------

    /**
     * Refreshes the view layout after a view pane has been modified.
     */
    @debounceMethod({ delay: "animationframe" })
    /*private*/ _paneResizeHandler() {
        this.refreshPaneLayout();
    }

    /**
     * Sets an invalid (negative) "tabindex" attribute to all elements in the
     * application window body that contain a "tabindex" attribute.
     */
    /*private*/ _invalidateTabIndexes() {
        this.$winBodyNode.find('[tabindex="1"]').attr("tabindex", -42);
    }

    /**
     * Restores all "tabindex" attributes in the application window body that
     * have been invalidated with the method invalidateTabIndexes().
     */
    /*private*/ _restoreTabIndexes() {
        this.$winBodyNode.find('[tabindex="-42"]').attr("tabindex", 1);
    }

    /**
     * Updates the "effectiveVisible" flag, and returns whether the flag has
     * actually changed.
     */
    /*private*/ _updateEffectiveVisible() {
        const oldEffectiveVisible = this._effectiveVisible;
        this._effectiveVisible = !this._viewHiddenFlag && this.docApp.isActive();
        return oldEffectiveVisible !== this._effectiveVisible;
    }

    /**
     * Updates the view after the application becomes active/visible.
     */
    /*private*/ _windowShowHandler() {

        // nothing to do if application is hidden already
        if (!this._updateEffectiveVisible() || this.docApp.isInQuit()) { return; }

        layoutLogger.takeTime("$badge{BaseView} windowShowHandler", () => {

            // move all application nodes from temporary storage into view
            if (this._$tempNode.children().length > 0) {
                this.$winBodyNode.append(this._$tempNode.children());
                // restore invalidated tabindex attribute for browser focus tab chain
                if (!this.isBusy()) {
                    this._restoreTabIndexes();
                    // bug 49359: due to the detached DOM calling idle() didn't remove the disabled attributes
                    // from input and textarea elements, so call it again after the DOM is attached
                    this.leaveBusy();
                }
            }

            // do not update GUI and grab focus while document is still being imported
            if (this.isImportSucceeded()) {
                this.setTimeout(() => {
                    this.refreshPaneLayout();
                    this.grabFocus({ afterImport: true });
                }, 0);
            }

            // notify event handlers
            this.trigger("view:show");
        });
    }

    /**
     * Updates the view after the application becomes inactive/hidden.
     */
    /*private*/ _windowHideHandler() {

        // nothing to do if application is hidden already
        if (!this._updateEffectiveVisible() || this.docApp.isInQuit()) { return; }

        layoutLogger.takeTime("$badge{BaseView} windowHideHandler", () => {

            // notify event handlers
            this.trigger("view:hide");

            // move all application nodes from view to temporary storage
            if (this._$tempNode.children().length === 0) {
                // remove all nodes from browser focus tab chain as long as they are hidden
                this._invalidateTabIndexes();
                this._$tempNode.append(this.$winBodyNode.children());
            }
        });
    }

    @debounceMethod({ delay: 500, maxDelay: 500 })
    /*private*/ _updateBusyProgressDebounced() {
        if (this.isBusy()) {
            this.appWindow.busy(this._currentProgress);
        }
    }
}
