/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary } from "@/io.ox/office/tk/algorithms";
import { convertCssLength } from "@/io.ox/office/tk/dom";
import type { Group } from "@/io.ox/office/tk/control/group";

import type { ToolPaneConfig, ToolPaneAutoFitFn, ToolPaneEventMap } from "@/io.ox/office/baseframework/view/pane/toolpane";
import { ToolPane } from "@/io.ox/office/baseframework/view/pane/toolpane";
import { type ControlOrGroup, type ToolBarYieldControlsOptions, ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import { type CompoundButtonConfig, CompoundButton, CompoundSplitButton } from "@/io.ox/office/baseframework/view/basecontrols";
import type { BaseView } from "@/io.ox/office/baseframework/view/baseview";

import "@/io.ox/office/baseframework/view/pane/dynamicpane.less";

// types ======================================================================

/**
 * Configuration options for the class `DynamicPane`.
 */
export interface DynamicPaneConfig extends ToolPaneConfig { }

/**
 * Options for a dropdown menu button with a toolbar shown in shrunken toolpane
 * mode.
 */
export interface ShrinkToMenuOptions extends CompoundButtonConfig {

    /**
     * The key of the controller item to be bound to the dropdown button.
     */
    itemKey?: string;

    /**
     * If set to `true`, an instance of the class `CompoundSplitButton` will be
     * created instead of `CompoundButton`. Default value is `false`.
     */
    splitButton?: boolean;
}

/**
 * Optional parameters for registering a dynamic toolbar.
 */
export interface DynamicToolBarOptions {

    /**
     * The keys of the controller items that control the visibility of the
     * toolbar. The visibility will be bound to the "enabled" state of the
     * respective controller items. If set to multiple keys, all controller
     * items must be enabled. If a key starts with an exclamation mark, the
     * enabled state will be reversed. If omitted, the toolbar will always be
     * visible.
     */
    visibleKey?: string | string[];

    /**
     * An object with all settings needed to prepare a dropdown menu button. If
     * there is not enough free space for the new toolbar, all its controls
     * will be moved into the dropdown menu, and it will be shown instead of
     * the toolbar. The value of this option is an object that contains the
     * initialization options to be passed to the constructor of the classes
     * `CompoundButton` or `CompoundSplitButton`.
     */
    shrinkToMenu?: ShrinkToMenuOptions;

    /**
     * If set to `true`, the controls in the toolbar will be hidden preferably,
     * if there is not enough space after shrinking controls, and after moving
     * toolbars into dropdown menus. Default value is `false`.
     */
    shrinkHide?: boolean;
}

/**
 * Options for creating an iterator for the toolbars in a dynamic toolpane.
 */
export interface DynamicPaneIteratorOptions extends ToolBarYieldControlsOptions {

    /**
     * A filter predicate for selecting specific toolbars to be visited.
     *
     * @param wrapper
     *  The wrapper of a dynamic toolbar about to be visited.
     */
    filter?(wrapper: DynamicToolBarWrapper): boolean;
}

/**
 * Type mapping for the events emitted by `DynamicPane` instances.
 */
export interface DynamicPaneEventMap extends ToolPaneEventMap { }

// class DynamicToolBarWrapper ================================================

/**
 * Represents a toolbar inserted into a dynamic toolpane. Wraps the toolbar
 * itself, and an additional toolbar containing a dropdown button for shrunken
 * mode.
 */
export class DynamicToolBarWrapper {

    readonly docView: BaseView;

    readonly toolPane: DynamicPane;
    readonly toolBar: ToolBar;
    readonly config: DynamicToolBarOptions;

    /** The toolbar containing the shrink dropdown button. */
    readonly shrinkToolBar: Opt<ToolBar>;
    /** The dropdown button control (may be a split button). */
    readonly shrinkButton: Opt<CompoundButton<BaseView> | CompoundSplitButton<BaseView, void>>;
    /** The original toolbar from the dropdown menu. */
    menuToolBar: Opt<ToolBar>;

    /** Controller keys for bound visibility of the toolbar. */
    readonly #visibleKeys: readonly string[];
    /** Whether this toolbar is in shrunken mode. */
    #shrinkMode = false;

    // constructor ------------------------------------------------------------

    constructor(toolPane: DynamicPane, toolBar: ToolBar, config?: DynamicToolBarOptions) {

        // context access properties
        this.docView = toolPane.docView;
        this.toolPane = toolPane;
        this.toolBar = toolBar;
        this.config = { ...toolBar.config, ...config };

        // configuration properties
        this.#visibleKeys = ary.wrap(this.config.visibleKey);

        // set the toolbar to hidden state initially
        toolBar.hide();
        toolPane.addToolBar(toolBar);

        // create another toolbar with a single dropdown button for shrunken mode
        const shrinkConfig = this.config.shrinkToMenu;
        if (shrinkConfig) {
            this.shrinkToolBar = toolPane.addToolBar(new ToolBar(this.docView));
            this.shrinkToolBar.hide();
            this.shrinkButton = shrinkConfig.splitButton ? new CompoundSplitButton(this.docView, shrinkConfig) : new CompoundButton(this.docView, shrinkConfig);
            this.shrinkToolBar.addControl(shrinkConfig.itemKey ?? null, this.shrinkButton);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the controls of this toolbar will be hidden preferredly
     * against other toolbars, when there is not enough space available.
     */
    isHiddenPreferred(): boolean {
        return !!this.config.shrinkHide;
    }

    /**
     * Creates an iterator that visits all form controls in the toolbar.
     */
    yieldControls(options: ToolBarYieldControlsOptions & { groups: true }): IterableIterator<Group>;
    yieldControls(options?: ToolBarYieldControlsOptions): IterableIterator<ControlOrGroup>;
    // implementation
    *yieldControls(options?: ToolBarYieldControlsOptions): IterableIterator<ControlOrGroup> {
        if (!this.#shrinkMode) {
            yield* this.toolBar.yieldControls(options);
        } else if (this.shrinkToolBar) {
            yield* this.shrinkToolBar.yieldControls(options);
        }
    }

    /**
     * Updates the visibility of the wrapped toolbar, and its associated
     * shrunken toolbar.
     */
    updateVisibility(state: boolean): void {
        const visible = state && this.#visibleKeys.every(key => this.docView.isControllerItemEnabled(key));
        this.toolBar.toggle(visible);
        this.shrinkToolBar?.toggle(visible && this.#shrinkMode);
    }

    /**
     * Toggles visibility of regular toolbar contents, or the dropdown button
     * for shrunken mode.
     *
     * @param shrink
     *  Whether to show the dropdown button (`true`), or the regular toolbar
     *  contents (`false`).
     */
    toggleShrinkMode(shrink: boolean): void {

        // nothing to do, if shrink state does not change
        if (!this.shrinkButton || (this.#shrinkMode === shrink)) { return; }
        this.#shrinkMode = shrink;

        // the dropdown menu element taking the toolbar
        const shrinkMenu = this.shrinkButton.menu;

        // initialize all controls in the toolpane
        for (const control of this.toolBar.yieldControls({ groups: true })) {
            // toggle shrink mode of form controls (disabled in dropdown, enabled in toolbar)
            control.toggleShrinkMode(!shrink);
            // initialize control for display in dropdown menus
            control.toggleDropDownMode(shrink);
        }

        // move toolbar into dropdown menu, or back into parent toolpane
        if (shrink) {
            this.toolPane.removeToolBar(this.toolBar);
            this.menuToolBar = shrinkMenu.replaceToolBar(this.toolBar);
        } else {
            shrinkMenu.replaceToolBar(this.menuToolBar!);
            this.toolPane.addToolBar(this.toolBar, { insertBefore: this.shrinkToolBar });
        }

        // update the visibility of the wrapped toolbars
        this.updateVisibility(true);
    }
}

// class DynamicPane ==========================================================

/**
 * Represents a dynamic toolpane in OX Document applications.
 *
 * Dynamic toolpanes support binding the visibility of toolbars to controller
 * items, and to shrink and expand the toolbar contents automatically according
 * to the available space in the toolpane.
 */
export class DynamicPane<
    DocViewT extends BaseView = BaseView,
    ConfT extends DynamicPaneConfig = DynamicPaneConfig,
    EvtMapT extends DynamicPaneEventMap = DynamicPaneEventMap
> extends ToolPane<DocViewT, ConfT, EvtMapT> {

    /** The registry entries for all toolbars. */
    readonly #toolBarRegistry: DynamicToolBarWrapper[] = [];

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view instance containing this toolpane.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(docView: DocViewT, config?: NoInfer<ConfT>) {

        // base constructor
        super(docView, config);

        // add CSS marker class for styling
        this.el.classList.add("dynamic-pane");

        // handle changed controller items
        this.listenTo(docView, "controller:update", this.#updateToolBarVisibility);

        // update toolbar visibility when showing the toolpane
        this.on("pane:show", this.#updateToolBarVisibility);

        // initialize all autofit steps for changed window size
        this.#initAutoFitMode();
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts a new dynamic toolbar into this toolpane. The visibility of
     * dynamic toolbars can be bound to controller items, and their contents
     * will be fit into the available space automatically.
     *
     * @param toolBar
     *  The toolbar to be added to this toolpane. The toolpane takes ownership
     *  of the toolbar!
     *
     * @param [options]
     *  Optional parameters. All of the supported options may have been passed
     *  to the constructor of the toolbar before instead, and will be used from
     *  there in this case. Options passed with this parameter will override
     *  the toolbar options though.
     *
     * @returns
     *  The toolbar instance passed to this method, for convenience.
     */
    addDynamicToolBar<ToolBarT extends ToolBar<any>>(toolBar: ToolBarT, options?: DynamicToolBarOptions): ToolBarT {
        // `DynamicToolBarWrapper` constructor inserts the toolbar into this toolpane
        this.#toolBarRegistry.push(new DynamicToolBarWrapper(this, toolBar, options));
        return toolBar;
    }

    // protected methods ------------------------------------------------------

    /**
     * Creates an iterator that visits all dynamic toolbar wrappers, and skips
     * the static toolbars.
     *
     * @yields
     *  The dynamic toolbar wrappers.
     */
    protected *yieldDynamicWrappers(options?: DynamicPaneIteratorOptions): IterableIterator<DynamicToolBarWrapper> {
        const visible = options?.visible;
        const filter = options?.filter;
        for (const wrapper of ary.values(this.#toolBarRegistry, options)) {
            if ((visible === undefined) || (visible === wrapper.toolBar.isVisible())) {
                if (!filter || filter(wrapper)) {
                    yield wrapper;
                }
            }
        }
    }

    /**
     * Creates an iterator that visits all form controls in dynamic toolbars.
     */
    protected yieldDynamicControls(options: DynamicPaneIteratorOptions & { groups: true }): IterableIterator<Group>;
    protected yieldDynamicControls(options?: DynamicPaneIteratorOptions): IterableIterator<ControlOrGroup>;
    // implementation
    protected *yieldDynamicControls(options?: DynamicPaneIteratorOptions): IterableIterator<ControlOrGroup> {
        for (const wrapper of this.yieldDynamicWrappers(options)) {
            yield* wrapper.yieldControls(options);
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Updates the visibility of all toolbars in this toolpane, according to
     * controller states and other configuration.
     */
    #updateToolBarVisibility(): void {
        for (const wrapper of this.yieldDynamicWrappers()) {
            wrapper.updateVisibility(true);
        }
    }

    /**
     * Creates an iterator that visits the values of a data source, until the
     * requested oversized state of the toolpane is reached.
     *
     * @param iterable
     *  The data source providing the values to be processed.
     *
     * @param shrink
     *  Whether to wait for toolpane contents to fit into the own root node
     *  (`true`, i.e. shrink mode), or to wait for toolpane contents to
     *  overflow (`false`, i.e. expand mode).
     *
     * @yields
     *  The values of the data source.
     */
    *#yieldForAutoFitMode<VT>(iterable: Iterable<VT>, shrink: boolean): IterableIterator<VT> {
        for (const value of iterable) {
            // exit loop if desired oversize state is reached (oversized when expanding, fitting when shrinking)
            if (shrink !== this.isContentOversized()) { break; }
            yield value;
        }
    }

    /**
     * Registers an autofit step handler with support for flexible form
     * controls.
     *
     * @param minRelWidth
     *  The minimum relative width to shrink flexible form controls to.
     *
     * @param maxRelWidth
     *  The maximum relative width to expand flexible form controls to.
     *
     * @param autoFitFn
     *  The autofit handler function to be registered.
     */
    #addFlexAutoFitStep(minRelWidth: number, maxRelWidth: number, autoFitFn: ToolPaneAutoFitFn): void {

        this.addAutoFitStep(shrink => {

            // find all flexible controls in the toolpane
            let controls = Array.from(this.yieldDynamicControls({ groups: true })).filter(control => control.hasFlexWidth());

            // first, shrink all flexible controls to the specified minimum width
            controls.forEach(control => control.setFlexWidth(minRelWidth));

            // invoke the actual autofit handler while flexible controls are shrunken
            autoFitFn.call(this, shrink);

            // expand mode: expand to maximum width to trigger oversize handling
            if (!shrink) {
                controls.forEach(control => control.setFlexWidth(maxRelWidth));
                return;
            }

            // shrink mode: exactly expand all *visible* controls to available size in toolpane
            controls = controls.filter(control => control.visible);
            if (!controls.length) { return; }

            // enlarge all controls to maximum width (needed to find the total excess width and relative shrink ratio)
            controls.forEach(control => control.setFlexWidth(1));
            const excessWidth = this.getContentExcessSize();
            if (!excessWidth) { return; }

            // collect minimum/maximum width of all flexible controls, in pixels
            let shrinkWidth = 0;
            controls.forEach(control => {
                const maxWidth = convertCssLength(control.config.width!, "px");
                const minWidth = convertCssLength(control.config.shrinkWidth!, "px");
                shrinkWidth += maxWidth - minWidth;
            });

            // determine the relative flexible width according to excess size
            const relWidth = Math.max(minRelWidth, 1 - excessWidth / shrinkWidth);
            controls.forEach(control => control.setFlexWidth(relWidth));
        });
    }

    /**
     * Initializes all autofit handlers for changed window size.
     */
    #initAutoFitMode(): void {

        // step 1: distance mode for reduced padding/distances
        this.addAutoFitDistanceSteps();

        // step 2: toggle shrink mode of all form controls
        this.#addFlexAutoFitStep(0.5, 1, shrink => {
            // shrink from last to first control, but expand from first to last
            const controls = this.yieldDynamicControls({ groups: true, reverse: shrink });
            for (const control of this.#yieldForAutoFitMode(controls, shrink)) { control.toggleShrinkMode(shrink); }
        });

        // step 3: toggle dropdown menu mode of toolbars
        this.#addFlexAutoFitStep(0, 0.5, shrink => {
            // shrink from last to first toolbar, but expand from first to last
            const wrappers = this.yieldDynamicWrappers({ reverse: shrink, filter: wrapper => !!wrapper.shrinkToolBar });
            for (const wrapper of this.#yieldForAutoFitMode(wrappers, shrink)) { wrapper.toggleShrinkMode(shrink); }
        });

        // step 4: toggle hidden mode of trailing controls in toolbars (prefer toolbars with "shrinkHide" flag)
        this.#addFlexAutoFitStep(0, 0.5, shrink => {
            // shrink from last to first control, but expand from first to last
            const controls = this.yieldDynamicControls({ reverse: shrink, filter: wrapper => wrapper.isHiddenPreferred() });
            for (const control of this.#yieldForAutoFitMode(controls, shrink)) { control.toggle(!shrink, "toolbar:shrink"); }
        });

        // step 5: toggle hidden mode of trailing controls in other toolbars
        this.#addFlexAutoFitStep(0, 0.5, shrink => {
            // shrink from last to first toolbar, but expand from first to last
            const controls = this.yieldDynamicControls({ reverse: shrink, filter: wrapper => !wrapper.isHiddenPreferred() });
            for (const control of this.#yieldForAutoFitMode(controls, shrink)) { control.toggle(!shrink, "toolbar:shrink"); }
        });
    }
}
