/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { math, is } from "@/io.ox/office/tk/algorithms";
import type { CSSBorderPos, ElementBaseOptions, ElementWrapper, RefreshOptions } from "@/io.ox/office/tk/dom";
import { isVerticalPosition, isLeadingPosition, getFloorNodeSize, setElementClasses, createDiv, addDeviceMarkers, isEscapeKey } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { isVisibleNode, showNodes } from "@/io.ox/office/tk/forms";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { layoutLogger } from "@/io.ox/office/tk/utils/logger";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";
import type { BaseView } from "@/io.ox/office/baseframework/view/baseview";

import "@/io.ox/office/baseframework/view/pane/basepane.less";

// types ======================================================================

/**
 * Configuration options for the class `BasePane`.
 */
export interface BasePaneConfig extends ElementBaseOptions {

    /**
     * The border of the application window to attach the view pane to.
     */
    position?: CSSBorderPos;

    /**
     * The size of the view pane, between window border and application pane.
     * If omitted, the size will be determined by the CSS styling of the pane
     * root node.
     */
    size?: number;

    /**
     * Whether the pane will be resizable at its inner border. Default value is
     * `false`.
     */
    resizable?: boolean;

    /**
     * The minimum size of resizable panes (when the option "resizable" is set
     * to `true`). Default value is `1`.
     */
    minSize?: number;

    /**
     * The maximum size of resizable panes (when the option "resizable" is set
     * to `true`). Default value is `Infinity`.
     */
    maxSize?: number;

    /**
     * Whether the view pane will be inserted into the chain of focusable
     * landmark nodes reachable with specific global keyboard shortcuts
     * (usually the F6 key with platform dependent modifier keys). Default
     * value is `true`.
     */
    landmark?: boolean;
}

/**
 * Type mapping for the events emitted by `BasePane` instances.
 */
export interface BasePaneEventMap {

    /**
     * Will be emitted before the view pane will be shown.
     */
    "pane:beforeshow": [];

    /**
     * Will be emitted after the view pane has been shown.
     */
    "pane:show": [];

    /**
     * Will be emitted before the view pane will be hidden.
     */
    "pane:beforehide": [];

    /**
     * Will be emitted after the view pane has been hidden.
     */
    "pane:hide": [];

    /**
     * Will be emitted after the size of the root node has been changed. This
     * event will trigger multiple times during a tracking sequence, see event
     * "pane:resize:tracking:end" that triggers only once after a tracking
     * sequence has finished.
     *
     * @param width
     *  The new width of the root node, in pixels.
     *
     * @param height
     *  The new height of the root node, in pixels.
     */
    "pane:resize": [width: number, height: number];

    /**
     * Will be emitted after the size of the root node has been changed by a
     * tracking sequence on the resizer node. In difference to the event
     * "pane:resize", this event will only be emitted once after a complete
     * tracking sequence, but not if the pane size has been changed somehow
     * else.
     *
     * @param width
     *  The new width of the root node, in pixels.
     *
     * @param height
     *  The new height of the root node, in pixels.
     */
    "pane:resize:tracking:end": [width: number, height: number];

    /**
     * Will be emitted after the view pane has been refreshed completely.
     */
    "pane:refreshed": [];
}

// class BasePane =============================================================

/**
 * Represents a container element attached to a specific border of the
 * application window.
 */
export class BasePane<
    DocViewT extends BaseView = BaseView,
    ConfT extends BasePaneConfig = BasePaneConfig,
    EvtMapT extends BasePaneEventMap = BasePaneEventMap
> extends ViewObject<DocViewT, EvtMapT> implements ElementWrapper<HTMLDivElement> {

    /**
     * The root element of this view pane.
     */
    readonly el: HTMLDivElement;

    /**
     * The root element of this view pane, as JQuery object.
     */
    readonly $el: JQuery<HTMLDivElement>;

    /**
     * The configuration options passed to the constructor.
     */
    readonly config: Readonly<ConfT>;

    /**
     * The effective position of this view pane.
     */
    readonly position: CSSBorderPos;

    /**
     * Whether this view pane is vertically aligned ("top" or "bottom").
     */
    readonly vertical: boolean;

    /**
     * Whether this view pane is aligned at the leading window border ("top" or
     * "left").
     */
    readonly leading: boolean;

    // the last cached CSS size of the root node, used to detect layout changes
    #width = 0;
    #height = 0;
    // whether a debounced refresh is pending
    #pendingRefresh = false;

    // workaround for type errors with extensible event maps
    get #evt(): BasePane { return this as BasePane; }

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view instance containing this pane element.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(docView: DocViewT, config?: NoInfer<ConfT>) {

        // base constructor
        super(docView);

        // the container element representing the view pane
        this.el = createDiv("view-pane");
        this.$el = $(this.el);

        // public access to configuration
        this.config = { position: "top", ...config } as unknown as Readonly<ConfT>;
        this.position = this.config.position!;
        this.vertical = isVerticalPosition(this.position);
        this.leading = isLeadingPosition(this.position);

        // set CSS classes and node identifier
        setElementClasses(this.el, config);

        // set position at element for CSS selectors
        this.el.dataset.pos = this.position;
        this.el.dataset.dir = this.vertical ? "row" : "column";

        // marker for touch devices and browser types
        addDeviceMarkers(this.$el[0]);

        // custom fixed size if specified (zero will be ignored)
        this.#initSize(config?.size);

        // initialize tracking for resizable view panes
        if (config?.resizable) {
            this.#initResizer();
        }

        // whether the pane will be a landmark for F6 focus traveling
        this.el.classList.toggle("f6-target", config?.landmark ?? true);

        // keyboard handler
        this.listenTo(this.$el, "keydown", this.#basePaneKeyDownHandler);

        // disable dragging of controls or dropping (otherwise, it is possible to drag buttons and other controls
        // around, or images can be dropped which will be loaded by the browser without any notification)
        this.$el.on("drop dragstart dragenter dragexit dragover dragleave", false);

        // log focus and layout events
        this.logEvents(layoutLogger, ["pane:beforeshow", "pane:show", "pane:beforehide", "pane:hide"]);
        this.logEvents(layoutLogger, "pane:resize", (width, height) => `width=${width} height=${height}`);
    }

    protected override destructor(): void {
        this.$el.remove();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Requests a refresh of this view pane. Checks the current outer size of
     * the root node of this view pane. Triggers a "pane:resize" event if the
     * pane is visible and its size has changed. Does nothing if the pane is
     * currently hidden.
     *
     * @param [options]
     *  Optional parameters.
     */
    refresh(options?: RefreshOptions): void {
        this.#pendingRefresh = true;
        if (options?.immediate) {
            this.#execRefresh();
        } else {
            this.#debouncedRefresh();
        }
    }

    /**
     * Returns whether this view pane is currently visible.
     *
     * @returns
     *  Whether the view pane is currently visible.
     */
    isVisible(): boolean {
        return isVisibleNode(this.el);
    }

    /**
     * Makes this view pane visible.
     */
    show(): void {
        this.toggle(true);
    }

    /**
     * Hides this view pane.
     */
    hide(): void {
        this.toggle(false);
    }

    /**
     * Changes the visibility of this view pane.
     *
     * @param [state]
     *  If specified, shows or hides this view pane independently from its
     *  current visibility state. If omitted, toggles the current visibility
     *  state of this view pane.
     */
    toggle(state?: boolean): void {

        // update node visibility and notify listeners, if really changed
        const oldVisible = this.isVisible();
        const newVisible = (state === true) || ((state !== false) && !oldVisible);
        if (oldVisible !== newVisible) {
            this.#evt.trigger(newVisible ? "pane:beforeshow" : "pane:beforehide");
            showNodes(this.el, newVisible);
            this.#evt.trigger(newVisible ? "pane:show" : "pane:hide");
            this.refresh({ immediate: true });
        }
    }

    /**
     * Get the size of this view pane in the variable direction.
     *
     * @returns
     *  The last cached size of the root node in the variable direction.
     */
    getSize(): number {
        return this.vertical ? this.#height : this.#width;
    }

    /**
     * Changes the size of this view pane in the variable direction.
     *
     * @param size
     *  The new size of the view pane, or `null` to return to automatic size.
     */
    setSize(size: number | null): void {
        this.#initSize(size);
        // "refresh" detects changes of the element size
        this.refresh({ immediate: true });
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses may implement updating the contents of this view pane, after
     * its size or visibility has changed, or after manually requesting a
     * refresh.
     */
    protected implRefresh(): void {
        // nothing to do
    }

    // private methods --------------------------------------------------------

    /**
     * Changes the size of this view pane in the variable direction.
     *
     * @param size
     *  The new size of the view pane, or `null` to return to automatic size.
     */
    #initSize(size: Nullable<number>): void {

        // minimum size of the view pane (for resizable panes)
        const minSize = Math.max(1, this.config.minSize ?? 1);
        // maximum size of the view pane (for resizable panes)
        const maxSize = Math.max(this.config.maxSize ?? Number.POSITIVE_INFINITY, minSize);

        // clamp to configured limits, convert to CSS attribute value
        const value = is.number(size) ? math.clamp(size, minSize, maxSize) : "";
        this.$el.css(this.vertical ? "height" : "width", value);
    }

    /**
     * Initializes resizer element and tracking mode for resizable view panes.
     */
    #initResizer(): void {

        const resizerEl = this.el.appendChild(createDiv("resizer"));
        resizerEl.append(createDiv("resizer-bar"));
        this.el.classList.add("resizable");

        // original size when tracking sequence has started
        let origSize = 0;

        // updates the size of the view pane
        const refreshSize = (offset: number): void => {
            this.#initSize(origSize + offset);
            this.refresh();
        };

        // initialize tracking observer
        const observer = this.member(new TrackingObserver({
            // remember initial size, needed to be restored on "cancel"
            start:   () => { origSize = this.vertical ? this.#height : this.#width; },
            move:    record => refreshSize((this.leading ? 1 : -1) * (this.vertical ? record.offset.y : record.offset.x)),
            cancel:  () => refreshSize(0),
            end:     () => this.#evt.trigger("pane:resize:tracking:end", this.#width, this.#height),
            finally: () => this.docView.grabFocus()
        }));

        // start observing the resizer element
        observer.observe(resizerEl);
    }

    /**
     * Refreshes this view pane synchronously. This is the actual refresh
     * implementation that will be invoked from synchronous and asynchronous
     * refresh requests.
     */
    #execRefresh(): void {

        // nothing to do, if no refresh request is pending (anymore), or in hidden panes
        if (!this.#pendingRefresh || !this.isVisible()) { return; }

        // current size of the DOM root node (bug 57575: round down to fix problems with font scaling in Chrome)
        const { width, height } = getFloorNodeSize(this.el);

        // notify listeners if size has changed
        if ((this.#width !== width) || (this.#height !== height)) {
            this.#width = width;
            this.#height = height;
            this.#evt.trigger("pane:resize", width, height);
        }

        // run the update implementation methods of all subclasses
        this.implRefresh();
        this.#evt.trigger("pane:refreshed");

        // start accepting more refresh requests
        this.#pendingRefresh = false;
    }

    /**
     * Collects multiple refresh requests, and decides whether to actually
     * execute the refresh (depending on whether another synchronous refresh
     * was executed before).
     */
    @debounceMethod({ delay: "animationframe" })
    #debouncedRefresh(): void {
        this.#execRefresh();
    }

    /**
     * Handles keyboard events.
     */
    #basePaneKeyDownHandler(event: JTriggeredEvent): false | void {
        if (isEscapeKey(event)) {
            this.docView.grabFocus();
            return false;
        }
    }
}
