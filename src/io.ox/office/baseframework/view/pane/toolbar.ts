/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { type ReverseOptions, is, ary, map, jpromise } from "@/io.ox/office/tk/algorithms";
import type { ElementWrapper, ElementBaseOptions, RefreshOptions, NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { TOUCH_DEVICE, getFloorNodeSize, setElementClasses, createDiv, setFocusInto } from "@/io.ox/office/tk/dom";
import { Control } from "@/io.ox/office/tk/form";
import { layoutLogger } from "@/io.ox/office/tk/utils/logger";
import { HIDDEN_CLASS, isVisibleNode, showNodes } from "@/io.ox/office/tk/forms";
import { type DObjectConfig, DObject, debounceMethod } from "@/io.ox/office/tk/objects";
import { Group } from "@/io.ox/office/tk/control/group";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";
import type { BaseView } from "@/io.ox/office/baseframework/view/baseview";
import type { DynamicToolBarOptions } from "@/io.ox/office/baseframework/view/pane/dynamicpane";
import type { ItemFocusOptions, ItemExecuteOptions } from "@/io.ox/office/baseframework/controller/controlleritem";

import "@/io.ox/office/baseframework/view/pane/toolbar.less";

// types ======================================================================

/**
 * Configuration options for the class `ToolBar`.
 */
export interface ToolBarConfig extends DObjectConfig, ElementBaseOptions, DynamicToolBarOptions {

    /**
     * If set to `true`, the toolbar will be inserted into the chain of
     * focusable landmark nodes reachable with specific global keyboard
     * shortcuts (usually the F6 key with platform dependent modifier keys).
     * Default value is `false`.
     */
    landmark?: boolean;

    /**
     * If set to `true`, all disabled form controls contained in this toolbar
     * will be hidden automatically. Default value is `false`.
     */
    hideDisabled?: boolean;
}

/**
 * Options for selecting a section container in a toolbar.
 */
export interface ToolBarTargetSectionOptions {

    /**
     * The identifier of the target section, as defined with the method
     * `ToolBar#addSection`. If omitted, the current (last) section will be
     * used.
     */
    section?: string;
}

export interface ToolBarAddContainerOptions {

    /**
     * If set to `true`, and this toolbar is displayed in a vertical layout,
     * the container will display the embedded form controls in a single row
     * instead of stacked on top of each other. Default value is `false`.
     */
    inline?: boolean;

    /**
     * If set to `true`, the container will display the embedded buttons with
     * border lines (only for `inline` mode). Default value is `false`.
     */
    boxed?: boolean;
}

/**
 * Options for creating a new section container in a toolbar.
 */
export interface ToolBarAddSectionOptions extends ToolBarAddContainerOptions {

    /**
     * The title for the new section. If omitted or set to an empty string, the
     * section will not contain a visible title.
     */
    label?: string;
}

export type ControlOrGroup = Control<any> | Group<any>;

export interface ToolBarAddControlOptions extends ToolBarTargetSectionOptions, ItemFocusOptions {

    /**
     * If specified, the visibility of the form control will be bound to the
     * enabled states of the controller items with the keys passed in this
     * option. If the item gets disabled, the form control will be hidden
     * automatically, and vice versa. If set to multiple keys, all controller
     * items must be enabled. If a key starts with an exclamation mark, the
     * enabled state will be reversed.
     */
    visibleKey?: string | string[];

    /**
     * If set to `true`, the form control is still visible while the parent
     * container hides its contents (used e.g. with CSS class "hidden-contents"
     * set by class `ToolPane`). Default value is `false`.
     */
    alwaysVisible?: boolean;

    /**
     * If the control is part of an inline container element (see option
     * "inline" of interface `ToolBarAddContainerOptions`), the control's root
     * node will be set to flex-grow mode, filling the available horizontal
     * space in the inline container. Default value is `false`.
     */
    expandInline?: boolean;
}

/**
 * Options for creating an iterator for the toolbars in a toolpane.
 */
export interface ToolBarYieldControlsOptions extends ReverseOptions {

    /**
     * If set to a boolean, filters for controls of the respective visibility
     * state (`true`: visible controls only, `false`: hidden controls only).
     */
    visible?: boolean;

    /**
     * If set to `true`, only instances of old class `Group` will be visited.
     * By default, all form controls (instances of `Control` and `Group`) will
     * be visited.
     */
    groups?: boolean;
}

/**
 * Type mapping for the events emitted by `ToolBar` instances.
 */
export interface ToolBarEventMap {

    /**
     * Will be emittedbefore the toolbar will be shown.
     */
    "toolbar:beforeshow": [];

    /**
     * Will be emitted after the toolbar has been shown.
     */
    "toolbar:show": [];

    /**
     * Will be emitted before the toolbar will be hidden.
     */
    "toolbar:beforehide": [];

    /**
     * Will be emitted after the toolbar has been hidden.
     */
    "toolbar:hide": [];

    /**
     * Will be emitted after the size of the root node has been changed.
     *
     * @param width
     *  The new width of the root node, in pixels.
     *
     * @param height
     *  The new height of the root node, in pixels.
     */
    "toolbar:resize": [width: number, height: number];

    /**
     * Will be emitted after a form control in this toolbar has been changed.
     */
    "toolbar:change": [];

    /**
     * Will be emitted after a form control in this toolbar has been activated
     * in the GUI (i.e. has triggered a "group:commit" event).
     */
    "toolbar:commit": [key: string | null, value: unknown];

    /**
     * Will be emitted after a form control in this toolbar has triggered a
     * "group:preview" event).
     */
    "toolbar:preview": [key: string | null, value: unknown];
}

// private functions ==========================================================

function hasVisibleChildren(elem: Element): boolean {
    return ary.some(elem.children, node => !node.classList.contains(HIDDEN_CLASS));
}

/**
 * Creates a new container node for form controls.
 */
function createContainerNode(options?: ToolBarAddContainerOptions): HTMLDivElement {
    const classes = { "group-container": true, "inline-groups": options?.inline, "boxed-groups": options?.boxed };
    const containerNode = createDiv({ classes });
    showNodes(containerNode, false);
    return containerNode;
}

// class ControlEntry =========================================================

/**
 * Entry for a form control in a toolbar's control registry.
 */
class ControlEntry extends DObject {

    readonly key: string | null;
    readonly control: ControlOrGroup;
    readonly visibleKeys: readonly string[];

    constructor(key: string | null, control: ControlOrGroup, options?: ToolBarAddControlOptions) {
        super();
        this.key = key;
        this.control = this.member(control);
        this.visibleKeys = ary.wrap(options?.visibleKey);
    }
}

// class ToolBar ==============================================================

/**
 * A toolbar is a container for various form control instances. It can be used
 * in horizontal layouts (e.g. toolpanes), or in vertical layouts (e.g.
 * dropdown menus, context menus, etc.).
 */
export class ToolBar<
    DocViewT extends BaseView = BaseView,
    ConfT extends ToolBarConfig = ToolBarConfig
> extends ViewObject<DocViewT, ToolBarEventMap> implements ElementWrapper<HTMLDivElement> {

    /**
     * The root element of this toolbar.
     */
    readonly el: HTMLDivElement;

    /**
     * The root element of this toolbar, as JQuery object.
     */
    readonly $el: JQuery<HTMLDivElement>;

    /**
     * The configuration options passed to the constructor.
     */
    readonly config: ConfT;

    // all form controls with additional data (has ownership)
    readonly #controlRegistry = this.member(new Map<ControlOrGroup, ControlEntry>());
    // all form controls as flat array, in insertion order
    readonly #controlsArr: ControlOrGroup[] = [];
    // all form controls, as sets mapped by controller key
    readonly #controlsByKey = new Map<string, Set<ControlEntry>>();

    // whether the toolbar is set to visible (regardless of effective visibility)
    #visible = true;
    // the last cached CSS size of the root node, used to detect layout changes
    #width = 0;
    #height = 0;
    // whether a debounced refresh is pending
    #pendingRefresh = false;
    // whether the form controls need to be updated
    #dirtyControls = false;

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view instance containing this toolbar.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(docView: DocViewT, config?: NoInfer<ConfT>) {

        // base constructor
        super(docView, config);

        // the container element representing the toolbar
        this.el = createDiv("toolbar");
        this.$el = $(this.el);

        // public access to configuration
        this.config = { ...config } as unknown as ConfT;

        // ARIA role, marker for touch devices
        this.$el.attr("role", "navigation");
        this.$el.toggleClass("touch", TOUCH_DEVICE);

        // additional classes, landmark for F6 focus traveling
        setElementClasses(this.el, config);
        this.el.classList.toggle("f6-target", !!config?.landmark);

        // initially hidden until the first visible form control will be inserted
        showNodes(this.el, false);

        // update all registered form controls after change events of the application controller
        this.listenTo(docView, "controller:update", changedItems => {
            for (const [key, itemState] of changedItems) {
                this.#updateControlState(key, itemState.enabled, itemState.value);
            }
            // update all controls with bound visibility
            this.#updateControlsVisibility();
        });

        // log focus and layout events
        this.logEvents(layoutLogger, ["toolbar:beforeshow", "toolbar:show", "toolbar:beforehide", "toolbar:hide"]);
        this.logEvents(layoutLogger, "toolbar:resize", (width, height) => `width=${width} height=${height}`);
    }

    protected override destructor(): void {
        this.$el.remove();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Requests a refresh of this view pane. Checks the current outer size of
     * the root node of this view pane. Triggers a "pane:resize" event if the
     * pane is visible and its size has changed. Does nothing if the pane is
     * currently hidden.
     *
     * @param [options]
     *  Optional parameters.
     */
    refresh(options?: RefreshOptions): void {
        this.#pendingRefresh = true;
        if (options?.immediate) {
            this.#execRefresh();
        } else {
            this.#debouncedRefresh();
        }
    }

    /**
     * Returns whether this toolbar is visible.
     */
    isVisible(): boolean {
        return this.#visible && isVisibleNode(this.$el);
    }

    /**
     * Displays this toolbar, if it is currently hidden.
     */
    show(): void {
        this.toggle(true);
    }

    /**
     * Hides this toolbar, if it is currently visible.
     */
    hide(): void {
        this.toggle(false);
    }

    /**
     * Toggles the visibility of this toolbar.
     *
     * @param [state]
     *  If specified, shows or hides the toolbar depending on the value. If
     *  omitted, toggles the current visibility of the toolbar.
     */
    toggle(state?: boolean): void {

        // resolve missing parameter for toggling visibility
        const wasVisible = this.isVisible();
        this.#visible = (state === true) || ((state !== false) && !wasVisible);

        // refresh all form controls
        if (!wasVisible && this.#visible) {
            this.#dirtyControls = true;
        }

        // immediately update layout, this sets the effective visibility
        this.refresh({ immediate: true });
    }

    /**
     * Appends the passed DOM nodes to a section in this toolbar.
     *
     * @param source
     *  The DOM nodes to be inserted into the toolbar.
     *
     * @param [options]
     *  Optional parameters.
     */
    addNodes(source: NodeOrJQuery, options?: ToolBarTargetSectionOptions): void {
        const $container = this.#getContainerNode(options);
        $container.append(source);
        this.#refreshContainerNode($container);
    }

    /**
     * Appends a section with optional header label (vertical layout only) to
     * this toolbar. If the toolbar is not empty anymore, automatically adds a
     * leading separator line (left border of the section, or top border in
     * vertical layout).
     *
     * @param id
     *  The identifier of the section. Can be used to add form controls to this
     *  section later.
     *
     * @param [options]
     *  Optional parameters. Can be set to a simple string to specify the title
     *  of the section (convenience shortcut for the option "label").
     */
    addSection(id: string, options?: ToolBarAddSectionOptions | string): void {
        this.#getSectionNode(id, is.string(options) ? { label: options } : options);
    }

    /**
     * Appends a new container for form controls to this toolbar.
     *
     * @param [options]
     *  Optional parameters.
     */
    addContainer(options?: ToolBarTargetSectionOptions & ToolBarAddContainerOptions): void {
        this.#getSectionNode(options?.section).append(createContainerNode(options));
    }

    /**
     * Adds the passed form control to this toolbar and binds it to the
     * specified controller item.
     *
     * @param key
     *  The key of a controller item associated to the form control. The
     *  enabled state and the current value of the form control will be bound
     *  to the state and value of the controller item. If the form control
     *  triggers a "group:commit" event, the controller item will be executed.
     *  The key can be set to the value null to insert an unbound form control
     *  into this toolbar.
     *
     * @param control
     *  The form control to be inserted.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The form control passed to this method.
     */
    addControl<ControlT extends ControlOrGroup>(key: string | null, control: ControlT, options?: ToolBarAddControlOptions): ControlT {

        // take ownership of the form control
        const entry = new ControlEntry(key, control, options);
        this.#controlRegistry.set(control, entry);
        this.#controlsArr.push(control);

        // get root element of the form control
        const elem = control.render();

        // insert the form control into this toolbar
        const $container = this.#getContainerNode(options);
        $container.append(elem);
        elem.classList.toggle("expand-inline", !!options?.expandInline);

        if (control instanceof Control) {

            // handle visibility state of form controls
            this.listenTo(control as Control, "show", () => this.#refreshContainerNode($container, true));
            this.listenTo(control as Control, "hide", () => this.#refreshContainerNode($container, false));

            // debounced refresh when size of a form control has changed
            this.listenTo(control as Control, "resize", () => this.refresh());

            if (key) {
                layoutLogger.error("$badge{ToolBar} addControl: bound `Control` instances not supported yet");
            }

        } else {

            // handle visibility state of form controls
            this.listenTo(control as Group, "group:show", () => this.#refreshContainerNode($container, true));
            this.listenTo(control as Group, "group:hide", () => this.#refreshContainerNode($container, false));

            // debounced refresh when size of a form control has changed
            this.listenTo(control as Group, "group:resize", () => this.refresh());

            // special handling if the form control has been activated in the GUI
            this.listenTo(control as Group, "group:commit", (value, opts) => {

                if (key) {
                    // execute the controller item
                    const executeOptions: ItemExecuteOptions = { focusTarget: options?.focusTarget, ...opts };
                    jpromise.floating(this.docView.executeControllerItem(key, value, executeOptions));
                } else {
                    // update all form controls after an unbound control has been activated (custom enabled states etc.)
                    this.updateAllControls();
                }

                // notify listeners
                this.trigger("toolbar:commit", key, value);
            });

            // special handling if the form control preview has been activated in the GUI
            this.listenTo(control as Group, "group:preview", value => {

                if (key) {
                    // execute the controller item
                    const executeOptions: ItemExecuteOptions = { focusTarget: options?.focusTarget, preview: true };
                    jpromise.floating(this.docView.executeControllerItem(key, value, executeOptions));
                } else {
                    // update all form controls after an unbound control has been activated (custom enabled states etc.)
                    this.updateAllControls();
                }

                // notify listeners
                this.trigger("toolbar:preview", key, value);
            });

            // special handling if the form control cancels the action (ESCAPE key etc.)
            this.listenTo(control as Group, "group:cancel", opts => {
                const focusOptions: ItemFocusOptions = { focusTarget: options?.focusTarget, ...opts };
                if (key) {
                    this.docApp.docController.cancelItem(key, focusOptions);
                } else {
                    this.docApp.docController.grabFocus(focusOptions);
                    jpromise.floating(this.docApp.docController.update());
                }
            });
        }

        // notify on any changed control value
        control.onChange(() => this.trigger("toolbar:change"));

        // special handling for form controls bound to controller items
        if (key) {
            // store form control reference in internal map
            map.upsert(this.#controlsByKey, key, () => new Set()).add(entry);
            // set the key as DOM data attribute at the root node of the form control
            elem.dataset.key = key;
        }

        // bug 37236: update enabled state of all new form controls, e.g. when initializing
        // a toolbar lazily (e.g. directly before showing a popup menu)
        this.#dirtyControls = true;

        // bug 39986: hide the form control with bound visibility initially, better than showing these
        // controls during application initialization and hiding them afterwards on first controller update
        if (entry.visibleKeys.length && !options?.alwaysVisible) {
            control.hide("toolbar:keys");
        }

        // update internal layout (toolbar visibility and size)
        this.#refreshContainerNode($container, control.visible);

        // return the control for further usage
        return control;
    }

    /**
     * Creates an iterator that visits the form control in this toolbar.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The new form control iterator.
     */
    yieldControls(options: ToolBarYieldControlsOptions & { groups: true }): IterableIterator<Group>;
    yieldControls(options?: ToolBarYieldControlsOptions): IterableIterator<ControlOrGroup>;
    // implementation
    *yieldControls(options?: ToolBarYieldControlsOptions): IterableIterator<ControlOrGroup> {
        const visible = options?.visible;
        const groups = options?.groups;
        for (const control of ary.values(this.#controlsArr, { reverse: options?.reverse })) {
            if ((visible === undefined) || (visible === control.visible)) {
                if (!groups || (control instanceof Group)) {
                    yield control;
                }
            }
        }
    }

    /**
     * Returns whether this toolbar contains visible form controls.
     *
     * @returns
     *  Whether this toolbar contains visible form controls.
     */
    hasVisibleControls(): boolean {
        return this.#controlsArr.some(control => control.visible);
    }

    /**
     * Updates the visibility, enabled state, and value of all form controls in
     * this toolbar.
     */
    updateAllControls(): void {

        // the application controller (DOCS-2048: check validity)
        const controller = this.docApp.docController;
        if (controller.destroyed) { return; }

        // use the "_controlsByKey" map for iteration, to resolve each controller item
        // once, even if multiple form controls have been registered for that item
        for (const key of this.#controlsByKey.keys()) {
            this.#updateControlState(key, controller.isItemEnabled(key), controller.getItemValue(key));
        }

        // update all form controls not bound to a controller item, but with visible keys
        this.#updateControlsVisibility(true);
    }

    /**
     * Removes all form controls from this toolbar, destroys them, and clears
     * the toolbar's root node.
     */
    destroyAllControls(): void {

        // destroy all form controls (they will remove themselves from the DOM)
        map.destroy(this.#controlRegistry);
        this.#controlsArr.length = 0;
        this.#controlsByKey.clear();

        // remove all remaining DOM nodes (separators etc.)
        this.$el.empty();

        // update internal layout (toolbar visibility and size)
        this.refresh();
    }

    /**
     * Returns whether this toolbar contains the form control that is currently
     * focused. Searches in all registered form controls. Returns also `true`,
     * if a DOM element is focused that is related to a form control in this
     * toolbar, but is not a child of the form control's root node, e.g. a
     * popup menu.
     *
     * @returns
     *  Whether a form control of this toolbar currently owns the browser
     *  focus.
     */
    hasFocus(): boolean {
        return this.#controlsArr.some(control => control.focused);
    }

    /**
     * Sets the focus to the first enabled form control in this toolbar, unless
     * the toolbar already contains the browser focus.
     */
    grabFocus(): void {
        if (!this.hasFocus()) { setFocusInto(this.el); }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns or creates the specified section container node.
     */
    #getSectionNode(id?: string, options?: ToolBarAddSectionOptions): JQuery {
        let $section: JQuery = id ? this.$el.children(`[data-section="${id}"]`) : this.$el.children("[data-section]").last();
        if ($section.length === 0) {
            $section = $(`<div data-section="${id || "default"}">`).appendTo(this.el);
            if (options?.label) {
                $section.append($(createDiv({ classes: "header-label", label: options.label })).attr("aria-hidden", "true"));
            }
            $section.append(createContainerNode(options));
            showNodes($section, false);
        }
        return $section;
    }

    #getContainerNode(options?: ToolBarTargetSectionOptions): JQuery {
        return this.#getSectionNode(options?.section).children(".group-container").last();
    }

    #refreshVisibility(hasVisSection?: boolean): void {

        // whether the toolbar is visible and contains at least one visible section
        const effectiveVisible = this.#visible && (hasVisSection ?? hasVisibleChildren(this.el));

        // update node visibility and notify listeners, if really changed
        if (isVisibleNode(this.el) !== effectiveVisible) {
            this.trigger(effectiveVisible ? "toolbar:beforeshow" : "toolbar:beforehide");
            showNodes(this.el, effectiveVisible);
            this.trigger(effectiveVisible ? "toolbar:show" : "toolbar:hide");
        }
    }

    #refreshContainerNode($container: JQuery, hasVisible?: boolean): void {

        // update visibility of the container node
        const isContVisible = hasVisible || hasVisibleChildren($container[0]);
        showNodes($container, isContVisible);

        // update visibility of the parent section node
        const $section = $container.parent();
        const isSectionVisible = isContVisible || $section.children(".group-container").get().some(isVisibleNode);
        showNodes($section, isSectionVisible);

        // refresh node size and visibility (pass `undefined` to force searching for other visible sections)
        this.#refreshVisibility(isSectionVisible || undefined);
        this.refresh();
    }

    /**
     * Shows or hides the passed form control.
     */
    #updateControlVisibility(entry: ControlEntry): void {
        const { hideDisabled } = this.config;
        const { control, visibleKeys } = entry;
        if (hideDisabled || visibleKeys?.length) {
            const controller = this.docApp.docController;
            const visible = (!hideDisabled || control.enabled) && (!visibleKeys || visibleKeys.every(key => controller.isItemEnabled(key)));
            control.toggle(visible, "toolbar:keys");
        }
    }

    /**
     * Updates the visibility, enabled state, and value of all form controls
     * that are bound to the specified controller item.
     *
     * @param key
     *  The identifier of a controller item.
     *
     * @param enabled
     *  Whether the controller item is enabled.
     *
     * @param value
     *  The current value of the controller item.
     */
    #updateControlState(key: string, enabled: boolean, value: unknown): void {

        // update all form controls with the passed key (may be more than one)
        this.#controlsByKey.get(key)?.forEach(entry => {

            // update enabled state and value of the controls
            entry.control.enable(enabled);
            entry.control.setValue(value, { silent: true });

            // show/hide all form controls with bound visibility
            this.#updateControlVisibility(entry);
        });
    }

    /**
     * Updates the visibility of all form controls not bound to a controller
     * item, but with bound visibility (item keys binding enabled state to the
     * control's visibility).
     */
    #updateControlsVisibility(unbound?: boolean): void {
        this.#controlRegistry.forEach(entry => {
            if (!unbound || !entry.key) {
                this.#updateControlVisibility(entry);
            }
        });
    }

    /**
     * Checks the current outer size of the root node of this toolbar. Triggers
     * a "toolbar:resize" event if the toolbar is visible and its size has
     * changed.
     */
    #execRefresh(): void {

        // nothing to do, if no refresh request is pending (anymore)
        if (!this.#pendingRefresh) { return; }

        // update visibility of sections according to visible controls
        const hasVisSection = this.$el.children("[data-section]").get().reduce((flag1, sectionNode) => {
            const hasVisContainer = $(sectionNode).children(".group-container").get().reduce((flag2, contNode) => {
                const hasVisChild = hasVisibleChildren(contNode);
                showNodes(contNode, hasVisChild);
                return flag2 || hasVisChild;
            }, false);
            showNodes(sectionNode, hasVisContainer);
            return flag1 || hasVisContainer;
        }, false);

        // refresh visibility of the toolbar itself
        this.#refreshVisibility(hasVisSection);

        // nothing else to do, if the toolbar is hidden, or is inside a hidden container node
        if (this.isVisible()) {

            // current size of the DOM root node
            const { width, height } = getFloorNodeSize(this.el);

            // notify listeners if size has changed
            if ((this.#width !== width) || (this.#height !== height)) {
                this.#width = width;
                this.#height = height;
                this.trigger("toolbar:resize", width, height);
            }

            // update all embedded form controls
            if (this.#dirtyControls && this.isImportFinished()) {
                this.#dirtyControls = false;
                this.updateAllControls();
            }
        }

        // start accepting more refresh requests
        this.#pendingRefresh = false;
    }

    /**
     * Collects multiple refresh requests, and decides whether to actually
     * execute the refresh (depending on whether another synchronous refresh
     * was executed before).
     */
    @debounceMethod({ delay: "animationframe" })
    #debouncedRefresh(): void {
        this.#execRefresh();
    }
}
