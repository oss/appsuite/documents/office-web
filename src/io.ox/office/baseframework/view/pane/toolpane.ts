/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { itr, ary } from "@/io.ox/office/tk/algorithms";
import { createDiv, hasKeyCode, hasModifierKeys, setFocusInto } from "@/io.ox/office/tk/dom";
import { enableCursorFocusNavigation, isVisibleNode, triggerClickForKey } from "@/io.ox/office/tk/forms";
import { onceMethod } from "@/io.ox/office/tk/objects";
import { layoutLogger } from "@/io.ox/office/tk/utils/logger";
import type { Group } from "@/io.ox/office/tk/control/group";

import type { ControlOrGroup, ToolBarYieldControlsOptions, ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import { type BasePaneConfig, type BasePaneEventMap, BasePane } from "@/io.ox/office/baseframework/view/pane/basepane";
import type { BaseView } from "@/io.ox/office/baseframework/view/baseview";

import "@/io.ox/office/baseframework/view/pane/toolpane.less";

// types ======================================================================

/**
 * Configuration options for the class `ToolPane`.
 */
export interface ToolPaneConfig extends BasePaneConfig {

    /**
     * Whether the cursor keys can be used to move the browser focus to the
     * next/previous focusable form control in this toolpane. Default value is
     * `false`.
     */
    cursorNavigate?: boolean;
}

/**
 * Type identifiers for the sections in a toolpane.
 */
export type ToolPaneSectionType = "leading" | "center" | "trailing";

/**
 * Options for inserting a toolbar into a toolpane.
 */
export interface AddToolBarOptions {

    /**
     * A toolbar that is already part of the toolpane. The new toolbar will be
     * inserted before its root node.
     */
    insertBefore?: ToolBar;

    /**
     * The target section container to insert the toolbar into. Has no effect,
     * if the option "insertBefore" has been used. Default value is "center".
     */
    targetSection?: ToolPaneSectionType;
}

/**
 * A callback function to be registered for toolpane autofit mode.
 *
 * @param shrink
 *  Whether to shrink (`true`) or expand (`false`) the toolpane contents.
 */
export type ToolPaneAutoFitFn = (shrink: boolean) => void;

/**
 * Type mapping for the events emitted by `ToolPane` instances.
 */
export interface ToolPaneEventMap extends BasePaneEventMap {

    /**
     * Will be emitted after the toolpane has displayed its contents for the
     * first time.
     */
    "toolpane:showcontents": [];
}

// class ToolPane =============================================================

/**
 * A view pane attached to a border of the application window that acts as
 * container for `ToolBar` instances, and adds specific styling to the form
 * controls contained in its toolbars. Provides a leading, a centered, and a
 * trailing section container (according to the position of the pane) as target
 * nodes for the toolbars.
 *
 * @param docView
 *  The document view instance containing this tool pane.
 *
 * @param [config]
 *  Configuration options.
 */
export class ToolPane<
    DocViewT extends BaseView = BaseView,
    ConfT extends ToolPaneConfig = ToolPaneConfig,
    EvtMapT extends ToolPaneEventMap = ToolPaneEventMap
> extends BasePane<DocViewT, ConfT, EvtMapT> {

    /**
     * The leading (left/top) section container for toolbars.
     */
    protected readonly $leadingSection = $(createDiv("tool-pane-section leading"));

    /**
     * The center section container for toolbars.
     */
    protected readonly $centerSection = $(createDiv("tool-pane-section center"));

    /**
     * The trailing (right/bottom) section container for toolbars.
     */
    protected readonly $trailingSection = $(createDiv("tool-pane-section trailing"));

    // section container nodes, mapped by section type identifier
    readonly #sectionMap: Record<ToolPaneSectionType, JQuery>;
    // all toolbars contained in this toolpane, in insertion order (has ownership)
    readonly #toolBars = this.member(new Set<ToolBar>());
    // all registered autofit callback handlers
    readonly #autoFitSteps: ToolPaneAutoFitFn[] = [];
    // state for autofit mode (array index of current autofit handler)
    #autoFitIdx = 0;

    // workaround for type errors with extensible event maps
    get #evt(): ToolPane { return this as ToolPane; }

    // constructor ------------------------------------------------------------

    constructor(docView: DocViewT, config?: NoInfer<ConfT>) {

        // base constructor
        super(docView, config);

        // add CSS marker class for special formatting; initially hide all contents
        this.el.classList.add("tool-pane", "translucent-constant", "hidden-contents", "borderless-buttons");
        this.el.dataset.distance = "large";

        // insert section container nodes
        this.#sectionMap = { leading: this.$leadingSection, center: this.$centerSection, trailing: this.$trailingSection };
        this.$el.append(this.$leadingSection, this.$centerSection, this.$trailingSection);

        // prevent unwanted scrolling on iOS
        this.$el.on("touchmove", event => { event.preventDefault(); });

        // keyboard handler
        this.listenTo(this.$el, "keydown", this.#toolPaneKeyDownHandler);
        if (config?.cursorNavigate) {
            enableCursorFocusNavigation(this.$el);
        }

        // show pane contents after import
        this.waitForImport(this.#showToolPaneContents);

        // show pane contents during import in preview state
        // @ts-expect-error -- FIXME: the "docs:state:*" events are defined in `EditApplication`
        this.listenTo(this.docApp, "docs:state:preview", this.#showToolPaneContents);
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds the passed toolbar into this toolpane.
     *
     * @param toolBar
     *  The toolbar to be added to this toolpane. The toolpane takes ownership
     *  of the toolbar!
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The toolbar passed to this method, for convenience.
     */
    addToolBar<ToolBarT extends ToolBar>(toolBar: ToolBarT, options?: AddToolBarOptions): ToolBarT {

        layoutLogger.log(`$badge{${this.constructor.name}} addToolBar: adding $badge{${toolBar.constructor.name}}`);

        // store toolbar reference in registry
        this.#toolBars.add(toolBar);

        // insert the root node of the toolbar into this view pane
        if (options?.insertBefore) {
            // insert before specified target
            options.insertBefore.$el.before(toolBar.el);
        } else {
            // insert into specified section container
            const sectionType = options?.targetSection || "center";
            const $section = this.#sectionMap[sectionType] || this.$centerSection;
            $section.append(toolBar.el);
        }

        // handle and forward toolbar events
        this.listenTo(toolBar, ["toolbar:show", "toolbar:hide", "toolbar:resize"], () => this.refresh());

        // update internal layout (pane visibility and size)
        if (toolBar.isVisible()) { this.refresh(); }

        return toolBar;
    }

    /**
     * Removes the passed toolbar from this view pane.
     *
     * @param toolBar
     *  The toolbar to be removed from this pane. Nothing will happen, if the
     *  toolbar has not been added to this view pane before using the method
     *  `addToolBar`.
     *
     * @returns
     *  The toolbar passed to this method.
     */
    removeToolBar<ToolBarT extends ToolBar>(toolBar: ToolBarT): ToolBarT {

        layoutLogger.log(`$badge{${this.constructor.name}} removeToolBar: removing $badge{${toolBar.constructor.name}}`);

        // unregister listeners and remove DOM node if the toolbar was part of this pane
        if (this.#toolBars.delete(toolBar)) {
            // stop listening to the toolbar
            this.stopListeningTo(toolBar);
            // remove the root node of the toolbar from this pane
            toolBar.$el.detach();
            // update internal layout (pane visibility and size)
            this.refresh();
        }

        return toolBar;
    }

    /**
     * Returns whether the passed toolbar has already been inserted into this
     * view pane.
     *
     * @param toolBar
     *  The toolbar to be checked.
     *
     * @returns
     *  Whether the toolbar has already been inserted into this view pane.
     */
    hasToolBar(toolBar: ToolBar): boolean {
        return this.#toolBars.has(toolBar);
    }

    /**
     * Creates an iterator that visits the toolbars in this toolpane.
     *
     * @param [options]
     *  Optional parameters. The options regarding form controls ("reverse" and
     *  "visible") apply to toolbars as well.
     *
     * @yields
     *  The toolbars.
     */
    *yieldToolBars(options?: ToolBarYieldControlsOptions): IterableIterator<ToolBar> {
        const visible = options?.visible;
        for (const toolBar of this.#toolBars) {
            if ((visible === undefined) || (visible === toolBar.isVisible())) {
                yield toolBar;
            }
        }
    }

    /**
     * Creates an iterator that visits the form controls in this toolpane.
     *
     * @param [options]
     *  Optional parameters. The options regarding form controls ("reverse" and
     *  "visible") apply to toolbars as well.
     *
     * @yields
     *  The form controls in this toolpane.
     */
    yieldControls(options: ToolBarYieldControlsOptions & { groups: true }): IterableIterator<Group>;
    yieldControls(options?: ToolBarYieldControlsOptions): IterableIterator<ControlOrGroup>;
    // implementation
    *yieldControls(options?: ToolBarYieldControlsOptions): IterableIterator<ControlOrGroup> {
        for (const toolBar of this.yieldToolBars(options)) {
            yield* toolBar.yieldControls(options);
        }
    }

    /**
     * Returns whether this toolpane contains a form control that is currently
     * focused. Searches in all form controls of all registered toolbars.
     * Returns also `true`, if a DOM element is focused that is related to a
     * form control in this toolpane, but is not a child of the control's root
     * node, e.g. a popup menu.
     *
     * @returns
     *  Whether a form control of this toolpane currently owns the browser
     *  focus.
     */
    hasFocus(): boolean {
        return itr.some(this.#toolBars, toolBar => toolBar.hasFocus());
    }

    /**
     * Sets the focus to the first enabled form control in this toolpane,
     * unless it contains the browser focus already.
     */
    grabFocus(): void {
        if (!this.hasFocus()) { setFocusInto(this.$el); }
    }

    // protected methods ------------------------------------------------------

    /**
     * Registers a new callback handler for toolpane autofit mode.
     *
     * If this toolpane detects excess space for its contents, it will invoke
     * the registered callback handlers with the value `true` in *reversed*
     * order of registration until the contents occupy as much space as
     * possible.
     *
     * If this toolpane detects oversized contents, it will invoke the
     * registered callback handlers with the value `false` in order of
     * registration until the contents have shrunken enough to fit into.
     *
     * @param autoFitFn
     *  The callback function to be registered for autofit mode.
     */
    protected addAutoFitStep(autoFitFn: ToolPaneAutoFitFn): void {
        this.#autoFitSteps.push(autoFitFn);
    }

    /**
     * Adds autofit handlers that modify the distance and padding for control
     * containers and sections.
     */
    protected addAutoFitDistanceSteps(): void {

        // step 1: "medium" distance mode for reduced padding/distances
        this.addAutoFitStep(shrink => {
            this.el.dataset.distance = shrink ? "medium" : "large";
        });

        // step 2: "small" distance mode for reduced padding/distances
        this.addAutoFitStep(shrink => {
            this.el.dataset.distance = shrink ? "small" : "medium";
        });
    }

    /**
     * Runs all autofit handlers to fully expand the contents, and resets the
     * internal state of the autofit algorithm.
     */
    protected resetAutoFitState(): void {
        layoutLogger.takeTime(`$badge{${this.constructor.name}} resetAutoFitState: processing autofit handlers`, () => {
            ary.forEach(this.#autoFitSteps, autoFitFn => autoFitFn.call(this, false), { reverse: true });
            this.#autoFitIdx = 0;
        });
    }

    /**
     * Returns the size the section container nodes exceed the pane area.
     *
     * @returns
     *  The size the section container nodes exceed the pane area, in pixels.
     */
    protected getContentExcessSize(): number {
        const clientSize = this.vertical ? this.el.clientWidth : this.el.clientHeight;
        const scrollSize = this.vertical ? this.el.scrollWidth : this.el.scrollHeight;
        return Math.max(0, scrollSize - clientSize);
    }

    /**
     * Returns whether the section container nodes do not fit into the pane
     * area.
     *
     * @returns
     *  Whether the section container nodes do not fit into the pane area.
     */
    protected isContentOversized(): boolean {
        return this.getContentExcessSize() > 0;
    }

    /**
     * Processes all registered autofit steps.
     */
    protected override implRefresh(): void {
        super.implRefresh();

        // nothing to do, if no autofit steps registered
        const length = this.#autoFitSteps.length;
        if (length === 0) { return; }

        layoutLogger.takeTime(`$badge{${this.constructor.name}} implRefresh: processing autofit handlers`, () => {

            // expand contents as long as sections do not become oversized
            if (!this.isContentOversized()) {
                // run current (last executed) handler again if needed until contents are oversized
                while (this.#autoFitIdx >= 0) {
                    this.#autoFitSteps[this.#autoFitIdx].call(this, false);
                    // stay at current handler that made the contents oversized
                    if (this.isContentOversized()) { break; }
                    this.#autoFitIdx -= 1;
                }
                // keep array index valid
                this.#autoFitIdx = Math.max(0, this.#autoFitIdx);
            }

            // shrink contents while sections are oversized
            if (this.isContentOversized()) {
                while (this.#autoFitIdx < length) {
                    this.#autoFitSteps[this.#autoFitIdx].call(this, true);
                    // stay at current handler that made the contents fitting
                    if (!this.isContentOversized()) { break; }
                    this.#autoFitIdx += 1;
                }
                // keep array index valid
                this.#autoFitIdx = Math.min(length - 1, this.#autoFitIdx);
            }
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Removes the CSS marker class that hides the pane contents during import.
     */
    @onceMethod
    #showToolPaneContents(): void {
        this.setTimeout(() => {
            this.el.classList.remove("hidden-contents");
            this.#evt.trigger("toolpane:showcontents");
        }, 250); // DOCS-962: reduced from 1500ms
    }

    /**
     * Handles keyboard events.
     */
    #toolPaneKeyDownHandler(event: JTriggeredEvent): false | void {

        // shortcut in toolpanes to open dropdown menus with simple UP/DOWN cursor key
        const $menuButton = $(event.target).closest(".group.dropdown-group", this.el).find(".btn.caret-button");
        if (($menuButton.length === 1) && isVisibleNode($menuButton) && !hasModifierKeys(event)) {
            if (hasKeyCode(event, "UP_ARROW", "DOWN_ARROW")) {
                event.target = $menuButton[0];
                triggerClickForKey(event);
                return false;
            }
        }
    }
}
