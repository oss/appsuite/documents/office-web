/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { DObjectConfig } from "@/io.ox/office/tk/objects";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import type { BaseView } from "@/io.ox/office/baseframework/view/baseview";

// class ViewObject ===========================================================

/**
 * A generic base class for view classes. Subclasses `ModelClass` (which adds
 * the public properties `docApp` and `docModel`), and adds the public property
 * `docView` to this instance.
 *
 * @param docView
 *  The document view containing this view object.
 */
export class ViewObject<
    DocViewT extends BaseView,
    EvtMapT = Empty
> extends ModelObject<DocViewT["docModel"], EvtMapT> {

    readonly docView: DocViewT;

    public constructor(docView: DocViewT, config?: DObjectConfig) {
        super(docView.docModel, config);
        this.docView = docView;
    }
}
