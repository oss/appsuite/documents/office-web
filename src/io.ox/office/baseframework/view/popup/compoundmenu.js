/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { CANCEL_LABEL, enableElement, createButton } from "@/io.ox/office/tk/dom";
import { enableCursorFocusNavigation } from "@/io.ox/office/tk/forms";
import { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";

import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";

import "@/io.ox/office/baseframework/view/popup/compoundmenu.less";

// class CompoundMenu =========================================================

/**
 * A popup menu element that contains multiple independent form controls bound
 * to different controller items. Internally, an instance of class `ToolBar` is
 * used to store the form controls.
 */
export class CompoundMenu extends BaseMenu {

    /** The toolbar in the dropdown menu for dynamic contents. */
    #toolBar/*: ToolBar*/;

    /** The action buttons shown in the footer. */
    #actionButtons = [];

    // constructor ------------------------------------------------------------

    /**
     * @param {BaseView} docView
     *  The document view instance containing this menu.
     *
     * @param {Object} [config]
     *  Optional parameters. Supports all options of the base class `BaseMenu`.
     *  Additionally, the following options are supported:
     *  - {boolean} [config.hideDisabled=false]
     *    If set to `true`, all disabled form controls contained in this menu
     *    will be hidden automatically.
     *  - {string|boolean} [config.cancelButton]
     *    If set to `true` or a string, a Cancel button will be shown in the
     *    footer area of this menu. A string will be used as button label, the
     *    value `true` will cause to show the translated label "Cancel".
     */
    constructor(docView, config) {

        // base constructor
        super(config);

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // create the toolbar in the dropdown menu for dynamic contents
        this.#toolBar = new ToolBar(docView, { hideDisabled: config?.hideDisabled, _kind: "private" });

        // CSS marker classes
        this.$el.addClass("compound-menu");
        // enable vertical borderless layout in the embedded toolbar
        this.$body.addClass("borderless-buttons").attr("data-dir", "column");

        // create the cancel button
        if (config?.cancelButton) {
            const cancelLabel = (config.cancelButton === true) ? CANCEL_LABEL : config.cancelButton;
            this.#createFooterButton({
                action: "cancel",
                label: cancelLabel,
                click: () => {
                    this.hide();
                    docView.grabFocus();
                }
            });
        }

        // insert the toolbar into the dropdown menu
        this.#registerToolBar(this.#toolBar);

        // update embedded toolbar (visibility of containers etc.) before positioning the menu
        this.on("popup:beforelayout", () => this.#toolBar.refresh({ immediate: true }));

        // update visibility of all controls after dropdown menu repaint
        this.on("menu:afterrepaint", () => this.#toolBar.updateAllControls());

        // update all action buttons after showing the menu
        this.on("popup:show popup:busy popup:idle", this.#updateActionButtons);

        // hide popup menu when application is not active
        this.listenTo(docView.docApp.getWindow(), "hide", () => this.hide());

        // additional cursor key shortcuts for focus navigation
        enableCursorFocusNavigation(this.$el, { homeEnd: true });
    }

    /*protected override*/ destructor() {
        this.#toolBar.destroy();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Appends the passed DOM nodes to the popup menu.
     *
     * @param {NodeSource} source
     *  The DOM nodes to be inserted into the popup menu.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The identifier of the target section (as defined with the method
     *    `addSection()`). If omitted, the current (last) section will be used.
     */
    addNodes(source, options) {
        this.#toolBar.addNodes(source, options);
    }

    /**
     * Appends a header label to the popup menu. If the menu is not empty
     * anymore, automatically adds a separator line above the header label.
     *
     * @param {string} id
     *  The identifier of the section. Can be used to add form controls to this
     *  section later.
     *
     * @param {object|string} [options]
     *  Optional parameters:
     *  - {string} [options.label]
     *    The label text. If omitted or set to an empty string, a section
     *    without a visible label will be created.
     */
    addSection(id, options) {
        this.#toolBar.addSection(id, options);
    }

    /**
     * Appends a new container for form controls to the popup menu.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The identifier of the target section (as defined with the method
     *    `addSection()`). If omitted, the current (last) section will be used.
     *  - {boolean} [options.inline=false]
     *    If set to `true`, the container will display the embedded form
     *    controls in a single row instead of stacked on top of each other.
     *  - {boolean} [options.boxed=false]
     *    If set to `true`, the container will display the embedded buttons
     *    with border lines (only for `inline` mode).
     */
    addContainer(options) {
        this.#toolBar.addContainer(options);
    }

    /**
     * Appends the passed form control to the popup menu and binds it to the
     * specified controller item.
     *
     * @param {string|null} key
     *  The key of a controller item to bind the form control to. The enabled
     *  state and the current value of the form control will be bound to the
     *  state and value of the controller item. If the form control triggers a
     *  "group:commit" event, the controller item will be executed. The key can
     *  be set to the value `null` to insert an unbound form control into this
     *  popup menu.
     *
     * @param {ControlT extends ControlOrGroup} control
     *  The form control to be inserted into the popup menu.
     *
     * @param {object} [options]
     *  Optional parameters. Supports all options supported by the method
     *  `ToolBar#addControl`, and the following additional options:
     *  - {boolean} [options.sticky=false]
     *    If set to `true`, this popup menu will not be closed after the
     *    inserted form control has been activated.
     *  - {string} [options.section]
     *    The identifier of the target section (as defined with the method
     *    `addSection`). If omitted, the current (last) section will be used.
     *
     * @returns {ControlT}
     *  The form control passed to this method.
     */
    addControl(key, control, options) {

        // set the root node of the embedded toolbar as focus target node for sticky form controls
        if (options?.sticky) {
            options = { focusTarget: this.$body, ...options };
            delete options.sticky;
        }

        // insert the passed form control
        this.#toolBar.addControl(key, control, options);

        // prevent auto-closing this menu when using embedded dropdown menus
        this.#registerSubMenu(control);

        // return the control for further usage
        return control;
    }

    /**
     * Inserts a new action button in the footer section of this menu.
     *
     * @param {object} options
     *  Optional parameters. Supports all options supported by the function
     *  `createButton()`, and the following options:
     *  - {FuncType<boolean>} [options.enabled]
     *    A predicate function that can be used to control the enabled state of
     *    the action button.
     *
     * @returns {JQuery}
     *  The button element, as JQuery object.
     */
    addActionButton(options) {
        const $button = this.#createFooterButton({ type: "primary", ...options });
        this.#actionButtons.push({ $button, options });
        return $button;
    }

    /**
     * Returns whether the toolbar contained in this popup menu contains
     * visible form controls.
     *
     * @returns {boolean}
     *  Whether this popup menu contains visible form controls.
     */
    hasVisibleControls() {
        return this.#toolBar.hasVisibleControls();
    }

    /**
     * Removes and destroys all form controls from this popup menu, and
     * completely clears its root node.
     */
    destroyAllControls() {
        this.#toolBar.destroyAllControls();
    }

    /**
     * Replaces the current toolbar with another toolbar. The old toolbar will
     * be returned.
     *
     * @param {ToolBar} toolBar
     *  The new toolbar to be inserted. This menu instance takes ownership of
     *  the passed toolbar!
     *
     * @returns {Opt<ToolBar>}
     *  The toolbar that has been removed from this menu. Caller MUST take
     *  ownership!
     */
    replaceToolBar(toolBar) {

        // if it's already there, get out here
        if (this.#toolBar === toolBar) { return undefined; }

        // save reference to the toolbar as method result
        const oldToolBar = this.#toolBar;

        // remove control dropdown menus from internal focus handling
        for (const control of oldToolBar.yieldControls()) {
            this.#unregisterSubMenu(control);
        }

        // stop listening to the toolbar, and remove it from the DOM
        this.stopListeningTo(oldToolBar);
        oldToolBar.$el.detach();

        // insert the new toolbar into the own DOM
        this.#registerToolBar(toolBar);

        // prevent auto-closing this menu when using embedded dropdown menus
        for (const control of toolBar.yieldControls()) {
            this.#registerSubMenu(control);
        }

        // refresh own layout
        this.refresh();

        // old toolbar back to caller
        return oldToolBar;
    }

    // protected methods ------------------------------------------------------

    /**
     * Prepares updating the position and size of this menu. Removes all custom
     * scroll settings from the inner toolbar.
     */
    /*protected override*/ implPrepareLayout() {
        this.#toolBar.refresh({ immediate: true });
    }

    // private methods --------------------------------------------------------

    #registerToolBar(toolBar) {

        // insert the new toolbar into the own DOM
        this.#toolBar = toolBar;
        this.$body.append(toolBar.el);

        // refresh the popup node when the size of the toolbar has been changed
        this.listenTo(toolBar, "toolbar:resize", () => this.refresh({ immediate: true }));
        // keep action buttons in the footer section up-to-date
        this.listenTo(toolBar, "toolbar:commit toolbar:change", this.#updateActionButtons);
    }

    #registerSubMenu(control) {
        this.registerFocusableNodes(control.menu?.el);
    }

    #unregisterSubMenu(control) {
        this.unregisterFocusableNodes(control.menu?.el);
    }

    #createFooterButton(options) {
        const buttonNode = createButton(options);
        this.$footer.append(buttonNode);
        return $(buttonNode);
    }

    /**
     * Updates the action buttons contained in the footer, if existing.
     */
    #updateActionButtons() {
        const isBusy = this.isBusy();
        for (const entry of this.#actionButtons) {
            const enabledFn = entry.options?.enabled;
            const enabled = !isBusy && (!enabledFn || (enabledFn.call(this) === true));
            enableElement(entry.$button[0], enabled);
        }
    }
}
