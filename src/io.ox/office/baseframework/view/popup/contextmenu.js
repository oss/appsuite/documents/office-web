/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { ary } from "@/io.ox/office/tk/algorithms";
import { Rectangle } from "@/io.ox/office/tk/dom";
import { ToolTip } from "@/io.ox/office/tk/popup/tooltip";

import { CompoundMenu } from "@/io.ox/office/baseframework/view/popup/compoundmenu";

// class ContextMenu ==========================================================

/**
 * An instance of this class represents a context menu bound to a specific DOM
 * element. Right-clicking that DOM element, or pressing specific keys (CONTEXT
 * key) or key combinations (e.g. Shift+F10) while the element is focused or
 * contains the browser focus will show this context menu.
 */
export class ContextMenu extends CompoundMenu {

    // static functions -------------------------------------------------------

    /**
     * Triggers a special DOM event at the specified target node that causes to
     * show a context menu (instance of `ContextMenu`) listening to that node.
     *
     * @param {NodeOrJQuery} targetNode
     *  The DOM element that will emit the internal context menu event.
     *
     * @param {JEvent} sourceEvent
     *  The original DOM event that caused to show the context menu. The
     *  properties `pageX` and `pageY` will be used as initial anchor point for
     *  the context menu.
     *
     * @param {object} [options]
     *  Additional options to be passed to the event handlers.
     */
    static triggerShowEvent(targetNode, sourceEvent, options) {
        $(targetNode).first().trigger("docs:contextmenu:show", sourceEvent, options);
    }

    // properties -------------------------------------------------------------

    /** A permanent tooltip shown for the context menu. */
    #toolTip/*: ToolTip*/;

    /** Controller keys specifying whether the context menu is enabled. */
    #enableKeys/*: string[]*/;

    // constructor ------------------------------------------------------------

    /**
     * @param {BaseView} docView
     *  The document view instance containing this context menu.
     *
     * @param {NodeOrJQuery} sourceNode
     *  The DOM element used as event source for context events (right mouse
     *  clicks, or `contextMenu` events for specific keyboard shortcuts). If
     *  this object is a jQuery collection, uses the first DOM element it
     *  contains.
     *
     * @param {Object} [config]
     *  Optional parameters:
     *  - {string} [config.selector]
     *    A JQuery selector for specific descendant elements of the passed
     *    event source node. The context menu will only be shown, if the mouse
     *    or keyboard events originate from the selected nodes.
     *  - {string|string[]} [config.enableKey]
     *    The key of a controller item, or an array of controller item keys,
     *    that specify whether the context menu is enabled. Uses the enabled
     *    state of the controller items registered for the specified keys. All
     *    specified items must be enabled to enable the context menu. If
     *    omitted, the context menu will always be enabled.
     *  - {number} [config.delay=0]
     *    The delay time to be waited before this context menu will show up
     *    after it has received a context event.
     */
    constructor(docView, sourceNode, config) {

        // base constructor
        super(docView, {
            anchorPadding: 0,
            hideDisabled: true,
            ...config
        });

        this.$sourceNode = $(sourceNode);

        // private properties
        this.#toolTip = this.member(new ToolTip({ anchor: this.$el, anchorAlign: "center" }));
        this.#enableKeys = ary.wrap(config?.enableKey);

        this.$el.addClass("context-menu");

        // DOCS-2298 when the Viewer plugs a Documents app,
        // avoid the context menu being hidden behind the Viewer overlay
        if (this.docApp.isViewerMode()) {
            this.$el.css("z-index", 1000);
        }

        // listen to global event triggered from `ContextMenu.triggerShowEvent`
        this.listenTo(this.$sourceNode, "docs:contextmenu:show", (_event, sourceEvent, options) => {
            this.#showContextMenu(sourceEvent, options);
            return false;
        }, { delegated: config?.selector });

        // hide displayed context menu, if controller item changes to disabled state
        if (this.#enableKeys.length) {
            this.listenTo(docView, "controller:update", this.#contextMenuControllerHandler);
        }

        // additional processing when showing/hiding the context menu
        this.on("popup:show", this.#contextMenuShowHandler);
        this.on("popup:hide", this.#contextMenuHideHandler);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this context menu is currently enabled (whether it will
     * be shown after right-clicking the anchor node, or using the keyboard).
     *
     * @returns {boolean}
     *  Whether this context menu is currently enabled.
     */
    isEnabled() {
        const controller = this.docApp.docController;
        return this.#enableKeys.every(key => controller.isItemEnabled(key));
    }

    /**
     * Sets a tooltip that will be shown permanently while this context menu is
     * visible.
     *
     * @param {string} label
     *  The text for the tooltip text to be shown. If set to an empty string,
     *  the context menu will not show a tooltip anymore.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.top=false]
     *    If set to `true`, the tooltip will be shown on top of the context
     *    menu (if possible). By default, the tooltip will be shown below the
     *    context menu.
     */
    setToolTip(label, options) {
        this.#toolTip.setText(label);
        const anchorBorder = options?.top ? ["top", "bottom"] : ["bottom", "top"];
        this.#toolTip.setAnchor(this.$el, { anchorBorder });
        if (this.isVisible()) { this.#toolTip.toggle(label.length > 0); }
    }

    // protected methods ------------------------------------------------------

    /**
     * Will be called before actually showing the context menu, after a browser
     * event (right mouse click, keyboard shortcut, etc.) has been caught.
     * Intended to be overwritten by subclasses.
     *
     * @param {JEvent} sourceEvent
     *  The originating JQuery event (the mouse click or keyboard event). The
     *  event contains the properties `pageX` and `pageY` which will be used as
     *  anchor position for the context menu. Initially, these properties have
     *  been set to the page position of the source event. The method may
     *  manipulate these properties to modify the position of the context menu.
     *
     * @returns {boolean|void}
     *  Whether to actually show the context menu. Returning `false` vetoes the
     *  context menu. Returning nothing or `true` will show the context menu.
     */
    /*protected*/ implPrepareContext(/*sourceEvent, options*/) {
        return true;
    }

    // private methods --------------------------------------------------------

    /**
     * Callback for the "popup:show" event of this context menu.
     */
    #contextMenuShowHandler() {
        this.#toolTip.show();
    }

    /**
     * Callback for the "popup:hide" event of this context menu.
     */
    #contextMenuHideHandler() {
        this.#toolTip.hide();
        // fix for bug 46769
        this.docView.grabFocus();
    }

    /**
     * Handles "docs:contextmenu:show" events of the source node, triggered by
     * the static function `ContextMenu.triggerShowEvent`.
     */
    #showContextMenu(sourceEvent, options) {

        // bug 38770: fail-safety: prevent context menus in busy state
        if (this.docView.isBusy()) { return; }

        // add missing page coordinates (keyboard events)
        sourceEvent.pageX ??= 0;
        sourceEvent.pageY ??= 0;

        // call the preparation method (also if the context menu is disabled,
        // e.g. to select an element on click, although the context menu will not appear)
        const showMenu = this.implPrepareContext(sourceEvent, options);

        // the effective anchor position of the context menu (may have been changed by preparation method)
        const { pageX, pageY } = sourceEvent;

        // do not show the context menu, if an event listener has vetoed, or if
        // the controller item bound to the context menu is disabled (now)
        if ((showMenu !== false) && this.isEnabled() && (pageX >= 0) && (pageY >= 0)) {

            // set the effective position as anchor
            this.hide();
            this.setAnchor(new Rectangle(pageX, pageY, 1, 1));

            // show the context menu (with delay if configured)
            if (this.config.delay) {
                this.setTimeout(() => this.show(), this.config.delay);
            } else {
                this.show();
            }
        }
    }

    /**
     * Hides the context menu if any of the controller items specified with
     * the constructor option "enableKey" has been disabled.
     */
    #contextMenuControllerHandler(changedItems) {

        // nothing to do, if the context menu is not visible
        if (!this.isVisible()) { return; }

        // whether to hide the context menu due to changed controller item states
        const hideMenu = this.#enableKeys.some(enableKey => {
            const changedItem = changedItems.get(enableKey);
            return changedItem && !changedItem.enabled;
        });

        if (hideMenu) {
            this.hide();
            this.docView.grabFocus();
        }
    }
}
