/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeSource } from "@/io.ox/office/tk/dom";
import { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";
import { Group } from "@/io.ox/office/tk/control/group";

import { BaseView } from "@/io.ox/office/baseframework/view/baseview";
import { ToolBarAddSectionOptions, ToolBarAddControlOptions, ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";

// types ======================================================================

export interface CompoundMenuConfig {
    hideDisabled?: boolean;
    cancelButton?: string | boolean;
}

export interface CompoundMenuAddControlOptions extends ToolBarAddControlOptions {
    sticky?: boolean;
}

export interface ICompoundContainer<DocViewT extends BaseView> {

    readonly docModel: DocViewT["docModel"];
    readonly docView: DocViewT;
    readonly docApp: DocViewT["docApp"];

    addNodes(source: NodeSource, options?: object): void;
    addSection(id: string, options?: ToolBarAddSectionOptions | string): void;
    addContainer(options?: object): void;
    addControl<ControlT extends Group<any>>(key: string | null, control: ControlT, options?: CompoundMenuAddControlOptions): ControlT;
    addActionButton(options?: object): JQuery<HTMLButtonElement>;
    replaceToolBar(toolBar: ToolBar): ToolBar;
}

// class CompoundMenu =========================================================

export interface CompoundMenu<DocViewT extends BaseView> extends ICompoundContainer<DocViewT> { }

export class CompoundMenu<DocViewT extends BaseView> extends BaseMenu implements ICompoundContainer<DocViewT> {
    public constructor(docView: DocViewT, config?: CompoundMenuConfig);
}
