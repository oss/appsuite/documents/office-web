/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { getNodePositionInPage } from "@/io.ox/office/tk/utils";

import { CompoundMenu } from "@/io.ox/office/baseframework/view/popup/compoundmenu";

import "@/io.ox/office/baseframework/view/popup/floatingmenu.less";

// class FloatingMenu =========================================================

/**
 * An instance of this class represents a floating modeless popup menu with a
 * draggable title bar.
 */
export class FloatingMenu extends CompoundMenu {

    /**
     * @param {BaseView} docView
     *  The document view instance containing this menu.
     *
     * @param {string} titleLabel
     *  The title shown on top of the menu.
     *
     * @param {object} [config]
     *  Optional parameters. Supports all options supported by the base class
     *  `CompoundMenu`. By default, the bounding box (option "boundingBox")
     *  will be set to the application window node (the menu node will not
     *  cover the top-level global AppSuite tab bar); and the option
     *  "autoClose" will be set to `false` (the browser focus may be located
     *  outside the menu node).
     */
    constructor(docView, titleLabel, config) {

        // base constructor
        super(docView, {
            displayType: "embedded", // insert floating menus into "#io-ox-core" to make them covered by modal dialogs
            anchor: () => this.#getDefaultPosition(),
            coverAnchor: true,
            detachedAnchor: true,
            autoClose: false,
            ...config,
            title: titleLabel,
            closable: true
        });

        this.$el.attr("id", this.uid).addClass("floating-menu f6-target");

        // trigger controller update to refresh UI on show/hide (toggle buttons for this menu)
        this.on("popup:show popup:hide", () => this.docApp.docController.update());

        // event handlers for the popup menu
        this.on("popup:show", this.#activateFloatingMenu);
        this.on("popup:beforehide", this.#floatingMenuBeforeHideHandler);

        // activate this menu (move on top of other menus) if focus is inside
        this.listenTo(this.$el, "focusin", this.#activateFloatingMenu);

        // initialize tracking observer for menu movement
        let offset, anchor, options;
        const observer = this.member(new TrackingObserver({
            prepare: event => {
                event.preventDefault(); // prevent focus change (e2e test DOCS-4430F)
                return true;
            },
            start: () => {
                offset = this.$el.offset();
                // store original anchor configuration, will be changed during tracking
                anchor = this.config.anchor;
                options = {
                    anchorBorder: this.config.anchorBorder,
                    anchorAlign: this.config.anchorAlign,
                    anchorPadding: this.config.anchorPadding
                };
                this.#activateFloatingMenu();
            },
            move: record => {
                const left = offset.left + record.offset.x;
                const top = offset.top + record.offset.y;
                // move the menu exactly to the new tracking position (no anchor padding etc.)
                this.setAnchor({ left, top }, { anchorBorder: "right", anchorAlign: "leading", anchorPadding: 0 });
            },
            cancel: () => {
                this.setAnchor(anchor, options);
            }
        }));

        // observe the label element in the menu title
        observer.observe(this.$header.children(".title-label")[0]);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a default position for the floating menu (the top-right corner
     * of the application pane).
     */
    #getDefaultPosition() {
        const pos = getNodePositionInPage(this.docView.$appPaneNode);
        return { left: pos.left + pos.width - this.$el.outerWidth() - 40, top: pos.top + 5 };
    }

    /**
     * Activates this menu, i.e. moves it on top of other floating menus.
     */
    #activateFloatingMenu() {
        this.$el.siblings(".io-ox-office-main.floating-menu").removeClass("active");
        this.$el.addClass("active");
    }

    /**
     * Moves the browser focus back to the application before this menu will be
     * hidden.
     */
    #floatingMenuBeforeHideHandler() {
        this.$el.removeClass("active");
        this.docView.grabFocus();
    }
}
