/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import yell, { type YellConfig } from "$/io.ox/core/yell";

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import type { CreateElementLabelOptions, CreateControlCaptionOptions } from "@/io.ox/office/tk/dom";
import { createSpan, createAnchor, createIcon, createButton, containsFocus } from "@/io.ox/office/tk/dom";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { EObject } from "@/io.ox/office/tk/objects";

import type { BaseView } from "@/io.ox/office/baseframework/view/baseview";

import "@/io.ox/office/baseframework/view/popup/alertbanner.less";

const { min, floor } = Math;

// types ======================================================================

/**
 * Enumeration for different alert banner types. The type specifies which icon
 * will be shown in front of the message text.
 */
export enum AlertBannerType {

    /**
     * A success alert (i.e. a green checkmark).
     */
    SUCCESS = "success",

    /**
     * A question alert (question mark and buttons for interaction).
     */
    QUESTION = "question",

    /**
     * An informational alert.
     */
    INFO = "info",

    /**
     * A warning alert.
     */
    WARN = "warning",

    /**
     * An error alert.
     */
    ERROR = "error"
}

/**
 * Configuration for high-priority alert banners.
 */
export interface AlertBannerPriorityOptions {

    /**
     * Specifies whether the alert banner has high priority. A visible alert
     * banner with high priority will prevent to show another alert banner
     * without priority flag.
     *
     * Default value if `false` except for error alerts which have priority by
     * default. To suppress this behavior for error alerts, this option must
     * explicitly be set to the value `false`.
     */
    priority?: boolean;
}

/**
 * Configuration for a hyperlink to be shown in an alert banner.
 *
 * The option `label` represents the display text shown as hyperlink. If
 * omitted, the specified URL will be displayed instead.
 */
export interface AlertBannerLinkConfig extends CreateElementLabelOptions {

    /**
     * The mandatory URL for the hyperlink.
     */
    href: string;
}

/**
 * Configuration for an action link to be shown in an alert banner.
 *
 * If the option `label` will be omitted, an icon MUST be specified in order to
 * get a visible link element.
 */
export interface AlertBannerActionConfig extends CreateControlCaptionOptions {

    /**
     * The mandatory key of a controller item that will be bound to the action
     * link. The link will be shown if the controller item is enabled, and upon
     * clicking the link, the controller item will be executed.
     */
    itemKey: string;
}

/**
 * A callback function that will be invoked when an alert banner actually gets
 * (re-)rendered.
 *
 * @param messageNode
 *  The content DOM element of the alert banner, containing the message text
 *  and the other visible elements (e.g.: progress bar, action link).
 *
 * @param alertNode
 *  The root DOM node of the alert banner (parent of the message node).
 */
export type AlertBannerRenderFn = (messageNode: JQuery<HTMLDivElement>, alertNode: JQuery<HTMLDivElement>) => void;

/**
 * Configuration for an alert banner shown modeless on top of the document.
 */
export interface AlertBannerConfig extends AlertBannerPriorityOptions {

    /**
     * The type of the alert banner. Default value is `INFO`.
     */
    type?: AlertBannerType;

    /**
     * An optional headline shown above the message text specified in the
     * property `message`.
     */
    headline?: string;

    /**
     * The mandatory main message text shown in the alert banner.
     */
    message: string;

    /**
     * An additional message text shown with a smaller font below the main
     * message text specified in the property `message`.
     */
    subMessage?: string;

    /**
     * The time to show the alert banner, in milliseconds. If set to the value
     * `Infinity`, a permanent alert banner will be shown. The default duration
     * is dependent on the alert type: permanent for error alerts, and specific
     * durations defined by the core toolkit for all other types.
     */
    duration?: number;

    /**
     * If set to `true`, or to an absolute timestamp, a progress bar will be
     * displayed inside the alert banner. The option `duration` MUST be set
     * explicitly to a finite number for this option to take effect (i.e., the
     * alert banner will be closed automatically when the progress bar is
     * full).
     *
     * The passed timestamp will be used as start time for the progress bar. In
     * case `true` has been passed, the time when the alert banner will be
     * shown the first time will be used.
     */
    progress?: boolean | number;

    /**
     * Settings for an arbitrary static hyperlink that will be shown below the
     * message text.
     */
    hyperlink?: AlertBannerLinkConfig;

    /**
     * Settings for an action link shown below the message text. The link will
     * be bound to an arbitrary item of the application controller.
     */
    action?: AlertBannerActionConfig;

    /**
     * A callback function that will be invoked when the alert banner actually
     * gets (re-)rendered.
     */
    renderFn?: AlertBannerRenderFn;

    /**
     * Set to `true` to prevent that the alert banner shows a close button, and
     * that it will be closed automatically, when the user clicks somewhere
     * else in the document. Default value is `false`.
     */
    disableCloser?: boolean;

    /**
     * If set to `true`, the entire application will shut down when the alert
     * banner will be closed. Default value is `false`.
     */
    quitOnClose?: boolean;

    /**
     * Specifies whether the alert banner will be restored after it was hidden
     * (manually by the user, or automatically because another application has
     * been activated), and the application will be shown again.
     *
     * Default value if `false` except for error alerts which will be restored
     * by default. To suppress this behavior for error alerts, this option must
     * explicitly be set to the value `false`.
     */
    restoreHidden?: boolean;

    /**
     * If set to `true`, the alert banner will not been shown at all if this
     * application is plugged (viewer mode). Default value is `false`.
     */
    skipPlugged?: boolean;
}

// logger =====================================================================

/**
 * Logger for tracking yell behavior, bound to the
 * debug configuration flag "office:log-yell".
 */
export const yellLogger = new Logger("office:log-yell", {
    recordAll: true,
    tag: "YELL",
    tagColor: 0xFFFF00
});

// public functions ===========================================================

/**
 * Returns whether the passed configuration represents an error alert banner.
 *
 * @param config
 *  The configuration of an alert banner.
 *
 * @returns
 *  Whether the passed configuration represents an error alert banner.
 */
export function isErrorAlert(config: AlertBannerConfig): boolean {
    return config.type === AlertBannerType.ERROR;
}

/**
 * Returns whether the passed configuration represents an alert banner that
 * will be restored when the application becomes visible again.
 *
 * @param config
 *  The configuration of an alert banner.
 *
 * @returns
 *  Whether the passed configuration represents an alert banner that will be
 *  restored when the application becomes visible again.
 */
export function isRestoreAlert(config: AlertBannerConfig): boolean {
    return config.restoreHidden ?? isErrorAlert(config);
}

/**
 * Returns whether the passed configuration represents an alert banner with
 * high priority, preventing low-priority banners to be shown while active.
 *
 * @param config
 *  The configuration of an alert banner.
 *
 * @returns
 *  Whether the passed configuration represents an alert banner with high
 *  priority.
 */
export function isPriorityAlert(config: AlertBannerConfig): boolean {
    return config.priority ?? isErrorAlert(config);
}

// class AlertBanner ==========================================================

/**
 * Instances of this class represent the contents and settings of an alert
 * banner and implements displaying and hiding the alert banner according to
 * the visibility state of the parent application.
 *
 * If constructed while the application is active and visible, the alert banner
 * will be rendered immediately; otherwise it starts to listen to activation
 * events of the application for rendering.
 *
 * @param docView
 *  The view instance of the application owning this alert banner wrapper.
 *
 * @param config
 *  The configuration of the alert banner.
 */
export class AlertBanner extends EObject {

    // properties -------------------------------------------------------------

    readonly #docView: BaseView;
    readonly #config: Readonly<AlertBannerConfig>;
    readonly #restore: boolean;
    #active = true;
    #startTime: Opt<number>;

    // constructor ------------------------------------------------------------

    constructor(docView: BaseView, config: AlertBannerConfig) {
        super();

        // initialize properties
        this.#docView = docView;
        this.#config = config;

        // whether the application is currently active, and the view is not hidden
        const viewVisible = docView.isVisible();

        // restore mode (DOCS-1329: never restore an alert banner later in viewer mode)
        this.#restore = (isRestoreAlert(config) || !viewVisible) && !docView.docApp.isViewerMode();

        // immediately show the alert banner if the view is visible
        if (viewVisible) { this.#renderAlert(); }

        // listen to "show" events of the view to (re-)render alert banner
        if (this.#restore) { this.listenTo(docView, "view:show", this.#renderAlert); }

        // listen to "hide" events of the view to hide the alert banner
        // (non-clickable banners do not auto-hide)
        this.listenTo(docView, "view:hide", () => yell.close());
    }

    protected override destructor(): void {
        yell.close();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this alert banner is still active (i.e. whether it is
     * currently visible, or it is in pending state to be shown when the
     * application will be activated again).
     *
     * @returns
     *  Whether this alert banner is still active.
     */
    isActive(): boolean {
        return this.#active;
    }

    /**
     * Returns whether this alert banner has high priority (i.e. it cannot be
     * replaced by an alert banner with low priority).
     *
     * @returns
     *  Whether this alert banner has high priority.
     */
    isPriority(): boolean {
        return isPriorityAlert(this.#config);
    }

    /**
     * Returns whether this alert banner can be closed by the user, i.e. shows
     * a "Close" button, and will be closed automatically when clicking
     * somewhere else in the page.
     *
     * @returns
     *  Whether this alert banner can be closed by the user.
     */
    isClosable(): boolean {
        return !this.#config.disableCloser;
    }

    // private methods --------------------------------------------------------

    /**
     * Creates and renders the alert banner.
     */
    #renderAlert(): void {

        // do not show inactive (hidden, non-restoreable) alert banners
        if (!this.#active) { return; }

        // shortcuts to own properties
        const docView = this.#docView;
        const config = this.#config;

        // initialize start time if alert is shown for the first time
        if (!this.#startTime) {
            this.#startTime = is.number(config.progress) ? config.progress : Date.now();
        }
        const startTime = this.#startTime;

        // the configuration for the core alert function
        const yellConfig: YellConfig = {};

        // process alert type (type "question" not supported by core alert, see below)
        const type = config.type || AlertBannerType.INFO;
        yellConfig.type = (type === AlertBannerType.QUESTION) ? AlertBannerType.INFO : type;

        // set the message text (required by core yell)
        yellConfig.message = config.message;

        // initialize duration to show the alert banner
        const duration = config.duration;
        if (is.number(duration)) {
            // infinite banner specified as -1 in core alerts
            yellConfig.duration = Number.isFinite(duration) ? duration : -1;
        }

        // initialize duration for alert banners with progress bar (reduce duration
        // according to initial display time when restoring the alert banner)
        const showProgress = config.progress && Number.isFinite(duration) && (duration! > 0);
        if (showProgress) {
            // reduce duration if alert is being restored
            yellConfig.duration = duration! + startTime - Date.now();
            // do not show the alert for less than a second
            if (yellConfig.duration < 1000) {
                this.#active = false;
                return;
            }
        }

        // bug 28554: default behavior: no auto-close for error messages
        if (isErrorAlert(this.#config) && !("duration" in yellConfig)) {
            yellConfig.duration = -1;
        }

        // explicitly set the "closeOnClick" flag (defaults to `true` in core yell)
        yellConfig.closeOnClick = this.isClosable();

        // create and show the notification DOM node
        const alertNode = yell(yellConfig);
        // bug 57923: core alert returns undefined when message text is missing
        if (!alertNode) {
            yellLogger.error("$badge{AlertBanner} show: no alert banner created for configuration:", config);
            this.#active = false;
            return;
        }
        alertNode.addClass("io-ox-office-alert");

        // wait for deferred initialization of the DOM node
        alertNode.one("notification:appear", () => {

            // the message node as target for additional contents
            const messageNode = alertNode.find<HTMLDivElement>(".message");

            // set the question icon manually (not supported by core notification)
            if (type === AlertBannerType.QUESTION) {
                alertNode.find(".icon").empty().append(createIcon("bi:question"));
            }

            // append a static hyperlink to the alert message
            const href = config.hyperlink?.href;
            if (href) {
                const anchorNode = createAnchor(href, { label: _.noI18n(href), ...config.hyperlink });
                anchorNode.target = "_blank";
                anchorNode.rel = "noopener";
                messageNode.append($("<div>").append(anchorNode));
            }

            // additional smaller message text
            if (config.subMessage) {
                messageNode.append($('<div class="small-text">').append(createSpan({ label: config.subMessage })));
            }

            // add a progress bar that will be completed within a given duration in milliseconds
            if (showProgress) {

                // create the DOM structure of the progress bar
                const barNode = $('<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">');
                const progressNode = $('<div class="progress">').append(barNode);
                messageNode.append(progressNode);

                // initialize progress bar (important for restore mode)
                const elapsedTime = Date.now() - startTime;
                const percent = min(100, floor(elapsedTime / duration! * 100));
                barNode.css({ width: `${percent}%` });

                // create the CSS animation for the progress
                window.setTimeout(() => {
                    const remainingTime = duration! - elapsedTime - 100;
                    barNode.css({ width: "100%", transition: `width ${remainingTime}ms linear` });
                }, 100);
            }

            // create an action link bound to a controller item
            const itemKey = config.action?.itemKey;
            if (itemKey) {

                // the button element to be inserted into the alert node
                const buttonNode = createButton({
                    ...config.action,
                    type: "link",
                    action: "action",
                    // execute the controller item on click
                    click: () => {
                        jpromise.floating(docView.executeControllerItem(itemKey)); // ignore the promise
                        this.#active = false;
                        docView.hideYell({ priority: true }); // ensure self destruction to prevent restoring this alert
                    }
                });

                // insert the link into the alert node
                const wrapperNode = $("<div>").append(buttonNode).appendTo(messageNode);

                // handle visibility of the link depending on controller item state (DOCS-3657: controller update races)
                wrapperNode.toggle(docView.isControllerItemEnabled(itemKey));
                this.listenTo(docView, "controller:update", changedItems => {
                    const itemState = changedItems.get(itemKey);
                    if (itemState) { wrapperNode.toggle(itemState.enabled); }
                });
            }

            // invoke the custom render callback function
            config.renderFn?.(messageNode, alertNode);
        });

        // quit or return focus to application after the notification closes
        alertNode.one("notification:removed", () => {

            // leave active state (unless the alert banner will be restored)
            if (!this.#restore) { this.#active = false; }

            // quit entire application when clicking the close button
            if (config.quitOnClose) {
                jpromise.floating(docView.docApp.quit()); // ignore the promise
                return;
            }

            // move focus back to application
            // bug 33735: only if focus is inside the alert box (e.g. after mouse click)
            const alertWasFocused = containsFocus(alertNode);
            this.setTimeout(() => {
                // clicking the close button ("mousedown" event) moves focus to <body>
                if (alertWasFocused || !this.#docView.hasFocus()) {
                    docView.grabFocus();
                }
            }, 0);
        });
    }
}
