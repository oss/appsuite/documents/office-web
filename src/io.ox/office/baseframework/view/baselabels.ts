/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import type { IconId, CreateButtonOptions } from "@/io.ox/office/tk/dom";
import type { BaseDialogConfig } from "@/io.ox/office/tk/dialogs";

// re-exports =================================================================

export * from "@/io.ox/office/settings/view/labels";

// types ======================================================================

/**
 * Additional configuration options for `BaseDialog` used in applications.
 */
export interface BaseFrameworkDialogConfig extends BaseDialogConfig {

    /**
     * Automatically close the dialog, if the specified controller item becomes
     * disabled.
     */
    autoCloseKey?: string;
}

// constants ==================================================================

/**
 * Default base file name (without extension) for new OX Documents files (will
 * be extended to e.g. "unnamed(1).docx").
 */
//#. default base file name (without extension) for new OX Documents files (will be extended to e.g. 'unnamed(1).docx')
export const DEFAULT_FILE_BASE_NAME = _.noI18n.fix(gt("unnamed")); // DOCS-2370: remove debug-i18n markers

// document name labels -------------------------------------------------------

/**
 * The name of the text documents.
 */
//#. document name of the Text application (German: "Textdokument")
export const TEXT_DOCUMENT_NAME = gt.pgettext("document-name", "Text document");

/**
 * The name of the encrypted text documents.
 */
//#. document name of the Text application (German: "Textdokument (verschlüsselt)")
export const TEXT_DOCUMENT_NAME_ENCRYPTED = gt.pgettext("document-name", "Text document (encrypted)");

/**
 * The label for new text documents.
 */
//#. document name of the Text application (German: "Neues Textdokument")
export const TEXT_DOCUMENT_NAME_NEW = gt.pgettext("document-name", "New text document");

/**
 * The label for new encrypted text documents.
 */
//#. document name of the Text application (German: "Neues Textdokument (verschlüsselt)")
export const TEXT_DOCUMENT_NAME_ENCRYPTED_NEW = gt.pgettext("document-name", "New text document (encrypted)");

/**
 * The name of the spreadsheet documents.
 */
//#. document name of the Spreadsheet application (German: "Tabellendokument")
export const SPREADSHEET_DOCUMENT_NAME = gt.pgettext("document-name", "Spreadsheet");

/**
 * The name of the encrypted spreadsheet documents.
 */
//#. document name of the Spreadsheet application (German: "Tabellendokument (verschlüsselt)")
export const SPREADSHEET_DOCUMENT_NAME_ENCRYPTED = gt.pgettext("document-name", "Spreadsheet (encrypted)");

/**
 * The name for new spreadsheet documents.
 */
//#. document name of the Spreadsheet application (German: "Neues Tabellendokument")
export const SPREADSHEET_DOCUMENT_NAME_NEW = gt.pgettext("document-name", "New spreadsheet");

/**
 * The name for new encrypted spreadsheet documents.
 */
//#. document name of the Spreadsheet application (German: "Neues Tabellendokument (verschlüsselt)")
export const SPREADSHEET_DOCUMENT_NAME_ENCRYPTED_NEW = gt.pgettext("document-name", "New spreadsheet (encrypted)");

/**
 * The name of the presentation documents.
 */
//#. document name of the Presentation application (German: "Präsentation")
export const PRESENTATION_DOCUMENT_NAME = gt.pgettext("document-name", "Presentation");

/**
 * The name of the encrypted presentation documents.
 */
//#. document name of the Presentation application (German: "Präsentation (verschlüsselt)")
export const PRESENTATION_DOCUMENT_NAME_ENCRYPTED = gt.pgettext("document-name", "Presentation (encrypted)");

/**
 * The name for new presentation documents.
 */
//#. document name of the Presentation application (German: "Neue Präsentation")
export const PRESENTATION_DOCUMENT_NAME_NEW = gt.pgettext("document-name", "New presentation");

/**
 * The name for new encrypted presentation documents.
 */
//#. document name of the Presentation application (German: "Neue Präsentation (verschlüsselt)")
export const PRESENTATION_DOCUMENT_NAME_ENCRYPTED_NEW = gt.pgettext("document-name", "New presentation (encrypted)");

// header labels --------------------------------------------------------------

/**
 * A standard header label for a menu with view options.
 */
//#. menu title: view settings (zoom etc.) for a document
export const VIEW_LABEL = gt.pgettext("menu-title", "View");

/**
 * A standard header label for a menu with more options or functions.
 */
//#. menu title: additional options or actions
export const MORE_LABEL = gt.pgettext("menu-title", "More");

/**
 * A standard header label for options in a drop-down menu etc.
 */
export const OPTIONS_LABEL = gt.pgettext("menu-title", "Options");

/**
 * A standard header label for a zoom menu.
 */
export const ZOOM_LABEL = gt.pgettext("menu-title", "Zoom");

// control labels and icons ---------------------------------------------------

/**
 * Standard label for a "Download" button.
 */
export const DOWNLOAD_LABEL = gt("Download");

/**
 * Standard icon for a "Download document" button.
 */
export const DOWNLOAD_ICON: IconId = "bi:cloud-download";

/**
 * Standard label for a "Print document" button.
 */
export const PRINT_LABEL = gt("Print as PDF");

/**
 * Standard icon for a "Print document" button.
 */
export const PRINT_ICON: IconId = "bi:printer";

/**
 * Standard label for a "Send document as mail" button.
 */
export const SEND_AS_MAIL_LABEL = gt("Send as mail");

/**
 * Standard label for a "Attach document to mail" button.
 */
export const ATTACH_AS_FILE_TO_MAIL_LABEL = gt("Attach document to mail");

/**
 * Standard label for a "Attach as PDF to mail" button.
 */
export const ATTACH_AS_PDF_TO_MAIL_LABEL = gt("Attach as PDF to mail");

/**
 * Standard label for a "Send content as mail" button.
 */
export const SEND_CONTENT_AS_NEW_MAIL_LABEL = gt("Send content as mail");

/**
 * Standard icon for a "Send document as mail" button.
 */
export const SEND_MAIL_ICON: IconId = "bi:envelope";

/**
 * Standard label for a "Edit document" button.
 */
//#. Switch from view mode (read-only) to edit mode.
export const EDIT_LABEL = gt("Edit document");

/**
 * Short icon for a "Edit document" button.
 */
//#. Short version for "Edit document"
export const EDIT_SHORT_LABEL = gt("Edit");

/**
 * Standard icon for a "Edit document" button.
 */
export const EDIT_ICON: IconId = "bi:pencil";

/**
 * Standard label for a "Reload document" button.
 */
//#. Full reload, e.g. after internal error
export const RELOAD_LABEL = gt("Reload document");

/**
 * Short label for a "Reload document" button.
 */
//#. Short version for "Reload document"
export const RELOAD_SHORT_LABEL = gt("Reload");

/**
 * Standard icon for a "Reload document" button.
 */
export const RELOAD_ICON: IconId = "bi:arrow-repeat";

/**
 * Standard label for a "Load restored document" button.
 */
//#. Full load restored, e.g. after a save error
export const LOAD_RESTORED_LABEL = gt("Load restored document");

/**
 * Short label for a "Reload document" button.
 */
//#. Short version for "Load restored document"
export const LOAD_RESTORED_SHORT_LABEL = gt("Load");

/**
 * Standard icon for a "Reload document" button.
 */
export const LOAD_RESTORED_ICON: IconId = "bi:arrow-repeat";

/**
 * Label for an entry "Fit to screen width" in a zoom options menu.
 */
//#. Zoom option: fit pages to current display width
export const ZOOM_SCREEN_WIDTH_LABEL = gt("Fit to screen width");

/**
 * Label for an entry "Fit to screen size" in a zoom options menu.
 */
//#. Zoom option: fit pages to current display size (width and height)
export const ZOOM_SCREEN_SIZE_LABEL = gt("Fit to screen size");

/**
 * Standard label for an "Open document" button.
 */
export const OPEN_DOCUMENT_LABEL = gt("Open document");

/**
 * Standard label for an "Add template" button.
 */
export const ADD_TEMPLATE_LABEL = gt("Add template");

// options for controls -------------------------------------------------------

/**
 * Standard options for the "Close" button.
 */
export const QUIT_BUTTON_OPTIONS: CreateButtonOptions = {
    classes: "always-visible",
    icon: "bi:x-lg",
    tooltip: gt("Close document")
};

/**
 * Standard options for the "Zoom out" button.
 */
export const ZOOMOUT_BUTTON_OPTIONS: CreateButtonOptions = {
    icon: "png:zoom-out",
    tooltip: gt("Zoom out")
};

/**
 * Standard options for the "Zoom in" button.
 */
export const ZOOMIN_BUTTON_OPTIONS: CreateButtonOptions = {
    icon: "png:zoom-in",
    tooltip: gt("Zoom in")
};
