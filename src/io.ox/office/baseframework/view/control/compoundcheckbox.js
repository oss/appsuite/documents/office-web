/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { createButtonNode, setToolTip } from "@/io.ox/office/tk/forms";
import { CheckBox } from "@/io.ox/office/tk/control/checkbox";

import { XCompoundMenuGroup } from "@/io.ox/office/baseframework/view/control/xcompoundmenugroup";

// class CompoundCheckBox =====================================================

/**
 * A checkbox control with a combined dropdown menu containing a complete
 * toolbar instance with completely independent controls.
 *
 * @param {BaseView} docView
 *  The document view instance containing this control.
 *
 * @param {object} [initOptions]
 *  Configuration options. Supports all options also supported by the base
 *  class `CheckBox`, and the mixin class `XCompoundMenuGroup`.
 */
export class CompoundCheckBox extends XCompoundMenuGroup.mixin(CheckBox) {

    constructor(docView, config) {

        // the dropdown split button
        const $menuButton = createButtonNode();

        // base constructor
        super(docView, $menuButton, config);

        // add the dropdown button to the group
        setToolTip($menuButton, config);
        this.$el.append($menuButton);
    }
}
