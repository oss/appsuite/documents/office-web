/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { XMenuControl } from "@/io.ox/office/tk/control/xmenucontrol";
import { CompoundMenu } from "@/io.ox/office/baseframework/view/popup/compoundmenu";

// private functions ==========================================================

function mixinCompoundMenuGroup(BaseClass) {

    class SubClass extends XMenuControl.mixin(BaseClass) {

        /**
         * @param {BaseView} docView
         *  The document view instance containing this control.
         *
         * @param {NodeOrJQuery} button
         *  The button element that will toggle the dropdown menu when clicked.
         *
         * @param {object} [config]
         *  Configuration options. Supports all options supported by the base
         *  class, by the mixin class `XMenuControl`, and by the class
         *  `CompoundMenu` (settings for the dropdown menu).
         */
        constructor(docView, button, config) {

            // base constructor (`XMenuControl` mixin class expects button and menu instance)
            super(button, new CompoundMenu(docView, {
                stretchToAnchor: true,
                ...config,
                anchor: () => this.$el
            }), config);

            // public properties
            this.docModel = docView.docModel;
            this.docView = docView;
            this.docApp = docView.docApp;
        }
    }

    // add public method forwarders
    ["addNodes", "addControl", "addContainer", "addSection", "addActionButton"].forEach(method => {
        SubClass.prototype[method] = function (...args) { return this.menu[method](...args); };
    });

    return SubClass;
}

// exports ====================================================================

export const XCompoundMenuGroup = {

    /**
     * Creates and returns a subclass of the passed form control class that
     * owns and controls a `CompoundMenu` element. The mixin class
     * `XMenuControl` will be mixed in automatically into the passed base
     * class.
     *
     * @param {CtorType<ModelT extends Group>} BaseClass
     *  The control base class to be extended.
     *
     * @returns {CtorType<ModelT & XMenuControl<CompoundMenu> & XCompoundMenuGroup>}
     *  The extended control class with support for a dropdown menu with an
     *  embedded toolbar.
     */
    mixin: mixinCompoundMenuGroup
};
