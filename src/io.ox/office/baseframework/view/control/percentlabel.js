/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { DynamicLabel } from "@/io.ox/office/baseframework/view/control/dynamiclabel";

// private functions ==========================================================

function labelResolver(value) {
    //#. a text label showing any number as integral percentage, e.g. "50%"
    //#. %1$d is the value, converted to percent
    //#. the trailing percent sign must remain in the text
    return gt("%1$d%", Math.round(value * 100));
}

// class PercentLabel =========================================================

/**
 * A label control that shows a number as integral percentage.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `DynamicLabel`, except for the option "labelResolver".
 */
export class PercentLabel extends DynamicLabel {

    constructor(config) {

        // base constructor
        super({
            textAlign: "right",
            ...config,
            labelResolver
        });

        this.$el.addClass("percentage-label");
    }
}
