/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { RadioGroup, RadioList, ComboField } from "@/io.ox/office/tk/controls";
import { XAppAccess } from "@/io.ox/office/baseframework/app/xappaccess";

// class AppRadioGroup ========================================================

/**
 * A subclass of `RadioGroup` with `XAppAccess` already mixed in. Provides the
 * additional public properties `docModel` and `docView`.
 */
export class AppRadioGroup extends XAppAccess.mixin(RadioGroup) {

    constructor(docView, config) {

        // base constructor ("docApp" for `XAppAccess`, "config" for `RadioGroup`)
        super(docView.docApp, config);

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
    }
}

// class AppRadioList =========================================================

/**
 * A subclass of `RadioList` with `XAppAccess` already mixed in. Provides the
 * additional public properties `docModel` and `docView`.
 */
export class AppRadioList extends XAppAccess.mixin(RadioList) {

    constructor(docView, config) {

        // base constructor ("docApp" for `XAppAccess`, "config" for `RadioList`)
        super(docView.docApp, config);

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;

        // synchronize shared split values across all controls in the document
        const sharedKey = config?.sharedSplitValueKey;
        if (sharedKey) {
            // notify the other form controls in the view, if this control commits a value
            this.on("group:commit", value => {
                docView.trigger("change:sharedsplitvalue", sharedKey, value);
            });
            // listen to changed value of other form controls with the same shared split key
            this.listenTo(docView, "change:sharedsplitvalue", (key, value) => {
                if (key === sharedKey) { this.setSplitValue(value); }
            });
        }
    }
}

// class AppComboField ========================================================

/**
 * A subclass of `ComboField` with `XAppAccess` already mixed in. Provides the
 * additional public properties `docModel` and `docView`.
 */
export class AppComboField extends XAppAccess.mixin(ComboField) {

    constructor(docView, config) {

        // base constructor ("docApp" for `XAppAccess`, "config" for `ComboField`)
        super(docView.docApp, config);

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
    }
}
