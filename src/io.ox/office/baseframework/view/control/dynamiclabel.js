/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is } from "@/io.ox/office/tk/algorithms";
import { Label } from "@/io.ox/office/tk/control/label";

// class DynamicLabel =========================================================

/**
 * A label control that updates its text dynamically via its current internal
 * value.
 *
 * @param {Object} [config]
 *  Optional parameters:
 *  - {Function} [config.labelResolver]
 *    A callback function that receives the control value, and returns a string
 *    to be shown as label. Will be called in the context of this instance. If
 *    omitted, the current value will be used.
 */
export class DynamicLabel extends Label {

    constructor(config) {

        // base constructor
        super(config);

        // additional options for an ARIA live region for dynamic labels
        this.$el.attr({
            role: "log",
            "aria-live": "assertive",
            "aria-relevant": "additions",
            "aria-atomic": true,
            "aria-readonly": true
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the label text according to current control value.
     */
    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);
        const { labelResolver } = this.config;
        const label = labelResolver ? labelResolver.call(this, value) : value;
        this.setLabel(is.string(label) ? label : "");
    }
}
