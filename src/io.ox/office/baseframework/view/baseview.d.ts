/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { BaseDialogConfig, BaseDialog } from "@/io.ox/office/tk/dialogs";
import { ZoomEventOptions } from "@/io.ox/office/baseframework/utils/zoomstate";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";
import { BaseModel } from "@/io.ox/office/baseframework/model/basemodel";
import { AlertBannerPriorityOptions, AlertBannerConfig } from "@/io.ox/office/baseframework/view/popup/alertbanner";
import { BasePane } from "@/io.ox/office/baseframework/view/pane/basepane";
import { ItemStateMap, ItemExecuteOptions } from "@/io.ox/office/baseframework/controller/controlleritem";

// types ======================================================================

export interface FocusableObject {
    grabFocus(options?: unknown): void;
}

export interface ShowableObject {
    isVisible(): boolean;
    show(): void;
    hide(): void;
}

export interface EnterBusyOptions {
    cancelHandler?: FuncType<void>;
    warningLabel?: string;
    delay?: number;
}

export interface AddViewPaneOptions {
    outer?: boolean;
}

export interface BaseViewConfig {

    /**
     * Predefined zoom steps to be used by this view.
     */
    zoomSteps: number[];

    /**
     * Additional CSS classes to be added to the root node of the application
     * window.
     */
    classes?: string;

    /**
     * A callback function that has to implement moving the browser focus
     * somewhere into the application pane. Will be called from the method
     * `BaseView::grabFocus`. If omitted, the application pane itself will be
     * focused.
     */
    grabFocusHandler?: VoidFunction;

    /**
     * If set to `true`, the container node for the document contents will be
     * focusable and will be registered for global focus traversal with the F6
     * key. Default value is `false`.
     */
    contentFocusable?: boolean;

    /**
     * If set to `true`, the container node for the document contents will be
     * scrollable. By default, the size of the container node is locked and
     * synchronized with the size of the application pane (with regard to
     * content margins, see option "contentMargin" for details). Default value
     * is `false`.
     */
    contentScrollable?: boolean;

    /**
     * The margins between the fixed application pane and the embedded
     * application container node, in pixels. If set to a number, all margins
     * will be set to the specified value. Otherwise, a partial object with the
     * properties "left", "right", "top", and "bottom" for specific margins for
     * each border. Missing properties default to the value zero. The content
     * margins can also be modified at runtime with the method
     * `BaseView::setContentMargin`. Default value is `0`.
     */
    contentMargin?: number | PtRecord<"left" | "right" | "top" | "bottom", number>;
}

/**
 * Type mapping for the events emitted by `BaseView` instances.
 */
export interface BaseViewEventMap {

    /**
     * Will be emitted after this view instance has made its contents visible.
     * In difference to the "show" event of the application window, this event
     * will not be emitted while this view has been hidden explicitly with the
     * method `BaseView::hide`. Calling the method `BaseView::show` while the
     * application is active will cause to finally emit this event.
     */
    "view:show": [];

    /**
     * Will be emitted before this view instance hides its contents, either by
     * deactivating the application (with visible view), or by hiding the
     * contents explicitly by calling the method `BaseView::hide` (while the
     * application is active).
     */
    "view:hide": [];

    /**
     * Will be emitted after this view instance has entered the busy mode, and
     * painted the busy overlay screen.
     */
    "view:busy": [];

    /**
     * Will be emitted after this view instance has left the busy mode, and
     * removed the busy overlay screen.
     */
    "view:idle": [];

    /**
     * Will be emitted after this view instance has refreshed the layout of all
     * registered view panes, e.g. after inserting new view panes, or content
     * nodes into the application pane, after showing/hiding view panes, while
     * and after the browser window is resized.
     */
    "refresh:layout": [];

    /**
     * Will be emitted after the current state (value and/or enabled state) of
     * at least one item of the document controller has been changed. This is
     * the forwarded controller event "change:items".
     *
     * @param itemStates
     *  The descriptors of the changed items in an `ItemStateMap` map.
     */
    "controller:update": [itemStates: ItemStateMap];

    /**
     * Will be emitted directly after an item setter of the document controller
     * has been executed, but before the controller states (of dependent items)
     * have been updated. This is the forwarded controller event
     * "execute:item".
     *
     * @param key
     *  The key of the executed item.
     *
     * @param value
     *  The value passed to the item setter function.
     */
    "controller:execute": [key: string, value: unknown];

    /**
     * Will be emitted after a modal dialog has been shown. This is the
     * forwarded event "show" of the modal dialog.
     *
     * @param dialog
     *  The dialog instance.
     *
     * @param config
     *  The configuration options passed to the dialog's constructor.
     *
     * @param stackLevel
     *  The number of open (stacked) modal dialogs including the notified
     *  dialog (minimum value is `1` for the root level dialog).
     */
    "dialog:show": [dialog: BaseDialog, config: BaseDialogConfig, stackLevel: number];

    /**
     * Will be emitted after a modal dialog has been closed. This is the
     * forwarded event "close" of the dialog.
     *
     * @param dialog
     *  The dialog instance.
     *
     * @param config
     *  The configuration options passed to the dialog's constructor.
     *
     * @param stackLevel
     *  The number of open (stacked) modal dialogs including the notified
     *  dialog (minimum value is `1` for the root level dialog).
     */
    "dialog:close": [dialog: BaseDialog, config: BaseDialogConfig, stackLevel: number];
}

// class BaseView =============================================================

export class BaseView<EvtMapT extends BaseViewEventMap = BaseViewEventMap> extends ModelObject<BaseModel, EvtMapT> implements FocusableObject, ShowableObject {

    readonly $winBodyNode: JQuery;
    readonly $appPaneNode: JQuery;
    readonly $contentRootNode: JQuery;
    readonly $appContentNode: JQuery;

    protected constructor(docApp: BaseApplication, docModel: BaseModel, config: BaseViewConfig);

    hasFocus(): boolean;
    hasAppFocus(): boolean;
    grabFocus(options?: ItemExecuteOptions): void;

    isVisible(): boolean;
    show(): void;
    hide(): void;

    enterBusy(options?: EnterBusyOptions): void;
    leaveBusy(): void;
    updateBusyProgress(progress: number): void;

    yell(config: AlertBannerConfig): boolean;
    hideYell(options?: AlertBannerPriorityOptions): boolean;
    sendA11YAnnouncement(text: string): void;

    getZoomFactor(): number;
    changeZoom(type: string, factor: number, options?: ZoomEventOptions): void;

    isControllerItemEnabled(itemKey: string): boolean;
    getControllerItemValue(itemKey: string): unknown;
    executeControllerItem(itemKey: string, value?: unknown, options?: ItemExecuteOptions): JPromise<unknown>;

    addPane<PaneT extends BasePane>(viewPane: PaneT, options?: AddViewPaneOptions): PaneT;

    getContentRootNode(): JQuery;

    insertHiddenNodes<ET extends NodeOrJQuery>(source: ET): ET;
}
