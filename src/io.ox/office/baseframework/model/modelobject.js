/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { BaseObject } from '@/io.ox/office/tk/objects';
import { XAppAccess } from "@/io.ox/office/baseframework/app/xappaccess";

// class ModelObject ==========================================================

/**
 * A generic base class for model classes. Adds the public properties `docApp`
 * and `docModel` to the instance, and provides helper functions for accessing
 * the application (import state, server requests) via the mix-in class
 * `XAppAccess`.
 *
 * @param {ModelT extends BaseModel} docModel
 *  The document model containing this model object.
 */
export class ModelObject extends XAppAccess.mixin(BaseObject) {

    constructor(docModel, config) {

        // base constructor
        super(docModel.docApp, config);

        // public properties
        this.docModel = docModel;
    }
}
