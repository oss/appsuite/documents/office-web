/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/objects";
import { XAppAccess } from "@/io.ox/office/baseframework/app/xappaccess";
import { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";

// types ======================================================================

/**
 * Type mapping for the events emitted by `BaseModel` instances.
 */
export interface BaseModelEventMap { }

// class BaseModel ============================================================

// declaration merging to add all properties of base interfaces automatically
export interface BaseModel<
    EvtMapT extends BaseModelEventMap = BaseModelEventMap
> extends EObject<EvtMapT>, XAppAccess<BaseApplication> { }

export class BaseModel<
    EvtMapT extends BaseModelEventMap = BaseModelEventMap
> extends EObject<EvtMapT> implements XAppAccess<BaseApplication> {
    protected constructor(docApp: BaseApplication);
}
